#include "FunctionID.h"
#include <iostream>

namespace PentagonFunctions {

template <KinType Tk> FunID<Tk>::FunID(int set_weight, int set_n, int set_i) : w(set_weight), n(set_n), i(set_i) {
    // TODO: implement checks on input
}


template <KinType Tk> FunID<Tk>::FunID(const std::array<int,2>& arr) : FunID(arr[0],arr[1],0) {}
template <KinType Tk> FunID<Tk>::FunID(const std::array<int,3>& arr) : FunID(arr[0],arr[1],arr[2]) {}


template <KinType Tk> std::ostream& operator<<(std::ostream& s, const FunID<Tk>& f) {
    s << "F[" << wise_enum::to_string(Tk) << "," <<  f.w << "," << f.n;
    if (f.i > 0) { s << "," << f.i; }
    s << "]";
    return s;
}

/**
 * Snippet taken from boost:
 * https://www.boost.org/doc/libs/1_55_0/doc/html/hash/reference.html#boost.hash_combine
 */
template <class T>
inline void hash_combine(std::size_t& seed, const T& v) {
  std::hash<T> hasher;
  seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}

template struct FunID<KinType::m0>;
template std::ostream& operator<<(std::ostream& s, const FunID<KinType::m0>& f);
#ifdef PENTAGON_FUNCTIONS_M1_ENABLED 
template struct FunID<KinType::m1>;
template std::ostream& operator<<(std::ostream& s, const FunID<KinType::m1>& f);
#endif // PENTAGON_FUNCTIONS_M1_ENABLED


} // PentagonFunctions

namespace std {

template <PentagonFunctions::KinType Tk> size_t hash<PentagonFunctions::FunID<Tk>>::operator() (const PentagonFunctions::FunID<Tk>& F) const {
    hash<int> hasher;
    size_t result = hasher(Tk);
    PentagonFunctions::hash_combine(result,F.w);
    PentagonFunctions::hash_combine(result, F.n);
    if (F.i != 0) { PentagonFunctions::hash_combine(result, F.i); }
    return result;
}

template size_t hash<PentagonFunctions::FunID<PentagonFunctions::KinType::m0>>::operator() (const PentagonFunctions::FunID<PentagonFunctions::KinType::m0>&) const;
template size_t hash<PentagonFunctions::FunID<PentagonFunctions::KinType::m1>>::operator() (const PentagonFunctions::FunID<PentagonFunctions::KinType::m1>&) const;

} // namespace std
