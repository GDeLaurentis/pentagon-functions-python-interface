#pragma once

#include "Kin.h"

#include <map>
#include <set>
#include <vector>

namespace PentagonFunctions {

template <KinType KT, typename T> struct PhaseSpacePath;

#ifdef PENTAGON_FUNCTIONS_M1_ENABLED

template <typename T> struct PhaseSpacePath<KinType::m1, T> {
    using Ptype = std::array<T, 6>;
    constexpr static size_t Nroots = 10;
    std::vector<Ptype> path_points;
    std::vector<T> t_break_points;
    /**
     * Constructors a path consisting of line segments withing the phase space.
     *
     * TODO: nontrivial part of doing this with multiple points, etc.
     */
    PhaseSpacePath(const Ptype& start, const Ptype& end);
    /**
     * Trivial constructor by accepting the list of points.
     *
     * TODO: check that all points are within phase space
     */
    PhaseSpacePath(const std::vector<Ptype>& set_path_points) : path_points(set_path_points) {}
    /**
     * Given a path parameter t : [0,1]
     * return
     *
     * {Kin(t), dv(point(t)), dlogs(point(t)), dlogs of roots (t)}
     */
    std::tuple<Kin<T,KinType::m1>, Ptype, Ptype, std::array<T, Nroots>> operator()(T t) const;
    /**
     * Return a list of path parameter values at which the path should be broken up.
     *
     * @param indices of letters (C++ indices, 0-based) which should be checked for vanishing.
     */
    std::vector<T> get_path_break_points(const std::vector<size_t>& letters_to_check);
    /**
     * Computes quadratic letters that can vanish on the line segment as
     *
     * c1 * (t-t0) + c2 * (t-t0)^2,
     *
     * where t0 is the value of the path parameter where the letter is vanishing.
     * Only those letters contained in vanishing_letters_per_segment list are adjusted.
     *
     * @param kin to adjust
     *
     */
    void set_divergent_letters(T t, Kin<T,KinType::m1>& k);
    /**
     * Same as above, but can use more precise value tc of difference between current t and the closes integration boundary.
     */
    void set_divergent_letters(T t, T tc, Kin<T,KinType::m1>& k);
    /**
     * Returns true if any of the give letters can vanish on any of the segments on this path.
     */
    bool can_letters_vanish_Q(const std::vector<size_t>& letters_to_check);

  private:
    /**
     * For each path segment contains a map of {letter index -> list of its singularities}.
     * Filled by get_path_break_points.
     */
    std::vector<std::map<size_t, std::set<T>>> vanishing_letters_per_segment;
};

namespace m1_set {

template <typename T> extern const Kin<T, KinType::m1> base_point{T(1), T(3), T(2), T(-2), T(7), T(-2)};

template <typename Tkin, typename T = typename Tkin::value_type> std::optional<int> number_of_gram5_zeroes_on_path(const Tkin& vb, const Tkin& ve);
template <typename Tkin> bool line_lies_within_physical_phase_space(const Tkin& vb, const Tkin& ve) {
    auto nn = number_of_gram5_zeroes_on_path(vb, ve);
    // if it was not possible to reliably determine the number of zeroes, we are taking the worst case here and saying the the line does not lie in the pase space
    if(!nn) return false;
    return nn.value() == 0; 
}
template <typename Tkin> std::vector<Tkin> construct_line_segments(const Tkin& vb, const Tkin& ve);

}

#endif


} // namespace PentagonFunctions
