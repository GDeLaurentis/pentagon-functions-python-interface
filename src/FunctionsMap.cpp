#include "FunctionID.h"
#include "Integrate.h"
#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "FunctionsMap.hpp"

namespace PentagonFunctions {

template <> std::unordered_map<FunID<KinType::m0>, FunctionObjectType<double,KinType::m0>> functions_map<double,KinType::m0> = {};

template void detail::fill_function_map<double,KinType::m0>();

template FunctionObjectType<double,KinType::m0> FunID<KinType::m0>::get_evaluator<double>() const;

#ifdef PENTAGON_FUNCTIONS_M1_ENABLED 
template <> std::unordered_map<FunID<KinType::m1>, FunctionObjectType<double,KinType::m1>> functions_map<double,KinType::m1> = {};

template void detail::fill_function_map<double,KinType::m1>();

template FunctionObjectType<double,KinType::m1> FunID<KinType::m1>::get_evaluator<double>() const;

#endif // PENTAGON_FUNCTIONS_M1_ENABLED


} // namespace PentagonFunctions
