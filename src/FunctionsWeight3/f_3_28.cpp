#include "f_3_28.h"

namespace PentagonFunctions {

template <typename T> T f_3_28_abbreviated (const std::array<T,33>&);

template <typename T> class SpDLog_f_3_28_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_28_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(2) * kin.v[0]) / T(3) + bc<T>[1] * T(2) + (bc<T>[1] + T(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[3]) + (T(-2) * kin.v[0] * kin.v[4]) / T(3) + T(2) * kin.v[2] * kin.v[4] + T(2) * kin.v[3] * kin.v[4] + bc<T>[1] * (-kin.v[4] + T(-2)) * kin.v[4] + prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(2) + kin.v[1]) + (-kin.v[4] + T(-2)) * kin.v[4]);
c[1] = (bc<T>[0] * int_to_imaginary<T>(2) + bc<T>[1] * T(2) + T(4)) * kin.v[1] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[4] + bc<T>[1] * T(-2) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[15] * (abb[16] * T(-4) + abb[3] * abb[4] * T(-2) + abb[4] * abb[14] * T(-2) + abb[1] * (abb[2] * T(-2) + abb[4] * T(2)) + abb[4] * (bc<T>[0] * int_to_imaginary<T>(2) + abb[4] * T(2)) + abb[2] * (abb[2] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[3] * T(2) + abb[4] * T(2) + abb[14] * T(2)));
    }
};
template <typename T> class SpDLog_f_3_28_W_7 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_28_W_7 (const Kin<T>& kin) {
        c[0] = (T(-8) * kin.v[0] * kin.v[4]) / T(3) + T(-4) * kin.v[1] * kin.v[4] + T(4) * kin.v[2] * kin.v[4] + T(2) * prod_pow(kin.v[4], 2) + kin.v[3] * ((T(-8) * kin.v[0]) / T(3) + T(-4) * kin.v[1] + T(4) * kin.v[2] + (bc<T>[1] * T(-2) + T(2)) * kin.v[3] + T(4) * kin.v[4] + bc<T>[1] * (T(4) + T(4) * kin.v[1] + T(-4) * kin.v[4])) + bc<T>[1] * (T(4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(4) + T(-2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] * kin.v[4] + kin.v[3] * (T(4) + T(4) * kin.v[1] + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(-2) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(4) + bc<T>[1] * T(4) + T(8)) * kin.v[3] + T(8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4] + bc<T>[1] * T(4) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[19] * (abb[8] * T(-8) + abb[5] * abb[6] * T(-4) + abb[6] * abb[14] * T(-4) + abb[1] * (abb[2] * T(-4) + abb[6] * T(4)) + abb[6] * (bc<T>[0] * int_to_imaginary<T>(4) + abb[6] * T(4)) + abb[2] * (abb[2] * T(-8) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[5] * T(4) + abb[6] * T(4) + abb[14] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_28_construct (const Kin<T>& kin) {
    return [&kin, 
            dl20 = DLog_W_20<T>(kin),dl26 = DLog_W_26<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl13 = DLog_W_13<T>(kin),dl12 = DLog_W_12<T>(kin),dl18 = DLog_W_18<T>(kin),dl7 = DLog_W_7<T>(kin),dl19 = DLog_W_19<T>(kin),dl4 = DLog_W_4<T>(kin),dl3 = DLog_W_3<T>(kin),dl2 = DLog_W_2<T>(kin),dl5 = DLog_W_5<T>(kin),dl1 = DLog_W_1<T>(kin),dl17 = DLog_W_17<T>(kin),spdl12 = SpDLog_f_3_28_W_12<T>(kin),spdl7 = SpDLog_f_3_28_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,33> abbr = 
            {dl20(t), rlog(v_path[0]), rlog(-v_path[1]), rlog(v_path[2]), rlog(-v_path[4]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_1(kin_path), f_2_1_11(kin_path), dl26(t) / kin_path.SqrtDelta, f_2_2_1(kin_path), dl28(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl13(t), rlog(v_path[3]), dl12(t), f_2_1_4(kin_path), dl18(t), f_2_1_9(kin_path), dl7(t), dl19(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl4(t), dl3(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl2(t), dl5(t), dl1(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl17(t)}
;

        auto result = f_3_28_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_3_28_abbreviated(const std::array<T,33>& abb)
{
using TR = typename T::value_type;
T z[77];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[3];
z[3] = abb[4];
z[4] = abb[5];
z[5] = abb[6];
z[6] = abb[2];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[13];
z[10] = abb[17];
z[11] = abb[20];
z[12] = abb[23];
z[13] = abb[24];
z[14] = abb[27];
z[15] = abb[28];
z[16] = abb[29];
z[17] = abb[32];
z[18] = abb[14];
z[19] = bc<TR>[0];
z[20] = abb[9];
z[21] = abb[10];
z[22] = abb[11];
z[23] = abb[12];
z[24] = abb[16];
z[25] = abb[18];
z[26] = abb[21];
z[27] = abb[22];
z[28] = abb[25];
z[29] = abb[26];
z[30] = abb[30];
z[31] = abb[31];
z[32] = int_to_imaginary<T>(1) * z[19];
z[33] = z[5] + z[32];
z[34] = T(4) * z[18];
z[35] = T(3) * z[1];
z[36] = T(3) * z[4];
z[33] = T(-5) * z[3] + z[6] + T(-4) * z[33] + z[34] + -z[35] + z[36];
z[33] = z[5] * z[33];
z[37] = T(2) * z[2];
z[38] = T(3) * z[32];
z[39] = T(2) * z[1];
z[40] = z[37] + z[38] + -z[39];
z[41] = (T(3) * z[18]) / T(2) + -z[40];
z[41] = z[18] * z[41];
z[42] = z[27] * z[38];
z[42] = T(3) * z[26] + z[42];
z[41] = z[41] + z[42];
z[43] = T(2) * z[3];
z[44] = -z[2] + z[43];
z[45] = z[1] + z[32];
z[46] = (T(9) * z[6]) / T(2) + T(-7) * z[18] + -z[36] + z[44] + T(4) * z[45];
z[46] = z[6] * z[46];
z[47] = -z[1] + z[2];
z[48] = z[36] * z[47];
z[49] = T(2) * z[8];
z[48] = z[48] + z[49];
z[50] = z[1] + -z[32];
z[50] = z[39] * z[50];
z[51] = (T(-3) * z[3]) / T(2) + T(3) * z[18] + -z[38] + T(4) * z[47];
z[51] = z[3] * z[51];
z[52] = T(2) * z[7];
z[53] = z[29] * z[38];
z[54] = prod_pow(z[19], 2);
z[55] = -z[2] + -z[45];
z[55] = z[2] * z[55];
z[33] = -z[24] + z[33] + z[41] + z[46] + z[48] + z[50] + z[51] + z[52] + z[53] + (T(-17) * z[54]) / T(24) + z[55];
z[33] = z[15] * z[33];
z[46] = T(2) * z[18];
z[50] = T(7) * z[3];
z[51] = T(5) * z[6];
z[55] = -z[1] + z[4];
z[56] = -z[32] + -z[46] + -z[50] + z[51] + z[55];
z[56] = z[6] * z[56];
z[57] = z[5] / T(2);
z[58] = -z[4] + z[32];
z[59] = z[1] + -z[18] + z[58];
z[60] = z[57] + -z[59];
z[61] = T(3) * z[6];
z[62] = T(4) * z[3];
z[63] = -z[60] + -z[61] + z[62];
z[63] = z[5] * z[63];
z[64] = z[4] * z[47];
z[65] = -z[49] + z[64];
z[66] = -(z[39] * z[45]);
z[67] = T(2) * z[32];
z[68] = -z[2] + z[67];
z[69] = z[35] + z[68];
z[69] = z[2] * z[69];
z[70] = z[3] / T(2);
z[71] = -z[18] + z[32];
z[72] = z[70] + -z[71];
z[72] = z[3] * z[72];
z[73] = T(3) * z[7];
z[74] = T(3) * z[25];
z[41] = T(7) * z[24] + z[41] + (T(-43) * z[54]) / T(24) + z[56] + z[63] + -z[65] + z[66] + z[69] + T(3) * z[72] + -z[73] + -z[74];
z[41] = z[12] * z[41];
z[56] = T(2) * z[4];
z[63] = -z[39] + z[56];
z[51] = z[51] + -z[57] + -z[62] + -z[63] + z[71];
z[51] = z[5] * z[51];
z[40] = z[4] * z[40];
z[57] = (T(5) * z[54]) / T(12) + z[74];
z[66] = -z[32] + -z[47];
z[66] = z[18] + T(2) * z[66];
z[66] = z[18] * z[66];
z[69] = -z[70] + T(-2) * z[71];
z[69] = z[3] * z[69];
z[43] = T(-2) * z[6] + z[43] + z[59];
z[43] = z[6] * z[43];
z[38] = z[31] * z[38];
z[67] = -z[1] + z[67];
z[72] = -(z[1] * z[67]);
z[68] = z[2] * z[68];
z[40] = (T(-5) * z[7]) / T(2) + T(-4) * z[8] + T(-2) * z[24] + -z[38] + z[40] + z[43] + z[51] + -z[57] + z[66] + z[68] + z[69] + z[72];
z[40] = z[16] * z[40];
z[43] = -z[47] + z[71];
z[51] = T(2) * z[43] + z[70];
z[51] = z[3] * z[51];
z[66] = z[1] / T(2);
z[68] = z[32] + z[66];
z[69] = z[1] * z[68];
z[51] = -z[24] + z[51] + -z[53] + z[69];
z[60] = -z[6] + z[60];
z[60] = z[5] * z[60];
z[69] = -z[32] + z[39];
z[70] = z[2] + -z[69];
z[70] = z[2] * z[70];
z[72] = z[4] / T(2);
z[75] = z[45] + -z[72];
z[75] = z[4] * z[75];
z[37] = -z[4] + -z[18] + z[37] + z[67];
z[37] = z[18] * z[37];
z[76] = -z[4] + z[6] + z[18] + z[44] + T(2) * z[45];
z[76] = z[6] * z[76];
z[37] = z[37] + -z[42] + z[49] + z[51] + (T(2) * z[54]) / T(3) + z[60] + z[70] + z[75] + z[76];
z[37] = z[14] * z[37];
z[49] = -z[2] + z[32];
z[39] = -z[39] + -z[49] + z[72];
z[39] = z[4] * z[39];
z[50] = (T(-7) * z[5]) / T(2) + T(8) * z[6] + -z[50] + z[63] + z[71];
z[50] = z[5] * z[50];
z[63] = -z[18] / T(2) + z[32] + z[55];
z[63] = z[18] * z[63];
z[43] = -z[43] + -z[62];
z[43] = z[3] * z[43];
z[56] = -z[2] + T(6) * z[3] + (T(-5) * z[6]) / T(2) + T(3) * z[45] + -z[56];
z[56] = z[6] * z[56];
z[49] = (T(3) * z[1]) / T(2) + z[49];
z[49] = z[1] * z[49];
z[39] = T(-6) * z[8] + T(-5) * z[24] + z[39] + -z[42] + z[43] + z[49] + z[50] + (T(13) * z[54]) / T(8) + z[56] + z[63] + z[74];
z[39] = z[11] * z[39];
z[35] = z[35] * z[68];
z[35] = z[35] + -z[54] / T(2) + z[74];
z[42] = z[3] * z[47];
z[43] = -z[3] + z[6];
z[49] = z[5] * z[43];
z[49] = z[24] + z[42] + -z[49];
z[50] = T(3) * z[2];
z[54] = z[2] / T(2);
z[56] = z[1] + -z[54];
z[56] = z[50] * z[56];
z[62] = -z[47] + -z[72];
z[62] = z[36] * z[62];
z[63] = z[6] / T(2);
z[58] = -z[3] + -z[58] + z[63];
z[58] = z[58] * z[61];
z[61] = T(3) * z[28];
z[38] = -z[35] + z[38] + T(3) * z[49] + z[53] + z[56] + z[58] + -z[61] + z[62];
z[38] = z[17] * z[38];
z[44] = -z[6] + z[44] + z[46] + -z[69];
z[44] = z[6] * z[44];
z[46] = z[54] + z[67];
z[46] = z[2] * z[46];
z[49] = -z[32] + z[47];
z[53] = z[49] + z[72];
z[36] = z[36] * z[53];
z[49] = -z[18] + T(-2) * z[49];
z[49] = z[18] * z[49];
z[36] = z[36] + z[44] + z[46] + z[49] + z[51] + z[57] + z[61];
z[36] = z[13] * z[36];
z[44] = -z[43] + z[55];
z[44] = z[5] * z[44];
z[46] = prod_pow(z[1], 2);
z[49] = z[1] * z[2];
z[43] = z[2] + -z[4] + z[43];
z[43] = z[6] * z[43];
z[42] = -z[42] + z[43] + z[44] + -z[46] + z[49] + -z[52] + -z[65];
z[42] = z[0] * z[42];
z[43] = -z[45] + z[54];
z[43] = z[43] * z[50];
z[44] = z[59] + z[63];
z[44] = z[6] * z[44];
z[35] = z[35] + z[43] + z[44] + z[48] + z[60] + z[73];
z[35] = z[10] * z[35];
z[43] = z[32] + -z[66];
z[43] = z[1] * z[43];
z[32] = -z[32] + z[54];
z[32] = z[2] * z[32];
z[34] = z[34] * z[47];
z[32] = (T(3) * z[7]) / T(2) + z[32] + z[34] + z[43] + T(-4) * z[64];
z[32] = z[9] * z[32];
z[34] = z[14] + -z[15];
z[34] = z[34] * z[61];
z[43] = T(3) * z[20] + -z[22] + -z[23];
z[43] = z[21] * z[43];
z[44] = z[16] + -z[17];
z[44] = z[30] * z[44];
return z[32] + z[33] + z[34] + z[35] + z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + z[43] + T(3) * z[44];
}



template IntegrandConstructorType<double> f_3_28_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_28_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_28_construct (const Kin<qd_real>&);
#endif

}