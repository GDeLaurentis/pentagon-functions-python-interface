#include "f_3_55.h"

namespace PentagonFunctions {

template <typename T> T f_3_55_abbreviated (const std::array<T,10>&);



template <typename T> IntegrandConstructorType<T> f_3_55_construct (const Kin<T>& kin) {
    return [&kin, 
            dl19 = DLog_W_19<T>(kin),dl3 = DLog_W_3<T>(kin),dl20 = DLog_W_20<T>(kin),dl16 = DLog_W_16<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,10> abbr = 
            {dl19(t), rlog(v_path[2]), rlog(-v_path[3] + v_path[0] + v_path[1]), f_2_1_13(kin_path), f_2_1_14(kin_path), rlog(v_path[0] + v_path[4]), dl3(t), rlog(-v_path[2] + v_path[0] + v_path[4]), dl20(t), dl16(t)}
;

        auto result = f_3_55_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_3_55_abbreviated(const std::array<T,10>& abb)
{
using TR = typename T::value_type;
T z[19];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[8];
z[9] = abb[9];
z[10] = abb[7];
z[11] = -z[0] + z[8];
z[12] = z[2] * z[11];
z[13] = z[7] + -z[9];
z[14] = z[10] * z[13];
z[12] = z[12] + -z[14];
z[15] = z[11] + -z[13];
z[16] = z[1] * z[15];
z[16] = z[12] + -z[16] / T(2);
z[16] = z[1] * z[16];
z[17] = z[6] * z[15];
z[12] = -z[12] + z[17];
z[12] = int_to_imaginary<T>(1) * z[3] * z[12];
z[15] = -(z[4] * z[15]);
z[17] = z[11] + z[13];
z[18] = z[2] * z[17];
z[14] = z[14] + -z[18] / T(2);
z[14] = z[2] * z[14];
z[11] = z[11] / T(2) + -z[13];
z[11] = prod_pow(z[3], 2) * z[11];
z[13] = -(z[5] * z[17]);
return z[11] / T(3) + z[12] + z[13] + z[14] + z[15] + z[16];
}



template IntegrandConstructorType<double> f_3_55_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_55_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_55_construct (const Kin<qd_real>&);
#endif

}