#include "f_3_58.h"

namespace PentagonFunctions {

template <typename T> T f_3_58_abbreviated (const std::array<T,5>&);

template <typename T> class SpDLog_f_3_58_W_21 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_58_W_21 (const Kin<T>& kin) {
        c[0] = (-kin.v[4] / T(2) + T(1) / T(2) + (T(-3) * kin.v[3]) / T(8)) * kin.v[3] + (-kin.v[3] / T(4) + -kin.v[4] / T(2) + T(1) / T(2) + kin.v[2] / T(8)) * kin.v[2] + (-kin.v[1] / T(4) + -kin.v[2] / T(4) + T(-1) / T(2) + kin.v[0] / T(8) + kin.v[3] / T(4) + kin.v[4] / T(2)) * kin.v[0] + (T(-1) / T(2) + (T(-3) * kin.v[1]) / T(8) + kin.v[2] / T(4) + (T(3) * kin.v[3]) / T(4) + kin.v[4] / T(2)) * kin.v[1];
c[1] = kin.v[0] / T(2) + kin.v[1] / T(2) + -kin.v[2] / T(2) + -kin.v[3] / T(2);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[4] * (prod_pow(abb[2], 2) / T(4) + -abb[3] / T(2) + abb[1] * (abb[2] / T(2) + (abb[1] * T(-3)) / T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_58_construct (const Kin<T>& kin) {
    return [&kin, 
            dl20 = DLog_W_20<T>(kin),dl21 = DLog_W_21<T>(kin),spdl21 = SpDLog_f_3_58_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,5> abbr = 
            {dl20(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_15(kin_path), dl21(t)}
;

        auto result = f_3_58_abbreviated(abbr);
        result = result + spdl21(t, abbr);

        return result;
    };
}

template <typename T> T f_3_58_abbreviated(const std::array<T,5>& abb)
{
T z[6];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = prod_pow(z[1], 2);
z[5] = z[1] + (T(-3) * z[2]) / T(2);
z[5] = z[2] * z[5];
z[4] = -z[3] + z[4] / T(2) + z[5];
return (z[0] * z[4]) / T(2);
}



template IntegrandConstructorType<double> f_3_58_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_58_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_58_construct (const Kin<qd_real>&);
#endif

}