#include "f_3_104.h"

namespace PentagonFunctions {

template <typename T> T f_3_104_abbreviated (const std::array<T,36>&);

template <typename T> class SpDLog_f_3_104_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_104_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-5) / T(2) + (T(5) * kin.v[0]) / T(2) + (T(35) * kin.v[2]) / T(4) + (T(-45) * kin.v[3]) / T(4) + (T(-35) * kin.v[4]) / T(4) + (T(45) / T(8) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-4) * kin.v[0] + T(4) * kin.v[3])) + (T(-5) / T(2) + (T(25) * kin.v[2]) / T(8) + (T(-35) * kin.v[3]) / T(4) + (T(-25) * kin.v[4]) / T(4)) * kin.v[2] + ((T(5) * kin.v[2]) / T(2) + (T(-5) * kin.v[3]) / T(2) + (T(-5) * kin.v[4]) / T(2)) * kin.v[0] + (T(5) / T(2) + (T(25) * kin.v[4]) / T(8)) * kin.v[4] + (T(5) / T(2) + (T(45) * kin.v[3]) / T(8) + (T(35) * kin.v[4]) / T(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = (T(-3) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + (T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + (T(3) * kin.v[4]) / T(2);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * ((abb[5] * T(-3)) / T(2) + abb[3] * abb[4] * T(-4) + abb[4] * (-abb[4] / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) + abb[2] * (abb[2] / T(2) + bc<T>[0] * int_to_imaginary<T>(4) + abb[3] * T(4)) + abb[1] * (abb[2] * T(-4) + abb[4] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_104_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl19 = DLog_W_19<T>(kin),dl4 = DLog_W_4<T>(kin),dl9 = DLog_W_9<T>(kin),dl15 = DLog_W_15<T>(kin),dl3 = DLog_W_3<T>(kin),dl17 = DLog_W_17<T>(kin),dl16 = DLog_W_16<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl1 = DLog_W_1<T>(kin),dl2 = DLog_W_2<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl28 = DLog_W_28<T>(kin),spdl22 = SpDLog_f_3_104_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,36> abbr = 
            {dl22(t), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), dl19(t), rlog(-v_path[1]), rlog(v_path[2]), rlog(-v_path[4]), f_2_1_4(kin_path), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl4(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl9(t), dl15(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl3(t), f_2_1_7(kin_path), dl17(t), dl16(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl5(t), dl20(t), dl1(t), dl2(t), dl26(t) / kin_path.SqrtDelta, f_2_2_6(kin_path), f_1_3_4(kin_path), f_1_3_5(kin_path), dl30(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta}
;

        auto result = f_3_104_abbreviated(abbr);
        result = result + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_3_104_abbreviated(const std::array<T,36>& abb)
{
using TR = typename T::value_type;
T z[77];
z[0] = abb[1];
z[1] = abb[13];
z[2] = abb[16];
z[3] = abb[22];
z[4] = abb[23];
z[5] = abb[26];
z[6] = abb[27];
z[7] = abb[28];
z[8] = abb[29];
z[9] = abb[2];
z[10] = abb[6];
z[11] = abb[20];
z[12] = abb[3];
z[13] = abb[4];
z[14] = abb[7];
z[15] = abb[9];
z[16] = bc<TR>[0];
z[17] = abb[8];
z[18] = abb[17];
z[19] = abb[5];
z[20] = abb[10];
z[21] = abb[11];
z[22] = abb[12];
z[23] = abb[14];
z[24] = abb[15];
z[25] = abb[18];
z[26] = abb[19];
z[27] = abb[21];
z[28] = abb[24];
z[29] = abb[25];
z[30] = abb[30];
z[31] = abb[31];
z[32] = abb[32];
z[33] = abb[33];
z[34] = abb[34];
z[35] = abb[35];
z[36] = z[17] + z[26];
z[37] = T(2) * z[9];
z[38] = z[0] + z[15] + z[37];
z[39] = T(2) * z[13];
z[40] = -z[12] + z[39];
z[41] = T(2) * z[24];
z[42] = T(3) * z[22];
z[43] = z[36] + z[38] + z[40] + -z[41] + -z[42];
z[43] = z[6] * z[43];
z[44] = -z[15] + z[39];
z[45] = T(2) * z[26];
z[46] = T(3) * z[29];
z[47] = T(2) * z[14];
z[48] = T(-4) * z[0] + (T(-21) * z[9]) / T(2) + z[12] + -z[17] + (T(13) * z[24]) / T(2) + z[44] + -z[45] + z[46] + -z[47];
z[48] = z[4] * z[48];
z[49] = T(3) * z[13];
z[50] = z[9] + -z[15];
z[46] = T(3) * z[14] + -z[24] + z[26] + z[42] + -z[46] + -z[49] + z[50];
z[46] = z[7] * z[46];
z[51] = T(3) * z[9];
z[52] = T(3) * z[12] + -z[51];
z[53] = T(3) * z[17];
z[54] = T(3) * z[26] + -z[52] + -z[53];
z[55] = T(2) * z[15];
z[56] = z[42] + z[47] + -z[54] + -z[55];
z[56] = z[11] * z[56];
z[57] = -z[0] + z[14];
z[36] = z[13] + z[36] + -z[52] + z[57];
z[36] = z[5] * z[36];
z[52] = -z[12] + z[50];
z[58] = T(2) * z[17];
z[45] = z[13] + -z[45] + -z[52] + -z[58];
z[59] = T(2) * z[18];
z[45] = z[45] * z[59];
z[41] = -z[14] + z[41];
z[60] = z[13] + z[15];
z[54] = z[41] + -z[54] + z[60];
z[54] = z[3] * z[54];
z[61] = z[32] + z[33];
z[62] = T(2) * z[30] + -z[35];
z[63] = T(2) * z[34] + z[62];
z[64] = T(2) * z[63];
z[61] = z[61] * z[64];
z[64] = z[13] + z[50];
z[41] = -z[41] + z[64];
z[41] = z[1] * z[41];
z[65] = z[9] + T(2) * z[60];
z[66] = -z[47] + z[65];
z[66] = z[10] * z[66];
z[57] = T(-2) * z[12] + -z[15] + z[24] + T(5) * z[26] + -z[39] + -z[57];
z[57] = z[8] * z[57];
z[67] = z[4] + -z[5] + -z[10];
z[42] = z[42] * z[67];
z[67] = z[9] + -z[24];
z[67] = z[0] + z[67] / T(2);
z[67] = z[2] * z[67];
z[36] = z[36] + z[41] + z[42] + z[43] + z[45] + z[46] + z[48] + z[54] + z[56] + z[57] + z[61] + z[66] + T(3) * z[67];
z[36] = int_to_imaginary<T>(1) * z[16] * z[36];
z[41] = -z[40] + z[50] + z[58];
z[41] = z[12] * z[41];
z[42] = z[37] + z[60];
z[43] = z[17] / T(2) + -z[42];
z[43] = z[17] * z[43];
z[43] = -z[25] + z[43];
z[45] = z[13] + -z[15];
z[46] = T(2) * z[45];
z[48] = z[9] / T(2);
z[54] = z[46] + -z[48];
z[54] = z[9] * z[54];
z[56] = z[0] / T(2);
z[52] = -z[52] + z[56];
z[52] = z[0] * z[52];
z[57] = z[13] / T(2) + z[15];
z[57] = z[13] * z[57];
z[61] = -z[0] + z[50];
z[66] = -z[12] + z[14] / T(2) + z[61];
z[66] = z[14] * z[66];
z[67] = prod_pow(z[15], 2);
z[68] = z[67] / T(2);
z[69] = T(3) * z[21];
z[41] = z[41] + z[43] + z[52] + z[54] + z[57] + z[66] + -z[68] + z[69];
z[41] = z[5] * z[41];
z[52] = T(2) * z[23];
z[54] = z[52] + z[68];
z[68] = z[12] / T(2) + z[53];
z[50] = -z[13] + z[50];
z[70] = T(-2) * z[50] + -z[68];
z[70] = z[12] * z[70];
z[51] = -z[0] + z[51] + -z[60];
z[51] = z[0] * z[51];
z[71] = T(3) * z[27];
z[72] = T(3) * z[25] + -z[71];
z[66] = T(2) * z[19] + -z[66];
z[73] = prod_pow(z[13], 2);
z[74] = prod_pow(z[16], 2);
z[64] = -(z[9] * z[64]);
z[51] = z[51] + -z[54] + z[64] + z[66] + z[70] + z[72] + z[73] + (T(37) * z[74]) / T(24);
z[51] = z[3] * z[51];
z[55] = -z[13] + z[48] + z[55];
z[55] = z[9] * z[55];
z[64] = z[12] + z[58];
z[46] = -z[9] + z[46] + z[64];
z[46] = z[12] * z[46];
z[70] = z[19] + z[69];
z[75] = z[13] * z[60];
z[76] = -z[12] + -z[13] + z[56];
z[76] = z[0] * z[76];
z[43] = z[43] + z[46] + z[54] + z[55] + z[70] + (T(-11) * z[74]) / T(12) + z[75] + z[76];
z[43] = z[6] * z[43];
z[46] = (T(-13) * z[9]) / T(4) + z[53] + -z[60];
z[46] = z[9] * z[46];
z[49] = T(-4) * z[9] + z[15] + z[49] + -z[64];
z[49] = z[12] * z[49];
z[47] = z[12] + -z[38] + z[47];
z[47] = z[14] * z[47];
z[54] = T(2) * z[25];
z[40] = (T(-5) * z[0]) / T(4) + (T(15) * z[9]) / T(2) + z[15] + -z[40];
z[40] = z[0] * z[40];
z[40] = (T(3) * z[19]) / T(2) + (T(-13) * z[23]) / T(2) + z[40] + z[46] + z[47] + z[49] + z[54] + -z[67] + -z[69];
z[40] = z[4] * z[40];
z[46] = -z[37] + z[45] + z[56];
z[46] = z[0] * z[46];
z[47] = -z[58] + z[65];
z[47] = z[17] * z[47];
z[49] = z[9] * z[13];
z[55] = -(z[15] * z[39]);
z[38] = (T(-3) * z[14]) / T(2) + -z[17] + z[38];
z[38] = z[14] * z[38];
z[38] = z[23] + -z[25] + z[38] + z[46] + z[47] + -z[49] + z[55] + -z[69] + z[74] / T(2);
z[38] = z[7] * z[38];
z[42] = (T(3) * z[17]) / T(2) + -z[42];
z[42] = z[17] * z[42];
z[46] = z[14] + -z[17] + z[61];
z[46] = z[14] * z[46];
z[42] = z[42] + -z[46] + z[70];
z[37] = (T(-3) * z[12]) / T(2) + T(3) * z[15] + -z[37] + z[39] + -z[53];
z[37] = z[12] * z[37];
z[39] = -z[9] + z[45];
z[46] = -(z[0] * z[39]);
z[37] = z[37] + -z[42] + z[46] + -z[49] + -z[57] + z[72] + (T(13) * z[74]) / T(8);
z[37] = z[11] * z[37];
z[46] = z[67] + -z[73];
z[44] = z[44] + -z[48];
z[44] = z[9] * z[44];
z[39] = -(z[12] * z[39]);
z[47] = -z[13] + -z[61];
z[47] = z[0] * z[47];
z[39] = z[39] + z[44] + z[46] / T(2) + z[47] + z[52] + -z[66];
z[39] = z[1] * z[39];
z[44] = z[13] * z[15];
z[46] = z[44] + -z[67];
z[45] = -(z[9] * z[45]);
z[47] = (T(-5) * z[17]) / T(2) + -z[50];
z[47] = z[17] * z[47];
z[49] = z[9] + T(-4) * z[15] + z[68];
z[49] = z[12] * z[49];
z[52] = -z[0] + z[9] + -z[12] + z[13];
z[52] = z[0] * z[52];
z[53] = z[12] + (T(5) * z[14]) / T(2) + z[17];
z[53] = z[14] * z[53];
z[45] = -z[23] + T(-5) * z[25] + z[45] + -z[46] + z[47] + z[49] + z[52] + z[53];
z[45] = z[8] * z[45];
z[47] = z[50] + z[58];
z[47] = z[17] * z[47];
z[49] = -(z[9] * z[15]);
z[52] = z[15] + -z[17];
z[52] = z[12] * z[52];
z[46] = z[46] + z[47] + z[49] + z[52] + z[54];
z[46] = z[46] * z[59];
z[47] = z[13] + z[48];
z[47] = z[9] * z[47];
z[48] = z[0] * z[50];
z[42] = z[42] + z[44] + z[47] + z[48];
z[42] = z[10] * z[42];
z[44] = prod_pow(z[9], 2);
z[47] = -z[9] + -z[56];
z[47] = z[0] * z[47];
z[44] = z[23] + (T(3) * z[44]) / T(2) + z[47];
z[44] = z[2] * z[44];
z[47] = z[1] + z[5];
z[48] = T(8) * z[34] + -z[47] + T(4) * z[62];
z[48] = z[2] + z[4] / T(4) + (T(37) * z[8]) / T(24) + -z[10] / T(2) + (T(-10) * z[18]) / T(3) + z[48] / T(3);
z[48] = z[48] * z[74];
z[49] = z[5] + z[8];
z[49] = z[49] * z[71];
z[50] = z[31] * z[63];
z[52] = z[4] + -z[7];
z[52] = z[28] * z[52];
z[47] = -z[3] + -z[8] + -z[10] + z[11] + z[47];
z[47] = z[20] * z[47];
return z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + z[43] + (T(3) * z[44]) / T(2) + z[45] + z[46] + T(2) * z[47] + z[48] + z[49] + z[50] + z[51] + T(3) * z[52];
}



template IntegrandConstructorType<double> f_3_104_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_104_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_104_construct (const Kin<qd_real>&);
#endif

}