#include "f_3_29.h"

namespace PentagonFunctions {

template <typename T> T f_3_29_abbreviated (const std::array<T,32>&);

template <typename T> class SpDLog_f_3_29_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_29_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-23) / T(2) + (T(-23) * kin.v[4]) / T(4) + bc<T>[0] * int_to_imaginary<T>(-4) + (T(-23) / T(8) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(4) + T(2) * kin.v[4]) + (T(23) / T(2) + (T(69) * kin.v[4]) / T(8)) * kin.v[4];
c[1] = (T(-9) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + (T(9) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[11] * ((abb[13] * T(9)) / T(2) + abb[9] * ((abb[9] * T(7)) / T(2) + abb[12] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4)) + abb[8] * abb[9] * T(4) + abb[6] * ((abb[6] * T(-7)) / T(2) + abb[8] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4) + abb[12] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_29_construct (const Kin<T>& kin) {
    return [&kin, 
            dl26 = DLog_W_26<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl13 = DLog_W_13<T>(kin),dl12 = DLog_W_12<T>(kin),dl4 = DLog_W_4<T>(kin),dl6 = DLog_W_6<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl3 = DLog_W_3<T>(kin),dl2 = DLog_W_2<T>(kin),dl18 = DLog_W_18<T>(kin),dl17 = DLog_W_17<T>(kin),dl19 = DLog_W_19<T>(kin),dl1 = DLog_W_1<T>(kin),spdl12 = SpDLog_f_3_29_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,32> abbr = 
            {dl26(t) / kin_path.SqrtDelta, f_2_2_1(kin_path), dl27(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl13(t), rlog(v_path[0]), rlog(-v_path[1]), rlog(v_path[2]), rlog(v_path[3]), rlog(-v_path[4]), f_2_1_1(kin_path), dl12(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_4(kin_path), dl4(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_9(kin_path), f_2_1_11(kin_path), dl6(t), dl5(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl20(t), dl3(t), f_2_1_2(kin_path), dl2(t), dl18(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl17(t), dl19(t), dl1(t)}
;

        auto result = f_3_29_abbreviated(abbr);
        result = result + spdl12(t, abbr);

        return result;
    };
}

template <typename T> T f_3_29_abbreviated(const std::array<T,32>& abb)
{
using TR = typename T::value_type;
T z[69];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[9];
z[10] = bc<TR>[0];
z[11] = abb[10];
z[12] = abb[18];
z[13] = abb[22];
z[14] = abb[23];
z[15] = abb[26];
z[16] = abb[29];
z[17] = abb[14];
z[18] = abb[19];
z[19] = abb[25];
z[20] = abb[31];
z[21] = abb[12];
z[22] = abb[30];
z[23] = abb[15];
z[24] = abb[13];
z[25] = abb[16];
z[26] = abb[17];
z[27] = abb[20];
z[28] = abb[21];
z[29] = abb[24];
z[30] = abb[27];
z[31] = abb[28];
z[32] = T(2) * z[17];
z[33] = -z[22] + z[32];
z[34] = z[18] + z[33];
z[35] = T(3) * z[14];
z[36] = T(3) * z[15];
z[37] = T(3) * z[16];
z[38] = T(-7) * z[19] + -z[34] + -z[35] + z[36] + z[37];
z[38] = z[6] * z[38];
z[39] = T(2) * z[18];
z[40] = T(3) * z[19];
z[33] = z[33] + z[39] + z[40];
z[33] = z[9] * z[33];
z[41] = z[14] + -z[15];
z[37] = T(3) * z[13] + -z[37];
z[42] = T(4) * z[4];
z[43] = -z[37] + -z[41] + z[42];
z[43] = z[5] * z[43];
z[44] = z[14] + -z[20];
z[45] = -z[18] + z[19];
z[46] = -z[13] + z[16];
z[47] = z[44] + z[45] + -z[46];
z[48] = z[28] * z[47];
z[49] = z[15] + z[16] + -z[20] + -z[22];
z[50] = -(z[31] * z[49]);
z[48] = z[48] + z[50];
z[50] = T(3) * z[20];
z[51] = z[40] + -z[50];
z[52] = z[14] + z[17];
z[53] = -z[42] + z[52];
z[54] = T(3) * z[18];
z[55] = -z[51] + z[53] + z[54];
z[55] = z[7] * z[55];
z[56] = T(2) * z[19];
z[57] = -z[17] + z[39] + z[41] + -z[56];
z[57] = z[8] * z[57];
z[58] = T(3) * z[22];
z[51] = T(5) * z[15] + -z[17] + z[51] + -z[58];
z[51] = z[23] * z[51];
z[59] = z[21] * z[45];
z[60] = -z[5] + z[23];
z[61] = z[12] * z[60];
z[38] = z[33] + z[38] + z[43] + T(3) * z[48] + z[51] + z[55] + z[57] + z[59] + T(4) * z[61];
z[38] = int_to_imaginary<T>(1) * z[38];
z[43] = (T(37) * z[15]) / T(8) + (T(13) * z[18]) / T(4) + -z[22];
z[43] = (T(41) * z[14]) / T(24) + -z[16] / T(2) + (T(-3) * z[17]) / T(8) + -z[19] / T(4) + z[43] / T(3);
z[43] = z[10] * z[43];
z[38] = z[38] + z[43];
z[38] = z[10] * z[38];
z[43] = T(2) * z[4];
z[48] = z[36] + -z[43] + -z[52];
z[48] = z[7] * z[48];
z[51] = z[17] / T(2);
z[55] = -z[41] / T(2) + z[45] + z[51];
z[55] = z[8] * z[55];
z[57] = z[4] * z[5];
z[59] = z[5] * z[41];
z[61] = z[57] + T(2) * z[59];
z[35] = z[15] + z[17] + -z[35];
z[35] = z[23] * z[35];
z[34] = T(4) * z[19] + z[34];
z[34] = z[6] * z[34];
z[33] = -z[33] + z[34] + z[35] + z[48] + z[55] + T(2) * z[61];
z[33] = z[8] * z[33];
z[34] = z[50] + z[56];
z[35] = -z[32] + z[34];
z[48] = T(-4) * z[13] + -z[16] + z[35] + -z[41] + z[54];
z[55] = -z[43] + -z[48];
z[55] = z[9] * z[55];
z[61] = T(2) * z[14];
z[37] = -z[17] + -z[37];
z[37] = z[37] / T(2) + z[43] + -z[61];
z[37] = z[7] * z[37];
z[62] = z[41] + -z[46];
z[62] = z[23] * z[62];
z[63] = z[5] * z[43];
z[64] = T(-2) * z[15] + z[17] + z[46];
z[64] = z[5] * z[64];
z[37] = z[37] + z[55] + z[62] + z[63] + z[64];
z[37] = z[7] * z[37];
z[55] = z[13] + T(4) * z[16];
z[43] = z[15] + T(5) * z[19] + -z[32] + z[43] + -z[55] + z[61];
z[43] = z[7] * z[43];
z[62] = z[41] + z[46];
z[64] = T(2) * z[22];
z[65] = z[17] + z[18];
z[66] = z[19] + -z[62] + -z[64] + z[65];
z[66] = z[9] * z[66];
z[57] = -z[57] + z[59];
z[59] = z[18] + z[58];
z[59] = -z[13] + (T(5) * z[16]) / T(2) + z[41] + z[59] / T(2);
z[59] = z[6] * z[59];
z[67] = -z[15] + z[17];
z[68] = -z[19] + z[67];
z[68] = z[13] + -z[14] + z[16] + T(2) * z[68];
z[68] = z[23] * z[68];
z[43] = z[43] + T(2) * z[57] + z[59] + z[66] + z[68];
z[43] = z[6] * z[43];
z[35] = z[35] + z[41] + -z[55] + z[58];
z[35] = z[35] * z[60];
z[55] = z[22] + z[62];
z[56] = z[55] + z[56] + z[65];
z[56] = z[9] * z[56];
z[36] = z[36] + -z[45] + -z[50];
z[36] = z[21] * z[36];
z[39] = -z[17] + -z[19] + -z[39] + -z[55];
z[39] = z[6] * z[39];
z[50] = -(z[8] * z[45]);
z[35] = z[35] + z[36] / T(2) + z[39] + z[50] + z[56];
z[35] = z[21] * z[35];
z[36] = z[5] * z[48];
z[39] = z[22] / T(2) + -z[40] + (T(-3) * z[44]) / T(2) + -z[65];
z[39] = z[9] * z[39];
z[36] = z[36] + z[39] + z[63];
z[36] = z[9] * z[36];
z[34] = -z[15] + z[34];
z[39] = T(5) * z[16];
z[40] = z[34] + -z[39] + -z[52] + z[58];
z[40] = z[25] * z[40];
z[44] = z[27] * z[47];
z[47] = z[30] * z[49];
z[44] = z[44] + -z[47];
z[47] = -z[6] + z[8] + z[21];
z[47] = z[47] * z[60];
z[48] = prod_pow(z[5], 2);
z[49] = z[25] + z[48];
z[50] = -z[5] + -z[23];
z[50] = z[23] * z[50];
z[47] = z[47] + T(2) * z[49] + z[50];
z[47] = z[12] * z[47];
z[45] = -z[13] + z[22] + -z[45] + -z[67];
z[45] = z[26] * z[45];
z[45] = z[45] + z[47];
z[46] = -z[17] + z[46] + z[61];
z[46] = z[5] * z[46];
z[47] = z[16] + -z[19];
z[47] = -z[15] + (T(3) * z[47]) / T(2) + z[51];
z[47] = z[23] * z[47];
z[46] = z[46] + z[47];
z[46] = z[23] * z[46];
z[34] = T(5) * z[13] + -z[34] + z[53] + -z[54];
z[34] = z[11] * z[34];
z[32] = T(-5) * z[14] + -z[18] + (T(-21) * z[19]) / T(2) + -z[32] + z[39] + z[64];
z[32] = z[24] * z[32];
z[39] = -z[42] + (T(-5) * z[62]) / T(2);
z[39] = z[39] * z[48];
z[42] = z[0] + z[2] + z[3];
z[42] = z[1] * z[42];
z[41] = z[29] * z[41];
return z[32] + z[33] + z[34] + z[35] + z[36] + z[37] + z[38] + z[39] + z[40] + T(-6) * z[41] + z[42] + z[43] + T(-3) * z[44] + T(2) * z[45] + z[46];
}



template IntegrandConstructorType<double> f_3_29_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_29_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_29_construct (const Kin<qd_real>&);
#endif

}