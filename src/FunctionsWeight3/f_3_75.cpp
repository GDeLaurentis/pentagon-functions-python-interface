#include "f_3_75.h"

namespace PentagonFunctions {

template <typename T> T f_3_75_abbreviated (const std::array<T,31>&);

template <typename T> class SpDLog_f_3_75_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_75_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-23) / T(2) + (T(-23) * kin.v[4]) / T(4) + bc<T>[0] * int_to_imaginary<T>(-4) + (T(-23) / T(8) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(4) + T(2) * kin.v[4]) + (T(23) / T(2) + (T(69) * kin.v[4]) / T(8)) * kin.v[4];
c[1] = (T(-9) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + (T(9) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[19] * ((abb[7] * T(9)) / T(2) + abb[2] * ((abb[2] * T(7)) / T(2) + abb[5] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4)) + abb[2] * abb[20] * T(4) + abb[1] * ((abb[1] * T(-7)) / T(2) + abb[20] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4) + abb[5] * T(4)));
    }
};
template <typename T> class SpDLog_f_3_75_W_23 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_75_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (kin.v[2] + T(-2) * kin.v[3]) + kin.v[0] * (-kin.v[0] + T(4) * kin.v[3] + T(-2) * kin.v[4]) + -prod_pow(kin.v[4], 2) + kin.v[3] * (T(-3) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[0] * (T(-2) + kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]));
c[1] = (T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(4) * kin.v[2] + T(4) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]) + T(-4) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[22] * (abb[12] * T(-4) + abb[2] * abb[4] * T(-2) + abb[4] * abb[20] * T(-2) + abb[1] * (abb[3] * T(-2) + abb[4] * T(2)) + abb[4] * (bc<T>[0] * int_to_imaginary<T>(2) + abb[4] * T(2)) + abb[3] * (abb[3] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[2] * T(2) + abb[4] * T(2) + abb[20] * T(2)));
    }
};
template <typename T> class SpDLog_f_3_75_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_75_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (-kin.v[2] + T(-4) * kin.v[3]) + kin.v[1] * (T(2) * kin.v[0] + kin.v[1] + T(-6) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + prod_pow(kin.v[4], 2) + kin.v[3] * (T(5) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3]) + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + T(-4) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[25] * (abb[6] * (abb[6] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) + abb[5] * abb[6] * T(2) + abb[6] * abb[20] * T(2) + abb[1] * (abb[6] * T(-2) + abb[3] * T(2)) + abb[9] * T(4) + abb[3] * (abb[5] * T(-2) + abb[6] * T(-2) + abb[20] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2) + abb[3] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_75_construct (const Kin<T>& kin) {
    return [&kin, 
            dl3 = DLog_W_3<T>(kin),dl16 = DLog_W_16<T>(kin),dl26 = DLog_W_26<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl18 = DLog_W_18<T>(kin),dl12 = DLog_W_12<T>(kin),dl19 = DLog_W_19<T>(kin),dl23 = DLog_W_23<T>(kin),dl5 = DLog_W_5<T>(kin),dl2 = DLog_W_2<T>(kin),dl22 = DLog_W_22<T>(kin),dl4 = DLog_W_4<T>(kin),dl17 = DLog_W_17<T>(kin),dl20 = DLog_W_20<T>(kin),spdl12 = SpDLog_f_3_75_W_12<T>(kin),spdl23 = SpDLog_f_3_75_W_23<T>(kin),spdl22 = SpDLog_f_3_75_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,31> abbr = 
            {dl3(t), rlog(-v_path[1]), rlog(-v_path[4]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_4(kin_path), f_2_1_7(kin_path), f_2_1_14(kin_path), f_2_1_15(kin_path), dl16(t), f_2_1_8(kin_path), dl26(t) / kin_path.SqrtDelta, f_2_2_7(kin_path), dl27(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl18(t), f_2_1_11(kin_path), dl12(t), rlog(v_path[3]), dl19(t), dl23(t), dl5(t), dl2(t), dl22(t), dl4(t), dl17(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl20(t)}
;

        auto result = f_3_75_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_3_75_abbreviated(const std::array<T,31>& abb)
{
using TR = typename T::value_type;
T z[59];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[4];
z[4] = abb[5];
z[5] = abb[6];
z[6] = abb[3];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[9];
z[10] = abb[10];
z[11] = abb[17];
z[12] = abb[21];
z[13] = abb[23];
z[14] = abb[27];
z[15] = abb[30];
z[16] = abb[24];
z[17] = abb[26];
z[18] = abb[20];
z[19] = bc<TR>[0];
z[20] = abb[11];
z[21] = abb[12];
z[22] = abb[13];
z[23] = abb[14];
z[24] = abb[15];
z[25] = abb[16];
z[26] = abb[18];
z[27] = abb[28];
z[28] = abb[29];
z[29] = T(3) * z[16];
z[30] = T(2) * z[13];
z[31] = T(2) * z[17];
z[32] = -z[12] + z[31];
z[33] = z[29] + z[30] + z[32];
z[34] = z[2] * z[33];
z[35] = z[14] + -z[15];
z[36] = z[13] + -z[16];
z[37] = -z[17] + z[35] + T(2) * z[36];
z[37] = z[18] * z[37];
z[38] = T(2) * z[35];
z[39] = z[6] * z[38];
z[40] = z[14] + z[17];
z[41] = T(3) * z[15] + -z[40];
z[42] = -(z[3] * z[41]);
z[43] = T(6) * z[35];
z[44] = -(z[28] * z[43]);
z[45] = -(z[4] * z[36]);
z[46] = T(3) * z[14] + -z[15] + -z[17];
z[47] = z[5] * z[46];
z[34] = z[34] + z[37] + z[39] + z[42] + z[44] + z[45] + z[47];
z[34] = int_to_imaginary<T>(1) * z[34];
z[37] = z[11] + z[12] / T(3) + (T(7) * z[13]) / T(6) + (T(-31) * z[14]) / T(12) + (T(37) * z[15]) / T(12) + (T(-5) * z[16]) / T(2) + (T(-3) * z[17]) / T(4);
z[37] = z[19] * z[37];
z[34] = z[34] + z[37] / T(2);
z[34] = z[19] * z[34];
z[37] = T(4) * z[0] + z[11];
z[42] = T(2) * z[14];
z[44] = -z[15] + T(-5) * z[16] + z[31] + z[37] + -z[42];
z[44] = z[3] * z[44];
z[45] = int_to_imaginary<T>(-1) * z[19];
z[45] = z[18] + z[45];
z[32] = z[13] + T(4) * z[16] + z[32];
z[32] = z[32] * z[45];
z[45] = -z[0] + z[11];
z[47] = -z[35] + z[45];
z[48] = T(2) * z[12] + -z[13];
z[49] = z[16] + z[17] + z[47] + -z[48];
z[49] = z[2] * z[49];
z[47] = -z[12] + -z[17] + z[47];
z[30] = -z[16] + -z[30] + z[47];
z[30] = z[4] * z[30];
z[50] = T(2) * z[16];
z[51] = -z[31] + z[50];
z[52] = -z[0] + z[51];
z[53] = T(2) * z[15];
z[54] = -z[11] + z[14] + z[52] + z[53];
z[55] = -(z[5] * z[54]);
z[56] = T(5) * z[0];
z[57] = T(3) * z[12];
z[58] = z[13] + z[56] + z[57];
z[58] = -z[11] + z[35] + z[58] / T(2);
z[58] = z[1] * z[58];
z[30] = z[30] + z[32] + z[39] + -z[44] + z[49] + z[55] + z[58];
z[30] = z[1] * z[30];
z[32] = -(z[18] * z[33]);
z[33] = T(3) * z[13];
z[39] = T(-4) * z[11] + z[33] + -z[35] + z[52];
z[39] = z[6] * z[39];
z[49] = T(3) * z[0];
z[29] = T(3) * z[11] + z[12] + T(-5) * z[13] + -z[29] + -z[49];
z[29] = -z[17] + z[29] / T(2);
z[29] = z[2] * z[29];
z[29] = z[29] + z[32] + z[39] + z[44];
z[29] = z[2] * z[29];
z[32] = z[35] + z[45];
z[32] = z[3] * z[32];
z[39] = -z[17] + z[42] + -z[45];
z[39] = z[6] * z[39];
z[42] = z[4] * z[54];
z[44] = T(3) * z[20];
z[52] = z[12] + -z[16];
z[52] = T(-5) * z[15] + z[17] + z[44] + T(3) * z[52];
z[52] = z[5] * z[52];
z[46] = -(z[18] * z[46]);
z[32] = z[32] + z[39] + z[42] + z[46] + z[52] / T(2);
z[32] = z[5] * z[32];
z[37] = -z[35] + z[37] + -z[51] + -z[57];
z[37] = z[6] * z[37];
z[39] = z[36] + -z[49] + z[57];
z[39] = z[4] * z[39];
z[42] = z[13] + -z[47] + z[50];
z[42] = z[2] * z[42];
z[46] = z[18] * z[36];
z[37] = z[37] + z[39] / T(2) + z[42] + z[46];
z[37] = z[4] * z[37];
z[39] = z[15] + z[40] + -z[50];
z[42] = -z[39] + -z[56] + z[57];
z[42] = z[9] * z[42];
z[46] = z[17] + -z[45] + -z[53];
z[46] = z[3] * z[46];
z[47] = z[35] + T(5) * z[45];
z[47] = z[6] * z[47];
z[38] = -(z[18] * z[38]);
z[38] = z[38] + z[46] + z[47] / T(2);
z[38] = z[6] * z[38];
z[31] = T(-5) * z[14] + (T(-21) * z[16]) / T(2) + -z[31] + z[48] + z[56];
z[31] = z[7] * z[31];
z[45] = z[14] + -z[36] + z[45];
z[45] = z[8] * z[45];
z[46] = -z[0] + z[12] + -z[15];
z[46] = z[10] * z[46];
z[45] = z[45] + z[46];
z[35] = z[17] / T(2) + (T(5) * z[35]) / T(2) + -z[36];
z[35] = prod_pow(z[18], 2) * z[35];
z[40] = T(-3) * z[36] + -z[40];
z[40] = z[3] * z[40];
z[41] = z[18] * z[41];
z[40] = z[40] / T(2) + z[41];
z[40] = z[3] * z[40];
z[33] = T(5) * z[11] + -z[33] + z[39];
z[33] = z[21] * z[33];
z[39] = z[2] + -z[4];
z[39] = z[6] * z[39];
z[41] = prod_pow(z[3], 2);
z[39] = -z[8] + z[9] + z[10] + -z[21] + z[39] + -z[41] / T(2);
z[39] = z[39] * z[44];
z[41] = -z[22] + -z[24] + -z[25];
z[41] = z[23] * z[41];
z[36] = -z[11] + z[12] + z[15] + -z[17] + z[36];
z[36] = z[26] * z[36];
z[43] = z[27] * z[43];
return z[29] + z[30] + z[31] + z[32] + z[33] + z[34] + z[35] + T(2) * z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + z[43] + T(3) * z[45];
}



template IntegrandConstructorType<double> f_3_75_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_75_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_75_construct (const Kin<qd_real>&);
#endif

}