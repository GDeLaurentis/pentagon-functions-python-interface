#include "f_3_102.h"

namespace PentagonFunctions {

template <typename T> T f_3_102_abbreviated (const std::array<T,37>&);

template <typename T> class SpDLog_f_3_102_W_23 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_102_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(5) * kin.v[2] + T(8) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[3] * (T(3) * kin.v[3] + T(-4) * kin.v[4]) + prod_pow(kin.v[4], 2) + kin.v[0] * (T(3) * kin.v[0] + T(-8) * kin.v[2] + T(-6) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[0] * (T(-2) + kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]));
c[1] = (T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(4) * kin.v[2] + T(4) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]) + T(-4) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[6] * T(-4) + abb[2] * (abb[3] * T(-2) + abb[5] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2) + abb[2] * T(2) + abb[4] * T(2)) + abb[1] * (abb[1] * T(-4) + abb[4] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[2] * T(2) + abb[3] * T(2) + abb[5] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_102_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl9 = DLog_W_9<T>(kin),dl4 = DLog_W_4<T>(kin),dl18 = DLog_W_18<T>(kin),dl16 = DLog_W_16<T>(kin),dl5 = DLog_W_5<T>(kin),dl19 = DLog_W_19<T>(kin),dl20 = DLog_W_20<T>(kin),dl17 = DLog_W_17<T>(kin),dl1 = DLog_W_1<T>(kin),dl25 = DLog_W_25<T>(kin),dl2 = DLog_W_2<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl26 = DLog_W_26<T>(kin),spdl23 = SpDLog_f_3_102_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,37> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_8(kin_path), dl9(t), rlog(-v_path[1]), rlog(v_path[3]), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl4(t), f_2_1_11(kin_path), f_2_1_14(kin_path), dl18(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl16(t), dl5(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl19(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl20(t), dl17(t), dl1(t), dl25(t), dl2(t), dl28(t) / kin_path.SqrtDelta, f_2_2_5(kin_path), f_1_3_1(kin_path), f_1_3_3(kin_path), f_1_3_5(kin_path), dl29(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta}
;

        auto result = f_3_102_abbreviated(abbr);
        result = result + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_3_102_abbreviated(const std::array<T,37>& abb)
{
using TR = typename T::value_type;
T z[92];
z[0] = abb[1];
z[1] = abb[7];
z[2] = abb[12];
z[3] = abb[15];
z[4] = abb[19];
z[5] = abb[25];
z[6] = abb[26];
z[7] = abb[29];
z[8] = abb[2];
z[9] = abb[18];
z[10] = abb[22];
z[11] = abb[27];
z[12] = abb[3];
z[13] = abb[4];
z[14] = abb[5];
z[15] = abb[8];
z[16] = abb[9];
z[17] = bc<TR>[0];
z[18] = abb[28];
z[19] = abb[6];
z[20] = abb[10];
z[21] = abb[11];
z[22] = abb[13];
z[23] = abb[14];
z[24] = abb[16];
z[25] = abb[17];
z[26] = abb[20];
z[27] = abb[21];
z[28] = abb[23];
z[29] = abb[24];
z[30] = abb[30];
z[31] = abb[31];
z[32] = abb[32];
z[33] = abb[33];
z[34] = abb[34];
z[35] = abb[35];
z[36] = abb[36];
z[37] = T(2) * z[15];
z[38] = -z[13] + z[37];
z[39] = z[0] + z[38];
z[40] = T(4) * z[8];
z[41] = T(3) * z[14];
z[42] = z[16] + z[41];
z[43] = T(2) * z[12];
z[44] = T(3) * z[29];
z[45] = T(2) * z[21];
z[46] = z[39] + -z[40] + z[42] + z[43] + -z[44] + z[45];
z[46] = z[5] * z[46];
z[47] = T(5) * z[13];
z[48] = T(7) * z[8];
z[49] = -z[15] + z[27];
z[49] = -z[0] + T(5) * z[12] + T(-4) * z[21] + T(-7) * z[29] + z[42] + z[47] + z[48] + T(3) * z[49];
z[49] = z[10] * z[49];
z[50] = T(3) * z[13];
z[51] = T(2) * z[8];
z[42] = z[12] + -z[15] + -z[42] + z[45] + z[50] + z[51];
z[42] = z[9] * z[42];
z[52] = T(2) * z[0] + -z[45];
z[53] = z[8] + -z[12];
z[54] = T(2) * z[14];
z[47] = T(-6) * z[15] + T(8) * z[16] + -z[29] + z[47] + -z[52] + z[53] + -z[54];
z[47] = z[6] * z[47];
z[55] = z[13] + -z[15];
z[56] = -z[45] + z[55];
z[57] = T(7) * z[16];
z[58] = T(6) * z[8];
z[56] = -z[0] + z[14] + T(3) * z[56] + z[57] + z[58];
z[56] = z[2] * z[56];
z[59] = T(4) * z[15];
z[60] = -z[50] + z[59];
z[61] = T(4) * z[14];
z[62] = T(-4) * z[12] + z[44] + z[60] + -z[61];
z[62] = z[18] * z[62];
z[63] = z[32] + z[34];
z[64] = z[30] + z[35];
z[65] = T(3) * z[36] + T(-2) * z[64];
z[66] = T(2) * z[65];
z[63] = z[63] * z[66];
z[67] = T(2) * z[16];
z[68] = -z[14] + z[55];
z[69] = z[67] + z[68];
z[45] = -z[0] + z[45];
z[70] = -z[45] + -z[69];
z[71] = T(4) * z[1];
z[70] = z[70] * z[71];
z[72] = T(3) * z[8];
z[52] = -z[13] + z[29] + -z[52] + -z[72];
z[52] = z[11] * z[52];
z[66] = -(z[33] * z[66]);
z[42] = z[42] + z[46] + z[47] + z[49] + z[52] + z[56] + z[62] + z[63] + z[66] + z[70];
z[42] = int_to_imaginary<T>(1) * z[42];
z[46] = int_to_imaginary<T>(1) * z[25];
z[47] = T(3) * z[46];
z[49] = z[6] + -z[9] + -z[10] + z[11];
z[49] = z[47] * z[49];
z[52] = -z[36] + (T(2) * z[64]) / T(3);
z[52] = (T(-20) * z[1]) / T(3) + (T(25) * z[2]) / T(8) + (T(3) * z[5]) / T(8) + (T(43) * z[6]) / T(8) + -z[9] / T(6) + (T(-13) * z[10]) / T(12) + T(2) * z[11] + -z[18] + T(4) * z[52];
z[52] = z[17] * z[52];
z[42] = z[42] + z[49] + z[52];
z[42] = z[17] * z[42];
z[49] = T(4) * z[16];
z[52] = (T(3) * z[8]) / T(2);
z[56] = z[41] + z[49] + z[52];
z[56] = z[8] * z[56];
z[62] = z[16] + z[55];
z[48] = T(4) * z[0] + -z[48] + -z[54] + -z[62];
z[48] = z[0] * z[48];
z[63] = T(3) * z[22];
z[64] = T(3) * z[23];
z[66] = z[63] + z[64];
z[70] = (T(3) * z[16]) / T(2);
z[73] = z[68] + -z[70];
z[73] = z[16] * z[73];
z[74] = z[12] + -z[13];
z[75] = T(2) * z[74];
z[76] = (T(3) * z[14]) / T(2) + -z[75];
z[76] = z[14] * z[76];
z[77] = prod_pow(z[15], 2);
z[78] = T(2) * z[20];
z[38] = -(z[13] * z[38]);
z[79] = z[12] / T(2);
z[80] = -z[15] + z[79];
z[80] = z[12] * z[80];
z[38] = z[38] + z[48] + z[56] + z[66] + z[73] + z[76] + (T(3) * z[77]) / T(2) + -z[78] + z[80];
z[38] = z[5] * z[38];
z[48] = -z[49] + z[50] + -z[51] + z[54];
z[48] = z[8] * z[48];
z[49] = T(2) * z[55];
z[56] = -z[14] + z[49] + z[70];
z[56] = z[16] * z[56];
z[70] = z[16] + z[68];
z[73] = z[51] + z[70];
z[73] = z[0] * z[73];
z[80] = T(3) * z[24];
z[81] = z[77] / T(2);
z[82] = z[26] + z[81];
z[83] = T(6) * z[13] + T(-5) * z[15];
z[83] = z[13] * z[83];
z[37] = z[12] + z[37];
z[84] = T(-7) * z[13] + z[37];
z[84] = z[12] * z[84];
z[85] = z[14] + -z[75];
z[85] = z[14] * z[85];
z[48] = T(4) * z[20] + z[48] + z[56] + z[63] + z[73] + -z[80] + T(3) * z[82] + z[83] + z[84] + z[85];
z[48] = z[10] * z[48];
z[56] = T(2) * z[13];
z[73] = -z[12] + z[56];
z[82] = -z[15] + z[16];
z[83] = z[14] + z[51] + z[73] + z[82];
z[83] = z[0] * z[83];
z[84] = z[12] + z[56];
z[85] = -z[54] + z[84];
z[86] = -z[8] + z[85];
z[86] = z[8] * z[86];
z[87] = z[16] / T(2);
z[68] = z[68] + z[87];
z[68] = z[16] * z[68];
z[64] = z[64] + z[80];
z[88] = -z[56] + -z[79];
z[88] = z[12] * z[88];
z[89] = z[19] + z[78];
z[90] = z[13] / T(2) + z[15];
z[90] = z[13] * z[90];
z[91] = -z[13] + -z[14] + -z[15] + z[43];
z[91] = z[14] * z[91];
z[81] = -z[64] + -z[68] + -z[81] + z[83] + z[86] + z[88] + -z[89] + z[90] + z[91];
z[81] = z[9] * z[81];
z[83] = z[0] / T(2);
z[37] = -z[37] + z[50] + z[58] + z[67] + z[83];
z[37] = z[0] * z[37];
z[50] = -z[13] + z[14];
z[40] = z[12] + -z[40] + z[50] + -z[57];
z[40] = z[8] * z[40];
z[49] = -z[14] + -z[49] + -z[87];
z[49] = z[16] * z[49];
z[57] = z[13] * z[15];
z[58] = z[12] * z[55];
z[67] = -z[14] / T(2) + -z[55];
z[67] = z[14] * z[67];
z[37] = T(6) * z[20] + z[37] + z[40] + z[49] + z[57] + -z[58] + -z[66] + z[67] + -z[77];
z[37] = z[2] * z[37];
z[40] = -z[12] + T(4) * z[13];
z[49] = T(5) * z[16];
z[41] = -z[40] + z[41] + -z[49] + -z[72];
z[41] = z[8] * z[41];
z[49] = -z[49] + T(-3) * z[55] + z[61];
z[49] = z[16] * z[49];
z[55] = T(3) * z[15];
z[66] = z[55] + -z[73];
z[66] = z[12] * z[66];
z[67] = -z[51] + z[55];
z[40] = (T(7) * z[0]) / T(2) + T(-7) * z[14] + T(3) * z[16] + z[40] + -z[67];
z[40] = z[0] * z[40];
z[72] = (T(5) * z[13]) / T(2) + -z[55];
z[72] = z[13] * z[72];
z[40] = z[40] + z[41] + z[49] + z[64] + z[66] + z[72] + z[76] + -z[89];
z[40] = z[6] * z[40];
z[41] = z[0] + z[13] + z[43] + z[54] + -z[67];
z[41] = int_to_imaginary<T>(1) * z[41];
z[41] = (T(11) * z[17]) / T(12) + z[41] + -z[47];
z[41] = z[17] * z[41];
z[43] = -z[0] + z[51] + -z[85];
z[43] = z[0] * z[43];
z[47] = prod_pow(z[13], 2);
z[49] = -z[12] + -z[13] + z[55];
z[49] = z[12] * z[49];
z[54] = -z[14] + -z[75];
z[54] = z[14] * z[54];
z[41] = -z[19] + z[41] + z[43] + -z[47] + z[49] + z[54] + -z[63] + -z[80] + z[86];
z[41] = z[3] * z[41];
z[43] = -z[15] + z[56] + -z[79];
z[43] = z[12] * z[43];
z[49] = z[16] + z[52] + -z[84];
z[49] = z[8] * z[49];
z[52] = -z[53] + z[82];
z[52] = z[0] * z[52];
z[53] = -(z[16] * z[62]);
z[43] = z[43] + z[49] + z[52] + z[53] + z[57] + -z[78] + z[80];
z[43] = z[11] * z[43];
z[44] = T(-3) * z[12] + -z[16] + z[44] + z[45] + -z[50] + -z[59];
z[44] = int_to_imaginary<T>(1) * z[44];
z[44] = (T(-5) * z[17]) / T(6) + z[44];
z[44] = z[17] * z[44];
z[45] = -z[47] + -z[77];
z[47] = z[12] * z[15];
z[45] = -z[22] + -z[28] + z[45] / T(2) + z[47];
z[47] = z[70] + -z[83];
z[47] = z[0] * z[47];
z[44] = z[44] + T(3) * z[45] + z[47] + -z[68] + -z[78];
z[44] = z[7] * z[44];
z[45] = -z[8] + -z[82] + z[83];
z[45] = z[0] * z[45];
z[39] = -z[27] + z[39] + -z[51];
z[39] = int_to_imaginary<T>(1) * z[39];
z[39] = z[39] + z[46];
z[39] = z[17] * z[39];
z[46] = z[8] / T(2) + -z[13] + z[16];
z[46] = z[8] * z[46];
z[39] = z[19] + z[22] + z[24] + -z[26] + z[39] + z[45] + z[46] + z[58];
z[39] = z[4] * z[39];
z[45] = z[16] * z[69];
z[46] = -z[0] + -z[70];
z[46] = z[0] * z[46];
z[45] = z[45] + z[46] + z[78];
z[45] = z[45] * z[71];
z[46] = -z[60] + z[79];
z[46] = z[12] * z[46];
z[47] = z[61] * z[74];
z[49] = (T(-7) * z[13]) / T(2) + z[59];
z[49] = z[13] * z[49];
z[46] = z[46] + z[47] + z[49];
z[46] = z[18] * z[46];
z[47] = -(z[31] * z[65]);
z[49] = T(-5) * z[2] + T(7) * z[5] + T(-2) * z[10];
z[49] = z[19] * z[49];
z[50] = -z[5] + z[18];
z[50] = z[6] + T(7) * z[10] + -z[11] + T(-3) * z[50];
z[50] = z[28] * z[50];
return z[37] + z[38] + T(3) * z[39] + z[40] + z[41] + z[42] + z[43] + z[44] + z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + z[81];
}



template IntegrandConstructorType<double> f_3_102_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_102_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_102_construct (const Kin<qd_real>&);
#endif

}