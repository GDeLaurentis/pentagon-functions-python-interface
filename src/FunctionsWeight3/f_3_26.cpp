#include "f_3_26.h"

namespace PentagonFunctions {

template <typename T> T f_3_26_abbreviated (const std::array<T,23>&);



template <typename T> IntegrandConstructorType<T> f_3_26_construct (const Kin<T>& kin) {
    return [&kin, 
            dl4 = DLog_W_4<T>(kin),dl17 = DLog_W_17<T>(kin),dl20 = DLog_W_20<T>(kin),dl31 = DLog_W_31<T>(kin),dl27 = DLog_W_27<T>(kin),dl26 = DLog_W_26<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,23> abbr = 
            {dl4(t), f_2_2_1(kin_path), dl17(t), dl20(t), dl31(t), dl27(t) / kin_path.SqrtDelta, rlog(v_path[0]), rlog(v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_1(kin_path), f_2_1_9(kin_path), f_2_1_11(kin_path), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl26(t) / kin_path.SqrtDelta, rlog(-v_path[4]), f_2_1_4(kin_path), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl28(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta}
;

        auto result = f_3_26_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_3_26_abbreviated(const std::array<T,23>& abb)
{
using TR = typename T::value_type;
T z[35];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = bc<TR>[0];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[12];
z[14] = abb[13];
z[15] = abb[14];
z[16] = abb[15];
z[17] = abb[16];
z[18] = abb[22];
z[19] = abb[21];
z[20] = abb[17];
z[21] = abb[18];
z[22] = abb[19];
z[23] = abb[20];
z[24] = -(z[17] * z[21]);
z[25] = z[5] + -z[18];
z[25] = z[12] * z[25];
z[26] = z[5] + z[18];
z[26] = z[14] * z[26];
z[27] = -z[17] + z[18];
z[28] = -z[5] + z[27];
z[29] = -(z[15] * z[28]);
z[30] = z[14] + -z[15] + z[21];
z[30] = z[19] * z[30];
z[31] = -(z[22] * z[27]);
z[32] = z[5] + z[17];
z[33] = z[19] + z[32];
z[33] = z[13] * z[33];
z[34] = -z[0] + -z[2] + -z[3] + T(2) * z[4];
z[34] = z[1] * z[34];
z[24] = z[24] + z[25] + z[26] + z[29] + z[30] + z[31] + z[33] + z[34];
z[25] = z[19] + z[27];
z[26] = z[20] * z[25];
z[25] = z[10] * z[25];
z[29] = z[18] + z[19];
z[30] = z[8] * z[29];
z[30] = -z[25] + z[26] + z[30];
z[31] = z[5] * z[11];
z[30] = T(2) * z[30] + z[31];
z[30] = z[11] * z[30];
z[31] = z[8] * z[28];
z[33] = z[17] * z[20];
z[31] = z[31] + z[33];
z[33] = z[7] * z[28];
z[29] = -(z[11] * z[29]);
z[29] = z[29] + z[31] + z[33];
z[34] = z[6] * z[28];
z[29] = T(2) * z[29] + -z[34];
z[29] = z[6] * z[29];
z[32] = -(z[8] * z[32]);
z[26] = -z[26] + z[32];
z[25] = z[25] + T(2) * z[26];
z[25] = z[10] * z[25];
z[26] = z[10] * z[18];
z[26] = z[26] + -z[31];
z[26] = T(2) * z[26] + -z[33];
z[26] = z[7] * z[26];
z[31] = z[19] * prod_pow(z[20], 2);
z[32] = -(prod_pow(z[8], 2) * z[28]);
z[24] = T(2) * z[24] + z[25] + z[26] + z[29] + z[30] + z[31] + z[32];
z[25] = -z[8] + z[16];
z[25] = z[19] * z[25];
z[26] = z[16] * z[28];
z[28] = -(z[10] * z[28]);
z[29] = z[23] * z[27];
z[30] = -(z[5] * z[7]);
z[25] = z[25] + z[26] + z[28] + z[29] + z[30] + -z[34];
z[25] = int_to_imaginary<T>(1) * z[25];
z[26] = T(-2) * z[5] + -z[19] + z[27];
z[26] = z[9] * z[26];
z[25] = T(6) * z[25] + z[26];
z[25] = z[9] * z[25];
return T(3) * z[24] + z[25];
}



template IntegrandConstructorType<double> f_3_26_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_26_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_26_construct (const Kin<qd_real>&);
#endif

}