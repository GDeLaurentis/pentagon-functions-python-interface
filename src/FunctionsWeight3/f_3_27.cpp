#include "f_3_27.h"

namespace PentagonFunctions {

template <typename T> T f_3_27_abbreviated (const std::array<T,33>&);

template <typename T> class SpDLog_f_3_27_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_27_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(2) * kin.v[0]) / T(3) + bc<T>[1] * T(2) + (bc<T>[1] + T(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[3]) + (T(-2) * kin.v[0] * kin.v[4]) / T(3) + T(2) * kin.v[2] * kin.v[4] + T(2) * kin.v[3] * kin.v[4] + bc<T>[1] * (-kin.v[4] + T(-2)) * kin.v[4] + prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(2) + kin.v[1]) + (-kin.v[4] + T(-2)) * kin.v[4]);
c[1] = (bc<T>[0] * int_to_imaginary<T>(2) + bc<T>[1] * T(2) + T(4)) * kin.v[1] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[4] + bc<T>[1] * T(-2) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[10] * (abb[13] * T(-4) + abb[6] * abb[12] * T(-2) + abb[7] * abb[12] * T(-2) + abb[5] * (abb[11] * T(-2) + abb[12] * T(2)) + abb[12] * (bc<T>[0] * int_to_imaginary<T>(2) + abb[12] * T(2)) + abb[11] * (abb[11] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[6] * T(2) + abb[7] * T(2) + abb[12] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_27_construct (const Kin<T>& kin) {
    return [&kin, 
            dl26 = DLog_W_26<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl13 = DLog_W_13<T>(kin),dl12 = DLog_W_12<T>(kin),dl6 = DLog_W_6<T>(kin),dl18 = DLog_W_18<T>(kin),dl4 = DLog_W_4<T>(kin),dl19 = DLog_W_19<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl3 = DLog_W_3<T>(kin),dl2 = DLog_W_2<T>(kin),dl17 = DLog_W_17<T>(kin),dl1 = DLog_W_1<T>(kin),spdl12 = SpDLog_f_3_27_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,33> abbr = 
            {dl26(t) / kin_path.SqrtDelta, f_2_2_1(kin_path), dl28(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl13(t), rlog(v_path[0]), rlog(v_path[2]), rlog(v_path[3]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_1(kin_path), dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), f_2_1_4(kin_path), dl6(t), f_2_1_9(kin_path), dl18(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), dl4(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl19(t), dl5(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl20(t), dl3(t), dl2(t), dl17(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl1(t)}
;

        auto result = f_3_27_abbreviated(abbr);
        result = result + spdl12(t, abbr);

        return result;
    };
}

template <typename T> T f_3_27_abbreviated(const std::array<T,33>& abb)
{
using TR = typename T::value_type;
T z[73];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[7];
z[7] = abb[8];
z[8] = bc<TR>[0];
z[9] = abb[6];
z[10] = abb[9];
z[11] = abb[14];
z[12] = abb[16];
z[13] = abb[22];
z[14] = abb[26];
z[15] = abb[27];
z[16] = abb[28];
z[17] = abb[29];
z[18] = abb[19];
z[19] = abb[23];
z[20] = abb[32];
z[21] = abb[11];
z[22] = abb[12];
z[23] = abb[17];
z[24] = abb[13];
z[25] = abb[15];
z[26] = abb[18];
z[27] = abb[20];
z[28] = abb[21];
z[29] = abb[24];
z[30] = abb[25];
z[31] = abb[30];
z[32] = abb[31];
z[33] = z[5] + z[21];
z[34] = -z[30] + -z[32] + z[33];
z[34] = z[17] * z[34];
z[35] = -z[5] + z[30];
z[35] = z[14] * z[35];
z[36] = -z[5] + z[7];
z[37] = z[11] * z[36];
z[34] = z[34] + z[35] + z[37];
z[35] = T(2) * z[22];
z[37] = T(4) * z[5] + -z[21] + z[35];
z[38] = T(4) * z[7];
z[39] = T(3) * z[30];
z[40] = T(3) * z[32] + z[37] + -z[38] + -z[39];
z[40] = z[20] * z[40];
z[41] = z[5] + z[7];
z[42] = -z[21] + z[41];
z[43] = -z[35] + z[42];
z[44] = T(2) * z[9];
z[45] = z[39] + z[43] + -z[44];
z[45] = z[15] * z[45];
z[46] = -z[16] + z[20];
z[47] = -z[13] + z[18];
z[48] = T(2) * z[15];
z[46] = T(3) * z[19] + T(2) * z[46] + z[47] + -z[48];
z[46] = z[6] * z[46];
z[49] = T(2) * z[21];
z[50] = z[5] + z[49];
z[51] = z[7] + z[50];
z[52] = -z[35] + -z[51];
z[52] = z[16] * z[52];
z[53] = T(3) * z[22];
z[54] = T(2) * z[5];
z[55] = z[53] + z[54];
z[56] = z[19] * z[55];
z[57] = -z[36] + z[53];
z[58] = z[21] + -z[57];
z[58] = z[13] * z[58];
z[59] = -z[16] + z[19];
z[39] = -(z[39] * z[59]);
z[60] = z[21] + z[54];
z[61] = z[53] + z[60];
z[61] = z[18] * z[61];
z[62] = T(2) * z[18];
z[63] = z[59] + -z[62];
z[63] = z[9] * z[63];
z[64] = -z[9] + z[33];
z[64] = z[12] * z[64];
z[65] = -z[5] + z[9];
z[65] = z[4] * z[65];
z[66] = -z[16] + z[18];
z[67] = z[19] + z[66];
z[68] = -z[13] + z[67];
z[68] = T(3) * z[68];
z[69] = -(z[28] * z[68]);
z[66] = -z[12] + z[66];
z[70] = z[20] + z[66];
z[71] = z[13] + z[70];
z[72] = -(z[23] * z[71]);
z[34] = T(3) * z[34] + z[39] + z[40] + z[45] + z[46] + z[52] + z[56] + z[58] + z[61] + z[63] + -z[64] + z[65] + z[69] + z[72];
z[34] = int_to_imaginary<T>(1) * z[34];
z[39] = z[12] / T(3) + (T(-23) * z[13]) / T(24) + (T(-7) * z[15]) / T(12) + (T(-2) * z[16]) / T(3) + -z[17] / T(2) + (T(3) * z[18]) / T(8) + (T(13) * z[19]) / T(8) + (T(3) * z[20]) / T(4);
z[39] = z[8] * z[39];
z[34] = z[34] + z[39];
z[34] = z[8] * z[34];
z[39] = z[35] + z[44] + -z[51];
z[39] = z[15] * z[39];
z[40] = z[35] + z[42];
z[40] = z[16] * z[40];
z[42] = T(3) * z[21] + -z[55];
z[42] = z[19] * z[42];
z[45] = T(4) * z[21];
z[46] = -z[45] + z[57];
z[46] = z[13] * z[46];
z[51] = -z[5] + z[21];
z[52] = T(2) * z[51] + -z[53];
z[52] = z[18] * z[52];
z[44] = z[44] * z[67];
z[55] = z[15] + -z[20];
z[47] = z[16] + (T(-3) * z[19]) / T(2) + -z[47] / T(2) + z[55];
z[47] = z[6] * z[47];
z[37] = z[7] + T(3) * z[9] + -z[37];
z[37] = z[20] * z[37];
z[37] = z[37] + z[39] + z[40] + z[42] + z[44] + z[46] + z[47] + z[52] + z[64] + T(-4) * z[65];
z[37] = z[6] * z[37];
z[39] = -z[14] + z[19];
z[40] = -z[7] + z[33];
z[42] = -z[22] + z[40];
z[39] = z[39] * z[42];
z[44] = -(z[40] * z[66]);
z[46] = T(2) * z[7];
z[47] = z[46] + -z[60];
z[52] = -z[22] + -z[47];
z[52] = z[17] * z[52];
z[47] = z[20] * z[47];
z[56] = z[22] + -z[49];
z[56] = z[13] * z[56];
z[57] = z[6] * z[71];
z[58] = T(7) * z[13] + z[70];
z[58] = z[23] * z[58];
z[39] = z[39] + z[44] + z[47] + z[52] + z[56] + z[57] + z[58] / T(2);
z[39] = z[23] * z[39];
z[44] = z[22] / T(2);
z[47] = z[21] + -z[44];
z[47] = z[47] * z[53];
z[52] = T(3) * z[24];
z[56] = z[7] * z[51];
z[57] = T(2) * z[26];
z[58] = -(z[21] * z[51]);
z[47] = z[47] + -z[52] + -z[56] + z[57] + z[58];
z[47] = z[18] * z[47];
z[58] = z[35] + z[60];
z[58] = z[16] * z[58];
z[61] = -z[7] + -z[33] + -z[35];
z[61] = z[19] * z[61];
z[42] = -(z[13] * z[42]);
z[41] = -(z[18] * z[41]);
z[63] = z[9] * z[67];
z[41] = z[41] + z[42] + z[58] + z[61] + z[63];
z[41] = z[9] * z[41];
z[42] = (T(3) * z[9]) / T(2);
z[46] = -z[5] + z[42] + -z[46] + -z[53];
z[46] = z[9] * z[46];
z[53] = z[7] / T(2);
z[58] = z[33] + z[53];
z[58] = z[7] * z[58];
z[61] = T(3) * z[5] + z[44] + z[49];
z[61] = z[22] * z[61];
z[63] = (T(3) * z[10]) / T(2);
z[64] = -(z[21] * z[50]);
z[46] = T(-2) * z[24] + z[46] + z[58] + z[61] + z[63] + z[64];
z[46] = z[20] * z[46];
z[35] = z[35] + z[40] + -z[42];
z[35] = z[9] * z[35];
z[40] = prod_pow(z[5], 2);
z[58] = z[40] / T(2);
z[61] = z[57] + z[58];
z[64] = prod_pow(z[21], 2);
z[65] = -(z[22] * z[60]);
z[35] = z[10] + z[35] + -z[56] + z[61] + z[64] + z[65];
z[35] = z[14] * z[35];
z[44] = T(2) * z[33] + z[44];
z[44] = z[22] * z[44];
z[44] = -z[24] + z[44];
z[56] = -z[53] + z[54];
z[56] = z[7] * z[56];
z[65] = z[9] / T(2);
z[43] = -z[43] + -z[65];
z[43] = z[9] * z[43];
z[60] = z[21] * z[60];
z[43] = z[10] + z[43] + -z[44] + z[56] + -z[58] + z[60];
z[43] = z[15] * z[43];
z[51] = z[51] + z[53];
z[51] = z[7] * z[51];
z[44] = -z[44] + z[51] + -z[60] + -z[61];
z[44] = z[16] * z[44];
z[51] = z[7] * z[33];
z[51] = z[51] + -z[57];
z[50] = (T(-3) * z[22]) / T(2) + T(2) * z[50];
z[50] = z[22] * z[50];
z[50] = z[50] + z[51] + -z[52] + (T(-5) * z[64]) / T(2);
z[50] = z[19] * z[50];
z[52] = prod_pow(z[7], 2);
z[53] = z[5] + (T(9) * z[21]) / T(2);
z[53] = z[21] * z[53];
z[45] = -z[5] + -z[45];
z[45] = z[22] * z[45];
z[45] = T(5) * z[24] + z[45] + -z[52] / T(2) + z[53] + z[61];
z[45] = z[13] * z[45];
z[53] = -z[7] + z[54] + -z[65];
z[53] = z[9] * z[53];
z[54] = -z[5] + -z[21] / T(2);
z[54] = z[21] * z[54];
z[51] = T(-2) * z[10] + (T(-3) * z[40]) / T(2) + z[51] + z[53] + z[54];
z[51] = z[12] * z[51];
z[36] = -z[22] + z[36] + z[42] + -z[49];
z[36] = z[9] * z[36];
z[42] = -z[40] + z[64];
z[49] = (T(3) * z[7]) / T(2) + -z[33];
z[49] = z[7] * z[49];
z[33] = z[22] * z[33];
z[33] = z[24] + z[33] + z[36] + z[42] / T(2) + z[49];
z[33] = z[17] * z[33];
z[36] = z[38] + -z[65];
z[36] = z[9] * z[36];
z[38] = -(z[5] * z[38]);
z[36] = z[36] + z[38] + z[58] + -z[63];
z[36] = z[4] * z[36];
z[38] = -z[14] + z[17] + -z[55] + z[59];
z[38] = z[29] * z[38];
z[42] = z[17] + -z[20];
z[42] = z[31] * z[42];
z[38] = z[38] + z[42];
z[42] = T(-2) * z[12] + z[13] + -z[17] + (T(13) * z[20]) / T(2) + -z[48] + z[62];
z[42] = z[25] * z[42];
z[40] = -z[25] + z[40] + -z[52];
z[40] = z[11] * z[40];
z[48] = -z[0] + -z[2] + -z[3];
z[48] = z[1] * z[48];
z[49] = -z[19] + z[62];
z[49] = z[10] * z[49];
z[52] = -(z[27] * z[68]);
return z[33] + z[34] + z[35] + z[36] + z[37] + T(3) * z[38] + z[39] + (T(3) * z[40]) / T(2) + z[41] + z[42] + z[43] + z[44] + z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + z[51] + z[52];
}



template IntegrandConstructorType<double> f_3_27_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_27_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_27_construct (const Kin<qd_real>&);
#endif

}