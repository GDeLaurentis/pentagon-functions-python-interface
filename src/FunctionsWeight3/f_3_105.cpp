#include "f_3_105.h"

namespace PentagonFunctions {

template <typename T> T f_3_105_abbreviated (const std::array<T,36>&);

template <typename T> class SpDLog_f_3_105_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_105_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-4) * kin.v[0] + T(2) * kin.v[1] + T(4) * kin.v[2] + T(8) * kin.v[3] + T(-4) * kin.v[4]) + T(4) * kin.v[0] * kin.v[4] + T(-4) * kin.v[2] * kin.v[4] + T(-8) * kin.v[3] * kin.v[4] + T(2) * prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(-4) + T(-2) * kin.v[1]) + kin.v[4] * (T(4) + T(2) * kin.v[4]));
c[1] = (T(-8) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[2] * abb[3] * T(4) + abb[3] * (abb[3] * T(-4) + abb[4] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[5] * T(4)) + abb[6] * T(8) + abb[1] * (abb[2] * T(-4) + abb[3] * T(-4) + abb[5] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4) + abb[4] * T(4) + abb[1] * T(8)));
    }
};
template <typename T> class SpDLog_f_3_105_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_105_W_22 (const Kin<T>& kin) {
        c[0] = (T(-5) / T(2) + (T(-45) * kin.v[3]) / T(8) + (T(-35) * kin.v[4]) / T(4)) * kin.v[3] + (T(-5) / T(2) + (T(-25) * kin.v[4]) / T(8)) * kin.v[4] + ((T(-5) * kin.v[2]) / T(2) + (T(5) * kin.v[3]) / T(2) + (T(5) * kin.v[4]) / T(2)) * kin.v[0] + (T(5) / T(2) + (T(-25) * kin.v[2]) / T(8) + (T(35) * kin.v[3]) / T(4) + (T(25) * kin.v[4]) / T(4)) * kin.v[2] + kin.v[1] * (T(5) / T(2) + (T(-5) * kin.v[0]) / T(2) + (T(-35) * kin.v[2]) / T(4) + (T(45) * kin.v[3]) / T(4) + (T(35) * kin.v[4]) / T(4) + (T(-45) / T(8) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(4) * kin.v[0] + T(-4) * kin.v[3])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(-2) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]));
c[1] = (T(3) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + (T(3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[11] * ((abb[10] * T(3)) / T(2) + abb[4] * (-abb[4] / T(2) + abb[5] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4)) + abb[9] * (abb[9] / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) + abb[5] * abb[9] * T(4) + abb[2] * (abb[9] * T(-4) + abb[4] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_105_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl19 = DLog_W_19<T>(kin),dl22 = DLog_W_22<T>(kin),dl4 = DLog_W_4<T>(kin),dl2 = DLog_W_2<T>(kin),dl15 = DLog_W_15<T>(kin),dl16 = DLog_W_16<T>(kin),dl3 = DLog_W_3<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl1 = DLog_W_1<T>(kin),dl17 = DLog_W_17<T>(kin),dl28 = DLog_W_28<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),spdl12 = SpDLog_f_3_105_W_12<T>(kin),spdl22 = SpDLog_f_3_105_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,36> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(v_path[3]), rlog(-v_path[4]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_4(kin_path), dl19(t), rlog(v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), dl22(t), dl4(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl2(t), f_2_1_5(kin_path), f_2_1_7(kin_path), rlog(-v_path[4] + v_path[2]), dl15(t), dl16(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl3(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl5(t), dl20(t), dl1(t), dl17(t), dl28(t) / kin_path.SqrtDelta, f_2_2_6(kin_path), f_1_3_4(kin_path), f_1_3_5(kin_path), dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta}
;

        auto result = f_3_105_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_3_105_abbreviated(const std::array<T,36>& abb)
{
using TR = typename T::value_type;
T z[82];
z[0] = abb[1];
z[1] = abb[7];
z[2] = abb[12];
z[3] = abb[15];
z[4] = abb[23];
z[5] = abb[26];
z[6] = abb[28];
z[7] = abb[29];
z[8] = abb[2];
z[9] = abb[20];
z[10] = abb[3];
z[11] = abb[4];
z[12] = abb[5];
z[13] = abb[8];
z[14] = bc<TR>[0];
z[15] = abb[9];
z[16] = abb[27];
z[17] = abb[19];
z[18] = abb[6];
z[19] = abb[10];
z[20] = abb[13];
z[21] = abb[14];
z[22] = abb[16];
z[23] = abb[17];
z[24] = abb[18];
z[25] = abb[21];
z[26] = abb[22];
z[27] = abb[24];
z[28] = abb[25];
z[29] = abb[30];
z[30] = abb[31];
z[31] = abb[32];
z[32] = abb[33];
z[33] = abb[34];
z[34] = abb[35];
z[35] = T(3) * z[2];
z[36] = T(2) * z[1];
z[37] = z[35] + -z[36];
z[38] = T(4) * z[4];
z[39] = T(2) * z[17];
z[40] = z[38] + -z[39];
z[41] = T(2) * z[3];
z[42] = -z[5] + z[7];
z[43] = T(2) * z[9];
z[44] = T(3) * z[6];
z[45] = T(-4) * z[16] + -z[37] + -z[40] + -z[41] + z[42] + -z[43] + z[44];
z[45] = z[15] * z[45];
z[46] = T(2) * z[16];
z[47] = z[39] + z[46];
z[48] = -z[44] + z[47];
z[49] = T(2) * z[7];
z[50] = T(5) * z[9] + z[49];
z[51] = T(3) * z[5];
z[52] = T(7) * z[4];
z[53] = -z[36] + -z[48] + z[50] + -z[51] + z[52];
z[53] = z[11] * z[53];
z[54] = T(2) * z[4];
z[55] = T(3) * z[7];
z[56] = z[54] + -z[55];
z[57] = T(5) * z[3] + -z[44];
z[58] = z[2] + z[5];
z[59] = T(6) * z[9] + -z[36] + z[56] + z[57] + -z[58];
z[59] = z[0] * z[59];
z[60] = T(4) * z[17];
z[61] = z[5] + z[16];
z[62] = z[4] + z[61];
z[57] = T(-7) * z[7] + z[43] + z[57] + z[60] + z[62];
z[63] = -(z[24] * z[57]);
z[64] = -z[9] + z[61];
z[65] = T(3) * z[4];
z[66] = T(8) * z[3] + -z[55] + -z[60] + -z[64] + z[65];
z[66] = z[13] * z[66];
z[67] = z[2] + -z[7];
z[68] = z[9] + z[67];
z[69] = -z[44] + z[68];
z[70] = -z[16] + z[54];
z[36] = z[3] + z[36] + z[39] + z[69] + z[70];
z[36] = z[10] * z[36];
z[71] = z[43] + z[55];
z[41] = z[41] + z[65];
z[51] = -z[41] + z[47] + z[51] + -z[71];
z[51] = z[12] * z[51];
z[72] = -z[4] + -z[6] + z[61];
z[73] = z[28] * z[72];
z[74] = z[6] + -z[9];
z[75] = z[26] * z[74];
z[73] = z[73] + z[75];
z[75] = z[33] + z[34];
z[76] = T(-2) * z[29] + T(-4) * z[75];
z[77] = z[31] + z[32];
z[76] = z[76] * z[77];
z[77] = z[3] + -z[5];
z[78] = T(3) * z[16];
z[79] = T(3) * z[9] + -z[77] + -z[78];
z[79] = z[8] * z[79];
z[80] = -z[3] + z[6] + z[9] + -z[16] + -z[67];
z[80] = T(3) * z[80];
z[81] = -(z[21] * z[80]);
z[36] = z[36] + z[45] + z[51] + z[53] + z[59] + z[63] + z[66] + T(3) * z[73] + z[76] + z[79] + z[81];
z[36] = int_to_imaginary<T>(1) * z[36];
z[45] = z[2] / T(2);
z[51] = z[4] + -z[7];
z[51] = (T(107) * z[3]) / T(8) + z[5] + z[16] / T(4) + (T(23) * z[51]) / T(8);
z[51] = -z[6] / T(2) + -z[9] / T(12) + (T(-10) * z[17]) / T(3) + (T(-4) * z[29]) / T(3) + z[45] + z[51] / T(3) + (T(-8) * z[75]) / T(3);
z[51] = z[14] * z[51];
z[36] = z[36] + z[51];
z[36] = z[14] * z[36];
z[41] = z[1] + z[41];
z[51] = z[5] + z[41] + z[69];
z[53] = z[0] + -z[8];
z[53] = z[51] * z[53];
z[59] = -z[1] + z[3];
z[62] = -z[39] + z[59] + z[62];
z[63] = z[13] * z[62];
z[66] = T(3) * z[3];
z[40] = z[40] + z[66];
z[69] = -z[5] + z[16];
z[68] = z[40] + z[68] + T(-2) * z[69];
z[68] = z[11] * z[68];
z[62] = -(z[15] * z[62]);
z[69] = z[7] + -z[69];
z[40] = z[9] + z[40] + -z[45] + z[69] / T(2);
z[40] = z[10] * z[40];
z[40] = z[40] + z[53] + z[62] + z[63] + z[68];
z[40] = z[10] * z[40];
z[53] = -z[5] + z[49];
z[46] = -z[3] + z[43] + z[46] + z[53] + z[54];
z[46] = z[11] * z[46];
z[39] = z[3] + -z[39] + T(-2) * z[61] + z[65] + z[71];
z[39] = z[13] * z[39];
z[47] = -z[2] + -z[9] + z[47] + -z[52] + -z[53];
z[47] = z[10] * z[47];
z[52] = (T(3) * z[7]) / T(2);
z[53] = z[3] / T(2);
z[54] = (T(3) * z[4]) / T(2) + z[52] + -z[53] + -z[64];
z[54] = z[12] * z[54];
z[61] = z[5] + z[67];
z[62] = -z[3] + -z[9] + z[38] + z[61];
z[62] = z[0] * z[62];
z[67] = z[4] + z[16];
z[68] = -z[9] + -z[42] + -z[67];
z[68] = z[15] * z[68];
z[69] = z[8] * z[77];
z[39] = z[39] + z[46] + z[47] + z[54] + z[62] + T(2) * z[68] + z[69];
z[39] = z[12] * z[39];
z[46] = T(5) * z[4];
z[47] = (T(3) * z[6]) / T(2);
z[54] = z[5] / T(2);
z[45] = -z[1] + -z[45] + -z[46] + z[47] + -z[52] + -z[53] + -z[54];
z[45] = prod_pow(z[0], 2) * z[45];
z[52] = T(2) * z[5];
z[38] = -z[38] + z[48] + z[52] + -z[59];
z[38] = z[11] * z[38];
z[48] = z[4] / T(2);
z[49] = (T(-3) * z[3]) / T(2) + z[48] + -z[49] + z[60] + -z[64] / T(2);
z[49] = z[13] * z[49];
z[53] = -z[7] + z[9];
z[46] = z[1] + T(-7) * z[3] + z[44] + -z[46] + T(-4) * z[53];
z[46] = z[0] * z[46];
z[38] = z[38] + z[46] + z[49];
z[38] = z[13] * z[38];
z[35] = z[35] + -z[44] + z[59];
z[44] = -z[35] + -z[52] + z[55] + -z[67];
z[44] = z[11] * z[44];
z[46] = (T(3) * z[2]) / T(2);
z[48] = -z[7] + -z[9] + -z[16] + z[46] + z[48] + -z[54];
z[48] = z[15] * z[48];
z[44] = z[44] + z[48] + z[63];
z[44] = z[15] * z[44];
z[48] = -z[7] + z[43];
z[35] = z[35] + z[48] + z[65] + z[78];
z[35] = z[15] * z[35];
z[49] = z[0] * z[51];
z[51] = -z[5] + z[55];
z[46] = -z[3] + -z[46] + z[47] + z[51] / T(2);
z[46] = z[8] * z[46];
z[47] = z[1] + z[3] + z[5] + -z[50] + -z[65];
z[47] = z[11] * z[47];
z[35] = z[35] + z[46] + z[47] + z[49];
z[35] = z[8] * z[35];
z[41] = -z[41] + z[43] + -z[61];
z[41] = z[0] * z[41];
z[43] = (T(5) * z[9]) / T(2);
z[46] = -z[1] + -z[7] / T(2) + z[43] + z[54] + z[70];
z[46] = z[11] * z[46];
z[41] = z[41] + z[46];
z[41] = z[11] * z[41];
z[46] = z[22] * z[57];
z[47] = -(z[27] * z[72]);
z[49] = z[25] * z[74];
z[42] = -z[3] + z[4] + z[42];
z[42] = z[23] * z[42];
z[42] = z[42] + z[47] + z[49];
z[47] = -z[1] + -z[4] + z[48] + -z[58] + z[66];
z[47] = z[18] * z[47];
z[37] = z[37] + -z[43] + z[56];
z[37] = z[19] * z[37];
z[43] = -z[29] + T(-2) * z[75];
z[43] = z[30] * z[43];
z[48] = z[20] * z[80];
return z[35] + z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + T(3) * z[42] + z[43] + z[44] + z[45] + z[46] + T(2) * z[47] + z[48];
}



template IntegrandConstructorType<double> f_3_105_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_105_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_105_construct (const Kin<qd_real>&);
#endif

}