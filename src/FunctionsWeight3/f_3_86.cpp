#include "f_3_86.h"

namespace PentagonFunctions {

template <typename T> T f_3_86_abbreviated (const std::array<T,32>&);

template <typename T> class SpDLog_f_3_86_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_86_W_22 (const Kin<T>& kin) {
        c[0] = (T(23) / T(2) + (T(69) * kin.v[2]) / T(8) + (T(-23) * kin.v[3]) / T(4) + (T(-69) * kin.v[4]) / T(4)) * kin.v[2] + kin.v[1] * (T(23) / T(2) + (T(-23) * kin.v[0]) / T(2) + (T(23) * kin.v[2]) / T(4) + (T(23) * kin.v[3]) / T(4) + (T(-23) * kin.v[4]) / T(4) + (T(-23) / T(8) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-4) * kin.v[0] + T(4) * kin.v[3])) + (T(-23) / T(2) + (T(-23) * kin.v[3]) / T(8) + (T(23) * kin.v[4]) / T(4)) * kin.v[3] + (T(-23) / T(2) + (T(69) * kin.v[4]) / T(8)) * kin.v[4] + ((T(-23) * kin.v[2]) / T(2) + (T(23) * kin.v[3]) / T(2) + (T(23) * kin.v[4]) / T(2)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = (T(9) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + (T(9) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + (T(-9) * kin.v[4]) / T(2);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[15] * ((abb[16] * T(9)) / T(2) + abb[9] * abb[12] * T(-4) + abb[9] * ((abb[9] * T(7)) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) + abb[7] * (abb[8] * T(-4) + abb[9] * T(4)) + abb[8] * ((abb[8] * T(-7)) / T(2) + bc<T>[0] * int_to_imaginary<T>(4) + abb[12] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_86_construct (const Kin<T>& kin) {
    return [&kin, 
            dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl30 = DLog_W_30<T>(kin),dl13 = DLog_W_13<T>(kin),dl6 = DLog_W_6<T>(kin),dl22 = DLog_W_22<T>(kin),dl4 = DLog_W_4<T>(kin),dl20 = DLog_W_20<T>(kin),dl5 = DLog_W_5<T>(kin),dl3 = DLog_W_3<T>(kin),dl16 = DLog_W_16<T>(kin),dl18 = DLog_W_18<T>(kin),dl19 = DLog_W_19<T>(kin),dl17 = DLog_W_17<T>(kin),dl1 = DLog_W_1<T>(kin),spdl22 = SpDLog_f_3_86_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,32> abbr = 
            {dl27(t) / kin_path.SqrtDelta, f_2_2_8(kin_path), dl28(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl13(t), rlog(v_path[0]), rlog(v_path[2]), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_1(kin_path), dl6(t), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_9(kin_path), dl22(t), f_2_1_14(kin_path), dl4(t), f_2_1_8(kin_path), dl20(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl5(t), dl3(t), f_2_1_2(kin_path), dl16(t), dl18(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl19(t), dl17(t), dl1(t)}
;

        auto result = f_3_86_abbreviated(abbr);
        result = result + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_3_86_abbreviated(const std::array<T,32>& abb)
{
using TR = typename T::value_type;
T z[67];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[9];
z[10] = bc<TR>[0];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[22];
z[14] = abb[23];
z[15] = abb[26];
z[16] = abb[29];
z[17] = abb[17];
z[18] = abb[19];
z[19] = abb[25];
z[20] = abb[31];
z[21] = abb[12];
z[22] = abb[13];
z[23] = abb[30];
z[24] = abb[14];
z[25] = abb[16];
z[26] = abb[18];
z[27] = abb[20];
z[28] = abb[21];
z[29] = abb[24];
z[30] = abb[27];
z[31] = abb[28];
z[32] = T(3) * z[23];
z[33] = T(3) * z[20];
z[34] = z[32] + z[33];
z[35] = T(3) * z[19];
z[36] = -z[17] + z[35];
z[37] = T(5) * z[15];
z[38] = T(4) * z[12];
z[39] = -z[34] + z[36] + z[37] + z[38];
z[39] = z[22] * z[39];
z[40] = T(2) * z[17];
z[41] = -z[23] + z[40];
z[42] = T(2) * z[18];
z[43] = z[35] + z[41] + z[42];
z[43] = z[9] * z[43];
z[44] = T(2) * z[19];
z[45] = z[14] + -z[15];
z[46] = -z[17] + z[42] + -z[44] + z[45];
z[46] = z[7] * z[46];
z[47] = T(4) * z[4];
z[48] = z[38] + -z[47];
z[49] = z[13] + -z[16];
z[50] = -z[45] + -z[48] + T(-3) * z[49];
z[50] = z[5] * z[50];
z[47] = -z[14] + z[47];
z[51] = T(3) * z[18] + z[33];
z[36] = -z[36] + -z[47] + z[51];
z[36] = z[6] * z[36];
z[52] = z[14] + -z[20];
z[53] = -z[18] + z[19];
z[54] = z[49] + z[52] + z[53];
z[54] = T(3) * z[54];
z[55] = z[28] * z[54];
z[41] = z[18] + z[41];
z[56] = z[16] + -z[45];
z[56] = T(-7) * z[19] + -z[41] + T(3) * z[56];
z[56] = z[8] * z[56];
z[57] = z[15] + z[16] + -z[20] + -z[23];
z[57] = T(3) * z[57];
z[58] = -(z[31] * z[57]);
z[59] = z[21] * z[53];
z[36] = z[36] + z[39] + z[43] + z[46] + z[50] + z[55] + z[56] + z[58] + z[59];
z[36] = int_to_imaginary<T>(1) * z[36];
z[39] = (T(41) * z[14]) / T(12) + (T(49) * z[15]) / T(12) + (T(-3) * z[17]) / T(4) + (T(13) * z[18]) / T(6) + -z[19] / T(2) + -z[20] + (T(-5) * z[23]) / T(3);
z[39] = z[10] * z[39];
z[36] = z[36] + z[39] / T(2);
z[36] = z[10] * z[36];
z[39] = -z[40] + z[44];
z[46] = -z[16] + z[39];
z[50] = T(2) * z[4];
z[55] = T(-4) * z[13] + -z[45] + z[46] + z[50] + z[51];
z[55] = z[9] * z[55];
z[56] = z[4] + -z[12];
z[58] = z[45] + -z[56];
z[58] = z[8] * z[58];
z[56] = T(2) * z[45] + z[56];
z[56] = z[7] * z[56];
z[56] = z[56] + z[58];
z[58] = T(2) * z[12];
z[59] = z[17] + z[58];
z[60] = T(2) * z[14];
z[61] = -z[49] + -z[59] + z[60];
z[61] = z[22] * z[61];
z[62] = -z[45] + z[49];
z[48] = z[48] + (T(5) * z[62]) / T(2);
z[48] = z[5] * z[48];
z[48] = z[48] + z[55] + T(2) * z[56] + z[61];
z[48] = z[5] * z[48];
z[56] = -z[13] + z[15];
z[61] = T(4) * z[16];
z[63] = T(5) * z[19] + -z[40] + z[50] + z[56] + z[60] + -z[61];
z[63] = z[8] * z[63];
z[64] = z[17] / T(2);
z[60] = (T(-3) * z[49]) / T(2) + z[50] + -z[60] + -z[64];
z[60] = z[6] * z[60];
z[50] = z[17] + z[50];
z[65] = T(2) * z[15];
z[66] = -z[49] + z[50] + -z[65];
z[66] = z[5] * z[66];
z[49] = z[45] + z[49];
z[49] = z[22] * z[49];
z[50] = -z[14] + T(3) * z[15] + -z[50];
z[50] = z[7] * z[50];
z[49] = z[49] + z[50] + -z[55] + z[60] + z[63] + z[66];
z[49] = z[6] * z[49];
z[50] = -z[13] + z[45];
z[39] = -z[34] + -z[39] + -z[50] + -z[58] + z[61];
z[39] = z[5] * z[39];
z[55] = -z[23] + z[62];
z[60] = z[17] + z[18];
z[61] = z[44] + -z[55] + z[60];
z[61] = z[9] * z[61];
z[46] = -z[13] + z[14] + z[46] + z[58] + z[65];
z[63] = z[22] * z[46];
z[42] = -z[17] + -z[19] + -z[42] + z[55];
z[42] = z[8] * z[42];
z[32] = T(-3) * z[16] + z[32] + -z[53];
z[32] = z[21] * z[32];
z[55] = -(z[7] * z[53]);
z[32] = z[32] / T(2) + z[39] + z[42] + z[55] + z[61] + z[63];
z[32] = z[21] * z[32];
z[33] = z[33] + -z[37];
z[37] = (T(3) * z[23]) / T(2);
z[33] = (T(-3) * z[19]) / T(2) + z[33] / T(2) + z[37] + -z[58] + z[64];
z[33] = z[22] * z[33];
z[39] = -(z[8] * z[46]);
z[33] = z[33] + z[39];
z[33] = z[22] * z[33];
z[39] = -z[45] / T(2) + z[53] + z[64];
z[39] = z[7] * z[39];
z[41] = T(4) * z[19] + z[41];
z[41] = z[8] * z[41];
z[42] = T(-3) * z[14] + z[15] + z[59];
z[42] = z[22] * z[42];
z[39] = z[39] + z[41] + z[42] + -z[43];
z[39] = z[7] * z[39];
z[35] = z[23] / T(2) + -z[35] + (T(-3) * z[52]) / T(2) + -z[60];
z[35] = prod_pow(z[9], 2) * z[35];
z[41] = T(2) * z[23];
z[42] = z[19] + -z[41] + z[60] + z[62];
z[42] = z[9] * z[42];
z[37] = (T(5) * z[16]) / T(2) + z[18] / T(2) + z[37] + z[50];
z[37] = z[8] * z[37];
z[37] = z[37] + z[42];
z[37] = z[8] * z[37];
z[42] = z[14] + -z[16];
z[40] = -z[18] + (T(-21) * z[19]) / T(2) + -z[40] + z[41] + T(-5) * z[42];
z[40] = z[25] * z[40];
z[41] = -(z[27] * z[54]);
z[42] = -z[17] + z[44];
z[43] = -z[11] + z[24];
z[42] = z[42] * z[43];
z[43] = T(5) * z[13] + z[15] + -z[47] + -z[51];
z[43] = z[11] * z[43];
z[34] = -z[14] + -z[15] + T(-5) * z[16] + z[34] + z[38];
z[34] = z[24] * z[34];
z[38] = -z[17] + z[23] + -z[53] + z[56];
z[38] = z[26] * z[38];
z[44] = z[29] * z[45];
z[45] = z[0] + z[2] + T(-2) * z[3];
z[45] = z[1] * z[45];
z[46] = -(z[30] * z[57]);
return z[32] + z[33] + z[34] + z[35] + z[36] + z[37] + T(2) * z[38] + z[39] + z[40] + z[41] + z[42] + z[43] + T(-6) * z[44] + z[45] + z[46] + z[48] + z[49];
}



template IntegrandConstructorType<double> f_3_86_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_86_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_86_construct (const Kin<qd_real>&);
#endif

}