#include "f_3_96.h"

namespace PentagonFunctions {

template <typename T> T f_3_96_abbreviated (const std::array<T,40>&);

template <typename T> class SpDLog_f_3_96_W_10 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_96_W_10 (const Kin<T>& kin) {
        c[0] = (T(-23) / T(2) + (T(69) * kin.v[2]) / T(8) + (T(-23) * kin.v[4]) / T(2)) * kin.v[2] + kin.v[1] * (T(-23) / T(2) + (T(69) * kin.v[2]) / T(4) + (T(-23) * kin.v[4]) / T(2) + (T(69) / T(8) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(4) * kin.v[2] + T(-4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]);
c[1] = (T(-9) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + (T(-9) * kin.v[2]) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[2];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[19] * ((abb[21] * T(9)) / T(2) + abb[20] * ((abb[20] * T(7)) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) + abb[17] * ((abb[17] * T(-7)) / T(2) + abb[15] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4)) + abb[15] * abb[20] * T(4) + abb[14] * (abb[20] * T(-4) + abb[17] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_96_construct (const Kin<T>& kin) {
    return [&kin, 
            dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl30 = DLog_W_30<T>(kin),dl11 = DLog_W_11<T>(kin),dl10 = DLog_W_10<T>(kin),dl6 = DLog_W_6<T>(kin),dl3 = DLog_W_3<T>(kin),dl4 = DLog_W_4<T>(kin),dl2 = DLog_W_2<T>(kin),dl17 = DLog_W_17<T>(kin),dl16 = DLog_W_16<T>(kin),dl1 = DLog_W_1<T>(kin),dl19 = DLog_W_19<T>(kin),dl5 = DLog_W_5<T>(kin),dl18 = DLog_W_18<T>(kin),spdl10 = SpDLog_f_3_96_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);

        std::array<std::complex<T>,40> abbr = 
            {dl27(t) / kin_path.SqrtDelta, f_2_2_1(kin_path), f_2_2_2(kin_path), f_2_2_3(kin_path), f_2_2_4(kin_path), f_2_2_5(kin_path), f_2_2_6(kin_path), f_2_2_7(kin_path), f_2_2_8(kin_path), f_2_2_9(kin_path), dl28(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl11(t), rlog(v_path[0]), rlog(-v_path[1]), rlog(v_path[2]), rlog(v_path[3]), rlog(-v_path[4]), f_2_1_2(kin_path), dl10(t), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_7(kin_path), dl6(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_9(kin_path), dl3(t), f_2_1_4(kin_path), dl4(t), f_2_1_1(kin_path), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl2(t), dl17(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl16(t), dl1(t), dl19(t), dl5(t), dl18(t)}
;

        auto result = f_3_96_abbreviated(abbr);
        result = result + spdl10(t, abbr);

        return result;
    };
}

template <typename T> T f_3_96_abbreviated(const std::array<T,40>& abb)
{
using TR = typename T::value_type;
T z[82];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[9];
z[10] = abb[10];
z[11] = abb[11];
z[12] = abb[12];
z[13] = abb[13];
z[14] = abb[14];
z[15] = abb[15];
z[16] = abb[16];
z[17] = abb[17];
z[18] = bc<TR>[0];
z[19] = abb[18];
z[20] = abb[22];
z[21] = abb[27];
z[22] = abb[35];
z[23] = abb[37];
z[24] = abb[39];
z[25] = abb[25];
z[26] = abb[31];
z[27] = abb[36];
z[28] = abb[38];
z[29] = abb[20];
z[30] = abb[32];
z[31] = abb[23];
z[32] = abb[21];
z[33] = abb[24];
z[34] = abb[26];
z[35] = abb[28];
z[36] = abb[29];
z[37] = abb[30];
z[38] = abb[33];
z[39] = abb[34];
z[40] = z[21] + -z[24];
z[41] = z[25] + z[40];
z[42] = T(2) * z[30];
z[43] = T(2) * z[28];
z[44] = -z[41] + z[42] + -z[43];
z[44] = z[15] * z[44];
z[45] = T(3) * z[22];
z[46] = T(4) * z[20];
z[47] = T(-3) * z[23] + z[40] + z[45] + z[46];
z[47] = z[13] * z[47];
z[48] = T(2) * z[25];
z[49] = -z[26] + z[48];
z[50] = T(3) * z[28];
z[42] = z[42] + z[49] + z[50];
z[51] = z[29] * z[42];
z[52] = z[24] + z[25];
z[53] = z[46] + -z[52];
z[54] = T(3) * z[27];
z[55] = z[50] + -z[54];
z[56] = T(3) * z[30];
z[57] = -z[53] + -z[55] + z[56];
z[57] = z[31] * z[57];
z[58] = T(3) * z[26];
z[55] = T(-5) * z[21] + z[25] + -z[55] + z[58];
z[55] = z[16] * z[55];
z[59] = -z[28] + z[30];
z[60] = -z[22] + z[23];
z[61] = -z[24] + z[27] + z[59] + -z[60];
z[61] = T(3) * z[61];
z[62] = -(z[39] * z[61]);
z[63] = T(4) * z[12];
z[64] = z[13] + -z[16];
z[65] = -(z[63] * z[64]);
z[66] = z[22] + -z[26];
z[67] = z[21] + -z[27] + z[66];
z[67] = T(3) * z[67];
z[68] = -(z[37] * z[67]);
z[44] = z[44] + z[47] + z[51] + -z[55] + z[57] + z[62] + z[65] + z[68];
z[44] = int_to_imaginary<T>(1) * z[44];
z[47] = (T(3) * z[28]) / T(2);
z[51] = (T(49) * z[21]) / T(12) + (T(29) * z[24]) / T(12) + (T(-3) * z[25]) / T(4) + (T(-5) * z[26]) / T(3) + (T(19) * z[30]) / T(6) + -z[47] + -z[60];
z[51] = z[18] * z[51];
z[44] = z[44] + z[51] / T(2);
z[44] = z[18] * z[44];
z[51] = z[24] + -z[25];
z[57] = T(4) * z[22];
z[62] = -z[21] + z[23];
z[65] = T(2) * z[20];
z[51] = T(-5) * z[28] + T(-2) * z[51] + z[57] + z[62] + -z[65];
z[68] = -(z[31] * z[51]);
z[49] = z[30] + z[49];
z[69] = T(3) * z[24];
z[70] = T(3) * z[21];
z[45] = T(-7) * z[28] + z[45] + -z[49] + -z[69] + z[70];
z[71] = int_to_imaginary<T>(1) * z[18];
z[45] = z[45] * z[71];
z[72] = z[30] / T(2);
z[73] = z[26] / T(2);
z[74] = T(2) * z[21];
z[75] = (T(5) * z[22]) / T(2) + z[28] + z[52] + -z[72] + z[73] + -z[74];
z[75] = z[17] * z[75];
z[76] = z[23] + z[48];
z[77] = z[22] + -z[24] + -z[43] + -z[74] + z[76];
z[77] = z[16] * z[77];
z[78] = T(2) * z[12];
z[64] = z[64] * z[78];
z[64] = z[64] + z[77];
z[50] = -z[23] + -z[24] + T(-3) * z[25] + -z[50] + -z[66] + z[70];
z[50] = z[14] * z[50];
z[66] = T(2) * z[13];
z[77] = -z[20] + -z[40];
z[77] = z[66] * z[77];
z[79] = z[40] + z[60];
z[80] = z[25] + z[30];
z[81] = T(-2) * z[26] + z[28] + z[79] + z[80];
z[81] = z[29] * z[81];
z[49] = T(4) * z[28] + z[49];
z[49] = z[15] * z[49];
z[45] = z[45] + z[49] + z[50] + z[64] + z[68] + z[75] + z[77] + z[81];
z[45] = z[17] * z[45];
z[49] = z[29] * z[51];
z[50] = z[27] + z[30];
z[50] = z[47] + (T(-3) * z[50]) / T(2) + -z[52] / T(2) + z[65];
z[50] = z[31] * z[50];
z[51] = -z[52] + -z[65] + z[70];
z[51] = z[15] * z[51];
z[68] = z[25] + -z[60] + z[65] + -z[74];
z[68] = z[13] * z[68];
z[70] = -z[40] + z[60];
z[70] = z[16] * z[70];
z[49] = z[49] + z[50] + z[51] + z[68] + z[70];
z[49] = z[31] * z[49];
z[50] = z[43] + z[54];
z[51] = z[50] + z[58];
z[54] = z[40] + -z[51] + z[57] + z[76];
z[54] = z[13] * z[54];
z[43] = z[26] + z[43] + -z[79] + z[80];
z[43] = z[29] * z[43];
z[57] = z[25] + z[62];
z[58] = (T(3) * z[22]) / T(2) + -z[73];
z[62] = z[28] / T(2) + z[57] + -z[58] + -z[72];
z[62] = z[14] * z[62];
z[68] = z[15] + -z[71];
z[68] = z[59] * z[68];
z[43] = z[43] + z[54] + z[62] + -z[64] + z[68];
z[43] = z[14] * z[43];
z[50] = z[50] + z[56];
z[48] = -z[22] + T(-4) * z[23] + z[40] + -z[48] + z[50] + z[65];
z[48] = z[13] * z[48];
z[42] = -(z[15] * z[42]);
z[47] = (T(3) * z[23]) / T(2) + -z[25] + (T(-5) * z[30]) / T(2) + -z[47] + -z[58];
z[47] = z[29] * z[47];
z[42] = z[42] + z[47] + z[48];
z[42] = z[29] * z[42];
z[47] = T(5) * z[22];
z[48] = -z[21] + -z[47] + z[51] + -z[52] + z[63];
z[48] = z[19] * z[48];
z[46] = -z[46] + (T(5) * z[79]) / T(2);
z[46] = z[13] * z[46];
z[51] = z[20] + T(-2) * z[40];
z[51] = z[15] * z[51];
z[52] = T(2) * z[24] + -z[25] + -z[60];
z[52] = z[16] * z[52];
z[46] = z[46] + T(2) * z[51] + z[52];
z[46] = z[13] * z[46];
z[50] = z[21] + T(5) * z[23] + -z[50] + -z[53];
z[50] = z[33] * z[50];
z[51] = -z[0] + -z[10] + -z[11];
z[52] = -z[1] + -z[2] + z[3] + -z[4] + -z[5] + -z[6] + -z[7] + z[8] + z[9];
z[51] = z[51] * z[52];
z[52] = -z[26] + z[57] + -z[59];
z[52] = z[34] * z[52];
z[41] = z[41] / T(2) + -z[59];
z[41] = prod_pow(z[15], 2) * z[41];
z[53] = z[21] + z[25] + -z[69];
z[53] = z[15] * z[53];
z[53] = z[53] + z[55] / T(2);
z[53] = z[16] * z[53];
z[54] = -(z[38] * z[61]);
z[55] = z[25] + -z[26];
z[47] = T(-5) * z[24] + (T(-21) * z[28]) / T(2) + -z[30] + z[47] + T(-2) * z[55];
z[47] = z[32] * z[47];
z[55] = z[15] + -z[16];
z[55] = z[16] * z[55];
z[56] = -z[15] + -z[16] + z[66];
z[56] = z[13] * z[56];
z[55] = z[55] + z[56];
z[55] = z[55] * z[78];
z[56] = -(z[36] * z[67]);
z[40] = z[35] * z[40];
return T(6) * z[40] + z[41] + z[42] + z[43] + z[44] + z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + z[51] + T(2) * z[52] + z[53] + z[54] + z[55] + z[56];
}



template IntegrandConstructorType<double> f_3_96_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_96_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_96_construct (const Kin<qd_real>&);
#endif

}