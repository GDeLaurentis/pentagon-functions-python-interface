#include "f_3_33.h"

namespace PentagonFunctions {

template <typename T> T f_3_33_abbreviated (const std::array<T,11>&);

template <typename T> class SpDLog_f_3_33_W_23 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_3_33_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (T(-2) + kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[3]) + kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[1], 2) + -prod_pow(abb[2], 2));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_33_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl9 = DLog_W_9<T>(kin),dl20 = DLog_W_20<T>(kin),dl4 = DLog_W_4<T>(kin),dl17 = DLog_W_17<T>(kin),spdl23 = SpDLog_f_3_33_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,11> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), dl9(t), rlog(v_path[3]), dl20(t), f_2_1_6(kin_path), f_2_1_8(kin_path), rlog(v_path[0] + v_path[1]), dl4(t), dl17(t)}
;

        auto result = f_3_33_abbreviated(abbr);
        result = result + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_3_33_abbreviated(const std::array<T,11>& abb)
{
using TR = typename T::value_type;
T z[17];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[5];
z[3] = abb[2];
z[4] = abb[9];
z[5] = abb[10];
z[6] = abb[4];
z[7] = bc<TR>[0];
z[8] = abb[6];
z[9] = abb[7];
z[10] = abb[8];
z[11] = z[8] + -z[9];
z[12] = z[2] + -z[4];
z[13] = -z[5] + z[12];
z[14] = T(2) * z[13];
z[11] = z[11] * z[14];
z[15] = -(z[6] * z[14]);
z[16] = z[3] * z[4];
z[15] = z[15] + z[16];
z[15] = z[3] * z[15];
z[16] = z[3] + -z[10];
z[13] = z[13] * z[16];
z[16] = z[1] + -z[5];
z[16] = z[6] * z[16];
z[13] = z[13] + z[16];
z[13] = int_to_imaginary<T>(1) * z[13];
z[16] = T(-2) * z[5] + -z[12];
z[16] = z[1] + z[16] / T(3);
z[16] = z[7] * z[16];
z[13] = T(2) * z[13] + z[16];
z[13] = z[7] * z[13];
z[14] = z[3] * z[14];
z[16] = z[1] + -z[2];
z[16] = z[0] * z[16];
z[14] = z[14] + z[16];
z[14] = z[0] * z[14];
z[12] = -z[1] + z[12];
z[12] = prod_pow(z[6], 2) * z[12];
return z[11] + z[12] + z[13] + z[14] + z[15];
}



template IntegrandConstructorType<double> f_3_33_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_33_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_33_construct (const Kin<qd_real>&);
#endif

}