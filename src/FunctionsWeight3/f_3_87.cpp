#include "f_3_87.h"

namespace PentagonFunctions {

template <typename T> T f_3_87_abbreviated (const std::array<T,32>&);

template <typename T> class SpDLog_f_3_87_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_87_W_22 (const Kin<T>& kin) {
        c[0] = ((T(27) * kin.v[2]) / T(4) + (T(-13) * kin.v[3]) / T(2) + (T(-27) * kin.v[4]) / T(2) + T(7)) * kin.v[2] + kin.v[1] * ((T(13) * kin.v[2]) / T(2) + kin.v[3] / T(2) + (T(-13) * kin.v[4]) / T(2) + T(7) + T(-7) * kin.v[0] + (T(-1) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-4) * kin.v[0] + T(4) * kin.v[3])) + (-kin.v[3] / T(4) + (T(13) * kin.v[4]) / T(2) + T(-7)) * kin.v[3] + ((T(27) * kin.v[4]) / T(4) + T(-7)) * kin.v[4] + kin.v[0] * (T(-7) * kin.v[2] + T(7) * kin.v[3] + T(7) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[1] + T(3) * kin.v[2] + T(-3) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + T(-3) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[20] * (abb[9] * abb[12] * T(-4) + abb[9] * (bc<T>[0] * int_to_imaginary<T>(-4) + abb[9] * T(2)) + abb[17] * T(3) + abb[7] * (abb[8] * T(-4) + abb[9] * T(4)) + abb[8] * (abb[8] * T(-2) + bc<T>[0] * int_to_imaginary<T>(4) + abb[12] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_87_construct (const Kin<T>& kin) {
    return [&kin, 
            dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl30 = DLog_W_30<T>(kin),dl13 = DLog_W_13<T>(kin),dl6 = DLog_W_6<T>(kin),dl17 = DLog_W_17<T>(kin),dl18 = DLog_W_18<T>(kin),dl22 = DLog_W_22<T>(kin),dl4 = DLog_W_4<T>(kin),dl19 = DLog_W_19<T>(kin),dl3 = DLog_W_3<T>(kin),dl1 = DLog_W_1<T>(kin),dl20 = DLog_W_20<T>(kin),dl5 = DLog_W_5<T>(kin),dl16 = DLog_W_16<T>(kin),spdl22 = SpDLog_f_3_87_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,32> abbr = 
            {dl27(t) / kin_path.SqrtDelta, f_2_2_8(kin_path), dl28(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl13(t), rlog(v_path[0]), rlog(v_path[2]), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_1(kin_path), dl6(t), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_9(kin_path), dl17(t), f_2_1_8(kin_path), f_2_1_14(kin_path), dl18(t), f_2_1_2(kin_path), dl22(t), dl4(t), dl19(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl3(t), dl1(t), dl20(t), dl5(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl16(t)}
;

        auto result = f_3_87_abbreviated(abbr);
        result = result + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_3_87_abbreviated(const std::array<T,32>& abb)
{
using TR = typename T::value_type;
T z[78];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[9];
z[10] = bc<TR>[0];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[15];
z[14] = abb[18];
z[15] = abb[21];
z[16] = abb[22];
z[17] = abb[25];
z[18] = abb[26];
z[19] = abb[27];
z[20] = abb[28];
z[21] = abb[31];
z[22] = abb[12];
z[23] = abb[13];
z[24] = abb[14];
z[25] = abb[16];
z[26] = abb[17];
z[27] = abb[19];
z[28] = abb[23];
z[29] = abb[24];
z[30] = abb[29];
z[31] = abb[30];
z[32] = T(7) * z[5];
z[33] = (T(3) * z[6]) / T(2);
z[34] = T(3) * z[7];
z[35] = -z[32] + z[33] + z[34];
z[35] = z[6] * z[35];
z[36] = T(3) * z[27];
z[37] = T(2) * z[24];
z[38] = z[36] + z[37];
z[39] = T(5) * z[5];
z[40] = -z[8] + z[22];
z[41] = z[39] + z[40];
z[41] = z[5] * z[41];
z[42] = -z[8] + z[9];
z[43] = z[5] + z[42];
z[43] = T(2) * z[43];
z[44] = (T(3) * z[7]) / T(2);
z[45] = -z[43] + z[44];
z[45] = z[7] * z[45];
z[46] = z[23] / T(2);
z[47] = z[40] + z[46];
z[48] = T(4) * z[6];
z[49] = -z[7] + z[48];
z[50] = T(-3) * z[5] + -z[47] + z[49];
z[50] = z[23] * z[50];
z[51] = T(3) * z[26];
z[52] = prod_pow(z[8], 2);
z[53] = T(2) * z[52];
z[54] = T(3) * z[25];
z[55] = z[22] * z[42];
z[56] = T(3) * z[8];
z[57] = -z[9] + z[56];
z[58] = z[9] * z[57];
z[35] = z[35] + z[38] + z[41] + z[45] + z[50] + -z[51] + -z[53] + -z[54] + -z[55] + z[58];
z[35] = z[15] * z[35];
z[41] = T(2) * z[7];
z[45] = -z[23] + z[41];
z[50] = T(2) * z[5];
z[58] = -z[6] + z[50];
z[59] = T(4) * z[8] + -z[9];
z[60] = T(3) * z[29];
z[61] = T(3) * z[31] + -z[45] + -z[58] + T(-2) * z[59] + z[60];
z[61] = z[21] * z[61];
z[62] = T(2) * z[9];
z[63] = T(9) * z[6] + z[8] + T(8) * z[23] + -z[34] + -z[39] + -z[60] + z[62];
z[63] = z[17] * z[63];
z[64] = z[8] + z[9];
z[60] = -z[6] + z[60];
z[65] = T(4) * z[5];
z[45] = z[22] + z[45] + z[60] + T(-2) * z[64] + -z[65];
z[45] = z[18] * z[45];
z[66] = T(3) * z[6];
z[43] = z[23] + -z[34] + z[43] + -z[66];
z[43] = z[15] * z[43];
z[67] = T(-8) * z[12] + -z[13];
z[68] = -z[5] + z[23];
z[67] = z[67] * z[68];
z[68] = T(3) * z[20];
z[69] = z[5] + z[8];
z[70] = -z[29] + -z[31] + z[69];
z[70] = z[68] * z[70];
z[71] = T(4) * z[4];
z[72] = -z[5] + z[6];
z[73] = -(z[71] * z[72]);
z[74] = -z[5] + z[7];
z[75] = z[42] + z[74];
z[60] = z[60] + T(2) * z[75];
z[60] = z[19] * z[60];
z[76] = z[8] + -z[29];
z[76] = z[16] * z[76];
z[77] = T(-9) * z[5] + T(8) * z[6] + z[7] + T(7) * z[23] + -z[40];
z[77] = z[14] * z[77];
z[43] = z[43] + z[45] + z[60] + z[61] + z[63] + z[67] + z[70] + z[73] + T(3) * z[76] + z[77];
z[43] = int_to_imaginary<T>(1) * z[10] * z[43];
z[39] = -z[7] + z[39] + T(-2) * z[40] + -z[46] + -z[48];
z[39] = z[23] * z[39];
z[45] = T(-2) * z[6] + z[41] + z[50] + z[56];
z[45] = z[6] * z[45];
z[46] = -z[40] + -z[50];
z[46] = z[5] * z[46];
z[48] = T(2) * z[42];
z[56] = -z[48] + z[74];
z[56] = z[7] * z[56];
z[60] = prod_pow(z[9], 2);
z[39] = T(-4) * z[24] + T(-7) * z[26] + T(3) * z[30] + z[39] + z[45] + z[46] + z[52] + -z[54] + T(2) * z[55] + z[56] + (T(-5) * z[60]) / T(2);
z[39] = z[21] * z[39];
z[34] = z[34] + z[50] + -z[59] + -z[66];
z[34] = z[6] * z[34];
z[32] = -z[32] + z[44] + -z[48];
z[32] = z[7] * z[32];
z[44] = T(3) * z[22];
z[45] = (T(9) * z[5]) / T(2) + -z[44] + z[59];
z[45] = z[5] * z[45];
z[46] = -z[7] + z[23];
z[46] = z[5] + T(-5) * z[6] + T(3) * z[40] + T(-4) * z[46];
z[46] = z[23] * z[46];
z[48] = z[9] / T(2);
z[56] = -z[8] + z[48];
z[56] = z[9] * z[56];
z[32] = -z[26] + z[32] + z[34] + z[38] + z[45] + z[46] + z[53] + T(3) * z[55] + z[56];
z[32] = z[17] * z[32];
z[34] = T(2) * z[8];
z[38] = z[9] + z[34];
z[41] = z[38] + -z[41] + z[58];
z[41] = z[6] * z[41];
z[45] = z[52] / T(2);
z[41] = z[41] + z[45];
z[46] = z[47] + z[74];
z[46] = z[23] * z[46];
z[47] = -z[34] + -z[48];
z[47] = z[9] * z[47];
z[34] = -z[9] + z[34];
z[48] = z[5] + -z[22];
z[52] = z[34] + z[48];
z[52] = z[5] * z[52];
z[53] = -z[8] + -z[22] + z[62] + -z[74];
z[53] = z[7] * z[53];
z[58] = z[22] / T(2);
z[59] = z[8] + -z[58];
z[59] = z[22] * z[59];
z[47] = -z[36] + z[37] + z[41] + z[46] + z[47] + z[52] + z[53] + z[59];
z[47] = z[18] * z[47];
z[34] = -z[34] + z[58];
z[34] = z[22] * z[34];
z[49] = T(6) * z[5] + z[42] + -z[49];
z[49] = z[6] * z[49];
z[52] = z[40] + z[65];
z[52] = T(-7) * z[6] + -z[7] + (T(-7) * z[23]) / T(2) + T(2) * z[52];
z[52] = z[23] * z[52];
z[53] = z[25] + z[45];
z[59] = z[8] * z[9];
z[57] = (T(-5) * z[5]) / T(2) + T(-2) * z[22] + z[57];
z[57] = z[5] * z[57];
z[60] = -z[7] / T(2) + z[40];
z[60] = z[7] * z[60];
z[34] = T(-5) * z[11] + T(-6) * z[24] + z[34] + -z[36] + z[49] + z[52] + T(3) * z[53] + z[57] + -z[59] + z[60];
z[34] = z[14] * z[34];
z[36] = z[5] + z[38];
z[33] = z[33] + -z[36];
z[33] = z[6] * z[33];
z[38] = z[40] + z[72];
z[38] = z[23] * z[38];
z[48] = z[9] + z[48];
z[48] = z[5] * z[48];
z[33] = z[26] + z[33] + z[37] + z[38] + z[45] + z[48] + -z[55] + z[59];
z[33] = z[16] * z[33];
z[38] = z[42] + z[58];
z[45] = -(z[22] * z[38]);
z[48] = z[5] / T(2);
z[49] = z[22] + z[48];
z[49] = z[5] * z[49];
z[52] = z[6] / T(2) + -z[69];
z[52] = z[6] * z[52];
z[57] = z[23] * z[72];
z[45] = -z[30] + z[45] + z[49] + z[52] + -z[53] + z[57] + z[59];
z[45] = z[45] * z[68];
z[38] = z[38] * z[44];
z[44] = -(z[9] * z[64]);
z[36] = -(z[5] * z[36]);
z[49] = z[5] + -z[42];
z[49] = -z[7] + T(2) * z[49];
z[49] = z[7] * z[49];
z[36] = z[36] + z[38] + z[41] + z[44] + z[49] + z[54];
z[36] = z[19] * z[36];
z[38] = z[53] + z[55] + z[56];
z[41] = z[5] * z[7];
z[37] = -z[37] + z[41];
z[44] = -z[40] + z[48];
z[44] = z[5] * z[44];
z[38] = -z[37] + T(3) * z[38] + z[44] + z[46] + z[51];
z[38] = z[13] * z[38];
z[44] = z[40] + -z[50];
z[44] = z[5] * z[44];
z[40] = z[23] + -z[40] + -z[74];
z[40] = z[23] * z[40];
z[37] = z[37] + z[40] + z[44];
z[37] = z[12] * z[37];
z[40] = z[42] + -z[50];
z[40] = z[5] * z[40];
z[42] = z[6] + -z[75];
z[42] = z[6] * z[42];
z[40] = z[40] + z[41] + z[42];
z[40] = z[4] * z[40];
z[41] = z[18] + z[19];
z[42] = T(7) * z[15] + -z[17] + T(-2) * z[21] + -z[41] + z[68] + -z[71];
z[42] = z[11] * z[42];
z[44] = -z[0] + -z[2] + T(-2) * z[3];
z[44] = z[1] * z[44];
z[46] = (T(-55) * z[15]) / T(2) + (T(109) * z[17]) / T(2) + z[21];
z[46] = -z[13] + -z[16] + (T(11) * z[19]) / T(6) + z[46] / T(6);
z[46] = (T(41) * z[14]) / T(8) + (T(-4) * z[18]) / T(3) + z[46] / T(2);
z[46] = prod_pow(z[10], 2) * z[46];
z[41] = z[16] + z[17] + z[20] + -z[21] + -z[41];
z[41] = z[28] * z[41];
return z[32] + z[33] + z[34] + z[35] + z[36] + T(4) * z[37] + z[38] + z[39] + T(2) * z[40] + T(3) * z[41] + z[42] + z[43] + z[44] + z[45] + z[46] + z[47];
}



template IntegrandConstructorType<double> f_3_87_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_87_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_87_construct (const Kin<qd_real>&);
#endif

}