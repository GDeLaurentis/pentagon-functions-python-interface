#include "f_3_73.h"

namespace PentagonFunctions {

template <typename T> T f_3_73_abbreviated (const std::array<T,31>&);

template <typename T> class SpDLog_f_3_73_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_73_W_22 (const Kin<T>& kin) {
        c[0] = ((T(9) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2)) * kin.v[0] + (T(-9) / T(2) + (T(9) * kin.v[0]) / T(2) + (T(21) * kin.v[1]) / T(8) + (T(3) * kin.v[2]) / T(4) + (T(-21) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(4)) * kin.v[1] + (T(9) / T(2) + (T(21) * kin.v[3]) / T(8) + (T(3) * kin.v[4]) / T(4)) * kin.v[3] + (T(9) / T(2) + (T(-15) * kin.v[4]) / T(8)) * kin.v[4] + (T(-9) / T(2) + (T(-15) * kin.v[2]) / T(8) + (T(-3) * kin.v[3]) / T(4) + (T(15) * kin.v[4]) / T(4)) * kin.v[2];
c[1] = (T(-3) * kin.v[1]) / T(2) + (T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * ((prod_pow(abb[2], 2) * T(-3)) / T(2) + (abb[3] * T(-3)) / T(2) + (prod_pow(abb[1], 2) * T(3)) / T(2));
    }
};
template <typename T> class SpDLog_f_3_73_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_73_W_12 (const Kin<T>& kin) {
        c[0] = T(-2) * kin.v[0] * kin.v[4] + T(2) * kin.v[2] * kin.v[4] + T(4) * kin.v[3] * kin.v[4] + -prod_pow(kin.v[4], 2) + kin.v[1] * (-kin.v[1] + T(2) * kin.v[0] + T(-2) * kin.v[2] + T(-4) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(2) + kin.v[1]) + (-kin.v[4] + T(-2)) * kin.v[4]);
c[1] = (bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[19] * (abb[9] * T(-4) + abb[6] * abb[20] * T(-2) + abb[6] * (abb[7] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2) + abb[1] * T(2) + abb[6] * T(2)) + abb[5] * (abb[5] * T(-4) + abb[1] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[6] * T(2) + abb[7] * T(2) + abb[20] * T(2)));
    }
};
template <typename T> class SpDLog_f_3_73_W_23 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_73_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (T(-5) / T(2) + (T(5) * kin.v[1]) / T(2) + (T(29) * kin.v[2]) / T(4) + (T(19) * kin.v[3]) / T(4) + (T(-29) * kin.v[4]) / T(4) + (T(-19) / T(8) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(4) * kin.v[1] + T(-4) * kin.v[3])) + (T(-5) / T(2) + (T(-39) * kin.v[4]) / T(8)) * kin.v[4] + ((T(-5) * kin.v[2]) / T(2) + (T(-5) * kin.v[3]) / T(2) + (T(5) * kin.v[4]) / T(2)) * kin.v[1] + (T(5) / T(2) + (T(-19) * kin.v[3]) / T(8) + (T(29) * kin.v[4]) / T(4)) * kin.v[3] + (T(5) / T(2) + (T(-39) * kin.v[2]) / T(8) + (T(-29) * kin.v[3]) / T(4) + (T(39) * kin.v[4]) / T(4)) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[4] * (T(-4) + T(-2) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = (T(-3) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + (T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + (T(-3) * kin.v[4]) / T(2);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[21] * ((abb[17] * T(-3)) / T(2) + abb[1] * (abb[1] / T(2) + abb[2] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4)) + abb[20] * (abb[7] * T(-4) + abb[1] * T(4)) + abb[7] * (-abb[7] / T(2) + bc<T>[0] * int_to_imaginary<T>(4) + abb[2] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_73_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl3 = DLog_W_3<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl30 = DLog_W_30<T>(kin),dl18 = DLog_W_18<T>(kin),dl12 = DLog_W_12<T>(kin),dl23 = DLog_W_23<T>(kin),dl20 = DLog_W_20<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),dl5 = DLog_W_5<T>(kin),dl4 = DLog_W_4<T>(kin),dl2 = DLog_W_2<T>(kin),dl19 = DLog_W_19<T>(kin),spdl22 = SpDLog_f_3_73_W_22<T>(kin),spdl12 = SpDLog_f_3_73_W_12<T>(kin),spdl23 = SpDLog_f_3_73_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,31> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), dl3(t), rlog(-v_path[1]), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_4(kin_path), f_2_1_7(kin_path), f_2_1_15(kin_path), dl27(t) / kin_path.SqrtDelta, f_2_2_7(kin_path), dl28(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl18(t), f_2_1_8(kin_path), f_2_1_11(kin_path), dl12(t), rlog(v_path[3]), dl23(t), dl20(t), dl16(t), dl17(t), dl5(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl4(t), dl2(t), dl19(t)}
;

        auto result = f_3_73_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_3_73_abbreviated(const std::array<T,31>& abb)
{
using TR = typename T::value_type;
T z[63];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[16];
z[3] = abb[22];
z[4] = abb[24];
z[5] = abb[29];
z[6] = abb[30];
z[7] = abb[2];
z[8] = abb[23];
z[9] = abb[25];
z[10] = abb[28];
z[11] = abb[6];
z[12] = abb[7];
z[13] = abb[8];
z[14] = abb[5];
z[15] = abb[20];
z[16] = bc<TR>[0];
z[17] = abb[3];
z[18] = abb[9];
z[19] = abb[10];
z[20] = abb[11];
z[21] = abb[12];
z[22] = abb[13];
z[23] = abb[14];
z[24] = abb[15];
z[25] = abb[17];
z[26] = abb[18];
z[27] = abb[26];
z[28] = abb[27];
z[29] = int_to_imaginary<T>(1) * z[16];
z[30] = -z[15] + z[29];
z[31] = z[0] / T(2);
z[32] = z[14] + -z[30] + z[31];
z[32] = z[0] * z[32];
z[33] = T(2) * z[14];
z[34] = z[30] + z[33];
z[35] = z[11] + (T(7) * z[13]) / T(2) + -z[34];
z[35] = z[13] * z[35];
z[36] = z[15] / T(2) + -z[29];
z[36] = z[15] * z[36];
z[37] = z[28] * z[29];
z[37] = z[27] + z[37];
z[37] = T(3) * z[37];
z[38] = T(2) * z[26];
z[39] = prod_pow(z[16], 2);
z[40] = (T(9) * z[14]) / T(2) + T(-4) * z[15] + z[29];
z[40] = z[14] * z[40];
z[41] = z[7] / T(2) + -z[30];
z[42] = -(z[7] * z[41]);
z[43] = T(3) * z[30];
z[44] = T(4) * z[14] + z[43];
z[45] = -z[0] + -z[44];
z[45] = z[11] * z[45];
z[46] = z[0] + z[14];
z[47] = -z[7] + z[46];
z[48] = -z[11] + z[47];
z[49] = -(z[12] * z[48]);
z[32] = z[17] + T(5) * z[18] + z[32] + z[35] + z[36] + z[37] + z[38] + (T(-23) * z[39]) / T(24) + z[40] + z[42] + z[45] + z[49];
z[32] = z[6] * z[32];
z[35] = z[30] + z[46];
z[35] = -z[11] + T(2) * z[35];
z[35] = z[11] * z[35];
z[35] = -z[18] + z[35];
z[40] = T(2) * z[29];
z[42] = -z[15] + z[40];
z[42] = z[15] * z[42];
z[45] = T(3) * z[19];
z[49] = z[42] + -z[45];
z[47] = z[13] / T(2) + -z[30] + -z[47];
z[47] = z[13] * z[47];
z[47] = z[38] + z[47];
z[50] = -z[31] + -z[34];
z[50] = z[0] * z[50];
z[51] = z[0] + -z[14];
z[52] = z[41] + -z[51];
z[52] = z[7] * z[52];
z[53] = z[11] + -z[14];
z[54] = T(2) * z[0];
z[55] = z[53] + -z[54];
z[56] = T(2) * z[30];
z[57] = z[12] / T(2) + -z[55] + z[56];
z[57] = z[12] * z[57];
z[40] = -z[14] + -z[15] + -z[40];
z[40] = z[14] * z[40];
z[40] = -z[35] + z[37] + -z[39] / T(6) + z[40] + -z[47] + -z[49] + z[50] + z[52] + z[57];
z[40] = z[5] * z[40];
z[31] = -z[31] + z[34];
z[31] = z[0] * z[31];
z[41] = -z[41] + z[54];
z[41] = z[7] * z[41];
z[50] = z[7] + z[56];
z[52] = z[0] + z[50];
z[53] = z[12] + -z[52] + -z[53];
z[53] = z[12] * z[53];
z[57] = z[25] + z[45];
z[58] = T(2) * z[17];
z[56] = z[14] + z[56];
z[59] = z[14] * z[56];
z[31] = z[31] + -z[35] + (T(-19) * z[39]) / T(12) + z[41] + -z[42] + z[53] + z[57] + -z[58] + z[59];
z[31] = z[4] * z[31];
z[35] = T(2) * z[7];
z[41] = -z[30] + z[35] + z[46];
z[41] = z[7] * z[41];
z[34] = -(z[14] * z[34]);
z[42] = z[14] + z[30];
z[53] = -z[7] + T(2) * z[13] + -z[42] + -z[54];
z[53] = z[13] * z[53];
z[35] = -z[0] + -z[35] + -z[43];
z[35] = z[12] * z[35];
z[43] = T(3) * z[20];
z[59] = z[39] / T(2);
z[60] = z[25] + z[59];
z[61] = -z[14] + T(4) * z[30];
z[61] = z[0] * z[61];
z[62] = T(3) * z[0] + -z[11] + T(2) * z[42];
z[62] = z[11] * z[62];
z[34] = (T(13) * z[17]) / T(2) + T(-2) * z[18] + z[34] + z[35] + z[41] + z[43] + z[49] + z[53] + (T(3) * z[60]) / T(2) + z[61] + z[62];
z[34] = z[8] * z[34];
z[35] = z[13] * z[48];
z[35] = z[35] + -z[38] + -z[57];
z[38] = T(3) * z[11];
z[41] = -z[38] + z[44] + z[54];
z[41] = z[11] * z[41];
z[44] = z[30] * z[54];
z[37] = T(3) * z[18] + z[37];
z[49] = z[7] * z[46];
z[53] = (T(-5) * z[14]) / T(2) + T(3) * z[15];
z[53] = z[14] * z[53];
z[41] = z[35] + T(-3) * z[36] + -z[37] + (T(9) * z[39]) / T(8) + z[41] + z[44] + z[49] + z[53];
z[41] = z[9] * z[41];
z[44] = T(2) * z[25] + z[47] + z[58];
z[47] = -z[11] / T(2) + z[42];
z[38] = z[38] * z[47];
z[47] = z[7] * z[51];
z[29] = -z[14] + T(2) * z[15] + z[29];
z[29] = z[14] * z[29];
z[51] = z[0] * z[56];
z[29] = z[29] + -z[36] + -z[37] + z[38] + (T(3) * z[39]) / T(8) + z[44] + z[47] + z[51];
z[29] = z[10] * z[29];
z[36] = (T(3) * z[11]) / T(2);
z[37] = -z[36] + z[46];
z[37] = z[11] * z[37];
z[38] = z[7] + (T(-3) * z[13]) / T(2) + -z[55];
z[38] = z[13] * z[38];
z[51] = prod_pow(z[0], 2);
z[51] = z[51] / T(2);
z[53] = prod_pow(z[14], 2);
z[37] = -z[17] + z[18] + z[37] + z[38] + -z[43] + -z[45] + -z[49] + -z[51] + z[53] / T(2);
z[37] = z[1] * z[37];
z[36] = -z[14] + z[36] + -z[54];
z[36] = z[11] * z[36];
z[35] = -z[35] + z[36] + z[47] + z[51] + z[53] + z[59];
z[35] = z[2] * z[35];
z[36] = -z[7] + z[30] + z[54];
z[36] = z[3] * z[36];
z[38] = z[11] + -z[46] + -z[50];
z[38] = z[9] * z[38];
z[43] = z[2] * z[48];
z[33] = -z[0] + z[7] + T(2) * z[11] + -z[33];
z[33] = z[1] * z[33];
z[45] = -(z[10] * z[52]);
z[46] = -z[3] + -z[9];
z[46] = z[10] + z[46] / T(2);
z[46] = z[12] * z[46];
z[33] = z[33] + z[36] + z[38] + z[43] + z[45] + z[46];
z[33] = z[12] * z[33];
z[30] = -z[14] / T(2) + -z[30];
z[30] = z[14] * z[30];
z[36] = (T(-3) * z[0]) / T(2) + -z[42];
z[36] = z[0] * z[36];
z[30] = z[30] + z[36] + z[39] / T(3) + -z[44] + z[49];
z[30] = z[3] * z[30];
z[36] = z[21] + z[23] + T(-2) * z[24];
z[36] = z[22] * z[36];
return z[29] + z[30] + z[31] + z[32] + z[33] + z[34] + z[35] + z[36] + z[37] + z[40] + z[41];
}



template IntegrandConstructorType<double> f_3_73_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_73_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_73_construct (const Kin<qd_real>&);
#endif

}