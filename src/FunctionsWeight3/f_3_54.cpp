#include "f_3_54.h"

namespace PentagonFunctions {

template <typename T> T f_3_54_abbreviated (const std::array<T,5>&);

template <typename T> class SpDLog_f_3_54_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_54_W_22 (const Kin<T>& kin) {
        c[0] = (-kin.v[3] / T(2) + -kin.v[4] / T(2) + kin.v[2] / T(2)) * kin.v[0] + (-kin.v[4] / T(4) + T(-1) / T(2) + kin.v[0] / T(2) + (T(3) * kin.v[1]) / T(8) + kin.v[2] / T(4) + (T(-3) * kin.v[3]) / T(4)) * kin.v[1] + (-kin.v[2] / T(8) + -kin.v[3] / T(4) + T(-1) / T(2) + kin.v[4] / T(4)) * kin.v[2] + (T(1) / T(2) + (T(3) * kin.v[3]) / T(8) + kin.v[4] / T(4)) * kin.v[3] + (-kin.v[4] / T(8) + T(1) / T(2)) * kin.v[4];
c[1] = kin.v[1] / T(2) + kin.v[2] / T(2) + -kin.v[3] / T(2) + -kin.v[4] / T(2);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[3] / T(2) + -prod_pow(abb[2], 2) / T(4) + abb[1] * (-abb[2] / T(2) + (abb[1] * T(3)) / T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_54_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl20 = DLog_W_20<T>(kin),spdl22 = SpDLog_f_3_54_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,5> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), dl20(t)}
;

        auto result = f_3_54_abbreviated(abbr);
        result = result + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_3_54_abbreviated(const std::array<T,5>& abb)
{
T z[6];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[2];
z[3] = abb[3];
z[4] = prod_pow(z[0], 2);
z[5] = -z[0] + (T(3) * z[2]) / T(2);
z[5] = z[2] * z[5];
z[4] = z[3] + -z[4] / T(2) + z[5];
return (z[1] * z[4]) / T(2);
}



template IntegrandConstructorType<double> f_3_54_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_54_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_54_construct (const Kin<qd_real>&);
#endif

}