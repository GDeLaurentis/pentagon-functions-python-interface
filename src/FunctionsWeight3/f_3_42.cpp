#include "f_3_42.h"

namespace PentagonFunctions {

template <typename T> T f_3_42_abbreviated (const std::array<T,18>&);



template <typename T> IntegrandConstructorType<T> f_3_42_construct (const Kin<T>& kin) {
    return [&kin, 
            dl15 = DLog_W_15<T>(kin),dl6 = DLog_W_6<T>(kin),dl3 = DLog_W_3<T>(kin),dl17 = DLog_W_17<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl19 = DLog_W_19<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,18> abbr = 
            {dl15(t), rlog(v_path[2]), rlog(-v_path[4]), dl6(t), rlog(v_path[0]), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl3(t), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_5(kin_path), f_2_1_7(kin_path), rlog(-v_path[4] + v_path[2]), dl17(t), f_2_1_9(kin_path), dl2(t), dl1(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl19(t)}
;

        auto result = f_3_42_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_3_42_abbreviated(const std::array<T,18>& abb)
{
using TR = typename T::value_type;
T z[34];
z[0] = abb[0];
z[1] = abb[1];
z[2] = bc<TR>[0];
z[3] = abb[2];
z[4] = abb[6];
z[5] = abb[11];
z[6] = abb[13];
z[7] = abb[7];
z[8] = abb[3];
z[9] = abb[4];
z[10] = abb[5];
z[11] = abb[14];
z[12] = abb[17];
z[13] = abb[8];
z[14] = abb[9];
z[15] = abb[10];
z[16] = abb[12];
z[17] = abb[15];
z[18] = abb[16];
z[19] = prod_pow(z[2], 2);
z[20] = int_to_imaginary<T>(1) * z[2];
z[21] = T(2) * z[20];
z[22] = -z[7] / T(2) + z[21];
z[22] = z[7] * z[22];
z[23] = -(z[15] * z[21]);
z[24] = z[3] / T(2);
z[25] = z[7] + z[24];
z[25] = z[3] * z[25];
z[26] = -z[7] + z[21];
z[27] = -z[1] + T(2) * z[26];
z[27] = z[1] * z[27];
z[22] = T(2) * z[13] + -z[14] + (T(11) * z[19]) / T(6) + z[22] + z[23] + z[25] + z[27];
z[22] = z[4] * z[22];
z[23] = -z[7] + -z[18];
z[23] = z[20] * z[23];
z[25] = -z[10] + z[21];
z[25] = z[10] * z[25];
z[27] = -z[7] + z[20];
z[28] = z[9] + T(-2) * z[27];
z[28] = z[9] * z[28];
z[23] = T(-2) * z[16] + -z[17] + (T(-2) * z[19]) / T(3) + z[23] + z[25] + z[28];
z[23] = z[11] * z[23];
z[28] = z[9] / T(2);
z[29] = -z[7] + -z[20] + z[28];
z[29] = z[9] * z[29];
z[30] = z[19] / T(6);
z[31] = -z[16] + z[30];
z[32] = z[18] * z[20];
z[33] = prod_pow(z[7], 2);
z[29] = z[17] + z[29] + -z[31] + z[32] + z[33] / T(2);
z[29] = z[12] * z[29];
z[27] = -z[27] + z[28];
z[27] = z[9] * z[27];
z[28] = z[15] * z[20];
z[27] = z[13] + z[27] + -z[28] + z[31];
z[27] = z[5] * z[27];
z[28] = -z[7] + z[15];
z[20] = z[20] * z[28];
z[24] = -z[7] + z[24];
z[24] = z[3] * z[24];
z[20] = -z[13] + z[14] + z[20] + z[24] + z[30];
z[20] = z[6] * z[20];
z[24] = z[5] * z[26];
z[26] = z[6] * z[7];
z[28] = -z[5] + -z[6];
z[28] = z[1] * z[28];
z[24] = z[24] + z[26] + z[28] / T(2);
z[24] = z[1] * z[24];
z[26] = prod_pow(z[3], 2);
z[28] = z[1] + -z[21];
z[28] = z[1] * z[28];
z[19] = -z[19] + -z[26] + z[28];
z[19] = z[0] * z[19];
z[21] = -z[9] + z[21];
z[21] = z[9] * z[21];
z[21] = z[21] + -z[25];
z[21] = z[8] * z[21];
return z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[27] + z[29];
}



template IntegrandConstructorType<double> f_3_42_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_42_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_42_construct (const Kin<qd_real>&);
#endif

}