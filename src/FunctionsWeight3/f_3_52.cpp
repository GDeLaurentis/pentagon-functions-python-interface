#include "f_3_52.h"

namespace PentagonFunctions {

template <typename T> T f_3_52_abbreviated (const std::array<T,11>&);

template <typename T> class SpDLog_f_3_52_W_22 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_3_52_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + kin.v[3] * (T(2) + kin.v[3]) + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[1], 2) + -prod_pow(abb[2], 2));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_52_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl8 = DLog_W_8<T>(kin),dl3 = DLog_W_3<T>(kin),dl16 = DLog_W_16<T>(kin),dl19 = DLog_W_19<T>(kin),spdl22 = SpDLog_f_3_52_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,11> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), dl8(t), rlog(v_path[2]), dl3(t), f_2_1_13(kin_path), f_2_1_14(kin_path), rlog(v_path[0] + v_path[4]), dl16(t), dl19(t)}
;

        auto result = f_3_52_abbreviated(abbr);
        result = result + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_3_52_abbreviated(const std::array<T,11>& abb)
{
using TR = typename T::value_type;
T z[17];
z[0] = abb[1];
z[1] = abb[9];
z[2] = abb[10];
z[3] = abb[4];
z[4] = abb[5];
z[5] = bc<TR>[0];
z[6] = abb[2];
z[7] = abb[3];
z[8] = abb[6];
z[9] = abb[7];
z[10] = abb[8];
z[11] = -z[2] + z[4];
z[12] = z[1] + z[11];
z[13] = z[0] * z[12];
z[14] = -(z[10] * z[12]);
z[15] = z[1] + -z[7];
z[16] = z[3] * z[15];
z[14] = z[13] + z[14] + z[16];
z[14] = int_to_imaginary<T>(1) * z[14];
z[16] = (T(2) * z[1]) / T(3) + -z[7] + -z[11] / T(3);
z[16] = z[5] * z[16];
z[14] = T(2) * z[14] + z[16];
z[14] = z[5] * z[14];
z[16] = z[8] + z[9];
z[12] = z[12] * z[16];
z[11] = z[7] + z[11];
z[11] = z[3] * z[11];
z[11] = z[11] + T(-2) * z[13];
z[11] = z[3] * z[11];
z[13] = z[4] + z[15];
z[13] = prod_pow(z[6], 2) * z[13];
z[15] = z[1] + -z[2];
z[15] = prod_pow(z[0], 2) * z[15];
return z[11] + T(2) * z[12] + z[13] + z[14] + z[15];
}



template IntegrandConstructorType<double> f_3_52_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_52_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_52_construct (const Kin<qd_real>&);
#endif

}