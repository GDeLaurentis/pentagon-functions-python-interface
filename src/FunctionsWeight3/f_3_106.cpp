#include "f_3_106.h"

namespace PentagonFunctions {

template <typename T> T f_3_106_abbreviated (const std::array<T,36>&);

template <typename T> class SpDLog_f_3_106_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_106_W_22 (const Kin<T>& kin) {
        c[0] = -prod_pow(kin.v[3], 2) + kin.v[2] * (T(5) * kin.v[2] + T(-4) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[0] + kin.v[1] + T(6) * kin.v[2] + T(-2) * kin.v[4]) + prod_pow(kin.v[4], 2) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]));
c[1] = (T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[6] * T(-4) + abb[4] * abb[5] * T(-2) + abb[3] * abb[5] * T(2) + abb[1] * (abb[5] * T(-2) + abb[2] * T(2)) + abb[5] * (bc<T>[0] * int_to_imaginary<T>(2) + abb[5] * T(2)) + abb[2] * (abb[2] * T(-4) + abb[3] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[4] * T(2) + abb[5] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_106_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl19 = DLog_W_19<T>(kin),dl9 = DLog_W_9<T>(kin),dl4 = DLog_W_4<T>(kin),dl16 = DLog_W_16<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl17 = DLog_W_17<T>(kin),dl3 = DLog_W_3<T>(kin),dl2 = DLog_W_2<T>(kin),dl15 = DLog_W_15<T>(kin),dl1 = DLog_W_1<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl26 = DLog_W_26<T>(kin),spdl22 = SpDLog_f_3_106_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,36> abbr = 
            {dl22(t), rlog(v_path[2]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), dl19(t), rlog(-v_path[1]), rlog(v_path[3]), f_2_1_4(kin_path), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl9(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl4(t), f_2_1_3(kin_path), f_2_1_8(kin_path), rlog(-v_path[1] + v_path[3]), dl16(t), dl5(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl20(t), dl17(t), dl3(t), dl2(t), dl15(t), dl1(t), dl28(t) / kin_path.SqrtDelta, f_2_2_6(kin_path), f_1_3_4(kin_path), f_1_3_5(kin_path), dl29(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta}
;

        auto result = f_3_106_abbreviated(abbr);
        result = result + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_3_106_abbreviated(const std::array<T,36>& abb)
{
using TR = typename T::value_type;
T z[84];
z[0] = abb[1];
z[1] = abb[7];
z[2] = abb[21];
z[3] = abb[24];
z[4] = abb[26];
z[5] = abb[27];
z[6] = abb[28];
z[7] = abb[29];
z[8] = abb[2];
z[9] = abb[16];
z[10] = abb[20];
z[11] = abb[25];
z[12] = abb[3];
z[13] = abb[4];
z[14] = abb[5];
z[15] = abb[8];
z[16] = bc<TR>[0];
z[17] = abb[13];
z[18] = abb[9];
z[19] = abb[6];
z[20] = abb[10];
z[21] = abb[11];
z[22] = abb[12];
z[23] = abb[14];
z[24] = abb[15];
z[25] = abb[17];
z[26] = abb[18];
z[27] = abb[19];
z[28] = abb[22];
z[29] = abb[23];
z[30] = abb[30];
z[31] = abb[31];
z[32] = abb[32];
z[33] = abb[33];
z[34] = abb[34];
z[35] = abb[35];
z[36] = int_to_imaginary<T>(1) * z[16];
z[37] = T(5) * z[36];
z[38] = z[29] * z[37];
z[39] = T(3) * z[36];
z[40] = z[22] * z[39];
z[41] = T(3) * z[21] + -z[40];
z[38] = z[38] + -z[41];
z[42] = -z[0] + z[18];
z[43] = T(2) * z[8];
z[44] = z[42] + -z[43];
z[44] = z[8] * z[44];
z[45] = T(7) * z[36];
z[46] = T(-6) * z[0] + (T(7) * z[12]) / T(2) + -z[18] + z[43] + z[45];
z[46] = z[12] * z[46];
z[47] = T(4) * z[8];
z[48] = z[13] / T(2);
z[49] = z[47] + -z[48];
z[50] = -z[18] + z[36];
z[51] = z[49] + T(3) * z[50];
z[51] = z[13] * z[51];
z[52] = T(2) * z[36];
z[53] = -z[0] + z[52];
z[54] = z[8] + -z[18];
z[55] = z[53] + z[54];
z[56] = z[12] + z[55];
z[57] = -(z[15] * z[56]);
z[58] = T(2) * z[12];
z[59] = T(-2) * z[0] + z[18] + z[36] + z[58];
z[60] = z[14] / T(2);
z[61] = -z[13] + z[59] + -z[60];
z[61] = z[14] * z[61];
z[62] = T(6) * z[26];
z[63] = prod_pow(z[18], 2);
z[64] = z[24] * z[36];
z[64] = -z[19] + z[64];
z[65] = prod_pow(z[16], 2);
z[66] = (T(5) * z[0]) / T(2) + z[39];
z[66] = z[0] * z[66];
z[44] = z[23] + -z[38] + z[44] + z[46] + z[51] + z[57] + z[61] + -z[62] + z[63] / T(2) + -z[64] + (T(-31) * z[65]) / T(24) + z[66];
z[44] = z[3] * z[44];
z[46] = T(2) * z[20];
z[41] = z[41] + z[46];
z[51] = (T(5) * z[0]) / T(4);
z[57] = z[51] + z[52];
z[57] = z[0] * z[57];
z[61] = T(2) * z[18];
z[66] = T(-19) * z[0] + T(23) * z[36];
z[66] = (T(25) * z[12]) / T(4) + -z[61] + z[66] / T(2);
z[66] = z[12] * z[66];
z[67] = T(4) * z[12];
z[68] = -z[0] + z[36];
z[69] = z[13] + z[67] + T(3) * z[68];
z[69] = z[13] * z[69];
z[70] = T(3) * z[12];
z[71] = -z[13] + z[15] / T(2) + T(-2) * z[55] + -z[70];
z[71] = z[15] * z[71];
z[72] = z[24] * z[52];
z[72] = T(2) * z[23] + -z[72];
z[73] = z[29] * z[36];
z[74] = (T(-5) * z[18]) / T(2) + z[39];
z[74] = z[18] * z[74];
z[75] = -z[18] + z[53];
z[76] = T(2) * z[75];
z[77] = -z[8] + -z[76];
z[77] = z[8] * z[77];
z[78] = z[12] + z[68];
z[78] = (T(3) * z[14]) / T(2) + T(5) * z[78];
z[78] = z[14] * z[78];
z[57] = T(-2) * z[19] + z[41] + T(3) * z[57] + (T(5) * z[65]) / T(12) + z[66] + z[69] + z[71] + -z[72] + (T(-21) * z[73]) / T(2) + z[74] + z[77] + z[78];
z[57] = z[2] * z[57];
z[47] = -z[12] + z[39] + -z[42] + z[47];
z[47] = z[7] * z[47];
z[66] = z[8] + z[53] + z[61] + z[70];
z[66] = z[9] * z[66];
z[69] = T(4) * z[6];
z[71] = z[68] * z[69];
z[74] = z[4] * z[13];
z[71] = z[71] + z[74];
z[74] = T(3) * z[8];
z[77] = z[5] + z[10];
z[78] = -(z[74] * z[77]);
z[55] = -z[12] + z[55];
z[55] = z[1] * z[55];
z[79] = z[4] * z[68];
z[80] = -(z[10] * z[39]);
z[81] = z[0] + T(-4) * z[36];
z[81] = z[5] * z[81];
z[69] = z[5] + z[69];
z[82] = z[12] * z[69];
z[83] = -z[4] + z[5] + T(-3) * z[7];
z[83] = z[1] + -z[9] + z[83] / T(2);
z[83] = z[15] * z[83];
z[47] = z[47] + z[55] + z[66] + z[71] + z[78] + -z[79] + z[80] + z[81] + z[82] + z[83];
z[47] = z[15] * z[47];
z[55] = z[37] + -z[61];
z[55] = z[18] * z[55];
z[42] = -z[42] + z[74];
z[42] = z[8] * z[42];
z[66] = -z[0] + -z[36] + z[61];
z[78] = z[58] + z[66];
z[80] = -z[43] + -z[78];
z[80] = z[12] * z[80];
z[49] = -z[49] + -z[50];
z[49] = z[13] * z[49];
z[81] = z[27] * z[39];
z[42] = z[42] + -z[46] + z[49] + z[55] + z[62] + -z[64] + (T(41) * z[65]) / T(24) + z[80] + -z[81];
z[42] = z[9] * z[42];
z[49] = z[15] + z[43] + -z[66] + -z[70];
z[49] = z[15] * z[49];
z[55] = z[0] + -z[29];
z[55] = z[52] * z[55];
z[62] = z[8] / T(2);
z[66] = z[62] + z[76];
z[66] = z[8] * z[66];
z[70] = z[12] * z[78];
z[48] = z[48] + z[59];
z[48] = z[13] * z[48];
z[59] = -(z[18] * z[50]);
z[48] = z[46] + z[48] + z[49] + z[55] + z[59] + -z[64] + (T(-3) * z[65]) / T(8) + z[66] + z[70];
z[48] = z[11] * z[48];
z[49] = z[65] / T(2);
z[55] = z[49] + -z[81];
z[59] = T(4) * z[0];
z[64] = z[18] + -z[37] + z[59];
z[66] = z[12] * z[64];
z[70] = prod_pow(z[0], 2);
z[76] = -z[0] + T(-4) * z[18] + z[37];
z[76] = z[8] * z[76];
z[38] = z[38] + z[55] + (T(5) * z[63]) / T(2) + z[66] + T(-4) * z[70] + z[76];
z[38] = z[7] * z[38];
z[59] = -z[39] + z[54] + z[59] + -z[67];
z[59] = z[7] * z[59];
z[43] = z[12] + T(3) * z[13] + -z[43] + z[75];
z[43] = z[9] * z[43];
z[56] = -(z[1] * z[56]);
z[63] = T(2) * z[68];
z[58] = z[8] + -z[13] + -z[58] + -z[63];
z[58] = z[11] * z[58];
z[66] = -z[4] + -z[10];
z[66] = -z[11] + T(3) * z[66];
z[60] = z[60] * z[66];
z[43] = z[43] + z[56] + z[58] + z[59] + z[60];
z[43] = z[14] * z[43];
z[56] = z[19] + z[21];
z[40] = -z[40] + T(3) * z[56];
z[56] = z[0] * z[63];
z[56] = -z[40] + -z[46] + z[56] + (T(5) * z[65]) / T(4) + -z[73];
z[56] = z[4] * z[56];
z[58] = (T(5) * z[8]) / T(2) + -z[64];
z[58] = z[8] * z[58];
z[59] = -(z[12] * z[75]);
z[41] = T(5) * z[19] + z[41] + -z[49] + z[58] + z[59] + (T(3) * z[70]) / T(2);
z[41] = z[1] * z[41];
z[49] = z[36] + z[54];
z[54] = z[12] + -z[13] + -z[15];
z[49] = z[49] * z[54];
z[54] = z[50] * z[61];
z[50] = z[8] + -z[50];
z[50] = z[8] * z[50];
z[49] = z[49] + z[50] + z[54] + (T(5) * z[65]) / T(3) + -z[72];
z[49] = z[17] * z[49];
z[50] = -z[0] / T(2) + -z[36];
z[50] = z[0] * z[50];
z[46] = -z[46] + z[50] + z[65] / T(3) + z[81];
z[46] = z[5] * z[46];
z[50] = z[18] + -z[62];
z[50] = z[50] * z[77];
z[53] = -(z[10] * z[53]);
z[54] = z[5] * z[36];
z[50] = z[50] + z[53] + -z[54] + -z[79];
z[50] = z[50] * z[74];
z[53] = -(z[5] * z[68]);
z[58] = z[4] + -z[69];
z[58] = z[12] * z[58];
z[53] = z[53] + z[58] + -z[71] + T(2) * z[79];
z[53] = z[13] * z[53];
z[39] = -(z[0] * z[39]);
z[39] = z[39] + -z[40] + -z[55];
z[39] = z[10] * z[39];
z[40] = -z[5] + z[11];
z[55] = (T(9) * z[6]) / T(2);
z[58] = T(5) * z[7];
z[40] = (T(21) * z[2]) / T(2) + T(5) * z[3] + z[4] + T(2) * z[40] + -z[55] + -z[58];
z[40] = z[28] * z[40];
z[59] = z[32] + z[33];
z[59] = z[36] * z[59];
z[59] = z[59] + (T(2) * z[65]) / T(3);
z[59] = z[31] + T(2) * z[59];
z[60] = z[30] + z[34] + T(-2) * z[35];
z[59] = z[59] * z[60];
z[60] = z[10] * z[36];
z[54] = z[54] + z[60];
z[54] = T(3) * z[54];
z[60] = -(z[18] * z[54]);
z[45] = -z[45] + z[51];
z[45] = z[0] * z[45];
z[45] = z[45] + T(-2) * z[65];
z[45] = z[6] * z[45];
z[36] = z[36] * z[55];
z[51] = z[5] * z[52];
z[36] = z[36] + z[51];
z[36] = z[29] * z[36];
z[51] = z[6] * z[68];
z[52] = -z[5] + (T(-23) * z[6]) / T(2);
z[52] = z[12] * z[52];
z[51] = T(-9) * z[51] + z[52];
z[51] = z[12] * z[51];
z[37] = -(z[7] * z[37]);
z[37] = z[37] + z[54];
z[37] = z[24] * z[37];
z[52] = z[9] + z[11] + z[58] + T(-3) * z[77];
z[52] = z[23] * z[52];
z[54] = -z[7] + -z[9] + z[77];
z[54] = z[25] * z[54];
return z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + z[43] + z[44] + z[45] + z[46] + z[47] + z[48] + T(2) * z[49] + z[50] + z[51] / T(2) + z[52] + z[53] + T(3) * z[54] + z[56] + z[57] + z[59] + z[60];
}



template IntegrandConstructorType<double> f_3_106_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_106_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_106_construct (const Kin<qd_real>&);
#endif

}