#include "f_3_108.h"

namespace PentagonFunctions {

template <typename T> T f_3_108_abbreviated (const std::array<T,37>&);

template <typename T> class SpDLog_f_3_108_W_21 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_108_W_21 (const Kin<T>& kin) {
        c[0] = (T(5) / T(2) + (T(45) * kin.v[1]) / T(8) + (T(-35) * kin.v[2]) / T(4) + (T(-45) * kin.v[3]) / T(4) + (T(-5) * kin.v[4]) / T(2)) * kin.v[1] + (T(-5) / T(2) + (T(45) * kin.v[3]) / T(8) + (T(5) * kin.v[4]) / T(2)) * kin.v[3] + (T(-5) / T(2) + (T(25) * kin.v[2]) / T(8) + (T(35) * kin.v[3]) / T(4) + (T(5) * kin.v[4]) / T(2)) * kin.v[2] + kin.v[0] * (T(5) / T(2) + (T(35) * kin.v[1]) / T(4) + (T(-25) * kin.v[2]) / T(4) + (T(-35) * kin.v[3]) / T(4) + (T(-5) * kin.v[4]) / T(2) + (T(25) / T(8) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(-4) * kin.v[2] + T(4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[3] * (T(4) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(-4) + T(-2) * kin.v[1] + T(4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = (T(3) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + (T(3) * kin.v[1]) / T(2) + (T(-3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * ((abb[5] * T(-3)) / T(2) + abb[4] * (-abb[4] / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) + abb[2] * (abb[2] / T(2) + abb[3] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4)) + abb[3] * abb[4] * T(4) + abb[1] * (abb[4] * T(-4) + abb[2] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_108_construct (const Kin<T>& kin) {
    return [&kin, 
            dl21 = DLog_W_21<T>(kin),dl16 = DLog_W_16<T>(kin),dl15 = DLog_W_15<T>(kin),dl3 = DLog_W_3<T>(kin),dl5 = DLog_W_5<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),dl25 = DLog_W_25<T>(kin),dl1 = DLog_W_1<T>(kin),dl17 = DLog_W_17<T>(kin),dl2 = DLog_W_2<T>(kin),dl19 = DLog_W_19<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl28 = DLog_W_28<T>(kin),spdl21 = SpDLog_f_3_108_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,37> abbr = 
            {dl21(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_15(kin_path), dl16(t), rlog(v_path[2]), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_7(kin_path), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl15(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl3(t), f_2_1_4(kin_path), dl5(t), dl18(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl20(t), dl25(t), dl1(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl17(t), dl2(t), dl19(t), dl26(t) / kin_path.SqrtDelta, f_2_2_3(kin_path), f_1_3_1(kin_path), f_1_3_3(kin_path), f_1_3_4(kin_path), dl30(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta}
;

        auto result = f_3_108_abbreviated(abbr);
        result = result + spdl21(t, abbr);

        return result;
    };
}

template <typename T> T f_3_108_abbreviated(const std::array<T,37>& abb)
{
using TR = typename T::value_type;
T z[90];
z[0] = abb[1];
z[1] = abb[18];
z[2] = abb[22];
z[3] = abb[27];
z[4] = abb[28];
z[5] = abb[29];
z[6] = abb[2];
z[7] = abb[16];
z[8] = abb[19];
z[9] = abb[3];
z[10] = abb[4];
z[11] = abb[7];
z[12] = abb[13];
z[13] = abb[8];
z[14] = abb[9];
z[15] = bc<TR>[0];
z[16] = abb[6];
z[17] = abb[23];
z[18] = abb[24];
z[19] = abb[5];
z[20] = abb[10];
z[21] = abb[11];
z[22] = abb[12];
z[23] = abb[14];
z[24] = abb[15];
z[25] = abb[17];
z[26] = abb[20];
z[27] = abb[21];
z[28] = abb[25];
z[29] = abb[26];
z[30] = abb[30];
z[31] = abb[31];
z[32] = abb[32];
z[33] = abb[33];
z[34] = abb[34];
z[35] = abb[35];
z[36] = abb[36];
z[37] = T(2) * z[16];
z[38] = T(3) * z[18];
z[39] = T(2) * z[5];
z[40] = z[37] + -z[38] + z[39];
z[41] = T(2) * z[3];
z[42] = T(2) * z[12];
z[43] = z[41] + -z[42];
z[44] = T(2) * z[2];
z[45] = z[1] + z[8];
z[46] = z[4] + z[40] + -z[43] + z[44] + z[45];
z[46] = z[10] * z[46];
z[47] = T(2) * z[7];
z[48] = z[4] + -z[8];
z[49] = -z[5] + z[48];
z[50] = -z[3] + z[42];
z[37] = z[2] + -z[18] + z[37] + -z[47] + z[49] + z[50];
z[37] = z[13] * z[37];
z[51] = -z[1] + z[3];
z[52] = z[48] + z[51];
z[40] = -z[40] + z[47] + -z[52];
z[40] = z[14] * z[40];
z[53] = T(4) * z[12];
z[54] = z[1] + z[2];
z[55] = z[53] + -z[54];
z[56] = T(3) * z[4];
z[57] = T(3) * z[7];
z[58] = z[56] + z[57];
z[59] = -z[5] + -z[55] + z[58];
z[59] = z[11] * z[59];
z[60] = z[39] + z[58];
z[55] = T(-5) * z[3] + -z[18] + z[55] + z[60];
z[61] = -(z[24] * z[55]);
z[62] = z[1] + -z[4];
z[43] = -z[2] + z[5] + -z[43] + z[57] + T(-3) * z[62];
z[43] = z[0] * z[43];
z[63] = z[30] + z[35];
z[64] = T(4) * z[36] + T(-2) * z[63];
z[65] = z[33] + z[34];
z[64] = z[64] * z[65];
z[65] = (T(3) * z[17]) / T(2);
z[66] = z[18] + z[44] + z[65];
z[67] = T(2) * z[48];
z[68] = z[3] + (T(13) * z[5]) / T(2) + -z[66] + z[67];
z[69] = z[27] * z[68];
z[70] = z[2] + z[51];
z[71] = T(4) * z[5];
z[72] = T(3) * z[17] + z[70] + -z[71];
z[72] = z[9] * z[72];
z[73] = z[7] + -z[16];
z[74] = z[54] + -z[73];
z[75] = -z[5] + -z[18] + z[74];
z[76] = z[22] * z[75];
z[77] = z[5] + -z[18];
z[78] = T(3) * z[77];
z[79] = z[29] * z[78];
z[80] = T(2) * z[36] + -z[63];
z[81] = z[32] * z[80];
z[37] = z[37] + z[40] + z[43] + z[46] + z[59] + z[61] + z[64] + z[69] + z[72] + T(-3) * z[76] + z[79] + T(-2) * z[81];
z[40] = int_to_imaginary<T>(1) * z[15];
z[37] = z[37] * z[40];
z[43] = z[7] + z[16];
z[46] = T(2) * z[18];
z[56] = z[3] + (T(15) * z[5]) / T(2) + z[43] + -z[45] + -z[46] + z[56] + -z[65];
z[56] = z[9] * z[56];
z[59] = -z[16] + z[42];
z[61] = T(3) * z[1];
z[58] = (T(-21) * z[5]) / T(2) + z[8] + -z[58] + -z[59] + z[61] + z[66];
z[40] = z[40] * z[58];
z[58] = T(2) * z[1];
z[64] = -z[44] + z[58];
z[49] = -z[49] + z[50] + z[64];
z[49] = z[13] * z[49];
z[65] = -z[48] + z[73];
z[66] = z[1] + z[65];
z[69] = -z[39] + z[46] + z[66];
z[69] = z[14] * z[69];
z[72] = T(3) * z[5];
z[76] = z[18] + z[50] + z[72] + T(-2) * z[74];
z[76] = z[11] * z[76];
z[79] = z[18] + z[73];
z[81] = z[2] + z[4];
z[82] = -z[3] + -z[5] + T(2) * z[8] + z[58] + -z[79] + -z[81];
z[82] = z[10] * z[82];
z[83] = T(2) * z[4];
z[47] = z[47] + z[83];
z[71] = -z[2] + z[3] + z[45] + -z[47] + -z[71];
z[71] = z[0] * z[71];
z[84] = z[8] / T(2);
z[85] = z[2] / T(2);
z[86] = z[84] + -z[85];
z[87] = z[16] / T(2);
z[88] = z[1] / T(2);
z[89] = -z[4] + (T(-13) * z[5]) / T(4) + (T(9) * z[17]) / T(4) + -z[86] + z[87] + -z[88];
z[89] = z[6] * z[89];
z[40] = z[40] + -z[49] + z[56] + z[69] + z[71] + z[76] + z[82] + z[89];
z[40] = z[6] * z[40];
z[42] = T(3) * z[3] + -z[42] + z[44] + z[58] + -z[60];
z[42] = z[11] * z[42];
z[44] = -z[8] + z[47] + -z[64] + z[72];
z[44] = z[10] * z[44];
z[47] = z[5] + z[52];
z[47] = z[14] * z[47];
z[52] = -z[3] + z[81];
z[56] = -z[5] + z[52] + -z[88];
z[56] = z[0] * z[56];
z[58] = z[5] + -z[70];
z[58] = z[9] * z[58];
z[42] = z[42] + z[44] + z[47] + z[49] + z[56] + z[58];
z[42] = z[0] * z[42];
z[44] = z[7] / T(2);
z[47] = z[3] / T(2) + -z[5] + z[44] + -z[59] + -z[61] + z[83] + -z[86];
z[47] = z[13] * z[47];
z[49] = -z[46] + z[50] + z[74];
z[50] = z[10] + -z[11];
z[50] = z[49] * z[50];
z[41] = -z[41] + z[66] + -z[77];
z[41] = z[14] * z[41];
z[41] = z[41] + z[47] + z[50];
z[41] = z[13] * z[41];
z[44] = -z[44] + z[81] + -z[84] + z[88];
z[44] = z[10] * z[44];
z[47] = -(z[11] * z[49]);
z[44] = z[44] + z[47];
z[44] = z[10] * z[44];
z[47] = z[18] / T(2);
z[49] = -z[3] + (T(-5) * z[5]) / T(4) + (T(-3) * z[17]) / T(4) + z[47] + -z[48] + z[85] + z[88];
z[49] = z[9] * z[49];
z[43] = -z[8] + z[18] + -z[39] + -z[43] + -z[52];
z[43] = z[10] * z[43];
z[50] = z[66] + z[77];
z[52] = z[13] + -z[14];
z[50] = z[50] * z[52];
z[43] = z[43] + z[49] + z[50];
z[43] = z[9] * z[43];
z[45] = (T(37) * z[4]) / T(8) + -z[45];
z[45] = (T(-11) * z[2]) / T(12) + (T(37) * z[3]) / T(24) + z[5] / T(4) + (T(13) * z[7]) / T(8) + (T(-10) * z[12]) / T(3) + z[17] + (T(-8) * z[36]) / T(3) + z[45] / T(3) + z[47] + (T(4) * z[63]) / T(3) + -z[87];
z[45] = prod_pow(z[15], 2) * z[45];
z[47] = T(3) * z[16] + z[54] + -z[57];
z[46] = (T(-5) * z[3]) / T(2) + -z[46] + z[47] / T(2) + z[53];
z[46] = z[11] * z[46];
z[47] = z[3] + -z[79];
z[47] = z[14] * z[47];
z[46] = z[46] + z[47];
z[46] = z[11] * z[46];
z[47] = z[23] * z[55];
z[49] = z[21] * z[75];
z[50] = -z[3] + z[7] + -z[62];
z[50] = z[25] * z[50];
z[49] = z[49] + z[50];
z[38] = -z[1] + -z[38] + z[48];
z[38] = (T(7) * z[3]) / T(2) + z[38] / T(2) + z[39];
z[38] = prod_pow(z[14], 2) * z[38];
z[39] = z[2] + (T(3) * z[5]) / T(2) + z[67] + -z[73];
z[39] = z[19] * z[39];
z[48] = z[51] + -z[65];
z[48] = z[20] * z[48];
z[50] = -(z[26] * z[68]);
z[51] = z[28] * z[78];
z[52] = z[31] * z[80];
return z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + z[43] + z[44] + z[45] + z[46] + z[47] + T(2) * z[48] + T(3) * z[49] + z[50] + z[51] + z[52];
}



template IntegrandConstructorType<double> f_3_108_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_108_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_108_construct (const Kin<qd_real>&);
#endif

}