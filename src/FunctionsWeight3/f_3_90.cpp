#include "f_3_90.h"

namespace PentagonFunctions {

template <typename T> T f_3_90_abbreviated (const std::array<T,33>&);

template <typename T> class SpDLog_f_3_90_W_7 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_90_W_7 (const Kin<T>& kin) {
        c[0] = (T(8) * kin.v[0] * kin.v[4]) / T(3) + T(4) * kin.v[1] * kin.v[4] + T(-4) * kin.v[2] * kin.v[4] + T(-2) * prod_pow(kin.v[4], 2) + bc<T>[1] * (T(-4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-4) + T(2) * kin.v[4])) + kin.v[3] * ((T(8) * kin.v[0]) / T(3) + T(4) * kin.v[1] + T(-4) * kin.v[2] + (T(-2) + bc<T>[1] * T(2)) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[1] * (T(-4) + T(-4) * kin.v[1] + T(4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[3] * (T(-4) + T(-4) * kin.v[1] + T(2) * kin.v[3] + T(4) * kin.v[4]));
c[1] = (T(-8) + bc<T>[1] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + T(-8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[4] + bc<T>[1] * T(-4) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[15] * (abb[5] * (abb[5] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4)) + abb[4] * abb[5] * T(4) + abb[5] * abb[14] * T(4) + abb[1] * (abb[5] * T(-4) + abb[2] * T(4)) + abb[8] * T(8) + abb[2] * (abb[4] * T(-4) + abb[5] * T(-4) + abb[14] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4) + abb[2] * T(8)));
    }
};
template <typename T> class SpDLog_f_3_90_W_21 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_90_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (T(-3) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(4) * kin.v[3] + T(2) * kin.v[4]) + kin.v[0] * ((T(-4) * kin.v[1]) / T(3) + (T(-2) * kin.v[2]) / T(3) + (T(10) * kin.v[3]) / T(3) + (T(-1) / T(3) + -bc<T>[1]) * kin.v[0] + T(2) * kin.v[4] + bc<T>[1] * (T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4])) + bc<T>[1] * (kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (-kin.v[0] + T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(2) + bc<T>[1] * T(2) + T(4)) * kin.v[0] + T(4) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[3]) + bc<T>[1] * (T(2) * kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[3]) + T(-4) * kin.v[3];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[18] * (abb[19] * T(-4) + abb[3] * abb[6] * T(-2) + abb[6] * abb[14] * T(-2) + abb[1] * (abb[5] * T(-2) + abb[6] * T(2)) + abb[6] * (bc<T>[0] * int_to_imaginary<T>(2) + abb[6] * T(2)) + abb[5] * (abb[5] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[3] * T(2) + abb[6] * T(2) + abb[14] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_90_construct (const Kin<T>& kin) {
    return [&kin, 
            dl5 = DLog_W_5<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl13 = DLog_W_13<T>(kin),dl7 = DLog_W_7<T>(kin),dl4 = DLog_W_4<T>(kin),dl21 = DLog_W_21<T>(kin),dl3 = DLog_W_3<T>(kin),dl1 = DLog_W_1<T>(kin),dl16 = DLog_W_16<T>(kin),dl2 = DLog_W_2<T>(kin),dl19 = DLog_W_19<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),spdl7 = SpDLog_f_3_90_W_7<T>(kin),spdl21 = SpDLog_f_3_90_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,33> abbr = 
            {dl5(t), rlog(v_path[0]), rlog(-v_path[1]), rlog(v_path[2]), rlog(v_path[3]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_1(kin_path), f_2_1_11(kin_path), dl27(t) / kin_path.SqrtDelta, f_2_2_2(kin_path), dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl13(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl7(t), dl4(t), f_2_1_2(kin_path), dl21(t), f_2_1_15(kin_path), dl3(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl1(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl16(t), dl2(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl19(t), dl18(t), dl20(t)}
;

        auto result = f_3_90_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_3_90_abbreviated(const std::array<T,33>& abb)
{
using TR = typename T::value_type;
T z[74];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[6];
z[6] = abb[5];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[13];
z[10] = abb[16];
z[11] = abb[20];
z[12] = abb[23];
z[13] = abb[26];
z[14] = abb[27];
z[15] = abb[30];
z[16] = abb[31];
z[17] = abb[32];
z[18] = abb[14];
z[19] = bc<TR>[0];
z[20] = abb[9];
z[21] = abb[10];
z[22] = abb[11];
z[23] = abb[12];
z[24] = abb[17];
z[25] = abb[19];
z[26] = abb[21];
z[27] = abb[22];
z[28] = abb[24];
z[29] = abb[25];
z[30] = abb[28];
z[31] = abb[29];
z[32] = T(3) * z[14];
z[33] = z[10] + -z[16];
z[34] = z[12] + z[33];
z[35] = T(4) * z[17];
z[36] = T(2) * z[15];
z[37] = z[32] + z[34] + z[35] + z[36];
z[38] = T(3) * z[13];
z[39] = -z[11] + z[38];
z[40] = z[37] + -z[39];
z[40] = z[6] * z[40];
z[41] = T(2) * z[16];
z[42] = T(2) * z[12];
z[43] = z[41] + z[42];
z[44] = -z[15] + z[17];
z[45] = T(3) * z[10];
z[46] = z[9] + z[45];
z[47] = T(2) * z[11];
z[48] = z[43] + -z[44] + -z[46] + z[47];
z[48] = z[3] * z[48];
z[49] = T(3) * z[17];
z[50] = z[42] + z[49];
z[51] = z[36] + z[47];
z[52] = T(3) * z[16];
z[53] = z[50] + -z[51] + z[52];
z[54] = z[14] + z[53];
z[54] = z[5] * z[54];
z[55] = T(2) * z[17];
z[43] = z[14] + z[15] + -z[43] + -z[55];
z[39] = -z[39] + z[43] + z[46];
z[39] = z[1] * z[39];
z[46] = z[14] + -z[53];
z[46] = z[18] * z[46];
z[53] = z[12] + z[14];
z[56] = z[15] + z[33];
z[57] = -z[35] + z[53] + -z[56];
z[58] = z[2] * z[57];
z[59] = -z[12] + z[13];
z[60] = z[29] * z[59];
z[61] = z[16] + z[17];
z[62] = -z[15] + z[61];
z[63] = -z[14] + z[62];
z[64] = z[31] * z[63];
z[60] = z[60] + z[64];
z[64] = -z[11] + z[13];
z[44] = z[44] + z[64];
z[44] = T(3) * z[44];
z[65] = z[27] * z[44];
z[66] = T(3) * z[11];
z[67] = -z[15] + z[66];
z[68] = T(3) * z[12] + -z[14] + -z[67];
z[68] = z[4] * z[68];
z[39] = z[39] + z[40] + z[46] + z[48] + -z[54] + z[58] + T(3) * z[60] + z[65] + z[68];
z[39] = int_to_imaginary<T>(1) * z[19] * z[39];
z[40] = T(2) * z[14];
z[46] = z[0] + z[40] + z[49];
z[34] = z[15] + z[34] + -z[38] + z[46];
z[48] = z[4] + z[6];
z[48] = z[34] * z[48];
z[46] = z[42] + -z[46] + -z[56];
z[46] = z[1] * z[46];
z[49] = z[12] + -z[16];
z[56] = T(5) * z[17];
z[58] = z[0] + z[38];
z[49] = T(-7) * z[14] + T(-4) * z[49] + -z[56] + z[58];
z[49] = z[5] * z[49];
z[57] = -(z[18] * z[57]);
z[60] = (T(3) * z[13]) / T(2);
z[65] = z[10] / T(2) + -z[60];
z[69] = z[15] / T(2);
z[70] = (T(3) * z[16]) / T(2) + z[69];
z[56] = -z[0] + -z[14] / T(2) + -z[56] + -z[65] + -z[70];
z[56] = z[2] * z[56];
z[46] = z[46] + z[48] + z[49] + z[56] + z[57];
z[46] = z[2] * z[46];
z[48] = -z[45] + z[58];
z[49] = z[11] + z[17];
z[36] = -z[14] + -z[36] + z[48] + -z[49] + z[52];
z[36] = z[1] * z[36];
z[56] = -z[0] + z[14];
z[35] = z[35] + z[38] + -z[51] + z[56];
z[57] = z[5] * z[35];
z[71] = T(4) * z[9];
z[48] = -z[16] + -z[48] + z[50] + -z[71];
z[50] = z[14] + z[48] + z[66];
z[50] = z[4] * z[50];
z[66] = z[11] / T(2);
z[60] = (T(3) * z[10]) / T(2) + -z[60] + z[66];
z[62] = z[12] + z[62];
z[72] = z[9] / T(2);
z[73] = z[60] + -z[62] + z[72];
z[73] = z[3] * z[73];
z[49] = z[15] + z[49];
z[56] = -z[49] + -z[56];
z[56] = z[6] * z[56];
z[36] = z[36] + z[50] + z[56] + z[57] + z[73];
z[36] = z[3] * z[36];
z[37] = z[37] + -z[47];
z[50] = -(z[6] * z[37]);
z[43] = -z[43] + z[47] + -z[71];
z[43] = z[1] * z[43];
z[47] = -z[11] + z[53] + z[69];
z[47] = z[18] * z[47];
z[53] = z[14] + -z[15];
z[53] = z[4] * z[53];
z[56] = T(2) * z[9] + -z[11] + -z[62];
z[56] = z[3] * z[56];
z[43] = z[43] + z[47] + z[50] + z[53] + z[54] + T(2) * z[56];
z[43] = z[18] * z[43];
z[41] = -z[0] + z[12] + (T(3) * z[14]) / T(2) + -z[41] + z[55] + z[60] + z[69] + -z[72];
z[41] = prod_pow(z[1], 2) * z[41];
z[35] = -(z[1] * z[35]);
z[47] = T(7) * z[16] + -z[42];
z[50] = T(6) * z[14] + -z[47] + z[51] + z[55] + -z[58];
z[50] = z[6] * z[50];
z[51] = -z[12] / T(2) + T(-4) * z[14] + (T(-3) * z[17]) / T(2) + z[66] + z[70];
z[51] = z[5] * z[51];
z[35] = z[35] + z[50] + z[51];
z[35] = z[5] * z[35];
z[37] = z[1] * z[37];
z[50] = -z[11] + (T(3) * z[15]) / T(2) + (T(5) * z[16]) / T(2) + z[40] + z[55] + -z[65];
z[50] = z[6] * z[50];
z[37] = z[37] + z[50];
z[37] = z[6] * z[37];
z[40] = -z[40] + -z[48] + -z[67];
z[40] = z[1] * z[40];
z[34] = -(z[6] * z[34]);
z[34] = z[34] + z[40] + -z[68] / T(2);
z[34] = z[4] * z[34];
z[33] = -z[12] + z[14] + z[33] + -z[64];
z[33] = z[24] * z[33];
z[40] = z[28] * z[59];
z[48] = -(z[30] * z[63]);
z[33] = z[33] + z[40] + z[48];
z[40] = T(-2) * z[0] + (T(3) * z[9]) / T(2) + (T(-5) * z[12]) / T(2) + z[45] + -z[52] + z[55];
z[40] = z[7] * z[40];
z[38] = T(-5) * z[14] + z[38] + z[47] + -z[49];
z[38] = z[25] * z[38];
z[32] = -z[0] + -z[10] + -z[15] + z[32] + z[42] + -z[61];
z[32] = z[8] * z[32];
z[42] = -(z[26] * z[44]);
z[44] = (T(5) * z[11]) / T(3) + (T(9) * z[14]) / T(2);
z[44] = -z[10] + z[12] / T(6) + z[15] / T(3) + (T(-31) * z[16]) / T(12) + (T(-5) * z[17]) / T(12) + z[44] / T(2);
z[44] = prod_pow(z[19], 2) * z[44];
z[45] = z[22] + z[23];
z[45] = T(-2) * z[20] + T(3) * z[45];
z[45] = z[21] * z[45];
return T(2) * z[32] + T(3) * z[33] + z[34] + z[35] + z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + z[43] + z[44] / T(2) + z[45] + z[46];
}



template IntegrandConstructorType<double> f_3_90_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_90_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_90_construct (const Kin<qd_real>&);
#endif

}