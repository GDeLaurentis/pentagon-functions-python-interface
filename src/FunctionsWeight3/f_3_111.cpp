#include "f_3_111.h"

namespace PentagonFunctions {

template <typename T> T f_3_111_abbreviated (const std::array<T,37>&);

template <typename T> class SpDLog_f_3_111_W_21 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_111_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (kin.v[1] + T(-4) * kin.v[2]) + kin.v[0] * (kin.v[0] + T(2) * kin.v[1] + T(-4) * kin.v[2]) + -prod_pow(kin.v[3], 2) + kin.v[2] * (T(3) * kin.v[2] + T(2) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (-kin.v[0] + T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[0] + T(4) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[3]) + T(-4) * kin.v[3];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[6] * T(-4) + abb[3] * abb[5] * T(-2) + abb[4] * abb[5] * T(2) + abb[1] * (abb[5] * T(-2) + abb[2] * T(2)) + abb[5] * (bc<T>[0] * int_to_imaginary<T>(2) + abb[5] * T(2)) + abb[2] * (abb[2] * T(-4) + abb[4] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[3] * T(2) + abb[5] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_111_construct (const Kin<T>& kin) {
    return [&kin, 
            dl21 = DLog_W_21<T>(kin),dl3 = DLog_W_3<T>(kin),dl15 = DLog_W_15<T>(kin),dl16 = DLog_W_16<T>(kin),dl25 = DLog_W_25<T>(kin),dl18 = DLog_W_18<T>(kin),dl5 = DLog_W_5<T>(kin),dl19 = DLog_W_19<T>(kin),dl2 = DLog_W_2<T>(kin),dl20 = DLog_W_20<T>(kin),dl1 = DLog_W_1<T>(kin),dl17 = DLog_W_17<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),spdl21 = SpDLog_f_3_111_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,37> abbr = 
            {dl21(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(v_path[2]), rlog(-v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_15(kin_path), dl3(t), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_7(kin_path), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl15(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl16(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl25(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl18(t), f_2_1_11(kin_path), dl5(t), dl19(t), dl2(t), dl20(t), dl1(t), dl17(t), dl27(t) / kin_path.SqrtDelta, f_2_2_3(kin_path), f_1_3_1(kin_path), f_1_3_3(kin_path), f_1_3_4(kin_path), dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta}
;

        auto result = f_3_111_abbreviated(abbr);
        result = result + spdl21(t, abbr);

        return result;
    };
}

template <typename T> T f_3_111_abbreviated(const std::array<T,37>& abb)
{
using TR = typename T::value_type;
T z[81];
z[0] = abb[1];
z[1] = abb[7];
z[2] = abb[22];
z[3] = abb[24];
z[4] = abb[25];
z[5] = abb[2];
z[6] = abb[19];
z[7] = abb[26];
z[8] = abb[27];
z[9] = abb[29];
z[10] = abb[3];
z[11] = abb[12];
z[12] = abb[4];
z[13] = abb[5];
z[14] = bc<TR>[0];
z[15] = abb[8];
z[16] = abb[16];
z[17] = abb[15];
z[18] = abb[28];
z[19] = abb[6];
z[20] = abb[9];
z[21] = abb[10];
z[22] = abb[11];
z[23] = abb[13];
z[24] = abb[14];
z[25] = abb[17];
z[26] = abb[18];
z[27] = abb[20];
z[28] = abb[21];
z[29] = abb[23];
z[30] = abb[30];
z[31] = abb[31];
z[32] = abb[32];
z[33] = abb[33];
z[34] = abb[34];
z[35] = abb[35];
z[36] = abb[36];
z[37] = T(3) * z[1];
z[38] = T(4) * z[11];
z[39] = z[37] + -z[38];
z[40] = z[8] + -z[17];
z[41] = T(4) * z[6];
z[42] = -z[4] + z[41];
z[43] = T(2) * z[7];
z[44] = T(3) * z[3];
z[45] = T(3) * z[2];
z[40] = T(-4) * z[9] + -z[39] + T(-6) * z[40] + z[42] + z[43] + -z[44] + -z[45];
z[40] = z[15] * z[40];
z[46] = T(5) * z[8];
z[47] = z[45] + z[46];
z[48] = z[7] + -z[9];
z[49] = z[41] + z[48];
z[50] = T(5) * z[3];
z[51] = z[1] + -z[18];
z[52] = T(3) * z[11];
z[53] = z[4] + -z[17];
z[53] = z[47] + -z[49] + z[50] + z[51] + -z[52] + T(3) * z[53];
z[53] = z[12] * z[53];
z[54] = T(2) * z[8];
z[41] = -z[41] + z[54];
z[55] = T(2) * z[1];
z[38] = z[38] + -z[55];
z[56] = T(3) * z[7];
z[57] = z[2] + z[56];
z[58] = z[3] + -z[4];
z[59] = -z[9] + -z[38] + -z[41] + z[57] + T(3) * z[58];
z[59] = z[0] * z[59];
z[60] = T(6) * z[2] + z[55];
z[61] = T(4) * z[7];
z[62] = T(7) * z[3];
z[63] = T(3) * z[17];
z[64] = z[4] + -z[63];
z[64] = z[8] + T(-3) * z[18] + z[60] + -z[61] + z[62] + T(2) * z[64];
z[64] = z[13] * z[64];
z[65] = -z[3] + z[49];
z[66] = -z[2] + z[63];
z[67] = T(2) * z[18];
z[68] = z[1] + -z[54] + z[65] + z[66] + -z[67];
z[68] = z[5] * z[68];
z[69] = T(7) * z[2];
z[70] = T(8) * z[6];
z[71] = T(8) * z[8] + z[48] + z[58] + z[69] + -z[70];
z[71] = z[16] * z[71];
z[38] = z[38] + -z[43];
z[43] = T(3) * z[9];
z[50] = z[4] + -z[8] + -z[38] + -z[43] + z[50];
z[50] = z[10] * z[50];
z[52] = -z[52] + z[62];
z[56] = -z[8] + z[18] + z[43] + -z[52] + -z[56];
z[62] = z[24] * z[56];
z[72] = T(2) * z[3];
z[42] = z[7] + z[8] + z[9] + z[18] + -z[42] + -z[45] + -z[72];
z[42] = T(2) * z[42];
z[73] = z[28] * z[42];
z[74] = T(2) * z[30] + T(-6) * z[35] + T(-6) * z[36];
z[75] = z[32] + -z[33] + -z[34];
z[74] = z[74] * z[75];
z[51] = z[4] + z[51];
z[75] = z[3] + -z[17];
z[76] = z[51] + z[75];
z[77] = z[8] + -z[76];
z[77] = z[22] * z[77];
z[78] = z[26] * z[75];
z[77] = z[77] + z[78];
z[40] = z[40] + z[50] + z[53] + z[59] + z[62] + z[64] + z[68] + z[71] + z[73] + z[74] + T(3) * z[77];
z[40] = int_to_imaginary<T>(1) * z[40];
z[50] = z[35] + z[36];
z[53] = (T(5) * z[1]) / T(12) + (T(25) * z[2]) / T(8) + (T(-19) * z[3]) / T(12) + (T(-2) * z[4]) / T(3) + (T(-20) * z[6]) / T(3) + (T(3) * z[7]) / T(8) + (T(47) * z[8]) / T(8) + (T(-5) * z[9]) / T(6) + -z[11] + z[17] / T(2) + (T(5) * z[18]) / T(2) + (T(4) * z[30]) / T(3) + T(-4) * z[50];
z[53] = z[14] * z[53];
z[40] = z[40] + z[53];
z[40] = z[14] * z[40];
z[53] = T(2) * z[4];
z[59] = z[53] + -z[63];
z[62] = z[59] + -z[67];
z[64] = z[1] + z[2];
z[52] = -z[52] + -z[54] + -z[62] + -z[64];
z[52] = z[10] * z[52];
z[68] = T(4) * z[8];
z[71] = -z[55] + z[68];
z[45] = z[45] + z[53] + -z[65] + z[71];
z[53] = z[5] * z[45];
z[73] = -z[63] + z[72];
z[39] = -z[7] + z[39] + z[43] + z[73];
z[43] = T(2) * z[2];
z[74] = T(3) * z[8];
z[77] = z[43] + z[74];
z[78] = z[4] + z[18];
z[79] = -z[39] + -z[77] + z[78];
z[79] = z[15] * z[79];
z[54] = z[54] + z[72];
z[38] = -z[2] + -z[4] + -z[38] + z[54];
z[38] = z[0] * z[38];
z[62] = -z[2] + z[44] + z[62] + -z[71];
z[71] = z[13] * z[62];
z[77] = z[77] + z[78];
z[72] = -z[49] + -z[72] + z[77];
z[78] = -(z[16] * z[72]);
z[80] = -z[4] + z[7];
z[47] = z[1] + T(9) * z[3] + T(-7) * z[11] + z[47] + -z[63] + -z[80];
z[47] = z[12] * z[47];
z[38] = z[38] + z[47] / T(2) + z[52] + z[53] + z[71] + z[78] + z[79];
z[38] = z[12] * z[38];
z[47] = T(-4) * z[3] + z[18] + -z[46] + z[61] + z[63] + -z[69];
z[47] = z[13] * z[47];
z[44] = -z[2] + -z[4] + -z[9] + z[44];
z[52] = (T(3) * z[7]) / T(2);
z[44] = -z[18] + z[44] / T(2) + -z[46] + -z[52] + z[70];
z[44] = z[16] * z[44];
z[46] = z[15] * z[72];
z[49] = -z[2] + -z[49] + -z[58] + z[68];
z[49] = z[0] * z[49];
z[53] = -z[63] + -z[65] + z[77];
z[61] = z[5] * z[53];
z[44] = z[44] + z[46] + z[47] + z[49] + z[61];
z[44] = z[16] * z[44];
z[45] = -(z[0] * z[45]);
z[46] = T(7) * z[7];
z[47] = -z[18] + -z[46] + z[54] + z[59] + z[60];
z[47] = z[13] * z[47];
z[49] = -(z[15] * z[53]);
z[53] = z[4] + z[17];
z[53] = -z[9] + T(3) * z[53];
z[41] = -z[1] + (T(5) * z[7]) / T(2) + z[41] + z[43] + z[53] / T(2);
z[41] = z[5] * z[41];
z[43] = -z[2] + -z[8] + -z[51];
z[43] = z[10] * z[43];
z[41] = z[41] + z[43] + z[45] + z[47] + z[49];
z[41] = z[5] * z[41];
z[43] = z[21] * z[76];
z[45] = -z[3] + -z[17] + -z[48] + z[64];
z[45] = z[20] * z[45];
z[47] = z[25] * z[75];
z[48] = z[2] + -z[80];
z[48] = z[29] * z[48];
z[49] = -z[21] + -z[29];
z[49] = z[8] * z[49];
z[43] = z[43] + z[45] + z[47] + z[48] + z[49];
z[45] = -z[55] + z[57] + T(2) * z[58] + z[74];
z[45] = z[0] * z[45];
z[47] = -(z[10] * z[62]);
z[48] = z[1] / T(2);
z[49] = T(-4) * z[2] + (T(-9) * z[8]) / T(2) + z[48] + z[52] + -z[58] / T(2);
z[49] = z[13] * z[49];
z[45] = z[45] + z[47] + z[49];
z[45] = z[13] * z[45];
z[39] = z[2] + -z[18] + z[39] + z[74];
z[39] = z[15] * z[39];
z[47] = z[7] + -z[8] + z[11];
z[47] = (T(5) * z[3]) / T(2) + z[4] + (T(-3) * z[17]) / T(2) + z[47] / T(2) + z[48] + -z[67];
z[47] = z[10] * z[47];
z[48] = z[1] + z[8];
z[49] = -z[3] + T(2) * z[11] + -z[48] + -z[80];
z[49] = z[0] * z[49];
z[39] = z[39] + z[47] + T(2) * z[49];
z[39] = z[10] * z[39];
z[46] = T(-5) * z[2] + -z[4] + z[46] + -z[48] + -z[73];
z[46] = z[19] * z[46];
z[42] = -(z[27] * z[42]);
z[47] = -(z[23] * z[56]);
z[37] = -z[4] + z[37] + -z[66];
z[37] = prod_pow(z[15], 2) * z[37];
z[48] = z[2] + -z[4];
z[48] = z[15] * z[48];
z[49] = -z[1] + z[2] + z[3] + z[4] / T(2);
z[49] = z[0] * z[49];
z[48] = z[48] + z[49];
z[48] = z[0] * z[48];
z[49] = -z[30] + T(3) * z[50];
z[49] = z[31] * z[49];
return z[37] / T(2) + z[38] + z[39] + z[40] + z[41] + z[42] + T(3) * z[43] + z[44] + z[45] + z[46] + z[47] + z[48] + z[49];
}



template IntegrandConstructorType<double> f_3_111_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_111_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_111_construct (const Kin<qd_real>&);
#endif

}