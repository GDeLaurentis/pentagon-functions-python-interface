#include "f_3_91.h"

namespace PentagonFunctions {

template <typename T> T f_3_91_abbreviated (const std::array<T,32>&);

template <typename T> class SpDLog_f_3_91_W_21 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_91_W_21 (const Kin<T>& kin) {
        c[0] = (T(23) / T(2) + (T(-23) * kin.v[3]) / T(8) + (T(-23) * kin.v[4]) / T(2)) * kin.v[3] + (T(23) / T(2) + (T(69) * kin.v[2]) / T(8) + (T(23) * kin.v[3]) / T(4) + (T(-23) * kin.v[4]) / T(2)) * kin.v[2] + (T(-23) / T(2) + (T(-23) * kin.v[1]) / T(8) + (T(-23) * kin.v[2]) / T(4) + (T(23) * kin.v[3]) / T(4) + (T(23) * kin.v[4]) / T(2)) * kin.v[1] + kin.v[0] * (T(-23) / T(2) + (T(23) * kin.v[1]) / T(4) + (T(-69) * kin.v[2]) / T(4) + (T(-23) * kin.v[3]) / T(4) + (T(23) * kin.v[4]) / T(2) + (T(69) / T(8) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(-4) * kin.v[2] + T(4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[3] * (T(4) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(-4) + T(-2) * kin.v[1] + T(4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = (T(-9) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + (T(-9) * kin.v[1]) / T(2) + (T(9) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[15] * ((abb[16] * T(9)) / T(2) + abb[9] * ((abb[9] * T(7)) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) + abb[8] * ((abb[8] * T(-7)) / T(2) + abb[7] * T(-4) + bc<T>[0] * int_to_imaginary<T>(4)) + abb[7] * abb[9] * T(4) + abb[12] * (abb[9] * T(-4) + abb[8] * T(4)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_91_construct (const Kin<T>& kin) {
    return [&kin, 
            dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl30 = DLog_W_30<T>(kin),dl13 = DLog_W_13<T>(kin),dl11 = DLog_W_11<T>(kin),dl21 = DLog_W_21<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),dl5 = DLog_W_5<T>(kin),dl3 = DLog_W_3<T>(kin),dl19 = DLog_W_19<T>(kin),dl4 = DLog_W_4<T>(kin),dl16 = DLog_W_16<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),spdl21 = SpDLog_f_3_91_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,32> abbr = 
            {dl27(t) / kin_path.SqrtDelta, f_2_2_2(kin_path), dl28(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl13(t), rlog(v_path[0]), rlog(v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_1(kin_path), dl11(t), rlog(-v_path[1]), rlog(v_path[3]), f_2_1_2(kin_path), dl21(t), f_2_1_15(kin_path), dl18(t), f_2_1_11(kin_path), dl20(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl5(t), dl3(t), f_2_1_9(kin_path), dl19(t), dl4(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl16(t), dl2(t), dl1(t)}
;

        auto result = f_3_91_abbreviated(abbr);
        result = result + spdl21(t, abbr);

        return result;
    };
}

template <typename T> T f_3_91_abbreviated(const std::array<T,32>& abb)
{
using TR = typename T::value_type;
T z[70];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[9];
z[10] = bc<TR>[0];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[22];
z[14] = abb[23];
z[15] = abb[26];
z[16] = abb[29];
z[17] = abb[17];
z[18] = abb[19];
z[19] = abb[25];
z[20] = abb[31];
z[21] = abb[12];
z[22] = abb[13];
z[23] = abb[30];
z[24] = abb[14];
z[25] = abb[16];
z[26] = abb[18];
z[27] = abb[20];
z[28] = abb[21];
z[29] = abb[24];
z[30] = abb[27];
z[31] = abb[28];
z[32] = T(2) * z[7];
z[33] = -z[21] + z[32];
z[34] = T(3) * z[6];
z[35] = T(2) * z[9];
z[36] = -z[8] + z[33] + z[34] + z[35];
z[36] = z[18] * z[36];
z[37] = z[5] + -z[22];
z[38] = z[12] * z[37];
z[39] = z[5] + -z[6];
z[40] = z[4] * z[39];
z[40] = -z[38] + z[40];
z[41] = T(3) * z[20];
z[42] = z[6] + -z[22];
z[43] = z[41] * z[42];
z[44] = -z[7] + T(3) * z[8];
z[45] = -z[39] + -z[44];
z[45] = z[14] * z[45];
z[46] = z[35] + z[42];
z[47] = T(2) * z[8];
z[48] = -z[7] + z[46] + -z[47];
z[48] = z[17] * z[48];
z[49] = -z[13] + z[18];
z[50] = -z[14] + z[16] + z[20] + z[49];
z[51] = T(3) * z[28];
z[52] = -(z[50] * z[51]);
z[44] = z[5] + T(5) * z[22] + z[44];
z[44] = z[15] * z[44];
z[53] = T(3) * z[5];
z[54] = -(z[13] * z[53]);
z[55] = T(3) * z[22];
z[56] = z[8] + -z[9] + -z[55];
z[56] = z[23] * z[56];
z[57] = z[5] + z[8];
z[57] = z[16] * z[57];
z[58] = -z[15] + -z[16] + z[20] + z[23];
z[58] = T(3) * z[58];
z[59] = z[31] * z[58];
z[36] = z[36] + T(4) * z[40] + z[43] + z[44] + z[45] + z[48] + z[52] + z[54] + z[56] + T(3) * z[57] + z[59];
z[40] = int_to_imaginary<T>(1) * z[10];
z[36] = z[36] * z[40];
z[43] = -z[9] + z[42];
z[33] = T(-7) * z[8] + -z[33] + T(-3) * z[43] + z[51];
z[33] = z[33] * z[40];
z[40] = T(3) * z[9];
z[44] = T(2) * z[39];
z[45] = -z[40] + z[44];
z[45] = z[9] * z[45];
z[48] = z[21] / T(2);
z[51] = -z[7] + z[48];
z[52] = -z[9] + z[37];
z[54] = z[51] + T(-2) * z[52];
z[54] = z[21] * z[54];
z[56] = T(3) * z[21];
z[57] = -z[8] + z[56];
z[59] = T(2) * z[22];
z[60] = -z[9] + z[59];
z[61] = T(5) * z[6] + T(4) * z[7] + -z[57] + -z[60];
z[61] = z[8] * z[61];
z[62] = T(3) * z[27];
z[63] = prod_pow(z[10], 2);
z[64] = T(2) * z[26];
z[65] = z[11] + -z[24];
z[66] = prod_pow(z[22], 2);
z[40] = z[7] + -z[40];
z[40] = z[7] * z[40];
z[33] = (T(-21) * z[25]) / T(2) + z[33] + z[40] + z[45] + z[54] + z[61] + -z[62] + -z[63] / T(4) + z[64] + T(-2) * z[65] + (T(-3) * z[66]) / T(2);
z[33] = z[19] * z[33];
z[32] = z[9] + z[32] + T(-2) * z[42] + -z[57];
z[32] = z[8] * z[32];
z[40] = z[7] / T(2);
z[45] = z[40] + -z[46];
z[45] = z[7] * z[45];
z[46] = z[9] + z[21];
z[54] = T(2) * z[37] + z[46];
z[54] = z[21] * z[54];
z[57] = prod_pow(z[6], 2);
z[61] = z[66] / T(2);
z[42] = z[5] * z[42];
z[44] = -z[9] + -z[44];
z[44] = z[9] * z[44];
z[32] = T(-2) * z[25] + z[32] + z[42] + z[44] + z[45] + z[54] + -z[57] / T(2) + z[61] + z[64] + z[65];
z[32] = z[17] * z[32];
z[42] = T(2) * z[5];
z[44] = -z[6] + z[42];
z[45] = -z[44] + -z[47] + z[56] + -z[60];
z[45] = z[8] * z[45];
z[47] = T(2) * z[6];
z[54] = (T(5) * z[5]) / T(2);
z[56] = -z[47] + z[54];
z[56] = z[5] * z[56];
z[67] = T(4) * z[5];
z[40] = -z[40] + z[67];
z[34] = z[22] + z[34] + -z[40];
z[34] = z[7] * z[34];
z[68] = z[9] * z[39];
z[69] = -(z[6] * z[22]);
z[60] = z[5] + -z[21] + z[60];
z[60] = z[21] * z[60];
z[34] = z[34] + z[45] + z[56] + z[60] + z[65] + (T(-5) * z[66]) / T(2) + z[68] + z[69];
z[34] = z[15] * z[34];
z[45] = -z[22] + z[47];
z[47] = z[8] + -z[21];
z[56] = -z[9] + z[42] + z[45] + z[47];
z[56] = z[8] * z[56];
z[59] = -z[54] + z[59];
z[59] = z[5] * z[59];
z[40] = -z[6] + z[40] + -z[55];
z[40] = z[7] * z[40];
z[55] = T(5) * z[25];
z[45] = -(z[6] * z[45]);
z[60] = (T(-3) * z[9]) / T(2) + -z[39];
z[60] = z[9] * z[60];
z[69] = -(z[21] * z[52]);
z[40] = z[40] + z[45] + -z[55] + z[56] + z[59] + z[60] + z[65] + z[69];
z[40] = z[14] * z[40];
z[45] = z[9] + z[48] + -z[53];
z[45] = z[21] * z[45];
z[48] = -z[7] + z[8] / T(2);
z[53] = z[21] + -z[35] + z[48];
z[53] = z[8] * z[53];
z[56] = z[24] + z[61];
z[59] = prod_pow(z[9], 2);
z[60] = z[7] * z[9];
z[45] = z[45] + z[53] + T(3) * z[56] + z[59] / T(2) + z[60] + -z[64];
z[45] = z[23] * z[45];
z[53] = -z[6] + -z[22] + z[54];
z[53] = z[5] * z[53];
z[54] = (T(3) * z[6]) / T(2) + -z[22];
z[54] = z[6] * z[54];
z[53] = z[53] + -z[54];
z[54] = z[9] + (T(-3) * z[21]) / T(2) + -z[22] + z[67];
z[54] = z[21] * z[54];
z[46] = T(-4) * z[6] + (T(5) * z[8]) / T(2) + z[22] + -z[46];
z[46] = z[8] * z[46];
z[46] = T(-5) * z[24] + z[46] + -z[53] + z[54] + z[55] + -z[68];
z[46] = z[16] * z[46];
z[54] = z[7] + -z[8];
z[54] = z[39] * z[54];
z[44] = -(z[5] * z[44]);
z[44] = T(-2) * z[11] + z[44] + z[54] + z[57] + z[68];
z[44] = z[4] * z[44];
z[42] = -z[22] + z[42];
z[42] = z[5] * z[42];
z[37] = -(z[7] * z[37]);
z[37] = T(2) * z[24] + z[37] + z[42] + -z[66];
z[37] = z[12] * z[37];
z[38] = z[38] * z[47];
z[37] = z[37] + z[38] + z[44];
z[38] = z[21] + z[52];
z[38] = z[21] * z[38];
z[42] = -z[21] + -z[43];
z[42] = z[8] * z[42];
z[38] = T(5) * z[11] + z[38] + z[42] + z[53] + T(-4) * z[68];
z[38] = z[13] * z[38];
z[42] = -z[9] + T(3) * z[39];
z[42] = z[9] * z[42];
z[35] = z[7] + z[35];
z[35] = -(z[7] * z[35]);
z[43] = z[9] + -z[51];
z[43] = z[21] * z[43];
z[44] = z[9] + -z[48];
z[44] = z[8] * z[44];
z[35] = T(-3) * z[11] + z[35] + z[42] + z[43] + z[44];
z[35] = z[18] * z[35];
z[39] = z[9] / T(2) + z[39];
z[39] = z[9] * z[39];
z[42] = -(z[5] * z[21]);
z[39] = z[39] + z[42] + z[61] + -z[65];
z[39] = z[39] * z[41];
z[41] = -z[15] + -z[49];
z[41] = z[41] * z[64];
z[42] = (T(49) * z[15]) / T(2) + T(13) * z[18];
z[42] = T(-5) * z[23] + z[42] / T(2);
z[42] = (T(41) * z[14]) / T(12) + (T(-3) * z[17]) / T(4) + -z[20] + z[42] / T(3);
z[42] = z[42] * z[63];
z[43] = z[50] * z[62];
z[44] = -z[0] + -z[2];
z[44] = z[3] + T(2) * z[44];
z[44] = z[1] * z[44];
z[47] = -z[18] + T(2) * z[23];
z[47] = z[25] * z[47];
z[48] = -z[14] + z[15];
z[48] = z[29] * z[48];
z[49] = z[30] * z[58];
return z[32] + z[33] + z[34] + z[35] + z[36] + T(2) * z[37] + z[38] + z[39] + z[40] + z[41] + z[42] / T(2) + z[43] + z[44] + z[45] + z[46] + z[47] + T(6) * z[48] + z[49];
}



template IntegrandConstructorType<double> f_3_91_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_91_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_91_construct (const Kin<qd_real>&);
#endif

}