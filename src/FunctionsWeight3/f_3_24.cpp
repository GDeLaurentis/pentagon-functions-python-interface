#include "f_3_24.h"

namespace PentagonFunctions {

template <typename T> T f_3_24_abbreviated (const std::array<T,18>&);

template <typename T> class SpDLog_f_3_24_W_7 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_3_24_W_7 (const Kin<T>& kin) {
        c[0] = (T(-4) * kin.v[0] * kin.v[4]) / T(3) + T(-2) * kin.v[1] * kin.v[4] + T(2) * kin.v[2] * kin.v[4] + prod_pow(kin.v[4], 2) + kin.v[3] * ((T(-4) * kin.v[0]) / T(3) + T(-2) * kin.v[1] + T(2) * kin.v[2] + (-bc<T>[1] + T(1)) * kin.v[3] + T(2) * kin.v[4] + bc<T>[1] * (T(2) + T(2) * kin.v[1] + T(-2) * kin.v[4])) + bc<T>[1] * ((-kin.v[4] + T(2)) * kin.v[4] + T(2) * kin.v[1] * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[4] + T(2)) * kin.v[4] + T(2) * kin.v[1] * kin.v[4] + kin.v[3] * (-kin.v[3] + T(2) + T(2) * kin.v[1] + T(-2) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(2) + bc<T>[1] * T(2) + T(4)) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[4] + bc<T>[1] * T(2) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[7] * (abb[6] * T(-4) + abb[3] * abb[5] * T(-2) + abb[4] * abb[5] * T(-2) + abb[1] * (abb[2] * T(-2) + abb[5] * T(2)) + abb[5] * (bc<T>[0] * int_to_imaginary<T>(2) + abb[5] * T(2)) + abb[2] * (abb[2] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[3] * T(2) + abb[4] * T(2) + abb[5] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_3_24_construct (const Kin<T>& kin) {
    return [&kin, 
            dl1 = DLog_W_1<T>(kin),dl7 = DLog_W_7<T>(kin),dl4 = DLog_W_4<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl19 = DLog_W_19<T>(kin),dl18 = DLog_W_18<T>(kin),dl2 = DLog_W_2<T>(kin),spdl7 = SpDLog_f_3_24_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,18> abbr = 
            {dl1(t), rlog(v_path[0]), rlog(-v_path[1]), rlog(v_path[3]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), dl7(t), dl4(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl5(t), dl20(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl19(t), dl18(t), dl2(t)}
;

        auto result = f_3_24_abbreviated(abbr);
        result = result + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_3_24_abbreviated(const std::array<T,18>& abb)
{
using TR = typename T::value_type;
T z[37];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[5];
z[4] = abb[3];
z[5] = abb[4];
z[6] = bc<TR>[0];
z[7] = abb[6];
z[8] = abb[15];
z[9] = abb[17];
z[10] = abb[8];
z[11] = abb[16];
z[12] = abb[11];
z[13] = abb[12];
z[14] = abb[9];
z[15] = abb[10];
z[16] = abb[13];
z[17] = abb[14];
z[18] = -z[8] + z[9];
z[19] = int_to_imaginary<T>(1) * z[6];
z[20] = z[18] * z[19];
z[21] = z[1] * z[18];
z[20] = T(2) * z[20] + -z[21];
z[22] = z[8] + z[10];
z[23] = z[0] + T(2) * z[9] + -z[22];
z[24] = T(3) * z[13];
z[25] = T(-2) * z[11] + -z[23] + z[24];
z[26] = -(z[2] * z[25]);
z[27] = T(-3) * z[11] + -z[18] + z[24];
z[27] = z[5] * z[27];
z[28] = z[9] + z[11];
z[29] = z[0] + T(2) * z[8] + -z[28];
z[30] = -z[10] + z[29];
z[31] = z[3] * z[30];
z[32] = z[4] * z[18];
z[26] = -z[20] + z[26] + z[27] / T(2) + -z[31] + -z[32];
z[26] = z[5] * z[26];
z[27] = T(3) * z[12];
z[29] = T(-2) * z[10] + z[27] + -z[29];
z[33] = z[3] * z[29];
z[25] = z[19] * z[25];
z[22] = T(2) * z[0] + z[22] + -z[27];
z[27] = z[11] / T(2);
z[34] = -z[9] + (T(3) * z[13]) / T(2) + -z[22] + z[27];
z[34] = z[2] * z[34];
z[23] = z[11] + -z[23];
z[23] = z[1] * z[23];
z[23] = z[23] + z[25] + -z[33] + z[34];
z[23] = z[2] * z[23];
z[25] = z[11] + -z[13] + z[18];
z[34] = z[17] * z[25];
z[35] = -z[10] + z[12] + z[18];
z[36] = z[15] * z[35];
z[34] = z[34] + z[36];
z[33] = -z[21] + -z[33] + T(3) * z[34];
z[19] = z[19] * z[33];
z[33] = -z[2] + z[3];
z[29] = z[29] * z[33];
z[20] = -z[20] + z[29] + z[32];
z[20] = z[4] * z[20];
z[22] = -z[22] + z[24] + -z[28];
z[22] = z[7] * z[22];
z[24] = -(z[16] * z[25]);
z[25] = z[14] * z[35];
z[24] = z[24] + z[25];
z[21] = -z[21] / T(2) + z[31];
z[21] = z[1] * z[21];
z[18] = -z[13] / T(2) + (T(-2) * z[18]) / T(3) + z[27];
z[18] = prod_pow(z[6], 2) * z[18];
z[25] = prod_pow(z[3], 2) * z[30];
return z[18] + z[19] + z[20] + z[21] + z[22] + z[23] + T(3) * z[24] + z[25] + z[26];
}



template IntegrandConstructorType<double> f_3_24_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_24_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_24_construct (const Kin<qd_real>&);
#endif

}