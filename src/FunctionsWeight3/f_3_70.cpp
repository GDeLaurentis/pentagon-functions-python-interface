#include "f_3_70.h"

namespace PentagonFunctions {

template <typename T> T f_3_70_abbreviated (const std::array<T,17>&);



template <typename T> IntegrandConstructorType<T> f_3_70_construct (const Kin<T>& kin) {
    return [&kin, 
            dl27 = DLog_W_27<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl28 = DLog_W_28<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,17> abbr = 
            {dl27(t) / kin_path.SqrtDelta, rlog(-v_path[4]), rlog(-v_path[3] + v_path[0] + v_path[1]), f_2_1_7(kin_path), f_2_1_8(kin_path), dl26(t) / kin_path.SqrtDelta, rlog(-v_path[1]), rlog(v_path[3]), f_2_1_4(kin_path), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl29(t) / kin_path.SqrtDelta, rlog(-v_path[4] + v_path[1] + v_path[2]), dl30(t) / kin_path.SqrtDelta, f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl28(t) / kin_path.SqrtDelta}
;

        auto result = f_3_70_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_3_70_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[29];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = bc<TR>[0];
z[6] = abb[5];
z[7] = abb[13];
z[8] = abb[6];
z[9] = abb[7];
z[10] = abb[16];
z[11] = abb[12];
z[12] = abb[11];
z[13] = abb[8];
z[14] = abb[9];
z[15] = abb[10];
z[16] = abb[14];
z[17] = abb[15];
z[18] = z[6] + z[7];
z[19] = -z[10] + z[18];
z[20] = z[1] * z[19];
z[21] = z[2] * z[6];
z[20] = z[20] + -z[21];
z[22] = z[7] + z[12];
z[23] = z[11] * z[22];
z[24] = z[12] + z[18];
z[25] = z[15] * z[24];
z[26] = -z[7] + z[10];
z[27] = z[17] * z[26];
z[28] = -(z[6] * z[8]);
z[25] = z[20] + -z[23] + z[25] + z[27] + z[28];
z[25] = int_to_imaginary<T>(1) * z[5] * z[25];
z[27] = -z[1] + z[8];
z[19] = z[19] * z[27];
z[27] = z[9] * z[24];
z[19] = z[19] + z[21] + z[23] + -z[27] / T(2);
z[19] = z[9] * z[19];
z[21] = -(z[7] * z[11]);
z[23] = z[10] + -z[18] / T(2);
z[23] = z[8] * z[23];
z[20] = z[20] + z[21] + z[23];
z[20] = z[8] * z[20];
z[21] = z[0] + z[6];
z[23] = z[21] + -z[22];
z[27] = prod_pow(z[2], 2);
z[27] = -z[4] + -z[27] / T(2);
z[23] = z[23] * z[27];
z[21] = z[2] * z[21];
z[18] = z[0] + z[18];
z[27] = z[1] * z[18];
z[21] = z[21] + -z[27] / T(2);
z[21] = z[1] * z[21];
z[22] = -(z[2] * z[22]);
z[27] = z[1] * z[7];
z[22] = z[22] + z[27];
z[22] = z[11] * z[22];
z[27] = -z[6] + z[10];
z[27] = z[13] * z[27];
z[24] = -(z[14] * z[24]);
z[28] = -z[0] + z[12];
z[28] = z[7] + z[28] / T(2);
z[28] = z[6] / T(2) + z[28] / T(3);
z[28] = prod_pow(z[5], 2) * z[28];
z[26] = z[16] * z[26];
z[18] = -(z[3] * z[18]);
return z[18] + z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[25] + z[26] + z[27] + z[28];
}



template IntegrandConstructorType<double> f_3_70_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_3_70_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_3_70_construct (const Kin<qd_real>&);
#endif

}