#pragma once
#include "PentagonFunctions_config.h"

#include "wise_enum/wise_enum.h"

#include <cmath>
#include <complex>
#include <array>

#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
#include <qd/qd_real.h>
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED

namespace PentagonFunctions {

/**
 * Enum for choosing the set of functions according to their kinematics.
 */
WISE_ENUM(KinType,
        m0, // massless
        m1  // one external mass
        )

template <typename T> std::complex<T> operator+(int i, const std::complex<T>& z) { return {z.real() + i, z.imag()}; }
template <typename T> std::complex<T> operator+(const std::complex<T>& z, int i) { return {z.real() + i, z.imag()}; }
template <typename T> std::complex<T> operator*(int i, const std::complex<T>& z) { return {z.real() * i, z.imag() * i}; }
template <typename T> std::complex<T> operator*(const std::complex<T>& z, int i) { return {z.real() * i, z.imag() * i}; }
template <typename T> std::complex<T> operator/(int i, const std::complex<T>& z) { return static_cast<T>(i)/z;}
template <typename T> std::complex<T> operator/(const std::complex<T>& z, int i) { return {z.real() / i, z.imag() / i}; }

template <typename T> inline T prod_pow(T x, size_t n) {
    T result(1);
    for (size_t i = 0; i < n; i++) { result *= x; }
    return result;
}

template <int n, typename T> inline T prod_pow(T x) {
    switch (n) {
        case 0: return T{1};
        case 1: return T{x};
        case 2: return T{x * x};
    }
    if constexpr (n < 0) { return 1 / prod_pow<-n>(x); }
    return prod_pow(x, n);
}

template <typename T, KinType Tk = KinType::m0> struct Kin;

template <typename T> struct Kin<T, KinType::m0> {
    using TC = std::complex<T>;
    static constexpr size_t Nvis = 5;
    static constexpr size_t NWs = 31;
    std::array<T, Nvis> v; /** Adjasent Mandelstam invariants */
    std::array<TC, NWs> W; /** Letters of the alphabet */
    TC SqrtDelta{}; /** Root of the gram determinant */

    Kin(const Kin&) = default;
    Kin(Kin&&) = default;
    Kin& operator=(const Kin&) = default;
    Kin& operator=(Kin&&) = default;
    /**
     * Construct 5-point kinematics from a list of adjacent Mandelstam invariants.
     */
    Kin(std::array<T, Nvis>&& v_in) : v(std::move(v_in)) { init(); }
    Kin(const std::array<T, Nvis>& v_in) : v(v_in) { init(); }
    template <typename... Ts> Kin(Ts... sij) : v{sij...} { init(); }
    // set im(sqrt(delta5)) from outside
    Kin(std::array<T, Nvis>&& v_in, T&& im_sqrtdelta5) : v(std::move(v_in)), SqrtDelta{0, std::move(im_sqrtdelta5)} { init(); }
    Kin(const std::array<T, Nvis>& v_in, const T& im_sqrtdelta5) : v(v_in), SqrtDelta{0, im_sqrtdelta5} { init(); }

  private:
    void init();
};

#ifdef PENTAGON_FUNCTIONS_M1_ENABLED

template <typename T> struct Kin<T, KinType::m1> {
    using TC = std::complex<T>;
    static constexpr size_t Nvis = 6;
    static constexpr size_t NWs = 204;
    static constexpr size_t sqrt_Delta5_index = 197;
    std::array<T, Nvis> v{}; /** Adjasent Mandelstam invariants */
    std::array<TC, NWs> W{}; /** Letters of the alphabet */

    Kin(const Kin&) = default;
    Kin(Kin&&) = default;
    Kin& operator=(const Kin&) = default;
    Kin& operator=(Kin&&) = default;

    /**
     * Construct 5-point kinematics from a list of adjacent Mandelstam invariants.
     */
    Kin(std::array<T, Nvis>&& v_in) : v(std::move(v_in)) { init(); }
    Kin(const std::array<T, Nvis>& v_in) : v(v_in) { init(); }
    template <typename... Ts> Kin(Ts... sij) : v{sij...} { init(); }
    // set im(sqrt(delta5)) from outside
    Kin(std::array<T, Nvis>&& v_in, T&& im_sqrtdelta5) : v(std::move(v_in)) {
        W[sqrt_Delta5_index] = {T{}, std::move(im_sqrtdelta5)};
        init();
    }
    Kin(const std::array<T, Nvis>& v_in, const T& im_sqrtdelta5) : v(v_in) {
        W[sqrt_Delta5_index] = {T{}, im_sqrtdelta5};
        init();
    }

    /**
     * Inittiate recomputation of the alphabet.
     * Can be used when certain letters were corrected for numerical stability.
     */
    void recompute_alphabet();
    void recompute_sigma5_letters();

  private:
    void init();
};

#endif // PENTAGON_FUNCTIONS_M1_ENABLED




extern template struct Kin<double, KinType::m0>;
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
extern template struct Kin<dd_real, KinType::m0>;
extern template struct Kin<qd_real, KinType::m0>;
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED


#ifdef PENTAGON_FUNCTIONS_M1_ENABLED 
extern template struct Kin<double, KinType::m1>;
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
extern template struct Kin<dd_real, KinType::m1>;
extern template struct Kin<qd_real, KinType::m1>;
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
#endif // PENTAGON_FUNCTIONS_M1_ENABLED

} // namespace PentagonFunctions

