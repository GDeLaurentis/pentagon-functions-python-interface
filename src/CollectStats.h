#pragma once

#include <cstddef>
#include <fstream>

struct count_n_hp_recompute_calls {
    std::string suffix{};
    size_t n_calls{0};
    size_t n_hp_recompute_calls{0};
    size_t n_hp_failed{0};

    ~count_n_hp_recompute_calls() {
        std::ofstream file("n_hp_recompute_calls."+suffix+".dat");
        file << n_calls << " " << n_hp_recompute_calls << " " << n_hp_failed << std::endl;
    };
};
