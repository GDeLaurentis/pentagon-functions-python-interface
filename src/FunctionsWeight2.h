#pragma once

#include "Kin.h"
#include "PolyLog.h"
#include "FunctionsWeight1.h"

#include "Constants.h"

namespace PentagonFunctions {

#ifdef PENTAGON_FUNCTIONS_M0_ENABLED 
template <typename T> T f_2_1_1(const Kin<T>& k) { return rLi2(-k.v[2] / k.v[0] + T(1)); }

template <typename T> T f_2_1_2(const Kin<T>& k) { return rLi2(-k.v[3] / k.v[0] + T(1)); }

template <typename T> T f_2_1_3(const Kin<T>& k) { return T(-1) * rlog(-k.v[3] / k.v[1]) * rlog(-k.v[3] / k.v[1] + T(1)) + T(-1) * rLi2(k.v[3] / k.v[1]); }

template <typename T> T f_2_1_4(const Kin<T>& k) { return rLi2(-k.v[4] / k.v[1] + T(1)); }

template <typename T> T f_2_1_5(const Kin<T>& k) { return T(-1) * rlog(-k.v[4] / k.v[2]) * rlog(-k.v[4] / k.v[2] + T(1)) + T(-1) * rLi2(k.v[4] / k.v[2]); }

template <typename T> T f_2_1_6(const Kin<T>& k) {
    return T(-1) * rlog(-(T(-1) * k.v[0] + T(-1) * k.v[1] + k.v[3]) / k.v[3]) * rlog(-(T(-1) * k.v[0] + T(-1) * k.v[1] + k.v[3]) / k.v[3] + T(1)) +
           T(-1) * rLi2((-k.v[0] + -k.v[1] + k.v[3]) / k.v[3]);
}

template <typename T> T f_2_1_7(const Kin<T>& k) { return rLi2(-(T(-1) * k.v[1] + T(-1) * k.v[2] + k.v[4]) / k.v[4] + T(1)); }

template <typename T> T f_2_1_8(const Kin<T>& k) { return rLi2(-(T(-1) * k.v[1] + T(-1) * k.v[2] + k.v[4]) / (-k.v[0] + -k.v[1] + k.v[3]) + T(1)); }

template <typename T> T f_2_1_9(const Kin<T>& k) { return rLi2(-(k.v[0] + T(-1) * k.v[2] + T(-1) * k.v[3]) / k.v[0] + T(1)); }

template <typename T> T f_2_1_10(const Kin<T>& k) {
    return T(-1) * rlog(-(k.v[0] + T(-1) * k.v[2] + T(-1) * k.v[3]) / (-k.v[1] + -k.v[2] + k.v[4])) *
               rlog(-(k.v[0] + T(-1) * k.v[2] + T(-1) * k.v[3]) / (-k.v[1] + -k.v[2] + k.v[4]) + T(1)) +
           T(-1) * rLi2((-k.v[2] + -k.v[3] + k.v[0]) / (-k.v[1] + -k.v[2] + k.v[4]));
}

template <typename T> T f_2_1_11(const Kin<T>& k) { return rLi2(-(k.v[1] + T(-1) * k.v[3] + T(-1) * k.v[4]) / k.v[1] + T(1)); }

template <typename T> T f_2_1_12(const Kin<T>& k) {
    return T(-1) * rlog(-(k.v[1] + T(-1) * k.v[3] + T(-1) * k.v[4]) / (-k.v[2] + -k.v[3] + k.v[0]) + T(1)) *
               rlog(-(k.v[1] + T(-1) * k.v[3] + T(-1) * k.v[4]) / (-k.v[2] + -k.v[3] + k.v[0])) +
           T(-1) * rLi2((-k.v[3] + -k.v[4] + k.v[1]) / (-k.v[2] + -k.v[3] + k.v[0]));
}

template <typename T> T f_2_1_13(const Kin<T>& k) {
    return T(-1) * rlog(-(T(-1) * k.v[0] + k.v[2] + T(-1) * k.v[4]) / k.v[2] + T(1)) * rlog(-(T(-1) * k.v[0] + k.v[2] + T(-1) * k.v[4]) / k.v[2]) +
           T(-1) * rLi2((-k.v[0] + -k.v[4] + k.v[2]) / k.v[2]);
}

template <typename T> T f_2_1_14(const Kin<T>& k) { return rLi2(-(T(-1) * k.v[0] + k.v[2] + T(-1) * k.v[4]) / (-k.v[0] + -k.v[1] + k.v[3]) + T(1)); }

template <typename T> T f_2_1_15(const Kin<T>& k) { return rLi2(-(T(-1) * k.v[0] + k.v[2] + T(-1) * k.v[4]) / (-k.v[3] + -k.v[4] + k.v[1]) + T(1)); }


template <typename T> std::complex<T> f_2_2_1 (const Kin<T>& k) {
    return (-SV_Li2(k.W[26] * k.W[27]) + SV_Li2(k.W[26]) + SV_Li2(k.W[27])) * int_to_imaginary<T>(-2);
}


template <typename T> std::complex<T> f_2_2_2 (const Kin<T>& k) {
    return (-SV_Li2(k.W[27] * k.W[28]) + SV_Li2(k.W[27]) + SV_Li2(k.W[28])) * int_to_imaginary<T>(-2);
}


template <typename T> std::complex<T> f_2_2_3 (const Kin<T>& k) {
    return (-SV_Li2((k.W[25] * k.W[26]) / k.W[28]) + -SV_Li2((k.W[27] * k.W[28]) / k.W[25]) +
        SV_Li2(k.W[26] * k.W[27])) *
       int_to_imaginary<T>(-2);
}


template <typename T> std::complex<T> f_2_2_4 (const Kin<T>& k) {
    return (-SV_Li2(k.W[25] * k.W[29]) + SV_Li2(k.W[25]) + SV_Li2(k.W[29])) * int_to_imaginary<T>(-2);
}


template <typename T> std::complex<T> f_2_2_5 (const Kin<T>& k) {
    return (-SV_Li2((k.W[26] * k.W[27]) / k.W[29]) + -SV_Li2((k.W[25] * k.W[29]) / k.W[27]) +
        SV_Li2(k.W[25] * k.W[26])) *
       int_to_imaginary<T>(2);
}


template <typename T> std::complex<T> f_2_2_6 (const Kin<T>& k) {
    return (-SV_Li2(k.W[28] * k.W[29]) + SV_Li2(k.W[28]) + SV_Li2(k.W[29])) * int_to_imaginary<T>(-2);
}


template <typename T> std::complex<T> f_2_2_7 (const Kin<T>& k) {
    return (-SV_Li2(k.W[28] * k.W[29]) + SV_Li2((k.W[27] * k.W[28]) / k.W[25]) +
        SV_Li2((k.W[25] * k.W[29]) / k.W[27])) *
       int_to_imaginary<T>(-2);
}


template <typename T> std::complex<T> f_2_2_8 (const Kin<T>& k) {
    return (-SV_Li2(k.W[25] * k.W[29]) + SV_Li2((k.W[25] * k.W[26]) / k.W[28]) +
        SV_Li2((k.W[28] * k.W[29]) / k.W[26])) *
       int_to_imaginary<T>(2);
}


template <typename T> std::complex<T> f_2_2_9 (const Kin<T>& k) {
    return (-SV_Li2((k.W[26] * k.W[27]) / k.W[29]) + -SV_Li2((k.W[28] * k.W[29]) / k.W[26]) +
        SV_Li2(k.W[27] * k.W[28])) *
       int_to_imaginary<T>(-2);
}


#endif // PENTAGON_FUNCTIONS_M0_ENABLED


#ifdef PENTAGON_FUNCTIONS_M1_ENABLED

namespace m1_set {

template <typename T> T f_2_1(const Kin<T, KinType::m1>& k) { return rLi2(-(k.W[2]).real() / (k.W[0]).real() + T(1)); }
template <typename T> T f_2_3(const Kin<T, KinType::m1>& k) { return rLi2(-(k.W[1]).real() / (k.W[0]).real() + T(1)); }
template <typename T> T f_2_5(const Kin<T, KinType::m1>& k) { return rLi2(-(k.W[1]).real() / (k.W[10]).real() + T(1)); }
template <typename T> T f_2_8(const Kin<T, KinType::m1>& k) { return rLi2(-(k.W[2]).real() / (k.W[10]).real() + T(1)); }

template <typename T> T f_2_13(const Kin<T, KinType::m1>& k) { return rLi2(-(k.W[6]).real() / (k.W[4]).real() + T(1)); }
template <typename T> T f_2_14(const Kin<T, KinType::m1>& k) { return rLi2(-(k.W[7]).real() / (k.W[3]).real() + T(1)); }
template <typename T> T f_2_15(const Kin<T, KinType::m1>& k) { return rLi2(-(k.W[9]).real() / (k.W[3]).real() + T(1)); }
template <typename T> T f_2_16(const Kin<T, KinType::m1>& k) { return rLi2(-(k.W[8]).real() / (k.W[4]).real() + T(1)); }
template <typename T> T f_2_17(const Kin<T, KinType::m1>& k) { return rLi2(-((k.W[1]).real() * (k.W[4]).real()) / ((k.W[0]).real() * (k.W[8]).real()) + T(1)); }
template <typename T> T f_2_18(const Kin<T, KinType::m1>& k) { return rLi2(-((k.W[1]).real() * (k.W[3]).real()) / ((k.W[0]).real() * (k.W[9]).real()) + T(1)); }
template <typename T> T f_2_19(const Kin<T, KinType::m1>& k) { return rLi2(-((k.W[2]).real() * (k.W[4]).real()) / ((k.W[0]).real() * (k.W[6]).real()) + T(1)); }
template <typename T> T f_2_20(const Kin<T, KinType::m1>& k) { return rLi2(-((k.W[2]).real() * (k.W[3]).real()) / ((k.W[0]).real() * (k.W[7]).real()) + T(1)); }
template <typename T> T f_2_21(const Kin<T, KinType::m1>& k) {
    return rLi2(-((k.W[1]).real() * (k.W[2]).real()) / ((k.W[0]).real() * (k.W[10]).real()) + T(1));
}

template <typename T> T f_2_2_re(const Kin<T, KinType::m1>& k) {
    return rlog(-(k.W[3]).real() / (k.W[0]).real()) * rlog(-(k.W[13]).real() / (k.W[0]).real()) + rLi2((k.W[3]).real() / (k.W[0]).real());
}
template <typename T> T f_2_2_im(const Kin<T, KinType::m1>& k) { return m1_set::bc<T>[0] * (-rlog((k.W[0]).real()) + rlog(-(k.W[13]).real())); }
template <typename T> std::complex<T> f_2_2(const Kin<T, KinType::m1>& k) { return {f_2_2_re(k), f_2_2_im(k)}; }
template <typename T> T f_2_4_re(const Kin<T, KinType::m1>& k) {
    return rlog(-(k.W[4]).real() / (k.W[0]).real()) * rlog(-(k.W[14]).real() / (k.W[0]).real()) + rLi2((k.W[4]).real() / (k.W[0]).real());
}
template <typename T> T f_2_4_im(const Kin<T, KinType::m1>& k) { return m1_set::bc<T>[0] * (-rlog((k.W[0]).real()) + rlog(-(k.W[14]).real())); }
template <typename T> std::complex<T> f_2_4(const Kin<T, KinType::m1>& k) { return {f_2_4_re(k), f_2_4_im(k)}; }
template <typename T> T f_2_6_re(const Kin<T, KinType::m1>& k) {
    return -(rlog((T(-1) * (k.W[1]).real()) / (k.W[8]).real()) * rlog((k.W[26]).real() / (k.W[1]).real())) + rLi2((k.W[8]).real() / (k.W[1]).real());
}
template <typename T> T f_2_6_im(const Kin<T, KinType::m1>& k) { return m1_set::bc<T>[0] * (-rlog((k.W[1]).real()) + rlog((k.W[26]).real())); }
template <typename T> std::complex<T> f_2_6(const Kin<T, KinType::m1>& k) { return {f_2_6_re(k), f_2_6_im(k)}; }
template <typename T> T f_2_7_re(const Kin<T, KinType::m1>& k) {
    return rlog(-(k.W[4]).real() / (k.W[5]).real()) * rlog((k.W[21]).real() / (k.W[5]).real()) + rLi2((k.W[4]).real() / (k.W[5]).real());
}
template <typename T> T f_2_7_im(const Kin<T, KinType::m1>& k) { return m1_set::bc<T>[0] * (-rlog((k.W[5]).real()) + rlog((k.W[21]).real())); }
template <typename T> std::complex<T> f_2_7(const Kin<T, KinType::m1>& k) { return {f_2_7_re(k), f_2_7_im(k)}; }
template <typename T> T f_2_9_re(const Kin<T, KinType::m1>& k) {
    return -(rlog((T(-1) * (k.W[1]).real()) / (k.W[9]).real()) * rlog((T(-1) * (k.W[23]).real()) / (k.W[1]).real())) + rLi2((k.W[9]).real() / (k.W[1]).real());
}
template <typename T> T f_2_9_im(const Kin<T, KinType::m1>& k) { return m1_set::bc<T>[0] * (-rlog((k.W[1]).real()) + rlog(-(k.W[23]).real())); }
template <typename T> std::complex<T> f_2_9(const Kin<T, KinType::m1>& k) { return {f_2_9_re(k), f_2_9_im(k)}; }
template <typename T> T f_2_10_re(const Kin<T, KinType::m1>& k) {
    return -(rlog((T(-1) * (k.W[2]).real()) / (k.W[7]).real()) * rlog((T(-1) * (k.W[22]).real()) / (k.W[2]).real())) + rLi2((k.W[7]).real() / (k.W[2]).real());
}
template <typename T> T f_2_10_im(const Kin<T, KinType::m1>& k) { return m1_set::bc<T>[0] * (-rlog((k.W[2]).real()) + rlog(-(k.W[22]).real())); }
template <typename T> std::complex<T> f_2_10(const Kin<T, KinType::m1>& k) { return {f_2_10_re(k), f_2_10_im(k)}; }
template <typename T> T f_2_11_re(const Kin<T, KinType::m1>& k) {
    return -(rlog((T(-1) * (k.W[2]).real()) / (k.W[6]).real()) * rlog((k.W[25]).real() / (k.W[2]).real())) + rLi2((k.W[6]).real() / (k.W[2]).real());
}
template <typename T> T f_2_11_im(const Kin<T, KinType::m1>& k) { return m1_set::bc<T>[0] * (-rlog((k.W[2]).real()) + rlog((k.W[25]).real())); }
template <typename T> std::complex<T> f_2_11(const Kin<T, KinType::m1>& k) { return {f_2_11_re(k), f_2_11_im(k)}; }
template <typename T> T f_2_12_re(const Kin<T, KinType::m1>& k) {
    return rlog(-(k.W[3]).real() / (k.W[5]).real()) * rlog(-(k.W[24]).real() / (k.W[5]).real()) + rLi2((k.W[3]).real() / (k.W[5]).real());
}
template <typename T> T f_2_12_im(const Kin<T, KinType::m1>& k) { return m1_set::bc<T>[0] * (-rlog((k.W[5]).real()) + rlog(-(k.W[24]).real())); }
template <typename T> std::complex<T> f_2_12(const Kin<T, KinType::m1>& k) { return {f_2_12_re(k), f_2_12_im(k)}; }
template <typename T> T f_2_22_re(const Kin<T, KinType::m1>& k) {
    return rLi2(-((k.W[3]).real() * (k.W[4]).real()) / ((k.W[0]).real() * (k.W[5]).real()) + T(1));
}
template <typename T> T f_2_22_im(const Kin<T, KinType::m1>& k) {
    return m1_set::bc<T>[0] * (-rlog((k.W[32]).real()) + rlog((k.W[0]).real()) + rlog((k.W[5]).real())) * T(2);
}
template <typename T> std::complex<T> f_2_22(const Kin<T, KinType::m1>& k) { return {f_2_22_re(k), f_2_22_im(k)}; }

template <typename T> T f_2_23(const Kin<T, KinType::m1>& k) {
    return prod_pow(m1_set::bc<T>[0], 2) / T(3) + prod_pow(rlog((T(2) * k.v[4]) / (-k.v[0] + -(k.W[194]).real() + k.v[2] + k.v[4]) + T(-1)), 2) / T(2) +
           prod_pow(rlog((-k.v[2] + -(k.W[194]).real() + k.v[0] + k.v[4]) / (-k.v[0] + -(k.W[194]).real() + k.v[2] + k.v[4])), 2) / T(2) +
           -prod_pow(rlog((k.v[0] + T(-1) * k.v[2] + k.v[4] + (k.W[194]).real()) / (T(-1) * k.v[0] + k.v[2] + k.v[4] + (k.W[194]).real())), 2) / T(2) +
           prod_pow(rlog((T(2) * k.v[4]) / (-k.v[0] + k.v[2] + k.v[4] + (k.W[194]).real()) + T(-1)), 2) / T(2) +
           rLi2((T(-2) * k.v[4]) / (-k.v[2] + -(k.W[194]).real() + k.v[0] + k.v[4]) + T(1)) * T(2) +
           rLi2((T(-2) * k.v[4]) / (-k.v[0] + -(k.W[194]).real() + k.v[2] + k.v[4]) + T(1)) * T(2);
}

template <typename T> T f_2_24_re(const Kin<T, KinType::m1>& k) {
    return prod_pow(m1_set::bc<T>[0], 2) / T(3) + prod_pow(rlog((T(-2) * k.v[3]) / (-k.v[1] + -k.v[5] + -(k.W[196]).real() + T(2) * k.v[3]) + T(1)), 2) / T(2) +
           prod_pow(rlog((-(k.W[196]).real() + k.v[1] + k.v[5]) / (-k.v[1] + -k.v[5] + -(k.W[196]).real() + T(2) * k.v[3])), 2) / T(2) +
           -prod_pow(
               rlog((T(-1) * k.v[1] + T(-1) * k.v[5] + T(-1) * (k.W[196]).real()) / (T(-1) * k.v[1] + T(2) * k.v[3] + T(-1) * k.v[5] + (k.W[196]).real())), 2) /
               T(2) +
           prod_pow(rlog((T(2) * k.v[3]) / (-k.v[1] + -k.v[5] + T(2) * k.v[3] + (k.W[196]).real()) + T(-1)), 2) / T(2) +
           rLi2((T(-2) * k.v[3]) / (-k.v[1] + -k.v[5] + -(k.W[196]).real() + T(2) * k.v[3]) + T(1)) * T(2) +
           rLi2((T(-2) * k.v[3]) / (-(k.W[196]).real() + k.v[1] + k.v[5]) + T(1)) * T(2);
}
template <typename T> T f_2_24_im(const Kin<T, KinType::m1>& k) {
    return m1_set::bc<T>[0] * rlog(-(k.v[1] + k.v[5] + (k.W[196]).real()) / (-(k.W[196]).real() + k.v[1] + k.v[5])) +
           m1_set::bc<T>[0] * rlog(-(T(-2) * k.v[0] + k.v[1] + k.v[5] + T(-1) * (k.W[196]).real()) / (T(-2) * k.v[0] + k.v[1] + k.v[5] + (k.W[196]).real()));
}
template <typename T> std::complex<T> f_2_24 (const Kin<T,KinType::m1>& k) { return {f_2_24_re(k), f_2_24_im(k)}; }

template <typename T> T f_2_25_re(const Kin<T, KinType::m1>& k) {
    return prod_pow(m1_set::bc<T>[0], 2) / T(3) +
           prod_pow(rlog((T(-2) * (-k.v[2] + -k.v[3] + k.v[5])) / (-k.v[0] + -k.v[2] + -k.v[4] + -(k.W[195]).real() + k.v[1] + T(-2) * k.v[3] + k.v[5]) + T(1)),
                    2) /
               T(2) +
           prod_pow(rlog((-k.v[1] + -k.v[2] + -(k.W[195]).real() + k.v[0] + k.v[4] + k.v[5]) /
                         (-k.v[0] + -k.v[2] + -k.v[4] + -(k.W[195]).real() + k.v[1] + T(-2) * k.v[3] + k.v[5])),
                    2) /
               T(2) +
           -prod_pow(rlog((T(-1) * k.v[0] + k.v[1] + k.v[2] + T(-1) * k.v[4] + T(-1) * k.v[5] + T(-1) * (k.W[195]).real()) /
                          (T(-1) * k.v[0] + k.v[1] + T(-1) * k.v[2] + T(-2) * k.v[3] + T(-1) * k.v[4] + k.v[5] + (k.W[195]).real())),
                     2) /
               T(2) +
           prod_pow(rlog((T(2) * (-k.v[2] + -k.v[3] + k.v[5])) / (-k.v[0] + -k.v[2] + -k.v[4] + k.v[1] + T(-2) * k.v[3] + k.v[5] + (k.W[195]).real()) + T(-1)),
                    2) /
               T(2) +
           rLi2((T(-2) * (-k.v[2] + -k.v[3] + k.v[5])) / (-k.v[0] + -k.v[2] + -k.v[4] + -(k.W[195]).real() + k.v[1] + T(-2) * k.v[3] + k.v[5]) + T(1)) * T(2) +
           rLi2((T(-2) * (-k.v[2] + -k.v[3] + k.v[5])) / (-k.v[1] + -k.v[2] + -(k.W[195]).real() + k.v[0] + k.v[4] + k.v[5]) + T(1)) * T(2);
}
template <typename T> T f_2_25_im(const Kin<T, KinType::m1>& k) {
    return m1_set::bc<T>[0] * rlog(-(k.v[0] + k.v[1] + k.v[2] + T(-1) * k.v[4] + T(-1) * k.v[5] + (k.W[195]).real()) /
                                   (-k.v[4] + -k.v[5] + -(k.W[195]).real() + k.v[0] + k.v[1] + k.v[2])) +
           m1_set::bc<T>[0] * rlog(-(k.v[0] + T(-1) * k.v[1] + T(-1) * k.v[2] + k.v[4] + k.v[5] + (k.W[195]).real()) /
                                   (-k.v[1] + -k.v[2] + -(k.W[195]).real() + k.v[0] + k.v[4] + k.v[5]));
}
template <typename T> std::complex<T> f_2_25(const Kin<T, KinType::m1>& k) { return {f_2_25_re(k), f_2_25_im(k)}; }


template <typename T> T f_2_26_re(const Kin<T, KinType::m1>& k) {
    using std::arg;

    T a = arg(k.W[136]);
    
    if (a > 0) a -= (2 * pi<T>());

    return -2*pi<T>()*a;
}

template <typename T> T f_2_26_im(const Kin<T, KinType::m1>& k) {
    return SV_Li2(k.W[134]) + SV_Li2(k.W[135]) + -SV_Li2(k.W[134] * k.W[135]) + SV_Li2(k.W[136]) + -SV_Li2(k.W[134] * k.W[136]) + -SV_Li2(k.W[135] * k.W[136]) + SV_Li2(k.W[134] * k.W[135] * k.W[136]);
}

template <typename T> std::complex<T> f_2_26(const Kin<T, KinType::m1>& k) { return {f_2_26_re(k), f_2_26_im(k)}; }


template <typename T> T f_2_27_re(const Kin<T, KinType::m1>& k) {
    using std::arg;

    T a = arg(k.W[136] / (k.W[131] * k.W[133]));
    if (a < 0) a += (2 * pi<T>());

    return -2*pi<T>()*a;
}

template <typename T> T f_2_27_im(const Kin<T, KinType::m1>& k) {
    return -SV_Li2(k.W[129]) + -SV_Li2(k.W[130]) + SV_Li2(k.W[129] * k.W[130]) + SV_Li2((k.W[129] * k.W[131] * k.W[133]) / k.W[136]) +
           SV_Li2((k.W[130] * k.W[131] * k.W[133]) / k.W[136]) + SV_Li2(k.W[136] / (k.W[131] * k.W[133])) +
           SV_Li2(k.W[136] / (k.W[129] * k.W[130] * k.W[131] * k.W[133]));
}

template <typename T> std::complex<T> f_2_27(const Kin<T, KinType::m1>& k) { return {f_2_27_re(k), f_2_27_im(k)}; }

template <typename T> T f_2_28_im(const Kin<T, KinType::m1>& k) {
    return SV_Li2(k.W[131]) + SV_Li2(k.W[132]) + -SV_Li2(k.W[131] * k.W[132]) + -SV_Li2(k.W[130] * k.W[133] * k.W[135]) +
           SV_Li2(k.W[130] * k.W[131] * k.W[133] * k.W[135]) + SV_Li2(k.W[130] * k.W[132] * k.W[133] * k.W[135]) +
           -SV_Li2(k.W[130] * k.W[131] * k.W[132] * k.W[133] * k.W[135]);
}

template <typename T> std::complex<T> f_2_28(const Kin<T, KinType::m1>& k) { return {0, f_2_28_im(k)}; }

template <typename T> T f_2_29_im(const Kin<T, KinType::m1>& k) {
    return -SV_Li2(k.W[129] * k.W[130] * k.W[131] * k.W[132]) + -SV_Li2(k.W[133]) + SV_Li2(k.W[129] * k.W[130] * k.W[131] * k.W[132] * k.W[133]) + SV_Li2((k.W[130] * k.W[132] * k.W[133]) / k.W[134]) + SV_Li2(k.W[129] * k.W[131] * k.W[134]) + SV_Li2(k.W[134] / (k.W[130] * k.W[132])) + -SV_Li2(k.W[129] * k.W[131] * k.W[133] * k.W[134]);
}

template <typename T> std::complex<T> f_2_29(const Kin<T, KinType::m1>& k) { return {0, f_2_29_im(k)}; }


// Sigma5-odd

template <typename T> T f_2_30_re(const Kin<T, KinType::m1>& k) {
    if (k.W[198].imag() != 0) return 0;
    return (-prod_pow(m1_set::bc<T>[0], 2) + rLi2(1 / ((k.W[161]).real() * (k.W[163]).real() * (k.W[182]).real() * (k.W[184]).real())) * T(-6) +
            rlog((k.W[161]).real()) * rlog((k.W[163]).real()) * T(-3) + rlog((k.W[163]).real()) * rlog((k.W[182]).real()) * T(-3) +
            rlog((k.W[161]).real()) * rlog((k.W[184]).real()) * T(-3) + rlog((k.W[182]).real()) * rlog((k.W[184]).real()) * T(-3) +
            rLi2(1 / ((k.W[161]).real() * (k.W[182]).real())) * T(6) + rLi2(1 / ((k.W[163]).real() * (k.W[184]).real())) * T(6)) /
           T(3);
}
template <typename T> T f_2_30_im(const Kin<T, KinType::m1>& k) {
    if (k.W[198].real() != 0) return 0;
    return SV_Li2(k.W[161] * k.W[182]) * T(-2) + SV_Li2(k.W[163] * k.W[184]) * T(-2) + SV_Li2(k.W[161] * k.W[163] * k.W[182] * k.W[184]) * T(2);
}
template <typename T> std::complex<T> f_2_30(const Kin<T, KinType::m1>& k) { return {f_2_30_re(k), f_2_30_im(k)}; }


template <typename T> T f_2_31_re(const Kin<T, KinType::m1>& k) {
    if (k.W[199].imag() != 0) return 0;
    return -prod_pow(m1_set::bc<T>[0], 2) / T(3) + -(rlog((k.W[162]).real()) * rlog((k.W[165]).real())) + -(rlog((k.W[165]).real()) * rlog((k.W[176]).real())) + -(rlog((k.W[162]).real()) * rlog((k.W[178]).real())) + -(rlog((k.W[176]).real()) * rlog((k.W[178]).real())) + rLi2(1 / ((k.W[162]).real() * (k.W[176]).real())) * T(2) + rLi2(1 / ((k.W[165]).real() * (k.W[178]).real())) * T(2) + rLi2(1 / ((k.W[162]).real() * (k.W[165]).real() * (k.W[176]).real() * (k.W[178]).real())) * T(-2);
}
template <typename T> T f_2_31_im(const Kin<T, KinType::m1>& k) {
    if (k.W[199].real() != 0) return 0;
    return SV_Li2(k.W[162] * k.W[176]) * T(-2) + SV_Li2(k.W[165] * k.W[178]) * T(-2) + SV_Li2(k.W[162] * k.W[165] * k.W[176] * k.W[178]) * T(2);
}
template <typename T> std::complex<T> f_2_31(const Kin<T, KinType::m1>& k) { return {f_2_31_re(k), f_2_31_im(k)}; }


template <typename T> T f_2_32_re(const Kin<T, KinType::m1>& k) {
    if (k.W[200].real() != 0) {
        return (prod_pow(m1_set::bc<T>[0], 2) * T(-11)) / T(3) + rlog((k.W[164]).real()) * rlog((k.W[166]).real()) + rlog((k.W[166]).real()) * rlog((k.W[170]).real()) + rlog((k.W[164]).real()) * rlog((k.W[172]).real()) + rlog((k.W[170]).real()) * rlog((k.W[172]).real()) + rLi2((k.W[164]).real() * (k.W[170]).real()) * T(-2) + rLi2((k.W[166]).real() * (k.W[172]).real()) * T(-2) + rLi2((k.W[164]).real() * (k.W[166]).real() * (k.W[170]).real() * (k.W[172]).real()) * T(2);
    }

    using std::arg;

    T phi = log_iphi_im(k.W[164]*k.W[166]*k.W[170]*k.W[172]);

    return (-2)*pi<T>()*phi;
}
template <typename T> T f_2_32_im(const Kin<T, KinType::m1>& k) {
    if (k.W[200].real() != 0) {
        return m1_set::bc<T>[0] * rlog((k.W[164]).real() * (k.W[166]).real() * (k.W[170]).real() * (k.W[172]).real()) * T(2);
    }
    return SV_Li2(k.W[164] * k.W[170]) * T(-2) + SV_Li2(k.W[166] * k.W[172]) * T(-2) + SV_Li2(k.W[164] * k.W[166] * k.W[170] * k.W[172]) * T(2);
}
template <typename T> std::complex<T> f_2_32(const Kin<T, KinType::m1>& k) { return {f_2_32_re(k), f_2_32_im(k)}; }


template <typename T> T f_2_33_re(const Kin<T, KinType::m1>& k) {
    if (k.W[201].imag() != 0) return 0;
    return -prod_pow(m1_set::bc<T>[0], 2) / T(3) + -(rlog((k.W[167]).real()) * rlog((k.W[169]).real())) + -(rlog((k.W[169]).real()) * rlog((k.W[180]).real())) + -(rlog((k.W[167]).real()) * rlog((k.W[183]).real())) + -(rlog((k.W[180]).real()) * rlog((k.W[183]).real())) + rLi2(1 / ((k.W[167]).real() * (k.W[180]).real())) * T(2) + rLi2(1 / ((k.W[169]).real() * (k.W[183]).real())) * T(2) + rLi2(1 / ((k.W[167]).real() * (k.W[169]).real() * (k.W[180]).real() * (k.W[183]).real())) * T(-2);
}
template <typename T> T f_2_33_im(const Kin<T, KinType::m1>& k) {
    if (k.W[201].real() != 0) return 0;
    return SV_Li2(k.W[167] * k.W[180]) * T(-2) + SV_Li2(k.W[169] * k.W[183]) * T(-2) + SV_Li2(k.W[167] * k.W[169] * k.W[180] * k.W[183]) * T(2);
}
template <typename T> std::complex<T> f_2_33(const Kin<T, KinType::m1>& k) { return {f_2_33_re(k), f_2_33_im(k)}; }




template <typename T> T f_2_34_re(const Kin<T, KinType::m1>& k) {
    if (k.W[202].imag() != 0) return 0;
    return -prod_pow(m1_set::bc<T>[0], 2) / T(3) + -(rlog((k.W[168]).real()) * rlog((k.W[171]).real())) + -(rlog((k.W[171]).real()) * rlog((k.W[174]).real())) + -(rlog((k.W[168]).real()) * rlog((k.W[177]).real())) + -(rlog((k.W[174]).real()) * rlog((k.W[177]).real())) + rLi2(1 / ((k.W[168]).real() * (k.W[174]).real())) * T(2) + rLi2(1 / ((k.W[171]).real() * (k.W[177]).real())) * T(2) + rLi2(1 / ((k.W[168]).real() * (k.W[171]).real() * (k.W[174]).real() * (k.W[177]).real())) * T(-2);
}
template <typename T> T f_2_34_im(const Kin<T, KinType::m1>& k) {
    if (k.W[202].real() != 0) return 0;
    return SV_Li2(k.W[168] * k.W[174]) * T(-2) + SV_Li2(k.W[171] * k.W[177]) * T(-2) + SV_Li2(k.W[168] * k.W[171] * k.W[174] * k.W[177]) * T(2);
}
template <typename T> std::complex<T> f_2_34(const Kin<T, KinType::m1>& k) { return {f_2_34_re(k), f_2_34_im(k)}; }


template <typename T> T f_2_35_re(const Kin<T, KinType::m1>& k) {
    if (k.W[203].imag() != 0) return 0;
    return prod_pow(m1_set::bc<T>[0], 2) / T(3) + rlog((k.W[173]).real()) * rlog((k.W[175]).real()) + rlog((k.W[175]).real()) * rlog((k.W[179]).real()) + rlog((k.W[173]).real()) * rlog((k.W[181]).real()) + rlog((k.W[179]).real()) * rlog((k.W[181]).real()) + rLi2((k.W[173]).real() * (k.W[179]).real()) * T(-2) + rLi2((k.W[175]).real() * (k.W[181]).real()) * T(-2) + rLi2((k.W[173]).real() * (k.W[175]).real() * (k.W[179]).real() * (k.W[181]).real()) * T(2);
}
template <typename T> T f_2_35_im(const Kin<T, KinType::m1>& k) {
    if (k.W[203].real() != 0) return 0;
    return SV_Li2(k.W[173] * k.W[179]) * T(-2) + SV_Li2(k.W[175] * k.W[181]) * T(-2) + SV_Li2(k.W[173] * k.W[175] * k.W[179] * k.W[181]) * T(2);
}
template <typename T> std::complex<T> f_2_35(const Kin<T, KinType::m1>& k) { return {f_2_35_re(k), f_2_35_im(k)}; }



} // namespace m1_set

#endif // PENTAGON_FUNCTIONS_M1_ENABLED


} // namespace PentagonFunctions

