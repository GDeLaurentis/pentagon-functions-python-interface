#include "f_4_34.h"

namespace PentagonFunctions {

template <typename T> T f_4_34_abbreviated (const std::array<T,16>&);



template <typename T> IntegrandConstructorType<T> f_4_34_construct (const Kin<T>& kin) {
    return [&kin, 
            dl14 = DLog_W_14<T>(kin),dl4 = DLog_W_4<T>(kin),dl19 = DLog_W_19<T>(kin),dl5 = DLog_W_5<T>(kin),dl2 = DLog_W_2<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,16> abbr = 
            {dl14(t), rlog(kin.W[1] / kin_path.W[1]), rlog(-v_path[1]), rlog(v_path[3]), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), rlog(kin.W[13] / kin_path.W[13]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[18] / kin_path.W[18]), dl4(t), f_2_1_11(kin_path), dl19(t), dl5(t), dl2(t)}
;

        auto result = f_4_34_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_34_abbreviated(const std::array<T,16>& abb)
{
using TR = typename T::value_type;
T z[42];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[8];
z[9] = abb[9];
z[10] = abb[10];
z[11] = abb[7];
z[12] = bc<TR>[2];
z[13] = bc<TR>[9];
z[14] = abb[11];
z[15] = abb[13];
z[16] = abb[14];
z[17] = abb[15];
z[18] = abb[12];
z[19] = z[0] / T(2);
z[20] = z[17] / T(2);
z[21] = z[19] + z[20];
z[22] = z[15] + -z[16] + z[21];
z[22] = z[1] * z[22];
z[19] = z[19] + -z[20];
z[23] = z[14] + z[15];
z[24] = T(2) * z[23];
z[25] = -z[16] + z[24];
z[26] = -z[19] + z[25];
z[26] = z[7] * z[26];
z[27] = z[9] + z[10];
z[24] = z[17] + z[24];
z[28] = T(3) * z[0] + -z[24];
z[29] = z[27] * z[28];
z[30] = z[8] * z[15];
z[31] = z[8] * z[17];
z[32] = z[0] * z[8];
z[22] = z[22] + -z[26] + -z[29] + -z[30] + -z[31] + T(2) * z[32];
z[22] = T(3) * z[22];
z[26] = -(z[6] * z[22]);
z[33] = z[30] + -z[32];
z[34] = -z[0] + -z[17];
z[34] = z[7] * z[34];
z[34] = -z[33] + z[34] / T(2);
z[35] = (T(3) * z[0]) / T(2);
z[36] = T(-2) * z[14] + z[15] + z[20] + z[35];
z[36] = z[1] * z[36];
z[34] = -z[29] + T(3) * z[34] + z[36];
z[34] = z[2] * z[34];
z[32] = z[31] + -z[32];
z[25] = z[0] + -z[25];
z[25] = z[7] * z[25];
z[25] = z[25] + -z[32];
z[36] = T(2) * z[27];
z[37] = z[28] * z[36];
z[38] = T(3) * z[16];
z[24] = -z[24] + z[38];
z[24] = z[1] * z[24];
z[24] = z[24] + T(-3) * z[25] + z[37];
z[25] = -(z[11] * z[24]);
z[37] = -z[21] + T(3) * z[23];
z[37] = -z[16] + z[37] / T(2);
z[37] = prod_pow(z[12], 2) * z[37];
z[39] = T(3) * z[7];
z[40] = -z[1] + z[39];
z[41] = -z[36] + z[40];
z[28] = z[4] * z[28] * z[41];
z[25] = z[25] + z[26] + z[28] + z[34] + T(3) * z[37];
z[25] = int_to_imaginary<T>(1) * z[25];
z[26] = T(2) * z[17] + (T(5) * z[23]) / T(2);
z[26] = z[26] / T(3) + -z[35];
z[26] = z[26] * z[27];
z[21] = -z[21] + z[23];
z[21] = int_to_imaginary<T>(1) * z[3] * z[21];
z[27] = (T(-9) * z[14]) / T(2) + T(-5) * z[15];
z[27] = (T(17) * z[0]) / T(4) + -z[17] + z[27] / T(2);
z[27] = z[7] * z[27];
z[28] = T(2) * z[15] + z[17];
z[34] = (T(17) * z[14]) / T(4) + z[28];
z[34] = (T(-7) * z[0]) / T(4) + z[34] / T(3);
z[34] = z[1] * z[34];
z[37] = (T(11) * z[0]) / T(2) + (T(-5) * z[17]) / T(2) + -z[23];
z[37] = -z[16] + z[37] / T(2);
z[37] = z[12] * z[37];
z[21] = z[21] / T(2) + z[26] + z[27] + z[33] + z[34] + (T(3) * z[37]) / T(2);
z[21] = z[3] * z[21];
z[21] = z[21] + z[25];
z[21] = z[3] * z[21];
z[22] = -(z[5] * z[22]);
z[19] = -z[16] + z[19] + z[23];
z[19] = z[7] * z[19];
z[19] = z[19] + z[32];
z[20] = z[20] + z[23];
z[25] = -z[20] + -z[35] + z[38];
z[25] = z[1] * z[25];
z[19] = T(3) * z[19] + z[25] + z[29];
z[19] = prod_pow(z[2], 2) * z[19];
z[25] = -(z[2] * z[24]);
z[20] = z[20] + -z[35];
z[20] = z[20] * z[40];
z[20] = z[20] + z[29];
z[20] = z[4] * z[20];
z[20] = z[20] + z[25];
z[20] = z[4] * z[20];
z[25] = -z[2] + z[4];
z[24] = z[24] * z[25];
z[25] = -z[17] + z[23];
z[26] = -(z[25] * z[36]);
z[27] = z[14] + -z[17];
z[27] = z[27] * z[39];
z[28] = -z[14] + z[28];
z[28] = z[1] * z[28];
z[26] = z[26] + z[27] + z[28];
z[26] = z[11] * z[26];
z[24] = z[24] + z[26];
z[24] = z[11] * z[24];
z[25] = -z[16] + z[25];
z[25] = z[7] * z[25];
z[26] = -z[14] + z[16];
z[26] = z[1] * z[26];
z[25] = z[25] + z[26] + -z[30] + z[31];
z[25] = z[18] * z[25];
z[23] = (T(325) * z[17]) / T(2) + T(-23) * z[23];
z[23] = (T(-105) * z[0]) / T(2) + z[23] / T(3);
z[23] = z[23] / T(2) + z[38];
z[23] = z[13] * z[23];
return z[19] + z[20] + z[21] + z[22] + z[23] / T(4) + z[24] + T(3) * z[25];
}



template IntegrandConstructorType<double> f_4_34_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_34_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_34_construct (const Kin<qd_real>&);
#endif

}