#include "f_4_127.h"

namespace PentagonFunctions {

template <typename T> T f_4_127_abbreviated (const std::array<T,41>&);



template <typename T> IntegrandConstructorType<T> f_4_127_construct (const Kin<T>& kin) {
    return [&kin, 
            dl13 = DLog_W_13<T>(kin),dl25 = DLog_W_25<T>(kin),dl16 = DLog_W_16<T>(kin),dl30 = DLog_W_30<T>(kin),dl2 = DLog_W_2<T>(kin),dl29 = DLog_W_29<T>(kin),dl5 = DLog_W_5<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl26 = DLog_W_26<T>(kin),dl3 = DLog_W_3<T>(kin),dl6 = DLog_W_6<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),dl17 = DLog_W_17<T>(kin),dl4 = DLog_W_4<T>(kin),dl1 = DLog_W_1<T>(kin),dl19 = DLog_W_19<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,41> abbr = 
            {dl13(t), rlog(v_path[0]), rlog(v_path[2]), rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[17] / kin_path.W[17]), dl25(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[18] / kin_path.W[18]), dl16(t), f_2_1_13(kin_path), f_2_1_15(kin_path), rlog(v_path[0] + v_path[4]), dl30(t) / kin_path.SqrtDelta, f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl2(t), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl29(t) / kin_path.SqrtDelta, f_2_1_1(kin_path), dl5(t), dl27(t) / kin_path.SqrtDelta, f_2_1_9(kin_path), dl28(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl3(t), rlog(kin.W[5] / kin_path.W[5]), rlog(kin.W[19] / kin_path.W[19]), dl6(t), dl18(t), dl20(t), dl17(t), dl4(t), dl1(t), dl19(t)}
;

        auto result = f_4_127_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_127_abbreviated(const std::array<T,41>& abb)
{
using TR = typename T::value_type;
T z[149];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[3];
z[3] = abb[4];
z[4] = abb[5];
z[5] = abb[6];
z[6] = bc<TR>[0];
z[7] = abb[2];
z[8] = abb[26];
z[9] = abb[31];
z[10] = abb[34];
z[11] = abb[35];
z[12] = abb[36];
z[13] = abb[37];
z[14] = abb[38];
z[15] = abb[39];
z[16] = abb[40];
z[17] = abb[10];
z[18] = abb[11];
z[19] = abb[17];
z[20] = abb[24];
z[21] = abb[29];
z[22] = abb[30];
z[23] = abb[18];
z[24] = abb[19];
z[25] = abb[32];
z[26] = abb[33];
z[27] = abb[8];
z[28] = abb[9];
z[29] = abb[21];
z[30] = abb[27];
z[31] = abb[12];
z[32] = abb[16];
z[33] = abb[7];
z[34] = abb[20];
z[35] = abb[13];
z[36] = abb[14];
z[37] = abb[15];
z[38] = abb[22];
z[39] = abb[23];
z[40] = abb[25];
z[41] = abb[28];
z[42] = bc<TR>[1];
z[43] = bc<TR>[3];
z[44] = bc<TR>[5];
z[45] = bc<TR>[2];
z[46] = bc<TR>[4];
z[47] = bc<TR>[7];
z[48] = bc<TR>[8];
z[49] = bc<TR>[9];
z[50] = z[3] / T(2);
z[51] = z[5] / T(2) + z[50];
z[52] = z[18] / T(2);
z[53] = z[51] + z[52];
z[54] = (T(3) * z[17]) / T(2);
z[55] = (T(3) * z[4]) / T(2);
z[56] = z[54] + z[55];
z[57] = (T(5) * z[2]) / T(2);
z[58] = -z[53] + -z[56] + z[57];
z[59] = T(2) * z[26];
z[60] = T(4) * z[25];
z[61] = z[59] + z[60];
z[62] = -z[58] + -z[61];
z[62] = z[1] * z[62];
z[63] = z[2] + z[18];
z[64] = T(2) * z[5];
z[65] = -z[63] + z[64];
z[66] = T(2) * z[25];
z[67] = z[26] + z[66];
z[68] = z[65] + z[67];
z[69] = -z[3] + z[68];
z[70] = T(2) * z[27];
z[69] = z[69] * z[70];
z[71] = z[3] + -z[4];
z[72] = -z[17] + z[18];
z[73] = z[71] + z[72];
z[74] = z[2] + -z[5];
z[75] = z[73] + z[74];
z[76] = z[39] * z[75];
z[77] = z[71] + -z[72];
z[78] = -z[74] + z[77];
z[79] = (T(3) * z[37]) / T(2);
z[80] = z[78] * z[79];
z[81] = T(2) * z[7];
z[82] = z[74] * z[81];
z[83] = T(3) * z[77];
z[84] = -z[74] + z[83];
z[85] = z[28] / T(2);
z[86] = -(z[84] * z[85]);
z[87] = z[42] * z[45];
z[88] = T(3) * z[87];
z[89] = z[29] * z[74];
z[90] = prod_pow(z[42], 2);
z[62] = T(-7) * z[46] + z[62] + z[69] + (T(3) * z[76]) / T(2) + z[80] + z[82] + z[86] + z[88] + T(-2) * z[89] + (T(-11) * z[90]) / T(2);
z[62] = z[12] * z[62];
z[86] = (T(3) * z[18]) / T(2);
z[91] = (T(3) * z[2]) / T(2);
z[92] = (T(-5) * z[17]) / T(2) + -z[51] + z[86] + z[91];
z[93] = z[4] / T(2);
z[94] = -z[67] + z[93];
z[95] = z[92] + -z[94];
z[96] = T(3) * z[95];
z[97] = z[39] * z[96];
z[98] = T(2) * z[2];
z[99] = z[67] + z[98];
z[100] = z[3] + z[5];
z[101] = T(3) * z[17];
z[102] = T(-2) * z[18] + -z[99] + z[100] + z[101];
z[103] = z[70] * z[102];
z[97] = z[97] + z[103];
z[104] = z[2] / T(2);
z[51] = z[51] + -z[56] + z[104];
z[105] = z[52] + z[67];
z[106] = z[51] + z[105];
z[107] = -(z[1] * z[106]);
z[108] = z[29] / T(2);
z[109] = -(z[84] * z[108]);
z[110] = T(2) * z[28];
z[111] = -(z[74] * z[110]);
z[112] = T(8) * z[46];
z[113] = T(6) * z[87];
z[107] = -z[82] + T(-5) * z[90] + z[97] + z[107] + z[109] + z[111] + -z[112] + z[113];
z[107] = z[16] * z[107];
z[109] = z[28] * z[106];
z[58] = -z[58] + -z[59];
z[58] = z[29] * z[58];
z[111] = z[61] + -z[101];
z[114] = T(2) * z[3];
z[64] = T(-4) * z[2] + -z[18] + z[64] + -z[111] + z[114];
z[64] = z[1] * z[64];
z[88] = -z[88] + (T(7) * z[90]) / T(2);
z[115] = z[3] + z[18];
z[116] = z[5] + z[115];
z[99] = z[99] + -z[116];
z[99] = z[81] * z[99];
z[73] = z[73] + -z[74];
z[117] = z[73] * z[79];
z[60] = z[29] * z[60];
z[58] = z[46] + z[58] + -z[60] + T(2) * z[64] + -z[88] + z[97] + z[99] + -z[109] + z[117];
z[58] = z[15] * z[58];
z[64] = T(3) * z[4];
z[68] = -z[64] + z[68] + z[114];
z[97] = z[1] * z[68];
z[99] = T(2) * z[97];
z[114] = z[68] * z[70];
z[117] = z[74] + z[83];
z[118] = z[108] * z[117];
z[119] = T(4) * z[7] + -z[110];
z[119] = z[74] * z[119];
z[120] = prod_pow(z[6], 2);
z[121] = (T(3) * z[120]) / T(4);
z[80] = -z[80] + T(-2) * z[90] + z[99] + -z[112] + -z[114] + z[118] + z[119] + z[121];
z[80] = z[9] * z[80];
z[112] = z[7] * z[68];
z[65] = -z[3] + z[26] + z[65];
z[65] = z[29] * z[65];
z[66] = z[29] * z[66];
z[119] = -z[65] + -z[66] + z[112];
z[69] = T(-4) * z[46] + z[69] + -z[90] + -z[119];
z[122] = z[17] / T(2);
z[123] = (T(3) * z[5]) / T(2) + -z[52] + -z[104] + -z[122];
z[50] = z[50] + z[94] + -z[123];
z[50] = T(3) * z[50];
z[124] = z[39] * z[50];
z[69] = T(2) * z[69] + z[99] + z[109] + -z[121] + z[124];
z[69] = z[11] * z[69];
z[99] = z[22] + z[30];
z[121] = z[20] + z[21];
z[124] = z[99] + -z[121];
z[125] = z[39] * z[124];
z[126] = -z[22] + z[121];
z[126] = z[1] * z[126];
z[127] = -z[20] + z[30];
z[128] = -z[32] + z[127];
z[129] = z[28] * z[128];
z[130] = z[32] + z[121];
z[131] = z[37] * z[130];
z[132] = z[21] * z[29];
z[125] = z[125] + z[126] + -z[129] + -z[131] + z[132];
z[126] = z[19] + z[23] + z[24];
z[129] = (T(-3) * z[126]) / T(2);
z[125] = z[125] * z[129];
z[129] = z[74] + z[77];
z[131] = z[16] * z[129];
z[133] = z[8] * z[73];
z[134] = z[31] * z[129];
z[131] = z[131] + -z[133] + -z[134];
z[79] = z[79] * z[131];
z[135] = z[10] + z[15];
z[136] = (T(5) * z[16]) / T(2);
z[137] = T(3) * z[14];
z[138] = z[21] + z[99];
z[135] = (T(3) * z[12]) / T(2) + T(3) * z[135] + z[136] + -z[137] + (T(11) * z[138]) / T(90);
z[135] = z[120] * z[135];
z[139] = z[72] + z[74];
z[139] = z[33] * z[139];
z[140] = z[27] * z[139];
z[71] = z[71] + -z[74];
z[71] = z[0] * z[71];
z[141] = -z[1] + z[7];
z[141] = z[71] * z[141];
z[140] = z[140] + z[141];
z[64] = -z[63] + z[64] + -z[100];
z[111] = z[64] + -z[111];
z[70] = -(z[70] * z[111]);
z[70] = T(-14) * z[46] + z[70] + T(-11) * z[90] + z[113];
z[70] = z[10] * z[70];
z[113] = z[10] * z[111];
z[141] = T(2) * z[113];
z[133] = (T(3) * z[133]) / T(2) + z[141];
z[133] = z[1] * z[133];
z[142] = z[29] * z[75];
z[76] = -z[76] + z[142];
z[143] = (T(3) * z[34]) / T(2);
z[76] = z[76] * z[143];
z[144] = z[1] + -z[39];
z[144] = z[95] * z[144];
z[145] = T(5) * z[46];
z[88] = z[88] + z[144] + z[145];
z[88] = T(3) * z[88] + T(-2) * z[120];
z[88] = z[13] * z[88];
z[144] = z[90] * z[138];
z[146] = (T(3) * z[134]) / T(2);
z[147] = z[28] * z[146];
z[148] = z[46] + z[90] / T(2);
z[87] = -z[87] + T(5) * z[148];
z[87] = z[87] * z[137];
z[58] = z[58] + z[62] + z[69] + z[70] + z[76] + z[79] + z[80] + z[87] + z[88] + z[107] + z[125] + z[133] + z[135] / T(2) + T(6) * z[140] + (T(3) * z[144]) / T(20) + z[147];
z[58] = z[6] * z[58];
z[62] = T(3) * z[42];
z[69] = -(z[43] * z[62]);
z[69] = (T(-36) * z[44]) / T(5) + z[69];
z[69] = z[69] * z[138];
z[58] = z[58] + z[69];
z[58] = int_to_imaginary<T>(1) * z[58];
z[51] = z[26] + z[51] + z[52];
z[52] = z[29] * z[51];
z[52] = z[52] + z[66];
z[69] = z[7] * z[106];
z[70] = z[52] + -z[69];
z[76] = z[28] * z[102];
z[76] = -z[70] + z[76] + -z[103];
z[76] = z[28] * z[76];
z[79] = z[102] * z[110];
z[80] = z[27] * z[106];
z[79] = z[79] + z[80];
z[87] = (T(5) * z[3]) / T(2);
z[55] = (T(-7) * z[2]) / T(2) + (T(5) * z[5]) / T(2) + -z[55] + z[87];
z[88] = -z[54] + -z[55] + z[105];
z[88] = z[1] * z[88];
z[102] = -z[26] + z[116];
z[98] = -z[98] + z[102];
z[98] = z[29] * z[98];
z[105] = -z[66] + z[98];
z[88] = z[69] + z[79] + z[88] + T(-2) * z[105];
z[88] = z[1] * z[88];
z[51] = z[25] + z[51] / T(2);
z[106] = z[27] * z[51];
z[106] = -z[70] + z[106];
z[106] = z[27] * z[106];
z[107] = z[38] * z[96];
z[106] = z[106] + z[107];
z[53] = z[53] + -z[91] + z[94] + z[122];
z[53] = z[40] * z[53];
z[91] = z[115] / T(4);
z[94] = (T(3) * z[4]) / T(4);
z[107] = (T(3) * z[17]) / T(4) + -z[67] + z[91] + z[94];
z[110] = z[5] / T(4);
z[116] = (T(-5) * z[2]) / T(4) + z[107] + z[110];
z[116] = z[7] * z[116];
z[105] = z[105] + z[116];
z[105] = z[7] * z[105];
z[102] = z[2] + z[25] + -z[102] / T(2);
z[116] = prod_pow(z[29], 2);
z[102] = z[102] * z[116];
z[125] = T(3) * z[46];
z[133] = z[90] / T(6) + -z[125];
z[133] = z[42] * z[133];
z[55] = (T(9) * z[17]) / T(2) + (T(-7) * z[18]) / T(2) + z[55] + -z[67];
z[135] = -(z[41] * z[55]);
z[140] = prod_pow(z[45], 3);
z[144] = (T(4) * z[140]) / T(3);
z[147] = -z[47] + (T(16) * z[49]) / T(3);
z[53] = T(3) * z[53] + z[76] + z[88] + z[102] + z[105] + -z[106] + z[133] + z[135] + z[144] + T(2) * z[147];
z[53] = z[15] * z[53];
z[76] = (T(9) * z[17]) / T(4);
z[61] = (T(7) * z[2]) / T(4) + (T(-9) * z[4]) / T(4) + z[61] + -z[76] + z[91] + z[110];
z[61] = z[1] * z[61];
z[66] = -z[66] + z[69];
z[61] = z[61] + -z[66] + -z[80] + -z[98] + -z[109];
z[61] = z[1] * z[61];
z[65] = z[65] + -z[66];
z[66] = -z[2] / T(4) + (T(5) * z[5]) / T(4) + -z[107];
z[69] = -(z[27] * z[66]);
z[69] = -z[65] + z[69];
z[69] = z[27] * z[69];
z[88] = z[7] / T(2);
z[91] = z[84] * z[88];
z[98] = z[28] / T(4);
z[102] = -(z[98] * z[117]);
z[102] = z[80] + -z[89] + z[91] + z[102];
z[102] = z[28] * z[102];
z[105] = z[40] * z[73];
z[107] = z[38] * z[75];
z[107] = z[105] + -z[107];
z[110] = T(6) * z[46] + (T(7) * z[90]) / T(3);
z[110] = z[42] * z[110];
z[133] = T(4) * z[47] + T(-7) * z[49];
z[110] = z[110] + T(2) * z[133];
z[133] = z[74] * z[116];
z[133] = z[133] / T(2);
z[135] = z[7] * z[117];
z[89] = -z[89] + -z[135] / T(4);
z[89] = z[7] * z[89];
z[135] = (T(3) * z[36]) / T(2);
z[147] = -(z[78] * z[135]);
z[61] = z[61] + z[69] + z[89] + z[102] + (T(3) * z[107]) / T(2) + z[110] + z[133] + -z[144] + z[147];
z[61] = z[12] * z[61];
z[69] = (T(8) * z[25]) / T(3) + (T(4) * z[26]) / T(3);
z[76] = (T(-41) * z[2]) / T(12) + (T(-11) * z[18]) / T(12) + (T(-11) * z[42]) / T(3) + (T(10) * z[45]) / T(3) + -z[69] + z[76] + -z[94] + (T(25) * z[100]) / T(12);
z[76] = z[15] * z[76];
z[89] = (T(3) * z[3]) / T(2) + (T(-5) * z[4]) / T(2) + z[123];
z[94] = z[26] + -z[42] + z[89];
z[94] = z[25] + z[45] + z[94] / T(2);
z[94] = z[14] * z[94];
z[100] = (T(19) * z[42]) / T(6);
z[72] = -z[72] + (T(-11) * z[74]) / T(3) + -z[100];
z[72] = -z[45] / T(3) + z[72] / T(2);
z[72] = z[12] * z[72];
z[102] = z[134] / T(4);
z[107] = (T(2) * z[45]) / T(3);
z[100] = -z[100] + -z[107];
z[100] = z[10] * z[100];
z[77] = (T(-17) * z[42]) / T(12) + (T(23) * z[45]) / T(6) + (T(-2) * z[74]) / T(3) + z[77] / T(2);
z[77] = z[16] * z[77];
z[123] = z[34] * z[75];
z[72] = z[72] + z[76] + z[77] + z[94] + z[100] + -z[102] + -z[123] / T(4) + T(3) * z[139];
z[72] = z[72] * z[120];
z[76] = z[68] * z[81];
z[76] = z[52] + z[76];
z[77] = -z[76] + z[97] + z[109] + -z[114];
z[77] = z[1] * z[77];
z[57] = (T(-7) * z[5]) / T(2) + (T(5) * z[18]) / T(2) + z[57];
z[54] = (T(-7) * z[3]) / T(2) + (T(9) * z[4]) / T(2) + -z[54] + z[57] + -z[67];
z[54] = z[41] * z[54];
z[67] = z[67] + z[89];
z[81] = z[40] * z[67];
z[89] = T(9) * z[46] + (T(13) * z[90]) / T(6);
z[89] = z[42] * z[89];
z[94] = (T(8) * z[140]) / T(3);
z[54] = T(2) * z[48] + -z[54] + z[77] + T(3) * z[81] + z[89] + -z[94];
z[66] = z[28] * z[66];
z[65] = z[65] + z[66] + -z[80];
z[65] = z[28] * z[65];
z[52] = z[52] + z[112];
z[52] = z[7] * z[52];
z[56] = -z[26] + -z[56] + z[57] + z[87];
z[56] = -z[25] + z[56] / T(2);
z[56] = z[27] * z[56];
z[56] = z[56] + T(2) * z[119];
z[56] = z[27] * z[56];
z[50] = -(z[38] * z[50]);
z[57] = (T(25) * z[42]) / T(12);
z[63] = (T(-31) * z[3]) / T(12) + (T(5) * z[4]) / T(4) + (T(35) * z[5]) / T(12) + z[17] / T(4) + (T(-11) * z[45]) / T(3) + z[57] + (T(-19) * z[63]) / T(12) + z[69];
z[63] = z[63] * z[120];
z[66] = -z[26] + z[115];
z[66] = -z[5] + -z[25] + z[66] / T(2) + z[104];
z[66] = z[66] * z[116];
z[69] = z[75] * z[135];
z[77] = T(5) * z[47] + (T(-37) * z[49]) / T(3);
z[50] = z[50] + z[52] + z[54] + z[56] + z[63] + z[65] + z[66] + z[69] + T(2) * z[77];
z[50] = z[11] * z[50];
z[52] = z[22] + z[121];
z[56] = z[1] / T(2);
z[52] = z[52] * z[56];
z[63] = z[7] * z[121];
z[63] = z[63] + -z[132];
z[65] = z[27] * z[121];
z[66] = -z[20] + z[99];
z[69] = z[28] * z[66];
z[77] = z[29] * z[30];
z[52] = z[52] + -z[63] + -z[65] + -z[69] + z[77];
z[52] = z[1] * z[52];
z[65] = z[21] + z[128];
z[69] = z[65] * z[85];
z[89] = z[7] * z[128];
z[66] = z[27] * z[66];
z[66] = z[66] + -z[69] + z[89] + z[132];
z[66] = z[28] * z[66];
z[69] = z[88] * z[130];
z[69] = z[69] + -z[77] + -z[132];
z[69] = z[7] * z[69];
z[77] = z[27] / T(2);
z[89] = z[77] * z[124];
z[63] = -z[63] + z[89];
z[63] = z[27] * z[63];
z[89] = z[38] * z[124];
z[97] = z[40] * z[127];
z[99] = z[41] * z[138];
z[65] = z[36] * z[65];
z[52] = -z[52] + z[63] + z[65] + -z[66] + -z[69] + z[89] + z[97] + -z[99];
z[63] = z[30] + -z[32];
z[63] = z[63] / T(2) + -z[121];
z[63] = z[63] * z[120];
z[52] = T(3) * z[52] + -z[63];
z[63] = z[126] / T(2);
z[52] = z[52] * z[63];
z[63] = z[27] * z[68];
z[63] = z[63] + z[76];
z[63] = z[27] * z[63];
z[65] = -z[98] + z[108];
z[65] = z[65] * z[84];
z[65] = z[65] + -z[80] + z[82];
z[65] = z[28] * z[65];
z[66] = -(z[29] * z[117]);
z[68] = T(-11) * z[74] + z[83];
z[69] = z[68] * z[88];
z[66] = z[66] + z[69];
z[66] = z[66] * z[88];
z[69] = (T(31) * z[2]) / T(6) + (T(7) * z[4]) / T(2) + (T(-37) * z[5]) / T(6) + -z[26] + z[86] + -z[87] + -z[122];
z[57] = -z[25] + z[57] + z[69] / T(2) + -z[107];
z[57] = z[57] * z[120];
z[69] = -(z[129] * z[135]);
z[54] = T(10) * z[47] + (T(-23) * z[49]) / T(2) + z[54] + z[57] + z[63] + z[65] + z[66] + z[69] + -z[133];
z[54] = z[9] * z[54];
z[57] = z[68] * z[98];
z[57] = z[57] + z[82] + -z[103] + -z[118];
z[57] = z[28] * z[57];
z[51] = -(z[1] * z[51]);
z[51] = z[51] + -z[70] + z[79];
z[51] = z[1] * z[51];
z[63] = (T(-5) * z[90]) / T(6) + -z[125];
z[63] = z[42] * z[63];
z[65] = -z[29] + z[88];
z[66] = -(z[65] * z[91]);
z[51] = T(-8) * z[47] + (T(39) * z[49]) / T(4) + z[51] + z[57] + z[63] + z[66] + -z[106] + -z[133] + (T(10) * z[140]) / T(3);
z[51] = z[16] * z[51];
z[57] = -z[59] + z[64] + z[101];
z[57] = z[29] * z[57];
z[57] = z[57] + -z[60];
z[59] = z[7] + z[27];
z[60] = z[59] * z[111];
z[60] = -z[57] + z[60];
z[60] = z[10] * z[60];
z[63] = z[67] * z[137];
z[64] = z[63] + z[141];
z[66] = -(z[1] * z[64]);
z[68] = z[28] * z[113];
z[59] = z[59] * z[63];
z[59] = z[59] + z[60] + z[66] + z[68];
z[59] = z[1] * z[59];
z[60] = z[28] * z[95];
z[63] = z[26] + z[92] + -z[93];
z[63] = z[25] + z[63] / T(2);
z[66] = -(z[1] * z[63]);
z[60] = z[60] + z[66];
z[60] = z[1] * z[60];
z[66] = -(z[27] * z[28]);
z[66] = z[38] + -z[41] + z[66];
z[66] = z[66] * z[96];
z[68] = prod_pow(z[27], 2);
z[68] = T(3) * z[68];
z[63] = z[63] * z[68];
z[69] = -(z[62] * z[148]);
z[62] = (T(-7) * z[45]) / T(2) + z[62];
z[62] = z[62] * z[120];
z[60] = (T(3) * z[48]) / T(2) + (T(17) * z[49]) / T(4) + T(3) * z[60] + z[62] + z[63] + z[66] + z[69] + T(-2) * z[140];
z[60] = z[13] * z[60];
z[62] = z[9] + -z[12];
z[62] = z[62] * z[78];
z[63] = -(z[126] * z[130]);
z[66] = -(z[15] * z[73]);
z[62] = z[62] + z[63] + z[66] + -z[131];
z[62] = z[35] * z[62];
z[63] = z[7] * z[65];
z[56] = z[29] + -z[56];
z[56] = z[1] * z[56];
z[56] = z[56] + z[63];
z[56] = z[56] * z[73];
z[56] = z[56] + -z[105];
z[56] = z[8] * z[56];
z[56] = z[56] + z[62];
z[62] = -z[7] + z[27];
z[62] = z[62] * z[111];
z[57] = z[57] + z[62];
z[57] = z[27] * z[57];
z[57] = z[57] + -z[94] + T(2) * z[110];
z[57] = z[10] * z[57];
z[62] = -(z[27] * z[113]);
z[63] = z[102] + z[139];
z[63] = z[28] * z[63];
z[65] = -(z[7] * z[146]);
z[62] = z[62] + T(3) * z[63] + z[65];
z[62] = z[28] * z[62];
z[63] = -(z[7] * z[27] * z[67]);
z[65] = (T(-3) * z[90]) / T(2) + -z[145];
z[65] = z[42] * z[65];
z[63] = T(-6) * z[47] + (T(17) * z[49]) / T(2) + z[63] + z[65] + -z[81];
z[63] = T(3) * z[63] + T(4) * z[140];
z[63] = z[14] * z[63];
z[65] = -(z[12] * z[111]);
z[55] = -(z[16] * z[55]);
z[55] = z[55] + -z[64] + z[65];
z[55] = z[41] * z[55];
z[64] = z[75] * z[77];
z[64] = z[64] + -z[142];
z[64] = z[27] * z[64];
z[65] = -(z[75] * z[85]);
z[65] = z[65] + z[142];
z[65] = z[28] * z[65];
z[66] = -z[36] + z[38];
z[66] = z[66] * z[75];
z[64] = z[64] + z[65] + z[66];
z[64] = z[64] * z[143];
z[65] = z[16] * z[78];
z[65] = z[65] + z[134];
z[65] = z[65] * z[135];
z[66] = prod_pow(z[7], 2);
z[67] = prod_pow(z[1], 2);
z[67] = -z[66] + z[67];
z[67] = z[67] * z[71];
z[68] = -(z[68] * z[139]);
z[66] = z[66] * z[134];
z[69] = T(2) * z[10] + z[12] + -z[15] + -z[136] + -z[137];
z[69] = z[48] * z[69];
z[70] = z[6] * z[43] * z[138];
return z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + (T(3) * z[56]) / T(2) + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + (T(3) * z[66]) / T(4) + T(3) * z[67] + z[68] + z[69] + z[70] + z[72];
}



template IntegrandConstructorType<double> f_4_127_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_127_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_127_construct (const Kin<qd_real>&);
#endif

}