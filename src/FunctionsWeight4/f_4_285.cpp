#include "f_4_285.h"

namespace PentagonFunctions {

template <typename T> T f_4_285_abbreviated (const std::array<T,58>&);

template <typename T> class SpDLog_f_4_285_W_12 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_285_W_12 (const Kin<T>& kin) {
        c[0] = rlog(kin.v[2]) * (kin.v[1] * (T(6) + T(3) * kin.v[1]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[1] * (T(6) + T(3) * kin.v[1]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4])) + rlog(-kin.v[1]) * (kin.v[1] * (T(-6) + T(-3) * kin.v[1]) + kin.v[4] * (T(6) + T(3) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[1] * (T(-6) + T(-3) * kin.v[1]) + kin.v[4] * (T(6) + T(3) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(3) + prod_pow(abb[1], 2) * (abb[3] * T(-3) + abb[6] * T(-3) + abb[4] * T(3) + abb[5] * T(3)) + prod_pow(abb[2], 2) * (abb[4] * T(-3) + abb[5] * T(-3) + abb[6] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_285_W_22 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_285_W_22 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[1] * (T(-6) + T(6) * kin.v[0] + T(3) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[0] * (T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(kin.v[2]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[1] * (T(6) + T(-6) * kin.v[0] + T(-3) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[0] * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[7] * (prod_pow(abb[9], 2) * abb[10] * T(-3) + abb[4] * prod_pow(abb[9], 2) * T(3) + prod_pow(abb[8], 2) * (abb[4] * T(-3) + abb[10] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_285_W_21 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_285_W_21 (const Kin<T>& kin) {
        c[0] = rlog(kin.v[2]) * (kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(6) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) + T(3) * kin.v[1] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[3] * (T(-6) + T(3) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(6) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) + T(3) * kin.v[1] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[3] * (T(-6) + T(3) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(6) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) + T(3) * kin.v[1] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[3] * (T(-6) + T(3) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[1]) * (kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[3] * (T(6) + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) + T(-3) * kin.v[1] + T(6) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[3] * (T(6) + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) + T(-3) * kin.v[1] + T(6) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[3] * (T(6) + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) + T(-3) * kin.v[1] + T(6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[11] * (abb[3] * prod_pow(abb[9], 2) * T(3) + prod_pow(abb[12], 2) * (abb[3] * T(-3) + abb[13] * T(-3) + abb[15] * T(-3) + abb[4] * T(3) + abb[10] * T(3) + abb[14] * T(3)) + prod_pow(abb[9], 2) * (abb[4] * T(-3) + abb[10] * T(-3) + abb[14] * T(-3) + abb[13] * T(3) + abb[15] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_285_W_10 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_285_W_10 (const Kin<T>& kin) {
        c[0] = T(4) * kin.v[0] * kin.v[2] + kin.v[1] * (T(4) * kin.v[0] + T(-2) * kin.v[1] + T(-8) * kin.v[2] + T(-8) * kin.v[3]) + kin.v[2] * (T(-6) * kin.v[2] + T(-8) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(-4) + T(2) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[4]));
c[1] = kin.v[0] * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[2] + T(4) * kin.v[2]) + kin.v[2] * (T(-4) * kin.v[2] + T(-8) * kin.v[3]) + kin.v[1] * (T(-4) * kin.v[2] + T(-8) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(-4) * kin.v[3])) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-2) * kin.v[2] + T(-4) * kin.v[3]) + rlog(-kin.v[1]) * (T(2) * kin.v[0] * kin.v[2] + kin.v[2] * (T(-6) + T(-4) * kin.v[3] + T(-6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(-6) + T(2) * kin.v[0] + (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(2) * kin.v[2] + T(-4) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(2) * kin.v[2] + T(-2) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (T(2) * kin.v[0] * kin.v[2] + kin.v[2] * (T(-6) + T(-4) * kin.v[3] + T(-6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(-6) + T(2) * kin.v[0] + (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(2) * kin.v[2] + T(-4) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(2) * kin.v[2] + T(-2) * kin.v[4]))) + rlog(kin.v[2]) * (kin.v[1] * (T(6) + T(-3) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (T(4) * kin.v[0] * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[2] * (T(6) + T(-9) * kin.v[2] + T(-8) * kin.v[3] + T(6) * kin.v[4]) + kin.v[1] * (T(6) + T(4) * kin.v[0] + (T(-5) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[1] + T(-14) * kin.v[2] + T(-8) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(4) * kin.v[2] + T(-4) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (T(-2) * kin.v[0] * kin.v[2] + kin.v[2] * (T(-6) + T(6) * kin.v[2] + T(4) * kin.v[3] + T(-6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-6) + T(-2) * kin.v[0] + (bc<T>[0] * int_to_imaginary<T>(-1) + T(4)) * kin.v[1] + T(10) * kin.v[2] + T(4) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (T(-2) * kin.v[0] * kin.v[2] + kin.v[2] * (T(3) * kin.v[2] + T(4) * kin.v[3]) + kin.v[1] * (T(-2) * kin.v[0] + kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4]))) + rlog(-kin.v[4]) * (T(-4) * kin.v[0] * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[2] * (T(6) + T(3) * kin.v[2] + T(8) * kin.v[3] + T(6) * kin.v[4]) + kin.v[1] * (T(6) + T(-4) * kin.v[0] + (T(-1) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(2) * kin.v[2] + T(8) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-4) * kin.v[2] + T(4) * kin.v[4])));
c[2] = (T(-8) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(-8) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[2];
c[3] = rlog(-kin.v[1]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[2]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[2]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[2] + T(4) * kin.v[2]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[2] + T(4) * kin.v[2]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-8) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(-8) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[2]) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(4) + T(8)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[2] + T(8) * kin.v[2]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[29] * (abb[3] * (abb[22] * (abb[22] + abb[12] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[30] * T(2)) + abb[24] * T(4)) + abb[1] * (abb[3] * abb[22] * T(2) + abb[22] * (abb[5] * T(-4) + abb[27] * T(-4) + abb[10] * T(-2) + abb[13] * T(-2) + abb[6] * T(2) + abb[14] * T(4)) + abb[2] * (abb[14] * T(-4) + abb[3] * T(-2) + abb[6] * T(-2) + abb[10] * T(2) + abb[13] * T(2) + abb[5] * T(4) + abb[27] * T(4))) + abb[22] * (abb[6] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[10] * bc<T>[0] * int_to_imaginary<T>(2) + abb[13] * bc<T>[0] * int_to_imaginary<T>(2) + abb[14] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[5] * bc<T>[0] * int_to_imaginary<T>(4) + abb[27] * bc<T>[0] * int_to_imaginary<T>(4) + abb[30] * (abb[5] * T(-4) + abb[27] * T(-4) + abb[10] * T(-2) + abb[13] * T(-2) + abb[6] * T(2) + abb[14] * T(4)) + abb[12] * (abb[14] * T(-4) + abb[6] * T(-2) + abb[10] * T(2) + abb[13] * T(2) + abb[5] * T(4) + abb[27] * T(4)) + abb[22] * (abb[5] + abb[6] + abb[14] * T(-7) + abb[4] * T(-3) + abb[10] * T(2) + abb[27] * T(4) + abb[13] * T(5))) + abb[24] * (abb[5] * T(-8) + abb[27] * T(-8) + abb[10] * T(-4) + abb[13] * T(-4) + abb[6] * T(4) + abb[14] * T(8)) + abb[2] * (abb[10] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[13] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[6] * bc<T>[0] * int_to_imaginary<T>(2) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[27] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[14] * bc<T>[0] * int_to_imaginary<T>(4) + abb[3] * (abb[22] * T(-2) + abb[30] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2) + abb[12] * T(2)) + abb[12] * (abb[5] * T(-4) + abb[27] * T(-4) + abb[10] * T(-2) + abb[13] * T(-2) + abb[6] * T(2) + abb[14] * T(4)) + abb[22] * (abb[14] * T(-4) + abb[6] * T(-2) + abb[10] * T(2) + abb[13] * T(2) + abb[5] * T(4) + abb[27] * T(4)) + abb[30] * (abb[14] * T(-4) + abb[6] * T(-2) + abb[10] * T(2) + abb[13] * T(2) + abb[5] * T(4) + abb[27] * T(4)) + abb[2] * (abb[3] + abb[6] + abb[27] * T(-8) + abb[13] * T(-7) + abb[5] * T(-5) + abb[10] * T(-4) + abb[4] * T(3) + abb[14] * T(11))));
    }
};
template <typename T> class SpDLog_f_4_285_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_285_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (T(-12) * kin.v[0] + T(32) * kin.v[2] + T(24) * kin.v[3] + T(-16) * kin.v[4]) + T(-4) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(-12) * kin.v[3] + T(16) * kin.v[4]) + kin.v[2] * (T(-20) * kin.v[2] + T(-32) * kin.v[3] + T(24) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-8) + T(-4) * kin.v[3]) + kin.v[0] * (T(8) + T(-4) * kin.v[0] + T(-8) * kin.v[1] + T(8) * kin.v[3]) + kin.v[2] * (T(-8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[1] * (T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[4] * (T(8) + T(4) * kin.v[4]));
c[1] = kin.v[3] * (T(-2) * kin.v[3] + T(-4) * kin.v[4]) + T(6) * prod_pow(kin.v[4], 2) + kin.v[2] * (T(-10) * kin.v[2] + T(-12) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * prod_pow(kin.v[4], 2) + kin.v[3] * (-kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (T(-5) * kin.v[2] + T(-6) * kin.v[3] + T(2) * kin.v[4])) + kin.v[0] * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(12) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(6) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (((T(-5) * kin.v[4]) / T(2) + T(-6)) * kin.v[4] + kin.v[3] * ((T(9) * kin.v[3]) / T(2) + T(6) + T(-2) * kin.v[4]) + kin.v[0] * (T(-6) + (T(9) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(6) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + T(-1) + kin.v[1]) + T(-4) * kin.v[2] + T(-9) * kin.v[3] + T(2) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(6) + T(4) * kin.v[3] + T(3) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(-kin.v[4]) * (((T(-5) * kin.v[4]) / T(2) + T(-6)) * kin.v[4] + kin.v[3] * ((T(9) * kin.v[3]) / T(2) + T(6) + T(-2) * kin.v[4]) + kin.v[0] * (T(-6) + (T(9) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(6) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + T(-1) + kin.v[1]) + T(-4) * kin.v[2] + T(-9) * kin.v[3] + T(2) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(6) + T(4) * kin.v[3] + T(3) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (((T(-5) * kin.v[4]) / T(2) + T(-6)) * kin.v[4] + kin.v[3] * ((T(9) * kin.v[3]) / T(2) + T(6) + T(-2) * kin.v[4]) + kin.v[0] * (T(-6) + (T(9) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(6) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + T(-1) + kin.v[1]) + T(-4) * kin.v[2] + T(-9) * kin.v[3] + T(2) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(6) + T(4) * kin.v[3] + T(3) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((kin.v[4] / T(2) + T(6)) * kin.v[4] + kin.v[0] * (T(6) + (T(-21) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-5) / T(2))) * kin.v[0] + T(-6) * kin.v[1] + T(20) * kin.v[2] + T(21) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(5) + T(-5) * kin.v[1] + T(5) * kin.v[3]) + T(-10) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * ((T(-19) * kin.v[2]) / T(2) + T(-6) + T(-20) * kin.v[3] + T(9) * kin.v[4]) + kin.v[3] * ((T(-21) * kin.v[3]) / T(2) + T(-6) + T(10) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-5) * kin.v[3]) / T(2) + T(-5)) * kin.v[3] + ((T(5) * kin.v[4]) / T(2) + T(5)) * kin.v[4] + kin.v[2] * ((T(5) * kin.v[2]) / T(2) + T(-5) + T(-5) * kin.v[4]) + kin.v[1] * (T(5) * kin.v[2] + T(5) * kin.v[3] + T(-5) * kin.v[4]))) + rlog(kin.v[3]) * ((kin.v[4] / T(2) + T(6)) * kin.v[4] + kin.v[0] * (T(6) + (T(-21) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-5) / T(2))) * kin.v[0] + T(-6) * kin.v[1] + T(20) * kin.v[2] + T(21) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(5) + T(-5) * kin.v[1] + T(5) * kin.v[3]) + T(-10) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * ((T(-19) * kin.v[2]) / T(2) + T(-6) + T(-20) * kin.v[3] + T(9) * kin.v[4]) + kin.v[3] * ((T(-21) * kin.v[3]) / T(2) + T(-6) + T(10) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-5) * kin.v[3]) / T(2) + T(-5)) * kin.v[3] + ((T(5) * kin.v[4]) / T(2) + T(5)) * kin.v[4] + kin.v[2] * ((T(5) * kin.v[2]) / T(2) + T(-5) + T(-5) * kin.v[4]) + kin.v[1] * (T(5) * kin.v[2] + T(5) * kin.v[3] + T(-5) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (((T(9) * kin.v[4]) / T(2) + T(6)) * kin.v[4] + kin.v[2] * ((T(21) * kin.v[2]) / T(2) + T(-6) + T(12) * kin.v[3] + T(-15) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-6) + T(-6) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(6) + (T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[0] + T(-6) * kin.v[1] + T(-12) * kin.v[2] + T(-3) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3]) + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (T(2) * prod_pow(kin.v[4], 2) + kin.v[2] * (T(10) * kin.v[2] + T(16) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[3] * (T(6) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[0] * (T(6) * kin.v[0] + T(-16) * kin.v[2] + T(-12) * kin.v[3] + T(8) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (T(-4) + T(2) * kin.v[0] + T(4) * kin.v[1] + T(-4) * kin.v[3]) + kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[4] * (T(-4) + T(-2) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(8) + T(16)) * kin.v[0] + T(-16) * kin.v[2] + T(-16) * kin.v[3] + T(16) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4]);
c[3] = rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-8) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + rlog(-kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((bc<T>[0] * int_to_imaginary<T>(5) + T(10)) * kin.v[0] + T(-10) * kin.v[2] + T(-10) * kin.v[3] + T(10) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-5) * kin.v[2] + T(-5) * kin.v[3] + T(5) * kin.v[4])) + rlog(kin.v[3]) * ((bc<T>[0] * int_to_imaginary<T>(5) + T(10)) * kin.v[0] + T(-10) * kin.v[2] + T(-10) * kin.v[3] + T(10) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-5) * kin.v[2] + T(-5) * kin.v[3] + T(5) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[37] * (abb[8] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[15] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[13] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[14] * bc<T>[0] * int_to_imaginary<T>(5) + abb[39] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[28] * (abb[9] * T(-5) + abb[22] * T(-5) + abb[30] * T(-5) + bc<T>[0] * int_to_imaginary<T>(5) + abb[12] * T(5)) + abb[12] * (-abb[5] + -abb[6] + -abb[15] + abb[39] * T(-8) + abb[10] * T(-4) + abb[13] * T(-3) + abb[14] * T(5)) + abb[8] * (abb[5] + abb[6] + abb[15] + abb[39] * T(-16) + abb[13] * T(-9) + abb[10] * T(-8) + abb[14] * T(7) + abb[28] * T(7)) + abb[9] * (abb[5] + abb[6] + abb[15] + abb[14] * T(-5) + abb[13] * T(3) + abb[10] * T(4) + abb[39] * T(8)) + abb[22] * (abb[5] + abb[6] + abb[15] + abb[14] * T(-5) + abb[13] * T(3) + abb[10] * T(4) + abb[39] * T(8)) + abb[30] * (abb[5] + abb[6] + abb[15] + abb[14] * T(-5) + abb[13] * T(3) + abb[10] * T(4) + abb[39] * T(8))) + abb[22] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(1) + abb[15] * bc<T>[0] * int_to_imaginary<T>(1) + abb[13] * bc<T>[0] * int_to_imaginary<T>(3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(4) + abb[14] * bc<T>[0] * int_to_imaginary<T>(-5) + abb[39] * bc<T>[0] * int_to_imaginary<T>(8) + abb[9] * (-abb[5] + -abb[6] + -abb[15] + abb[39] * T(-8) + abb[10] * T(-4) + abb[13] * T(-3) + abb[14] * T(5)) + abb[30] * (-abb[5] + -abb[6] + -abb[15] + abb[39] * T(-8) + abb[10] * T(-4) + abb[13] * T(-3) + abb[14] * T(5)) + abb[12] * (abb[5] + abb[6] + abb[15] + abb[14] * T(-5) + abb[13] * T(3) + abb[10] * T(4) + abb[39] * T(8)) + abb[22] * (abb[5] * T(-2) + abb[6] * T(-2) + abb[14] * T(-2) + abb[15] * T(-2) + abb[10] * T(4) + abb[13] * T(6) + abb[39] * T(8))) + abb[38] * (abb[39] * T(-16) + abb[10] * T(-8) + abb[13] * T(-6) + abb[5] * T(-2) + abb[6] * T(-2) + abb[15] * T(-2) + abb[14] * T(10)) + abb[28] * (abb[22] * (abb[12] * T(-5) + abb[22] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-5) + abb[9] * T(5) + abb[30] * T(5)) + abb[38] * T(10)));
    }
};
template <typename T> class SpDLog_f_4_285_W_7 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_285_W_7 (const Kin<T>& kin) {
        c[0] = (T(-3) / T(2) + (T(-3) * kin.v[1]) / T(2) + (T(3) * kin.v[3]) / T(8) + (T(3) * kin.v[4]) / T(4)) * kin.v[3] + (T(-3) * kin.v[1] * kin.v[4]) / T(2) + (T(-3) / T(2) + (T(3) * kin.v[4]) / T(8)) * kin.v[4];
c[1] = (T(-3) * kin.v[1] * kin.v[4]) / T(2) + (T(-3) / T(2) + (T(3) * kin.v[4]) / T(2)) * kin.v[4] + kin.v[3] * (T(-3) / T(2) + (T(-3) * kin.v[1]) / T(2) + (T(3) * kin.v[3]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[1]) + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[4] + T(-8) * kin.v[1] * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (T(6) * kin.v[1] * kin.v[4] + kin.v[3] * (T(6) + T(6) * kin.v[1] + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4])) + rlog(-kin.v[1]) * (((T(-9) * kin.v[3]) / T(4) + (T(-9) * kin.v[4]) / T(2) + T(3) + T(3) * kin.v[1]) * kin.v[3] + ((T(-9) * kin.v[4]) / T(4) + T(3)) * kin.v[4] + T(3) * kin.v[1] * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (T(-6) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[3] * (T(-6) + T(-6) * kin.v[1] + T(3) * kin.v[3] + T(6) * kin.v[4])) + rlog(kin.v[3]) * (-(kin.v[1] * kin.v[4]) + ((T(31) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[3] * ((T(31) * kin.v[4]) / T(2) + -kin.v[1] + T(-1) + (T(31) / T(4) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(8) * kin.v[1] + T(-8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[1] * kin.v[4] + kin.v[4] * (T(8) + T(-4) * kin.v[4]))) + rlog(-kin.v[4]) * (((T(-31) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-8) + T(4) * kin.v[4])) + kin.v[3] * ((T(-31) * kin.v[4]) / T(2) + T(1) + kin.v[1] + (T(-31) / T(4) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[1] + T(8) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-19) * kin.v[1] * kin.v[4]) / T(2) + (T(-19) / T(2) + (T(-17) * kin.v[4]) / T(8)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-8) + T(4) * kin.v[4])) + kin.v[3] * (T(-19) / T(2) + (T(-19) * kin.v[1]) / T(2) + (T(-17) * kin.v[4]) / T(4) + (T(-17) / T(8) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[1] + T(8) * kin.v[4])));
c[2] = (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2);
c[3] = rlog(-kin.v[1]) * (T(3) * kin.v[3] + T(3) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-9) * kin.v[4]) / T(2) + (T(-9) / T(2) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[4]) + rlog(-kin.v[4]) * ((T(-3) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[4]) + rlog(kin.v[3]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(3)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[4] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[40] * (abb[3] * ((prod_pow(abb[12], 2) * T(-3)) / T(2) + abb[41] * T(-3)) + abb[41] * ((abb[42] * T(3)) / T(2) + (abb[10] * T(9)) / T(2) + abb[28] * T(-3) + abb[5] * T(3)) + abb[12] * (abb[9] * (abb[5] * T(-8) + abb[10] * T(-8)) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[12] * (abb[28] / T(2) + -abb[5] / T(2) + (abb[42] * T(-3)) / T(4) + (abb[10] * T(19)) / T(4) + abb[15] * T(-3) + abb[14] * T(3)) + abb[28] * (abb[30] * T(-8) + bc<T>[0] * int_to_imaginary<T>(8) + abb[9] * T(8)) + abb[30] * (abb[5] * T(8) + abb[10] * T(8))) + abb[1] * (abb[30] * (abb[5] * T(-8) + abb[10] * T(-8)) + abb[5] * bc<T>[0] * int_to_imaginary<T>(8) + abb[10] * bc<T>[0] * int_to_imaginary<T>(8) + abb[3] * abb[12] * T(3) + abb[1] * (-abb[10] / T(4) + (abb[28] * T(-7)) / T(2) + (abb[3] * T(-3)) / T(2) + (abb[5] * T(7)) / T(2) + (abb[42] * T(9)) / T(4) + abb[14] * T(-3) + abb[15] * T(3)) + abb[12] * ((abb[10] * T(-9)) / T(2) + (abb[42] * T(-3)) / T(2) + abb[5] * T(-3) + abb[28] * T(3)) + abb[9] * (abb[5] * T(8) + abb[10] * T(8)) + abb[28] * (abb[9] * T(-8) + bc<T>[0] * int_to_imaginary<T>(-8) + abb[30] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_285_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl22 = DLog_W_22<T>(kin),dl21 = DLog_W_21<T>(kin),dl31 = DLog_W_31<T>(kin),dl3 = DLog_W_3<T>(kin),dl10 = DLog_W_10<T>(kin),dl24 = DLog_W_24<T>(kin),dl1 = DLog_W_1<T>(kin),dl23 = DLog_W_23<T>(kin),dl7 = DLog_W_7<T>(kin),dl29 = DLog_W_29<T>(kin),dl26 = DLog_W_26<T>(kin),dl28 = DLog_W_28<T>(kin),dl27 = DLog_W_27<T>(kin),dl30 = DLog_W_30<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl19 = DLog_W_19<T>(kin),dl17 = DLog_W_17<T>(kin),dl2 = DLog_W_2<T>(kin),dl16 = DLog_W_16<T>(kin),dl4 = DLog_W_4<T>(kin),dl18 = DLog_W_18<T>(kin),spdl12 = SpDLog_f_4_285_W_12<T>(kin),spdl22 = SpDLog_f_4_285_W_22<T>(kin),spdl21 = SpDLog_f_4_285_W_21<T>(kin),spdl10 = SpDLog_f_4_285_W_10<T>(kin),spdl23 = SpDLog_f_4_285_W_23<T>(kin),spdl7 = SpDLog_f_4_285_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,58> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[16] / kin_path.W[16]), dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[18] / kin_path.W[18]), dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[19] / kin_path.W[19]), dl31(t), f_2_2_7(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl3(t), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_4(kin_path), f_2_1_7(kin_path), f_2_1_14(kin_path), f_2_1_15(kin_path), -rlog(t), rlog(kin.W[3] / kin_path.W[3]), dl10(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl24(t), dl1(t), f_2_1_10(kin_path), f_2_1_12(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl23(t), f_2_1_8(kin_path), -rlog(t), dl7(t), f_2_1_11(kin_path), -rlog(t), dl29(t) / kin_path.SqrtDelta, f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl26(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl5(t), dl20(t), dl19(t), dl17(t), dl2(t), dl16(t), dl4(t), dl18(t)}
;

        auto result = f_4_285_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr) + spdl21(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_285_abbreviated(const std::array<T,58>& abb)
{
using TR = typename T::value_type;
T z[246];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[21];
z[3] = abb[50];
z[4] = abb[51];
z[5] = abb[52];
z[6] = abb[53];
z[7] = abb[54];
z[8] = abb[56];
z[9] = abb[57];
z[10] = abb[4];
z[11] = abb[5];
z[12] = abb[6];
z[13] = abb[10];
z[14] = abb[13];
z[15] = abb[14];
z[16] = abb[15];
z[17] = abb[18];
z[18] = abb[43];
z[19] = abb[46];
z[20] = abb[47];
z[21] = abb[48];
z[22] = abb[19];
z[23] = abb[20];
z[24] = abb[27];
z[25] = abb[28];
z[26] = abb[42];
z[27] = abb[44];
z[28] = abb[2];
z[29] = abb[55];
z[30] = abb[9];
z[31] = abb[12];
z[32] = abb[22];
z[33] = abb[30];
z[34] = bc<TR>[0];
z[35] = abb[49];
z[36] = abb[8];
z[37] = abb[32];
z[38] = abb[23];
z[39] = abb[24];
z[40] = abb[26];
z[41] = abb[33];
z[42] = abb[34];
z[43] = abb[35];
z[44] = abb[36];
z[45] = abb[41];
z[46] = abb[25];
z[47] = abb[38];
z[48] = abb[17];
z[49] = abb[31];
z[50] = abb[39];
z[51] = abb[45];
z[52] = abb[16];
z[53] = bc<TR>[3];
z[54] = bc<TR>[1];
z[55] = bc<TR>[5];
z[56] = bc<TR>[2];
z[57] = bc<TR>[4];
z[58] = bc<TR>[9];
z[59] = z[13] + -z[15];
z[60] = z[24] / T(3);
z[61] = z[14] / T(4);
z[62] = T(2) * z[50];
z[63] = -z[12] + (T(-5) * z[16]) / T(4) + z[62];
z[59] = (T(-5) * z[1]) / T(12) + z[10] + (T(-11) * z[11]) / T(12) + -z[59] / T(6) + z[60] + -z[61] + z[63] / T(3);
z[59] = z[6] * z[59];
z[63] = z[15] / T(4);
z[64] = z[1] / T(2);
z[65] = z[16] / T(4);
z[66] = z[63] + z[64] + -z[65];
z[67] = z[12] / T(4);
z[68] = z[66] + z[67];
z[69] = z[25] / T(4);
z[70] = z[61] + z[69];
z[71] = (T(9) * z[11]) / T(4) + -z[62] + -z[68] + z[70];
z[71] = z[8] * z[71];
z[72] = z[12] + z[14];
z[73] = z[16] + z[72];
z[74] = -z[15] + z[73];
z[75] = z[11] + -z[25];
z[76] = T(2) * z[1];
z[77] = -z[74] + z[75] + z[76];
z[77] = z[37] * z[77];
z[78] = z[25] / T(2);
z[79] = -z[16] + T(-7) * z[26];
z[79] = (T(7) * z[1]) / T(12) + -z[10] + z[11] / T(12) + z[12] / T(3) + (T(35) * z[13]) / T(12) + (T(5) * z[15]) / T(3) + -z[61] + -z[78] + z[79] / T(4);
z[79] = z[5] * z[79];
z[80] = T(3) * z[14];
z[81] = T(3) * z[12];
z[82] = z[16] + z[80] + z[81];
z[83] = z[15] / T(2);
z[82] = z[1] + z[82] / T(2) + -z[83];
z[84] = z[11] / T(4);
z[82] = -z[13] + -z[24] + z[69] + z[82] / T(2) + -z[84];
z[82] = z[2] * z[82];
z[85] = (T(3) * z[11]) / T(4);
z[86] = -z[26] / T(3) + z[50];
z[86] = (T(3) * z[1]) / T(4) + (T(-2) * z[12]) / T(3) + (T(7) * z[13]) / T(12) + z[14] / T(3) + z[15] / T(12) + (T(-11) * z[25]) / T(12) + z[65] + -z[85] + T(2) * z[86];
z[86] = z[4] * z[86];
z[87] = z[1] + z[12];
z[88] = T(2) * z[14];
z[60] = (T(5) * z[11]) / T(12) + (T(11) * z[13]) / T(12) + (T(47) * z[15]) / T(12) + (T(2) * z[25]) / T(3) + -z[60] + (T(-19) * z[87]) / T(12) + -z[88];
z[60] = z[3] * z[60];
z[89] = (T(5) * z[13]) / T(2);
z[90] = (T(9) * z[15]) / T(4);
z[91] = T(3) * z[16];
z[92] = (T(5) * z[12]) / T(3) + -z[14] / T(6) + (T(7) * z[26]) / T(2) + z[91];
z[92] = (T(-4) * z[1]) / T(3) + -z[10] + (T(-7) * z[11]) / T(3) + z[24] + (T(19) * z[25]) / T(12) + -z[89] + -z[90] + z[92] / T(2);
z[92] = z[7] * z[92];
z[93] = z[4] + z[6];
z[94] = z[3] + z[9];
z[95] = T(2) * z[7];
z[96] = -z[8] + -z[93] + z[94] + z[95];
z[97] = z[54] * z[96];
z[98] = (T(11) * z[12]) / T(4);
z[99] = (T(-49) * z[14]) / T(4) + (T(11) * z[16]) / T(4) + -z[62] + z[98];
z[63] = z[10] + (T(-13) * z[13]) / T(12) + -z[54] + -z[63] + (T(5) * z[75]) / T(6) + z[99] / T(3);
z[63] = z[29] * z[63];
z[99] = z[11] + z[15];
z[100] = z[72] + -z[99];
z[101] = T(3) * z[49];
z[100] = z[100] * z[101];
z[101] = (T(3) * z[51]) / T(4);
z[102] = T(-7) * z[17] + T(13) * z[23];
z[102] = -z[22] + z[102] / T(4);
z[102] = -z[27] / T(4) + z[101] + z[102] / T(3);
z[102] = z[18] * z[102];
z[103] = z[17] + -z[23];
z[103] = z[22] + (T(5) * z[103]) / T(2);
z[104] = -z[27] + z[51];
z[104] = -z[104] / T(2);
z[105] = -z[103] / T(3) + -z[104];
z[105] = z[35] * z[105];
z[106] = z[16] / T(2);
z[107] = (T(-17) * z[1]) / T(6) + (T(37) * z[11]) / T(12) + (T(-7) * z[12]) / T(3) + (T(13) * z[13]) / T(6) + z[14] / T(12) + (T(11) * z[15]) / T(4) + (T(2) * z[26]) / T(3) + z[78] + -z[106];
z[107] = z[9] * z[107];
z[108] = T(3) * z[56];
z[94] = -z[6] + z[94];
z[109] = z[29] + -z[94];
z[109] = z[108] * z[109];
z[110] = z[17] + z[23] + z[27] + z[51];
z[111] = z[110] / T(2);
z[112] = z[20] * z[111];
z[113] = z[21] * z[111];
z[114] = z[4] + z[8];
z[115] = (T(-7) * z[3]) / T(6) + -z[5] + (T(3) * z[7]) / T(2) + z[9] / T(6) + -z[18] / T(9) + z[19] / T(3) + (T(56) * z[20]) / T(135) + (T(101) * z[21]) / T(135) + (T(-11) * z[29]) / T(12) + (T(-17) * z[35]) / T(135) + -z[114] / T(4);
z[115] = int_to_imaginary<T>(1) * z[34] * z[115];
z[59] = z[59] + z[60] + z[63] + z[71] + z[77] + z[79] + z[82] + z[86] + z[92] + z[97] + z[100] + z[102] + z[105] + z[107] + z[109] + z[112] + -z[113] + z[115];
z[59] = z[34] * z[59];
z[60] = -z[76] + z[80];
z[63] = T(6) * z[13];
z[71] = T(8) * z[26];
z[77] = T(4) * z[15];
z[79] = T(4) * z[11];
z[82] = -z[12] + -z[60] + z[63] + -z[71] + z[77] + z[79];
z[82] = z[5] * z[82];
z[86] = (T(3) * z[16]) / T(2);
z[92] = (T(5) * z[15]) / T(2);
z[97] = z[86] + -z[92];
z[102] = z[14] / T(2);
z[105] = z[12] / T(2);
z[107] = z[102] + z[105];
z[109] = T(2) * z[26];
z[115] = z[97] + z[107] + z[109];
z[116] = T(4) * z[13];
z[117] = (T(5) * z[11]) / T(2);
z[118] = (T(3) * z[25]) / T(2);
z[119] = z[1] + -z[115] + z[116] + z[117] + -z[118];
z[119] = z[9] * z[119];
z[120] = -z[13] + z[109];
z[121] = (T(3) * z[11]) / T(2);
z[122] = (T(5) * z[25]) / T(2);
z[97] = z[97] + z[120] + z[121] + -z[122];
z[123] = z[97] + z[107];
z[124] = z[4] * z[123];
z[125] = T(2) * z[24];
z[126] = z[79] + -z[125];
z[127] = T(2) * z[12];
z[128] = T(4) * z[50];
z[129] = -z[13] + z[128];
z[130] = -z[16] + z[129];
z[60] = -z[15] + z[60] + z[126] + z[127] + -z[130];
z[131] = z[6] * z[60];
z[132] = z[11] / T(2);
z[133] = z[1] + z[132];
z[134] = z[83] + z[133];
z[73] = -z[73] / T(2) + -z[78] + z[134];
z[135] = z[37] * z[73];
z[136] = (T(3) * z[27]) / T(2);
z[137] = (T(3) * z[51]) / T(2) + -z[136];
z[103] = -z[103] + z[137];
z[138] = z[35] * z[103];
z[139] = (T(3) * z[110]) / T(2);
z[140] = z[21] * z[139];
z[140] = T(3) * z[135] + z[138] + -z[140];
z[141] = T(4) * z[25];
z[142] = -z[79] + z[141];
z[143] = T(3) * z[13];
z[144] = z[142] + -z[143];
z[145] = z[1] + z[72];
z[146] = -z[125] + z[145];
z[147] = z[144] + -z[146];
z[148] = z[3] * z[147];
z[149] = -z[12] + z[14];
z[150] = z[1] + z[149];
z[144] = -z[144] + z[150];
z[144] = z[7] * z[144];
z[151] = z[18] * z[103];
z[82] = -z[82] + -z[119] + -z[124] + z[131] + z[140] + -z[144] + z[148] + z[151];
z[119] = z[105] + z[106];
z[124] = z[119] + z[128];
z[144] = T(2) * z[13];
z[152] = -z[122] + z[124] + z[144];
z[153] = z[83] + z[102];
z[154] = z[121] + -z[152] + z[153];
z[155] = z[29] * z[154];
z[155] = -z[82] + z[155];
z[155] = z[31] * z[155];
z[156] = z[87] + -z[125];
z[157] = T(8) * z[11];
z[158] = T(5) * z[13];
z[159] = -z[77] + z[80] + -z[141] + -z[156] + z[157] + z[158];
z[159] = z[3] * z[159];
z[160] = z[125] + z[129];
z[161] = (T(11) * z[11]) / T(2);
z[162] = z[118] + -z[161];
z[163] = -z[83] + z[106];
z[164] = (T(3) * z[14]) / T(2);
z[165] = -z[1] + -z[105] + z[160] + z[162] + z[163] + -z[164];
z[166] = z[9] * z[165];
z[167] = T(2) * z[6];
z[168] = z[5] + z[167];
z[60] = z[60] * z[168];
z[168] = (T(7) * z[14]) / T(2);
z[169] = (T(-9) * z[15]) / T(2) + z[117] + z[152] + z[168];
z[169] = z[29] * z[169];
z[170] = z[4] * z[154];
z[170] = -z[151] + z[170];
z[171] = z[7] * z[147];
z[60] = z[60] + z[140] + z[159] + z[166] + z[169] + -z[170] + z[171];
z[140] = -(z[32] * z[60]);
z[159] = z[6] * z[154];
z[166] = z[19] * z[103];
z[159] = z[159] + z[166];
z[166] = z[159] + z[170];
z[169] = -z[92] + z[116];
z[172] = z[119] + -z[128];
z[172] = T(3) * z[172];
z[173] = z[161] + z[172];
z[174] = (T(-11) * z[14]) / T(2) + z[122] + -z[169] + z[173];
z[174] = z[3] * z[174];
z[175] = z[5] * z[154];
z[176] = -z[16] + z[128];
z[177] = -z[79] + z[176];
z[178] = z[13] + z[15] + z[149] + z[177];
z[179] = T(2) * z[9];
z[180] = z[178] * z[179];
z[103] = z[20] * z[103];
z[174] = -z[103] + z[166] + z[174] + -z[175] + z[180];
z[174] = z[36] * z[174];
z[175] = (T(5) * z[14]) / T(2);
z[180] = (T(3) * z[12]) / T(2);
z[97] = z[97] + z[175] + -z[180];
z[97] = z[7] * z[97];
z[123] = z[5] * z[123];
z[181] = z[103] + -z[138];
z[182] = T(2) * z[4];
z[183] = z[179] + z[182];
z[183] = z[149] * z[183];
z[97] = -z[97] + z[123] + z[159] + -z[181] + z[183];
z[97] = z[30] * z[97];
z[97] = -z[97] + z[174];
z[123] = z[1] + z[125];
z[129] = z[123] + z[129];
z[174] = z[78] + -z[102];
z[183] = (T(3) * z[15]) / T(2);
z[105] = (T(13) * z[11]) / T(2) + -z[105] + z[106] + -z[129] + -z[174] + z[183];
z[105] = z[9] * z[105];
z[184] = -z[78] + z[180];
z[185] = (T(9) * z[11]) / T(2);
z[129] = -z[129] + z[163] + z[175] + z[184] + z[185];
z[129] = z[6] * z[129];
z[113] = z[113] + -z[135];
z[186] = z[5] * z[73];
z[187] = z[19] * z[111];
z[112] = -z[112] + z[187];
z[187] = z[35] * z[111];
z[105] = z[105] + -z[112] + z[113] + -z[129] + z[186] + -z[187];
z[129] = z[117] + -z[128];
z[123] = z[13] + z[92] + -z[106] + -z[123] + -z[129] + z[164] + -z[184];
z[186] = -(z[3] * z[123]);
z[186] = -z[105] + z[186];
z[186] = z[43] * z[186];
z[187] = -z[106] + z[109];
z[134] = -z[78] + z[107] + -z[134] + z[187];
z[188] = z[5] * z[134];
z[111] = z[18] * z[111];
z[111] = -z[111] + z[112] + z[113];
z[112] = -z[111] + z[188];
z[112] = z[44] * z[112];
z[113] = -(z[44] * z[134]);
z[113] = -z[57] + z[113];
z[113] = z[9] * z[113];
z[133] = -z[133] + -z[153] + z[184] + z[187];
z[187] = z[44] * z[133];
z[188] = z[57] + z[187];
z[188] = z[4] * z[188];
z[189] = -(z[44] * z[73]);
z[189] = z[57] + z[189];
z[189] = z[6] * z[189];
z[190] = z[3] + -z[8];
z[190] = -(z[57] * z[190]);
z[112] = z[112] + z[113] + z[186] + z[188] + z[189] + z[190];
z[113] = z[5] * z[147];
z[186] = T(3) * z[11];
z[188] = z[144] + z[186];
z[189] = T(-3) * z[15] + z[76] + z[127] + -z[188];
z[190] = -z[14] + z[125] + z[189];
z[191] = z[179] * z[190];
z[192] = T(4) * z[17];
z[193] = T(3) * z[27];
z[194] = z[22] + -z[23] + z[192] + z[193];
z[195] = z[35] * z[194];
z[195] = -z[171] + z[195];
z[147] = z[6] * z[147];
z[196] = z[19] * z[194];
z[147] = z[147] + -z[196];
z[196] = z[20] * z[194];
z[197] = z[147] + z[196];
z[190] = z[3] * z[190];
z[113] = z[113] + T(2) * z[190] + z[191] + z[195] + -z[197];
z[191] = -(z[28] * z[113]);
z[77] = -z[14] + z[77];
z[198] = z[13] + z[77];
z[199] = T(3) * z[1];
z[200] = z[71] + z[81] + -z[141] + -z[198] + -z[199];
z[200] = z[4] * z[200];
z[201] = z[142] + z[150] + -z[158];
z[201] = z[5] * z[201];
z[202] = -z[12] + z[15];
z[120] = -z[1] + -z[11] + z[120] + -z[202];
z[203] = T(4) * z[120];
z[204] = z[7] + z[9];
z[203] = z[203] * z[204];
z[194] = z[18] * z[194];
z[148] = -z[148] + z[194] + z[197] + z[200] + -z[201] + -z[203];
z[194] = -(z[0] * z[148]);
z[190] = z[100] + -z[190];
z[197] = T(2) * z[11];
z[200] = z[144] + z[197];
z[201] = T(4) * z[26];
z[203] = T(2) * z[15];
z[204] = z[201] + -z[203];
z[205] = z[72] + z[204];
z[206] = z[76] + z[200] + -z[205];
z[206] = z[5] * z[206];
z[189] = -z[14] + -z[189];
z[189] = z[9] * z[189];
z[120] = z[95] * z[120];
z[207] = z[4] * z[149];
z[120] = z[120] + -z[131] + z[189] + z[190] + z[206] + z[207];
z[131] = T(2) * z[33];
z[120] = z[120] * z[131];
z[189] = z[43] * z[123];
z[189] = z[57] + z[189];
z[206] = T(6) * z[24];
z[207] = -z[141] + z[206];
z[208] = z[13] + z[14];
z[209] = z[197] + z[208];
z[210] = z[81] + z[199];
z[211] = T(10) * z[15];
z[212] = z[207] + z[209] + z[210] + -z[211];
z[213] = z[28] * z[212];
z[214] = z[30] * z[154];
z[215] = T(2) * z[36];
z[131] = -z[131] + -z[215];
z[131] = z[131] * z[178];
z[216] = prod_pow(z[54], 2);
z[131] = z[131] + T(3) * z[189] + z[213] + -z[214] + (T(3) * z[216]) / T(2);
z[131] = z[29] * z[131];
z[187] = T(-2) * z[57] + -z[187];
z[189] = T(3) * z[7];
z[187] = z[187] * z[189];
z[94] = z[94] + -z[114];
z[114] = z[20] + z[21];
z[94] = (T(-2) * z[35]) / T(5) + (T(-3) * z[94]) / T(2) + (T(-4) * z[114]) / T(5) + -z[189];
z[94] = z[94] * z[216];
z[96] = -z[29] + z[96];
z[96] = z[54] * z[96] * z[108];
z[94] = z[94] + z[96] + -z[97] + T(3) * z[112] + z[120] + z[131] + z[140] + z[155] + z[187] + z[191] + z[194];
z[94] = int_to_imaginary<T>(1) * z[94];
z[96] = T(3) * z[18] + T(-9) * z[19] + T(-4) * z[20] + T(-13) * z[21] + T(7) * z[35];
z[96] = z[53] * z[96];
z[59] = z[59] + z[94] + z[96];
z[59] = z[34] * z[59];
z[94] = z[62] + -z[65];
z[96] = (T(7) * z[11]) / T(4);
z[108] = -z[64] + z[96];
z[112] = (T(13) * z[15]) / T(4);
z[120] = (T(7) * z[13]) / T(2);
z[131] = T(3) * z[10];
z[140] = (T(-33) * z[14]) / T(4) + z[24] + z[67] + z[69] + T(-3) * z[94] + z[108] + z[112] + -z[120] + z[131];
z[140] = z[3] * z[140];
z[155] = T(3) * z[24];
z[187] = -z[131] + z[155];
z[189] = (T(7) * z[25]) / T(4);
z[191] = (T(5) * z[12]) / T(4);
z[194] = (T(19) * z[11]) / T(4) + z[191];
z[213] = (T(3) * z[1]) / T(2);
z[120] = (T(-23) * z[14]) / T(4) + (T(27) * z[15]) / T(4) + z[62] + z[65] + -z[120] + -z[187] + z[189] + -z[194] + -z[213];
z[120] = z[29] * z[120];
z[216] = z[13] / T(2);
z[217] = -z[24] + -z[62] + z[216];
z[218] = z[12] + -z[16];
z[219] = (T(5) * z[15]) / T(4);
z[220] = (T(3) * z[25]) / T(4);
z[108] = (T(-21) * z[14]) / T(4) + z[108] + -z[217] + (T(-7) * z[218]) / T(4) + z[219] + -z[220];
z[108] = z[6] * z[108];
z[66] = (T(9) * z[14]) / T(4) + -z[66] + -z[160] + z[194] + -z[220];
z[160] = -(z[5] * z[66]);
z[194] = (T(3) * z[14]) / T(4);
z[68] = (T(-11) * z[11]) / T(4) + -z[68] + -z[194] + -z[217] + z[220];
z[68] = z[9] * z[68];
z[135] = (T(3) * z[135]) / T(2);
z[218] = T(2) * z[22];
z[221] = T(-5) * z[17] + -z[23];
z[101] = (T(3) * z[27]) / T(4) + z[101] + -z[218] + z[221] / T(4);
z[101] = z[35] * z[101];
z[221] = -z[15] + z[88];
z[222] = z[186] + z[221];
z[223] = -z[25] + z[131];
z[224] = -z[12] + z[130] + -z[222] + z[223];
z[225] = -(z[4] * z[224]);
z[226] = z[19] + -z[20];
z[227] = -z[21] + z[226];
z[110] = (T(3) * z[110]) / T(4);
z[227] = z[110] * z[227];
z[228] = T(2) * z[17];
z[218] = z[23] + z[218] + z[228];
z[229] = z[18] * z[218];
z[230] = z[75] + z[146];
z[231] = z[7] * z[230];
z[68] = z[68] + z[100] + z[101] + z[108] + z[120] + z[135] + z[140] + z[160] + z[225] + z[227] + -z[229] + z[231];
z[68] = z[32] * z[68];
z[60] = -(z[31] * z[60]);
z[100] = T(2) * z[25];
z[101] = z[13] + -z[14] + z[100] + -z[203] + z[206] + -z[210];
z[101] = z[2] * z[101];
z[95] = z[95] + z[167];
z[95] = z[95] * z[230];
z[108] = z[156] + z[203] + -z[209];
z[120] = z[3] * z[108];
z[140] = -z[13] + z[146];
z[156] = z[9] * z[140];
z[160] = T(5) * z[22];
z[193] = T(8) * z[17] + z[23] + z[160] + z[193];
z[209] = z[21] * z[193];
z[156] = z[156] + z[209];
z[140] = z[5] * z[140];
z[225] = T(2) * z[218];
z[227] = z[19] * z[225];
z[230] = -z[14] + z[15];
z[231] = -z[11] + z[230];
z[232] = T(2) * z[231];
z[233] = z[8] * z[232];
z[95] = -z[95] + -z[101] + z[120] + -z[140] + z[156] + z[196] + z[227] + -z[233];
z[101] = z[28] * z[95];
z[120] = z[29] * z[232];
z[95] = -z[95] + -z[120];
z[95] = z[0] * z[95];
z[140] = z[18] * z[225];
z[188] = z[188] + -z[230];
z[196] = -z[100] + z[188];
z[234] = T(2) * z[196];
z[235] = z[3] * z[234];
z[234] = z[2] * z[234];
z[140] = z[140] + z[181] + z[234] + -z[235];
z[153] = z[124] + -z[144] + -z[153] + z[162];
z[162] = z[9] * z[153];
z[181] = T(13) * z[17] + -z[23];
z[137] = -z[137] + z[160] + z[181] / T(2);
z[160] = z[21] * z[137];
z[162] = z[160] + z[162];
z[153] = z[5] * z[153];
z[167] = z[167] + z[182];
z[167] = z[167] * z[224];
z[181] = T(6) * z[10];
z[236] = -z[83] + z[181];
z[175] = -z[175] + z[236];
z[237] = (T(9) * z[25]) / T(2);
z[173] = -z[173] + z[175] + -z[237];
z[173] = z[8] * z[173];
z[153] = -z[140] + -z[153] + z[162] + -z[167] + z[173] + z[227];
z[167] = z[30] + -z[36];
z[227] = z[153] * z[167];
z[106] = -z[106] + z[128];
z[238] = (T(15) * z[14]) / T(2);
z[239] = T(8) * z[13];
z[240] = T(4) * z[24];
z[106] = T(-4) * z[1] + (T(-11) * z[12]) / T(2) + (T(7) * z[15]) / T(2) + T(3) * z[106] + -z[122] + z[132] + z[238] + z[239] + -z[240];
z[106] = z[3] * z[106];
z[241] = -z[5] + z[6];
z[165] = z[165] * z[241];
z[241] = T(7) * z[11];
z[130] = -z[12] + -z[76] + -z[125] + -z[130] + z[203] + z[241];
z[130] = z[130] * z[179];
z[139] = -(z[139] * z[226]);
z[106] = z[106] + z[130] + z[139] + z[165] + -z[170] + -z[195];
z[106] = z[33] * z[106];
z[130] = T(10) * z[11];
z[139] = T(5) * z[12];
z[165] = T(-12) * z[15] + z[130] + z[139] + T(-2) * z[176] + z[199] + z[207] + -z[208];
z[165] = z[33] * z[165];
z[170] = -z[92] + z[164];
z[152] = z[132] + z[152] + z[170];
z[167] = z[152] * z[167];
z[195] = z[28] * z[232];
z[165] = z[165] + z[167] + z[195];
z[165] = z[29] * z[165];
z[60] = z[60] + z[68] + z[95] + z[101] + z[106] + z[165] + z[227];
z[60] = z[32] * z[60];
z[68] = -z[67] + z[90];
z[90] = -z[65] + z[68];
z[62] = -z[13] + -z[62] + z[90];
z[95] = (T(19) * z[14]) / T(4);
z[96] = -z[62] + z[95] + -z[96] + -z[131] + z[189];
z[96] = z[9] * z[96];
z[101] = T(8) * z[50];
z[70] = (T(25) * z[11]) / T(4) + -z[70] + z[90] + -z[101] + z[216];
z[70] = z[5] * z[70];
z[74] = -z[74] / T(2) + z[117] + -z[122] + z[143];
z[90] = z[2] * z[74];
z[94] = -z[67] + z[94];
z[63] = (T(13) * z[11]) / T(4) + (T(-21) * z[25]) / T(4) + z[63] + T(3) * z[94] + z[95] + -z[112];
z[63] = z[3] * z[63];
z[94] = z[160] / T(2);
z[95] = -(z[93] * z[224]);
z[106] = z[19] * z[218];
z[63] = z[63] + z[70] + (T(-3) * z[90]) / T(2) + z[94] + z[95] + z[96] + -z[103] / T(2) + z[106] + z[138] + z[173] + -z[229];
z[70] = prod_pow(z[36], 2);
z[63] = z[63] * z[70];
z[90] = (T(5) * z[14]) / T(4);
z[69] = (T(-7) * z[15]) / T(4) + (T(9) * z[16]) / T(4) + -z[26] + z[69] + z[76] + z[84] + z[90] + -z[131] + z[191] + -z[216];
z[69] = z[9] * z[69];
z[95] = -z[78] + z[185];
z[96] = T(5) * z[14];
z[103] = T(9) * z[16] + z[26] + -z[96] + -z[139];
z[106] = T(5) * z[1];
z[103] = (T(-21) * z[13]) / T(2) + z[83] + -z[95] + z[103] / T(2) + z[106];
z[103] = z[5] * z[103];
z[112] = T(11) * z[14];
z[165] = z[12] + z[16];
z[167] = T(3) * z[165];
z[173] = T(11) * z[15] + -z[112] + -z[167];
z[173] = -z[13] + z[95] + -z[173] / T(2);
z[189] = z[2] * z[173];
z[207] = -(z[20] * z[137]);
z[103] = z[103] + -z[138] + -z[151] + z[189] + z[207];
z[138] = (T(3) * z[13]) / T(2);
z[151] = (T(5) * z[25]) / T(4);
z[207] = z[26] + z[65];
z[68] = -z[1] + -z[68] + -z[84] + z[90] + -z[138] + -z[151] + T(3) * z[207];
z[68] = z[7] * z[68];
z[84] = z[61] + z[67];
z[90] = (T(3) * z[16]) / T(4) + z[26] + -z[219];
z[207] = z[84] + z[85] + z[90] + -z[151] + -z[216];
z[207] = z[4] * z[207];
z[219] = -z[1] + z[197];
z[163] = -z[12] + -z[163] + -z[164] + -z[217] + -z[219];
z[163] = z[6] * z[163];
z[217] = (T(7) * z[14]) / T(4);
z[227] = (T(5) * z[11]) / T(4);
z[62] = z[62] + z[151] + -z[217] + -z[227];
z[62] = z[29] * z[62];
z[151] = z[131] + -z[145];
z[242] = z[24] + z[89];
z[243] = z[11] + z[78] + z[151] + -z[242];
z[243] = z[3] * z[243];
z[62] = z[62] + z[68] + z[69] + -z[94] + z[103] / T(2) + z[163] + z[207] + z[243];
z[62] = z[31] * z[62];
z[68] = z[33] * z[82];
z[69] = T(7) * z[13];
z[82] = T(6) * z[25] + -z[69] + z[88] + -z[176] + -z[197] + -z[202];
z[82] = z[5] * z[82];
z[94] = T(7) * z[15];
z[103] = T(-7) * z[14] + z[94] + -z[167];
z[163] = (T(7) * z[25]) / T(2);
z[103] = z[103] / T(2) + z[121] + z[158] + -z[163];
z[103] = z[2] * z[103];
z[121] = z[20] * z[225];
z[167] = -(z[35] * z[225]);
z[82] = z[82] + z[103] + z[121] + z[162] + -z[166] + z[167] + -z[233] + z[235];
z[82] = z[36] * z[82];
z[103] = T(3) * z[25];
z[162] = z[103] + z[125];
z[166] = -z[14] + z[87];
z[167] = -z[15] + z[131];
z[207] = z[144] + -z[162] + T(-2) * z[166] + z[167] + z[241];
z[235] = T(2) * z[3];
z[207] = z[207] * z[235];
z[235] = z[181] + -z[203];
z[243] = -z[14] + z[235];
z[244] = -z[103] + z[243];
z[245] = z[157] + -z[210] + z[244];
z[245] = z[8] * z[245];
z[121] = z[121] + -z[147] + -z[234] + z[245];
z[147] = T(6) * z[11];
z[151] = z[13] + -z[147] + T(-2) * z[151] + z[162];
z[151] = z[5] * z[151];
z[151] = z[121] + z[151] + z[156] + -z[171] + -z[207];
z[151] = z[28] * z[151];
z[156] = T(10) * z[26] + -z[185];
z[162] = T(6) * z[1] + (T(9) * z[16]) / T(2) + -z[156] + z[158];
z[171] = z[162] + z[174] + z[180] + -z[236];
z[174] = z[7] * z[171];
z[115] = (T(-13) * z[25]) / T(2) + z[115] + z[143] + z[161];
z[115] = z[5] * z[115];
z[161] = -z[91] + z[201];
z[201] = z[161] + -z[199];
z[207] = z[25] + z[131] + z[201];
z[234] = -z[12] + z[144];
z[236] = -z[207] + z[222] + z[234];
z[182] = z[182] * z[236];
z[201] = z[201] + z[223];
z[202] = -z[11] + z[201] + z[202];
z[179] = z[179] * z[202];
z[115] = z[115] + -z[140] + z[159] + z[174] + z[179] + z[182] + z[233];
z[115] = z[30] * z[115];
z[140] = -z[25] + z[234];
z[159] = z[80] + -z[140] + -z[177] + -z[203];
z[159] = z[159] * z[215];
z[174] = z[30] * z[152];
z[154] = -(z[33] * z[154]);
z[154] = z[154] + z[159] + z[174] + -z[195];
z[154] = z[29] * z[154];
z[62] = z[62] + z[68] + z[82] + z[115] + -z[151] + z[154];
z[62] = z[31] * z[62];
z[68] = z[100] + -z[102] + -z[180] + -z[204] + z[213] + z[216];
z[68] = z[4] * z[68];
z[82] = -z[24] + z[131] + z[138] + -z[163] + -z[166] + -z[203] + z[241];
z[82] = z[3] * z[82];
z[72] = (T(-7) * z[13]) / T(4) + (T(5) * z[26]) / T(4) + -z[64] + -z[72] + -z[100] + z[117] + z[167];
z[72] = z[5] * z[72];
z[107] = (T(-5) * z[1]) / T(2) + -z[11] + z[15] + -z[107] + z[161] + -z[216] + z[223];
z[107] = z[9] * z[107];
z[76] = T(-3) * z[26] + z[76] + z[142] + -z[149] + z[187];
z[76] = z[7] * z[76];
z[115] = z[23] / T(2);
z[117] = (T(5) * z[22]) / T(2) + z[115] + z[136] + z[192];
z[117] = z[21] * z[117];
z[142] = z[20] * z[218];
z[117] = z[117] + z[142];
z[115] = z[22] / T(2) + -z[115] + z[136] + z[228];
z[136] = -z[18] + -z[19];
z[136] = z[115] * z[136];
z[154] = z[143] + z[145];
z[159] = -z[100] + z[197];
z[154] = -z[24] + z[154] / T(2) + z[159];
z[161] = -(z[6] * z[154]);
z[174] = z[13] + z[145];
z[174] = -z[24] + z[75] + z[174] / T(2);
z[174] = z[2] * z[174];
z[68] = z[68] + z[72] + z[76] + z[82] + z[107] + -z[117] + z[136] + z[161] + T(3) * z[174] + -z[245];
z[68] = z[0] * z[68];
z[72] = z[33] * z[148];
z[76] = z[11] + (T(11) * z[26]) / T(2);
z[82] = z[1] + (T(15) * z[13]) / T(2) + -z[76] + z[88] + z[103] + z[127] + -z[235];
z[82] = z[5] * z[82];
z[88] = -z[125] + z[157];
z[107] = z[87] + -z[167];
z[107] = z[13] + -z[88] + z[103] + T(2) * z[107];
z[107] = z[3] * z[107];
z[125] = z[13] + -z[141] + z[205] + z[219];
z[125] = z[7] * z[125];
z[127] = z[13] + -z[150];
z[127] = z[9] * z[127];
z[82] = z[82] + z[107] + -z[120] + z[121] + z[125] + z[127] + z[209];
z[82] = z[31] * z[82];
z[107] = -z[188] + z[207];
z[120] = z[4] + z[7];
z[120] = z[107] * z[120];
z[121] = z[2] + -z[3];
z[121] = z[121] * z[196];
z[75] = z[13] + z[75];
z[125] = T(2) * z[5];
z[127] = -(z[75] * z[125]);
z[136] = -z[201] + -z[231];
z[136] = z[9] * z[136];
z[141] = z[8] * z[231];
z[120] = z[120] + z[121] + z[127] + z[136] + -z[141] + z[229];
z[120] = z[30] * z[120];
z[121] = z[28] + z[30];
z[121] = z[29] * z[121] * z[231];
z[120] = z[120] + z[121];
z[68] = z[68] + z[72] + z[82] + T(2) * z[120] + z[151];
z[68] = z[0] * z[68];
z[64] = -z[64] + z[220];
z[65] = -z[26] + z[65];
z[65] = T(3) * z[65];
z[72] = -z[64] + -z[65] + z[98] + (T(-15) * z[99]) / T(4) + -z[144] + z[217];
z[72] = z[9] * z[72];
z[61] = (T(3) * z[15]) / T(4) + -z[61] + z[65] + z[85] + -z[191] + z[213] + z[220];
z[61] = z[4] * z[61];
z[64] = -z[64] + -z[90] + z[144] + z[227];
z[65] = -z[64] + z[84];
z[65] = z[5] * z[65];
z[66] = z[6] * z[66];
z[64] = z[64] + z[67] + -z[194];
z[64] = z[7] * z[64];
z[67] = -z[18] + z[19];
z[82] = -z[20] + z[67];
z[84] = z[21] + z[82];
z[84] = z[84] * z[110];
z[61] = z[61] + z[64] + z[65] + z[66] + z[72] + z[84] + -z[135] + -z[190];
z[61] = z[33] * z[61];
z[61] = z[61] + z[97];
z[61] = z[33] * z[61];
z[64] = z[203] + z[223];
z[65] = -z[13] + z[64] + -z[87] + z[197] + -z[240];
z[65] = z[5] * z[65];
z[66] = z[155] + z[186];
z[72] = z[14] + -z[210];
z[72] = -z[25] + z[66] + z[72] / T(2) + z[89] + -z[203];
z[72] = z[2] * z[72];
z[84] = -z[11] + z[24];
z[84] = z[64] + T(-5) * z[84] + z[138] + (T(-7) * z[166]) / T(2);
z[84] = z[3] * z[84];
z[85] = z[6] + z[7];
z[89] = -(z[85] * z[154]);
z[90] = z[19] + z[35];
z[97] = -(z[90] * z[115]);
z[64] = -z[11] + z[64];
z[98] = T(9) * z[14];
z[99] = -z[87] + z[98];
z[99] = -z[64] + z[99] / T(2) + z[242];
z[99] = z[9] * z[99];
z[65] = z[65] + z[72] + z[84] + z[89] + z[97] + z[99] + -z[117];
z[65] = z[28] * z[65];
z[72] = z[33] * z[113];
z[64] = z[64] + -z[96] + -z[144];
z[64] = z[9] * z[64];
z[84] = -z[25] + z[197];
z[89] = z[13] + z[84] + -z[230];
z[89] = z[89] * z[125];
z[97] = z[35] * z[218];
z[99] = -(z[2] * z[196]);
z[110] = -z[10] + z[221];
z[110] = T(3) * z[110];
z[113] = z[25] + z[110] + z[200];
z[113] = z[3] * z[113];
z[64] = z[64] + z[89] + z[97] + z[99] + z[113] + z[141];
z[64] = z[64] * z[215];
z[64] = z[64] + z[65] + z[72];
z[64] = z[28] * z[64];
z[65] = z[106] + z[139];
z[72] = T(9) * z[13] + -z[65] + z[88] + z[100] + z[112] + -z[235];
z[72] = z[9] * z[72];
z[88] = -z[100] + z[181];
z[65] = z[13] + -z[65] + z[88] + -z[98] + z[126] + z[211];
z[65] = z[3] * z[65];
z[89] = -z[100] + z[206];
z[77] = z[77] + z[210];
z[97] = z[147] + z[158];
z[98] = -z[77] + z[89] + z[97];
z[98] = z[2] * z[98];
z[99] = T(2) * z[10];
z[106] = -z[80] + z[87] + z[99] + -z[203];
z[69] = -z[69] + -z[79] + z[89] + T(3) * z[106];
z[69] = z[29] * z[69];
z[89] = z[143] + -z[146] + z[159];
z[89] = -(z[85] * z[89]);
z[106] = z[108] * z[125];
z[108] = -(z[90] * z[193]);
z[65] = z[65] + z[69] + z[72] + z[89] + z[98] + z[106] + z[108] + -z[209];
z[65] = z[39] * z[65];
z[69] = (T(13) * z[15]) / T(2);
z[72] = T(11) * z[13] + z[69] + -z[86] + -z[156] + -z[168] + -z[184];
z[72] = z[5] * z[72];
z[86] = z[9] * z[171];
z[89] = -z[162] + z[175] + z[184];
z[89] = z[4] * z[89];
z[98] = z[18] + -z[20] + z[35];
z[98] = z[98] * z[137];
z[72] = z[72] + z[86] + z[89] + z[98] + -z[160] + z[189];
z[72] = z[40] * z[72];
z[86] = -(z[36] * z[153]);
z[89] = -z[25] + z[158] + z[186] + -z[205];
z[89] = z[5] * z[89];
z[98] = -(z[6] * z[224]);
z[106] = z[143] + -z[149] + -z[199];
z[106] = z[4] * z[106];
z[107] = z[7] * z[107];
z[90] = z[90] * z[218];
z[108] = -(z[9] * z[149]);
z[89] = z[89] + z[90] + z[98] + z[106] + z[107] + z[108] + -z[142];
z[89] = z[30] * z[89];
z[86] = z[86] + z[89];
z[86] = z[30] * z[86];
z[89] = -z[128] + (T(5) * z[165]) / T(2);
z[78] = (T(-25) * z[14]) / T(2) + -z[78] + z[89] + z[132] + z[181] + z[183] + -z[239];
z[78] = z[47] * z[78];
z[90] = -z[122] + -z[124] + z[131] + -z[170] + z[185];
z[70] = z[70] * z[90];
z[90] = z[96] + z[210];
z[66] = z[66] + z[90] / T(2) + -z[94] + -z[100] + z[216];
z[66] = z[28] * z[66];
z[90] = -(z[33] * z[212]);
z[79] = -z[25] + z[79] + z[110] + z[116];
z[79] = z[79] * z[215];
z[66] = z[66] + z[79] + z[90];
z[66] = z[28] * z[66];
z[79] = -(z[36] * z[152]);
z[90] = z[140] + z[176] + -z[222];
z[90] = z[30] * z[90];
z[79] = z[79] + z[90];
z[79] = z[30] * z[79];
z[90] = -(z[40] * z[173]);
z[94] = z[33] + z[215];
z[94] = z[94] * z[178];
z[94] = z[94] + z[214];
z[94] = z[33] * z[94];
z[66] = (T(53) * z[58]) / T(6) + z[66] + z[70] + z[78] + z[79] + z[90] + z[94];
z[66] = z[29] * z[66];
z[70] = -z[3] + z[29];
z[70] = z[70] * z[123];
z[70] = z[70] + -z[105];
z[70] = z[41] * z[70];
z[78] = -z[87] + z[99] + -z[100] + z[126] + z[208];
z[78] = z[3] * z[78];
z[79] = z[84] + z[99] + -z[145];
z[84] = z[5] + -z[8];
z[79] = z[79] * z[84];
z[84] = z[13] + z[146] + z[159];
z[85] = z[2] + -z[85];
z[84] = z[84] * z[85];
z[85] = -z[22] + -z[23] + z[27];
z[85] = -(z[85] * z[226]);
z[78] = z[78] + z[79] + z[84] + z[85];
z[78] = z[38] * z[78];
z[79] = -z[2] + z[5];
z[74] = z[74] * z[79];
z[79] = -z[83] + z[119] + z[164];
z[83] = z[79] + z[118] + z[129];
z[84] = z[83] + -z[99];
z[85] = -z[8] + z[93];
z[84] = z[84] * z[85];
z[83] = -z[83] + z[144];
z[83] = z[29] * z[83];
z[85] = z[17] + T(3) * z[23];
z[85] = z[22] + z[85] / T(2) + -z[104];
z[82] = z[35] + z[82];
z[82] = z[82] * z[85];
z[74] = z[74] + z[82] + z[83] + z[84];
z[74] = z[46] * z[74];
z[70] = z[70] + z[74] + z[78];
z[74] = (T(7) * z[11]) / T(2);
z[78] = -z[74] + -z[92] + -z[102] + -z[172] + z[181] + -z[237];
z[78] = z[8] * z[78];
z[82] = z[15] + z[25];
z[80] = -z[11] + -z[80] + T(5) * z[82] + -z[101] + -z[116] + -z[165];
z[80] = z[5] * z[80];
z[67] = -z[35] + z[67];
z[67] = z[67] * z[137];
z[67] = z[67] + z[78] + z[80];
z[67] = z[47] * z[67];
z[71] = z[71] + -z[91];
z[71] = T(-9) * z[1] + T(2) * z[71] + z[81] + -z[97] + -z[100] + z[243];
z[71] = z[45] * z[71];
z[78] = z[89] + -z[181];
z[69] = -z[69] + z[78] + z[163] + z[185] + z[238];
z[69] = z[47] * z[69];
z[80] = -(z[48] * z[225]);
z[69] = (T(13) * z[58]) / T(3) + z[69] + z[71] + z[80];
z[69] = z[9] * z[69];
z[80] = -z[5] + z[9];
z[80] = z[80] * z[134];
z[82] = -(z[4] * z[133]);
z[73] = z[6] * z[73];
z[73] = z[73] + z[80] + z[82] + z[111];
z[80] = T(3) * z[42];
z[73] = z[73] * z[80];
z[82] = z[91] + -z[109];
z[82] = T(2) * z[82] + -z[88] + -z[198] + z[210];
z[82] = z[45] * z[82];
z[80] = z[80] * z[133];
z[83] = -(z[40] * z[171]);
z[80] = (T(10) * z[58]) / T(3) + z[80] + z[82] + z[83];
z[80] = z[7] * z[80];
z[78] = z[78] + z[95] + z[168] + z[169];
z[78] = z[47] * z[78];
z[71] = (T(-5) * z[58]) / T(2) + -z[71] + z[78];
z[71] = z[4] * z[71];
z[82] = -(z[8] * z[218]);
z[83] = z[52] * z[225];
z[82] = z[82] + z[83];
z[83] = -(z[2] * z[225]);
z[75] = T(4) * z[75];
z[84] = z[20] * z[75];
z[85] = -(z[35] * z[232]);
z[82] = T(2) * z[82] + z[83] + z[84] + z[85];
z[82] = z[48] * z[82];
z[77] = z[77] + z[103] + -z[130] + -z[181];
z[83] = z[8] * z[77];
z[76] = (T(-17) * z[13]) / T(2) + z[76] + -z[81] + z[244];
z[76] = z[5] * z[76];
z[81] = -z[18] + -z[20];
z[81] = z[81] * z[193];
z[76] = z[76] + z[81] + z[83];
z[76] = z[45] * z[76];
z[78] = (T(-85) * z[58]) / T(6) + z[78];
z[78] = z[6] * z[78];
z[77] = -(z[45] * z[77]);
z[79] = -z[79] + z[99] + z[128];
z[74] = (T(-17) * z[25]) / T(2) + -z[74] + T(3) * z[79] + z[116];
z[74] = z[47] * z[74];
z[74] = (T(-23) * z[58]) / T(3) + z[74] + z[77];
z[74] = z[3] * z[74];
z[77] = z[47] * z[137];
z[79] = -(z[45] * z[193]);
z[75] = z[48] * z[75];
z[75] = z[75] + z[77] + z[79];
z[75] = z[21] * z[75];
z[77] = (T(-47) * z[5]) / T(3) + (T(89) * z[8]) / T(6);
z[77] = z[58] * z[77];
z[79] = z[35] + T(2) * z[114];
z[79] = int_to_imaginary<T>(1) * z[55] * z[79];
return z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + z[68] + z[69] + T(3) * z[70] + z[71] + z[72] + z[73] + z[74] + z[75] + z[76] + z[77] + z[78] + (T(96) * z[79]) / T(5) + z[80] + z[82] + z[86];
}



template IntegrandConstructorType<double> f_4_285_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_285_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_285_construct (const Kin<qd_real>&);
#endif

}