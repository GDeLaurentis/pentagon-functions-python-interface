#include "f_4_205.h"

namespace PentagonFunctions {

template <typename T> T f_4_205_abbreviated (const std::array<T,18>&);



template <typename T> IntegrandConstructorType<T> f_4_205_construct (const Kin<T>& kin) {
    return [&kin, 
            dl8 = DLog_W_8<T>(kin),dl13 = DLog_W_13<T>(kin),dl3 = DLog_W_3<T>(kin),dl1 = DLog_W_1<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl15 = DLog_W_15<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,18> abbr = 
            {dl8(t), rlog(kin.W[4] / kin_path.W[4]), rlog(v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[2] / kin_path.W[2]), dl13(t), rlog(v_path[0]), f_2_1_1(kin_path), dl3(t), rlog(kin.W[14] / kin_path.W[14]), dl1(t), dl5(t), dl20(t), dl15(t)}
;

        auto result = f_4_205_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_205_abbreviated(const std::array<T,18>& abb)
{
using TR = typename T::value_type;
T z[52];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[12];
z[12] = abb[15];
z[13] = abb[16];
z[14] = abb[14];
z[15] = abb[10];
z[16] = abb[11];
z[17] = abb[13];
z[18] = bc<TR>[1];
z[19] = bc<TR>[2];
z[20] = bc<TR>[4];
z[21] = bc<TR>[7];
z[22] = bc<TR>[8];
z[23] = bc<TR>[9];
z[24] = abb[17];
z[25] = z[7] * z[11];
z[26] = z[8] * z[11];
z[25] = z[25] + -z[26];
z[27] = -z[7] + z[8];
z[28] = z[1] + -z[9];
z[29] = z[27] + z[28];
z[29] = z[10] * z[29];
z[30] = T(2) * z[17];
z[31] = -z[8] + z[30];
z[32] = z[7] + -z[31];
z[33] = z[9] + z[32];
z[34] = T(2) * z[14];
z[35] = z[33] * z[34];
z[36] = T(4) * z[17];
z[37] = T(2) * z[7] + -z[36];
z[38] = T(2) * z[8];
z[39] = z[37] + z[38];
z[40] = z[1] + z[9];
z[41] = -z[39] + -z[40];
z[41] = z[13] * z[41];
z[35] = -z[25] + z[29] + z[35] + z[41];
z[35] = z[2] * z[35];
z[41] = -z[1] + T(3) * z[9];
z[42] = z[32] + z[41] / T(2);
z[42] = z[12] * z[42];
z[43] = z[1] + z[7];
z[44] = z[9] + z[43];
z[30] = -z[30] + z[44] / T(2);
z[30] = z[11] * z[30];
z[30] = (T(3) * z[26]) / T(2) + z[30] + -z[42];
z[42] = z[32] + z[40] / T(2);
z[42] = z[13] * z[42];
z[31] = -z[31] + z[43];
z[43] = -(z[14] * z[31]);
z[43] = (T(-3) * z[29]) / T(2) + z[30] + z[42] + z[43];
z[43] = z[15] * z[43];
z[35] = z[35] + z[43];
z[35] = z[15] * z[35];
z[39] = z[39] + z[41];
z[41] = z[12] * z[39];
z[43] = -z[36] + z[40];
z[45] = T(3) * z[7] + z[43];
z[45] = z[11] * z[45];
z[46] = -(z[31] * z[34]);
z[47] = z[13] * z[39];
z[45] = z[26] + -z[29] + -z[41] + z[45] + z[46] + z[47];
z[45] = z[16] * z[45];
z[46] = T(2) * z[13];
z[31] = z[31] * z[46];
z[39] = z[14] * z[39];
z[31] = z[31] + -z[39];
z[36] = -z[36] + z[44];
z[36] = z[11] * z[36];
z[27] = z[27] + -z[28];
z[27] = z[0] * z[27];
z[36] = T(3) * z[26] + z[27] + -z[31] + z[36] + -z[41];
z[39] = z[6] * z[36];
z[44] = z[25] + z[27];
z[47] = T(2) * z[29];
z[31] = z[31] + z[44] + -z[47];
z[31] = z[2] * z[31];
z[48] = z[28] * z[34];
z[41] = z[41] + z[48];
z[43] = -(z[11] * z[43]);
z[26] = T(-4) * z[26] + z[41] + z[43] + z[47];
z[26] = z[15] * z[26];
z[43] = T(4) * z[24];
z[34] = T(3) * z[11] + -z[12] + z[34] + -z[43] + z[46];
z[34] = z[18] * z[34];
z[47] = z[11] + z[14];
z[48] = -z[12] + -z[13] + z[47];
z[48] = z[19] * z[48];
z[34] = z[34] + z[48] / T(2);
z[34] = z[19] * z[34];
z[48] = T(2) * z[24];
z[49] = -z[46] + z[48];
z[50] = (T(-7) * z[11]) / T(2) + (T(5) * z[12]) / T(2) + z[49];
z[50] = prod_pow(z[18], 2) * z[50];
z[51] = T(-5) * z[11] + T(3) * z[12] + T(-4) * z[13] + z[43];
z[51] = z[20] * z[51];
z[26] = z[26] + z[31] + z[34] + z[39] + z[50] + z[51];
z[26] = int_to_imaginary<T>(1) * z[26];
z[25] = T(7) * z[25] + T(5) * z[27];
z[31] = T(5) * z[1] + T(-7) * z[9];
z[31] = z[31] / T(2) + -z[32];
z[31] = z[14] * z[31];
z[32] = z[13] * z[33];
z[25] = z[25] / T(2) + z[31] + z[32];
z[31] = z[14] / T(2);
z[32] = z[11] + z[24];
z[32] = -z[12] + (T(-7) * z[13]) / T(6) + z[31] + (T(2) * z[32]) / T(3);
z[32] = z[19] * z[32];
z[31] = (T(-17) * z[11]) / T(12) + (T(11) * z[12]) / T(12) + -z[13] / T(2) + z[24] + -z[31];
z[31] = z[18] * z[31];
z[34] = -z[12] / T(3) + z[13] / T(6) + -z[24] + (T(5) * z[47]) / T(6);
z[34] = int_to_imaginary<T>(1) * z[3] * z[34];
z[25] = z[25] / T(3) + z[31] + z[32] + z[34];
z[25] = z[3] * z[25];
z[25] = z[25] + z[26];
z[25] = z[3] * z[25];
z[26] = -(z[14] * z[33]);
z[26] = z[26] + -z[27] + z[29] / T(2) + -z[30] + z[42];
z[26] = prod_pow(z[2], 2) * z[26];
z[27] = -(z[5] * z[36]);
z[29] = z[37] + z[38] + z[40];
z[29] = z[11] * z[29];
z[30] = z[28] * z[46];
z[29] = z[29] + -z[30] + -z[41];
z[30] = z[2] + -z[15];
z[29] = z[29] * z[30];
z[28] = z[14] * z[28];
z[28] = z[28] + z[44];
z[28] = z[4] * z[28];
z[28] = z[28] + z[29];
z[28] = z[4] * z[28];
z[29] = z[11] + -z[49];
z[29] = z[18] * z[20] * z[29];
z[30] = z[11] + -z[43];
z[30] = z[21] * z[30];
z[29] = z[29] + z[30];
z[30] = T(3) * z[21] + (T(-14) * z[23]) / T(3);
z[30] = z[30] * z[46];
z[31] = z[12] + z[14];
z[32] = z[11] + -z[24];
z[32] = -z[31] + T(2) * z[32];
z[32] = z[13] + z[32] / T(3);
z[32] = prod_pow(z[18], 3) * z[32];
z[33] = -z[11] + T(8) * z[24];
z[33] = (T(-5) * z[13]) / T(3) + -z[31] + z[33] / T(3);
z[33] = prod_pow(z[19], 3) * z[33];
z[31] = z[11] / T(4) + (T(5) * z[13]) / T(4) + (T(3) * z[31]) / T(4) + -z[48];
z[31] = z[22] * z[31];
z[34] = (T(-17) * z[11]) / T(4) + (T(35) * z[24]) / T(3);
z[34] = z[23] * z[34];
z[36] = T(2) * z[21];
z[37] = (T(-7) * z[23]) / T(3) + z[36];
z[37] = z[14] * z[37];
z[36] = (T(-19) * z[23]) / T(12) + z[36];
z[36] = z[12] * z[36];
return z[25] + z[26] + z[27] + z[28] + T(2) * z[29] + z[30] + z[31] + z[32] + z[33] + z[34] + z[35] + z[36] + z[37] + z[45];
}



template IntegrandConstructorType<double> f_4_205_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_205_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_205_construct (const Kin<qd_real>&);
#endif

}