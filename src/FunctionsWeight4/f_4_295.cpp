#include "f_4_295.h"

namespace PentagonFunctions {

template <typename T> T f_4_295_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_295_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_295_W_12 (const Kin<T>& kin) {
        c[0] = (-kin.v[1] + T(-2)) * kin.v[1] + kin.v[4] * (T(2) + kin.v[4]);
c[1] = kin.v[1] * (T(2) + kin.v[1]) + (-kin.v[4] + T(-2)) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] + prod_pow(abb[1], 2) * (abb[4] + -abb[3]) + -(prod_pow(abb[2], 2) * abb[4]));
    }
};
template <typename T> class SpDLog_f_4_295_W_7 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_295_W_7 (const Kin<T>& kin) {
        c[0] = (kin.v[3] / T(4) + kin.v[4] / T(2) + -kin.v[1] + T(-1)) * kin.v[3] + -(kin.v[1] * kin.v[4]) + (kin.v[4] / T(4) + T(-1)) * kin.v[4];
c[1] = kin.v[3] + kin.v[4];
c[2] = (kin.v[3] / T(4) + kin.v[4] / T(2) + -kin.v[1] + T(-1)) * kin.v[3] + -(kin.v[1] * kin.v[4]) + (kin.v[4] / T(4) + T(-1)) * kin.v[4];
c[3] = kin.v[3] + kin.v[4];
c[4] = (kin.v[3] / T(4) + kin.v[4] / T(2) + -kin.v[1] + T(-1)) * kin.v[3] + -(kin.v[1] * kin.v[4]) + (kin.v[4] / T(4) + T(-1)) * kin.v[4];
c[5] = kin.v[3] + kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[8] * (t * c[0] + c[1]) + abb[4] * (t * c[2] + c[3]) + abb[9] * (t * c[4] + c[5]);
        }

        return abb[5] * (prod_pow(abb[6], 2) * (abb[4] / T(2) + abb[9] / T(2)) + abb[1] * (abb[6] * abb[8] + abb[6] * (abb[4] + abb[9]) + abb[1] * ((abb[4] * T(-3)) / T(2) + (abb[8] * T(-3)) / T(2) + (abb[9] * T(-3)) / T(2))) + abb[8] * (prod_pow(abb[6], 2) / T(2) + -abb[7]) + abb[7] * (-abb[4] + -abb[9]));
    }
};
template <typename T> class SpDLog_f_4_295_W_21 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_295_W_21 (const Kin<T>& kin) {
        c[0] = (-kin.v[3] / T(4) + -kin.v[4] + T(1)) * kin.v[3] + ((T(3) * kin.v[2]) / T(4) + kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + kin.v[0] * (-kin.v[3] / T(2) + (T(3) * kin.v[0]) / T(4) + kin.v[1] / T(2) + (T(-3) * kin.v[2]) / T(2) + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(4) + -kin.v[2] / T(2) + kin.v[3] / T(2) + T(-1) + kin.v[4]);
c[1] = kin.v[0] + kin.v[1] + -kin.v[2] + -kin.v[3];
c[2] = (-kin.v[3] / T(4) + -kin.v[4] + T(1)) * kin.v[3] + ((T(3) * kin.v[2]) / T(4) + kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + kin.v[0] * (-kin.v[3] / T(2) + (T(3) * kin.v[0]) / T(4) + kin.v[1] / T(2) + (T(-3) * kin.v[2]) / T(2) + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(4) + -kin.v[2] / T(2) + kin.v[3] / T(2) + T(-1) + kin.v[4]);
c[3] = kin.v[0] + kin.v[1] + -kin.v[2] + -kin.v[3];
c[4] = (-kin.v[3] / T(2) + kin.v[1] / T(4) + kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[1] + (-kin.v[1] / T(2) + (T(-3) * kin.v[0]) / T(4) + (T(3) * kin.v[2]) / T(2) + kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[0] + kin.v[2] * (-kin.v[3] / T(2) + (T(-3) * kin.v[2]) / T(4) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(4) + T(-1) + kin.v[4]);
c[5] = -kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3];
c[6] = (-kin.v[3] / T(2) + kin.v[1] / T(4) + kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[1] + (-kin.v[1] / T(2) + (T(-3) * kin.v[0]) / T(4) + (T(3) * kin.v[2]) / T(2) + kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[0] + kin.v[2] * (-kin.v[3] / T(2) + (T(-3) * kin.v[2]) / T(4) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(4) + T(-1) + kin.v[4]);
c[7] = -kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[9] * (t * c[0] + c[1]) + abb[13] * (t * c[2] + c[3]) + abb[8] * (t * c[4] + c[5]) + abb[4] * (t * c[6] + c[7]);
        }

        return abb[10] * (prod_pow(abb[11], 2) * (abb[9] + abb[13] + -abb[4]) + abb[8] * (abb[12] + -prod_pow(abb[11], 2)) + abb[12] * (abb[4] + -abb[9] + -abb[13]) + prod_pow(abb[6], 2) * (abb[4] + abb[8] + -abb[9] + -abb[13]));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_295_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl7 = DLog_W_7<T>(kin),dl21 = DLog_W_21<T>(kin),dl4 = DLog_W_4<T>(kin),dl18 = DLog_W_18<T>(kin),dl8 = DLog_W_8<T>(kin),dl16 = DLog_W_16<T>(kin),dl15 = DLog_W_15<T>(kin),dl20 = DLog_W_20<T>(kin),dl5 = DLog_W_5<T>(kin),dl3 = DLog_W_3<T>(kin),dl2 = DLog_W_2<T>(kin),dl19 = DLog_W_19<T>(kin),dl17 = DLog_W_17<T>(kin),dl1 = DLog_W_1<T>(kin),dl31 = DLog_W_31<T>(kin),dl30 = DLog_W_30<T>(kin),dl26 = DLog_W_26<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),spdl12 = SpDLog_f_4_295_W_12<T>(kin),spdl7 = SpDLog_f_4_295_W_7<T>(kin),spdl21 = SpDLog_f_4_295_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,46> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl7(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl21(t), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_15(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl4(t), f_2_1_4(kin_path), dl18(t), dl8(t), rlog(v_path[2]), dl16(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl15(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl20(t), dl5(t), dl3(t), dl2(t), dl19(t), dl17(t), dl1(t), dl31(t), dl30(t) / kin_path.SqrtDelta, rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl26(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta}
;

        auto result = f_4_295_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl21(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_295_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[112];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[14];
z[3] = abb[16];
z[4] = abb[25];
z[5] = abb[27];
z[6] = abb[28];
z[7] = abb[29];
z[8] = abb[30];
z[9] = abb[4];
z[10] = abb[8];
z[11] = abb[9];
z[12] = abb[26];
z[13] = abb[34];
z[14] = abb[42];
z[15] = abb[43];
z[16] = abb[44];
z[17] = abb[35];
z[18] = abb[36];
z[19] = abb[37];
z[20] = abb[38];
z[21] = abb[39];
z[22] = abb[40];
z[23] = abb[41];
z[24] = abb[2];
z[25] = abb[22];
z[26] = abb[6];
z[27] = abb[11];
z[28] = abb[18];
z[29] = bc<TR>[0];
z[30] = abb[45];
z[31] = abb[31];
z[32] = abb[19];
z[33] = abb[7];
z[34] = abb[12];
z[35] = abb[15];
z[36] = abb[20];
z[37] = abb[21];
z[38] = abb[23];
z[39] = abb[24];
z[40] = abb[17];
z[41] = abb[13];
z[42] = abb[33];
z[43] = bc<TR>[3];
z[44] = abb[32];
z[45] = bc<TR>[2];
z[46] = bc<TR>[1];
z[47] = bc<TR>[4];
z[48] = bc<TR>[9];
z[49] = -z[1] + z[11];
z[50] = z[49] / T(4);
z[51] = z[9] + z[50];
z[52] = z[10] / T(2) + z[51];
z[52] = z[3] * z[52];
z[53] = z[25] * z[49];
z[54] = -z[10] + z[51];
z[54] = z[4] * z[54];
z[54] = z[52] + -z[53] + z[54];
z[55] = z[1] / T(3);
z[56] = z[11] + z[55];
z[56] = z[12] * z[56];
z[55] = -z[11] + z[55];
z[57] = z[2] * z[55];
z[58] = z[16] + z[42];
z[59] = z[14] + -z[15] + T(3) * z[30] + z[58];
z[60] = z[23] * z[59];
z[56] = z[56] + z[57] + z[60];
z[57] = z[9] / T(3);
z[60] = z[41] / T(3);
z[61] = (T(7) * z[1]) / T(3) + -z[11];
z[61] = -z[57] + z[60] + z[61] / T(4);
z[61] = z[5] * z[61];
z[62] = z[20] + z[21];
z[63] = z[17] / T(2) + -z[22];
z[64] = z[13] / T(4) + z[18] + z[63] / T(2);
z[65] = z[62] / T(4) + -z[64] / T(3);
z[66] = z[46] / T(3);
z[67] = z[65] + -z[66];
z[67] = z[16] * z[67];
z[57] = z[50] + z[57];
z[57] = z[8] * z[57];
z[66] = z[65] + z[66];
z[66] = z[14] * z[66];
z[68] = -(z[15] * z[65]);
z[55] = z[55] / T(2) + -z[60];
z[55] = z[32] * z[55];
z[60] = (T(3) * z[62]) / T(4) + -z[64];
z[64] = (T(-4) * z[46]) / T(3) + z[60];
z[64] = z[30] * z[64];
z[65] = z[45] + z[65];
z[65] = z[42] * z[65];
z[69] = z[7] / T(2);
z[70] = (T(-5) * z[1]) / T(3) + -z[11];
z[70] = z[10] / T(3) + (T(-5) * z[41]) / T(3) + z[70] / T(2);
z[70] = z[69] * z[70];
z[71] = z[11] + z[41];
z[72] = z[9] + z[71];
z[72] = z[40] * z[72];
z[73] = z[9] * z[12];
z[51] = z[31] * z[51];
z[74] = z[14] + z[16];
z[75] = z[30] + z[74] / T(2);
z[75] = z[45] * z[75];
z[51] = (T(-5) * z[51]) / T(3) + z[54] / T(3) + z[55] / T(2) + z[56] / T(4) + z[57] + z[61] + z[64] + z[65] + z[66] + z[67] + z[68] + z[70] + z[72] + (T(2) * z[73]) / T(3) + z[75];
z[51] = z[29] * z[51];
z[54] = z[4] + z[5];
z[55] = z[12] + z[54];
z[55] = T(2) * z[7] + -z[8] + -z[31] + T(-4) * z[44] + T(3) * z[55];
z[55] = z[43] * z[55];
z[51] = z[51] + z[55];
z[51] = z[29] * z[51];
z[55] = z[12] / T(2);
z[56] = z[49] * z[55];
z[57] = -z[13] / T(2) + T(-2) * z[18] + (T(3) * z[62]) / T(2) + -z[63];
z[61] = z[16] * z[57];
z[62] = z[56] + z[61];
z[63] = z[42] * z[57];
z[64] = T(3) * z[11];
z[65] = -z[1] + z[64];
z[66] = z[65] / T(2);
z[67] = z[41] + z[66];
z[68] = z[32] * z[67];
z[63] = z[63] + -z[68];
z[70] = z[62] + z[63];
z[75] = z[49] / T(2);
z[76] = z[4] * z[75];
z[77] = -z[53] + z[76];
z[78] = z[7] * z[71];
z[79] = z[70] + z[77] + z[78];
z[79] = z[26] * z[79];
z[80] = T(2) * z[9];
z[81] = z[75] + z[80];
z[82] = z[31] * z[81];
z[83] = z[12] * z[80];
z[83] = -z[82] + z[83];
z[84] = z[30] * z[57];
z[85] = z[83] + z[84];
z[86] = z[7] * z[75];
z[86] = z[53] + z[86];
z[87] = z[4] * z[80];
z[87] = z[85] + z[86] + z[87];
z[88] = z[27] * z[87];
z[87] = z[24] * z[87];
z[79] = z[79] + z[87] + z[88];
z[88] = z[14] * z[57];
z[89] = (T(3) * z[49]) / T(2) + z[80];
z[90] = z[8] * z[89];
z[88] = z[88] + z[90];
z[62] = -z[62] + z[88];
z[90] = z[75] + -z[80];
z[90] = z[4] * z[90];
z[91] = z[7] * z[49];
z[83] = T(2) * z[53] + z[62] + z[83] + -z[90] + z[91];
z[90] = z[39] * z[83];
z[91] = z[4] * z[81];
z[92] = z[7] * z[67];
z[70] = z[70] + z[85] + z[91] + z[92];
z[85] = z[37] * z[70];
z[62] = z[62] + -z[76] + -z[84] + z[86];
z[76] = -(z[0] * z[62]);
z[86] = z[26] * z[58];
z[91] = z[24] * z[30];
z[92] = z[27] * z[30];
z[86] = z[86] + z[91] + z[92];
z[91] = z[14] + -z[16];
z[93] = z[39] * z[91];
z[94] = -z[30] + z[91];
z[95] = z[0] * z[94];
z[96] = z[30] + z[58];
z[97] = z[37] * z[96];
z[93] = z[86] + -z[93] + z[95] + -z[97];
z[95] = (T(3) * z[23]) / T(2);
z[97] = -(z[93] * z[95]);
z[80] = (T(5) * z[49]) / T(2) + z[80];
z[98] = z[39] * z[80];
z[99] = z[9] + z[49];
z[100] = T(2) * z[99];
z[101] = z[0] * z[100];
z[102] = z[24] * z[75];
z[98] = -z[98] + z[101] + z[102];
z[101] = z[26] * z[71];
z[101] = T(2) * z[101];
z[64] = z[1] + z[64];
z[103] = T(2) * z[41] + z[64] / T(2);
z[104] = z[37] * z[103];
z[105] = z[27] * z[75];
z[104] = z[98] + -z[101] + z[104] + z[105];
z[104] = z[5] * z[104];
z[105] = -z[26] + z[27];
z[105] = z[75] * z[105];
z[100] = z[28] * z[100];
z[106] = prod_pow(z[29], 2);
z[98] = z[98] + z[100] + z[105] + -z[106] / T(9);
z[98] = z[6] * z[98];
z[78] = z[72] + -z[78];
z[105] = z[4] * z[9];
z[107] = z[73] + z[105];
z[108] = -z[53] + z[78] + -z[107];
z[108] = z[28] * z[108];
z[109] = prod_pow(z[46], 2);
z[110] = T(2) * z[47] + z[109];
z[110] = z[30] * z[110];
z[108] = z[108] + z[110];
z[109] = z[47] + z[109] / T(2);
z[109] = -(z[91] * z[109]);
z[110] = T(-4) * z[30] + z[91];
z[110] = z[46] * z[110];
z[111] = -(z[45] * z[91]);
z[110] = z[110] + z[111];
z[110] = z[45] * z[110];
z[111] = z[8] / T(9) + -z[12] / T(3) + -z[16] / T(4);
z[54] = (T(-2) * z[7]) / T(27) + -z[30] / T(2) + z[31] / T(27) + -z[42] / T(12) + (T(4) * z[44]) / T(27) + -z[54] / T(9) + z[111] / T(3);
z[54] = z[54] * z[106];
z[54] = z[54] + z[76] + -z[79] + z[85] + z[90] + z[97] + z[98] + z[104] + T(2) * z[108] + z[109] + z[110];
z[76] = int_to_imaginary<T>(1) * z[29];
z[54] = z[54] * z[76];
z[85] = z[16] * z[24];
z[90] = z[15] + z[30];
z[97] = z[27] * z[90];
z[85] = z[85] + z[97];
z[97] = z[16] * z[26];
z[94] = z[28] * z[94];
z[74] = z[15] + z[74];
z[98] = z[0] * z[74];
z[94] = z[85] + z[94] + z[97] + -z[98] / T(2);
z[94] = z[0] * z[94];
z[97] = z[14] + z[30] + z[42];
z[98] = z[28] / T(2);
z[104] = z[97] * z[98];
z[86] = -z[86] + z[104];
z[86] = z[28] * z[86];
z[58] = -z[58] + z[90];
z[104] = z[26] / T(2);
z[108] = z[58] * z[104];
z[85] = -z[85] + z[108];
z[85] = z[26] * z[85];
z[108] = z[34] * z[58];
z[90] = z[16] + z[90];
z[90] = z[33] * z[90];
z[109] = -z[14] + z[30];
z[109] = z[35] * z[109];
z[110] = z[38] * z[91];
z[92] = z[24] * z[92];
z[96] = z[36] * z[96];
z[85] = z[85] + -z[86] + -z[90] + -z[92] + z[94] + -z[96] + z[108] + z[109] + -z[110];
z[86] = z[85] * z[95];
z[76] = z[76] * z[93];
z[76] = -z[76] + z[85];
z[59] = z[59] * z[106];
z[59] = z[59] / T(2) + T(3) * z[76];
z[59] = z[19] * z[59];
z[76] = z[35] * z[89];
z[80] = z[38] * z[80];
z[85] = prod_pow(z[24], 2);
z[89] = z[85] * z[99];
z[76] = z[76] + z[80] + z[89];
z[80] = z[9] + z[10];
z[89] = T(2) * z[80];
z[90] = z[27] * z[89];
z[90] = z[90] + z[102];
z[92] = z[9] + z[75];
z[93] = -(z[26] * z[92]);
z[94] = -z[10] + z[49];
z[95] = z[9] / T(2) + z[94];
z[95] = z[0] * z[95];
z[93] = z[90] + z[93] + z[95] + -z[100];
z[93] = z[0] * z[93];
z[92] = z[92] * z[104];
z[90] = -z[90] + z[92];
z[90] = z[26] * z[90];
z[92] = z[24] * z[49];
z[95] = z[27] * z[49];
z[96] = z[92] + z[95];
z[99] = z[26] * z[49];
z[106] = z[28] * z[75];
z[106] = -z[96] + z[99] + z[106];
z[98] = z[98] * z[106];
z[80] = z[27] * z[80];
z[80] = z[80] + z[102];
z[80] = z[27] * z[80];
z[81] = z[10] + z[81];
z[106] = z[34] * z[81];
z[108] = -z[9] + T(-2) * z[10] + z[75];
z[109] = z[33] * z[108];
z[110] = z[9] + -z[10] + (T(5) * z[49]) / T(4);
z[110] = z[29] * z[110];
z[110] = T(3) * z[43] + z[110] / T(3);
z[110] = z[29] * z[110];
z[80] = z[76] + z[80] + z[90] + z[93] + z[98] + z[106] + z[109] + z[110];
z[80] = z[6] * z[80];
z[90] = -(z[60] * z[97]);
z[93] = T(3) * z[1] + -z[11];
z[93] = z[41] + z[93] / T(2);
z[97] = z[69] * z[93];
z[49] = z[9] + (T(3) * z[49]) / T(4);
z[49] = z[8] * z[49];
z[68] = z[68] / T(2);
z[72] = -z[49] + z[68] + -z[72] + z[82] + z[90] + z[97] + -z[107];
z[72] = z[28] * z[72];
z[72] = z[72] + z[79];
z[72] = z[28] * z[72];
z[79] = z[9] + -z[93];
z[79] = z[28] * z[79];
z[79] = z[79] + -z[96] / T(2) + z[101];
z[79] = z[28] * z[79];
z[82] = z[92] + -z[95];
z[90] = z[82] + z[99];
z[50] = -(z[0] * z[50]);
z[50] = z[50] + z[90] / T(2) + -z[100];
z[50] = z[0] * z[50];
z[71] = -(z[27] * z[71]);
z[71] = z[71] + z[102];
z[71] = z[27] * z[71];
z[90] = -(z[26] * z[75]);
z[82] = -z[82] + z[90];
z[82] = z[82] * z[104];
z[90] = -(z[36] * z[103]);
z[67] = -(z[34] * z[67]);
z[50] = z[50] + z[67] + z[71] + z[76] + z[79] + z[82] + z[90];
z[50] = z[5] * z[50];
z[67] = z[11] * z[55];
z[71] = -z[10] + z[75];
z[75] = z[4] * z[71];
z[67] = z[67] + z[75] / T(2);
z[74] = -(z[60] * z[74]);
z[76] = -(z[69] * z[94]);
z[79] = z[2] * z[66];
z[49] = -z[49] + z[52] + -z[67] + z[74] + z[76] + z[79];
z[49] = z[0] * z[49];
z[57] = z[15] * z[57];
z[74] = z[3] * z[81];
z[57] = z[57] + -z[74] + z[84];
z[71] = z[7] * z[71];
z[74] = z[4] * z[89];
z[56] = z[56] + z[57] + z[71] + z[74];
z[56] = z[27] * z[56];
z[71] = -z[61] + z[79];
z[74] = z[7] * z[11];
z[76] = z[71] + -z[74];
z[82] = z[11] * z[12];
z[77] = z[76] + -z[77] + T(-2) * z[82];
z[77] = z[24] * z[77];
z[56] = z[56] + -z[77];
z[62] = z[28] * z[62];
z[75] = z[75] + -z[76] + z[82];
z[75] = z[26] * z[75];
z[49] = z[49] + z[56] + z[62] + z[75];
z[49] = z[0] * z[49];
z[62] = -(z[55] * z[65]);
z[65] = -(z[4] * z[81]);
z[75] = z[10] + -z[66];
z[75] = z[7] * z[75];
z[62] = -z[57] + z[62] + z[65] + z[71] + z[75];
z[62] = z[33] * z[62];
z[55] = -(z[55] * z[64]);
z[64] = -(z[7] * z[66]);
z[55] = z[55] + z[64] + z[79] + z[84] + -z[88];
z[55] = z[35] * z[55];
z[58] = z[58] * z[60];
z[60] = z[1] + z[11];
z[60] = z[10] + z[41] + z[60] / T(2);
z[64] = -(z[60] * z[69]);
z[52] = -z[52] + z[58] + z[64] + -z[67] + z[68];
z[52] = z[26] * z[52];
z[52] = z[52] + -z[56];
z[52] = z[26] * z[52];
z[56] = -(z[38] * z[83]);
z[58] = -(z[4] * z[108]);
z[60] = -(z[7] * z[60]);
z[57] = z[57] + z[58] + z[60] + -z[61] + -z[63];
z[57] = z[34] * z[57];
z[58] = -(z[36] * z[70]);
z[53] = -z[53] + -z[74] + -z[105];
z[53] = z[53] * z[85];
z[60] = -z[73] + z[78];
z[60] = z[27] * z[60];
z[60] = z[60] + -z[87];
z[60] = z[27] * z[60];
z[61] = -z[15] + (T(19) * z[30]) / T(6) + (T(-11) * z[91]) / T(12);
z[61] = z[48] * z[61];
return z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] / T(2) + z[60] + z[61] + z[62] + z[72] + z[80] + z[86];
}



template IntegrandConstructorType<double> f_4_295_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_295_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_295_construct (const Kin<qd_real>&);
#endif

}