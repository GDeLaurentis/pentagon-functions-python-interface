#include "f_4_383.h"

namespace PentagonFunctions {

template <typename T> T f_4_383_abbreviated (const std::array<T,63>&);

template <typename T> class SpDLog_f_4_383_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_383_W_12 (const Kin<T>& kin) {
        c[0] = ((T(-3) * kin.v[1]) / T(2) + T(-3)) * kin.v[1] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4];
c[1] = ((T(3) * kin.v[1]) / T(2) + T(3)) * kin.v[1] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1]);
        }

        return abb[0] * ((prod_pow(abb[2], 2) * abb[4] * T(-3)) / T(2) + (prod_pow(abb[2], 2) * abb[3] * T(3)) / T(2) + prod_pow(abb[1], 2) * ((abb[3] * T(-3)) / T(2) + (abb[4] * T(3)) / T(2)));
    }
};
template <typename T> class SpDLog_f_4_383_W_10 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_383_W_10 (const Kin<T>& kin) {
        c[0] = (kin.v[0] * kin.v[2]) / T(2) + kin.v[2] * ((T(-13) * kin.v[4]) / T(2) + -kin.v[3] + T(-7) + T(8) * kin.v[2]) + bc<T>[0] * (T(9) / T(2) + (T(-9) * kin.v[2]) / T(4) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) * kin.v[2] + kin.v[1] * (kin.v[0] / T(2) + (T(-13) * kin.v[4]) / T(2) + -kin.v[3] + T(-7) + bc<T>[0] * (T(9) / T(2) + (T(-9) * kin.v[2]) / T(2) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) + (bc<T>[0] * (int_to_imaginary<T>(-9) / T(4)) + T(8)) * kin.v[1] + T(16) * kin.v[2]);
c[1] = (T(-4) + bc<T>[0] * (int_to_imaginary<T>(9) / T(2))) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) * kin.v[2];
c[2] = (kin.v[0] * kin.v[2]) / T(2) + kin.v[2] * ((T(-13) * kin.v[4]) / T(2) + -kin.v[3] + T(-7) + T(8) * kin.v[2]) + bc<T>[0] * (T(9) / T(2) + (T(-9) * kin.v[2]) / T(4) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) * kin.v[2] + kin.v[1] * (kin.v[0] / T(2) + (T(-13) * kin.v[4]) / T(2) + -kin.v[3] + T(-7) + bc<T>[0] * (T(9) / T(2) + (T(-9) * kin.v[2]) / T(2) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) + (bc<T>[0] * (int_to_imaginary<T>(-9) / T(4)) + T(8)) * kin.v[1] + T(16) * kin.v[2]);
c[3] = (T(-4) + bc<T>[0] * (int_to_imaginary<T>(9) / T(2))) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) * kin.v[2];
c[4] = (kin.v[0] * kin.v[2]) / T(2) + kin.v[2] * ((T(-13) * kin.v[4]) / T(2) + -kin.v[3] + T(-7) + T(8) * kin.v[2]) + bc<T>[0] * (T(9) / T(2) + (T(-9) * kin.v[2]) / T(4) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) * kin.v[2] + kin.v[1] * (kin.v[0] / T(2) + (T(-13) * kin.v[4]) / T(2) + -kin.v[3] + T(-7) + bc<T>[0] * (T(9) / T(2) + (T(-9) * kin.v[2]) / T(2) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) + (bc<T>[0] * (int_to_imaginary<T>(-9) / T(4)) + T(8)) * kin.v[1] + T(16) * kin.v[2]);
c[5] = (T(-4) + bc<T>[0] * (int_to_imaginary<T>(9) / T(2))) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) * kin.v[2];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[3] * (t * c[0] + c[1]) + abb[8] * (t * c[2] + c[3]) + abb[14] * (t * c[4] + c[5]);
        }

        return abb[9] * (abb[11] * abb[12] * (-abb[8] / T(2) + -abb[14] / T(2)) + abb[1] * ((abb[3] * abb[12]) / T(2) + abb[12] * (abb[8] / T(2) + abb[14] / T(2)) + abb[2] * (-abb[3] / T(2) + -abb[8] / T(2) + -abb[14] / T(2))) + abb[10] * abb[12] * ((abb[8] * T(9)) / T(2) + (abb[14] * T(9)) / T(2)) + abb[12] * (abb[7] * (abb[8] * T(-4) + abb[14] * T(-4)) + abb[8] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[14] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[12] * (abb[8] * T(3) + abb[14] * T(3))) + abb[3] * (-(abb[11] * abb[12]) / T(2) + (abb[10] * abb[12] * T(9)) / T(2) + abb[12] * (abb[7] * T(-4) + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[12] * T(3)) + abb[13] * T(4)) + abb[13] * (abb[8] * T(4) + abb[14] * T(4)) + abb[2] * (abb[11] * (abb[8] / T(2) + abb[14] / T(2)) + abb[2] * (abb[3] / T(2) + abb[8] / T(2) + abb[14] / T(2)) + abb[10] * ((abb[8] * T(-9)) / T(2) + (abb[14] * T(-9)) / T(2)) + abb[12] * ((abb[8] * T(-7)) / T(2) + (abb[14] * T(-7)) / T(2)) + abb[8] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[14] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[3] * (abb[11] / T(2) + (abb[10] * T(-9)) / T(2) + (abb[12] * T(-7)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[7] * T(4)) + abb[7] * (abb[8] * T(4) + abb[14] * T(4))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_383_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl6 = DLog_W_6<T>(kin),dl10 = DLog_W_10<T>(kin),dl20 = DLog_W_20<T>(kin),dl29 = DLog_W_29<T>(kin),dl24 = DLog_W_24<T>(kin),dl14 = DLog_W_14<T>(kin),dl11 = DLog_W_11<T>(kin),dl17 = DLog_W_17<T>(kin),dl4 = DLog_W_4<T>(kin),dl18 = DLog_W_18<T>(kin),dl1 = DLog_W_1<T>(kin),dl2 = DLog_W_2<T>(kin),dl5 = DLog_W_5<T>(kin),dl16 = DLog_W_16<T>(kin),dl3 = DLog_W_3<T>(kin),dl19 = DLog_W_19<T>(kin),dl31 = DLog_W_31<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),spdl12 = SpDLog_f_4_383_W_12<T>(kin),spdl10 = SpDLog_f_4_383_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,63> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl6(t), rlog(v_path[0]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_1_3_2(kin) - f_1_3_2(kin_path), dl10(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(v_path[3]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_7(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl20(t), f_2_1_6(kin_path), f_2_1_8(kin_path), rlog(v_path[0] + v_path[1]), dl29(t) / kin_path.SqrtDelta, rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[18] / kin_path.W[18]), dl24(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl14(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl11(t), f_2_1_2(kin_path), dl17(t), f_2_1_4(kin_path), f_2_1_9(kin_path), dl4(t), dl18(t), dl1(t), dl2(t), dl5(t), dl16(t), dl3(t), dl19(t), dl31(t), dl26(t) / kin_path.SqrtDelta, f_2_2_1(kin_path), f_2_2_2(kin_path), f_2_2_3(kin_path), f_2_2_4(kin_path), f_2_2_5(kin_path), f_2_2_6(kin_path), f_2_2_7(kin_path), f_2_2_8(kin_path), f_2_2_9(kin_path), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[30] / kin_path.W[30]), dl30(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta}
;

        auto result = f_4_383_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr);

        return result;
    };
}

template <typename T> T f_4_383_abbreviated(const std::array<T,63>& abb)
{
using TR = typename T::value_type;
T z[245];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[34];
z[3] = abb[37];
z[4] = abb[39];
z[5] = abb[40];
z[6] = abb[41];
z[7] = abb[42];
z[8] = abb[43];
z[9] = abb[44];
z[10] = abb[4];
z[11] = abb[29];
z[12] = abb[8];
z[13] = abb[14];
z[14] = abb[20];
z[15] = abb[46];
z[16] = abb[60];
z[17] = abb[62];
z[18] = abb[21];
z[19] = abb[22];
z[20] = abb[23];
z[21] = abb[24];
z[22] = abb[25];
z[23] = abb[56];
z[24] = abb[57];
z[25] = abb[58];
z[26] = abb[59];
z[27] = abb[2];
z[28] = abb[38];
z[29] = abb[6];
z[30] = abb[10];
z[31] = abb[11];
z[32] = abb[12];
z[33] = bc<TR>[0];
z[34] = abb[32];
z[35] = abb[61];
z[36] = abb[7];
z[37] = abb[26];
z[38] = abb[5];
z[39] = abb[13];
z[40] = abb[16];
z[41] = abb[17];
z[42] = abb[18];
z[43] = abb[27];
z[44] = abb[28];
z[45] = abb[30];
z[46] = abb[31];
z[47] = abb[33];
z[48] = abb[35];
z[49] = abb[36];
z[50] = abb[47];
z[51] = abb[48];
z[52] = abb[49];
z[53] = abb[50];
z[54] = abb[51];
z[55] = abb[52];
z[56] = abb[53];
z[57] = abb[54];
z[58] = abb[55];
z[59] = abb[15];
z[60] = abb[19];
z[61] = bc<TR>[1];
z[62] = bc<TR>[3];
z[63] = bc<TR>[5];
z[64] = abb[45];
z[65] = bc<TR>[2];
z[66] = bc<TR>[4];
z[67] = bc<TR>[7];
z[68] = bc<TR>[8];
z[69] = bc<TR>[9];
z[70] = z[14] + -z[22];
z[71] = z[19] + z[70];
z[72] = z[23] + (T(5) * z[24]) / T(4) + -z[25] / T(4) + -z[26];
z[73] = -z[18] + z[20];
z[74] = -z[73] / T(2);
z[71] = z[21] / T(4) + (T(3) * z[71]) / T(4) + z[72] + -z[74];
z[75] = z[61] + -z[65];
z[75] = -z[71] + -z[75] / T(4);
z[75] = z[17] * z[75];
z[76] = -z[4] + z[9];
z[77] = (T(-41) * z[5]) / T(2) + T(31) * z[64];
z[77] = (T(-31) * z[2]) / T(4) + (T(-31) * z[3]) / T(5) + (T(-71) * z[6]) / T(20) + (T(31) * z[7]) / T(20) + (T(41) * z[8]) / T(10) + (T(-53) * z[28]) / T(20) + (T(73) * z[76]) / T(20) + z[77] / T(5);
z[78] = z[35] / T(2);
z[77] = z[15] / T(12) + (T(9) * z[17]) / T(4) + z[77] / T(9) + -z[78];
z[79] = int_to_imaginary<T>(1) * z[33];
z[77] = z[77] * z[79];
z[75] = z[75] + z[77];
z[77] = -z[19] + z[70];
z[77] = (T(5) * z[21]) / T(4) + z[72] + z[73] + z[77] / T(4);
z[77] = (T(9) * z[61]) / T(8) + (T(-5) * z[65]) / T(12) + z[77] / T(2);
z[77] = z[16] * z[77];
z[80] = z[10] + z[12] / T(4);
z[81] = T(2) * z[13];
z[82] = z[1] / T(2);
z[83] = -z[80] + z[81] + z[82];
z[83] = z[4] * z[83];
z[84] = z[21] + z[61] / T(24) + (T(-7) * z[65]) / T(6) + (T(3) * z[70]) / T(8) + z[72] + (T(7) * z[73]) / T(8);
z[84] = z[15] * z[84];
z[73] = z[21] + z[73];
z[85] = z[19] / T(2) + z[70];
z[72] = z[72] + (T(3) * z[73]) / T(4) + z[85] / T(2);
z[73] = z[61] / T(3) + (T(5) * z[65]) / T(4) + -z[72];
z[73] = z[73] * z[78];
z[70] = z[19] + -z[21] + z[70] / T(2) + z[74];
z[74] = z[60] * z[70];
z[78] = z[10] / T(2);
z[85] = z[12] + z[78];
z[86] = z[59] * z[85];
z[74] = z[74] + z[86];
z[86] = z[1] + z[13];
z[87] = z[10] + z[12];
z[88] = -z[86] + z[87];
z[88] = z[37] * z[88];
z[89] = z[10] + -z[13];
z[90] = z[11] * z[89];
z[91] = (T(5) * z[88]) / T(2) + -z[90];
z[92] = z[1] + -z[13];
z[93] = z[92] / T(2);
z[94] = z[12] / T(2);
z[95] = -z[10] + z[94];
z[96] = z[93] + z[95];
z[97] = z[28] / T(4);
z[98] = z[96] * z[97];
z[99] = z[10] + z[94];
z[100] = -z[86] / T(2) + -z[99];
z[101] = z[6] / T(2);
z[100] = z[100] * z[101];
z[102] = T(7) * z[10];
z[103] = T(3) * z[13];
z[104] = (T(29) * z[1]) / T(3) + z[12] + z[102] + -z[103];
z[105] = z[7] / T(8);
z[104] = z[104] * z[105];
z[106] = T(5) * z[10];
z[107] = T(3) * z[12];
z[108] = z[106] + z[107];
z[109] = -z[1] / T(3) + -z[13];
z[109] = z[108] + T(7) * z[109];
z[110] = z[9] / T(8);
z[109] = z[109] * z[110];
z[111] = z[10] / T(4);
z[112] = z[1] + z[94] + -z[111];
z[113] = z[8] / T(2);
z[112] = z[112] * z[113];
z[114] = z[92] / T(4);
z[115] = z[5] * z[114];
z[116] = T(3) * z[1];
z[117] = (T(-17) * z[10]) / T(6) + (T(25) * z[13]) / T(3) + z[116];
z[117] = (T(-4) * z[12]) / T(3) + z[117] / T(4);
z[117] = z[2] * z[117];
z[118] = T(-23) * z[1] + T(-29) * z[13] + z[106];
z[118] = -z[12] + z[118] / T(3);
z[118] = z[3] * z[118];
z[73] = z[73] + -z[74] / T(4) + z[75] / T(2) + z[77] + z[83] + z[84] + z[91] / T(3) + z[98] + z[100] + z[104] + z[109] + z[112] + z[115] + z[117] + z[118] / T(8);
z[73] = z[33] * z[73];
z[75] = T(11) * z[13];
z[77] = z[75] + -z[116];
z[83] = T(11) * z[12];
z[84] = T(17) * z[10];
z[91] = z[77] + -z[83] + -z[84];
z[98] = z[6] / T(4);
z[91] = z[91] * z[98];
z[100] = T(5) * z[1];
z[104] = T(9) * z[13];
z[109] = z[100] + -z[104];
z[112] = T(15) * z[10];
z[115] = T(9) * z[12];
z[117] = z[112] + z[115];
z[118] = z[109] + z[117];
z[119] = z[7] / T(4);
z[118] = z[118] * z[119];
z[120] = T(3) * z[71];
z[121] = z[17] * z[120];
z[122] = z[16] * z[120];
z[123] = T(9) * z[1];
z[124] = T(23) * z[13] + z[123];
z[125] = -z[84] + z[124];
z[126] = T(23) * z[12] + -z[125];
z[127] = z[2] / T(4);
z[126] = z[126] * z[127];
z[128] = T(9) * z[10];
z[129] = z[107] + z[128];
z[130] = T(17) * z[1] + z[103];
z[131] = -z[129] + z[130];
z[131] = z[97] * z[131];
z[132] = -z[1] + z[89];
z[133] = z[12] + z[132];
z[134] = z[3] / T(2);
z[133] = z[133] * z[134];
z[83] = -z[83] + z[125];
z[125] = z[4] / T(4);
z[135] = z[83] * z[125];
z[136] = z[1] + z[103];
z[137] = -z[117] + T(7) * z[136];
z[138] = z[9] / T(4);
z[139] = z[137] * z[138];
z[140] = T(4) * z[88];
z[91] = z[91] + z[118] + -z[121] + z[122] + -z[126] + z[131] + z[133] + z[135] + -z[139] + z[140];
z[118] = -(z[44] * z[91]);
z[126] = z[88] / T(2);
z[131] = z[122] + z[126];
z[133] = T(7) * z[1];
z[135] = z[104] + z[133];
z[141] = -z[107] + z[112] + -z[135];
z[141] = z[119] * z[141];
z[142] = z[1] + z[87];
z[142] = z[38] * z[142];
z[143] = z[4] * z[132];
z[143] = z[142] + z[143];
z[144] = z[28] * z[86];
z[145] = z[12] + -z[13];
z[146] = z[34] * z[145];
z[144] = z[144] + z[146];
z[144] = T(3) * z[144];
z[147] = T(3) * z[10];
z[148] = z[107] + z[147];
z[149] = z[92] + z[148];
z[149] = z[5] * z[149];
z[150] = z[75] + z[123];
z[151] = z[12] + z[150];
z[152] = -z[106] + z[151];
z[153] = z[127] * z[152];
z[154] = T(5) * z[86];
z[155] = z[10] + z[154];
z[155] = z[134] * z[155];
z[156] = T(2) * z[90];
z[139] = z[131] + -z[139] + z[141] + T(-3) * z[143] + z[144] + (T(-3) * z[149]) / T(4) + z[153] + z[155] + -z[156];
z[139] = z[29] * z[139];
z[141] = z[16] * z[70];
z[74] = z[74] + z[141];
z[141] = z[35] * z[120];
z[143] = (T(3) * z[74]) / T(2) + -z[90] + -z[141];
z[153] = -z[100] + -z[103] + z[129];
z[155] = -(z[97] * z[153]);
z[157] = T(5) * z[13];
z[158] = z[116] + z[157];
z[159] = z[158] / T(2);
z[160] = -z[99] + z[159];
z[161] = z[5] * z[160];
z[162] = (T(3) * z[161]) / T(2);
z[152] = z[125] * z[152];
z[163] = T(7) * z[13];
z[123] = -z[123] + -z[163];
z[164] = (T(5) * z[12]) / T(2);
z[123] = -z[10] + z[123] / T(2) + -z[164];
z[123] = z[101] * z[123];
z[165] = z[2] * z[12];
z[111] = z[12] + z[111];
z[166] = z[7] * z[111];
z[167] = -(z[3] * z[164]);
z[123] = (T(7) * z[88]) / T(2) + z[123] + z[143] + z[152] + z[155] + -z[162] + T(-6) * z[165] + T(-3) * z[166] + z[167];
z[123] = z[32] * z[123];
z[152] = T(3) * z[72];
z[155] = z[16] * z[152];
z[167] = z[90] + z[155];
z[168] = (T(3) * z[12]) / T(4);
z[169] = z[147] + z[168];
z[170] = T(19) * z[1];
z[171] = T(21) * z[13] + z[170];
z[172] = -z[169] + z[171] / T(4);
z[172] = z[9] * z[172];
z[173] = z[150] / T(2);
z[174] = (T(13) * z[12]) / T(2);
z[175] = -z[10] + z[173] + z[174];
z[176] = z[2] / T(2);
z[175] = z[175] * z[176];
z[177] = z[135] / T(2);
z[178] = (T(21) * z[12]) / T(2) + z[128] + -z[177];
z[179] = z[7] / T(2);
z[178] = z[178] * z[179];
z[180] = z[6] * z[132];
z[181] = z[3] * z[132];
z[162] = -z[140] + z[162] + z[167] + -z[172] + z[175] + z[178] + T(-3) * z[180] + (T(-5) * z[181]) / T(2);
z[162] = z[27] * z[162];
z[175] = z[17] * z[152];
z[178] = -z[155] + z[175];
z[181] = z[103] + z[170];
z[182] = (T(3) * z[12]) / T(2);
z[183] = z[147] + z[182];
z[184] = z[181] / T(2) + z[183];
z[184] = z[5] * z[184];
z[184] = T(-7) * z[90] + z[184];
z[185] = z[104] + z[170];
z[186] = z[128] + z[182];
z[187] = z[185] / T(2) + -z[186];
z[187] = z[179] * z[187];
z[188] = T(13) * z[13];
z[189] = z[170] + z[188];
z[190] = T(11) * z[10];
z[191] = -z[182] + z[189] / T(2) + -z[190];
z[192] = z[4] / T(2);
z[191] = z[191] * z[192];
z[170] = T(25) * z[13] + z[170];
z[193] = T(4) * z[10] + -z[170] / T(4);
z[194] = z[168] + z[193];
z[194] = z[6] * z[194];
z[193] = -z[168] + z[193];
z[193] = z[3] * z[193];
z[195] = z[2] * z[89];
z[172] = z[172] + z[178] + z[184] / T(2) + z[187] + -z[191] + z[193] + z[194] + -z[195];
z[184] = z[46] * z[172];
z[187] = z[2] * z[111];
z[193] = z[1] * z[5];
z[187] = z[187] + z[193];
z[194] = (T(-5) * z[10]) / T(4) + z[86] + -z[94];
z[194] = z[4] * z[194];
z[195] = (T(-5) * z[10]) / T(2) + -z[12] + z[136];
z[195] = z[101] * z[195];
z[196] = z[85] * z[134];
z[197] = z[1] * z[28];
z[194] = (T(3) * z[88]) / T(2) + z[90] + -z[187] + z[194] + z[195] + z[196] + z[197];
z[195] = T(3) * z[30];
z[194] = z[194] * z[195];
z[196] = z[5] / T(2);
z[198] = -z[3] + z[64] + -z[196];
z[199] = -z[28] + z[76];
z[200] = -z[6] + z[7];
z[198] = z[8] / T(10) + T(3) * z[16] + -z[127] + z[198] / T(5) + (T(3) * z[199]) / T(20) + z[200] / T(20);
z[198] = z[61] * z[198];
z[201] = z[16] * z[65];
z[198] = z[198] + T(-5) * z[201];
z[198] = z[61] * z[198];
z[201] = z[3] + -z[7];
z[202] = -z[12] + z[78];
z[201] = z[201] * z[202];
z[203] = z[78] + z[145];
z[204] = z[1] + z[203];
z[205] = z[4] + -z[5];
z[205] = z[204] * z[205];
z[206] = z[2] * z[85];
z[201] = z[74] + z[201] + z[205] + -z[206];
z[205] = z[15] * z[70];
z[206] = -z[201] + -z[205];
z[206] = z[42] * z[206];
z[198] = z[198] + z[206];
z[206] = -z[2] + -z[4];
z[207] = z[12] + z[89];
z[206] = z[206] * z[207];
z[207] = z[3] * z[86];
z[208] = z[1] * z[7];
z[206] = z[88] + z[142] + z[206] + -z[207] + z[208];
z[206] = z[36] * z[206];
z[207] = z[27] * z[72];
z[209] = z[29] * z[71];
z[210] = z[207] + z[209];
z[211] = prod_pow(z[65], 2);
z[212] = (T(-11) * z[61]) / T(4) + T(3) * z[65];
z[212] = z[61] * z[212];
z[212] = (T(-9) * z[66]) / T(2) + -z[210] + (T(-5) * z[211]) / T(4) + z[212];
z[212] = z[17] * z[212];
z[212] = z[206] + z[212];
z[213] = z[29] + -z[44];
z[213] = z[120] * z[213];
z[214] = z[27] * z[152];
z[215] = z[30] * z[70];
z[216] = T(2) * z[211];
z[217] = T(4) * z[61] + (T(-17) * z[65]) / T(2);
z[217] = z[61] * z[217];
z[213] = T(5) * z[66] + z[213] + z[214] + (T(3) * z[215]) / T(2) + z[216] + z[217];
z[213] = z[15] * z[213];
z[214] = T(5) * z[90];
z[217] = -(z[5] * z[100]);
z[218] = T(5) * z[12];
z[219] = -(z[2] * z[218]);
z[217] = -z[88] + z[214] + z[217] + z[219];
z[219] = z[89] + z[100];
z[219] = z[107] + z[219] / T(2);
z[219] = z[4] * z[219];
z[180] = (T(5) * z[180]) / T(2);
z[220] = z[7] * z[107];
z[144] = -z[144] + -z[180] + z[217] / T(2) + z[219] + z[220];
z[144] = z[31] * z[144];
z[217] = z[44] * z[120];
z[219] = (T(-3) * z[61]) / T(4) + T(2) * z[65];
z[219] = z[61] * z[219];
z[216] = (T(-3) * z[66]) / T(2) + -z[216] + z[217] + z[219];
z[216] = z[35] * z[216];
z[211] = T(6) * z[66] + (T(15) * z[211]) / T(4);
z[211] = z[16] * z[211];
z[118] = z[118] + z[123] + z[139] + z[144] + z[162] + z[184] + z[194] + (T(3) * z[198]) / T(2) + z[211] + T(3) * z[212] + z[213] + z[216];
z[118] = int_to_imaginary<T>(1) * z[118];
z[123] = z[3] + z[6] + -z[64];
z[139] = -z[5] + z[8];
z[76] = (T(-3) * z[76]) / T(2);
z[123] = T(5) * z[2] + -z[7] + z[76] + T(4) * z[123] + (T(-7) * z[139]) / T(2);
z[123] = z[62] * z[123];
z[73] = z[73] + z[118] + z[123];
z[73] = z[33] * z[73];
z[118] = z[82] + -z[89] + -z[94];
z[118] = z[6] * z[118];
z[123] = z[135] + -z[147] + z[218];
z[123] = z[119] * z[123];
z[139] = z[147] + z[218];
z[144] = T(11) * z[1] + z[104];
z[162] = z[139] + z[144];
z[184] = z[8] / T(4);
z[194] = z[162] * z[184];
z[103] = T(2) * z[1] + z[103];
z[198] = z[9] * z[103];
z[194] = z[194] + z[198];
z[211] = (T(5) * z[1]) / T(2);
z[212] = z[81] + z[211];
z[213] = -(z[5] * z[212]);
z[216] = -z[10] + z[107];
z[217] = -z[150] + -z[216];
z[217] = z[127] * z[217];
z[157] = -z[1] + z[157];
z[216] = -z[157] + z[216];
z[216] = z[125] * z[216];
z[219] = (T(5) * z[86]) / T(2);
z[220] = z[95] + -z[219];
z[220] = z[3] * z[220];
z[221] = (T(7) * z[13]) / T(2) + z[116];
z[222] = -(z[28] * z[221]);
z[123] = z[118] + -z[122] + z[123] + (T(-5) * z[146]) / T(2) + z[156] + z[194] + z[213] + z[216] + z[217] + z[220] + z[222];
z[123] = z[29] * z[123];
z[213] = z[102] + -z[116] + z[145];
z[216] = -(z[125] * z[213]);
z[217] = z[100] + z[163];
z[220] = z[129] + -z[217];
z[220] = z[97] * z[220];
z[222] = z[13] + z[116];
z[223] = z[222] / T(2);
z[224] = -z[183] + -z[223];
z[224] = z[196] * z[224];
z[217] = z[99] + z[217] / T(2);
z[217] = z[101] * z[217];
z[225] = z[2] * z[107];
z[226] = z[7] * z[10];
z[227] = (T(7) * z[12]) / T(2);
z[228] = z[3] * z[227];
z[143] = -z[126] + -z[143] + -z[146] + z[216] + z[217] + z[220] + z[224] + z[225] + (T(3) * z[226]) / T(4) + z[228];
z[143] = z[32] * z[143];
z[216] = z[74] + z[205];
z[217] = -z[90] + z[146];
z[220] = (T(3) * z[10]) / T(8);
z[224] = (T(3) * z[13]) / T(4) + -z[220];
z[168] = z[1] + -z[168] + z[224];
z[168] = z[5] * z[168];
z[225] = z[1] + z[12];
z[224] = -z[224] + -z[225];
z[224] = z[4] * z[224];
z[226] = z[97] * z[135];
z[228] = z[128] + -z[135];
z[228] = z[12] + z[228] / T(2);
z[228] = z[101] * z[228];
z[229] = z[3] / T(4);
z[230] = (T(3) * z[10]) / T(2) + -z[218];
z[230] = z[229] * z[230];
z[220] = z[12] + -z[220];
z[220] = z[2] * z[220];
z[168] = (T(-3) * z[166]) / T(2) + z[168] + (T(3) * z[216]) / T(4) + (T(9) * z[217]) / T(4) + z[220] + z[224] + z[226] + z[228] + z[230];
z[168] = z[31] * z[168];
z[216] = z[94] + z[147];
z[217] = -z[177] + z[216];
z[217] = z[179] * z[217];
z[220] = T(2) * z[10];
z[224] = z[94] + z[220];
z[226] = (T(3) * z[1]) / T(2);
z[81] = z[81] + z[226];
z[228] = z[81] + -z[224];
z[228] = z[4] * z[228];
z[230] = z[13] + z[82];
z[231] = z[28] * z[230];
z[232] = z[228] + z[231];
z[167] = z[167] + z[217] + z[232];
z[217] = z[116] + z[163];
z[233] = -z[183] + z[217] / T(2);
z[196] = z[196] * z[233];
z[233] = z[94] + z[212] + -z[220];
z[234] = z[6] * z[233];
z[144] = z[144] / T(4) + -z[169];
z[144] = z[9] * z[144];
z[169] = z[12] + z[147];
z[235] = z[103] + -z[169];
z[235] = z[8] * z[235];
z[236] = z[144] + z[235];
z[106] = -z[106] + z[173];
z[237] = -z[94] + z[106];
z[237] = z[176] * z[237];
z[238] = -z[102] + z[154];
z[239] = -z[12] + z[238];
z[239] = z[134] * z[239];
z[196] = z[167] + -z[175] + z[196] + z[234] + -z[236] + z[237] + z[239];
z[196] = z[27] * z[196];
z[234] = z[158] + -z[169];
z[237] = z[97] * z[234];
z[237] = z[146] + z[237];
z[239] = z[101] * z[160];
z[239] = z[90] + z[239];
z[100] = -z[13] + z[100];
z[240] = z[100] + z[148];
z[241] = z[5] / T(4);
z[240] = z[240] * z[241];
z[242] = -z[10] + z[182];
z[100] = -z[100] / T(2) + -z[242];
z[100] = z[100] * z[192];
z[243] = z[10] * z[127];
z[244] = -(z[7] * z[12]);
z[111] = -(z[3] * z[111]);
z[100] = z[100] + z[111] + z[237] + -z[239] + z[240] + z[243] + z[244];
z[100] = z[100] * z[195];
z[81] = z[5] * z[81];
z[111] = z[81] + z[118];
z[118] = z[135] + -z[169];
z[118] = z[118] * z[119];
z[118] = -z[111] + z[118] + -z[131] + -z[146];
z[131] = z[99] + z[219];
z[131] = z[3] * z[131];
z[240] = z[87] + -z[150];
z[240] = z[127] * z[240];
z[212] = z[28] * z[212];
z[217] = -z[87] + z[217];
z[217] = z[125] * z[217];
z[131] = -z[118] + z[131] + -z[194] + z[212] + z[217] + -z[240];
z[194] = z[15] * z[120];
z[212] = z[131] + z[194];
z[212] = z[36] * z[212];
z[240] = -z[210] + -z[215] / T(2);
z[243] = T(3) * z[15];
z[240] = z[240] * z[243];
z[244] = -(z[30] * z[141]);
z[100] = z[100] + z[123] + z[143] + z[168] + -z[196] + z[212] + z[240] + z[244];
z[100] = z[31] * z[100];
z[123] = z[157] / T(2) + -z[183];
z[123] = z[5] * z[123];
z[106] = z[106] + -z[164];
z[106] = z[2] * z[106];
z[106] = z[106] + z[123] + z[214];
z[123] = z[4] * z[233];
z[143] = z[147] + z[227];
z[157] = z[143] + -z[177];
z[157] = z[157] * z[179];
z[168] = z[15] * z[152];
z[212] = (T(-7) * z[10]) / T(2) + -z[94] + z[221];
z[212] = z[6] * z[212];
z[214] = z[12] + z[238];
z[214] = z[134] * z[214];
z[221] = T(2) * z[146];
z[106] = z[106] / T(2) + z[123] + z[157] + z[168] + -z[178] + z[212] + z[214] + -z[221] + -z[231] + -z[236];
z[106] = z[31] * z[106];
z[111] = z[111] + -z[232];
z[123] = -z[198] + z[235];
z[157] = z[2] * z[224];
z[168] = -z[12] + z[147];
z[178] = z[7] * z[168];
z[212] = -(z[3] * z[95]);
z[156] = -z[111] + z[123] + -z[156] + z[157] + z[178] + z[212] + z[221];
z[156] = z[29] * z[156];
z[102] = z[102] + -z[173] + z[182];
z[102] = z[102] * z[176];
z[157] = z[177] + -z[186];
z[157] = z[157] * z[179];
z[178] = z[183] + z[211];
z[178] = z[5] * z[178];
z[90] = -z[90] / T(2) + z[102] + z[157] + z[178] + z[180] + -z[191];
z[102] = (T(3) * z[72]) / T(2);
z[157] = z[15] + z[16];
z[178] = -(z[102] * z[157]);
z[180] = (T(-5) * z[86]) / T(4) + z[220];
z[180] = z[3] * z[180];
z[186] = -z[216] + z[223];
z[191] = z[8] * z[186];
z[90] = z[90] / T(2) + z[144] + z[175] + z[178] + z[180] + (T(3) * z[191]) / T(4);
z[90] = z[0] * z[90];
z[93] = z[93] + -z[99];
z[93] = z[93] * z[192];
z[144] = z[95] + z[159];
z[144] = z[134] * z[144];
z[175] = z[160] * z[176];
z[93] = -z[93] + -z[144] + z[175] + z[193] + z[208] + -z[239];
z[144] = -(z[93] * z[195]);
z[104] = -z[1] + z[104];
z[175] = z[94] + -z[104] / T(2) + -z[147];
z[175] = z[113] * z[175];
z[175] = z[175] + -z[198];
z[178] = z[10] + z[13];
z[178] = T(2) * z[178];
z[180] = -z[94] + -z[178];
z[180] = z[2] * z[180];
z[75] = z[75] + z[133];
z[191] = -z[75] / T(2) + z[99];
z[191] = z[101] * z[191];
z[133] = z[133] + z[188];
z[188] = z[133] / T(2);
z[95] = -z[95] + -z[188];
z[95] = z[95] * z[134];
z[193] = -z[13] + z[226];
z[193] = z[5] * z[193];
z[95] = z[95] + -z[167] + -z[175] + z[180] + z[191] + z[193];
z[95] = z[32] * z[95];
z[167] = -(z[15] * z[72]);
z[93] = -z[93] + z[167];
z[79] = z[79] * z[93];
z[93] = -(z[30] * z[72]);
z[93] = z[93] + z[207];
z[93] = z[93] * z[243];
z[79] = T(3) * z[79] + z[90] + z[93] + z[95] + z[106] + z[144] + z[156] + z[196];
z[79] = z[0] * z[79];
z[90] = z[147] + z[174] + -z[177];
z[90] = z[90] * z[179];
z[93] = T(4) * z[13];
z[95] = z[93] + z[226];
z[95] = z[5] * z[95];
z[106] = z[178] + z[227];
z[106] = z[2] * z[106];
z[144] = z[10] + z[182];
z[156] = (T(7) * z[86]) / T(2) + z[144];
z[156] = z[101] * z[156];
z[167] = -z[144] + z[188];
z[167] = z[134] * z[167];
z[82] = z[13] + -z[82];
z[82] = z[28] * z[82];
z[82] = z[82] + z[90] + z[95] + z[106] + -z[140] + z[155] + z[156] + z[167] + z[175] + -z[228];
z[82] = z[27] * z[82];
z[90] = T(3) * z[158];
z[87] = -z[87] + z[90];
z[87] = z[87] * z[127];
z[95] = T(13) * z[10] + z[218];
z[106] = -z[1] + -z[95] + z[163];
z[106] = z[98] * z[106];
z[127] = z[1] + z[10];
z[127] = T(3) * z[127] + z[218];
z[127] = z[7] * z[127];
z[90] = -z[90] + z[95];
z[90] = z[90] * z[229];
z[95] = -z[13] + z[211];
z[95] = z[28] * z[95];
z[81] = z[81] + z[87] + z[90] + z[95] + z[106] + z[123] + z[126] + z[127] + z[194] + z[217];
z[81] = z[36] * z[81];
z[87] = -z[89] + -z[164] + -z[226];
z[87] = z[4] * z[87];
z[75] = z[75] + -z[129];
z[75] = z[75] * z[97];
z[89] = z[13] + -z[220];
z[89] = T(2) * z[89] + -z[164];
z[89] = z[2] * z[89];
z[90] = T(-7) * z[12] + z[133] + -z[190];
z[90] = z[90] * z[229];
z[95] = -z[104] + z[117];
z[95] = z[95] * z[138];
z[75] = z[75] + z[87] + z[89] + z[90] + z[95] + -z[118] + -z[121] + -z[235];
z[75] = z[29] * z[75];
z[87] = z[74] / T(2);
z[89] = T(3) * z[88];
z[90] = z[89] + -z[161];
z[95] = z[125] * z[234];
z[104] = -z[169] + z[222];
z[104] = z[97] * z[104];
z[106] = -z[144] + -z[223];
z[106] = z[101] * z[106];
z[94] = -(z[3] * z[94]);
z[90] = z[87] + z[90] / T(2) + z[94] + z[95] + z[104] + z[106] + T(-2) * z[165] + -z[166];
z[90] = z[90] * z[195];
z[94] = z[4] * z[213];
z[95] = z[28] * z[153];
z[83] = z[2] * z[83];
z[84] = z[84] + -z[151];
z[84] = z[6] * z[84];
z[83] = -z[83] + z[84] + -z[94] + z[95];
z[84] = (T(3) * z[71]) / T(2);
z[94] = z[17] + z[35];
z[95] = z[94] + -z[157];
z[95] = z[84] * z[95];
z[104] = T(-33) * z[12] + -z[109] + -z[112];
z[104] = z[104] * z[105];
z[105] = -z[132] + z[218];
z[105] = z[105] * z[229];
z[106] = z[110] * z[137];
z[110] = z[5] * z[230];
z[83] = z[83] / T(8) + z[88] + z[95] + z[104] + z[105] + z[106] + T(-3) * z[110];
z[83] = z[32] * z[83];
z[88] = z[30] * z[71];
z[88] = z[88] + z[209];
z[95] = z[35] * z[88];
z[75] = z[75] + z[81] + z[82] + z[83] + z[90] + T(-3) * z[95];
z[75] = z[32] * z[75];
z[81] = -z[91] + z[141] + -z[194];
z[81] = z[43] * z[81];
z[82] = z[115] + z[128];
z[83] = z[82] + z[189];
z[90] = z[5] * z[83];
z[91] = z[150] + z[169];
z[91] = z[2] * z[91];
z[90] = z[90] + z[91];
z[91] = z[108] + z[222];
z[95] = z[9] * z[91];
z[104] = z[168] + -z[211];
z[104] = z[4] * z[104];
z[105] = -z[12] + -z[128] + -z[135];
z[105] = z[105] * z[119];
z[106] = z[147] + z[154];
z[106] = z[106] * z[134];
z[110] = -(z[113] * z[162]);
z[112] = z[28] * z[219];
z[90] = z[90] / T(4) + (T(-3) * z[95]) / T(4) + z[104] + z[105] + z[106] + z[110] + z[112] + z[122] + z[146] / T(2);
z[90] = z[90] / T(2) + (T(3) * z[142]) / T(2);
z[104] = prod_pow(z[29], 2);
z[90] = z[90] * z[104];
z[83] = z[83] * z[241];
z[105] = T(13) * z[12];
z[106] = -z[105] + z[128] + -z[181];
z[106] = z[106] * z[125];
z[105] = -z[105] + -z[128] + -z[185];
z[105] = z[105] * z[119];
z[110] = -z[129] + z[170];
z[97] = z[97] * z[110];
z[82] = -z[82] + z[170];
z[82] = z[82] * z[229];
z[110] = -z[12] + z[128] + -z[171];
z[110] = z[110] * z[184];
z[112] = z[2] * z[145];
z[82] = z[82] + z[83] + z[97] + z[105] + z[106] + z[110] + z[112] + z[122] + (T(7) * z[146]) / T(2);
z[82] = z[47] * z[82];
z[83] = z[45] * z[172];
z[97] = -(z[29] * z[131]);
z[105] = -z[220] + -z[227];
z[105] = z[2] * z[105];
z[106] = -(z[7] * z[139]);
z[110] = -(z[3] * z[99]);
z[105] = z[105] + z[106] + z[110] + -z[111] + -z[123] + z[140];
z[105] = z[27] * z[105];
z[106] = z[139] + -z[158];
z[106] = z[106] * z[176];
z[110] = -(z[192] * z[234]);
z[89] = -z[89] + z[106] + z[110];
z[106] = z[92] + z[169];
z[98] = z[98] * z[106];
z[110] = z[229] * z[234];
z[89] = z[89] / T(2) + z[98] + z[110] + -z[197] + -z[208];
z[89] = z[89] * z[195];
z[88] = -(z[88] * z[243]);
z[88] = z[88] + z[89] + z[97] + z[105] + (T(-3) * z[206]) / T(2);
z[88] = z[36] * z[88];
z[89] = -z[1] + z[203];
z[89] = z[28] * z[89];
z[97] = z[86] + -z[202];
z[97] = z[97] * z[101];
z[98] = z[134] * z[202];
z[105] = z[4] * z[225];
z[110] = z[10] * z[119];
z[87] = -z[87] + z[89] / T(2) + z[97] + z[98] + z[105] + z[110] + -z[187];
z[87] = z[30] * z[87];
z[89] = z[2] * z[234];
z[89] = z[89] + -z[149];
z[97] = z[148] + -z[158];
z[97] = z[97] * z[229];
z[98] = z[4] + z[7];
z[98] = z[98] * z[225];
z[89] = z[89] / T(4) + z[97] + z[98] + -z[237];
z[89] = z[29] * z[89];
z[97] = z[159] + z[242];
z[97] = z[2] * z[97];
z[98] = -(z[28] * z[96]);
z[97] = z[97] + z[98] + z[161];
z[98] = z[6] + z[7];
z[98] = z[98] * z[225];
z[105] = -(z[134] * z[160]);
z[97] = z[97] / T(2) + z[98] + z[105];
z[97] = z[27] * z[97];
z[87] = z[87] / T(2) + z[89] + z[97];
z[87] = z[87] * z[195];
z[89] = z[92] + z[108];
z[92] = z[3] + -z[8];
z[89] = z[89] * z[92];
z[91] = z[2] * z[91];
z[92] = z[10] + z[107] + z[222];
z[92] = z[4] * z[92];
z[97] = z[28] * z[106];
z[89] = z[89] + z[91] + z[92] + -z[95] + z[97];
z[91] = z[15] + z[94];
z[71] = z[71] * z[91];
z[71] = z[71] + z[89] / T(4);
z[71] = z[49] * z[71];
z[89] = z[99] + -z[223];
z[89] = z[5] * z[89];
z[91] = -(z[2] * z[186]);
z[92] = -(z[6] * z[96]);
z[89] = z[89] + z[91] + z[92];
z[91] = z[15] + -z[17];
z[72] = -(z[72] * z[91]);
z[80] = z[80] + -z[114];
z[91] = z[3] + -z[9];
z[80] = z[80] * z[91];
z[91] = z[113] * z[186];
z[72] = z[72] + z[80] + z[89] / T(2) + z[91];
z[72] = z[48] * z[72];
z[80] = -z[16] + z[17];
z[80] = z[67] * z[80];
z[71] = -z[71] + -z[72] + z[80];
z[72] = T(2) * z[103];
z[80] = z[72] + -z[147];
z[80] = z[50] * z[80];
z[72] = z[72] + -z[169];
z[89] = z[56] * z[72];
z[91] = T(4) * z[1] + T(6) * z[13] + -z[147];
z[91] = z[53] * z[91];
z[92] = prod_pow(z[65], 3);
z[94] = z[50] + z[53];
z[95] = -(z[12] * z[94]);
z[96] = prod_pow(z[61], 2);
z[97] = T(3) * z[66] + z[96];
z[97] = z[61] * z[97];
z[80] = (T(13) * z[68]) / T(8) + (T(-259) * z[69]) / T(12) + z[80] + z[89] + z[91] + (T(-13) * z[92]) / T(6) + z[95] + z[97];
z[80] = z[16] * z[80];
z[77] = -z[77] / T(2) + -z[147] + z[182];
z[77] = z[28] * z[77];
z[89] = (T(17) * z[12]) / T(2) + z[130] / T(2) + z[147];
z[89] = z[6] * z[89];
z[77] = -z[77] + z[89];
z[89] = -z[109] / T(2) + z[147] + -z[164];
z[89] = z[7] * z[89];
z[91] = (T(-7) * z[136]) / T(2) + z[147] + z[164];
z[95] = -(z[8] * z[91]);
z[97] = z[124] / T(2);
z[98] = -z[97] + z[183];
z[98] = z[5] * z[98];
z[99] = (T(9) * z[12]) / T(2) + -z[147];
z[97] = -z[97] + -z[99];
z[97] = z[2] * z[97];
z[86] = z[12] + z[86];
z[86] = z[3] * z[86];
z[86] = -z[77] + z[86] + z[89] + z[95] + z[97] + z[98];
z[86] = z[86] / T(2) + -z[155];
z[86] = z[39] * z[86];
z[89] = z[210] + z[215] / T(4);
z[89] = z[89] * z[195];
z[95] = z[39] * z[152];
z[97] = prod_pow(z[27], 2);
z[98] = z[97] * z[102];
z[89] = z[89] + -z[95] + -z[98];
z[93] = -z[93] + -z[116] + z[224];
z[94] = z[56] + z[94];
z[95] = -(z[93] * z[94]);
z[84] = z[84] * z[104];
z[98] = (T(3) * z[40]) / T(2);
z[102] = z[70] * z[98];
z[104] = T(11) * z[66] + (T(17) * z[96]) / T(6);
z[104] = z[61] * z[104];
z[95] = T(8) * z[67] + T(2) * z[68] + (T(-77) * z[69]) / T(3) + z[84] + z[89] + (T(-8) * z[92]) / T(3) + z[95] + z[102] + z[104] / T(2);
z[95] = z[15] * z[95];
z[102] = -z[2] + z[7];
z[78] = z[78] * z[102];
z[102] = -z[6] + z[28];
z[102] = z[102] * z[204];
z[70] = z[35] * z[70];
z[85] = z[3] * z[85];
z[70] = z[70] + -z[74] + z[78] + z[85] + z[102] + z[205];
z[70] = z[41] * z[70];
z[74] = -z[173] + z[183];
z[74] = z[5] * z[74];
z[78] = z[143] + z[177];
z[78] = z[7] * z[78];
z[85] = -z[99] + -z[173];
z[85] = z[2] * z[85];
z[74] = z[74] + -z[77] + z[78] + z[85];
z[77] = -(z[91] * z[113]);
z[78] = z[216] + -z[219];
z[78] = z[3] * z[78];
z[74] = z[74] / T(2) + z[77] + z[78] + -z[155];
z[74] = z[74] * z[97];
z[77] = -(z[47] * z[120]);
z[78] = -(z[94] * z[230]);
z[85] = z[66] + z[96] / T(6);
z[85] = z[61] * z[85];
z[77] = T(2) * z[67] + (T(-3) * z[68]) / T(8) + (T(-133) * z[69]) / T(48) + z[77] + z[78] + z[85] + z[89] + z[92] / T(2);
z[77] = z[35] * z[77];
z[78] = -(z[15] * z[93]);
z[72] = z[16] * z[72];
z[85] = z[17] * z[103];
z[89] = z[35] * z[230];
z[72] = -z[72] + -z[78] + z[85] + z[89];
z[78] = -z[51] + z[52] + -z[54] + -z[55] + z[57] + z[58];
z[72] = z[72] * z[78];
z[78] = z[98] * z[201];
z[85] = -(z[94] * z[103]);
z[89] = prod_pow(z[61], 3);
z[84] = (T(-3) * z[68]) / T(4) + (T(-785) * z[69]) / T(144) + z[84] + z[85] + z[89] / T(2) + z[92];
z[84] = z[17] * z[84];
z[85] = -z[5] + z[8] + T(2) * z[64];
z[76] = (T(5) * z[2]) / T(2) + T(2) * z[3] + (T(3) * z[28]) / T(2) + z[76] + -z[85] + z[101] + -z[179];
z[76] = z[61] * z[62] * z[76];
z[85] = z[2] + (T(4) * z[3]) / T(5) + (T(-2) * z[85]) / T(5) + (T(-3) * z[199]) / T(5) + -z[200] / T(5);
z[85] = z[63] * z[85];
z[76] = z[76] + T(6) * z[85];
z[85] = int_to_imaginary<T>(3);
z[76] = z[76] * z[85];
return (T(3) * z[70]) / T(2) + T(-3) * z[71] + z[72] + z[73] + z[74] / T(2) + z[75] + z[76] + z[77] + z[78] + z[79] + z[80] + z[81] + z[82] + z[83] + z[84] + z[86] + z[87] + z[88] + z[90] + z[95] + z[100];
}



template IntegrandConstructorType<double> f_4_383_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_383_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_383_construct (const Kin<qd_real>&);
#endif

}