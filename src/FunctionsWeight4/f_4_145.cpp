#include "f_4_145.h"

namespace PentagonFunctions {

template <typename T> T f_4_145_abbreviated (const std::array<T,40>&);



template <typename T> IntegrandConstructorType<T> f_4_145_construct (const Kin<T>& kin) {
    return [&kin, 
            dl4 = DLog_W_4<T>(kin),dl25 = DLog_W_25<T>(kin),dl30 = DLog_W_30<T>(kin),dl16 = DLog_W_16<T>(kin),dl29 = DLog_W_29<T>(kin),dl2 = DLog_W_2<T>(kin),dl17 = DLog_W_17<T>(kin),dl26 = DLog_W_26<T>(kin),dl28 = DLog_W_28<T>(kin),dl27 = DLog_W_27<T>(kin),dl8 = DLog_W_8<T>(kin),dl13 = DLog_W_13<T>(kin),dl19 = DLog_W_19<T>(kin),dl1 = DLog_W_1<T>(kin),dl5 = DLog_W_5<T>(kin),dl3 = DLog_W_3<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,40> abbr = 
            {dl4(t), rlog(kin.W[18] / kin_path.W[18]), rlog(v_path[0]), rlog(v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_1(kin_path), f_2_1_9(kin_path), rlog(kin.W[12] / kin_path.W[12]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[17] / kin_path.W[17]), dl25(t), rlog(-v_path[1] + v_path[3] + v_path[4]), dl30(t) / kin_path.SqrtDelta, f_1_3_2(kin) - f_1_3_2(kin_path), f_2_1_13(kin_path), f_2_1_15(kin_path), rlog(v_path[0] + v_path[4]), f_1_3_3(kin) - f_1_3_3(kin_path), dl16(t), rlog(kin.W[7] / kin_path.W[7]), dl29(t) / kin_path.SqrtDelta, f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl2(t), rlog(-v_path[2] + v_path[0] + v_path[4]), dl17(t), dl26(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl8(t), rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[19] / kin_path.W[19]), dl13(t), dl19(t), dl1(t), dl5(t), dl3(t), dl18(t), dl20(t)}
;

        auto result = f_4_145_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_145_abbreviated(const std::array<T,40>& abb)
{
using TR = typename T::value_type;
T z[148];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[26];
z[13] = abb[34];
z[14] = abb[35];
z[15] = abb[36];
z[16] = abb[37];
z[17] = abb[38];
z[18] = abb[39];
z[19] = abb[12];
z[20] = abb[25];
z[21] = abb[19];
z[22] = abb[30];
z[23] = abb[11];
z[24] = abb[24];
z[25] = abb[15];
z[26] = abb[16];
z[27] = abb[17];
z[28] = abb[22];
z[29] = abb[23];
z[30] = abb[33];
z[31] = abb[14];
z[32] = abb[21];
z[33] = abb[27];
z[34] = abb[28];
z[35] = abb[18];
z[36] = abb[31];
z[37] = abb[32];
z[38] = abb[29];
z[39] = abb[13];
z[40] = abb[20];
z[41] = bc<TR>[1];
z[42] = bc<TR>[3];
z[43] = bc<TR>[5];
z[44] = bc<TR>[2];
z[45] = bc<TR>[9];
z[46] = bc<TR>[4];
z[47] = bc<TR>[7];
z[48] = bc<TR>[8];
z[49] = T(7) * z[44];
z[50] = T(11) * z[11];
z[51] = T(3) * z[1];
z[52] = T(3) * z[9] + z[51];
z[53] = z[10] + -z[49] + z[50] + z[52];
z[54] = (T(5) * z[40]) / T(4);
z[55] = T(3) * z[36];
z[56] = z[37] / T(4);
z[53] = z[53] / T(4) + -z[54] + -z[55] + z[56];
z[53] = z[16] * z[53];
z[57] = (T(9) * z[9]) / T(2);
z[58] = z[10] / T(2);
z[49] = (T(7) * z[1]) / T(2) + -z[8] + (T(-13) * z[11]) / T(2) + T(-3) * z[37] + z[49] + -z[55] + z[57] + -z[58];
z[59] = z[17] / T(2);
z[49] = z[49] * z[59];
z[60] = z[9] / T(2);
z[61] = z[1] / T(2);
z[62] = z[60] + z[61];
z[63] = (T(3) * z[40]) / T(4) + z[56];
z[64] = T(2) * z[11];
z[65] = z[36] + z[44] / T(8) + z[58] + z[62] + -z[63] + -z[64];
z[65] = z[18] * z[65];
z[66] = z[11] / T(2);
z[60] = z[60] + z[66];
z[67] = -z[8] + z[58] + z[60] + -z[61];
z[68] = z[0] * z[67];
z[69] = z[10] + -z[11];
z[70] = z[1] + z[9];
z[71] = z[69] + z[70];
z[71] = z[71] / T(2);
z[72] = -z[40] + z[71];
z[73] = z[21] * z[72];
z[74] = z[68] + -z[73];
z[75] = (T(3) * z[40]) / T(2);
z[76] = -z[1] + z[11];
z[77] = (T(-23) * z[44]) / T(2) + -z[76];
z[77] = z[9] + z[58] + -z[75] + z[77] / T(2);
z[77] = z[13] * z[77];
z[78] = T(2) * z[36];
z[79] = T(2) * z[10] + -z[78];
z[80] = T(2) * z[1];
z[81] = T(2) * z[9];
z[82] = T(4) * z[40] + (T(33) * z[44]) / T(8) + -z[79] + -z[80] + -z[81];
z[82] = z[22] * z[82];
z[83] = T(7) * z[36];
z[84] = T(3) * z[11];
z[85] = z[1] + -z[84];
z[57] = (T(5) * z[10]) / T(2) + T(5) * z[44] + -z[57] + z[83] + (T(3) * z[85]) / T(2);
z[57] = z[37] + z[57] / T(2);
z[57] = z[14] * z[57];
z[85] = z[31] + z[35];
z[86] = z[38] + -z[39];
z[87] = z[86] / T(4);
z[87] = z[85] * z[87];
z[88] = z[85] / T(2);
z[89] = z[32] * z[88];
z[90] = z[34] * z[88];
z[91] = z[89] + z[90];
z[92] = z[10] + z[11];
z[93] = -z[70] + z[92];
z[94] = z[24] * z[93];
z[95] = (T(3) * z[15]) / T(2);
z[96] = -z[30] + -z[95];
z[96] = z[44] * z[96];
z[76] = z[23] * z[76];
z[76] = T(3) * z[76];
z[49] = z[49] + z[53] + z[57] + z[65] + -z[74] / T(2) + z[76] + z[77] + z[82] + z[87] + -z[91] + -z[94] / T(4) + z[96];
z[49] = z[7] * z[49];
z[53] = z[34] + z[38];
z[57] = z[42] * z[53];
z[49] = z[49] + -z[57];
z[49] = z[7] * z[49];
z[65] = z[36] + z[37];
z[74] = z[65] + z[81];
z[77] = T(3) * z[40];
z[82] = z[11] + -z[74] + z[77] + -z[80];
z[87] = z[16] * z[82];
z[96] = z[13] * z[82];
z[87] = z[87] + z[96];
z[97] = z[11] + z[70];
z[98] = T(3) * z[10];
z[99] = z[97] + -z[98];
z[99] = z[65] + z[99] / T(2);
z[100] = z[18] * z[99];
z[101] = z[14] * z[99];
z[102] = z[100] + z[101];
z[103] = -z[10] + -z[40] + z[65] + z[70];
z[104] = T(3) * z[22];
z[103] = z[103] * z[104];
z[105] = T(3) * z[73];
z[106] = z[17] * z[99];
z[107] = (T(3) * z[85]) / T(2);
z[108] = z[39] * z[107];
z[109] = z[38] * z[107];
z[110] = z[32] * z[107];
z[87] = T(-2) * z[87] + z[102] + -z[103] + -z[105] + -z[106] + z[108] + -z[109] + z[110];
z[87] = z[19] * z[87];
z[108] = z[9] + z[11];
z[51] = -z[10] + z[51] + -z[108];
z[51] = z[51] / T(2) + z[65];
z[51] = z[13] * z[51];
z[84] = -z[10] + -z[70] + z[84];
z[84] = z[65] + z[84] / T(2);
z[84] = z[17] * z[84];
z[111] = z[18] / T(2);
z[112] = z[93] * z[111];
z[113] = z[38] * z[88];
z[114] = -z[10] + z[108];
z[115] = -z[1] + z[114];
z[116] = z[12] * z[115];
z[117] = -z[113] + z[116] / T(2);
z[118] = z[94] / T(2);
z[115] = z[115] / T(2);
z[119] = z[14] * z[115];
z[51] = z[51] + -z[84] + z[91] + -z[112] + z[117] + z[118] + -z[119];
z[51] = T(3) * z[51];
z[84] = -(z[29] * z[51]);
z[52] = z[52] + -z[92];
z[52] = z[52] / T(2);
z[92] = z[36] + z[52];
z[92] = z[27] * z[92];
z[112] = z[27] * z[37];
z[119] = z[27] * z[40];
z[120] = prod_pow(z[44], 2);
z[92] = -z[92] + -z[112] + T(2) * z[119] + z[120] / T(2);
z[121] = z[13] + z[16];
z[92] = z[92] * z[121];
z[122] = z[32] + z[39];
z[122] = -z[122] / T(2);
z[123] = z[27] * z[85];
z[122] = z[122] * z[123];
z[69] = z[69] + -z[70];
z[124] = z[36] + z[69] / T(2);
z[125] = z[27] * z[124];
z[125] = z[120] + z[125];
z[126] = -(z[15] * z[125]);
z[71] = z[27] * z[71];
z[71] = z[71] + -z[119];
z[71] = z[21] * z[71];
z[71] = z[71] + z[92] + z[122] + z[126];
z[92] = -z[1] + z[64];
z[122] = T(3) * z[8];
z[74] = z[74] + z[92] + -z[122];
z[126] = T(2) * z[17];
z[127] = z[74] * z[126];
z[128] = T(2) * z[74];
z[128] = z[16] * z[128];
z[127] = -z[110] + z[127] + z[128];
z[128] = z[15] * z[124];
z[129] = z[13] * z[99];
z[130] = T(3) * z[128] + z[129];
z[131] = T(6) * z[30];
z[132] = -z[8] + -z[36] + z[108];
z[133] = z[131] * z[132];
z[134] = (T(3) * z[116]) / T(2);
z[133] = -z[127] + z[130] + z[133] + z[134];
z[133] = z[2] * z[133];
z[79] = -z[70] + z[79];
z[135] = -(z[27] * z[79]);
z[135] = (T(3) * z[112]) / T(2) + -z[119] / T(2) + -z[120] / T(4) + z[135];
z[135] = z[104] * z[135];
z[136] = z[27] * z[69];
z[112] = -z[112] + z[119] + (T(3) * z[120]) / T(2) + z[136];
z[119] = z[97] + z[98];
z[120] = T(5) * z[36] + -z[119] / T(2);
z[136] = T(2) * z[37];
z[137] = z[120] + z[136];
z[138] = z[2] * z[137];
z[112] = (T(3) * z[112]) / T(2) + z[138];
z[112] = z[18] * z[112];
z[138] = z[17] * z[74];
z[139] = T(3) * z[30];
z[132] = z[132] * z[139];
z[132] = z[132] + -z[138];
z[140] = -z[37] + z[77];
z[141] = -z[36] + z[97];
z[142] = -z[140] + T(2) * z[141];
z[143] = z[16] * z[142];
z[144] = T(4) * z[36] + z[37] + -z[97];
z[145] = z[14] * z[144];
z[96] = z[96] + z[132] + -z[143] + z[145];
z[140] = T(-2) * z[70] + z[140];
z[140] = z[104] * z[140];
z[142] = z[18] * z[142];
z[96] = T(-2) * z[96] + z[140] + z[142];
z[96] = z[3] * z[96];
z[81] = T(-6) * z[8] + T(11) * z[36] + T(5) * z[37] + -z[81] + -z[92] + z[98];
z[81] = z[2] * z[81];
z[81] = z[81] + T(3) * z[125];
z[81] = z[14] * z[81];
z[92] = z[14] + z[18];
z[125] = T(-11) * z[13] + z[16] + T(13) * z[17];
z[125] = -z[22] + z[125] / T(3);
z[92] = (T(-11) * z[53]) / T(90) + z[92] / T(3) + z[125] / T(2);
z[125] = prod_pow(z[7], 2);
z[92] = z[92] * z[125];
z[140] = T(2) * z[30];
z[142] = T(3) * z[14];
z[143] = z[13] + T(2) * z[16] + z[126] + -z[140] + -z[142];
z[146] = T(3) * z[46];
z[143] = z[143] * z[146];
z[147] = z[2] * z[85];
z[123] = -z[123] + z[147];
z[123] = z[34] * z[123];
z[71] = T(3) * z[71] + z[81] + z[84] + z[87] + z[92] / T(2) + z[96] + z[112] + (T(3) * z[123]) / T(2) + z[133] + z[135] + z[143];
z[71] = z[7] * z[71];
z[81] = z[14] + (T(5) * z[18]) / T(2) + (T(-3) * z[53]) / T(20) + z[95] + T(2) * z[121] + z[126] + -z[131];
z[81] = z[7] * z[41] * z[81];
z[84] = -z[13] + z[14];
z[84] = z[7] * z[44] * z[84];
z[57] = z[57] + z[84];
z[57] = T(3) * z[57] + z[81];
z[57] = z[41] * z[57];
z[53] = z[43] * z[53];
z[53] = (T(36) * z[53]) / T(5) + z[57] + z[71];
z[53] = int_to_imaginary<T>(1) * z[53];
z[57] = -z[50] + z[70] + -z[98];
z[57] = z[37] + z[57] / T(2) + z[83];
z[57] = z[16] * z[57];
z[57] = z[57] + -z[105] + z[130];
z[71] = -z[10] + z[36];
z[54] = -z[54] + -z[56] + z[70] / T(2) + -z[71];
z[54] = z[54] * z[104];
z[56] = z[36] + z[63] + -z[119] / T(4);
z[56] = z[18] * z[56];
z[63] = z[37] + z[120] / T(2);
z[63] = z[14] * z[63];
z[81] = z[34] * z[85];
z[81] = (T(3) * z[81]) / T(4);
z[84] = z[32] * z[85];
z[84] = (T(3) * z[84]) / T(4);
z[92] = (T(3) * z[85]) / T(4);
z[95] = z[39] * z[92];
z[54] = z[54] + z[56] + z[57] / T(2) + z[63] + z[81] + z[84] + z[95] + z[132];
z[54] = z[3] * z[54];
z[56] = T(3) * z[68];
z[57] = -z[8] + z[65] + z[114];
z[63] = z[57] * z[139];
z[95] = z[34] * z[107];
z[63] = z[56] + z[63] + z[95] + -z[102] + -z[127] + z[129];
z[96] = -(z[2] * z[63]);
z[54] = z[54] + -z[87] + z[96];
z[54] = z[3] * z[54];
z[87] = z[16] * z[74];
z[96] = -z[84] + z[87] + -z[129] / T(2);
z[102] = -z[76] + z[81] + (T(3) * z[94]) / T(4);
z[105] = -z[1] / T(4) + (T(3) * z[10]) / T(4) + z[65] + (T(5) * z[108]) / T(4) + -z[122];
z[105] = z[14] * z[105];
z[107] = T(5) * z[9] + -z[98];
z[112] = T(5) * z[1] + z[107];
z[114] = T(7) * z[11] + -z[112];
z[114] = z[65] + z[114] / T(2);
z[114] = z[59] * z[114];
z[98] = T(-5) * z[11] + z[70] + z[98];
z[98] = -z[65] + z[98] / T(4);
z[120] = -(z[18] * z[98]);
z[116] = (T(3) * z[116]) / T(4);
z[123] = -(z[38] * z[92]);
z[105] = -z[96] + z[102] + z[105] + z[114] + z[116] + z[120] + z[123];
z[105] = z[4] * z[105];
z[114] = -z[2] + z[3];
z[63] = z[63] * z[114];
z[99] = z[16] * z[99];
z[106] = z[99] + z[106] + z[109];
z[80] = z[65] + z[80] + -z[108];
z[114] = T(2) * z[13];
z[120] = z[80] * z[114];
z[123] = z[14] * z[80];
z[100] = z[100] + -z[106] + z[110] + z[120] + z[123] + z[134];
z[110] = -(z[19] * z[100]);
z[120] = z[64] + z[65] + -z[70];
z[127] = z[18] * z[120];
z[126] = z[120] * z[126];
z[127] = z[126] + z[127];
z[80] = z[13] * z[80];
z[74] = -(z[14] * z[74]);
z[74] = z[74] + z[76] + z[80] + z[87] + -z[127];
z[76] = int_to_imaginary<T>(1) * z[7];
z[74] = z[74] * z[76];
z[80] = (T(3) * z[94]) / T(2) + z[95];
z[87] = z[80] + z[99] + z[129];
z[94] = -z[87] + z[101] + z[127];
z[94] = z[20] * z[94];
z[63] = z[63] + T(2) * z[74] + z[94] + z[105] + z[110];
z[63] = z[4] * z[63];
z[52] = T(-2) * z[40] + z[52] + z[65];
z[52] = z[52] * z[121];
z[74] = z[39] * z[88];
z[74] = -z[73] + z[74];
z[94] = z[40] / T(2);
z[79] = (T(-3) * z[37]) / T(2) + z[79] + z[94];
z[79] = z[22] * z[79];
z[69] = z[37] + -z[40] + -z[69];
z[69] = z[69] * z[111];
z[99] = -(z[14] * z[124]);
z[52] = z[52] + z[69] + z[74] + z[79] + z[91] + z[99] + z[128];
z[52] = z[25] * z[52];
z[69] = -z[18] + z[121];
z[69] = z[69] * z[72];
z[59] = z[59] * z[93];
z[59] = z[59] + z[69] + z[74] + z[89] + -z[90] + -z[113] + -z[118];
z[59] = z[26] * z[59];
z[69] = z[16] + z[17];
z[72] = z[14] + -z[69];
z[67] = z[67] * z[72];
z[72] = z[13] * z[115];
z[67] = z[67] + z[68] + z[72] + z[90] + -z[117];
z[67] = z[6] * z[67];
z[72] = z[15] + z[69];
z[74] = -z[18] + -z[72] + z[114] + z[139] + -z[142];
z[74] = z[47] * z[74];
z[52] = z[52] + z[59] + z[67] + z[74];
z[59] = -z[11] + z[112];
z[67] = z[59] / T(4) + z[65] + -z[75];
z[74] = -z[16] + z[18];
z[67] = z[67] * z[74];
z[74] = z[78] + z[136];
z[50] = T(19) * z[1] + -z[50] + -z[107];
z[50] = z[50] / T(4) + z[74] + -z[75];
z[50] = z[13] * z[50];
z[79] = -(z[86] * z[92]);
z[86] = z[17] * z[98];
z[50] = z[50] + z[67] + (T(-3) * z[73]) / T(2) + z[79] + z[84] + z[86] + -z[102] + z[123];
z[50] = z[19] * z[50];
z[67] = z[2] * z[100];
z[50] = z[50] + z[67];
z[50] = z[19] * z[50];
z[59] = z[59] / T(2) + z[74] + -z[77];
z[59] = z[59] * z[121];
z[67] = -(z[17] * z[120]);
z[73] = -(z[18] * z[82]);
z[59] = z[59] + z[67] + z[73] + z[80] + z[101] + -z[103];
z[59] = z[19] * z[59];
z[67] = z[37] / T(2);
z[71] = z[67] + z[71] + z[94];
z[71] = z[71] * z[104];
z[73] = z[67] + -z[75];
z[70] = z[70] + z[73];
z[64] = -z[64] + z[70] + z[78];
z[64] = z[18] * z[64];
z[74] = z[14] * z[137];
z[64] = z[64] + -z[71] + z[74] + z[87] + -z[126];
z[64] = z[64] * z[76];
z[74] = z[36] / T(2);
z[66] = -z[66] + z[70] + z[74];
z[66] = z[66] * z[121];
z[62] = z[11] + -z[62] + z[67] + z[74];
z[62] = z[17] * z[62];
z[67] = -z[67] + -z[78] + z[97] / T(2);
z[67] = z[14] * z[67];
z[70] = z[22] * z[70];
z[73] = z[73] + z[141];
z[74] = z[73] * z[111];
z[62] = z[62] + z[66] + z[67] + (T(-3) * z[70]) / T(2) + z[74];
z[62] = z[20] * z[62];
z[66] = z[95] + z[106] + z[130];
z[67] = T(-10) * z[36] + T(-4) * z[37] + z[119];
z[67] = z[14] * z[67];
z[70] = -(z[18] * z[144]);
z[67] = z[66] + z[67] + z[70];
z[67] = z[2] * z[67];
z[70] = -(z[18] * z[73]);
z[66] = -z[66] + z[70] + z[71] + z[145];
z[66] = z[3] * z[66];
z[59] = z[59] + z[62] + z[64] + z[66] + z[67];
z[59] = z[20] * z[59];
z[62] = -z[10] + z[37] + z[78];
z[62] = z[62] * z[139];
z[56] = z[56] + z[62] + z[81] + -z[96] + -z[116] + (T(-3) * z[128]) / T(2) + -z[138];
z[62] = prod_pow(z[2], 2);
z[56] = z[56] * z[62];
z[51] = z[28] * z[51];
z[58] = T(-2) * z[8] + -z[58] + -z[61] + z[65] + (T(3) * z[108]) / T(2);
z[58] = -(z[58] * z[69]);
z[57] = z[30] * z[57];
z[57] = z[57] + z[58] + z[68] + z[89] + -z[128];
z[58] = T(3) * z[5];
z[57] = z[57] * z[58];
z[61] = z[6] + -z[28];
z[61] = z[61] * z[85];
z[64] = z[62] * z[88];
z[65] = z[29] * z[85];
z[65] = z[65] + -z[147];
z[65] = z[65] * z[76];
z[66] = z[19] * z[85];
z[67] = -(z[4] * z[88]);
z[66] = z[66] + z[67];
z[66] = z[4] * z[66];
z[67] = -(z[19] * z[147]);
z[61] = z[61] + z[64] + z[65] + z[66] + z[67];
z[61] = z[33] * z[61];
z[64] = z[13] + (T(13) * z[14]) / T(4) + (T(5) * z[15]) / T(4) + z[18] + (T(-9) * z[69]) / T(4);
z[64] = z[64] * z[125];
z[65] = z[13] + -z[72] + z[140];
z[65] = z[65] * z[146];
z[66] = (T(-5) * z[16]) / T(2) + z[114];
z[66] = (T(-13) * z[14]) / T(6) + -z[15] + (T(-5) * z[17]) / T(6) + (T(-7) * z[18]) / T(6) + z[66] / T(3) + z[139];
z[66] = prod_pow(z[41], 2) * z[66];
z[64] = z[64] + z[65] + z[66];
z[64] = z[41] * z[64];
z[65] = z[58] * z[124];
z[66] = T(9) * z[10] + -z[97];
z[66] = z[66] / T(2) + -z[83];
z[66] = z[66] / T(2) + -z[136];
z[66] = z[62] * z[66];
z[67] = prod_pow(z[44], 3);
z[68] = (T(3) * z[48]) / T(2) + T(-2) * z[67];
z[65] = (T(805) * z[45]) / T(48) + z[65] + z[66] + -z[68];
z[65] = z[18] * z[65];
z[66] = -z[10] + -z[97];
z[55] = z[37] + z[55] + z[66] / T(2);
z[55] = z[55] * z[58];
z[58] = -z[1] + (T(-13) * z[36]) / T(2) + (T(-7) * z[37]) / T(2) + z[60] + z[122];
z[58] = z[58] * z[62];
z[60] = T(3) * z[48] + T(-4) * z[67];
z[55] = (T(109) * z[45]) / T(12) + z[55] + z[58] + -z[60];
z[55] = z[14] * z[55];
z[58] = (T(11) * z[15]) / T(4) + (T(-105) * z[22]) / T(16);
z[58] = z[45] * z[58];
z[62] = (T(157) * z[45]) / T(24) + z[68];
z[62] = z[13] * z[62];
z[66] = (T(109) * z[45]) / T(24) + -z[68];
z[66] = z[16] * z[66];
z[67] = (T(215) * z[45]) / T(12) + -z[68];
z[67] = z[17] * z[67];
z[60] = (T(-35) * z[45]) / T(2) + z[60];
z[60] = z[30] * z[60];
z[68] = -(z[5] * z[109]);
return z[49] + z[50] + z[51] + T(3) * z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + (T(3) * z[61]) / T(2) + z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + z[68];
}



template IntegrandConstructorType<double> f_4_145_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_145_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_145_construct (const Kin<qd_real>&);
#endif

}