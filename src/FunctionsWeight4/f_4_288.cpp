#include "f_4_288.h"

namespace PentagonFunctions {

template <typename T> T f_4_288_abbreviated (const std::array<T,58>&);

template <typename T> class SpDLog_f_4_288_W_22 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_288_W_22 (const Kin<T>& kin) {
        c[0] = rlog(kin.v[3]) * (kin.v[1] * (T(-6) + T(6) * kin.v[0] + T(3) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[0] * (T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[1] * (T(6) + T(-6) * kin.v[0] + T(-3) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[0] * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-3) + prod_pow(abb[2], 2) * abb[4] * T(3) + prod_pow(abb[1], 2) * (abb[4] * T(-3) + abb[3] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_288_W_12 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_288_W_12 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[1] * (T(6) + T(3) * kin.v[1]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[1] * (T(6) + T(3) * kin.v[1]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[1] * (T(6) + T(3) * kin.v[1]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4])) + rlog(-kin.v[1]) * (kin.v[1] * (T(-6) + T(-3) * kin.v[1]) + kin.v[4] * (T(6) + T(3) * kin.v[4])) + rlog(kin.v[2]) * (kin.v[1] * (T(-6) + T(-3) * kin.v[1]) + kin.v[4] * (T(6) + T(3) * kin.v[4])) + rlog(kin.v[3]) * (kin.v[1] * (T(-6) + T(-3) * kin.v[1]) + kin.v[4] * (T(6) + T(3) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[5] * (prod_pow(abb[7], 2) * abb[8] * T(3) + prod_pow(abb[7], 2) * (abb[4] * T(-3) + abb[10] * T(-3) + abb[11] * T(-3) + abb[3] * T(3) + abb[9] * T(3)) + prod_pow(abb[6], 2) * (abb[3] * T(-3) + abb[8] * T(-3) + abb[9] * T(-3) + abb[4] * T(3) + abb[10] * T(3) + abb[11] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_288_W_23 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_288_W_23 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(-6) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(3) * kin.v[4])) + rlog(kin.v[3]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(-6) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(3) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(-6) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(3) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[12] * (abb[3] * prod_pow(abb[13], 2) * T(3) + prod_pow(abb[13], 2) * (abb[10] * T(-3) + abb[14] * T(-3) + abb[16] * T(-3) + abb[4] * T(3) + abb[15] * T(3)) + prod_pow(abb[1], 2) * (abb[3] * T(-3) + abb[4] * T(-3) + abb[15] * T(-3) + abb[10] * T(3) + abb[14] * T(3) + abb[16] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_288_W_7 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_288_W_7 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (T(4) * kin.v[0] + T(-8) * kin.v[2] + T(-6) * kin.v[3] + T(-8) * kin.v[4]) + T(4) * kin.v[0] * kin.v[4] + T(-8) * kin.v[2] * kin.v[4] + T(-2) * prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[3] * (T(-4) + T(-4) * kin.v[1] + T(2) * kin.v[3] + T(4) * kin.v[4]));
c[1] = kin.v[3] * (T(-8) * kin.v[2] + (T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] + T(-2) * kin.v[4])) + T(-8) * kin.v[2] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[2] * kin.v[4] + kin.v[0] * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[4] + T(4) * kin.v[4]) + rlog(kin.v[3]) * (T(6) * kin.v[1] * kin.v[4] + kin.v[3] * (T(6) + T(6) * kin.v[1] + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4])) + rlog(-kin.v[1]) * ((-kin.v[4] + T(6)) * kin.v[4] + T(-4) * kin.v[0] * kin.v[4] + T(6) * kin.v[1] * kin.v[4] + T(8) * kin.v[2] * kin.v[4] + kin.v[3] * (T(6) + T(-4) * kin.v[0] + T(6) * kin.v[1] + T(8) * kin.v[2] + (bc<T>[0] * int_to_imaginary<T>(-2) + T(3)) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(4) * kin.v[1] + T(-4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(4) + T(-2) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (T(-2) * kin.v[0] * kin.v[4] + T(-6) * kin.v[1] * kin.v[4] + T(4) * kin.v[2] * kin.v[4] + kin.v[4] * (T(-6) + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[4] + T(2)) * kin.v[4] + T(2) * kin.v[1] * kin.v[4]) + kin.v[3] * (T(-6) + T(-2) * kin.v[0] + T(-6) * kin.v[1] + T(4) * kin.v[2] + (bc<T>[0] * int_to_imaginary<T>(-1) + T(6)) * kin.v[3] + T(10) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(2) * kin.v[1] + T(-2) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (T(-2) * kin.v[0] * kin.v[4] + T(4) * kin.v[2] * kin.v[4] + prod_pow(kin.v[4], 2) + kin.v[3] * (T(-2) * kin.v[0] + T(4) * kin.v[2] + T(3) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[4] + T(2)) * kin.v[4] + T(2) * kin.v[1] * kin.v[4] + kin.v[3] * (-kin.v[3] + T(2) + T(2) * kin.v[1] + T(-2) * kin.v[4]))) + rlog(-kin.v[4]) * (T(2) * kin.v[0] * kin.v[4] + T(-6) * kin.v[1] * kin.v[4] + T(-4) * kin.v[2] * kin.v[4] + kin.v[4] * (T(-6) + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-2) + kin.v[4])) + kin.v[3] * (T(-6) + T(2) * kin.v[0] + T(-6) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(-2) * kin.v[1] + T(2) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (T(2) * kin.v[0] * kin.v[4] + T(-6) * kin.v[1] * kin.v[4] + T(-4) * kin.v[2] * kin.v[4] + kin.v[4] * (T(-6) + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-2) + kin.v[4])) + kin.v[3] * (T(-6) + T(2) * kin.v[0] + T(-6) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(-2) * kin.v[1] + T(2) * kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (T(4) * kin.v[0] * kin.v[4] + T(6) * kin.v[1] * kin.v[4] + T(-8) * kin.v[2] * kin.v[4] + kin.v[4] * (T(6) + T(-5) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-4) + T(2) * kin.v[4])) + kin.v[3] * (T(6) + T(4) * kin.v[0] + T(6) * kin.v[1] + T(-8) * kin.v[2] + (T(-9) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[3] + T(-14) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(-4) * kin.v[1] + T(4) * kin.v[4])));
c[2] = (T(-8) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + T(-8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[4];
c[3] = rlog(-kin.v[4]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[4] + T(4) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[4] + T(4) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-8) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + T(-8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[4]) + rlog(-kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(4) + T(8)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4] + T(8) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[31] * (abb[7] * abb[23] * (abb[28] * T(-4) + abb[4] * T(-2) + abb[16] * T(-2) + abb[10] * T(2) + abb[11] * T(2) + abb[15] * T(4)) + abb[23] * abb[30] * (abb[28] * T(-4) + abb[4] * T(-2) + abb[16] * T(-2) + abb[10] * T(2) + abb[11] * T(2) + abb[15] * T(4)) + abb[8] * (abb[26] * T(-8) + abb[7] * abb[23] * T(-4) + abb[23] * abb[30] * T(-4) + abb[23] * (abb[23] + bc<T>[0] * int_to_imaginary<T>(4)) + abb[13] * abb[23] * T(4)) + abb[13] * abb[23] * (abb[15] * T(-4) + abb[10] * T(-2) + abb[11] * T(-2) + abb[4] * T(2) + abb[16] * T(2) + abb[28] * T(4)) + abb[23] * (abb[10] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[4] * bc<T>[0] * int_to_imaginary<T>(2) + abb[16] * bc<T>[0] * int_to_imaginary<T>(2) + abb[15] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[28] * bc<T>[0] * int_to_imaginary<T>(4) + abb[23] * (abb[10] + abb[11] + abb[15] * T(-7) + abb[3] * T(-3) + abb[4] * T(2) + abb[28] * T(4) + abb[16] * T(5))) + abb[26] * (abb[28] * T(-8) + abb[4] * T(-4) + abb[16] * T(-4) + abb[10] * T(4) + abb[11] * T(4) + abb[15] * T(8)) + abb[6] * (abb[4] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[16] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[10] * bc<T>[0] * int_to_imaginary<T>(2) + abb[11] * bc<T>[0] * int_to_imaginary<T>(2) + abb[28] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[15] * bc<T>[0] * int_to_imaginary<T>(4) + abb[13] * (abb[28] * T(-4) + abb[4] * T(-2) + abb[16] * T(-2) + abb[10] * T(2) + abb[11] * T(2) + abb[15] * T(4)) + abb[7] * (abb[15] * T(-4) + abb[10] * T(-2) + abb[11] * T(-2) + abb[4] * T(2) + abb[16] * T(2) + abb[28] * T(4)) + abb[23] * (abb[15] * T(-4) + abb[10] * T(-2) + abb[11] * T(-2) + abb[4] * T(2) + abb[16] * T(2) + abb[28] * T(4)) + abb[30] * (abb[15] * T(-4) + abb[10] * T(-2) + abb[11] * T(-2) + abb[4] * T(2) + abb[16] * T(2) + abb[28] * T(4)) + abb[8] * (abb[13] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[7] * T(4) + abb[23] * T(4) + abb[30] * T(4)) + abb[6] * (abb[10] + abb[11] + abb[28] * T(-8) + abb[16] * T(-7) + abb[8] * T(-5) + abb[4] * T(-4) + abb[3] * T(3) + abb[15] * T(11))));
    }
};
template <typename T> class SpDLog_f_4_288_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_288_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(-12) * kin.v[2] + T(-32) * kin.v[3]) + T(-20) * prod_pow(kin.v[3], 2) + kin.v[1] * (T(-4) * kin.v[1] + T(16) * kin.v[2] + T(24) * kin.v[3]) + kin.v[0] * (T(-12) * kin.v[0] + T(-16) * kin.v[1] + T(24) * kin.v[2] + T(32) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (T(8) + T(-4) * kin.v[0] + T(8) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[1] * (T(8) + T(4) * kin.v[1] + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[2] * (T(-8) + T(-4) * kin.v[2] + T(8) * kin.v[4]) + kin.v[3] * (T(-8) + T(4) * kin.v[3] + T(8) * kin.v[4]));
c[1] = kin.v[2] * (T(-2) * kin.v[2] + T(-12) * kin.v[3]) + T(-10) * prod_pow(kin.v[3], 2) + kin.v[1] * (T(6) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-5) * prod_pow(kin.v[3], 2) + kin.v[2] * (-kin.v[2] + T(-6) * kin.v[3]) + kin.v[1] * (T(3) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3])) + kin.v[0] * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(4) * kin.v[1] + T(4) * kin.v[2] + T(12) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[1] + T(2) * kin.v[2] + T(6) * kin.v[3])) + rlog(-kin.v[1]) * (kin.v[3] * (-kin.v[3] / T(2) + T(6) + T(-6) * kin.v[4]) + kin.v[2] * ((T(9) * kin.v[2]) / T(2) + T(6) + T(4) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[1] * ((T(-5) * kin.v[1]) / T(2) + T(-6) + T(-2) * kin.v[2] + T(3) * kin.v[3] + T(6) * kin.v[4]) + kin.v[0] * (T(-6) + (T(9) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(2) * kin.v[1] + T(-9) * kin.v[2] + T(-4) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[3] * (-kin.v[3] / T(2) + T(6) + T(-6) * kin.v[4]) + kin.v[2] * ((T(9) * kin.v[2]) / T(2) + T(6) + T(4) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[1] * ((T(-5) * kin.v[1]) / T(2) + T(-6) + T(-2) * kin.v[2] + T(3) * kin.v[3] + T(6) * kin.v[4]) + kin.v[0] * (T(-6) + (T(9) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(2) * kin.v[1] + T(-9) * kin.v[2] + T(-4) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[3] * (-kin.v[3] / T(2) + T(6) + T(-6) * kin.v[4]) + kin.v[2] * ((T(9) * kin.v[2]) / T(2) + T(6) + T(4) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[1] * ((T(-5) * kin.v[1]) / T(2) + T(-6) + T(-2) * kin.v[2] + T(3) * kin.v[3] + T(6) * kin.v[4]) + kin.v[0] * (T(-6) + (T(9) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(2) * kin.v[1] + T(-9) * kin.v[2] + T(-4) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[1] * ((T(9) * kin.v[1]) / T(2) + T(6) + T(-6) * kin.v[2] + T(-15) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[3] * ((T(21) * kin.v[3]) / T(2) + T(-6) + T(6) * kin.v[4]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-6) + T(12) * kin.v[3] + T(6) * kin.v[4]) + kin.v[0] * (T(6) + (T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[0] + T(6) * kin.v[1] + T(-3) * kin.v[2] + T(-12) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(-3) * kin.v[2] + T(3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-3) + T(3) * kin.v[3] + T(3) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (T(10) * prod_pow(kin.v[3], 2) + kin.v[0] * (T(6) * kin.v[0] + T(8) * kin.v[1] + T(-12) * kin.v[2] + T(-16) * kin.v[3]) + kin.v[1] * (T(2) * kin.v[1] + T(-8) * kin.v[2] + T(-12) * kin.v[3]) + kin.v[2] * (T(6) * kin.v[2] + T(16) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[3] * (T(4) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[0] * (T(-4) + T(2) * kin.v[0] + T(-4) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) + T(-2) * kin.v[1] + T(4) * kin.v[3] + T(4) * kin.v[4]))) + rlog(kin.v[2]) * (kin.v[1] * (kin.v[1] / T(2) + T(6) + T(10) * kin.v[2] + T(9) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[3] * ((T(-19) * kin.v[3]) / T(2) + T(-6) + T(6) * kin.v[4]) + kin.v[2] * ((T(-21) * kin.v[2]) / T(2) + T(-6) + T(-20) * kin.v[3] + T(6) * kin.v[4]) + kin.v[0] * (T(6) + (T(-21) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-5) / T(2))) * kin.v[0] + T(-10) * kin.v[1] + T(21) * kin.v[2] + T(20) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(5) + T(5) * kin.v[2] + T(-5) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * ((T(5) * kin.v[1]) / T(2) + T(5) + T(-5) * kin.v[3] + T(-5) * kin.v[4]) + kin.v[2] * ((T(-5) * kin.v[2]) / T(2) + T(-5) + T(5) * kin.v[4]) + kin.v[3] * ((T(5) * kin.v[3]) / T(2) + T(-5) + T(5) * kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[1] * (kin.v[1] / T(2) + T(6) + T(10) * kin.v[2] + T(9) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[3] * ((T(-19) * kin.v[3]) / T(2) + T(-6) + T(6) * kin.v[4]) + kin.v[2] * ((T(-21) * kin.v[2]) / T(2) + T(-6) + T(-20) * kin.v[3] + T(6) * kin.v[4]) + kin.v[0] * (T(6) + (T(-21) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-5) / T(2))) * kin.v[0] + T(-10) * kin.v[1] + T(21) * kin.v[2] + T(20) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(5) + T(5) * kin.v[2] + T(-5) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * ((T(5) * kin.v[1]) / T(2) + T(5) + T(-5) * kin.v[3] + T(-5) * kin.v[4]) + kin.v[2] * ((T(-5) * kin.v[2]) / T(2) + T(-5) + T(5) * kin.v[4]) + kin.v[3] * ((T(5) * kin.v[3]) / T(2) + T(-5) + T(5) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(8) + T(16)) * kin.v[0] + T(16) * kin.v[1] + T(-16) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[1] + T(-8) * kin.v[2] + T(-8) * kin.v[3]) + T(-16) * kin.v[3];
c[3] = rlog(kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(5) + T(10)) * kin.v[0] + T(10) * kin.v[1] + T(-10) * kin.v[2] + T(-10) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(5) * kin.v[1] + T(-5) * kin.v[2] + T(-5) * kin.v[3])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((bc<T>[0] * int_to_imaginary<T>(5) + T(10)) * kin.v[0] + T(10) * kin.v[1] + T(-10) * kin.v[2] + T(-10) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(5) * kin.v[1] + T(-5) * kin.v[2] + T(-5) * kin.v[3])) + rlog(-kin.v[1]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + T(-6) * kin.v[1] + T(6) * kin.v[2] + T(6) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[1] + T(3) * kin.v[2] + T(3) * kin.v[3])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-8) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(-8) * kin.v[1] + T(8) * kin.v[2] + T(8) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[32] * (abb[8] * (abb[2] * abb[30] + -(abb[2] * abb[13]) + abb[33] * T(2) + abb[2] * (bc<T>[0] * int_to_imaginary<T>(-1) + abb[2] * T(2))) + abb[2] * (abb[11] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[14] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[16] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[9] * bc<T>[0] * int_to_imaginary<T>(5) + abb[15] * bc<T>[0] * int_to_imaginary<T>(5) + abb[34] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[2] * (abb[34] * T(-8) + abb[16] * T(-6) + abb[4] * T(-4) + abb[9] * T(2) + abb[11] * T(2) + abb[14] * T(2) + abb[15] * T(2))) + abb[2] * abb[13] * (-abb[11] + -abb[14] + abb[34] * T(-8) + abb[4] * T(-4) + abb[16] * T(-3) + abb[9] * T(5) + abb[15] * T(5)) + abb[2] * abb[30] * (abb[11] + abb[14] + abb[9] * T(-5) + abb[15] * T(-5) + abb[16] * T(3) + abb[4] * T(4) + abb[34] * T(8)) + abb[1] * (abb[2] * abb[8] + abb[23] * (-abb[8] + -abb[11] + -abb[14] + abb[34] * T(-8) + abb[4] * T(-4) + abb[16] * T(-3) + abb[9] * T(5) + abb[15] * T(5)) + abb[2] * (abb[11] + abb[14] + abb[9] * T(-5) + abb[15] * T(-5) + abb[16] * T(3) + abb[4] * T(4) + abb[34] * T(8))) + abb[33] * (abb[9] * T(-10) + abb[15] * T(-10) + abb[11] * T(2) + abb[14] * T(2) + abb[16] * T(6) + abb[4] * T(8) + abb[34] * T(16)) + abb[23] * (abb[11] * bc<T>[0] * int_to_imaginary<T>(1) + abb[14] * bc<T>[0] * int_to_imaginary<T>(1) + abb[8] * (abb[13] + -abb[2] + -abb[30] + bc<T>[0] * int_to_imaginary<T>(1)) + abb[16] * bc<T>[0] * int_to_imaginary<T>(3) + abb[4] * bc<T>[0] * int_to_imaginary<T>(4) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-5) + abb[15] * bc<T>[0] * int_to_imaginary<T>(-5) + abb[34] * bc<T>[0] * int_to_imaginary<T>(8) + abb[2] * (-abb[11] + -abb[14] + abb[34] * T(-8) + abb[4] * T(-4) + abb[16] * T(-3) + abb[9] * T(5) + abb[15] * T(5)) + abb[30] * (-abb[11] + -abb[14] + abb[34] * T(-8) + abb[4] * T(-4) + abb[16] * T(-3) + abb[9] * T(5) + abb[15] * T(5)) + abb[13] * (abb[11] + abb[14] + abb[9] * T(-5) + abb[15] * T(-5) + abb[16] * T(3) + abb[4] * T(4) + abb[34] * T(8)) + abb[23] * (-abb[8] + -abb[11] + -abb[14] + abb[9] * T(-7) + abb[15] * T(-7) + abb[4] * T(8) + abb[16] * T(9) + abb[34] * T(16))));
    }
};
template <typename T> class SpDLog_f_4_288_W_10 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_288_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[2] * ((T(3) * kin.v[2]) / T(4) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(4) + (T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]);
c[1] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[2] * ((T(7) * kin.v[2]) / T(2) + T(-20) + T(-20) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[1] * (T(-20) + (T(7) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + T(7) * kin.v[2] + T(-20) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[2] + T(-8) * kin.v[4]))) + rlog(-kin.v[1]) * (kin.v[2] * ((T(-5) * kin.v[2]) / T(2) + T(-8) + T(-8) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[1] * (T(-8) + (T(-5) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + T(-5) * kin.v[2] + T(-8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[2] + T(-8) * kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(-6) + T(3) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(-8) + T(-8) * kin.v[4]) + kin.v[2] * (T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(-3) + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[1] * ((T(-9) * kin.v[1]) / T(4) + (T(-9) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[2] * ((T(-9) * kin.v[2]) / T(4) + T(3) + T(3) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[1] * (T(6) + T(-3) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(kin.v[2]) * (kin.v[2] * ((T(5) * kin.v[2]) / T(2) + T(8) + T(8) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * (T(8) + T(-4) * kin.v[2] + T(8) * kin.v[4]) + kin.v[1] * (T(8) + (T(5) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(5) * kin.v[2] + T(8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[2] + T(8) * kin.v[4])));
c[2] = T(3) * kin.v[1] + T(3) * kin.v[2];
c[3] = rlog(-kin.v[4]) * (T(3) * kin.v[1] + T(3) * kin.v[2]) + rlog(-kin.v[1]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(-6) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[2]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(-6) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-8) * kin.v[2]) + rlog(kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[2] + T(6) * kin.v[2]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[40] * (abb[41] * (abb[9] * T(-6) + abb[10] * T(-3) + abb[42] * T(3) + abb[4] * T(6)) + abb[1] * abb[13] * (abb[4] * T(-8) + abb[9] * T(8)) + abb[7] * (abb[9] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[4] * bc<T>[0] * int_to_imaginary<T>(8) + abb[7] * ((abb[10] * T(-3)) / T(2) + (abb[42] * T(9)) / T(2) + abb[4] * T(-4) + abb[15] * T(-3) + abb[9] * T(-2) + abb[8] * T(2) + abb[14] * T(3)) + abb[13] * (abb[4] * T(-6) + abb[42] * T(-3) + abb[10] * T(3) + abb[9] * T(6)) + abb[8] * (abb[30] * T(-8) + abb[13] * T(-6) + bc<T>[0] * int_to_imaginary<T>(8) + abb[1] * T(8)) + abb[1] * (abb[9] * T(-8) + abb[4] * T(8)) + abb[30] * (abb[4] * T(-8) + abb[9] * T(8))) + abb[8] * (abb[1] * abb[13] * T(-8) + abb[41] * T(6) + abb[13] * (bc<T>[0] * int_to_imaginary<T>(-8) + abb[13] * T(4) + abb[30] * T(8))) + abb[13] * (abb[4] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[9] * bc<T>[0] * int_to_imaginary<T>(8) + abb[30] * (abb[9] * T(-8) + abb[4] * T(8)) + abb[13] * ((abb[10] * T(-3)) / T(2) + (abb[42] * T(-3)) / T(2) + abb[9] * T(-4) + abb[14] * T(-3) + abb[15] * T(3) + abb[4] * T(10))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_288_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl12 = DLog_W_12<T>(kin),dl23 = DLog_W_23<T>(kin),dl31 = DLog_W_31<T>(kin),dl4 = DLog_W_4<T>(kin),dl25 = DLog_W_25<T>(kin),dl7 = DLog_W_7<T>(kin),dl21 = DLog_W_21<T>(kin),dl1 = DLog_W_1<T>(kin),dl10 = DLog_W_10<T>(kin),dl30 = DLog_W_30<T>(kin),dl28 = DLog_W_28<T>(kin),dl26 = DLog_W_26<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl2 = DLog_W_2<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),dl19 = DLog_W_19<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl3 = DLog_W_3<T>(kin),dl18 = DLog_W_18<T>(kin),spdl22 = SpDLog_f_4_288_W_22<T>(kin),spdl12 = SpDLog_f_4_288_W_12<T>(kin),spdl23 = SpDLog_f_4_288_W_23<T>(kin),spdl7 = SpDLog_f_4_288_W_7<T>(kin),spdl21 = SpDLog_f_4_288_W_21<T>(kin),spdl10 = SpDLog_f_4_288_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,58> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[16] / kin_path.W[16]), dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[18] / kin_path.W[18]), dl23(t), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[19] / kin_path.W[19]), dl31(t), f_2_2_7(kin_path), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl4(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_4(kin_path), f_2_1_8(kin_path), f_2_1_11(kin_path), f_2_1_14(kin_path), -rlog(t), dl25(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl7(t), dl21(t), f_2_1_15(kin_path), -rlog(t), dl1(t), f_2_1_10(kin_path), f_2_1_12(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl10(t), f_2_1_7(kin_path), -rlog(t), dl30(t) / kin_path.SqrtDelta, f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl28(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl2(t), dl16(t), dl17(t), dl19(t), dl5(t), dl20(t), dl3(t), dl18(t)}
;

        auto result = f_4_288_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr) + spdl21(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_288_abbreviated(const std::array<T,58>& abb)
{
using TR = typename T::value_type;
T z[258];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[56];
z[3] = abb[57];
z[4] = abb[4];
z[5] = abb[22];
z[6] = abb[51];
z[7] = abb[52];
z[8] = abb[53];
z[9] = abb[54];
z[10] = abb[55];
z[11] = abb[8];
z[12] = abb[9];
z[13] = abb[10];
z[14] = abb[11];
z[15] = abb[14];
z[16] = abb[15];
z[17] = abb[16];
z[18] = abb[19];
z[19] = abb[43];
z[20] = abb[46];
z[21] = abb[47];
z[22] = abb[48];
z[23] = abb[49];
z[24] = abb[20];
z[25] = abb[21];
z[26] = abb[34];
z[27] = abb[42];
z[28] = abb[44];
z[29] = abb[45];
z[30] = abb[2];
z[31] = abb[50];
z[32] = abb[7];
z[33] = abb[23];
z[34] = abb[13];
z[35] = abb[30];
z[36] = bc<TR>[0];
z[37] = abb[6];
z[38] = abb[24];
z[39] = abb[25];
z[40] = abb[26];
z[41] = abb[27];
z[42] = abb[33];
z[43] = abb[41];
z[44] = abb[18];
z[45] = abb[37];
z[46] = abb[39];
z[47] = abb[28];
z[48] = abb[35];
z[49] = abb[29];
z[50] = abb[36];
z[51] = abb[38];
z[52] = abb[17];
z[53] = bc<TR>[3];
z[54] = bc<TR>[1];
z[55] = bc<TR>[5];
z[56] = bc<TR>[2];
z[57] = bc<TR>[4];
z[58] = bc<TR>[9];
z[59] = (T(7) * z[12]) / T(2);
z[60] = -z[14] + z[15];
z[61] = z[60] / T(2);
z[62] = (T(5) * z[11]) / T(2);
z[63] = z[16] / T(2);
z[64] = z[13] + z[63];
z[65] = z[17] / T(2);
z[66] = T(3) * z[4];
z[67] = z[59] + z[61] + z[62] + -z[64] + z[65] + -z[66];
z[68] = T(2) * z[26];
z[67] = z[67] / T(2) + -z[68];
z[67] = z[2] * z[67];
z[69] = T(3) * z[56];
z[70] = z[12] / T(2);
z[71] = z[69] + -z[70];
z[72] = (T(7) * z[16]) / T(4);
z[73] = (T(5) * z[11]) / T(12);
z[74] = (T(5) * z[27]) / T(3);
z[75] = (T(5) * z[4]) / T(3) + (T(-17) * z[13]) / T(6) + (T(-11) * z[14]) / T(6) + -z[15] + z[17] / T(12) + z[47] + z[68] + -z[71] + z[72] + -z[73] + z[74];
z[75] = z[3] * z[75];
z[76] = z[14] / T(2);
z[77] = T(2) * z[15];
z[78] = z[76] + z[77];
z[79] = (T(7) * z[12]) / T(12);
z[80] = (T(4) * z[26]) / T(3);
z[73] = (T(-7) * z[4]) / T(12) + -z[13] / T(2) + z[16] + (T(-10) * z[17]) / T(3) + -z[47] + z[69] + -z[73] + z[78] / T(3) + -z[79] + z[80];
z[73] = z[10] * z[73];
z[78] = (T(2) * z[47]) / T(3);
z[81] = (T(-13) * z[13]) / T(6) + (T(-5) * z[14]) / T(3) + z[15] / T(2) + (T(-11) * z[17]) / T(2);
z[69] = (T(17) * z[4]) / T(12) + (T(7) * z[11]) / T(6) + (T(8) * z[16]) / T(3) + -z[68] + -z[69] + z[78] + -z[79] + z[81] / T(2);
z[69] = z[31] * z[69];
z[79] = -z[11] + z[12];
z[81] = T(2) * z[13];
z[82] = z[16] + -z[17];
z[83] = z[14] + z[15];
z[84] = -z[79] + z[81] + z[82] + -z[83];
z[84] = z[48] * z[84];
z[85] = T(3) * z[14];
z[86] = z[15] + z[85];
z[86] = z[86] / T(2);
z[87] = (T(3) * z[17]) / T(2);
z[88] = z[13] + z[87];
z[89] = z[86] + z[88];
z[90] = z[12] / T(4);
z[91] = -z[4] + z[90];
z[92] = z[11] / T(4);
z[93] = z[16] / T(4);
z[89] = -z[47] + z[89] / T(2) + z[91] + -z[92] + -z[93];
z[89] = z[5] * z[89];
z[94] = (T(5) * z[13]) / T(2);
z[95] = z[14] + (T(-5) * z[15]) / T(2) + -z[94];
z[95] = z[87] + z[95] / T(3);
z[71] = z[4] / T(3) + (T(19) * z[11]) / T(12) + z[16] / T(6) + -z[54] + z[71] + -z[78] + -z[80] + z[95] / T(2);
z[71] = z[8] * z[71];
z[78] = z[4] + z[17];
z[80] = (T(-17) * z[14]) / T(6) + z[15] + (T(7) * z[78]) / T(6) + z[94];
z[95] = z[11] / T(2);
z[74] = (T(-2) * z[12]) / T(3) + z[16] / T(3) + -z[54] + z[68] + -z[74] + z[80] / T(2) + -z[95];
z[74] = z[6] * z[74];
z[80] = T(3) * z[27];
z[96] = (T(5) * z[16]) / T(2);
z[97] = (T(19) * z[14]) / T(3) + T(5) * z[15];
z[97] = (T(-11) * z[13]) / T(3) + z[97] / T(2);
z[97] = (T(-11) * z[4]) / T(4) + (T(-31) * z[11]) / T(12) + (T(4) * z[12]) / T(3) + -z[17] / T(3) + z[47] + z[80] + -z[96] + z[97] / T(2);
z[97] = z[9] * z[97];
z[98] = -z[15] + z[17];
z[99] = (T(3) * z[12]) / T(2);
z[80] = (T(14) * z[4]) / T(3) + (T(13) * z[11]) / T(12) + (T(7) * z[13]) / T(12) + z[14] / T(3) + (T(5) * z[16]) / T(3) + -z[80] + -z[98] / T(4) + -z[99];
z[80] = z[7] * z[80];
z[98] = -z[10] + z[31];
z[100] = z[2] + -z[3];
z[101] = T(2) * z[9];
z[102] = z[98] + -z[100] + z[101];
z[103] = z[54] * z[102];
z[104] = -z[24] + z[25];
z[105] = z[18] / T(3);
z[106] = z[28] / T(2);
z[107] = z[29] / T(2) + (T(5) * z[104]) / T(6) + -z[105] + -z[106];
z[107] = z[19] * z[107];
z[108] = z[11] + z[16];
z[109] = z[14] + z[17];
z[110] = z[108] + -z[109];
z[111] = T(3) * z[49];
z[110] = z[110] * z[111];
z[111] = (T(3) * z[29]) / T(4);
z[105] = (T(-7) * z[24]) / T(12) + (T(13) * z[25]) / T(12) + -z[28] / T(4) + -z[105] + z[111];
z[105] = z[23] * z[105];
z[112] = z[24] + z[25] + z[28] + z[29];
z[113] = z[112] / T(2);
z[114] = z[22] * z[113];
z[115] = z[20] * z[113];
z[67] = z[67] + z[69] + z[71] + z[73] + z[74] + z[75] + z[80] + z[84] + z[89] + z[97] + z[103] + z[105] + z[107] + -z[110] + -z[114] + z[115];
z[69] = prod_pow(z[36], 2);
z[67] = z[67] * z[69];
z[71] = T(7) * z[13];
z[73] = (T(7) * z[17]) / T(2);
z[74] = T(3) * z[15];
z[75] = -z[14] + z[74];
z[80] = z[71] + z[73] + (T(5) * z[75]) / T(2);
z[84] = (T(5) * z[16]) / T(4);
z[89] = (T(11) * z[11]) / T(4);
z[97] = T(5) * z[27];
z[80] = z[80] / T(2) + -z[84] + z[89] + -z[91] + -z[97];
z[80] = z[9] * z[80];
z[91] = T(3) * z[13];
z[103] = T(9) * z[15];
z[105] = T(7) * z[14] + z[103];
z[105] = z[87] + z[91] + z[105] / T(2);
z[107] = (T(9) * z[11]) / T(4);
z[116] = (T(7) * z[12]) / T(4);
z[117] = T(2) * z[4];
z[72] = -z[27] + -z[72] + z[105] / T(2) + z[107] + -z[116] + z[117];
z[72] = z[6] * z[72];
z[105] = z[4] / T(2);
z[118] = (T(3) * z[12]) / T(4);
z[119] = z[105] + -z[118];
z[120] = -z[68] + z[93];
z[121] = -z[61] + z[88];
z[89] = -z[47] + z[89] + z[119] + z[120] + z[121] / T(2);
z[89] = z[8] * z[89];
z[122] = z[75] / T(2);
z[88] = z[88] + z[122];
z[123] = (T(5) * z[11]) / T(4);
z[84] = z[84] + z[123];
z[124] = z[27] + -z[66] + -z[84] + z[88] / T(2) + z[118];
z[124] = z[3] * z[124];
z[125] = z[64] + z[95];
z[126] = -z[65] + z[125];
z[127] = z[83] / T(2);
z[128] = z[70] + -z[126] + z[127];
z[129] = z[48] * z[128];
z[130] = (T(3) * z[129]) / T(2);
z[131] = (T(3) * z[4]) / T(2);
z[127] = z[68] + -z[127] + z[131];
z[132] = z[11] + z[12];
z[133] = z[17] + -z[63] + -z[127] + z[132];
z[133] = z[10] * z[133];
z[134] = T(7) * z[4];
z[135] = T(2) * z[11];
z[136] = z[134] + z[135];
z[137] = z[13] + z[14];
z[138] = z[17] + z[137];
z[139] = z[47] + z[138];
z[140] = T(5) * z[12];
z[141] = -z[136] + -z[139] + z[140];
z[141] = z[31] * z[141];
z[142] = z[20] + -z[21];
z[143] = -z[22] + -z[142];
z[144] = (T(3) * z[112]) / T(4);
z[143] = z[143] * z[144];
z[145] = z[14] + z[103];
z[145] = T(-16) * z[4] + (T(-39) * z[11]) / T(4) + z[13] + (T(-15) * z[16]) / T(4) + (T(5) * z[17]) / T(4) + (T(27) * z[27]) / T(2) + -z[118] + z[145] / T(4);
z[145] = z[7] * z[145];
z[146] = T(3) * z[1];
z[147] = z[6] + z[9];
z[148] = z[31] + -z[147];
z[148] = z[146] * z[148];
z[149] = z[18] + z[24];
z[149] = z[25] + T(2) * z[149];
z[150] = z[23] * z[149];
z[111] = T(-2) * z[18] + (T(-5) * z[24]) / T(4) + -z[25] / T(4) + (T(3) * z[28]) / T(4) + z[111];
z[111] = z[19] * z[111];
z[72] = z[72] + z[80] + -z[89] + z[111] + z[124] + -z[130] + z[133] + z[141] + z[143] + z[145] + z[148] + -z[150];
z[72] = z[34] * z[72];
z[80] = T(2) * z[47];
z[111] = -z[4] + z[80];
z[124] = z[81] + z[111];
z[133] = T(4) * z[26];
z[141] = z[16] + z[133];
z[143] = T(4) * z[11];
z[145] = z[141] + -z[143];
z[148] = T(2) * z[14];
z[151] = T(3) * z[17];
z[152] = z[15] + -z[124] + -z[145] + z[148] + z[151];
z[153] = z[8] * z[152];
z[154] = z[63] + -z[133];
z[155] = -z[17] + z[83];
z[156] = -z[154] + z[155] / T(2);
z[157] = (T(3) * z[11]) / T(2);
z[158] = (T(5) * z[12]) / T(2);
z[159] = -z[117] + -z[156] + z[157] + z[158];
z[160] = z[10] * z[159];
z[161] = -z[28] + z[29];
z[161] = (T(-3) * z[161]) / T(2);
z[104] = z[18] + (T(-5) * z[104]) / T(2) + z[161];
z[162] = z[23] * z[104];
z[160] = z[160] + z[162];
z[163] = T(2) * z[27];
z[164] = -z[62] + -z[96] + z[99] + z[163];
z[165] = z[14] + z[74];
z[166] = z[165] / T(2);
z[167] = T(4) * z[4];
z[168] = -z[13] + z[65];
z[169] = -z[164] + -z[166] + z[167] + -z[168];
z[169] = z[3] * z[169];
z[170] = T(8) * z[27];
z[171] = T(4) * z[16];
z[172] = z[14] + z[151] + z[170] + -z[171];
z[173] = T(6) * z[4];
z[174] = z[81] + z[143] + -z[172] + z[173];
z[174] = z[7] * z[174];
z[112] = (T(3) * z[112]) / T(2);
z[175] = z[22] * z[112];
z[175] = T(3) * z[129] + z[175];
z[176] = T(4) * z[12];
z[177] = -z[143] + z[176];
z[178] = -z[66] + z[177];
z[179] = z[80] + -z[137];
z[180] = -z[17] + z[179];
z[181] = z[178] + z[180];
z[182] = z[31] * z[181];
z[183] = z[96] + z[158];
z[184] = -z[4] + z[163];
z[185] = -z[157] + z[183] + -z[184];
z[186] = z[74] + z[109];
z[186] = z[186] / T(2);
z[187] = -z[185] + z[186];
z[188] = z[6] * z[187];
z[189] = z[19] * z[104];
z[190] = -z[14] + z[17];
z[191] = z[13] + z[190];
z[178] = -z[178] + z[191];
z[178] = z[9] * z[178];
z[169] = -z[153] + z[160] + z[169] + z[174] + z[175] + z[178] + -z[182] + z[188] + z[189];
z[174] = int_to_imaginary<T>(1) * z[36];
z[169] = z[169] * z[174];
z[178] = z[171] + z[176];
z[188] = T(8) * z[11];
z[192] = T(5) * z[4];
z[193] = -z[151] + z[178] + -z[179] + -z[188] + -z[192];
z[193] = z[31] * z[193];
z[194] = T(7) * z[17];
z[195] = z[83] + z[194];
z[196] = (T(9) * z[16]) / T(2) + -z[195] / T(2);
z[197] = z[62] + -z[158];
z[198] = z[117] + z[133];
z[199] = z[196] + -z[197] + -z[198];
z[199] = z[10] * z[199];
z[200] = T(2) * z[8];
z[201] = z[7] + z[200];
z[152] = z[152] * z[201];
z[201] = (T(11) * z[11]) / T(2);
z[202] = -z[99] + z[201];
z[121] = -z[111] + z[121] + z[154] + z[202];
z[154] = z[3] * z[121];
z[203] = z[6] * z[159];
z[203] = z[189] + z[203];
z[204] = z[9] * z[181];
z[152] = -z[152] + z[154] + z[162] + z[175] + z[193] + z[199] + z[203] + -z[204];
z[152] = z[33] * z[152];
z[154] = T(3) * z[83];
z[175] = z[154] + z[194];
z[175] = z[175] / T(2);
z[59] = (T(7) * z[16]) / T(2) + -z[59] + z[157] + -z[175] + z[192];
z[59] = z[5] * z[59];
z[193] = z[8] * z[159];
z[199] = z[20] * z[104];
z[193] = z[193] + -z[199];
z[205] = T(2) * z[149];
z[206] = z[23] * z[205];
z[207] = z[21] * z[205];
z[208] = -z[11] + z[82];
z[209] = T(2) * z[208];
z[210] = z[2] * z[209];
z[211] = z[59] + -z[193] + -z[206] + z[207] + -z[210];
z[186] = -z[164] + -z[186] + z[192];
z[186] = z[3] * z[186];
z[212] = z[25] / T(2);
z[213] = T(5) * z[18];
z[161] = (T(-13) * z[24]) / T(2) + -z[161] + z[212] + -z[213];
z[214] = z[22] * z[161];
z[215] = T(6) * z[12];
z[172] = T(14) * z[4] + T(10) * z[11] + -z[172] + -z[215];
z[172] = z[7] * z[172];
z[216] = -z[83] + z[133];
z[217] = T(3) * z[16];
z[218] = T(-4) * z[17] + z[216] + z[217];
z[219] = T(2) * z[12];
z[220] = z[66] + -z[219];
z[221] = -z[143] + z[218] + z[220];
z[221] = z[10] * z[221];
z[75] = z[75] + z[151];
z[75] = z[75] / T(2) + -z[185];
z[75] = z[6] * z[75];
z[222] = T(3) * z[11];
z[223] = z[117] + z[222];
z[224] = -z[82] + z[223];
z[225] = -z[219] + z[224];
z[226] = T(2) * z[225];
z[227] = z[31] * z[226];
z[75] = z[75] + z[172] + z[186] + z[189] + -z[211] + z[214] + z[221] + z[227];
z[75] = z[0] * z[75];
z[172] = T(6) * z[27];
z[186] = (T(5) * z[17]) / T(2);
z[221] = (T(3) * z[16]) / T(2);
z[228] = T(11) * z[14] + -z[74];
z[228] = T(-4) * z[13] + -z[66] + z[158] + z[172] + -z[186] + -z[201] + -z[221] + z[228] / T(2);
z[228] = z[9] * z[228];
z[229] = T(4) * z[27];
z[230] = T(2) * z[16];
z[231] = z[229] + -z[230];
z[232] = z[81] + -z[109] + z[117] + z[135] + -z[231];
z[233] = T(2) * z[3];
z[234] = -(z[232] * z[233]);
z[88] = z[88] + z[164] + -z[173];
z[88] = z[7] * z[88];
z[121] = z[8] * z[121];
z[164] = z[170] + -z[178];
z[170] = T(5) * z[14];
z[78] = z[78] + z[91] + -z[164] + -z[170];
z[78] = z[6] * z[78];
z[178] = T(4) * z[24];
z[235] = T(3) * z[28];
z[236] = z[18] + z[235];
z[237] = -z[25] + z[178] + z[236];
z[238] = z[19] * z[237];
z[182] = -z[182] + z[238];
z[112] = z[112] * z[142];
z[78] = z[78] + z[88] + z[112] + z[121] + -z[160] + -z[182] + z[228] + z[234];
z[78] = z[35] * z[78];
z[88] = T(9) * z[4];
z[112] = z[88] + -z[215];
z[121] = z[17] + z[230];
z[228] = T(3) * z[137];
z[234] = z[121] + z[228];
z[238] = z[112] + -z[135] + z[234];
z[238] = z[2] * z[238];
z[239] = z[8] * z[181];
z[240] = z[20] * z[237];
z[239] = -z[239] + z[240];
z[226] = z[5] * z[226];
z[207] = -z[207] + z[226] + z[238] + -z[239];
z[238] = z[4] + z[135];
z[240] = -z[230] + z[238];
z[109] = -z[13] + z[109] + -z[176] + z[229] + z[240];
z[109] = z[9] * z[109];
z[241] = T(-7) * z[27] + z[188] + -z[215];
z[242] = T(2) * z[17];
z[148] = T(18) * z[4] + z[13] + z[148] + z[230] + z[241] + z[242];
z[148] = z[7] * z[148];
z[243] = z[47] + z[137];
z[244] = T(3) * z[12];
z[245] = -z[16] + z[244];
z[246] = -z[11] + z[192] + z[243] + -z[245];
z[247] = T(2) * z[31];
z[246] = z[246] * z[247];
z[213] = z[213] + z[235];
z[235] = T(8) * z[24] + z[25] + z[213];
z[248] = z[22] * z[235];
z[209] = z[10] * z[209];
z[249] = z[4] + -z[191];
z[249] = z[3] * z[249];
z[109] = z[109] + z[148] + -z[207] + -z[209] + z[246] + z[248] + z[249];
z[109] = z[32] * z[109];
z[148] = z[4] + z[180];
z[246] = z[3] * z[148];
z[209] = -z[209] + z[246] + -z[248];
z[139] = -z[139] + -z[192] + z[244];
z[246] = T(2) * z[7];
z[139] = z[139] * z[246];
z[243] = -z[17] + z[243];
z[243] = T(2) * z[243];
z[249] = z[4] + z[16];
z[250] = T(7) * z[11];
z[251] = -z[243] + -z[249] + z[250];
z[251] = z[247] * z[251];
z[139] = z[139] + z[204] + z[207] + z[209] + z[251];
z[139] = z[37] * z[139];
z[207] = z[83] + z[151];
z[251] = -z[12] + z[230];
z[252] = -z[143] + z[198] + -z[207] + z[251];
z[253] = T(2) * z[10];
z[252] = z[252] * z[253];
z[156] = z[117] + -z[156] + z[202];
z[202] = z[3] * z[156];
z[202] = z[202] + z[214] + -z[227];
z[227] = z[83] + z[242];
z[141] = -z[141] + z[227];
z[136] = z[136] + -z[141] + -z[215];
z[136] = z[7] * z[136];
z[136] = z[136] + z[202] + z[203] + -z[211] + z[252];
z[211] = -(z[30] * z[136]);
z[215] = -z[32] + z[37];
z[252] = z[7] + z[31];
z[254] = -z[2] + z[252];
z[255] = T(6) * z[1];
z[215] = z[215] * z[254] * z[255];
z[72] = z[72] + z[75] + z[78] + z[109] + z[139] + z[152] + z[169] + z[211] + z[215];
z[72] = z[34] * z[72];
z[75] = (T(5) * z[4]) / T(2);
z[78] = z[14] + -z[103];
z[78] = (T(-3) * z[17]) / T(4) + -z[75] + z[78] / T(4) + -z[90] + -z[91] + -z[93] + z[97] + -z[107];
z[78] = z[3] * z[78];
z[93] = T(6) * z[26];
z[97] = (T(11) * z[4]) / T(2) + z[16] + z[93] + -z[97] + z[122] + -z[157] + -z[219] + -z[242];
z[97] = z[6] * z[97];
z[103] = z[151] + z[165];
z[103] = z[27] + z[103] / T(2) + z[158] + -z[192] + -z[222] + -z[230];
z[103] = z[7] * z[103];
z[107] = z[12] + z[222];
z[109] = -z[96] + z[107] + -z[127] + z[151];
z[109] = z[10] * z[109];
z[127] = z[214] / T(2);
z[139] = T(5) * z[17];
z[60] = T(3) * z[60] + z[139];
z[60] = z[60] / T(2);
z[157] = z[4] + -z[60] + z[96] + -z[157];
z[165] = (T(5) * z[12]) / T(4);
z[157] = -z[27] + z[157] / T(2) + z[165];
z[157] = z[9] * z[157];
z[169] = T(5) * z[11];
z[211] = z[169] + z[207] + z[245];
z[215] = -z[68] + z[211] / T(4);
z[242] = T(3) * z[2];
z[215] = z[215] * z[242];
z[165] = -z[4] + z[165];
z[120] = (T(-3) * z[11]) / T(4) + -z[120] + z[155] / T(4) + -z[165];
z[245] = z[8] * z[120];
z[256] = z[21] * z[149];
z[59] = z[59] + z[78] + z[97] + z[103] + z[109] + -z[127] + -z[150] + z[157] + -z[189] + z[199] / T(2) + z[215] + z[245] + z[256];
z[59] = z[0] * z[59];
z[78] = z[207] / T(2);
z[97] = z[78] + z[95] + -z[183] + z[198];
z[97] = z[10] * z[97];
z[103] = z[7] * z[156];
z[109] = z[4] + z[107] + z[141];
z[141] = T(2) * z[6] + z[200];
z[141] = z[109] * z[141];
z[104] = z[21] * z[104];
z[156] = T(9) * z[12] + z[154];
z[157] = T(-11) * z[11] + -z[16] + -z[139] + -z[156];
z[199] = T(12) * z[26];
z[157] = z[157] / T(2) + z[199];
z[157] = z[2] * z[157];
z[215] = z[20] * z[205];
z[245] = -(z[19] * z[205]);
z[97] = z[97] + z[103] + z[104] + z[141] + z[157] + -z[162] + -z[202] + z[215] + -z[226] + z[245];
z[97] = z[33] * z[97];
z[103] = z[13] + z[15];
z[103] = T(3) * z[103] + -z[229];
z[141] = -z[12] + z[103] + z[224];
z[141] = -(z[141] * z[147]);
z[157] = z[5] + -z[31];
z[157] = z[157] * z[225];
z[103] = z[12] + z[103] + -z[208];
z[103] = z[3] * z[103];
z[202] = -z[4] + z[79];
z[224] = z[202] * z[246];
z[245] = z[2] * z[208];
z[256] = z[10] * z[208];
z[257] = z[19] * z[149];
z[103] = z[103] + z[141] + z[157] + z[224] + -z[245] + z[256] + z[257];
z[103] = z[32] * z[103];
z[59] = z[59] + z[97] + T(2) * z[103];
z[59] = z[0] * z[59];
z[97] = z[80] + z[81];
z[103] = (T(7) * z[11]) / T(2);
z[141] = T(8) * z[26];
z[157] = z[4] + -z[86] + -z[87] + z[97] + -z[103] + z[141] + -z[158] + -z[221];
z[157] = z[7] * z[157];
z[158] = z[74] + -z[170];
z[224] = (T(15) * z[17]) / T(4);
z[97] = (T(21) * z[12]) / T(4) + (T(25) * z[16]) / T(4) + -z[92] + -z[93] + -z[97] + z[158] / T(4) + -z[167] + -z[224];
z[97] = z[31] * z[97];
z[158] = T(-9) * z[14] + -z[15];
z[116] = (T(19) * z[11]) / T(4) + (T(21) * z[16]) / T(4) + -z[68] + -z[116] + -z[124] + z[158] / T(4) + -z[224];
z[116] = z[3] * z[116];
z[124] = z[20] * z[161];
z[124] = z[124] + z[162] + z[189];
z[123] = (T(9) * z[16]) / T(4) + -z[68] + -z[123] + z[165] + -z[195] / T(4);
z[123] = z[10] * z[123];
z[158] = T(4) * z[47];
z[162] = T(-21) * z[14] + -z[15];
z[90] = (T(-29) * z[11]) / T(4) + z[13] + (T(13) * z[16]) / T(4) + (T(-43) * z[17]) / T(4) + T(10) * z[26] + z[90] + z[158] + z[162] / T(4) + -z[167];
z[90] = z[8] * z[90];
z[162] = T(5) * z[16];
z[165] = z[17] + z[156] + z[162] + z[250];
z[189] = -z[93] + z[165] / T(4);
z[189] = z[2] * z[189];
z[120] = -(z[6] * z[120]);
z[195] = z[79] + z[180];
z[224] = -(z[9] * z[195]);
z[90] = z[90] + z[97] + -z[110] + z[116] + z[120] + z[123] + z[124] / T(2) + z[127] + z[157] + z[189] + z[224];
z[97] = prod_pow(z[33], 2);
z[90] = z[90] * z[97];
z[76] = -z[16] + z[65] + z[76];
z[94] = -z[74] + -z[76] + -z[94] + -z[105] + -z[132] + z[229];
z[94] = z[3] * z[94];
z[116] = -z[17] + z[88] + z[228];
z[116] = z[116] / T(2) + -z[135] + z[230] + -z[244];
z[116] = z[2] * z[116];
z[120] = z[135] + z[251];
z[123] = -z[17] + z[137];
z[123] = z[80] + -z[120] + z[123] / T(2) + -z[131];
z[123] = z[31] * z[123];
z[124] = z[178] + z[212] + z[213] / T(2);
z[124] = z[22] * z[124];
z[127] = z[20] * z[149];
z[124] = z[124] + z[127];
z[71] = z[17] + z[71] + z[88] + z[170];
z[71] = z[71] / T(2) + z[79] + -z[172];
z[71] = z[9] * z[71];
z[76] = z[12] + z[13] + z[27] / T(2) + -z[75] + z[76] + -z[135];
z[76] = z[7] * z[76];
z[79] = z[13] + -z[14];
z[127] = z[4] + -z[17] + T(3) * z[79];
z[131] = z[127] / T(2) + z[219] + -z[231];
z[131] = z[6] * z[131];
z[132] = T(2) * z[24] + -z[212] + z[236] / T(2);
z[157] = z[19] + z[21];
z[172] = -(z[132] * z[157]);
z[178] = -(z[8] * z[195]);
z[71] = z[71] + z[76] + z[94] + z[116] + z[123] + -z[124] + z[131] + z[172] + z[178];
z[71] = z[32] * z[71];
z[76] = z[101] + z[200];
z[76] = z[76] * z[195];
z[94] = z[17] + z[179] + z[240];
z[116] = z[31] * z[94];
z[123] = T(6) * z[47];
z[131] = -z[4] + -z[123] + -z[219] + z[234];
z[131] = z[5] * z[131];
z[148] = z[7] * z[148];
z[172] = z[21] * z[237];
z[76] = z[76] + -z[116] + z[131] + z[148] + z[172] + -z[209] + -z[210] + z[215];
z[116] = z[33] * z[76];
z[71] = z[71] + -z[116];
z[71] = z[32] * z[71];
z[148] = T(3) * z[47];
z[178] = z[105] + z[148];
z[179] = z[139] + z[228];
z[189] = T(7) * z[16];
z[179] = z[178] + z[179] / T(2) + -z[189] + -z[219] + z[222];
z[179] = z[10] * z[179];
z[200] = z[137] + z[151];
z[120] = z[105] + z[120] + -z[158] + z[200] / T(2);
z[120] = z[7] * z[120];
z[158] = z[66] + z[169] + -z[243] + z[251];
z[158] = z[31] * z[158];
z[200] = T(9) * z[17];
z[209] = -z[137] + z[200];
z[210] = -z[11] + z[251];
z[75] = z[47] + z[75] + z[209] / T(2) + -z[210];
z[75] = z[3] * z[75];
z[209] = z[8] + z[9];
z[195] = -(z[195] * z[209]);
z[212] = -z[21] + -z[23];
z[132] = z[132] * z[212];
z[212] = -z[66] + -z[138];
z[212] = z[12] + z[212] / T(2);
z[212] = z[212] * z[242];
z[75] = z[75] + z[120] + -z[124] + -z[131] + z[132] + z[158] + z[179] + z[195] + z[212];
z[75] = z[37] * z[75];
z[76] = z[32] * z[76];
z[75] = z[75] + z[76] + z[116];
z[75] = z[37] * z[75];
z[76] = T(5) * z[137];
z[116] = T(11) * z[17];
z[88] = -z[76] + -z[80] + z[88] + z[116] + z[188] + z[219] + z[230];
z[88] = z[3] * z[88];
z[120] = T(10) * z[16];
z[76] = -z[76] + -z[111] + z[120] + z[143] + -z[200] + -z[219];
z[76] = z[31] * z[76];
z[124] = z[80] + z[137] + -z[151];
z[124] = T(-6) * z[16] + T(3) * z[124] + -z[134] + -z[143] + -z[219];
z[124] = z[10] * z[124];
z[131] = -z[17] + z[171];
z[132] = z[131] + z[228];
z[134] = T(6) * z[11] + z[192];
z[158] = z[123] + -z[132] + z[134] + -z[219];
z[158] = z[5] * z[158];
z[179] = z[135] + z[180] + z[220];
z[179] = -(z[179] * z[209]);
z[94] = -(z[94] * z[246]);
z[195] = z[10] + z[31];
z[200] = -z[3] + z[195];
z[200] = z[200] * z[255];
z[212] = -z[20] + -z[23];
z[212] = z[212] * z[235];
z[76] = z[76] + z[88] + z[94] + z[124] + z[158] + z[179] + z[200] + z[212] + -z[248];
z[76] = z[40] * z[76];
z[88] = -z[83] + -z[141] + z[188] + z[194] + z[249];
z[88] = z[10] * z[88];
z[94] = -z[12] + z[194];
z[124] = z[94] + z[167] + z[169] + -z[171];
z[124] = z[31] * z[124];
z[107] = -z[16] + z[107] + -z[198] + z[227];
z[107] = z[7] * z[107];
z[158] = z[6] + z[8];
z[109] = -(z[109] * z[158]);
z[171] = z[19] + -z[142];
z[171] = z[149] * z[171];
z[145] = z[4] + z[145] + -z[155];
z[155] = z[3] * z[145];
z[179] = -z[31] + z[158];
z[146] = z[146] * z[179];
z[88] = z[88] + z[107] + z[109] + z[124] + z[146] + z[155] + z[171];
z[88] = z[30] * z[88];
z[104] = z[104] + z[193];
z[107] = z[104] + z[203];
z[109] = (T(13) * z[11]) / T(2);
z[124] = (T(13) * z[12]) / T(2) + -z[109] + -z[133] + -z[173] + z[196];
z[124] = z[7] * z[124];
z[94] = -z[94] + -z[192] + -z[216] + z[230];
z[94] = z[94] * z[253];
z[146] = z[4] + z[12] + z[169] + -z[218];
z[146] = z[3] * z[146];
z[146] = z[146] + -z[245];
z[155] = T(-19) * z[11] + T(-13) * z[17] + -z[156] + z[189];
z[155] = z[155] / T(2) + z[199];
z[155] = z[31] * z[155];
z[94] = z[94] + -z[107] + z[124] + T(2) * z[146] + z[155] + -z[206] + z[226];
z[94] = z[33] * z[94];
z[124] = -z[117] + -z[139] + z[210];
z[124] = z[3] * z[124];
z[146] = z[117] + z[151];
z[146] = -z[12] + z[143] + T(2) * z[146] + -z[217];
z[146] = z[10] * z[146];
z[151] = z[4] + z[151];
z[135] = z[12] + z[135] + T(2) * z[151] + -z[217];
z[135] = z[31] * z[135];
z[82] = -z[12] + -z[82] + z[238];
z[82] = z[82] * z[246];
z[151] = -(z[5] * z[225]);
z[82] = z[82] + z[124] + z[135] + z[146] + z[150] + z[151] + z[245];
z[82] = z[37] * z[82];
z[124] = z[0] * z[136];
z[135] = z[116] + -z[154];
z[136] = z[167] + z[199];
z[135] = z[135] / T(2) + z[136] + -z[183] + -z[201];
z[135] = z[31] * z[135];
z[146] = z[7] * z[159];
z[150] = z[233] + z[253];
z[150] = z[145] * z[150];
z[107] = -z[107] + z[135] + z[146] + -z[150];
z[135] = z[107] * z[174];
z[146] = z[33] + -z[37];
z[146] = z[146] * z[200];
z[82] = T(2) * z[82] + z[88] + z[94] + z[124] + z[135] + z[146];
z[82] = z[30] * z[82];
z[88] = T(5) * z[83] + z[194];
z[94] = z[70] + z[133];
z[124] = (T(9) * z[11]) / T(2);
z[88] = -z[88] / T(2) + z[94] + z[96] + -z[124] + -z[167];
z[88] = z[88] * z[158];
z[135] = z[11] + -z[140] + z[141] + -z[162] + z[167] + z[207];
z[135] = z[7] * z[135];
z[103] = (T(17) * z[12]) / T(2) + z[103] + -z[136] + (T(3) * z[207]) / T(2) + -z[221];
z[103] = z[31] * z[103];
z[136] = z[19] + z[23];
z[140] = z[20] + -z[136];
z[140] = z[140] * z[161];
z[141] = z[165] / T(2) + -z[199];
z[141] = z[2] * z[141];
z[146] = -z[94] + z[221];
z[139] = -z[83] + z[139];
z[95] = T(8) * z[4] + -z[95] + (T(5) * z[139]) / T(2) + -z[146];
z[95] = z[10] * z[95];
z[139] = -z[100] + z[158] + -z[195];
z[139] = z[139] * z[255];
z[150] = T(-9) * z[11] + T(-7) * z[12] + T(13) * z[16] + T(-5) * z[207];
z[150] = z[133] + z[150] / T(2);
z[150] = z[3] * z[150];
z[88] = z[88] + z[95] + z[103] + z[135] + z[139] + z[140] + z[141] + z[150] + z[214];
z[88] = z[42] * z[88];
z[95] = z[17] + -z[120] + z[123] + -z[176] + z[228] + z[238];
z[95] = z[10] * z[95];
z[103] = T(2) * z[137] + -z[217] + -z[223];
z[80] = -z[17] + z[80];
z[120] = z[80] + z[103];
z[123] = z[233] + z[247];
z[123] = z[120] * z[123];
z[135] = z[7] * z[181];
z[139] = -z[172] + z[239];
z[140] = z[23] * z[237];
z[95] = z[95] + -z[123] + -z[135] + -z[139] + -z[140] + z[204];
z[95] = z[37] * z[95];
z[123] = z[177] + z[191] + -z[192];
z[123] = z[7] * z[123];
z[79] = z[79] + z[108] + -z[184];
z[108] = z[3] + z[9];
z[108] = z[79] * z[108];
z[127] = -z[127] + z[164];
z[127] = z[6] * z[127];
z[108] = T(4) * z[108] + -z[123] + z[127] + -z[139] + z[182];
z[108] = z[32] * z[108];
z[60] = z[60] + -z[185];
z[60] = z[9] * z[60];
z[123] = z[7] * z[187];
z[127] = z[190] * z[233];
z[135] = z[6] * z[190];
z[60] = -z[60] + z[104] + z[123] + z[127] + T(2) * z[135] + -z[160];
z[60] = z[0] * z[60];
z[60] = z[60] + z[95] + -z[108] + z[152];
z[95] = -z[15] + z[170];
z[95] = z[91] + z[95] / T(2) + -z[186];
z[68] = (T(-11) * z[16]) / T(4) + -z[68] + -z[92] + z[95] / T(2) + -z[105] + -z[118] + z[148];
z[68] = z[10] * z[68];
z[92] = -z[122] + z[168];
z[84] = -z[84] + z[92] / T(2) + -z[117] + -z[118] + z[229];
z[84] = z[7] * z[84];
z[74] = z[74] + z[170];
z[73] = z[13] + z[73] + z[74] / T(2);
z[74] = (T(-27) * z[11]) / T(4) + (T(3) * z[16]) / T(4) + z[93];
z[73] = z[73] / T(2) + -z[74] + -z[118] + -z[178];
z[73] = z[3] * z[73];
z[79] = z[79] * z[101];
z[79] = z[79] + z[110] + -z[135];
z[92] = z[13] + -z[166] + z[186];
z[74] = -z[47] + z[74] + z[92] / T(2) + -z[119];
z[74] = z[31] * z[74];
z[92] = z[22] + -z[23] + -z[142];
z[92] = z[92] * z[144];
z[68] = z[68] + z[73] + z[74] + z[79] + z[84] + -z[89] + z[92] + z[130];
z[68] = z[35] * z[68];
z[73] = z[7] * z[232];
z[74] = -z[17] + -z[103];
z[74] = z[3] * z[74];
z[84] = -(z[31] * z[120]);
z[89] = -(z[10] * z[145]);
z[73] = z[73] + z[74] + -z[79] + z[84] + z[89] + -z[153];
z[73] = z[73] * z[174];
z[74] = -(z[30] * z[107]);
z[68] = -z[60] + z[68] + T(2) * z[73] + z[74];
z[68] = z[35] * z[68];
z[73] = z[86] + -z[94];
z[64] = -z[64] + z[73] + -z[111] + z[124] + z[186];
z[64] = z[8] * z[64];
z[74] = z[61] + z[109] + -z[111] + z[146] + z[168];
z[74] = z[3] * z[74];
z[73] = z[13] + z[62] + z[73] + -z[87] + -z[96] + z[111];
z[73] = z[73] * z[98];
z[79] = z[7] * z[128];
z[84] = z[114] + z[129];
z[86] = z[21] * z[113];
z[86] = z[86] + -z[115];
z[87] = z[23] * z[113];
z[64] = z[64] + z[73] + -z[74] + z[79] + -z[84] + -z[86] + z[87];
z[64] = T(3) * z[64];
z[73] = z[46] * z[64];
z[74] = -z[70] + z[163];
z[79] = -z[15] + z[85];
z[79] = z[65] + -z[74] + -z[79] / T(2) + z[125];
z[87] = -z[6] + z[9];
z[79] = z[79] * z[87];
z[61] = z[61] + -z[74] + z[126];
z[74] = z[3] + -z[7];
z[61] = z[61] * z[74];
z[74] = z[8] * z[128];
z[87] = z[19] * z[113];
z[61] = z[61] + z[74] + z[79] + -z[84] + z[86] + z[87];
z[61] = T(3) * z[61];
z[74] = z[51] * z[61];
z[79] = z[8] + -z[102];
z[79] = z[57] * z[79];
z[84] = z[54] / T(2) + -z[56];
z[86] = z[54] * z[84];
z[86] = z[57] + z[86];
z[86] = z[6] * z[86];
z[79] = z[79] + z[86];
z[86] = -z[2] + z[31];
z[87] = z[3] + -z[10] + z[86] + z[101];
z[87] = z[56] * z[87];
z[84] = z[8] * z[84];
z[84] = z[84] + z[87];
z[87] = z[2] / T(2);
z[89] = -z[3] / T(2) + -z[9] + z[87];
z[89] = (T(-4) * z[22]) / T(5) + (T(-2) * z[23]) / T(5) + T(3) * z[89] + (T(-3) * z[98]) / T(2);
z[89] = z[54] * z[89];
z[84] = T(3) * z[84] + z[89];
z[84] = z[54] * z[84];
z[87] = z[3] / T(3) + T(3) * z[9] + -z[87];
z[87] = -z[6] / T(4) + -z[7] + (T(-11) * z[10]) / T(12) + -z[19] / T(9) + z[20] / T(3) + (T(56) * z[21]) / T(135) + (T(101) * z[22]) / T(135) + (T(-17) * z[23]) / T(135) + (T(-7) * z[31]) / T(6) + z[87] / T(2);
z[87] = z[69] * z[87];
z[89] = z[21] * prod_pow(z[54], 2);
z[60] = z[60] + z[73] + z[74] + T(3) * z[79] + z[84] + z[87] + (T(-4) * z[89]) / T(5);
z[60] = z[60] * z[174];
z[73] = z[116] + z[154];
z[73] = -z[4] + (T(-11) * z[16]) / T(2) + -z[70] + z[73] / T(2) + z[124];
z[74] = -z[5] + z[10];
z[73] = z[73] * z[74];
z[70] = T(10) * z[27] + -z[70] + -z[124];
z[74] = -z[63] + z[70] + -z[192];
z[79] = z[81] + z[166];
z[65] = -z[65] + -z[74] + T(3) * z[79];
z[79] = -z[3] + z[9];
z[65] = z[65] * z[79];
z[79] = -z[21] + z[136];
z[79] = z[79] * z[161];
z[70] = T(-11) * z[4] + (T(-13) * z[16]) / T(2) + z[70] + z[175];
z[70] = z[7] * z[70];
z[81] = z[81] + z[122];
z[74] = -z[74] + T(3) * z[81] + z[186];
z[74] = z[6] * z[74];
z[81] = -z[3] + z[147];
z[84] = -(z[81] * z[255]);
z[65] = z[65] + z[70] + z[73] + z[74] + z[79] + z[84] + -z[214];
z[65] = z[39] * z[65];
z[62] = z[62] + -z[63] + z[78] + z[99] + -z[117];
z[62] = z[41] * z[62];
z[70] = z[41] * z[133];
z[62] = z[62] + -z[70];
z[62] = z[10] * z[62];
z[73] = -z[180] + -z[219] + z[238];
z[74] = -z[5] + z[209];
z[73] = z[73] * z[74];
z[74] = z[4] + z[80] + z[137] + -z[143];
z[74] = z[31] * z[74];
z[78] = z[18] + z[25] + -z[28];
z[78] = -(z[78] * z[142]);
z[79] = z[138] + z[220];
z[80] = -z[2] + z[7];
z[79] = z[79] * z[80];
z[73] = z[73] + z[74] + z[78] + z[79];
z[73] = z[38] * z[73];
z[74] = z[17] + z[83];
z[63] = z[63] + z[66] + -z[74] / T(2) + z[197];
z[63] = z[41] * z[63];
z[66] = -(z[7] * z[63]);
z[63] = z[5] * z[63];
z[74] = z[41] / T(2);
z[78] = z[74] * z[211];
z[70] = -z[70] + z[78];
z[78] = z[2] * z[70];
z[79] = z[18] + z[24] / T(2) + (T(3) * z[25]) / T(2) + -z[106];
z[79] = z[41] * z[79];
z[74] = z[29] * z[74];
z[74] = z[74] + z[79];
z[79] = -(z[23] * z[74]);
z[62] = z[62] + z[63] + z[66] + z[73] + z[78] + z[79];
z[63] = -(z[45] * z[64]);
z[64] = z[77] + z[137];
z[64] = -z[4] + T(3) * z[64] + -z[131] + z[219] + -z[229];
z[64] = z[9] * z[64];
z[66] = -z[14] + z[77] + z[91];
z[66] = T(-16) * z[27] + T(3) * z[66] + z[121] + z[134] + z[219];
z[73] = -(z[3] * z[66]);
z[77] = z[112] + z[132] + -z[143];
z[77] = -(z[77] * z[86]);
z[78] = T(-19) * z[4] + -z[85] + -z[121] + -z[241];
z[78] = z[7] * z[78];
z[64] = z[64] + z[73] + z[77] + z[78] + -z[248];
z[64] = z[43] * z[64];
z[61] = z[50] * z[61];
z[73] = z[9] + z[100];
z[77] = -z[73] + z[252];
z[77] = z[43] * z[77];
z[78] = -z[2] + z[8];
z[78] = z[41] * z[78];
z[79] = z[41] + -z[43];
z[79] = z[6] * z[79];
z[80] = z[38] * z[254];
z[77] = z[77] + z[78] + z[79] + -z[80];
z[78] = z[8] + -z[100];
z[78] = z[78] * z[97];
z[79] = z[2] + -z[158];
z[79] = z[33] * z[79];
z[80] = z[32] * z[81];
z[79] = z[79] + z[80];
z[80] = -(z[0] * z[100]);
z[79] = T(2) * z[79] + z[80];
z[79] = z[0] * z[79];
z[73] = -(prod_pow(z[32], 2) * z[73]);
z[80] = prod_pow(z[37], 2) * z[100];
z[73] = z[73] + T(2) * z[77] + z[78] + z[79] + z[80];
z[77] = -z[7] + z[8] + -z[9] + z[10];
z[69] = z[69] * z[77];
z[69] = z[69] + T(3) * z[73];
z[69] = z[1] * z[69];
z[66] = z[43] * z[66];
z[70] = T(3) * z[70];
z[66] = (T(-5) * z[58]) / T(2) + z[66] + -z[70];
z[66] = z[6] * z[66];
z[73] = z[21] + z[22];
z[73] = T(2) * z[73];
z[77] = -(z[73] * z[202]);
z[78] = -z[2] + -z[3] + -z[5];
z[78] = z[78] * z[149];
z[79] = z[52] * z[205];
z[80] = -(z[23] * z[208]);
z[77] = z[77] + z[78] + z[79] + z[80];
z[77] = z[44] * z[77];
z[78] = z[43] * z[235];
z[74] = T(3) * z[74];
z[78] = -z[74] + z[78];
z[78] = -(z[78] * z[157]);
z[70] = (T(-85) * z[58]) / T(6) + -z[70];
z[70] = z[8] * z[70];
z[74] = -(z[20] * z[74]);
z[79] = (T(161) * z[2]) / T(2) + T(13) * z[3] + T(-83) * z[7] + T(10) * z[9] + (T(53) * z[10]) / T(2) + T(-11) * z[31];
z[79] = z[58] * z[79];
z[80] = T(3) * z[19] + T(-9) * z[20] + T(-4) * z[21] + T(-13) * z[22] + T(7) * z[23];
z[80] = z[36] * z[53] * z[80];
z[73] = z[23] + z[73];
z[73] = int_to_imaginary<T>(1) * z[55] * z[73];
return z[59] + z[60] + z[61] + T(3) * z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + z[68] + z[69] + z[70] + z[71] + z[72] + (T(96) * z[73]) / T(5) + z[74] + z[75] + z[76] + T(2) * z[77] + z[78] + z[79] / T(3) + z[80] + z[82] + z[88] + z[90];
}



template IntegrandConstructorType<double> f_4_288_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_288_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_288_construct (const Kin<qd_real>&);
#endif

}