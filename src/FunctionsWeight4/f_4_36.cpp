#include "f_4_36.h"

namespace PentagonFunctions {

template <typename T> T f_4_36_abbreviated (const std::array<T,17>&);



template <typename T> IntegrandConstructorType<T> f_4_36_construct (const Kin<T>& kin) {
    return [&kin, 
            dl3 = DLog_W_3<T>(kin),dl1 = DLog_W_1<T>(kin),dl4 = DLog_W_4<T>(kin),dl18 = DLog_W_18<T>(kin),dl11 = DLog_W_11<T>(kin),dl6 = DLog_W_6<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,17> abbr = 
            {dl3(t), rlog(kin.W[2] / kin_path.W[2]), rlog(v_path[0]), rlog(v_path[3]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_2(kin_path), f_2_1_9(kin_path), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[10] / kin_path.W[10]), rlog(kin.W[17] / kin_path.W[17]), dl1(t), rlog(kin.W[5] / kin_path.W[5]), rlog(kin.W[0] / kin_path.W[0]), dl4(t), dl18(t), dl11(t), dl6(t)}
;

        auto result = f_4_36_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_36_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[48];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = bc<TR>[2];
z[12] = bc<TR>[1];
z[13] = bc<TR>[4];
z[14] = bc<TR>[7];
z[15] = bc<TR>[8];
z[16] = bc<TR>[9];
z[17] = abb[13];
z[18] = abb[14];
z[19] = abb[15];
z[20] = abb[16];
z[21] = abb[10];
z[22] = abb[11];
z[23] = abb[12];
z[24] = z[8] + z[10];
z[25] = -z[0] + z[19];
z[26] = z[24] * z[25];
z[26] = T(2) * z[26];
z[27] = z[17] + z[18];
z[28] = T(3) * z[0];
z[29] = -z[27] + z[28];
z[30] = T(2) * z[19] + -z[29];
z[31] = z[1] * z[30];
z[32] = -z[22] + z[23];
z[33] = -z[9] + z[24] + (T(3) * z[32]) / T(2);
z[33] = z[21] * z[33];
z[34] = -z[0] + z[27];
z[35] = -(z[9] * z[34]);
z[31] = -z[26] + z[31] + z[33] + z[35];
z[33] = prod_pow(z[2], 2);
z[31] = z[31] * z[33];
z[35] = T(2) * z[27];
z[28] = -z[19] + z[28] + -z[35];
z[36] = z[1] * z[28];
z[35] = -z[0] + -z[19] + z[35];
z[35] = z[9] * z[35];
z[26] = z[26] + z[35] + z[36];
z[35] = z[2] * z[26];
z[36] = z[32] / T(2);
z[37] = z[24] + z[36];
z[38] = -z[9] + z[37];
z[38] = z[21] * z[38];
z[39] = z[1] + -z[9];
z[40] = z[17] * z[39];
z[41] = z[18] * z[32];
z[40] = z[40] + -z[41];
z[42] = -z[38] + z[40];
z[42] = z[4] * z[42];
z[35] = z[35] + z[42];
z[35] = z[4] * z[35];
z[42] = T(2) * z[24];
z[43] = T(3) * z[1] + -z[9] + -z[42];
z[43] = z[34] * z[43];
z[38] = -z[38] + z[43];
z[38] = z[6] * z[38];
z[43] = -z[2] + z[4];
z[44] = -(z[26] * z[43]);
z[45] = -z[18] + z[19];
z[45] = z[3] * z[39] * z[45];
z[44] = z[44] + -z[45];
z[44] = z[3] * z[44];
z[46] = prod_pow(z[4], 2);
z[33] = -z[33] + z[46];
z[37] = -z[1] + z[37];
z[33] = z[33] * z[37];
z[36] = -z[1] + z[24] + -z[36];
z[36] = z[6] * z[36];
z[37] = prod_pow(z[12], 3);
z[46] = z[14] + z[37];
z[47] = -z[15] / T(2) + -z[46];
z[33] = z[33] + z[36] + z[47] / T(2);
z[33] = z[20] * z[33];
z[26] = -(z[5] * z[26]);
z[36] = T(3) * z[14] + z[37];
z[37] = T(2) * z[0] + -z[19] + -z[27];
z[36] = z[36] * z[37];
z[28] = z[12] * z[13] * z[28];
z[29] = -z[19] + z[21] / T(4) + z[29] / T(2);
z[47] = z[15] * z[29];
z[46] = z[21] * z[46];
z[26] = z[26] + T(2) * z[28] + z[31] + z[33] + z[35] + z[36] + z[38] + z[44] + z[46] / T(2) + z[47];
z[28] = prod_pow(z[12], 2);
z[25] = z[25] * z[28];
z[31] = -(z[13] * z[37]);
z[25] = z[25] + z[31] + z[45];
z[31] = T(2) * z[9];
z[33] = z[31] + T(-3) * z[32] + -z[42];
z[33] = z[21] * z[33];
z[35] = -z[19] + z[27];
z[35] = z[35] * z[39];
z[33] = z[33] + T(2) * z[35];
z[33] = z[2] * z[33];
z[35] = z[32] + z[42];
z[31] = -z[31] + z[35];
z[31] = z[21] * z[31];
z[31] = z[31] + T(-2) * z[40];
z[31] = z[4] * z[31];
z[35] = T(2) * z[1] + -z[35];
z[35] = z[35] * z[43];
z[28] = -z[13] + z[28];
z[35] = z[28] + z[35];
z[35] = z[20] * z[35];
z[28] = -(z[21] * z[28]);
z[25] = T(2) * z[25] + z[28] + z[31] + z[33] + z[35];
z[25] = int_to_imaginary<T>(1) * z[25];
z[28] = -z[12] + -z[32];
z[28] = z[21] * z[28];
z[31] = z[12] * z[20];
z[28] = z[28] + z[31] + z[41];
z[31] = int_to_imaginary<T>(-1) * z[7];
z[24] = z[24] + z[31];
z[24] = z[24] * z[34];
z[31] = -z[0] + z[18];
z[32] = z[9] * z[31];
z[31] = -z[17] + (T(-3) * z[31]) / T(2);
z[31] = z[1] * z[31];
z[24] = z[24] + (T(13) * z[28]) / T(4) + z[31] + z[32] / T(2);
z[24] = z[7] * z[24];
z[24] = z[24] + T(3) * z[25];
z[24] = z[7] * z[24];
z[25] = z[20] / T(4) + -z[29];
z[25] = prod_pow(z[7], 2) * z[25];
z[28] = z[20] + -z[21];
z[29] = z[28] + T(2) * z[30];
z[29] = prod_pow(z[11], 2) * z[29];
z[25] = z[25] + z[29];
z[25] = z[11] * z[25];
z[27] = T(-111) * z[0] + T(41) * z[27];
z[27] = T(35) * z[19] + z[27] / T(2) + (T(21) * z[28]) / T(4);
z[27] = z[16] * z[27];
return z[24] + z[25] + T(3) * z[26] + z[27] / T(2);
}



template IntegrandConstructorType<double> f_4_36_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_36_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_36_construct (const Kin<qd_real>&);
#endif

}