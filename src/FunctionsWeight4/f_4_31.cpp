#include "f_4_31.h"

namespace PentagonFunctions {

template <typename T> T f_4_31_abbreviated (const std::array<T,16>&);



template <typename T> IntegrandConstructorType<T> f_4_31_construct (const Kin<T>& kin) {
    return [&kin, 
            dl6 = DLog_W_6<T>(kin),dl3 = DLog_W_3<T>(kin),dl1 = DLog_W_1<T>(kin),dl11 = DLog_W_11<T>(kin),dl4 = DLog_W_4<T>(kin),dl18 = DLog_W_18<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,16> abbr = 
            {dl6(t), rlog(kin.W[0] / kin_path.W[0]), rlog(v_path[0]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_9(kin_path), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[17] / kin_path.W[17]), dl3(t), rlog(v_path[3]), f_2_1_2(kin_path), rlog(kin.W[10] / kin_path.W[10]), dl1(t), dl11(t), dl4(t), dl18(t)}
;

        auto result = f_4_31_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_31_abbreviated(const std::array<T,16>& abb)
{
using TR = typename T::value_type;
T z[46];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[12];
z[11] = abb[13];
z[12] = abb[14];
z[13] = abb[15];
z[14] = abb[9];
z[15] = abb[10];
z[16] = abb[11];
z[17] = bc<TR>[1];
z[18] = bc<TR>[2];
z[19] = bc<TR>[4];
z[20] = bc<TR>[7];
z[21] = bc<TR>[8];
z[22] = bc<TR>[9];
z[23] = z[2] + -z[14];
z[24] = z[4] * z[23];
z[25] = T(2) * z[24];
z[26] = T(2) * z[14];
z[27] = -z[2] + z[26];
z[27] = z[2] * z[27];
z[28] = prod_pow(z[14], 2);
z[29] = z[25] + z[27] + -z[28];
z[30] = T(3) * z[15];
z[31] = z[29] + -z[30];
z[31] = z[13] * z[31];
z[32] = z[2] * z[23];
z[32] = z[5] + z[15] + -z[24] + z[32];
z[33] = z[10] * z[32];
z[34] = z[9] * z[32];
z[35] = prod_pow(z[2], 2);
z[36] = prod_pow(z[4], 2);
z[37] = z[5] + z[35] + -z[36];
z[37] = z[0] * z[37];
z[35] = -z[15] + z[28] + -z[35];
z[35] = z[11] * z[35];
z[38] = T(2) * z[23];
z[39] = -z[4] + z[38];
z[39] = z[4] * z[39];
z[27] = z[27] + z[39];
z[39] = T(-3) * z[5] + z[27];
z[39] = z[12] * z[39];
z[31] = z[31] + -z[33] + T(3) * z[34] + z[35] + -z[37] + z[39];
z[31] = z[1] * z[31];
z[35] = z[4] + z[38];
z[35] = z[4] * z[35];
z[39] = z[2] * z[38];
z[30] = z[5] + z[28] + z[30] + -z[35] + z[39];
z[30] = z[10] * z[30];
z[35] = T(3) * z[2];
z[39] = T(4) * z[14] + -z[35];
z[39] = z[2] * z[39];
z[39] = T(-7) * z[15] + T(4) * z[24] + -z[28] + z[39];
z[39] = z[11] * z[39];
z[40] = T(2) * z[12];
z[41] = T(2) * z[13];
z[42] = z[40] + z[41];
z[43] = z[5] + -z[15];
z[44] = z[42] * z[43];
z[45] = T(2) * z[9];
z[32] = z[32] * z[45];
z[30] = z[30] + z[32] + -z[37] + z[39] + -z[44];
z[32] = z[7] + z[8];
z[30] = -(z[30] * z[32]);
z[39] = z[5] + T(2) * z[15];
z[27] = z[27] + -z[39];
z[27] = z[12] * z[27];
z[27] = z[27] + z[34];
z[34] = z[2] + z[14];
z[34] = z[2] * z[34];
z[24] = z[24] + T(-2) * z[28] + z[34];
z[24] = z[11] * z[24];
z[34] = z[28] + -z[36] + -z[43];
z[34] = z[10] * z[34];
z[36] = -z[29] + z[39];
z[36] = z[13] * z[36];
z[24] = z[24] + -z[27] + z[34] + z[36];
z[34] = T(2) * z[16];
z[24] = z[24] * z[34];
z[26] = z[26] + -z[35];
z[26] = z[2] * z[26];
z[25] = T(-5) * z[15] + z[25] + z[26] + z[28];
z[25] = z[11] * z[25];
z[26] = T(-4) * z[5] + z[15] + z[29];
z[26] = z[13] * z[26];
z[25] = z[25] + z[26] + z[27] + T(3) * z[33] + -z[37];
z[25] = z[6] * z[25];
z[26] = z[13] * z[23];
z[27] = -z[2] + z[4];
z[28] = z[12] * z[27];
z[26] = z[26] + -z[28];
z[27] = z[0] * z[27];
z[23] = z[11] * z[23];
z[28] = -z[23] + z[26] + z[27];
z[29] = -z[1] + -z[6];
z[28] = z[28] * z[29];
z[29] = z[4] + -z[14];
z[29] = z[10] * z[29];
z[23] = -z[23] + -z[27] + z[29];
z[23] = -(z[23] * z[32]);
z[27] = -(z[11] * z[38]);
z[26] = z[26] + z[27] + z[29];
z[26] = z[26] * z[34];
z[27] = -prod_pow(z[17], 2);
z[27] = T(-2) * z[19] + z[27];
z[29] = z[9] + z[10];
z[32] = T(2) * z[11] + -z[29];
z[27] = z[27] * z[32];
z[23] = z[23] + z[26] + z[27] + z[28];
z[23] = int_to_imaginary<T>(1) * z[23];
z[26] = z[29] + -z[41];
z[27] = z[26] + -z[40];
z[27] = z[8] * z[27];
z[28] = z[9] + -z[13];
z[28] = z[16] * z[28];
z[29] = -z[11] + z[45];
z[32] = -z[10] + z[29];
z[33] = z[18] * z[32];
z[27] = z[27] + z[28] + z[33];
z[28] = -z[9] + z[10] / T(3);
z[33] = z[12] / T(3);
z[28] = z[13] / T(3) + z[28] / T(2) + z[33];
z[34] = z[1] + z[17];
z[28] = z[28] * z[34];
z[26] = -z[12] + z[26];
z[34] = int_to_imaginary<T>(1) * z[3];
z[34] = -z[7] + z[34];
z[34] = -z[34] / T(3);
z[26] = z[26] * z[34];
z[34] = -z[9] / T(3) + -z[10];
z[33] = (T(2) * z[13]) / T(3) + z[33] + z[34] / T(2);
z[33] = z[6] * z[33];
z[26] = z[26] + z[27] / T(3) + z[28] + z[33];
z[26] = z[3] * z[26];
z[23] = T(2) * z[23] + z[26];
z[23] = z[3] * z[23];
z[26] = T(4) * z[20];
z[27] = (T(5) * z[22]) / T(3) + z[26];
z[27] = z[27] * z[41];
z[28] = T(-5) * z[9] + -z[10] + T(4) * z[11] + z[42];
z[28] = prod_pow(z[17], 3) * z[28];
z[26] = z[21] + (T(-49) * z[22]) / T(6) + z[26];
z[26] = z[11] * z[26];
z[33] = T(-14) * z[20] + T(-2) * z[21] + (T(43) * z[22]) / T(3);
z[33] = z[9] * z[33];
z[34] = T(2) * z[20] + z[21] + (T(-19) * z[22]) / T(2);
z[34] = z[10] * z[34];
z[35] = T(8) * z[20] + (T(7) * z[22]) / T(3);
z[35] = z[12] * z[35];
z[32] = prod_pow(z[18], 3) * z[32];
z[29] = z[12] + z[13] + -z[29];
z[29] = z[17] * z[19] * z[29];
return z[23] + z[24] + z[25] + z[26] + z[27] + z[28] / T(3) + T(4) * z[29] + z[30] + z[31] + (T(4) * z[32]) / T(3) + z[33] + z[34] + z[35];
}



template IntegrandConstructorType<double> f_4_31_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_31_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_31_construct (const Kin<qd_real>&);
#endif

}