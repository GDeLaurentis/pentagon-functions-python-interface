#include "f_4_242.h"

namespace PentagonFunctions {

template <typename T> T f_4_242_abbreviated (const std::array<T,42>&);

template <typename T> class SpDLog_f_4_242_W_23 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_242_W_23 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(-6) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(3) * kin.v[4])) + rlog(kin.v[3]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(-6) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(3) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(3) + prod_pow(abb[2], 2) * (abb[5] * T(-3) + abb[6] * T(-3) + abb[4] * T(3)) + prod_pow(abb[1], 2) * (abb[3] * T(-3) + abb[4] * T(-3) + abb[5] * T(3) + abb[6] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_242_W_21 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_242_W_21 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(6) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) + T(3) * kin.v[1] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[3] * (T(-6) + T(3) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(6) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) + T(3) * kin.v[1] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[3] * (T(-6) + T(3) * kin.v[3] + T(6) * kin.v[4])) + rlog(kin.v[2]) * (kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[3] * (T(6) + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) + T(-3) * kin.v[1] + T(6) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[3] * (T(6) + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) + T(-3) * kin.v[1] + T(6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[7] * (prod_pow(abb[9], 2) * abb[10] * T(3) + prod_pow(abb[9], 2) * (abb[4] * T(-3) + abb[11] * T(-3) + abb[6] * T(3)) + prod_pow(abb[8], 2) * (abb[6] * T(-3) + abb[10] * T(-3) + abb[4] * T(3) + abb[11] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_242_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_242_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-8) * kin.v[0] + T(-2) * kin.v[1] + T(8) * kin.v[2] + T(12) * kin.v[3]) + kin.v[2] * (T(10) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[3] * (T(-10) * kin.v[3] + T(-8) * kin.v[4]) + T(2) * prod_pow(kin.v[4], 2) + kin.v[0] * (T(-8) * kin.v[2] + T(8) * kin.v[3] + T(8) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[1] * (T(4) + T(-4) * kin.v[0] + T(-2) * kin.v[1] + T(4) * kin.v[3]) + kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = kin.v[1] * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[1] + T(8) * kin.v[2] + T(12) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] + T(-6) * kin.v[3])) + kin.v[2] * (T(10) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[3] * (T(-10) * kin.v[3] + T(-8) * kin.v[4]) + T(2) * prod_pow(kin.v[4], 2) + kin.v[0] * ((T(-8) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + T(-8) * kin.v[2] + T(8) * kin.v[3] + T(8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (-prod_pow(kin.v[4], 2) + kin.v[3] * (T(5) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * (T(-5) * kin.v[2] + T(6) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (-prod_pow(kin.v[4], 2) / T(2) + kin.v[1] * (kin.v[1] / T(2) + T(2) * kin.v[0] + T(-2) * kin.v[2] + T(-3) * kin.v[3]) + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[3] * ((T(5) * kin.v[3]) / T(2) + T(2) * kin.v[4]) + kin.v[2] * ((T(-5) * kin.v[2]) / T(2) + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + T(-1) + kin.v[0]) * kin.v[1] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (-prod_pow(kin.v[4], 2) / T(2) + kin.v[1] * (kin.v[1] / T(2) + T(2) * kin.v[0] + T(-2) * kin.v[2] + T(-3) * kin.v[3]) + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[3] * ((T(5) * kin.v[3]) / T(2) + T(2) * kin.v[4]) + kin.v[2] * ((T(-5) * kin.v[2]) / T(2) + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + T(-1) + kin.v[0]) * kin.v[1] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (-prod_pow(kin.v[4], 2) / T(2) + kin.v[1] * (kin.v[1] / T(2) + T(2) * kin.v[0] + T(-2) * kin.v[2] + T(-3) * kin.v[3]) + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[3] * ((T(5) * kin.v[3]) / T(2) + T(2) * kin.v[4]) + kin.v[2] * ((T(-5) * kin.v[2]) / T(2) + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + T(-1) + kin.v[0]) * kin.v[1] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (-prod_pow(kin.v[4], 2) / T(2) + kin.v[1] * (kin.v[1] / T(2) + T(2) * kin.v[0] + T(-2) * kin.v[2] + T(-3) * kin.v[3]) + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[3] * ((T(5) * kin.v[3]) / T(2) + T(2) * kin.v[4]) + kin.v[2] * ((T(-5) * kin.v[2]) / T(2) + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + T(-1) + kin.v[0]) * kin.v[1] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (-prod_pow(kin.v[4], 2) + kin.v[1] * (T(4) * kin.v[0] + kin.v[1] + T(-4) * kin.v[2] + T(-6) * kin.v[3]) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[3] * (T(5) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * (T(-5) * kin.v[2] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]))) + rlog(kin.v[2]) * ((T(3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-6) * kin.v[0] + T(6) * kin.v[2] + T(9) * kin.v[3]) + kin.v[2] * ((T(15) * kin.v[2]) / T(2) + T(-9) * kin.v[4]) + kin.v[3] * ((T(-15) * kin.v[3]) / T(2) + T(-6) * kin.v[4]) + kin.v[0] * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]))) + rlog(kin.v[3]) * ((T(3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-6) * kin.v[0] + T(6) * kin.v[2] + T(9) * kin.v[3]) + kin.v[2] * ((T(15) * kin.v[2]) / T(2) + T(-9) * kin.v[4]) + kin.v[3] * ((T(-15) * kin.v[3]) / T(2) + T(-6) * kin.v[4]) + kin.v[0] * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4])));
c[2] = (T(-8) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + T(-8) * kin.v[2] + T(8) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + T(8) * kin.v[4];
c[3] = rlog(kin.v[2]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4])) + rlog(kin.v[3]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((bc<T>[0] * int_to_imaginary<T>(-2) + T(4)) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[26] * (abb[9] * abb[13] * (-abb[4] + -abb[5] + -abb[6] + -abb[11] + abb[29] * T(-4) + abb[28] * T(-2) + abb[3] * T(3)) + abb[9] * (abb[4] * bc<T>[0] * int_to_imaginary<T>(1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(1) + abb[28] * bc<T>[0] * int_to_imaginary<T>(2) + abb[3] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[29] * bc<T>[0] * int_to_imaginary<T>(4) + abb[9] * (-abb[4] + -abb[5] + -abb[6] + -abb[11] + abb[29] * T(-4) + abb[28] * T(-2) + abb[3] * T(3))) + abb[10] * (abb[27] * T(-6) + abb[2] * abb[9] * T(-3) + abb[8] * abb[9] * T(-3) + abb[9] * abb[13] * T(3) + abb[9] * (bc<T>[0] * int_to_imaginary<T>(-3) + abb[9] * T(3))) + abb[2] * abb[9] * (abb[4] + abb[5] + abb[6] + abb[11] + abb[3] * T(-3) + abb[28] * T(2) + abb[29] * T(4)) + abb[8] * abb[9] * (abb[4] + abb[5] + abb[6] + abb[11] + abb[3] * T(-3) + abb[28] * T(2) + abb[29] * T(4)) + abb[27] * (abb[3] * T(-6) + abb[4] * T(2) + abb[5] * T(2) + abb[6] * T(2) + abb[11] * T(2) + abb[28] * T(4) + abb[29] * T(8)) + abb[1] * (abb[4] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[28] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[3] * bc<T>[0] * int_to_imaginary<T>(3) + abb[29] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[2] * (-abb[4] + -abb[5] + -abb[6] + -abb[11] + abb[29] * T(-4) + abb[28] * T(-2) + abb[3] * T(3)) + abb[8] * (-abb[4] + -abb[5] + -abb[6] + -abb[11] + abb[29] * T(-4) + abb[28] * T(-2) + abb[3] * T(3)) + abb[9] * (-abb[4] + -abb[5] + -abb[6] + -abb[11] + abb[29] * T(-4) + abb[28] * T(-2) + abb[3] * T(3)) + abb[10] * (abb[13] * T(-3) + bc<T>[0] * int_to_imaginary<T>(3) + abb[2] * T(3) + abb[8] * T(3) + abb[9] * T(3)) + abb[13] * (abb[4] + abb[5] + abb[6] + abb[11] + abb[3] * T(-3) + abb[28] * T(2) + abb[29] * T(4)) + abb[1] * (abb[3] * T(-6) + abb[10] * T(-6) + abb[4] * T(2) + abb[5] * T(2) + abb[6] * T(2) + abb[11] * T(2) + abb[28] * T(4) + abb[29] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_242_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl21 = DLog_W_21<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl27 = DLog_W_27<T>(kin),dl22 = DLog_W_22<T>(kin),dl18 = DLog_W_18<T>(kin),dl5 = DLog_W_5<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl20 = DLog_W_20<T>(kin),dl4 = DLog_W_4<T>(kin),dl19 = DLog_W_19<T>(kin),dl3 = DLog_W_3<T>(kin),dl17 = DLog_W_17<T>(kin),dl16 = DLog_W_16<T>(kin),spdl23 = SpDLog_f_4_242_W_23<T>(kin),spdl21 = SpDLog_f_4_242_W_21<T>(kin),spdl22 = SpDLog_f_4_242_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,42> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[19] / kin_path.W[19]), dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[18] / kin_path.W[18]), dl2(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_12(kin_path), f_2_1_15(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl1(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl27(t) / kin_path.SqrtDelta, f_1_3_1(kin) - f_1_3_1(kin_path), f_2_1_8(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl22(t), f_2_1_14(kin_path), rlog(kin.W[17] / kin_path.W[17]), -rlog(t), dl18(t), dl5(t), dl29(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl20(t), dl4(t), dl19(t), dl3(t), dl17(t), dl16(t)}
;

        auto result = f_4_242_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_242_abbreviated(const std::array<T,42>& abb)
{
using TR = typename T::value_type;
T z[126];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[30];
z[3] = abb[31];
z[4] = abb[36];
z[5] = abb[37];
z[6] = abb[38];
z[7] = abb[39];
z[8] = abb[40];
z[9] = abb[41];
z[10] = abb[4];
z[11] = abb[5];
z[12] = abb[6];
z[13] = abb[10];
z[14] = abb[11];
z[15] = abb[20];
z[16] = abb[21];
z[17] = abb[23];
z[18] = abb[24];
z[19] = abb[25];
z[20] = abb[33];
z[21] = abb[35];
z[22] = abb[28];
z[23] = abb[29];
z[24] = abb[2];
z[25] = abb[8];
z[26] = abb[9];
z[27] = abb[13];
z[28] = bc<TR>[0];
z[29] = abb[32];
z[30] = abb[34];
z[31] = abb[17];
z[32] = abb[12];
z[33] = abb[14];
z[34] = abb[15];
z[35] = abb[16];
z[36] = abb[18];
z[37] = abb[19];
z[38] = abb[22];
z[39] = abb[27];
z[40] = bc<TR>[3];
z[41] = bc<TR>[1];
z[42] = bc<TR>[2];
z[43] = bc<TR>[4];
z[44] = bc<TR>[9];
z[45] = -z[10] + z[12];
z[46] = z[13] + -z[14];
z[47] = z[1] + -z[11];
z[48] = -z[45] + z[46] + z[47];
z[49] = z[9] * z[48];
z[50] = z[3] * z[48];
z[51] = -z[15] + z[21];
z[52] = -z[20] + z[30];
z[53] = z[51] + z[52];
z[54] = z[18] + z[19];
z[55] = z[53] * z[54];
z[55] = -z[50] + z[55];
z[56] = z[46] + -z[47];
z[57] = z[45] + z[56];
z[58] = z[2] * z[57];
z[59] = z[45] + z[47];
z[60] = -z[46] + z[59];
z[61] = z[31] * z[60];
z[58] = z[58] + z[61];
z[62] = z[6] * z[60];
z[63] = z[8] * z[57];
z[49] = z[49] + z[55] + -z[58] + z[62] + z[63];
z[49] = (T(3) * z[49]) / T(2);
z[62] = z[37] * z[49];
z[63] = (T(3) * z[13]) / T(2);
z[64] = z[22] + T(2) * z[23];
z[65] = z[63] + -z[64];
z[66] = z[11] / T(2);
z[67] = z[12] / T(2) + z[66];
z[68] = (T(3) * z[1]) / T(2);
z[69] = z[14] / T(2);
z[70] = z[10] / T(2) + z[69];
z[71] = z[65] + -z[67] + z[68] + -z[70];
z[72] = z[9] * z[71];
z[73] = z[29] + z[51];
z[74] = z[54] * z[73];
z[74] = -z[61] + z[74];
z[75] = z[6] + z[8];
z[76] = T(2) * z[45];
z[76] = z[75] * z[76];
z[77] = T(3) * z[13];
z[78] = T(3) * z[14] + -z[77];
z[47] = T(3) * z[47];
z[79] = z[47] + z[78];
z[80] = -z[45] + z[79];
z[81] = z[2] * z[80];
z[72] = z[72] + (T(3) * z[74]) / T(2) + z[76] + z[81] / T(2);
z[74] = z[25] * z[72];
z[76] = T(2) * z[10] + -z[11] + -z[12];
z[82] = -z[14] + z[64];
z[83] = z[76] + z[82];
z[84] = z[0] * z[83];
z[82] = -z[10] + T(2) * z[12] + z[82];
z[85] = -z[11] + z[82];
z[86] = z[26] * z[85];
z[87] = z[84] + -z[86];
z[87] = z[2] * z[87];
z[87] = z[74] + z[87];
z[88] = z[0] * z[71];
z[89] = z[26] * z[71];
z[90] = z[88] + -z[89];
z[91] = T(3) * z[43];
z[92] = -z[90] + z[91];
z[92] = z[6] * z[92];
z[93] = z[41] / T(2) + -z[42];
z[94] = T(3) * z[41];
z[93] = z[93] * z[94];
z[91] = z[91] + z[93];
z[94] = -z[90] + -z[91];
z[94] = z[8] * z[94];
z[95] = z[0] * z[50];
z[46] = z[46] + z[59];
z[59] = z[32] * z[46];
z[96] = z[26] * z[59];
z[97] = z[96] / T(2);
z[98] = z[26] * z[52];
z[99] = z[21] * z[26];
z[98] = z[98] + z[99];
z[100] = -z[29] + z[52];
z[101] = z[0] * z[100];
z[101] = -z[98] + z[101];
z[102] = z[54] / T(2);
z[103] = -(z[101] * z[102]);
z[104] = -z[5] + z[7];
z[105] = -(z[43] * z[104]);
z[103] = z[95] / T(2) + -z[97] + z[103] + z[105];
z[105] = z[15] + -z[29];
z[52] = z[52] + z[105];
z[106] = z[52] * z[54];
z[106] = -z[59] + z[106];
z[57] = z[6] * z[57];
z[57] = z[57] + -z[58] + -z[106];
z[58] = z[8] * z[60];
z[58] = z[57] + z[58];
z[58] = z[35] * z[58];
z[84] = T(2) * z[84] + -z[89];
z[107] = -z[84] + -z[91];
z[107] = z[9] * z[107];
z[83] = z[9] * z[83];
z[108] = z[2] + -z[75];
z[108] = z[45] * z[108];
z[108] = -z[83] + z[108];
z[108] = z[27] * z[108];
z[104] = -z[6] + z[104];
z[93] = -(z[93] * z[104]);
z[58] = (T(3) * z[58]) / T(2) + z[62] + -z[87] + z[92] + z[93] + z[94] + T(3) * z[103] + z[107] + z[108];
z[58] = int_to_imaginary<T>(1) * z[58];
z[62] = z[11] + z[12];
z[92] = z[10] / T(4);
z[93] = z[14] / T(4);
z[94] = z[22] / T(2) + z[23];
z[103] = (T(-5) * z[1]) / T(4) + -z[13] / T(4) + (T(3) * z[62]) / T(4) + -z[92] + -z[93] + z[94];
z[103] = z[5] * z[103];
z[107] = z[10] / T(3);
z[108] = z[1] / T(2);
z[109] = z[12] / T(6) + -z[13] + z[69] + z[94] + z[107] + -z[108];
z[109] = z[6] * z[109];
z[110] = z[12] / T(3);
z[111] = -z[10] / T(6) + -z[11] + z[41] + z[68] + z[69] + -z[94] + -z[110];
z[111] = z[8] * z[111];
z[112] = z[13] / T(2);
z[113] = T(5) * z[94];
z[114] = z[11] + -z[113];
z[114] = (T(-7) * z[10]) / T(6) + (T(4) * z[14]) / T(3) + z[41] + z[108] + z[110] + -z[112] + z[114] / T(3);
z[114] = z[9] * z[114];
z[115] = z[1] / T(4);
z[116] = -z[94] + z[115];
z[117] = z[10] + z[14];
z[118] = z[11] / T(4);
z[119] = z[12] / T(4) + z[118];
z[120] = (T(-5) * z[13]) / T(4) + -z[116] + (T(3) * z[117]) / T(4) + -z[119];
z[120] = z[7] * z[120];
z[121] = z[59] / T(4) + -z[120];
z[56] = -z[56] + -z[107] + z[110];
z[56] = z[2] * z[56];
z[107] = z[20] + (T(3) * z[29]) / T(2) + z[51];
z[110] = z[102] * z[107];
z[122] = z[8] + z[104];
z[123] = z[21] + z[100];
z[124] = z[9] / T(2) + (T(3) * z[122]) / T(4) + z[123] / T(9);
z[125] = int_to_imaginary<T>(1) * z[28];
z[124] = z[124] * z[125];
z[104] = z[41] * z[104];
z[56] = z[56] + -z[61] + z[103] + z[104] + z[109] + z[110] + z[111] + z[114] + z[121] + z[124];
z[56] = z[28] * z[56];
z[103] = int_to_imaginary<T>(1) * z[24];
z[72] = -(z[72] * z[103]);
z[104] = -(z[40] * z[123]);
z[56] = z[56] + z[58] + z[72] + T(3) * z[104];
z[56] = z[28] * z[56];
z[58] = T(2) * z[86] + -z[88];
z[72] = z[25] * z[71];
z[104] = z[58] + z[72];
z[46] = (T(3) * z[46]) / T(2);
z[109] = -(z[35] * z[46]);
z[110] = z[27] * z[85];
z[91] = z[91] + z[104] + z[109] + z[110];
z[91] = int_to_imaginary<T>(1) * z[91];
z[109] = (T(3) * z[13]) / T(4);
z[110] = (T(-13) * z[11]) / T(4) + z[113];
z[110] = (T(11) * z[12]) / T(12) + -z[41] + -z[109] + z[110] / T(3) + z[115] + -z[117] / T(12) + -z[125] / T(2);
z[110] = z[28] * z[110];
z[111] = z[71] * z[103];
z[91] = z[91] + z[110] + z[111];
z[91] = z[28] * z[91];
z[93] = z[93] + z[119];
z[109] = (T(3) * z[1]) / T(4) + z[109];
z[110] = -z[92] + -z[93] + -z[94] + z[109];
z[111] = z[25] * z[110];
z[58] = z[58] + z[111];
z[58] = z[25] * z[58];
z[111] = z[27] * z[110];
z[104] = -z[104] + z[111];
z[104] = z[27] * z[104];
z[71] = -(z[27] * z[71]);
z[111] = T(3) * z[1];
z[82] = T(-2) * z[11] + -z[82] + z[111];
z[113] = z[26] * z[82];
z[114] = z[0] * z[82];
z[115] = z[113] + -z[114];
z[115] = T(2) * z[115];
z[119] = -(z[24] * z[82]);
z[71] = z[71] + z[72] + -z[115] + z[119];
z[71] = z[24] * z[71];
z[63] = (T(-9) * z[1]) / T(2) + (T(7) * z[62]) / T(2) + z[63] + z[64] + (T(-5) * z[117]) / T(2);
z[63] = z[39] * z[63];
z[72] = T(2) * z[113] + -z[114];
z[72] = z[0] * z[72];
z[112] = -z[64] + z[112];
z[70] = (T(3) * z[12]) / T(2) + -z[70] + -z[112];
z[113] = (T(-5) * z[1]) / T(2) + (T(3) * z[11]) / T(2) + z[70];
z[114] = T(3) * z[38];
z[119] = z[113] * z[114];
z[63] = z[63] + z[72] + z[119];
z[66] = z[66] + -z[70] + z[108];
z[70] = T(3) * z[34];
z[66] = z[66] * z[70];
z[72] = prod_pow(z[26], 2);
z[85] = z[72] * z[85];
z[119] = z[33] * z[46];
z[58] = (T(-11) * z[44]) / T(3) + z[58] + z[63] + z[66] + z[71] + T(-2) * z[85] + z[91] + z[104] + z[119];
z[58] = z[4] * z[58];
z[66] = -(z[8] * z[90]);
z[71] = -(z[9] * z[110]);
z[91] = -z[54] / T(4);
z[104] = -z[20] + z[73];
z[91] = z[91] * z[104];
z[91] = z[91] + z[121];
z[47] = z[45] + -z[47] + z[78];
z[47] = z[2] * z[47];
z[78] = -z[116] + -z[118];
z[78] = (T(5) * z[12]) / T(4) + (T(-15) * z[13]) / T(4) + (T(9) * z[14]) / T(4) + T(3) * z[78] + z[92];
z[78] = z[6] * z[78];
z[92] = -(z[8] * z[45]);
z[47] = z[47] / T(4) + z[71] + z[78] + T(3) * z[91] + z[92];
z[47] = z[25] * z[47];
z[71] = T(2) * z[14] + z[64] + z[76] + -z[77];
z[76] = z[0] * z[71];
z[76] = T(2) * z[76] + -z[89];
z[78] = z[6] + z[9];
z[76] = -(z[76] * z[78]);
z[91] = (T(3) * z[10]) / T(2) + -z[108];
z[92] = (T(5) * z[13]) / T(2) + (T(-3) * z[14]) / T(2) + -z[64] + z[67] + -z[91];
z[108] = z[7] * z[92];
z[116] = -(z[0] * z[108]);
z[105] = z[30] + z[105];
z[118] = z[0] * z[105];
z[98] = -z[98] + z[118];
z[118] = -(z[98] * z[102]);
z[97] = -z[97] + z[116] + z[118];
z[86] = z[86] + z[88];
z[86] = z[2] * z[86];
z[47] = z[47] + z[66] + z[76] + z[86] + T(3) * z[97];
z[47] = z[25] * z[47];
z[66] = z[75] * z[90];
z[75] = z[61] + z[106];
z[76] = z[45] + z[79];
z[86] = z[8] * z[76];
z[80] = z[6] * z[80];
z[75] = T(3) * z[75] + z[80] + -z[81] + -z[86];
z[67] = z[67] + z[69];
z[81] = z[10] + -z[67] + z[94];
z[81] = z[9] * z[81];
z[75] = z[75] / T(4) + z[81];
z[75] = z[27] * z[75];
z[81] = z[54] * z[101];
z[81] = z[81] + -z[95] + z[96];
z[84] = z[9] * z[84];
z[66] = z[66] + z[75] + (T(3) * z[81]) / T(2) + z[84] + z[87];
z[66] = z[27] * z[66];
z[75] = z[2] + -z[78];
z[75] = z[75] * z[90];
z[55] = z[55] + -z[61];
z[61] = z[2] * z[76];
z[76] = T(11) * z[45] + -z[79];
z[76] = z[8] * z[76];
z[55] = T(3) * z[55] + z[61] + z[76] + z[80];
z[61] = (T(-5) * z[10]) / T(4) + -z[64] + z[93] + z[109];
z[61] = z[9] * z[61];
z[55] = z[55] / T(4) + z[61];
z[55] = z[24] * z[55];
z[54] = -(z[54] * z[100]);
z[54] = z[50] + z[54];
z[54] = T(3) * z[54] + -z[80] + z[86];
z[45] = z[2] * z[45];
z[45] = z[45] + z[54] / T(2) + -z[83];
z[45] = z[27] * z[45];
z[54] = z[5] * z[113];
z[61] = z[0] + -z[26];
z[61] = z[54] * z[61];
z[76] = z[15] + z[20];
z[78] = z[26] * z[76];
z[78] = z[78] + -z[99];
z[51] = -z[20] + z[51];
z[51] = z[0] * z[51];
z[51] = z[51] + z[78];
z[79] = -(z[51] * z[102]);
z[61] = z[61] + z[79];
z[79] = -(z[8] * z[115]);
z[45] = z[45] + z[55] + T(3) * z[61] + -z[74] + z[75] + z[79];
z[45] = z[24] * z[45];
z[55] = z[24] * z[53];
z[61] = z[27] * z[100];
z[74] = z[25] * z[73];
z[51] = z[51] + -z[55] / T(2) + z[61] + z[74];
z[51] = z[24] * z[51];
z[55] = -z[21] + z[76] / T(2);
z[55] = z[0] * z[55];
z[55] = z[55] + -z[78];
z[55] = z[0] * z[55];
z[61] = z[38] + z[39];
z[61] = z[21] * z[61];
z[75] = z[39] * z[100];
z[76] = z[38] * z[105];
z[55] = -z[55] + z[61] + z[75] + -z[76];
z[61] = z[27] * z[52];
z[74] = z[74] + z[101];
z[61] = z[61] / T(2) + z[74];
z[61] = z[27] * z[61];
z[75] = z[25] * z[104];
z[75] = z[75] / T(2) + z[98];
z[75] = z[25] * z[75];
z[76] = z[33] * z[52];
z[78] = z[36] * z[53];
z[79] = z[34] * z[104];
z[51] = z[51] + -z[55] + -z[61] + z[75] + -z[76] + -z[78] + z[79];
z[52] = z[35] * z[52];
z[53] = z[37] * z[53];
z[52] = z[52] + -z[53] + z[74];
z[52] = int_to_imaginary<T>(1) * z[52];
z[53] = z[73] * z[103];
z[52] = z[52] + z[53];
z[53] = z[28] * z[107];
z[52] = T(3) * z[52] + -z[53];
z[52] = z[28] * z[52];
z[51] = T(3) * z[51] + z[52];
z[52] = z[16] + z[17];
z[52] = z[52] / T(2);
z[51] = z[51] * z[52];
z[52] = z[55] * z[102];
z[53] = z[50] / T(4) + -z[54] + -z[120];
z[53] = z[0] * z[53];
z[55] = z[26] * z[54];
z[53] = z[53] + z[55];
z[53] = z[0] * z[53];
z[55] = -(z[102] * z[104]);
z[55] = z[55] + z[59] / T(2) + z[108];
z[55] = z[34] * z[55];
z[59] = -z[54] + z[108];
z[59] = z[39] * z[59];
z[50] = z[50] / T(2) + -z[54];
z[50] = z[38] * z[50];
z[50] = z[50] + z[52] + z[53] + z[55] + z[59];
z[52] = T(2) * z[64];
z[53] = z[52] + z[62] + -z[77] + -z[111] + z[117];
z[53] = z[39] * z[53];
z[54] = z[1] + z[13];
z[52] = (T(7) * z[10]) / T(4) + z[52] + (T(-9) * z[54]) / T(4) + z[93];
z[52] = z[0] * z[52];
z[52] = z[52] + z[89];
z[52] = z[0] * z[52];
z[48] = z[38] * z[48];
z[46] = -(z[34] * z[46]);
z[46] = z[46] + (T(-3) * z[48]) / T(2) + z[52] + z[53] + -z[85];
z[46] = z[2] * z[46];
z[48] = z[36] * z[49];
z[49] = z[0] * z[110];
z[49] = z[49] + -z[89];
z[49] = z[0] * z[49];
z[52] = z[71] * z[72];
z[53] = z[70] * z[92];
z[54] = (T(-7) * z[10]) / T(2) + (T(5) * z[62]) / T(2) + -z[68];
z[55] = (T(9) * z[13]) / T(2) + (T(-7) * z[14]) / T(2) + z[54] + -z[64];
z[55] = z[39] * z[55];
z[52] = -z[52] + z[53] + z[55];
z[49] = z[49] + -z[52];
z[49] = z[6] * z[49];
z[53] = -z[54] + -z[65] + z[69];
z[53] = z[0] * z[53];
z[53] = z[53] + -z[89];
z[53] = z[0] * z[53];
z[54] = z[67] + -z[91] + z[112];
z[54] = z[54] * z[114];
z[52] = -z[52] + z[53] + z[54];
z[52] = z[9] * z[52];
z[53] = (T(3) * z[33]) / T(2);
z[54] = -(z[53] * z[57]);
z[55] = -(z[72] * z[82]);
z[53] = -(z[53] * z[60]);
z[53] = z[53] + z[55] + z[63];
z[53] = z[8] * z[53];
z[55] = (T(11) * z[9]) / T(3) + (T(5) * z[122]) / T(2);
z[55] = z[44] * z[55];
return z[45] + z[46] + z[47] + z[48] + z[49] + T(3) * z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + z[58] + z[66];
}



template IntegrandConstructorType<double> f_4_242_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_242_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_242_construct (const Kin<qd_real>&);
#endif

}