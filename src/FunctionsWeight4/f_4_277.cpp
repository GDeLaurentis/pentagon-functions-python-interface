#include "f_4_277.h"

namespace PentagonFunctions {

template <typename T> T f_4_277_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_277_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_277_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + kin.v[3] * (T(2) + kin.v[3]) + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]);
c[1] = kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + kin.v[3] * (T(2) + kin.v[3]) + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]);
c[2] = (-kin.v[3] + T(-2)) * kin.v[3] + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]);
c[3] = (-kin.v[3] + T(-2)) * kin.v[3] + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[6] * c[1] + abb[4] * c[2] + abb[5] * c[3]);
        }

        return abb[0] * (-(prod_pow(abb[2], 2) * abb[3]) + prod_pow(abb[1], 2) * (abb[3] + abb[6] + -abb[4] + -abb[5]) + prod_pow(abb[2], 2) * (abb[4] + abb[5] + -abb[6]));
    }
};
template <typename T> class SpDLog_f_4_277_W_23 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_277_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (-kin.v[3] / T(2) + -kin.v[4] / T(2) + kin.v[2] / T(4) + T(-1)) * kin.v[2] + (-kin.v[4] / T(2) + (T(-3) * kin.v[0]) / T(4) + kin.v[2] / T(2) + (T(3) * kin.v[3]) / T(2) + -kin.v[1] + T(1)) * kin.v[0] + ((T(-3) * kin.v[3]) / T(4) + kin.v[4] / T(2) + T(-1)) * kin.v[3] + (kin.v[4] / T(4) + T(1)) * kin.v[4];
c[1] = -kin.v[0] + kin.v[2] + kin.v[3] + -kin.v[4];
c[2] = kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (-kin.v[3] / T(2) + -kin.v[4] / T(2) + kin.v[2] / T(4) + T(-1)) * kin.v[2] + (-kin.v[4] / T(2) + (T(-3) * kin.v[0]) / T(4) + kin.v[2] / T(2) + (T(3) * kin.v[3]) / T(2) + -kin.v[1] + T(1)) * kin.v[0] + ((T(-3) * kin.v[3]) / T(4) + kin.v[4] / T(2) + T(-1)) * kin.v[3] + (kin.v[4] / T(4) + T(1)) * kin.v[4];
c[3] = -kin.v[0] + kin.v[2] + kin.v[3] + -kin.v[4];
c[4] = (-kin.v[4] / T(2) + (T(3) * kin.v[3]) / T(4) + T(1)) * kin.v[3] + kin.v[0] * (-kin.v[2] / T(2) + (T(3) * kin.v[0]) / T(4) + (T(-3) * kin.v[3]) / T(2) + kin.v[4] / T(2) + T(-1) + kin.v[1]) + (-kin.v[2] / T(4) + kin.v[3] / T(2) + kin.v[4] / T(2) + T(1)) * kin.v[2] + (-kin.v[4] / T(4) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]);
c[5] = kin.v[0] + -kin.v[2] + -kin.v[3] + kin.v[4];
c[6] = (-kin.v[4] / T(2) + (T(3) * kin.v[3]) / T(4) + T(1)) * kin.v[3] + kin.v[0] * (-kin.v[2] / T(2) + (T(3) * kin.v[0]) / T(4) + (T(-3) * kin.v[3]) / T(2) + kin.v[4] / T(2) + T(-1) + kin.v[1]) + (-kin.v[2] / T(4) + kin.v[3] / T(2) + kin.v[4] / T(2) + T(1)) * kin.v[2] + (-kin.v[4] / T(4) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]);
c[7] = kin.v[0] + -kin.v[2] + -kin.v[3] + kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[3] * (t * c[0] + c[1]) + abb[10] * (t * c[2] + c[3]) + abb[5] * (t * c[4] + c[5]) + abb[6] * (t * c[6] + c[7]);
        }

        return abb[7] * (prod_pow(abb[8], 2) * (abb[10] / T(2) + -abb[5] / T(2) + -abb[6] / T(2)) + abb[1] * (abb[3] * abb[8] + abb[1] * ((abb[3] * T(-3)) / T(2) + (abb[10] * T(-3)) / T(2) + (abb[5] * T(3)) / T(2) + (abb[6] * T(3)) / T(2)) + abb[8] * (abb[10] + -abb[5] + -abb[6])) + abb[3] * (prod_pow(abb[8], 2) / T(2) + -abb[9]) + abb[9] * (abb[5] + abb[6] + -abb[10]));
    }
};
template <typename T> class SpDLog_f_4_277_W_21 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_277_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(3) * kin.v[1]) / T(2) + -kin.v[3] + T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[0] * (T(4) + (T(-7) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[0] + T(-2) * kin.v[1] + T(7) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(1) + kin.v[2]) + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * ((T(-7) * kin.v[2]) / T(2) + T(-4) + T(-4) * kin.v[3] + T(4) * kin.v[4]) + kin.v[3] * (-kin.v[3] / T(2) + T(-4) + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + T(1)) * kin.v[1] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-1) + kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[1]) + T(-2) * kin.v[3];
c[2] = kin.v[1] * ((T(3) * kin.v[1]) / T(2) + -kin.v[3] + T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[0] * (T(4) + (T(-7) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[0] + T(-2) * kin.v[1] + T(7) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(1) + kin.v[2]) + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * ((T(-7) * kin.v[2]) / T(2) + T(-4) + T(-4) * kin.v[3] + T(4) * kin.v[4]) + kin.v[3] * (-kin.v[3] / T(2) + T(-4) + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + T(1)) * kin.v[1] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-1) + kin.v[4]));
c[3] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[1]) + T(-2) * kin.v[3];
c[4] = kin.v[3] * (kin.v[3] / T(2) + T(4) + T(-4) * kin.v[4]) + kin.v[2] * ((T(7) * kin.v[2]) / T(2) + T(4) + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-4) + T(-2) * kin.v[2] + kin.v[3] + T(4) * kin.v[4]) + kin.v[0] * (T(-4) + (T(7) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(2) * kin.v[1] + T(-7) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]));
c[5] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3]);
c[6] = kin.v[3] * (kin.v[3] / T(2) + T(4) + T(-4) * kin.v[4]) + kin.v[2] * ((T(7) * kin.v[2]) / T(2) + T(4) + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-4) + T(-2) * kin.v[2] + kin.v[3] + T(4) * kin.v[4]) + kin.v[0] * (T(-4) + (T(7) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(2) * kin.v[1] + T(-7) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]));
c[7] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[10] * (t * c[0] + c[1]) + abb[4] * (t * c[2] + c[3]) + abb[5] * (t * c[4] + c[5]) + abb[6] * (t * c[6] + c[7]);
        }

        return abb[16] * (abb[2] * abb[14] * (abb[5] + abb[6] + -abb[4]) + abb[2] * abb[8] * (abb[4] + -abb[5] + -abb[6]) + abb[1] * (abb[2] * (abb[5] + abb[6] + -abb[4]) + abb[15] * (abb[4] + abb[10] + -abb[5] + -abb[6]) + -(abb[2] * abb[10])) + abb[2] * (abb[2] * (abb[5] + abb[6] + -abb[4]) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[4] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[15] * (abb[8] * (abb[5] + abb[6] + -abb[4]) + abb[2] * (abb[4] + -abb[5] + -abb[6]) + abb[14] * (abb[4] + -abb[5] + -abb[6]) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[10] * (abb[2] + abb[14] + -abb[8] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[10] * (abb[2] * abb[8] + -(abb[2] * abb[14]) + abb[17] * T(-2) + abb[2] * (-abb[2] + bc<T>[0] * int_to_imaginary<T>(1))) + abb[17] * (abb[4] * T(-2) + abb[5] * T(2) + abb[6] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_277_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl23 = DLog_W_23<T>(kin),dl4 = DLog_W_4<T>(kin),dl25 = DLog_W_25<T>(kin),dl21 = DLog_W_21<T>(kin),dl24 = DLog_W_24<T>(kin),dl1 = DLog_W_1<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl19 = DLog_W_19<T>(kin),dl18 = DLog_W_18<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),dl3 = DLog_W_3<T>(kin),dl2 = DLog_W_2<T>(kin),dl31 = DLog_W_31<T>(kin),dl28 = DLog_W_28<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl27 = DLog_W_27<T>(kin),spdl22 = SpDLog_f_4_277_W_22<T>(kin),spdl23 = SpDLog_f_4_277_W_23<T>(kin),spdl21 = SpDLog_f_4_277_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,46> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl23(t), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_8(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), dl4(t), f_2_1_14(kin_path), dl25(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), dl21(t), f_2_1_15(kin_path), dl24(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl1(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl5(t), dl20(t), dl19(t), dl18(t), dl16(t), dl17(t), dl3(t), dl2(t), dl31(t), dl28(t) / kin_path.SqrtDelta, rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl26(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta}
;

        auto result = f_4_277_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_277_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[125];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[24];
z[3] = abb[28];
z[4] = abb[30];
z[5] = abb[4];
z[6] = abb[11];
z[7] = abb[25];
z[8] = abb[26];
z[9] = abb[27];
z[10] = abb[29];
z[11] = abb[5];
z[12] = abb[6];
z[13] = abb[10];
z[14] = abb[33];
z[15] = abb[34];
z[16] = abb[35];
z[17] = abb[36];
z[18] = abb[37];
z[19] = abb[38];
z[20] = abb[39];
z[21] = abb[40];
z[22] = abb[41];
z[23] = abb[44];
z[24] = abb[45];
z[25] = abb[2];
z[26] = abb[8];
z[27] = abb[14];
z[28] = abb[15];
z[29] = bc<TR>[0];
z[30] = abb[42];
z[31] = abb[43];
z[32] = abb[31];
z[33] = abb[18];
z[34] = abb[9];
z[35] = abb[12];
z[36] = abb[13];
z[37] = abb[17];
z[38] = abb[19];
z[39] = abb[20];
z[40] = abb[22];
z[41] = abb[23];
z[42] = abb[21];
z[43] = bc<TR>[3];
z[44] = abb[32];
z[45] = bc<TR>[1];
z[46] = bc<TR>[2];
z[47] = bc<TR>[4];
z[48] = bc<TR>[9];
z[49] = z[11] / T(3);
z[50] = z[13] / T(3);
z[51] = z[5] + -z[12];
z[52] = -z[49] + z[50] + -z[51];
z[53] = z[1] / T(3);
z[52] = z[52] / T(4) + z[53];
z[52] = z[32] * z[52];
z[54] = z[11] / T(2);
z[55] = -z[12] + z[13] + -z[54];
z[55] = z[10] * z[55];
z[56] = -z[11] + z[13];
z[57] = z[1] + z[56];
z[58] = z[5] / T(2) + z[57];
z[58] = z[8] * z[58];
z[55] = z[55] + z[58];
z[58] = z[13] + z[51];
z[49] = z[49] + -z[58];
z[49] = z[6] * z[49];
z[59] = -z[14] + z[30];
z[60] = T(-9) * z[24] + z[59];
z[60] = z[46] * z[60];
z[49] = z[49] + z[60];
z[60] = z[14] + -z[24];
z[61] = -z[16] / T(6) + z[18] / T(3) + z[19] / T(2);
z[60] = z[60] * z[61];
z[62] = z[1] + -z[5];
z[63] = z[56] + z[62];
z[63] = z[36] * z[63];
z[64] = z[51] / T(3) + -z[56];
z[53] = -z[53] + z[64] / T(4);
z[53] = z[4] * z[53];
z[54] = -z[13] / T(2) + z[51] + z[54];
z[64] = z[1] / T(2);
z[54] = z[54] / T(3) + -z[64];
z[54] = z[9] * z[54];
z[65] = z[11] + z[50] + (T(-7) * z[51]) / T(3);
z[66] = (T(2) * z[1]) / T(3);
z[65] = z[65] / T(4) + z[66];
z[65] = z[7] * z[65];
z[61] = (T(5) * z[46]) / T(4) + z[61];
z[61] = z[23] * z[61];
z[67] = -z[12] + z[56];
z[68] = z[5] / T(3) + z[67];
z[68] = z[42] * z[68];
z[69] = -z[16] / T(2) + z[18] + (T(3) * z[19]) / T(2);
z[70] = z[46] + z[69] / T(2);
z[70] = z[31] * z[70];
z[49] = z[49] / T(4) + z[52] + z[53] + z[54] + z[55] / T(3) + z[60] + z[61] + -z[63] + z[65] + z[68] + z[70];
z[49] = z[29] * z[49];
z[52] = z[4] + z[32];
z[53] = z[7] + z[9];
z[54] = z[8] + z[53];
z[55] = T(2) * z[10] + -z[52] + T(3) * z[54];
z[55] = z[43] * z[55];
z[49] = z[49] + z[55];
z[49] = z[29] * z[49];
z[55] = T(3) * z[51];
z[60] = z[55] + -z[56];
z[61] = T(2) * z[1];
z[65] = z[60] / T(2) + -z[61];
z[68] = z[32] * z[65];
z[70] = z[30] * z[69];
z[71] = z[14] * z[69];
z[72] = z[70] + -z[71];
z[73] = z[68] + z[72];
z[74] = -z[1] + z[51];
z[75] = T(2) * z[74];
z[75] = z[53] * z[75];
z[76] = z[51] + z[56];
z[77] = z[76] / T(2);
z[78] = z[10] * z[77];
z[79] = z[23] * z[69];
z[75] = -z[73] + z[75] + -z[78] + -z[79];
z[80] = z[25] * z[75];
z[81] = T(3) * z[5];
z[82] = T(3) * z[11];
z[83] = z[81] + z[82];
z[84] = z[12] + z[13];
z[85] = -z[83] + z[84];
z[86] = z[61] + z[85] / T(2);
z[87] = z[2] * z[86];
z[88] = -z[72] + z[87];
z[89] = z[5] + z[13];
z[90] = z[11] + z[12] + z[89];
z[91] = z[10] / T(2);
z[92] = z[90] * z[91];
z[93] = z[7] * z[77];
z[94] = z[31] * z[69];
z[93] = z[93] + -z[94];
z[62] = -z[11] + z[62];
z[95] = T(2) * z[62];
z[96] = z[9] * z[95];
z[92] = -z[88] + z[92] + z[93] + z[96];
z[96] = z[0] * z[92];
z[97] = T(3) * z[13];
z[98] = -z[82] + z[97];
z[99] = T(3) * z[12];
z[100] = -z[98] + z[99];
z[101] = -z[5] + z[100];
z[102] = z[101] / T(2);
z[103] = z[42] * z[102];
z[69] = z[24] * z[69];
z[104] = z[69] + -z[79];
z[105] = z[103] + z[104];
z[106] = z[10] * z[67];
z[107] = z[93] + z[105] + z[106];
z[108] = z[9] * z[77];
z[109] = z[107] + z[108];
z[110] = z[28] * z[109];
z[80] = z[80] + z[96] + -z[110];
z[96] = z[69] + -z[94] + z[103];
z[65] = z[53] * z[65];
z[101] = z[91] * z[101];
z[65] = z[65] + -z[73] + -z[96] + z[101];
z[73] = z[41] * z[65];
z[101] = T(2) * z[28];
z[103] = z[26] + z[101];
z[103] = z[67] * z[103];
z[110] = z[25] * z[77];
z[100] = z[5] + z[100];
z[100] = z[100] / T(2);
z[111] = z[41] * z[100];
z[112] = z[0] * z[77];
z[103] = z[103] + -z[110] + z[111] + z[112];
z[103] = z[8] * z[103];
z[111] = -(z[7] * z[74]);
z[112] = z[8] * z[67];
z[106] = -z[63] + z[106] + z[111] + z[112];
z[111] = T(2) * z[27];
z[106] = z[106] * z[111];
z[82] = z[5] + -z[13] + z[82] + z[99];
z[82] = z[82] * z[91];
z[82] = z[82] + -z[105];
z[83] = z[83] + z[84];
z[83] = -z[1] + z[83] / T(2);
z[84] = z[9] * z[83];
z[84] = z[82] + -z[84] + -z[88];
z[88] = z[8] * z[102];
z[88] = z[84] + z[88];
z[88] = z[39] * z[88];
z[99] = z[23] + -z[24];
z[105] = z[31] + z[99];
z[112] = z[28] * z[105];
z[113] = -z[31] + z[59];
z[114] = z[0] * z[113];
z[115] = z[23] + z[59];
z[116] = z[25] * z[115];
z[112] = z[112] + z[114] + -z[116];
z[59] = z[59] + z[99];
z[114] = z[39] * z[59];
z[116] = z[24] + z[113];
z[117] = z[41] * z[116];
z[118] = z[26] * z[105];
z[114] = z[112] + -z[114] + z[117] + z[118];
z[117] = z[15] / T(2);
z[119] = z[114] * z[117];
z[120] = -z[51] + z[56];
z[120] = z[1] + z[120] / T(2);
z[121] = z[9] * z[120];
z[107] = z[107] + z[121];
z[107] = z[26] * z[107];
z[121] = T(-5) * z[23] + T(7) * z[24] + z[30];
z[121] = -z[31] + z[121] / T(6);
z[52] = (T(-2) * z[10]) / T(27) + z[52] / T(27) + -z[54] / T(9) + z[121] / T(2);
z[54] = prod_pow(z[29], 2);
z[52] = z[52] * z[54];
z[121] = prod_pow(z[46], 2) * z[59];
z[52] = z[52] + z[73] + -z[80] + z[88] + z[103] + z[106] + z[107] + z[119] + z[121] / T(2);
z[73] = int_to_imaginary<T>(1) * z[29];
z[52] = z[52] * z[73];
z[88] = z[26] * z[76];
z[103] = z[28] * z[76];
z[106] = z[88] + z[103];
z[107] = z[25] * z[57];
z[106] = z[106] / T(2) + -z[107];
z[106] = z[25] * z[106];
z[107] = z[51] + T(5) * z[56];
z[119] = z[61] + z[107] / T(2);
z[119] = z[37] * z[119];
z[98] = -z[51] + z[98];
z[121] = z[61] + z[98] / T(2);
z[122] = z[35] * z[121];
z[106] = z[106] + -z[119] + -z[122];
z[57] = z[57] * z[101];
z[57] = z[57] + -z[110];
z[119] = -(z[26] * z[120]);
z[122] = z[5] + z[12];
z[64] = z[13] + z[64] + z[122] / T(2);
z[64] = z[0] * z[64];
z[64] = -z[57] + z[64] + z[119];
z[64] = z[0] * z[64];
z[119] = z[28] * z[77];
z[122] = z[0] + -z[26];
z[123] = z[95] * z[122];
z[62] = z[27] * z[62];
z[62] = z[62] + z[110] + -z[119] + z[123];
z[62] = z[27] * z[62];
z[123] = -(z[25] * z[76]);
z[124] = -(z[26] * z[90]);
z[103] = z[103] + z[123] + z[124];
z[123] = -z[0] + -z[27];
z[95] = z[95] * z[123];
z[123] = z[39] * z[86];
z[95] = -z[54] / T(9) + z[95] + z[103] / T(2) + z[123];
z[95] = z[73] * z[95];
z[88] = z[88] + z[119];
z[88] = z[28] * z[88];
z[103] = prod_pow(z[26], 2);
z[119] = z[103] * z[120];
z[88] = z[88] + z[119];
z[119] = z[38] * z[86];
z[83] = z[34] * z[83];
z[50] = z[5] + (T(5) * z[11]) / T(6) + z[12] / T(6) + z[50] + -z[66];
z[50] = z[29] * z[50];
z[50] = T(3) * z[43] + z[50];
z[50] = z[29] * z[50];
z[50] = z[50] + z[62] + z[64] + z[83] + z[88] / T(2) + z[95] + -z[106] + z[119];
z[50] = z[3] * z[50];
z[55] = z[55] + z[97];
z[62] = -z[11] + z[55];
z[64] = z[62] / T(2);
z[66] = z[6] * z[64];
z[81] = -z[11] + z[12] + -z[81] + -z[97];
z[81] = z[81] * z[91];
z[83] = -(z[9] * z[86]);
z[64] = -(z[7] * z[64]);
z[64] = z[64] + z[66] + -z[70] + z[81] + z[83] + z[87] + z[94] + -z[104];
z[64] = z[34] * z[64];
z[81] = z[1] + z[98] / T(4);
z[81] = z[4] * z[81];
z[83] = z[71] + z[104];
z[86] = z[83] + -z[94];
z[87] = z[76] / T(4);
z[88] = z[53] * z[87];
z[60] = -z[1] + z[60] / T(4);
z[95] = -(z[32] * z[60]);
z[97] = -z[5] + z[67];
z[98] = z[91] * z[97];
z[86] = -z[63] + -z[81] + z[86] / T(2) + z[88] + z[95] + z[98];
z[86] = z[28] * z[86];
z[88] = z[26] * z[109];
z[86] = z[86] + z[88];
z[86] = z[28] * z[86];
z[79] = z[66] + z[79];
z[88] = z[9] * z[90];
z[90] = z[7] * z[58];
z[95] = z[88] / T(4) + z[90] / T(2);
z[85] = z[1] + z[85] / T(4);
z[85] = z[2] * z[85];
z[71] = -z[69] + -z[71];
z[89] = -(z[89] * z[91]);
z[71] = z[71] / T(2) + z[79] + -z[81] + z[85] + z[89] + -z[95];
z[71] = z[0] * z[71];
z[81] = z[4] * z[121];
z[69] = -z[69] + -z[70] + z[78] + z[81] + -z[93] + -z[108];
z[69] = z[28] * z[69];
z[70] = z[10] * z[58];
z[66] = -z[66] + z[70] + z[83];
z[78] = z[66] + z[88] / T(2) + z[90];
z[78] = z[26] * z[78];
z[66] = z[66] + T(2) * z[90] + z[108];
z[88] = z[25] * z[66];
z[69] = z[69] + z[71] + z[78] + z[88];
z[69] = z[0] * z[69];
z[71] = z[26] * z[77];
z[78] = -(z[0] * z[87]);
z[57] = -z[57] + z[71] + z[78];
z[57] = z[0] * z[57];
z[71] = -(z[40] * z[100]);
z[78] = -(z[87] * z[103]);
z[88] = z[1] + z[107] / T(4);
z[88] = z[28] * z[88];
z[89] = z[26] * z[67];
z[88] = z[88] + T(2) * z[89];
z[88] = z[28] * z[88];
z[89] = z[38] * z[102];
z[57] = z[57] + z[71] + z[78] + z[88] + z[89] + -z[106];
z[57] = z[8] * z[57];
z[71] = z[72] + z[96];
z[78] = z[7] + -z[8];
z[78] = z[78] * z[87];
z[87] = -z[9] + z[32];
z[60] = z[60] * z[87];
z[87] = z[10] * z[97];
z[60] = z[60] + z[63] + z[71] / T(2) + z[78] + -z[87] / T(4);
z[60] = z[27] * z[60];
z[63] = -(z[26] * z[92]);
z[71] = -(z[77] * z[122]);
z[67] = -(z[67] * z[101]);
z[67] = z[67] + z[71] + z[110];
z[67] = z[8] * z[67];
z[60] = z[60] + z[63] + z[67] + z[80];
z[60] = z[27] * z[60];
z[63] = z[28] * z[115];
z[67] = -z[14] + z[99];
z[71] = z[26] * z[67];
z[63] = z[63] + z[71];
z[63] = z[25] * z[63];
z[77] = z[38] * z[59];
z[78] = z[23] + z[113];
z[78] = z[35] * z[78];
z[80] = -z[30] + z[105];
z[80] = z[34] * z[80];
z[87] = z[40] * z[116];
z[88] = -z[14] + z[105];
z[89] = z[37] * z[88];
z[63] = z[63] + z[77] + z[78] + z[80] + z[87] + -z[89];
z[77] = z[73] * z[114];
z[78] = z[63] + -z[77];
z[80] = -z[24] + -z[30] + z[31];
z[80] = z[28] * z[80];
z[67] = z[25] * z[67];
z[67] = -z[67] + -z[71] + z[80];
z[71] = z[14] + z[24];
z[80] = T(-2) * z[23] + z[71];
z[80] = z[0] * z[80];
z[80] = T(-2) * z[67] + z[80];
z[80] = z[0] * z[80];
z[87] = z[26] * z[113];
z[87] = z[87] + -z[112];
z[89] = z[27] * z[116];
z[90] = T(2) * z[87] + -z[89];
z[90] = z[27] * z[90];
z[59] = z[59] * z[103];
z[88] = z[28] * z[88];
z[92] = z[88] + T(2) * z[118];
z[92] = z[28] * z[92];
z[93] = z[14] + z[99];
z[96] = -z[31] + (T(-2) * z[93]) / T(3);
z[96] = z[54] * z[96];
z[78] = -z[59] + T(-2) * z[78] + z[80] + z[90] + z[92] + z[96];
z[78] = z[17] * z[78];
z[71] = -z[23] + z[71] / T(2);
z[71] = z[0] * z[71];
z[67] = -z[67] + z[71];
z[67] = z[0] * z[67];
z[71] = -z[87] + z[89] / T(2);
z[71] = z[27] * z[71];
z[80] = z[88] / T(2) + z[118];
z[80] = z[28] * z[80];
z[59] = -z[59] / T(2) + -z[63] + z[67] + -z[71] + z[80];
z[63] = z[59] + z[77];
z[67] = (T(3) * z[31]) / T(2) + z[93];
z[67] = z[54] * z[67];
z[63] = T(3) * z[63] + -z[67];
z[67] = z[20] + z[21] + z[22];
z[67] = -z[67] / T(2);
z[63] = z[63] * z[67];
z[51] = T(5) * z[51] + z[56];
z[51] = z[51] / T(2) + -z[61];
z[51] = z[51] * z[53];
z[53] = z[81] + z[94];
z[56] = -(z[10] * z[76]);
z[51] = z[51] + -z[53] + z[56] + -z[68] + z[83];
z[51] = z[37] * z[51];
z[56] = -(z[62] * z[91]);
z[55] = -z[11] + -z[55];
z[55] = z[7] * z[55];
z[53] = -z[53] + z[55] / T(2) + z[56] + z[72] + z[79];
z[53] = z[35] * z[53];
z[55] = -z[31] / T(2) + -z[93] / T(3);
z[55] = z[54] * z[55];
z[55] = z[55] + z[59];
z[55] = z[55] * z[117];
z[56] = -(z[40] * z[65]);
z[59] = z[72] + z[82];
z[59] = z[59] / T(2) + -z[85] + -z[95];
z[59] = z[59] * z[103];
z[61] = -(z[28] * z[75]);
z[62] = -(z[26] * z[66]);
z[65] = z[9] * z[74];
z[65] = z[65] + -z[70];
z[65] = z[25] * z[65];
z[61] = z[61] + z[62] + z[65];
z[61] = z[25] * z[61];
z[62] = z[38] * z[84];
z[65] = z[26] + -z[39] + -z[111];
z[65] = z[65] * z[73];
z[66] = prod_pow(z[27], 2);
z[65] = -z[38] + (T(-7) * z[54]) / T(6) + z[65] + z[66] + -z[103];
z[58] = -z[1] + z[58];
z[58] = z[33] * z[58] * z[65];
z[65] = -z[14] + T(3) * z[105];
z[65] = z[65] * z[73];
z[66] = z[45] / T(2) + -z[46];
z[66] = z[65] * z[66];
z[67] = z[14] / T(3) + -z[105];
z[54] = z[54] * z[67];
z[54] = z[54] + z[66];
z[54] = z[45] * z[54];
z[65] = z[47] * z[65];
z[66] = -(z[29] * z[43]);
z[67] = int_to_imaginary<T>(1) * prod_pow(z[29], 3);
z[66] = z[66] + z[67] / T(27);
z[66] = z[44] * z[66];
z[67] = -z[24] + z[30];
z[67] = (T(-61) * z[14]) / T(3) + T(-9) * z[23] + T(-5) * z[67];
z[67] = -z[31] + z[67] / T(4);
z[67] = z[48] * z[67];
return z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + T(4) * z[66] + z[67] / T(2) + z[69] + z[78] + z[86];
}



template IntegrandConstructorType<double> f_4_277_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_277_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_277_construct (const Kin<qd_real>&);
#endif

}