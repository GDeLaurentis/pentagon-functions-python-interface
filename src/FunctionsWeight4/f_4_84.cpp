#include "f_4_84.h"

namespace PentagonFunctions {

template <typename T> T f_4_84_abbreviated (const std::array<T,18>&);

template <typename T> class SpDLog_f_4_84_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_84_W_23 (const Kin<T>& kin) {
        c[0] = ((T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * kin.v[1] + (T(-3) / T(2) + (T(3) * kin.v[2]) / T(8) + (T(-3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(4)) * kin.v[2] + (T(3) / T(2) + (T(-9) * kin.v[0]) / T(8) + (T(-3) * kin.v[1]) / T(2) + (T(3) * kin.v[2]) / T(4) + (T(9) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(4)) * kin.v[0] + (T(-3) / T(2) + (T(-9) * kin.v[3]) / T(8) + (T(3) * kin.v[4]) / T(4)) * kin.v[3] + (T(3) / T(2) + (T(3) * kin.v[4]) / T(8)) * kin.v[4];
c[1] = kin.v[0] * (T(3) / T(2) + (T(-3) * kin.v[1]) / T(2) + (T(21) * kin.v[2]) / T(2) + (T(-21) * kin.v[4]) / T(2) + T(-6) * kin.v[0] + T(12) * kin.v[3]) + ((T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * kin.v[1] + (T(3) / T(2) + (T(-9) * kin.v[4]) / T(2)) * kin.v[4] + kin.v[2] * (T(-3) / T(2) + (T(-9) * kin.v[2]) / T(2) + (T(-21) * kin.v[3]) / T(2) + T(9) * kin.v[4]) + kin.v[3] * (T(-3) / T(2) + (T(21) * kin.v[4]) / T(2) + T(-6) * kin.v[3]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(3) / T(2) + (T(-9) * kin.v[0]) / T(8) + (T(-3) * kin.v[1]) / T(2) + (T(3) * kin.v[2]) / T(4) + (T(9) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(4)) * kin.v[0] + ((T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * kin.v[1] + (T(-3) / T(2) + (T(3) * kin.v[2]) / T(8) + (T(-3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(4)) * kin.v[2] + (T(-3) / T(2) + (T(-9) * kin.v[3]) / T(8) + (T(3) * kin.v[4]) / T(4)) * kin.v[3] + (T(3) / T(2) + (T(3) * kin.v[4]) / T(8)) * kin.v[4]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[0] * ((T(-15) * kin.v[0]) / T(4) + (T(-3) * kin.v[2]) / T(2) + (T(15) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(9) + T(-9) * kin.v[1]) + ((T(21) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(-21) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + ((T(-15) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + ((T(21) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + kin.v[1] * (T(9) * kin.v[2] + T(9) * kin.v[3] + T(-9) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[0] * ((T(-15) * kin.v[0]) / T(4) + (T(-3) * kin.v[2]) / T(2) + (T(15) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(9) + T(-9) * kin.v[1]) + ((T(21) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(-21) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + ((T(-15) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + ((T(21) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + kin.v[1] * (T(9) * kin.v[2] + T(9) * kin.v[3] + T(-9) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[0] * ((T(15) * kin.v[0]) / T(4) + (T(3) * kin.v[2]) / T(2) + (T(-15) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-9) + T(9) * kin.v[1]) + ((T(-21) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(21) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + ((T(15) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + ((T(-21) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + kin.v[1] * (T(-9) * kin.v[2] + T(-9) * kin.v[3] + T(9) * kin.v[4]));
c[2] = (T(-3) * kin.v[0]) / T(2) + (T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2);
c[3] = rlog(-kin.v[4]) * (T(-3) * kin.v[0] + T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(-3) * kin.v[0]) / T(2) + (T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * rlog(-kin.v[4] + kin.v[1] + kin.v[2]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (T(3) * kin.v[0] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (T(3) * kin.v[0] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[0] * (prod_pow(abb[2], 2) * ((abb[8] * T(-3)) / T(4) + (abb[6] * T(3)) / T(4) + (abb[5] * T(9)) / T(2) + (abb[7] * T(9)) / T(2)) + abb[4] * ((prod_pow(abb[2], 2) * T(-9)) / T(2) + abb[3] * T(-3)) + abb[1] * (abb[1] * ((abb[6] * T(-9)) / T(4) + (abb[5] * T(-3)) / T(2) + (abb[7] * T(-3)) / T(2) + (abb[4] * T(3)) / T(2) + (abb[8] * T(9)) / T(4)) + abb[2] * ((abb[8] * T(-3)) / T(2) + (abb[6] * T(3)) / T(2) + abb[5] * T(-3) + abb[7] * T(-3)) + abb[2] * abb[4] * T(3)) + abb[3] * ((abb[6] * T(-3)) / T(2) + (abb[8] * T(3)) / T(2) + abb[5] * T(3) + abb[7] * T(3)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_84_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl17 = DLog_W_17<T>(kin),dl18 = DLog_W_18<T>(kin),dl16 = DLog_W_16<T>(kin),dl5 = DLog_W_5<T>(kin),dl24 = DLog_W_24<T>(kin),spdl23 = SpDLog_f_4_84_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,18> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_8(kin_path), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[17] / kin_path.W[17]), -rlog(t), dl17(t), rlog(kin.W[23] / kin_path.W[23]), dl18(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl16(t), dl5(t), dl24(t)}
;

        auto result = f_4_84_abbreviated(abbr);
        result = result + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_84_abbreviated(const std::array<T,18>& abb)
{
using TR = typename T::value_type;
T z[36];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[11];
z[3] = abb[15];
z[4] = abb[16];
z[5] = abb[5];
z[6] = abb[9];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[10];
z[11] = abb[2];
z[12] = abb[17];
z[13] = abb[12];
z[14] = bc<TR>[0];
z[15] = abb[3];
z[16] = abb[13];
z[17] = abb[14];
z[18] = bc<TR>[2];
z[19] = bc<TR>[9];
z[20] = z[6] / T(2);
z[21] = z[2] + z[3];
z[22] = -z[4] + z[21];
z[23] = z[20] + -z[22];
z[24] = z[5] + z[8];
z[23] = z[23] * z[24];
z[25] = z[10] / T(2);
z[26] = z[2] + -z[3] + z[4] + -z[6];
z[26] = z[25] * z[26];
z[27] = -z[7] + z[9];
z[28] = -(z[3] * z[27]);
z[29] = z[6] * z[27];
z[30] = T(3) * z[4];
z[31] = z[2] + T(3) * z[3] + -z[30];
z[31] = z[1] * z[31];
z[23] = z[23] + z[26] + z[28] + -z[29] / T(4) + z[31] / T(2);
z[23] = z[0] * z[23];
z[26] = -z[12] + z[21];
z[31] = z[6] + T(2) * z[26];
z[31] = z[24] * z[31];
z[20] = z[20] * z[27];
z[27] = -(z[1] * z[26]);
z[32] = -z[6] + -z[26];
z[32] = z[10] * z[32];
z[27] = -z[20] + z[27] + z[31] + z[32];
z[27] = z[11] * z[27];
z[31] = T(2) * z[21];
z[32] = -z[12] + z[30] + -z[31];
z[32] = z[1] * z[32];
z[33] = z[4] + z[12];
z[31] = z[31] + -z[33];
z[31] = z[10] * z[31];
z[34] = T(2) * z[24];
z[35] = -z[4] + z[12];
z[34] = z[34] * z[35];
z[31] = z[31] + z[32] + z[34];
z[32] = z[13] * z[31];
z[23] = z[23] + z[27] + z[32];
z[23] = z[0] * z[23];
z[27] = T(2) * z[12];
z[32] = -z[4] + (T(-3) * z[6]) / T(2) + -z[21] + z[27];
z[32] = z[24] * z[32];
z[30] = z[21] + z[30];
z[27] = -z[27] + z[30] / T(2);
z[27] = z[1] * z[27];
z[30] = T(3) * z[6] + z[22];
z[25] = z[25] * z[30];
z[25] = z[25] + z[27] + (T(7) * z[29]) / T(4) + z[32];
z[25] = prod_pow(z[11], 2) * z[25];
z[27] = -(z[11] * z[31]);
z[30] = -z[3] + z[12];
z[32] = z[10] * z[30];
z[30] = z[1] * z[30];
z[32] = -z[30] + z[32];
z[32] = z[13] * z[32];
z[27] = z[27] + -z[32];
z[27] = z[13] * z[27];
z[34] = z[6] + T(2) * z[22];
z[34] = -(z[24] * z[34]);
z[35] = z[1] * z[22];
z[22] = z[6] + -z[22];
z[22] = z[10] * z[22];
z[20] = z[20] + z[22] + z[34] + T(3) * z[35];
z[20] = z[15] * z[20];
z[22] = z[16] * z[31];
z[34] = (T(25) * z[4]) / T(2) + (T(-7) * z[12]) / T(2) + T(-9) * z[21];
z[34] = z[19] * z[34];
z[20] = z[20] + z[22] + z[23] + z[25] + z[27] + z[34] / T(4);
z[22] = -(z[24] * z[26]);
z[23] = -z[28] + -z[29];
z[24] = (T(5) * z[12]) / T(2);
z[25] = z[2] + (T(-5) * z[3]) / T(2) + z[24];
z[25] = z[10] * z[25];
z[26] = (T(-5) * z[4]) / T(2) + (T(3) * z[12]) / T(2) + z[21];
z[26] = z[18] * z[26];
z[22] = z[22] + z[23] / T(4) + z[25] + (T(3) * z[26]) / T(2) + (T(-7) * z[30]) / T(2);
z[23] = prod_pow(z[14], 2);
z[22] = z[22] * z[23];
z[25] = -z[0] + z[17];
z[25] = z[25] * z[31];
z[26] = z[21] + -z[33] / T(2);
z[26] = prod_pow(z[18], 2) * z[26];
z[25] = z[25] + z[26] + T(2) * z[32];
z[21] = (T(3) * z[4]) / T(2) + z[21] + -z[24];
z[21] = z[21] * z[23];
z[21] = z[21] / T(2) + T(3) * z[25];
z[21] = int_to_imaginary<T>(1) * z[14] * z[21];
return T(3) * z[20] + z[21] + z[22];
}



template IntegrandConstructorType<double> f_4_84_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_84_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_84_construct (const Kin<qd_real>&);
#endif

}