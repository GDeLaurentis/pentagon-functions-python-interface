#include "f_4_239.h"

namespace PentagonFunctions {

template <typename T> T f_4_239_abbreviated (const std::array<T,9>&);



template <typename T> IntegrandConstructorType<T> f_4_239_construct (const Kin<T>& kin) {
    return [&kin, 
            dl19 = DLog_W_19<T>(kin),dl25 = DLog_W_25<T>(kin),dl18 = DLog_W_18<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,9> abbr = 
            {dl19(t), rlog(kin.W[17] / kin_path.W[17]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(kin.W[24] / kin_path.W[24]), dl25(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl18(t)}
;

        auto result = f_4_239_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_239_abbreviated(const std::array<T,9>& abb)
{
using TR = typename T::value_type;
T z[21];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[8];
z[7] = abb[5];
z[8] = abb[6];
z[9] = abb[7];
z[10] = bc<TR>[2];
z[11] = bc<TR>[9];
z[12] = prod_pow(z[3], 2);
z[13] = -z[5] + (T(7) * z[6]) / T(3);
z[13] = z[12] * z[13];
z[14] = z[5] + z[6];
z[15] = prod_pow(z[10], 2) * z[14];
z[13] = z[13] + z[15];
z[15] = z[1] + z[4];
z[16] = z[5] * z[15];
z[17] = T(-7) * z[1] + z[4];
z[17] = z[6] * z[17];
z[17] = z[16] + z[17];
z[17] = z[7] * z[17];
z[18] = z[14] * z[15];
z[19] = z[9] * z[18];
z[13] = z[13] / T(2) + z[17] + z[19];
z[13] = int_to_imaginary<T>(1) * z[3] * z[13];
z[17] = z[5] + -z[6];
z[19] = prod_pow(z[2], 2);
z[15] = z[15] * z[17] * z[19];
z[17] = -(z[8] * z[18]);
z[18] = T(3) * z[1] + -z[4];
z[18] = z[6] * z[18];
z[18] = -z[16] + z[18];
z[18] = prod_pow(z[7], 2) * z[18];
z[20] = z[11] * z[14];
z[13] = z[13] + z[15] + z[17] + z[18] + (T(21) * z[20]) / T(8);
z[15] = -z[1] + z[4];
z[15] = z[0] * z[15];
z[17] = (T(-5) * z[1]) / T(3) + z[4] / T(2);
z[17] = z[6] * z[17];
z[14] = z[10] * z[14];
z[14] = (T(3) * z[14]) / T(8) + -z[15] / T(12) + (T(5) * z[16]) / T(12) + z[17];
z[12] = z[12] * z[14];
z[14] = z[15] * z[19];
return z[12] + z[13] / T(2) + z[14];
}



template IntegrandConstructorType<double> f_4_239_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_239_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_239_construct (const Kin<qd_real>&);
#endif

}