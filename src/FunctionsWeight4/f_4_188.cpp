#include "f_4_188.h"

namespace PentagonFunctions {

template <typename T> T f_4_188_abbreviated (const std::array<T,43>&);

template <typename T> class SpDLog_f_4_188_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_188_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(12) + T(6) * kin.v[1]) + kin.v[4] * (T(-12) + T(-6) * kin.v[4]);
c[1] = kin.v[1] * (T(-12) + T(-6) * kin.v[1]) + kin.v[4] * (T(12) + T(6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-6) + prod_pow(abb[2], 2) * abb[4] * T(6) + prod_pow(abb[1], 2) * (abb[4] * T(-6) + abb[3] * T(6)));
    }
};
template <typename T> class SpDLog_f_4_188_W_7 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_188_W_7 (const Kin<T>& kin) {
        c[0] = T(-12) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[3] * (T(-12) + T(-12) * kin.v[1] + T(6) * kin.v[3] + T(12) * kin.v[4]);
c[1] = T(-12) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[3] * (T(-12) + T(-12) * kin.v[1] + T(6) * kin.v[3] + T(12) * kin.v[4]);
c[2] = T(-12) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[3] * (T(-12) + T(-12) * kin.v[1] + T(6) * kin.v[3] + T(12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[7] * c[0] + abb[4] * c[1] + abb[8] * c[2]);
        }

        return abb[5] * (prod_pow(abb[1], 2) * (abb[4] * T(-6) + abb[7] * T(-6) + abb[8] * T(-6)) + prod_pow(abb[6], 2) * abb[7] * T(6) + prod_pow(abb[6], 2) * (abb[4] * T(6) + abb[8] * T(6)));
    }
};
template <typename T> class SpDLog_f_4_188_W_10 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_188_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(-12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(-12) + T(6) * kin.v[1] + T(12) * kin.v[2] + T(-12) * kin.v[4]);
c[1] = kin.v[2] * (T(-12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(-12) + T(6) * kin.v[1] + T(12) * kin.v[2] + T(-12) * kin.v[4]);
c[2] = kin.v[2] * (T(-12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(-12) + T(6) * kin.v[1] + T(12) * kin.v[2] + T(-12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[7] * c[1] + abb[11] * c[2]);
        }

        return abb[9] * (prod_pow(abb[2], 2) * (abb[3] * T(-6) + abb[7] * T(-6) + abb[11] * T(-6)) + abb[3] * prod_pow(abb[10], 2) * T(6) + prod_pow(abb[10], 2) * (abb[7] * T(6) + abb[11] * T(6)));
    }
};
template <typename T> class SpDLog_f_4_188_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_188_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (T(-12) + T(6) * kin.v[0] + T(12) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[4] * (T(-12) + T(-6) * kin.v[4]) + kin.v[2] * (T(12) + T(-6) * kin.v[2] + T(12) * kin.v[4]) + kin.v[1] * (T(-12) * kin.v[2] + T(-12) * kin.v[3] + T(12) * kin.v[4]);
c[1] = kin.v[0] * (T(-12) + T(6) * kin.v[0] + T(12) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[4] * (T(-12) + T(-6) * kin.v[4]) + kin.v[2] * (T(12) + T(-6) * kin.v[2] + T(12) * kin.v[4]) + kin.v[1] * (T(-12) * kin.v[2] + T(-12) * kin.v[3] + T(12) * kin.v[4]);
c[2] = kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[0] * (T(12) + T(-6) * kin.v[0] + T(-12) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(-12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(12) * kin.v[2] + T(12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(6) * kin.v[4]);
c[3] = kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[0] * (T(12) + T(-6) * kin.v[0] + T(-12) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(-12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(12) * kin.v[2] + T(12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[7] * c[1] + abb[8] * c[2] + abb[11] * c[3]);
        }

        return abb[12] * (abb[3] * prod_pow(abb[10], 2) * T(-6) + prod_pow(abb[13], 2) * (abb[8] * T(-6) + abb[11] * T(-6) + abb[3] * T(6) + abb[7] * T(6)) + prod_pow(abb[10], 2) * (abb[7] * T(-6) + abb[8] * T(6) + abb[11] * T(6)));
    }
};
template <typename T> class SpDLog_f_4_188_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_188_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-12) + T(12) * kin.v[0] + T(6) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[0] * (T(12) * kin.v[2] + T(-12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(-6) * kin.v[4]) + kin.v[2] * (T(-12) + T(-6) * kin.v[2] + T(12) * kin.v[4]);
c[1] = kin.v[1] * (T(-12) + T(12) * kin.v[0] + T(6) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[0] * (T(12) * kin.v[2] + T(-12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(-6) * kin.v[4]) + kin.v[2] * (T(-12) + T(-6) * kin.v[2] + T(12) * kin.v[4]);
c[2] = kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[1] * (T(12) + T(-12) * kin.v[0] + T(-6) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[0] * (T(-12) * kin.v[2] + T(12) * kin.v[3] + T(12) * kin.v[4]);
c[3] = kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[1] * (T(12) + T(-12) * kin.v[0] + T(-6) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[0] * (T(-12) * kin.v[2] + T(12) * kin.v[3] + T(12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[11] * c[1] + abb[4] * c[2] + abb[8] * c[3]);
        }

        return abb[14] * (abb[3] * prod_pow(abb[15], 2) * T(-6) + prod_pow(abb[15], 2) * (abb[11] * T(-6) + abb[4] * T(6) + abb[8] * T(6)) + prod_pow(abb[13], 2) * (abb[4] * T(-6) + abb[8] * T(-6) + abb[3] * T(6) + abb[11] * T(6)));
    }
};
template <typename T> class SpDLog_f_4_188_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_188_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[3] * (T(12) + T(-6) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[0] * (T(-12) + T(6) * kin.v[0] + T(-12) * kin.v[2] + T(12) * kin.v[4]) + kin.v[1] * (T(-12) + T(-6) * kin.v[1] + T(12) * kin.v[3] + T(12) * kin.v[4]);
c[1] = kin.v[2] * (T(12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[3] * (T(12) + T(-6) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[0] * (T(-12) + T(6) * kin.v[0] + T(-12) * kin.v[2] + T(12) * kin.v[4]) + kin.v[1] * (T(-12) + T(-6) * kin.v[1] + T(12) * kin.v[3] + T(12) * kin.v[4]);
c[2] = kin.v[0] * (T(12) + T(-6) * kin.v[0] + T(12) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(12) + T(6) * kin.v[1] + T(-12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[2] * (T(-12) + T(-6) * kin.v[2] + T(12) * kin.v[4]) + kin.v[3] * (T(-12) + T(6) * kin.v[3] + T(12) * kin.v[4]);
c[3] = kin.v[0] * (T(12) + T(-6) * kin.v[0] + T(12) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(12) + T(6) * kin.v[1] + T(-12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[2] * (T(-12) + T(-6) * kin.v[2] + T(12) * kin.v[4]) + kin.v[3] * (T(-12) + T(6) * kin.v[3] + T(12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[7] * c[0] + abb[4] * c[1] + abb[8] * c[2] + abb[11] * c[3]);
        }

        return abb[16] * (abb[7] * prod_pow(abb[15], 2) * T(6) + prod_pow(abb[15], 2) * (abb[8] * T(-6) + abb[11] * T(-6) + abb[4] * T(6)) + prod_pow(abb[6], 2) * (abb[4] * T(-6) + abb[7] * T(-6) + abb[8] * T(6) + abb[11] * T(6)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_188_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl7 = DLog_W_7<T>(kin),dl10 = DLog_W_10<T>(kin),dl23 = DLog_W_23<T>(kin),dl22 = DLog_W_22<T>(kin),dl21 = DLog_W_21<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin),dl27 = DLog_W_27<T>(kin),dl30 = DLog_W_30<T>(kin),dl28 = DLog_W_28<T>(kin),dl17 = DLog_W_17<T>(kin),dl19 = DLog_W_19<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl16 = DLog_W_16<T>(kin),dl2 = DLog_W_2<T>(kin),dl31 = DLog_W_31<T>(kin),dl18 = DLog_W_18<T>(kin),dl3 = DLog_W_3<T>(kin),dl4 = DLog_W_4<T>(kin),spdl12 = SpDLog_f_4_188_W_12<T>(kin),spdl7 = SpDLog_f_4_188_W_7<T>(kin),spdl10 = SpDLog_f_4_188_W_10<T>(kin),spdl23 = SpDLog_f_4_188_W_23<T>(kin),spdl22 = SpDLog_f_4_188_W_22<T>(kin),spdl21 = SpDLog_f_4_188_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,43> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl7(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl10(t), rlog(-v_path[4] + v_path[1] + v_path[2]), f_1_3_5(kin) - f_1_3_5(kin_path), dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), dl22(t), rlog(-v_path[2] + v_path[0] + v_path[4]), dl21(t), dl26(t) / kin_path.SqrtDelta, f_2_1_4(kin_path), f_2_1_7(kin_path), f_2_1_8(kin_path), f_2_1_14(kin_path), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[30] / kin_path.W[30]), dl29(t) / kin_path.SqrtDelta, f_2_1_11(kin_path), f_2_1_15(kin_path), dl27(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl17(t), dl19(t), dl5(t), dl20(t), dl16(t), dl2(t), dl31(t), f_2_2_7(kin_path), dl18(t), dl3(t), dl4(t)}
;

        auto result = f_4_188_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr) + spdl21(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_188_abbreviated(const std::array<T,43>& abb)
{
using TR = typename T::value_type;
T z[82];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[41];
z[3] = abb[4];
z[4] = abb[33];
z[5] = abb[34];
z[6] = abb[37];
z[7] = abb[42];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[40];
z[11] = abb[22];
z[12] = abb[29];
z[13] = abb[31];
z[14] = abb[23];
z[15] = abb[24];
z[16] = abb[25];
z[17] = abb[2];
z[18] = abb[10];
z[19] = abb[32];
z[20] = abb[6];
z[21] = abb[15];
z[22] = abb[35];
z[23] = abb[17];
z[24] = abb[26];
z[25] = abb[11];
z[26] = abb[13];
z[27] = abb[36];
z[28] = abb[30];
z[29] = abb[18];
z[30] = abb[19];
z[31] = abb[20];
z[32] = abb[21];
z[33] = bc<TR>[0];
z[34] = abb[27];
z[35] = abb[28];
z[36] = abb[38];
z[37] = abb[39];
z[38] = bc<TR>[1];
z[39] = bc<TR>[5];
z[40] = prod_pow(z[0], 2);
z[41] = T(2) * z[34];
z[42] = z[40] + z[41];
z[43] = -z[0] + z[17];
z[44] = -z[26] + z[43];
z[44] = z[20] + T(2) * z[44];
z[44] = z[20] * z[44];
z[45] = T(2) * z[0];
z[46] = -z[17] + z[45];
z[46] = z[17] * z[46];
z[47] = T(2) * z[35];
z[44] = z[42] + z[44] + -z[46] + z[47];
z[48] = T(2) * z[31];
z[49] = prod_pow(z[26], 2);
z[50] = z[48] + z[49];
z[51] = z[21] + -z[26];
z[52] = z[43] + z[51];
z[53] = T(2) * z[18];
z[52] = z[52] * z[53];
z[54] = T(2) * z[30];
z[55] = T(2) * z[26];
z[56] = z[21] * z[55];
z[57] = -z[44] + z[50] + z[52] + -z[54] + -z[56];
z[57] = z[12] * z[57];
z[58] = z[17] * z[26];
z[58] = -z[30] + z[58];
z[59] = z[20] * z[26];
z[59] = -z[32] + -z[35] + z[59];
z[60] = z[58] + -z[59];
z[61] = prod_pow(z[18], 2);
z[62] = prod_pow(z[21], 2);
z[60] = -z[48] + T(2) * z[60] + -z[61] + z[62];
z[63] = -(z[28] * z[60]);
z[64] = T(2) * z[29];
z[65] = T(2) * z[32];
z[44] = z[44] + z[62] + z[64] + z[65];
z[66] = z[13] * z[44];
z[67] = -z[0] + z[20];
z[68] = z[26] + -z[67];
z[69] = T(2) * z[21];
z[68] = z[68] * z[69];
z[70] = z[18] + T(2) * z[51];
z[70] = z[18] * z[70];
z[68] = z[47] + z[68] + -z[70];
z[65] = z[49] + z[65];
z[71] = z[48] + z[65];
z[72] = z[41] + -z[68] + z[71];
z[72] = z[24] * z[72];
z[73] = -z[21] + z[55];
z[73] = z[21] * z[73];
z[71] = -z[71] + z[73];
z[74] = -z[29] + z[30];
z[74] = T(2) * z[74];
z[52] = -z[52] + z[71] + z[74];
z[75] = z[23] * z[52];
z[76] = z[2] + z[7] + z[10] + T(-2) * z[36];
z[77] = z[37] * z[76];
z[57] = -z[57] + z[63] + z[66] + z[72] + z[75] + T(2) * z[77];
z[63] = z[15] + T(-2) * z[16];
z[63] = z[57] * z[63];
z[44] = z[4] * z[44];
z[66] = z[7] * z[41];
z[44] = -z[44] + z[66];
z[66] = z[0] * z[17];
z[72] = z[20] * z[43];
z[66] = z[29] + z[40] + -z[66] + z[72];
z[72] = T(2) * z[7];
z[75] = z[66] * z[72];
z[77] = T(2) * z[43];
z[78] = z[20] + z[77];
z[78] = z[20] * z[78];
z[42] = z[42] + z[64] + z[78];
z[46] = -z[42] + z[46];
z[46] = z[5] * z[46];
z[64] = z[47] + z[65];
z[78] = -z[20] + z[55];
z[78] = z[20] * z[78];
z[78] = -z[64] + z[78];
z[79] = z[2] * z[78];
z[80] = z[22] * z[62];
z[79] = z[79] + z[80];
z[59] = T(2) * z[59] + -z[62];
z[80] = z[27] * z[59];
z[46] = z[44] + z[46] + z[75] + -z[79] + z[80];
z[46] = z[3] * z[46];
z[69] = z[67] * z[69];
z[75] = prod_pow(z[20], 2);
z[69] = z[40] + -z[47] + z[69] + -z[75];
z[69] = z[10] * z[69];
z[80] = z[21] * z[26];
z[51] = z[18] * z[51];
z[51] = z[32] + z[49] + z[51] + -z[80];
z[80] = z[51] * z[72];
z[81] = -z[7] + z[22];
z[81] = z[48] * z[81];
z[49] = z[27] * z[49];
z[49] = z[49] + z[80] + -z[81];
z[68] = -z[65] + z[68];
z[68] = z[22] * z[68];
z[80] = z[10] + -z[22];
z[80] = z[41] * z[80];
z[71] = -z[70] + z[71];
z[71] = z[19] * z[71];
z[75] = -(z[4] * z[75]);
z[68] = z[49] + z[68] + z[69] + z[71] + z[75] + z[80];
z[68] = z[9] * z[68];
z[60] = z[27] * z[60];
z[69] = z[17] + -z[55];
z[69] = z[17] * z[69];
z[50] = z[50] + z[54] + z[69];
z[50] = z[10] * z[50];
z[48] = -z[48] + T(2) * z[58] + -z[61];
z[48] = z[5] * z[48];
z[54] = -(z[4] * z[59]);
z[58] = -(z[19] * z[61]);
z[48] = z[48] + z[50] + z[54] + z[58] + z[60] + z[79];
z[48] = z[25] * z[48];
z[43] = z[43] * z[53];
z[50] = -z[40] + z[74];
z[53] = prod_pow(z[17], 2);
z[43] = -z[43] + z[50] + z[53];
z[54] = z[2] * z[43];
z[52] = z[19] * z[52];
z[58] = z[65] + z[70];
z[59] = z[58] + -z[73];
z[59] = z[22] * z[59];
z[53] = z[5] * z[53];
z[49] = -z[49] + -z[52] + z[53] + z[54] + z[59];
z[49] = z[1] * z[49];
z[46] = z[46] + z[48] + z[49] + z[63] + z[68];
z[48] = -z[51] + z[66];
z[48] = z[48] * z[72];
z[43] = z[43] + -z[78];
z[43] = z[2] * z[43];
z[45] = z[17] * z[45];
z[42] = -z[42] + z[45];
z[42] = z[5] * z[42];
z[45] = z[20] * z[55];
z[45] = z[45] + -z[62] + -z[64];
z[45] = z[27] * z[45];
z[49] = -z[56] + z[58];
z[49] = z[22] * z[49];
z[42] = z[42] + z[43] + z[44] + z[45] + z[48] + z[49] + -z[52] + z[81];
z[43] = prod_pow(z[33], 2);
z[44] = z[5] + -z[22] + -z[27];
z[44] = z[43] * z[44];
z[45] = -z[18] + z[77];
z[45] = z[18] * z[45];
z[48] = z[45] + -z[50];
z[48] = z[43] + T(3) * z[48];
z[48] = z[6] * z[48];
z[42] = T(3) * z[42] + z[44] + z[48];
z[42] = z[8] * z[42];
z[44] = z[11] + z[14];
z[44] = T(6) * z[44];
z[44] = z[44] * z[57];
z[48] = (T(5) * z[5]) / T(2) + z[22] / T(2);
z[49] = -z[2] + z[7];
z[49] = T(2) * z[49];
z[50] = (T(-5) * z[27]) / T(2) + z[48] + -z[49];
z[50] = z[3] * z[50];
z[51] = z[27] / T(2);
z[52] = -z[7] + z[10];
z[52] = z[4] / T(2) + (T(5) * z[19]) / T(2) + -z[51] + T(2) * z[52];
z[52] = z[9] * z[52];
z[53] = -z[2] + z[10];
z[48] = (T(5) * z[4]) / T(2) + z[19] / T(2) + -z[48] + T(2) * z[53];
z[48] = z[25] * z[48];
z[49] = -z[5] / T(2) + (T(-5) * z[22]) / T(2) + z[49] + z[51];
z[49] = z[1] * z[49];
z[48] = z[48] + z[49] + z[50] + z[52];
z[48] = z[43] * z[48];
z[49] = z[21] + T(-2) * z[67];
z[49] = z[21] * z[49];
z[41] = -z[41] + z[47] + z[49];
z[41] = z[9] * z[41];
z[45] = z[45] + -z[74];
z[45] = z[1] * z[45];
z[40] = z[3] * z[40];
z[40] = z[40] + z[41] + z[45];
z[41] = z[1] + -z[9];
z[41] = -z[3] + T(5) * z[41];
z[41] = z[41] * z[43];
z[40] = T(6) * z[40] + z[41] / T(2);
z[40] = z[6] * z[40];
z[41] = prod_pow(z[38], 2);
z[41] = T(3) * z[41] + -z[43];
z[41] = z[33] * z[41];
z[41] = T(-144) * z[39] + z[41];
z[41] = int_to_imaginary<T>(1) * z[41] * z[76];
return z[40] + (T(4) * z[41]) / T(5) + T(2) * z[42] + z[44] + T(6) * z[46] + z[48];
}



template IntegrandConstructorType<double> f_4_188_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_188_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_188_construct (const Kin<qd_real>&);
#endif

}