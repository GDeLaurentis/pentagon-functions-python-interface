#include "f_4_221.h"

namespace PentagonFunctions {

template <typename T> T f_4_221_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_221_W_10 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_221_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(-2) + kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[4]);
c[1] = kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(-2) + kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[4]);
c[2] = kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(-2) + kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1] + abb[5] * c[2]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] + prod_pow(abb[2], 2) * (abb[4] + abb[5]) + prod_pow(abb[1], 2) * (-abb[3] + -abb[4] + -abb[5]));
    }
};
template <typename T> class SpDLog_f_4_221_W_23 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_221_W_23 (const Kin<T>& kin) {
        c[0] = ((T(3) * kin.v[2]) / T(4) + kin.v[3] / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-1)) * kin.v[2] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (-kin.v[3] / T(4) + -kin.v[4] / T(2) + T(-1)) * kin.v[3] + (-kin.v[0] / T(4) + -kin.v[2] / T(2) + kin.v[3] / T(2) + kin.v[4] / T(2) + -kin.v[1] + T(1)) * kin.v[0] + ((T(3) * kin.v[4]) / T(4) + T(1)) * kin.v[4];
c[1] = -kin.v[0] + kin.v[2] + kin.v[3] + -kin.v[4];
c[2] = ((T(3) * kin.v[2]) / T(4) + kin.v[3] / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-1)) * kin.v[2] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (-kin.v[3] / T(4) + -kin.v[4] / T(2) + T(-1)) * kin.v[3] + (-kin.v[0] / T(4) + -kin.v[2] / T(2) + kin.v[3] / T(2) + kin.v[4] / T(2) + -kin.v[1] + T(1)) * kin.v[0] + ((T(3) * kin.v[4]) / T(4) + T(1)) * kin.v[4];
c[3] = -kin.v[0] + kin.v[2] + kin.v[3] + -kin.v[4];
c[4] = kin.v[0] * (-kin.v[3] / T(2) + -kin.v[4] / T(2) + kin.v[0] / T(4) + kin.v[2] / T(2) + T(-1) + kin.v[1]) + (kin.v[3] / T(4) + kin.v[4] / T(2) + T(1)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + (-kin.v[3] / T(2) + (T(-3) * kin.v[2]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(1)) * kin.v[2];
c[5] = kin.v[0] + -kin.v[2] + -kin.v[3] + kin.v[4];
c[6] = kin.v[0] * (-kin.v[3] / T(2) + -kin.v[4] / T(2) + kin.v[0] / T(4) + kin.v[2] / T(2) + T(-1) + kin.v[1]) + (kin.v[3] / T(4) + kin.v[4] / T(2) + T(1)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + (-kin.v[3] / T(2) + (T(-3) * kin.v[2]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(1)) * kin.v[2];
c[7] = kin.v[0] + -kin.v[2] + -kin.v[3] + kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[3] * (t * c[0] + c[1]) + abb[4] * (t * c[2] + c[3]) + abb[9] * (t * c[4] + c[5]) + abb[5] * (t * c[6] + c[7]);
        }

        return abb[6] * (abb[8] * (abb[5] + abb[9] + -abb[4]) + prod_pow(abb[7], 2) * (abb[5] + abb[9] + -abb[3] + -abb[4]) + abb[3] * (prod_pow(abb[2], 2) + -abb[8]) + prod_pow(abb[2], 2) * (abb[4] + -abb[5] + -abb[9]));
    }
};
template <typename T> class SpDLog_f_4_221_W_22 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_221_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (-kin.v[4] / T(2) + kin.v[3] / T(4) + T(1)) * kin.v[3] + (-kin.v[2] / T(2) + -kin.v[3] / T(2) + kin.v[1] / T(4) + kin.v[4] / T(2) + T(-1) + kin.v[0]) * kin.v[1] + ((T(-3) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + ((T(-3) * kin.v[2]) / T(4) + kin.v[3] / T(2) + (T(3) * kin.v[4]) / T(2) + T(-1)) * kin.v[2];
c[1] = kin.v[1] + kin.v[2] + -kin.v[3] + -kin.v[4];
c[2] = kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (-kin.v[4] / T(2) + kin.v[3] / T(4) + T(1)) * kin.v[3] + (-kin.v[2] / T(2) + -kin.v[3] / T(2) + kin.v[1] / T(4) + kin.v[4] / T(2) + T(-1) + kin.v[0]) * kin.v[1] + ((T(-3) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + ((T(-3) * kin.v[2]) / T(4) + kin.v[3] / T(2) + (T(3) * kin.v[4]) / T(2) + T(-1)) * kin.v[2];
c[3] = kin.v[1] + kin.v[2] + -kin.v[3] + -kin.v[4];
c[4] = (-kin.v[3] / T(2) + (T(3) * kin.v[2]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(1)) * kin.v[2] + (-kin.v[1] / T(4) + -kin.v[4] / T(2) + kin.v[2] / T(2) + kin.v[3] / T(2) + -kin.v[0] + T(1)) * kin.v[1] + (-kin.v[3] / T(4) + kin.v[4] / T(2) + T(-1)) * kin.v[3] + ((T(3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]);
c[5] = -kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4];
c[6] = (-kin.v[3] / T(2) + (T(3) * kin.v[2]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(1)) * kin.v[2] + (-kin.v[1] / T(4) + -kin.v[4] / T(2) + kin.v[2] / T(2) + kin.v[3] / T(2) + -kin.v[0] + T(1)) * kin.v[1] + (-kin.v[3] / T(4) + kin.v[4] / T(2) + T(-1)) * kin.v[3] + ((T(3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]);
c[7] = -kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[3] * (t * c[0] + c[1]) + abb[5] * (t * c[2] + c[3]) + abb[13] * (t * c[4] + c[5]) + abb[9] * (t * c[6] + c[7]);
        }

        return abb[10] * (prod_pow(abb[11], 2) * (abb[9] + abb[13] + -abb[5]) + abb[3] * (abb[12] + -prod_pow(abb[11], 2)) + abb[12] * (abb[5] + -abb[9] + -abb[13]) + prod_pow(abb[7], 2) * (abb[3] + abb[5] + -abb[9] + -abb[13]));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_221_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl23 = DLog_W_23<T>(kin),dl22 = DLog_W_22<T>(kin),dl4 = DLog_W_4<T>(kin),dl27 = DLog_W_27<T>(kin),dl18 = DLog_W_18<T>(kin),dl8 = DLog_W_8<T>(kin),dl19 = DLog_W_19<T>(kin),dl15 = DLog_W_15<T>(kin),dl31 = DLog_W_31<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl16 = DLog_W_16<T>(kin),dl3 = DLog_W_3<T>(kin),dl17 = DLog_W_17<T>(kin),dl20 = DLog_W_20<T>(kin),dl5 = DLog_W_5<T>(kin),dl28 = DLog_W_28<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),spdl10 = SpDLog_f_4_221_W_10<T>(kin),spdl23 = SpDLog_f_4_221_W_23<T>(kin),spdl22 = SpDLog_f_4_221_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,46> abbr = 
            {dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), f_2_1_8(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl22(t), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl4(t), dl27(t) / kin_path.SqrtDelta, rlog(kin.W[0] / kin_path.W[0]), f_2_1_7(kin_path), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[19] / kin_path.W[19]), dl18(t), dl8(t), rlog(v_path[2]), dl19(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl15(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl31(t), dl2(t), dl1(t), dl16(t), dl3(t), dl17(t), dl20(t), dl5(t), dl28(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta}
;

        auto result = f_4_221_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_221_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[120];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[25];
z[3] = abb[31];
z[4] = abb[35];
z[5] = abb[37];
z[6] = abb[38];
z[7] = abb[39];
z[8] = abb[40];
z[9] = abb[41];
z[10] = abb[4];
z[11] = abb[5];
z[12] = abb[9];
z[13] = abb[15];
z[14] = abb[16];
z[15] = abb[18];
z[16] = abb[19];
z[17] = abb[20];
z[18] = abb[21];
z[19] = abb[22];
z[20] = abb[23];
z[21] = abb[24];
z[22] = abb[43];
z[23] = abb[45];
z[24] = abb[2];
z[25] = abb[44];
z[26] = abb[7];
z[27] = abb[11];
z[28] = abb[36];
z[29] = abb[27];
z[30] = bc<TR>[0];
z[31] = abb[14];
z[32] = abb[28];
z[33] = abb[8];
z[34] = abb[12];
z[35] = abb[17];
z[36] = abb[29];
z[37] = abb[30];
z[38] = abb[32];
z[39] = abb[33];
z[40] = abb[26];
z[41] = abb[13];
z[42] = abb[42];
z[43] = abb[34];
z[44] = bc<TR>[3];
z[45] = bc<TR>[2];
z[46] = bc<TR>[1];
z[47] = bc<TR>[4];
z[48] = bc<TR>[9];
z[49] = z[22] + z[23];
z[50] = z[14] / T(4) + z[16] + -z[19] / T(2);
z[51] = z[15] / T(4) + (T(-3) * z[20]) / T(4) + z[50];
z[52] = -(z[49] * z[51]);
z[53] = z[25] + z[42];
z[54] = -z[13] + z[53];
z[50] = -z[15] / T(12) + z[20] / T(4) + -z[50] / T(3);
z[50] = z[50] * z[54];
z[55] = z[11] / T(3);
z[56] = -z[1] + -z[12] / T(3);
z[56] = -z[55] + z[56] / T(4);
z[56] = z[28] * z[56];
z[57] = z[1] / T(3);
z[58] = z[12] + z[57];
z[58] = z[55] + z[58] / T(4);
z[59] = z[4] * z[58];
z[60] = z[41] / T(3);
z[61] = z[1] + -z[12];
z[62] = z[61] / T(2);
z[63] = -z[60] + z[62];
z[63] = z[32] * z[63];
z[58] = -z[58] + z[60];
z[58] = z[6] * z[58];
z[60] = z[10] / T(3);
z[57] = -z[12] + z[57];
z[57] = (T(-5) * z[41]) / T(3) + z[57] / T(2) + -z[60];
z[64] = z[5] / T(2);
z[57] = z[57] * z[64];
z[65] = z[4] + z[28] + T(4) * z[43];
z[66] = (T(2) * z[5]) / T(27) + z[6] / T(9) + (T(-5) * z[22]) / T(12) + -z[23] / T(2) + -z[53] / T(12) + -z[65] / T(27);
z[67] = int_to_imaginary<T>(1) * z[30];
z[66] = z[66] * z[67];
z[68] = z[2] * z[61];
z[69] = z[11] + z[12];
z[70] = z[41] + z[69];
z[70] = z[40] * z[70];
z[71] = z[2] * z[10];
z[72] = z[1] + z[12];
z[73] = z[11] + z[72] / T(4);
z[73] = z[31] * z[73];
z[50] = z[50] + z[52] + T(5) * z[56] + z[57] + z[58] + z[59] + z[63] / T(2) + z[66] + z[68] / T(4) + z[70] + z[71] / T(6) + z[73] / T(3);
z[50] = z[30] * z[50];
z[52] = z[14] + z[15];
z[52] = T(-2) * z[16] + z[19] + (T(3) * z[20]) / T(2) + -z[52] / T(2);
z[56] = z[42] * z[52];
z[57] = (T(3) * z[61]) / T(2);
z[58] = -z[41] + z[57];
z[59] = z[32] * z[58];
z[63] = z[25] * z[52];
z[56] = z[56] + z[59] + z[63];
z[66] = z[22] * z[52];
z[73] = z[56] + -z[66];
z[73] = z[26] * z[73];
z[74] = T(3) * z[12];
z[75] = z[1] + z[74];
z[76] = T(2) * z[11];
z[77] = z[75] / T(2) + z[76];
z[78] = z[4] * z[77];
z[79] = -z[63] + z[78];
z[80] = z[23] * z[52];
z[81] = z[79] + -z[80];
z[81] = z[24] * z[81];
z[82] = T(3) * z[1];
z[83] = z[12] + z[82];
z[83] = z[76] + z[83] / T(2);
z[84] = z[28] * z[83];
z[85] = -z[66] + z[84];
z[86] = -z[80] + z[85];
z[87] = z[27] * z[86];
z[81] = z[81] + -z[87];
z[86] = z[0] * z[86];
z[73] = z[73] + z[81] + -z[86];
z[79] = z[79] + -z[85];
z[79] = z[39] * z[79];
z[86] = z[24] * z[69];
z[87] = z[27] * z[62];
z[88] = T(2) * z[86] + -z[87];
z[89] = z[0] * z[62];
z[90] = z[88] + -z[89];
z[91] = -z[41] + z[61];
z[92] = z[26] * z[91];
z[93] = z[90] + T(2) * z[92];
z[94] = -z[1] + T(5) * z[12];
z[94] = z[76] + z[94] / T(2);
z[95] = z[39] * z[94];
z[96] = z[93] + -z[95];
z[96] = z[6] * z[96];
z[97] = z[24] + z[27];
z[98] = z[61] * z[97];
z[99] = z[0] * z[61];
z[100] = z[98] + z[99];
z[92] = z[92] + z[100] / T(2);
z[101] = -(z[39] * z[61]);
z[101] = z[92] + z[101];
z[101] = z[5] * z[101];
z[102] = z[5] * z[91];
z[102] = z[70] + z[102];
z[103] = T(2) * z[29];
z[102] = z[102] * z[103];
z[104] = T(3) * z[22] + T(4) * z[23] + z[25];
z[105] = z[47] * z[104];
z[79] = -z[73] + z[79] + z[96] + z[101] + z[102] + z[105];
z[79] = int_to_imaginary<T>(1) * z[79];
z[96] = z[56] + z[80] + -z[84];
z[101] = T(2) * z[41] + -z[57];
z[102] = z[6] * z[101];
z[105] = -(z[5] * z[58]);
z[102] = z[96] + z[102] + z[105];
z[105] = int_to_imaginary<T>(1) * z[37];
z[102] = z[102] * z[105];
z[65] = T(-2) * z[5] + T(-3) * z[6] + z[65];
z[65] = z[44] * z[65];
z[50] = z[50] + z[65] + z[79] + z[102];
z[50] = z[30] * z[50];
z[65] = z[13] + z[42];
z[79] = -z[23] + z[65] / T(2);
z[79] = z[26] * z[79];
z[102] = -z[22] + z[25];
z[106] = -z[13] + z[102];
z[107] = z[0] * z[106];
z[108] = z[23] * z[27];
z[109] = z[23] * z[24];
z[79] = z[79] + z[107] + z[108] + z[109];
z[79] = z[26] * z[79];
z[107] = z[27] * z[49];
z[108] = z[23] + z[25];
z[108] = z[24] * z[108];
z[107] = z[107] + -z[108];
z[108] = z[42] + z[102];
z[108] = z[26] * z[108];
z[110] = z[0] * z[49];
z[108] = z[107] + z[108] + z[110];
z[110] = z[42] + z[49];
z[111] = z[29] / T(2);
z[111] = z[110] * z[111];
z[111] = -z[108] + z[111];
z[111] = z[29] * z[111];
z[112] = z[13] + z[49];
z[113] = z[0] / T(2);
z[114] = z[112] * z[113];
z[107] = z[107] + z[114];
z[107] = z[0] * z[107];
z[114] = -z[49] + z[53];
z[114] = z[34] * z[114];
z[106] = z[23] + z[106];
z[106] = z[33] * z[106];
z[115] = -(z[38] * z[102]);
z[109] = z[27] * z[109];
z[53] = z[23] + z[53];
z[116] = z[36] * z[53];
z[117] = z[35] * z[112];
z[79] = z[79] + -z[106] + z[107] + -z[109] + z[111] + z[114] + z[115] + z[116] + z[117];
z[106] = z[39] * z[102];
z[106] = z[106] + z[108];
z[106] = int_to_imaginary<T>(1) * z[106];
z[53] = z[53] * z[105];
z[53] = -z[53] + z[106];
z[49] = T(3) * z[49] + z[54];
z[54] = z[30] / T(2);
z[49] = z[49] * z[54];
z[49] = -z[49] + T(3) * z[53];
z[49] = z[30] * z[49];
z[49] = z[49] + T(3) * z[79];
z[53] = z[17] + z[18] + z[21];
z[53] = -z[53] / T(2);
z[49] = z[49] * z[53];
z[53] = -(z[72] * z[97]);
z[54] = z[26] / T(2);
z[79] = -z[10] + -z[41] + z[72];
z[79] = z[54] * z[79];
z[106] = z[10] + z[61];
z[107] = z[0] * z[106];
z[53] = z[53] / T(2) + z[79] + z[107];
z[53] = z[26] * z[53];
z[79] = prod_pow(z[27], 2);
z[91] = z[79] * z[91];
z[106] = z[24] * z[106];
z[72] = z[72] / T(2);
z[108] = z[27] * z[72];
z[108] = -z[106] + z[108];
z[108] = z[24] * z[108];
z[109] = z[10] + z[62];
z[109] = z[0] * z[109];
z[109] = z[98] + z[109];
z[109] = z[109] * z[113];
z[111] = z[38] * z[61];
z[113] = z[10] + z[57];
z[114] = -(z[35] * z[113]);
z[115] = z[36] * z[58];
z[82] = -z[12] + z[82];
z[82] = -z[41] + z[82] / T(2);
z[82] = z[34] * z[82];
z[53] = z[53] + z[82] + z[91] + z[108] + z[109] + z[111] + z[114] + z[115];
z[53] = z[5] * z[53];
z[82] = z[24] * z[62];
z[108] = z[1] + z[11];
z[109] = T(2) * z[108];
z[114] = z[27] * z[109];
z[82] = z[82] + z[114];
z[114] = z[0] * z[109];
z[115] = z[26] * z[62];
z[114] = z[82] + z[114] + -z[115];
z[116] = z[29] * z[108];
z[116] = -z[114] + z[116];
z[116] = z[29] * z[116];
z[117] = z[36] * z[83];
z[118] = T(5) * z[1] + -z[12];
z[118] = z[76] + z[118] / T(2);
z[119] = z[38] * z[118];
z[116] = z[116] + z[117] + z[119];
z[117] = z[39] * z[118];
z[109] = z[29] * z[109];
z[109] = z[109] + z[114] + -z[117];
z[109] = int_to_imaginary<T>(1) * z[109];
z[83] = z[83] * z[105];
z[105] = T(3) * z[44];
z[83] = -z[83] + z[105] + z[109];
z[109] = z[67] / T(9);
z[114] = (T(5) * z[1]) / T(3) + z[12];
z[60] = (T(2) * z[11]) / T(3) + -z[60] + z[109] + z[114] / T(4);
z[60] = z[30] * z[60];
z[60] = z[60] + -z[83];
z[60] = z[30] * z[60];
z[114] = z[26] * z[61];
z[107] = z[98] / T(2) + T(2) * z[107] + -z[114] / T(4);
z[107] = z[26] * z[107];
z[117] = -(z[79] * z[108]);
z[106] = -z[87] + -z[106];
z[106] = z[24] * z[106];
z[118] = -z[10] + (T(-3) * z[61]) / T(4);
z[118] = z[0] * z[118];
z[118] = -z[82] + z[118];
z[118] = z[0] * z[118];
z[119] = T(-2) * z[10] + -z[57];
z[119] = z[35] * z[119];
z[113] = -(z[33] * z[113]);
z[60] = z[60] + z[106] + z[107] + z[113] + -z[116] + z[117] + z[118] + z[119];
z[60] = z[9] * z[60];
z[88] = -z[88] + z[99] / T(4);
z[88] = z[0] * z[88];
z[77] = z[35] * z[77];
z[94] = z[38] * z[94];
z[77] = z[77] + z[88] + -z[94];
z[88] = z[90] + z[115];
z[69] = z[69] * z[103];
z[69] = z[69] + z[88] + -z[95];
z[69] = int_to_imaginary<T>(1) * z[69];
z[90] = z[1] + (T(5) * z[12]) / T(3);
z[55] = z[55] + z[90] / T(4) + z[109];
z[55] = z[30] * z[55];
z[55] = z[55] + z[69] + -z[105];
z[55] = z[30] * z[55];
z[69] = z[11] * z[27];
z[90] = z[11] * z[24];
z[69] = z[69] + z[90];
z[94] = z[11] * z[26];
z[69] = T(2) * z[69] + -z[89] + -z[94];
z[69] = z[26] * z[69];
z[89] = z[72] + z[76];
z[94] = z[34] * z[89];
z[95] = z[29] * z[61];
z[88] = -z[88] + -z[95] / T(4);
z[88] = z[29] * z[88];
z[95] = z[11] * z[79];
z[76] = z[27] * z[76];
z[105] = z[24] * z[76];
z[106] = -z[11] + z[72];
z[107] = -(z[33] * z[106]);
z[55] = z[55] + -z[69] + -z[77] + z[88] + z[94] + z[95] + z[105] + z[107];
z[55] = z[7] * z[55];
z[88] = z[51] * z[110];
z[62] = z[41] + z[62];
z[62] = z[62] * z[64];
z[64] = z[11] + z[75] / T(4);
z[64] = z[4] * z[64];
z[59] = z[59] / T(2);
z[72] = z[11] + -z[41] + z[72];
z[72] = z[6] * z[72];
z[62] = -z[59] + z[62] + -z[64] + -z[70] + z[72] + z[84] + z[88];
z[62] = z[29] * z[62];
z[72] = -(z[6] * z[93]);
z[75] = -(z[5] * z[92]);
z[62] = z[62] + z[72] + z[73] + z[75];
z[62] = z[29] * z[62];
z[72] = T(7) * z[1] + z[12];
z[72] = z[11] + z[67] / T(3) + z[72] / T(4);
z[73] = z[30] / T(3);
z[72] = z[72] * z[73];
z[72] = z[72] + -z[83];
z[72] = z[30] * z[72];
z[75] = z[76] + z[90];
z[75] = z[24] * z[75];
z[76] = -(z[0] * z[108]);
z[76] = z[76] + -z[82];
z[76] = z[0] * z[76];
z[82] = z[33] * z[89];
z[83] = -(z[34] * z[106]);
z[69] = -z[69] + z[72] + z[75] + z[76] + z[82] + z[83] + -z[116];
z[69] = z[8] * z[69];
z[72] = -z[98] + z[99] + z[115];
z[54] = z[54] * z[72];
z[72] = -z[86] + z[87];
z[72] = z[24] * z[72];
z[75] = -(z[36] * z[101]);
z[54] = z[54] + z[72] + z[75] + -z[77] + z[91];
z[54] = z[6] * z[54];
z[72] = -z[26] + T(-2) * z[39] + z[103];
z[72] = z[61] * z[72];
z[72] = z[72] + z[100];
z[72] = int_to_imaginary<T>(1) * z[72];
z[61] = z[61] * z[73];
z[61] = z[61] + z[72];
z[61] = z[30] * z[61];
z[72] = z[0] * z[100];
z[73] = -z[100] + z[114];
z[73] = z[29] * z[73];
z[75] = -(z[26] * z[99]);
z[61] = z[61] + z[72] + z[73] + z[75] + T(2) * z[111];
z[61] = z[3] * z[61];
z[68] = (T(3) * z[68]) / T(4) + z[71] / T(2);
z[72] = z[51] * z[112];
z[64] = z[64] + z[68] + z[72];
z[64] = z[0] * z[64];
z[64] = z[64] + -z[81];
z[64] = z[0] * z[64];
z[57] = z[2] * z[57];
z[72] = z[13] * z[52];
z[57] = z[57] + z[71] + -z[72];
z[71] = z[66] + z[80];
z[72] = z[57] + -z[71] + z[78];
z[72] = z[35] * z[72];
z[51] = z[51] * z[65];
z[65] = z[31] * z[89];
z[73] = z[65] + -z[80];
z[51] = z[51] + -z[59] + z[68] + -z[73];
z[51] = z[26] * z[51];
z[57] = z[57] + z[63] + -z[66];
z[59] = -(z[0] * z[57]);
z[63] = z[73] * z[97];
z[51] = z[51] + z[59] + z[63];
z[51] = z[26] * z[51];
z[58] = z[6] * z[58];
z[56] = -z[56] + z[58] + -z[65] + z[71];
z[56] = z[34] * z[56];
z[58] = -z[78] + z[85];
z[58] = z[38] * z[58];
z[59] = -(z[36] * z[96]);
z[63] = -z[1] + z[74];
z[63] = -z[10] + z[63] / T(2);
z[63] = z[5] * z[63];
z[57] = z[57] + z[63] + -z[73];
z[57] = z[33] * z[57];
z[63] = prod_pow(z[30], 2);
z[65] = -z[22] + (T(-4) * z[23]) / T(3) + -z[25] / T(3);
z[65] = z[63] * z[65];
z[66] = z[25] / T(2);
z[68] = (T(3) * z[22]) / T(2) + T(2) * z[23] + z[66];
z[68] = z[46] * z[67] * z[68];
z[65] = z[65] + z[68];
z[65] = z[46] * z[65];
z[68] = -(z[46] * z[104]);
z[71] = z[45] * z[102];
z[68] = z[68] + z[71];
z[67] = z[67] * z[68];
z[66] = z[22] / T(2) + z[23] + z[42] + z[66];
z[63] = z[63] * z[66];
z[63] = z[63] + z[67];
z[63] = z[45] * z[63];
z[66] = -(z[24] * z[27] * z[73]);
z[67] = z[70] * z[79];
z[68] = (T(13) * z[22]) / T(2) + (T(19) * z[23]) / T(3);
z[68] = z[48] * z[68];
z[52] = z[38] * z[52];
z[52] = (T(11) * z[48]) / T(12) + z[52];
z[52] = z[25] * z[52];
return z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + z[68] / T(2) + z[69] + z[72];
}



template IntegrandConstructorType<double> f_4_221_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_221_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_221_construct (const Kin<qd_real>&);
#endif

}