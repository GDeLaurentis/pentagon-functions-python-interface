#include "f_4_377.h"

namespace PentagonFunctions {

template <typename T> T f_4_377_abbreviated (const std::array<T,63>&);

template <typename T> class SpDLog_f_4_377_W_10 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_377_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(-3) + T(3) * kin.v[2] + T(-3) * kin.v[4]);
c[1] = kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(-3) + T(3) * kin.v[2] + T(-3) * kin.v[4]);
c[2] = kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(-3) + T(3) * kin.v[2] + T(-3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1] + abb[5] * c[2]);
        }

        return abb[0] * (prod_pow(abb[1], 2) * ((abb[3] * T(-3)) / T(2) + (abb[4] * T(-3)) / T(2) + (abb[5] * T(-3)) / T(2)) + (prod_pow(abb[2], 2) * abb[3] * T(3)) / T(2) + prod_pow(abb[2], 2) * ((abb[4] * T(3)) / T(2) + (abb[5] * T(3)) / T(2)));
    }
};
template <typename T> class SpDLog_f_4_377_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_377_W_12 (const Kin<T>& kin) {
        c[0] = (kin.v[0] * kin.v[4]) / T(2) + -(kin.v[2] * kin.v[4]) + -(kin.v[3] * kin.v[4]) + bc<T>[0] * (T(-9) / T(2) + (T(-9) * kin.v[4]) / T(4)) * int_to_imaginary<T>(1) * kin.v[4] + ((T(-3) * kin.v[4]) / T(2) + T(7)) * kin.v[4] + kin.v[1] * (-kin.v[0] / T(2) + (T(19) * kin.v[4]) / T(2) + T(-7) + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + (T(-8) + bc<T>[0] * (int_to_imaginary<T>(9) / T(4))) * kin.v[1] + kin.v[2] + kin.v[3]);
c[1] = (T(-4) + bc<T>[0] * (int_to_imaginary<T>(9) / T(2))) * kin.v[1] + T(4) * kin.v[4] + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) * kin.v[4];
c[2] = kin.v[1] * (kin.v[0] / T(2) + (T(-19) * kin.v[4]) / T(2) + -kin.v[2] + -kin.v[3] + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + T(7) + (bc<T>[0] * (int_to_imaginary<T>(-9) / T(4)) + T(8)) * kin.v[1]) + -(kin.v[0] * kin.v[4]) / T(2) + kin.v[2] * kin.v[4] + kin.v[3] * kin.v[4] + ((T(3) * kin.v[4]) / T(2) + T(-7)) * kin.v[4] + bc<T>[0] * (T(9) / T(2) + (T(9) * kin.v[4]) / T(4)) * int_to_imaginary<T>(1) * kin.v[4];
c[3] = (bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + T(4)) * kin.v[1] + T(-4) * kin.v[4] + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[3] * (t * c[0] + c[1]) + abb[14] * (t * c[2] + c[3]);
        }

        return abb[32] * (abb[14] * abb[33] * T(-4) + abb[1] * ((abb[11] * abb[14]) / T(2) + -(abb[2] * abb[14]) / T(2) + (abb[12] * abb[14] * T(-9)) / T(2) + (abb[1] * abb[14] * T(-3)) / T(2) + abb[14] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2))) + abb[1] * abb[8] * abb[14] * T(4) + abb[3] * (abb[1] * abb[8] * T(-4) + abb[1] * (abb[2] / T(2) + -abb[11] / T(2) + (abb[1] * T(3)) / T(2) + (abb[12] * T(9)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(9) / T(2))) + abb[33] * T(4)) + abb[10] * ((abb[1] * abb[14]) / T(2) + (abb[2] * abb[14]) / T(2) + -(abb[11] * abb[14]) / T(2) + (abb[12] * abb[14] * T(9)) / T(2) + abb[10] * (abb[14] + -abb[3]) + abb[8] * abb[14] * T(-4) + abb[14] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[3] * (abb[11] / T(2) + -abb[1] / T(2) + -abb[2] / T(2) + (abb[12] * T(-9)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[8] * T(4))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_377_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl11 = DLog_W_11<T>(kin),dl6 = DLog_W_6<T>(kin),dl29 = DLog_W_29<T>(kin),dl20 = DLog_W_20<T>(kin),dl24 = DLog_W_24<T>(kin),dl14 = DLog_W_14<T>(kin),dl12 = DLog_W_12<T>(kin),dl31 = DLog_W_31<T>(kin),dl16 = DLog_W_16<T>(kin),dl3 = DLog_W_3<T>(kin),dl19 = DLog_W_19<T>(kin),dl5 = DLog_W_5<T>(kin),dl17 = DLog_W_17<T>(kin),dl4 = DLog_W_4<T>(kin),dl1 = DLog_W_1<T>(kin),dl18 = DLog_W_18<T>(kin),dl2 = DLog_W_2<T>(kin),dl28 = DLog_W_28<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl27 = DLog_W_27<T>(kin),spdl10 = SpDLog_f_4_377_W_10<T>(kin),spdl12 = SpDLog_f_4_377_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,63> abbr = 
            {dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl11(t), rlog(v_path[0]), rlog(v_path[3]), dl6(t), rlog(-v_path[1]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_9(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl29(t) / kin_path.SqrtDelta, rlog(kin.W[0] / kin_path.W[0]), f_2_1_11(kin_path), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), dl20(t), dl24(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl14(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl12(t), f_2_1_4(kin_path), dl31(t), dl16(t), f_2_1_2(kin_path), f_2_1_7(kin_path), dl3(t), dl19(t), dl5(t), dl17(t), dl4(t), dl1(t), dl18(t), dl2(t), dl28(t) / kin_path.SqrtDelta, f_2_2_1(kin_path), f_2_2_2(kin_path), f_2_2_3(kin_path), f_2_2_4(kin_path), f_2_2_5(kin_path), f_2_2_6(kin_path), f_2_2_7(kin_path), f_2_2_8(kin_path), f_2_2_9(kin_path), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[17] / kin_path.W[17]), dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta}
;

        auto result = f_4_377_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr);

        return result;
    };
}

template <typename T> T f_4_377_abbreviated(const std::array<T,63>& abb)
{
using TR = typename T::value_type;
T z[245];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[35];
z[3] = abb[38];
z[4] = abb[39];
z[5] = abb[40];
z[6] = abb[41];
z[7] = abb[44];
z[8] = abb[45];
z[9] = abb[4];
z[10] = abb[5];
z[11] = abb[14];
z[12] = abb[16];
z[13] = abb[60];
z[14] = abb[61];
z[15] = abb[62];
z[16] = abb[20];
z[17] = abb[21];
z[18] = abb[22];
z[19] = abb[23];
z[20] = abb[24];
z[21] = abb[56];
z[22] = abb[57];
z[23] = abb[58];
z[24] = abb[59];
z[25] = abb[2];
z[26] = abb[26];
z[27] = abb[43];
z[28] = abb[42];
z[29] = abb[46];
z[30] = abb[8];
z[31] = abb[10];
z[32] = abb[11];
z[33] = abb[12];
z[34] = bc<TR>[0];
z[35] = abb[29];
z[36] = abb[7];
z[37] = abb[9];
z[38] = abb[13];
z[39] = abb[25];
z[40] = abb[17];
z[41] = abb[18];
z[42] = abb[19];
z[43] = abb[27];
z[44] = abb[28];
z[45] = abb[30];
z[46] = abb[31];
z[47] = abb[33];
z[48] = abb[36];
z[49] = abb[37];
z[50] = abb[47];
z[51] = abb[48];
z[52] = abb[49];
z[53] = abb[50];
z[54] = abb[51];
z[55] = abb[52];
z[56] = abb[53];
z[57] = abb[54];
z[58] = abb[55];
z[59] = abb[6];
z[60] = abb[15];
z[61] = abb[34];
z[62] = bc<TR>[1];
z[63] = bc<TR>[3];
z[64] = bc<TR>[5];
z[65] = bc<TR>[2];
z[66] = bc<TR>[4];
z[67] = bc<TR>[7];
z[68] = bc<TR>[8];
z[69] = bc<TR>[9];
z[70] = z[16] + -z[20];
z[71] = (T(3) * z[18]) / T(2) + -z[19];
z[72] = z[21] / T(4) + z[22] + (T(-5) * z[23]) / T(4) + -z[24];
z[73] = z[17] / T(4);
z[74] = z[12] / T(2);
z[71] = (T(-3) * z[70]) / T(4) + z[71] / T(2) + -z[72] + z[73] + z[74];
z[75] = T(3) * z[71];
z[76] = -z[14] + z[29];
z[77] = z[15] + z[76];
z[77] = z[75] * z[77];
z[78] = z[13] * z[75];
z[79] = T(5) * z[9];
z[80] = (T(9) * z[11]) / T(2);
z[81] = z[79] + -z[80];
z[82] = T(9) * z[10];
z[83] = (T(15) * z[1]) / T(2);
z[84] = z[82] + z[83];
z[85] = z[81] + z[84];
z[86] = z[4] / T(2);
z[85] = z[85] * z[86];
z[87] = (T(3) * z[1]) / T(4);
z[88] = (T(3) * z[10]) / T(2);
z[89] = z[87] + z[88];
z[90] = T(4) * z[9];
z[91] = (T(3) * z[11]) / T(4) + -z[90];
z[92] = z[89] + z[91];
z[92] = z[6] * z[92];
z[93] = z[1] + -z[9];
z[94] = z[10] + -z[11];
z[95] = z[93] + z[94];
z[96] = z[8] * z[95];
z[97] = (T(13) * z[11]) / T(2);
z[98] = T(3) * z[9];
z[99] = z[97] + -z[98];
z[100] = (T(19) * z[1]) / T(2) + T(11) * z[10] + -z[99];
z[101] = z[27] / T(2);
z[100] = z[100] * z[101];
z[102] = z[26] * z[95];
z[103] = (T(25) * z[11]) / T(2) + z[98];
z[104] = T(4) * z[10] + -z[103] / T(2);
z[105] = (T(19) * z[1]) / T(4) + z[104];
z[105] = z[7] * z[105];
z[104] = (T(13) * z[1]) / T(4) + z[104];
z[104] = z[5] * z[104];
z[77] = -z[77] + z[78] + -z[85] + z[92] + -z[96] + z[100] + (T(-7) * z[102]) / T(2) + z[104] + z[105];
z[85] = z[44] * z[77];
z[92] = T(3) * z[1];
z[96] = z[10] + z[92];
z[104] = z[4] * z[96];
z[105] = z[1] + -z[10];
z[106] = -(z[39] * z[105]);
z[104] = z[104] + -z[106];
z[107] = -z[70] / T(2);
z[74] = z[17] + -z[18] + -z[19] / T(2) + z[74] + -z[107];
z[108] = z[60] * z[74];
z[104] = z[104] / T(2) + -z[108];
z[109] = z[1] / T(4);
z[110] = z[10] / T(4);
z[111] = z[11] / T(2);
z[112] = z[9] + z[111];
z[113] = z[109] + -z[110] + z[112];
z[114] = z[6] + -z[27];
z[114] = z[113] * z[114];
z[115] = z[74] / T(2);
z[116] = z[13] * z[115];
z[117] = z[7] / T(4);
z[96] = z[96] * z[117];
z[118] = z[8] / T(4);
z[119] = z[105] * z[118];
z[120] = z[29] * z[115];
z[115] = z[15] * z[115];
z[96] = z[96] + -z[104] / T(2) + z[114] + -z[115] + -z[116] + z[119] + z[120];
z[96] = T(3) * z[96];
z[104] = -(z[42] * z[96]);
z[114] = z[1] + z[94];
z[116] = z[5] * z[114];
z[119] = z[6] * z[9];
z[121] = z[116] + -z[119];
z[122] = T(3) * z[11];
z[123] = z[28] * z[122];
z[124] = z[35] * z[94];
z[125] = z[124] / T(2);
z[123] = z[123] + z[125];
z[126] = (T(5) * z[102]) / T(2);
z[127] = z[98] + z[111];
z[128] = (T(5) * z[1]) / T(2);
z[129] = z[127] + z[128];
z[130] = z[10] / T(2);
z[131] = -z[129] + z[130];
z[131] = z[27] * z[131];
z[132] = z[1] + z[9];
z[133] = z[11] + z[132];
z[134] = z[37] * z[133];
z[135] = -(z[4] * z[92]);
z[136] = z[8] * z[128];
z[121] = (T(-5) * z[121]) / T(2) + -z[123] + z[126] + z[131] + T(3) * z[134] + z[135] + z[136];
z[121] = z[32] * z[121];
z[105] = z[105] * z[117];
z[105] = z[105] + z[115];
z[131] = (T(3) * z[124]) / T(2);
z[135] = z[9] * z[28];
z[136] = z[131] + -z[135];
z[137] = (T(5) * z[10]) / T(4) + z[87];
z[138] = (T(3) * z[11]) / T(2);
z[139] = z[9] + z[138];
z[140] = -z[137] + z[139];
z[140] = z[5] * z[140];
z[141] = -z[10] + z[92];
z[142] = z[118] * z[141];
z[137] = z[11] + -z[137];
z[137] = z[27] * z[137];
z[137] = z[102] + -z[105] + z[119] + z[136] + z[137] + z[140] + z[142];
z[140] = T(3) * z[33];
z[137] = z[137] * z[140];
z[142] = -z[9] + z[10];
z[143] = z[1] / T(2);
z[144] = z[142] + z[143];
z[145] = (T(5) * z[11]) / T(2);
z[146] = z[144] + -z[145];
z[147] = z[5] / T(2);
z[148] = z[146] * z[147];
z[148] = -z[102] + z[148];
z[149] = -z[142] + z[145];
z[150] = (T(3) * z[1]) / T(2);
z[151] = z[149] + -z[150];
z[152] = z[7] / T(2);
z[151] = z[151] * z[152];
z[153] = z[10] + z[143];
z[154] = z[112] + z[153];
z[154] = z[101] * z[154];
z[155] = z[8] / T(2);
z[156] = z[146] * z[155];
z[157] = z[4] * z[9];
z[151] = -z[119] + z[148] + -z[151] + z[154] + -z[156] + -z[157];
z[154] = T(3) * z[25];
z[156] = -(z[151] * z[154]);
z[158] = (T(11) * z[11]) / T(2);
z[159] = T(7) * z[9];
z[160] = z[158] + z[159];
z[161] = (T(17) * z[10]) / T(2);
z[162] = z[92] + -z[160] + z[161];
z[162] = z[5] * z[162];
z[163] = z[80] + z[159];
z[164] = (T(15) * z[10]) / T(2);
z[165] = z[92] + z[164];
z[166] = z[163] + -z[165];
z[166] = z[4] * z[166];
z[162] = z[162] + z[166];
z[166] = (T(9) * z[10]) / T(2);
z[167] = z[92] + z[166];
z[168] = -z[138] + z[167];
z[169] = z[159] + z[168];
z[170] = z[28] / T(2);
z[169] = z[169] * z[170];
z[171] = (T(23) * z[11]) / T(2) + z[159];
z[161] = -z[161] + z[171];
z[172] = z[92] + z[161];
z[172] = z[155] * z[172];
z[161] = -z[92] + z[161];
z[161] = z[101] * z[161];
z[173] = z[94] * z[152];
z[174] = T(4) * z[124];
z[161] = -z[161] + z[162] / T(2) + z[169] + -z[172] + -z[173] + -z[174];
z[162] = z[46] * z[161];
z[169] = (T(3) * z[74]) / T(2);
z[172] = z[29] * z[169];
z[173] = z[78] + -z[102] + z[172];
z[175] = z[4] * z[141];
z[175] = z[106] + z[175];
z[175] = z[108] + z[175] / T(2);
z[176] = (T(7) * z[11]) / T(2);
z[177] = -z[142] + z[150] + -z[176];
z[177] = z[5] * z[177];
z[178] = z[9] + z[158];
z[179] = (T(5) * z[10]) / T(2);
z[180] = -z[92] + z[178] + -z[179];
z[181] = z[27] * z[180];
z[168] = -z[9] + -z[168];
z[168] = z[28] * z[168];
z[168] = T(7) * z[124] + z[168] + T(3) * z[175] + z[177] + z[181];
z[177] = (T(3) * z[6]) / T(2);
z[181] = z[146] * z[177];
z[182] = z[7] * z[128];
z[183] = z[1] * z[8];
z[168] = z[168] / T(2) + z[173] + z[181] + z[182] + T(6) * z[183];
z[168] = z[31] * z[168];
z[181] = -(z[0] * z[75]);
z[182] = z[12] + z[17];
z[183] = -z[18] + T(3) * z[19];
z[107] = -z[72] + z[107] + (T(3) * z[182]) / T(4) + -z[183] / T(4);
z[182] = z[36] * z[107];
z[183] = T(3) * z[182];
z[184] = prod_pow(z[65], 2);
z[185] = (T(7) * z[184]) / T(4);
z[181] = T(-12) * z[66] + z[181] + -z[183] + -z[185];
z[181] = z[13] * z[181];
z[186] = z[0] * z[71];
z[187] = z[182] + z[186];
z[188] = z[46] * z[107];
z[184] = (T(5) * z[184]) / T(4) + z[187] + -z[188];
z[188] = (T(-9) * z[66]) / T(2) + -z[184];
z[189] = T(3) * z[14];
z[188] = z[188] * z[189];
z[190] = z[25] * z[75];
z[185] = z[66] + z[185] + z[190];
z[185] = z[15] * z[185];
z[184] = T(2) * z[66] + z[184];
z[190] = T(3) * z[29];
z[184] = z[184] * z[190];
z[180] = z[36] * z[180];
z[191] = -z[83] + -z[142] + z[158];
z[191] = z[0] * z[191];
z[180] = z[180] + z[191];
z[191] = -z[9] + z[94];
z[191] = z[30] * z[191];
z[180] = z[180] / T(2) + T(-3) * z[191];
z[180] = z[8] * z[180];
z[192] = -(z[59] * z[142]);
z[193] = -z[124] + z[157] + z[192];
z[193] = z[30] * z[193];
z[194] = z[36] * z[134];
z[195] = z[193] + z[194];
z[196] = z[9] + z[80];
z[164] = T(9) * z[1] + z[164] + -z[196];
z[164] = z[86] * z[164];
z[123] = z[123] + z[164] + T(3) * z[192];
z[123] = z[36] * z[123];
z[82] = z[82] + -z[150] + -z[196];
z[82] = z[82] * z[86];
z[82] = z[82] + T(-3) * z[116] + -z[174];
z[82] = z[0] * z[82];
z[116] = -z[88] + z[112];
z[116] = z[36] * z[116];
z[164] = z[0] * z[146];
z[116] = z[116] + -z[164];
z[177] = z[116] * z[177];
z[197] = z[7] + -z[61];
z[198] = -z[4] + z[5] + T(3) * z[28];
z[199] = z[6] / T(2);
z[200] = (T(3) * z[27]) / T(4) + z[197] + z[198] / T(4) + z[199];
z[200] = T(-5) * z[13] + (T(-11) * z[14]) / T(2) + z[118] + z[200] / T(5);
z[200] = z[15] + T(9) * z[29] + T(3) * z[200];
z[200] = z[62] * z[200];
z[201] = T(7) * z[13] + T(9) * z[14] + z[15] + (T(-15) * z[29]) / T(2);
z[201] = z[65] * z[201];
z[200] = z[200] / T(2) + z[201];
z[200] = z[62] * z[200];
z[201] = -z[0] + T(2) * z[36];
z[95] = z[26] * z[95] * z[201];
z[201] = -(z[36] * z[114]);
z[201] = -z[191] + z[201];
z[202] = T(3) * z[27];
z[201] = z[201] * z[202];
z[203] = T(5) * z[11];
z[204] = z[1] + z[10];
z[205] = z[203] + z[204];
z[205] = z[36] * z[205];
z[114] = z[0] * z[114];
z[114] = T(-5) * z[114] + z[205];
z[205] = -(z[30] * z[122]);
z[114] = z[114] / T(2) + z[205];
z[114] = z[7] * z[114];
z[205] = prod_pow(z[62], 2);
z[206] = z[3] * z[205];
z[82] = z[82] + z[85] + -z[95] + z[104] + z[114] + z[121] + z[123] + z[137] + z[156] + z[162] + z[168] + z[177] + z[180] + z[181] + z[184] + z[185] + z[188] + T(-3) * z[195] + z[200] + z[201] + (T(-3) * z[206]) / T(20);
z[82] = int_to_imaginary<T>(1) * z[82];
z[85] = z[12] + T(5) * z[18] + -z[19];
z[70] = z[70] + z[72] + z[73] + -z[85] / T(4);
z[72] = (T(3) * z[65]) / T(2) + z[70];
z[72] = z[15] * z[72];
z[70] = (T(-5) * z[65]) / T(6) + -z[70];
z[70] = z[29] * z[70];
z[73] = (T(7) * z[10]) / T(2) + z[92];
z[85] = (T(-19) * z[9]) / T(3) + z[73] + -z[138];
z[85] = z[4] * z[85];
z[85] = z[85] + -z[106] / T(2) + -z[108];
z[104] = z[10] + z[150];
z[114] = z[104] + z[112];
z[121] = z[114] * z[170];
z[123] = -z[111] + -z[153];
z[123] = z[5] * z[123];
z[137] = -z[9] + -z[87] + -z[110];
z[137] = z[3] * z[137];
z[156] = T(-31) * z[4] + T(71) * z[5] + T(53) * z[28];
z[156] = (T(41) * z[6]) / T(2) + (T(73) * z[27]) / T(4) + z[156] / T(4) + T(31) * z[197];
z[156] = (T(-73) * z[2]) / T(180) + (T(-41) * z[3]) / T(90) + (T(31) * z[8]) / T(36) + (T(11) * z[13]) / T(4) + (T(9) * z[14]) / T(4) + -z[15] / T(12) + z[156] / T(45);
z[156] = int_to_imaginary<T>(1) * z[34] * z[156];
z[162] = z[13] * z[65];
z[70] = z[70] + z[72] + z[85] / T(2) + -z[121] + z[123] + (T(5) * z[124]) / T(3) + z[137] + z[156] + -z[162];
z[72] = z[65] / T(4) + -z[107];
z[85] = z[14] / T(2);
z[72] = z[72] * z[85];
z[123] = -z[9] / T(3) + -z[111];
z[123] = z[1] + T(7) * z[123] + z[179];
z[123] = z[2] * z[123];
z[137] = (T(-7) * z[13]) / T(3) + -z[14];
z[137] = (T(13) * z[15]) / T(3) + (T(9) * z[29]) / T(2) + z[137] / T(2);
z[137] = z[62] * z[137];
z[123] = z[123] + z[137];
z[87] = z[10] + z[87];
z[137] = T(2) * z[11];
z[156] = (T(3) * z[9]) / T(2) + -z[87] + z[137];
z[156] = z[27] * z[156];
z[162] = -(z[112] * z[199]);
z[168] = -z[9] + (T(5) * z[10]) / T(6) + (T(-29) * z[11]) / T(6);
z[168] = z[1] / T(3) + z[168] / T(4);
z[168] = z[7] * z[168];
z[90] = (T(-17) * z[10]) / T(8) + (T(25) * z[11]) / T(4) + z[90];
z[90] = (T(5) * z[1]) / T(8) + z[90] / T(3);
z[90] = z[8] * z[90];
z[70] = z[70] / T(2) + z[72] + z[90] + -z[102] / T(3) + z[123] / T(4) + z[156] + z[162] + z[168];
z[70] = z[34] * z[70];
z[72] = z[3] + -z[6];
z[90] = T(-4) * z[197];
z[123] = (T(3) * z[27]) / T(2);
z[156] = z[4] + T(-4) * z[5] + T(-5) * z[8] + (T(7) * z[72]) / T(2) + z[90] + -z[123];
z[156] = z[63] * z[156];
z[162] = (T(9) * z[1]) / T(4);
z[168] = T(3) * z[10];
z[177] = z[162] + z[168];
z[180] = z[9] + (T(21) * z[11]) / T(2);
z[181] = -z[177] + z[180] / T(2);
z[184] = -z[0] + z[44];
z[184] = z[181] * z[184];
z[139] = T(7) * z[139];
z[185] = z[139] + -z[165];
z[188] = z[36] / T(2);
z[195] = z[46] / T(2) + -z[188];
z[195] = z[185] * z[195];
z[184] = z[184] + z[195] + (T(-9) * z[205]) / T(40);
z[184] = int_to_imaginary<T>(1) * z[184];
z[184] = (T(3) * z[63]) / T(2) + z[184];
z[184] = z[2] * z[184];
z[70] = z[70] + z[82] + z[156] + z[184];
z[70] = z[34] * z[70];
z[82] = z[9] / T(2);
z[156] = -z[82] + z[137];
z[184] = T(2) * z[10];
z[195] = -z[128] + z[156] + -z[184];
z[200] = z[27] * z[195];
z[201] = z[98] + z[145];
z[206] = z[150] + z[168];
z[207] = z[201] + -z[206];
z[207] = z[199] * z[207];
z[208] = z[143] + -z[168];
z[209] = -z[196] + -z[208];
z[209] = z[86] * z[209];
z[210] = z[9] + T(7) * z[11];
z[211] = T(7) * z[10];
z[212] = z[210] + -z[211];
z[212] = -z[92] + z[212] / T(2);
z[212] = z[5] * z[212];
z[213] = z[203] + -z[211];
z[213] = z[213] / T(2);
z[214] = T(4) * z[1];
z[215] = z[213] + -z[214];
z[215] = z[7] * z[215];
z[216] = T(5) * z[10];
z[217] = -z[178] + z[216];
z[218] = -z[128] + -z[217];
z[218] = z[155] * z[218];
z[219] = z[11] + z[82];
z[219] = z[28] * z[219];
z[126] = -z[78] + z[126] + T(2) * z[134] + z[200] + z[207] + z[209] + z[212] + z[215] + z[218] + -z[219];
z[126] = z[25] * z[126];
z[200] = (T(7) * z[1]) / T(2);
z[207] = z[166] + -z[196] + z[200];
z[207] = z[5] * z[207];
z[209] = z[102] + z[134];
z[212] = -z[13] + -z[15];
z[169] = z[169] * z[212];
z[212] = z[9] + z[88] + -z[122];
z[215] = (T(11) * z[1]) / T(2);
z[218] = z[212] + z[215];
z[218] = z[101] * z[218];
z[220] = T(13) * z[1] + z[168];
z[220] = z[117] * z[220];
z[212] = z[150] + -z[212];
z[212] = z[199] * z[212];
z[221] = T(-11) * z[1] + -z[168];
z[221] = z[118] * z[221];
z[222] = z[28] * z[196];
z[169] = z[169] + z[172] + (T(3) * z[175]) / T(2) + z[207] + (T(-9) * z[209]) / T(2) + z[212] + z[218] + z[220] + z[221] + z[222];
z[169] = z[32] * z[169];
z[172] = z[25] * z[71];
z[175] = -z[172] + z[186];
z[175] = z[175] * z[189];
z[189] = z[30] * z[107];
z[189] = z[172] + -z[187] + z[189];
z[207] = z[189] * z[190];
z[209] = z[4] * z[204];
z[209] = -z[106] + z[209];
z[209] = -z[108] + z[209] / T(2);
z[212] = -z[9] + z[111];
z[73] = z[73] + -z[212];
z[73] = z[27] * z[73];
z[176] = z[9] + z[176];
z[218] = z[153] + z[176];
z[218] = z[5] * z[218];
z[176] = z[167] + -z[176];
z[176] = z[28] * z[176];
z[176] = -z[73] + -z[124] + z[176] + T(3) * z[209] + z[218];
z[218] = z[206] + z[212];
z[220] = -(z[199] * z[218]);
z[221] = -(z[7] * z[200]);
z[222] = -(z[8] * z[92]);
z[173] = z[134] + -z[173] + z[176] / T(2) + z[220] + z[221] + z[222];
z[173] = z[31] * z[173];
z[176] = -(z[5] * z[195]);
z[195] = z[128] + z[168];
z[220] = z[195] + -z[196];
z[220] = z[86] * z[220];
z[220] = z[219] + z[220];
z[176] = z[78] + z[176] + -z[220];
z[176] = z[0] * z[176];
z[221] = z[82] + -z[94] + -z[143];
z[221] = z[5] * z[221];
z[222] = -z[88] + z[196];
z[223] = T(2) * z[1];
z[222] = z[222] / T(2) + -z[223];
z[222] = z[4] * z[222];
z[210] = -(z[170] * z[210]);
z[210] = z[210] + z[221] + z[222];
z[210] = z[36] * z[210];
z[222] = -z[104] + -z[145];
z[222] = z[36] * z[222];
z[213] = z[92] + -z[213];
z[213] = z[0] * z[213];
z[224] = z[145] + z[153];
z[225] = z[30] * z[224];
z[213] = z[213] + z[222] + z[225];
z[213] = z[7] * z[213];
z[178] = -z[130] + z[178];
z[222] = z[1] + -z[178] / T(2);
z[222] = z[36] * z[222];
z[225] = (T(9) * z[1]) / T(2);
z[217] = z[217] + z[225];
z[226] = z[0] / T(2);
z[217] = z[217] * z[226];
z[227] = z[30] / T(2);
z[228] = z[178] * z[227];
z[217] = z[217] + z[222] + z[228];
z[217] = z[8] * z[217];
z[222] = -z[188] + z[227];
z[228] = -z[88] + z[132];
z[229] = -z[80] + z[228];
z[222] = z[222] * z[229];
z[230] = z[168] + z[223];
z[122] = z[9] + z[122];
z[231] = -z[122] + z[230];
z[232] = z[0] * z[231];
z[233] = z[25] * z[231];
z[222] = z[222] + -z[232] + z[233];
z[222] = z[3] * z[222];
z[233] = z[88] + z[93];
z[234] = -z[80] + z[233];
z[234] = z[86] * z[234];
z[234] = z[125] + z[221] + z[234];
z[235] = z[28] * z[156];
z[235] = z[234] + z[235];
z[236] = z[30] * z[235];
z[237] = z[150] + z[184];
z[82] = z[82] + z[137];
z[238] = -z[82] + z[237];
z[239] = z[0] * z[238];
z[240] = (T(7) * z[11]) / T(4);
z[241] = z[9] + z[240];
z[110] = -z[110] + z[241];
z[242] = z[30] * z[110];
z[242] = z[239] + z[242];
z[201] = -z[130] + -z[201];
z[201] = -z[1] + z[201] / T(2);
z[201] = z[36] * z[201];
z[201] = z[201] + z[242];
z[201] = z[27] * z[201];
z[89] = z[89] + -z[241];
z[89] = z[0] * z[89];
z[241] = z[30] * z[82];
z[89] = z[89] + z[241];
z[156] = -(z[36] * z[156]);
z[156] = z[89] + z[156];
z[156] = z[6] * z[156];
z[241] = -z[9] + z[80];
z[177] = -z[177] + z[241] / T(2);
z[241] = -z[0] + z[25];
z[177] = -(z[177] * z[241]);
z[243] = z[36] * z[122];
z[244] = -(z[30] * z[122]);
z[177] = z[177] + z[243] + z[244];
z[177] = z[2] * z[177];
z[244] = -z[30] + (T(5) * z[36]) / T(2);
z[244] = z[134] * z[244];
z[126] = z[95] + z[126] + z[156] + z[169] / T(2) + z[173] + z[175] + z[176] + z[177] + z[201] + z[207] + z[210] + z[213] + z[217] + z[222] + z[236] + z[244];
z[126] = z[32] * z[126];
z[156] = (T(3) * z[71]) / T(2);
z[169] = z[13] * z[156];
z[173] = (T(3) * z[107]) / T(2);
z[175] = z[14] * z[173];
z[74] = z[15] * z[74];
z[74] = z[74] + z[108];
z[108] = z[6] * z[218];
z[73] = z[73] + z[108];
z[108] = z[4] * z[208];
z[176] = (T(13) * z[10]) / T(2);
z[177] = T(7) * z[112];
z[201] = z[1] + z[176] + -z[177];
z[201] = z[147] * z[201];
z[207] = z[168] + z[225];
z[177] = z[177] + z[207];
z[177] = z[170] * z[177];
z[208] = T(7) * z[1];
z[210] = -z[208] + -z[216];
z[210] = z[7] * z[210];
z[213] = -z[92] + z[211];
z[213] = z[118] * z[213];
z[73] = -z[73] / T(4) + (T(3) * z[74]) / T(4) + (T(3) * z[106]) / T(8) + z[108] + z[124] + -z[169] + z[175] + z[177] + z[201] + z[210] / T(8) + z[213];
z[73] = z[31] * z[73];
z[74] = z[15] * z[189];
z[106] = z[9] + (T(11) * z[11]) / T(4);
z[108] = z[106] + -z[109] + -z[130];
z[108] = z[5] * z[108];
z[108] = z[108] + z[220];
z[124] = z[27] * z[238];
z[97] = z[97] + z[98];
z[98] = z[97] + -z[104];
z[175] = -(z[98] * z[152]);
z[177] = z[9] + z[11];
z[189] = z[10] + z[177];
z[189] = z[150] + T(2) * z[189];
z[201] = -(z[8] * z[189]);
z[210] = (T(5) * z[9]) / T(2);
z[213] = z[11] + z[210];
z[216] = -(z[6] * z[213]);
z[124] = -z[102] + -z[108] + z[124] + z[175] + z[201] + z[216];
z[124] = z[25] * z[124];
z[175] = (T(7) * z[9]) / T(2);
z[201] = -z[94] + z[128] + z[175];
z[201] = z[5] * z[201];
z[216] = T(2) * z[9];
z[217] = z[216] + z[240];
z[162] = -z[88] + -z[162] + -z[217];
z[162] = z[28] * z[162];
z[218] = z[9] + z[10];
z[218] = z[1] + T(3) * z[218];
z[218] = z[4] * z[218];
z[162] = z[78] + z[162] + -z[174] + z[201] + z[218];
z[162] = z[0] * z[162];
z[182] = -z[172] + z[182];
z[182] = z[182] * z[190];
z[201] = (T(9) * z[10]) / T(4);
z[106] = z[106] + -z[150] + -z[201];
z[106] = z[28] * z[106];
z[106] = z[106] + z[234];
z[106] = z[36] * z[106];
z[142] = T(3) * z[142] + -z[223];
z[142] = z[4] * z[142];
z[217] = (T(-13) * z[10]) / T(4) + z[217] + -z[223];
z[217] = z[5] * z[217];
z[175] = -z[11] + -z[175];
z[175] = z[28] * z[175];
z[125] = z[125] + z[142] + z[175] + z[217];
z[125] = z[30] * z[125];
z[94] = -z[94] + z[150] + z[210];
z[94] = z[36] * z[94];
z[94] = z[94] + z[242];
z[94] = z[27] * z[94];
z[142] = (T(-11) * z[10]) / T(2) + z[97];
z[142] = -z[1] + z[142] / T(2);
z[142] = z[36] * z[142];
z[175] = z[9] + z[145];
z[217] = T(3) * z[175];
z[218] = -z[153] + z[217];
z[218] = z[218] * z[226];
z[176] = z[176] + -z[217];
z[176] = z[176] / T(2) + z[223];
z[176] = z[30] * z[176];
z[142] = z[142] + z[176] + z[218];
z[142] = z[7] * z[142];
z[176] = z[211] + -z[217] + z[225];
z[176] = z[176] * z[226];
z[211] = z[177] + -z[184];
z[211] = -z[150] + T(2) * z[211];
z[211] = z[36] * z[211];
z[217] = -z[130] + z[217];
z[217] = z[217] * z[227];
z[176] = z[176] + z[211] + z[217];
z[176] = z[8] * z[176];
z[211] = -(z[14] * z[183]);
z[217] = z[36] * z[82];
z[89] = z[89] + z[217];
z[89] = z[6] * z[89];
z[73] = z[73] + T(-3) * z[74] + z[89] + z[94] + z[106] + z[124] + z[125] + z[142] + z[162] + z[176] + z[182] + -z[194] + z[211];
z[73] = z[31] * z[73];
z[71] = z[13] * z[71];
z[89] = -z[88] + z[127];
z[94] = z[1] + z[89];
z[94] = z[94] * z[147];
z[106] = -z[104] + -z[127];
z[106] = z[106] * z[170];
z[124] = -z[145] + z[233];
z[125] = -(z[101] * z[124]);
z[127] = z[117] * z[204];
z[142] = z[146] * z[199];
z[162] = -z[10] + z[208];
z[162] = z[118] * z[162];
z[176] = z[1] * z[4];
z[71] = z[71] + z[94] + z[106] + -z[115] + z[125] + z[127] + z[131] + z[142] + z[162] + z[176];
z[71] = z[31] * z[71];
z[94] = z[13] * z[107];
z[106] = z[124] * z[170];
z[118] = z[118] * z[204];
z[125] = z[10] + z[129];
z[101] = z[101] * z[125];
z[117] = z[117] * z[141];
z[89] = -(z[89] * z[199]);
z[89] = z[89] + z[94] + z[101] + -z[106] + z[115] + z[117] + z[118] + -z[134] + z[148] + z[176];
z[89] = z[32] * z[89];
z[94] = -(z[25] * z[151]);
z[101] = -(z[13] * z[187]);
z[115] = z[88] + -z[175];
z[115] = z[36] * z[115];
z[117] = -(z[30] * z[124]);
z[115] = z[115] + z[117] + z[164];
z[115] = z[115] * z[152];
z[117] = -(z[36] * z[124]);
z[125] = -z[128] + z[149];
z[125] = z[0] * z[125];
z[127] = -z[145] + -z[228];
z[127] = z[30] * z[127];
z[117] = z[117] + z[125] + z[127];
z[117] = z[117] * z[155];
z[125] = -z[5] + -z[27];
z[125] = z[125] * z[132];
z[127] = z[7] + z[8];
z[127] = z[1] * z[127];
z[119] = z[119] + z[125] + z[127] + z[135];
z[119] = z[33] * z[119];
z[125] = z[4] * z[132];
z[127] = -(z[5] * z[132]);
z[121] = z[121] + -z[125] + z[127];
z[121] = z[0] * z[121];
z[127] = -z[111] + z[233];
z[128] = z[127] * z[147];
z[128] = z[128] + -z[136] + z[157];
z[128] = z[30] * z[128];
z[116] = z[116] * z[199];
z[106] = z[106] + -z[125];
z[106] = z[36] * z[106];
z[124] = z[124] * z[227];
z[129] = -(z[36] * z[132]);
z[124] = z[124] + z[129];
z[124] = z[27] * z[124];
z[71] = z[71] + z[74] + z[89] + z[94] + z[101] + z[106] + z[115] + z[116] + z[117] + z[119] / T(2) + z[121] + z[124] + z[128] + z[194];
z[71] = z[71] * z[140];
z[74] = -z[84] + z[196];
z[74] = z[74] * z[86];
z[84] = -z[10] + z[97] / T(2);
z[86] = -z[84] + z[109];
z[86] = z[5] * z[86];
z[84] = (T(7) * z[1]) / T(4) + -z[84];
z[84] = z[7] * z[84];
z[74] = z[74] + z[84] + z[86] + z[100] + -z[102] / T(2);
z[84] = -z[130] + -z[143] + -z[177];
z[84] = z[8] * z[84];
z[86] = z[1] + z[11];
z[86] = (T(3) * z[10]) / T(4) + (T(3) * z[86]) / T(8) + -z[216];
z[86] = z[6] * z[86];
z[74] = z[74] / T(2) + z[84] + z[86] + z[169];
z[74] = z[25] * z[74];
z[84] = -(z[36] * z[238]);
z[84] = z[84] + -z[239];
z[84] = z[27] * z[84];
z[86] = -z[219] + z[221];
z[89] = z[168] + z[214];
z[89] = z[4] * z[89];
z[89] = -z[86] + z[89];
z[89] = z[36] * z[89];
z[94] = z[0] * z[108];
z[97] = z[36] * z[237];
z[100] = z[0] * z[189];
z[97] = z[97] + z[100];
z[97] = z[8] * z[97];
z[98] = z[98] * z[226];
z[100] = z[36] * z[104];
z[98] = z[98] + z[100];
z[98] = z[7] * z[98];
z[100] = z[0] * z[213];
z[100] = z[100] + -z[217];
z[100] = z[6] * z[100];
z[74] = z[74] + z[84] + z[89] + z[94] + -z[95] + z[97] + z[98] + z[100] + T(-2) * z[194];
z[74] = z[25] * z[74];
z[77] = z[43] * z[77];
z[79] = z[79] + z[80];
z[80] = -z[79] + z[165];
z[80] = z[80] * z[188];
z[84] = -z[30] + z[241];
z[84] = z[84] * z[122];
z[89] = z[31] / T(4);
z[94] = z[89] * z[185];
z[80] = z[80] + z[84] + z[94];
z[80] = z[31] * z[80];
z[84] = z[93] + z[179];
z[93] = z[84] + z[111];
z[94] = prod_pow(z[36], 2);
z[95] = z[93] * z[94];
z[87] = z[87] + z[112] / T(2);
z[97] = prod_pow(z[0], 2);
z[98] = z[87] * z[97];
z[95] = -z[95] / T(2) + z[98];
z[98] = z[0] * z[122];
z[98] = z[98] + z[243];
z[100] = z[25] / T(2);
z[101] = z[100] * z[181];
z[101] = -z[98] + z[101];
z[101] = z[25] * z[101];
z[98] = z[30] * z[98];
z[102] = T(3) * z[49];
z[104] = z[87] * z[102];
z[106] = (T(3) * z[48]) / T(2);
z[108] = -(z[93] * z[106]);
z[109] = z[43] * z[181];
z[112] = z[45] * z[185];
z[115] = z[62] * z[63];
z[116] = (T(54) * z[64]) / T(5) + (T(9) * z[115]) / T(2);
z[116] = int_to_imaginary<T>(1) * z[116];
z[80] = z[80] + (T(3) * z[95]) / T(2) + z[98] + z[101] + z[104] + z[108] + z[109] + z[112] / T(2) + z[116];
z[80] = z[2] * z[80];
z[95] = T(3) * z[107];
z[98] = z[13] * z[95];
z[91] = -z[91] + z[201] + z[215];
z[91] = z[27] * z[91];
z[81] = z[81] + -z[166];
z[81] = z[1] + z[81] / T(2);
z[81] = z[4] * z[81];
z[101] = z[103] + -z[167];
z[101] = z[101] * z[170];
z[103] = z[103] + -z[166];
z[103] = z[103] * z[152];
z[99] = z[99] + z[166];
z[104] = z[99] * z[199];
z[108] = -(z[8] * z[133]);
z[81] = z[81] + z[91] + z[98] + z[101] + z[103] + z[104] + z[108] + (T(-7) * z[134]) / T(2);
z[81] = z[38] * z[81];
z[91] = z[41] * z[96];
z[96] = z[172] / T(2) + -z[186];
z[96] = z[96] * z[154];
z[98] = z[38] * z[95];
z[96] = z[96] + -z[98];
z[98] = z[49] * z[75];
z[101] = z[97] * z[156];
z[98] = z[98] + z[101];
z[101] = -z[9] + -z[137] + z[153];
z[103] = z[50] + z[56];
z[101] = z[101] * z[103];
z[104] = -z[137] + z[144];
z[108] = z[53] + z[54];
z[109] = -z[57] + z[108];
z[112] = z[104] * z[109];
z[116] = prod_pow(z[65], 3);
z[117] = T(5) * z[67] + (T(3) * z[68]) / T(8) + -z[116] / T(2);
z[101] = (T(49) * z[69]) / T(12) + -z[96] + -z[98] + z[101] + z[112] + -z[117];
z[101] = z[15] * z[101];
z[75] = z[47] * z[75];
z[112] = z[94] * z[173];
z[119] = -(z[30] * z[183]);
z[109] = z[103] + z[109];
z[121] = T(2) * z[122] + -z[230];
z[124] = -(z[109] * z[121]);
z[128] = T(3) * z[67];
z[75] = (T(13) * z[68]) / T(8) + (T(-259) * z[69]) / T(12) + z[75] + -z[96] + z[112] + (T(-13) * z[116]) / T(6) + z[119] + z[124] + z[128];
z[75] = z[29] * z[75];
z[96] = z[139] + -z[143] + -z[168];
z[119] = z[47] * z[96];
z[124] = z[94] * z[229];
z[129] = z[195] + -z[212];
z[129] = (T(3) * z[129]) / T(2);
z[131] = z[97] * z[129];
z[133] = T(5) * z[1] + z[166] + -z[180];
z[133] = z[38] * z[133];
z[119] = -z[119] + z[124] + z[131] + z[133];
z[79] = z[79] + z[168] + z[200];
z[100] = z[79] * z[100];
z[89] = -(z[89] * z[96]);
z[96] = -z[30] + z[36];
z[96] = z[96] * z[231];
z[89] = z[89] + z[96] + z[100] + -z[232];
z[89] = z[31] * z[89];
z[96] = -(z[188] * z[229]);
z[96] = z[96] + z[232];
z[96] = z[30] * z[96];
z[79] = -(z[79] * z[226]);
z[100] = -(z[36] * z[231]);
z[79] = z[79] + z[100];
z[79] = z[25] * z[79];
z[100] = z[49] * z[129];
z[84] = z[84] + -z[111];
z[106] = -(z[84] * z[106]);
z[79] = z[79] + z[89] + z[96] + z[100] + z[106] + z[119] / T(2);
z[79] = z[3] * z[79];
z[89] = -z[5] + z[28];
z[89] = z[89] * z[113];
z[89] = z[89] + z[105] + z[118] + z[120] + -z[209] / T(2);
z[89] = z[40] * z[89];
z[96] = z[111] + z[130] + -z[132];
z[96] = z[27] * z[96];
z[105] = z[28] * z[127];
z[84] = z[7] * z[84];
z[93] = z[8] * z[93];
z[84] = z[84] + z[93] + z[96] + z[105];
z[93] = -z[14] + z[15];
z[93] = -(z[93] * z[107]);
z[84] = z[84] / T(2) + z[93];
z[84] = z[48] * z[84];
z[84] = z[84] + z[89];
z[89] = z[98] + -z[112];
z[93] = -z[57] + z[103];
z[93] = z[82] * z[93];
z[93] = (T(-193) * z[69]) / T(72) + -z[89] + z[93] + -z[117];
z[93] = z[13] * z[93];
z[96] = prod_pow(z[25], 2) * z[156];
z[98] = z[109] * z[122];
z[89] = (T(-3) * z[68]) / T(4) + (T(-785) * z[69]) / T(144) + -z[89] + z[96] + z[98] + z[116] + -z[128];
z[89] = z[14] * z[89];
z[96] = z[138] + -z[159] + z[168] + -z[215];
z[96] = z[5] * z[96];
z[98] = -z[163] + -z[168] + -z[215];
z[98] = z[4] * z[98];
z[103] = z[160] + z[207];
z[103] = z[28] * z[103];
z[105] = z[171] + -z[206];
z[105] = z[6] * z[105];
z[106] = z[1] + -z[11];
z[106] = z[7] * z[106];
z[96] = z[96] + z[98] + z[103] + z[105] + z[106];
z[83] = -z[83] + -z[168] + z[171];
z[83] = z[83] * z[155];
z[78] = -z[78] + z[83] + z[96] / T(2);
z[78] = z[47] * z[78];
z[76] = -(z[76] * z[95]);
z[76] = z[76] + z[161];
z[76] = z[45] * z[76];
z[83] = -(z[36] * z[235]);
z[95] = -z[168] + z[223];
z[95] = z[4] * z[95];
z[86] = -z[86] + z[95] + z[174];
z[86] = z[0] * z[86];
z[83] = z[83] + z[86] + (T(3) * z[193]) / T(2);
z[83] = z[30] * z[83];
z[72] = (T(-5) * z[8]) / T(2) + z[72] + -z[123] + T(-2) * z[197] + -z[198] / T(2);
z[72] = z[72] * z[115];
z[86] = T(-2) * z[6] + z[90] + -z[198] + -z[202];
z[86] = (T(2) * z[3]) / T(5) + -z[8] + z[86] / T(5);
z[86] = z[64] * z[86];
z[72] = T(3) * z[72] + T(18) * z[86];
z[72] = int_to_imaginary<T>(1) * z[72];
z[86] = z[29] * z[121];
z[90] = z[15] * z[104];
z[95] = z[13] * z[82];
z[96] = z[14] * z[122];
z[86] = -z[86] + z[90] + z[95] + z[96];
z[90] = z[51] + -z[52] + z[55] + -z[58];
z[86] = z[86] * z[90];
z[90] = z[92] + z[168] + z[203];
z[90] = z[90] * z[94];
z[92] = z[143] + -z[175];
z[96] = T(3) * z[97];
z[92] = z[92] * z[96];
z[90] = z[90] + z[92];
z[92] = -(z[36] * z[224]);
z[98] = -(z[0] * z[153]);
z[103] = z[30] * z[138];
z[92] = z[92] + z[98] + z[103];
z[92] = z[30] * z[92];
z[90] = z[90] / T(4) + z[92];
z[90] = z[7] * z[90];
z[92] = z[94] * z[99];
z[98] = -(z[96] * z[146]);
z[92] = z[92] + z[98];
z[82] = -(z[0] * z[82]);
z[82] = z[82] + -z[217];
z[82] = z[30] * z[82];
z[82] = z[82] + z[92] / T(4);
z[82] = z[6] * z[82];
z[92] = z[114] * z[147];
z[87] = -(z[7] * z[87]);
z[98] = -z[153] + z[212];
z[98] = z[98] * z[199];
z[87] = z[87] + -z[92] + z[98];
z[87] = z[87] * z[102];
z[88] = z[88] + z[132] + z[158];
z[88] = z[88] * z[94];
z[98] = z[175] + -z[195];
z[96] = z[96] * z[98];
z[88] = z[88] + z[96];
z[96] = -(z[178] * z[188]);
z[98] = z[150] + -z[184];
z[98] = z[0] * z[98];
z[99] = (T(3) * z[191]) / T(2);
z[96] = z[96] + z[98] + z[99];
z[96] = z[30] * z[96];
z[88] = z[88] / T(4) + z[96] + -z[100];
z[88] = z[8] * z[88];
z[96] = -z[166] + -z[196];
z[96] = -z[1] + z[96] / T(4);
z[96] = z[4] * z[96];
z[98] = z[11] * z[28];
z[96] = z[96] + (T(5) * z[98]) / T(4) + (T(-3) * z[192]) / T(2);
z[96] = z[94] * z[96];
z[98] = -(z[36] * z[110]);
z[98] = z[98] + z[99] + -z[239];
z[98] = z[30] * z[98];
z[99] = z[168] + z[210];
z[99] = z[99] / T(2) + z[223];
z[99] = z[94] * z[99];
z[98] = z[98] + z[99];
z[98] = z[27] * z[98];
z[95] = z[95] * z[108];
z[92] = -z[92] + -z[125];
z[92] = z[92] * z[97];
z[97] = z[30] * z[36];
z[94] = -z[94] / T(4) + z[97];
z[94] = z[94] * z[134];
z[85] = z[13] / T(3) + (T(-5) * z[15]) / T(12) + z[29] + z[85];
z[85] = z[85] * z[205];
z[97] = -z[13] + (T(-5) * z[15]) / T(2) + z[190];
z[97] = z[66] * z[97];
z[85] = z[85] + z[97];
z[85] = z[62] * z[85];
return z[70] + z[71] + z[72] + z[73] + z[74] + z[75] + z[76] + z[77] + z[78] + z[79] + z[80] + z[81] + z[82] + z[83] + T(3) * z[84] + z[85] + z[86] + z[87] + z[88] + z[89] + z[90] + z[91] + (T(3) * z[92]) / T(2) + z[93] + z[94] + z[95] + z[96] + z[98] + z[101] + z[126];
}



template IntegrandConstructorType<double> f_4_377_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_377_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_377_construct (const Kin<qd_real>&);
#endif

}