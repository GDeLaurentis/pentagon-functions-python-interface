#include "f_4_267.h"

namespace PentagonFunctions {

template <typename T> T f_4_267_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_267_W_7 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_267_W_7 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[1]) * ((-kin.v[3] / T(4) + -kin.v[4] / T(2) + T(1) + kin.v[1]) * kin.v[3] + (-kin.v[4] / T(4) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((-kin.v[3] / T(4) + -kin.v[4] / T(2) + T(1) + kin.v[1]) * kin.v[3] + (-kin.v[4] / T(4) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (-(kin.v[1] * kin.v[4]) + (kin.v[3] / T(4) + kin.v[4] / T(2) + -kin.v[1] + T(-1)) * kin.v[3] + (kin.v[4] / T(4) + T(-1)) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (-(kin.v[1] * kin.v[4]) + (kin.v[3] / T(4) + kin.v[4] / T(2) + -kin.v[1] + T(-1)) * kin.v[3] + (kin.v[4] / T(4) + T(-1)) * kin.v[4]);
c[1] = (-kin.v[3] + -kin.v[4]) * rlog(-kin.v[1]) + (-kin.v[3] + -kin.v[4]) * rlog(-kin.v[2] + kin.v[0] + kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[3] + kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[3] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[4] * (abb[3] + -prod_pow(abb[2], 2) / T(2)) + prod_pow(abb[2], 2) * (abb[5] / T(2) + abb[6] / T(2) + -abb[7] / T(2)) + abb[3] * (abb[7] + -abb[5] + -abb[6]) + abb[1] * (abb[1] * ((abb[5] * T(-3)) / T(2) + (abb[6] * T(-3)) / T(2) + (abb[4] * T(3)) / T(2) + (abb[7] * T(3)) / T(2)) + -(abb[2] * abb[4]) + abb[2] * (abb[5] + abb[6] + -abb[7])));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_267_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl25 = DLog_W_25<T>(kin),dl2 = DLog_W_2<T>(kin),dl20 = DLog_W_20<T>(kin),dl18 = DLog_W_18<T>(kin),dl19 = DLog_W_19<T>(kin),spdl7 = SpDLog_f_4_267_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,17> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl25(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl2(t), -rlog(t), dl20(t), dl18(t), dl19(t)}
;

        auto result = f_4_267_abbreviated(abbr);
        result = result + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_267_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[45];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[12];
z[3] = abb[14];
z[4] = abb[15];
z[5] = abb[16];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[13];
z[10] = abb[2];
z[11] = abb[9];
z[12] = bc<TR>[0];
z[13] = abb[8];
z[14] = abb[3];
z[15] = abb[10];
z[16] = abb[11];
z[17] = bc<TR>[2];
z[18] = bc<TR>[9];
z[19] = int_to_imaginary<T>(1) * z[12];
z[20] = z[16] * z[19];
z[21] = T(3) * z[20];
z[22] = prod_pow(z[11], 2);
z[23] = z[22] / T(2);
z[24] = z[11] * z[19];
z[24] = T(2) * z[24];
z[25] = prod_pow(z[12], 2);
z[26] = z[25] / T(3);
z[27] = T(-3) * z[15] + z[21] + -z[23] + -z[24] + -z[26];
z[28] = -z[11] + z[19];
z[29] = T(2) * z[28];
z[30] = -(z[0] * z[29]);
z[30] = z[27] + z[30];
z[30] = z[2] * z[30];
z[31] = z[0] / T(2);
z[32] = z[28] + z[31];
z[32] = z[0] * z[32];
z[32] = z[14] + z[32];
z[20] = -z[15] + z[20];
z[33] = -z[20] + z[23] + -z[25] / T(6) + z[32];
z[34] = -(z[5] * z[33]);
z[35] = z[10] / T(2);
z[36] = -z[2] + -z[4];
z[36] = z[35] * z[36];
z[37] = z[0] + z[29];
z[38] = z[4] * z[37];
z[39] = T(-2) * z[0] + -z[28];
z[39] = z[2] * z[39];
z[36] = z[36] + z[38] + z[39];
z[36] = z[10] * z[36];
z[38] = -z[20] + (T(3) * z[22]) / T(2) + -z[24];
z[39] = -z[28] + z[35];
z[39] = z[10] * z[39];
z[39] = (T(4) * z[25]) / T(3) + -z[38] + z[39];
z[40] = z[13] * z[39];
z[41] = z[29] + z[31];
z[41] = z[0] * z[41];
z[42] = -z[26] + -z[41];
z[42] = z[4] * z[42];
z[43] = z[4] * z[14];
z[30] = z[30] + z[34] + z[36] + z[40] + z[42] + T(-3) * z[43];
z[30] = z[7] * z[30];
z[34] = z[0] * z[37];
z[36] = T(2) * z[14];
z[34] = z[34] + z[36];
z[20] = -z[20] + -z[23] + z[24] + z[25] + z[34];
z[20] = z[8] * z[20];
z[24] = T(2) * z[16];
z[24] = z[19] * z[24];
z[22] = T(2) * z[15] + z[22] + -z[24];
z[24] = prod_pow(z[0], 2);
z[24] = -z[22] + z[24] + (T(2) * z[25]) / T(3) + z[36];
z[36] = z[1] + z[6];
z[37] = z[24] * z[36];
z[19] = z[17] * z[19];
z[19] = z[19] + (T(5) * z[25]) / T(2);
z[40] = z[17] / T(2);
z[19] = z[19] * z[40];
z[40] = int_to_imaginary<T>(1) * prod_pow(z[12], 3);
z[40] = z[40] / T(3);
z[42] = (T(21) * z[18]) / T(4) + z[40];
z[42] = -z[19] + z[42] / T(2);
z[20] = z[20] + z[37] + -z[42];
z[20] = z[2] * z[20];
z[33] = z[8] * z[33];
z[37] = (T(-3) * z[0]) / T(2) + -z[29];
z[37] = z[0] * z[37];
z[27] = z[27] + z[37];
z[27] = z[1] * z[27];
z[37] = z[25] + -z[38] + -z[41];
z[37] = z[6] * z[37];
z[38] = z[18] / T(2) + z[40];
z[19] = -z[19] + z[38] / T(4);
z[38] = -z[1] + T(-3) * z[6];
z[38] = z[14] * z[38];
z[27] = -z[19] + z[27] + -z[33] + z[37] + z[38];
z[27] = z[5] * z[27];
z[37] = z[0] + z[28];
z[38] = z[1] * z[37];
z[40] = z[6] * z[37];
z[41] = z[38] + z[40];
z[44] = z[4] * z[41];
z[28] = -(z[8] * z[28]);
z[28] = z[28] + T(-2) * z[41];
z[28] = z[2] * z[28];
z[38] = -z[38] + z[40];
z[38] = z[5] * z[38];
z[40] = z[4] * z[8];
z[41] = z[2] * z[8];
z[41] = z[40] + z[41];
z[35] = z[35] * z[41];
z[40] = z[0] * z[40];
z[28] = z[28] + z[35] + z[38] + z[40] + T(2) * z[44];
z[28] = z[10] * z[28];
z[35] = z[4] * z[24];
z[24] = -(z[2] * z[24]);
z[22] = z[22] + -z[26];
z[26] = z[22] + z[34];
z[34] = z[5] * z[26];
z[38] = z[2] + -z[4];
z[37] = z[10] * z[37] * z[38];
z[24] = z[24] + z[34] + z[35] + T(2) * z[37];
z[34] = T(2) * z[9];
z[24] = z[24] * z[34];
z[34] = -z[34] + z[36];
z[26] = z[26] * z[34];
z[23] = z[15] + z[23] + z[32];
z[21] = -z[21] + T(3) * z[23] + -z[25] / T(2);
z[21] = z[7] * z[21];
z[19] = z[19] + z[21] + z[26] + -z[33];
z[19] = z[3] * z[19];
z[21] = -(z[35] * z[36]);
z[23] = z[29] + -z[31];
z[23] = z[0] * z[23];
z[22] = z[22] + z[23];
z[22] = z[4] * z[22];
z[22] = z[22] + z[43];
z[22] = z[8] * z[22];
z[23] = -z[1] + z[6] + z[8];
z[23] = -(z[13] * z[23] * z[39]);
z[25] = z[4] * z[42];
return z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[25] + z[27] + z[28] + z[30];
}



template IntegrandConstructorType<double> f_4_267_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_267_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_267_construct (const Kin<qd_real>&);
#endif

}