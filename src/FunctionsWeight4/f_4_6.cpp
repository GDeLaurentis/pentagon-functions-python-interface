#include "f_4_6.h"

namespace PentagonFunctions {

template <typename T> T f_4_6_abbreviated (const std::array<T,17>&);



template <typename T> IntegrandConstructorType<T> f_4_6_construct (const Kin<T>& kin) {
    return [&kin, 
            dl11 = DLog_W_11<T>(kin),dl6 = DLog_W_6<T>(kin),dl13 = DLog_W_13<T>(kin),dl3 = DLog_W_3<T>(kin),dl18 = DLog_W_18<T>(kin),dl4 = DLog_W_4<T>(kin),dl1 = DLog_W_1<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,17> abbr = 
            {dl11(t), rlog(kin.W[2] / kin_path.W[2]), rlog(v_path[0]), rlog(v_path[3]), f_2_1_2(kin_path), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[17] / kin_path.W[17]), dl6(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_9(kin_path), dl13(t), dl3(t), rlog(kin.W[12] / kin_path.W[12]), dl18(t), dl4(t), dl1(t)}
;

        auto result = f_4_6_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_6_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[50];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[12];
z[11] = abb[14];
z[12] = abb[15];
z[13] = abb[16];
z[14] = abb[9];
z[15] = abb[10];
z[16] = abb[13];
z[17] = abb[11];
z[18] = bc<TR>[1];
z[19] = bc<TR>[2];
z[20] = bc<TR>[4];
z[21] = bc<TR>[7];
z[22] = bc<TR>[8];
z[23] = bc<TR>[9];
z[24] = T(3) * z[10];
z[25] = -z[13] + z[24];
z[25] = z[7] * z[25];
z[26] = z[10] + z[13];
z[27] = z[1] * z[26];
z[25] = z[25] + -z[27];
z[28] = z[10] + -z[13];
z[29] = z[16] * z[28];
z[29] = -z[25] + T(4) * z[29];
z[30] = -z[6] + z[8];
z[31] = z[1] + -z[7];
z[32] = z[30] + z[31];
z[32] = z[9] * z[32];
z[33] = -z[6] + -z[8] + T(2) * z[16];
z[34] = -z[1] + z[33];
z[35] = T(2) * z[11];
z[36] = z[34] * z[35];
z[37] = T(2) * z[10];
z[38] = T(3) * z[13] + -z[37];
z[39] = z[6] * z[38];
z[37] = -z[13] + z[37];
z[40] = -(z[8] * z[37]);
z[41] = z[1] + T(-3) * z[7] + T(2) * z[33];
z[42] = -(z[12] * z[41]);
z[36] = z[29] + -z[32] + z[36] + z[39] + z[40] + z[42];
z[36] = z[15] * z[36];
z[39] = z[30] + -z[31];
z[39] = z[0] * z[39];
z[40] = z[11] * z[31];
z[42] = z[39] + z[40];
z[43] = z[12] * z[31];
z[44] = z[32] + -z[43];
z[45] = -z[42] + z[44];
z[45] = z[2] * z[45];
z[30] = -(z[13] * z[30]);
z[42] = z[30] + z[42];
z[42] = z[4] * z[42];
z[46] = prod_pow(z[18], 2);
z[47] = T(2) * z[20] + z[46];
z[47] = z[28] * z[47];
z[30] = z[30] + z[44];
z[44] = -(z[14] * z[30]);
z[44] = z[42] + z[44] + z[45] + z[47];
z[44] = int_to_imaginary<T>(1) * z[44];
z[40] = z[40] + z[43];
z[33] = -(z[28] * z[33]);
z[35] = T(2) * z[12] + z[35];
z[43] = T(4) * z[17] + -z[26] + -z[35];
z[45] = z[19] * z[43];
z[47] = z[11] + z[12];
z[48] = T(2) * z[17];
z[49] = -z[47] + z[48];
z[26] = -z[26] / T(2) + z[49];
z[26] = z[18] * z[26];
z[28] = int_to_imaginary<T>(1) * z[3] * z[28];
z[26] = z[26] + -z[28] + z[33] + z[40] + -z[45];
z[28] = -z[10] + z[13] / T(3);
z[45] = z[7] * z[28];
z[26] = z[26] / T(3) + -z[27] / T(6) + -z[45] / T(2);
z[26] = z[3] * z[26];
z[26] = z[26] + T(2) * z[44];
z[26] = z[3] * z[26];
z[25] = z[25] + T(2) * z[33];
z[27] = -z[25] + -z[32] + z[39] + -z[40];
z[27] = z[2] * z[27];
z[31] = z[31] * z[35];
z[25] = z[25] + z[31];
z[31] = z[4] + z[14];
z[31] = z[25] * z[31];
z[27] = z[27] + z[31];
z[27] = z[2] * z[27];
z[31] = -(z[6] * z[37]);
z[32] = z[8] * z[38];
z[29] = z[29] + z[31] + z[32] + z[39];
z[29] = z[5] * z[29];
z[25] = -(z[14] * z[25]);
z[25] = z[25] + -z[42];
z[25] = z[4] * z[25];
z[31] = -z[10] + z[49];
z[31] = z[20] * z[31];
z[28] = (T(4) * z[17]) / T(3) + z[28] + (T(-2) * z[47]) / T(3);
z[28] = z[28] * z[46];
z[28] = z[28] + T(4) * z[31];
z[28] = z[18] * z[28];
z[31] = prod_pow(z[19], 3);
z[31] = z[22] + (T(-4) * z[31]) / T(3);
z[31] = z[31] * z[43];
z[32] = T(8) * z[21] + (T(-49) * z[23]) / T(3);
z[33] = -(z[5] * z[41]);
z[33] = -z[32] + z[33];
z[33] = z[11] * z[33];
z[35] = z[32] * z[48];
z[34] = z[5] * z[34];
z[32] = -z[32] + T(2) * z[34];
z[32] = z[12] * z[32];
z[30] = prod_pow(z[14], 2) * z[30];
z[24] = -z[13] + -z[24];
z[24] = z[21] * z[24];
z[34] = (T(41) * z[10]) / T(3) + T(19) * z[13];
z[34] = z[23] * z[34];
return T(2) * z[24] + z[25] + z[26] + z[27] + z[28] + z[29] + z[30] + z[31] + z[32] + z[33] + z[34] / T(2) + z[35] + z[36];
}



template IntegrandConstructorType<double> f_4_6_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_6_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_6_construct (const Kin<qd_real>&);
#endif

}