#include "f_4_102.h"

namespace PentagonFunctions {

template <typename T> T f_4_102_abbreviated (const std::array<T,39>&);

template <typename T> class SpDLog_f_4_102_W_23 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_102_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + ((T(3) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + ((T(-9) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + kin.v[0] * (-kin.v[4] / T(2) + (T(7) * kin.v[2]) / T(2) + (T(13) * kin.v[3]) / T(2) + T(9) + (T(-17) / T(4) + (bc<T>[1] * T(-3)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[0] + T(-6) * kin.v[1] + bc<T>[1] * (T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3])) + ((T(15) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4])) + bc<T>[1] * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(3) + bc<T>[1] * T(3) + T(9)) * kin.v[0] + T(-9) * kin.v[2] + T(-9) * kin.v[3] + T(9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]);
c[2] = kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + ((T(3) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + ((T(-9) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + kin.v[0] * (-kin.v[4] / T(2) + (T(7) * kin.v[2]) / T(2) + (T(13) * kin.v[3]) / T(2) + T(9) + (T(-17) / T(4) + (bc<T>[1] * T(-3)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[0] + T(-6) * kin.v[1] + bc<T>[1] * (T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3])) + ((T(15) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4])) + bc<T>[1] * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]));
c[3] = (bc<T>[0] * int_to_imaginary<T>(3) + bc<T>[1] * T(3) + T(9)) * kin.v[0] + T(-9) * kin.v[2] + T(-9) * kin.v[3] + T(9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]);
c[4] = kin.v[0] * ((T(-7) * kin.v[2]) / T(2) + (T(-13) * kin.v[3]) / T(2) + kin.v[4] / T(2) + T(-9) + ((bc<T>[1] * T(3)) / T(2) + T(17) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[0] + T(6) * kin.v[1] + bc<T>[1] * (T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3])) + ((T(-15) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + ((T(9) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + ((T(-3) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4])) + bc<T>[1] * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]));
c[5] = (T(-9) + bc<T>[1] * T(-3) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + T(9) * kin.v[2] + T(9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + bc<T>[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + T(-9) * kin.v[4];
c[6] = kin.v[0] * ((T(-7) * kin.v[2]) / T(2) + (T(-13) * kin.v[3]) / T(2) + kin.v[4] / T(2) + T(-9) + ((bc<T>[1] * T(3)) / T(2) + T(17) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[0] + T(6) * kin.v[1] + bc<T>[1] * (T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3])) + ((T(-15) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + ((T(9) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + ((T(-3) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4])) + bc<T>[1] * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]));
c[7] = (T(-9) + bc<T>[1] * T(-3) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + T(9) * kin.v[2] + T(9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + bc<T>[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + T(-9) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[5] * (t * c[0] + c[1]) + abb[6] * (t * c[2] + c[3]) + abb[10] * (t * c[4] + c[5]) + abb[15] * (t * c[6] + c[7]);
        }

        return abb[11] * (abb[2] * abb[13] * (abb[10] * T(-3) + abb[15] * T(-3) + abb[6] * T(3)) + abb[13] * (abb[13] * ((abb[10] * T(-3)) / T(2) + (abb[15] * T(-3)) / T(2) + (abb[6] * T(3)) / T(2)) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(3) + abb[15] * bc<T>[0] * int_to_imaginary<T>(3) + abb[3] * (abb[10] * T(-3) + abb[15] * T(-3) + abb[6] * T(3))) + abb[1] * (abb[5] * abb[13] * T(-3) + abb[12] * (abb[10] * T(-3) + abb[15] * T(-3) + abb[5] * T(3) + abb[6] * T(3)) + abb[13] * (abb[6] * T(-3) + abb[10] * T(3) + abb[15] * T(3))) + abb[12] * (abb[12] * ((abb[10] * T(-9)) / T(2) + (abb[15] * T(-9)) / T(2) + (abb[5] * T(9)) / T(2) + (abb[6] * T(9)) / T(2)) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[15] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[6] * bc<T>[0] * int_to_imaginary<T>(3) + abb[5] * (abb[13] * T(-6) + abb[2] * T(-3) + abb[3] * T(-3) + bc<T>[0] * int_to_imaginary<T>(3)) + abb[2] * (abb[6] * T(-3) + abb[10] * T(3) + abb[15] * T(3)) + abb[3] * (abb[6] * T(-3) + abb[10] * T(3) + abb[15] * T(3)) + abb[13] * (abb[6] * T(-6) + abb[10] * T(6) + abb[15] * T(6))) + abb[14] * (abb[10] * T(-9) + abb[15] * T(-9) + abb[6] * T(9)) + abb[5] * (abb[2] * abb[13] * T(3) + abb[13] * ((abb[13] * T(3)) / T(2) + bc<T>[0] * int_to_imaginary<T>(-3) + abb[3] * T(3)) + abb[14] * T(9)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_102_construct (const Kin<T>& kin) {
    return [&kin, 
            dl6 = DLog_W_6<T>(kin),dl3 = DLog_W_3<T>(kin),dl23 = DLog_W_23<T>(kin),dl4 = DLog_W_4<T>(kin),dl18 = DLog_W_18<T>(kin),dl16 = DLog_W_16<T>(kin),dl24 = DLog_W_24<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl20 = DLog_W_20<T>(kin),dl5 = DLog_W_5<T>(kin),dl17 = DLog_W_17<T>(kin),dl19 = DLog_W_19<T>(kin),dl30 = DLog_W_30<T>(kin),dl29 = DLog_W_29<T>(kin),dl26 = DLog_W_26<T>(kin),dl28 = DLog_W_28<T>(kin),dl27 = DLog_W_27<T>(kin),spdl23 = SpDLog_f_4_102_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,39> abbr = 
            {dl6(t), rlog(v_path[0]), rlog(v_path[3]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_9(kin_path), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl3(t), f_2_1_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_8(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl4(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl18(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl16(t), dl24(t), dl2(t), dl1(t), dl20(t), dl5(t), dl17(t), dl19(t), dl30(t) / kin_path.SqrtDelta, rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[18] / kin_path.W[18]), dl29(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta}
;

        auto result = f_4_102_abbreviated(abbr);
        result = result + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_102_abbreviated(const std::array<T,39>& abb)
{
using TR = typename T::value_type;
T z[95];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[5];
z[3] = abb[6];
z[4] = abb[7];
z[5] = abb[2];
z[6] = abb[3];
z[7] = abb[4];
z[8] = abb[8];
z[9] = abb[22];
z[10] = abb[24];
z[11] = abb[25];
z[12] = abb[28];
z[13] = abb[29];
z[14] = abb[10];
z[15] = abb[30];
z[16] = abb[31];
z[17] = abb[32];
z[18] = abb[33];
z[19] = abb[34];
z[20] = abb[36];
z[21] = abb[37];
z[22] = abb[12];
z[23] = abb[16];
z[24] = abb[19];
z[25] = abb[13];
z[26] = bc<TR>[0];
z[27] = abb[15];
z[28] = abb[38];
z[29] = abb[26];
z[30] = abb[35];
z[31] = abb[23];
z[32] = abb[27];
z[33] = abb[9];
z[34] = abb[14];
z[35] = abb[17];
z[36] = abb[18];
z[37] = abb[20];
z[38] = abb[21];
z[39] = bc<TR>[1];
z[40] = bc<TR>[3];
z[41] = bc<TR>[5];
z[42] = bc<TR>[2];
z[43] = bc<TR>[9];
z[44] = bc<TR>[4];
z[45] = bc<TR>[7];
z[46] = bc<TR>[8];
z[47] = z[16] + z[17] + -z[18] + -z[19];
z[48] = z[47] / T(2);
z[49] = z[21] * z[48];
z[50] = z[4] + z[14];
z[51] = -z[2] + z[50];
z[52] = z[51] / T(2);
z[53] = z[13] * z[52];
z[54] = z[49] + -z[53];
z[55] = z[2] + z[50];
z[56] = z[8] * z[55];
z[57] = z[56] / T(2);
z[58] = z[54] + z[57];
z[59] = z[2] + -z[14];
z[60] = z[4] + z[59];
z[60] = z[60] / T(2);
z[61] = z[3] + z[60];
z[62] = z[23] * z[61];
z[63] = z[28] * z[48];
z[62] = z[62] + z[63];
z[64] = z[20] / T(2);
z[65] = z[47] * z[64];
z[66] = z[24] * z[55];
z[67] = z[2] + z[4];
z[68] = z[3] + z[67];
z[68] = z[0] * z[68];
z[69] = z[12] * z[52];
z[61] = z[11] * z[61];
z[70] = z[58] + z[61] + z[62] + z[65] + z[66] / T(2) + T(-2) * z[68] + z[69];
z[70] = z[7] * z[70];
z[71] = T(3) * z[14];
z[72] = z[2] + -z[4];
z[73] = z[71] + -z[72];
z[74] = -z[3] + z[27];
z[73] = z[73] / T(2) + z[74];
z[73] = z[23] * z[73];
z[75] = -z[59] + z[74];
z[75] = z[11] * z[75];
z[50] = T(3) * z[2] + -z[50];
z[76] = z[24] * z[50];
z[77] = z[9] * z[50];
z[76] = z[76] + z[77];
z[63] = -z[63] + z[76] / T(2);
z[76] = z[14] + z[72];
z[76] = z[74] + z[76] / T(2);
z[76] = z[32] * z[76];
z[73] = -z[63] + z[65] + z[73] + -z[75] + z[76];
z[48] = z[30] * z[48];
z[75] = z[29] * z[55];
z[48] = z[48] + z[75] / T(2);
z[78] = z[15] / T(2);
z[78] = z[47] * z[78];
z[79] = z[48] + z[78];
z[67] = -z[67] + z[71];
z[71] = z[67] / T(2) + T(2) * z[74];
z[71] = z[12] * z[71];
z[71] = z[71] + z[73] + -z[79];
z[71] = z[34] * z[71];
z[80] = z[30] * z[47];
z[75] = z[75] + z[80];
z[67] = z[67] / T(4) + z[74];
z[67] = z[12] * z[67];
z[80] = z[15] * z[47];
z[81] = z[80] / T(4);
z[67] = z[67] + z[73] / T(2) + -z[75] / T(4) + -z[81];
z[67] = z[22] * z[67];
z[73] = z[23] * z[51];
z[82] = z[20] * z[47];
z[83] = z[73] + z[82];
z[84] = z[10] * z[51];
z[85] = z[83] + -z[84];
z[86] = z[24] * z[51];
z[87] = z[28] * z[47];
z[86] = z[86] + z[87];
z[87] = z[85] + z[86];
z[60] = z[60] + -z[74];
z[88] = z[12] * z[60];
z[87] = z[87] / T(2) + -z[88];
z[89] = z[1] * z[87];
z[67] = z[67] + z[89];
z[67] = z[22] * z[67];
z[55] = z[12] * z[55];
z[89] = z[11] * z[51];
z[90] = z[9] * z[51];
z[55] = z[55] + -z[75] + -z[80] + -z[85] + -z[89] + z[90];
z[75] = z[5] * z[55];
z[65] = z[65] + z[78];
z[69] = z[65] + z[69];
z[85] = z[56] + -z[90];
z[68] = -z[61] + z[68];
z[85] = -z[68] + z[69] + z[85] / T(2);
z[91] = -z[1] + z[6];
z[91] = z[85] * z[91];
z[87] = -(z[22] * z[87]);
z[92] = z[14] + z[74];
z[93] = z[12] * z[92];
z[79] = -z[79] + z[93];
z[86] = z[86] + z[90];
z[52] = z[11] * z[52];
z[93] = -z[52] + z[79] + z[86] / T(2);
z[94] = z[25] * z[93];
z[75] = -z[75] / T(4) + z[87] + z[91] + z[94];
z[75] = z[5] * z[75];
z[87] = z[84] + -z[90];
z[66] = -z[66] + z[87];
z[57] = z[57] + z[61] + -z[62] + z[66] / T(2) + z[78];
z[57] = z[33] * z[57];
z[61] = z[72] + z[74];
z[61] = z[31] * z[61];
z[62] = -z[61] + z[76];
z[52] = -z[52] + z[62] + z[83] / T(2) + -z[88];
z[66] = z[22] * z[52];
z[72] = -(z[1] * z[85]);
z[72] = -z[66] + z[72];
z[72] = z[6] * z[72];
z[83] = z[12] * z[51];
z[82] = z[82] + z[83];
z[47] = z[21] * z[47];
z[51] = z[13] * z[51];
z[80] = z[47] + -z[51] + -z[80] + -z[82];
z[83] = z[80] + z[86] + z[89];
z[62] = -z[62] + z[83] / T(2);
z[83] = z[37] * z[62];
z[53] = -z[53] + z[56] + z[87] / T(2);
z[47] = z[47] + z[82];
z[47] = z[47] / T(4) + z[53] / T(2) + -z[68] + z[81];
z[47] = prod_pow(z[1], 2) * z[47];
z[53] = (T(59) * z[43]) / T(8) + T(-4) * z[45];
z[53] = z[28] * z[53];
z[47] = z[47] + z[53] + z[57] + z[67] + z[70] + z[71] + z[72] + z[75] + z[83];
z[53] = T(3) * z[4];
z[56] = z[2] + z[14] + z[53];
z[56] = z[56] / T(2) + -z[74];
z[56] = z[12] * z[56];
z[57] = z[30] / T(2) + -z[64];
z[64] = z[21] / T(2);
z[67] = -z[15] + z[57] + z[64];
z[67] = z[39] * z[67];
z[50] = z[27] + z[50] / T(2);
z[50] = z[23] * z[50];
z[68] = -(z[11] * z[27]);
z[70] = z[15] + z[20];
z[71] = -z[21] + z[70];
z[72] = -z[28] + T(3) * z[71];
z[72] = -z[30] + z[72] / T(2);
z[72] = z[42] * z[72];
z[48] = -z[48] + z[50] + z[56] + -z[58] + z[61] + -z[65] + z[67] + z[68] + z[72] + -z[77] / T(2);
z[48] = z[26] * z[48];
z[50] = -z[12] + -z[13] + z[29] + z[32];
z[56] = -(z[40] * z[50]);
z[48] = z[48] / T(2) + z[56];
z[48] = z[26] * z[48];
z[58] = z[23] * z[92];
z[58] = z[58] + -z[63];
z[53] = z[53] + -z[59];
z[53] = z[53] / T(2) + -z[74];
z[53] = z[11] * z[53];
z[53] = z[53] + z[54] + z[58] + -z[69] + -z[76];
z[53] = z[53] / T(2) + z[61];
z[53] = z[25] * z[53];
z[54] = -(z[11] * z[60]);
z[54] = z[54] + -z[58] + -z[61] + -z[79];
z[54] = z[22] * z[54];
z[51] = z[51] + -z[73] + -z[86];
z[58] = z[4] + -z[74];
z[58] = z[12] * z[58];
z[49] = -z[49] + z[51] / T(2) + z[58] + z[78];
z[49] = z[1] * z[49];
z[51] = z[6] * z[52];
z[49] = z[49] + z[51] + z[53] + z[54];
z[51] = T(3) * z[25];
z[49] = z[49] * z[51];
z[52] = -z[80] + -z[84] + -z[90];
z[52] = z[1] * z[52];
z[53] = prod_pow(z[39], 2);
z[54] = -(z[50] * z[53]);
z[58] = -z[28] + z[71];
z[58] = prod_pow(z[42], 2) * z[58];
z[52] = z[52] + z[54] / T(10) + z[58];
z[54] = z[38] * z[62];
z[52] = z[52] / T(2) + z[54] + z[66];
z[54] = (T(3) * z[55]) / T(2);
z[55] = z[36] * z[54];
z[58] = -z[28] + z[30];
z[59] = (T(-11) * z[50]) / T(45) + -z[58] + -z[70];
z[59] = prod_pow(z[26], 2) * z[59];
z[51] = -(z[51] * z[93]);
z[51] = z[51] + T(3) * z[52] + z[55] + z[59] / T(4);
z[51] = z[26] * z[51];
z[50] = z[41] * z[50];
z[52] = -(z[39] * z[56]);
z[50] = (T(12) * z[50]) / T(5) + z[52];
z[50] = T(3) * z[50] + z[51];
z[50] = int_to_imaginary<T>(1) * z[50];
z[51] = -(z[35] * z[54]);
z[52] = prod_pow(z[42], 3);
z[52] = (T(3) * z[46]) / T(2) + T(-2) * z[52];
z[52] = z[52] * z[58];
z[54] = -z[28] + z[57] + -z[64];
z[53] = z[53] * z[54];
z[54] = -z[20] + -z[21] + T(-2) * z[28] + z[30];
z[54] = z[44] * z[54];
z[53] = z[53] + T(3) * z[54];
z[53] = z[39] * z[53];
z[54] = T(6) * z[45];
z[55] = (T(-47) * z[43]) / T(8) + -z[54];
z[55] = z[20] * z[55];
z[56] = z[15] * z[43];
z[57] = (T(43) * z[43]) / T(8) + -z[54];
z[57] = z[21] * z[57];
z[54] = (T(-67) * z[43]) / T(4) + z[54];
z[54] = z[30] * z[54];
return T(3) * z[47] + z[48] + z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + (T(-87) * z[56]) / T(8) + z[57];
}



template IntegrandConstructorType<double> f_4_102_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_102_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_102_construct (const Kin<qd_real>&);
#endif

}