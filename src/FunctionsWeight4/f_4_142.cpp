#include "f_4_142.h"

namespace PentagonFunctions {

template <typename T> T f_4_142_abbreviated (const std::array<T,45>&);

template <typename T> class SpDLog_f_4_142_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_142_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-12) + T(12) * kin.v[0] + T(6) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[0] * (T(12) * kin.v[2] + T(-12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(-6) * kin.v[4]) + kin.v[2] * (T(-12) + T(-6) * kin.v[2] + T(12) * kin.v[4]);
c[1] = kin.v[1] * (T(-12) + T(12) * kin.v[0] + T(6) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[0] * (T(12) * kin.v[2] + T(-12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(-6) * kin.v[4]) + kin.v[2] * (T(-12) + T(-6) * kin.v[2] + T(12) * kin.v[4]);
c[2] = kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[1] * (T(12) + T(-12) * kin.v[0] + T(-6) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[0] * (T(-12) * kin.v[2] + T(12) * kin.v[3] + T(12) * kin.v[4]);
c[3] = kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[1] * (T(12) + T(-12) * kin.v[0] + T(-6) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[0] * (T(-12) * kin.v[2] + T(12) * kin.v[3] + T(12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[6] * c[1] + abb[4] * c[2] + abb[5] * c[3]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-6) + prod_pow(abb[2], 2) * (abb[6] * T(-6) + abb[4] * T(6) + abb[5] * T(6)) + prod_pow(abb[1], 2) * (abb[4] * T(-6) + abb[5] * T(-6) + abb[3] * T(6) + abb[6] * T(6)));
    }
};
template <typename T> class SpDLog_f_4_142_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_142_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[3] * (T(12) + T(-6) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[0] * (T(-12) + T(6) * kin.v[0] + T(-12) * kin.v[2] + T(12) * kin.v[4]) + kin.v[1] * (T(-12) + T(-6) * kin.v[1] + T(12) * kin.v[3] + T(12) * kin.v[4]);
c[1] = kin.v[2] * (T(12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[3] * (T(12) + T(-6) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[0] * (T(-12) + T(6) * kin.v[0] + T(-12) * kin.v[2] + T(12) * kin.v[4]) + kin.v[1] * (T(-12) + T(-6) * kin.v[1] + T(12) * kin.v[3] + T(12) * kin.v[4]);
c[2] = kin.v[0] * (T(12) + T(-6) * kin.v[0] + T(12) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(12) + T(6) * kin.v[1] + T(-12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[2] * (T(-12) + T(-6) * kin.v[2] + T(12) * kin.v[4]) + kin.v[3] * (T(-12) + T(6) * kin.v[3] + T(12) * kin.v[4]);
c[3] = kin.v[0] * (T(12) + T(-6) * kin.v[0] + T(12) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(12) + T(6) * kin.v[1] + T(-12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[2] * (T(-12) + T(-6) * kin.v[2] + T(12) * kin.v[4]) + kin.v[3] * (T(-12) + T(6) * kin.v[3] + T(12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[9] * c[0] + abb[4] * c[1] + abb[5] * c[2] + abb[6] * c[3]);
        }

        return abb[7] * (prod_pow(abb[2], 2) * abb[9] * T(6) + prod_pow(abb[2], 2) * (abb[5] * T(-6) + abb[6] * T(-6) + abb[4] * T(6)) + prod_pow(abb[8], 2) * (abb[4] * T(-6) + abb[9] * T(-6) + abb[5] * T(6) + abb[6] * T(6)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_142_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl21 = DLog_W_21<T>(kin),dl11 = DLog_W_11<T>(kin),dl6 = DLog_W_6<T>(kin),dl9 = DLog_W_9<T>(kin),dl25 = DLog_W_25<T>(kin),dl16 = DLog_W_16<T>(kin),dl4 = DLog_W_4<T>(kin),dl19 = DLog_W_19<T>(kin),dl29 = DLog_W_29<T>(kin),dl20 = DLog_W_20<T>(kin),dl18 = DLog_W_18<T>(kin),dl1 = DLog_W_1<T>(kin),dl3 = DLog_W_3<T>(kin),dl2 = DLog_W_2<T>(kin),dl17 = DLog_W_17<T>(kin),dl31 = DLog_W_31<T>(kin),dl26 = DLog_W_26<T>(kin),dl27 = DLog_W_27<T>(kin),dl30 = DLog_W_30<T>(kin),dl28 = DLog_W_28<T>(kin),spdl22 = SpDLog_f_4_142_W_22<T>(kin),spdl21 = SpDLog_f_4_142_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,45> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_1_3_2(kin) - f_1_3_2(kin_path), dl11(t), rlog(v_path[0]), rlog(v_path[3]), dl6(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl9(t), dl25(t), dl16(t), f_2_1_14(kin_path), f_2_1_15(kin_path), f_2_1_2(kin_path), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl4(t), f_2_1_9(kin_path), dl19(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl29(t) / kin_path.SqrtDelta, rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[30] / kin_path.W[30]), dl20(t), dl18(t), dl1(t), dl3(t), f_2_2_9(kin_path), dl2(t), dl17(t), dl31(t), dl26(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta}
;

        auto result = f_4_142_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_142_abbreviated(const std::array<T,45>& abb)
{
using TR = typename T::value_type;
T z[104];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[15];
z[3] = abb[17];
z[4] = abb[35];
z[5] = abb[36];
z[6] = abb[4];
z[7] = abb[5];
z[8] = abb[39];
z[9] = abb[6];
z[10] = abb[29];
z[11] = abb[41];
z[12] = abb[43];
z[13] = abb[30];
z[14] = abb[31];
z[15] = abb[32];
z[16] = abb[2];
z[17] = abb[23];
z[18] = abb[33];
z[19] = abb[28];
z[20] = abb[8];
z[21] = abb[25];
z[22] = abb[11];
z[23] = abb[38];
z[24] = abb[12];
z[25] = abb[44];
z[26] = abb[34];
z[27] = abb[9];
z[28] = abb[42];
z[29] = abb[14];
z[30] = bc<TR>[0];
z[31] = abb[16];
z[32] = abb[13];
z[33] = abb[18];
z[34] = abb[19];
z[35] = abb[20];
z[36] = abb[21];
z[37] = abb[22];
z[38] = abb[24];
z[39] = abb[26];
z[40] = abb[27];
z[41] = abb[10];
z[42] = abb[37];
z[43] = abb[40];
z[44] = bc<TR>[1];
z[45] = bc<TR>[3];
z[46] = bc<TR>[5];
z[47] = bc<TR>[2];
z[48] = bc<TR>[4];
z[49] = bc<TR>[7];
z[50] = bc<TR>[8];
z[51] = bc<TR>[9];
z[52] = z[10] + z[13] + z[14] + T(-2) * z[15];
z[53] = z[25] * z[52];
z[54] = z[12] * z[52];
z[55] = z[11] * z[52];
z[56] = z[1] + z[27];
z[57] = -z[7] + z[56];
z[58] = z[23] * z[57];
z[59] = z[7] * z[8];
z[60] = z[9] + -z[27];
z[61] = -z[6] + z[60];
z[62] = z[5] * z[61];
z[62] = z[53] + -z[54] + z[55] + z[58] + z[59] + -z[62];
z[63] = z[26] * z[56];
z[64] = z[28] * z[52];
z[65] = z[63] + -z[64];
z[66] = -z[6] + z[57];
z[67] = z[31] * z[66];
z[68] = z[1] + z[9];
z[69] = z[7] + z[68];
z[70] = z[2] * z[69];
z[71] = z[67] + z[70];
z[72] = z[6] * z[26];
z[73] = z[9] * z[17];
z[74] = z[72] + -z[73];
z[75] = T(2) * z[1];
z[76] = T(9) * z[7] + T(-17) * z[27];
z[76] = -z[75] + z[76] / T(2);
z[76] = z[17] * z[76];
z[77] = -z[7] + z[27];
z[78] = T(-2) * z[6] + (T(5) * z[9]) / T(2) + z[75] + -z[77] / T(2);
z[78] = z[21] * z[78];
z[79] = z[7] * z[26];
z[80] = z[6] + -z[9];
z[80] = -z[80] / T(2);
z[81] = T(4) * z[1] + (T(9) * z[27]) / T(2) + -z[80];
z[81] = z[18] * z[81];
z[80] = T(6) * z[1] + (T(13) * z[27]) / T(2) + -z[80];
z[80] = z[4] * z[80];
z[75] = (T(-5) * z[6]) / T(2) + -z[7] / T(2) + T(2) * z[9] + z[75];
z[75] = z[3] * z[75];
z[82] = -z[12] + z[25];
z[83] = T(7) * z[11] + (T(13) * z[28]) / T(2) + -z[82] / T(2);
z[83] = z[44] * z[83];
z[62] = T(2) * z[62] + T(-4) * z[65] + T(-6) * z[71] + (T(-13) * z[74]) / T(2) + z[75] + z[76] + z[78] + (T(-9) * z[79]) / T(2) + z[80] + z[81] + z[83];
z[62] = z[30] * z[62];
z[65] = z[5] + z[23];
z[71] = -z[8] + T(2) * z[43];
z[74] = z[65] + -z[71];
z[75] = z[45] * z[74];
z[62] = z[62] + T(16) * z[75];
z[62] = z[30] * z[62];
z[73] = -z[63] + -z[70] + z[73];
z[76] = z[3] * z[68];
z[78] = z[4] * z[68];
z[80] = z[54] + -z[76] + -z[78];
z[81] = z[19] * z[52];
z[60] = z[41] * z[60];
z[83] = z[7] * z[18];
z[77] = -(z[17] * z[77]);
z[77] = -z[60] + z[64] + z[73] + z[77] + -z[80] + -z[81] + z[83];
z[77] = int_to_imaginary<T>(1) * z[30] * z[77];
z[84] = z[54] + z[55];
z[78] = -z[78] + z[84];
z[85] = z[23] * z[68];
z[76] = z[76] + -z[78] + -z[85];
z[86] = z[0] * z[76];
z[87] = z[17] * z[56];
z[88] = z[63] + z[87];
z[89] = z[55] + -z[88];
z[90] = z[5] * z[56];
z[91] = z[89] + z[90];
z[92] = z[29] * z[91];
z[93] = z[29] * z[64];
z[86] = z[86] + z[92] + z[93];
z[92] = z[55] + z[64];
z[94] = -z[88] + z[92];
z[95] = z[90] + z[94];
z[95] = z[22] * z[95];
z[96] = z[7] * z[17];
z[97] = -z[59] + z[96];
z[83] = z[83] + z[97];
z[98] = -z[81] + z[83];
z[99] = -(z[16] * z[98]);
z[77] = z[77] + -z[86] + z[95] + z[99];
z[60] = z[60] + -z[85];
z[95] = z[17] * z[27];
z[73] = -z[59] + z[60] + -z[73] + -z[92] + z[95];
z[73] = z[24] * z[73];
z[73] = z[73] + T(2) * z[77];
z[73] = z[24] * z[73];
z[63] = -z[63] + z[79];
z[77] = z[58] + z[63];
z[57] = z[18] * z[57];
z[95] = -z[57] + z[77];
z[99] = -z[81] + z[95];
z[100] = -z[92] + -z[99];
z[100] = z[20] * z[100];
z[55] = z[55] + z[95];
z[55] = z[29] * z[55];
z[101] = z[0] * z[83];
z[102] = z[29] * z[52];
z[103] = -(z[0] * z[52]);
z[103] = -z[102] + z[103];
z[103] = z[19] * z[103];
z[55] = z[55] + z[93] + z[100] + z[101] + z[103];
z[54] = z[54] + -z[64];
z[64] = z[3] + z[21];
z[68] = -z[6] + z[68];
z[64] = z[64] * z[68];
z[64] = z[53] + z[64];
z[61] = z[18] * z[61];
z[61] = -z[54] + z[61] + z[63] + z[64] + -z[96];
z[61] = z[16] * z[61];
z[55] = T(2) * z[55] + z[61];
z[55] = z[16] * z[55];
z[61] = z[5] * z[68];
z[63] = z[3] * z[69];
z[59] = z[59] + -z[61] + z[63] + -z[70] + -z[78];
z[59] = prod_pow(z[0], 2) * z[59];
z[63] = z[4] * z[6];
z[53] = z[53] + -z[63];
z[66] = z[21] * z[66];
z[66] = z[53] + z[58] + -z[61] + z[66] + -z[67];
z[66] = z[20] * z[66];
z[68] = z[6] * z[21];
z[69] = z[6] * z[8];
z[70] = z[53] + -z[68] + z[69];
z[78] = -(z[29] * z[70]);
z[61] = -z[61] + z[64];
z[64] = z[61] + -z[84];
z[84] = -(z[0] * z[64]);
z[78] = z[78] + z[84];
z[66] = z[66] + T(2) * z[78];
z[66] = z[20] * z[66];
z[78] = z[20] * z[70];
z[78] = z[78] + z[86];
z[84] = z[4] * z[56];
z[86] = z[6] + z[56];
z[86] = z[32] * z[86];
z[60] = z[60] + z[69] + z[84] + -z[86];
z[84] = -z[60] + T(-2) * z[90] + -z[94];
z[84] = z[22] * z[84];
z[78] = T(2) * z[78] + z[84];
z[78] = z[22] * z[78];
z[67] = -z[67] + z[86];
z[84] = z[8] + z[26];
z[84] = z[6] * z[84];
z[58] = -z[58] + -z[67] + z[84] + -z[89];
z[84] = prod_pow(z[29], 2);
z[58] = z[58] * z[84];
z[86] = prod_pow(z[44], 2);
z[89] = T(4) * z[48] + z[86];
z[93] = z[11] + z[28];
z[89] = z[44] * z[89] * z[93];
z[55] = z[55] + z[58] + z[59] + z[66] + z[73] + z[78] + z[89];
z[53] = z[53] + z[57] + z[67] + -z[72] + -z[79] + -z[87];
z[53] = z[29] * z[53];
z[57] = z[63] + -z[69] + z[95];
z[57] = z[40] * z[57];
z[58] = z[76] + z[83];
z[59] = -(z[37] * z[58]);
z[63] = z[40] * z[52];
z[66] = z[37] * z[52];
z[66] = -z[63] + z[66] + z[102];
z[66] = z[19] * z[66];
z[67] = T(2) * z[48];
z[69] = -z[63] + -z[67];
z[69] = z[25] * z[69];
z[72] = -z[29] + z[40];
z[68] = z[68] * z[72];
z[72] = z[63] + -z[67];
z[72] = z[28] * z[72];
z[67] = z[12] * z[67];
z[63] = z[11] * z[63];
z[53] = z[53] + z[57] + z[59] + z[63] + z[66] + z[67] + z[68] + z[69] + z[72];
z[57] = T(-3) * z[82];
z[59] = z[57] + z[74];
z[63] = T(3) * z[28];
z[66] = T(3) * z[11];
z[59] = T(2) * z[59] + -z[63] + z[66];
z[59] = z[59] * z[86];
z[53] = T(6) * z[53] + z[59];
z[59] = z[60] + -z[94];
z[59] = z[22] * z[59];
z[56] = z[18] * z[56];
z[56] = z[56] + -z[77] + -z[92] + z[97];
z[56] = z[16] * z[56];
z[60] = -z[11] + z[82];
z[60] = z[44] * z[47] * z[60];
z[56] = z[56] + z[59] + z[60];
z[57] = -z[57] + z[63] + T(4) * z[74];
z[59] = prod_pow(z[30], 2);
z[57] = z[57] * z[59];
z[53] = T(2) * z[53] + T(12) * z[56] + z[57];
z[53] = z[30] * z[53];
z[56] = -(z[46] * z[74]);
z[57] = -(z[44] * z[75]);
z[56] = T(4) * z[56] + z[57];
z[53] = z[53] + T(48) * z[56];
z[53] = int_to_imaginary<T>(1) * z[53];
z[56] = z[80] + z[85] + z[88] + -z[90];
z[56] = z[35] * z[56];
z[57] = z[70] + -z[92] + -z[95];
z[57] = z[39] * z[57];
z[54] = -z[54] + z[61] + z[99];
z[54] = z[34] * z[54];
z[60] = -z[70] + -z[91];
z[60] = z[38] * z[60];
z[58] = z[36] * z[58];
z[61] = z[64] + -z[98];
z[61] = z[33] * z[61];
z[63] = -z[36] + z[39];
z[63] = z[63] * z[81];
z[54] = z[54] + z[56] + z[57] + z[58] + z[60] + z[61] + z[63];
z[56] = z[35] + z[38];
z[56] = T(-6) * z[56] + T(-3) * z[84];
z[56] = z[52] * z[56];
z[57] = T(3) * z[50];
z[58] = T(6) * z[49] + T(-7) * z[51];
z[56] = z[56] + z[57] + T(2) * z[58];
z[58] = T(2) * z[28];
z[56] = z[56] * z[58];
z[60] = T(-15) * z[11] + T(-8) * z[28] + T(7) * z[82];
z[59] = z[59] * z[60];
z[60] = z[66] + -z[82];
z[58] = -z[58] + -z[60];
z[58] = prod_pow(z[47], 2) * z[58];
z[58] = T(4) * z[58] + z[59];
z[58] = z[47] * z[58];
z[57] = T(12) * z[49] + z[57];
z[57] = z[57] * z[60];
z[59] = T(12) * z[65] + T(-12) * z[71];
z[52] = z[42] * z[52] * z[59];
z[59] = (T(-91) * z[11]) / T(2) + (T(35) * z[82]) / T(2);
z[59] = z[51] * z[59];
return z[52] + z[53] + T(12) * z[54] + T(6) * z[55] + z[56] + z[57] + z[58] + z[59] + z[62];
}



template IntegrandConstructorType<double> f_4_142_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_142_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_142_construct (const Kin<qd_real>&);
#endif

}