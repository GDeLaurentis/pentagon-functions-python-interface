#include "f_4_75.h"

namespace PentagonFunctions {

template <typename T> T f_4_75_abbreviated (const std::array<T,16>&);



template <typename T> IntegrandConstructorType<T> f_4_75_construct (const Kin<T>& kin) {
    return [&kin, 
            dl17 = DLog_W_17<T>(kin),dl16 = DLog_W_16<T>(kin),dl18 = DLog_W_18<T>(kin),dl5 = DLog_W_5<T>(kin),dl24 = DLog_W_24<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,16> abbr = 
            {dl17(t), rlog(kin.W[23] / kin_path.W[23]), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_7(kin_path), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[15] / kin_path.W[15]), dl16(t), dl18(t), dl5(t), dl24(t)}
;

        auto result = f_4_75_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_75_abbreviated(const std::array<T,16>& abb)
{
using TR = typename T::value_type;
T z[42];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = bc<TR>[0];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[11];
z[13] = bc<TR>[2];
z[14] = bc<TR>[9];
z[15] = abb[12];
z[16] = abb[13];
z[17] = abb[14];
z[18] = abb[15];
z[19] = T(2) * z[16];
z[20] = T(2) * z[17];
z[21] = z[19] + z[20];
z[22] = -z[18] + z[21];
z[22] = z[1] * z[22];
z[23] = -z[0] + z[18];
z[24] = z[12] * z[23];
z[25] = z[1] * z[15];
z[22] = z[22] + -z[24] + -z[25];
z[24] = z[2] * z[22];
z[26] = -z[21] + z[23] / T(2);
z[26] = z[1] * z[26];
z[27] = z[0] + z[17];
z[28] = T(2) * z[18] + -z[27];
z[28] = z[12] * z[28];
z[26] = z[25] + z[26] + z[28];
z[28] = z[8] * z[26];
z[29] = T(3) * z[18];
z[30] = -z[0] + z[29];
z[31] = -z[21] + z[30];
z[32] = z[5] * z[31];
z[33] = -(z[1] * z[32]);
z[34] = z[17] + -z[18];
z[34] = z[12] * z[34];
z[35] = z[0] + z[18];
z[35] = z[35] / T(2);
z[36] = z[1] * z[35];
z[36] = z[34] + z[36];
z[36] = z[3] * z[36];
z[37] = z[16] + z[17];
z[38] = z[35] + T(-3) * z[37];
z[38] = z[15] + z[38] / T(2);
z[38] = prod_pow(z[13], 2) * z[38];
z[24] = z[24] + z[28] + z[33] + z[36] + z[38];
z[28] = T(3) * z[15];
z[21] = -z[0] + -z[21] + z[28];
z[33] = z[2] * z[21];
z[36] = -z[15] + z[17] + z[35];
z[38] = T(3) * z[8];
z[39] = z[36] * z[38];
z[40] = -z[0] + -z[29];
z[19] = -z[17] + z[19] + z[40] / T(2);
z[19] = z[3] * z[19];
z[19] = z[19] + z[32] + z[33] + z[39];
z[19] = z[9] * z[19];
z[33] = -z[3] + z[38];
z[33] = z[31] * z[33];
z[38] = z[2] + z[5];
z[38] = z[31] * z[38];
z[38] = T(2) * z[38];
z[33] = z[33] + -z[38];
z[39] = -(z[11] * z[33]);
z[35] = z[35] + -z[37];
z[40] = prod_pow(z[4], 2);
z[35] = z[35] * z[40];
z[19] = z[19] + T(3) * z[24] + z[35] / T(2) + z[39];
z[24] = int_to_imaginary<T>(1) * z[4];
z[19] = z[19] * z[24];
z[22] = z[5] * z[22];
z[23] = -z[23] + z[37];
z[23] = z[1] * z[23];
z[23] = z[23] + z[34];
z[23] = z[2] * z[23];
z[35] = -z[18] + -z[37];
z[35] = z[1] * z[35];
z[35] = z[25] + z[35];
z[27] = z[18] + -z[27] / T(2);
z[27] = z[12] * z[27];
z[27] = z[27] + z[35] / T(2);
z[27] = z[3] * z[27];
z[23] = z[22] + z[23] + z[27];
z[23] = z[3] * z[23];
z[27] = -z[0] + z[17];
z[27] = z[12] * z[27];
z[25] = z[25] + z[27];
z[27] = -z[0] + z[16];
z[35] = z[17] + -z[27];
z[35] = z[1] * z[35];
z[35] = -z[25] + z[35];
z[35] = z[2] * z[35];
z[22] = -z[22] + z[35] / T(2);
z[22] = z[2] * z[22];
z[35] = -z[0] + z[37];
z[39] = z[1] * z[35];
z[41] = z[15] + -z[16];
z[41] = z[9] * z[41];
z[25] = -z[25] + z[39] + z[41];
z[25] = z[6] * z[25];
z[30] = z[30] / T(2) + -z[37];
z[39] = prod_pow(z[5], 2);
z[30] = z[30] * z[39];
z[41] = z[1] * z[30];
z[22] = z[22] + z[23] + z[25] + z[41];
z[23] = z[3] * z[31];
z[23] = -z[23] + z[38];
z[23] = z[3] * z[23];
z[25] = T(3) * z[7];
z[38] = z[25] + z[39];
z[31] = z[31] * z[38];
z[38] = (T(2) * z[0]) / T(3) + (T(-3) * z[18]) / T(2) + (T(5) * z[37]) / T(6);
z[38] = z[38] * z[40];
z[35] = z[2] * z[35];
z[32] = -z[32] + z[35];
z[35] = T(2) * z[2];
z[32] = z[32] * z[35];
z[23] = z[23] + -z[31] + z[32] + -z[38];
z[31] = z[11] * z[23];
z[24] = -(z[24] * z[33]);
z[23] = z[23] + z[24];
z[23] = z[10] * z[23];
z[24] = T(5) * z[0] + T(-11) * z[18];
z[24] = z[24] / T(2) + z[37];
z[24] = z[15] + z[24] / T(2);
z[24] = z[13] * z[24];
z[32] = z[0] + (T(9) * z[16]) / T(4) + (T(5) * z[17]) / T(2) + (T(-17) * z[18]) / T(4);
z[32] = z[1] * z[32];
z[33] = -z[0] / T(3) + (T(-17) * z[16]) / T(12) + (T(-2) * z[17]) / T(3) + (T(7) * z[18]) / T(4);
z[33] = z[9] * z[33];
z[24] = (T(3) * z[24]) / T(2) + z[32] + z[33] + -z[34];
z[24] = z[24] * z[40];
z[21] = z[5] * z[21];
z[32] = -z[20] + z[27];
z[32] = z[2] * z[32];
z[27] = -z[27] + z[29];
z[29] = (T(3) * z[15]) / T(2);
z[27] = z[17] + z[27] / T(2) + -z[29];
z[27] = z[3] * z[27];
z[27] = z[21] + z[27] + z[32];
z[27] = z[3] * z[27];
z[20] = -z[0] + -z[16] / T(2) + -z[20] + z[29];
z[20] = z[2] * z[20];
z[20] = z[20] + -z[21];
z[20] = z[2] * z[20];
z[20] = z[20] + z[27] + -z[30];
z[20] = z[9] * z[20];
z[21] = z[9] * z[36];
z[21] = z[21] + z[26];
z[21] = z[21] * z[25];
z[25] = (T(-65) * z[0]) / T(3) + T(21) * z[18];
z[25] = (T(5) * z[25]) / T(2) + (T(23) * z[37]) / T(3);
z[25] = z[25] / T(2) + -z[28];
z[25] = z[14] * z[25];
return z[19] + z[20] + z[21] + T(3) * z[22] + z[23] + z[24] + z[25] / T(4) + z[31];
}



template IntegrandConstructorType<double> f_4_75_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_75_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_75_construct (const Kin<qd_real>&);
#endif

}