#include "f_4_279.h"

namespace PentagonFunctions {

template <typename T> T f_4_279_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_279_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_279_W_12 (const Kin<T>& kin) {
        c[0] = (-kin.v[1] + T(-2)) * kin.v[1] + kin.v[4] * (T(2) + kin.v[4]);
c[1] = kin.v[1] * (T(2) + kin.v[1]) + (-kin.v[4] + T(-2)) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] + prod_pow(abb[1], 2) * (abb[4] + -abb[3]) + -(prod_pow(abb[2], 2) * abb[4]));
    }
};
template <typename T> class SpDLog_f_4_279_W_7 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_279_W_7 (const Kin<T>& kin) {
        c[0] = (kin.v[3] / T(4) + kin.v[4] / T(2) + -kin.v[1] + T(-1)) * kin.v[3] + -(kin.v[1] * kin.v[4]) + (kin.v[4] / T(4) + T(-1)) * kin.v[4];
c[1] = kin.v[3] + kin.v[4];
c[2] = (kin.v[3] / T(4) + kin.v[4] / T(2) + -kin.v[1] + T(-1)) * kin.v[3] + -(kin.v[1] * kin.v[4]) + (kin.v[4] / T(4) + T(-1)) * kin.v[4];
c[3] = kin.v[3] + kin.v[4];
c[4] = (kin.v[3] / T(4) + kin.v[4] / T(2) + -kin.v[1] + T(-1)) * kin.v[3] + -(kin.v[1] * kin.v[4]) + (kin.v[4] / T(4) + T(-1)) * kin.v[4];
c[5] = kin.v[3] + kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[8] * (t * c[0] + c[1]) + abb[4] * (t * c[2] + c[3]) + abb[9] * (t * c[4] + c[5]);
        }

        return abb[5] * (prod_pow(abb[6], 2) * (abb[4] / T(2) + abb[9] / T(2)) + abb[1] * (abb[6] * abb[8] + abb[6] * (abb[4] + abb[9]) + abb[1] * ((abb[4] * T(-3)) / T(2) + (abb[8] * T(-3)) / T(2) + (abb[9] * T(-3)) / T(2))) + abb[8] * (prod_pow(abb[6], 2) / T(2) + -abb[7]) + abb[7] * (-abb[4] + -abb[9]));
    }
};
template <typename T> class SpDLog_f_4_279_W_10 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_279_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[0] * kin.v[2] + kin.v[2] * (kin.v[2] / T(2) + T(-4) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(-4) + kin.v[0] + (T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[1] + T(2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(-1) + kin.v[2]) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + bc<T>[0] * (kin.v[2] / T(2) + -kin.v[4] + T(-1)) * int_to_imaginary<T>(1) * kin.v[2];
c[1] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[2];
c[2] = kin.v[0] * kin.v[2] + kin.v[2] * (kin.v[2] / T(2) + T(-4) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(-4) + kin.v[0] + (T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[1] + T(2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(-1) + kin.v[2]) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + bc<T>[0] * (kin.v[2] / T(2) + -kin.v[4] + T(-1)) * int_to_imaginary<T>(1) * kin.v[2];
c[3] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[2];
c[4] = kin.v[0] * kin.v[2] + kin.v[2] * (kin.v[2] / T(2) + T(-4) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(-4) + kin.v[0] + (T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[1] + T(2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(-1) + kin.v[2]) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + bc<T>[0] * (kin.v[2] / T(2) + -kin.v[4] + T(-1)) * int_to_imaginary<T>(1) * kin.v[2];
c[5] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[2];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[3] * (t * c[0] + c[1]) + abb[8] * (t * c[2] + c[3]) + abb[12] * (t * c[4] + c[5]);
        }

        return abb[22] * (abb[1] * (abb[3] * abb[14] + (abb[8] + abb[12]) * abb[14] + abb[2] * (-abb[3] + -abb[8] + -abb[12])) + abb[14] * ((abb[8] + abb[12]) * abb[14] + (abb[8] + abb[12]) * abb[15] + abb[6] * (-abb[8] + -abb[12]) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-1)) + abb[2] * (abb[6] * (abb[8] + abb[12]) + abb[14] * (-abb[8] + -abb[12]) + abb[15] * (-abb[8] + -abb[12]) + abb[8] * bc<T>[0] * int_to_imaginary<T>(1) + abb[12] * bc<T>[0] * int_to_imaginary<T>(1) + abb[3] * (abb[6] + -abb[14] + -abb[15] + bc<T>[0] * int_to_imaginary<T>(1))) + abb[23] * (abb[8] * T(2) + abb[12] * T(2)) + abb[3] * (abb[14] * (abb[14] + abb[15] + -abb[6] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[23] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_279_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl7 = DLog_W_7<T>(kin),dl4 = DLog_W_4<T>(kin),dl24 = DLog_W_24<T>(kin),dl20 = DLog_W_20<T>(kin),dl1 = DLog_W_1<T>(kin),dl10 = DLog_W_10<T>(kin),dl25 = DLog_W_25<T>(kin),dl31 = DLog_W_31<T>(kin),dl3 = DLog_W_3<T>(kin),dl16 = DLog_W_16<T>(kin),dl19 = DLog_W_19<T>(kin),dl5 = DLog_W_5<T>(kin),dl17 = DLog_W_17<T>(kin),dl2 = DLog_W_2<T>(kin),dl18 = DLog_W_18<T>(kin),dl26 = DLog_W_26<T>(kin),dl28 = DLog_W_28<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),spdl12 = SpDLog_f_4_279_W_12<T>(kin),spdl7 = SpDLog_f_4_279_W_7<T>(kin),spdl10 = SpDLog_f_4_279_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,46> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl7(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl4(t), f_2_1_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl24(t), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl20(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl1(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl10(t), f_2_1_7(kin_path), dl25(t), dl31(t), dl3(t), dl16(t), dl19(t), dl5(t), dl17(t), dl2(t), dl18(t), dl26(t) / kin_path.SqrtDelta, rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[18] / kin_path.W[18]), dl28(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta}
;

        auto result = f_4_279_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_279_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[124];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[10];
z[3] = abb[16];
z[4] = abb[26];
z[5] = abb[28];
z[6] = abb[29];
z[7] = abb[30];
z[8] = abb[31];
z[9] = abb[32];
z[10] = abb[4];
z[11] = abb[8];
z[12] = abb[9];
z[13] = abb[12];
z[14] = abb[33];
z[15] = abb[34];
z[16] = abb[35];
z[17] = abb[36];
z[18] = abb[37];
z[19] = abb[38];
z[20] = abb[39];
z[21] = abb[40];
z[22] = abb[41];
z[23] = abb[43];
z[24] = abb[44];
z[25] = abb[2];
z[26] = abb[6];
z[27] = abb[14];
z[28] = abb[15];
z[29] = bc<TR>[0];
z[30] = abb[42];
z[31] = abb[27];
z[32] = abb[45];
z[33] = abb[24];
z[34] = abb[19];
z[35] = abb[7];
z[36] = abb[11];
z[37] = abb[13];
z[38] = abb[17];
z[39] = abb[18];
z[40] = abb[20];
z[41] = abb[21];
z[42] = abb[23];
z[43] = abb[25];
z[44] = bc<TR>[3];
z[45] = bc<TR>[1];
z[46] = bc<TR>[2];
z[47] = bc<TR>[4];
z[48] = bc<TR>[9];
z[49] = z[13] / T(2);
z[50] = z[12] + z[49];
z[51] = z[1] / T(2);
z[52] = z[50] + -z[51];
z[53] = z[11] / T(2);
z[54] = z[52] + z[53];
z[54] = z[5] * z[54];
z[55] = z[1] + z[13];
z[56] = T(3) * z[11];
z[57] = z[55] + z[56];
z[58] = z[7] * z[57];
z[59] = z[34] * z[56];
z[60] = z[34] * z[55];
z[59] = z[59] + z[60];
z[61] = z[59] / T(2);
z[62] = z[10] + z[12];
z[63] = z[1] + z[11];
z[64] = z[62] + -z[63];
z[64] = z[33] * z[64];
z[54] = z[54] + z[58] / T(2) + -z[61] + -z[64];
z[58] = -(z[39] * z[54]);
z[65] = -z[14] + z[30];
z[66] = z[24] + z[65];
z[67] = z[0] * z[66];
z[68] = z[23] * z[26];
z[67] = z[67] + -z[68];
z[69] = z[23] * z[27];
z[70] = -z[32] + z[65];
z[71] = z[25] * z[70];
z[66] = -z[23] + z[66];
z[72] = z[39] * z[66];
z[73] = z[23] + z[70];
z[74] = z[41] * z[73];
z[69] = -z[67] + z[69] + z[71] + z[72] + -z[74];
z[71] = z[18] / T(2);
z[72] = -(z[69] * z[71]);
z[49] = -z[49] + z[53];
z[53] = -z[12] + z[49] + z[51];
z[74] = z[26] * z[53];
z[75] = T(2) * z[0];
z[62] = -z[1] + z[62];
z[76] = z[62] * z[75];
z[77] = z[74] + z[76];
z[78] = T(2) * z[28];
z[79] = -(z[62] * z[78]);
z[80] = (T(3) * z[1]) / T(2);
z[81] = -z[12] + -z[49] + z[80];
z[82] = T(2) * z[10];
z[83] = z[81] + -z[82];
z[84] = -(z[39] * z[83]);
z[85] = z[11] + z[55];
z[86] = z[85] / T(2);
z[87] = z[27] * z[86];
z[88] = -(z[25] * z[86]);
z[79] = -z[77] + z[79] + z[84] + z[87] + z[88];
z[79] = z[8] * z[79];
z[84] = -z[11] + T(3) * z[55];
z[88] = -z[82] + z[84] / T(2);
z[89] = z[41] * z[88];
z[90] = -z[10] + z[55];
z[91] = z[25] * z[90];
z[89] = -z[87] + -z[89] + T(2) * z[91];
z[91] = z[78] * z[90];
z[92] = z[0] + -z[26];
z[93] = -(z[85] * z[92]);
z[94] = z[93] / T(2);
z[91] = z[91] + -z[94];
z[95] = -z[89] + -z[91];
z[95] = z[6] * z[95];
z[96] = -z[11] + z[55];
z[97] = z[96] / T(2);
z[98] = -z[10] + z[97];
z[98] = z[26] * z[98];
z[76] = z[76] + z[98];
z[49] = -z[10] + T(-2) * z[12] + z[49] + z[80];
z[99] = -(z[39] * z[49]);
z[89] = -z[76] + -z[89] + z[99];
z[89] = z[9] * z[89];
z[99] = z[24] + -z[30] + z[32];
z[100] = z[4] + z[31];
z[101] = z[6] + z[7] + z[8];
z[102] = z[9] / T(3);
z[99] = (T(2) * z[5]) / T(9) + (T(7) * z[23]) / T(4) + (T(-4) * z[43]) / T(9) + -z[99] / T(4) + -z[100] / T(9) + z[101] / T(3) + z[102];
z[103] = prod_pow(z[29], 2);
z[104] = z[103] / T(3);
z[99] = z[99] * z[104];
z[105] = z[5] * z[11];
z[106] = -z[61] + z[105];
z[107] = z[7] * z[11];
z[108] = -z[64] + z[106] + z[107];
z[108] = z[26] * z[108];
z[109] = z[5] * z[53];
z[110] = z[7] * z[86];
z[109] = z[109] + -z[110];
z[109] = z[0] * z[109];
z[108] = z[108] + -z[109];
z[109] = z[64] + z[107];
z[111] = -z[11] + z[90];
z[111] = z[37] * z[111];
z[105] = z[105] + z[109] + z[111];
z[78] = z[78] * z[105];
z[105] = z[5] * z[85];
z[112] = z[7] * z[85];
z[112] = z[105] + -z[112];
z[113] = z[31] * z[88];
z[112] = z[112] / T(2) + z[113];
z[114] = z[25] * z[112];
z[115] = -z[55] + z[56];
z[116] = z[7] * z[115];
z[59] = -z[59] + z[116];
z[57] = z[5] * z[57];
z[57] = z[57] + z[59];
z[57] = z[57] / T(2) + z[113];
z[116] = -(z[41] * z[57]);
z[117] = -prod_pow(z[45], 2);
z[117] = -z[47] + z[117] / T(2);
z[118] = z[14] + z[32];
z[119] = T(3) * z[23] + z[118];
z[117] = z[117] * z[119];
z[106] = z[106] + T(2) * z[107];
z[107] = z[27] * z[106];
z[119] = z[45] * z[119];
z[120] = z[46] * z[66];
z[119] = z[119] + z[120] / T(2);
z[119] = z[46] * z[119];
z[120] = -z[0] + z[39];
z[120] = z[3] * z[83] * z[120];
z[58] = z[58] + z[72] + z[78] + z[79] + z[89] + z[95] + z[99] + z[107] + z[108] + z[114] + z[116] + z[117] + z[119] + z[120];
z[72] = int_to_imaginary<T>(1) * z[29];
z[58] = z[58] * z[72];
z[78] = z[10] + z[11];
z[79] = z[7] * z[78];
z[60] = z[60] + z[79];
z[89] = z[5] / T(2);
z[95] = z[1] + -z[12] + -z[13];
z[95] = z[11] + z[95] / T(3);
z[95] = z[89] * z[95];
z[99] = z[10] / T(3);
z[107] = -z[11] + z[55] / T(3);
z[107] = -z[99] + z[107] / T(4);
z[107] = z[4] * z[107];
z[102] = -(z[53] * z[102]);
z[114] = (T(3) * z[1]) / T(4) + -z[12] + -z[13] / T(4);
z[116] = (T(5) * z[11]) / T(12) + -z[99] + z[114];
z[116] = z[8] * z[116];
z[117] = z[23] + z[118] / T(3);
z[117] = z[45] * z[117];
z[118] = z[11] * z[34];
z[119] = T(-5) * z[1] + z[11] + T(-7) * z[13];
z[119] = z[10] + z[119] / T(6);
z[119] = z[6] * z[119];
z[120] = T(-9) * z[23] + T(5) * z[24] + z[65];
z[120] = z[32] + z[120] / T(4);
z[120] = z[46] * z[120];
z[121] = z[23] + -z[30];
z[122] = z[24] / T(2) + -z[121];
z[123] = z[18] * z[122];
z[60] = z[60] / T(3) + (T(4) * z[64]) / T(3) + z[95] + z[102] + z[107] + z[111] + z[116] + z[117] + z[118] + z[119] + z[120] + -z[123] / T(6);
z[60] = z[29] * z[60];
z[95] = z[9] + z[101];
z[95] = T(-2) * z[5] + T(4) * z[43] + T(-3) * z[95] + z[100];
z[95] = z[44] * z[95];
z[60] = z[60] + z[95];
z[60] = z[29] * z[60];
z[95] = z[28] / T(2);
z[100] = -(z[53] * z[95]);
z[77] = z[77] + z[100];
z[77] = z[28] * z[77];
z[100] = z[26] + -z[28];
z[100] = z[86] * z[100];
z[75] = -z[27] + -z[75];
z[75] = z[75] * z[78];
z[75] = z[75] + z[100];
z[75] = z[27] * z[75];
z[100] = z[28] * z[85];
z[93] = -z[93] + z[100];
z[85] = z[85] / T(4);
z[100] = z[25] * z[85];
z[78] = T(2) * z[78];
z[78] = z[27] * z[78];
z[78] = z[78] + z[93] / T(2) + -z[100];
z[78] = z[25] * z[78];
z[93] = prod_pow(z[26], 2);
z[93] = z[93] / T(2);
z[62] = z[62] * z[93];
z[101] = z[82] + z[115] / T(2);
z[102] = z[36] * z[101];
z[107] = z[10] / T(2);
z[116] = z[11] + -z[52] + z[107];
z[116] = z[0] * z[116];
z[98] = z[98] + z[116];
z[98] = z[0] * z[98];
z[116] = T(5) * z[11] + z[55];
z[116] = z[82] + z[116] / T(2);
z[117] = -(z[42] * z[116]);
z[118] = z[38] * z[83];
z[119] = z[35] * z[49];
z[75] = -z[62] + z[75] + z[77] + z[78] + z[98] + z[102] + z[117] + z[118] + z[119];
z[75] = z[8] * z[75];
z[77] = z[27] * z[85];
z[77] = z[77] + z[91];
z[77] = z[27] * z[77];
z[55] = z[11] + T(5) * z[55];
z[78] = z[55] / T(2) + -z[82];
z[78] = z[42] * z[78];
z[82] = z[40] * z[88];
z[77] = z[77] + z[78] + -z[82];
z[78] = z[0] / T(2);
z[53] = -(z[53] * z[78]);
z[53] = z[53] + z[74];
z[53] = z[0] * z[53];
z[74] = z[11] / T(4) + -z[107] + z[114];
z[74] = z[28] * z[74];
z[74] = z[74] + z[76];
z[74] = z[28] * z[74];
z[76] = z[87] + z[91] + -z[100];
z[76] = z[25] * z[76];
z[49] = z[38] * z[49];
z[82] = z[35] * z[83];
z[49] = z[49] + z[53] + -z[62] + z[74] + z[76] + -z[77] + z[82];
z[49] = z[9] * z[49];
z[53] = -z[23] + z[32] + -z[65];
z[53] = z[40] * z[53];
z[62] = z[23] + z[30];
z[74] = z[24] + z[62];
z[74] = z[35] * z[74];
z[65] = z[36] * z[65];
z[76] = z[38] * z[66];
z[82] = z[14] + z[23];
z[88] = z[32] + z[82];
z[91] = z[42] * z[88];
z[53] = -z[53] + -z[65] + z[74] + z[76] + z[91];
z[65] = z[69] * z[72];
z[69] = z[53] + -z[65];
z[72] = z[0] * z[82];
z[74] = z[27] * z[62];
z[76] = z[26] * z[82];
z[70] = z[28] * z[70];
z[72] = -z[70] + z[72] + z[74] + -z[76];
z[74] = z[25] * z[88];
z[88] = T(-2) * z[72] + z[74];
z[88] = z[25] * z[88];
z[62] = z[0] * z[62];
z[62] = z[62] + -z[68] + -z[70];
z[68] = z[27] * z[73];
z[70] = T(2) * z[62] + z[68];
z[70] = z[27] * z[70];
z[73] = z[24] + z[82];
z[82] = z[0] * z[73];
z[82] = T(-2) * z[76] + z[82];
z[82] = z[0] * z[82];
z[91] = z[28] * z[66];
z[91] = T(-2) * z[67] + z[91];
z[91] = z[28] * z[91];
z[98] = -z[24] + T(2) * z[121];
z[98] = z[98] * z[104];
z[69] = T(2) * z[69] + z[70] + z[82] + z[88] + z[91] + z[98];
z[69] = z[21] * z[69];
z[70] = -z[72] + z[74] / T(2);
z[70] = z[25] * z[70];
z[62] = z[62] + z[68] / T(2);
z[62] = z[27] * z[62];
z[66] = z[66] * z[95];
z[66] = z[66] + -z[67];
z[66] = z[28] * z[66];
z[67] = z[73] * z[78];
z[67] = z[67] + -z[76];
z[67] = z[0] * z[67];
z[53] = z[53] + z[62] + z[66] + z[67] + z[70];
z[62] = z[53] + -z[65];
z[65] = z[103] * z[122];
z[66] = z[62] + -z[65] / T(3);
z[67] = z[16] / T(2) + -z[22];
z[66] = z[66] * z[67];
z[62] = T(-3) * z[62] + z[65];
z[65] = z[15] + z[17] + z[19] + z[20];
z[65] = z[65] / T(2);
z[62] = z[62] * z[65];
z[65] = z[10] + z[115] / T(4);
z[65] = z[4] * z[65];
z[67] = z[13] + -z[63];
z[67] = z[67] * z[89];
z[68] = -z[10] + z[84] / T(4);
z[68] = z[31] * z[68];
z[70] = z[7] * z[85];
z[67] = z[65] + z[67] + z[68] + -z[70];
z[67] = z[25] * z[67];
z[72] = z[28] * z[112];
z[73] = z[4] * z[101];
z[74] = z[5] * z[86];
z[74] = -z[73] + -z[74] + T(2) * z[79];
z[76] = z[27] * z[74];
z[79] = z[5] * z[63];
z[79] = z[79] + z[110];
z[82] = z[26] * z[79];
z[79] = z[0] * z[79];
z[67] = z[67] + -z[72] + z[76] + z[79] + -z[82];
z[67] = z[25] * z[67];
z[76] = z[63] * z[92];
z[79] = z[28] * z[90];
z[76] = z[76] + z[79];
z[55] = z[10] + -z[55] / T(4);
z[55] = z[25] * z[55];
z[55] = z[55] + T(2) * z[76] + z[87];
z[55] = z[25] * z[55];
z[56] = T(3) * z[1] + z[56];
z[76] = z[13] + -z[56];
z[76] = z[36] * z[76];
z[56] = z[13] + z[56];
z[84] = z[35] * z[56];
z[76] = z[76] + -z[84];
z[78] = z[26] + -z[78];
z[78] = z[0] * z[78];
z[78] = z[78] + -z[93];
z[78] = z[63] * z[78];
z[79] = z[79] + -z[94];
z[79] = z[28] * z[79];
z[55] = z[55] + z[76] / T(2) + -z[77] + z[78] + z[79];
z[55] = z[6] * z[55];
z[53] = z[53] * z[71];
z[71] = -(z[0] * z[74]);
z[74] = -(z[5] * z[96]);
z[59] = -z[59] + z[74];
z[59] = z[59] / T(4) + -z[68];
z[59] = z[27] * z[59];
z[68] = z[26] * z[106];
z[59] = z[59] + z[68] + z[71] + z[72];
z[59] = z[27] * z[59];
z[54] = z[38] * z[54];
z[68] = z[0] * z[83];
z[71] = -z[10] + z[81] / T(2);
z[72] = -(z[28] * z[71]);
z[68] = z[68] + z[72];
z[68] = z[28] * z[68];
z[72] = -z[35] + -z[38];
z[72] = z[72] * z[83];
z[71] = -(prod_pow(z[0], 2) * z[71]);
z[74] = z[11] + -z[13];
z[51] = -z[12] / T(3) + z[51] + -z[74] / T(6);
z[51] = z[51] / T(2) + -z[99];
z[51] = z[51] * z[103];
z[51] = z[51] + z[68] + z[71] + z[72];
z[51] = z[3] * z[51];
z[68] = -(z[56] * z[89]);
z[71] = z[7] * z[101];
z[68] = z[68] + z[71] + -z[73];
z[68] = z[36] * z[68];
z[71] = (T(3) * z[11]) / T(2);
z[52] = z[52] + -z[71];
z[52] = z[5] * z[52];
z[72] = z[7] * z[97];
z[52] = z[52] + -z[61] + T(-3) * z[64] + z[72];
z[52] = z[52] * z[95];
z[52] = z[52] + -z[108];
z[52] = z[28] * z[52];
z[61] = -(z[7] * z[116]);
z[61] = z[61] + z[73] + z[105] + z[113];
z[61] = z[42] * z[61];
z[57] = -(z[40] * z[57]);
z[64] = z[12] + -z[63];
z[64] = z[64] * z[89];
z[64] = z[64] + -z[65] + -z[70];
z[64] = z[0] * z[64];
z[64] = z[64] + z[82];
z[64] = z[0] * z[64];
z[50] = z[50] + -z[71] + -z[80];
z[50] = z[5] * z[35] * z[50];
z[65] = z[0] + -z[25];
z[65] = z[65] * z[92];
z[65] = z[35] + z[36] + z[65];
z[56] = z[56] * z[65];
z[63] = -z[13] / T(3) + -z[63];
z[63] = z[63] * z[103];
z[56] = z[56] + z[63] / T(2);
z[56] = z[2] * z[56];
z[63] = z[93] * z[109];
z[65] = -prod_pow(z[28], 2);
z[70] = prod_pow(z[27], 2);
z[65] = z[65] + z[70];
z[65] = z[65] * z[111];
z[70] = (T(-61) * z[14]) / T(3) + -z[24] + T(5) * z[121];
z[70] = (T(-8) * z[32]) / T(3) + z[70] / T(8);
z[70] = z[48] * z[70];
return z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] / T(2) + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + z[68] + z[69] + z[70] + z[75];
}



template IntegrandConstructorType<double> f_4_279_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_279_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_279_construct (const Kin<qd_real>&);
#endif

}