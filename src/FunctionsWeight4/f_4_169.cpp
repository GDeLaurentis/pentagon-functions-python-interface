#include "f_4_169.h"

namespace PentagonFunctions {

template <typename T> T f_4_169_abbreviated (const std::array<T,42>&);

template <typename T> class SpDLog_f_4_169_W_10 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_169_W_10 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(-6) + T(3) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(-6) + T(3) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[1] * (T(6) + T(-3) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[1] * (T(6) + T(-3) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-3) + prod_pow(abb[1], 2) * (abb[5] * T(-3) + abb[6] * T(-3) + abb[3] * T(3) + abb[4] * T(3)) + prod_pow(abb[2], 2) * (abb[4] * T(-3) + abb[5] * T(3) + abb[6] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_169_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_169_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(2) * kin.v[2] + T(-4) * kin.v[3]) + kin.v[0] * (T(-2) * kin.v[0] + T(8) * kin.v[3] + T(-4) * kin.v[4]) + T(-2) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(-6) * kin.v[3] + T(8) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (T(-4) + T(2) * kin.v[0] + T(4) * kin.v[1] + T(-4) * kin.v[3]) + kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[4] * (T(-4) + T(-2) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = kin.v[2] * (T(14) * kin.v[2] + T(20) * kin.v[3] + T(-24) * kin.v[4]) + kin.v[3] * (T(6) * kin.v[3] + T(-16) * kin.v[4]) + T(10) * prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(5) * prod_pow(kin.v[4], 2) + kin.v[2] * (T(7) * kin.v[2] + T(10) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[3] * (T(3) * kin.v[3] + T(-8) * kin.v[4])) + kin.v[0] * ((bc<T>[0] * int_to_imaginary<T>(5) + T(10)) * kin.v[0] + T(-24) * kin.v[2] + T(-16) * kin.v[3] + T(20) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-12) * kin.v[2] + T(-8) * kin.v[3] + T(10) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (prod_pow(kin.v[4], 2) / T(2) + kin.v[2] * (-kin.v[2] / T(2) + kin.v[3]) + kin.v[0] * (kin.v[0] / T(2) + T(-2) * kin.v[3] + kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[1] + T(1) + kin.v[3]) + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (kin.v[4] / T(2) + T(1)) * kin.v[4])) + rlog(kin.v[3]) * (prod_pow(kin.v[4], 2) / T(2) + kin.v[2] * (-kin.v[2] / T(2) + kin.v[3]) + kin.v[0] * (kin.v[0] / T(2) + T(-2) * kin.v[3] + kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[1] + T(1) + kin.v[3]) + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (kin.v[4] / T(2) + T(1)) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (prod_pow(kin.v[4], 2) / T(2) + kin.v[2] * (-kin.v[2] / T(2) + kin.v[3]) + kin.v[0] * (kin.v[0] / T(2) + T(-2) * kin.v[3] + kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[1] + T(1) + kin.v[3]) + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (kin.v[4] / T(2) + T(1)) * kin.v[4])) + rlog(-kin.v[4]) * (prod_pow(kin.v[4], 2) / T(2) + kin.v[2] * (-kin.v[2] / T(2) + kin.v[3]) + kin.v[0] * (kin.v[0] / T(2) + T(-2) * kin.v[3] + kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[1] + T(1) + kin.v[3]) + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (kin.v[4] / T(2) + T(1)) * kin.v[4])) + rlog(-kin.v[1]) * (kin.v[2] * (-kin.v[2] + T(2) * kin.v[3]) + prod_pow(kin.v[4], 2) + kin.v[3] * (T(3) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[0] * (kin.v[0] + T(-4) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[0] * (-kin.v[0] + T(2) + T(-2) * kin.v[1] + T(2) * kin.v[3]) + kin.v[4] * (T(2) + kin.v[4]) + kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) * kin.v[3]) + kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(6) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[3] * ((T(-9) * kin.v[3]) / T(2) + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3]) + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) * kin.v[3]) + kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(6) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[3] * ((T(-9) * kin.v[3]) / T(2) + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3]) + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4])));
c[2] = (T(-8) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(8) * kin.v[2] + T(8) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + T(-8) * kin.v[4];
c[3] = rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[4])) + rlog(kin.v[3]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[4])) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[4])) + rlog(-kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[0] + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[16] * (abb[2] * (abb[3] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(3) + abb[12] * bc<T>[0] * int_to_imaginary<T>(3) + abb[19] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[2] * (-abb[3] + -abb[4] + -abb[5] + -abb[8] + abb[19] * T(-4) + abb[6] * T(3) + abb[12] * T(3))) + abb[17] * (abb[2] * (abb[2] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) + abb[1] * abb[2] * T(2) + abb[2] * abb[14] * T(2) + abb[18] * T(4)) + abb[1] * abb[2] * (abb[3] + abb[4] + abb[5] + abb[8] + abb[6] * T(-3) + abb[12] * T(-3) + abb[19] * T(4)) + abb[2] * abb[14] * (abb[3] + abb[4] + abb[5] + abb[8] + abb[6] * T(-3) + abb[12] * T(-3) + abb[19] * T(4)) + abb[9] * (abb[2] * abb[17] * T(-2) + abb[2] * (-abb[3] + -abb[4] + -abb[5] + -abb[8] + abb[19] * T(-4) + abb[6] * T(3) + abb[12] * T(3)) + abb[15] * (abb[3] + abb[4] + abb[5] + abb[8] + abb[6] * T(-3) + abb[12] * T(-3) + abb[17] * T(2) + abb[19] * T(4))) + abb[18] * (abb[6] * T(-6) + abb[12] * T(-6) + abb[3] * T(2) + abb[4] * T(2) + abb[5] * T(2) + abb[8] * T(2) + abb[19] * T(8)) + abb[15] * (abb[3] * bc<T>[0] * int_to_imaginary<T>(1) + abb[4] * bc<T>[0] * int_to_imaginary<T>(1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(1) + abb[17] * (abb[1] * T(-2) + abb[2] * T(-2) + abb[14] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2)) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[19] * bc<T>[0] * int_to_imaginary<T>(4) + abb[1] * (-abb[3] + -abb[4] + -abb[5] + -abb[8] + abb[19] * T(-4) + abb[6] * T(3) + abb[12] * T(3)) + abb[2] * (-abb[3] + -abb[4] + -abb[5] + -abb[8] + abb[19] * T(-4) + abb[6] * T(3) + abb[12] * T(3)) + abb[14] * (-abb[3] + -abb[4] + -abb[5] + -abb[8] + abb[19] * T(-4) + abb[6] * T(3) + abb[12] * T(3)) + abb[15] * (abb[6] * T(-6) + abb[12] * T(-6) + abb[3] * T(2) + abb[4] * T(2) + abb[5] * T(2) + abb[8] * T(2) + abb[17] * T(4) + abb[19] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_169_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl3 = DLog_W_3<T>(kin),dl9 = DLog_W_9<T>(kin),dl23 = DLog_W_23<T>(kin),dl19 = DLog_W_19<T>(kin),dl28 = DLog_W_28<T>(kin),dl1 = DLog_W_1<T>(kin),dl2 = DLog_W_2<T>(kin),dl27 = DLog_W_27<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl5 = DLog_W_5<T>(kin),dl17 = DLog_W_17<T>(kin),dl4 = DLog_W_4<T>(kin),dl20 = DLog_W_20<T>(kin),dl16 = DLog_W_16<T>(kin),dl18 = DLog_W_18<T>(kin),spdl10 = SpDLog_f_4_169_W_10<T>(kin),spdl23 = SpDLog_f_4_169_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,42> abbr = 
            {dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[17] / kin_path.W[17]), dl3(t), rlog(kin.W[3] / kin_path.W[3]), rlog(-v_path[1]), f_2_1_4(kin_path), f_2_1_7(kin_path), rlog(kin.W[19] / kin_path.W[19]), dl9(t), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), dl23(t), rlog(kin.W[1] / kin_path.W[1]), f_2_1_8(kin_path), -rlog(t), dl19(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl28(t) / kin_path.SqrtDelta, f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl1(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl2(t), dl27(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl5(t), dl17(t), dl4(t), dl20(t), dl16(t), dl18(t)}
;

        auto result = f_4_169_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_169_abbreviated(const std::array<T,42>& abb)
{
using TR = typename T::value_type;
T z[119];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[7];
z[3] = abb[31];
z[4] = abb[36];
z[5] = abb[37];
z[6] = abb[40];
z[7] = abb[41];
z[8] = abb[4];
z[9] = abb[38];
z[10] = abb[5];
z[11] = abb[6];
z[12] = abb[8];
z[13] = abb[12];
z[14] = abb[17];
z[15] = abb[19];
z[16] = abb[24];
z[17] = abb[32];
z[18] = abb[33];
z[19] = abb[35];
z[20] = abb[25];
z[21] = abb[26];
z[22] = abb[27];
z[23] = abb[2];
z[24] = abb[9];
z[25] = abb[20];
z[26] = abb[14];
z[27] = abb[15];
z[28] = bc<TR>[0];
z[29] = abb[23];
z[30] = abb[39];
z[31] = abb[34];
z[32] = abb[10];
z[33] = abb[11];
z[34] = abb[28];
z[35] = abb[18];
z[36] = abb[21];
z[37] = abb[22];
z[38] = abb[29];
z[39] = abb[30];
z[40] = abb[13];
z[41] = bc<TR>[3];
z[42] = bc<TR>[1];
z[43] = bc<TR>[2];
z[44] = bc<TR>[4];
z[45] = bc<TR>[9];
z[46] = z[8] + z[12];
z[47] = -z[10] + -z[13] + z[46];
z[48] = -z[1] + z[11];
z[49] = z[47] + -z[48];
z[50] = z[2] * z[49];
z[51] = -z[16] + -z[20] + z[21] + z[22];
z[52] = z[17] * z[51];
z[53] = z[31] * z[51];
z[54] = z[18] * z[51];
z[55] = z[50] + z[52] + -z[53] + T(-3) * z[54];
z[56] = z[11] / T(2);
z[57] = z[12] / T(2) + -z[56];
z[58] = z[14] / T(2) + z[15];
z[59] = -z[42] + T(3) * z[43];
z[60] = (T(11) * z[8]) / T(6) + (T(-4) * z[10]) / T(3) + -z[13] + z[57] + z[58] + z[59];
z[60] = z[9] * z[60];
z[61] = z[8] / T(3);
z[62] = z[10] + z[12];
z[63] = z[13] + -z[61] + -z[62] / T(3);
z[64] = z[14] / T(3) + (T(2) * z[15]) / T(3) + z[59];
z[63] = (T(5) * z[1]) / T(6) + -z[56] + z[63] / T(2) + -z[64];
z[63] = z[6] * z[63];
z[65] = z[1] / T(2);
z[56] = z[56] + z[65];
z[66] = T(5) * z[13];
z[67] = -z[8] + T(3) * z[62] + -z[66];
z[67] = z[67] / T(2);
z[68] = z[14] + -z[56] + z[67];
z[68] = z[15] + z[68] / T(2);
z[59] = -z[59] + -z[68];
z[59] = z[30] * z[59];
z[69] = z[13] / T(2) + -z[65];
z[57] = z[10] / T(3) + z[57] + -z[61] + -z[69];
z[57] = z[3] * z[57];
z[61] = -z[8] + -z[13] + z[62];
z[70] = -z[48] + z[61];
z[71] = z[34] * z[70];
z[58] = -z[8] / T(6) + (T(2) * z[10]) / T(3) + -z[11] + -z[42] + z[58] + -z[69];
z[58] = z[4] * z[58];
z[69] = T(-3) * z[8] + z[13] + z[62];
z[69] = z[69] / T(2);
z[72] = (T(3) * z[1]) / T(2);
z[73] = (T(5) * z[11]) / T(2) + z[69] + -z[72];
z[74] = -z[14] + z[73];
z[74] = -z[15] + z[74] / T(2);
z[75] = z[42] + z[74];
z[75] = z[7] * z[75];
z[76] = -z[10] + T(-19) * z[12];
z[66] = (T(5) * z[8]) / T(3) + z[66] + z[76] / T(3);
z[64] = (T(5) * z[1]) / T(12) + (T(-3) * z[11]) / T(4) + z[64] + z[66] / T(4);
z[64] = z[5] * z[64];
z[66] = z[19] * z[51];
z[76] = z[66] / T(2);
z[77] = z[40] * z[61];
z[77] = T(3) * z[77];
z[55] = z[55] / T(4) + z[57] + z[58] + z[59] + z[60] + z[63] + z[64] + -z[71] + z[75] + -z[76] + z[77];
z[57] = prod_pow(z[28], 2);
z[55] = z[55] * z[57];
z[58] = z[14] + T(2) * z[15];
z[59] = -z[1] + z[58];
z[60] = T(3) * z[13];
z[63] = -z[8] + z[59] + -z[60] + T(2) * z[62];
z[64] = T(2) * z[5];
z[75] = z[63] * z[64];
z[78] = -z[8] + z[60];
z[79] = -z[62] + z[78];
z[80] = (T(3) * z[11]) / T(2);
z[81] = -z[65] + z[79] / T(2) + z[80];
z[82] = -z[58] + z[81];
z[83] = z[3] * z[82];
z[84] = z[9] * z[63];
z[56] = -z[56] + z[58];
z[67] = z[56] + z[67];
z[85] = T(3) * z[30];
z[86] = z[67] * z[85];
z[87] = z[4] * z[82];
z[88] = z[6] * z[82];
z[89] = z[53] + z[66];
z[75] = z[75] + -z[83] + T(2) * z[84] + -z[86] + z[87] + z[88] + (T(-3) * z[89]) / T(2);
z[90] = z[23] * z[75];
z[47] = z[47] + z[48];
z[91] = z[25] * z[47];
z[51] = z[29] * z[51];
z[51] = z[51] + z[91];
z[91] = z[54] + z[66];
z[92] = z[51] + -z[91];
z[93] = z[5] * z[82];
z[94] = T(3) * z[12];
z[95] = -z[60] + z[94];
z[96] = T(3) * z[11];
z[97] = T(3) * z[1] + -z[96];
z[98] = -z[95] + z[97];
z[99] = z[8] + -z[10];
z[100] = z[98] + z[99];
z[101] = z[3] / T(2);
z[102] = z[100] * z[101];
z[103] = T(2) * z[99];
z[104] = z[4] * z[103];
z[103] = z[9] * z[103];
z[92] = -z[88] + (T(-3) * z[92]) / T(2) + z[93] + -z[102] + z[103] + z[104];
z[102] = -(z[0] * z[92]);
z[82] = z[9] * z[82];
z[87] = z[82] + z[87];
z[104] = T(2) * z[8] + -z[62];
z[105] = z[59] + z[104];
z[106] = z[3] * z[105];
z[107] = z[54] + z[71];
z[105] = z[6] * z[105];
z[106] = z[87] + z[93] + T(2) * z[105] + z[106] + (T(3) * z[107]) / T(2);
z[108] = z[27] * z[106];
z[48] = z[48] + z[61];
z[61] = z[3] * z[48];
z[61] = z[51] + -z[61];
z[109] = z[6] * z[70];
z[110] = z[4] * z[47];
z[111] = z[9] * z[48];
z[109] = -z[61] + z[66] + -z[71] + z[109] + z[110] + -z[111];
z[109] = (T(3) * z[109]) / T(2);
z[110] = z[37] * z[109];
z[111] = -z[89] + -z[107];
z[70] = z[70] * z[101];
z[111] = z[70] + z[111] / T(2);
z[111] = z[39] * z[111];
z[112] = z[39] * z[67];
z[113] = z[42] / T(2) + -z[43];
z[113] = z[42] * z[113];
z[113] = z[44] + z[113];
z[112] = z[112] + z[113];
z[114] = -z[9] + z[30];
z[115] = -z[5] + z[114];
z[112] = -(z[112] * z[115]);
z[116] = -z[4] + z[7];
z[117] = -(z[113] * z[116]);
z[69] = -z[56] + z[69];
z[118] = z[39] * z[69];
z[113] = -z[113] + z[118];
z[113] = z[6] * z[113];
z[111] = z[111] + z[112] + z[113] + z[117];
z[63] = -(z[5] * z[63]);
z[112] = z[3] * z[99];
z[105] = z[105] + z[112];
z[112] = z[4] * z[99];
z[63] = z[63] + z[77] + z[103] + z[105] + -z[112];
z[63] = z[26] * z[63];
z[100] = z[4] * z[100];
z[98] = -z[98] + z[99];
z[98] = z[9] * z[98];
z[98] = z[98] + z[100] + T(3) * z[107];
z[98] = z[93] + z[98] / T(2) + z[105];
z[98] = z[24] * z[98];
z[100] = z[17] + z[18] + -z[19] + -z[31];
z[103] = -z[100] / T(9) + (T(3) * z[114]) / T(4) + z[116] / T(4);
z[57] = z[57] * z[103];
z[57] = z[57] + T(2) * z[63] + -z[90] + z[98] + z[102] + z[108] + z[110] + T(3) * z[111];
z[57] = int_to_imaginary<T>(1) * z[57];
z[63] = z[41] * z[100];
z[57] = z[57] + T(3) * z[63];
z[57] = z[28] * z[57];
z[63] = T(5) * z[8];
z[98] = -z[60] + -z[62] + z[63];
z[100] = T(2) * z[14] + T(4) * z[15];
z[98] = -z[65] + -z[80] + z[98] / T(2) + z[100];
z[98] = z[3] * z[98];
z[68] = z[68] * z[85];
z[68] = z[68] + -z[77];
z[77] = z[52] + z[54];
z[85] = z[77] + -z[89];
z[81] = -z[14] + z[81];
z[81] = -z[15] + z[81] / T(2);
z[89] = z[4] + z[9];
z[89] = z[81] * z[89];
z[102] = z[5] * z[81];
z[103] = T(23) * z[8] + z[60] + T(-13) * z[62];
z[105] = (T(9) * z[11]) / T(2);
z[65] = T(5) * z[14] + -z[65] + z[103] / T(2) + -z[105];
z[65] = T(5) * z[15] + z[65] / T(2);
z[65] = z[6] * z[65];
z[103] = T(3) * z[7];
z[74] = z[74] * z[103];
z[65] = z[65] + -z[68] + z[74] + (T(3) * z[85]) / T(4) + z[89] + z[98] + z[102];
z[65] = z[27] * z[65];
z[89] = T(2) * z[1] + z[58] + -z[96] + z[104];
z[98] = z[4] + z[6];
z[89] = z[89] * z[98];
z[73] = -z[58] + z[73];
z[103] = z[73] * z[103];
z[77] = (T(-3) * z[77]) / T(2) + -z[82] + z[83] + T(-2) * z[89] + -z[93] + -z[103];
z[77] = z[0] * z[77];
z[82] = z[24] + -z[26];
z[82] = z[82] * z[106];
z[65] = z[65] + z[77] + z[82] + -z[90];
z[65] = z[27] * z[65];
z[77] = z[24] * z[92];
z[82] = z[26] * z[92];
z[81] = z[6] * z[81];
z[81] = z[81] + -z[102];
z[83] = z[9] * z[99];
z[83] = -z[81] + z[83];
z[52] = -z[50] + z[52] + z[91];
z[78] = T(5) * z[10] + -z[78] + -z[94];
z[90] = T(3) * z[14];
z[78] = (T(9) * z[1]) / T(2) + (T(-15) * z[11]) / T(2) + z[78] / T(2) + z[90];
z[92] = T(3) * z[15];
z[78] = z[78] / T(2) + z[92];
z[78] = z[4] * z[78];
z[93] = z[95] + z[97];
z[94] = z[93] + -z[99];
z[95] = z[3] / T(4);
z[94] = z[94] * z[95];
z[74] = (T(3) * z[52]) / T(4) + z[74] + z[78] + z[83] + z[94];
z[74] = z[0] * z[74];
z[74] = z[74] + -z[77] + z[82];
z[74] = z[0] * z[74];
z[75] = z[26] * z[75];
z[66] = z[50] + -z[66];
z[59] = T(2) * z[10] + -z[46] + z[59];
z[64] = z[3] + z[64];
z[59] = z[59] * z[64];
z[64] = z[59] + (T(3) * z[66]) / T(2) + z[87] + z[88];
z[66] = z[0] + -z[24];
z[64] = z[64] * z[66];
z[59] = -z[59] + z[84] + z[89];
z[59] = z[23] * z[59];
z[59] = z[59] + z[64] + z[75];
z[59] = z[23] * z[59];
z[48] = -(z[4] * z[48]);
z[47] = z[9] * z[47];
z[64] = -(z[5] * z[49]);
z[47] = z[47] + z[48] + z[50] + z[54] + -z[61] + z[64];
z[47] = z[32] * z[47];
z[48] = z[53] + z[71] + z[91];
z[53] = z[67] * z[115];
z[61] = -(z[6] * z[69]);
z[53] = z[48] / T(2) + z[53] + z[61] + -z[70];
z[53] = z[38] * z[53];
z[61] = -z[6] + z[116];
z[61] = z[61] * z[73];
z[49] = z[49] * z[101];
z[46] = T(-3) * z[10] + z[13] + z[46];
z[46] = z[46] / T(2) + -z[56];
z[46] = z[5] * z[46];
z[46] = z[46] + z[49] + z[52] / T(2) + z[61];
z[46] = z[33] * z[46];
z[46] = z[46] + z[47] / T(2) + z[53];
z[47] = -z[10] + T(-9) * z[12] + T(15) * z[13] + -z[63];
z[47] = z[47] / T(2) + z[72] + z[80] + -z[90];
z[47] = z[47] / T(2) + -z[92];
z[47] = z[9] * z[47];
z[49] = -z[93] + -z[99];
z[49] = z[49] * z[95];
z[47] = z[47] + (T(3) * z[48]) / T(4) + z[49] + z[68] + -z[81] + z[112];
z[47] = z[26] * z[47];
z[47] = z[47] + -z[77];
z[47] = z[26] * z[47];
z[48] = T(-9) * z[13] + T(7) * z[62] + -z[63];
z[48] = (T(-5) * z[1]) / T(2) + z[48] / T(2) + z[58] + z[80];
z[49] = z[5] + z[9];
z[48] = z[48] * z[49];
z[49] = T(7) * z[8] + z[60] + T(-5) * z[62];
z[49] = (T(7) * z[1]) / T(2) + z[49] / T(2) + z[58] + -z[105];
z[49] = z[49] * z[98];
z[52] = z[1] + -z[79] + -z[96] + z[100];
z[52] = z[3] * z[52];
z[48] = z[48] + z[49] + z[52] + (T(3) * z[85]) / T(2) + -z[86] + z[103];
z[48] = z[35] * z[48];
z[49] = z[36] * z[109];
z[52] = -z[12] + z[13];
z[52] = (T(-3) * z[52]) / T(2) + -z[72] + z[80] + -z[99];
z[52] = z[3] * z[52];
z[50] = z[50] + z[54] + -z[71];
z[50] = z[50] / T(2) + -z[51] + z[76];
z[50] = (T(3) * z[50]) / T(2) + z[52] + z[83] + z[112];
z[50] = prod_pow(z[24], 2) * z[50];
z[51] = z[5] + -z[6];
z[51] = (T(-85) * z[51]) / T(6) + z[114] + (T(5) * z[116]) / T(2);
z[51] = z[45] * z[51];
return T(3) * z[46] + z[47] + z[48] + z[49] + z[50] + z[51] + z[55] + z[57] + z[59] + z[65] + z[74];
}



template IntegrandConstructorType<double> f_4_169_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_169_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_169_construct (const Kin<qd_real>&);
#endif

}