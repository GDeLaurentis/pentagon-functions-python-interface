#include "f_4_185.h"

namespace PentagonFunctions {

template <typename T> T f_4_185_abbreviated (const std::array<T,16>&);



template <typename T> IntegrandConstructorType<T> f_4_185_construct (const Kin<T>& kin) {
    return [&kin, 
            dl16 = DLog_W_16<T>(kin),dl20 = DLog_W_20<T>(kin),dl4 = DLog_W_4<T>(kin),dl17 = DLog_W_17<T>(kin),dl9 = DLog_W_9<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,16> abbr = 
            {dl16(t), rlog(kin.W[19] / kin_path.W[19]), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_6(kin_path), f_2_1_8(kin_path), rlog(v_path[0] + v_path[1]), rlog(kin.W[8] / kin_path.W[8]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[16] / kin_path.W[16]), dl20(t), dl4(t), dl17(t), dl9(t)}
;

        auto result = f_4_185_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_185_abbreviated(const std::array<T,16>& abb)
{
using TR = typename T::value_type;
T z[53];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[4];
z[4] = abb[3];
z[5] = bc<TR>[0];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[11];
z[13] = bc<TR>[2];
z[14] = bc<TR>[9];
z[15] = abb[14];
z[16] = abb[15];
z[17] = abb[12];
z[18] = abb[13];
z[19] = z[11] + z[12];
z[20] = -z[9] + z[19];
z[21] = T(2) * z[18];
z[21] = z[20] * z[21];
z[22] = T(2) * z[9] + -z[10];
z[23] = T(2) * z[19];
z[24] = -z[1] + z[23];
z[25] = -z[22] + z[24];
z[25] = z[15] * z[25];
z[21] = z[21] + z[25];
z[21] = z[8] * z[21];
z[25] = z[15] + z[18];
z[26] = prod_pow(z[13], 2);
z[27] = -(z[25] * z[26]);
z[28] = z[10] / T(2);
z[29] = z[19] + z[28];
z[30] = z[9] / T(2);
z[31] = z[29] + -z[30];
z[32] = -z[1] + z[31];
z[32] = z[8] * z[32];
z[33] = z[26] / T(4);
z[32] = z[32] + z[33];
z[32] = z[0] * z[32];
z[21] = z[21] + (T(3) * z[27]) / T(2) + z[32];
z[27] = z[10] + z[23];
z[32] = T(3) * z[9];
z[34] = -z[27] + z[32];
z[35] = z[15] * z[34];
z[36] = z[18] * z[34];
z[35] = z[35] + z[36];
z[35] = T(2) * z[35];
z[37] = T(3) * z[1];
z[27] = -z[27] + z[37];
z[38] = z[0] * z[27];
z[38] = z[35] + z[38];
z[39] = z[3] * z[38];
z[40] = z[0] * z[34];
z[35] = z[35] + z[40];
z[35] = z[2] * z[35];
z[27] = z[15] * z[27];
z[41] = (T(3) * z[9]) / T(2);
z[42] = z[29] + -z[41];
z[43] = z[0] * z[42];
z[44] = -z[10] + z[19];
z[45] = z[18] * z[44];
z[27] = -z[27] + z[43] + T(2) * z[45];
z[43] = -(z[4] * z[27]);
z[46] = z[9] + -z[10];
z[47] = z[3] * z[46];
z[48] = z[8] * z[46];
z[26] = z[26] + -z[47] + z[48];
z[48] = T(3) * z[17];
z[26] = z[26] * z[48];
z[21] = T(3) * z[21] + z[26] + z[35] + z[39] + z[43];
z[21] = int_to_imaginary<T>(1) * z[21];
z[26] = (T(3) * z[13]) / T(2);
z[35] = z[26] + z[32] + -z[37] + z[44] / T(3);
z[35] = z[15] * z[35];
z[43] = (T(5) * z[9]) / T(2);
z[49] = (T(-17) * z[10]) / T(2) + z[19];
z[26] = z[26] + z[43] + z[49] / T(3);
z[26] = z[18] * z[26];
z[49] = -z[1] + (T(15) * z[13]) / T(4) + -z[29] / T(3) + z[41];
z[49] = z[0] * z[49];
z[50] = z[0] / T(2);
z[25] = -z[25] + z[50];
z[51] = int_to_imaginary<T>(1) * z[5];
z[25] = z[25] * z[51];
z[52] = T(3) * z[13] + z[46];
z[52] = z[17] * z[52];
z[25] = z[25] + z[26] + z[35] + z[49] + z[52];
z[25] = z[5] * z[25];
z[21] = z[21] + z[25] / T(2);
z[21] = z[5] * z[21];
z[25] = z[2] * z[27];
z[26] = (T(3) * z[1]) / T(2) + -z[29];
z[26] = z[15] * z[26];
z[26] = z[26] + -z[45];
z[27] = (T(9) * z[9]) / T(2) + -z[29] + -z[37];
z[27] = z[27] * z[50];
z[27] = z[26] + z[27];
z[27] = z[4] * z[27];
z[25] = z[25] + z[27] + z[39];
z[25] = z[4] * z[25];
z[27] = prod_pow(z[2], 2);
z[35] = -z[6] + -z[27] / T(2);
z[35] = z[35] * z[46];
z[39] = z[4] / T(2);
z[45] = z[39] * z[46];
z[45] = z[45] + -z[47];
z[45] = z[4] * z[45];
z[46] = z[7] * z[10];
z[49] = z[7] * z[9];
z[47] = z[2] * z[47];
z[35] = -z[14] / T(4) + z[35] + z[45] + -z[46] + z[47] + z[49];
z[35] = z[35] * z[48];
z[45] = T(2) * z[1];
z[47] = T(3) * z[19] + -z[28] + -z[30] + -z[45];
z[48] = -(z[8] * z[47]);
z[28] = -z[1] + z[19] + -z[28] + z[30];
z[30] = z[4] * z[28];
z[34] = -(z[2] * z[34]);
z[24] = -z[9] + z[24];
z[24] = z[3] * z[24];
z[30] = z[24] + z[30] + z[33] + z[34] + z[48];
z[30] = int_to_imaginary<T>(1) * z[30];
z[33] = (T(-33) * z[13]) / T(8) + T(-2) * z[22] + z[45] + z[51] / T(4);
z[33] = z[5] * z[33];
z[30] = T(3) * z[30] + z[33];
z[30] = z[5] * z[30];
z[33] = z[6] * z[47];
z[28] = -(z[2] * z[28]);
z[34] = z[39] * z[42];
z[28] = z[24] + z[28] + z[34];
z[28] = z[4] * z[28];
z[19] = (T(-3) * z[10]) / T(2) + z[19] + z[43];
z[19] = -z[1] + z[19] / T(2);
z[19] = z[19] * z[27];
z[24] = -(z[2] * z[24]);
z[19] = (T(35) * z[14]) / T(16) + z[19] + z[24] + z[28] + z[33];
z[19] = T(3) * z[19] + z[30];
z[19] = z[16] * z[19];
z[24] = -(z[2] * z[38]);
z[28] = T(2) * z[44];
z[28] = z[15] * z[28];
z[28] = z[28] + -z[36] + z[40];
z[28] = z[3] * z[28];
z[24] = z[24] + z[28];
z[24] = z[3] * z[24];
z[28] = z[7] * z[32];
z[30] = (T(23) * z[14]) / T(24) + -z[28];
z[20] = z[6] * z[20];
z[20] = T(-6) * z[20] + z[30] + T(3) * z[46];
z[20] = z[18] * z[20];
z[22] = z[22] + -z[23];
z[23] = T(3) * z[6];
z[22] = z[22] * z[23];
z[32] = z[6] + z[7];
z[32] = z[32] * z[37];
z[22] = z[22] + z[30] + z[32];
z[22] = z[15] * z[22];
z[23] = -(z[23] * z[31]);
z[30] = z[6] + -z[7];
z[30] = z[30] * z[37];
z[23] = (T(-325) * z[14]) / T(48) + z[23] + z[28] + z[30];
z[23] = z[0] * z[23];
z[28] = -z[29] + z[37] + -z[41];
z[28] = z[28] * z[50];
z[26] = z[26] + z[28];
z[26] = z[26] * z[27];
return z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[25] + z[26] + z[35];
}



template IntegrandConstructorType<double> f_4_185_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_185_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_185_construct (const Kin<qd_real>&);
#endif

}