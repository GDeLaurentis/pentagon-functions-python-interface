#include "f_4_201.h"

namespace PentagonFunctions {

template <typename T> T f_4_201_abbreviated (const std::array<T,55>&);

template <typename T> class SpDLog_f_4_201_W_10 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_201_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(-3) + T(3) * kin.v[2] + T(-3) * kin.v[4]);
c[1] = kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(-3) + T(3) * kin.v[2] + T(-3) * kin.v[4]);
c[2] = kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(-3) + T(3) * kin.v[2] + T(-3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1] + abb[5] * c[2]);
        }

        return abb[0] * (prod_pow(abb[1], 2) * ((abb[3] * T(-3)) / T(2) + (abb[4] * T(-3)) / T(2) + (abb[5] * T(-3)) / T(2)) + (prod_pow(abb[2], 2) * abb[3] * T(3)) / T(2) + prod_pow(abb[2], 2) * ((abb[4] * T(3)) / T(2) + (abb[5] * T(3)) / T(2)));
    }
};
template <typename T> class SpDLog_f_4_201_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_201_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(-3) + T(-3) * kin.v[2] + T(3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-3) + T(3) * kin.v[3] + T(3) * kin.v[4]);
c[1] = kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(-3) + T(-3) * kin.v[2] + T(3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-3) + T(3) * kin.v[3] + T(3) * kin.v[4]);
c[2] = kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(3) * kin.v[2] + T(-3) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(3) * kin.v[4]);
c[3] = kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(3) * kin.v[2] + T(-3) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[4] * c[0] + abb[9] * c[1] + abb[10] * c[2] + abb[5] * c[3]);
        }

        return abb[6] * ((abb[4] * prod_pow(abb[8], 2) * T(3)) / T(2) + prod_pow(abb[8], 2) * ((abb[5] * T(-3)) / T(2) + (abb[10] * T(-3)) / T(2) + (abb[9] * T(3)) / T(2)) + prod_pow(abb[7], 2) * ((abb[4] * T(-3)) / T(2) + (abb[9] * T(-3)) / T(2) + (abb[5] * T(3)) / T(2) + (abb[10] * T(3)) / T(2)));
    }
};
template <typename T> class SpDLog_f_4_201_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_201_W_12 (const Kin<T>& kin) {
        c[0] = -(kin.v[0] * kin.v[4]) + kin.v[2] * kin.v[4] + T(2) * kin.v[3] * kin.v[4] + (T(-13) / T(2) + (T(-43) * kin.v[4]) / T(8)) * kin.v[4] + bc<T>[0] * ((T(-3) * kin.v[4]) / T(2) + T(-3)) * int_to_imaginary<T>(1) * kin.v[4] + kin.v[1] * (T(13) / T(2) + (T(17) * kin.v[4]) / T(4) + -kin.v[2] + bc<T>[0] * int_to_imaginary<T>(3) + kin.v[0] + (T(9) / T(8) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[1] + T(-2) * kin.v[3]);
c[1] = (T(7) / T(2) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[1] + (T(-7) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(-3) * kin.v[4];
c[2] = kin.v[1] * (T(-13) / T(2) + (T(-17) * kin.v[4]) / T(4) + -kin.v[0] + bc<T>[0] * int_to_imaginary<T>(-3) + (T(-9) / T(8) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[1] + kin.v[2] + T(2) * kin.v[3]) + kin.v[0] * kin.v[4] + -(kin.v[2] * kin.v[4]) + T(-2) * kin.v[3] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + (T(13) / T(2) + (T(43) * kin.v[4]) / T(8)) * kin.v[4];
c[3] = (T(-7) / T(2) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1] + (T(7) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[9] * (t * c[0] + c[1]) + abb[3] * (t * c[2] + c[3]);
        }

        return abb[11] * ((abb[9] * abb[15] * T(-7)) / T(2) + abb[1] * abb[9] * abb[13] * T(-3) + abb[1] * (abb[9] * abb[14] + (abb[1] * abb[9] * T(-3)) / T(2) + -(abb[2] * abb[9]) + abb[9] * bc<T>[0] * int_to_imaginary<T>(3) + abb[7] * abb[9] * T(2)) + abb[3] * ((abb[15] * T(7)) / T(2) + abb[1] * (abb[2] + (abb[1] * T(3)) / T(2) + -abb[14] + abb[7] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-3)) + abb[1] * abb[13] * T(3)) + abb[12] * (abb[1] * abb[9] + abb[2] * abb[9] + abb[12] * (abb[9] / T(2) + -abb[3] / T(2)) + -(abb[9] * abb[14]) + abb[7] * abb[9] * T(-2) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[3] * (abb[14] + -abb[1] + -abb[2] + abb[13] * T(-3) + bc<T>[0] * int_to_imaginary<T>(3) + abb[7] * T(2)) + abb[9] * abb[13] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_201_W_23 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_201_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * ((T(13) * kin.v[2]) / T(2) + kin.v[3] / T(2) + T(-7) + bc<T>[0] * (T(-9) / T(2) + (T(9) * kin.v[1]) / T(2) + (T(-9) * kin.v[3]) / T(2)) * int_to_imaginary<T>(1) + bc<T>[0] * (int_to_imaginary<T>(9) / T(4)) * kin.v[0] + T(7) * kin.v[1] + T(-7) * kin.v[4]) + kin.v[4] * (T(-7) + T(-7) * kin.v[4]) + kin.v[1] * (T(-7) * kin.v[2] + T(-7) * kin.v[3] + T(7) * kin.v[4]) + (-kin.v[3] / T(2) + (T(15) * kin.v[4]) / T(2) + T(7)) * kin.v[3] + kin.v[2] * ((T(-13) * kin.v[2]) / T(2) + (T(27) * kin.v[4]) / T(2) + T(7) + T(-7) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-9) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2)) * kin.v[1] + (T(9) / T(2) + (T(-9) * kin.v[2]) / T(4) + (T(9) * kin.v[4]) / T(2)) * kin.v[2] + (T(9) / T(2) + (T(9) * kin.v[3]) / T(4)) * kin.v[3] + (T(-9) / T(2) + (T(-9) * kin.v[4]) / T(4)) * kin.v[4]);
c[1] = (T(-4) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2))) * kin.v[0] + T(4) * kin.v[2] + T(4) * kin.v[3] + bc<T>[0] * ((T(9) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) + T(-4) * kin.v[4];
c[2] = kin.v[0] * ((T(13) * kin.v[2]) / T(2) + kin.v[3] / T(2) + T(-7) + bc<T>[0] * (T(-9) / T(2) + (T(9) * kin.v[1]) / T(2) + (T(-9) * kin.v[3]) / T(2)) * int_to_imaginary<T>(1) + bc<T>[0] * (int_to_imaginary<T>(9) / T(4)) * kin.v[0] + T(7) * kin.v[1] + T(-7) * kin.v[4]) + kin.v[4] * (T(-7) + T(-7) * kin.v[4]) + kin.v[1] * (T(-7) * kin.v[2] + T(-7) * kin.v[3] + T(7) * kin.v[4]) + (-kin.v[3] / T(2) + (T(15) * kin.v[4]) / T(2) + T(7)) * kin.v[3] + kin.v[2] * ((T(-13) * kin.v[2]) / T(2) + (T(27) * kin.v[4]) / T(2) + T(7) + T(-7) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-9) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2)) * kin.v[1] + (T(9) / T(2) + (T(-9) * kin.v[2]) / T(4) + (T(9) * kin.v[4]) / T(2)) * kin.v[2] + (T(9) / T(2) + (T(9) * kin.v[3]) / T(4)) * kin.v[3] + (T(-9) / T(2) + (T(-9) * kin.v[4]) / T(4)) * kin.v[4]);
c[3] = (T(-4) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2))) * kin.v[0] + T(4) * kin.v[2] + T(4) * kin.v[3] + bc<T>[0] * ((T(9) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) + T(-4) * kin.v[4];
c[4] = kin.v[2] * ((T(13) * kin.v[2]) / T(2) + (T(-27) * kin.v[4]) / T(2) + T(-7) + T(7) * kin.v[3]) + (kin.v[3] / T(2) + (T(-15) * kin.v[4]) / T(2) + T(-7)) * kin.v[3] + kin.v[1] * (T(7) * kin.v[2] + T(7) * kin.v[3] + T(-7) * kin.v[4]) + kin.v[4] * (T(7) + T(7) * kin.v[4]) + kin.v[0] * (-kin.v[3] / T(2) + (T(-13) * kin.v[2]) / T(2) + bc<T>[0] * (T(9) / T(2) + (T(-9) * kin.v[1]) / T(2) + (T(9) * kin.v[3]) / T(2)) * int_to_imaginary<T>(1) + T(7) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(4)) * kin.v[0] + T(-7) * kin.v[1] + T(7) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(9) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2)) * kin.v[1] + (T(-9) / T(2) + (T(9) * kin.v[2]) / T(4) + (T(-9) * kin.v[4]) / T(2)) * kin.v[2] + (T(-9) / T(2) + (T(-9) * kin.v[3]) / T(4)) * kin.v[3] + (T(9) / T(2) + (T(9) * kin.v[4]) / T(4)) * kin.v[4]);
c[5] = (bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + T(4)) * kin.v[0] + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * ((T(-9) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1);
c[6] = kin.v[2] * ((T(13) * kin.v[2]) / T(2) + (T(-27) * kin.v[4]) / T(2) + T(-7) + T(7) * kin.v[3]) + (kin.v[3] / T(2) + (T(-15) * kin.v[4]) / T(2) + T(-7)) * kin.v[3] + kin.v[1] * (T(7) * kin.v[2] + T(7) * kin.v[3] + T(-7) * kin.v[4]) + kin.v[4] * (T(7) + T(7) * kin.v[4]) + kin.v[0] * (-kin.v[3] / T(2) + (T(-13) * kin.v[2]) / T(2) + bc<T>[0] * (T(9) / T(2) + (T(-9) * kin.v[1]) / T(2) + (T(9) * kin.v[3]) / T(2)) * int_to_imaginary<T>(1) + T(7) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(4)) * kin.v[0] + T(-7) * kin.v[1] + T(7) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(9) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2)) * kin.v[1] + (T(-9) / T(2) + (T(9) * kin.v[2]) / T(4) + (T(-9) * kin.v[4]) / T(2)) * kin.v[2] + (T(-9) / T(2) + (T(-9) * kin.v[3]) / T(4)) * kin.v[3] + (T(9) / T(2) + (T(9) * kin.v[4]) / T(4)) * kin.v[4]);
c[7] = (bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + T(4)) * kin.v[0] + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * ((T(-9) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[10] * (t * c[0] + c[1]) + abb[5] * (t * c[2] + c[3]) + abb[3] * (t * c[4] + c[5]) + abb[4] * (t * c[6] + c[7]);
        }

        return abb[16] * (abb[1] * abb[2] * (abb[4] / T(2) + -abb[5] / T(2) + -abb[10] / T(2)) + abb[12] * (-(abb[2] * abb[3]) / T(2) + abb[2] * (abb[5] / T(2) + abb[10] / T(2) + -abb[4] / T(2)) + abb[14] * (abb[3] / T(2) + abb[4] / T(2) + -abb[5] / T(2) + -abb[10] / T(2))) + abb[2] * abb[13] * ((abb[5] * T(-9)) / T(2) + (abb[10] * T(-9)) / T(2) + (abb[4] * T(9)) / T(2)) + abb[17] * (abb[5] * T(-4) + abb[10] * T(-4) + abb[4] * T(4)) + abb[3] * ((abb[1] * abb[2]) / T(2) + (abb[2] * abb[13] * T(9)) / T(2) + abb[2] * ((abb[2] * T(3)) / T(2) + abb[8] * T(-4) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2))) + abb[17] * T(4)) + abb[14] * (abb[1] * (abb[5] / T(2) + abb[10] / T(2) + -abb[4] / T(2)) + abb[2] * (abb[5] / T(2) + abb[10] / T(2) + -abb[4] / T(2)) + abb[13] * ((abb[4] * T(-9)) / T(2) + (abb[5] * T(9)) / T(2) + (abb[10] * T(9)) / T(2)) + abb[14] * (abb[5] + abb[10] + -abb[3] + -abb[4]) + abb[5] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[10] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[4] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[8] * (abb[5] * T(-4) + abb[10] * T(-4) + abb[4] * T(4)) + abb[3] * (-abb[1] / T(2) + -abb[2] / T(2) + (abb[13] * T(-9)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[8] * T(4))) + abb[2] * (abb[2] * ((abb[5] * T(-3)) / T(2) + (abb[10] * T(-3)) / T(2) + (abb[4] * T(3)) / T(2)) + abb[4] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[5] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[10] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[8] * (abb[4] * T(-4) + abb[5] * T(4) + abb[10] * T(4))));
    }
};
template <typename T> class SpDLog_f_4_201_W_7 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_201_W_7 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (T(13) / T(2) + (T(13) * kin.v[1]) / T(2) + (T(-47) * kin.v[4]) / T(4) + kin.v[2] + (T(-51) / T(8) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(3) * kin.v[1] + T(-3) * kin.v[4])) + (T(13) * kin.v[1] * kin.v[4]) / T(2) + kin.v[2] * kin.v[4] + (T(13) / T(2) + (T(-43) * kin.v[4]) / T(8)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + T(3) * kin.v[1] * kin.v[4]);
c[1] = (T(7) / T(2) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[3] + (T(7) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4];
c[2] = kin.v[3] * (T(13) / T(2) + (T(13) * kin.v[1]) / T(2) + (T(-47) * kin.v[4]) / T(4) + kin.v[2] + (T(-51) / T(8) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(3) * kin.v[1] + T(-3) * kin.v[4])) + (T(13) * kin.v[1] * kin.v[4]) / T(2) + kin.v[2] * kin.v[4] + (T(13) / T(2) + (T(-43) * kin.v[4]) / T(8)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + T(3) * kin.v[1] * kin.v[4]);
c[3] = (T(7) / T(2) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[3] + (T(7) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4];
c[4] = kin.v[3] * (T(13) / T(2) + (T(13) * kin.v[1]) / T(2) + (T(-47) * kin.v[4]) / T(4) + kin.v[2] + (T(-51) / T(8) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(3) * kin.v[1] + T(-3) * kin.v[4])) + (T(13) * kin.v[1] * kin.v[4]) / T(2) + kin.v[2] * kin.v[4] + (T(13) / T(2) + (T(-43) * kin.v[4]) / T(8)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + T(3) * kin.v[1] * kin.v[4]);
c[5] = (T(7) / T(2) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[3] + (T(7) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[4] * (t * c[0] + c[1]) + abb[9] * (t * c[2] + c[3]) + abb[10] * (t * c[4] + c[5]);
        }

        return abb[18] * (abb[7] * (abb[9] + abb[10]) * abb[14] + abb[19] * ((abb[9] * T(-7)) / T(2) + (abb[10] * T(-7)) / T(2)) + abb[7] * abb[13] * (abb[9] * T(-3) + abb[10] * T(-3)) + abb[7] * (abb[7] * ((abb[9] * T(-3)) / T(2) + (abb[10] * T(-3)) / T(2)) + abb[8] * (-abb[9] + -abb[10]) + abb[9] * bc<T>[0] * int_to_imaginary<T>(3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(3)) + abb[4] * (abb[7] * abb[14] + (abb[19] * T(-7)) / T(2) + abb[7] * abb[13] * T(-3) + abb[7] * ((abb[7] * T(-3)) / T(2) + -abb[8] + bc<T>[0] * int_to_imaginary<T>(3)) + abb[1] * abb[7] * T(2)) + abb[1] * abb[7] * (abb[9] * T(2) + abb[10] * T(2)) + abb[12] * (abb[7] * (abb[9] + abb[10]) + abb[8] * (abb[9] + abb[10]) + abb[12] * (abb[4] / T(2) + abb[9] / T(2) + abb[10] / T(2)) + abb[14] * (-abb[9] + -abb[10]) + abb[1] * (abb[9] * T(-2) + abb[10] * T(-2)) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[13] * (abb[9] * T(3) + abb[10] * T(3)) + abb[4] * (abb[7] + abb[8] + -abb[14] + abb[1] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-3) + abb[13] * T(3))));
    }
};
template <typename T> class SpDLog_f_4_201_W_22 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_201_W_22 (const Kin<T>& kin) {
        c[0] = ((T(13) * kin.v[2]) / T(2) + (T(-15) * kin.v[3]) / T(2) + (T(-27) * kin.v[4]) / T(2) + T(7)) * kin.v[2] + kin.v[1] * ((T(-13) * kin.v[0]) / T(2) + (T(13) * kin.v[2]) / T(2) + -kin.v[3] + bc<T>[0] * (T(9) / T(2) + (T(-9) * kin.v[0]) / T(2) + (T(9) * kin.v[3]) / T(2)) * int_to_imaginary<T>(1) + T(7) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(4)) * kin.v[1] + T(-7) * kin.v[4]) + ((T(-13) * kin.v[2]) / T(2) + (T(13) * kin.v[3]) / T(2) + (T(13) * kin.v[4]) / T(2)) * kin.v[0] + kin.v[4] * (T(-7) + T(7) * kin.v[4]) + kin.v[3] * (T(-7) + kin.v[3] + T(8) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-9) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2)) * kin.v[0] + (T(9) / T(2) + (T(9) * kin.v[2]) / T(4) + (T(-9) * kin.v[4]) / T(2)) * kin.v[2] + (T(-9) / T(2) + (T(-9) * kin.v[3]) / T(4)) * kin.v[3] + (T(-9) / T(2) + (T(9) * kin.v[4]) / T(4)) * kin.v[4]);
c[1] = (bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + T(4)) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[3] + bc<T>[0] * ((T(9) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) + T(-4) * kin.v[4];
c[2] = ((T(13) * kin.v[2]) / T(2) + (T(-15) * kin.v[3]) / T(2) + (T(-27) * kin.v[4]) / T(2) + T(7)) * kin.v[2] + kin.v[1] * ((T(-13) * kin.v[0]) / T(2) + (T(13) * kin.v[2]) / T(2) + -kin.v[3] + bc<T>[0] * (T(9) / T(2) + (T(-9) * kin.v[0]) / T(2) + (T(9) * kin.v[3]) / T(2)) * int_to_imaginary<T>(1) + T(7) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(4)) * kin.v[1] + T(-7) * kin.v[4]) + ((T(-13) * kin.v[2]) / T(2) + (T(13) * kin.v[3]) / T(2) + (T(13) * kin.v[4]) / T(2)) * kin.v[0] + kin.v[4] * (T(-7) + T(7) * kin.v[4]) + kin.v[3] * (T(-7) + kin.v[3] + T(8) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-9) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2)) * kin.v[0] + (T(9) / T(2) + (T(9) * kin.v[2]) / T(4) + (T(-9) * kin.v[4]) / T(2)) * kin.v[2] + (T(-9) / T(2) + (T(-9) * kin.v[3]) / T(4)) * kin.v[3] + (T(-9) / T(2) + (T(9) * kin.v[4]) / T(4)) * kin.v[4]);
c[3] = (bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + T(4)) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[3] + bc<T>[0] * ((T(9) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) + T(-4) * kin.v[4];
c[4] = kin.v[3] * (-kin.v[3] + T(7) + T(-8) * kin.v[4]) + ((T(13) * kin.v[2]) / T(2) + (T(-13) * kin.v[3]) / T(2) + (T(-13) * kin.v[4]) / T(2)) * kin.v[0] + kin.v[4] * (T(7) + T(-7) * kin.v[4]) + kin.v[1] * ((T(13) * kin.v[0]) / T(2) + (T(-13) * kin.v[2]) / T(2) + T(-7) + bc<T>[0] * (T(-9) / T(2) + (T(9) * kin.v[0]) / T(2) + (T(-9) * kin.v[3]) / T(2)) * int_to_imaginary<T>(1) + bc<T>[0] * (int_to_imaginary<T>(9) / T(4)) * kin.v[1] + kin.v[3] + T(7) * kin.v[4]) + ((T(-13) * kin.v[2]) / T(2) + (T(15) * kin.v[3]) / T(2) + (T(27) * kin.v[4]) / T(2) + T(-7)) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(9) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2)) * kin.v[0] + (T(-9) / T(2) + (T(-9) * kin.v[2]) / T(4) + (T(9) * kin.v[4]) / T(2)) * kin.v[2] + (T(9) / T(2) + (T(9) * kin.v[3]) / T(4)) * kin.v[3] + (T(9) / T(2) + (T(-9) * kin.v[4]) / T(4)) * kin.v[4]);
c[5] = (T(-4) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2))) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * ((T(-9) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1);
c[6] = kin.v[3] * (-kin.v[3] + T(7) + T(-8) * kin.v[4]) + ((T(13) * kin.v[2]) / T(2) + (T(-13) * kin.v[3]) / T(2) + (T(-13) * kin.v[4]) / T(2)) * kin.v[0] + kin.v[4] * (T(7) + T(-7) * kin.v[4]) + kin.v[1] * ((T(13) * kin.v[0]) / T(2) + (T(-13) * kin.v[2]) / T(2) + T(-7) + bc<T>[0] * (T(-9) / T(2) + (T(9) * kin.v[0]) / T(2) + (T(-9) * kin.v[3]) / T(2)) * int_to_imaginary<T>(1) + bc<T>[0] * (int_to_imaginary<T>(9) / T(4)) * kin.v[1] + kin.v[3] + T(7) * kin.v[4]) + ((T(-13) * kin.v[2]) / T(2) + (T(15) * kin.v[3]) / T(2) + (T(27) * kin.v[4]) / T(2) + T(-7)) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(9) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2)) * kin.v[0] + (T(-9) / T(2) + (T(-9) * kin.v[2]) / T(4) + (T(9) * kin.v[4]) / T(2)) * kin.v[2] + (T(9) / T(2) + (T(9) * kin.v[3]) / T(4)) * kin.v[3] + (T(9) / T(2) + (T(-9) * kin.v[4]) / T(4)) * kin.v[4]);
c[7] = (T(-4) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2))) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * ((T(-9) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[3] * (t * c[0] + c[1]) + abb[5] * (t * c[2] + c[3]) + abb[9] * (t * c[4] + c[5]) + abb[10] * (t * c[6] + c[7]);
        }

        return abb[20] * (abb[7] * abb[8] * (abb[5] / T(2) + -abb[9] / T(2) + -abb[10] / T(2)) + abb[12] * (abb[8] * (abb[9] / T(2) + abb[10] / T(2) + -abb[5] / T(2)) + -(abb[3] * abb[8]) / T(2) + abb[14] * (abb[3] / T(2) + abb[5] / T(2) + -abb[9] / T(2) + -abb[10] / T(2))) + abb[8] * abb[13] * ((abb[9] * T(-9)) / T(2) + (abb[10] * T(-9)) / T(2) + (abb[5] * T(9)) / T(2)) + abb[8] * (abb[8] * ((abb[9] * T(-3)) / T(2) + (abb[10] * T(-3)) / T(2) + (abb[5] * T(3)) / T(2)) + abb[5] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[9] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[10] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2))) + abb[21] * (abb[9] * T(-4) + abb[10] * T(-4) + abb[5] * T(4)) + abb[2] * abb[8] * (abb[5] * T(-4) + abb[9] * T(4) + abb[10] * T(4)) + abb[3] * ((abb[7] * abb[8]) / T(2) + (abb[8] * abb[13] * T(9)) / T(2) + abb[2] * abb[8] * T(-4) + abb[8] * ((abb[8] * T(3)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2))) + abb[21] * T(4)) + abb[14] * (abb[7] * (abb[9] / T(2) + abb[10] / T(2) + -abb[5] / T(2)) + abb[8] * (abb[9] / T(2) + abb[10] / T(2) + -abb[5] / T(2)) + abb[13] * ((abb[5] * T(-9)) / T(2) + (abb[9] * T(9)) / T(2) + (abb[10] * T(9)) / T(2)) + abb[14] * (abb[9] + abb[10] + -abb[3] + -abb[5]) + abb[9] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[10] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[5] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[3] * (-abb[7] / T(2) + -abb[8] / T(2) + (abb[13] * T(-9)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[2] * T(4)) + abb[2] * (abb[9] * T(-4) + abb[10] * T(-4) + abb[5] * T(4))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_201_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl21 = DLog_W_21<T>(kin),dl12 = DLog_W_12<T>(kin),dl23 = DLog_W_23<T>(kin),dl7 = DLog_W_7<T>(kin),dl22 = DLog_W_22<T>(kin),dl30 = DLog_W_30<T>(kin),dl1 = DLog_W_1<T>(kin),dl20 = DLog_W_20<T>(kin),dl17 = DLog_W_17<T>(kin),dl4 = DLog_W_4<T>(kin),dl31 = DLog_W_31<T>(kin),dl18 = DLog_W_18<T>(kin),dl3 = DLog_W_3<T>(kin),dl5 = DLog_W_5<T>(kin),dl19 = DLog_W_19<T>(kin),dl2 = DLog_W_2<T>(kin),dl16 = DLog_W_16<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),dl27 = DLog_W_27<T>(kin),spdl10 = SpDLog_f_4_201_W_10<T>(kin),spdl21 = SpDLog_f_4_201_W_21<T>(kin),spdl12 = SpDLog_f_4_201_W_12<T>(kin),spdl23 = SpDLog_f_4_201_W_23<T>(kin),spdl7 = SpDLog_f_4_201_W_7<T>(kin),spdl22 = SpDLog_f_4_201_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,55> abbr = 
            {dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl12(t), rlog(-v_path[1]), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), f_2_1_4(kin_path), dl23(t), f_2_1_8(kin_path), dl7(t), f_2_1_11(kin_path), dl22(t), f_2_1_14(kin_path), dl30(t) / kin_path.SqrtDelta, f_2_2_7(kin_path), rlog(kin.W[3] / kin_path.W[3]), f_2_1_7(kin_path), f_2_1_15(kin_path), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), f_2_1_3(kin_path), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), rlog(-v_path[1] + v_path[3]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl1(t), dl20(t), dl17(t), dl4(t), dl31(t), dl18(t), dl3(t), dl5(t), dl19(t), dl2(t), dl16(t), dl26(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta}
;

        auto result = f_4_201_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr) + spdl21(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_201_abbreviated(const std::array<T,55>& abb)
{
using TR = typename T::value_type;
T z[190];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[42];
z[3] = abb[43];
z[4] = abb[45];
z[5] = abb[46];
z[6] = abb[47];
z[7] = abb[49];
z[8] = abb[50];
z[9] = abb[4];
z[10] = abb[5];
z[11] = abb[9];
z[12] = abb[10];
z[13] = abb[22];
z[14] = abb[24];
z[15] = abb[27];
z[16] = abb[28];
z[17] = abb[29];
z[18] = abb[30];
z[19] = abb[35];
z[20] = abb[36];
z[21] = abb[37];
z[22] = abb[38];
z[23] = abb[39];
z[24] = abb[51];
z[25] = abb[54];
z[26] = abb[2];
z[27] = abb[41];
z[28] = abb[48];
z[29] = abb[7];
z[30] = abb[12];
z[31] = abb[13];
z[32] = abb[14];
z[33] = bc<TR>[0];
z[34] = abb[53];
z[35] = abb[8];
z[36] = abb[52];
z[37] = abb[40];
z[38] = abb[15];
z[39] = abb[17];
z[40] = abb[19];
z[41] = abb[21];
z[42] = abb[23];
z[43] = abb[25];
z[44] = abb[26];
z[45] = abb[31];
z[46] = abb[32];
z[47] = abb[33];
z[48] = abb[34];
z[49] = bc<TR>[3];
z[50] = abb[44];
z[51] = bc<TR>[1];
z[52] = bc<TR>[2];
z[53] = bc<TR>[4];
z[54] = bc<TR>[9];
z[55] = z[19] + -z[22];
z[56] = -z[23] + z[55];
z[57] = z[14] / T(4) + z[15] + -z[16] + (T(-5) * z[17]) / T(4);
z[58] = z[20] + -z[21];
z[59] = -z[58] / T(2);
z[56] = z[18] / T(4) + (T(-3) * z[56]) / T(4) + -z[57] + -z[59];
z[60] = z[34] * z[56];
z[61] = (T(3) * z[9]) / T(4);
z[62] = (T(11) * z[10]) / T(3);
z[63] = T(11) * z[1] + (T(-35) * z[11]) / T(4);
z[63] = -z[12] + z[61] + z[62] + z[63] / T(3);
z[64] = z[27] / T(2);
z[63] = z[63] * z[64];
z[65] = z[1] / T(2);
z[66] = -z[11] + z[65];
z[67] = z[9] / T(2);
z[68] = z[12] / T(2);
z[69] = -z[66] + z[67] + z[68];
z[70] = z[3] / T(2);
z[69] = z[69] * z[70];
z[71] = T(19) * z[1] + T(-25) * z[11];
z[71] = -z[9] + z[62] + z[68] + z[71] / T(6);
z[72] = z[6] / T(2);
z[71] = z[71] * z[72];
z[73] = z[18] + (T(-3) * z[55]) / T(8) + -z[57] + (T(7) * z[58]) / T(8);
z[73] = z[24] * z[73];
z[74] = -z[23] + (T(7) * z[55]) / T(8) + z[57] + (T(-3) * z[58]) / T(8);
z[74] = z[36] * z[74];
z[75] = z[7] + -z[50];
z[75] = -z[27] + T(-2) * z[75];
z[75] = T(2) * z[75];
z[76] = z[6] + z[28];
z[76] = T(2) * z[2] + -z[3] + T(5) * z[8] + (T(5) * z[76]) / T(2);
z[77] = z[75] + -z[76];
z[78] = z[4] + z[5];
z[79] = -z[24] + z[36];
z[77] = -z[25] / T(8) + (T(3) * z[34]) / T(4) + z[77] / T(9) + (T(5) * z[78]) / T(18) + (T(7) * z[79]) / T(8);
z[77] = int_to_imaginary<T>(1) * z[33] * z[77];
z[80] = -z[25] + z[34];
z[80] = -z[79] + -z[80] / T(2);
z[81] = z[51] * z[80];
z[82] = -z[55] / T(2);
z[59] = -z[18] + z[23] + z[59] + z[82];
z[83] = z[13] * z[59];
z[84] = z[83] / T(2);
z[85] = -z[1] + (T(5) * z[11]) / T(2) + z[12];
z[85] = z[67] + z[85] / T(3);
z[85] = z[7] * z[85];
z[86] = z[1] + z[11];
z[86] = z[9] + z[86] / T(2);
z[87] = (T(-19) * z[12]) / T(6) + -z[62] + -z[86];
z[88] = z[28] / T(2);
z[87] = z[87] * z[88];
z[89] = (T(3) * z[11]) / T(4);
z[62] = z[1] + (T(7) * z[9]) / T(4) + (T(-11) * z[12]) / T(3) + -z[62] + z[89];
z[90] = z[2] / T(2);
z[62] = z[62] * z[90];
z[91] = (T(13) * z[1]) / T(4) + -z[11];
z[92] = (T(13) * z[12]) / T(12) + -z[61] + -z[91] / T(3);
z[92] = z[8] * z[92];
z[93] = z[25] * z[59];
z[94] = z[10] + z[68];
z[95] = (T(3) * z[11]) / T(2) + z[67];
z[96] = -z[1] + z[95];
z[96] = -z[94] + z[96] / T(2);
z[97] = z[5] / T(2);
z[96] = z[96] * z[97];
z[98] = z[11] / T(2) + -z[67];
z[99] = -z[1] + z[98];
z[94] = z[94] + -z[99] / T(2);
z[100] = z[4] / T(2);
z[94] = z[94] * z[100];
z[99] = z[12] + z[99];
z[99] = z[37] * z[99];
z[62] = -z[60] + z[62] + z[63] + z[69] + z[71] + z[73] + z[74] + z[77] + -z[81] + -z[84] + z[85] + z[87] + z[92] + z[93] / T(4) + z[94] + z[96] + z[99];
z[62] = z[33] * z[62];
z[63] = (T(3) * z[10]) / T(2);
z[69] = z[63] + -z[68];
z[71] = T(3) * z[1];
z[73] = -z[11] + z[71];
z[73] = z[9] + z[69] + z[73] / T(2);
z[73] = z[27] * z[73];
z[74] = z[69] + -z[86];
z[77] = z[28] * z[74];
z[85] = -z[1] + z[12];
z[87] = z[11] + z[85];
z[92] = z[7] * z[87];
z[73] = z[73] + z[77] + -z[92] + z[99];
z[77] = z[36] * z[56];
z[93] = z[18] + z[58];
z[82] = z[23] / T(4) + -z[57] + z[82] + (T(3) * z[93]) / T(4);
z[93] = z[24] * z[82];
z[94] = z[77] + -z[93];
z[96] = z[60] + z[94];
z[101] = (T(3) * z[12]) / T(2);
z[102] = z[63] + z[101];
z[103] = (T(3) * z[9]) / T(2);
z[104] = z[11] + z[65] + z[103];
z[105] = z[102] + -z[104];
z[105] = z[90] * z[105];
z[106] = z[9] / T(4);
z[89] = z[85] + z[89] + -z[106];
z[107] = z[3] * z[89];
z[108] = -z[69] + z[104];
z[109] = z[72] * z[108];
z[110] = z[8] * z[87];
z[73] = z[73] / T(2) + -z[96] + -z[105] + -z[107] + z[109] + T(-2) * z[110];
z[73] = z[32] * z[73];
z[105] = z[7] / T(2);
z[107] = -z[88] + z[105];
z[107] = z[74] * z[107];
z[111] = z[24] * z[56];
z[112] = z[13] * z[56];
z[113] = T(5) * z[11];
z[114] = z[71] + -z[113];
z[114] = -z[9] + z[114] / T(2);
z[115] = z[63] + z[114];
z[116] = (T(5) * z[12]) / T(2);
z[117] = z[115] + -z[116];
z[118] = z[8] / T(2);
z[117] = z[117] * z[118];
z[119] = z[10] / T(2);
z[120] = z[68] + z[119];
z[121] = T(3) * z[11];
z[122] = -z[1] + z[121];
z[122] = z[9] + z[122] / T(2);
z[123] = z[120] + -z[122];
z[124] = z[90] * z[123];
z[125] = z[1] + z[10];
z[126] = -z[11] + z[125];
z[127] = z[27] * z[126];
z[128] = z[3] * z[126];
z[107] = z[107] + -z[111] + -z[112] + -z[117] + z[124] + -z[127] + -z[128];
z[111] = -(z[35] * z[107]);
z[117] = z[7] * z[108];
z[124] = z[104] + z[120];
z[129] = z[27] * z[124];
z[117] = z[117] + -z[129];
z[129] = z[36] * z[82];
z[130] = z[13] * z[82];
z[103] = (T(5) * z[1]) / T(2) + -z[11] + z[103];
z[131] = -z[102] + z[103];
z[131] = z[118] * z[131];
z[132] = z[10] + z[12];
z[133] = z[2] * z[132];
z[134] = z[3] * z[132];
z[109] = -z[109] + z[117] / T(2) + z[129] + z[130] + -z[131] + z[133] + z[134];
z[117] = -(z[26] * z[109]);
z[129] = z[24] + z[36];
z[129] = z[59] * z[129];
z[131] = z[70] + -z[118];
z[135] = z[9] + z[11];
z[131] = z[131] * z[135];
z[136] = -z[12] + z[98] + -z[125];
z[137] = z[2] + -z[27];
z[137] = z[136] * z[137];
z[98] = z[85] + z[98];
z[138] = z[7] * z[98];
z[131] = z[83] + -z[99] + z[129] + -z[131] + z[137] + z[138];
z[137] = z[47] * z[131];
z[138] = prod_pow(z[51], 2) * z[80];
z[137] = z[137] + -z[138];
z[138] = z[6] + -z[28];
z[136] = z[136] * z[138];
z[95] = z[85] + z[95];
z[138] = z[3] * z[95];
z[139] = z[8] * z[98];
z[140] = z[7] * z[95];
z[136] = -z[83] + -z[99] + -z[136] + -z[138] + z[139] + z[140];
z[138] = z[48] / T(2);
z[139] = -(z[136] * z[138]);
z[141] = z[63] + z[68] + z[86];
z[141] = z[28] * z[141];
z[142] = -(z[2] * z[108]);
z[143] = z[27] * z[74];
z[144] = z[3] * z[98];
z[141] = T(3) * z[92] + -z[99] + z[141] + z[142] + -z[143] + z[144];
z[142] = z[12] / T(4);
z[144] = -z[11] + z[142];
z[145] = -z[1] / T(4) + (T(-3) * z[10]) / T(4) + z[61] + -z[144];
z[145] = z[6] * z[145];
z[146] = z[87] * z[118];
z[96] = z[96] + z[141] / T(2) + z[145] + z[146];
z[96] = z[30] * z[96];
z[126] = z[6] * z[126];
z[132] = z[28] * z[132];
z[141] = z[126] + -z[132];
z[127] = z[92] + z[110] + z[127] + -z[133] + z[141];
z[127] = z[31] * z[127];
z[133] = z[90] * z[108];
z[133] = z[93] + z[133];
z[145] = z[34] * z[82];
z[145] = -z[133] + z[145];
z[108] = z[108] * z[118];
z[146] = z[128] + z[130];
z[147] = z[1] + z[9];
z[148] = T(3) * z[147];
z[149] = -z[10] + z[12];
z[150] = z[148] + -z[149];
z[150] = z[28] * z[150];
z[151] = -z[10] + z[147];
z[151] = -z[12] + T(3) * z[151];
z[151] = z[7] * z[151];
z[150] = -z[150] + z[151];
z[108] = -z[108] + z[126] + -z[145] + z[146] + z[150] / T(4);
z[126] = z[0] * z[108];
z[112] = z[77] + z[112];
z[150] = z[112] + z[134];
z[151] = -z[63] + z[101] + z[122];
z[151] = z[7] * z[151];
z[151] = -z[143] + z[151];
z[152] = -z[86] + z[119];
z[153] = z[101] + z[152];
z[153] = z[72] * z[153];
z[74] = z[74] * z[118];
z[74] = z[74] + z[132] + z[150] + z[151] / T(2) + -z[153];
z[132] = -(z[29] * z[74]);
z[151] = -(z[35] * z[56]);
z[138] = -(z[59] * z[138]);
z[138] = z[138] + z[151];
z[138] = z[34] * z[138];
z[81] = -(z[52] * z[81]);
z[81] = -z[73] + z[81] + z[96] + z[111] + z[117] + z[126] + z[127] + z[132] + -z[137] / T(2) + z[138] + z[139];
z[81] = int_to_imaginary<T>(1) * z[81];
z[75] = -z[75] + z[76] + (T(-5) * z[78]) / T(2);
z[75] = z[49] * z[75];
z[76] = int_to_imaginary<T>(1) * z[53] * z[80];
z[75] = z[75] + z[76] + z[81];
z[62] = z[62] + T(3) * z[75];
z[62] = z[33] * z[62];
z[75] = z[25] * z[82];
z[76] = -z[60] + z[75];
z[78] = T(5) * z[1];
z[80] = z[11] + z[78];
z[81] = (T(9) * z[12]) / T(2);
z[96] = (T(7) * z[10]) / T(2);
z[111] = T(3) * z[9];
z[80] = z[80] / T(2) + z[81] + -z[96] + z[111];
z[117] = z[5] / T(4);
z[126] = -(z[80] * z[117]);
z[55] = -z[55] + z[58];
z[58] = z[18] + z[23];
z[55] = (T(-5) * z[55]) / T(8) + z[57] + -z[58] / T(2);
z[57] = T(-3) * z[79];
z[55] = z[55] * z[57];
z[57] = (T(7) * z[9]) / T(2) + z[65] + z[121];
z[58] = z[57] + -z[102];
z[127] = z[6] / T(4);
z[132] = z[58] * z[127];
z[83] = (T(3) * z[83]) / T(4);
z[106] = (T(5) * z[11]) / T(4) + z[85] + z[106];
z[106] = z[7] * z[106];
z[137] = z[86] + z[119];
z[137] = -z[68] + T(3) * z[137];
z[138] = z[28] / T(4);
z[139] = z[137] * z[138];
z[61] = -z[11] / T(4) + z[61] + -z[85];
z[61] = z[3] * z[61];
z[151] = T(2) * z[12];
z[153] = z[10] + (T(-15) * z[135]) / T(8) + z[151];
z[153] = z[2] * z[153];
z[154] = T(5) * z[12];
z[155] = -z[78] + z[154];
z[156] = (T(11) * z[9]) / T(4);
z[157] = (T(9) * z[11]) / T(4) + z[155] + -z[156];
z[157] = z[118] * z[157];
z[158] = T(2) * z[1];
z[159] = (T(-23) * z[9]) / T(8) + -z[10] + (T(-7) * z[11]) / T(8) + -z[158];
z[159] = z[27] * z[159];
z[55] = z[55] + (T(7) * z[61]) / T(2) + (T(-3) * z[76]) / T(2) + -z[83] + z[106] + z[126] + z[132] + z[139] + z[153] + z[157] + z[159];
z[55] = z[32] * z[55];
z[61] = T(9) * z[11];
z[76] = z[61] + z[78];
z[106] = T(7) * z[9];
z[76] = z[76] / T(2) + z[106];
z[126] = (T(9) * z[10]) / T(2);
z[132] = -z[68] + z[126];
z[139] = z[76] + -z[132];
z[139] = z[118] * z[139];
z[153] = T(3) * z[56];
z[157] = z[24] * z[153];
z[159] = z[13] * z[153];
z[160] = -z[68] + T(3) * z[152];
z[160] = z[88] * z[160];
z[160] = z[159] + z[160];
z[161] = z[86] + z[116] + -z[126];
z[161] = z[105] * z[161];
z[158] = z[11] + z[111] + z[158];
z[162] = T(3) * z[10];
z[163] = z[154] + z[162];
z[164] = -z[158] + z[163];
z[164] = z[3] * z[164];
z[165] = -z[116] + -z[119] + T(3) * z[122];
z[165] = z[90] * z[165];
z[166] = T(2) * z[9];
z[167] = z[1] + z[121];
z[168] = z[166] + z[167] / T(2);
z[169] = -z[63] + z[168];
z[169] = z[6] * z[169];
z[170] = T(7) * z[1];
z[171] = -z[113] + z[170];
z[172] = (T(5) * z[10]) / T(2);
z[171] = z[9] + z[171] / T(2) + z[172];
z[171] = z[27] * z[171];
z[161] = -z[139] + z[157] + z[160] + z[161] + z[164] + z[165] + -z[169] + z[171];
z[161] = z[35] * z[161];
z[164] = z[68] + z[172];
z[165] = T(3) * z[104];
z[171] = z[164] + -z[165];
z[171] = z[2] * z[171];
z[58] = z[6] * z[58];
z[173] = z[86] + z[172];
z[174] = -z[101] + z[173];
z[174] = z[27] * z[174];
z[137] = z[28] * z[137];
z[175] = T(3) * z[99];
z[176] = z[3] * z[135];
z[58] = z[58] + T(7) * z[92] + z[137] + z[171] + -z[174] + -z[175] + (T(-3) * z[176]) / T(2);
z[92] = z[36] * z[153];
z[137] = z[34] * z[153];
z[177] = z[92] + z[137];
z[82] = T(3) * z[82];
z[178] = z[24] * z[82];
z[58] = z[58] / T(2) + T(3) * z[110] + z[177] + -z[178];
z[58] = z[30] * z[58];
z[110] = z[35] * z[137];
z[137] = T(2) * z[11];
z[179] = T(2) * z[10];
z[180] = z[1] + z[111] + z[137] + -z[179];
z[180] = z[5] * z[180];
z[181] = z[35] * z[180];
z[110] = z[110] + -z[181];
z[55] = z[55] + z[58] + -z[110] + z[161];
z[55] = z[32] * z[55];
z[56] = z[25] * z[56];
z[58] = z[56] + z[94];
z[94] = z[68] + z[122];
z[94] = z[7] * z[94];
z[161] = -z[174] + z[175];
z[174] = T(5) * z[10];
z[148] = z[148] + z[174];
z[175] = z[12] + T(3) * z[148];
z[175] = z[88] * z[175];
z[161] = z[94] + z[161] / T(2) + z[175];
z[175] = (T(19) * z[10]) / T(2);
z[182] = (T(9) * z[9]) / T(2) + -z[175];
z[65] = -z[65] + z[101] + z[113] + z[182];
z[117] = z[65] * z[117];
z[183] = z[34] * z[82];
z[184] = -z[1] + T(15) * z[11];
z[106] = (T(-15) * z[10]) / T(2) + -z[81] + z[106] + z[184] / T(2);
z[106] = z[72] * z[106];
z[184] = z[89] * z[118];
z[185] = z[1] + z[113];
z[185] = z[111] + z[185] / T(2);
z[186] = z[68] + -z[185];
z[186] = z[3] * z[186];
z[58] = (T(3) * z[58]) / T(2) + z[83] + z[106] + z[117] + z[161] / T(2) + z[171] / T(4) + z[183] + z[184] + z[186];
z[58] = z[30] * z[58];
z[83] = -z[1] + z[113];
z[83] = z[83] / T(2) + z[166] + -z[172];
z[83] = z[27] * z[83];
z[57] = -z[57] + z[164];
z[57] = z[7] * z[57];
z[106] = z[25] * z[153];
z[113] = -z[68] + z[96];
z[117] = -z[113] + z[185];
z[161] = z[70] * z[117];
z[164] = z[90] * z[149];
z[171] = z[164] + z[169];
z[184] = z[1] + z[61];
z[132] = T(5) * z[9] + -z[132] + z[184] / T(2);
z[132] = z[118] * z[132];
z[57] = -z[57] + z[83] + z[106] + z[132] + -z[160] + -z[161] + z[171];
z[57] = z[35] * z[57];
z[57] = z[57] + z[110];
z[58] = -z[57] + z[58];
z[58] = z[30] * z[58];
z[83] = -z[113] + z[165];
z[83] = z[83] * z[90];
z[83] = z[83] + z[178] + -z[180];
z[106] = z[13] * z[82];
z[110] = (T(7) * z[1]) / T(2) + (T(13) * z[9]) / T(2) + -z[68] + z[121] + -z[126];
z[110] = z[105] * z[110];
z[61] = -z[61] + z[78];
z[61] = z[61] / T(2) + z[126] + -z[166];
z[61] = z[6] * z[61];
z[132] = T(9) * z[10];
z[160] = z[132] + -z[147];
z[161] = -z[154] + z[160];
z[184] = z[8] / T(4);
z[161] = z[161] * z[184];
z[186] = z[27] * z[152];
z[125] = -z[9] + -z[125];
z[125] = T(9) * z[125] + z[154];
z[125] = z[125] * z[138];
z[158] = z[12] + z[158] + z[162];
z[158] = z[3] * z[158];
z[61] = z[61] + z[83] + z[106] + z[110] + z[125] + z[158] + z[161] + -z[183] + -z[186];
z[61] = z[30] * z[61];
z[104] = -z[104] + z[113];
z[104] = z[70] * z[104];
z[110] = -z[12] + z[162];
z[113] = z[88] * z[110];
z[125] = z[113] + z[186];
z[104] = z[104] + -z[125];
z[158] = -z[1] + z[10];
z[144] = -z[144] + z[156] + (T(-7) * z[158]) / T(4);
z[144] = z[7] * z[144];
z[156] = z[78] + z[121];
z[158] = T(4) * z[9];
z[156] = -z[68] + z[156] / T(2) + z[158];
z[156] = z[8] * z[156];
z[183] = z[25] * z[82];
z[186] = z[78] + -z[121];
z[186] = z[9] + z[63] + z[186] / T(2);
z[186] = z[6] * z[186];
z[83] = z[83] + z[104] + z[144] + z[156] + z[183] + z[186];
z[83] = z[32] * z[83];
z[144] = (T(3) * z[1]) / T(2);
z[156] = (T(5) * z[9]) / T(2) + z[11] + z[144];
z[183] = -z[68] + z[156];
z[186] = z[119] + z[183];
z[187] = z[97] * z[186];
z[75] = -z[75] + z[187];
z[183] = z[63] + -z[183];
z[183] = z[105] * z[183];
z[67] = z[67] + z[69];
z[144] = -z[11] + z[67] + z[144];
z[187] = -(z[72] * z[144]);
z[66] = z[66] + -z[67];
z[66] = z[66] * z[118];
z[66] = z[66] + z[75] + -z[133] + -z[146] + z[183] + z[187];
z[66] = z[0] * z[66];
z[67] = T(3) * z[31];
z[108] = -(z[67] * z[108]);
z[61] = z[61] + (T(3) * z[66]) / T(2) + z[83] + z[108];
z[61] = z[0] * z[61];
z[66] = -z[111] + z[151];
z[83] = z[66] + -z[78] + z[137] + -z[162];
z[83] = z[3] * z[83];
z[108] = T(3) * z[12];
z[133] = z[108] + -z[162];
z[146] = z[133] + z[147];
z[127] = z[127] * z[146];
z[127] = -z[106] + z[127];
z[146] = -(z[36] * z[82]);
z[151] = (T(5) * z[1]) / T(4) + (T(13) * z[9]) / T(4) + z[137];
z[183] = (T(3) * z[12]) / T(4);
z[187] = z[10] / T(4) + z[183];
z[188] = z[151] + z[187];
z[188] = z[27] * z[188];
z[151] = (T(9) * z[10]) / T(4) + -z[142] + -z[151];
z[151] = z[7] * z[151];
z[189] = T(-7) * z[12] + -z[174];
z[189] = z[90] * z[189];
z[83] = z[83] + -z[113] + z[127] + z[146] + z[151] + -z[161] + z[180] + z[188] + z[189];
z[83] = z[32] * z[83];
z[103] = (T(11) * z[10]) / T(2) + -z[101] + z[103];
z[103] = z[97] * z[103];
z[113] = -z[68] + z[173];
z[113] = z[7] * z[113];
z[146] = -z[12] + z[160];
z[146] = z[146] * z[184];
z[151] = z[12] + z[174];
z[151] = z[90] * z[151];
z[103] = -z[103] + -z[104] + z[113] + -z[127] + z[146] + z[151];
z[104] = z[0] + -z[30];
z[103] = z[103] * z[104];
z[109] = z[67] * z[109];
z[113] = z[125] + z[171];
z[125] = z[111] + -z[137] + -z[155];
z[125] = z[3] * z[125];
z[127] = -z[121] + z[170];
z[137] = (T(7) * z[12]) / T(2);
z[127] = z[127] / T(2) + -z[137] + z[166];
z[127] = z[8] * z[127];
z[146] = -z[68] + z[86];
z[146] = z[7] * z[146];
z[125] = z[113] + z[125] + z[127] + z[146];
z[125] = z[35] * z[125];
z[127] = -z[6] + -z[8];
z[127] = z[127] * z[147];
z[146] = -(z[7] * z[10]);
z[127] = z[127] + z[134] + z[146];
z[127] = z[26] * z[127];
z[104] = -z[32] + z[104];
z[134] = -z[35] + -z[104];
z[146] = -z[12] + z[179];
z[134] = z[4] * z[134] * z[146];
z[83] = z[83] + z[103] + z[109] + z[125] + (T(3) * z[127]) / T(2) + z[134] + -z[181];
z[83] = z[26] * z[83];
z[103] = -z[86] + z[96] + -z[101];
z[103] = z[27] * z[103];
z[109] = z[122] + z[137];
z[125] = -z[109] + z[126];
z[125] = z[7] * z[125];
z[127] = -z[132] + -z[154];
z[127] = z[28] * z[127];
z[125] = z[103] + z[125] + z[127];
z[127] = T(4) * z[11];
z[66] = -z[1] + -z[66] + z[127] + -z[162];
z[66] = z[3] * z[66];
z[76] = -z[76] + z[81] + z[126];
z[76] = z[72] * z[76];
z[66] = z[66] + z[76] + -z[92] + z[125] / T(2) + z[139] + -z[159] + -z[164] + -z[180];
z[66] = z[30] * z[66];
z[76] = z[3] * z[117];
z[109] = z[96] + -z[109];
z[109] = z[7] * z[109];
z[117] = -(z[28] * z[163]);
z[125] = -(z[2] * z[149]);
z[76] = z[76] + z[103] + z[109] + z[117] + z[125];
z[103] = z[81] + -z[111];
z[109] = z[1] + T(-7) * z[11];
z[109] = z[103] + z[109] / T(2) + z[119];
z[109] = z[97] * z[109];
z[117] = -z[116] + z[168];
z[117] = z[8] * z[117];
z[76] = z[76] / T(2) + -z[92] + z[109] + z[117] + -z[169];
z[76] = z[32] * z[76];
z[92] = -z[85] + -z[111] + -z[127];
z[92] = z[3] * z[92];
z[109] = z[68] + -z[168];
z[109] = z[8] * z[109];
z[92] = z[92] + -z[94] + z[109] + z[113] + z[180];
z[92] = z[0] * z[92];
z[94] = -z[102] + z[185];
z[109] = z[94] * z[97];
z[113] = z[9] + z[167] / T(4);
z[117] = -z[113] + z[187];
z[125] = z[4] * z[117];
z[56] = z[56] + z[60] + -z[109] + z[125];
z[109] = z[1] + -z[11];
z[125] = -z[109] + z[133];
z[105] = z[105] * z[125];
z[125] = -z[86] + z[102];
z[127] = z[28] * z[125];
z[105] = z[105] + z[127] + -z[143];
z[127] = z[69] + -z[185];
z[127] = z[118] * z[127];
z[105] = -z[56] + z[105] / T(2) + z[127] + z[150];
z[105] = z[29] * z[105];
z[74] = z[67] * z[74];
z[104] = z[104] * z[146];
z[113] = T(3) * z[113];
z[127] = (T(11) * z[10]) / T(4) + (T(5) * z[12]) / T(4) + -z[113];
z[127] = z[35] * z[127];
z[104] = z[104] + z[127];
z[104] = z[4] * z[104];
z[57] = z[57] + z[66] + z[74] + z[76] + z[92] + z[104] + (T(3) * z[105]) / T(2);
z[57] = z[29] * z[57];
z[66] = -(z[7] * z[89]);
z[74] = z[101] + z[114] + z[172];
z[74] = z[72] * z[74];
z[76] = -z[108] + -z[148];
z[76] = z[76] * z[138];
z[92] = -(z[135] * z[184]);
z[104] = -(z[3] * z[87]);
z[66] = z[66] + z[74] + z[76] + -z[77] + -z[84] + z[92] + z[104] + z[143] / T(2) + -z[145];
z[66] = z[30] * z[66];
z[60] = z[60] + z[107];
z[60] = z[35] * z[60];
z[74] = -z[10] + z[95];
z[74] = z[27] * z[74];
z[74] = z[74] + -z[99] + z[129] + -z[140];
z[76] = -(z[8] * z[89]);
z[77] = z[10] + z[98];
z[77] = z[77] * z[90];
z[74] = z[74] / T(2) + z[76] + z[77] + z[84] + -z[141] + -z[176] / T(4);
z[74] = z[31] * z[74];
z[60] = z[60] + z[66] + z[73] + z[74] / T(2);
z[60] = z[60] * z[67];
z[65] = z[65] * z[97];
z[66] = -z[108] + T(9) * z[147];
z[67] = T(19) * z[10];
z[73] = -z[66] + z[67];
z[73] = z[7] * z[73];
z[66] = z[66] + z[67];
z[66] = z[28] * z[66];
z[66] = z[66] + z[73];
z[67] = (T(19) * z[10]) / T(4);
z[73] = (T(-9) * z[9]) / T(4) + -z[67] + -z[91] + z[183];
z[73] = z[3] * z[73];
z[74] = -z[101] + -z[165] + z[175];
z[74] = z[74] * z[90];
z[76] = T(11) * z[11];
z[77] = (T(-13) * z[1]) / T(2) + z[76] + -z[101] + z[182];
z[77] = z[72] * z[77];
z[84] = z[8] * z[109];
z[65] = z[65] + z[66] / T(4) + z[73] + z[74] + z[77] + z[84] + -z[178];
z[65] = z[38] * z[65];
z[66] = -(z[94] * z[118]);
z[64] = z[64] * z[123];
z[73] = z[7] * z[117];
z[74] = z[88] * z[125];
z[56] = -z[56] + z[64] + z[66] + z[73] + z[74] + z[112];
z[56] = z[44] * z[56];
z[64] = -(z[2] * z[124]);
z[66] = -(z[7] * z[186]);
z[73] = -(z[6] * z[144]);
z[69] = z[69] + z[156];
z[74] = -(z[8] * z[69]);
z[64] = z[64] + z[66] + z[73] + z[74];
z[64] = z[64] / T(2) + z[75] + -z[93] + -z[130];
z[64] = z[43] * z[64];
z[56] = z[56] + z[64];
z[64] = (T(17) * z[10]) / T(2);
z[66] = T(-17) * z[1] + z[76];
z[66] = -z[64] + z[66] / T(2) + z[101] + -z[111];
z[66] = z[27] * z[66];
z[73] = z[76] + -z[78];
z[73] = z[73] / T(2) + -z[103] + -z[172];
z[73] = z[3] * z[73];
z[63] = -z[63] + z[86];
z[74] = (T(11) * z[12]) / T(2);
z[63] = T(3) * z[63] + z[74];
z[63] = z[28] * z[63];
z[75] = z[119] + -z[122];
z[74] = z[74] + T(3) * z[75];
z[74] = z[2] * z[74];
z[75] = (T(23) * z[12]) / T(2) + T(-3) * z[115];
z[75] = z[8] * z[75];
z[76] = z[10] + -z[87];
z[76] = z[7] * z[76];
z[63] = z[63] + z[66] + z[73] + z[74] + z[75] + z[76];
z[66] = -(z[80] * z[97]);
z[63] = z[63] / T(2) + z[66] + -z[157] + -z[159] + z[177];
z[63] = z[41] * z[63];
z[66] = z[46] * z[131];
z[73] = z[8] + z[28];
z[73] = z[12] * z[73];
z[74] = z[10] + -z[135];
z[74] = z[7] * z[74];
z[73] = z[73] + z[74] + -z[128];
z[73] = prod_pow(z[35], 2) * z[73];
z[66] = z[66] + z[73];
z[73] = -z[67] + z[158];
z[74] = T(13) * z[11] + z[71];
z[74] = z[73] + z[74] / T(4) + z[183];
z[74] = z[27] * z[74];
z[75] = T(19) * z[11];
z[76] = -z[71] + z[75];
z[73] = (T(9) * z[12]) / T(4) + z[73] + z[76] / T(4);
z[73] = z[7] * z[73];
z[71] = z[71] + z[75];
z[71] = T(11) * z[9] + z[71] / T(2) + -z[81] + -z[175];
z[71] = z[71] * z[72];
z[72] = (T(13) * z[12]) / T(2) + z[175];
z[75] = z[72] + T(3) * z[86];
z[75] = z[75] * z[88];
z[72] = z[72] + T(-3) * z[185];
z[70] = z[70] * z[72];
z[72] = -z[12] + -z[135];
z[72] = z[8] * z[72];
z[70] = z[70] + z[71] + z[72] + z[73] + z[74] + z[75];
z[70] = z[40] * z[70];
z[71] = (T(3) * z[45]) / T(2);
z[72] = -(z[71] * z[136]);
z[67] = -z[67] + z[113] + -z[142];
z[73] = -(z[40] * z[67]);
z[67] = z[30] * z[67];
z[67] = -z[67] / T(2) + -z[127];
z[67] = z[30] * z[67];
z[74] = T(3) * z[156];
z[75] = -z[74] + -z[120];
z[75] = z[32] * z[75];
z[76] = z[0] * z[69];
z[77] = -(z[30] * z[146]);
z[75] = z[75] / T(2) + (T(3) * z[76]) / T(4) + z[77];
z[75] = z[0] * z[75];
z[69] = z[43] * z[69];
z[74] = z[74] + z[116];
z[76] = z[74] + -z[96];
z[77] = z[32] * z[76];
z[78] = z[35] * z[146];
z[77] = z[77] / T(4) + z[78];
z[77] = z[32] * z[77];
z[67] = z[67] + (T(3) * z[69]) / T(2) + z[73] + z[75] + z[77];
z[67] = z[4] * z[67];
z[69] = (T(11) * z[1]) / T(2) + z[121];
z[73] = (T(17) * z[9]) / T(2);
z[75] = -z[69] + -z[73] + -z[102];
z[75] = z[27] * z[75];
z[73] = z[73] + -z[126];
z[69] = -z[69] + -z[73] + -z[101];
z[69] = z[6] * z[69];
z[73] = (T(-23) * z[1]) / T(2) + -z[73] + z[81] + z[121];
z[73] = z[8] * z[73];
z[74] = z[74] + z[172];
z[74] = z[3] * z[74];
z[64] = (T(17) * z[12]) / T(2) + z[64] + -z[165];
z[64] = z[2] * z[64];
z[77] = z[9] + -z[10] + -z[85];
z[77] = z[7] * z[77];
z[64] = z[64] + z[69] + z[73] + z[74] + z[75] + z[77];
z[69] = -z[25] + z[36];
z[69] = z[69] * z[82];
z[73] = z[76] * z[100];
z[64] = z[64] / T(2) + z[69] + z[73] + z[106] + -z[178];
z[64] = z[39] * z[64];
z[69] = z[40] * z[153];
z[73] = z[38] * z[82];
z[59] = -(z[59] * z[71]);
z[71] = z[42] * z[110];
z[59] = z[59] + z[69] + z[71] / T(2) + z[73];
z[59] = z[34] * z[59];
z[68] = z[68] + -z[162] + z[168];
z[68] = z[13] * z[68];
z[71] = z[24] * z[149];
z[68] = z[68] + -z[71] / T(2);
z[68] = z[42] * z[68];
z[71] = z[42] * z[146];
z[71] = z[69] + z[71];
z[71] = z[25] * z[71];
z[73] = z[42] * z[152];
z[69] = z[69] + z[73];
z[69] = z[36] * z[69];
z[73] = (T(73) * z[25]) / T(3) + T(77) * z[79];
z[73] = (T(76) * z[34]) / T(3) + z[73] / T(4);
z[73] = z[54] * z[73];
return z[55] + T(3) * z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + (T(3) * z[66]) / T(2) + z[67] + z[68] + z[69] + z[70] + z[71] + z[72] + z[73] + z[83];
}



template IntegrandConstructorType<double> f_4_201_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_201_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_201_construct (const Kin<qd_real>&);
#endif

}