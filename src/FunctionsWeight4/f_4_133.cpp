#include "f_4_133.h"

namespace PentagonFunctions {

template <typename T> T f_4_133_abbreviated (const std::array<T,18>&);

template <typename T> class SpDLog_f_4_133_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_133_W_22 (const Kin<T>& kin) {
        c[0] = (T(-3) / T(2) + (T(-9) * kin.v[3]) / T(8) + (T(-3) * kin.v[4]) / T(4)) * kin.v[3] + (T(3) / T(2) + (T(3) * kin.v[2]) / T(8) + (T(3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(4)) * kin.v[2] + (T(3) / T(2) + (T(-3) * kin.v[0]) / T(2) + (T(-9) * kin.v[1]) / T(8) + (T(-3) * kin.v[2]) / T(4) + (T(9) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(4)) * kin.v[1] + (T(-3) / T(2) + (T(3) * kin.v[4]) / T(8)) * kin.v[4] + ((T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2)) * kin.v[0];
c[1] = kin.v[3] * (T(-3) / T(2) + (T(-21) * kin.v[4]) / T(2) + T(-6) * kin.v[3]) + (T(-3) / T(2) + (T(-9) * kin.v[4]) / T(2)) * kin.v[4] + ((T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2)) * kin.v[0] + kin.v[2] * (T(3) / T(2) + (T(-9) * kin.v[2]) / T(2) + (T(21) * kin.v[3]) / T(2) + T(9) * kin.v[4]) + kin.v[1] * (T(3) / T(2) + (T(-3) * kin.v[0]) / T(2) + (T(-21) * kin.v[2]) / T(2) + (T(21) * kin.v[4]) / T(2) + T(-6) * kin.v[1] + T(12) * kin.v[3]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (((T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2)) * kin.v[0] + (T(3) / T(2) + (T(-3) * kin.v[0]) / T(2) + (T(-9) * kin.v[1]) / T(8) + (T(-3) * kin.v[2]) / T(4) + (T(9) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(4)) * kin.v[1] + (T(3) / T(2) + (T(3) * kin.v[2]) / T(8) + (T(3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(4)) * kin.v[2] + (T(-3) / T(2) + (T(-9) * kin.v[3]) / T(8) + (T(-3) * kin.v[4]) / T(4)) * kin.v[3] + (T(-3) / T(2) + (T(3) * kin.v[4]) / T(8)) * kin.v[4]) + rlog(kin.v[2]) * (((T(-15) * kin.v[1]) / T(4) + (T(3) * kin.v[2]) / T(2) + (T(15) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(9) + T(-9) * kin.v[0]) * kin.v[1] + ((T(21) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(-21) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + ((T(-15) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + ((T(21) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + kin.v[0] * (T(-9) * kin.v[2] + T(9) * kin.v[3] + T(9) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (((T(-15) * kin.v[1]) / T(4) + (T(3) * kin.v[2]) / T(2) + (T(15) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(9) + T(-9) * kin.v[0]) * kin.v[1] + ((T(21) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(-21) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + ((T(-15) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + ((T(21) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + kin.v[0] * (T(-9) * kin.v[2] + T(9) * kin.v[3] + T(9) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (((T(15) * kin.v[1]) / T(4) + (T(-3) * kin.v[2]) / T(2) + (T(-15) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(-9) + T(9) * kin.v[0]) * kin.v[1] + ((T(-21) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(21) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + ((T(15) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + ((T(-21) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + kin.v[0] * (T(9) * kin.v[2] + T(-9) * kin.v[3] + T(-9) * kin.v[4]));
c[2] = (T(-3) * kin.v[1]) / T(2) + (T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2);
c[3] = rlog(kin.v[2]) * (T(3) * kin.v[1] + T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (T(3) * kin.v[1] + T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(-3) * kin.v[1]) / T(2) + (T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2)) * rlog(-kin.v[2] + kin.v[0] + kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (T(-3) * kin.v[1] + T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[0] * (prod_pow(abb[2], 2) * ((abb[6] * T(-9)) / T(2) + (abb[8] * T(-3)) / T(4) + (abb[7] * T(3)) / T(4) + (abb[5] * T(9)) / T(2)) + abb[4] * ((prod_pow(abb[2], 2) * T(9)) / T(2) + abb[3] * T(3)) + abb[3] * ((abb[7] * T(-3)) / T(2) + (abb[8] * T(3)) / T(2) + abb[6] * T(-3) + abb[5] * T(3)) + abb[1] * (abb[1] * ((abb[7] * T(-9)) / T(4) + (abb[4] * T(-3)) / T(2) + (abb[5] * T(-3)) / T(2) + (abb[6] * T(3)) / T(2) + (abb[8] * T(9)) / T(4)) + abb[2] * abb[4] * T(-3) + abb[2] * ((abb[8] * T(-3)) / T(2) + (abb[7] * T(3)) / T(2) + abb[5] * T(-3) + abb[6] * T(3))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_133_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl20 = DLog_W_20<T>(kin),dl3 = DLog_W_3<T>(kin),dl16 = DLog_W_16<T>(kin),dl19 = DLog_W_19<T>(kin),dl8 = DLog_W_8<T>(kin),spdl22 = SpDLog_f_4_133_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,18> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), -rlog(t), dl20(t), rlog(kin.W[7] / kin_path.W[7]), dl3(t), rlog(v_path[2]), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl16(t), dl19(t), dl8(t)}
;

        auto result = f_4_133_abbreviated(abbr);
        result = result + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_133_abbreviated(const std::array<T,18>& abb)
{
using TR = typename T::value_type;
T z[37];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[9];
z[3] = abb[11];
z[4] = abb[15];
z[5] = abb[16];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[10];
z[11] = abb[2];
z[12] = abb[17];
z[13] = abb[12];
z[14] = bc<TR>[0];
z[15] = abb[3];
z[16] = abb[13];
z[17] = abb[14];
z[18] = bc<TR>[2];
z[19] = bc<TR>[9];
z[20] = z[1] + z[6];
z[21] = z[7] + z[10];
z[22] = -z[20] + z[21] / T(2);
z[22] = z[3] * z[22];
z[23] = z[8] + -z[9];
z[24] = -z[10] + z[20];
z[25] = -z[23] / T(2) + -z[24];
z[26] = z[2] / T(2);
z[27] = -(z[25] * z[26]);
z[28] = T(3) * z[7] + -z[10];
z[29] = -z[20] + z[28] / T(2);
z[30] = z[23] + z[29];
z[30] = z[4] * z[30];
z[31] = -(z[5] * z[29]);
z[22] = z[22] + z[27] + z[30] + z[31];
z[22] = z[0] * z[22];
z[25] = z[2] * z[25];
z[27] = T(2) * z[20];
z[21] = -z[21] + z[27];
z[30] = z[3] + z[4];
z[31] = -z[12] + z[30];
z[32] = z[21] * z[31];
z[32] = -z[25] + z[32];
z[32] = z[11] * z[32];
z[22] = z[22] + z[32];
z[22] = z[0] * z[22];
z[27] = z[27] + -z[28];
z[28] = z[5] * z[27];
z[21] = z[12] * z[21];
z[32] = -z[7] + z[10];
z[33] = z[3] * z[32];
z[34] = z[4] * z[32];
z[35] = z[33] + z[34];
z[21] = z[21] + -z[28] + T(2) * z[35];
z[28] = -(z[16] * z[21]);
z[35] = z[0] * z[21];
z[36] = -z[5] + z[12];
z[36] = z[29] * z[36];
z[33] = -z[33] + z[36];
z[33] = z[13] * z[33];
z[33] = z[33] + z[35];
z[33] = z[13] * z[33];
z[31] = z[29] * z[31];
z[24] = (T(-7) * z[23]) / T(2) + T(-3) * z[24];
z[24] = z[24] * z[26];
z[24] = z[24] + z[31];
z[24] = prod_pow(z[11], 2) * z[24];
z[26] = z[15] * z[27];
z[27] = (T(9) * z[19]) / T(4) + z[26];
z[27] = -(z[27] * z[30]);
z[25] = z[15] * z[25];
z[26] = (T(25) * z[19]) / T(8) + z[26];
z[26] = z[5] * z[26];
z[31] = z[12] * z[19];
z[22] = z[22] + z[24] + z[25] + z[26] + z[27] + z[28] + (T(-7) * z[31]) / T(8) + z[33];
z[24] = (T(3) * z[18]) / T(2) + -z[20];
z[25] = T(5) * z[7] + T(-3) * z[10];
z[25] = -z[23] / T(4) + z[24] + z[25] / T(2);
z[25] = z[4] * z[25];
z[26] = (T(-15) * z[18]) / T(4) + z[29];
z[26] = z[5] * z[26];
z[24] = -z[7] + T(2) * z[10] + z[24];
z[24] = z[3] * z[24];
z[23] = z[2] * z[23];
z[20] = T(-2) * z[7] + z[10] + z[20];
z[20] = (T(9) * z[18]) / T(4) + T(2) * z[20];
z[20] = z[12] * z[20];
z[20] = z[20] + z[23] / T(4) + z[24] + z[25] + z[26];
z[23] = prod_pow(z[14], 2);
z[20] = z[20] * z[23];
z[24] = -z[0] + z[17];
z[21] = z[21] * z[24];
z[24] = z[5] + z[12];
z[24] = -z[24] / T(2) + z[30];
z[24] = prod_pow(z[18], 2) * z[24];
z[25] = z[12] * z[32];
z[25] = z[25] + -z[34];
z[25] = z[13] * z[25];
z[21] = z[21] + z[24] + T(2) * z[25];
z[24] = (T(3) * z[5]) / T(2) + (T(-5) * z[12]) / T(2) + z[30];
z[23] = z[23] * z[24];
z[21] = T(3) * z[21] + z[23] / T(2);
z[21] = int_to_imaginary<T>(1) * z[14] * z[21];
return z[20] + z[21] + T(3) * z[22];
}



template IntegrandConstructorType<double> f_4_133_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_133_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_133_construct (const Kin<qd_real>&);
#endif

}