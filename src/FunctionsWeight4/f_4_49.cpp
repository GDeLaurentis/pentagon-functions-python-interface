#include "f_4_49.h"

namespace PentagonFunctions {

template <typename T> T f_4_49_abbreviated (const std::array<T,38>&);



template <typename T> IntegrandConstructorType<T> f_4_49_construct (const Kin<T>& kin) {
    return [&kin, 
            dl13 = DLog_W_13<T>(kin),dl11 = DLog_W_11<T>(kin),dl19 = DLog_W_19<T>(kin),dl26 = DLog_W_26<T>(kin),dl17 = DLog_W_17<T>(kin),dl3 = DLog_W_3<T>(kin),dl4 = DLog_W_4<T>(kin),dl5 = DLog_W_5<T>(kin),dl14 = DLog_W_14<T>(kin),dl2 = DLog_W_2<T>(kin),dl16 = DLog_W_16<T>(kin),dl1 = DLog_W_1<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),dl30 = DLog_W_30<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),dl27 = DLog_W_27<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,38> abbr = 
            {dl13(t), rlog(v_path[0]), rlog(-v_path[1]), rlog(v_path[2]), rlog(v_path[3]), rlog(-v_path[4]), f_2_1_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl11(t), f_2_1_2(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl19(t), f_2_1_3(kin_path), f_2_1_4(kin_path), rlog(-v_path[1] + v_path[3]), dl26(t) / kin_path.SqrtDelta, rlog(kin.W[0] / kin_path.W[0]), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[19] / kin_path.W[19]), dl17(t), dl3(t), dl4(t), dl5(t), f_1_3_3(kin) - f_1_3_3(kin_path), dl14(t), dl2(t), dl16(t), dl1(t), dl18(t), dl20(t), dl30(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta}
;

        auto result = f_4_49_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_49_abbreviated(const std::array<T,38>& abb)
{
using TR = typename T::value_type;
T z[94];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[7];
z[3] = abb[8];
z[4] = abb[2];
z[5] = abb[3];
z[6] = abb[4];
z[7] = abb[5];
z[8] = abb[6];
z[9] = abb[9];
z[10] = abb[26];
z[11] = abb[29];
z[12] = abb[30];
z[13] = abb[31];
z[14] = abb[32];
z[15] = abb[33];
z[16] = abb[11];
z[17] = abb[17];
z[18] = abb[34];
z[19] = abb[35];
z[20] = abb[37];
z[21] = abb[20];
z[22] = abb[21];
z[23] = abb[22];
z[24] = abb[24];
z[25] = abb[25];
z[26] = bc<TR>[0];
z[27] = abb[12];
z[28] = abb[23];
z[29] = abb[28];
z[30] = abb[16];
z[31] = abb[36];
z[32] = abb[27];
z[33] = abb[10];
z[34] = abb[13];
z[35] = abb[14];
z[36] = abb[15];
z[37] = abb[18];
z[38] = abb[19];
z[39] = bc<TR>[1];
z[40] = bc<TR>[3];
z[41] = bc<TR>[5];
z[42] = bc<TR>[2];
z[43] = bc<TR>[4];
z[44] = bc<TR>[7];
z[45] = bc<TR>[9];
z[46] = bc<TR>[8];
z[47] = z[24] + z[25];
z[47] = -z[47] / T(2);
z[48] = z[3] + z[16];
z[49] = T(3) * z[2] + -z[48];
z[47] = z[47] * z[49];
z[49] = -z[17] + -z[21] + z[22] + z[23];
z[50] = z[49] / T(2);
z[51] = z[19] * z[50];
z[52] = z[2] + z[48];
z[53] = z[15] * z[52];
z[51] = z[51] + z[53] / T(2);
z[54] = z[20] * z[50];
z[55] = z[2] + -z[16];
z[56] = z[3] + z[55];
z[57] = z[56] / T(2);
z[58] = z[14] * z[57];
z[54] = z[54] + z[58];
z[58] = z[13] / T(2);
z[59] = T(3) * z[3];
z[60] = -z[2] + -z[16] + z[59];
z[60] = z[58] * z[60];
z[59] = -z[55] + z[59];
z[61] = z[10] / T(2);
z[59] = z[59] * z[61];
z[62] = -z[2] + z[3];
z[63] = -(z[0] * z[62]);
z[64] = -(z[11] * z[62]);
z[47] = z[47] + -z[51] + z[54] + z[59] + z[60] + T(3) * z[63] + z[64];
z[47] = z[8] * z[47];
z[59] = z[19] * z[49];
z[53] = z[53] + z[59];
z[59] = -z[2] + z[48];
z[60] = z[10] * z[59];
z[64] = z[53] + -z[60];
z[65] = z[49] / T(4);
z[66] = z[18] * z[65];
z[67] = z[12] * z[59];
z[66] = -z[66] + z[67] / T(4);
z[68] = z[11] * z[59];
z[69] = z[2] / T(2);
z[70] = z[3] + -z[69];
z[70] = z[13] * z[70];
z[64] = z[54] + z[63] + -z[64] / T(4) + z[66] + (T(-3) * z[68]) / T(4) + z[70];
z[70] = prod_pow(z[1], 2);
z[64] = z[64] * z[70];
z[71] = z[13] * z[52];
z[72] = z[28] * z[59];
z[73] = z[24] * z[59];
z[53] = z[53] + z[60] + -z[71] + -z[72] + -z[73];
z[60] = z[30] * z[49];
z[71] = z[53] + -z[60] + z[68];
z[74] = z[5] * z[71];
z[75] = z[11] / T(2);
z[75] = z[59] * z[75];
z[51] = z[51] + z[75];
z[76] = z[59] / T(2);
z[77] = z[25] * z[76];
z[78] = z[24] * z[76];
z[79] = z[77] + z[78];
z[80] = z[3] * z[13];
z[80] = z[51] + -z[63] + -z[79] + -z[80];
z[80] = z[7] * z[80];
z[62] = -z[16] + z[62];
z[58] = z[58] * z[62];
z[61] = z[59] * z[61];
z[63] = z[61] + z[63];
z[58] = z[58] + z[63];
z[55] = z[9] * z[55];
z[54] = z[54] + -z[55] + z[58] + -z[75];
z[81] = -(z[1] * z[54]);
z[82] = z[30] * z[50];
z[83] = z[28] * z[76];
z[84] = z[82] + z[83];
z[58] = -z[58] + -z[77] + z[84];
z[58] = z[4] * z[58];
z[58] = z[58] + z[74] / T(4) + -z[80] + z[81];
z[58] = z[5] * z[58];
z[54] = z[5] * z[54];
z[74] = z[27] * z[52];
z[77] = z[13] * z[59];
z[85] = z[74] + z[77];
z[86] = -z[73] + z[85];
z[87] = z[31] / T(2);
z[87] = z[49] * z[87];
z[57] = z[32] + z[57];
z[88] = z[11] * z[57];
z[87] = z[87] + z[88];
z[86] = z[86] / T(2) + -z[87];
z[88] = z[7] * z[86];
z[86] = z[4] * z[86];
z[89] = -z[16] + z[32];
z[90] = z[7] * z[89];
z[91] = z[4] * z[89];
z[92] = z[90] + -z[91];
z[93] = -(z[29] * z[92]);
z[54] = z[54] + z[81] + z[86] + -z[88] + z[93];
z[54] = z[6] * z[54];
z[81] = z[12] * z[76];
z[50] = z[18] * z[50];
z[81] = -z[50] + z[81];
z[76] = -(z[13] * z[76]);
z[76] = -z[74] + z[76] + z[78] + z[81] + -z[83];
z[60] = z[60] / T(4);
z[76] = -z[60] + z[76] / T(2) + z[87];
z[76] = z[4] * z[76];
z[83] = -(z[13] * z[16]);
z[63] = z[63] + z[79] + -z[81] + z[83];
z[63] = z[1] * z[63];
z[63] = z[63] + z[76] + z[88];
z[63] = z[4] * z[63];
z[57] = z[10] * z[57];
z[76] = z[57] + z[84];
z[79] = z[25] * z[52];
z[74] = -z[74] + z[79];
z[74] = z[74] / T(2) + -z[76] + z[78] + z[87];
z[74] = z[35] * z[74];
z[78] = -z[67] + z[79] + z[85];
z[50] = z[50] + -z[57] + z[78] / T(2) + -z[87];
z[57] = z[34] * z[50];
z[78] = z[1] * z[80];
z[80] = z[4] * z[92];
z[83] = z[34] * z[89];
z[80] = z[80] + T(-2) * z[83];
z[80] = z[29] * z[80];
z[70] = -z[33] + -z[70];
z[55] = z[55] * z[70];
z[70] = z[18] + T(-3) * z[19];
z[70] = z[44] * z[70];
z[47] = z[47] + z[54] + z[55] + -z[57] + z[58] + z[63] + z[64] + z[70] + z[74] + z[78] + z[80];
z[54] = z[14] * z[56];
z[52] = z[24] * z[52];
z[55] = z[11] * z[62];
z[52] = -z[52] + z[54] + -z[55] + -z[72];
z[55] = z[39] / T(2);
z[57] = (T(-5) * z[18]) / T(2) + (T(-3) * z[19]) / T(2) + z[20];
z[57] = z[55] * z[57];
z[58] = z[29] * z[89];
z[62] = T(3) * z[18] + z[20];
z[62] = (T(-3) * z[31]) / T(2) + z[62] / T(2);
z[62] = z[42] * z[62];
z[63] = -z[3] / T(2) + -z[16] + z[69];
z[63] = z[13] * z[63];
z[64] = -(z[25] * z[69]);
z[56] = z[32] + z[56] / T(4);
z[56] = z[10] * z[56];
z[65] = -(z[20] * z[65]);
z[69] = -z[12] + -z[13] + z[14] + z[15];
z[70] = z[20] + (T(11) * z[69]) / T(90);
z[70] = int_to_imaginary<T>(1) * z[26] * z[70];
z[52] = -z[52] / T(4) + z[56] + z[57] + -z[58] + z[60] + z[62] + z[63] + z[64] + z[65] + -z[66] + z[70] / T(2);
z[52] = z[26] * z[52];
z[53] = -z[53] / T(2) + -z[75] + z[82];
z[53] = z[38] * z[53];
z[48] = -(z[13] * z[48]);
z[48] = z[48] + z[51] + z[61] + -z[81];
z[48] = z[1] * z[48];
z[51] = z[77] + z[79];
z[51] = z[51] / T(2) + -z[76];
z[51] = z[4] * z[51];
z[56] = z[18] + -z[31];
z[56] = prod_pow(z[42], 2) * z[56];
z[57] = -(z[20] * z[43]);
z[48] = z[48] + z[51] + z[53] + z[56] + z[57] + z[88];
z[48] = int_to_imaginary<T>(1) * z[48];
z[50] = -z[50] + T(-2) * z[58];
z[50] = int_to_imaginary<T>(1) * z[36] * z[50];
z[51] = -z[18] + -z[19] + -z[20] + z[69] / T(10);
z[51] = z[51] * z[55];
z[53] = z[20] * z[42];
z[51] = z[51] + z[53];
z[53] = int_to_imaginary<T>(1) * z[39];
z[51] = z[51] * z[53];
z[55] = z[90] + z[91];
z[55] = int_to_imaginary<T>(1) * z[29] * z[55];
z[48] = z[48] + z[50] + z[51] + z[55];
z[48] = T(3) * z[48] + z[52];
z[48] = z[26] * z[48];
z[50] = z[18] + -z[20];
z[49] = -(z[49] * z[50]);
z[51] = -(z[25] * z[59]);
z[49] = z[49] + z[51] + z[54] + z[67] + -z[68] + -z[73] + z[77];
z[49] = z[33] * z[49];
z[51] = z[37] * z[71];
z[49] = z[49] + z[51];
z[51] = int_to_imaginary<T>(1) * z[41];
z[52] = z[26] + T(-3) * z[53];
z[52] = z[40] * z[52];
z[51] = (T(-36) * z[51]) / T(5) + z[52];
z[51] = z[51] * z[69];
z[50] = -z[19] + z[50];
z[50] = z[43] * z[50];
z[52] = z[18] + (T(-3) * z[20]) / T(2);
z[52] = prod_pow(z[39], 2) * z[52];
z[50] = T(3) * z[50] + z[52];
z[50] = z[39] * z[50];
z[52] = prod_pow(z[42], 3);
z[52] = (T(-3) * z[46]) / T(2) + T(2) * z[52];
z[52] = z[20] * z[52];
z[53] = (T(-11) * z[18]) / T(4) + T(8) * z[19] + (T(9) * z[20]) / T(4) + (T(3) * z[31]) / T(4);
z[53] = z[45] * z[53];
return T(3) * z[47] + z[48] + (T(3) * z[49]) / T(2) + z[50] + z[51] + z[52] + z[53];
}



template IntegrandConstructorType<double> f_4_49_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_49_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_49_construct (const Kin<qd_real>&);
#endif

}