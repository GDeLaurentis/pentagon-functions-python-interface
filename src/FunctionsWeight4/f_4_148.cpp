#include "f_4_148.h"

namespace PentagonFunctions {

template <typename T> T f_4_148_abbreviated (const std::array<T,39>&);

template <typename T> class SpDLog_f_4_148_W_22 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_148_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (T(5) * kin.v[2] + T(-5) * kin.v[3] + T(-5) * kin.v[4]) + ((T(-3) * kin.v[3]) / T(4) + (T(-9) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + ((T(-15) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + kin.v[1] * ((T(3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(-9) + T(5) * kin.v[0] + ((bc<T>[1] * T(3)) / T(2) + T(9) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[1] + bc<T>[1] * (T(-3) + T(3) * kin.v[0] + T(-3) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[0] + T(-3) * kin.v[3])) + ((T(-3) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4])) + bc<T>[1] * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]));
c[1] = (T(-9) + bc<T>[1] * T(-3) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1] + T(-9) * kin.v[2] + T(9) * kin.v[3] + T(9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[1] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]);
c[2] = ((T(3) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + kin.v[1] * ((T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(9) + T(-5) * kin.v[0] + (T(-9) / T(4) + (bc<T>[1] * T(-3)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[1] + bc<T>[1] * (T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3])) + ((T(15) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + ((T(3) * kin.v[3]) / T(4) + (T(9) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + kin.v[0] * (T(-5) * kin.v[2] + T(5) * kin.v[3] + T(5) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4])) + bc<T>[1] * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]));
c[3] = (bc<T>[0] * int_to_imaginary<T>(3) + bc<T>[1] * T(3) + T(9)) * kin.v[1] + T(9) * kin.v[2] + T(-9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + bc<T>[1] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + T(-9) * kin.v[4];
c[4] = ((T(3) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + kin.v[1] * ((T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(9) + T(-5) * kin.v[0] + (T(-9) / T(4) + (bc<T>[1] * T(-3)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[1] + bc<T>[1] * (T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3])) + ((T(15) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + ((T(3) * kin.v[3]) / T(4) + (T(9) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + kin.v[0] * (T(-5) * kin.v[2] + T(5) * kin.v[3] + T(5) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4])) + bc<T>[1] * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]));
c[5] = (bc<T>[0] * int_to_imaginary<T>(3) + bc<T>[1] * T(3) + T(9)) * kin.v[1] + T(9) * kin.v[2] + T(-9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + bc<T>[1] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + T(-9) * kin.v[4];
c[6] = kin.v[0] * (T(5) * kin.v[2] + T(-5) * kin.v[3] + T(-5) * kin.v[4]) + ((T(-3) * kin.v[3]) / T(4) + (T(-9) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + ((T(-15) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + kin.v[1] * ((T(3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(-9) + T(5) * kin.v[0] + ((bc<T>[1] * T(3)) / T(2) + T(9) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[1] + bc<T>[1] * (T(-3) + T(3) * kin.v[0] + T(-3) * kin.v[3]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[0] + T(-3) * kin.v[3])) + ((T(-3) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4])) + bc<T>[1] * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]));
c[7] = (T(-9) + bc<T>[1] * T(-3) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1] + T(-9) * kin.v[2] + T(9) * kin.v[3] + T(9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[1] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[8] * (t * c[0] + c[1]) + abb[10] * (t * c[2] + c[3]) + abb[6] * (t * c[4] + c[5]) + abb[15] * (t * c[6] + c[7]);
        }

        return abb[11] * (abb[13] * (abb[13] * ((abb[15] * T(-3)) / T(2) + (abb[6] * T(3)) / T(2) + (abb[10] * T(3)) / T(2)) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[15] * bc<T>[0] * int_to_imaginary<T>(3)) + abb[8] * (abb[14] * T(-9) + abb[2] * abb[13] * T(-3) + abb[3] * abb[13] * T(-3) + abb[13] * ((abb[13] * T(-3)) / T(2) + bc<T>[0] * int_to_imaginary<T>(3))) + abb[2] * abb[13] * (abb[15] * T(-3) + abb[6] * T(3) + abb[10] * T(3)) + abb[3] * abb[13] * (abb[15] * T(-3) + abb[6] * T(3) + abb[10] * T(3)) + abb[1] * (abb[8] * abb[13] * T(3) + abb[12] * (abb[8] * T(-3) + abb[15] * T(-3) + abb[6] * T(3) + abb[10] * T(3)) + abb[13] * (abb[6] * T(-3) + abb[10] * T(-3) + abb[15] * T(3))) + abb[12] * (abb[12] * ((abb[8] * T(-9)) / T(2) + (abb[15] * T(-9)) / T(2) + (abb[6] * T(9)) / T(2) + (abb[10] * T(9)) / T(2)) + abb[15] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[6] * bc<T>[0] * int_to_imaginary<T>(3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(3) + abb[2] * (abb[6] * T(-3) + abb[10] * T(-3) + abb[15] * T(3)) + abb[3] * (abb[6] * T(-3) + abb[10] * T(-3) + abb[15] * T(3)) + abb[8] * (bc<T>[0] * int_to_imaginary<T>(-3) + abb[2] * T(3) + abb[3] * T(3) + abb[13] * T(6)) + abb[13] * (abb[6] * T(-6) + abb[10] * T(-6) + abb[15] * T(6))) + abb[14] * (abb[15] * T(-9) + abb[6] * T(9) + abb[10] * T(9)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_148_construct (const Kin<T>& kin) {
    return [&kin, 
            dl13 = DLog_W_13<T>(kin),dl18 = DLog_W_18<T>(kin),dl22 = DLog_W_22<T>(kin),dl3 = DLog_W_3<T>(kin),dl4 = DLog_W_4<T>(kin),dl1 = DLog_W_1<T>(kin),dl16 = DLog_W_16<T>(kin),dl8 = DLog_W_8<T>(kin),dl2 = DLog_W_2<T>(kin),dl20 = DLog_W_20<T>(kin),dl5 = DLog_W_5<T>(kin),dl19 = DLog_W_19<T>(kin),dl17 = DLog_W_17<T>(kin),dl28 = DLog_W_28<T>(kin),dl30 = DLog_W_30<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin),dl27 = DLog_W_27<T>(kin),spdl22 = SpDLog_f_4_148_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,39> abbr = 
            {dl13(t), rlog(v_path[0]), rlog(v_path[2]), rlog(v_path[3]), f_2_1_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl18(t), f_1_3_1(kin) - f_1_3_1(kin_path), f_2_1_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl3(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl4(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl1(t), dl16(t), dl8(t), dl2(t), dl20(t), dl5(t), dl19(t), dl17(t), dl28(t) / kin_path.SqrtDelta, rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[19] / kin_path.W[19]), dl30(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta}
;

        auto result = f_4_148_abbreviated(abbr);
        result = result + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_148_abbreviated(const std::array<T,39>& abb)
{
using TR = typename T::value_type;
T z[86];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[5];
z[3] = abb[6];
z[4] = abb[2];
z[5] = abb[3];
z[6] = abb[4];
z[7] = abb[22];
z[8] = abb[7];
z[9] = abb[23];
z[10] = abb[25];
z[11] = abb[26];
z[12] = abb[27];
z[13] = abb[8];
z[14] = abb[10];
z[15] = abb[31];
z[16] = abb[35];
z[17] = abb[37];
z[18] = abb[38];
z[19] = abb[32];
z[20] = abb[33];
z[21] = abb[34];
z[22] = abb[12];
z[23] = abb[16];
z[24] = abb[19];
z[25] = abb[13];
z[26] = bc<TR>[0];
z[27] = abb[15];
z[28] = abb[24];
z[29] = abb[28];
z[30] = abb[30];
z[31] = abb[29];
z[32] = abb[36];
z[33] = abb[9];
z[34] = abb[14];
z[35] = abb[17];
z[36] = abb[18];
z[37] = abb[20];
z[38] = abb[21];
z[39] = bc<TR>[1];
z[40] = bc<TR>[3];
z[41] = bc<TR>[5];
z[42] = bc<TR>[2];
z[43] = bc<TR>[4];
z[44] = bc<TR>[7];
z[45] = bc<TR>[8];
z[46] = bc<TR>[9];
z[47] = -z[15] + -z[19] + z[20] + z[21];
z[48] = z[30] * z[47];
z[49] = z[3] + z[14];
z[50] = z[13] + z[49];
z[51] = z[29] * z[50];
z[52] = z[48] + -z[51];
z[53] = z[13] + z[14];
z[54] = z[31] * z[53];
z[55] = z[3] * z[31];
z[55] = z[54] + z[55];
z[56] = -z[13] + T(3) * z[49];
z[57] = z[9] * z[56];
z[58] = z[52] + z[55] + z[57];
z[59] = z[23] / T(2);
z[56] = z[56] * z[59];
z[60] = z[16] / T(2);
z[60] = z[47] * z[60];
z[56] = z[56] + -z[60];
z[61] = z[17] / T(2);
z[62] = z[47] * z[61];
z[63] = T(3) * z[13] + -z[49];
z[64] = z[63] / T(2);
z[65] = z[27] + z[64];
z[65] = z[24] * z[65];
z[66] = z[7] + -z[29];
z[66] = z[27] * z[66];
z[67] = -z[13] + z[49];
z[68] = z[7] * z[67];
z[58] = z[56] + z[58] / T(2) + z[62] + -z[65] + z[66] + -z[68];
z[65] = z[32] * z[47];
z[63] = z[27] + z[63] / T(4);
z[63] = z[11] * z[63];
z[63] = -z[58] / T(2) + z[63] + z[65] / T(4);
z[63] = prod_pow(z[22], 2) * z[63];
z[66] = z[47] / T(2);
z[69] = z[32] * z[66];
z[64] = T(2) * z[27] + z[64];
z[64] = z[11] * z[64];
z[58] = -z[58] + z[64] + z[69];
z[58] = z[34] * z[58];
z[59] = z[50] * z[59];
z[64] = -z[3] + z[53];
z[64] = -z[2] + -z[64] / T(2);
z[70] = z[24] * z[64];
z[59] = z[59] + z[70];
z[66] = z[18] * z[66];
z[64] = z[7] * z[64];
z[66] = z[64] + z[66];
z[70] = z[3] * z[8];
z[53] = z[8] * z[53];
z[70] = z[53] + z[70];
z[71] = z[12] * z[67];
z[72] = z[70] + z[71];
z[73] = -z[2] + z[3];
z[73] = z[0] * z[73];
z[74] = z[67] / T(2);
z[75] = z[11] * z[74];
z[72] = z[59] + -z[62] + z[66] + z[72] / T(2) + T(-2) * z[73] + -z[75];
z[72] = z[6] * z[72];
z[76] = z[7] * z[27];
z[77] = z[13] + z[27];
z[78] = z[24] * z[77];
z[79] = z[27] + z[49];
z[79] = z[28] * z[79];
z[76] = z[76] + -z[78] + -z[79];
z[77] = z[11] * z[77];
z[69] = z[69] + z[77];
z[55] = z[55] + -z[68];
z[57] = z[55] + z[57];
z[56] = z[56] + z[57] / T(2) + -z[69] + z[76];
z[56] = z[22] * z[56];
z[57] = z[16] * z[47];
z[77] = z[9] * z[67];
z[78] = z[57] + -z[77];
z[55] = z[55] + -z[78];
z[80] = z[23] * z[67];
z[81] = z[55] + z[80];
z[69] = -z[69] + z[81] / T(2);
z[81] = int_to_imaginary<T>(1) * z[26];
z[81] = -z[5] + z[81];
z[69] = z[69] * z[81];
z[78] = z[71] + z[78];
z[81] = z[24] * z[67];
z[82] = z[78] + -z[80] + -z[81];
z[83] = z[11] * z[27];
z[82] = z[82] / T(2) + z[83];
z[83] = -(z[1] * z[82]);
z[84] = -z[9] + -z[23];
z[84] = z[49] * z[84];
z[76] = -z[76] + z[84];
z[76] = z[25] * z[76];
z[56] = z[56] + z[69] + z[76] / T(2) + z[83];
z[56] = z[25] * z[56];
z[52] = z[52] + -z[68];
z[68] = z[27] * z[29];
z[68] = z[68] + -z[79];
z[69] = z[24] * z[74];
z[76] = -z[27] + z[74];
z[76] = z[11] * z[76];
z[69] = z[52] / T(2) + z[62] + -z[68] + z[69] + z[76];
z[69] = z[22] * z[69];
z[83] = z[70] + z[77];
z[83] = z[66] + z[83] / T(2);
z[62] = z[60] + z[62];
z[75] = z[62] + z[75];
z[84] = -z[73] + -z[75] + z[83];
z[85] = -z[1] + z[5];
z[85] = z[84] * z[85];
z[52] = z[52] + z[71] + -z[77];
z[71] = z[23] * z[74];
z[52] = z[52] / T(2) + -z[68] + -z[71] + z[75];
z[68] = z[4] * z[52];
z[71] = z[25] * z[82];
z[68] = -z[68] / T(2) + z[69] + z[71] + z[85];
z[68] = z[4] * z[68];
z[71] = -(z[1] * z[84]);
z[75] = z[10] * z[67];
z[77] = z[17] * z[47];
z[82] = z[75] + -z[77];
z[81] = -z[81] + z[82];
z[50] = z[11] * z[50];
z[50] = z[50] + -z[55] + z[65] + -z[81];
z[55] = z[5] * z[50];
z[65] = -z[80] + z[81];
z[65] = z[65] / T(2) + -z[76];
z[65] = z[22] * z[65];
z[55] = -z[55] / T(4) + -z[65] + z[71];
z[55] = z[5] * z[55];
z[71] = -(z[10] * z[74]);
z[59] = -z[59] + -z[60] + z[71] + z[83];
z[59] = z[33] * z[59];
z[60] = z[11] * z[67];
z[57] = z[57] + z[60] + z[75] + z[77];
z[67] = z[9] + z[12];
z[67] = z[67] * z[74];
z[67] = z[67] + z[70];
z[57] = -z[57] / T(4) + z[66] + z[67] / T(2) + -z[73];
z[57] = z[1] * z[57];
z[57] = z[57] + z[65];
z[57] = z[1] * z[57];
z[65] = z[35] * z[52];
z[55] = z[55] + z[56] + z[57] + z[58] + z[59] + z[63] + -z[65] + z[68] + z[72];
z[56] = -z[8] + -z[31];
z[56] = z[3] * z[56];
z[57] = z[18] * z[47];
z[47] = z[39] + z[47];
z[47] = z[32] * z[47];
z[47] = z[47] + z[48] + -z[51] + -z[53] + -z[54] + z[56] + -z[57] + -z[80];
z[48] = z[30] / T(2);
z[51] = z[18] + z[48] + -z[61];
z[53] = -z[16] + z[51];
z[53] = z[39] * z[53];
z[54] = -z[7] + -z[29];
z[54] = z[27] * z[54];
z[56] = z[13] + -z[14];
z[56] = -z[2] + (T(3) * z[3]) / T(2) + z[27] + -z[56] / T(2);
z[56] = z[24] * z[56];
z[57] = -z[27] + z[49];
z[57] = z[11] * z[57];
z[53] = z[53] + z[54] + z[56] + z[57] + -z[64];
z[49] = z[13] / T(2) + -z[49];
z[49] = z[9] * z[49];
z[47] = z[47] / T(4) + z[49] + z[53] / T(2) + z[62] + z[79];
z[47] = z[26] * z[47];
z[49] = z[11] + z[12] + -z[29] + -z[31];
z[53] = -(z[40] * z[49]);
z[47] = z[47] + z[53];
z[47] = z[26] * z[47];
z[53] = z[38] * z[50];
z[54] = -z[60] + -z[78] + z[82];
z[54] = z[1] * z[54];
z[53] = z[53] + z[54];
z[52] = z[36] * z[52];
z[54] = prod_pow(z[39], 2);
z[56] = z[49] * z[54];
z[52] = z[52] + z[53] / T(2) + -z[56] / T(20) + -z[69];
z[53] = z[16] + z[17];
z[56] = z[30] + z[53];
z[57] = -z[32] + (T(-11) * z[49]) / T(45) + -z[56];
z[58] = prod_pow(z[26], 2);
z[57] = z[57] * z[58];
z[59] = prod_pow(z[42], 2);
z[56] = z[56] * z[59];
z[52] = T(3) * z[52] + (T(3) * z[56]) / T(2) + z[57] / T(4);
z[52] = z[26] * z[52];
z[56] = z[39] * z[40];
z[56] = (T(12) * z[41]) / T(5) + z[56];
z[49] = z[49] * z[56];
z[49] = T(3) * z[49] + z[52];
z[49] = int_to_imaginary<T>(1) * z[49];
z[50] = z[37] * z[50];
z[52] = T(6) * z[44];
z[56] = T(3) * z[45];
z[57] = (T(-67) * z[46]) / T(2) + z[56];
z[60] = T(3) * z[43] + z[54] / T(2);
z[60] = z[39] * z[60];
z[57] = z[52] + z[57] / T(2) + z[60];
z[57] = z[32] * z[57];
z[60] = z[18] + z[32];
z[48] = z[48] + (T(3) * z[53]) / T(2) + -z[60];
z[48] = z[48] * z[58];
z[53] = -z[30] + -z[60];
z[53] = z[53] * z[59];
z[48] = z[48] / T(2) + T(2) * z[53];
z[48] = z[42] * z[48];
z[53] = z[18] + z[30];
z[53] = z[53] * z[56];
z[56] = (T(-87) * z[16]) / T(2) + (T(-47) * z[17]) / T(2) + T(-45) * z[18] + (T(-137) * z[30]) / T(2);
z[56] = z[46] * z[56];
z[53] = z[53] + z[56] / T(2);
z[56] = -z[17] + T(2) * z[18] + z[30];
z[52] = z[52] * z[56];
z[51] = z[51] * z[54];
z[54] = z[43] * z[56];
z[51] = z[51] + T(3) * z[54];
z[51] = z[39] * z[51];
return z[47] + z[48] + z[49] + (T(-3) * z[50]) / T(2) + z[51] + z[52] + z[53] / T(2) + T(3) * z[55] + z[57];
}



template IntegrandConstructorType<double> f_4_148_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_148_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_148_construct (const Kin<qd_real>&);
#endif

}