#include "f_4_203.h"

namespace PentagonFunctions {

template <typename T> T f_4_203_abbreviated (const std::array<T,19>&);



template <typename T> IntegrandConstructorType<T> f_4_203_construct (const Kin<T>& kin) {
    return [&kin, 
            dl8 = DLog_W_8<T>(kin),dl15 = DLog_W_15<T>(kin),dl1 = DLog_W_1<T>(kin),dl3 = DLog_W_3<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl13 = DLog_W_13<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,19> abbr = 
            {dl8(t), rlog(kin.W[2] / kin_path.W[2]), rlog(v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[19] / kin_path.W[19]), dl15(t), rlog(-v_path[4]), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl1(t), rlog(kin.W[12] / kin_path.W[12]), dl3(t), dl5(t), dl20(t), dl13(t)}
;

        auto result = f_4_203_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_203_abbreviated(const std::array<T,19>& abb)
{
using TR = typename T::value_type;
T z[55];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[13];
z[12] = abb[15];
z[13] = abb[16];
z[14] = abb[17];
z[15] = abb[10];
z[16] = abb[11];
z[17] = abb[12];
z[18] = abb[14];
z[19] = bc<TR>[1];
z[20] = bc<TR>[2];
z[21] = bc<TR>[4];
z[22] = bc<TR>[9];
z[23] = abb[18];
z[24] = int_to_imaginary<T>(1) * z[3];
z[25] = -z[2] + z[24];
z[26] = z[2] * z[25];
z[27] = prod_pow(z[3], 2);
z[28] = (T(5) * z[27]) / T(6);
z[26] = z[26] + z[28];
z[29] = z[6] * z[24];
z[29] = -z[5] + z[29];
z[30] = prod_pow(z[4], 2);
z[30] = z[26] + z[29] + z[30];
z[30] = z[0] * z[30];
z[31] = z[17] * z[24];
z[31] = -z[16] + z[31];
z[32] = prod_pow(z[15], 2);
z[26] = z[26] + z[31] + z[32];
z[26] = z[10] * z[26];
z[32] = z[26] + z[30];
z[33] = z[4] * z[25];
z[34] = z[4] + z[25];
z[35] = z[15] * z[34];
z[36] = prod_pow(z[2], 2);
z[37] = z[33] + z[35] + z[36];
z[38] = z[28] + z[31];
z[39] = z[29] + -z[37] + z[38];
z[40] = z[12] * z[39];
z[41] = z[2] + z[24];
z[41] = z[2] * z[41];
z[28] = -z[28] + z[41];
z[25] = T(2) * z[25];
z[42] = z[4] + z[25];
z[42] = z[4] * z[42];
z[43] = T(-3) * z[29];
z[44] = z[42] + z[43];
z[35] = T(2) * z[35];
z[45] = z[28] + z[35] + z[44];
z[45] = z[13] * z[45];
z[34] = T(2) * z[34];
z[46] = z[15] + z[34];
z[46] = z[15] * z[46];
z[47] = T(-3) * z[31];
z[48] = z[46] + z[47];
z[33] = T(2) * z[33];
z[49] = z[28] + z[33] + z[48];
z[49] = z[14] * z[49];
z[45] = -z[32] + -z[40] + z[45] + z[49];
z[45] = z[1] * z[45];
z[35] = T(-2) * z[31] + z[35];
z[33] = T(2) * z[29] + -z[33];
z[36] = (T(5) * z[27]) / T(3) + T(-2) * z[36];
z[49] = -z[33] + z[35] + -z[36];
z[50] = z[7] + z[9];
z[51] = T(2) * z[18];
z[52] = -z[50] + z[51];
z[52] = z[49] * z[52];
z[37] = (T(5) * z[27]) / T(2) + T(-3) * z[37] + -z[43] + -z[47];
z[37] = z[1] * z[37];
z[39] = -(z[8] * z[39]);
z[43] = z[20] * z[24];
z[47] = (T(3) * z[27]) / T(2) + z[43];
z[47] = z[20] * z[47];
z[43] = z[27] / T(3) + z[43];
z[53] = z[19] * z[24];
z[43] = T(2) * z[43] + -z[53];
z[53] = -(z[19] * z[43]);
z[54] = T(2) * z[24];
z[54] = z[21] * z[54];
z[37] = (T(-65) * z[22]) / T(12) + z[37] + z[39] + z[47] + z[52] + z[53] + z[54];
z[37] = z[11] * z[37];
z[39] = -(z[49] * z[51]);
z[49] = int_to_imaginary<T>(1) * z[21];
z[51] = int_to_imaginary<T>(1) * z[27];
z[52] = T(-6) * z[49] + z[51];
z[52] = z[3] * z[52];
z[24] = T(3) * z[24];
z[53] = z[20] * z[24];
z[27] = z[27] + z[53];
z[24] = -(z[19] * z[24]);
z[24] = z[24] + T(2) * z[27];
z[24] = z[19] * z[24];
z[24] = (T(3) * z[22]) / T(4) + z[24] + z[39] + -z[47] + z[52];
z[24] = z[12] * z[24];
z[26] = -z[26] + z[30];
z[27] = -z[15] + z[34];
z[27] = z[15] * z[27];
z[27] = z[27] + -z[31] + -z[36] + z[44];
z[27] = z[12] * z[27];
z[30] = -z[29] + z[31];
z[31] = T(2) * z[30];
z[34] = z[13] * z[31];
z[27] = -z[26] + z[27] + z[34];
z[27] = z[7] * z[27];
z[28] = -z[28] + z[29] + -z[35] + -z[42];
z[28] = z[13] * z[28];
z[28] = z[28] + z[32] + -z[40];
z[28] = z[8] * z[28];
z[25] = -z[4] + z[25];
z[25] = z[4] * z[25];
z[25] = z[25] + -z[29] + -z[36] + z[48];
z[25] = z[12] * z[25];
z[25] = z[25] + z[26] + z[34];
z[25] = z[9] * z[25];
z[26] = z[33] + z[38] + -z[41] + -z[46];
z[26] = z[8] * z[26];
z[29] = T(2) * z[19];
z[29] = z[29] * z[43];
z[32] = T(4) * z[49] + -z[51];
z[32] = z[3] * z[32];
z[29] = (T(14) * z[22]) / T(3) + -z[29] + z[32];
z[30] = z[18] * z[30];
z[30] = T(4) * z[30];
z[31] = -(z[31] * z[50]);
z[26] = z[26] + -z[29] + z[30] + z[31];
z[26] = z[14] * z[26];
z[30] = -z[29] + -z[30];
z[30] = z[13] * z[30];
z[29] = z[23] * z[29];
return z[24] + z[25] + z[26] + z[27] + z[28] + T(2) * z[29] + z[30] + z[37] + z[45];
}



template IntegrandConstructorType<double> f_4_203_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_203_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_203_construct (const Kin<qd_real>&);
#endif

}