#include "f_4_76.h"

namespace PentagonFunctions {

template <typename T> T f_4_76_abbreviated (const std::array<T,18>&);

template <typename T> class SpDLog_f_4_76_W_10 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_76_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-2) * kin.v[1] + T(-4) * kin.v[2]) + T(-2) * prod_pow(kin.v[2], 2) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[2] * (kin.v[2] / T(2) + T(-2) + T(-2) * kin.v[4]) + kin.v[1] * (kin.v[1] / T(2) + T(-2) + kin.v[2] + T(-2) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[2] * (-kin.v[2] / T(2) + T(2) + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] / T(2) + -kin.v[2] + T(2) + T(2) * kin.v[4]));
c[1] = rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (T(-2) * kin.v[1] + T(-2) * kin.v[2]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (T(2) * kin.v[1] + T(2) * kin.v[2]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (-(prod_pow(abb[2], 2) * abb[5]) + abb[4] * (prod_pow(abb[2], 2) + abb[3] * T(-2)) + abb[3] * abb[5] * T(2) + abb[1] * (abb[2] * abb[5] * T(-2) + abb[2] * abb[4] * T(2) + abb[1] * (abb[4] * T(-3) + abb[5] * T(3))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_76_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl16 = DLog_W_16<T>(kin),dl18 = DLog_W_18<T>(kin),dl24 = DLog_W_24<T>(kin),dl5 = DLog_W_5<T>(kin),dl17 = DLog_W_17<T>(kin),spdl10 = SpDLog_f_4_76_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,18> abbr = 
            {dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_7(kin_path), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[17] / kin_path.W[17]), dl16(t), rlog(kin.W[23] / kin_path.W[23]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), -rlog(t), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[16] / kin_path.W[16]), dl18(t), dl24(t), dl5(t), dl17(t)}
;

        auto result = f_4_76_abbreviated(abbr);
        result = result + spdl10(t, abbr);

        return result;
    };
}

template <typename T> T f_4_76_abbreviated(const std::array<T,18>& abb)
{
using TR = typename T::value_type;
T z[62];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[6];
z[3] = abb[14];
z[4] = abb[16];
z[5] = abb[17];
z[6] = abb[5];
z[7] = abb[7];
z[8] = abb[11];
z[9] = abb[12];
z[10] = abb[13];
z[11] = abb[2];
z[12] = abb[15];
z[13] = abb[8];
z[14] = bc<TR>[0];
z[15] = abb[3];
z[16] = abb[9];
z[17] = abb[10];
z[18] = bc<TR>[2];
z[19] = bc<TR>[9];
z[20] = z[4] * z[8];
z[21] = z[3] * z[8];
z[20] = z[20] + -z[21];
z[22] = T(4) * z[20];
z[23] = (T(3) * z[12]) / T(2);
z[24] = z[5] / T(2);
z[25] = z[23] + z[24];
z[26] = T(2) * z[3];
z[27] = z[4] + z[25] + -z[26];
z[27] = z[10] * z[27];
z[28] = z[7] / T(2);
z[29] = z[5] + z[12];
z[30] = z[28] * z[29];
z[31] = T(2) * z[4];
z[32] = z[29] + z[31];
z[33] = -z[26] + z[32];
z[33] = z[9] * z[33];
z[34] = z[29] + -z[31];
z[35] = -z[26] + -z[34];
z[35] = z[6] * z[35];
z[36] = -z[4] + z[12];
z[36] = z[1] * z[36];
z[27] = -z[22] + z[27] + z[30] + z[33] + z[35] + -z[36];
z[27] = z[11] * z[27];
z[30] = z[26] + z[31];
z[33] = -z[2] + z[30];
z[25] = z[25] + -z[33];
z[25] = z[7] * z[25];
z[35] = z[12] / T(2);
z[37] = z[24] + z[35];
z[38] = -z[2] + z[4];
z[39] = z[37] + z[38];
z[40] = T(3) * z[10];
z[39] = z[39] * z[40];
z[41] = T(3) * z[4] + -z[5];
z[42] = T(2) * z[2];
z[43] = T(4) * z[3] + -z[42];
z[44] = z[41] + z[43];
z[44] = z[1] * z[44];
z[45] = z[2] + -z[5];
z[46] = -z[4] + z[45];
z[47] = z[8] * z[46];
z[47] = z[21] + z[47];
z[25] = -z[25] + -z[39] + z[44] + T(-4) * z[47];
z[39] = z[17] * z[25];
z[43] = T(4) * z[4] + -z[29] + z[43];
z[43] = z[1] * z[43];
z[44] = z[5] + z[30];
z[48] = T(3) * z[2];
z[49] = -z[44] + z[48];
z[49] = z[10] * z[49];
z[33] = -z[12] + z[33];
z[33] = z[7] * z[33];
z[50] = z[8] * z[45];
z[33] = z[33] + z[43] + z[49] + T(-4) * z[50];
z[43] = z[0] * z[33];
z[49] = -z[5] + z[12];
z[50] = -z[30] + z[42] + z[49];
z[51] = z[0] * z[50];
z[29] = -z[29] + z[30];
z[29] = z[13] * z[29];
z[30] = z[29] + -z[51];
z[52] = T(4) * z[2] + T(-6) * z[4];
z[53] = T(3) * z[12];
z[54] = z[5] + z[26];
z[55] = z[52] + z[53] + -z[54];
z[56] = z[17] * z[55];
z[30] = T(2) * z[30] + z[56];
z[30] = z[6] * z[30];
z[56] = T(3) * z[3] + z[4];
z[57] = (T(-3) * z[5]) / T(2) + -z[23] + z[56];
z[57] = prod_pow(z[18], 2) * z[57];
z[58] = z[3] + z[4];
z[59] = z[2] + z[58];
z[59] = (T(11) * z[12]) / T(6) + -z[24] + -z[59] / T(3);
z[60] = prod_pow(z[14], 2);
z[59] = z[59] * z[60];
z[57] = z[57] + z[59];
z[44] = -z[44] + z[53];
z[53] = -(z[10] * z[44]);
z[34] = T(-6) * z[3] + z[34];
z[34] = z[7] * z[34];
z[34] = z[34] + T(4) * z[36] + z[53];
z[34] = z[13] * z[34];
z[44] = z[13] * z[44];
z[53] = -z[44] + -z[51];
z[59] = -z[12] + z[26];
z[52] = T(3) * z[5] + -z[52] + z[59];
z[61] = -(z[17] * z[52]);
z[53] = T(2) * z[53] + z[61];
z[53] = z[9] * z[53];
z[27] = z[27] + z[30] + z[34] + z[39] + -z[43] + z[53] + z[57] / T(2);
z[27] = int_to_imaginary<T>(1) * z[14] * z[27];
z[30] = z[4] / T(2);
z[34] = -z[2] + z[3];
z[39] = z[12] + -z[24] + z[30] + z[34];
z[39] = z[1] * z[39];
z[53] = z[3] / T(2);
z[48] = -z[5] + z[48];
z[48] = -z[4] + -z[23] + z[48] / T(2) + z[53];
z[48] = z[10] * z[48];
z[35] = z[5] + z[35];
z[30] = -z[2] / T(2) + z[30] + -z[35] + z[53];
z[30] = z[7] * z[30];
z[32] = -z[32] + z[42];
z[32] = z[9] * z[32];
z[57] = z[12] + T(-2) * z[38];
z[57] = z[6] * z[57];
z[30] = z[30] + z[32] + z[39] + T(-2) * z[47] + z[48] + z[57];
z[30] = z[11] * z[30];
z[32] = z[13] * z[50];
z[39] = -z[31] + z[49];
z[39] = -(z[0] * z[39]);
z[39] = -z[32] + z[39];
z[39] = z[9] * z[39];
z[47] = -z[12] + z[31];
z[47] = z[0] * z[47];
z[32] = -z[32] + z[47];
z[32] = z[6] * z[32];
z[32] = z[32] + z[39];
z[39] = z[49] + -z[58];
z[39] = z[7] * z[39];
z[47] = -z[4] + -z[59];
z[47] = z[1] * z[47];
z[48] = z[5] + z[31];
z[49] = -z[3] + z[48];
z[49] = z[10] * z[49];
z[22] = -z[22] + z[39] + z[47] + z[49];
z[22] = z[0] * z[22];
z[33] = -(z[13] * z[33]);
z[22] = z[22] + z[30] + T(2) * z[32] + z[33];
z[22] = z[11] * z[22];
z[20] = z[20] / T(3) + z[36];
z[30] = (T(3) * z[4]) / T(2);
z[32] = (T(11) * z[3]) / T(6) + z[30] + -z[35];
z[32] = z[6] * z[32];
z[33] = (T(17) * z[3]) / T(12) + (T(-7) * z[12]) / T(4) + z[48] / T(3);
z[33] = z[10] * z[33];
z[35] = (T(-5) * z[4]) / T(2) + T(2) * z[5];
z[35] = (T(-35) * z[3]) / T(12) + z[12] / T(4) + z[35] / T(3);
z[35] = z[7] * z[35];
z[39] = -z[3] / T(4) + (T(-11) * z[4]) / T(4) + (T(-15) * z[5]) / T(8) + z[12] / T(8) + z[42];
z[39] = z[18] * z[39];
z[47] = (T(13) * z[3]) / T(2) + (T(-17) * z[12]) / T(2) + T(2) * z[48];
z[47] = z[9] * z[47];
z[20] = T(2) * z[20] + z[32] + z[33] + z[35] + z[39] + z[47] / T(3);
z[20] = z[20] * z[60];
z[32] = z[4] + z[45];
z[32] = z[8] * z[32];
z[21] = -z[21] + z[32];
z[24] = z[3] + z[24];
z[30] = z[2] + z[24] + -z[30];
z[30] = z[1] * z[30];
z[32] = (T(-3) * z[2]) / T(2) + z[48] + z[53];
z[32] = z[10] * z[32];
z[33] = z[3] + z[46];
z[28] = z[28] * z[33];
z[28] = T(2) * z[21] + z[28] + z[30] + z[32];
z[30] = prod_pow(z[0], 2);
z[28] = z[28] * z[30];
z[32] = z[6] * z[55];
z[33] = -(z[9] * z[52]);
z[25] = z[25] + z[32] + z[33];
z[25] = z[16] * z[25];
z[26] = -z[26] + -z[41] + z[42];
z[26] = z[1] * z[26];
z[32] = z[34] * z[40];
z[33] = -z[3] + z[5] + -z[38];
z[33] = z[7] * z[33];
z[21] = T(4) * z[21] + z[26] + z[32] + z[33];
z[21] = z[15] * z[21];
z[23] = -z[4] + z[23] + -z[24];
z[23] = z[10] * z[23];
z[24] = -z[37] + z[56];
z[24] = z[7] * z[24];
z[23] = z[23] + z[24] + T(-2) * z[36];
z[23] = z[13] * z[23];
z[23] = z[23] + z[43];
z[23] = z[13] * z[23];
z[24] = T(2) * z[15];
z[26] = z[24] * z[34];
z[32] = -(z[30] * z[45]);
z[26] = z[26] + z[32];
z[32] = T(2) * z[51];
z[33] = z[32] + z[44];
z[33] = z[13] * z[33];
z[26] = T(2) * z[26] + z[33];
z[26] = z[9] * z[26];
z[29] = -z[29] + z[32];
z[29] = z[13] * z[29];
z[32] = -z[42] + z[54];
z[24] = z[24] * z[32];
z[31] = z[5] + -z[31] + -z[42];
z[30] = z[30] * z[31];
z[24] = z[24] + z[29] + z[30];
z[24] = z[6] * z[24];
z[29] = (T(-149) * z[3]) / T(3) + T(27) * z[4] + (T(325) * z[5]) / T(6) + (T(7) * z[12]) / T(2);
z[29] = z[19] * z[29];
return z[20] + z[21] + z[22] + z[23] + z[24] + z[25] + z[26] + z[27] + z[28] + z[29] / T(8);
}



template IntegrandConstructorType<double> f_4_76_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_76_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_76_construct (const Kin<qd_real>&);
#endif

}