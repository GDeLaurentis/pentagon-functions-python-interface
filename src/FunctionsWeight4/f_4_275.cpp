#include "f_4_275.h"

namespace PentagonFunctions {

template <typename T> T f_4_275_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_275_W_21 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_275_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(2) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3]) + kin.v[0] * (T(2) * kin.v[0] + T(4) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3]) + T(2) * prod_pow(kin.v[3], 2) + kin.v[2] * (T(2) * kin.v[2] + T(4) * kin.v[3]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((-kin.v[1] / T(2) + (T(-3) * kin.v[0]) / T(4) + (T(3) * kin.v[2]) / T(2) + kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[0] + (-kin.v[3] / T(2) + kin.v[1] / T(4) + kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[1] + kin.v[2] * (-kin.v[3] / T(2) + (T(-3) * kin.v[2]) / T(4) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(4) + T(-1) + kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((-kin.v[1] / T(2) + (T(-3) * kin.v[0]) / T(4) + (T(3) * kin.v[2]) / T(2) + kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[0] + (-kin.v[3] / T(2) + kin.v[1] / T(4) + kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[1] + kin.v[2] * (-kin.v[3] / T(2) + (T(-3) * kin.v[2]) / T(4) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(4) + T(-1) + kin.v[4])) + rlog(-kin.v[1]) * (((T(3) * kin.v[2]) / T(4) + kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(4) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (-kin.v[3] / T(2) + (T(3) * kin.v[0]) / T(4) + kin.v[1] / T(2) + (T(-3) * kin.v[2]) / T(2) + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(4) + -kin.v[2] / T(2) + kin.v[3] / T(2) + T(-1) + kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (((T(3) * kin.v[2]) / T(4) + kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(4) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[0] * (-kin.v[3] / T(2) + (T(3) * kin.v[0]) / T(4) + kin.v[1] / T(2) + (T(-3) * kin.v[2]) / T(2) + T(-1) + kin.v[4]) + kin.v[1] * (-kin.v[1] / T(4) + -kin.v[2] / T(2) + kin.v[3] / T(2) + T(-1) + kin.v[4]));
c[1] = rlog(-kin.v[1]) * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[1]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[1]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[4] * (prod_pow(abb[2], 2) + -abb[3]) + abb[3] * (abb[5] + abb[7] + -abb[6]) + prod_pow(abb[1], 2) * (abb[5] + abb[7] + -abb[4] + -abb[6]) + prod_pow(abb[2], 2) * (abb[6] + -abb[5] + -abb[7]));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_275_construct (const Kin<T>& kin) {
    return [&kin, 
            dl21 = DLog_W_21<T>(kin),dl25 = DLog_W_25<T>(kin),dl19 = DLog_W_19<T>(kin),dl2 = DLog_W_2<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),spdl21 = SpDLog_f_4_275_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,17> abbr = 
            {dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_15(kin_path), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl25(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[24] / kin_path.W[24]), dl19(t), dl2(t), dl18(t), dl20(t)}
;

        auto result = f_4_275_abbreviated(abbr);
        result = result + spdl21(t, abbr);

        return result;
    };
}

template <typename T> T f_4_275_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[54];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[8];
z[3] = abb[13];
z[4] = abb[14];
z[5] = abb[16];
z[6] = abb[5];
z[7] = abb[15];
z[8] = abb[6];
z[9] = abb[7];
z[10] = abb[12];
z[11] = abb[2];
z[12] = abb[9];
z[13] = bc<TR>[0];
z[14] = abb[3];
z[15] = abb[10];
z[16] = abb[11];
z[17] = bc<TR>[2];
z[18] = bc<TR>[9];
z[19] = int_to_imaginary<T>(1) * z[13];
z[20] = -z[12] + z[19];
z[21] = T(3) * z[20];
z[22] = z[0] / T(2);
z[23] = z[21] + -z[22];
z[23] = z[0] * z[23];
z[24] = z[16] * z[19];
z[25] = -z[15] + z[24];
z[26] = z[0] + z[20];
z[27] = z[11] * z[26];
z[28] = T(2) * z[27];
z[29] = T(2) * z[19];
z[29] = z[12] * z[29];
z[30] = prod_pow(z[12], 2);
z[31] = prod_pow(z[13], 2);
z[23] = z[23] + T(-5) * z[25] + z[28] + -z[29] + (T(7) * z[30]) / T(2) + (T(-8) * z[31]) / T(3);
z[23] = z[1] * z[23];
z[32] = int_to_imaginary<T>(1) * prod_pow(z[13], 3);
z[33] = z[17] * z[31];
z[23] = T(-7) * z[18] + z[23] + (T(-7) * z[32]) / T(6) + T(4) * z[33];
z[23] = z[2] * z[23];
z[21] = z[21] + z[22];
z[21] = z[0] * z[21];
z[33] = T(4) * z[27];
z[34] = (T(4) * z[31]) / T(3);
z[21] = z[21] + T(-7) * z[25] + z[29] + (T(5) * z[30]) / T(2) + z[33] + -z[34];
z[21] = z[2] * z[21];
z[35] = T(3) * z[24];
z[36] = z[31] / T(3);
z[37] = T(-3) * z[15] + -z[29] + z[35] + -z[36];
z[38] = -z[20] + z[22];
z[38] = z[0] * z[38];
z[26] = T(2) * z[26];
z[39] = z[11] + z[26];
z[39] = z[11] * z[39];
z[40] = z[30] / T(2);
z[41] = -z[14] + z[40];
z[39] = z[37] + z[38] + -z[39] + -z[41];
z[39] = z[3] * z[39];
z[42] = T(2) * z[15];
z[43] = T(2) * z[24] + -z[42];
z[44] = z[30] + -z[43];
z[45] = -z[36] + z[44];
z[46] = prod_pow(z[0], 2);
z[47] = T(2) * z[14];
z[48] = z[28] + z[45] + -z[46] + -z[47];
z[48] = z[4] * z[48];
z[21] = z[21] + z[39] + -z[48];
z[39] = z[0] + T(2) * z[20];
z[39] = z[0] * z[39];
z[47] = z[39] + z[47];
z[49] = z[44] + z[47];
z[50] = -z[34] + z[49];
z[51] = z[7] * z[50];
z[51] = -z[21] + z[51];
z[51] = z[9] * z[51];
z[52] = z[12] * z[19];
z[52] = T(4) * z[52];
z[53] = prod_pow(z[11], 2);
z[43] = z[30] + -z[34] + z[43] + -z[47] + -z[52] + T(-2) * z[53];
z[43] = z[3] * z[43];
z[47] = z[29] + -z[30] + z[31];
z[53] = z[0] + -z[20];
z[53] = z[0] * z[53];
z[47] = z[27] + T(2) * z[47] + z[53];
z[47] = z[2] * z[47];
z[26] = -z[11] + z[26];
z[26] = z[11] * z[26];
z[53] = -z[14] + z[26];
z[44] = (T(2) * z[31]) / T(3) + -z[44] + -z[53];
z[44] = z[7] * z[44];
z[44] = z[44] + z[47];
z[43] = z[43] + T(2) * z[44] + z[48];
z[43] = z[10] * z[43];
z[44] = -z[31] + z[49];
z[44] = z[7] * z[44];
z[21] = -z[21] + z[44];
z[21] = z[6] * z[21];
z[42] = z[14] + -z[42];
z[33] = T(4) * z[24] + -z[31] + -z[33] + T(2) * z[42] + -z[52];
z[33] = z[10] * z[33];
z[25] = -z[25] + -z[29] + (T(3) * z[30]) / T(2);
z[20] = z[20] + z[22];
z[20] = z[0] * z[20];
z[22] = T(-4) * z[14] + T(-3) * z[20] + -z[25] + z[28] + T(2) * z[31];
z[22] = z[1] * z[22];
z[20] = z[20] + z[28] + -z[37] + z[40];
z[20] = z[8] * z[20];
z[28] = z[6] + z[9];
z[28] = z[28] * z[50];
z[19] = z[17] * z[19];
z[29] = T(7) * z[19] + (T(3) * z[31]) / T(2);
z[29] = z[17] * z[29];
z[30] = z[29] + (T(5) * z[32]) / T(3);
z[20] = (T(29) * z[18]) / T(8) + z[20] + z[22] + z[28] + z[30] / T(2) + z[33];
z[20] = z[5] * z[20];
z[22] = z[15] + z[27] + z[41];
z[24] = z[22] + -z[24] + -z[31] / T(6) + -z[46] / T(2);
z[27] = -(z[1] * z[24]);
z[22] = T(-3) * z[22] + z[31] / T(2) + z[35] + (T(3) * z[46]) / T(2);
z[28] = z[8] * z[22];
z[30] = T(-5) * z[19] + (T(-9) * z[31]) / T(2);
z[30] = z[17] * z[30];
z[33] = z[32] / T(6);
z[30] = z[30] + z[33];
z[27] = (T(5) * z[18]) / T(8) + z[27] + z[28] + z[30] / T(2);
z[27] = z[4] * z[27];
z[22] = z[1] * z[22];
z[19] = -z[19] + (T(-5) * z[31]) / T(2);
z[19] = z[17] * z[19];
z[19] = z[19] + z[33];
z[19] = z[18] / T(8) + z[19] / T(2) + z[22];
z[19] = z[3] * z[19];
z[22] = z[3] * z[24];
z[24] = z[25] + -z[34] + -z[38];
z[24] = z[2] * z[24];
z[22] = z[22] + z[24];
z[22] = z[8] * z[22];
z[24] = T(-3) * z[14] + z[26] + z[36] + -z[39];
z[24] = z[8] * z[24];
z[25] = z[29] + z[32];
z[26] = z[45] + z[53];
z[26] = z[1] * z[26];
z[24] = (T(21) * z[18]) / T(8) + z[24] + z[25] / T(2) + z[26];
z[24] = z[7] * z[24];
return z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[27] + z[43] + z[51];
}



template IntegrandConstructorType<double> f_4_275_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_275_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_275_construct (const Kin<qd_real>&);
#endif

}