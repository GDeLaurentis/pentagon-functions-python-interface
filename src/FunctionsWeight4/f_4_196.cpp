#include "f_4_196.h"

namespace PentagonFunctions {

template <typename T> T f_4_196_abbreviated (const std::array<T,22>&);

template <typename T> class SpDLog_f_4_196_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_196_W_22 (const Kin<T>& kin) {
        c[0] = ((T(3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * kin.v[0] + (T(3) / T(2) + (T(3) * kin.v[3]) / T(8) + (T(-3) * kin.v[4]) / T(4)) * kin.v[3] + (T(-3) / T(2) + (T(3) * kin.v[0]) / T(2) + (T(3) * kin.v[1]) / T(8) + (T(-3) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(4)) * kin.v[1] + (T(3) / T(2) + (T(-9) * kin.v[4]) / T(8)) * kin.v[4] + (T(-3) / T(2) + (T(-9) * kin.v[2]) / T(8) + (T(3) * kin.v[3]) / T(4) + (T(9) * kin.v[4]) / T(4)) * kin.v[2];
c[1] = (T(-3) / T(2) + (T(-3) * kin.v[3]) / T(2)) * kin.v[2] + ((T(3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * kin.v[0] + kin.v[1] * (T(-3) / T(2) + (T(3) * kin.v[0]) / T(2) + (T(3) * kin.v[2]) / T(2) + (T(-3) * kin.v[4]) / T(2) + (T(3) / T(2) + bc<T>[0] * int_to_imaginary<T>(8)) * kin.v[1] + T(-3) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(16) * kin.v[2] + T(-16) * kin.v[3] + T(-16) * kin.v[4])) + (T(3) * kin.v[4]) / T(2) + (T(3) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2)) * kin.v[3] + rlog(kin.v[3]) * (((T(21) * kin.v[1]) / T(4) + (T(3) * kin.v[2]) / T(2) + (T(-21) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-9) + T(9) * kin.v[0]) * kin.v[1] + ((T(-15) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(15) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + ((T(21) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + ((T(-15) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + kin.v[0] * (T(9) * kin.v[2] + T(-9) * kin.v[3] + T(-9) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (((T(-21) * kin.v[1]) / T(4) + (T(-3) * kin.v[2]) / T(2) + (T(21) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(9) + T(-9) * kin.v[0]) * kin.v[1] + ((T(15) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(-15) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + ((T(-21) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + ((T(15) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + kin.v[0] * (T(-9) * kin.v[2] + T(9) * kin.v[3] + T(9) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * prod_pow(kin.v[4], 2) + kin.v[2] * (T(8) * kin.v[2] + T(-16) * kin.v[3] + T(-16) * kin.v[4]) + kin.v[3] * (T(8) * kin.v[3] + T(16) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (((T(31) * kin.v[2]) / T(2) + (T(-31) * kin.v[3]) / T(2) + (T(-31) * kin.v[4]) / T(2)) * kin.v[0] + (T(-31) / T(2) + (T(-117) * kin.v[2]) / T(8) + (T(55) * kin.v[3]) / T(4) + (T(117) * kin.v[4]) / T(4)) * kin.v[2] + (T(31) / T(2) + (T(7) * kin.v[3]) / T(8) + (T(-55) * kin.v[4]) / T(4)) * kin.v[3] + kin.v[1] * (T(-31) / T(2) + (T(31) * kin.v[0]) / T(2) + (T(-55) * kin.v[2]) / T(4) + (T(-7) * kin.v[3]) / T(4) + (T(55) * kin.v[4]) / T(4) + (T(7) / T(8) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[0] + T(-8) * kin.v[3])) + (T(31) / T(2) + (T(-117) * kin.v[4]) / T(8)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(8) + T(4) * kin.v[3]) + kin.v[0] * (T(8) * kin.v[2] + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[4] * (T(8) + T(-4) * kin.v[4]) + kin.v[2] * (T(-8) + T(-4) * kin.v[2] + T(8) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (((T(39) * kin.v[2]) / T(4) + (T(-29) * kin.v[3]) / T(2) + (T(-39) * kin.v[4]) / T(2) + T(5)) * kin.v[2] + ((T(19) * kin.v[3]) / T(4) + (T(29) * kin.v[4]) / T(2) + T(-5)) * kin.v[3] + kin.v[1] * ((T(29) * kin.v[2]) / T(2) + (T(-19) * kin.v[3]) / T(2) + (T(-29) * kin.v[4]) / T(2) + T(5) + T(-5) * kin.v[0] + (T(19) / T(4) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[0] + T(8) * kin.v[3])) + ((T(39) * kin.v[4]) / T(4) + T(-5)) * kin.v[4] + kin.v[0] * (T(-5) * kin.v[2] + T(5) * kin.v[3] + T(5) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-8) + T(-4) * kin.v[3]) + kin.v[2] * (T(8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[4] * (T(-8) + T(4) * kin.v[4]) + kin.v[0] * (T(-8) * kin.v[2] + T(8) * kin.v[3] + T(8) * kin.v[4])));
c[2] = (T(3) * kin.v[1]) / T(2) + (T(3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2);
c[3] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (T(3) * kin.v[1] + T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(3)) * kin.v[1] + T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[2] + T(-8) * kin.v[3] + T(-8) * kin.v[4])) + rlog(kin.v[3]) * (T(-3) * kin.v[1] + T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-9) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2) + (T(-9) / T(2) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[2] + T(8) * kin.v[3] + T(8) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[0] * (abb[4] * (prod_pow(abb[2], 2) * T(-3) + abb[3] * T(-3)) + abb[3] * ((abb[7] * T(-9)) / T(2) + (abb[6] * T(-3)) / T(2) + abb[5] * T(3) + abb[10] * T(3)) + abb[1] * (abb[7] * (abb[9] * T(-8) + bc<T>[0] * int_to_imaginary<T>(-8)) + abb[10] * bc<T>[0] * int_to_imaginary<T>(8) + abb[1] * ((abb[6] * T(-3)) / T(2) + (abb[7] * T(11)) / T(2) + -abb[10] + abb[5] * T(-3) + abb[4] * T(3)) + abb[9] * abb[10] * T(8) + abb[8] * (abb[10] * T(-8) + abb[7] * T(8))) + abb[2] * (abb[9] * abb[10] * T(-8) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[2] * (abb[10] + (abb[7] * T(-11)) / T(2) + (abb[6] * T(3)) / T(2) + abb[5] * T(3)) + abb[7] * (bc<T>[0] * int_to_imaginary<T>(8) + abb[9] * T(8)) + abb[8] * (abb[7] * T(-8) + abb[10] * T(8))));
    }
};
template <typename T> class SpDLog_f_4_196_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_196_W_23 (const Kin<T>& kin) {
        c[0] = (T(-3) / T(2) + (T(3) * kin.v[0]) / T(8) + (T(3) * kin.v[1]) / T(2) + (T(3) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(4)) * kin.v[0] + (T(3) / T(2) + (T(3) * kin.v[3]) / T(8) + (T(3) * kin.v[4]) / T(4)) * kin.v[3] + (T(-3) / T(2) + (T(-9) * kin.v[4]) / T(8)) * kin.v[4] + ((T(-3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2)) * kin.v[1] + (T(3) / T(2) + (T(-9) * kin.v[2]) / T(8) + (T(-3) * kin.v[3]) / T(4) + (T(9) * kin.v[4]) / T(4)) * kin.v[2];
c[1] = (T(3) / T(2) + (T(9) * kin.v[3]) / T(2) + (T(-15) * kin.v[4]) / T(2)) * kin.v[3] + kin.v[2] * (T(3) / T(2) + (T(15) * kin.v[3]) / T(2) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + ((T(-3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2)) * kin.v[1] + kin.v[4] * (T(-3) / T(2) + T(3) * kin.v[4]) + kin.v[0] * (T(-3) / T(2) + (T(9) * kin.v[0]) / T(2) + (T(3) * kin.v[1]) / T(2) + (T(-15) * kin.v[2]) / T(2) + (T(15) * kin.v[4]) / T(2) + T(-9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[1] + T(8) * kin.v[2] + T(-8) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (((T(3) * kin.v[4]) / T(2) + T(6)) * kin.v[4] + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-6) + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[0] * ((T(-9) * kin.v[0]) / T(2) + T(6) + T(-6) * kin.v[1] + T(3) * kin.v[2] + T(9) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[3] * ((T(-9) * kin.v[3]) / T(2) + T(-6) + T(3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[4] * (T(-8) + T(-8) * kin.v[4]) + kin.v[3] * (T(8) + T(8) * kin.v[4]) + kin.v[1] * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4]) + kin.v[2] * (T(8) + T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(16) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (((T(39) * kin.v[2]) / T(4) + (T(29) * kin.v[3]) / T(2) + (T(-39) * kin.v[4]) / T(2) + T(-5)) * kin.v[2] + ((T(19) * kin.v[3]) / T(4) + (T(-29) * kin.v[4]) / T(2) + T(-5)) * kin.v[3] + kin.v[0] * ((T(-29) * kin.v[2]) / T(2) + (T(-19) * kin.v[3]) / T(2) + (T(29) * kin.v[4]) / T(2) + T(5) + (T(19) / T(4) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(-5) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[1] + T(8) * kin.v[3])) + ((T(39) * kin.v[4]) / T(4) + T(5)) * kin.v[4] + kin.v[1] * (T(5) * kin.v[2] + T(5) * kin.v[3] + T(-5) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-8) + T(-4) * kin.v[3]) + kin.v[2] * (T(-8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[1] * (T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[4] * (T(8) + T(4) * kin.v[4]))) + rlog(kin.v[3]) * (((T(-39) * kin.v[2]) / T(4) + (T(-29) * kin.v[3]) / T(2) + (T(39) * kin.v[4]) / T(2) + T(5)) * kin.v[2] + ((T(-19) * kin.v[3]) / T(4) + (T(29) * kin.v[4]) / T(2) + T(5)) * kin.v[3] + kin.v[0] * ((T(29) * kin.v[2]) / T(2) + (T(19) * kin.v[3]) / T(2) + (T(-29) * kin.v[4]) / T(2) + T(-5) + (T(-19) / T(4) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + T(5) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[1] + T(-8) * kin.v[3])) + ((T(-39) * kin.v[4]) / T(4) + T(-5)) * kin.v[4] + kin.v[1] * (T(-5) * kin.v[2] + T(-5) * kin.v[3] + T(5) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(8) + T(4) * kin.v[3]) + kin.v[4] * (T(-8) + T(-4) * kin.v[4]) + kin.v[2] * (T(8) + T(-4) * kin.v[2] + T(8) * kin.v[4]) + kin.v[1] * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (((T(-25) * kin.v[2]) / T(2) + (T(-25) * kin.v[3]) / T(2) + (T(25) * kin.v[4]) / T(2)) * kin.v[1] + (T(25) / T(2) + (T(-99) * kin.v[2]) / T(8) + (T(-49) * kin.v[3]) / T(4) + (T(99) * kin.v[4]) / T(4)) * kin.v[2] + (T(25) / T(2) + kin.v[3] / T(8) + (T(49) * kin.v[4]) / T(4)) * kin.v[3] + kin.v[0] * (-kin.v[3] / T(4) + T(-25) / T(2) + (T(25) * kin.v[1]) / T(2) + (T(49) * kin.v[2]) / T(4) + (T(-49) * kin.v[4]) / T(4) + (T(1) / T(8) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[1] + T(-8) * kin.v[3])) + (T(-25) / T(2) + (T(-99) * kin.v[4]) / T(8)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(8) + T(4) * kin.v[3]) + kin.v[4] * (T(-8) + T(-4) * kin.v[4]) + kin.v[2] * (T(8) + T(-4) * kin.v[2] + T(8) * kin.v[4]) + kin.v[1] * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4])));
c[2] = (T(3) * kin.v[0]) / T(2) + (T(-3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2);
c[3] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(15) * kin.v[2]) / T(2) + (T(15) * kin.v[3]) / T(2) + (T(-15) * kin.v[4]) / T(2) + (T(-15) / T(2) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4])) + rlog(kin.v[3]) * ((T(-3) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (T(6) * kin.v[0] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(3)) * kin.v[0] + T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[11] * (abb[12] * ((abb[7] * T(-15)) / T(2) + (abb[13] * T(-3)) / T(2) + abb[10] * T(3) + abb[5] * T(6)) + abb[4] * (abb[12] * T(-3) + abb[9] * (-abb[9] + bc<T>[0] * int_to_imaginary<T>(8) + abb[2] * T(8))) + abb[9] * (abb[9] * (abb[10] + (abb[7] * T(-5)) / T(2) + (abb[13] * T(3)) / T(2)) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[7] * bc<T>[0] * int_to_imaginary<T>(8) + abb[2] * (abb[10] * T(-8) + abb[7] * T(8))) + abb[1] * (abb[1] * (abb[4] + (abb[13] * T(-3)) / T(2) + (abb[7] * T(5)) / T(2) + -abb[10]) + abb[7] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[4] * (abb[2] * T(-8) + bc<T>[0] * int_to_imaginary<T>(-8)) + abb[10] * bc<T>[0] * int_to_imaginary<T>(8) + abb[2] * (abb[7] * T(-8) + abb[10] * T(8))) + abb[8] * (abb[4] * abb[9] * T(-8) + abb[1] * (abb[10] * T(-8) + abb[4] * T(8) + abb[7] * T(8)) + abb[9] * (abb[7] * T(-8) + abb[10] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_196_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl23 = DLog_W_23<T>(kin),dl20 = DLog_W_20<T>(kin),dl9 = DLog_W_9<T>(kin),dl17 = DLog_W_17<T>(kin),dl4 = DLog_W_4<T>(kin),dl16 = DLog_W_16<T>(kin),spdl22 = SpDLog_f_4_196_W_22<T>(kin),spdl23 = SpDLog_f_4_196_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,22> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[16] / kin_path.W[16]), -rlog(t), rlog(kin.W[15] / kin_path.W[15]), rlog(v_path[3]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[19] / kin_path.W[19]), dl23(t), f_2_1_8(kin_path), -rlog(t), dl20(t), rlog(kin.W[8] / kin_path.W[8]), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl9(t), dl17(t), dl4(t), dl16(t)}
;

        auto result = f_4_196_abbreviated(abbr);
        result = result + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_196_abbreviated(const std::array<T,22>& abb)
{
using TR = typename T::value_type;
T z[82];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[14];
z[3] = abb[18];
z[4] = abb[19];
z[5] = abb[20];
z[6] = abb[21];
z[7] = abb[5];
z[8] = abb[6];
z[9] = abb[7];
z[10] = abb[10];
z[11] = abb[13];
z[12] = abb[15];
z[13] = abb[2];
z[14] = abb[8];
z[15] = abb[9];
z[16] = bc<TR>[0];
z[17] = abb[3];
z[18] = abb[12];
z[19] = abb[16];
z[20] = abb[17];
z[21] = bc<TR>[2];
z[22] = bc<TR>[9];
z[23] = z[6] * z[11];
z[24] = z[4] * z[11];
z[25] = z[23] + -z[24];
z[26] = T(5) * z[6];
z[27] = T(41) * z[2] + T(-61) * z[4] + T(-65) * z[5];
z[27] = z[26] + z[27] / T(4);
z[27] = z[1] * z[27];
z[28] = T(2) * z[3];
z[29] = (T(19) * z[2]) / T(3) + T(11) * z[5];
z[29] = (T(25) * z[4]) / T(12) + -z[6] + -z[28] + z[29] / T(4);
z[29] = z[10] * z[29];
z[30] = z[4] / T(2);
z[31] = T(-3) * z[2] + (T(-5) * z[5]) / T(2);
z[31] = T(4) * z[3] + (T(-3) * z[6]) / T(4) + -z[30] + z[31] / T(2);
z[31] = z[12] * z[31];
z[32] = T(9) * z[4];
z[33] = T(19) * z[2] + T(-61) * z[5];
z[33] = -z[32] + z[33] / T(3);
z[33] = (T(2) * z[6]) / T(3) + z[33] / T(4);
z[33] = z[7] * z[33];
z[34] = -z[2] + (T(7) * z[5]) / T(3);
z[28] = (T(-13) * z[4]) / T(12) + (T(37) * z[6]) / T(12) + -z[28] + (T(5) * z[34]) / T(4);
z[28] = z[9] * z[28];
z[34] = z[2] + -z[5];
z[35] = z[11] * z[34];
z[36] = z[2] + z[5];
z[37] = (T(11) * z[3]) / T(2) + -z[36];
z[37] = -z[4] + (T(-5) * z[6]) / T(4) + z[37] / T(2);
z[37] = z[21] * z[37];
z[38] = -z[2] + z[6];
z[39] = z[8] * z[38];
z[27] = (T(-7) * z[25]) / T(4) + z[27] / T(3) + z[28] + z[29] + z[31] + z[33] + (T(-2) * z[35]) / T(3) + (T(3) * z[37]) / T(2) + -z[39] / T(4);
z[28] = prod_pow(z[16], 2);
z[27] = z[27] * z[28];
z[29] = T(2) * z[36];
z[31] = -z[4] + z[29];
z[33] = z[3] / T(2);
z[37] = z[6] / T(2);
z[39] = z[31] + -z[33] + z[37];
z[39] = z[12] * z[39];
z[40] = T(3) * z[4];
z[41] = T(4) * z[5];
z[42] = T(-5) * z[3] + z[38] + z[40] + z[41];
z[42] = z[10] * z[42];
z[43] = T(4) * z[4];
z[44] = T(3) * z[3];
z[45] = z[43] + -z[44];
z[46] = T(2) * z[34];
z[47] = -z[6] + -z[45] + z[46];
z[47] = z[1] * z[47];
z[48] = z[4] + z[37];
z[49] = -z[2] + z[33] + z[48];
z[49] = z[9] * z[49];
z[29] = z[29] + -z[44];
z[50] = T(3) * z[6];
z[51] = -z[4] + z[50];
z[52] = T(2) * z[29] + z[51];
z[52] = z[7] * z[52];
z[53] = z[25] + z[35];
z[39] = z[39] + z[42] + z[47] + -z[49] + -z[52] + T(2) * z[53];
z[39] = T(3) * z[39];
z[42] = z[20] * z[39];
z[47] = T(2) * z[2];
z[49] = T(5) * z[5] + z[47];
z[52] = T(9) * z[3];
z[53] = T(7) * z[4];
z[49] = T(8) * z[6] + T(2) * z[49] + -z[52] + z[53];
z[49] = z[10] * z[49];
z[54] = T(3) * z[5];
z[55] = z[2] + z[54];
z[56] = T(6) * z[4];
z[52] = -z[6] + z[52] + T(-2) * z[55] + -z[56];
z[52] = z[7] * z[52];
z[55] = -z[47] + z[54];
z[55] = T(9) * z[6] + -z[44] + T(2) * z[55];
z[56] = z[55] + z[56];
z[56] = z[1] * z[56];
z[57] = z[3] + z[6];
z[58] = (T(3) * z[12]) / T(2);
z[57] = z[57] * z[58];
z[59] = T(4) * z[23] + -z[35];
z[49] = z[49] + z[52] + -z[56] + z[57] + T(2) * z[59];
z[49] = z[0] * z[49];
z[52] = T(5) * z[2];
z[56] = z[6] + z[52];
z[57] = T(6) * z[3];
z[59] = z[41] + z[43] + -z[56] + -z[57];
z[59] = z[10] * z[59];
z[60] = z[4] + z[5];
z[61] = z[6] + z[47] + z[60];
z[61] = z[1] * z[61];
z[62] = -z[24] + z[35];
z[61] = z[61] + T(4) * z[62];
z[51] = z[34] + -z[44] + z[51];
z[62] = T(2) * z[7];
z[63] = z[51] * z[62];
z[59] = z[59] + T(2) * z[61] + -z[63];
z[61] = -(z[15] * z[59]);
z[51] = z[1] * z[51];
z[63] = T(2) * z[6];
z[64] = -z[44] + z[63];
z[65] = T(2) * z[4];
z[66] = z[36] + -z[64] + -z[65];
z[66] = z[10] * z[66];
z[67] = T(4) * z[25];
z[51] = z[51] + z[66] + -z[67];
z[66] = -z[6] + -z[29];
z[68] = T(3) * z[12];
z[66] = z[66] * z[68];
z[50] = -z[50] + z[57];
z[69] = T(4) * z[2] + -z[50] + -z[60];
z[69] = z[62] * z[69];
z[51] = T(2) * z[51] + z[66] + z[69];
z[51] = z[14] * z[51];
z[66] = T(2) * z[5];
z[69] = z[33] + z[66];
z[69] = z[4] + (T(33) * z[6]) / T(2) + T(3) * z[69];
z[69] = z[0] * z[69];
z[55] = z[55] + -z[65];
z[55] = z[14] * z[55];
z[70] = z[65] + z[66];
z[71] = -z[2] + T(7) * z[6] + z[70];
z[72] = -(z[15] * z[71]);
z[55] = z[55] + z[69] + z[72];
z[55] = z[9] * z[55];
z[45] = -z[36] + -z[45] + z[63];
z[45] = z[1] * z[45];
z[72] = z[4] + z[36];
z[73] = z[63] + z[72];
z[74] = z[10] * z[73];
z[45] = -z[25] + z[45] + -z[74];
z[64] = T(8) * z[36] + -z[53] + z[64];
z[64] = z[7] * z[64];
z[31] = -z[3] + z[31];
z[31] = z[31] * z[68];
z[45] = -z[31] + T(-2) * z[45] + z[64];
z[64] = -z[4] + z[36];
z[74] = z[63] + z[64];
z[75] = T(4) * z[9];
z[76] = -(z[74] * z[75]);
z[76] = z[45] + z[76];
z[76] = z[13] * z[76];
z[77] = -z[33] + -z[52] + z[54];
z[78] = z[6] / T(4);
z[77] = z[40] + z[77] / T(2) + -z[78];
z[77] = prod_pow(z[21], 2) * z[77];
z[79] = -z[2] + (T(3) * z[5]) / T(2);
z[33] = (T(19) * z[4]) / T(3) + (T(-5) * z[6]) / T(6) + -z[33] + T(5) * z[79];
z[28] = z[28] * z[33];
z[28] = z[28] / T(2) + z[42] + -z[49] + z[51] + z[55] + z[61] + z[76] + T(3) * z[77];
z[28] = int_to_imaginary<T>(1) * z[16] * z[28];
z[33] = (T(3) * z[3]) / T(2);
z[42] = -z[33] + -z[34] + z[37] + -z[65];
z[42] = z[1] * z[42];
z[51] = (T(3) * z[6]) / T(2);
z[55] = (T(11) * z[4]) / T(2) + z[51];
z[61] = T(9) * z[5];
z[76] = (T(9) * z[3]) / T(2);
z[77] = (T(-11) * z[2]) / T(2) + -z[55] + z[61] + z[76];
z[77] = z[10] * z[77];
z[55] = -z[34] + z[55] + -z[57];
z[55] = z[7] * z[55];
z[57] = -z[4] + z[33] + -z[51];
z[57] = z[57] * z[58];
z[79] = T(5) * z[24];
z[80] = z[6] * z[8];
z[42] = (T(-5) * z[23]) / T(2) + T(9) * z[35] + z[42] + z[55] + z[57] + z[77] + z[79] + (T(9) * z[80]) / T(2);
z[55] = prod_pow(z[0], 2);
z[42] = z[42] * z[55];
z[57] = z[2] / T(2);
z[77] = (T(5) * z[4]) / T(2);
z[80] = T(7) * z[5];
z[76] = z[37] + z[57] + z[76] + -z[77] + -z[80];
z[76] = z[10] * z[76];
z[33] = -z[33] + T(-4) * z[34] + -z[51] + z[53];
z[33] = z[1] * z[33];
z[53] = -z[44] + z[47];
z[30] = -z[30] + z[51] + z[53] + z[80];
z[30] = z[7] * z[30];
z[48] = (T(-5) * z[3]) / T(2) + z[48];
z[48] = z[48] * z[58];
z[30] = z[25] + z[30] + z[33] + T(-3) * z[35] + z[48] + z[76];
z[30] = z[14] * z[30];
z[30] = z[30] + z[49];
z[30] = z[14] * z[30];
z[33] = -(z[19] * z[39]);
z[39] = (T(11) * z[6]) / T(2) + z[52] + -z[65] + z[66];
z[39] = z[9] * z[39];
z[48] = z[26] + -z[72];
z[48] = z[1] * z[48];
z[49] = z[5] + -z[6];
z[49] = z[49] * z[68];
z[52] = z[2] + z[4];
z[58] = z[52] + -z[63];
z[76] = -z[41] + -z[58];
z[76] = z[7] * z[76];
z[81] = z[6] + -z[72];
z[81] = z[10] * z[81];
z[37] = -z[2] + -z[37];
z[37] = z[8] * z[37];
z[37] = T(3) * z[37] + z[39] + z[48] + z[49] + -z[67] + z[76] + z[81];
z[37] = z[13] * z[37];
z[39] = T(5) * z[4];
z[29] = z[29] + z[39];
z[29] = z[1] * z[29];
z[48] = z[52] + -z[54];
z[48] = z[10] * z[48];
z[49] = T(4) * z[35];
z[29] = z[23] + z[29] + T(2) * z[48] + -z[49] + -z[79];
z[36] = T(10) * z[36] + -z[39] + -z[44] + -z[63];
z[36] = z[7] * z[36];
z[29] = T(2) * z[29] + -z[31] + z[36];
z[29] = z[0] * z[29];
z[31] = z[24] + z[35];
z[36] = -(z[1] * z[73]);
z[39] = -z[58] + z[80];
z[39] = z[10] * z[39];
z[44] = z[63] + -z[72];
z[44] = z[7] * z[44];
z[31] = T(4) * z[31] + z[36] + z[39] + z[44];
z[31] = z[15] * z[31];
z[36] = -(z[14] * z[45]);
z[39] = z[14] * z[74];
z[44] = -(z[15] * z[73]);
z[45] = z[0] * z[65];
z[39] = z[39] + z[44] + z[45];
z[39] = z[39] * z[75];
z[29] = z[29] + T(2) * z[31] + z[36] + z[37] + z[39];
z[29] = z[13] * z[29];
z[31] = T(-11) * z[2] + z[32] + -z[61] + z[63];
z[31] = z[7] * z[31];
z[25] = -z[25] + z[35];
z[32] = -z[2] + -z[40] + z[54];
z[32] = T(11) * z[6] + T(2) * z[32];
z[32] = z[1] * z[32];
z[36] = -z[4] + z[5];
z[36] = -z[26] + T(8) * z[36];
z[36] = z[10] * z[36];
z[37] = -z[6] + z[64];
z[37] = z[37] * z[68];
z[39] = -(z[8] * z[51]);
z[25] = T(10) * z[25] + z[31] + z[32] + z[36] + z[37] + z[39];
z[25] = z[17] * z[25];
z[31] = -z[52] + -z[63] + z[66];
z[31] = z[10] * z[31];
z[32] = -z[4] + z[34];
z[36] = z[6] + -z[32];
z[36] = z[1] * z[36];
z[32] = -(z[7] * z[32]);
z[31] = (T(-3) * z[23]) / T(2) + -z[24] + z[31] + z[32] + z[36] + z[49];
z[31] = z[15] * z[31];
z[32] = z[14] * z[59];
z[36] = z[1] * z[38];
z[35] = T(-8) * z[35] + z[36];
z[36] = T(7) * z[2] + T(-18) * z[5] + z[50] + -z[65];
z[36] = z[10] * z[36];
z[37] = z[6] + z[53];
z[37] = z[37] * z[62];
z[35] = T(2) * z[35] + z[36] + z[37];
z[35] = z[0] * z[35];
z[31] = z[31] + z[32] + z[35];
z[31] = z[15] * z[31];
z[32] = z[56] + z[70];
z[32] = z[0] * z[32];
z[35] = z[14] * z[71];
z[36] = (T(7) * z[6]) / T(2) + -z[47] + z[60];
z[36] = z[15] * z[36];
z[32] = z[32] + z[35] + z[36];
z[32] = z[15] * z[32];
z[35] = (T(9) * z[3]) / T(4) + (T(-15) * z[6]) / T(4) + -z[54] + z[57] + z[77];
z[35] = z[14] * z[35];
z[35] = z[35] + -z[69];
z[35] = z[14] * z[35];
z[36] = (T(-5) * z[2]) / T(2) + (T(-3) * z[3]) / T(4) + (T(-7) * z[4]) / T(2) + -z[5] + z[78];
z[36] = z[36] * z[55];
z[37] = (T(29) * z[6]) / T(2) + T(-5) * z[34] + -z[65];
z[37] = z[18] * z[37];
z[38] = -z[2] + z[41] + -z[43];
z[38] = (T(35) * z[6]) / T(2) + T(2) * z[38];
z[38] = z[17] * z[38];
z[32] = z[32] + z[35] + z[36] + z[37] + z[38];
z[32] = z[9] * z[32];
z[24] = z[24] + z[49];
z[35] = z[26] + -z[46];
z[35] = z[1] * z[35];
z[26] = -z[26] + T(-11) * z[34];
z[26] = z[10] * z[26];
z[34] = -z[4] + -z[63];
z[34] = z[34] * z[62];
z[23] = (T(-11) * z[23]) / T(2) + T(4) * z[24] + z[26] + z[34] + z[35];
z[23] = z[18] * z[23];
z[24] = T(-1123) * z[2] + T(769) * z[5];
z[24] = (T(-105) * z[3]) / T(2) + z[24] / T(3);
z[24] = (T(431) * z[4]) / T(3) + (T(279) * z[6]) / T(4) + z[24] / T(2);
z[24] = z[22] * z[24];
return z[23] + z[24] / T(4) + z[25] + z[27] + z[28] + z[29] + z[30] + z[31] + z[32] + z[33] + z[42];
}



template IntegrandConstructorType<double> f_4_196_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_196_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_196_construct (const Kin<qd_real>&);
#endif

}