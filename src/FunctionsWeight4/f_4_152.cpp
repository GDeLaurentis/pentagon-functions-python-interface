#include "f_4_152.h"

namespace PentagonFunctions {

template <typename T> T f_4_152_abbreviated (const std::array<T,21>&);

template <typename T> class SpDLog_f_4_152_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_152_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(6) * kin.v[2] + T(-12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[1] * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[1] + T(12) * kin.v[2] + T(-12) * kin.v[3] + T(-12) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(16) * kin.v[2] + T(-16) * kin.v[3] + T(-16) * kin.v[4])) + T(6) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(6) * kin.v[3] + T(12) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * prod_pow(kin.v[4], 2) + kin.v[2] * (T(8) * kin.v[2] + T(-16) * kin.v[3] + T(-16) * kin.v[4]) + kin.v[3] * (T(8) * kin.v[3] + T(16) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((-kin.v[4] / T(2) + T(14)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-14) + T(-13) * kin.v[3] + kin.v[4]) + kin.v[0] * (T(14) * kin.v[2] + T(-14) * kin.v[3] + T(-14) * kin.v[4]) + kin.v[1] * (T(-14) + T(14) * kin.v[0] + (T(27) / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[1] + T(13) * kin.v[2] + T(-27) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(8) * kin.v[0] + T(-8) * kin.v[3]) + T(-13) * kin.v[4]) + kin.v[3] * ((T(27) * kin.v[3]) / T(2) + T(14) + T(13) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(8) + T(4) * kin.v[3]) + kin.v[0] * (T(8) * kin.v[2] + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[4] * (T(8) + T(-4) * kin.v[4]) + kin.v[2] * (T(-8) + T(-4) * kin.v[2] + T(8) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[2] * (kin.v[2] / T(2) + -kin.v[4] + T(14) + T(13) * kin.v[3]) + (kin.v[4] / T(2) + T(-14)) * kin.v[4] + kin.v[3] * ((T(-27) * kin.v[3]) / T(2) + T(-14) + T(-13) * kin.v[4]) + kin.v[1] * (T(14) + T(-14) * kin.v[0] + (T(-27) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(-13) * kin.v[2] + T(27) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(-8) * kin.v[0] + T(8) * kin.v[3]) + T(13) * kin.v[4]) + kin.v[0] * (T(-14) * kin.v[2] + T(14) * kin.v[3] + T(14) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-8) + T(-4) * kin.v[3]) + kin.v[2] * (T(8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[4] * (T(-8) + T(4) * kin.v[4]) + kin.v[0] * (T(-8) * kin.v[2] + T(8) * kin.v[3] + T(8) * kin.v[4])));
c[1] = rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(6)) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[2] + T(-8) * kin.v[3] + T(-8) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[2] + T(8) * kin.v[3] + T(8) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[4] * abb[5] * abb[7] * T(-8) + abb[6] * abb[7] * T(6) + abb[4] * (abb[7] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[4] * abb[7] * T(7)) + abb[1] * (abb[6] * T(-6) + abb[4] * (abb[4] * T(-7) + bc<T>[0] * int_to_imaginary<T>(8)) + abb[4] * abb[5] * T(8)) + abb[2] * (abb[2] * (abb[1] + -abb[7]) + abb[4] * abb[7] * T(-6) + abb[7] * bc<T>[0] * int_to_imaginary<T>(8) + abb[1] * (abb[5] * T(-8) + bc<T>[0] * int_to_imaginary<T>(-8) + abb[4] * T(6)) + abb[5] * abb[7] * T(8)) + abb[3] * (abb[1] * abb[4] * T(-8) + abb[4] * abb[7] * T(8) + abb[2] * (abb[7] * T(-8) + abb[1] * T(8))));
    }
};
template <typename T> class SpDLog_f_4_152_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_152_W_21 (const Kin<T>& kin) {
        c[0] = (T(9) / T(2) + (T(-27) * kin.v[3]) / T(8) + (T(-9) * kin.v[4]) / T(2)) * kin.v[3] + (T(9) / T(2) + (T(9) * kin.v[2]) / T(8) + (T(-9) * kin.v[3]) / T(4) + (T(-9) * kin.v[4]) / T(2)) * kin.v[2] + (T(-9) / T(2) + (T(9) * kin.v[0]) / T(8) + (T(-9) * kin.v[1]) / T(4) + (T(-9) * kin.v[2]) / T(4) + (T(9) * kin.v[3]) / T(4) + (T(9) * kin.v[4]) / T(2)) * kin.v[0] + (T(-9) / T(2) + (T(-27) * kin.v[1]) / T(8) + (T(9) * kin.v[2]) / T(4) + (T(27) * kin.v[3]) / T(4) + (T(9) * kin.v[4]) / T(2)) * kin.v[1];
c[1] = (T(9) / T(2) + (T(-9) * kin.v[2]) / T(2) + (T(-27) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2)) * kin.v[2] + kin.v[3] * (T(9) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(-9) * kin.v[3]) + kin.v[1] * (T(-9) / T(2) + (T(27) * kin.v[2]) / T(2) + (T(9) * kin.v[4]) / T(2) + T(-9) * kin.v[1] + T(18) * kin.v[3]) + kin.v[0] * (T(-9) / T(2) + (T(-9) * kin.v[0]) / T(2) + (T(-27) * kin.v[1]) / T(2) + (T(27) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2) + T(9) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[1] + T(8) * kin.v[3] + T(8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(8) + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[3] * (T(8) + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[1] * (T(-8) + T(-8) * kin.v[1] + T(8) * kin.v[2] + T(16) * kin.v[3] + T(8) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[2] * (T(12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[3] * (T(12) + T(-6) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[0] * (T(-12) + T(6) * kin.v[0] + T(-12) * kin.v[2] + T(12) * kin.v[4]) + kin.v[1] * (T(-12) + T(-6) * kin.v[1] + T(12) * kin.v[3] + T(12) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[1] * ((T(69) * kin.v[1]) / T(4) + (T(-23) * kin.v[2]) / T(2) + (T(-69) * kin.v[3]) / T(2) + T(23) + T(-23) * kin.v[4]) + kin.v[2] * ((T(-23) * kin.v[2]) / T(4) + (T(23) * kin.v[3]) / T(2) + T(-23) + T(23) * kin.v[4]) + kin.v[3] * ((T(69) * kin.v[3]) / T(4) + T(-23) + T(23) * kin.v[4]) + kin.v[0] * ((T(23) * kin.v[1]) / T(2) + (T(23) * kin.v[2]) / T(2) + (T(-23) * kin.v[3]) / T(2) + T(23) + (T(-23) / T(4) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(-23) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) + T(8) * kin.v[2] + T(-8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(8) + T(4) * kin.v[1] + T(-8) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[2] * (T(-8) + T(-4) * kin.v[2] + T(8) * kin.v[4]) + kin.v[3] * (T(-8) + T(4) * kin.v[3] + T(8) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-31) / T(2) + (T(-117) * kin.v[1]) / T(8) + (T(55) * kin.v[2]) / T(4) + (T(117) * kin.v[3]) / T(4) + (T(31) * kin.v[4]) / T(2)) * kin.v[1] + (T(31) / T(2) + (T(7) * kin.v[2]) / T(8) + (T(-55) * kin.v[3]) / T(4) + (T(-31) * kin.v[4]) / T(2)) * kin.v[2] + (T(31) / T(2) + (T(-117) * kin.v[3]) / T(8) + (T(-31) * kin.v[4]) / T(2)) * kin.v[3] + kin.v[0] * (T(-31) / T(2) + (T(-55) * kin.v[1]) / T(4) + (T(-7) * kin.v[2]) / T(4) + (T(55) * kin.v[3]) / T(4) + (T(31) * kin.v[4]) / T(2) + (T(7) / T(8) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[2] + T(8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[3] * (T(8) + T(-4) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[1] * (T(-8) + T(-4) * kin.v[1] + T(8) * kin.v[3] + T(8) * kin.v[4]))) + rlog(kin.v[2]) * (kin.v[3] * ((T(-69) * kin.v[3]) / T(4) + T(23) + T(-23) * kin.v[4]) + kin.v[2] * ((T(23) * kin.v[2]) / T(4) + (T(-23) * kin.v[3]) / T(2) + T(23) + T(-23) * kin.v[4]) + kin.v[1] * ((T(-69) * kin.v[1]) / T(4) + (T(23) * kin.v[2]) / T(2) + (T(69) * kin.v[3]) / T(2) + T(-23) + T(23) * kin.v[4]) + kin.v[0] * ((T(-23) * kin.v[1]) / T(2) + (T(-23) * kin.v[2]) / T(2) + (T(23) * kin.v[3]) / T(2) + T(-23) + (T(23) / T(4) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + T(23) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) + T(-8) * kin.v[2] + T(8) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(8) + T(4) * kin.v[2] + T(-8) * kin.v[4]) + kin.v[3] * (T(8) + T(-4) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[1] * (T(-8) + T(-4) * kin.v[1] + T(8) * kin.v[3] + T(8) * kin.v[4])));
c[2] = (T(9) * kin.v[0]) / T(2) + (T(9) * kin.v[1]) / T(2) + (T(-9) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2);
c[3] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(8) + T(9)) * kin.v[0] + T(9) * kin.v[1] + T(-9) * kin.v[2] + T(-9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(8) * kin.v[1] + T(-8) * kin.v[2] + T(-8) * kin.v[3])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-9) * kin.v[1]) / T(2) + (T(9) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2) + (T(-9) / T(2) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[1] + T(8) * kin.v[2] + T(8) * kin.v[3])) + rlog(kin.v[2]) * ((T(-9) + bc<T>[0] * int_to_imaginary<T>(-8)) * kin.v[0] + T(-9) * kin.v[1] + T(9) * kin.v[2] + T(9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-8) * kin.v[1] + T(8) * kin.v[2] + T(8) * kin.v[3]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[8] * (abb[10] * ((abb[7] * T(9)) / T(2) + (abb[11] * T(9)) / T(2) + abb[1] * T(-9)) + abb[4] * (abb[7] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[1] * bc<T>[0] * int_to_imaginary<T>(8) + abb[4] * ((abb[1] * T(-23)) / T(2) + (abb[11] * T(-9)) / T(4) + (abb[7] * T(31)) / T(4) + abb[9] * T(6))) + abb[2] * abb[4] * (abb[7] * T(-8) + abb[1] * T(8)) + abb[3] * (abb[4] * abb[12] * T(8) + abb[5] * (abb[7] * T(-8) + abb[12] * T(-8) + abb[1] * T(8)) + abb[4] * (abb[1] * T(-8) + abb[7] * T(8))) + abb[12] * (abb[2] * abb[4] * T(-8) + abb[4] * ((abb[4] * T(23)) / T(2) + bc<T>[0] * int_to_imaginary<T>(-8)) + abb[10] * T(9)) + abb[5] * (abb[5] * ((abb[7] * T(-13)) / T(4) + (abb[12] * T(-5)) / T(2) + (abb[1] * T(5)) / T(2) + (abb[11] * T(27)) / T(4) + abb[9] * T(-6)) + abb[1] * bc<T>[0] * int_to_imaginary<T>(-8) + abb[7] * bc<T>[0] * int_to_imaginary<T>(8) + abb[12] * (abb[4] * T(-9) + bc<T>[0] * int_to_imaginary<T>(8) + abb[2] * T(8)) + abb[2] * (abb[1] * T(-8) + abb[7] * T(8)) + abb[4] * ((abb[7] * T(-9)) / T(2) + (abb[11] * T(-9)) / T(2) + abb[1] * T(9))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_152_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl21 = DLog_W_21<T>(kin),dl3 = DLog_W_3<T>(kin),dl16 = DLog_W_16<T>(kin),dl20 = DLog_W_20<T>(kin),dl19 = DLog_W_19<T>(kin),dl8 = DLog_W_8<T>(kin),spdl22 = SpDLog_f_4_152_W_22<T>(kin),spdl21 = SpDLog_f_4_152_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,21> abbr = 
            {dl22(t), rlog(kin.W[15] / kin_path.W[15]), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_14(kin_path), rlog(kin.W[19] / kin_path.W[19]), dl21(t), rlog(kin.W[18] / kin_path.W[18]), f_2_1_15(kin_path), -rlog(t), rlog(kin.W[2] / kin_path.W[2]), dl3(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), rlog(kin.W[7] / kin_path.W[7]), dl16(t), dl20(t), dl19(t), dl8(t)}
;

        auto result = f_4_152_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_152_abbreviated(const std::array<T,21>& abb)
{
using TR = typename T::value_type;
T z[85];
z[0] = abb[1];
z[1] = abb[2];
z[2] = abb[13];
z[3] = abb[18];
z[4] = abb[3];
z[5] = abb[17];
z[6] = abb[19];
z[7] = abb[4];
z[8] = abb[5];
z[9] = bc<TR>[0];
z[10] = abb[20];
z[11] = abb[6];
z[12] = abb[10];
z[13] = abb[14];
z[14] = abb[15];
z[15] = abb[7];
z[16] = abb[9];
z[17] = abb[11];
z[18] = abb[12];
z[19] = abb[16];
z[20] = bc<TR>[2];
z[21] = bc<TR>[9];
z[22] = int_to_imaginary<T>(1) * z[9];
z[23] = -z[4] + z[22];
z[24] = z[6] * z[23];
z[25] = z[2] * z[23];
z[26] = z[24] + z[25];
z[27] = T(5) * z[26];
z[28] = T(3) * z[10];
z[29] = z[23] * z[28];
z[30] = z[2] + z[5];
z[31] = T(2) * z[30];
z[32] = z[6] + -z[28] + z[31];
z[32] = z[7] * z[32];
z[33] = T(2) * z[3];
z[34] = -z[6] + z[33];
z[35] = -z[30] + z[34];
z[35] = z[1] * z[35];
z[36] = z[5] * z[23];
z[37] = T(7) * z[36];
z[38] = z[3] * z[23];
z[32] = -z[27] + -z[29] + z[32] + z[35] + z[37] + z[38];
z[35] = T(5) * z[5];
z[39] = -z[2] + z[35];
z[40] = z[3] + z[39];
z[40] = z[8] * z[40];
z[32] = T(2) * z[32] + z[40];
z[32] = z[8] * z[32];
z[40] = T(2) * z[6];
z[41] = z[23] * z[40];
z[42] = -z[38] + z[41];
z[43] = z[25] + z[36];
z[44] = T(2) * z[43];
z[45] = T(4) * z[2] + -z[28];
z[46] = T(4) * z[6];
z[47] = -z[5] + z[45] + z[46];
z[47] = z[7] * z[47];
z[48] = (T(3) * z[5]) / T(2);
z[49] = -z[2] + -z[3] / T(2) + -z[6] + z[48];
z[49] = z[1] * z[49];
z[29] = -z[29] + z[42] + z[44] + z[47] + z[49];
z[29] = z[1] * z[29];
z[47] = prod_pow(z[4], 2);
z[49] = prod_pow(z[9], 2);
z[50] = T(13) * z[47] + (T(-17) * z[49]) / T(6);
z[51] = T(12) * z[12];
z[52] = z[4] * z[22];
z[53] = T(2) * z[52];
z[54] = z[14] * z[22];
z[50] = z[50] / T(2) + z[51] + z[53] + T(-15) * z[54];
z[50] = z[5] * z[50];
z[55] = T(2) * z[49];
z[56] = T(3) * z[54];
z[57] = (T(-5) * z[47]) / T(2) + z[53] + z[55] + z[56];
z[57] = z[28] * z[57];
z[58] = T(2) * z[12];
z[59] = -z[47] + (T(5) * z[49]) / T(6) + z[53] + -z[58];
z[59] = z[3] * z[59];
z[60] = z[3] + -z[10];
z[61] = T(7) * z[2];
z[60] = T(-6) * z[6] + (T(-15) * z[60]) / T(2) + -z[61];
z[60] = z[7] * z[60];
z[62] = T(2) * z[25];
z[42] = -z[36] + z[42] + z[60] + z[62];
z[42] = z[7] * z[42];
z[60] = T(2) * z[2];
z[63] = -z[28] + z[35] + -z[40] + -z[60];
z[64] = T(3) * z[13];
z[63] = z[63] * z[64];
z[65] = T(6) * z[14];
z[65] = z[22] * z[65];
z[66] = T(2) * z[47];
z[67] = T(10) * z[52] + -z[65] + -z[66];
z[68] = (T(23) * z[49]) / T(4) + z[67];
z[69] = -(z[2] * z[68]);
z[67] = T(-10) * z[12] + (T(-65) * z[49]) / T(12) + -z[67];
z[67] = z[6] * z[67];
z[70] = z[3] + -z[5];
z[70] = z[11] * z[70];
z[71] = z[2] * z[12];
z[29] = z[29] + z[32] + z[42] + z[50] + z[57] + z[59] + z[63] + z[67] + z[69] + T(-5) * z[70] + T(-12) * z[71];
z[29] = z[16] * z[29];
z[32] = z[47] + (T(3) * z[49]) / T(4) + z[53] + T(-4) * z[54];
z[32] = T(14) * z[12] + T(3) * z[32];
z[32] = z[5] * z[32];
z[42] = z[24] + -z[36];
z[50] = T(4) * z[38];
z[57] = z[42] + z[50] + z[62];
z[28] = (T(-25) * z[3]) / T(2) + T(-7) * z[6] + z[28] + z[35] + -z[61];
z[28] = z[7] * z[28];
z[28] = z[28] + T(2) * z[57];
z[28] = z[7] * z[28];
z[57] = z[24] + -z[38];
z[23] = z[10] * z[23];
z[59] = -z[23] + z[43];
z[45] = T(5) * z[3] + z[40] + z[45];
z[45] = z[7] * z[45];
z[45] = z[45] + z[57] + T(3) * z[59];
z[59] = T(3) * z[2];
z[62] = -z[5] + z[59];
z[63] = -z[6] + -z[62];
z[63] = z[1] * z[63];
z[45] = T(2) * z[45] + z[63];
z[45] = z[1] * z[45];
z[27] = -z[27] + T(4) * z[36] + T(-3) * z[38];
z[63] = T(2) * z[5];
z[67] = z[59] + -z[63];
z[67] = T(9) * z[3] + z[46] + T(2) * z[67];
z[67] = z[7] * z[67];
z[33] = z[6] + z[33];
z[69] = z[30] + z[33];
z[72] = T(2) * z[1];
z[73] = -(z[69] * z[72]);
z[74] = T(3) * z[5];
z[75] = -z[2] + z[74];
z[76] = (T(-5) * z[3]) / T(2) + -z[6] + T(2) * z[75];
z[76] = z[8] * z[76];
z[27] = T(2) * z[27] + z[67] + z[73] + z[76];
z[27] = z[8] * z[27];
z[67] = T(6) * z[52];
z[73] = T(3) * z[47];
z[76] = (T(-25) * z[49]) / T(4) + -z[67] + z[73];
z[76] = z[2] * z[76];
z[66] = (T(3) * z[49]) / T(2) + T(4) * z[52] + -z[66];
z[77] = T(11) * z[12];
z[78] = z[66] + -z[77];
z[78] = z[3] * z[78];
z[68] = -z[51] + -z[68];
z[68] = z[6] * z[68];
z[79] = -z[65] + z[73];
z[80] = -z[55] + z[79];
z[81] = -(z[10] * z[80]);
z[82] = z[6] + z[10];
z[83] = z[63] + -z[82];
z[84] = T(6) * z[13];
z[83] = z[83] * z[84];
z[27] = z[27] + z[28] + z[32] + z[45] + z[68] + T(-8) * z[70] + T(-14) * z[71] + z[76] + z[78] + z[81] + z[83];
z[27] = z[18] * z[27];
z[28] = T(4) * z[5];
z[32] = T(-13) * z[2] + T(-8) * z[3] + T(6) * z[10] + -z[28] + -z[40];
z[32] = z[7] * z[32];
z[34] = -z[5] + z[34] + z[61];
z[34] = z[34] * z[72];
z[45] = T(5) * z[6];
z[68] = -z[2] + T(7) * z[3] + T(-9) * z[5];
z[68] = z[45] + z[68] / T(2);
z[68] = z[8] * z[68];
z[70] = T(8) * z[25];
z[76] = T(7) * z[38];
z[32] = T(6) * z[23] + T(8) * z[24] + z[32] + z[34] + T(-13) * z[36] + z[68] + z[70] + z[76];
z[32] = z[8] * z[32];
z[34] = T(4) * z[47];
z[68] = -z[34] + (T(11) * z[49]) / T(6) + z[53] + z[65] + -z[77];
z[77] = -(z[3] * z[68]);
z[78] = T(3) * z[3];
z[62] = -z[40] + -z[62] + -z[78];
z[62] = z[7] * z[62];
z[43] = z[38] + z[43];
z[81] = -z[2] + z[3];
z[81] = z[72] * z[81];
z[41] = -z[41] + T(3) * z[43] + z[62] + z[81];
z[41] = z[41] * z[72];
z[43] = T(9) * z[47] + T(-8) * z[49] + T(-12) * z[52] + -z[65];
z[43] = z[10] * z[43];
z[62] = T(-5) * z[47] + (T(61) * z[49]) / T(6);
z[52] = T(8) * z[52];
z[51] = z[51] + z[52] + -z[56] + z[62] / T(2);
z[51] = z[6] * z[51];
z[62] = T(-4) * z[25] + -z[36];
z[72] = (T(17) * z[3]) / T(2);
z[28] = (T(25) * z[2]) / T(2) + (T(15) * z[6]) / T(2) + T(-9) * z[10] + z[28] + z[72];
z[28] = z[7] * z[28];
z[28] = -z[24] + z[28] + T(2) * z[62] + -z[76];
z[28] = z[7] * z[28];
z[62] = (T(13) * z[49]) / T(6) + -z[73];
z[62] = T(-7) * z[12] + T(9) * z[54] + (T(5) * z[62]) / T(2) + z[67];
z[62] = z[5] * z[62];
z[33] = T(2) * z[10] + z[33] + z[60] + -z[74];
z[33] = z[33] * z[64];
z[60] = -z[65] + z[67];
z[81] = (T(19) * z[49]) / T(12) + z[60];
z[81] = z[2] * z[81];
z[83] = T(4) * z[3];
z[75] = z[6] + z[75] + z[83];
z[75] = z[11] * z[75];
z[28] = z[28] + z[32] + z[33] + z[41] + z[43] + z[51] + z[62] + T(7) * z[71] + T(2) * z[75] + z[77] + z[81];
z[28] = z[0] * z[28];
z[32] = (T(11) * z[3]) / T(2) + z[59];
z[32] = T(3) * z[32] + -z[46];
z[32] = z[7] * z[32];
z[33] = z[1] * z[69];
z[41] = T(5) * z[2];
z[43] = T(7) * z[5] + -z[41] + -z[72];
z[43] = z[43] / T(2) + z[46];
z[43] = z[8] * z[43];
z[26] = T(-6) * z[26] + z[32] + T(-4) * z[33] + z[37] + T(-9) * z[38] + z[43];
z[26] = z[8] * z[26];
z[32] = -(z[5] * z[68]);
z[33] = z[6] / T(2);
z[37] = T(-8) * z[2] + (T(-85) * z[3]) / T(4) + z[33] + z[48];
z[37] = z[7] * z[37];
z[37] = z[37] + T(16) * z[38] + -z[42] + z[70];
z[37] = z[7] * z[37];
z[33] = (T(-3) * z[2]) / T(2) + z[33] + z[35] + -z[78];
z[33] = z[1] * z[33];
z[24] = -z[24] + z[44];
z[35] = T(17) * z[3] + z[61];
z[35] = z[7] * z[35];
z[33] = -z[24] + z[33] + z[35] + -z[76];
z[33] = z[1] * z[33];
z[35] = z[2] * z[66];
z[34] = (T(-35) * z[12]) / T(2) + -z[34] + (T(13) * z[49]) / T(4) + z[52];
z[34] = z[3] * z[34];
z[38] = (T(-17) * z[49]) / T(12) + z[58] + -z[60];
z[38] = z[6] * z[38];
z[41] = T(-16) * z[3] + -z[41] + z[45] + z[63];
z[41] = z[11] * z[41];
z[43] = -z[5] + z[6];
z[43] = -(z[43] * z[84]);
z[26] = z[26] + z[32] + z[33] + z[34] + z[35] + z[37] + z[38] + z[41] + z[43] + T(-11) * z[71];
z[26] = z[15] * z[26];
z[32] = -z[47] + (T(9) * z[49]) / T(4) + z[52] + -z[65];
z[33] = (T(17) * z[12]) / T(2) + -z[32];
z[33] = z[3] * z[33];
z[32] = T(-4) * z[12] + z[32];
z[32] = z[6] * z[32];
z[34] = -z[25] + z[36] + -z[50];
z[35] = -z[2] + z[5];
z[36] = T(4) * z[35];
z[37] = (T(63) * z[3]) / T(4) + -z[36] + -z[40];
z[37] = z[7] * z[37];
z[34] = T(2) * z[34] + z[37];
z[34] = z[7] * z[34];
z[37] = z[35] + -z[83];
z[37] = z[7] * z[37];
z[37] = z[37] + -z[57];
z[38] = -z[3] + z[6];
z[40] = T(-5) * z[35] + -z[38];
z[40] = z[1] * z[40];
z[37] = T(2) * z[37] + z[40];
z[37] = z[1] * z[37];
z[40] = z[6] + z[35];
z[40] = z[1] * z[40];
z[25] = z[25] + z[40] + z[42];
z[40] = z[46] + -z[72];
z[40] = z[7] * z[40];
z[36] = -z[3] / T(4) + T(-9) * z[6] + -z[36];
z[36] = z[8] * z[36];
z[25] = T(8) * z[25] + z[36] + z[40];
z[25] = z[8] * z[25];
z[36] = (T(2) * z[49]) / T(3) + z[79];
z[40] = z[2] * z[36];
z[36] = T(-16) * z[12] + -z[36];
z[36] = z[5] * z[36];
z[41] = -z[35] + -z[38];
z[41] = z[11] * z[41];
z[38] = -z[35] + z[38];
z[42] = z[38] * z[84];
z[25] = z[25] + z[32] + z[33] + z[34] + z[36] + z[37] + z[40] + T(10) * z[41] + z[42] + T(16) * z[71];
z[25] = z[17] * z[25];
z[32] = z[1] * z[38];
z[33] = -z[10] + z[30];
z[34] = -z[3] + -z[33];
z[34] = z[7] * z[34];
z[23] = z[23] + -z[24] + z[32] / T(2) + z[34];
z[23] = z[1] * z[23];
z[24] = z[3] + z[6] + -z[30];
z[24] = z[11] * z[24];
z[23] = z[23] + z[24];
z[24] = (T(-3) * z[47]) / T(2) + z[55] + -z[56] + z[67];
z[24] = z[10] * z[24];
z[30] = -z[49] / T(2) + -z[53] + T(2) * z[54];
z[30] = z[30] * z[74];
z[32] = -(z[2] * z[80]);
z[34] = -z[49] + z[73];
z[34] = z[34] / T(2) + -z[56];
z[34] = z[6] * z[34];
z[33] = -z[33] + z[78];
z[33] = prod_pow(z[7], 2) * z[33];
z[31] = -z[31] + z[82];
z[31] = z[31] * z[64];
z[23] = T(3) * z[23] + z[24] + z[30] + z[31] + z[32] + (T(3) * z[33]) / T(2) + z[34];
z[23] = z[19] * z[23];
z[24] = (T(7) * z[6]) / T(2) + (T(3) * z[10]) / T(2) + -z[39];
z[24] = z[24] * z[49];
z[30] = (T(3) * z[6]) / T(2) + -z[10] / T(2) + -z[35];
z[22] = z[20] * z[22] * z[30];
z[22] = z[22] + z[24] / T(2);
z[22] = z[20] * z[22];
z[24] = (T(15) * z[2]) / T(4) + -z[3] / T(6) + -z[5] + (T(29) * z[6]) / T(12) + (T(-5) * z[10]) / T(4);
z[24] = int_to_imaginary<T>(1) * prod_pow(z[9], 3) * z[24];
z[30] = T(21) * z[2] + T(-19) * z[5];
z[30] = (T(-21) * z[10]) / T(2) + T(5) * z[30];
z[30] = (T(32) * z[3]) / T(3) + (T(377) * z[6]) / T(24) + z[30] / T(4);
z[30] = z[21] * z[30];
return T(3) * z[22] + z[23] + z[24] + z[25] + z[26] + z[27] + z[28] + z[29] + z[30];
}



template IntegrandConstructorType<double> f_4_152_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_152_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_152_construct (const Kin<qd_real>&);
#endif

}