#include "f_4_47.h"

namespace PentagonFunctions {

template <typename T> T f_4_47_abbreviated (const std::array<T,21>&);

template <typename T> class SpDLog_f_4_47_W_7 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_47_W_7 (const Kin<T>& kin) {
        c[0] = T(-2) * kin.v[1] * kin.v[4] + ((T(3) * kin.v[4]) / T(2) + T(-2)) * kin.v[4] + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-2) + T(-2) * kin.v[1] + T(3) * kin.v[4]);
c[1] = (T(-2) + T(-2) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(-4) * kin.v[1])) * kin.v[3] + T(-2) * kin.v[4] + T(-2) * kin.v[1] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[4] + T(-4) * kin.v[1] * kin.v[4]) + rlog(-kin.v[4]) * (((T(-7) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[4] + T(2)) * kin.v[4] + T(2) * kin.v[1] * kin.v[4]) + kin.v[3] * ((T(-7) * kin.v[4]) / T(2) + T(1) + kin.v[1] + (T(-7) / T(4) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(2) * kin.v[1] + T(-2) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (((T(-7) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[4] + T(2)) * kin.v[4] + T(2) * kin.v[1] * kin.v[4]) + kin.v[3] * ((T(-7) * kin.v[4]) / T(2) + T(1) + kin.v[1] + (T(-7) / T(4) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(2) * kin.v[1] + T(-2) * kin.v[4]))) + rlog(kin.v[3]) * (-(kin.v[1] * kin.v[4]) + ((T(7) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-2) + kin.v[4])) + kin.v[3] * ((T(7) * kin.v[4]) / T(2) + -kin.v[1] + T(-1) + (T(7) / T(4) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(-2) * kin.v[1] + T(2) * kin.v[4]))) + rlog(-kin.v[1]) * (((T(33) * kin.v[4]) / T(4) + T(-7)) * kin.v[4] + T(-7) * kin.v[1] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-6) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-6) + T(3) * kin.v[4])) + kin.v[3] * ((T(33) * kin.v[4]) / T(2) + T(-7) + T(-7) * kin.v[1] + (T(33) / T(4) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-6) + T(-6) * kin.v[1] + T(6) * kin.v[4])));
c[2] = T(2) * kin.v[3] + T(2) * kin.v[4];
c[3] = rlog(kin.v[3]) * ((T(-3) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[4]) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(3)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[4] + T(3) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(3)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[4] + T(3) * kin.v[4]) + rlog(-kin.v[1]) * ((T(-5) + bc<T>[0] * int_to_imaginary<T>(-6)) * kin.v[3] + T(-5) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[9] * (abb[10] * (abb[10] * (abb[6] + abb[8] + -abb[7] + abb[12] * T(-2)) + abb[7] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[6] * bc<T>[0] * int_to_imaginary<T>(2) + abb[8] * bc<T>[0] * int_to_imaginary<T>(2)) + abb[3] * abb[10] * (abb[6] * T(-2) + abb[8] * T(-2) + abb[7] * T(2)) + abb[10] * abb[13] * (abb[7] * T(-2) + abb[6] * T(2) + abb[8] * T(2)) + abb[11] * (abb[6] * T(-3) + abb[8] * T(-3) + abb[12] * T(2) + abb[7] * T(3)) + abb[1] * (abb[10] * abb[13] * T(-6) + abb[10] * (abb[10] + bc<T>[0] * int_to_imaginary<T>(-6)) + abb[11] * T(5) + abb[3] * abb[10] * T(6)) + abb[2] * (abb[6] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[7] * bc<T>[0] * int_to_imaginary<T>(2) + abb[13] * (abb[6] * T(-2) + abb[8] * T(-2) + abb[7] * T(2)) + abb[3] * (abb[7] * T(-2) + abb[6] * T(2) + abb[8] * T(2)) + abb[2] * (abb[7] + -abb[1] + -abb[6] + -abb[8] + abb[12] * T(2)) + abb[1] * (abb[3] * T(-6) + bc<T>[0] * int_to_imaginary<T>(6) + abb[13] * T(6))));
    }
};
template <typename T> class SpDLog_f_4_47_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_47_W_12 (const Kin<T>& kin) {
        c[0] = (T(-3) / T(2) + (T(-3) * kin.v[1]) / T(8) + (T(-3) * kin.v[4]) / T(4)) * kin.v[1] + (T(3) / T(2) + (T(9) * kin.v[4]) / T(8)) * kin.v[4];
c[1] = (T(3) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4] + rlog(kin.v[3]) * (((T(5) * kin.v[4]) / T(2) + T(2)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(2) + kin.v[4]) + kin.v[1] * (T(-2) + bc<T>[0] * int_to_imaginary<T>(-2) + (T(1) / T(2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-3) * kin.v[4])) + rlog(-kin.v[4]) * (((T(-5) * kin.v[4]) / T(2) + T(-2)) * kin.v[4] + bc<T>[0] * (-kin.v[4] + T(-2)) * int_to_imaginary<T>(1) * kin.v[4] + kin.v[1] * (bc<T>[0] * int_to_imaginary<T>(2) + T(2) + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[1] + T(3) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (((T(-5) * kin.v[4]) / T(2) + T(-2)) * kin.v[4] + bc<T>[0] * (-kin.v[4] + T(-2)) * int_to_imaginary<T>(1) * kin.v[4] + kin.v[1] * (bc<T>[0] * int_to_imaginary<T>(2) + T(2) + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[1] + T(3) * kin.v[4])) + kin.v[1] * (T(-3) / T(2) + (T(3) * kin.v[4]) / T(2) + (T(-3) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(4) * kin.v[4])) + rlog(-kin.v[1]) * (kin.v[1] * (T(-17) / T(2) + (T(-41) * kin.v[4]) / T(4) + bc<T>[0] * int_to_imaginary<T>(-6) + (T(7) / T(8) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1]) + (T(17) / T(2) + (T(75) * kin.v[4]) / T(8)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] * (T(6) + T(3) * kin.v[4]));
c[2] = (T(3) * kin.v[1]) / T(2) + (T(-3) * kin.v[4]) / T(2);
c[3] = rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(2)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(2)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[4]) + rlog(kin.v[3]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[4] + T(2) * kin.v[4]) + rlog(-kin.v[1]) * ((T(7) * kin.v[4]) / T(2) + (T(-7) / T(2) + bc<T>[0] * int_to_imaginary<T>(-6)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[14] * (abb[3] * abb[13] * (abb[6] * T(-2) + abb[8] * T(-2) + abb[7] * T(2)) + abb[15] * ((abb[16] * T(3)) / T(2) + abb[6] * T(-2) + abb[8] * T(-2) + abb[7] * T(2)) + abb[13] * ((abb[13] * abb[16] * T(-3)) / T(2) + abb[7] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[6] * bc<T>[0] * int_to_imaginary<T>(2) + abb[8] * bc<T>[0] * int_to_imaginary<T>(2) + abb[10] * (abb[7] * T(-2) + abb[6] * T(2) + abb[8] * T(2))) + abb[1] * ((abb[15] * T(7)) / T(2) + abb[13] * ((abb[13] * T(5)) / T(2) + abb[10] * T(-6) + bc<T>[0] * int_to_imaginary<T>(-6)) + abb[3] * abb[13] * T(6)) + abb[2] * (abb[2] * ((abb[1] * T(-5)) / T(2) + (abb[16] * T(3)) / T(2)) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[7] * bc<T>[0] * int_to_imaginary<T>(2) + abb[10] * (abb[6] * T(-2) + abb[8] * T(-2) + abb[7] * T(2)) + abb[3] * (abb[7] * T(-2) + abb[6] * T(2) + abb[8] * T(2)) + abb[1] * (abb[3] * T(-6) + bc<T>[0] * int_to_imaginary<T>(6) + abb[10] * T(6))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_47_construct (const Kin<T>& kin) {
    return [&kin, 
            dl14 = DLog_W_14<T>(kin),dl7 = DLog_W_7<T>(kin),dl12 = DLog_W_12<T>(kin),dl2 = DLog_W_2<T>(kin),dl4 = DLog_W_4<T>(kin),dl19 = DLog_W_19<T>(kin),dl5 = DLog_W_5<T>(kin),spdl7 = SpDLog_f_4_47_W_7<T>(kin),spdl12 = SpDLog_f_4_47_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,21> abbr = 
            {dl14(t), rlog(kin.W[1] / kin_path.W[1]), rlog(-v_path[1]), rlog(v_path[3]), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[18] / kin_path.W[18]), dl7(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), -rlog(t), rlog(-v_path[4]), dl12(t), f_2_1_4(kin_path), -rlog(t), dl2(t), dl4(t), dl19(t), dl5(t)}
;

        auto result = f_4_47_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_47_abbreviated(const std::array<T,21>& abb)
{
using TR = typename T::value_type;
T z[64];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[17];
z[11] = abb[18];
z[12] = abb[19];
z[13] = abb[20];
z[14] = abb[10];
z[15] = abb[13];
z[16] = abb[11];
z[17] = abb[15];
z[18] = abb[12];
z[19] = abb[16];
z[20] = bc<TR>[2];
z[21] = bc<TR>[9];
z[22] = T(2) * z[8];
z[23] = T(6) * z[18] + -z[22];
z[24] = T(3) * z[7];
z[25] = -z[1] + z[23] + -z[24];
z[25] = z[11] * z[25];
z[26] = z[1] + z[7];
z[23] = -z[23] + z[26];
z[23] = z[13] * z[23];
z[27] = T(2) * z[11];
z[28] = T(2) * z[13];
z[29] = z[0] + -z[27] + -z[28];
z[29] = z[9] * z[29];
z[30] = T(2) * z[19];
z[31] = -z[11] + z[12];
z[32] = z[13] + T(-3) * z[31];
z[32] = z[30] * z[32];
z[24] = (T(-3) * z[1]) / T(2) + T(4) * z[9] + (T(-9) * z[19]) / T(2) + -z[22] + z[24];
z[24] = z[10] * z[24];
z[26] = -z[8] + z[26];
z[26] = z[0] * z[26];
z[33] = T(2) * z[18];
z[34] = T(5) * z[1] + -z[7] + T(4) * z[8] + -z[33];
z[34] = z[12] * z[34];
z[23] = z[23] + z[24] + z[25] + -z[26] + z[29] + z[32] + z[34];
z[23] = z[2] * z[23];
z[24] = T(6) * z[4];
z[25] = T(2) * z[15];
z[29] = int_to_imaginary<T>(1) * z[3];
z[32] = z[24] + z[25] + T(-5) * z[29];
z[32] = z[8] * z[32];
z[34] = T(2) * z[29];
z[35] = -z[4] + z[34];
z[36] = -z[15] + z[35];
z[36] = z[33] * z[36];
z[37] = T(2) * z[14];
z[38] = -z[29] + z[37];
z[24] = -z[24] + -z[38];
z[24] = z[9] * z[24];
z[39] = z[4] + z[14];
z[40] = z[34] + z[39];
z[40] = z[30] * z[40];
z[41] = T(-5) * z[4] + -z[14] + z[34];
z[41] = z[7] * z[41];
z[42] = T(11) * z[4] + -z[14] + T(-10) * z[29];
z[42] = z[1] * z[42];
z[24] = z[24] + z[32] + z[36] + z[40] + z[41] + z[42];
z[24] = z[10] * z[24];
z[32] = T(3) * z[15];
z[36] = -z[4] + -z[32] + -z[37];
z[36] = z[22] * z[36];
z[40] = T(2) * z[4];
z[41] = z[38] + z[40];
z[41] = z[7] * z[41];
z[42] = z[29] + z[37];
z[43] = z[40] + z[42];
z[44] = T(-8) * z[15] + -z[43];
z[44] = z[1] * z[44];
z[32] = -z[4] + z[32];
z[32] = z[32] * z[33];
z[32] = z[32] + z[36] + z[41] + z[44];
z[32] = z[12] * z[32];
z[36] = T(4) * z[14] + z[25] + -z[29] + z[40];
z[41] = -(z[18] * z[36]);
z[44] = z[14] + z[15];
z[45] = -z[29] + z[44];
z[45] = z[8] * z[45];
z[42] = z[4] + z[15] + z[42];
z[42] = z[7] * z[42];
z[46] = -z[4] + z[29];
z[47] = z[15] + T(-3) * z[46];
z[47] = z[1] * z[47];
z[41] = z[41] + z[42] + z[45] + z[47];
z[41] = z[27] * z[41];
z[36] = z[33] * z[36];
z[38] = -z[25] + -z[38];
z[38] = z[22] * z[38];
z[42] = -(z[7] * z[39]);
z[45] = T(3) * z[4];
z[47] = T(-5) * z[14] + z[45];
z[47] = z[1] * z[47];
z[36] = z[36] + z[38] + z[42] + z[47];
z[36] = z[13] * z[36];
z[38] = z[34] + z[44];
z[27] = z[27] * z[38];
z[38] = z[28] * z[39];
z[39] = z[0] * z[29];
z[34] = -(z[12] * z[34]);
z[27] = z[27] + z[34] + z[38] + -z[39];
z[27] = z[9] * z[27];
z[34] = z[26] * z[29];
z[38] = T(4) * z[15];
z[39] = z[38] + z[43];
z[39] = z[31] * z[39];
z[42] = z[14] + -z[45];
z[42] = z[13] * z[42];
z[39] = z[39] + z[42];
z[30] = z[30] * z[39];
z[23] = z[23] + z[24] + z[27] + z[30] + z[32] + z[34] + z[36] + z[41];
z[23] = z[2] * z[23];
z[24] = z[14] + z[46];
z[27] = z[24] * z[25];
z[30] = T(2) * z[17];
z[27] = z[27] + -z[30];
z[32] = z[6] * z[29];
z[34] = z[5] + z[32];
z[36] = z[4] * z[35];
z[39] = prod_pow(z[3], 2);
z[41] = -z[14] + T(-4) * z[46];
z[41] = z[14] * z[41];
z[41] = T(3) * z[16] + -z[27] + T(5) * z[34] + -z[36] + (T(-5) * z[39]) / T(6) + z[41];
z[41] = z[9] * z[41];
z[42] = T(8) * z[17];
z[43] = z[36] + z[39] / T(2) + z[42];
z[44] = T(2) * z[46];
z[45] = z[14] + -z[15] + z[44];
z[45] = z[25] * z[45];
z[47] = T(5) * z[16];
z[44] = -z[14] + z[44];
z[48] = z[14] * z[44];
z[45] = z[45] + -z[47] + z[48];
z[49] = -z[34] + -z[43] + z[45];
z[49] = z[8] * z[49];
z[35] = z[35] * z[40];
z[35] = z[35] + (T(17) * z[39]) / T(12);
z[40] = -z[37] + T(5) * z[46];
z[40] = z[14] * z[40];
z[50] = (T(-5) * z[15]) / T(2) + T(6) * z[24];
z[50] = z[15] * z[50];
z[40] = T(-9) * z[16] + (T(-21) * z[17]) / T(2) + -z[34] + -z[35] + z[40] + z[50];
z[40] = z[1] * z[40];
z[50] = T(3) * z[17];
z[51] = -z[34] + z[50];
z[52] = z[36] + z[51];
z[53] = prod_pow(z[14], 2);
z[54] = z[39] / T(3);
z[55] = z[16] + z[54];
z[56] = -z[15] + z[46];
z[56] = -(z[15] * z[56]);
z[55] = z[52] + z[53] + T(2) * z[55] + z[56];
z[55] = z[33] * z[55];
z[56] = z[24] * z[37];
z[57] = T(6) * z[34];
z[58] = T(2) * z[16];
z[56] = z[35] + z[56] + -z[57] + z[58];
z[59] = prod_pow(z[15], 2);
z[59] = (T(7) * z[17]) / T(2) + z[56] + (T(3) * z[59]) / T(2);
z[59] = z[19] * z[59];
z[60] = z[14] * z[46];
z[61] = -z[16] + -z[32] + z[60];
z[62] = T(3) * z[5];
z[27] = -z[27] + T(-3) * z[61] + z[62];
z[27] = z[7] * z[27];
z[29] = z[20] * z[29];
z[29] = z[29] + (T(5) * z[39]) / T(2);
z[29] = z[20] * z[29];
z[61] = int_to_imaginary<T>(1) * prod_pow(z[3], 3);
z[63] = (T(-71) * z[21]) / T(2) + (T(-5) * z[61]) / T(3);
z[63] = -z[29] + z[63] / T(2);
z[27] = z[27] + z[40] + z[41] + z[49] + z[55] + z[59] + z[63] / T(2);
z[27] = z[10] * z[27];
z[32] = T(3) * z[32] + z[62];
z[40] = -z[14] + z[46];
z[40] = -z[15] + T(2) * z[40];
z[25] = z[25] * z[40];
z[40] = z[37] * z[46];
z[41] = -z[25] + z[32] + z[40] + z[43] + z[58];
z[41] = z[1] * z[41];
z[43] = -z[37] + z[46];
z[46] = -z[15] + z[43];
z[46] = z[15] * z[46];
z[49] = z[54] + -z[58];
z[50] = z[34] + -z[46] + -z[49] + z[50];
z[50] = z[22] * z[50];
z[36] = z[36] + (T(7) * z[39]) / T(6);
z[54] = -z[34] + z[36] + z[40] + -z[58];
z[54] = z[7] * z[54];
z[55] = (T(2) * z[39]) / T(3);
z[40] = -z[16] + -z[40] + z[46] + -z[52] + -z[55];
z[33] = z[33] * z[40];
z[40] = (T(3) * z[21]) / T(4) + z[29];
z[33] = z[33] + z[40] / T(2) + z[41] + z[50] + z[54];
z[33] = z[12] * z[33];
z[40] = z[37] * z[44];
z[38] = z[14] * z[38];
z[38] = T(6) * z[17] + z[38] + -z[39];
z[39] = T(2) * z[34];
z[40] = T(8) * z[16] + z[38] + z[39] + -z[40];
z[40] = z[18] * z[40];
z[41] = -(z[14] * z[15]);
z[41] = z[41] + z[49] + -z[51];
z[22] = z[22] * z[41];
z[41] = -z[42] + z[45] + z[55];
z[41] = z[1] * z[41];
z[44] = (T(-5) * z[21]) / T(4) + z[29] + -z[61] / T(3);
z[37] = z[15] * z[37];
z[37] = T(4) * z[34] + z[37];
z[45] = -z[16] + -z[37] + z[48];
z[45] = z[7] * z[45];
z[22] = z[22] + z[40] + z[41] + z[44] / T(2) + z[45];
z[22] = z[11] * z[22];
z[24] = -z[15] + T(4) * z[24];
z[24] = z[15] * z[24];
z[24] = z[24] + z[30];
z[30] = -(z[14] * z[43]);
z[30] = z[24] + z[30] + -z[32] + z[35] + z[47];
z[30] = z[1] * z[30];
z[32] = T(4) * z[16] + z[38] + -z[39] + T(2) * z[53];
z[32] = z[8] * z[32];
z[35] = (T(25) * z[21]) / T(2) + z[61];
z[29] = -z[29] + z[35] / T(2);
z[35] = -z[16] + -z[34] + z[60];
z[38] = -(z[7] * z[35]);
z[29] = z[29] / T(2) + z[30] + z[32] + z[38] + -z[40];
z[29] = z[13] * z[29];
z[30] = -z[37] + -z[49];
z[30] = z[11] * z[30];
z[32] = z[34] + z[36];
z[34] = z[0] * z[32];
z[36] = z[39] + z[49];
z[36] = z[12] * z[36];
z[28] = z[28] * z[35];
z[28] = z[28] + z[30] + z[34] + z[36];
z[28] = z[9] * z[28];
z[25] = -z[25] + z[42] + -z[49] + z[57];
z[25] = -(z[25] * z[31]);
z[24] = -z[24] + -z[56];
z[24] = z[13] * z[24];
z[24] = z[24] + z[25];
z[24] = z[19] * z[24];
z[25] = -(z[26] * z[32]);
return z[22] + z[23] + z[24] + z[25] + z[27] + z[28] + z[29] + z[33];
}



template IntegrandConstructorType<double> f_4_47_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_47_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_47_construct (const Kin<qd_real>&);
#endif

}