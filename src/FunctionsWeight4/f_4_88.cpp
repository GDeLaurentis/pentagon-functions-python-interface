#include "f_4_88.h"

namespace PentagonFunctions {

template <typename T> T f_4_88_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_88_W_23 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_88_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (T(2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(2) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + T(2) * prod_pow(kin.v[4], 2) + kin.v[0] * (T(2) * kin.v[0] + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((-kin.v[4] / T(2) + (T(-3) * kin.v[0]) / T(4) + kin.v[2] / T(2) + (T(3) * kin.v[3]) / T(2) + -kin.v[1] + T(1)) * kin.v[0] + (-kin.v[3] / T(2) + -kin.v[4] / T(2) + kin.v[2] / T(4) + T(-1)) * kin.v[2] + ((T(-3) * kin.v[3]) / T(4) + kin.v[4] / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (kin.v[4] / T(4) + T(1)) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((-kin.v[4] / T(2) + (T(-3) * kin.v[0]) / T(4) + kin.v[2] / T(2) + (T(3) * kin.v[3]) / T(2) + -kin.v[1] + T(1)) * kin.v[0] + (-kin.v[3] / T(2) + -kin.v[4] / T(2) + kin.v[2] / T(4) + T(-1)) * kin.v[2] + ((T(-3) * kin.v[3]) / T(4) + kin.v[4] / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (kin.v[4] / T(4) + T(1)) * kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[0] * (-kin.v[2] / T(2) + (T(3) * kin.v[0]) / T(4) + (T(-3) * kin.v[3]) / T(2) + kin.v[4] / T(2) + T(-1) + kin.v[1]) + (-kin.v[2] / T(4) + kin.v[3] / T(2) + kin.v[4] / T(2) + T(1)) * kin.v[2] + (-kin.v[4] / T(2) + (T(3) * kin.v[3]) / T(4) + T(1)) * kin.v[3] + (-kin.v[4] / T(4) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4])) + rlog(-kin.v[4]) * (kin.v[0] * (-kin.v[2] / T(2) + (T(3) * kin.v[0]) / T(4) + (T(-3) * kin.v[3]) / T(2) + kin.v[4] / T(2) + T(-1) + kin.v[1]) + (-kin.v[2] / T(4) + kin.v[3] / T(2) + kin.v[4] / T(2) + T(1)) * kin.v[2] + (-kin.v[4] / T(2) + (T(3) * kin.v[3]) / T(4) + T(1)) * kin.v[3] + (-kin.v[4] / T(4) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]));
c[1] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (-kin.v[0] + -kin.v[4] + kin.v[2] + kin.v[3]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (-kin.v[0] + -kin.v[4] + kin.v[2] + kin.v[3]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]) + rlog(-kin.v[4]) * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[4] * (abb[3] + -prod_pow(abb[2], 2) / T(2)) + prod_pow(abb[2], 2) * (abb[5] / T(2) + abb[7] / T(2) + -abb[6] / T(2)) + abb[1] * (abb[1] * ((abb[5] * T(-3)) / T(2) + (abb[7] * T(-3)) / T(2) + (abb[4] * T(3)) / T(2) + (abb[6] * T(3)) / T(2)) + -(abb[2] * abb[4]) + abb[2] * (abb[5] + abb[7] + -abb[6])) + abb[3] * (abb[6] + -abb[5] + -abb[7]));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_88_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl16 = DLog_W_16<T>(kin),dl24 = DLog_W_24<T>(kin),dl17 = DLog_W_17<T>(kin),dl5 = DLog_W_5<T>(kin),dl18 = DLog_W_18<T>(kin),spdl23 = SpDLog_f_4_88_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,17> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_8(kin_path), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[17] / kin_path.W[17]), dl16(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(kin.W[23] / kin_path.W[23]), dl24(t), dl17(t), dl5(t), dl18(t)}
;

        auto result = f_4_88_abbreviated(abbr);
        result = result + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_88_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[50];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[8];
z[3] = abb[14];
z[4] = abb[15];
z[5] = abb[16];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[12];
z[10] = abb[2];
z[11] = abb[13];
z[12] = abb[11];
z[13] = bc<TR>[0];
z[14] = abb[3];
z[15] = abb[9];
z[16] = abb[10];
z[17] = bc<TR>[2];
z[18] = bc<TR>[9];
z[19] = -z[10] + z[16];
z[20] = int_to_imaginary<T>(1) * z[13];
z[21] = T(2) * z[20];
z[22] = z[19] * z[21];
z[23] = T(2) * z[0];
z[24] = -z[10] + z[23];
z[24] = z[10] * z[24];
z[25] = T(2) * z[15];
z[22] = -z[22] + z[24] + -z[25];
z[26] = T(2) * z[14];
z[27] = z[22] + -z[26];
z[28] = -z[12] + z[21];
z[29] = T(2) * z[12];
z[30] = -(z[28] * z[29]);
z[31] = prod_pow(z[13], 2);
z[32] = (T(5) * z[31]) / T(3);
z[33] = prod_pow(z[0], 2);
z[30] = -z[27] + z[30] + -z[32] + -z[33];
z[30] = z[3] * z[30];
z[34] = -z[0] + z[10];
z[35] = T(4) * z[20] + -z[29] + z[34];
z[35] = z[12] * z[35];
z[36] = -(z[20] * z[34]);
z[37] = z[0] + z[10];
z[37] = z[10] * z[37];
z[35] = T(2) * z[31] + z[35] + z[36] + z[37];
z[35] = z[11] * z[35];
z[36] = -z[0] + z[16];
z[37] = z[20] * z[36];
z[38] = z[15] + z[37];
z[38] = -z[24] + T(4) * z[38];
z[39] = z[26] + z[33];
z[40] = z[20] + z[34];
z[40] = -z[12] + T(2) * z[40];
z[29] = -(z[29] * z[40]);
z[29] = z[29] + -z[32] + z[38] + -z[39];
z[29] = z[2] * z[29];
z[36] = z[21] * z[36];
z[41] = z[12] * z[34];
z[42] = T(2) * z[41];
z[25] = z[25] + z[36] + -z[42];
z[36] = prod_pow(z[10], 2);
z[43] = z[25] + z[36] + -z[39];
z[43] = z[4] * z[43];
z[29] = z[29] + z[30] + T(2) * z[35] + -z[43];
z[29] = z[9] * z[29];
z[30] = (T(2) * z[31]) / T(3);
z[26] = -z[26] + z[30] + z[33] + z[38] + T(-4) * z[41];
z[26] = z[9] * z[26];
z[35] = -z[0] + z[10] / T(2);
z[35] = z[10] * z[35];
z[38] = z[33] / T(2);
z[35] = z[35] + z[38];
z[21] = -(z[21] * z[34]);
z[21] = T(3) * z[14] + z[21] + z[31] / T(3) + z[35] + z[42];
z[21] = z[7] * z[21];
z[25] = z[14] + -z[25] + -z[35];
z[25] = z[1] * z[25];
z[22] = z[22] + -z[39];
z[35] = z[22] + -z[31];
z[39] = z[6] * z[35];
z[42] = prod_pow(z[17], 2);
z[44] = T(7) * z[42];
z[45] = z[31] + z[44];
z[45] = z[20] * z[45];
z[46] = z[17] * z[31];
z[47] = (T(3) * z[46]) / T(2);
z[45] = z[45] + z[47];
z[21] = (T(21) * z[18]) / T(8) + z[21] + z[25] + z[26] + z[39] + z[45] / T(2);
z[21] = z[5] * z[21];
z[25] = T(3) * z[10];
z[26] = -z[23] + z[25];
z[39] = -(z[10] * z[26]);
z[45] = T(-5) * z[16] + z[23] + z[25];
z[45] = z[20] * z[45];
z[48] = -z[20] + z[34];
z[48] = z[12] + T(2) * z[48];
z[48] = z[12] * z[48];
z[49] = (T(11) * z[31]) / T(6);
z[39] = T(-5) * z[15] + z[39] + z[45] + z[48] + -z[49];
z[39] = z[1] * z[39];
z[19] = -(z[19] * z[20]);
z[28] = -(z[12] * z[28]);
z[19] = -z[15] + z[19] + z[28] + (T(-7) * z[31]) / T(6) + -z[36];
z[19] = z[7] * z[19];
z[28] = int_to_imaginary<T>(1) * prod_pow(z[13], 3);
z[19] = z[19] + (T(-7) * z[28]) / T(6) + z[39] + T(4) * z[46];
z[19] = z[11] * z[19];
z[28] = z[12] * z[40];
z[26] = z[16] + -z[26];
z[26] = z[20] * z[26];
z[26] = T(4) * z[14] + z[15] + -z[24] + z[26] + z[28] + T(2) * z[33] + z[49];
z[26] = z[1] * z[26];
z[23] = z[10] + T(-3) * z[16] + z[23];
z[23] = z[20] * z[23];
z[39] = (T(5) * z[31]) / T(6);
z[23] = z[23] + z[28] + z[39];
z[24] = T(-3) * z[15] + z[23] + z[24];
z[24] = z[7] * z[24];
z[28] = z[32] + z[44];
z[28] = z[20] * z[28];
z[28] = z[28] + z[47];
z[24] = z[24] + z[26] + z[28] / T(2);
z[24] = z[2] * z[24];
z[26] = z[20] + T(2) * z[34];
z[26] = -z[12] + T(2) * z[26];
z[26] = z[12] * z[26];
z[28] = T(4) * z[0];
z[32] = T(7) * z[16] + -z[25] + -z[28];
z[32] = z[20] * z[32];
z[25] = -z[25] + z[28];
z[25] = z[10] * z[25];
z[28] = z[31] / T(6);
z[25] = T(7) * z[15] + -z[25] + -z[26] + z[28] + z[32];
z[25] = z[11] * z[25];
z[26] = -z[0] + (T(3) * z[10]) / T(2);
z[26] = z[10] * z[26];
z[31] = -z[15] + z[38];
z[23] = z[14] + z[23] + -z[26] + T(3) * z[31];
z[23] = z[3] * z[23];
z[23] = z[23] + z[25] + -z[43];
z[25] = z[27] + T(-3) * z[33] + -z[39];
z[25] = z[2] * z[25];
z[25] = z[23] + z[25];
z[25] = z[6] * z[25];
z[26] = z[2] * z[35];
z[22] = z[22] + -z[30];
z[22] = z[5] * z[22];
z[22] = z[22] + z[23] + z[26];
z[22] = z[8] * z[22];
z[23] = -z[14] + -z[31] + z[36] / T(2) + z[37] + -z[41];
z[26] = z[1] + T(3) * z[7];
z[26] = z[23] * z[26];
z[27] = z[28] + T(-5) * z[42];
z[27] = z[20] * z[27];
z[27] = z[27] + (T(-9) * z[46]) / T(2);
z[26] = z[26] + z[27] / T(2);
z[26] = z[4] * z[26];
z[27] = T(3) * z[1] + -z[7];
z[23] = z[23] * z[27];
z[27] = z[28] + -z[42];
z[20] = z[20] * z[27];
z[20] = z[20] + (T(-5) * z[46]) / T(2);
z[20] = z[20] / T(2) + z[23];
z[20] = z[3] * z[20];
z[23] = z[3] + T(5) * z[4];
z[23] = (T(-41) * z[2]) / T(24) + T(-7) * z[11] + z[23] / T(8);
z[23] = z[18] * z[23];
return z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[25] + z[26] + z[29];
}



template IntegrandConstructorType<double> f_4_88_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_88_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_88_construct (const Kin<qd_real>&);
#endif

}