#include "f_4_293.h"

namespace PentagonFunctions {

template <typename T> T f_4_293_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_293_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_293_W_12 (const Kin<T>& kin) {
        c[0] = (-kin.v[1] + T(-2)) * kin.v[1] + kin.v[4] * (T(2) + kin.v[4]);
c[1] = kin.v[1] * (T(2) + kin.v[1]) + (-kin.v[4] + T(-2)) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[4] * c[0] + abb[3] * c[1]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[4] + -(prod_pow(abb[2], 2) * abb[3]) + prod_pow(abb[1], 2) * (abb[3] + -abb[4]));
    }
};
template <typename T> class SpDLog_f_4_293_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_293_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) + kin.v[0] + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4]);
c[1] = kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) + kin.v[0] + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4]);
c[2] = kin.v[0] * (-kin.v[0] + T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4]);
c[3] = kin.v[0] * (-kin.v[0] + T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[8] * c[0] + abb[4] * c[1] + abb[9] * c[2] + abb[10] * c[3]);
        }

        return abb[5] * (prod_pow(abb[7], 2) * abb[8] + prod_pow(abb[6], 2) * (abb[9] + abb[10] + -abb[4] + -abb[8]) + prod_pow(abb[7], 2) * (abb[4] + -abb[9] + -abb[10]));
    }
};
template <typename T> class SpDLog_f_4_293_W_7 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_293_W_7 (const Kin<T>& kin) {
        c[0] = -(kin.v[0] * kin.v[4]) + T(3) * kin.v[1] * kin.v[4] + T(2) * kin.v[2] * kin.v[4] + ((T(-3) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4]) + kin.v[3] * (-kin.v[0] + T(4) + T(3) * kin.v[1] + T(2) * kin.v[2] + (T(-3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + T(-1) + kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4];
c[2] = -(kin.v[0] * kin.v[4]) + T(3) * kin.v[1] * kin.v[4] + T(2) * kin.v[2] * kin.v[4] + ((T(-3) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4]) + kin.v[3] * (-kin.v[0] + T(4) + T(3) * kin.v[1] + T(2) * kin.v[2] + (T(-3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + T(-1) + kin.v[4]));
c[3] = (bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4];
c[4] = -(kin.v[0] * kin.v[4]) + T(3) * kin.v[1] * kin.v[4] + T(2) * kin.v[2] * kin.v[4] + ((T(-3) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4]) + kin.v[3] * (-kin.v[0] + T(4) + T(3) * kin.v[1] + T(2) * kin.v[2] + (T(-3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + T(-1) + kin.v[4]));
c[5] = (bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[8] * (t * c[0] + c[1]) + abb[4] * (t * c[2] + c[3]) + abb[9] * (t * c[4] + c[5]);
        }

        return abb[23] * ((abb[6] * abb[8] + abb[6] * (abb[4] + abb[9])) * abb[12] + abb[2] * abb[6] * (-abb[4] + -abb[9]) + abb[24] * (abb[4] * T(-2) + abb[9] * T(-2)) + abb[6] * (abb[6] * (-abb[4] + -abb[9]) + abb[7] * (-abb[4] + -abb[9]) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-1)) + abb[8] * (-(abb[2] * abb[6]) + abb[24] * T(-2) + abb[6] * (-abb[6] + -abb[7] + bc<T>[0] * int_to_imaginary<T>(-1))) + abb[1] * (abb[2] * (abb[4] + abb[9]) + abb[6] * (abb[4] + abb[9]) + abb[7] * (abb[4] + abb[9]) + abb[12] * (-abb[4] + -abb[8] + -abb[9]) + abb[4] * bc<T>[0] * int_to_imaginary<T>(1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(1) + abb[8] * (abb[2] + abb[6] + abb[7] + bc<T>[0] * int_to_imaginary<T>(1))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_293_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl21 = DLog_W_21<T>(kin),dl8 = DLog_W_8<T>(kin),dl16 = DLog_W_16<T>(kin),dl15 = DLog_W_15<T>(kin),dl17 = DLog_W_17<T>(kin),dl1 = DLog_W_1<T>(kin),dl7 = DLog_W_7<T>(kin),dl31 = DLog_W_31<T>(kin),dl4 = DLog_W_4<T>(kin),dl18 = DLog_W_18<T>(kin),dl3 = DLog_W_3<T>(kin),dl20 = DLog_W_20<T>(kin),dl5 = DLog_W_5<T>(kin),dl19 = DLog_W_19<T>(kin),dl2 = DLog_W_2<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl30 = DLog_W_30<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin),spdl12 = SpDLog_f_4_293_W_12<T>(kin),spdl21 = SpDLog_f_4_293_W_21<T>(kin),spdl7 = SpDLog_f_4_293_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,46> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl8(t), rlog(v_path[2]), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl16(t), f_2_1_15(kin_path), dl15(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl17(t), f_2_1_4(kin_path), dl1(t), dl7(t), f_2_1_11(kin_path), dl31(t), dl4(t), dl18(t), dl3(t), dl20(t), dl5(t), dl19(t), dl2(t), dl27(t) / kin_path.SqrtDelta, rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[17] / kin_path.W[17]), dl28(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta}
;

        auto result = f_4_293_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl21(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_293_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[120];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[20];
z[3] = abb[28];
z[4] = abb[32];
z[5] = abb[4];
z[6] = abb[26];
z[7] = abb[27];
z[8] = abb[29];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[30];
z[12] = abb[31];
z[13] = abb[33];
z[14] = abb[34];
z[15] = abb[35];
z[16] = abb[36];
z[17] = abb[37];
z[18] = abb[38];
z[19] = abb[39];
z[20] = abb[40];
z[21] = abb[41];
z[22] = abb[44];
z[23] = abb[45];
z[24] = abb[2];
z[25] = abb[12];
z[26] = bc<TR>[0];
z[27] = abb[6];
z[28] = abb[7];
z[29] = abb[42];
z[30] = abb[17];
z[31] = abb[22];
z[32] = abb[18];
z[33] = abb[19];
z[34] = abb[21];
z[35] = abb[15];
z[36] = abb[11];
z[37] = abb[13];
z[38] = abb[14];
z[39] = abb[16];
z[40] = abb[24];
z[41] = abb[10];
z[42] = abb[43];
z[43] = abb[25];
z[44] = bc<TR>[3];
z[45] = bc<TR>[1];
z[46] = bc<TR>[2];
z[47] = bc<TR>[4];
z[48] = bc<TR>[9];
z[49] = z[24] + z[28];
z[50] = z[5] + z[9];
z[51] = z[10] + z[50];
z[52] = z[49] * z[51];
z[53] = z[5] + z[10];
z[54] = -z[1] + z[53];
z[55] = z[0] * z[54];
z[56] = z[51] / T(2);
z[57] = z[27] * z[56];
z[58] = z[52] / T(2) + T(2) * z[55] + -z[57];
z[59] = T(3) * z[5];
z[60] = z[9] + z[59];
z[61] = T(3) * z[10];
z[62] = z[60] + z[61];
z[62] = -z[1] + z[62] / T(2);
z[63] = z[33] * z[62];
z[64] = prod_pow(z[26], 2);
z[65] = z[64] / T(9);
z[66] = -(z[25] * z[54]);
z[63] = -z[58] + z[63] + z[65] + z[66];
z[66] = int_to_imaginary<T>(1) * z[26];
z[63] = z[63] * z[66];
z[67] = T(2) * z[28];
z[68] = z[50] * z[67];
z[69] = z[24] * z[56];
z[68] = z[68] + z[69];
z[69] = z[0] / T(2);
z[70] = z[51] * z[69];
z[71] = z[51] / T(4);
z[72] = z[27] * z[71];
z[72] = z[68] + -z[70] + z[72];
z[72] = z[27] * z[72];
z[73] = -z[5] + z[10];
z[74] = T(3) * z[9];
z[75] = -z[73] + z[74];
z[75] = z[1] + z[75] / T(2);
z[75] = z[0] * z[75];
z[75] = -z[68] + z[75];
z[75] = z[0] * z[75];
z[76] = z[25] * z[71];
z[58] = z[58] + -z[76];
z[58] = z[25] * z[58];
z[77] = z[10] + T(5) * z[50];
z[78] = z[40] / T(2);
z[79] = z[77] * z[78];
z[80] = prod_pow(z[28], 2);
z[81] = -(z[50] * z[80]);
z[54] = z[24] * z[54];
z[56] = z[28] * z[56];
z[82] = -z[54] + -z[56];
z[82] = z[24] * z[82];
z[83] = -(z[32] * z[62]);
z[84] = -z[9] + z[59] + z[61];
z[84] = z[84] / T(2);
z[85] = T(2) * z[1] + -z[84];
z[85] = z[34] * z[85];
z[86] = z[64] / T(2);
z[87] = z[5] + T(7) * z[9];
z[87] = -z[10] + z[87] / T(3);
z[87] = z[1] + z[87] / T(2);
z[87] = z[86] * z[87];
z[88] = T(3) * z[26];
z[89] = z[44] * z[88];
z[90] = -z[10] + T(3) * z[50];
z[91] = z[39] * z[90];
z[58] = z[58] + z[63] + z[72] + z[75] + z[79] + z[81] + z[82] + z[83] + z[85] + z[87] + -z[89] + -z[91] / T(2);
z[58] = z[4] * z[58];
z[63] = z[2] * z[62];
z[75] = -z[1] + z[84];
z[79] = z[3] * z[75];
z[81] = z[1] + -z[10];
z[81] = z[30] * z[81];
z[60] = -z[10] + z[60];
z[82] = z[31] * z[60];
z[83] = z[82] / T(2);
z[60] = z[60] / T(2);
z[84] = -(z[8] * z[60]);
z[79] = -z[63] + z[79] + -z[81] + z[83] + z[84];
z[79] = z[33] * z[79];
z[84] = -z[9] + z[53];
z[85] = z[24] * z[84];
z[84] = z[28] * z[84];
z[85] = z[84] + z[85];
z[55] = z[55] + z[85] / T(2);
z[55] = z[3] * z[55];
z[85] = z[50] + z[61];
z[85] = z[41] + z[85] / T(2);
z[87] = z[35] * z[85];
z[92] = z[10] + z[41];
z[93] = z[3] * z[92];
z[93] = -z[87] + z[93];
z[93] = z[27] * z[93];
z[94] = z[0] * z[63];
z[55] = z[55] + -z[93] + -z[94];
z[93] = -z[22] + z[23];
z[94] = z[29] + z[93];
z[94] = z[0] * z[94];
z[95] = z[23] + z[42];
z[96] = z[27] * z[95];
z[97] = z[28] * z[29];
z[98] = z[24] * z[29];
z[94] = -z[94] + z[96] + z[97] + z[98];
z[96] = z[33] * z[93];
z[97] = z[29] + z[95];
z[99] = z[38] * z[97];
z[96] = z[94] + z[96] + -z[99];
z[99] = z[20] / T(2);
z[100] = -(z[96] * z[99]);
z[73] = z[9] + z[73];
z[73] = z[41] + z[73] / T(2);
z[73] = z[3] * z[73];
z[73] = z[73] + -z[83];
z[101] = z[41] + z[53];
z[101] = z[36] * z[101];
z[102] = -z[73] + z[87] + -z[101];
z[102] = z[38] * z[102];
z[103] = z[5] * z[28];
z[104] = z[5] * z[24];
z[105] = z[103] + z[104];
z[57] = z[57] + -z[70] + T(2) * z[105];
z[70] = z[5] * z[25];
z[105] = z[57] + z[70];
z[106] = -z[9] + z[10];
z[107] = z[59] + z[106];
z[107] = z[107] / T(2);
z[108] = -(z[38] * z[107]);
z[108] = z[105] + z[108];
z[108] = z[8] * z[108];
z[109] = z[6] + z[7];
z[110] = z[22] + z[42];
z[111] = -z[13] + z[23];
z[112] = (T(2) * z[3]) / T(27) + z[8] / T(9) + z[29] / T(6) + (T(-4) * z[43]) / T(27) + -z[109] / T(27) + z[110] / T(12) + -z[111] / T(4);
z[112] = z[64] * z[112];
z[113] = T(3) * z[29];
z[114] = -z[111] + z[113];
z[115] = -z[45] / T(2) + z[46];
z[115] = z[45] * z[115];
z[115] = -z[47] + z[115];
z[114] = z[114] * z[115];
z[83] = -(z[49] * z[83]);
z[115] = z[81] + z[101];
z[116] = -(z[25] * z[115]);
z[117] = z[29] + z[110];
z[118] = prod_pow(z[46], 2) * z[117];
z[79] = -z[55] + z[79] + z[83] + z[100] + z[102] + z[108] + z[112] + z[114] + z[116] + -z[118] / T(2);
z[79] = z[66] * z[79];
z[83] = T(5) * z[10];
z[100] = z[50] + z[83];
z[78] = z[78] * z[100];
z[100] = T(2) * z[10];
z[102] = z[24] * z[100];
z[102] = z[56] + z[102];
z[108] = z[0] * z[10];
z[108] = -z[102] + z[108];
z[108] = z[0] * z[108];
z[61] = -z[50] + z[61];
z[112] = z[61] / T(2);
z[114] = z[34] * z[112];
z[78] = z[78] + z[108] + z[114];
z[108] = z[0] * z[51];
z[108] = -z[52] + z[108];
z[114] = z[108] / T(2);
z[116] = T(2) * z[27];
z[118] = z[25] + z[116];
z[118] = z[92] * z[118];
z[119] = -(z[38] * z[85]);
z[118] = z[65] + -z[114] + z[118] + z[119];
z[118] = z[66] * z[118];
z[100] = z[0] * z[100];
z[100] = z[100] + -z[102];
z[102] = z[41] + z[61] / T(4);
z[102] = z[27] * z[102];
z[102] = -z[100] + z[102];
z[102] = z[27] * z[102];
z[116] = -(z[92] * z[116]);
z[76] = z[76] + z[114] + z[116];
z[76] = z[25] * z[76];
z[92] = z[80] * z[92];
z[112] = T(2) * z[41] + z[112];
z[112] = z[39] * z[112];
z[114] = z[10] * z[24];
z[56] = z[56] + z[114];
z[56] = z[24] * z[56];
z[114] = z[37] * z[85];
z[116] = z[10] / T(3);
z[119] = -z[50] + -z[116];
z[119] = z[41] + z[119] / T(2);
z[119] = z[86] * z[119];
z[56] = z[56] + z[76] + z[78] + z[92] + z[102] + z[112] + z[114] + z[118] + z[119];
z[56] = z[12] * z[56];
z[76] = -(z[33] * z[107]);
z[102] = -(z[38] * z[60]);
z[65] = z[65] + z[76] + z[102] + z[105];
z[65] = z[65] * z[66];
z[57] = z[57] + -z[70];
z[57] = z[25] * z[57];
z[67] = z[67] * z[104];
z[70] = z[32] * z[107];
z[76] = z[10] * z[27];
z[76] = z[76] + -z[100];
z[76] = z[27] * z[76];
z[100] = z[9] / T(3);
z[59] = -z[10] + -z[59] + z[100];
z[59] = z[59] * z[64];
z[102] = z[5] * z[80];
z[105] = z[37] * z[60];
z[59] = -z[57] + z[59] / T(4) + z[65] + z[67] + z[70] + z[76] + z[78] + -z[89] + z[102] + z[105];
z[59] = z[11] * z[59];
z[65] = z[40] * z[77];
z[65] = z[65] + -z[91];
z[67] = T(2) * z[103] + z[104];
z[67] = z[24] * z[67];
z[70] = z[0] * z[71];
z[68] = -z[68] + z[70];
z[68] = z[0] * z[68];
z[70] = z[37] * z[107];
z[60] = z[32] * z[60];
z[57] = -z[57] + z[60] + z[65] / T(2) + z[67] + z[68] + z[70] + z[72];
z[57] = z[8] * z[57];
z[60] = z[5] + z[100] + -z[116];
z[60] = z[31] * z[60];
z[65] = z[50] / T(3);
z[67] = z[10] + -z[65];
z[67] = z[6] * z[67];
z[68] = (T(-7) * z[5]) / T(3) + z[9] + -z[116];
z[68] = z[8] * z[68];
z[60] = T(5) * z[60] + z[67] + z[68];
z[67] = -z[53] + -z[100];
z[67] = z[1] / T(3) + z[67] / T(2);
z[67] = z[2] * z[67];
z[68] = z[1] + z[41];
z[70] = T(7) * z[5] + T(-5) * z[9] + z[83];
z[70] = -z[68] + z[70] / T(2);
z[70] = z[3] * z[70];
z[71] = -z[50] + z[116];
z[72] = z[7] / T(2);
z[71] = z[71] * z[72];
z[76] = z[46] * z[117];
z[65] = z[10] + z[65];
z[65] = z[41] / T(3) + z[65] / T(2);
z[65] = z[35] * z[65];
z[60] = z[60] / T(2) + z[65] + z[67] + z[70] / T(3) + z[71] + (T(-3) * z[76]) / T(2) + (T(-5) * z[115]) / T(3);
z[65] = -z[111] / T(3);
z[67] = z[29] + z[65];
z[67] = z[45] * z[67];
z[60] = z[60] / T(2) + z[67];
z[60] = z[60] * z[64];
z[67] = z[13] + z[29];
z[70] = z[67] + -z[95];
z[71] = z[39] * z[70];
z[76] = z[23] + z[67];
z[76] = z[40] * z[76];
z[77] = z[22] + -z[29];
z[77] = z[34] * z[77];
z[78] = z[32] * z[93];
z[83] = z[28] * z[98];
z[89] = z[37] * z[97];
z[71] = -z[71] + z[76] + z[77] + -z[78] + z[83] + z[89];
z[76] = z[25] * z[117];
z[77] = z[76] / T(2) + -z[94];
z[77] = z[25] * z[77];
z[78] = z[13] + z[22] + z[23];
z[83] = z[69] * z[78];
z[89] = z[23] * z[24];
z[67] = z[28] * z[67];
z[67] = z[67] + z[89];
z[83] = -z[67] + z[83];
z[83] = z[0] * z[83];
z[89] = z[27] / T(2);
z[93] = z[70] * z[89];
z[97] = z[0] * z[23];
z[97] = -z[67] + z[97];
z[93] = z[93] + z[97];
z[93] = z[27] * z[93];
z[77] = z[71] + z[77] + z[83] + -z[93];
z[65] = -z[29] + z[65] + -z[110] / T(3);
z[83] = z[65] * z[86];
z[83] = z[77] + z[83];
z[66] = z[66] * z[96];
z[93] = z[66] + z[83];
z[96] = z[18] + -z[21] / T(2);
z[93] = z[93] * z[96];
z[77] = z[66] + z[77];
z[95] = -z[13] + z[22] + z[95] + z[113];
z[86] = z[86] * z[95];
z[77] = T(-3) * z[77] + z[86];
z[86] = z[14] + z[15] + z[16] + z[17];
z[86] = -z[86] / T(2);
z[77] = z[77] * z[86];
z[53] = -z[53] + -z[74];
z[53] = -z[1] + z[53] / T(2);
z[53] = z[0] * z[53];
z[52] = z[52] + z[53];
z[52] = z[52] * z[69];
z[53] = z[39] * z[85];
z[54] = -z[54] + -z[84] / T(2);
z[54] = z[24] * z[54];
z[74] = -(z[32] * z[75]);
z[51] = -(z[40] * z[51]);
z[51] = z[51] + z[52] + z[53] + z[54] + z[74] + z[92];
z[51] = z[3] * z[51];
z[52] = -(z[83] * z[99]);
z[53] = z[66] + z[71];
z[54] = -z[76] + T(2) * z[94];
z[54] = z[25] * z[54];
z[66] = z[27] * z[70];
z[66] = z[66] + T(2) * z[97];
z[66] = z[27] * z[66];
z[70] = -(z[0] * z[78]);
z[67] = T(2) * z[67] + z[70];
z[67] = z[0] * z[67];
z[64] = -(z[64] * z[65]);
z[53] = T(-2) * z[53] + z[54] + z[64] + z[66] + z[67];
z[53] = z[19] * z[53];
z[54] = -z[63] + z[82] + z[87];
z[64] = -z[68] + z[106];
z[64] = -z[5] + -z[64] / T(2);
z[64] = z[3] * z[64];
z[54] = -z[54] / T(2) + z[64] + z[115];
z[54] = z[25] * z[54];
z[49] = z[49] * z[82];
z[49] = z[49] / T(2) + z[54] + z[55];
z[49] = z[25] * z[49];
z[54] = -(z[24] * z[28]);
z[54] = -z[32] + z[54];
z[54] = z[54] * z[82];
z[55] = z[6] * z[61];
z[61] = -(z[40] * z[55]);
z[64] = (T(11) * z[29]) / T(4) + (T(7) * z[110]) / T(4) + (T(19) * z[111]) / T(3);
z[64] = z[48] * z[64];
z[54] = z[54] + z[61] + z[64];
z[61] = -(z[3] * z[62]);
z[61] = -z[55] / T(2) + z[61] + z[63];
z[61] = z[34] * z[61];
z[62] = z[0] + -z[24];
z[62] = z[55] * z[62];
z[64] = z[3] * z[108];
z[50] = -z[10] + z[50];
z[50] = -z[41] + z[50] / T(2);
z[50] = z[3] * z[50];
z[50] = z[50] + -z[87];
z[50] = z[27] * z[50];
z[50] = z[50] + z[62] + z[64];
z[50] = z[50] * z[89];
z[62] = -z[55] + z[63];
z[62] = z[0] * z[62];
z[55] = z[24] * z[55];
z[55] = z[55] + z[62];
z[55] = z[55] * z[69];
z[62] = z[32] * z[63];
z[63] = -prod_pow(z[24], 2);
z[63] = z[32] + z[63];
z[63] = z[63] * z[81];
z[64] = z[37] * z[73];
z[65] = z[37] + -z[80];
z[65] = z[65] * z[101];
z[66] = -z[37] + -z[39];
z[66] = z[66] * z[87];
z[67] = z[28] + -z[69];
z[67] = z[0] * z[67];
z[68] = -z[28] + z[89];
z[68] = z[27] * z[68];
z[67] = -z[40] + z[67] + z[68];
z[67] = z[67] * z[90];
z[67] = z[67] + z[91];
z[67] = z[67] * z[72];
z[68] = T(-2) * z[3] + T(-3) * z[8] + T(4) * z[43] + z[109];
z[68] = z[26] * z[68];
z[69] = -(z[12] * z[88]);
z[68] = z[68] + z[69];
z[68] = z[44] * z[68];
return z[49] + z[50] + z[51] + z[52] + z[53] + z[54] / T(2) + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + z[68] + z[77] + z[79] + z[93];
}



template IntegrandConstructorType<double> f_4_293_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_293_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_293_construct (const Kin<qd_real>&);
#endif

}