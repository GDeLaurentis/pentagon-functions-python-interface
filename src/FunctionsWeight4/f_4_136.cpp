#include "f_4_136.h"

namespace PentagonFunctions {

template <typename T> T f_4_136_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_136_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_136_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(2) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + T(2) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(2) * kin.v[3] + T(4) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((-kin.v[4] / T(2) + (T(3) * kin.v[1]) / T(4) + kin.v[2] / T(2) + (T(-3) * kin.v[3]) / T(2) + T(-1) + kin.v[0]) * kin.v[1] + (-kin.v[2] / T(4) + -kin.v[3] / T(2) + kin.v[4] / T(2) + T(-1)) * kin.v[2] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + ((T(3) * kin.v[3]) / T(4) + kin.v[4] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(4) + T(1)) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((-kin.v[4] / T(2) + (T(3) * kin.v[1]) / T(4) + kin.v[2] / T(2) + (T(-3) * kin.v[3]) / T(2) + T(-1) + kin.v[0]) * kin.v[1] + (-kin.v[2] / T(4) + -kin.v[3] / T(2) + kin.v[4] / T(2) + T(-1)) * kin.v[2] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + ((T(3) * kin.v[3]) / T(4) + kin.v[4] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(4) + T(1)) * kin.v[4]) + rlog(kin.v[2]) * ((-kin.v[2] / T(2) + (T(-3) * kin.v[1]) / T(4) + (T(3) * kin.v[3]) / T(2) + kin.v[4] / T(2) + -kin.v[0] + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + kin.v[2] / T(4) + kin.v[3] / T(2) + T(1)) * kin.v[2] + (-kin.v[4] / T(2) + (T(-3) * kin.v[3]) / T(4) + T(-1)) * kin.v[3] + (kin.v[4] / T(4) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((-kin.v[2] / T(2) + (T(-3) * kin.v[1]) / T(4) + (T(3) * kin.v[3]) / T(2) + kin.v[4] / T(2) + -kin.v[0] + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + kin.v[2] / T(4) + kin.v[3] / T(2) + T(1)) * kin.v[2] + (-kin.v[4] / T(2) + (T(-3) * kin.v[3]) / T(4) + T(-1)) * kin.v[3] + (kin.v[4] / T(4) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]));
c[1] = rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (-kin.v[3] + -kin.v[4] + kin.v[1] + kin.v[2]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (-kin.v[3] + -kin.v[4] + kin.v[1] + kin.v[2]) + rlog(kin.v[2]) * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (prod_pow(abb[2], 2) * (abb[5] / T(2) + -abb[6] / T(2) + -abb[7] / T(2)) + abb[4] * (prod_pow(abb[2], 2) / T(2) + -abb[3]) + abb[3] * (abb[6] + abb[7] + -abb[5]) + abb[1] * (abb[2] * abb[4] + abb[1] * ((abb[4] * T(-3)) / T(2) + (abb[5] * T(-3)) / T(2) + (abb[6] * T(3)) / T(2) + (abb[7] * T(3)) / T(2)) + abb[2] * (abb[5] + -abb[6] + -abb[7])));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_136_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl16 = DLog_W_16<T>(kin),dl8 = DLog_W_8<T>(kin),dl20 = DLog_W_20<T>(kin),dl19 = DLog_W_19<T>(kin),dl3 = DLog_W_3<T>(kin),spdl22 = SpDLog_f_4_136_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,17> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl16(t), rlog(v_path[2]), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), rlog(kin.W[7] / kin_path.W[7]), dl8(t), dl20(t), dl19(t), dl3(t)}
;

        auto result = f_4_136_abbreviated(abbr);
        result = result + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_136_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[54];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[8];
z[3] = abb[14];
z[4] = abb[15];
z[5] = abb[16];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[12];
z[10] = abb[2];
z[11] = abb[13];
z[12] = abb[9];
z[13] = bc<TR>[0];
z[14] = abb[3];
z[15] = abb[10];
z[16] = abb[11];
z[17] = bc<TR>[2];
z[18] = bc<TR>[9];
z[19] = (T(3) * z[8]) / T(2);
z[20] = z[7] / T(2);
z[21] = -z[9] + z[20];
z[22] = z[19] + z[21];
z[22] = z[4] * z[22];
z[23] = z[8] / T(2);
z[24] = -z[21] + z[23];
z[24] = z[5] * z[24];
z[25] = z[2] + z[5];
z[26] = (T(3) * z[3]) / T(2) + z[4];
z[27] = -z[25] + z[26];
z[27] = z[1] * z[27];
z[28] = T(3) * z[2];
z[26] = -z[5] + z[26] + -z[28];
z[26] = z[6] * z[26];
z[29] = (T(3) * z[7]) / T(2) + -z[23];
z[30] = -z[9] + -z[29];
z[30] = z[3] * z[30];
z[31] = T(2) * z[7] + -z[9];
z[32] = z[2] * z[31];
z[24] = -z[22] + z[24] + z[26] + z[27] + z[30] + z[32];
z[24] = z[0] * z[24];
z[26] = T(2) * z[5];
z[27] = T(2) * z[2];
z[30] = z[26] + z[27];
z[32] = z[7] + z[8];
z[33] = T(2) * z[9];
z[34] = z[32] + -z[33];
z[35] = z[30] * z[34];
z[36] = z[3] + z[4];
z[37] = T(2) * z[11];
z[38] = z[36] + -z[37];
z[38] = T(2) * z[38];
z[39] = z[1] + z[6];
z[38] = z[38] * z[39];
z[40] = z[7] + z[9];
z[37] = z[37] * z[40];
z[41] = -z[7] + z[33];
z[42] = T(3) * z[8];
z[43] = z[41] + -z[42];
z[44] = z[4] * z[43];
z[45] = T(3) * z[7];
z[46] = -z[8] + z[45];
z[47] = z[3] * z[46];
z[35] = -z[35] + -z[37] + -z[38] + -z[44] + z[47];
z[38] = z[12] * z[35];
z[47] = z[3] + z[30];
z[48] = T(4) * z[11];
z[49] = z[47] + -z[48];
z[49] = z[39] * z[49];
z[40] = z[8] + -z[40];
z[40] = z[27] * z[40];
z[50] = z[3] * z[33];
z[51] = -z[8] + -z[41];
z[51] = z[5] * z[51];
z[37] = z[37] + z[40] + z[49] + -z[50] + z[51];
z[37] = z[10] * z[37];
z[24] = z[24] + z[37] + z[38];
z[24] = z[0] * z[24];
z[37] = z[3] + -z[4];
z[38] = (T(-7) * z[2]) / T(2) + T(-4) * z[5] + z[37] + z[48];
z[38] = z[6] * z[38];
z[40] = T(4) * z[9];
z[49] = -z[7] + z[8] + z[40];
z[49] = z[5] * z[49];
z[38] = z[38] + z[49];
z[49] = z[21] / T(3) + z[23];
z[49] = z[4] * z[49];
z[51] = z[8] / T(3);
z[52] = z[31] + z[51];
z[52] = z[2] * z[52];
z[51] = (T(-2) * z[7]) / T(3) + z[9] + -z[51];
z[51] = z[48] * z[51];
z[53] = -z[2] + z[11];
z[37] = -z[5] + z[37] / T(3) + (T(4) * z[53]) / T(3);
z[37] = z[1] * z[37];
z[28] = T(-5) * z[3] + T(-9) * z[4] + T(3) * z[5] + z[28];
z[28] = z[28] / T(4) + z[48];
z[28] = z[17] * z[28];
z[48] = -z[8] / T(6) + (T(-4) * z[9]) / T(3) + z[20];
z[48] = z[3] * z[48];
z[28] = z[28] + z[37] + z[38] / T(3) + z[48] + z[49] + z[51] + z[52];
z[37] = prod_pow(z[13], 2);
z[28] = z[28] * z[37];
z[38] = T(7) * z[11];
z[48] = T(2) * z[4];
z[49] = T(-3) * z[3] + -z[30] + z[38] + -z[48];
z[49] = z[39] * z[49];
z[42] = z[7] + z[40] + -z[42];
z[42] = z[2] * z[42];
z[51] = z[33] + z[46];
z[51] = z[3] * z[51];
z[26] = z[26] * z[41];
z[52] = T(5) * z[7] + z[8];
z[52] = z[11] * z[52];
z[26] = z[26] + z[42] + -z[44] + z[49] + z[51] + -z[52];
z[42] = z[16] * z[26];
z[35] = -(z[0] * z[35]);
z[49] = -z[3] + z[11];
z[49] = -(z[39] * z[49]);
z[34] = z[2] * z[34];
z[32] = -z[32] + z[40];
z[32] = z[11] * z[32];
z[32] = z[32] + z[34] + z[49] + -z[50];
z[32] = z[12] * z[32];
z[34] = z[8] + -z[33] + z[45];
z[34] = z[11] * z[34];
z[45] = T(3) * z[11] + -z[47];
z[45] = z[39] * z[45];
z[47] = z[3] * z[9];
z[49] = z[5] * z[8];
z[47] = z[47] + z[49];
z[49] = z[2] * z[46];
z[34] = -z[34] + z[45] + T(2) * z[47] + z[49];
z[45] = -(z[10] * z[34]);
z[47] = z[3] + T(5) * z[4];
z[50] = T(7) * z[25] + -z[47];
z[50] = prod_pow(z[17], 2) * z[50];
z[36] = (T(5) * z[2]) / T(3) + z[5] + (T(-7) * z[11]) / T(3) + z[36] / T(6);
z[36] = z[36] * z[37];
z[36] = z[36] + z[50];
z[32] = T(2) * z[32] + z[35] + z[36] / T(2) + z[42] + z[45];
z[32] = int_to_imaginary<T>(1) * z[13] * z[32];
z[35] = z[3] / T(2) + z[4] + (T(-5) * z[11]) / T(2) + z[25];
z[35] = z[35] * z[39];
z[19] = (T(7) * z[7]) / T(2) + z[19] + -z[40];
z[19] = z[11] * z[19];
z[29] = z[9] + -z[29];
z[29] = z[3] * z[29];
z[36] = -(z[5] * z[41]);
z[19] = z[19] + -z[22] + z[29] + z[35] + z[36] + -z[49] / T(2);
z[19] = prod_pow(z[12], 2) * z[19];
z[22] = -z[3] + z[30] + -z[48];
z[22] = -(z[22] * z[39]);
z[29] = z[33] + -z[46];
z[29] = z[3] * z[29];
z[30] = -(z[5] * z[43]);
z[27] = z[27] * z[31];
z[22] = z[22] + z[27] + z[29] + z[30] + z[44];
z[22] = z[14] * z[22];
z[26] = -(z[15] * z[26]);
z[27] = z[12] * z[34];
z[20] = -z[20] + -z[23] + z[33] + -z[39] / T(2);
z[20] = z[11] * z[20];
z[21] = z[21] + z[23];
z[21] = z[21] * z[25];
z[20] = z[20] + z[21];
z[20] = z[10] * z[20];
z[20] = z[20] + z[27];
z[20] = z[10] * z[20];
z[21] = (T(-41) * z[2]) / T(3) + T(21) * z[5] + z[47];
z[21] = z[21] / T(8) + -z[38];
z[21] = z[18] * z[21];
return z[19] + z[20] + z[21] + z[22] + z[24] + z[26] + z[28] + z[32];
}



template IntegrandConstructorType<double> f_4_136_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_136_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_136_construct (const Kin<qd_real>&);
#endif

}