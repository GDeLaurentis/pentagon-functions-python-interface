#include "f_4_96.h"

namespace PentagonFunctions {

template <typename T> T f_4_96_abbreviated (const std::array<T,43>&);

template <typename T> class SpDLog_f_4_96_W_10 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_96_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(12) + T(-6) * kin.v[1] + T(-12) * kin.v[2] + T(12) * kin.v[4]) + kin.v[2] * (T(12) + T(-6) * kin.v[2] + T(12) * kin.v[4]);
c[1] = kin.v[1] * (T(12) + T(-6) * kin.v[1] + T(-12) * kin.v[2] + T(12) * kin.v[4]) + kin.v[2] * (T(12) + T(-6) * kin.v[2] + T(12) * kin.v[4]);
c[2] = kin.v[1] * (T(12) + T(-6) * kin.v[1] + T(-12) * kin.v[2] + T(12) * kin.v[4]) + kin.v[2] * (T(12) + T(-6) * kin.v[2] + T(12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1] + abb[5] * c[2]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-6) + prod_pow(abb[2], 2) * (abb[4] * T(-6) + abb[5] * T(-6)) + prod_pow(abb[1], 2) * (abb[3] * T(6) + abb[4] * T(6) + abb[5] * T(6)));
    }
};
template <typename T> class SpDLog_f_4_96_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_96_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (T(-12) + T(6) * kin.v[0] + T(12) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[4] * (T(-12) + T(-6) * kin.v[4]) + kin.v[2] * (T(12) + T(-6) * kin.v[2] + T(12) * kin.v[4]) + kin.v[1] * (T(-12) * kin.v[2] + T(-12) * kin.v[3] + T(12) * kin.v[4]);
c[1] = kin.v[0] * (T(-12) + T(6) * kin.v[0] + T(12) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[4] * (T(-12) + T(-6) * kin.v[4]) + kin.v[2] * (T(12) + T(-6) * kin.v[2] + T(12) * kin.v[4]) + kin.v[1] * (T(-12) * kin.v[2] + T(-12) * kin.v[3] + T(12) * kin.v[4]);
c[2] = kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[0] * (T(12) + T(-6) * kin.v[0] + T(-12) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(-12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(12) * kin.v[2] + T(12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(6) * kin.v[4]);
c[3] = kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[0] * (T(12) + T(-6) * kin.v[0] + T(-12) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(-12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[1] * (T(12) * kin.v[2] + T(12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[8] * c[0] + abb[5] * c[1] + abb[3] * c[2] + abb[4] * c[3]);
        }

        return abb[6] * (prod_pow(abb[2], 2) * abb[3] * T(6) + prod_pow(abb[2], 2) * (abb[5] * T(-6) + abb[8] * T(-6) + abb[4] * T(6)) + prod_pow(abb[7], 2) * (abb[3] * T(-6) + abb[4] * T(-6) + abb[5] * T(6) + abb[8] * T(6)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_96_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl23 = DLog_W_23<T>(kin),dl27 = DLog_W_27<T>(kin),dl13 = DLog_W_13<T>(kin),dl11 = DLog_W_11<T>(kin),dl15 = DLog_W_15<T>(kin),dl9 = DLog_W_9<T>(kin),dl4 = DLog_W_4<T>(kin),dl3 = DLog_W_3<T>(kin),dl5 = DLog_W_5<T>(kin),dl16 = DLog_W_16<T>(kin),dl1 = DLog_W_1<T>(kin),dl17 = DLog_W_17<T>(kin),dl26 = DLog_W_26<T>(kin),dl31 = DLog_W_31<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),dl2 = DLog_W_2<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),spdl10 = SpDLog_f_4_96_W_10<T>(kin),spdl23 = SpDLog_f_4_96_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,43> abbr = 
            {dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), f_1_3_4(kin) - f_1_3_4(kin_path), dl27(t) / kin_path.SqrtDelta, rlog(v_path[0]), rlog(v_path[3]), rlog(v_path[2]), f_2_1_1(kin_path), f_2_1_2(kin_path), f_2_1_7(kin_path), f_2_1_8(kin_path), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[30] / kin_path.W[30]), dl13(t), dl11(t), dl15(t), dl9(t), dl4(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl3(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl5(t), dl16(t), dl1(t), dl17(t), dl26(t) / kin_path.SqrtDelta, dl31(t), f_2_2_4(kin_path), dl18(t), dl20(t), dl2(t), dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta}
;

        auto result = f_4_96_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_96_abbreviated(const std::array<T,43>& abb)
{
using TR = typename T::value_type;
T z[100];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[23];
z[3] = abb[31];
z[4] = abb[40];
z[5] = abb[4];
z[6] = abb[33];
z[7] = abb[5];
z[8] = abb[38];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[17];
z[12] = abb[18];
z[13] = abb[19];
z[14] = abb[20];
z[15] = abb[2];
z[16] = abb[28];
z[17] = abb[34];
z[18] = abb[35];
z[19] = abb[41];
z[20] = abb[10];
z[21] = abb[39];
z[22] = abb[12];
z[23] = abb[7];
z[24] = abb[32];
z[25] = abb[42];
z[26] = abb[25];
z[27] = abb[11];
z[28] = bc<TR>[0];
z[29] = abb[24];
z[30] = abb[15];
z[31] = abb[16];
z[32] = abb[26];
z[33] = abb[27];
z[34] = abb[29];
z[35] = abb[30];
z[36] = abb[21];
z[37] = abb[22];
z[38] = abb[13];
z[39] = abb[14];
z[40] = abb[36];
z[41] = abb[37];
z[42] = bc<TR>[1];
z[43] = bc<TR>[3];
z[44] = bc<TR>[5];
z[45] = bc<TR>[2];
z[46] = bc<TR>[4];
z[47] = bc<TR>[7];
z[48] = bc<TR>[8];
z[49] = bc<TR>[9];
z[50] = z[5] + -z[9];
z[51] = z[1] + z[50];
z[52] = z[4] * z[51];
z[53] = z[5] * z[16];
z[54] = z[1] + z[5];
z[55] = z[21] * z[54];
z[56] = z[7] * z[8];
z[56] = -z[52] + z[53] + z[55] + -z[56];
z[57] = z[7] + z[9];
z[58] = z[1] + z[57];
z[59] = z[29] * z[58];
z[60] = z[1] + -z[9];
z[61] = z[2] * z[60];
z[62] = z[59] + -z[61];
z[63] = T(2) * z[9];
z[64] = z[1] / T(2);
z[65] = (T(5) * z[7]) / T(2) + z[63] + z[64];
z[65] = z[3] * z[65];
z[64] = T(2) * z[57] + -z[64];
z[64] = z[24] * z[64];
z[66] = -z[4] + -z[8] + -z[21] + T(2) * z[40];
z[67] = T(3) * z[25];
z[68] = T(4) * z[66] + -z[67];
z[69] = int_to_imaginary<T>(1) * z[28];
z[68] = z[68] * z[69];
z[70] = z[1] * z[16];
z[71] = z[9] * z[16];
z[72] = z[7] / T(2);
z[73] = T(4) * z[9] + -z[72];
z[73] = z[17] * z[73];
z[63] = (T(9) * z[1]) / T(2) + T(-2) * z[5] + (T(13) * z[7]) / T(2) + -z[63];
z[63] = z[26] * z[63];
z[72] = T(6) * z[9] + -z[72];
z[72] = z[6] * z[72];
z[74] = z[11] + z[12] + z[13] + T(-2) * z[14];
z[75] = T(2) * z[74];
z[76] = z[42] / T(2) + T(-7) * z[45] + -z[75];
z[76] = z[25] * z[76];
z[75] = T(7) * z[42] + T(-15) * z[45] + z[75];
z[75] = z[19] * z[75];
z[56] = T(2) * z[56] + T(-6) * z[62] + z[63] + z[64] + z[65] + z[68] + (T(-9) * z[70]) / T(2) + T(-4) * z[71] + z[72] + z[73] + z[75] + z[76];
z[56] = z[28] * z[56];
z[62] = z[26] * z[54];
z[63] = -z[55] + z[62];
z[54] = z[17] * z[54];
z[64] = z[18] * z[74];
z[54] = z[54] + -z[64];
z[65] = z[54] + z[63];
z[68] = z[19] * z[74];
z[72] = -z[7] + z[50];
z[73] = z[4] * z[72];
z[75] = z[68] + -z[73];
z[76] = z[25] * z[74];
z[77] = z[6] * z[72];
z[77] = z[76] + z[77];
z[72] = z[24] * z[72];
z[72] = z[72] + z[77];
z[78] = z[65] + -z[72] + -z[75];
z[79] = z[33] * z[78];
z[53] = z[53] + -z[71];
z[80] = -z[52] + z[53];
z[81] = z[70] + z[80];
z[51] = z[17] * z[51];
z[51] = z[51] + -z[64];
z[64] = z[51] + z[81];
z[82] = z[3] * z[5];
z[83] = z[5] * z[6];
z[82] = z[82] + z[83];
z[84] = z[5] * z[21];
z[85] = z[82] + -z[84];
z[86] = -z[64] + z[85];
z[87] = z[68] + -z[86];
z[87] = z[35] * z[87];
z[79] = z[79] + -z[87];
z[87] = z[9] * z[17];
z[63] = z[63] + -z[68] + -z[81] + z[87];
z[81] = T(6) * z[15];
z[63] = z[63] * z[81];
z[50] = z[36] * z[50];
z[84] = z[50] + -z[84];
z[87] = z[6] * z[9];
z[88] = z[5] + -z[7];
z[89] = z[37] * z[88];
z[87] = -z[73] + -z[84] + -z[87] + z[89];
z[90] = z[9] * z[26];
z[91] = z[71] + z[90];
z[92] = z[68] + -z[91];
z[93] = -z[87] + -z[92];
z[94] = T(6) * z[20];
z[93] = z[93] * z[94];
z[71] = -z[59] + -z[71] + z[89];
z[89] = z[1] + z[7];
z[89] = z[26] * z[89];
z[54] = z[54] + z[71] + -z[72] + z[89];
z[89] = T(6) * z[27];
z[54] = z[54] * z[89];
z[95] = prod_pow(z[42], 2);
z[96] = z[66] * z[95];
z[97] = z[42] + -z[45];
z[97] = z[42] * z[97];
z[97] = T(2) * z[46] + z[97];
z[67] = z[67] * z[97];
z[67] = z[67] + z[96];
z[96] = T(3) * z[19];
z[97] = z[42] + T(-2) * z[45];
z[97] = z[42] * z[96] * z[97];
z[54] = z[54] + z[63] + T(2) * z[67] + T(-6) * z[79] + z[93] + z[97];
z[54] = int_to_imaginary<T>(1) * z[54];
z[63] = -(z[43] * z[66]);
z[54] = z[54] + T(-8) * z[63];
z[54] = T(2) * z[54] + z[56];
z[54] = z[28] * z[54];
z[56] = z[8] * z[9];
z[63] = z[56] + -z[91];
z[67] = z[10] * z[74];
z[79] = z[67] + -z[68];
z[91] = -z[63] + z[79] + -z[85];
z[91] = z[38] * z[91];
z[78] = z[32] * z[78];
z[65] = z[65] + -z[67];
z[93] = z[3] + z[24];
z[93] = z[57] * z[93];
z[57] = z[8] * z[57];
z[97] = -z[57] + z[93];
z[98] = -z[65] + z[97];
z[98] = z[31] * z[98];
z[86] = z[34] * z[86];
z[97] = -z[76] + z[97];
z[99] = z[64] + -z[67] + z[97];
z[99] = z[30] * z[99];
z[72] = z[72] + -z[73];
z[63] = -z[63] + -z[67] + z[72];
z[63] = z[39] * z[63];
z[73] = -(z[41] * z[66] * z[74]);
z[96] = z[25] + z[96];
z[96] = z[47] * z[96];
z[63] = z[63] + z[73] + z[78] + z[86] + z[91] + z[96] + z[98] + z[99];
z[60] = -(z[3] * z[60]);
z[52] = -z[52] + -z[57] + z[60] + z[61] + -z[67] + z[83];
z[52] = prod_pow(z[0], 2) * z[52];
z[58] = z[24] * z[58];
z[57] = z[55] + -z[57] + z[58] + -z[59] + -z[77] + z[79];
z[57] = z[23] * z[57];
z[58] = z[15] * z[65];
z[59] = z[72] + -z[79];
z[60] = -z[20] + z[27];
z[59] = z[59] * z[60];
z[65] = z[68] + -z[97];
z[65] = z[0] * z[65];
z[59] = z[58] + z[59] + z[65];
z[57] = z[57] + T(2) * z[59];
z[57] = z[23] * z[57];
z[59] = z[61] + -z[90];
z[50] = -z[50] + -z[51] + z[59] + -z[70] + z[82];
z[50] = z[50] * z[69];
z[51] = z[64] + -z[79];
z[61] = z[15] * z[51];
z[64] = z[56] + z[92];
z[60] = -(z[60] * z[64]);
z[65] = -z[67] + z[85];
z[67] = -(z[0] * z[65]);
z[50] = z[50] + z[60] + z[61] + z[67];
z[59] = -z[59] + -z[68] + -z[80] + z[84];
z[59] = z[22] * z[59];
z[50] = T(2) * z[50] + z[59];
z[50] = z[22] * z[50];
z[50] = z[50] + z[52] + z[57];
z[52] = z[7] * z[17];
z[52] = z[52] + z[53] + -z[62] + z[70] + -z[76] + z[93];
z[52] = z[15] * z[52];
z[53] = T(2) * z[0];
z[51] = -(z[51] * z[53]);
z[51] = z[51] + z[52];
z[51] = z[51] * z[81];
z[52] = T(-2) * z[56] + z[87] + -z[92];
z[52] = z[20] * z[52];
z[53] = z[53] * z[65];
z[52] = z[52] + z[53];
z[52] = z[52] * z[94];
z[53] = z[26] * z[88];
z[53] = z[53] + -z[55] + -z[71] + -z[75];
z[53] = z[27] * z[53];
z[55] = z[20] * z[64];
z[55] = z[55] + -z[58];
z[53] = z[53] + T(2) * z[55];
z[53] = z[53] * z[89];
z[55] = -z[31] + -z[34];
z[56] = T(12) * z[74];
z[55] = z[55] * z[56];
z[57] = prod_pow(z[45], 3);
z[58] = T(4) * z[46] + z[95];
z[58] = z[42] * z[58];
z[55] = T(9) * z[48] + (T(-91) * z[49]) / T(2) + z[55] + T(-12) * z[57] + T(6) * z[58];
z[55] = z[19] * z[55];
z[56] = -(z[31] * z[56]);
z[56] = T(3) * z[48] + (T(-35) * z[49]) / T(2) + z[56] + T(-4) * z[57];
z[56] = z[25] * z[56];
z[57] = z[42] * z[43];
z[57] = T(4) * z[44] + z[57];
z[57] = int_to_imaginary<T>(-1) * z[57] * z[66];
return T(6) * z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + T(48) * z[57] + T(12) * z[63];
}



template IntegrandConstructorType<double> f_4_96_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_96_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_96_construct (const Kin<qd_real>&);
#endif

}