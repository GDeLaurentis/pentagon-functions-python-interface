#include "f_4_171.h"

namespace PentagonFunctions {

template <typename T> T f_4_171_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_171_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_171_W_12 (const Kin<T>& kin) {
        c[0] = (-kin.v[1] / T(4) + -kin.v[4] / T(2) + T(-1)) * kin.v[1] + ((T(3) * kin.v[4]) / T(4) + T(1)) * kin.v[4];
c[1] = kin.v[1] + -kin.v[4];
c[2] = (kin.v[1] / T(4) + kin.v[4] / T(2) + T(1)) * kin.v[1] + ((T(-3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4];
c[3] = -kin.v[1] + kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[5] * (t * c[0] + c[1]) + abb[4] * (t * c[2] + c[3]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[5] + abb[4] * (abb[3] + -prod_pow(abb[2], 2)) + prod_pow(abb[1], 2) * (abb[4] + -abb[5]) + -(abb[3] * abb[5]));
    }
};
template <typename T> class SpDLog_f_4_171_W_10 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_171_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]);
c[1] = kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]);
c[2] = kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[4] * c[0] + abb[8] * c[1] + abb[9] * c[2]);
        }

        return abb[6] * (prod_pow(abb[2], 2) * (abb[4] + abb[8] + abb[9]) + -(abb[4] * prod_pow(abb[7], 2)) + prod_pow(abb[7], 2) * (-abb[8] + -abb[9]));
    }
};
template <typename T> class SpDLog_f_4_171_W_23 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_171_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (-kin.v[4] + T(-4) + (T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(4) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + T(-1) + kin.v[1]) + T(-2) * kin.v[3]) + ((T(-5) * kin.v[4]) / T(2) + T(-4)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + T(4) + T(2) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + -kin.v[3] + T(4) + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]));
c[1] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4];
c[2] = kin.v[0] * (-kin.v[4] + T(-4) + (T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(4) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + T(-1) + kin.v[1]) + T(-2) * kin.v[3]) + ((T(-5) * kin.v[4]) / T(2) + T(-4)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + T(4) + T(2) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + -kin.v[3] + T(4) + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]));
c[3] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4];
c[4] = kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-4) + kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[3] * (-kin.v[3] / T(2) + T(-4) + T(-2) * kin.v[4]) + kin.v[0] * (T(4) + (T(-3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[0] + T(-4) * kin.v[1] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + T(1) + kin.v[3]) + kin.v[4]) + ((T(5) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (kin.v[4] / T(2) + T(1)) * kin.v[4]);
c[5] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[4]);
c[6] = kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-4) + kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[3] * (-kin.v[3] / T(2) + T(-4) + T(-2) * kin.v[4]) + kin.v[0] * (T(4) + (T(-3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[0] + T(-4) * kin.v[1] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + T(1) + kin.v[3]) + kin.v[4]) + ((T(5) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (kin.v[4] / T(2) + T(1)) * kin.v[4]);
c[7] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[4] * (t * c[0] + c[1]) + abb[8] * (t * c[2] + c[3]) + abb[11] * (t * c[4] + c[5]) + abb[9] * (t * c[6] + c[7]);
        }

        return abb[16] * (abb[2] * abb[7] * (abb[9] + abb[11] + -abb[8]) + abb[7] * abb[14] * (abb[9] + abb[11] + -abb[8]) + abb[1] * (abb[4] * abb[7] + abb[15] * (abb[9] + abb[11] + -abb[4] + -abb[8]) + abb[7] * (abb[8] + -abb[9] + -abb[11])) + abb[7] * (abb[7] * (abb[9] + abb[11] + -abb[8]) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[15] * (abb[2] * (abb[8] + -abb[9] + -abb[11]) + abb[7] * (abb[8] + -abb[9] + -abb[11]) + abb[14] * (abb[8] + -abb[9] + -abb[11]) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[4] * (abb[2] + abb[7] + abb[14] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[9] * bc<T>[0] * int_to_imaginary<T>(1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[4] * (-(abb[2] * abb[7]) + -(abb[7] * abb[14]) + abb[17] * T(-2) + abb[7] * (-abb[7] + bc<T>[0] * int_to_imaginary<T>(1))) + abb[17] * (abb[8] * T(-2) + abb[9] * T(2) + abb[11] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_171_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl10 = DLog_W_10<T>(kin),dl3 = DLog_W_3<T>(kin),dl9 = DLog_W_9<T>(kin),dl23 = DLog_W_23<T>(kin),dl14 = DLog_W_14<T>(kin),dl19 = DLog_W_19<T>(kin),dl1 = DLog_W_1<T>(kin),dl31 = DLog_W_31<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),dl2 = DLog_W_2<T>(kin),dl17 = DLog_W_17<T>(kin),dl5 = DLog_W_5<T>(kin),dl4 = DLog_W_4<T>(kin),dl16 = DLog_W_16<T>(kin),dl27 = DLog_W_27<T>(kin),dl26 = DLog_W_26<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),spdl12 = SpDLog_f_4_171_W_12<T>(kin),spdl10 = SpDLog_f_4_171_W_10<T>(kin),spdl23 = SpDLog_f_4_171_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,46> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), f_2_1_4(kin_path), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl10(t), rlog(-v_path[4] + v_path[1] + v_path[2]), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl3(t), f_1_3_4(kin) - f_1_3_4(kin_path), f_2_1_7(kin_path), dl9(t), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), dl23(t), f_2_1_8(kin_path), dl14(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl19(t), dl1(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl31(t), dl18(t), dl20(t), dl2(t), dl17(t), dl5(t), dl4(t), dl16(t), dl27(t) / kin_path.SqrtDelta, rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[30] / kin_path.W[30]), dl26(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta}
;

        auto result = f_4_171_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_171_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[110];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[10];
z[3] = abb[21];
z[4] = abb[22];
z[5] = abb[28];
z[6] = abb[29];
z[7] = abb[30];
z[8] = abb[31];
z[9] = abb[32];
z[10] = abb[5];
z[11] = abb[18];
z[12] = abb[8];
z[13] = abb[9];
z[14] = abb[11];
z[15] = abb[34];
z[16] = abb[42];
z[17] = abb[43];
z[18] = abb[45];
z[19] = abb[35];
z[20] = abb[36];
z[21] = abb[37];
z[22] = abb[38];
z[23] = abb[39];
z[24] = abb[40];
z[25] = abb[41];
z[26] = abb[2];
z[27] = abb[7];
z[28] = abb[14];
z[29] = abb[15];
z[30] = bc<TR>[0];
z[31] = abb[26];
z[32] = abb[33];
z[33] = abb[3];
z[34] = abb[27];
z[35] = abb[12];
z[36] = abb[13];
z[37] = abb[17];
z[38] = abb[19];
z[39] = abb[20];
z[40] = abb[23];
z[41] = abb[24];
z[42] = abb[44];
z[43] = abb[25];
z[44] = bc<TR>[3];
z[45] = bc<TR>[1];
z[46] = bc<TR>[2];
z[47] = bc<TR>[4];
z[48] = bc<TR>[9];
z[49] = z[12] / T(2);
z[50] = z[14] / T(2);
z[51] = z[49] + z[50];
z[52] = (T(3) * z[1]) / T(2);
z[53] = -z[10] + z[52];
z[54] = (T(3) * z[13]) / T(2);
z[55] = z[51] + z[53] + z[54];
z[56] = z[7] * z[55];
z[57] = z[3] * z[55];
z[58] = -z[10] + z[13];
z[59] = z[11] * z[58];
z[60] = -z[57] + z[59];
z[61] = z[13] / T(2) + -z[51];
z[53] = z[53] + z[61];
z[53] = z[5] * z[53];
z[62] = T(3) * z[1];
z[63] = z[13] + z[14];
z[64] = -z[12] + z[63];
z[65] = z[62] + -z[64];
z[66] = z[9] / T(2);
z[67] = z[65] * z[66];
z[54] = -z[51] + z[54];
z[52] = T(2) * z[10] + -z[52] + -z[54];
z[68] = z[8] * z[52];
z[69] = z[4] * z[65];
z[70] = z[69] / T(2);
z[53] = z[53] + -z[56] + -z[60] + z[67] + z[68] + -z[70];
z[56] = -(z[39] * z[53]);
z[67] = z[7] + z[8];
z[58] = z[1] + z[58];
z[68] = T(2) * z[58];
z[67] = z[67] * z[68];
z[68] = z[1] / T(2) + -z[10];
z[61] = z[61] + z[68];
z[71] = z[5] * z[61];
z[72] = z[1] + z[12];
z[73] = z[63] + -z[72];
z[74] = z[73] / T(2);
z[75] = z[6] * z[74];
z[76] = z[9] * z[74];
z[67] = z[57] + -z[67] + z[71] + z[75] + -z[76];
z[71] = z[26] * z[67];
z[62] = z[62] + z[64];
z[62] = z[9] * z[62];
z[65] = z[5] * z[65];
z[77] = T(3) * z[13];
z[78] = z[72] + z[77];
z[79] = T(3) * z[14] + z[78];
z[80] = z[34] * z[79];
z[62] = z[62] + z[65] + -z[69] + -z[80];
z[65] = -(z[6] * z[79]);
z[65] = -z[62] + z[65];
z[81] = z[41] / T(2);
z[65] = z[65] * z[81];
z[82] = z[8] * z[74];
z[83] = z[7] * z[74];
z[84] = z[82] + z[83];
z[85] = z[1] * z[5];
z[70] = -z[70] + -z[75] + z[85];
z[75] = z[1] * z[9];
z[86] = z[70] + T(2) * z[75] + -z[84];
z[87] = z[29] * z[86];
z[76] = z[76] + z[83];
z[74] = z[5] * z[74];
z[83] = T(2) * z[63];
z[88] = z[6] * z[83];
z[74] = z[74] + -z[76] + z[80] / T(2) + -z[88];
z[74] = z[27] * z[74];
z[88] = z[1] + z[63];
z[88] = z[36] * z[88];
z[89] = z[7] * z[58];
z[90] = -z[59] + z[88] + z[89];
z[91] = z[6] * z[63];
z[85] = z[75] + z[85] + -z[90] + z[91];
z[85] = z[28] * z[85];
z[91] = T(3) * z[16];
z[92] = z[18] + z[42];
z[93] = z[32] + z[91] + -z[92];
z[94] = prod_pow(z[45], 2);
z[94] = -z[47] + -z[94] / T(2);
z[94] = z[93] * z[94];
z[81] = -(z[79] * z[81]);
z[83] = z[27] * z[83];
z[81] = z[81] + z[83];
z[81] = z[8] * z[81];
z[56] = z[56] + z[65] + z[71] + -z[74] + z[81] + T(2) * z[85] + z[87] + z[94];
z[56] = int_to_imaginary<T>(1) * z[56];
z[65] = z[63] + z[72] / T(3);
z[65] = z[34] * z[65];
z[71] = z[63] / T(3);
z[81] = -z[71] + -z[72];
z[81] = z[31] * z[81];
z[85] = -z[14] + z[72];
z[94] = z[13] + -z[85] / T(3);
z[94] = z[2] * z[94];
z[95] = (T(7) * z[13]) / T(3) + (T(11) * z[14]) / T(3) + -z[72];
z[95] = z[6] * z[95];
z[65] = z[65] + z[81] + z[94] + z[95];
z[81] = z[1] + -z[12] + z[14];
z[81] = z[5] * z[81];
z[65] = (T(7) * z[59]) / T(3) + z[65] / T(2) + z[81] / T(3);
z[81] = z[6] + z[8];
z[94] = z[7] + z[9];
z[95] = z[81] + z[94];
z[96] = z[31] + z[34];
z[97] = z[32] + z[42];
z[97] = z[16] + z[97] / T(6);
z[97] = (T(2) * z[5]) / T(27) + z[17] / T(12) + (T(-4) * z[43]) / T(27) + z[95] / T(9) + -z[96] / T(27) + z[97] / T(2);
z[97] = int_to_imaginary<T>(1) * z[30] * z[97];
z[98] = T(-5) * z[1] + T(7) * z[10];
z[50] = z[12] + z[50] + z[98] / T(2);
z[50] = -z[13] + z[50] / T(3);
z[50] = z[7] * z[50];
z[49] = z[1] / T(3) + z[49] + -z[63] / T(6);
z[49] = z[9] * z[49];
z[98] = -z[63] + -z[72] / T(2);
z[98] = z[8] * z[98];
z[99] = z[18] / T(3);
z[100] = z[32] + -z[42];
z[101] = -z[100] / T(3);
z[102] = z[16] + -z[99] + -z[101];
z[102] = z[45] * z[102];
z[71] = z[1] + z[12] / T(3) + -z[71];
z[71] = z[4] * z[71];
z[49] = z[49] + z[50] + z[65] / T(2) + z[71] + -z[88] + z[97] + z[98] / T(3) + z[102];
z[49] = z[30] * z[49];
z[50] = z[7] * z[61];
z[61] = z[63] + z[72];
z[65] = z[8] / T(2);
z[61] = z[61] * z[65];
z[50] = z[50] + -z[59] + z[61] + z[70] + z[75];
z[59] = int_to_imaginary<T>(1) * z[0];
z[50] = z[50] * z[59];
z[61] = T(-2) * z[5] + T(4) * z[43] + T(-3) * z[95] + z[96];
z[61] = z[44] * z[61];
z[70] = z[45] * z[93];
z[71] = z[17] + -z[18];
z[75] = z[46] * z[71];
z[70] = z[70] + -z[75] / T(2);
z[70] = int_to_imaginary<T>(1) * z[70];
z[75] = -z[16] + (T(-5) * z[17]) / T(4) + z[18] / T(4) + -z[42];
z[75] = z[30] * z[75];
z[70] = z[70] + z[75];
z[70] = z[46] * z[70];
z[49] = z[49] + z[50] + z[56] + z[61] + z[70];
z[49] = z[30] * z[49];
z[50] = z[63] + T(3) * z[72];
z[56] = z[50] * z[94];
z[61] = z[31] * z[50];
z[70] = -z[77] + z[85];
z[75] = z[2] * z[70];
z[77] = -z[14] + z[78];
z[77] = z[6] * z[77];
z[78] = -(z[5] * z[70]);
z[56] = z[56] + -z[61] + z[75] + z[77] + z[78];
z[56] = z[35] * z[56];
z[77] = z[38] * z[71];
z[78] = -z[16] + z[17];
z[78] = z[33] * z[78];
z[93] = z[16] + z[18];
z[95] = z[42] + z[93];
z[96] = z[40] * z[95];
z[97] = z[32] + z[93];
z[98] = z[35] * z[97];
z[77] = z[77] + z[78] + -z[96] + -z[98];
z[78] = z[16] * z[29];
z[96] = -z[17] + z[93];
z[98] = z[26] * z[96];
z[78] = z[78] + -z[98];
z[98] = z[18] * z[27];
z[96] = z[28] * z[96];
z[96] = -z[96] + z[98];
z[98] = z[78] + z[96];
z[102] = -z[17] + z[93] / T(2);
z[102] = z[0] * z[102];
z[102] = z[98] + z[102];
z[102] = z[0] * z[102];
z[103] = z[16] + z[32];
z[104] = -z[92] + z[103];
z[105] = z[104] / T(2);
z[106] = z[29] * z[105];
z[107] = z[16] * z[28];
z[92] = z[27] * z[92];
z[107] = -z[92] + z[107];
z[106] = z[106] + -z[107];
z[106] = z[29] * z[106];
z[103] = z[29] * z[103];
z[96] = z[96] + z[103];
z[97] = z[26] * z[97];
z[103] = -z[96] + z[97] / T(2);
z[103] = z[26] * z[103];
z[108] = z[28] * z[95];
z[109] = -z[92] + z[108] / T(2);
z[109] = z[28] * z[109];
z[102] = z[77] + -z[102] + -z[103] + -z[106] + -z[109];
z[71] = z[39] * z[71];
z[95] = z[41] * z[95];
z[71] = z[71] + -z[78] + -z[92] + z[95];
z[71] = int_to_imaginary<T>(1) * z[71];
z[59] = z[16] * z[59];
z[59] = -z[59] + z[71];
z[71] = z[91] + -z[100];
z[71] = z[18] + z[71] / T(2);
z[71] = z[30] * z[71];
z[71] = T(3) * z[59] + z[71];
z[71] = z[30] * z[71];
z[71] = z[71] + T(3) * z[102];
z[78] = z[37] * z[104];
z[91] = -z[71] + T(3) * z[78];
z[91] = z[21] * z[91];
z[81] = z[79] * z[81];
z[62] = z[62] + z[81];
z[62] = z[40] * z[62];
z[56] = z[56] + z[62] + z[91];
z[53] = -(z[38] * z[53]);
z[62] = -(z[28] * z[67]);
z[81] = z[6] * z[13];
z[91] = z[5] * z[13];
z[76] = z[75] / T(2) + z[76] + T(2) * z[81] + z[82] + z[91];
z[76] = z[27] * z[76];
z[81] = z[6] * z[73];
z[95] = z[61] + z[81];
z[100] = z[9] * z[73];
z[50] = z[7] * z[50];
z[85] = -z[13] + -z[85];
z[85] = z[5] * z[85];
z[50] = z[50] + z[75] + z[85] + -z[95] + z[100];
z[85] = z[8] * z[58];
z[50] = z[50] / T(4) + z[85];
z[50] = z[26] * z[50];
z[73] = z[5] * z[73];
z[103] = -z[73] + z[95];
z[106] = T(-2) * z[94];
z[106] = z[72] * z[106];
z[82] = z[82] + z[103] / T(2) + z[106];
z[82] = z[29] * z[82];
z[50] = z[50] + z[62] + -z[76] + z[82];
z[50] = z[26] * z[50];
z[62] = -z[81] + z[100];
z[62] = -z[62] / T(4);
z[69] = z[69] / T(4);
z[51] = z[51] + -z[58];
z[51] = z[5] * z[51];
z[51] = z[51] + z[60] + -z[62] + z[69] + -z[75] / T(4) + z[85] + z[89];
z[51] = z[0] * z[51];
z[58] = z[26] + z[28];
z[58] = z[58] * z[67];
z[51] = z[51] + z[58] + z[76] + z[87];
z[51] = z[0] * z[51];
z[58] = T(5) * z[63] + -z[72];
z[60] = -(z[6] * z[58]);
z[67] = -z[63] + T(5) * z[72];
z[76] = z[7] * z[67];
z[60] = z[60] + -z[61] + z[76] + z[80];
z[61] = z[23] + z[24];
z[76] = -(z[61] * z[105]);
z[66] = z[66] * z[67];
z[67] = z[15] + z[19] + z[20];
z[81] = z[22] + (T(3) * z[67]) / T(2);
z[81] = z[81] * z[104];
z[58] = -(z[58] * z[65]);
z[58] = z[58] + z[60] / T(2) + z[66] + z[73] + z[76] + z[81];
z[58] = z[37] * z[58];
z[60] = z[77] + -z[78];
z[65] = z[16] + z[101];
z[73] = (T(2) * z[18]) / T(3) + z[65];
z[73] = z[30] * z[73];
z[73] = T(2) * z[59] + z[73];
z[73] = z[30] * z[73];
z[76] = -(z[29] * z[104]);
z[76] = z[76] + T(2) * z[107];
z[76] = z[29] * z[76];
z[77] = T(2) * z[17] + -z[93];
z[77] = z[0] * z[77];
z[77] = z[77] + T(-2) * z[98];
z[77] = z[0] * z[77];
z[78] = T(2) * z[92] + -z[108];
z[78] = z[28] * z[78];
z[81] = T(2) * z[96] + -z[97];
z[81] = z[26] * z[81];
z[60] = T(2) * z[60] + z[73] + z[76] + z[77] + z[78] + z[81];
z[60] = z[25] * z[60];
z[67] = -z[67] / T(2);
z[67] = z[67] * z[71];
z[65] = z[65] / T(2) + z[99];
z[65] = z[30] * z[65];
z[59] = z[59] + z[65];
z[59] = z[30] * z[59];
z[59] = z[59] + z[102];
z[61] = -z[22] + z[61] / T(2);
z[59] = z[59] * z[61];
z[61] = z[8] * z[83];
z[61] = z[61] + -z[74];
z[65] = -(z[28] * z[86]);
z[71] = z[80] + -z[95];
z[64] = z[1] + z[64];
z[64] = z[5] * z[64];
z[66] = z[64] + z[66] + z[71] / T(2) + -z[84];
z[66] = z[66] / T(2) + -z[88];
z[66] = z[29] * z[66];
z[65] = z[61] + z[65] + z[66];
z[65] = z[29] * z[65];
z[66] = z[6] * z[70];
z[66] = z[66] + -z[75];
z[55] = z[8] * z[55];
z[54] = -z[54] + -z[68];
z[54] = z[5] * z[54];
z[52] = -(z[7] * z[52]);
z[52] = z[52] + z[54] + z[55] + -z[57] + z[66] / T(2);
z[52] = z[33] * z[52];
z[54] = z[8] * z[79];
z[54] = -z[54] + z[64] + z[80];
z[54] = -z[54] / T(4) + -z[62] + -z[69] + z[90];
z[54] = z[28] * z[54];
z[54] = z[54] + -z[61];
z[54] = z[28] * z[54];
z[55] = z[72] * z[94];
z[57] = -(z[8] * z[63]);
z[55] = z[55] + z[57] + z[91];
z[55] = prod_pow(z[27], 2) * z[55];
z[57] = z[16] / T(2) + z[17] / T(8) + (T(61) * z[18]) / T(24) + (T(-19) * z[32]) / T(6) + (T(8) * z[42]) / T(3);
z[57] = z[48] * z[57];
return z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] / T(2) + z[57] + z[58] + z[59] + z[60] + z[65] + z[67];
}



template IntegrandConstructorType<double> f_4_171_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_171_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_171_construct (const Kin<qd_real>&);
#endif

}