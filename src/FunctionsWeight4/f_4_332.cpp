#include "f_4_332.h"

namespace PentagonFunctions {

template <typename T> T f_4_332_abbreviated (const std::array<T,55>&);

template <typename T> class SpDLog_f_4_332_W_7 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_332_W_7 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (T(6) + T(6) * kin.v[1] + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + T(6) * kin.v[1] * kin.v[4] + kin.v[4] * (T(6) + T(-3) * kin.v[4]);
c[1] = kin.v[3] * (T(6) + T(6) * kin.v[1] + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + T(6) * kin.v[1] * kin.v[4] + kin.v[4] * (T(6) + T(-3) * kin.v[4]);
c[2] = kin.v[3] * (T(6) + T(6) * kin.v[1] + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + T(6) * kin.v[1] * kin.v[4] + kin.v[4] * (T(6) + T(-3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1] + abb[5] * c[2]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-3) + prod_pow(abb[2], 2) * (abb[4] * T(-3) + abb[5] * T(-3)) + prod_pow(abb[1], 2) * (abb[3] * T(3) + abb[4] * T(3) + abb[5] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_332_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_332_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]);
c[1] = kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]);
c[2] = kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(-6) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(3) * kin.v[4]);
c[3] = kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(-6) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[5] * c[0] + abb[10] * c[1] + abb[9] * c[2] + abb[3] * c[3]);
        }

        return abb[6] * (prod_pow(abb[8], 2) * abb[9] * T(3) + prod_pow(abb[8], 2) * (abb[5] * T(-3) + abb[10] * T(-3) + abb[3] * T(3)) + prod_pow(abb[7], 2) * (abb[3] * T(-3) + abb[9] * T(-3) + abb[5] * T(3) + abb[10] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_332_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_332_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + kin.v[0] + T(-2) * kin.v[2]) + -(kin.v[0] * kin.v[4]) + T(2) * kin.v[2] * kin.v[4] + kin.v[3] * kin.v[4] + prod_pow(kin.v[4], 2) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4]);
c[1] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4];
c[2] = kin.v[0] * kin.v[4] + T(-2) * kin.v[2] * kin.v[4] + -(kin.v[3] * kin.v[4]) + -prod_pow(kin.v[4], 2) / T(2) + kin.v[1] * (-kin.v[1] / T(2) + -kin.v[0] + T(2) * kin.v[2] + kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[1] / T(2) + T(-1)) * kin.v[1] + (kin.v[4] / T(2) + T(1)) * kin.v[4]);
c[3] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[9] * (t * c[0] + c[1]) + abb[4] * (t * c[2] + c[3]);
        }

        return abb[11] * (abb[4] * abb[12] * abb[13] + abb[13] * (abb[2] * abb[4] + -(abb[4] * abb[13]) + -(abb[4] * abb[14]) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-1)) + abb[9] * (-(abb[12] * abb[13]) + abb[15] * T(-2) + abb[13] * (abb[13] + abb[14] + -abb[2] + bc<T>[0] * int_to_imaginary<T>(1))) + abb[4] * abb[15] * T(2) + abb[1] * (abb[4] * abb[14] + -(abb[2] * abb[4]) + -(abb[4] * abb[12]) + -(abb[4] * abb[13]) + abb[9] * (abb[2] + abb[12] + abb[13] + -abb[14] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[4] * bc<T>[0] * int_to_imaginary<T>(1) + abb[1] * (abb[9] * T(-2) + abb[4] * T(2))));
    }
};
template <typename T> class SpDLog_f_4_332_W_21 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_332_W_21 (const Kin<T>& kin) {
        c[0] = ((T(-11) * kin.v[2]) / T(4) + (T(-13) * kin.v[3]) / T(2) + -kin.v[4] + T(1)) * kin.v[2] + ((T(-15) * kin.v[3]) / T(4) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * ((T(-15) * kin.v[1]) / T(4) + (T(13) * kin.v[2]) / T(2) + (T(15) * kin.v[3]) / T(2) + T(-1) + kin.v[4]) + kin.v[0] * ((T(-13) * kin.v[1]) / T(2) + (T(11) * kin.v[2]) / T(2) + (T(13) * kin.v[3]) / T(2) + T(-1) + (T(-11) / T(4) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[0] + kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(-4) * kin.v[2] + T(4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[3] * (T(4) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(-4) + T(-2) * kin.v[1] + T(4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = (T(-3) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(-3) * kin.v[1] + T(3) * kin.v[2] + T(3) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3]);
c[2] = ((T(-11) * kin.v[2]) / T(4) + (T(-13) * kin.v[3]) / T(2) + -kin.v[4] + T(1)) * kin.v[2] + ((T(-15) * kin.v[3]) / T(4) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * ((T(-15) * kin.v[1]) / T(4) + (T(13) * kin.v[2]) / T(2) + (T(15) * kin.v[3]) / T(2) + T(-1) + kin.v[4]) + kin.v[0] * ((T(-13) * kin.v[1]) / T(2) + (T(11) * kin.v[2]) / T(2) + (T(13) * kin.v[3]) / T(2) + T(-1) + (T(-11) / T(4) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[0] + kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(-4) * kin.v[2] + T(4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[3] * (T(4) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (T(-4) + T(-2) * kin.v[1] + T(4) * kin.v[3] + T(4) * kin.v[4]));
c[3] = (T(-3) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(-3) * kin.v[1] + T(3) * kin.v[2] + T(3) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3]);
c[4] = ((T(15) * kin.v[1]) / T(4) + (T(-13) * kin.v[2]) / T(2) + (T(-15) * kin.v[3]) / T(2) + -kin.v[4] + T(1)) * kin.v[1] + kin.v[0] * ((T(13) * kin.v[1]) / T(2) + (T(-11) * kin.v[2]) / T(2) + (T(-13) * kin.v[3]) / T(2) + -kin.v[4] + T(1) + (T(11) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(4) * kin.v[2] + T(-4) * kin.v[4])) + kin.v[3] * ((T(15) * kin.v[3]) / T(4) + T(-1) + kin.v[4]) + kin.v[2] * ((T(11) * kin.v[2]) / T(4) + (T(13) * kin.v[3]) / T(2) + T(-1) + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(4) + T(2) * kin.v[1] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[3] * (T(-4) + T(2) * kin.v[3] + T(4) * kin.v[4]));
c[5] = (bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[0] + T(3) * kin.v[1] + T(-3) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3]) + T(-3) * kin.v[3];
c[6] = ((T(15) * kin.v[1]) / T(4) + (T(-13) * kin.v[2]) / T(2) + (T(-15) * kin.v[3]) / T(2) + -kin.v[4] + T(1)) * kin.v[1] + kin.v[0] * ((T(13) * kin.v[1]) / T(2) + (T(-11) * kin.v[2]) / T(2) + (T(-13) * kin.v[3]) / T(2) + -kin.v[4] + T(1) + (T(11) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(4) * kin.v[2] + T(-4) * kin.v[4])) + kin.v[3] * ((T(15) * kin.v[3]) / T(4) + T(-1) + kin.v[4]) + kin.v[2] * ((T(11) * kin.v[2]) / T(4) + (T(13) * kin.v[3]) / T(2) + T(-1) + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(4) + T(2) * kin.v[1] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[3] * (T(-4) + T(2) * kin.v[3] + T(4) * kin.v[4]));
c[7] = (bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[0] + T(3) * kin.v[1] + T(-3) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] + T(-4) * kin.v[2] + T(-4) * kin.v[3]) + T(-3) * kin.v[3];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[3] * (t * c[0] + c[1]) + abb[4] * (t * c[2] + c[3]) + abb[5] * (t * c[4] + c[5]) + abb[10] * (t * c[6] + c[7]);
        }

        return abb[16] * (abb[14] * (abb[14] * (abb[4] / T(2) + -abb[5] / T(2) + -abb[10] / T(2)) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[5] * bc<T>[0] * int_to_imaginary<T>(4) + abb[10] * bc<T>[0] * int_to_imaginary<T>(4)) + abb[17] * (abb[5] * T(-3) + abb[10] * T(-3) + abb[4] * T(3)) + abb[3] * (abb[7] * abb[14] * T(-4) + abb[14] * (abb[14] / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) + abb[17] * T(3)) + abb[7] * abb[14] * (abb[4] * T(-4) + abb[5] * T(4) + abb[10] * T(4)) + abb[2] * (abb[2] * ((abb[5] * T(-5)) / T(2) + (abb[10] * T(-5)) / T(2) + (abb[3] * T(5)) / T(2) + (abb[4] * T(5)) / T(2)) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[4] * bc<T>[0] * int_to_imaginary<T>(4) + abb[14] * (abb[4] * T(-3) + abb[5] * T(3) + abb[10] * T(3)) + abb[7] * (abb[5] * T(-4) + abb[10] * T(-4) + abb[4] * T(4)) + abb[3] * (abb[14] * T(-3) + bc<T>[0] * int_to_imaginary<T>(4) + abb[7] * T(4))) + abb[12] * (abb[3] * abb[14] * T(4) + abb[14] * (abb[5] * T(-4) + abb[10] * T(-4) + abb[4] * T(4)) + abb[2] * (abb[3] * T(-4) + abb[4] * T(-4) + abb[5] * T(4) + abb[10] * T(4))));
    }
};
template <typename T> class SpDLog_f_4_332_W_10 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_332_W_10 (const Kin<T>& kin) {
        c[0] = ((T(3) * kin.v[2]) / T(2) + -kin.v[3]) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[3] + T(2) * kin.v[2]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-1) + kin.v[2]));
c[1] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[2];
c[2] = ((T(3) * kin.v[2]) / T(2) + -kin.v[3]) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[3] + T(2) * kin.v[2]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-1) + kin.v[2]));
c[3] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[2];
c[4] = ((T(3) * kin.v[2]) / T(2) + -kin.v[3]) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[3] + T(2) * kin.v[2]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2] + kin.v[1] * (kin.v[1] / T(2) + -kin.v[4] + T(-1) + kin.v[2]));
c[5] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[2];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[9] * (t * c[0] + c[1]) + abb[3] * (t * c[2] + c[3]) + abb[10] * (t * c[4] + c[5]);
        }

        return abb[23] * (abb[7] * abb[8] * (abb[3] + abb[10]) + abb[12] * (abb[8] * abb[9] + abb[8] * (abb[3] + abb[10]) + abb[13] * (-abb[3] + -abb[9] + -abb[10])) + abb[8] * (abb[8] * (-abb[3] + -abb[10]) + abb[14] * (-abb[3] + -abb[10]) + abb[3] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-1)) + abb[24] * (abb[3] * T(2) + abb[10] * T(2)) + abb[9] * (abb[7] * abb[8] + abb[8] * (-abb[8] + -abb[14] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[24] * T(2)) + abb[13] * ((abb[3] + abb[10]) * abb[14] + abb[7] * (-abb[3] + -abb[10]) + abb[8] * (-abb[3] + -abb[10]) + abb[3] * bc<T>[0] * int_to_imaginary<T>(1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(1) + abb[9] * (abb[14] + -abb[7] + -abb[8] + bc<T>[0] * int_to_imaginary<T>(1)) + abb[13] * (abb[3] * T(2) + abb[9] * T(2) + abb[10] * T(2))));
    }
};
template <typename T> class SpDLog_f_4_332_W_22 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_332_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(13) * kin.v[2]) / T(2) + (T(-15) * kin.v[3]) / T(2) + (T(-13) * kin.v[4]) / T(2) + T(-1) + kin.v[0] + (T(15) / T(4) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(4) * kin.v[0] + T(-4) * kin.v[3])) + ((T(11) * kin.v[2]) / T(4) + (T(-13) * kin.v[3]) / T(2) + (T(-11) * kin.v[4]) / T(2) + T(-1)) * kin.v[2] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + ((T(11) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + ((T(15) * kin.v[3]) / T(4) + (T(13) * kin.v[4]) / T(2) + T(1)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(-2) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]));
c[1] = (T(-3) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]);
c[2] = ((T(-15) * kin.v[3]) / T(4) + (T(-13) * kin.v[4]) / T(2) + T(-1)) * kin.v[3] + ((T(-11) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]) + ((T(-11) * kin.v[2]) / T(4) + (T(13) * kin.v[3]) / T(2) + (T(11) * kin.v[4]) / T(2) + T(1)) * kin.v[2] + kin.v[1] * ((T(-13) * kin.v[2]) / T(2) + (T(15) * kin.v[3]) / T(2) + (T(13) * kin.v[4]) / T(2) + -kin.v[0] + T(1) + (T(-15) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-4) * kin.v[0] + T(4) * kin.v[3])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]));
c[3] = (bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[1] + T(3) * kin.v[2] + T(-3) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + T(-3) * kin.v[4];
c[4] = ((T(-15) * kin.v[3]) / T(4) + (T(-13) * kin.v[4]) / T(2) + T(-1)) * kin.v[3] + ((T(-11) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]) + ((T(-11) * kin.v[2]) / T(4) + (T(13) * kin.v[3]) / T(2) + (T(11) * kin.v[4]) / T(2) + T(1)) * kin.v[2] + kin.v[1] * ((T(-13) * kin.v[2]) / T(2) + (T(15) * kin.v[3]) / T(2) + (T(13) * kin.v[4]) / T(2) + -kin.v[0] + T(1) + (T(-15) / T(4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-4) * kin.v[0] + T(4) * kin.v[3])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]));
c[5] = (bc<T>[0] * int_to_imaginary<T>(4) + T(3)) * kin.v[1] + T(3) * kin.v[2] + T(-3) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + T(-3) * kin.v[4];
c[6] = kin.v[1] * ((T(13) * kin.v[2]) / T(2) + (T(-15) * kin.v[3]) / T(2) + (T(-13) * kin.v[4]) / T(2) + T(-1) + kin.v[0] + (T(15) / T(4) + bc<T>[0] * int_to_imaginary<T>(2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(4) * kin.v[0] + T(-4) * kin.v[3])) + ((T(11) * kin.v[2]) / T(4) + (T(-13) * kin.v[3]) / T(2) + (T(-11) * kin.v[4]) / T(2) + T(-1)) * kin.v[2] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + ((T(11) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + ((T(15) * kin.v[3]) / T(4) + (T(13) * kin.v[4]) / T(2) + T(1)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(-2) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]));
c[7] = (T(-3) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[9] * (t * c[0] + c[1]) + abb[4] * (t * c[2] + c[3]) + abb[5] * (t * c[4] + c[5]) + abb[10] * (t * c[6] + c[7]);
        }

        return abb[40] * (abb[14] * (abb[14] * (abb[4] / T(2) + abb[5] / T(2) + -abb[10] / T(2)) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[10] * bc<T>[0] * int_to_imaginary<T>(4)) + abb[30] * (abb[10] * T(-3) + abb[4] * T(3) + abb[5] * T(3)) + abb[2] * abb[14] * (abb[4] * T(-4) + abb[5] * T(-4) + abb[10] * T(4)) + abb[9] * (abb[30] * T(-3) + abb[14] * (-abb[14] / T(2) + bc<T>[0] * int_to_imaginary<T>(4)) + abb[2] * abb[14] * T(4)) + abb[7] * (abb[7] * ((abb[9] * T(-5)) / T(2) + (abb[10] * T(-5)) / T(2) + (abb[4] * T(5)) / T(2) + (abb[5] * T(5)) / T(2)) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[4] * bc<T>[0] * int_to_imaginary<T>(4) + abb[5] * bc<T>[0] * int_to_imaginary<T>(4) + abb[14] * (abb[4] * T(-3) + abb[5] * T(-3) + abb[10] * T(3)) + abb[9] * (abb[2] * T(-4) + bc<T>[0] * int_to_imaginary<T>(-4) + abb[14] * T(3)) + abb[2] * (abb[10] * T(-4) + abb[4] * T(4) + abb[5] * T(4))) + abb[12] * (abb[9] * abb[14] * T(-4) + abb[14] * (abb[10] * T(-4) + abb[4] * T(4) + abb[5] * T(4)) + abb[7] * (abb[4] * T(-4) + abb[5] * T(-4) + abb[9] * T(4) + abb[10] * T(4))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_332_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl23 = DLog_W_23<T>(kin),dl12 = DLog_W_12<T>(kin),dl21 = DLog_W_21<T>(kin),dl1 = DLog_W_1<T>(kin),dl10 = DLog_W_10<T>(kin),dl29 = DLog_W_29<T>(kin),dl22 = DLog_W_22<T>(kin),dl19 = DLog_W_19<T>(kin),dl16 = DLog_W_16<T>(kin),dl17 = DLog_W_17<T>(kin),dl2 = DLog_W_2<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl18 = DLog_W_18<T>(kin),dl4 = DLog_W_4<T>(kin),dl3 = DLog_W_3<T>(kin),dl31 = DLog_W_31<T>(kin),dl26 = DLog_W_26<T>(kin),dl27 = DLog_W_27<T>(kin),dl30 = DLog_W_30<T>(kin),dl28 = DLog_W_28<T>(kin),spdl7 = SpDLog_f_4_332_W_7<T>(kin),spdl23 = SpDLog_f_4_332_W_23<T>(kin),spdl12 = SpDLog_f_4_332_W_12<T>(kin),spdl21 = SpDLog_f_4_332_W_21<T>(kin),spdl10 = SpDLog_f_4_332_W_10<T>(kin),spdl22 = SpDLog_f_4_332_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,55> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl12(t), rlog(v_path[2]), rlog(-v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_4(kin_path), dl21(t), f_2_1_15(kin_path), dl1(t), f_2_1_5(kin_path), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), rlog(-v_path[4] + v_path[2]), dl10(t), f_2_1_7(kin_path), dl29(t) / kin_path.SqrtDelta, f_2_2_7(kin_path), rlog(kin.W[1] / kin_path.W[1]), f_2_1_8(kin_path), f_2_1_11(kin_path), f_2_1_14(kin_path), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[3] / kin_path.W[3]), dl22(t), dl19(t), dl16(t), dl17(t), dl2(t), dl5(t), dl20(t), dl18(t), dl4(t), dl3(t), dl31(t), dl26(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta}
;

        auto result = f_4_332_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr) + spdl21(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_332_abbreviated(const std::array<T,55>& abb)
{
using TR = typename T::value_type;
T z[211];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[41];
z[3] = abb[43];
z[4] = abb[44];
z[5] = abb[45];
z[6] = abb[46];
z[7] = abb[47];
z[8] = abb[48];
z[9] = abb[49];
z[10] = abb[4];
z[11] = abb[5];
z[12] = abb[9];
z[13] = abb[25];
z[14] = abb[27];
z[15] = abb[31];
z[16] = abb[32];
z[17] = abb[33];
z[18] = abb[35];
z[19] = abb[36];
z[20] = abb[37];
z[21] = abb[38];
z[22] = abb[39];
z[23] = abb[51];
z[24] = abb[52];
z[25] = abb[54];
z[26] = abb[2];
z[27] = abb[8];
z[28] = abb[12];
z[29] = abb[13];
z[30] = abb[14];
z[31] = bc<TR>[0];
z[32] = abb[42];
z[33] = abb[10];
z[34] = abb[53];
z[35] = abb[7];
z[36] = abb[18];
z[37] = abb[15];
z[38] = abb[17];
z[39] = abb[19];
z[40] = abb[20];
z[41] = abb[21];
z[42] = abb[22];
z[43] = abb[24];
z[44] = abb[26];
z[45] = abb[28];
z[46] = abb[29];
z[47] = abb[30];
z[48] = abb[34];
z[49] = abb[50];
z[50] = bc<TR>[3];
z[51] = bc<TR>[1];
z[52] = bc<TR>[5];
z[53] = bc<TR>[9];
z[54] = bc<TR>[2];
z[55] = bc<TR>[4];
z[56] = T(7) * z[33];
z[57] = z[1] + T(7) * z[12] + z[56];
z[58] = T(3) * z[10];
z[59] = T(2) * z[11];
z[57] = z[57] / T(2) + -z[58] + -z[59];
z[57] = z[35] * z[57];
z[60] = (T(3) * z[12]) / T(2);
z[61] = z[10] / T(2);
z[62] = z[60] + z[61];
z[63] = T(2) * z[1];
z[64] = -z[59] + z[63];
z[65] = z[62] + z[64];
z[66] = z[0] * z[65];
z[67] = z[1] + z[12];
z[68] = z[33] + z[67];
z[69] = z[68] / T(2);
z[70] = -z[59] + z[69];
z[71] = z[27] * z[70];
z[72] = z[66] + -z[71];
z[57] = z[57] + z[72];
z[73] = (T(7) * z[10]) / T(2);
z[74] = -z[60] + z[73];
z[75] = T(3) * z[33];
z[76] = z[64] + z[74] + -z[75];
z[77] = -(z[26] * z[76]);
z[78] = z[1] + z[10];
z[79] = z[42] * z[78];
z[80] = z[33] * z[42];
z[81] = z[79] + -z[80];
z[82] = z[12] + z[33];
z[83] = -z[10] + z[82];
z[84] = z[28] * z[83];
z[85] = T(2) * z[84];
z[86] = z[30] * z[83];
z[87] = -z[33] + z[78];
z[88] = (T(3) * z[87]) / T(2);
z[89] = z[41] * z[88];
z[90] = z[29] * z[83];
z[91] = prod_pow(z[31], 2);
z[77] = z[57] + z[77] + (T(-3) * z[81]) / T(2) + -z[85] + T(-5) * z[86] + z[89] + -z[90] + (T(4) * z[91]) / T(9);
z[77] = z[6] * z[77];
z[92] = z[10] + -z[12];
z[93] = z[11] + z[92];
z[94] = T(2) * z[35];
z[95] = z[93] * z[94];
z[96] = -z[71] + z[95];
z[97] = z[29] * z[70];
z[98] = T(2) * z[28];
z[98] = z[93] * z[98];
z[99] = z[33] + z[78];
z[100] = (T(3) * z[41]) / T(2);
z[100] = z[99] * z[100];
z[82] = -z[1] + z[82];
z[82] = z[82] / T(2);
z[101] = z[10] + z[59];
z[102] = z[82] + -z[101];
z[103] = -(z[30] * z[102]);
z[62] = z[62] + z[63];
z[104] = z[33] + z[62];
z[105] = -(z[26] * z[104]);
z[103] = z[91] / T(9) + -z[96] + -z[97] + -z[98] + z[100] + z[103] + z[105];
z[103] = z[32] * z[103];
z[105] = z[33] / T(2);
z[106] = -z[61] + z[105];
z[107] = z[1] / T(2);
z[108] = z[12] + z[106] + z[107];
z[109] = z[42] * z[108];
z[109] = T(3) * z[109];
z[110] = T(2) * z[0];
z[111] = z[93] * z[110];
z[112] = z[11] + z[33];
z[113] = T(2) * z[27];
z[114] = z[112] * z[113];
z[115] = prod_pow(z[51], 2);
z[95] = -z[89] + -z[95] + -z[109] + -z[111] + z[114] + (T(-4) * z[115]) / T(5);
z[95] = z[9] * z[95];
z[116] = z[35] * z[70];
z[72] = z[72] + z[116];
z[117] = z[72] + T(2) * z[90];
z[118] = z[26] * z[65];
z[89] = -z[85] + -z[86] + -z[89] + z[109] + -z[117] + z[118];
z[89] = z[5] * z[89];
z[109] = -z[26] + z[30];
z[109] = z[65] * z[109];
z[118] = (T(5) * z[12]) / T(2);
z[64] = -z[61] + z[64] + z[118];
z[119] = z[29] * z[64];
z[79] = z[79] + z[80];
z[80] = -z[67] + z[75];
z[120] = -(z[27] * z[80]);
z[120] = T(3) * z[79] + z[120];
z[98] = -z[98] + z[109] + -z[111] + -z[119] + z[120] / T(2);
z[98] = z[4] * z[98];
z[109] = z[2] * z[65];
z[120] = (T(3) * z[16]) / T(2);
z[121] = T(2) * z[19];
z[122] = z[21] / T(2) + -z[120] + z[121];
z[123] = z[14] + z[15];
z[124] = z[48] + z[123];
z[124] = z[17] + z[124] / T(2);
z[125] = T(3) * z[124];
z[126] = -z[20] / T(2) + z[125];
z[127] = T(2) * z[22];
z[128] = z[122] + -z[126] + z[127];
z[129] = z[34] * z[128];
z[130] = z[36] * z[88];
z[129] = z[129] + -z[130];
z[131] = z[59] + z[69];
z[131] = z[3] * z[131];
z[132] = (T(3) * z[1]) / T(2);
z[106] = -z[12] + z[106] + -z[132];
z[133] = z[9] * z[106];
z[109] = -z[109] + -z[129] + z[131] + z[133];
z[109] = z[29] * z[109];
z[133] = -z[33] + z[65];
z[134] = z[2] * z[133];
z[135] = T(2) * z[12];
z[136] = (T(-7) * z[33]) / T(2) + z[73] + z[132] + -z[135];
z[137] = z[9] * z[136];
z[138] = z[3] * z[70];
z[129] = z[129] + z[134] + z[137] + -z[138];
z[129] = z[30] * z[129];
z[134] = z[18] + z[123];
z[134] = z[17] + z[134] / T(2);
z[137] = T(3) * z[134];
z[139] = T(2) * z[20];
z[122] = z[22] / T(2) + z[122] + -z[137] + z[139];
z[140] = z[0] * z[122];
z[141] = z[35] * z[128];
z[142] = z[140] + z[141];
z[143] = z[29] * z[122];
z[144] = z[30] * z[122];
z[145] = z[51] / T(2) + -z[54];
z[146] = T(3) * z[51];
z[145] = z[145] * z[146];
z[146] = -z[18] + z[20] + z[48];
z[147] = -z[22] + z[146];
z[148] = (T(3) * z[147]) / T(2);
z[149] = z[41] * z[148];
z[150] = z[142] + -z[143] + -z[144] + -z[145] + z[149];
z[150] = z[25] * z[150];
z[151] = (T(3) * z[10]) / T(2);
z[152] = z[12] / T(2);
z[153] = z[63] + z[151] + z[152];
z[154] = z[0] * z[153];
z[154] = z[114] + z[116] + z[154];
z[155] = T(2) * z[112];
z[156] = z[28] * z[155];
z[79] = (T(-3) * z[79]) / T(2) + z[154] + z[156];
z[79] = z[3] * z[79];
z[157] = z[29] + z[30];
z[157] = z[128] * z[157];
z[158] = -(z[42] * z[147]);
z[158] = (T(3) * z[158]) / T(2);
z[157] = -z[142] + z[157] + -z[158];
z[157] = z[23] * z[157];
z[159] = z[27] * z[128];
z[160] = z[142] + -z[159];
z[161] = -(z[26] * z[122]);
z[158] = z[149] + z[158] + z[160] + z[161];
z[158] = z[13] * z[158];
z[149] = z[145] + z[149] + -z[159];
z[149] = z[34] * z[149];
z[161] = T(3) * z[12];
z[162] = -z[33] + z[161];
z[163] = z[1] + z[162];
z[163] = -z[10] + z[163] / T(2);
z[164] = z[35] * z[163];
z[164] = z[66] + z[164];
z[100] = -z[100] + z[156] + -z[164];
z[100] = z[2] * z[100];
z[165] = -(z[34] * z[122]);
z[166] = z[2] + z[9];
z[155] = z[155] * z[166];
z[155] = z[155] + z[165];
z[155] = z[26] * z[155];
z[165] = z[7] + z[8];
z[166] = z[2] + T(2) * z[3] + (T(11) * z[165]) / T(15);
z[167] = z[25] + -z[34];
z[168] = z[4] + z[5];
z[166] = (T(26) * z[9]) / T(45) + (T(-5) * z[23]) / T(4) + (T(-112) * z[49]) / T(45) + z[166] / T(3) + T(2) * z[167] + (T(2) * z[168]) / T(3);
z[166] = z[91] * z[166];
z[169] = (T(8) * z[49]) / T(5) + (T(-4) * z[165]) / T(5);
z[115] = z[115] * z[169];
z[169] = z[41] * z[87];
z[81] = z[81] + z[169];
z[81] = z[36] * z[81];
z[169] = T(3) * z[55];
z[167] = -(z[167] * z[169]);
z[77] = z[77] + z[79] + (T(3) * z[81]) / T(2) + z[89] + z[95] + z[98] + z[100] + z[103] + -z[109] + z[115] + z[129] + z[149] + z[150] + z[155] + z[157] + z[158] + z[166] / T(3) + z[167];
z[77] = z[31] * z[77];
z[79] = z[9] + z[165];
z[81] = T(-2) * z[49] + z[79];
z[81] = z[52] * z[81];
z[77] = z[77] + (T(192) * z[81]) / T(5);
z[77] = int_to_imaginary<T>(1) * z[77];
z[81] = T(5) * z[10];
z[89] = T(3) * z[1];
z[95] = T(4) * z[11];
z[98] = T(-4) * z[33] + z[81] + z[89] + z[95] + -z[135];
z[98] = z[30] * z[98];
z[100] = z[1] + -z[12];
z[103] = T(2) * z[10];
z[115] = T(2) * z[33];
z[129] = -z[100] + -z[103] + z[115];
z[129] = z[94] * z[129];
z[76] = z[28] * z[76];
z[149] = z[92] / T(2);
z[150] = z[59] + z[149];
z[155] = z[0] * z[150];
z[157] = z[29] * z[150];
z[158] = (T(5) * z[10]) / T(2);
z[166] = z[1] + -z[60] + z[158];
z[167] = z[75] + -z[166];
z[167] = z[11] + z[167] / T(2);
z[167] = z[26] * z[167];
z[76] = z[76] + z[98] + z[129] + -z[155] + z[157] + z[167];
z[76] = z[26] * z[76];
z[98] = T(5) * z[33];
z[129] = z[12] + z[89];
z[129] = T(4) * z[10] + -z[95] + -z[98] + T(2) * z[129];
z[129] = z[35] * z[129];
z[157] = z[33] + T(-6) * z[67] + z[95];
z[167] = z[27] * z[157];
z[170] = z[89] + z[135];
z[171] = -z[10] + -z[95] + -z[170];
z[171] = z[0] * z[171];
z[90] = T(5) * z[84] + -z[90] + z[129] + z[167] + z[171];
z[90] = z[30] * z[90];
z[129] = (T(-7) * z[1]) / T(2) + z[59] + z[115] + -z[135] + -z[151];
z[167] = prod_pow(z[35], 2);
z[129] = z[129] * z[167];
z[57] = -z[57] + z[84];
z[57] = z[28] * z[57];
z[171] = -z[33] + T(3) * z[67];
z[171] = -z[59] + z[171] / T(2);
z[172] = z[35] * z[171];
z[173] = -z[155] + z[172];
z[174] = T(5) * z[67];
z[175] = -z[33] + -z[174];
z[175] = z[59] + z[175] / T(2);
z[175] = z[27] * z[175];
z[82] = z[10] + -z[82];
z[82] = z[29] * z[82];
z[82] = z[82] + z[84] + z[173] + z[175];
z[82] = z[29] * z[82];
z[157] = -(z[35] * z[157]);
z[175] = T(2) * z[67];
z[176] = -z[33] + z[175];
z[177] = -z[59] + z[176];
z[178] = -(z[27] * z[177]);
z[157] = z[157] + z[178];
z[157] = z[27] * z[157];
z[178] = z[67] * z[113];
z[179] = z[1] + z[61] + z[152];
z[180] = z[11] + z[179] / T(2);
z[180] = z[0] * z[180];
z[180] = z[178] + z[180];
z[180] = z[0] * z[180];
z[181] = z[33] / T(6);
z[182] = z[12] / T(3);
z[183] = z[10] / T(6) + z[107] + -z[181] + z[182];
z[183] = z[31] * z[183];
z[183] = T(-12) * z[50] + (T(5) * z[183]) / T(2);
z[183] = z[31] * z[183];
z[184] = z[40] * z[78];
z[185] = z[33] * z[40];
z[186] = z[184] + -z[185];
z[186] = (T(3) * z[186]) / T(2);
z[187] = z[59] + z[179];
z[188] = T(3) * z[46];
z[187] = z[187] * z[188];
z[189] = -z[1] + z[33] + -z[151] + z[152];
z[189] = -z[59] + T(3) * z[189];
z[189] = z[38] * z[189];
z[88] = z[39] * z[88];
z[190] = z[37] * z[92];
z[57] = z[57] + z[76] + z[82] + z[88] + z[90] + z[129] + z[157] + z[180] + z[183] + -z[186] + z[187] + z[189] + z[190];
z[57] = z[6] * z[57];
z[76] = z[11] / T(2);
z[82] = -z[76] + -z[115] + z[153];
z[82] = z[30] * z[82];
z[88] = z[28] * z[102];
z[90] = -z[33] + z[67];
z[90] = z[11] + z[90] / T(2);
z[90] = z[35] * z[90];
z[82] = z[71] + z[82] + z[88] + z[90] + -z[97];
z[82] = z[30] * z[82];
z[88] = -z[11] + z[67];
z[90] = -z[88] + z[115];
z[97] = z[27] * z[90];
z[97] = z[97] + -z[116];
z[97] = z[27] * z[97];
z[102] = z[10] / T(4);
z[116] = z[11] + z[102];
z[129] = (T(3) * z[1]) / T(4);
z[157] = -z[12] + (T(-3) * z[33]) / T(4) + z[116] + -z[129];
z[157] = z[28] * z[157];
z[96] = z[96] + z[157];
z[96] = z[28] * z[96];
z[90] = z[90] * z[94];
z[71] = -z[71] + z[90];
z[70] = z[28] * z[70];
z[90] = z[68] / T(4);
z[180] = -z[11] + z[90];
z[183] = -(z[29] * z[180]);
z[70] = z[70] + -z[71] + z[183];
z[70] = z[29] * z[70];
z[183] = z[1] + z[105];
z[187] = (T(3) * z[12]) / T(4);
z[189] = -z[102] + -z[183] + -z[187];
z[189] = z[26] * z[189];
z[104] = z[28] * z[104];
z[190] = z[35] * z[87];
z[104] = z[104] + z[189] + T(-4) * z[190];
z[104] = z[26] * z[104];
z[189] = T(7) * z[1];
z[190] = -z[56] + z[189];
z[191] = z[161] + z[190];
z[192] = T(3) * z[11];
z[191] = z[103] + z[191] / T(2) + -z[192];
z[191] = z[47] * z[191];
z[74] = z[63] + z[74];
z[193] = z[74] + -z[98];
z[193] = z[38] * z[193];
z[184] = z[184] + z[185];
z[185] = (T(7) * z[11]) / T(2);
z[194] = -z[135] + z[185];
z[183] = -z[10] + z[183] + -z[194];
z[183] = z[167] * z[183];
z[102] = (T(7) * z[1]) / T(4) + (T(5) * z[33]) / T(4) + -z[102] + -z[194];
z[102] = z[31] * z[102];
z[102] = T(-3) * z[50] + z[102] / T(3);
z[102] = z[31] * z[102];
z[194] = -z[56] + z[174];
z[194] = -z[95] + z[194] / T(2);
z[195] = -(z[43] * z[194]);
z[70] = z[70] + z[82] + z[96] + z[97] + z[102] + z[104] + z[183] + (T(-3) * z[184]) / T(2) + z[191] + z[193] + z[195];
z[70] = z[32] * z[70];
z[82] = z[28] * z[122];
z[96] = -z[82] + -z[140] + z[143];
z[102] = (T(3) * z[134]) / T(2);
z[104] = (T(3) * z[16]) / T(4);
z[122] = z[19] + z[21] / T(4) + -z[104];
z[143] = z[20] + z[22] / T(4) + -z[102] + z[122];
z[183] = -(z[26] * z[143]);
z[191] = -z[20] + -z[21] + -z[22] + z[121];
z[195] = T(4) * z[191];
z[196] = z[30] * z[195];
z[183] = -z[96] + z[183] + z[196];
z[183] = z[26] * z[183];
z[196] = z[16] / T(2);
z[121] = (T(3) * z[21]) / T(2) + -z[121] + -z[196];
z[197] = (T(3) * z[22]) / T(2);
z[198] = z[121] + -z[134] + z[139] + z[197];
z[198] = z[188] * z[198];
z[199] = -z[28] + z[29];
z[160] = z[160] * z[199];
z[120] = T(-10) * z[19] + (T(7) * z[21]) / T(2) + z[120];
z[137] = (T(7) * z[22]) / T(2) + z[120] + z[137] + z[139];
z[139] = z[38] * z[137];
z[125] = (T(7) * z[20]) / T(2) + z[125];
z[120] = z[120] + z[125] + z[127];
z[199] = -(z[47] * z[120]);
z[200] = z[91] / T(4);
z[201] = (T(-3) * z[40]) / T(2) + z[200];
z[201] = z[147] * z[201];
z[202] = T(2) * z[191];
z[203] = z[167] * z[202];
z[148] = z[39] * z[148];
z[148] = z[148] + z[203];
z[203] = -(z[94] * z[191]);
z[204] = z[27] * z[191];
z[203] = z[203] + z[204];
z[203] = z[113] * z[203];
z[205] = prod_pow(z[0], 2);
z[206] = -(z[143] * z[205]);
z[207] = z[0] * z[191];
z[208] = z[35] * z[191];
z[207] = z[207] + z[208];
z[209] = z[204] + -z[207];
z[209] = z[30] * z[209];
z[210] = z[59] + -z[67];
z[210] = z[44] * z[210];
z[160] = z[139] + z[148] + z[160] + z[183] + -z[198] + z[199] + z[201] + z[203] + z[206] + T(4) * z[209] + T(2) * z[210];
z[160] = z[13] * z[160];
z[183] = z[28] * z[65];
z[199] = z[88] * z[94];
z[201] = z[0] + -z[29];
z[201] = z[149] * z[201];
z[170] = z[101] + z[170];
z[170] = z[30] * z[170];
z[60] = -z[1] + -z[60] + z[61];
z[60] = z[11] + z[60] / T(2);
z[60] = z[26] * z[60];
z[60] = z[60] + z[170] + z[183] + -z[199] + z[201];
z[60] = z[26] * z[60];
z[64] = z[28] * z[64];
z[170] = -z[59] + (T(-3) * z[68]) / T(2);
z[170] = z[27] * z[170];
z[149] = z[0] * z[149];
z[201] = T(-9) * z[1] + T(-5) * z[12] + z[75];
z[201] = -z[10] + z[11] + z[201] / T(4);
z[201] = z[29] * z[201];
z[64] = z[64] + z[149] + z[170] + z[199] + z[201];
z[64] = z[29] * z[64];
z[149] = z[27] + z[35];
z[149] = z[88] * z[149];
z[92] = -z[92] + -z[192];
z[92] = z[0] * z[92];
z[92] = z[92] + z[149];
z[135] = (T(-5) * z[1]) / T(2) + -z[61] + -z[135];
z[135] = z[30] * z[135];
z[92] = T(2) * z[92] + -z[119] + z[135] + -z[183];
z[92] = z[30] * z[92];
z[119] = z[80] / T(2);
z[135] = z[27] * z[119];
z[111] = z[111] + z[135] + z[157];
z[111] = z[28] * z[111];
z[135] = z[11] + z[67];
z[135] = z[113] * z[135];
z[149] = -z[11] + -z[166];
z[149] = z[0] * z[149];
z[135] = z[135] + z[149];
z[135] = z[0] * z[135];
z[149] = z[95] + z[158];
z[157] = -z[63] + -z[149] + z[152];
z[157] = z[37] * z[157];
z[158] = -(z[35] * z[88]);
z[166] = z[27] * z[67];
z[158] = z[158] + z[166];
z[158] = z[113] * z[158];
z[166] = (T(3) * z[39]) / T(2);
z[170] = -(z[99] * z[166]);
z[199] = z[150] * z[188];
z[201] = -z[59] + T(-3) * z[179];
z[201] = z[38] * z[201];
z[60] = z[60] + z[64] + z[92] + z[111] + z[135] + z[157] + z[158] + z[170] + z[199] + z[201];
z[60] = z[4] * z[60];
z[64] = T(5) * z[19] + (T(-7) * z[21]) / T(4) + -z[104];
z[92] = -z[20] + (T(-7) * z[22]) / T(4) + z[64] + -z[102];
z[92] = z[26] * z[92];
z[102] = z[29] * z[191];
z[102] = z[102] + -z[207];
z[92] = z[92] + T(4) * z[102] + -z[144];
z[92] = z[26] * z[92];
z[92] = z[92] + -z[139] + -z[198];
z[102] = z[27] * z[195];
z[104] = z[102] + z[141];
z[64] = -z[22] + z[64] + -z[125] / T(2);
z[64] = z[29] * z[64];
z[111] = z[0] * z[195];
z[64] = z[64] + -z[104] + -z[111];
z[64] = z[29] * z[64];
z[125] = -z[204] + z[208];
z[125] = T(4) * z[125];
z[135] = z[125] + z[140];
z[135] = z[30] * z[135];
z[139] = -(z[43] * z[120]);
z[140] = -z[145] + -z[169] + z[200];
z[140] = int_to_imaginary<T>(1) * z[31] * z[140];
z[124] = (T(3) * z[20]) / T(2) + -z[124];
z[121] = z[121] + z[124] + z[127];
z[127] = T(3) * z[45];
z[144] = z[121] * z[127];
z[145] = z[44] * z[59];
z[157] = z[35] * z[102];
z[145] = z[145] + -z[157];
z[157] = z[16] / T(4) + z[19] + (T(-3) * z[21]) / T(4);
z[124] = z[22] + z[124] / T(2) + -z[157];
z[124] = z[124] * z[167];
z[134] = -z[20] + (T(-3) * z[22]) / T(4) + z[134] / T(2) + z[157];
z[134] = z[0] * z[134];
z[134] = z[102] + T(3) * z[134];
z[134] = z[0] * z[134];
z[157] = z[51] + -z[147] / T(4);
z[91] = z[91] * z[157];
z[64] = (T(-73) * z[53]) / T(6) + z[64] + z[91] + z[92] + T(3) * z[124] + z[134] + z[135] + z[139] + z[140] + z[144] + -z[145];
z[64] = z[24] * z[64];
z[91] = z[58] + -z[59];
z[56] = -z[56] + T(3) * z[100];
z[56] = z[56] / T(2) + z[91];
z[56] = z[35] * z[56];
z[100] = -(z[28] * z[136]);
z[124] = -(z[27] * z[171]);
z[56] = z[56] + z[100] + z[124] + -z[155];
z[56] = z[9] * z[56];
z[100] = z[28] * z[128];
z[124] = z[100] + z[159];
z[134] = -z[124] + z[141];
z[134] = z[34] * z[134];
z[133] = -(z[28] * z[133]);
z[66] = -z[66] + z[133];
z[66] = z[2] * z[66];
z[133] = (T(3) * z[33]) / T(2);
z[76] = z[63] + z[76] + z[103] + -z[133];
z[76] = z[2] * z[76];
z[105] = -z[105] + z[175];
z[135] = -(z[3] * z[105]);
z[136] = -(z[34] * z[202]);
z[83] = z[9] * z[83];
z[76] = z[76] + (T(5) * z[83]) / T(2) + z[135] + z[136];
z[76] = z[30] * z[76];
z[83] = T(11) * z[67] + -z[75];
z[83] = z[83] / T(2) + -z[95];
z[83] = z[8] * z[83];
z[135] = -z[33] + -z[59] + T(4) * z[67];
z[135] = z[3] * z[135];
z[136] = z[7] * z[59];
z[135] = -z[83] + z[135] + -z[136];
z[135] = z[35] * z[135];
z[139] = z[176] + -z[192];
z[139] = z[3] * z[139];
z[140] = z[7] * z[11];
z[139] = z[139] + -z[140];
z[139] = -z[83] + T(2) * z[139];
z[144] = -(z[27] * z[139]);
z[157] = z[7] * z[179];
z[158] = T(3) * z[157];
z[88] = z[8] * z[88];
z[169] = T(2) * z[88];
z[170] = z[7] * z[95];
z[176] = z[158] + z[169] + z[170];
z[179] = z[3] * z[59];
z[198] = z[176] + z[179];
z[198] = z[0] * z[198];
z[138] = z[130] + z[138];
z[138] = z[28] * z[138];
z[56] = z[56] + z[66] + z[76] + -z[109] + z[134] + z[135] + z[138] + z[144] + z[198];
z[56] = z[30] * z[56];
z[66] = T(-5) * z[1] + T(-13) * z[12] + -z[33];
z[66] = z[59] + z[66] / T(4) + z[103];
z[66] = z[29] * z[66];
z[76] = z[11] + T(2) * z[78];
z[103] = z[76] * z[110];
z[66] = z[66] + -z[71] + z[85] + -z[103];
z[66] = z[29] * z[66];
z[65] = z[30] * z[65];
z[71] = z[26] + T(2) * z[29] + -z[110];
z[71] = z[71] * z[76];
z[65] = z[65] + z[71] + -z[183];
z[65] = z[26] * z[65];
z[71] = z[153] * z[188];
z[85] = z[76] * z[205];
z[71] = z[71] + z[85];
z[72] = z[72] + -z[84] / T(2);
z[72] = z[28] * z[72];
z[84] = z[84] + -z[86] / T(2) + -z[117];
z[84] = z[30] * z[84];
z[85] = z[39] * z[108];
z[85] = T(3) * z[85];
z[86] = z[167] * z[180];
z[65] = z[65] + z[66] + z[71] + z[72] + z[84] + -z[85] + z[86] + z[97] + z[186];
z[65] = z[5] * z[65];
z[66] = z[10] + -z[33];
z[72] = (T(-7) * z[66]) / T(6) + -z[132] + -z[182];
z[72] = z[9] * z[72];
z[84] = z[11] + z[12] + (T(5) * z[33]) / T(2) + -z[107];
z[61] = -z[61] + z[84] / T(3);
z[61] = z[3] * z[61];
z[84] = (T(-11) * z[1]) / T(6) + (T(-5) * z[10]) / T(6) + (T(7) * z[11]) / T(3) + -z[12] + z[181];
z[84] = z[2] * z[84];
z[61] = z[61] + z[72] + z[84];
z[72] = z[1] / T(4);
z[84] = z[33] / T(4);
z[86] = (T(3) * z[10]) / T(4);
z[97] = z[11] + -z[12] + -z[72] + z[84] + z[86];
z[97] = z[8] * z[97];
z[66] = (T(-13) * z[66]) / T(12) + -z[129] + z[182];
z[66] = z[5] * z[66];
z[78] = -z[75] + -z[78];
z[78] = z[7] * z[78];
z[107] = -z[18] / T(2) + (T(3) * z[48]) / T(2) + z[123];
z[107] = z[20] / T(12) + (T(-11) * z[22]) / T(12) + z[107] / T(2);
z[108] = -z[17] + (T(2) * z[19]) / T(3) + z[21] / T(6) + -z[196];
z[109] = z[51] + z[108];
z[110] = z[107] + -z[109];
z[110] = z[34] * z[110];
z[117] = (T(-3) * z[18]) / T(2) + z[48] / T(2) + -z[123];
z[109] = (T(11) * z[20]) / T(12) + -z[22] / T(12) + z[109] + z[117] / T(2);
z[109] = z[25] * z[109];
z[72] = z[12] + -z[72];
z[72] = (T(-5) * z[10]) / T(12) + -z[11] / T(6) + z[72] / T(3) + z[84];
z[72] = z[4] * z[72];
z[84] = z[107] + -z[108];
z[84] = z[23] * z[84];
z[107] = z[36] * z[87];
z[61] = z[61] / T(2) + z[66] + z[72] + z[78] / T(4) + z[84] + z[97] + (T(5) * z[107]) / T(4) + z[109] + z[110] + -z[140];
z[61] = z[31] * z[61];
z[66] = z[3] + z[168];
z[66] = T(-3) * z[2] + T(2) * z[9] + T(8) * z[49] + T(-6) * z[66] + T(5) * z[165];
z[66] = z[50] * z[66];
z[61] = z[61] + z[66];
z[61] = z[31] * z[61];
z[66] = z[7] * z[80];
z[72] = (T(3) * z[66]) / T(4);
z[78] = -z[22] + -z[122] + z[126] / T(2);
z[80] = -(z[34] * z[78]);
z[84] = -z[1] + z[162];
z[84] = z[84] / T(4) + -z[101];
z[84] = z[9] * z[84];
z[97] = z[2] * z[76];
z[90] = -z[11] + -z[90];
z[90] = z[3] * z[90];
z[80] = -z[72] + z[80] + z[84] + z[90] + z[97] + -z[136];
z[80] = z[29] * z[80];
z[84] = (T(3) * z[66]) / T(2);
z[69] = z[3] * z[69];
z[69] = z[69] + z[84] + z[136] + -z[169];
z[69] = z[35] * z[69];
z[90] = z[35] * z[195];
z[101] = -z[90] + -z[124];
z[101] = z[34] * z[101];
z[108] = z[11] + z[68];
z[108] = z[108] * z[113];
z[106] = z[28] * z[106];
z[106] = z[106] + z[108] + -z[173];
z[106] = z[9] * z[106];
z[108] = (T(9) * z[10]) / T(2);
z[109] = T(4) * z[1] + z[59] + z[108] + -z[152];
z[109] = z[8] * z[109];
z[110] = z[3] * z[11];
z[110] = z[110] + -z[140];
z[117] = -z[109] + T(2) * z[110];
z[122] = z[0] * z[117];
z[123] = z[88] + z[140];
z[124] = -(z[3] * z[177]);
z[124] = z[123] + z[124];
z[124] = z[113] * z[124];
z[103] = -z[103] + -z[183];
z[103] = z[2] * z[103];
z[126] = z[130] + z[131];
z[126] = z[28] * z[126];
z[69] = z[69] + z[80] + z[101] + z[103] + z[106] + -z[122] + z[124] + z[126];
z[69] = z[29] * z[69];
z[80] = -(z[34] * z[143]);
z[101] = -z[1] + (T(-7) * z[10]) / T(4) + z[187];
z[103] = z[33] + -z[101] + z[185];
z[103] = z[2] * z[103];
z[101] = -z[11] + z[101] + z[133];
z[101] = z[9] * z[101];
z[80] = z[80] + z[101] + z[103] + z[140] + (T(3) * z[157]) / T(2);
z[80] = z[26] * z[80];
z[101] = (T(7) * z[12]) / T(2);
z[73] = z[59] + z[73] + -z[75] + -z[101];
z[73] = z[9] * z[73];
z[62] = -z[11] + z[62];
z[62] = z[2] * z[62];
z[62] = z[62] + z[73] + -z[176] + z[179];
z[62] = z[30] * z[62];
z[73] = z[82] + z[90];
z[73] = z[34] * z[73];
z[87] = -(z[87] * z[94]);
z[76] = -(z[0] * z[76]);
z[90] = -(z[28] * z[112]);
z[76] = z[76] + z[87] + z[90];
z[87] = T(2) * z[2];
z[76] = z[76] * z[87];
z[87] = z[67] * z[94];
z[87] = z[87] + z[155] + -z[156];
z[87] = z[9] * z[87];
z[90] = -(z[9] * z[150]);
z[90] = z[90] + T(2) * z[97] + z[117];
z[90] = z[29] * z[90];
z[88] = z[88] + -z[110];
z[88] = z[88] * z[94];
z[62] = z[62] + z[73] + z[76] + z[80] + z[87] + z[88] + z[90] + -z[122];
z[62] = z[26] * z[62];
z[73] = -z[12] + -z[190];
z[73] = -z[58] + z[73] / T(2);
z[73] = z[11] + z[73] / T(2);
z[73] = z[73] * z[167];
z[76] = z[27] * z[112];
z[80] = -z[76] + z[172];
z[80] = z[27] * z[80];
z[87] = z[1] + (T(5) * z[12]) / T(4) + -z[116];
z[87] = z[0] * z[87];
z[87] = z[87] + -z[178];
z[87] = z[0] * z[87];
z[88] = z[0] + z[35];
z[88] = z[88] * z[93];
z[76] = -z[76] + z[88];
z[88] = z[28] * z[67];
z[76] = T(2) * z[76] + (T(3) * z[88]) / T(2);
z[76] = z[28] * z[76];
z[73] = z[73] + z[76] + z[80] + z[85] + z[87] + z[186] + -z[193];
z[73] = z[9] * z[73];
z[76] = z[30] * z[202];
z[80] = z[76] + -z[96];
z[80] = z[30] * z[80];
z[85] = z[28] * z[147];
z[85] = (T(3) * z[85]) / T(4);
z[87] = -z[85] + -z[142];
z[87] = z[28] * z[87];
z[88] = z[29] * z[202];
z[82] = z[82] + z[88] + -z[111];
z[82] = z[29] * z[82];
z[88] = (T(3) * z[146]) / T(2) + -z[197];
z[88] = z[40] * z[88];
z[90] = -(z[78] * z[167]);
z[93] = z[202] * z[205];
z[94] = T(7) * z[53];
z[80] = z[80] + z[82] + z[87] + -z[88] + z[90] + z[92] + z[93] + z[94];
z[80] = z[25] * z[80];
z[82] = -z[85] + z[142];
z[82] = z[28] * z[82];
z[78] = -(z[29] * z[78]);
z[78] = z[78] + -z[100] + -z[104];
z[78] = z[29] * z[78];
z[87] = z[29] * z[128];
z[76] = -z[76] + z[87] + -z[100] + z[125];
z[76] = z[30] * z[76];
z[87] = z[37] * z[137];
z[90] = -(z[0] * z[143]);
z[90] = z[90] + z[102];
z[90] = z[0] * z[90];
z[76] = (T(-115) * z[53]) / T(6) + z[76] + z[78] + z[82] + z[87] + z[90] + -z[145] + -z[148];
z[76] = z[23] * z[76];
z[78] = z[23] + z[34];
z[82] = -(z[78] * z[120]);
z[87] = T(7) * z[67];
z[90] = z[87] + -z[98];
z[90] = z[90] / T(2) + -z[95];
z[90] = z[3] * z[90];
z[67] = -z[67] + -z[98];
z[67] = z[67] / T(2) + -z[95];
z[67] = z[9] * z[67];
z[92] = -(z[5] * z[194]);
z[93] = z[95] + z[119];
z[93] = z[4] * z[93];
z[68] = z[6] * z[68];
z[67] = z[67] + z[68] + z[82] + -z[84] + z[90] + z[92] + z[93] + -z[170];
z[67] = z[43] * z[67];
z[68] = -(z[163] * z[167]);
z[68] = z[68] + T(3) * z[184];
z[58] = -z[33] + z[58] + z[89];
z[58] = -z[11] + z[58] / T(4);
z[82] = z[28] * z[58];
z[82] = z[82] + z[164];
z[82] = z[28] * z[82];
z[84] = z[74] + -z[115] + z[192];
z[84] = z[38] * z[84];
z[68] = z[68] / T(2) + z[71] + z[82] + z[84];
z[68] = z[2] * z[68];
z[71] = -z[63] + z[95] + -z[101] + z[151];
z[71] = z[3] * z[71];
z[82] = -z[63] + -z[95] + -z[108] + z[118];
z[82] = z[8] * z[82];
z[84] = -(z[25] * z[137]);
z[74] = z[74] + z[95];
z[89] = z[2] + z[5];
z[74] = z[74] * z[89];
z[63] = (T(9) * z[12]) / T(2) + z[63] + -z[149];
z[63] = z[9] * z[63];
z[63] = z[63] + z[71] + z[74] + z[82] + z[84];
z[63] = z[37] * z[63];
z[71] = -z[13] + z[23] + -z[34];
z[71] = z[71] * z[121];
z[74] = -z[33] + z[174];
z[74] = -z[59] + z[74] / T(2);
z[82] = -z[6] + z[8];
z[74] = z[74] * z[82];
z[82] = z[5] + z[32];
z[82] = z[82] * z[119];
z[84] = -(z[3] * z[171]);
z[66] = -z[66] / T(2) + z[71] + z[74] + z[82] + z[84];
z[66] = z[66] * z[127];
z[71] = -(z[128] * z[167]);
z[74] = z[113] * z[191];
z[74] = z[74] + z[141];
z[74] = z[27] * z[74];
z[82] = -z[85] + z[159];
z[82] = z[28] * z[82];
z[71] = z[71] + z[74] + z[82] + -z[88] + -z[94];
z[71] = z[34] * z[71];
z[74] = -z[25] + z[78];
z[74] = z[74] * z[120];
z[75] = -z[75] + z[87];
z[75] = -z[59] + z[75] / T(2);
z[78] = -z[3] + z[8];
z[75] = z[75] * z[78];
z[78] = -z[161] + z[190];
z[78] = z[78] / T(2) + z[81];
z[81] = z[2] + -z[9];
z[78] = z[78] * z[81];
z[81] = -z[12] + T(9) * z[33] + -z[189];
z[81] = z[81] / T(2) + -z[91];
z[81] = z[6] * z[81];
z[74] = z[74] + z[75] + z[78] + z[81];
z[74] = z[47] * z[74];
z[75] = z[59] + -z[105];
z[75] = z[3] * z[75];
z[72] = -z[72] + z[75] + z[83];
z[72] = z[72] * z[167];
z[75] = -z[140] + -z[157] / T(2);
z[59] = z[1] + z[12] / T(4) + z[59] + z[86];
z[59] = z[3] * z[59];
z[59] = z[59] + T(3) * z[75] + -z[109];
z[59] = z[0] * z[59];
z[75] = -z[11] + z[175];
z[75] = z[3] * z[75];
z[75] = z[75] + -z[123];
z[75] = z[75] * z[113];
z[59] = z[59] + z[75];
z[59] = z[0] * z[59];
z[75] = -(z[3] * z[154]);
z[58] = z[3] * z[58];
z[58] = z[58] + -z[130];
z[58] = z[28] * z[58];
z[58] = z[58] + z[75];
z[58] = z[28] * z[58];
z[75] = z[35] * z[139];
z[78] = z[3] * z[114];
z[75] = z[75] + z[78];
z[75] = z[27] * z[75];
z[78] = -(z[8] * z[153]);
z[78] = z[78] + -z[136] + -z[157];
z[78] = z[78] * z[188];
z[81] = z[34] * z[137];
z[81] = z[81] + z[136] + z[158];
z[81] = z[38] * z[81];
z[82] = -(z[36] * z[186]);
z[83] = z[3] * z[99];
z[83] = z[83] + -z[107];
z[83] = z[83] * z[166];
z[79] = z[79] * z[191];
z[84] = -(z[49] * z[202]);
z[79] = z[79] + z[84];
z[79] = z[44] * z[79];
return z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + z[68] + z[69] + z[70] + z[71] + z[72] + z[73] + z[74] + z[75] + z[76] + z[77] + z[78] + T(4) * z[79] + z[80] + z[81] + z[82] + z[83] + z[160];
}



template IntegrandConstructorType<double> f_4_332_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_332_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_332_construct (const Kin<qd_real>&);
#endif

}