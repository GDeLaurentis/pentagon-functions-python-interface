#include "f_4_129.h"

namespace PentagonFunctions {

template <typename T> T f_4_129_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_129_W_21 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_129_W_21 (const Kin<T>& kin) {
        c[0] = ((T(-3) * kin.v[3]) / T(4) + -kin.v[4] + T(1)) * kin.v[3] + (-kin.v[3] / T(2) + kin.v[2] / T(4) + -kin.v[4] + T(1)) * kin.v[2] + kin.v[0] * (-kin.v[1] / T(2) + -kin.v[2] / T(2) + kin.v[0] / T(4) + kin.v[3] / T(2) + T(-1) + kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(4) + kin.v[2] / T(2) + (T(3) * kin.v[3]) / T(2) + T(-1) + kin.v[4]);
c[1] = kin.v[0] + kin.v[1] + -kin.v[2] + -kin.v[3];
c[2] = ((T(-3) * kin.v[3]) / T(4) + -kin.v[4] + T(1)) * kin.v[3] + (-kin.v[3] / T(2) + kin.v[2] / T(4) + -kin.v[4] + T(1)) * kin.v[2] + kin.v[0] * (-kin.v[1] / T(2) + -kin.v[2] / T(2) + kin.v[0] / T(4) + kin.v[3] / T(2) + T(-1) + kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(4) + kin.v[2] / T(2) + (T(3) * kin.v[3]) / T(2) + T(-1) + kin.v[4]);
c[3] = kin.v[0] + kin.v[1] + -kin.v[2] + -kin.v[3];
c[4] = (-kin.v[2] / T(2) + (T(3) * kin.v[1]) / T(4) + (T(-3) * kin.v[3]) / T(2) + -kin.v[4] + T(1)) * kin.v[1] + (-kin.v[0] / T(4) + -kin.v[3] / T(2) + kin.v[1] / T(2) + kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[0] + kin.v[2] * (-kin.v[2] / T(4) + kin.v[3] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(4) + T(-1) + kin.v[4]);
c[5] = -kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3];
c[6] = (-kin.v[2] / T(2) + (T(3) * kin.v[1]) / T(4) + (T(-3) * kin.v[3]) / T(2) + -kin.v[4] + T(1)) * kin.v[1] + (-kin.v[0] / T(4) + -kin.v[3] / T(2) + kin.v[1] / T(2) + kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[0] + kin.v[2] * (-kin.v[2] / T(4) + kin.v[3] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(4) + T(-1) + kin.v[4]);
c[7] = -kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[4] * (t * c[0] + c[1]) + abb[5] * (t * c[2] + c[3]) + abb[6] * (t * c[4] + c[5]) + abb[7] * (t * c[6] + c[7]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * (abb[5] / T(2) + -abb[6] / T(2) + -abb[7] / T(2)) + abb[4] * (prod_pow(abb[2], 2) / T(2) + -abb[3]) + abb[3] * (abb[6] + abb[7] + -abb[5]) + abb[1] * (abb[2] * abb[4] + abb[1] * ((abb[4] * T(-3)) / T(2) + (abb[5] * T(-3)) / T(2) + (abb[6] * T(3)) / T(2) + (abb[7] * T(3)) / T(2)) + abb[2] * (abb[5] + -abb[6] + -abb[7])));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_129_construct (const Kin<T>& kin) {
    return [&kin, 
            dl21 = DLog_W_21<T>(kin),dl13 = DLog_W_13<T>(kin),dl25 = DLog_W_25<T>(kin),dl6 = DLog_W_6<T>(kin),dl16 = DLog_W_16<T>(kin),dl2 = DLog_W_2<T>(kin),dl8 = DLog_W_8<T>(kin),dl5 = DLog_W_5<T>(kin),dl18 = DLog_W_18<T>(kin),dl3 = DLog_W_3<T>(kin),dl19 = DLog_W_19<T>(kin),dl1 = DLog_W_1<T>(kin),dl20 = DLog_W_20<T>(kin),dl4 = DLog_W_4<T>(kin),dl17 = DLog_W_17<T>(kin),dl31 = DLog_W_31<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl26 = DLog_W_26<T>(kin),spdl21 = SpDLog_f_4_129_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,46> abbr = 
            {dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_15(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl13(t), rlog(v_path[0]), rlog(v_path[2]), dl25(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_1_3_1(kin) - f_1_3_1(kin_path), dl6(t), f_2_1_9(kin_path), dl16(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl2(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl8(t), dl5(t), f_2_1_1(kin_path), dl18(t), dl3(t), dl19(t), dl1(t), dl20(t), dl4(t), dl17(t), dl31(t), dl27(t) / kin_path.SqrtDelta, rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), dl28(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta}
;

        auto result = f_4_129_abbreviated(abbr);
        result = result + spdl21(t, abbr);

        return result;
    };
}

template <typename T> T f_4_129_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[145];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[11];
z[3] = abb[16];
z[4] = abb[19];
z[5] = abb[25];
z[6] = abb[26];
z[7] = abb[27];
z[8] = abb[28];
z[9] = abb[29];
z[10] = abb[5];
z[11] = abb[6];
z[12] = abb[7];
z[13] = abb[13];
z[14] = abb[33];
z[15] = abb[34];
z[16] = abb[35];
z[17] = abb[36];
z[18] = abb[37];
z[19] = abb[38];
z[20] = abb[39];
z[21] = abb[40];
z[22] = abb[41];
z[23] = abb[42];
z[24] = abb[43];
z[25] = abb[44];
z[26] = abb[2];
z[27] = abb[9];
z[28] = abb[14];
z[29] = abb[31];
z[30] = abb[10];
z[31] = abb[12];
z[32] = bc<TR>[0];
z[33] = abb[45];
z[34] = abb[22];
z[35] = abb[23];
z[36] = abb[3];
z[37] = abb[8];
z[38] = abb[30];
z[39] = abb[15];
z[40] = abb[17];
z[41] = abb[18];
z[42] = abb[20];
z[43] = abb[21];
z[44] = abb[24];
z[45] = bc<TR>[1];
z[46] = bc<TR>[3];
z[47] = bc<TR>[5];
z[48] = abb[32];
z[49] = bc<TR>[2];
z[50] = bc<TR>[4];
z[51] = bc<TR>[7];
z[52] = bc<TR>[9];
z[53] = bc<TR>[8];
z[54] = (T(3) * z[43]) / T(2);
z[55] = (T(3) * z[41]) / T(2);
z[56] = z[54] + z[55];
z[57] = T(2) * z[30];
z[58] = z[0] / T(2);
z[59] = z[57] + z[58];
z[60] = z[27] / T(2);
z[61] = -z[26] + z[60];
z[62] = -z[56] + z[59] + -z[61];
z[62] = z[9] * z[62];
z[63] = z[26] / T(2);
z[64] = z[59] + -z[63];
z[65] = -z[55] + z[64];
z[66] = z[43] / T(2);
z[67] = T(2) * z[27];
z[68] = z[65] + -z[66] + z[67];
z[68] = z[8] * z[68];
z[69] = T(2) * z[26];
z[70] = -z[59] + z[67] + z[69];
z[71] = -z[54] + z[70];
z[71] = z[5] * z[71];
z[72] = z[26] + -z[41];
z[73] = -z[57] + z[72];
z[73] = z[34] * z[73];
z[74] = z[67] + -z[72] / T(2);
z[74] = z[6] * z[74];
z[75] = T(2) * z[31];
z[76] = z[2] + z[6];
z[77] = z[9] + -z[76];
z[77] = z[75] * z[77];
z[78] = (T(3) * z[4]) / T(2);
z[79] = -z[26] + z[43];
z[80] = z[78] * z[79];
z[81] = -z[0] + z[41];
z[82] = z[3] * z[81];
z[62] = z[62] + z[68] + z[71] + z[73] + z[74] + z[77] + z[80] + z[82] / T(2);
z[62] = z[32] * z[62];
z[68] = -z[27] + z[31];
z[71] = z[28] * z[32];
z[71] = T(2) * z[71];
z[71] = z[68] * z[71];
z[62] = z[62] + z[71];
z[62] = int_to_imaginary<T>(1) * z[62];
z[74] = -z[27] + z[64];
z[74] = z[27] * z[74];
z[77] = (T(3) * z[36]) / T(2);
z[80] = (T(5) * z[39]) / T(2);
z[83] = z[77] + -z[80];
z[84] = z[30] + z[58];
z[84] = z[58] * z[84];
z[85] = z[30] / T(2);
z[86] = z[0] + -z[63] + -z[85];
z[86] = z[26] * z[86];
z[87] = (T(3) * z[44]) / T(2);
z[88] = prod_pow(z[30], 2);
z[84] = -z[74] + -z[83] + z[84] + z[86] + z[87] + z[88];
z[84] = z[5] * z[84];
z[70] = -(z[5] * z[70]);
z[64] = z[64] + -z[67];
z[64] = z[6] * z[64];
z[86] = z[0] + z[30];
z[89] = z[86] / T(2);
z[90] = z[61] + -z[89];
z[90] = z[9] * z[90];
z[91] = z[8] / T(2);
z[92] = z[27] + -z[30];
z[93] = z[26] + z[92];
z[94] = -(z[91] * z[93]);
z[95] = z[8] + -z[9];
z[96] = (T(3) * z[5]) / T(4) + z[76] + z[95] / T(4);
z[96] = z[31] * z[96];
z[64] = z[64] + z[70] + z[90] + z[94] + z[96];
z[64] = z[31] * z[64];
z[70] = -z[60] + z[69] + -z[85];
z[70] = z[27] * z[70];
z[90] = -z[30] + z[58];
z[94] = z[63] + z[90];
z[94] = z[26] * z[94];
z[96] = z[88] / T(2);
z[97] = -(z[0] * z[30]);
z[97] = -z[96] + z[97];
z[98] = z[39] / T(2);
z[99] = (T(3) * z[40]) / T(2);
z[70] = z[70] + z[94] + z[97] / T(2) + z[98] + z[99];
z[70] = z[8] * z[70];
z[97] = z[26] + -z[30];
z[100] = z[60] + z[97];
z[100] = z[27] * z[100];
z[101] = prod_pow(z[0], 2);
z[102] = -z[88] + z[101];
z[103] = z[0] + -z[30];
z[104] = z[26] * z[103];
z[105] = z[40] + -z[102] / T(2) + z[104];
z[106] = -z[36] + z[39];
z[100] = z[100] + z[105] + z[106];
z[107] = z[31] / T(2);
z[108] = -z[93] + z[107];
z[108] = z[31] * z[108];
z[109] = z[27] + -z[43];
z[110] = z[72] + z[109];
z[111] = int_to_imaginary<T>(1) * z[32];
z[112] = z[110] * z[111];
z[113] = prod_pow(z[32], 2);
z[114] = z[113] / T(3);
z[108] = z[42] + z[100] + z[108] + z[112] + -z[114];
z[108] = z[7] * z[108];
z[112] = z[109] * z[111];
z[115] = -z[0] + z[107];
z[115] = z[31] * z[115];
z[115] = z[42] + z[115];
z[116] = -z[0] + z[60];
z[116] = z[27] * z[116];
z[116] = -z[39] + z[115] + -z[116];
z[117] = z[112] + z[116];
z[117] = z[29] * z[117];
z[118] = z[40] + z[96];
z[90] = z[0] * z[90];
z[119] = z[36] + z[90] + z[118];
z[120] = z[3] * z[119];
z[108] = -z[108] + z[117] + z[120];
z[121] = z[27] + -z[41];
z[121] = z[111] * z[121];
z[61] = z[27] * z[61];
z[122] = z[26] * z[30];
z[123] = z[44] + z[122];
z[61] = -z[61] + z[118] + z[121] + -z[123];
z[61] = z[35] * z[61];
z[121] = (T(3) * z[61]) / T(2);
z[124] = z[27] / T(4);
z[89] = z[26] + z[89] + -z[124];
z[89] = z[27] * z[89];
z[89] = -z[39] + z[89] + z[99];
z[125] = (T(3) * z[0]) / T(2) + -z[30];
z[125] = z[0] * z[125];
z[125] = -z[96] + z[125];
z[126] = z[36] / T(2);
z[125] = z[89] + z[104] + z[125] / T(2) + -z[126];
z[125] = z[9] * z[125];
z[127] = -z[0] + z[26];
z[128] = -z[31] + z[92] + z[127];
z[128] = z[31] * z[128];
z[129] = -z[30] + z[127];
z[129] = z[27] * z[129];
z[130] = T(2) * z[39];
z[128] = -z[128] + z[129] + z[130];
z[128] = z[28] * z[128];
z[129] = -z[36] + T(-5) * z[39] + z[105];
z[129] = z[129] / T(2);
z[74] = -z[74] + -z[129];
z[74] = z[6] * z[74];
z[131] = -z[26] + z[107];
z[131] = z[31] * z[131];
z[132] = z[0] * z[26];
z[131] = -z[36] + z[42] + -z[101] / T(2) + z[131] + z[132];
z[78] = -(z[78] * z[131]);
z[132] = z[4] + z[38];
z[133] = (T(4) * z[34]) / T(3);
z[134] = z[3] + -z[6];
z[135] = (T(13) * z[8]) / T(2) + T(5) * z[9] + z[134] / T(2);
z[132] = -z[2] + (T(-7) * z[5]) / T(12) + z[132] / T(4) + -z[133] + z[135] / T(6);
z[132] = z[113] * z[132];
z[135] = z[30] + z[63];
z[135] = z[26] * z[135];
z[136] = z[40] + (T(3) * z[88]) / T(2) + -z[135];
z[136] = z[34] * z[136];
z[137] = z[2] * z[101];
z[138] = z[136] + -z[137];
z[139] = -(z[68] * z[92]);
z[139] = z[39] + z[44] + z[139];
z[139] = z[38] * z[139];
z[140] = z[8] + z[9];
z[141] = z[6] + -z[140];
z[87] = z[87] * z[141];
z[141] = T(3) * z[9];
z[142] = T(3) * z[5];
z[143] = z[8] + z[141] + z[142];
z[144] = z[42] / T(2);
z[143] = z[143] * z[144];
z[62] = z[62] + z[64] + z[70] + z[74] + z[78] + z[84] + z[87] + -z[108] / T(2) + -z[121] + z[125] + -z[128] + z[132] + z[138] + (T(-3) * z[139]) / T(2) + z[143];
z[62] = z[10] * z[62];
z[64] = -z[0] + z[92];
z[64] = z[9] * z[64];
z[70] = z[6] * z[127];
z[64] = z[64] + -z[70];
z[70] = T(2) * z[0];
z[74] = z[70] + z[85];
z[78] = -z[63] + z[74];
z[84] = -z[60] + z[78];
z[87] = z[8] * z[84];
z[108] = -z[5] + z[140];
z[108] = -z[2] + z[108] / T(4);
z[108] = z[31] * z[108];
z[125] = z[5] * z[58];
z[64] = z[64] / T(2) + z[87] + z[108] + z[125];
z[64] = z[31] * z[64];
z[87] = z[4] / T(2);
z[108] = z[87] * z[131];
z[125] = z[27] * z[127];
z[106] = z[106] + z[125];
z[123] = -z[90] + z[106] + -z[123];
z[131] = z[5] / T(2);
z[132] = z[123] * z[131];
z[140] = T(3) * z[8];
z[143] = z[5] + -z[9] + z[140];
z[143] = z[143] * z[144];
z[64] = z[64] + -z[108] + (T(3) * z[117]) / T(2) + -z[128] + z[132] + z[137] + z[139] / T(2) + -z[143];
z[108] = z[26] + z[86];
z[108] = z[63] * z[108];
z[117] = z[60] * z[127];
z[127] = z[0] / T(4) + z[57];
z[127] = z[0] * z[127];
z[77] = -z[77] + (T(-3) * z[88]) / T(4) + z[98] + -z[99] + -z[108] + z[117] + z[127];
z[77] = z[6] * z[77];
z[98] = T(2) * z[86];
z[56] = z[56] + z[60] + z[63] + -z[75] + -z[98];
z[56] = z[56] * z[111];
z[84] = z[31] / T(4) + z[84];
z[84] = z[31] * z[84];
z[78] = -z[78] + z[124];
z[78] = z[27] * z[78];
z[78] = (T(-3) * z[42]) / T(2) + z[78] + z[84];
z[84] = z[88] / T(4);
z[99] = z[84] + -z[99];
z[117] = (T(-3) * z[0]) / T(4) + z[57];
z[117] = z[0] * z[117];
z[56] = z[56] + z[78] + -z[83] + z[99] + -z[108] + (T(-5) * z[113]) / T(6) + z[117];
z[56] = z[7] * z[56];
z[83] = -z[0] + z[43];
z[108] = z[83] * z[131];
z[117] = z[2] + -z[8];
z[124] = z[75] * z[117];
z[79] = z[79] * z[87];
z[79] = z[79] + z[108] + z[124];
z[66] = -z[55] + z[59] + -z[60] + -z[66];
z[66] = z[9] * z[66];
z[55] = z[55] + z[63] + -z[70];
z[55] = z[6] * z[55];
z[54] = z[54] + z[65];
z[54] = z[8] * z[54];
z[65] = T(2) * z[92];
z[87] = z[37] * z[65];
z[54] = z[54] + z[55] + z[66] + z[79] + (T(-3) * z[82]) / T(2) + z[87];
z[54] = z[32] * z[54];
z[54] = z[54] + z[71];
z[54] = int_to_imaginary<T>(1) * z[54];
z[55] = z[60] + z[69] + -z[74];
z[55] = z[27] * z[55];
z[66] = z[0] + z[85];
z[66] = z[0] * z[66];
z[66] = z[66] + -z[80];
z[55] = z[55] + -z[66] + z[94] + -z[99];
z[55] = z[8] * z[55];
z[69] = z[90] + -z[96];
z[69] = z[69] / T(2) + z[89] + -z[122] + z[126];
z[69] = z[9] * z[69];
z[80] = prod_pow(z[27], 2);
z[80] = z[80] + -z[88];
z[80] = z[37] * z[80];
z[85] = -z[6] + -z[140] + -z[141];
z[87] = z[44] / T(2);
z[85] = z[85] * z[87];
z[89] = z[4] + z[5] + -z[38];
z[89] = z[2] + z[89] / T(12);
z[90] = z[8] / T(3) + -z[134];
z[90] = z[89] + z[90] / T(4);
z[90] = z[90] * z[113];
z[54] = z[54] + z[55] + z[56] + z[64] + z[69] + z[77] + -z[80] + z[85] + z[90] + (T(3) * z[120]) / T(2) + -z[121];
z[54] = z[1] * z[54];
z[55] = z[26] + z[41];
z[56] = -z[55] + z[67] + z[70];
z[56] = z[6] * z[56];
z[69] = z[8] * z[109];
z[69] = z[69] + z[82];
z[77] = z[5] + -z[37];
z[65] = z[65] * z[77];
z[77] = -z[6] + -z[117];
z[77] = z[75] * z[77];
z[56] = z[56] + z[65] + T(2) * z[69] + z[73] + z[77];
z[56] = z[56] * z[111];
z[65] = -z[27] + z[57];
z[65] = z[27] * z[65];
z[69] = T(2) * z[44];
z[65] = z[65] + -z[69] + -z[130];
z[77] = T(2) * z[36];
z[85] = -(z[0] * z[57]);
z[85] = -z[65] + z[77] + z[85] + z[118] + z[135];
z[85] = z[6] * z[85];
z[90] = -z[5] + z[134];
z[94] = z[38] / T(3);
z[90] = z[90] / T(3) + z[94] + -z[117] + -z[133];
z[90] = z[90] * z[113];
z[99] = T(2) * z[42] + -z[130];
z[108] = -z[27] + z[70];
z[108] = z[27] * z[108];
z[101] = z[99] + z[101] + z[108];
z[101] = z[8] * z[101];
z[117] = T(2) * z[40];
z[118] = z[88] + z[117];
z[121] = -z[0] + z[57];
z[121] = z[0] * z[121];
z[121] = -z[77] + z[121];
z[122] = z[118] + -z[121];
z[124] = -(z[3] * z[122]);
z[65] = -z[65] + z[88];
z[65] = z[5] * z[65];
z[88] = -z[5] + -z[6];
z[88] = z[88] * z[92];
z[126] = -(z[0] * z[8]);
z[88] = z[88] + z[126];
z[76] = z[31] * z[76];
z[76] = z[76] + T(2) * z[88];
z[76] = z[31] * z[76];
z[56] = z[56] + z[65] + z[76] + z[80] + z[85] + z[90] + z[101] + z[124] + z[138] + T(-2) * z[139];
z[56] = z[11] * z[56];
z[65] = z[81] + -z[109];
z[65] = z[24] * z[65];
z[76] = z[23] * z[110];
z[80] = z[33] * z[109];
z[85] = z[25] * z[81];
z[83] = z[14] * z[83];
z[65] = -z[65] + z[76] + -z[80] + z[83] + -z[85];
z[65] = z[65] * z[111];
z[76] = z[60] + z[103];
z[76] = z[27] * z[76];
z[76] = z[76] + z[119];
z[76] = z[24] * z[76];
z[80] = -z[115] + z[123];
z[80] = z[14] * z[80];
z[83] = z[0] + z[92];
z[83] = z[24] * z[83];
z[85] = z[23] * z[93];
z[83] = z[83] + z[85];
z[85] = z[23] + z[24];
z[88] = z[85] * z[107];
z[88] = -z[83] + z[88];
z[88] = z[31] * z[88];
z[90] = z[23] * z[100];
z[92] = z[24] * z[44];
z[93] = z[42] * z[85];
z[92] = z[92] + z[93];
z[93] = z[25] * z[119];
z[100] = z[33] * z[116];
z[76] = z[65] + z[76] + z[80] + z[88] + z[90] + z[92] + z[93] + -z[100];
z[80] = z[25] / T(2);
z[88] = z[14] / T(2) + -z[80] + -z[85];
z[88] = z[88] * z[113];
z[90] = z[76] + z[88] / T(3);
z[93] = z[15] + z[16];
z[93] = z[18] + -z[93] / T(2);
z[90] = z[90] * z[93];
z[76] = T(3) * z[76] + z[88];
z[88] = z[19] + z[20] + z[21] + z[22];
z[88] = z[88] / T(2);
z[76] = z[76] * z[88];
z[88] = -z[27] + T(-2) * z[97];
z[88] = z[27] * z[88];
z[77] = z[77] + z[88] + z[102] + T(-2) * z[104] + -z[117] + -z[130];
z[77] = z[23] * z[77];
z[65] = z[65] + z[92];
z[88] = -z[31] + z[70];
z[88] = z[31] * z[88];
z[88] = z[88] + -z[99];
z[92] = z[26] * z[57];
z[69] = z[69] + -z[88] + z[92] + -z[121] + T(-2) * z[125];
z[69] = z[14] * z[69];
z[92] = -z[27] + T(-2) * z[103];
z[92] = z[27] * z[92];
z[92] = z[92] + -z[122];
z[92] = z[24] * z[92];
z[93] = -(z[25] * z[122]);
z[97] = -(z[31] * z[85]);
z[83] = T(2) * z[83] + z[97];
z[83] = z[31] * z[83];
z[88] = z[88] + -z[108];
z[97] = -(z[33] * z[88]);
z[100] = T(2) * z[85];
z[101] = -z[14] + z[25] + z[100];
z[101] = z[101] * z[114];
z[65] = T(-2) * z[65] + z[69] + z[77] + z[83] + z[92] + z[93] + z[97] + z[101];
z[65] = z[17] * z[65];
z[69] = z[105] + z[106];
z[69] = z[6] * z[69];
z[61] = z[61] + -z[69] + -z[120];
z[69] = (T(3) * z[27]) / T(2);
z[74] = z[69] + -z[74];
z[74] = z[27] * z[74];
z[77] = z[26] * z[58];
z[66] = z[40] / T(2) + -z[66] + z[74] + z[77] + z[84];
z[66] = z[8] * z[66];
z[74] = z[81] + z[109];
z[74] = z[9] * z[74];
z[77] = z[6] * z[72];
z[74] = z[74] + z[77] + -z[82];
z[77] = T(3) * z[43];
z[83] = z[26] + z[77] + -z[81];
z[67] = -z[67] + z[83] / T(2);
z[67] = z[8] * z[67];
z[67] = z[67] + z[74] / T(2) + z[79];
z[67] = z[32] * z[67];
z[67] = z[67] + z[71];
z[67] = int_to_imaginary<T>(1) * z[67];
z[71] = z[27] + -z[72] + z[77];
z[71] = z[71] / T(2) + -z[75];
z[71] = z[71] * z[111];
z[71] = z[71] + z[78] + z[113] / T(6) + -z[129];
z[71] = z[7] * z[71];
z[69] = -z[69] + z[86];
z[60] = z[60] * z[69];
z[60] = -z[39] + z[60] + -z[119] / T(2);
z[60] = z[9] * z[60];
z[69] = -z[6] + z[95];
z[69] = z[69] * z[87];
z[72] = -z[3] + -z[6];
z[72] = z[9] + z[72] / T(2);
z[72] = (T(-5) * z[8]) / T(2) + z[72] / T(3);
z[72] = z[72] / T(2) + z[89];
z[72] = z[72] * z[113];
z[60] = z[60] + -z[61] / T(2) + z[64] + z[66] + z[67] + z[69] + z[71] + z[72];
z[60] = z[13] * z[60];
z[61] = -(z[68] * z[70]);
z[57] = -z[57] + z[58];
z[57] = z[0] * z[57];
z[58] = z[30] + z[31] + -z[43] + -z[81];
z[58] = z[58] * z[111];
z[63] = z[0] + z[63];
z[63] = z[26] * z[63];
z[63] = z[36] + z[63];
z[57] = z[57] + T(2) * z[58] + z[61] + z[63] + z[99] + (T(2) * z[113]) / T(3) + z[117];
z[57] = z[11] * z[57];
z[58] = z[40] + -z[96] + -z[121] + z[135];
z[58] = z[12] * z[58];
z[55] = -z[55] + z[98];
z[55] = z[12] * z[55];
z[61] = prod_pow(z[45], 2);
z[55] = z[55] + (T(-3) * z[61]) / T(20) + (T(-11) * z[113]) / T(180);
z[55] = z[32] * z[55];
z[64] = z[45] * z[46];
z[64] = (T(12) * z[47]) / T(5) + z[64];
z[55] = z[55] + T(3) * z[64];
z[55] = int_to_imaginary<T>(1) * z[55];
z[66] = z[12] * z[32];
z[66] = -z[46] + z[66];
z[66] = z[32] * z[66];
z[55] = z[55] + z[57] + z[58] + z[66];
z[55] = z[7] * z[55];
z[57] = -(z[0] * z[59]);
z[57] = z[57] + z[63] + z[118];
z[57] = z[6] * z[57];
z[58] = -(z[9] * z[119]);
z[57] = z[57] + z[58] + -z[120] + z[136];
z[57] = z[12] * z[57];
z[58] = z[3] + z[9];
z[58] = -z[6] + z[58] / T(2);
z[58] = z[58] / T(3) + -z[133];
z[58] = z[12] * z[58];
z[59] = z[49] / T(2);
z[63] = T(-5) * z[25] + T(-11) * z[85];
z[63] = (T(7) * z[33]) / T(3) + z[63] / T(2);
z[63] = z[59] * z[63];
z[66] = z[33] * z[45];
z[67] = (T(5) * z[23]) / T(2) + (T(7) * z[24]) / T(3);
z[67] = z[45] * z[67];
z[68] = z[45] / T(12) + z[49];
z[68] = z[14] * z[68];
z[58] = z[58] + z[63] + (T(-4) * z[66]) / T(3) + z[67] / T(2) + z[68];
z[58] = z[32] * z[58];
z[63] = z[6] + z[8];
z[67] = -z[5] + (T(-2) * z[9]) / T(3) + -z[63] + z[94];
z[67] = z[46] * z[67];
z[58] = z[58] + z[67];
z[58] = z[32] * z[58];
z[67] = T(-2) * z[6] + z[9];
z[67] = z[67] * z[81];
z[67] = z[67] + z[73] + z[82];
z[67] = z[12] * z[67];
z[68] = -z[25] + -z[85];
z[59] = z[59] * z[68];
z[68] = -(z[45] * z[100]);
z[59] = z[59] + z[66] + z[68];
z[59] = z[49] * z[59];
z[68] = -z[6] / T(2) + -z[9] / T(3) + -z[91];
z[68] = (T(-11) * z[5]) / T(30) + (T(11) * z[38]) / T(90) + (T(11) * z[68]) / T(15) + z[80] + -z[85] / T(2);
z[68] = -z[14] / T(4) + z[33] + z[68] / T(2);
z[68] = z[68] * z[114];
z[63] = -z[9] + (T(-3) * z[63]) / T(2);
z[63] = (T(-3) * z[5]) / T(20) + (T(-5) * z[33]) / T(2) + z[38] / T(20) + z[63] / T(10) + z[85];
z[63] = z[61] * z[63];
z[59] = z[59] + z[63] + z[67] + z[68];
z[59] = z[32] * z[59];
z[63] = T(3) * z[6] + T(2) * z[9] + -z[38] + z[140] + z[142];
z[63] = z[63] * z[64];
z[59] = z[59] + z[63];
z[59] = int_to_imaginary<T>(1) * z[59];
z[63] = -z[14] + T(3) * z[23] + T(4) * z[24];
z[63] = z[45] * z[63];
z[67] = z[33] * z[111];
z[63] = z[63] + -z[66] + T(-3) * z[67];
z[63] = z[50] * z[63];
z[66] = z[88] + T(-2) * z[112];
z[66] = z[11] * z[66];
z[61] = z[61] + (T(11) * z[113]) / T(27);
z[61] = z[32] * z[61];
z[67] = z[61] / T(20) + -z[64];
z[67] = int_to_imaginary<T>(1) * z[67];
z[68] = z[32] * z[46];
z[66] = z[66] + z[67] + z[68] / T(3);
z[66] = z[29] * z[66];
z[67] = prod_pow(z[45], 3);
z[69] = (T(5) * z[23]) / T(6) + z[24];
z[69] = z[67] * z[69];
z[70] = T(2) * z[51];
z[71] = T(2) * z[23] + T(3) * z[24];
z[71] = z[70] * z[71];
z[67] = z[67] / T(6);
z[70] = -z[67] + -z[70];
z[70] = z[14] * z[70];
z[61] = z[61] / T(5) + T(-4) * z[64];
z[61] = int_to_imaginary<T>(1) * z[61];
z[61] = z[61] + (T(4) * z[68]) / T(3);
z[61] = z[48] * z[61];
z[64] = T(-4) * z[51] + z[67];
z[64] = z[33] * z[64];
z[67] = z[33] / T(3) + -z[85];
z[67] = prod_pow(z[49], 3) * z[67];
z[68] = T(-127) * z[23] + T(-167) * z[24];
z[68] = z[25] + z[68] / T(3);
z[68] = T(13) * z[33] + z[68] / T(2);
z[68] = (T(5) * z[14]) / T(3) + z[68] / T(4);
z[68] = z[52] * z[68];
z[72] = -z[33] + T(3) * z[85];
z[72] = z[53] * z[72];
return z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + z[66] + T(2) * z[67] + z[68] + z[69] + z[70] + z[71] + z[72] / T(2) + z[76] + z[90];
}



template IntegrandConstructorType<double> f_4_129_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_129_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_129_construct (const Kin<qd_real>&);
#endif

}