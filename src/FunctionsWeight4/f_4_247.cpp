#include "f_4_247.h"

namespace PentagonFunctions {

template <typename T> T f_4_247_abbreviated (const std::array<T,18>&);



template <typename T> IntegrandConstructorType<T> f_4_247_construct (const Kin<T>& kin) {
    return [&kin, 
            dl6 = DLog_W_6<T>(kin),dl24 = DLog_W_24<T>(kin),dl17 = DLog_W_17<T>(kin),dl1 = DLog_W_1<T>(kin),dl18 = DLog_W_18<T>(kin),dl19 = DLog_W_19<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,18> abbr = 
            {dl6(t), rlog(kin.W[0] / kin_path.W[0]), rlog(v_path[0]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_9(kin_path), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[5] / kin_path.W[5]), rlog(kin.W[17] / kin_path.W[17]), dl24(t), rlog(kin.W[23] / kin_path.W[23]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl17(t), dl1(t), dl18(t), dl19(t)}
;

        auto result = f_4_247_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_247_abbreviated(const std::array<T,18>& abb)
{
using TR = typename T::value_type;
T z[47];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = bc<TR>[0];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = bc<TR>[1];
z[11] = bc<TR>[2];
z[12] = bc<TR>[4];
z[13] = bc<TR>[7];
z[14] = bc<TR>[8];
z[15] = bc<TR>[9];
z[16] = abb[14];
z[17] = abb[15];
z[18] = abb[16];
z[19] = abb[17];
z[20] = abb[9];
z[21] = abb[11];
z[22] = abb[12];
z[23] = abb[13];
z[24] = abb[10];
z[25] = z[1] + z[6];
z[26] = z[0] + -z[18];
z[27] = z[25] * z[26];
z[28] = z[0] * z[7];
z[29] = z[18] * z[24];
z[27] = -z[27] + z[28] + -z[29];
z[28] = z[8] + -z[9];
z[29] = T(3) * z[0] + -z[18];
z[29] = -z[17] + z[29] / T(4);
z[29] = z[28] * z[29];
z[30] = T(3) * z[7] + -z[24];
z[31] = -z[25] + z[30] / T(2);
z[32] = z[19] * z[31];
z[33] = z[17] * z[31];
z[34] = z[7] + z[24];
z[35] = -z[25] + z[34] / T(2);
z[35] = z[16] * z[35];
z[29] = z[27] / T(2) + z[29] + -z[32] + z[33] + z[35];
z[29] = prod_pow(z[2], 2) * z[29];
z[33] = T(2) * z[25];
z[34] = z[33] + -z[34];
z[35] = z[16] + z[17];
z[36] = z[34] * z[35];
z[37] = z[0] + z[18];
z[38] = z[37] / T(2);
z[39] = -(z[28] * z[38]);
z[34] = z[20] * z[34];
z[36] = z[27] + -z[34] + z[36] + z[39];
z[36] = z[2] * z[36];
z[39] = z[28] / T(4);
z[40] = -z[0] + T(7) * z[18];
z[39] = z[39] * z[40];
z[40] = -z[20] + z[35];
z[40] = z[31] * z[40];
z[39] = (T(-3) * z[27]) / T(2) + z[39] + z[40];
z[39] = z[3] * z[39];
z[36] = z[36] + z[39];
z[36] = z[3] * z[36];
z[30] = -z[30] + z[33];
z[33] = -(z[30] * z[35]);
z[30] = z[19] * z[30];
z[38] = -(z[9] * z[38]);
z[33] = -z[27] + z[30] + z[33] + z[38];
z[33] = z[5] * z[33];
z[30] = z[30] + -z[34];
z[34] = -z[7] + z[24];
z[38] = z[17] * z[34];
z[39] = z[16] * z[34];
z[40] = z[38] + z[39];
z[40] = -z[30] + T(2) * z[40];
z[41] = z[2] * z[40];
z[31] = z[20] * z[31];
z[31] = z[31] + -z[32] + -z[39];
z[31] = z[21] * z[31];
z[31] = z[31] + z[41];
z[31] = z[21] * z[31];
z[41] = T(2) * z[17];
z[42] = T(2) * z[16] + z[41];
z[43] = T(2) * z[20];
z[44] = T(-4) * z[19] + z[26] + z[42] + z[43];
z[44] = z[13] * z[44];
z[45] = -(z[22] * z[40]);
z[29] = z[29] + z[31] + z[33] + z[36] + z[44] + z[45];
z[31] = T(3) * z[19];
z[33] = T(4) * z[20] + (T(13) * z[26]) / T(4) + -z[31] + -z[35];
z[33] = z[10] * z[33];
z[36] = z[16] + -z[43];
z[44] = -z[7] + T(2) * z[24] + -z[25];
z[36] = z[36] * z[44];
z[44] = (T(13) * z[28]) / T(4);
z[45] = z[17] + -z[18];
z[44] = z[44] * z[45];
z[45] = T(-7) * z[7] + T(9) * z[24];
z[25] = -z[25] + z[45] / T(2);
z[25] = z[17] * z[25];
z[45] = z[26] / T(2);
z[46] = T(-17) * z[20] + T(5) * z[35] + -z[45];
z[46] = z[31] + z[46] / T(4);
z[46] = z[11] * z[46];
z[25] = z[25] + z[32] + z[33] + z[36] + z[44] + z[46];
z[32] = prod_pow(z[4], 2);
z[25] = z[25] * z[32];
z[33] = -z[27] + z[38];
z[36] = -z[26] + z[41];
z[36] = z[28] * z[36];
z[30] = -z[30] + T(2) * z[33] + z[36];
z[30] = z[2] * z[30];
z[27] = z[27] + z[38] + z[39];
z[33] = -z[0] + T(3) * z[18];
z[28] = -(z[28] * z[33]);
z[33] = -(z[34] * z[43]);
z[27] = T(2) * z[27] + z[28] + z[33];
z[27] = z[3] * z[27];
z[28] = -(z[23] * z[40]);
z[33] = T(-5) * z[19] + z[20] + -z[26] + T(4) * z[35];
z[33] = z[12] * z[33];
z[34] = (T(-7) * z[19]) / T(2) + (T(3) * z[20]) / T(2) + z[42] + z[45];
z[36] = prod_pow(z[10], 2);
z[34] = z[34] * z[36];
z[38] = z[19] + z[20];
z[38] = -z[35] + z[38] / T(2);
z[39] = z[11] * z[38];
z[40] = z[19] + -z[20];
z[41] = z[10] * z[40];
z[39] = z[39] + T(2) * z[41];
z[39] = z[11] * z[39];
z[27] = z[27] + z[28] + z[30] + z[33] + z[34] + z[39];
z[28] = T(2) * z[19] + -z[20] + -z[35];
z[28] = z[28] * z[32];
z[27] = T(3) * z[27] + z[28];
z[27] = int_to_imaginary<T>(1) * z[4] * z[27];
z[28] = z[26] + -z[40];
z[28] = z[12] * z[28];
z[30] = z[36] * z[38];
z[28] = T(3) * z[28] + z[30];
z[28] = z[10] * z[28];
z[30] = z[35] + z[45];
z[31] = -z[30] + z[31] + -z[43];
z[31] = prod_pow(z[11], 3) * z[31];
z[30] = (T(-3) * z[19]) / T(2) + z[20] + z[30] / T(2);
z[30] = z[14] * z[30];
z[32] = z[5] * z[8] * z[37];
z[30] = z[30] + z[32];
z[26] = T(-35) * z[20] + (T(-21) * z[26]) / T(2) + T(-19) * z[35];
z[26] = T(27) * z[19] + z[26] / T(2);
z[26] = z[15] * z[26];
return z[25] + z[26] / T(4) + z[27] + z[28] + T(3) * z[29] + (T(3) * z[30]) / T(2) + z[31];
}



template IntegrandConstructorType<double> f_4_247_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_247_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_247_construct (const Kin<qd_real>&);
#endif

}