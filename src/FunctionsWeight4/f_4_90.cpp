#include "f_4_90.h"

namespace PentagonFunctions {

template <typename T> T f_4_90_abbreviated (const std::array<T,29>&);

template <typename T> class SpDLog_f_4_90_W_23 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_90_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (-kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4]) + -prod_pow(kin.v[4], 2) + kin.v[2] * (-kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[3] * (-kin.v[3] + T(2) * kin.v[4]) + rlog(kin.v[3]) * ((-kin.v[4] / T(2) + (T(-3) * kin.v[0]) / T(4) + kin.v[2] / T(2) + (T(3) * kin.v[3]) / T(2) + -kin.v[1] + T(1)) * kin.v[0] + (-kin.v[3] / T(2) + -kin.v[4] / T(2) + kin.v[2] / T(4) + T(-1)) * kin.v[2] + ((T(-3) * kin.v[3]) / T(4) + kin.v[4] / T(2) + T(-1)) * kin.v[3] + kin.v[1] * (-kin.v[4] + kin.v[2] + kin.v[3]) + (kin.v[4] / T(4) + T(1)) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[0] * (-kin.v[2] / T(2) + (T(3) * kin.v[0]) / T(4) + (T(-3) * kin.v[3]) / T(2) + kin.v[4] / T(2) + T(-1) + kin.v[1]) + (-kin.v[2] / T(4) + kin.v[3] / T(2) + kin.v[4] / T(2) + T(1)) * kin.v[2] + (-kin.v[4] / T(2) + (T(3) * kin.v[3]) / T(4) + T(1)) * kin.v[3] + (-kin.v[4] / T(4) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]));
c[1] = rlog(kin.v[3]) * (-kin.v[0] + -kin.v[4] + kin.v[2] + kin.v[3]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[3] * abb[5] + -(prod_pow(abb[2], 2) * abb[5]) / T(2) + abb[4] * (prod_pow(abb[2], 2) / T(2) + -abb[3]) + abb[1] * (abb[2] * abb[4] + abb[1] * ((abb[4] * T(-3)) / T(2) + (abb[5] * T(3)) / T(2)) + -(abb[2] * abb[5])));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_90_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl16 = DLog_W_16<T>(kin),dl9 = DLog_W_9<T>(kin),dl18 = DLog_W_18<T>(kin),dl24 = DLog_W_24<T>(kin),dl6 = DLog_W_6<T>(kin),dl20 = DLog_W_20<T>(kin),dl17 = DLog_W_17<T>(kin),dl4 = DLog_W_4<T>(kin),dl1 = DLog_W_1<T>(kin),dl19 = DLog_W_19<T>(kin),spdl23 = SpDLog_f_4_90_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,29> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_8(kin_path), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[19] / kin_path.W[19]), dl16(t), rlog(kin.W[8] / kin_path.W[8]), dl9(t), rlog(v_path[3]), rlog(kin.W[16] / kin_path.W[16]), dl18(t), rlog(kin.W[5] / kin_path.W[5]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(kin.W[0] / kin_path.W[0]), dl24(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), rlog(kin.W[18] / kin_path.W[18]), dl6(t), rlog(v_path[0]), dl20(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl17(t), f_2_1_9(kin_path), dl4(t), dl1(t), dl19(t)}
;

        auto result = f_4_90_abbreviated(abbr);
        result = result + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_90_abbreviated(const std::array<T,29>& abb)
{
using TR = typename T::value_type;
T z[69];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[6];
z[3] = abb[8];
z[4] = abb[21];
z[5] = abb[24];
z[6] = abb[26];
z[7] = abb[5];
z[8] = abb[7];
z[9] = abb[10];
z[10] = abb[2];
z[11] = abb[28];
z[12] = abb[14];
z[13] = abb[15];
z[14] = abb[18];
z[15] = abb[9];
z[16] = bc<TR>[0];
z[17] = abb[20];
z[18] = abb[27];
z[19] = abb[3];
z[20] = abb[22];
z[21] = abb[23];
z[22] = abb[13];
z[23] = abb[19];
z[24] = abb[16];
z[25] = abb[17];
z[26] = abb[25];
z[27] = abb[11];
z[28] = abb[12];
z[29] = bc<TR>[1];
z[30] = bc<TR>[2];
z[31] = bc<TR>[4];
z[32] = bc<TR>[7];
z[33] = bc<TR>[8];
z[34] = bc<TR>[9];
z[35] = T(2) * z[12];
z[36] = z[9] + z[35];
z[37] = z[14] + z[36];
z[38] = T(2) * z[18];
z[39] = -(z[37] * z[38]);
z[40] = z[18] * z[28];
z[41] = z[14] + -z[36];
z[42] = z[5] * z[41];
z[43] = z[23] * z[41];
z[44] = z[11] * z[41];
z[39] = z[39] + T(4) * z[40] + z[42] + T(-2) * z[43] + z[44];
z[39] = z[17] * z[39];
z[45] = T(2) * z[1];
z[46] = -z[7] + z[45];
z[47] = -z[9] + -z[46];
z[48] = T(2) * z[6];
z[49] = z[47] * z[48];
z[50] = z[4] * z[47];
z[49] = z[49] + -z[50];
z[51] = z[18] * z[41];
z[52] = -z[12] + z[14];
z[53] = z[13] * z[52];
z[46] = -z[12] + z[46];
z[46] = z[5] * z[46];
z[46] = z[46] + -z[49] + z[51] + -z[53];
z[46] = z[10] * z[46];
z[52] = z[5] * z[52];
z[51] = z[51] + z[52];
z[52] = -z[12] + z[28];
z[52] = z[27] * z[52];
z[54] = -z[43] + z[51] + T(2) * z[52] + -z[53];
z[55] = -(z[22] * z[54]);
z[45] = z[9] + z[45];
z[56] = -z[8] + z[45];
z[56] = z[48] * z[56];
z[57] = z[5] * z[47];
z[58] = z[3] * z[47];
z[56] = z[56] + -z[57] + z[58];
z[56] = z[15] * z[56];
z[51] = -z[44] + z[51] + z[53];
z[59] = z[25] * z[51];
z[49] = z[49] + z[57];
z[57] = z[21] * z[49];
z[60] = z[5] + z[38];
z[61] = -z[11] + z[60];
z[62] = T(3) * z[31];
z[62] = z[61] * z[62];
z[63] = T(2) * z[23];
z[64] = z[5] / T(2);
z[65] = z[18] + z[64];
z[66] = (T(-5) * z[11]) / T(2) + z[63] + z[65];
z[66] = prod_pow(z[29], 2) * z[66];
z[67] = -(z[29] * z[61]);
z[68] = z[11] + -z[18];
z[68] = z[30] * z[68];
z[67] = z[67] + z[68] / T(2);
z[67] = z[30] * z[67];
z[39] = z[39] + z[46] + T(2) * z[55] + T(2) * z[56] + z[57] + z[59] + z[62] + z[66] + z[67];
z[39] = int_to_imaginary<T>(1) * z[39];
z[46] = z[9] / T(2);
z[55] = z[14] / T(2) + -z[46];
z[35] = (T(11) * z[1]) / T(2) + (T(-5) * z[7]) / T(2) + -z[35] + -z[55];
z[35] = z[5] * z[35];
z[46] = -z[1] + z[7] / T(2) + -z[46];
z[56] = z[4] * z[46];
z[55] = z[12] + -z[55];
z[57] = z[11] * z[55];
z[59] = T(-13) * z[8] + T(11) * z[9];
z[59] = T(11) * z[1] + z[7] + z[59] / T(2);
z[59] = z[6] * z[59];
z[35] = z[35] + -z[56] + -z[57] + z[59];
z[40] = z[40] + -z[52];
z[52] = T(-7) * z[5] + (T(-19) * z[18]) / T(2);
z[48] = z[4] + (T(17) * z[11]) / T(12) + -z[48] + z[52] / T(6);
z[48] = z[30] * z[48];
z[52] = z[1] + -z[8];
z[52] = z[2] * z[52];
z[36] = (T(-3) * z[14]) / T(2) + (T(-2) * z[36]) / T(3);
z[36] = z[18] * z[36];
z[59] = (T(-17) * z[11]) / T(6) + (T(13) * z[23]) / T(3) + T(-3) * z[65];
z[59] = z[29] * z[59];
z[62] = z[3] + -z[64];
z[62] = -z[4] / T(2) + T(5) * z[62];
z[62] = T(-3) * z[6] + z[62] / T(3);
z[62] = z[11] / T(3) + z[62] / T(2);
z[62] = int_to_imaginary<T>(1) * z[16] * z[62];
z[35] = z[35] / T(3) + z[36] + (T(13) * z[40]) / T(6) + z[48] + -z[52] / T(6) + (T(7) * z[53]) / T(6) + z[58] + z[59] / T(2) + z[62];
z[35] = z[16] * z[35];
z[35] = z[35] + z[39];
z[35] = z[16] * z[35];
z[36] = z[18] * z[37];
z[37] = z[5] * z[55];
z[39] = -(z[28] * z[38]);
z[36] = z[36] + z[37] + z[39] + z[43] + z[57];
z[36] = z[17] * z[36];
z[37] = z[38] * z[41];
z[37] = z[37] + z[42] + -z[44];
z[38] = -(z[10] * z[37]);
z[36] = z[36] + z[38];
z[36] = z[17] * z[36];
z[38] = -z[1] + z[14];
z[38] = z[38] * z[64];
z[39] = z[6] * z[46];
z[38] = z[38] + z[39] + z[53] + z[57];
z[38] = z[10] * z[38];
z[40] = z[15] * z[49];
z[38] = z[38] + z[40];
z[38] = z[10] * z[38];
z[40] = z[56] + -z[58];
z[41] = -z[1] + z[7];
z[42] = -(z[41] * z[64]);
z[39] = -z[39] + -z[40] + z[42] + T(2) * z[52];
z[39] = z[0] * z[39];
z[41] = z[5] * z[41];
z[42] = z[6] * z[47];
z[41] = z[41] + z[42] + -z[50];
z[42] = -(z[10] * z[41]);
z[39] = z[39] + z[42];
z[39] = z[0] * z[39];
z[42] = prod_pow(z[22], 2) * z[54];
z[43] = z[5] * z[46];
z[44] = -z[7] + T(2) * z[8] + -z[45];
z[44] = z[6] * z[44];
z[40] = z[40] + z[43] + z[44];
z[40] = prod_pow(z[15], 2) * z[40];
z[37] = z[26] * z[37];
z[43] = z[24] * z[51];
z[41] = z[19] * z[41];
z[44] = prod_pow(z[30], 3);
z[44] = T(2) * z[32] + (T(-2) * z[44]) / T(3);
z[44] = z[44] * z[61];
z[45] = z[11] + z[60] + -z[63];
z[45] = prod_pow(z[29], 3) * z[45];
z[46] = -(z[20] * z[49]);
z[47] = -z[11] / T(2) + z[65];
z[47] = z[33] * z[47];
z[48] = (T(-49) * z[5]) / T(3) + (T(41) * z[18]) / T(2);
z[48] = (T(-32) * z[6]) / T(3) + (T(35) * z[11]) / T(24) + z[48] / T(4);
z[48] = z[34] * z[48];
return z[35] + z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42] + z[43] + z[44] + z[45] / T(3) + z[46] + z[47] + z[48];
}



template IntegrandConstructorType<double> f_4_90_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_90_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_90_construct (const Kin<qd_real>&);
#endif

}