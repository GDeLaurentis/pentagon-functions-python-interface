#include "f_4_144.h"

namespace PentagonFunctions {

template <typename T> T f_4_144_abbreviated (const std::array<T,21>&);

template <typename T> class SpDLog_f_4_144_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_144_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + -kin.v[4] + T(-2) + T(2) * kin.v[0] + kin.v[2] + T(-3) * kin.v[3]) + (-kin.v[4] / T(2) + T(2)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + -kin.v[3] + T(-2) + kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(2) + kin.v[4]);
c[1] = kin.v[3] * (T(2) + T(-2) * kin.v[4]) + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[4] * (T(2) + T(-2) * kin.v[4]) + kin.v[2] * (T(-2) + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(4) * kin.v[4]) + kin.v[1] * (T(-2) + T(2) * kin.v[0] + T(-2) * kin.v[2] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) + T(4) * kin.v[0] + T(-4) * kin.v[2] + T(4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(4) + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(-4) * kin.v[4]) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-4) * kin.v[2] + T(4) * kin.v[3] + T(8) * kin.v[4])) + rlog(kin.v[2]) * (((T(3) * kin.v[2]) / T(4) + (T(-5) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-1)) * kin.v[2] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + ((T(7) * kin.v[3]) / T(4) + (T(5) * kin.v[4]) / T(2) + T(1)) * kin.v[3] + kin.v[1] * ((T(5) * kin.v[2]) / T(2) + (T(-7) * kin.v[3]) / T(2) + (T(-5) * kin.v[4]) / T(2) + T(-1) + kin.v[0] + (T(7) / T(4) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(2) * kin.v[0] + T(-2) * kin.v[3])) + ((T(3) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (((T(-3) * kin.v[2]) / T(4) + (T(5) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(1)) * kin.v[2] + ((T(-7) * kin.v[3]) / T(4) + (T(-5) * kin.v[4]) / T(2) + T(-1)) * kin.v[3] + kin.v[1] * ((T(-5) * kin.v[2]) / T(2) + (T(7) * kin.v[3]) / T(2) + (T(5) * kin.v[4]) / T(2) + -kin.v[0] + T(1) + (T(-7) / T(4) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3])) + ((T(-3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (((T(-3) * kin.v[2]) / T(4) + (T(5) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(1)) * kin.v[2] + ((T(-7) * kin.v[3]) / T(4) + (T(-5) * kin.v[4]) / T(2) + T(-1)) * kin.v[3] + kin.v[1] * ((T(-5) * kin.v[2]) / T(2) + (T(7) * kin.v[3]) / T(2) + (T(5) * kin.v[4]) / T(2) + -kin.v[0] + T(1) + (T(-7) / T(4) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3])) + ((T(-3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (((T(5) * kin.v[2]) / T(4) + (T(-19) * kin.v[3]) / T(2) + (T(-5) * kin.v[4]) / T(2) + T(-7)) * kin.v[2] + ((T(33) * kin.v[3]) / T(4) + (T(19) * kin.v[4]) / T(2) + T(7)) * kin.v[3] + kin.v[1] * ((T(19) * kin.v[2]) / T(2) + (T(-33) * kin.v[3]) / T(2) + (T(-19) * kin.v[4]) / T(2) + T(-7) + T(7) * kin.v[0] + (T(33) / T(4) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-6) + T(6) * kin.v[0] + T(-6) * kin.v[3])) + ((T(5) * kin.v[4]) / T(4) + T(7)) * kin.v[4] + kin.v[0] * (T(7) * kin.v[2] + T(-7) * kin.v[3] + T(-7) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[0] * (T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4])));
c[2] = T(2) * kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4];
c[3] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(3)) * kin.v[1] + T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(3)) * kin.v[1] + T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4])) + rlog(kin.v[2]) * ((T(-3) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-5) + bc<T>[0] * int_to_imaginary<T>(-6)) * kin.v[1] + T(-5) * kin.v[2] + T(5) * kin.v[3] + T(5) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[9] * (abb[3] * (abb[3] * (abb[12] + abb[1] / T(2) + abb[8] / T(2) + (abb[6] * T(-7)) / T(2)) + abb[1] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[6] * bc<T>[0] * int_to_imaginary<T>(6)) + abb[7] * (abb[11] * T(-3) + abb[3] * (-abb[3] / T(2) + bc<T>[0] * int_to_imaginary<T>(2)) + abb[3] * abb[13] * T(2)) + abb[11] * (abb[6] * T(-5) + abb[12] * T(-2) + abb[1] * T(3) + abb[8] * T(3)) + abb[10] * (abb[10] * ((abb[7] * T(-5)) / T(2) + (abb[6] * T(-3)) / T(2) + (abb[1] * T(5)) / T(2) + (abb[8] * T(5)) / T(2) + abb[12] * T(-3)) + abb[1] * bc<T>[0] * int_to_imaginary<T>(2) + abb[8] * bc<T>[0] * int_to_imaginary<T>(2) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-6) + abb[13] * (abb[6] * T(-6) + abb[1] * T(2) + abb[8] * T(2)) + abb[7] * (abb[13] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[3] * T(3)) + abb[3] * (abb[1] * T(-3) + abb[8] * T(-3) + abb[12] * T(2) + abb[6] * T(5))) + abb[3] * abb[13] * (abb[1] * T(-2) + abb[8] * T(-2) + abb[6] * T(6)) + abb[2] * (abb[3] * abb[7] * T(-2) + abb[3] * (abb[6] * T(-6) + abb[1] * T(2) + abb[8] * T(2)) + abb[10] * (abb[1] * T(-2) + abb[8] * T(-2) + abb[7] * T(2) + abb[6] * T(6))));
    }
};
template <typename T> class SpDLog_f_4_144_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_144_W_21 (const Kin<T>& kin) {
        c[0] = (T(3) / T(2) + (T(9) * kin.v[1]) / T(8) + (T(-3) * kin.v[2]) / T(4) + (T(-9) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2)) * kin.v[1] + (T(3) / T(2) + (T(-3) * kin.v[0]) / T(8) + (T(3) * kin.v[1]) / T(4) + (T(3) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2)) * kin.v[0] + (T(-3) / T(2) + (T(-3) * kin.v[2]) / T(8) + (T(3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2)) * kin.v[2] + (T(-3) / T(2) + (T(9) * kin.v[3]) / T(8) + (T(3) * kin.v[4]) / T(2)) * kin.v[3];
c[1] = (T(3) / T(2) + (T(3) * kin.v[2]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * kin.v[1] + kin.v[0] * (T(3) / T(2) + (T(-3) * kin.v[1]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + (T(-3) / T(2) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[0] + T(3) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) + T(-4) * kin.v[1] + T(8) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4])) + (T(-3) / T(2) + (T(3) * kin.v[4]) / T(2)) * kin.v[3] + (T(-3) / T(2) + (T(-3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2)) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(4) + T(4) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[3] * (T(-4) + T(4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4])) + rlog(kin.v[2]) * (kin.v[1] * ((T(5) * kin.v[1]) / T(2) + T(2) + T(-3) * kin.v[2] + T(-5) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[3] * ((T(5) * kin.v[3]) / T(2) + T(-2) + T(2) * kin.v[4]) + kin.v[2] * (kin.v[2] / T(2) + T(-2) + T(3) * kin.v[3] + T(2) * kin.v[4]) + kin.v[0] * (-kin.v[2] + T(2) + (T(1) / T(2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(3) * kin.v[1] + T(-3) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[3] * ((T(-5) * kin.v[3]) / T(2) + T(2) + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(2) + T(-3) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[1] * ((T(-5) * kin.v[1]) / T(2) + T(-2) + T(3) * kin.v[2] + T(5) * kin.v[3] + T(2) * kin.v[4]) + kin.v[0] * (T(-2) + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[0] + T(-3) * kin.v[1] + kin.v[2] + T(3) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(-2) * kin.v[2] + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[3] * ((T(-5) * kin.v[3]) / T(2) + T(2) + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(2) + T(-3) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[1] * ((T(-5) * kin.v[1]) / T(2) + T(-2) + T(3) * kin.v[2] + T(5) * kin.v[3] + T(2) * kin.v[4]) + kin.v[0] * (T(-2) + (T(-1) / T(2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[0] + T(-3) * kin.v[1] + kin.v[2] + T(3) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) + T(-2) * kin.v[2] + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(17) / T(2) + (T(75) * kin.v[1]) / T(8) + (T(-41) * kin.v[2]) / T(4) + (T(-75) * kin.v[3]) / T(4) + (T(-17) * kin.v[4]) / T(2)) * kin.v[1] + (T(-17) / T(2) + (T(7) * kin.v[2]) / T(8) + (T(41) * kin.v[3]) / T(4) + (T(17) * kin.v[4]) / T(2)) * kin.v[2] + (T(-17) / T(2) + (T(75) * kin.v[3]) / T(8) + (T(17) * kin.v[4]) / T(2)) * kin.v[3] + kin.v[0] * (T(17) / T(2) + (T(41) * kin.v[1]) / T(4) + (T(-7) * kin.v[2]) / T(4) + (T(-41) * kin.v[3]) / T(4) + (T(-17) * kin.v[4]) / T(2) + (T(7) / T(8) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(6) + T(6) * kin.v[2] + T(-6) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(6) + T(3) * kin.v[1] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[3] * (T(-6) + T(3) * kin.v[3] + T(6) * kin.v[4])));
c[2] = (T(-3) * kin.v[0]) / T(2) + (T(-3) * kin.v[1]) / T(2) + (T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2);
c[3] = rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(7) * kin.v[1]) / T(2) + (T(-7) * kin.v[2]) / T(2) + (T(-7) * kin.v[3]) / T(2) + (T(7) / T(2) + bc<T>[0] * int_to_imaginary<T>(6)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(6) * kin.v[1] + T(-6) * kin.v[2] + T(-6) * kin.v[3])) + rlog(kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(2)) * kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[3])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[14] * (abb[3] * (abb[3] * (abb[1] + abb[8] + (abb[6] * T(-17)) / T(4) + (abb[16] * T(3)) / T(4)) + abb[1] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[6] * bc<T>[0] * int_to_imaginary<T>(6)) + abb[15] * ((abb[6] * T(-7)) / T(2) + (abb[16] * T(-3)) / T(2) + abb[1] * T(2) + abb[8] * T(2)) + abb[7] * (abb[15] * T(-2) + abb[3] * (-abb[3] + bc<T>[0] * int_to_imaginary<T>(2)) + abb[3] * abb[10] * T(2)) + abb[13] * (abb[13] * (abb[1] + abb[8] + (abb[16] * T(-9)) / T(4) + (abb[6] * T(3)) / T(4) + -abb[7]) + abb[3] * ((abb[16] * T(3)) / T(2) + (abb[6] * T(7)) / T(2) + abb[1] * T(-2) + abb[8] * T(-2)) + abb[1] * bc<T>[0] * int_to_imaginary<T>(2) + abb[8] * bc<T>[0] * int_to_imaginary<T>(2) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-6) + abb[7] * (abb[10] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2) + abb[3] * T(2)) + abb[10] * (abb[6] * T(-6) + abb[1] * T(2) + abb[8] * T(2))) + abb[3] * abb[10] * (abb[1] * T(-2) + abb[8] * T(-2) + abb[6] * T(6)) + abb[2] * (abb[3] * abb[7] * T(-2) + abb[3] * (abb[6] * T(-6) + abb[1] * T(2) + abb[8] * T(2)) + abb[13] * (abb[1] * T(-2) + abb[8] * T(-2) + abb[7] * T(2) + abb[6] * T(6))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_144_construct (const Kin<T>& kin) {
    return [&kin, 
            dl8 = DLog_W_8<T>(kin),dl22 = DLog_W_22<T>(kin),dl21 = DLog_W_21<T>(kin),dl20 = DLog_W_20<T>(kin),dl3 = DLog_W_3<T>(kin),dl16 = DLog_W_16<T>(kin),dl19 = DLog_W_19<T>(kin),spdl22 = SpDLog_f_4_144_W_22<T>(kin),spdl21 = SpDLog_f_4_144_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,21> abbr = 
            {dl8(t), rlog(kin.W[18] / kin_path.W[18]), rlog(v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[15] / kin_path.W[15]), dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), f_2_1_14(kin_path), -rlog(t), rlog(-v_path[1] + v_path[3] + v_path[4]), dl21(t), f_2_1_15(kin_path), -rlog(t), dl20(t), dl3(t), dl16(t), dl19(t)}
;

        auto result = f_4_144_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_144_abbreviated(const std::array<T,21>& abb)
{
using TR = typename T::value_type;
T z[67];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = bc<TR>[0];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[17];
z[11] = abb[18];
z[12] = abb[19];
z[13] = abb[20];
z[14] = abb[10];
z[15] = abb[13];
z[16] = abb[11];
z[17] = abb[15];
z[18] = abb[12];
z[19] = abb[16];
z[20] = bc<TR>[2];
z[21] = bc<TR>[9];
z[22] = T(3) * z[7];
z[23] = T(2) * z[9];
z[24] = z[8] + -z[18];
z[25] = -z[1] + z[19] + z[22] + -z[23] + z[24];
z[25] = z[11] * z[25];
z[26] = z[13] * z[24];
z[25] = z[25] + -z[26];
z[26] = T(2) * z[19];
z[27] = z[23] + -z[26];
z[28] = z[1] + z[7];
z[29] = z[27] + z[28];
z[29] = z[12] * z[29];
z[30] = T(4) * z[18];
z[31] = T(5) * z[8] + -z[30];
z[32] = T(4) * z[19];
z[33] = T(2) * z[1] + T(-10) * z[7] + z[9] + -z[31] + z[32];
z[33] = z[10] * z[33];
z[34] = z[8] + z[9];
z[35] = -z[28] + z[34];
z[35] = z[0] * z[35];
z[25] = T(2) * z[25] + z[29] + -z[33] + z[35];
z[25] = z[3] * z[25];
z[29] = T(5) * z[7];
z[36] = T(3) * z[1];
z[37] = T(2) * z[8];
z[38] = T(4) * z[9] + -z[26] + -z[29] + z[36] + -z[37];
z[39] = z[10] * z[38];
z[40] = T(2) * z[11];
z[41] = T(2) * z[12];
z[42] = z[40] + z[41];
z[43] = T(2) * z[18];
z[44] = z[28] + -z[43];
z[42] = z[42] * z[44];
z[27] = z[27] + -z[28] + z[30];
z[27] = z[13] * z[27];
z[27] = z[27] + -z[39] + z[42];
z[30] = z[14] * z[27];
z[23] = -z[1] + z[23];
z[39] = T(6) * z[19];
z[42] = z[22] + z[23] + z[37] + -z[39] + z[43];
z[45] = -z[12] + z[13];
z[42] = z[42] * z[45];
z[45] = z[7] + z[43];
z[36] = T(5) * z[9] + z[36];
z[39] = z[8] + -z[36] + z[39] + z[45];
z[39] = z[10] * z[39];
z[46] = T(3) * z[19];
z[47] = z[1] + z[9];
z[48] = -z[8] + -z[18] + -z[46] + T(2) * z[47];
z[40] = z[40] * z[48];
z[39] = -z[35] + z[39] + z[40] + z[42];
z[40] = -(z[6] * z[39]);
z[42] = T(2) * z[7] + -z[26];
z[49] = -z[34] + -z[42] + z[43];
z[49] = z[10] * z[49];
z[44] = z[12] * z[44];
z[50] = z[7] + -z[19];
z[51] = z[13] * z[50];
z[44] = z[35] + z[44] + z[49] + T(2) * z[51];
z[49] = T(2) * z[2];
z[44] = z[44] * z[49];
z[52] = z[11] * z[50];
z[51] = z[51] + z[52];
z[52] = -z[18] + z[37];
z[53] = -z[47] + z[52];
z[54] = z[22] + z[53];
z[55] = z[10] * z[54];
z[56] = z[24] + z[42];
z[56] = z[12] * z[56];
z[51] = T(2) * z[51] + z[55] + -z[56];
z[55] = T(2) * z[15];
z[57] = z[51] * z[55];
z[58] = -z[11] + z[13];
z[59] = z[10] + -z[12] + z[58];
z[60] = prod_pow(z[20], 2) * z[59];
z[30] = -z[25] + z[30] + z[40] + z[44] + z[57] + -z[60] / T(2);
z[30] = int_to_imaginary<T>(1) * z[30];
z[40] = z[1] / T(2);
z[44] = z[40] + z[43];
z[57] = (T(11) * z[7]) / T(4);
z[60] = T(4) * z[8];
z[61] = -z[9] + (T(-5) * z[19]) / T(4) + z[44] + z[57] + -z[60];
z[61] = z[13] * z[61];
z[62] = -z[1] + z[7] + -z[18];
z[63] = (T(2) * z[19]) / T(3);
z[62] = z[8] + -z[9] + (T(2) * z[62]) / T(3) + z[63];
z[62] = z[11] * z[62];
z[28] = -z[8] / T(3) + (T(2) * z[9]) / T(3) + -z[18] + z[28] + -z[63];
z[28] = z[12] * z[28];
z[63] = (T(-19) * z[7]) / T(12) + (T(-2) * z[8]) / T(3) + z[18] + (T(5) * z[19]) / T(12) + z[40];
z[63] = z[10] * z[63];
z[59] = z[20] * z[59];
z[64] = (T(-5) * z[10]) / T(6) + -z[11] / T(3) + z[13] / T(2);
z[64] = int_to_imaginary<T>(1) * z[4] * z[64];
z[28] = z[28] + (T(4) * z[35]) / T(3) + (T(-5) * z[59]) / T(4) + z[61] / T(3) + z[62] + z[63] + z[64] / T(2);
z[28] = z[4] * z[28];
z[28] = z[28] + z[30];
z[28] = z[4] * z[28];
z[27] = -(z[2] * z[27]);
z[30] = -z[8] + z[26] + z[45] + -z[47];
z[30] = z[11] * z[30];
z[45] = z[24] + z[50];
z[59] = z[13] * z[45];
z[61] = -z[8] + z[47];
z[22] = z[22] + -z[61];
z[22] = z[10] * z[22];
z[45] = z[41] * z[45];
z[22] = z[22] + z[30] + z[45] + T(2) * z[59];
z[22] = z[22] * z[55];
z[30] = z[9] + -z[37] + z[44];
z[37] = (T(3) * z[7]) / T(2);
z[44] = -z[19] + -z[30] + z[37];
z[44] = z[11] * z[44];
z[45] = z[7] / T(2) + z[19] + z[40];
z[55] = z[9] + z[43] + -z[45];
z[55] = z[13] * z[55];
z[62] = T(3) * z[8];
z[36] = -z[36] + z[62];
z[63] = (T(5) * z[7]) / T(2);
z[64] = z[19] + z[63];
z[36] = z[36] / T(2) + z[64];
z[36] = z[10] * z[36];
z[65] = z[50] + z[53];
z[66] = -(z[12] * z[65]);
z[36] = z[36] + z[44] + z[55] + z[66];
z[36] = z[14] * z[36];
z[38] = z[11] * z[38];
z[44] = -z[9] + -z[18] + z[19];
z[41] = z[41] * z[44];
z[33] = z[33] + z[38] + z[41];
z[33] = z[3] * z[33];
z[22] = z[22] + z[27] + z[33] + z[36];
z[22] = z[14] * z[22];
z[27] = z[26] + -z[37] + z[40] + -z[52];
z[27] = z[12] * z[27];
z[30] = (T(11) * z[7]) / T(2) + -z[26] + -z[30];
z[30] = z[11] * z[30];
z[33] = T(-2) * z[24] + -z[50];
z[33] = z[13] * z[33];
z[36] = (T(35) * z[7]) / T(4) + (T(-17) * z[19]) / T(4) + z[31] + -z[47];
z[36] = z[10] * z[36];
z[27] = z[27] + z[30] + z[33] + z[35] / T(2) + z[36];
z[27] = prod_pow(z[3], 2) * z[27];
z[30] = -z[9] + -z[24] + -z[40] + z[46] + -z[63];
z[30] = z[12] * z[30];
z[33] = z[11] * z[48];
z[34] = z[18] + z[34] + -z[45];
z[34] = z[13] * z[34];
z[36] = -z[18] + (T(-3) * z[61]) / T(2) + z[64];
z[36] = z[10] * z[36];
z[30] = z[30] + z[33] + z[34] + (T(-3) * z[35]) / T(2) + z[36];
z[30] = z[2] * z[30];
z[25] = z[25] + z[30];
z[25] = z[2] * z[25];
z[30] = -(z[11] * z[54]);
z[30] = z[30] + z[59];
z[33] = z[43] + z[47] + -z[62];
z[34] = (T(-21) * z[7]) / T(2) + (T(7) * z[19]) / T(2);
z[33] = T(2) * z[33] + z[34];
z[33] = z[10] * z[33];
z[30] = T(2) * z[30] + z[33];
z[30] = z[3] * z[30];
z[33] = -(z[49] * z[51]);
z[24] = T(3) * z[24];
z[35] = z[24] + z[42];
z[35] = -(z[35] * z[58]);
z[36] = -z[19] / T(4) + z[53] + z[57];
z[36] = z[10] * z[36];
z[35] = z[35] + z[36] + -z[56];
z[35] = z[15] * z[35];
z[30] = z[30] + z[33] + z[35];
z[30] = z[15] * z[30];
z[33] = z[5] * z[39];
z[23] = T(8) * z[18] + z[23] + z[26] + -z[29] + -z[60];
z[23] = z[16] * z[23];
z[29] = T(-4) * z[7] + -z[24] + z[32];
z[32] = T(2) * z[17];
z[29] = z[29] * z[32];
z[35] = (T(-5) * z[21]) / T(8) + -z[23] + -z[29];
z[35] = z[11] * z[35];
z[24] = -z[24] + -z[50];
z[24] = z[24] * z[32];
z[23] = (T(25) * z[21]) / T(8) + z[23] + z[24];
z[23] = z[13] * z[23];
z[24] = T(9) * z[7] + -z[26] + z[31] + T(-3) * z[47];
z[24] = z[16] * z[24];
z[26] = T(-3) * z[18] + -z[47] + z[60];
z[26] = T(2) * z[26] + -z[34];
z[26] = z[17] * z[26];
z[24] = (T(-71) * z[21]) / T(8) + z[24] + z[26];
z[24] = z[10] * z[24];
z[26] = z[16] * z[65];
z[26] = (T(3) * z[21]) / T(8) + T(-2) * z[26] + z[29];
z[26] = z[12] * z[26];
return z[22] + z[23] + z[24] + z[25] + z[26] + z[27] + z[28] + z[30] + z[33] + z[35];
}



template IntegrandConstructorType<double> f_4_144_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_144_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_144_construct (const Kin<qd_real>&);
#endif

}