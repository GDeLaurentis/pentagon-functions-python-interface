#include "f_4_192.h"

namespace PentagonFunctions {

template <typename T> T f_4_192_abbreviated (const std::array<T,39>&);

template <typename T> class SpDLog_f_4_192_W_7 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_192_W_7 (const Kin<T>& kin) {
        c[0] = T(-9) * kin.v[1] * kin.v[4] + T(-3) * kin.v[2] * kin.v[4] + ((T(21) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + T(-3) * kin.v[1] * kin.v[4]) + kin.v[3] * ((T(27) * kin.v[4]) / T(2) + T(-9) + T(-9) * kin.v[1] + T(-3) * kin.v[2] + (T(33) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(-3) * kin.v[1] + T(3) * kin.v[4]));
c[1] = (T(-9) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[3] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-3) * kin.v[4];
c[2] = T(-9) * kin.v[1] * kin.v[4] + T(-3) * kin.v[2] * kin.v[4] + ((T(21) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + T(-3) * kin.v[1] * kin.v[4]) + kin.v[3] * ((T(27) * kin.v[4]) / T(2) + T(-9) + T(-9) * kin.v[1] + T(-3) * kin.v[2] + (T(33) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(-3) * kin.v[1] + T(3) * kin.v[4]));
c[3] = (T(-9) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[3] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-3) * kin.v[4];
c[4] = T(-9) * kin.v[1] * kin.v[4] + T(-3) * kin.v[2] * kin.v[4] + ((T(21) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + T(-3) * kin.v[1] * kin.v[4]) + kin.v[3] * ((T(27) * kin.v[4]) / T(2) + T(-9) + T(-9) * kin.v[1] + T(-3) * kin.v[2] + (T(33) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(-3) * kin.v[1] + T(3) * kin.v[4]));
c[5] = (T(-9) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[3] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-3) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[1] * (t * c[0] + c[1]) + abb[11] * (t * c[2] + c[3]) + abb[12] * (t * c[4] + c[5]);
        }

        return abb[6] * (abb[2] * abb[3] * (abb[11] * T(-3) + abb[12] * T(-3)) + abb[3] * abb[8] * (abb[11] * T(3) + abb[12] * T(3)) + abb[3] * (abb[11] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[9] * (abb[11] * T(3) + abb[12] * T(3))) + abb[7] * (abb[3] * (abb[11] * T(-3) + abb[12] * T(-3)) + abb[8] * (abb[11] * T(-3) + abb[12] * T(-3)) + abb[9] * (abb[11] * T(-3) + abb[12] * T(-3)) + abb[11] * bc<T>[0] * int_to_imaginary<T>(3) + abb[12] * bc<T>[0] * int_to_imaginary<T>(3) + abb[1] * (abb[3] * T(-3) + abb[8] * T(-3) + abb[9] * T(-3) + bc<T>[0] * int_to_imaginary<T>(3) + abb[2] * T(3)) + abb[2] * (abb[11] * T(3) + abb[12] * T(3)) + abb[7] * (abb[1] * T(3) + abb[11] * T(3) + abb[12] * T(3))) + abb[1] * (abb[2] * abb[3] * T(-3) + abb[3] * abb[8] * T(3) + abb[3] * (bc<T>[0] * int_to_imaginary<T>(-3) + abb[9] * T(3)) + abb[10] * T(9)) + abb[10] * (abb[11] * T(9) + abb[12] * T(9)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_192_construct (const Kin<T>& kin) {
    return [&kin, 
            dl3 = DLog_W_3<T>(kin),dl7 = DLog_W_7<T>(kin),dl20 = DLog_W_20<T>(kin),dl17 = DLog_W_17<T>(kin),dl16 = DLog_W_16<T>(kin),dl14 = DLog_W_14<T>(kin),dl19 = DLog_W_19<T>(kin),dl4 = DLog_W_4<T>(kin),dl9 = DLog_W_9<T>(kin),dl18 = DLog_W_18<T>(kin),dl5 = DLog_W_5<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl28 = DLog_W_28<T>(kin),dl30 = DLog_W_30<T>(kin),dl26 = DLog_W_26<T>(kin),dl29 = DLog_W_29<T>(kin),dl27 = DLog_W_27<T>(kin),spdl7 = SpDLog_f_4_192_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,39> abbr = 
            {dl3(t), f_1_3_2(kin) - f_1_3_2(kin_path), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_14(kin_path), f_2_1_15(kin_path), dl7(t), rlog(-v_path[1]), rlog(v_path[3]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_11(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl20(t), f_1_3_5(kin) - f_1_3_5(kin_path), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), f_1_3_1(kin) - f_1_3_1(kin_path), dl17(t), dl16(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl14(t), dl19(t), dl4(t), dl9(t), dl18(t), dl5(t), dl2(t), dl1(t), dl28(t) / kin_path.SqrtDelta, rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[17] / kin_path.W[17]), dl30(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta}
;

        auto result = f_4_192_abbreviated(abbr);
        result = result + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_192_abbreviated(const std::array<T,39>& abb)
{
using TR = typename T::value_type;
T z[86];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = bc<TR>[0];
z[7] = abb[13];
z[8] = abb[18];
z[9] = abb[19];
z[10] = abb[23];
z[11] = abb[24];
z[12] = abb[28];
z[13] = abb[7];
z[14] = abb[29];
z[15] = abb[8];
z[16] = abb[9];
z[17] = abb[26];
z[18] = abb[27];
z[19] = abb[10];
z[20] = abb[15];
z[21] = abb[16];
z[22] = abb[20];
z[23] = abb[21];
z[24] = abb[12];
z[25] = abb[17];
z[26] = abb[14];
z[27] = abb[30];
z[28] = abb[31];
z[29] = abb[32];
z[30] = abb[33];
z[31] = abb[34];
z[32] = abb[35];
z[33] = abb[36];
z[34] = abb[37];
z[35] = abb[11];
z[36] = abb[25];
z[37] = abb[38];
z[38] = abb[22];
z[39] = bc<TR>[3];
z[40] = bc<TR>[2];
z[41] = bc<TR>[9];
z[42] = bc<TR>[1];
z[43] = bc<TR>[4];
z[44] = z[2] / T(2);
z[45] = -z[15] + z[44];
z[46] = -z[3] + z[13];
z[47] = z[45] + -z[46];
z[47] = z[2] * z[47];
z[48] = z[4] + z[20];
z[49] = z[3] + z[15];
z[50] = z[13] / T(2) + -z[49];
z[50] = z[13] * z[50];
z[51] = prod_pow(z[3], 2);
z[51] = z[5] + z[51] / T(2);
z[52] = z[15] / T(2);
z[53] = -z[3] + z[52];
z[53] = z[15] * z[53];
z[47] = z[47] + z[48] + -z[50] + -z[51] + z[53];
z[53] = int_to_imaginary<T>(1) * z[6];
z[54] = T(3) * z[53];
z[55] = z[21] * z[54];
z[56] = prod_pow(z[6], 2);
z[57] = z[56] / T(2);
z[58] = z[55] + z[57];
z[59] = z[2] + -z[46];
z[59] = z[54] * z[59];
z[47] = T(3) * z[47] + -z[58] + z[59];
z[47] = z[7] * z[47];
z[59] = -z[15] + z[46];
z[60] = z[44] + z[59];
z[60] = z[2] * z[60];
z[61] = prod_pow(z[15], 2);
z[62] = z[61] / T(2);
z[63] = z[20] + z[62];
z[64] = z[16] * z[46];
z[65] = z[4] + -z[22];
z[66] = -z[60] + -z[63] + z[64] + -z[65];
z[67] = T(3) * z[19];
z[68] = -z[56] + z[67];
z[69] = z[2] + z[13];
z[70] = -z[23] + z[69];
z[71] = -(z[54] * z[70]);
z[66] = z[55] + T(3) * z[66] + -z[68] + z[71];
z[66] = z[11] * z[66];
z[71] = z[13] + -z[15];
z[72] = z[44] + z[71];
z[72] = z[2] * z[72];
z[73] = z[2] + -z[15];
z[74] = z[46] + z[73];
z[74] = z[16] * z[74];
z[74] = z[51] + z[74];
z[65] = z[65] + z[72] + -z[74];
z[72] = z[16] + z[23];
z[69] = z[69] + -z[72];
z[69] = z[54] * z[69];
z[65] = T(3) * z[65] + z[68] + z[69];
z[68] = -(z[10] * z[65]);
z[69] = prod_pow(z[13], 2);
z[69] = z[69] / T(2);
z[75] = z[51] + z[64] + -z[69];
z[75] = -z[57] + -z[67] + T(3) * z[75];
z[76] = z[17] * z[75];
z[77] = -z[3] + z[23];
z[78] = z[53] * z[77];
z[59] = z[13] * z[59];
z[79] = z[3] * z[15];
z[59] = z[59] + z[79];
z[80] = z[22] + z[59];
z[78] = z[78] + z[80];
z[81] = T(3) * z[18];
z[78] = z[78] * z[81];
z[81] = prod_pow(z[2], 2);
z[81] = z[4] + -z[20] + z[81] / T(2);
z[82] = z[16] * z[73];
z[83] = z[62] + -z[81] + z[82];
z[84] = z[16] * z[54];
z[58] = -z[58] + T(3) * z[83] + z[84];
z[83] = z[8] * z[58];
z[84] = T(-3) * z[7] + -z[18];
z[84] = z[67] * z[84];
z[85] = -z[3] + z[44];
z[85] = z[2] * z[85];
z[85] = z[4] + z[51] + z[85];
z[85] = z[57] + T(3) * z[85];
z[85] = z[0] * z[85];
z[47] = -z[47] + -z[66] + -z[68] + z[76] + z[78] + z[83] + -z[84] + -z[85];
z[66] = z[3] + z[52];
z[66] = z[15] * z[66];
z[68] = z[22] + z[66] + -z[81];
z[73] = -z[46] + z[73];
z[73] = z[16] * z[73];
z[72] = -z[3] + z[72];
z[81] = z[53] * z[72];
z[84] = z[21] * z[53];
z[85] = (T(3) * z[13]) / T(2) + -z[49];
z[85] = z[13] * z[85];
z[73] = -z[51] + z[68] + z[73] + z[81] + -z[84] + z[85];
z[73] = z[19] + z[73] / T(2);
z[73] = z[9] * z[73];
z[47] = -z[47] / T(2) + T(3) * z[73];
z[47] = z[1] * z[47];
z[73] = z[48] + -z[57] + -z[84];
z[81] = z[19] + z[50] + z[51];
z[85] = z[2] + z[46];
z[85] = z[53] * z[85];
z[60] = z[60] + z[66] + z[73] + z[81] + z[85];
z[85] = T(3) * z[34];
z[60] = z[60] * z[85];
z[50] = z[50] + z[68] + z[74];
z[68] = z[54] * z[72];
z[50] = T(3) * z[50] + -z[55] + -z[56] + z[68];
z[50] = z[32] * z[50];
z[65] = z[27] * z[65];
z[68] = z[37] * z[75];
z[74] = z[33] * z[58];
z[50] = z[50] + z[60] + z[65] + -z[68] + z[74];
z[60] = -z[28] + -z[29] + z[30] + z[31];
z[60] = z[60] / T(2);
z[50] = z[50] * z[60];
z[52] = -z[44] + z[46] + z[52];
z[52] = z[16] * z[52];
z[51] = -z[20] + -z[22] + z[51] + -z[66];
z[49] = -z[13] + z[49] / T(2);
z[49] = z[13] * z[49];
z[44] = z[3] * z[44];
z[44] = z[44] + z[49] + z[51] / T(2) + z[52];
z[49] = (T(3) * z[53]) / T(2);
z[51] = -(z[49] * z[72]);
z[52] = (T(3) * z[84]) / T(2);
z[44] = (T(-9) * z[19]) / T(2) + T(3) * z[44] + z[51] + z[52] + -z[57];
z[44] = z[1] * z[44];
z[51] = -z[2] + -z[53];
z[51] = z[46] * z[51];
z[51] = z[51] + -z[79] + -z[81];
z[51] = z[24] * z[51];
z[60] = -(z[2] * z[46]);
z[59] = -z[59] + z[60] + z[64];
z[46] = -(z[46] * z[54]);
z[46] = T(-6) * z[19] + z[46] + -z[57] + T(3) * z[59];
z[46] = z[35] * z[46];
z[59] = -(z[26] * z[75]);
z[60] = T(3) * z[6];
z[60] = z[39] * z[60];
z[64] = int_to_imaginary<T>(1) * prod_pow(z[6], 3);
z[60] = -z[60] + z[64] / T(9);
z[44] = z[44] + z[46] + T(3) * z[51] + z[59] + z[60];
z[44] = z[12] * z[44];
z[45] = z[2] * z[45];
z[45] = z[45] + z[62];
z[46] = z[45] + z[48];
z[48] = z[2] * z[54];
z[46] = T(3) * z[46] + z[48] + -z[55] + -z[56] + -z[67];
z[46] = z[11] * z[46];
z[48] = z[2] * z[15];
z[48] = T(-2) * z[20] + z[48] + -z[61] + -z[82];
z[51] = z[2] + z[16];
z[51] = z[51] * z[54];
z[48] = T(-3) * z[48] + z[51] + T(-2) * z[56] + T(-6) * z[84];
z[48] = z[36] * z[48];
z[51] = -z[57] + z[67];
z[55] = z[10] * z[51];
z[46] = z[46] + -z[48] + -z[55] + z[83];
z[51] = z[9] * z[51];
z[55] = z[46] + z[51];
z[55] = z[24] * z[55];
z[59] = z[2] * z[53];
z[45] = z[45] + z[59] + z[73];
z[45] = z[7] * z[45];
z[58] = z[9] * z[58];
z[45] = T(3) * z[45] + -z[48] + z[58];
z[45] = z[25] * z[45];
z[48] = z[54] * z[77];
z[48] = z[48] + z[57] + T(3) * z[80];
z[48] = z[38] * z[48];
z[58] = z[7] + z[18];
z[58] = z[58] * z[67];
z[59] = z[7] * z[57];
z[48] = z[48] + -z[58] + z[59] + -z[78];
z[46] = z[46] + -z[48] + z[76];
z[46] = z[26] * z[46];
z[48] = z[48] + z[51];
z[48] = z[35] * z[48];
z[51] = z[2] * z[71];
z[51] = -z[22] + z[51] + z[63] + -z[69];
z[58] = z[49] * z[70];
z[51] = (T(3) * z[51]) / T(2) + -z[52] + -z[56] + z[58];
z[51] = z[1] * z[51];
z[51] = z[51] + z[60];
z[51] = z[14] * z[51];
z[52] = z[27] + z[32];
z[52] = z[33] + z[52] / T(2);
z[58] = z[52] * z[56];
z[52] = z[40] * z[52] * z[53];
z[52] = z[52] + z[58] / T(2);
z[52] = z[40] * z[52];
z[58] = z[24] * z[75];
z[58] = z[58] + -z[60];
z[58] = z[17] * z[58];
z[54] = -(z[40] * z[54]);
z[49] = z[42] * z[49];
z[49] = z[49] + z[54] + -z[56];
z[49] = z[42] * z[49];
z[54] = T(3) * z[43] + -z[56] / T(4);
z[54] = z[53] * z[54];
z[49] = (T(19) * z[41]) / T(2) + z[49] + z[54];
z[49] = z[37] * z[49];
z[54] = (T(-15) * z[41]) / T(2) + z[64];
z[54] = z[32] * z[54];
z[56] = (T(-87) * z[41]) / T(2) + -z[64];
z[56] = z[27] * z[56];
z[54] = z[54] + z[56];
z[56] = -(z[18] * z[60]);
z[53] = z[40] * z[53];
z[53] = z[53] + z[57];
z[53] = z[40] * z[53];
z[53] = (T(-17) * z[41]) / T(4) + z[53];
z[53] = z[53] * z[85];
z[57] = z[33] * z[41];
return z[44] + z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + z[51] + T(3) * z[52] + z[53] + z[54] / T(4) + z[55] + z[56] + (T(-3) * z[57]) / T(4) + z[58];
}



template IntegrandConstructorType<double> f_4_192_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_192_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_192_construct (const Kin<qd_real>&);
#endif

}