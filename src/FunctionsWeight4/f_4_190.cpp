#include "f_4_190.h"

namespace PentagonFunctions {

template <typename T> T f_4_190_abbreviated (const std::array<T,39>&);

template <typename T> class SpDLog_f_4_190_W_23 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_190_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * ((T(-3) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-9) + (T(15) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[0] + T(9) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3])) + ((T(-21) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + ((T(3) * kin.v[3]) / T(4) + (T(9) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + ((T(-9) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(15) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + kin.v[1] * (T(-9) * kin.v[2] + T(-9) * kin.v[3] + T(9) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]));
c[1] = (T(-9) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + T(9) * kin.v[2] + T(9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + T(-9) * kin.v[4];
c[2] = kin.v[0] * ((T(-3) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-9) + (T(15) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[0] + T(9) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3])) + ((T(-21) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + ((T(3) * kin.v[3]) / T(4) + (T(9) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + ((T(-9) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(15) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + kin.v[1] * (T(-9) * kin.v[2] + T(-9) * kin.v[3] + T(9) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]));
c[3] = (T(-9) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + T(9) * kin.v[2] + T(9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + T(-9) * kin.v[4];
c[4] = kin.v[1] * (T(9) * kin.v[2] + T(9) * kin.v[3] + T(-9) * kin.v[4]) + ((T(9) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(-15) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + ((T(-3) * kin.v[3]) / T(4) + (T(-9) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + kin.v[0] * ((T(3) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(9) + (T(-15) / T(4) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[0] + T(-9) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3])) + ((T(21) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]));
c[5] = (bc<T>[0] * int_to_imaginary<T>(3) + T(9)) * kin.v[0] + T(-9) * kin.v[2] + T(-9) * kin.v[3] + T(9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]);
c[6] = kin.v[1] * (T(9) * kin.v[2] + T(9) * kin.v[3] + T(-9) * kin.v[4]) + ((T(9) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(-15) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + ((T(-3) * kin.v[3]) / T(4) + (T(-9) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + kin.v[0] * ((T(3) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(9) + (T(-15) / T(4) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[0] + T(-9) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3])) + ((T(21) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]));
c[7] = (bc<T>[0] * int_to_imaginary<T>(3) + T(9)) * kin.v[0] + T(-9) * kin.v[2] + T(-9) * kin.v[3] + T(9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[10] * (t * c[0] + c[1]) + abb[8] * (t * c[2] + c[3]) + abb[1] * (t * c[4] + c[5]) + abb[7] * (t * c[6] + c[7]);
        }

        return abb[9] * (abb[10] * (abb[13] * T(-9) + abb[3] * abb[4] * T(-3) + abb[4] * abb[12] * T(-3) + abb[4] * bc<T>[0] * int_to_imaginary<T>(3)) + abb[4] * (abb[1] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[7] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[8] * bc<T>[0] * int_to_imaginary<T>(3)) + abb[3] * abb[4] * (abb[8] * T(-3) + abb[1] * T(3) + abb[7] * T(3)) + abb[4] * abb[12] * (abb[8] * T(-3) + abb[1] * T(3) + abb[7] * T(3)) + abb[2] * (abb[4] * abb[10] * T(3) + abb[11] * (abb[8] * T(-3) + abb[10] * T(-3) + abb[1] * T(3) + abb[7] * T(3)) + abb[4] * (abb[1] * T(-3) + abb[7] * T(-3) + abb[8] * T(3))) + abb[11] * (abb[8] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[1] * bc<T>[0] * int_to_imaginary<T>(3) + abb[7] * bc<T>[0] * int_to_imaginary<T>(3) + abb[11] * (abb[8] * T(-3) + abb[10] * T(-3) + abb[1] * T(3) + abb[7] * T(3)) + abb[3] * (abb[1] * T(-3) + abb[7] * T(-3) + abb[8] * T(3)) + abb[4] * (abb[1] * T(-3) + abb[7] * T(-3) + abb[8] * T(3)) + abb[12] * (abb[1] * T(-3) + abb[7] * T(-3) + abb[8] * T(3)) + abb[10] * (bc<T>[0] * int_to_imaginary<T>(-3) + abb[3] * T(3) + abb[4] * T(3) + abb[12] * T(3))) + abb[13] * (abb[8] * T(-9) + abb[1] * T(9) + abb[7] * T(9)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_190_construct (const Kin<T>& kin) {
    return [&kin, 
            dl3 = DLog_W_3<T>(kin),dl23 = DLog_W_23<T>(kin),dl9 = DLog_W_9<T>(kin),dl17 = DLog_W_17<T>(kin),dl19 = DLog_W_19<T>(kin),dl2 = DLog_W_2<T>(kin),dl14 = DLog_W_14<T>(kin),dl4 = DLog_W_4<T>(kin),dl5 = DLog_W_5<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),dl16 = DLog_W_16<T>(kin),dl1 = DLog_W_1<T>(kin),dl27 = DLog_W_27<T>(kin),dl30 = DLog_W_30<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),dl26 = DLog_W_26<T>(kin),spdl23 = SpDLog_f_4_190_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,39> abbr = 
            {dl3(t), f_1_3_4(kin) - f_1_3_4(kin_path), rlog(-v_path[1]), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_4(kin_path), f_2_1_7(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), dl23(t), f_1_3_1(kin) - f_1_3_1(kin_path), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(v_path[3]), f_2_1_8(kin_path), dl9(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl17(t), dl19(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl2(t), f_1_3_3(kin) - f_1_3_3(kin_path), dl14(t), dl4(t), dl5(t), dl18(t), dl20(t), dl16(t), dl1(t), dl27(t) / kin_path.SqrtDelta, rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[19] / kin_path.W[19]), dl30(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta}
;

        auto result = f_4_190_abbreviated(abbr);
        result = result + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_190_abbreviated(const std::array<T,39>& abb)
{
using TR = typename T::value_type;
T z[93];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[4];
z[4] = abb[3];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[17];
z[11] = abb[18];
z[12] = abb[21];
z[13] = abb[28];
z[14] = abb[29];
z[15] = abb[24];
z[16] = abb[25];
z[17] = abb[11];
z[18] = abb[12];
z[19] = abb[26];
z[20] = abb[14];
z[21] = abb[27];
z[22] = abb[13];
z[23] = abb[15];
z[24] = abb[16];
z[25] = abb[19];
z[26] = abb[20];
z[27] = abb[23];
z[28] = abb[22];
z[29] = abb[31];
z[30] = abb[35];
z[31] = abb[37];
z[32] = abb[38];
z[33] = abb[32];
z[34] = abb[33];
z[35] = abb[34];
z[36] = abb[10];
z[37] = abb[30];
z[38] = abb[36];
z[39] = bc<TR>[3];
z[40] = bc<TR>[1];
z[41] = bc<TR>[2];
z[42] = bc<TR>[4];
z[43] = bc<TR>[9];
z[44] = -z[2] + z[4];
z[45] = z[24] + z[44];
z[46] = int_to_imaginary<T>(1) * z[7];
z[47] = z[45] * z[46];
z[48] = z[2] + -z[18];
z[49] = z[46] + z[48];
z[50] = -z[4] + z[49];
z[51] = z[17] / T(2);
z[52] = z[50] + z[51];
z[53] = z[17] * z[52];
z[53] = z[23] + z[53];
z[54] = z[4] / T(2);
z[55] = -z[48] + z[54];
z[55] = z[4] * z[55];
z[56] = z[2] / T(2) + -z[18];
z[56] = z[2] * z[56];
z[57] = z[55] + z[56];
z[58] = prod_pow(z[18], 2);
z[59] = z[58] / T(2);
z[60] = z[5] + z[59];
z[61] = z[6] + z[22];
z[62] = prod_pow(z[7], 2);
z[63] = z[62] / T(2);
z[47] = -z[47] + z[53] + z[57] + z[60] + z[61] + -z[63];
z[64] = z[10] * z[47];
z[54] = z[48] + z[54];
z[54] = z[4] * z[54];
z[65] = -z[6] + z[59];
z[54] = z[54] + -z[65];
z[66] = T(3) * z[22];
z[56] = -z[25] + z[54] + -z[56] + z[66];
z[67] = z[4] + -z[26];
z[68] = z[24] + z[67];
z[69] = (T(3) * z[46]) / T(2);
z[70] = z[68] * z[69];
z[71] = (T(3) * z[23]) / T(2);
z[70] = z[70] + -z[71];
z[72] = T(3) * z[17];
z[73] = -z[4] + (T(3) * z[17]) / T(4);
z[73] = z[72] * z[73];
z[50] = z[17] + z[50];
z[74] = (T(3) * z[3]) / T(2);
z[75] = -(z[50] * z[74]);
z[56] = (T(3) * z[56]) / T(2) + z[62] + z[70] + z[73] + z[75];
z[56] = z[13] * z[56];
z[52] = z[52] * z[72];
z[73] = T(3) * z[23];
z[52] = z[52] + z[73];
z[75] = z[2] + -z[26];
z[76] = -z[24] + z[75];
z[77] = T(3) * z[46];
z[78] = z[76] * z[77];
z[79] = z[22] + -z[25] + z[60];
z[78] = z[52] + -z[62] + z[78] + T(3) * z[79];
z[78] = z[15] * z[78];
z[79] = prod_pow(z[4], 2);
z[79] = z[79] / T(2);
z[61] = z[61] + z[79];
z[80] = -z[4] + z[51];
z[80] = z[72] * z[80];
z[61] = T(3) * z[61] + z[63] + z[80];
z[80] = z[19] * z[61];
z[79] = z[6] + z[79];
z[81] = prod_pow(z[2], 2);
z[81] = z[81] / T(2);
z[82] = -z[5] + z[79] + -z[81];
z[82] = -z[63] + T(3) * z[82];
z[82] = z[0] * z[82];
z[78] = z[78] + -z[80] + z[82];
z[81] = z[25] + -z[59] + z[81];
z[83] = (T(3) * z[17]) / T(2);
z[84] = z[49] * z[83];
z[76] = z[69] * z[76];
z[76] = z[62] + -z[76] + (T(3) * z[81]) / T(2) + -z[84];
z[76] = z[14] * z[76];
z[81] = z[0] + -z[15];
z[44] = z[44] * z[74] * z[81];
z[81] = z[14] * z[71];
z[44] = -z[44] + z[76] + -z[81];
z[76] = z[46] * z[67];
z[81] = z[4] * z[48];
z[48] = z[2] * z[48];
z[84] = z[5] + z[25] + z[48];
z[76] = z[76] + z[81] + -z[84];
z[85] = z[11] * z[76];
z[85] = (T(3) * z[85]) / T(2);
z[86] = z[17] + -z[18] + z[46];
z[87] = T(3) * z[3];
z[88] = z[86] * z[87];
z[89] = prod_pow(z[17], 2);
z[73] = -z[73] + (T(3) * z[89]) / T(2);
z[89] = z[24] * z[77];
z[88] = z[73] + -z[88] + z[89];
z[89] = -z[22] + z[59];
z[90] = -z[63] + -z[88] + T(3) * z[89];
z[91] = z[21] * z[90];
z[92] = -z[85] + z[91] / T(2);
z[56] = z[44] + z[56] + (T(3) * z[64]) / T(2) + z[78] / T(2) + -z[92];
z[56] = z[9] * z[56];
z[57] = z[6] + z[57];
z[64] = z[25] + z[57] + z[89];
z[68] = z[68] * z[77];
z[78] = z[50] * z[87];
z[64] = z[62] + T(-3) * z[64] + z[68] + z[73] + -z[78];
z[68] = z[13] * z[64];
z[68] = z[68] + z[80] + z[82];
z[57] = z[57] + z[60] + z[66];
z[45] = z[45] * z[77];
z[45] = -z[45] + z[52] + T(3) * z[57] + (T(-5) * z[62]) / T(2);
z[52] = z[10] / T(2);
z[45] = z[45] * z[52];
z[52] = z[24] + z[75];
z[52] = z[46] * z[52];
z[57] = z[5] + -z[25];
z[60] = z[57] + -z[66];
z[52] = z[52] + -z[53] + -z[59] + z[60];
z[53] = (T(3) * z[15]) / T(2);
z[52] = z[52] * z[53];
z[53] = (T(3) * z[58]) / T(2) + -z[62] + -z[88];
z[53] = z[20] * z[53];
z[44] = -z[44] + -z[45] + z[52] + z[53] + -z[68] / T(2);
z[45] = z[44] + z[85];
z[45] = z[1] * z[45];
z[52] = -z[2] + -z[4] + T(2) * z[26];
z[52] = z[52] * z[77];
z[48] = T(2) * z[25] + z[48] + -z[81];
z[48] = T(3) * z[48] + z[52] + z[62];
z[48] = z[27] * z[48];
z[44] = z[44] + z[48] + -z[92];
z[44] = z[8] * z[44];
z[52] = T(3) * z[32];
z[47] = z[47] * z[52];
z[52] = z[30] * z[64];
z[58] = z[38] * z[90];
z[59] = z[37] * z[61];
z[47] = z[47] + -z[52] + z[58] + z[59];
z[52] = -z[29] + z[35];
z[52] = -z[52] / T(2);
z[58] = z[33] / T(2) + z[52];
z[58] = z[47] * z[58];
z[59] = -(z[46] * z[75]);
z[49] = z[49] + z[51];
z[51] = z[17] * z[49];
z[61] = -(z[3] * z[50]);
z[51] = z[51] + z[59] + -z[60] + z[61] + -z[79];
z[51] = z[9] * z[51];
z[49] = z[49] * z[72];
z[49] = z[49] + -z[78];
z[59] = z[75] * z[77];
z[59] = z[59] + -z[62];
z[60] = -z[22] + z[79];
z[61] = -z[57] + z[60];
z[61] = -z[49] + -z[59] + T(3) * z[61];
z[64] = z[1] / T(2);
z[61] = z[61] * z[64];
z[60] = z[57] + z[60];
z[49] = -z[49] + z[59] + T(3) * z[60];
z[49] = z[8] * z[49];
z[60] = -z[63] + z[66];
z[60] = z[36] * z[60];
z[57] = T(-3) * z[57] + -z[59];
z[57] = z[28] * z[57];
z[49] = z[49] / T(2) + (T(3) * z[51]) / T(2) + z[57] + z[60] + z[61];
z[49] = z[16] * z[49];
z[51] = T(3) * z[76];
z[57] = -(z[31] * z[51]);
z[47] = -z[47] + z[57];
z[47] = z[34] * z[47];
z[57] = z[62] / T(4);
z[59] = z[40] * z[46];
z[61] = z[57] + -z[59];
z[61] = z[30] * z[61];
z[66] = -z[57] + -z[59];
z[66] = z[38] * z[66];
z[68] = z[41] * z[46];
z[73] = z[30] + -z[32] + -z[38];
z[73] = z[68] * z[73];
z[59] = z[37] * z[59];
z[57] = -(z[32] * z[57]);
z[57] = z[57] + z[59] + z[61] + z[66] + z[73] / T(2);
z[57] = z[41] * z[57];
z[59] = z[30] + -z[37] + z[38];
z[46] = z[42] * z[46] * z[59];
z[46] = z[46] + z[57];
z[50] = z[50] * z[72];
z[50] = T(6) * z[22] + z[50] + z[63] + -z[78];
z[50] = z[13] * z[50];
z[50] = z[50] + z[53] + -z[91];
z[50] = z[36] * z[50];
z[53] = z[74] * z[86];
z[57] = -z[4] + z[17];
z[57] = z[57] * z[83];
z[53] = z[53] + -z[57] + -z[63];
z[54] = z[54] + -z[84];
z[54] = z[22] + z[54] / T(2);
z[54] = -z[53] + T(3) * z[54] + z[70];
z[57] = z[8] + -z[9];
z[54] = z[54] * z[57];
z[55] = z[55] + -z[65] + z[84];
z[55] = z[22] + z[55] / T(2);
z[57] = z[24] + -z[67];
z[57] = z[57] * z[69];
z[53] = -z[53] + T(3) * z[55] + z[57] + -z[71];
z[53] = z[1] * z[53];
z[51] = -(z[28] * z[51]);
z[51] = z[51] + z[53] + z[54] + -z[60];
z[51] = z[12] * z[51];
z[52] = z[52] * z[76];
z[53] = z[33] * z[76];
z[53] = z[43] / T(2) + z[53];
z[54] = -z[63] + -z[68];
z[54] = z[41] * z[54];
z[52] = z[52] + z[53] / T(2) + z[54];
z[53] = T(3) * z[31];
z[52] = z[52] * z[53];
z[48] = -(z[28] * z[48]);
z[53] = -(z[64] * z[90]);
z[54] = T(3) * z[7];
z[54] = z[39] * z[54];
z[55] = int_to_imaginary<T>(1) * prod_pow(z[7], 3);
z[54] = -z[54] + z[55] / T(9);
z[53] = z[53] + -z[54];
z[53] = z[21] * z[53];
z[57] = z[13] + z[14] + -z[19];
z[54] = z[54] * z[57];
z[57] = z[40] * z[69];
z[57] = z[57] + -z[62];
z[57] = z[40] * z[57];
z[59] = (T(61) * z[43]) / T(8) + z[57];
z[59] = z[30] * z[59];
z[60] = T(-19) * z[43] + z[55] / T(2);
z[60] = -z[57] + z[60] / T(2);
z[60] = z[37] * z[60];
z[57] = (T(67) * z[43]) / T(8) + z[57];
z[57] = z[38] * z[57];
z[55] = (T(87) * z[43]) / T(2) + z[55];
z[55] = z[32] * z[55];
return z[44] + z[45] + T(3) * z[46] + z[47] / T(2) + z[48] + z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] / T(4) + z[56] + z[57] + z[58] + z[59] + z[60];
}



template IntegrandConstructorType<double> f_4_190_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_190_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_190_construct (const Kin<qd_real>&);
#endif

}