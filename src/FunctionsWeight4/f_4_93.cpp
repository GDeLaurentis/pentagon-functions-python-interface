#include "f_4_93.h"

namespace PentagonFunctions {

template <typename T> T f_4_93_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_93_W_10 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_93_W_10 (const Kin<T>& kin) {
        c[0] = (T(2) * kin.v[0] * kin.v[2]) / T(3) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + -kin.v[3] + T(-4) + T(-3) * kin.v[4]) + kin.v[1] * ((T(2) * kin.v[0]) / T(3) + -kin.v[3] + T(-4) + (bc<T>[1] / T(2) + T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[1] + T(3) * kin.v[2] + bc<T>[1] * (-kin.v[4] + T(-1) + kin.v[2]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(-1) + kin.v[2]) + T(-3) * kin.v[4]) + bc<T>[0] * (kin.v[2] / T(2) + -kin.v[4] + T(-1)) * int_to_imaginary<T>(1) * kin.v[2] + bc<T>[1] * (kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2];
c[1] = (-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[2] + -(bc<T>[1] * kin.v[2]);
c[2] = (T(2) * kin.v[0] * kin.v[2]) / T(3) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + -kin.v[3] + T(-4) + T(-3) * kin.v[4]) + kin.v[1] * ((T(2) * kin.v[0]) / T(3) + -kin.v[3] + T(-4) + (bc<T>[1] / T(2) + T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[1] + T(3) * kin.v[2] + bc<T>[1] * (-kin.v[4] + T(-1) + kin.v[2]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(-1) + kin.v[2]) + T(-3) * kin.v[4]) + bc<T>[0] * (kin.v[2] / T(2) + -kin.v[4] + T(-1)) * int_to_imaginary<T>(1) * kin.v[2] + bc<T>[1] * (kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2];
c[3] = (-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[2] + -(bc<T>[1] * kin.v[2]);
c[4] = (T(2) * kin.v[0] * kin.v[2]) / T(3) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + -kin.v[3] + T(-4) + T(-3) * kin.v[4]) + kin.v[1] * ((T(2) * kin.v[0]) / T(3) + -kin.v[3] + T(-4) + (bc<T>[1] / T(2) + T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[1] + T(3) * kin.v[2] + bc<T>[1] * (-kin.v[4] + T(-1) + kin.v[2]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(-1) + kin.v[2]) + T(-3) * kin.v[4]) + bc<T>[0] * (kin.v[2] / T(2) + -kin.v[4] + T(-1)) * int_to_imaginary<T>(1) * kin.v[2] + bc<T>[1] * (kin.v[2] / T(2) + -kin.v[4] + T(-1)) * kin.v[2];
c[5] = (-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[2] + -(bc<T>[1] * kin.v[2]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[7] * (t * c[0] + c[1]) + abb[1] * (t * c[2] + c[3]) + abb[8] * (t * c[4] + c[5]);
        }

        return abb[13] * (abb[3] * (abb[1] + abb[8]) * abb[14] + abb[2] * ((abb[1] + abb[7] + abb[8]) * abb[11] + abb[14] * (-abb[1] + -abb[8]) + -(abb[7] * abb[14])) + abb[14] * (abb[4] * (abb[1] + abb[8]) + (abb[1] + abb[8]) * abb[14] + abb[1] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-1)) + abb[11] * (abb[3] * (-abb[1] + -abb[8]) + abb[4] * (-abb[1] + -abb[8]) + abb[14] * (-abb[1] + -abb[8]) + abb[1] * bc<T>[0] * int_to_imaginary<T>(1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(1) + abb[7] * (-abb[3] + -abb[4] + -abb[14] + bc<T>[0] * int_to_imaginary<T>(1))) + abb[15] * (abb[1] * T(2) + abb[8] * T(2)) + abb[7] * (abb[3] * abb[14] + abb[14] * (abb[4] + abb[14] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[15] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_93_construct (const Kin<T>& kin) {
    return [&kin, 
            dl4 = DLog_W_4<T>(kin),dl15 = DLog_W_15<T>(kin),dl13 = DLog_W_13<T>(kin),dl10 = DLog_W_10<T>(kin),dl6 = DLog_W_6<T>(kin),dl24 = DLog_W_24<T>(kin),dl19 = DLog_W_19<T>(kin),dl20 = DLog_W_20<T>(kin),dl3 = DLog_W_3<T>(kin),dl5 = DLog_W_5<T>(kin),dl18 = DLog_W_18<T>(kin),dl17 = DLog_W_17<T>(kin),dl1 = DLog_W_1<T>(kin),dl2 = DLog_W_2<T>(kin),dl16 = DLog_W_16<T>(kin),dl31 = DLog_W_31<T>(kin),dl30 = DLog_W_30<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl26 = DLog_W_26<T>(kin),dl27 = DLog_W_27<T>(kin),spdl10 = SpDLog_f_4_93_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);

        std::array<std::complex<T>,46> abbr = 
            {dl4(t), f_1_3_2(kin) - f_1_3_2(kin_path), rlog(v_path[0]), rlog(v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_1(kin_path), f_2_1_9(kin_path), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl15(t), f_1_3_4(kin) - f_1_3_4(kin_path), rlog(-v_path[4]), dl13(t), dl10(t), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_7(kin_path), dl6(t), f_1_3_3(kin) - f_1_3_3(kin_path), dl24(t), dl19(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl20(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl3(t), dl5(t), dl18(t), dl17(t), dl1(t), dl2(t), dl16(t), dl31(t), dl30(t) / kin_path.SqrtDelta, rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[17] / kin_path.W[17]), dl28(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta}
;

        auto result = f_4_93_abbreviated(abbr);
        result = result + spdl10(t, abbr);

        return result;
    };
}

template <typename T> T f_4_93_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[119];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[12];
z[11] = abb[16];
z[12] = abb[19];
z[13] = abb[22];
z[14] = abb[26];
z[15] = abb[28];
z[16] = abb[29];
z[17] = abb[11];
z[18] = abb[25];
z[19] = abb[27];
z[20] = abb[14];
z[21] = abb[30];
z[22] = abb[18];
z[23] = abb[31];
z[24] = abb[15];
z[25] = abb[20];
z[26] = abb[21];
z[27] = abb[23];
z[28] = abb[24];
z[29] = abb[10];
z[30] = abb[17];
z[31] = abb[34];
z[32] = abb[42];
z[33] = abb[43];
z[34] = abb[44];
z[35] = abb[35];
z[36] = abb[36];
z[37] = abb[37];
z[38] = abb[38];
z[39] = abb[39];
z[40] = abb[40];
z[41] = abb[41];
z[42] = abb[45];
z[43] = abb[9];
z[44] = abb[33];
z[45] = bc<TR>[1];
z[46] = bc<TR>[3];
z[47] = bc<TR>[5];
z[48] = abb[32];
z[49] = bc<TR>[2];
z[50] = bc<TR>[4];
z[51] = bc<TR>[7];
z[52] = bc<TR>[8];
z[53] = bc<TR>[9];
z[54] = z[15] + z[18];
z[55] = z[14] + z[19];
z[56] = z[54] + z[55];
z[57] = z[21] + z[23];
z[58] = z[16] / T(2);
z[59] = z[48] + z[57] / T(4) + -z[58];
z[60] = z[34] + -z[42];
z[61] = z[32] + z[33];
z[62] = z[44] / T(2);
z[60] = (T(-11) * z[56]) / T(60) + (T(11) * z[59]) / T(45) + -z[60] / T(2) + -z[61] + -z[62];
z[60] = int_to_imaginary<T>(1) * z[7] * z[60];
z[63] = z[8] * z[19];
z[64] = z[8] + z[9];
z[65] = z[64] / T(4);
z[66] = (T(13) * z[1]) / T(4) + z[30] + z[65];
z[66] = z[15] * z[66];
z[60] = z[60] + z[63] + z[66];
z[66] = z[21] * z[64];
z[67] = z[1] * z[21];
z[68] = T(5) * z[8] + z[9];
z[68] = -z[1] + z[68] / T(3);
z[68] = z[18] * z[68];
z[69] = z[1] + -z[9];
z[70] = -z[8] + z[69] / T(3);
z[70] = z[0] * z[70];
z[67] = -z[66] / T(3) + z[67] + z[68] + z[70];
z[68] = z[1] + z[30];
z[70] = -z[64] + z[68];
z[70] = z[22] * z[70];
z[71] = z[1] + z[9];
z[72] = z[8] + -z[29];
z[71] = (T(7) * z[30]) / T(6) + z[71] / T(6) + -z[72];
z[71] = z[16] * z[71];
z[73] = z[1] / T(2);
z[74] = z[64] / T(2);
z[75] = z[30] + z[74];
z[76] = -z[73] + -z[75] / T(3);
z[76] = z[12] * z[76];
z[77] = z[35] + z[36];
z[78] = z[38] + z[39] + z[40] + z[41];
z[79] = z[77] / T(3) + -z[78];
z[79] = -z[31] / T(3) + z[79] / T(2);
z[79] = z[37] / T(3) + z[79] / T(2);
z[80] = (T(-7) * z[49]) / T(6) + -z[79];
z[80] = z[32] * z[80];
z[79] = (T(7) * z[49]) / T(3) + -z[79];
z[79] = z[34] * z[79];
z[81] = T(13) * z[32] + -z[42];
z[81] = (T(13) * z[33]) / T(2) + T(-7) * z[34] + z[62] + z[81] / T(2);
z[81] = z[45] * z[81];
z[82] = -z[42] + z[44];
z[83] = (T(-7) * z[33]) / T(6) + (T(5) * z[82]) / T(6);
z[83] = z[49] * z[83];
z[84] = z[43] * z[72];
z[85] = -z[1] + T(-13) * z[64];
z[85] = z[14] * z[85];
z[60] = z[60] / T(3) + z[67] / T(4) + -z[70] + z[71] + z[76] / T(2) + z[79] + z[80] + z[81] / T(6) + z[83] + z[84] + z[85] / T(12);
z[60] = z[7] * z[60];
z[67] = -z[77] + T(3) * z[78];
z[67] = z[31] + z[67] / T(2);
z[71] = T(-2) * z[37] + z[67];
z[76] = z[33] * z[71];
z[77] = (T(3) * z[1]) / T(2);
z[78] = (T(3) * z[8]) / T(2) + z[77];
z[79] = -z[29] + z[78];
z[80] = z[9] / T(2);
z[81] = z[79] + z[80];
z[83] = z[13] * z[81];
z[76] = z[76] + z[83];
z[85] = T(3) * z[1];
z[86] = -z[64] + z[85];
z[87] = z[18] / T(2);
z[88] = z[86] * z[87];
z[85] = z[21] * z[85];
z[66] = -z[66] + z[85];
z[85] = z[66] / T(2) + -z[88];
z[78] = T(-2) * z[29] + z[78] + -z[80];
z[78] = z[14] * z[78];
z[88] = z[16] * z[81];
z[89] = z[15] / T(2);
z[86] = z[86] * z[89];
z[90] = z[34] * z[71];
z[78] = z[76] + -z[78] + -z[85] + z[86] + -z[88] + -z[90];
z[86] = -(z[28] * z[78]);
z[88] = z[14] * z[64];
z[91] = z[8] * z[18];
z[92] = z[16] * z[68];
z[68] = z[15] * z[68];
z[93] = z[1] + z[8];
z[94] = z[30] + z[93];
z[94] = z[11] * z[94];
z[88] = -z[68] + z[70] + z[88] + -z[91] + -z[92] + z[94];
z[88] = z[4] * z[88];
z[95] = z[1] + -z[29];
z[95] = z[10] * z[95];
z[96] = z[63] + z[84] + z[95];
z[97] = -z[29] + z[93];
z[98] = z[16] * z[97];
z[97] = z[14] * z[97];
z[99] = z[1] * z[15];
z[99] = z[96] + -z[97] + -z[98] + z[99];
z[99] = z[3] * z[99];
z[99] = z[88] + -z[99];
z[100] = z[32] * z[71];
z[75] = z[75] + z[77];
z[101] = z[12] * z[75];
z[100] = z[100] + -z[101];
z[102] = z[42] * z[71];
z[103] = z[100] + z[102];
z[71] = z[44] * z[71];
z[71] = z[71] + z[90];
z[77] = T(-2) * z[30] + z[74] + -z[77];
z[77] = z[15] * z[77];
z[104] = z[19] / T(2);
z[105] = z[14] / T(2);
z[106] = z[104] + z[105];
z[107] = -z[1] + T(3) * z[64];
z[108] = z[106] * z[107];
z[109] = z[16] * z[75];
z[107] = z[23] * z[107];
z[110] = z[107] / T(2);
z[77] = z[71] + z[77] + -z[103] + z[108] + -z[109] + -z[110];
z[108] = z[26] * z[77];
z[109] = z[1] + z[64];
z[111] = z[109] / T(2);
z[112] = z[15] * z[111];
z[112] = z[90] + z[112];
z[105] = z[105] * z[109];
z[113] = T(2) * z[8];
z[113] = z[18] * z[113];
z[113] = T(2) * z[63] + -z[105] + z[112] + z[113];
z[114] = z[94] + -z[95];
z[72] = z[30] + -z[72];
z[72] = z[16] * z[72];
z[72] = z[72] + z[76] + z[100] + -z[113] + T(2) * z[114];
z[72] = z[2] * z[72];
z[114] = z[19] * z[111];
z[114] = z[102] + z[114];
z[105] = z[105] + z[114];
z[115] = z[16] * z[109];
z[116] = z[66] + z[115];
z[117] = T(2) * z[1];
z[54] = z[54] * z[117];
z[54] = z[54] + z[105] + -z[116] / T(2);
z[54] = z[20] * z[54];
z[116] = z[18] * z[109];
z[117] = z[15] * z[109];
z[116] = -z[107] + -z[115] + z[116] + z[117];
z[118] = T(2) * z[64];
z[55] = z[55] * z[118];
z[55] = z[55] + z[71] + z[116] / T(2);
z[116] = -(z[17] * z[55]);
z[59] = (T(-3) * z[56]) / T(4) + z[59];
z[59] = z[59] / T(5) + T(2) * z[61];
z[59] = z[45] * z[59];
z[118] = z[34] + -z[61];
z[118] = z[49] * z[118];
z[59] = z[59] + z[118];
z[59] = z[45] * z[59];
z[61] = z[34] + T(3) * z[61];
z[61] = z[50] * z[61];
z[59] = z[54] + z[59] + z[61] + z[72] + z[86] + T(-2) * z[99] + z[108] + z[116];
z[59] = int_to_imaginary<T>(1) * z[59];
z[57] = T(-2) * z[16] + T(4) * z[48] + z[57];
z[61] = -z[56] + z[57] / T(3);
z[61] = z[46] * z[61];
z[59] = z[59] + z[60] + z[61];
z[59] = z[7] * z[59];
z[60] = z[101] / T(2);
z[61] = -z[37] + z[67] / T(2);
z[67] = -z[32] + z[82];
z[67] = z[61] * z[67];
z[72] = z[34] * z[61];
z[86] = z[30] + z[73];
z[74] = -z[74] + z[86];
z[74] = z[16] * z[74];
z[74] = z[74] + -z[110];
z[99] = z[109] / T(4);
z[101] = z[19] * z[99];
z[108] = z[14] * z[99];
z[109] = -(z[1] * z[18]);
z[65] = (T(-3) * z[1]) / T(4) + -z[30] + z[65];
z[65] = z[15] * z[65];
z[65] = z[60] + z[65] + z[67] + -z[70] + z[72] + z[74] / T(2) + -z[101] + -z[108] + z[109];
z[65] = prod_pow(z[20], 2) * z[65];
z[67] = z[33] * z[61];
z[67] = z[67] + z[83] / T(2);
z[70] = T(3) * z[8];
z[74] = z[69] + -z[70];
z[83] = z[74] / T(2);
z[109] = z[0] * z[83];
z[94] = z[94] + z[109];
z[72] = z[72] + z[117] / T(4);
z[108] = z[72] + -z[108];
z[110] = -z[9] + -z[29] + -z[30];
z[110] = z[8] + z[73] + z[110] / T(2);
z[110] = z[16] * z[110];
z[116] = -(z[32] * z[61]);
z[60] = z[60] + -z[63] + -z[67] + -z[91] + -z[94] + z[95] + -z[108] + z[110] + z[116];
z[60] = z[2] * z[60];
z[63] = z[18] * z[111];
z[91] = z[63] + -z[76] + T(2) * z[97] + z[98] + z[112] + z[114];
z[97] = -(z[17] * z[91]);
z[63] = z[63] + T(2) * z[68] + z[92] + z[100] + z[105];
z[63] = z[20] * z[63];
z[68] = -z[9] + z[93];
z[58] = z[58] * z[68];
z[58] = z[58] + -z[109] + -z[113];
z[68] = -z[3] + -z[4];
z[68] = z[58] * z[68];
z[60] = z[60] + z[63] + z[68] + z[97];
z[60] = z[2] * z[60];
z[63] = z[8] + -z[9];
z[63] = z[63] / T(2) + -z[86];
z[63] = z[16] * z[63];
z[68] = z[18] * z[83];
z[75] = -(z[15] * z[75]);
z[69] = z[69] + z[70];
z[70] = -(z[69] * z[104]);
z[63] = z[63] + z[68] + z[70] + z[75] + -z[90] + -z[94] + -z[103];
z[63] = z[6] * z[63];
z[68] = z[79] + -z[80];
z[68] = z[16] * z[68];
z[69] = -(z[69] * z[87]);
z[70] = z[74] * z[104];
z[74] = z[14] * z[81];
z[68] = z[68] + z[69] + z[70] + z[74] + -z[76] + z[95] + z[102] + -z[109];
z[68] = z[5] * z[68];
z[69] = z[27] * z[78];
z[66] = z[66] + z[107];
z[70] = z[1] + T(5) * z[64];
z[74] = z[14] * z[70];
z[74] = z[66] + -z[74];
z[73] = z[8] / T(2) + -z[29] + z[73] + -z[80];
z[73] = z[16] * z[73];
z[75] = -z[42] + -z[44];
z[61] = z[61] * z[75];
z[75] = -(z[18] * z[99]);
z[61] = z[61] + -z[72] + -z[73] + z[74] / T(4) + z[75] + z[84] + -z[101];
z[61] = z[17] * z[61];
z[72] = z[3] * z[91];
z[74] = z[4] * z[55];
z[61] = z[54] + z[61] + z[72] + z[74];
z[61] = z[17] * z[61];
z[55] = -(z[20] * z[55]);
z[55] = z[55] + z[88];
z[55] = z[4] * z[55];
z[72] = -z[87] + -z[89];
z[64] = T(5) * z[1] + z[64];
z[64] = z[64] * z[72];
z[70] = -(z[70] * z[106]);
z[64] = z[64] + z[66] / T(2) + z[70] + -z[71] + -z[102] + z[115];
z[64] = z[24] * z[64];
z[66] = z[73] + -z[85];
z[66] = z[66] / T(2) + z[67] + -z[96] + -z[108];
z[66] = z[3] * z[66];
z[58] = z[4] * z[58];
z[54] = -z[54] + z[58] + z[66];
z[54] = z[3] * z[54];
z[58] = z[25] * z[77];
z[66] = prod_pow(z[49], 3);
z[67] = -z[51] + z[66] / T(3);
z[70] = z[52] / T(2);
z[67] = T(2) * z[67] + -z[70];
z[71] = z[42] * z[67];
z[70] = -z[51] + (T(2) * z[66]) / T(3) + -z[70];
z[72] = -(z[32] * z[70]);
z[67] = (T(-67) * z[53]) / T(12) + -z[67];
z[67] = z[44] * z[67];
z[70] = (T(-7) * z[53]) / T(12) + -z[70];
z[70] = z[33] * z[70];
z[73] = z[32] + z[33] + z[42];
z[62] = T(-2) * z[34] + z[62] + -z[73] / T(2);
z[62] = prod_pow(z[45], 2) * z[62];
z[73] = T(-3) * z[34] + z[82];
z[73] = z[50] * z[73];
z[62] = z[62] / T(3) + z[73];
z[62] = z[45] * z[62];
z[56] = T(3) * z[56] + -z[57];
z[57] = z[45] * z[46];
z[57] = (T(12) * z[47]) / T(5) + z[57];
z[56] = int_to_imaginary<T>(1) * z[56] * z[57];
z[57] = T(-7) * z[32] + T(67) * z[42];
z[57] = z[53] * z[57];
z[66] = T(-5) * z[51] + -z[52] + (T(11) * z[53]) / T(6) + (T(4) * z[66]) / T(3);
z[66] = z[34] * z[66];
return z[54] + z[55] + z[56] + z[57] / T(12) + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + z[68] + z[69] + z[70] + z[71] + z[72];
}



template IntegrandConstructorType<double> f_4_93_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_93_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_93_construct (const Kin<qd_real>&);
#endif

}