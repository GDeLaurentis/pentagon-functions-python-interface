#include "f_4_67.h"

namespace PentagonFunctions {

template <typename T> T f_4_67_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_67_W_23 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_67_W_23 (const Kin<T>& kin) {
        c[0] = rlog(kin.v[3]) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[0] * (T(4) + T(-2) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[3]) + kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(2) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[0] * (T(4) + T(-2) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[3]) + kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(2) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[0] * (T(-4) + T(2) * kin.v[0] + T(4) * kin.v[1] + T(-4) * kin.v[3]) + kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[4] * (T(-4) + T(-2) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(2) + prod_pow(abb[2], 2) * (abb[5] * T(-2) + abb[4] * T(2)) + prod_pow(abb[1], 2) * (abb[3] * T(-2) + abb[4] * T(-2) + abb[5] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_67_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl16 = DLog_W_16<T>(kin),dl9 = DLog_W_9<T>(kin),dl20 = DLog_W_20<T>(kin),dl4 = DLog_W_4<T>(kin),dl17 = DLog_W_17<T>(kin),spdl23 = SpDLog_f_4_67_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,17> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[19] / kin_path.W[19]), dl16(t), rlog(kin.W[8] / kin_path.W[8]), -rlog(t), dl9(t), rlog(v_path[3]), dl20(t), f_2_1_6(kin_path), f_2_1_8(kin_path), rlog(v_path[0] + v_path[1]), dl4(t), dl17(t)}
;

        auto result = f_4_67_abbreviated(abbr);
        result = result + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_67_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[32];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[6];
z[3] = abb[9];
z[4] = abb[11];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[7];
z[8] = abb[8];
z[9] = abb[2];
z[10] = abb[15];
z[11] = abb[16];
z[12] = abb[10];
z[13] = bc<TR>[0];
z[14] = abb[12];
z[15] = abb[13];
z[16] = abb[14];
z[17] = bc<TR>[2];
z[18] = bc<TR>[9];
z[19] = z[1] + z[5];
z[20] = -z[6] + z[19];
z[21] = z[4] * z[20];
z[22] = z[3] * z[20];
z[23] = z[1] + -z[6];
z[24] = z[7] + z[23];
z[24] = z[10] * z[24];
z[24] = -z[21] + z[22] + z[24];
z[24] = prod_pow(z[12], 2) * z[24];
z[25] = -z[4] + z[10];
z[26] = z[11] + z[25];
z[27] = z[20] * z[26];
z[28] = T(2) * z[27];
z[29] = -(z[12] * z[28]);
z[30] = -z[1] + z[8];
z[30] = z[11] * z[30];
z[31] = -(z[10] * z[20]);
z[30] = z[30] + z[31];
z[30] = z[9] * z[30];
z[29] = z[29] + z[30];
z[29] = z[9] * z[29];
z[30] = z[9] * z[28];
z[31] = -z[3] + z[4];
z[31] = z[20] * z[31];
z[19] = z[7] + z[8] + -z[19];
z[19] = z[2] * z[19];
z[31] = -z[19] + z[31];
z[31] = z[0] * z[31];
z[30] = z[30] + z[31];
z[30] = z[0] * z[30];
z[31] = z[14] + -z[15];
z[28] = z[28] * z[31];
z[24] = z[24] + z[28] + z[29] + z[30];
z[28] = z[17] * z[26];
z[28] = z[22] + z[28];
z[19] = z[19] / T(2) + T(2) * z[21];
z[21] = z[5] + -z[6];
z[21] = (T(3) * z[1]) / T(2) + -z[8] / T(6) + (T(4) * z[21]) / T(3);
z[21] = z[11] * z[21];
z[23] = (T(3) * z[5]) / T(2) + (T(-13) * z[7]) / T(6) + (T(-2) * z[23]) / T(3);
z[23] = z[10] * z[23];
z[25] = (T(5) * z[3]) / T(6) + -z[11] / T(2) + z[25] / T(3);
z[25] = int_to_imaginary<T>(1) * z[13] * z[25];
z[19] = z[19] / T(3) + z[21] + z[23] + z[25] + T(-2) * z[28];
z[19] = z[13] * z[19];
z[20] = z[11] * z[20];
z[21] = -z[5] + z[7];
z[21] = -(z[10] * z[21]);
z[20] = z[20] + z[21] + -z[22];
z[20] = z[12] * z[20];
z[21] = z[9] + -z[16];
z[21] = z[21] * z[27];
z[20] = z[20] + z[21];
z[20] = int_to_imaginary<T>(1) * z[20];
z[19] = z[19] + T(4) * z[20];
z[19] = z[13] * z[19];
z[20] = z[18] * z[26];
return z[19] + T(3) * z[20] + T(2) * z[24];
}



template IntegrandConstructorType<double> f_4_67_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_67_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_67_construct (const Kin<qd_real>&);
#endif

}