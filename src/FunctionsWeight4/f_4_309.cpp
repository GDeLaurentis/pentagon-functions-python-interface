#include "f_4_309.h"

namespace PentagonFunctions {

template <typename T> T f_4_309_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_309_W_7 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_309_W_7 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (-kin.v[3] + T(2) + T(2) * kin.v[1] + T(-2) * kin.v[4]) + T(2) * kin.v[1] * kin.v[4] + (-kin.v[4] + T(2)) * kin.v[4];
c[1] = kin.v[3] * (-kin.v[3] + T(2) + T(2) * kin.v[1] + T(-2) * kin.v[4]) + T(2) * kin.v[1] * kin.v[4] + (-kin.v[4] + T(2)) * kin.v[4];
c[2] = kin.v[3] * (-kin.v[3] + T(2) + T(2) * kin.v[1] + T(-2) * kin.v[4]) + T(2) * kin.v[1] * kin.v[4] + (-kin.v[4] + T(2)) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1] + abb[5] * c[2]);
        }

        return abb[0] * (prod_pow(abb[1], 2) * (abb[3] + abb[4] + abb[5]) + -(prod_pow(abb[2], 2) * abb[3]) + prod_pow(abb[2], 2) * (-abb[4] + -abb[5]));
    }
};
template <typename T> class SpDLog_f_4_309_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_309_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + kin.v[3] * (T(2) + kin.v[3]) + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]);
c[1] = kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + kin.v[3] * (T(2) + kin.v[3]) + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]);
c[2] = (-kin.v[3] + T(-2)) * kin.v[3] + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]);
c[3] = (-kin.v[3] + T(-2)) * kin.v[3] + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[4] * c[0] + abb[5] * c[1] + abb[9] * c[2] + abb[10] * c[3]);
        }

        return abb[6] * (prod_pow(abb[8], 2) * abb[9] + prod_pow(abb[8], 2) * (abb[10] + -abb[4] + -abb[5]) + prod_pow(abb[7], 2) * (abb[4] + abb[5] + -abb[9] + -abb[10]));
    }
};
template <typename T> class SpDLog_f_4_309_W_21 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_309_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (kin.v[3] / T(2) + T(4) + T(-3) * kin.v[4]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(4) + T(2) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-4) + kin.v[3] + T(3) * kin.v[4]) + kin.v[0] * (T(-4) + (T(5) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[0] + kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(1) + kin.v[2]) + T(-3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + T(1)) * kin.v[1] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-1) + kin.v[4]));
c[1] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[1]) + T(2) * kin.v[3];
c[2] = kin.v[3] * (kin.v[3] / T(2) + T(4) + T(-3) * kin.v[4]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(4) + T(2) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-4) + kin.v[3] + T(3) * kin.v[4]) + kin.v[0] * (T(-4) + (T(5) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[0] + kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(1) + kin.v[2]) + T(-3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + T(1)) * kin.v[1] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]) + kin.v[3] * (kin.v[3] / T(2) + T(-1) + kin.v[4]));
c[3] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(1)) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + -kin.v[3] + kin.v[1]) + T(2) * kin.v[3];
c[4] = kin.v[1] * ((T(3) * kin.v[1]) / T(2) + -kin.v[3] + T(4) + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-4) + T(-2) * kin.v[3] + T(3) * kin.v[4]) + kin.v[3] * (-kin.v[3] / T(2) + T(-4) + T(3) * kin.v[4]) + kin.v[0] * (-kin.v[1] + T(4) + (T(-5) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(4) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]));
c[5] = (bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3]);
c[6] = kin.v[1] * ((T(3) * kin.v[1]) / T(2) + -kin.v[3] + T(4) + T(-3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-4) + T(-2) * kin.v[3] + T(3) * kin.v[4]) + kin.v[3] * (-kin.v[3] / T(2) + T(-4) + T(3) * kin.v[4]) + kin.v[0] * (-kin.v[1] + T(4) + (T(-5) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[0] + T(4) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + -kin.v[4] + T(1)) * kin.v[3] + kin.v[1] * (-kin.v[1] / T(2) + T(-1) + kin.v[3] + kin.v[4]));
c[7] = (bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[1] + kin.v[2] + kin.v[3]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[3] * (t * c[0] + c[1]) + abb[4] * (t * c[2] + c[3]) + abb[5] * (t * c[4] + c[5]) + abb[10] * (t * c[6] + c[7]);
        }

        return abb[11] * (abb[8] * abb[12] * (abb[5] + abb[10] + -abb[4]) + abb[7] * abb[8] * (abb[4] + -abb[5] + -abb[10]) + abb[1] * (abb[3] * abb[8] + abb[2] * (abb[5] + abb[10] + -abb[3] + -abb[4]) + abb[8] * (abb[4] + -abb[5] + -abb[10])) + abb[8] * (abb[8] * (abb[4] + -abb[5] + -abb[10]) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[4] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[2] * (abb[7] * (abb[5] + abb[10] + -abb[4]) + abb[8] * (abb[5] + abb[10] + -abb[4]) + abb[12] * (abb[4] + -abb[5] + -abb[10]) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[3] * (abb[12] + -abb[7] + -abb[8] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[13] * (abb[5] * T(-2) + abb[10] * T(-2) + abb[4] * T(2)) + abb[3] * (abb[7] * abb[8] + -(abb[8] * abb[12]) + abb[8] * (abb[8] + bc<T>[0] * int_to_imaginary<T>(1)) + abb[13] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_309_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl22 = DLog_W_22<T>(kin),dl21 = DLog_W_21<T>(kin),dl9 = DLog_W_9<T>(kin),dl17 = DLog_W_17<T>(kin),dl14 = DLog_W_14<T>(kin),dl5 = DLog_W_5<T>(kin),dl1 = DLog_W_1<T>(kin),dl2 = DLog_W_2<T>(kin),dl16 = DLog_W_16<T>(kin),dl20 = DLog_W_20<T>(kin),dl19 = DLog_W_19<T>(kin),dl4 = DLog_W_4<T>(kin),dl3 = DLog_W_3<T>(kin),dl18 = DLog_W_18<T>(kin),dl31 = DLog_W_31<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),spdl7 = SpDLog_f_4_309_W_7<T>(kin),spdl22 = SpDLog_f_4_309_W_22<T>(kin),spdl21 = SpDLog_f_4_309_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,46> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl21(t), rlog(v_path[3]), f_2_1_15(kin_path), dl9(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl17(t), f_2_1_14(kin_path), dl14(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl5(t), f_2_1_11(kin_path), dl1(t), dl2(t), dl16(t), dl20(t), dl19(t), dl4(t), dl3(t), dl18(t), dl31(t), dl27(t) / kin_path.SqrtDelta, rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[17] / kin_path.W[17]), dl29(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta}
;

        auto result = f_4_309_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_309_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[124];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[22];
z[3] = abb[24];
z[4] = abb[25];
z[5] = abb[26];
z[6] = abb[27];
z[7] = abb[29];
z[8] = abb[31];
z[9] = abb[4];
z[10] = abb[19];
z[11] = abb[5];
z[12] = abb[28];
z[13] = abb[10];
z[14] = abb[33];
z[15] = abb[34];
z[16] = abb[35];
z[17] = abb[36];
z[18] = abb[37];
z[19] = abb[38];
z[20] = abb[39];
z[21] = abb[40];
z[22] = abb[41];
z[23] = abb[42];
z[24] = abb[45];
z[25] = abb[2];
z[26] = abb[7];
z[27] = abb[8];
z[28] = abb[12];
z[29] = bc<TR>[0];
z[30] = abb[43];
z[31] = abb[30];
z[32] = abb[17];
z[33] = abb[13];
z[34] = abb[15];
z[35] = abb[16];
z[36] = abb[18];
z[37] = abb[20];
z[38] = abb[21];
z[39] = abb[23];
z[40] = abb[14];
z[41] = abb[9];
z[42] = abb[44];
z[43] = bc<TR>[3];
z[44] = abb[32];
z[45] = bc<TR>[1];
z[46] = bc<TR>[2];
z[47] = bc<TR>[4];
z[48] = bc<TR>[9];
z[49] = z[4] + z[6] + z[12];
z[50] = -z[14] + z[24];
z[51] = z[8] + z[31] + T(4) * z[44];
z[52] = z[23] + z[30];
z[53] = (T(-2) * z[7]) / T(27) + z[42] / T(12) + -z[49] / T(9) + -z[50] / T(4) + z[51] / T(27) + z[52] / T(6);
z[54] = int_to_imaginary<T>(1) * z[29];
z[53] = z[53] * z[54];
z[55] = z[1] + -z[13];
z[56] = z[9] + z[55];
z[57] = z[11] / T(3);
z[58] = -z[56] + -z[57];
z[58] = z[8] * z[58];
z[59] = -z[11] + -z[56] / T(3);
z[59] = z[31] * z[59];
z[58] = z[58] + z[59];
z[59] = z[13] / T(3);
z[60] = z[1] / T(3);
z[61] = z[59] + -z[60];
z[62] = z[9] + -z[61];
z[62] = z[3] * z[62];
z[63] = -z[57] + z[60];
z[59] = z[9] + z[59];
z[59] = -z[41] / T(6) + z[59] / T(2) + -z[63];
z[59] = z[7] * z[59];
z[64] = z[11] / T(2);
z[65] = -z[9] + z[55] / T(2);
z[65] = -z[64] + z[65] / T(3);
z[65] = z[4] * z[65];
z[66] = z[9] + -z[13];
z[63] = -z[63] + -z[66];
z[63] = z[41] / T(3) + z[63] / T(2);
z[63] = z[32] * z[63];
z[60] = -z[9] / T(3) + -z[13] + -z[57] + -z[60];
z[67] = z[12] / T(2);
z[60] = z[60] * z[67];
z[68] = z[21] + z[22];
z[69] = -z[17] + z[68] / T(3);
z[70] = z[20] / T(3) + z[69] / T(4);
z[71] = (T(3) * z[46]) / T(4);
z[72] = z[70] + -z[71];
z[72] = z[42] * z[72];
z[69] = (T(2) * z[20]) / T(3) + z[69] / T(2);
z[71] = -z[45] + z[71];
z[73] = z[69] + -z[71];
z[73] = z[30] * z[73];
z[68] = T(-3) * z[17] + z[68];
z[74] = z[20] + z[68] / T(4);
z[71] = -z[71] + z[74];
z[71] = z[23] * z[71];
z[75] = z[6] / T(2);
z[76] = T(7) * z[1] + z[66];
z[76] = -z[11] + z[76] / T(3);
z[76] = z[41] + z[76] / T(2);
z[76] = z[75] * z[76];
z[77] = z[45] * z[50];
z[78] = z[11] + z[13];
z[79] = z[41] + z[78];
z[79] = z[40] * z[79];
z[80] = z[3] * z[11];
z[81] = -(z[10] * z[66]);
z[70] = -(z[14] * z[70]);
z[69] = z[24] * z[69];
z[53] = z[53] + z[58] / T(4) + z[59] + z[60] + z[62] + z[63] / T(2) + z[65] + z[69] + z[70] + z[71] + z[72] + z[73] + z[76] + -z[77] / T(3) + (T(-5) * z[79]) / T(6) + z[80] + (T(2) * z[81]) / T(3);
z[53] = z[29] * z[53];
z[58] = T(3) * z[9];
z[59] = T(3) * z[11];
z[60] = -z[55] + z[58] + z[59];
z[62] = z[4] / T(2);
z[63] = z[60] * z[62];
z[65] = T(3) * z[13] + z[59];
z[69] = -z[1] + z[9] + z[65];
z[70] = z[7] / T(2);
z[69] = z[69] * z[70];
z[71] = z[1] + z[9];
z[72] = z[65] + -z[71];
z[73] = z[67] * z[72];
z[76] = z[55] + z[58];
z[82] = z[3] * z[76];
z[80] = (T(3) * z[80]) / T(2) + z[82] / T(2);
z[83] = z[80] + z[81];
z[84] = z[2] / T(2);
z[84] = z[72] * z[84];
z[68] = T(2) * z[20] + z[68] / T(2);
z[85] = z[30] * z[68];
z[86] = z[24] * z[68];
z[87] = z[85] + -z[86];
z[63] = -z[63] + z[69] + z[73] + z[83] + -z[84] + z[87];
z[69] = z[38] * z[63];
z[73] = -z[11] + z[55];
z[88] = -z[9] + -z[73];
z[89] = z[67] * z[88];
z[90] = z[87] + z[89];
z[91] = T(3) * z[66];
z[92] = -z[1] + z[11];
z[93] = z[91] + -z[92];
z[93] = -z[41] + z[93] / T(2);
z[94] = z[32] * z[93];
z[95] = z[42] * z[68];
z[95] = z[94] + -z[95];
z[96] = -z[41] + z[66];
z[97] = z[7] * z[96];
z[98] = z[88] / T(2);
z[99] = z[4] * z[98];
z[100] = -z[90] + -z[95] + z[97] + -z[99];
z[100] = z[27] * z[100];
z[101] = z[9] + z[11];
z[102] = -z[55] + z[101];
z[103] = z[7] * z[102];
z[104] = z[3] * z[59];
z[82] = z[82] + z[104];
z[103] = z[82] + z[103];
z[104] = z[23] * z[68];
z[85] = z[85] + z[104];
z[105] = T(2) * z[101];
z[106] = z[4] * z[105];
z[89] = -z[85] + -z[89] + -z[103] / T(2) + z[106];
z[103] = z[26] * z[89];
z[86] = z[86] + z[104];
z[106] = z[7] * z[78];
z[106] = -z[84] + -z[86] + z[106];
z[107] = z[12] * z[78];
z[99] = z[99] + z[106] + T(2) * z[107];
z[99] = z[25] * z[99];
z[99] = z[99] + -z[103];
z[103] = z[26] * z[98];
z[108] = z[25] * z[98];
z[103] = z[103] + -z[108];
z[109] = T(2) * z[27];
z[110] = z[96] * z[109];
z[111] = z[103] + -z[110];
z[111] = z[6] * z[111];
z[111] = -z[99] + -z[100] + z[111];
z[112] = z[4] * z[101];
z[96] = -(z[6] * z[96]);
z[96] = -z[79] + z[81] + z[96] + -z[107] + z[112];
z[96] = z[28] * z[96];
z[113] = z[6] * z[98];
z[89] = z[89] + z[113];
z[89] = z[0] * z[89];
z[114] = T(3) * z[45] + -z[46] / T(2);
z[114] = z[46] * z[114];
z[115] = prod_pow(z[45], 2);
z[115] = z[47] + z[115] / T(2);
z[114] = z[114] + T(-3) * z[115];
z[114] = z[52] * z[114];
z[115] = z[50] * z[115];
z[77] = -(z[46] * z[77]);
z[116] = z[42] / T(2);
z[117] = -(prod_pow(z[46], 2) * z[116]);
z[69] = z[69] + z[77] + z[89] + z[96] + z[111] + z[114] + z[115] + z[117];
z[69] = int_to_imaginary<T>(1) * z[69];
z[58] = -z[13] + z[58] + z[92];
z[58] = -z[41] + z[58] / T(2);
z[77] = z[7] * z[58];
z[76] = z[59] + z[76];
z[89] = z[62] * z[76];
z[77] = z[77] + -z[79] + z[82] / T(2) + z[86] + -z[89] + -z[95];
z[82] = z[6] * z[93];
z[82] = z[77] + z[82];
z[86] = int_to_imaginary<T>(1) * z[35];
z[82] = z[82] * z[86];
z[49] = T(2) * z[7] + T(3) * z[49] + -z[51];
z[49] = z[43] * z[49];
z[49] = z[49] + z[53] + z[69] + z[82];
z[49] = z[29] * z[49];
z[51] = z[23] + z[24];
z[53] = z[14] + z[51];
z[69] = z[0] / T(2);
z[69] = z[53] * z[69];
z[82] = z[25] * z[51];
z[89] = z[26] * z[52];
z[82] = z[82] + -z[89];
z[89] = z[28] * z[51];
z[96] = -z[24] + z[30];
z[114] = z[14] + z[96];
z[115] = z[27] * z[114];
z[69] = z[69] + -z[82] + -z[89] + -z[115];
z[69] = z[0] * z[69];
z[51] = z[42] + z[51];
z[89] = z[28] / T(2);
z[117] = z[51] * z[89];
z[118] = -z[42] + z[96];
z[119] = z[27] * z[118];
z[82] = z[82] + z[119];
z[117] = z[82] + z[117];
z[117] = z[28] * z[117];
z[120] = z[23] * z[25];
z[118] = z[23] + z[118];
z[121] = z[26] * z[118];
z[119] = -z[119] + -z[120] + z[121] / T(2);
z[119] = z[26] * z[119];
z[120] = prod_pow(z[25], 2);
z[121] = -z[33] + -z[120] / T(2);
z[114] = -z[23] + z[114];
z[114] = z[114] * z[121];
z[121] = z[14] + z[52];
z[121] = z[39] * z[121];
z[122] = z[37] * z[96];
z[123] = z[34] * z[51];
z[115] = z[25] * z[115];
z[118] = z[36] * z[118];
z[69] = z[69] + z[114] + z[115] + z[117] + z[118] + z[119] + z[121] + -z[122] + z[123];
z[96] = z[38] * z[96];
z[114] = z[0] * z[52];
z[82] = z[82] + z[96] + -z[114];
z[82] = int_to_imaginary<T>(1) * z[82];
z[96] = z[51] * z[86];
z[82] = z[82] + z[96];
z[96] = z[14] / T(2) + -z[24] + -z[30] + -z[116];
z[114] = (T(3) * z[23]) / T(2) + -z[96];
z[114] = z[29] * z[114];
z[114] = T(3) * z[82] + z[114];
z[114] = z[29] * z[114];
z[114] = T(-3) * z[69] + z[114];
z[115] = z[15] + z[16] + z[18];
z[115] = -z[115] / T(2);
z[114] = z[114] * z[115];
z[96] = -z[23] / T(2) + z[96] / T(3);
z[96] = z[29] * z[96];
z[82] = -z[82] + z[96];
z[82] = z[29] * z[82];
z[69] = z[69] + z[82];
z[69] = z[19] * z[69];
z[82] = z[25] + -z[27];
z[82] = z[82] * z[88];
z[96] = z[26] * z[105];
z[82] = z[82] / T(2) + -z[96];
z[76] = z[76] / T(2);
z[96] = -(z[38] * z[76]);
z[101] = z[28] * z[101];
z[105] = z[0] * z[105];
z[96] = -z[82] + z[96] + z[101] + z[105];
z[96] = int_to_imaginary<T>(1) * z[96];
z[86] = z[60] * z[86];
z[61] = -z[9] + -z[61];
z[54] = -z[54] / T(9) + -z[57] + z[61] / T(2);
z[54] = z[29] * z[54];
z[54] = T(3) * z[43] + z[54] + -z[86] / T(2) + z[96];
z[54] = z[29] * z[54];
z[57] = z[34] * z[60];
z[60] = z[98] * z[120];
z[59] = z[56] + z[59];
z[61] = z[36] * z[59];
z[86] = T(5) * z[11] + -z[56];
z[96] = z[33] * z[86];
z[57] = z[57] + z[60] + z[61] + z[96];
z[60] = z[26] * z[59];
z[61] = z[11] * z[25];
z[96] = z[27] * z[98];
z[60] = z[60] / T(4) + T(-2) * z[61] + z[96];
z[60] = z[26] * z[60];
z[61] = z[28] * z[102];
z[61] = z[61] / T(4) + z[82];
z[61] = z[28] * z[61];
z[89] = z[0] / T(4) + -z[89];
z[89] = z[89] * z[102];
z[82] = -z[82] + z[89];
z[82] = z[0] * z[82];
z[89] = z[11] * z[27];
z[89] = z[89] + -z[108];
z[89] = z[27] * z[89];
z[76] = -(z[37] * z[76]);
z[54] = z[54] + z[57] / T(2) + z[60] + z[61] + z[76] + z[82] + z[89];
z[54] = z[5] * z[54];
z[57] = z[11] + T(3) * z[56];
z[60] = z[8] * z[57];
z[61] = z[60] / T(2);
z[76] = -z[61] + z[80] + T(3) * z[81];
z[80] = z[67] * z[78];
z[82] = z[88] / T(4);
z[89] = z[6] * z[82];
z[80] = z[80] + z[89];
z[53] = -(z[53] * z[74]);
z[73] = -(z[70] * z[73]);
z[55] = z[55] + -z[64];
z[55] = z[4] * z[55];
z[53] = z[53] + z[55] + z[73] + z[76] / T(2) + z[80] + -z[84];
z[53] = z[0] * z[53];
z[55] = z[11] + z[56];
z[64] = -(z[55] * z[62]);
z[64] = z[64] + -z[81] + -z[106] + -z[107] + -z[113];
z[64] = z[28] * z[64];
z[68] = z[14] * z[68];
z[61] = z[61] + z[68];
z[68] = z[7] * z[98];
z[73] = z[4] * z[56];
z[68] = z[61] + -z[68] + T(-2) * z[73] + z[90];
z[76] = z[27] * z[68];
z[81] = z[56] * z[109];
z[89] = -z[81] + z[103];
z[89] = z[6] * z[89];
z[53] = z[53] + z[64] + z[76] + z[89] + -z[99];
z[53] = z[0] * z[53];
z[63] = z[37] * z[63];
z[62] = z[62] + z[75];
z[57] = z[57] * z[62];
z[64] = z[70] * z[72];
z[65] = z[65] + z[71];
z[65] = z[65] * z[67];
z[57] = z[57] + -z[61] + z[64] + z[65] + -z[84] + -z[85];
z[57] = z[39] * z[57];
z[64] = -(z[34] * z[77]);
z[65] = z[31] * z[59];
z[72] = z[65] / T(2) + z[104];
z[75] = z[7] * z[93];
z[59] = z[59] * z[67];
z[59] = z[59] + -z[72] + z[75] + -z[87] + -z[95];
z[59] = z[36] * z[59];
z[75] = z[79] + z[94] / T(2);
z[76] = z[65] / T(4);
z[77] = z[24] + z[42] + -z[52];
z[77] = z[74] * z[77];
z[66] = -z[66] + -z[92];
z[66] = z[41] + z[66] / T(2);
z[66] = z[66] * z[70];
z[79] = -(z[12] * z[82]);
z[66] = z[66] + -z[75] + -z[76] + z[77] + z[79] + z[112];
z[66] = z[26] * z[66];
z[77] = -z[4] + z[7];
z[77] = z[77] * z[88];
z[65] = z[65] + z[77];
z[77] = z[11] * z[12];
z[65] = z[65] / T(2) + T(-2) * z[77] + z[104];
z[65] = z[25] * z[65];
z[65] = z[65] + z[66] + -z[100];
z[65] = z[26] * z[65];
z[66] = z[26] * z[82];
z[66] = z[66] + -z[108] + -z[110];
z[66] = z[26] * z[66];
z[79] = z[82] * z[120];
z[84] = -(z[34] * z[93]);
z[81] = z[25] * z[81];
z[85] = z[91] + z[92];
z[85] = T(-2) * z[41] + z[85] / T(2);
z[85] = z[36] * z[85];
z[66] = z[66] + z[79] + z[81] + z[84] + z[85];
z[66] = z[6] * z[66];
z[51] = -(z[51] * z[74]);
z[58] = -(z[58] * z[70]);
z[55] = z[4] * z[55];
z[51] = z[51] + z[55] / T(4) + z[58] + z[75] + z[80] + -z[83] / T(2);
z[51] = z[28] * z[51];
z[51] = z[51] + -z[111];
z[51] = z[28] * z[51];
z[55] = -z[11] + T(5) * z[56];
z[55] = -(z[55] * z[62]);
z[56] = z[67] * z[86];
z[58] = -(z[7] * z[88]);
z[55] = z[55] + z[56] + z[58] + z[61] + -z[72] + z[87];
z[55] = z[33] * z[55];
z[56] = z[12] * z[86];
z[56] = z[56] + z[60];
z[58] = -z[23] + z[30] + -z[50];
z[58] = z[58] * z[74];
z[60] = z[71] + z[78];
z[60] = z[60] * z[70];
z[61] = z[4] * z[82];
z[56] = z[56] / T(4) + z[58] + z[60] + z[61] + -z[76];
z[56] = z[56] * z[120];
z[58] = -(z[25] * z[68]);
z[60] = -z[73] + z[77] + z[97];
z[60] = z[27] * z[60];
z[58] = z[58] + z[60];
z[58] = z[27] * z[58];
z[50] = (T(7) * z[42]) / T(4) + (T(19) * z[50]) / T(3) + (T(11) * z[52]) / T(4);
z[50] = z[48] * z[50];
return z[49] + z[50] / T(2) + z[51] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[63] + z[64] + z[65] + z[66] + z[69] + z[114];
}



template IntegrandConstructorType<double> f_4_309_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_309_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_309_construct (const Kin<qd_real>&);
#endif

}