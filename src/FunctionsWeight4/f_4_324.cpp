#include "f_4_324.h"

namespace PentagonFunctions {

template <typename T> T f_4_324_abbreviated (const std::array<T,42>&);

template <typename T> class SpDLog_f_4_324_W_23 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_324_W_23 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(-6) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(3) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(-6) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(3) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4])) + rlog(kin.v[3]) * (kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(6) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-3) + prod_pow(abb[1], 2) * (abb[5] * T(-3) + abb[6] * T(-3) + abb[3] * T(3) + abb[4] * T(3)) + prod_pow(abb[2], 2) * (abb[4] * T(-3) + abb[5] * T(3) + abb[6] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_324_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_324_W_22 (const Kin<T>& kin) {
        c[0] = T(2) * prod_pow(kin.v[3], 2) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + T(-2) * prod_pow(kin.v[4], 2) + kin.v[1] * (T(4) * kin.v[0] + T(-2) * kin.v[1] + T(-12) * kin.v[2] + T(4) * kin.v[4]) + kin.v[2] * (T(-10) * kin.v[2] + T(8) * kin.v[3] + T(12) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[1] * (T(4) + T(-4) * kin.v[0] + T(-2) * kin.v[1] + T(4) * kin.v[3]) + kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = kin.v[1] * ((bc<T>[0] * int_to_imaginary<T>(5) + T(10)) * kin.v[1] + T(12) * kin.v[2] + T(-24) * kin.v[3] + T(-20) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(6) * kin.v[2] + T(-12) * kin.v[3] + T(-10) * kin.v[4])) + kin.v[2] * (T(2) * kin.v[2] + T(-16) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[0] * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4])) + T(10) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(14) * kin.v[3] + T(24) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(5) * prod_pow(kin.v[4], 2) + kin.v[2] * (kin.v[2] + T(-8) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[3] * (T(7) * kin.v[3] + T(12) * kin.v[4])) + rlog(kin.v[2]) * (-prod_pow(kin.v[3], 2) / T(2) + prod_pow(kin.v[4], 2) / T(2) + kin.v[1] * (kin.v[1] / T(2) + -kin.v[0] + -kin.v[4] + T(3) * kin.v[2]) + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]) + kin.v[2] * ((T(5) * kin.v[2]) / T(2) + T(-2) * kin.v[3] + T(-3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + T(-1) + kin.v[0]) * kin.v[1] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (-prod_pow(kin.v[3], 2) / T(2) + prod_pow(kin.v[4], 2) / T(2) + kin.v[1] * (kin.v[1] / T(2) + -kin.v[0] + -kin.v[4] + T(3) * kin.v[2]) + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]) + kin.v[2] * ((T(5) * kin.v[2]) / T(2) + T(-2) * kin.v[3] + T(-3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + T(-1) + kin.v[0]) * kin.v[1] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (-prod_pow(kin.v[3], 2) / T(2) + prod_pow(kin.v[4], 2) / T(2) + kin.v[1] * (kin.v[1] / T(2) + -kin.v[0] + -kin.v[4] + T(3) * kin.v[2]) + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]) + kin.v[2] * ((T(5) * kin.v[2]) / T(2) + T(-2) * kin.v[3] + T(-3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + T(-1) + kin.v[0]) * kin.v[1] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (-prod_pow(kin.v[3], 2) / T(2) + prod_pow(kin.v[4], 2) / T(2) + kin.v[1] * (kin.v[1] / T(2) + -kin.v[0] + -kin.v[4] + T(3) * kin.v[2]) + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]) + kin.v[2] * ((T(5) * kin.v[2]) / T(2) + T(-2) * kin.v[3] + T(-3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + T(-1) + kin.v[0]) * kin.v[1] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]))) + rlog(-kin.v[4]) * (-prod_pow(kin.v[3], 2) + prod_pow(kin.v[4], 2) + kin.v[2] * (T(5) * kin.v[2] + T(-4) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[0] + kin.v[1] + T(6) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]))) + rlog(kin.v[3]) * ((T(3) * prod_pow(kin.v[3], 2)) / T(2) + (T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(3) * kin.v[0] + T(-9) * kin.v[2] + T(3) * kin.v[4]) + kin.v[2] * ((T(-15) * kin.v[2]) / T(2) + T(6) * kin.v[3] + T(9) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(3) * prod_pow(kin.v[3], 2)) / T(2) + (T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(3) * kin.v[0] + T(-9) * kin.v[2] + T(3) * kin.v[4]) + kin.v[2] * ((T(-15) * kin.v[2]) / T(2) + T(6) * kin.v[3] + T(9) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(4) + T(8)) * kin.v[1] + T(8) * kin.v[2] + T(-8) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + T(-8) * kin.v[4];
c[3] = rlog(kin.v[3]) * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4])) + rlog(kin.v[2]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(-kin.v[4]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[21] * (abb[8] * (abb[9] * abb[20] + -(abb[2] * abb[20]) + abb[22] * T(-2) + abb[20] * (abb[20] + bc<T>[0] * int_to_imaginary<T>(1))) + abb[2] * abb[20] * (-abb[4] + -abb[5] + -abb[6] + abb[24] * T(-4) + abb[23] * T(-2) + abb[3] * T(3) + abb[12] * T(3)) + abb[9] * abb[20] * (abb[4] + abb[5] + abb[6] + abb[3] * T(-3) + abb[12] * T(-3) + abb[23] * T(2) + abb[24] * T(4)) + abb[20] * (abb[4] * bc<T>[0] * int_to_imaginary<T>(1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(1) + abb[23] * bc<T>[0] * int_to_imaginary<T>(2) + abb[3] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[24] * bc<T>[0] * int_to_imaginary<T>(4) + abb[20] * (abb[4] + abb[5] + abb[6] + abb[3] * T(-3) + abb[12] * T(-3) + abb[23] * T(2) + abb[24] * T(4))) + abb[19] * (-(abb[8] * abb[20]) + abb[20] * (-abb[4] + -abb[5] + -abb[6] + abb[24] * T(-4) + abb[23] * T(-2) + abb[3] * T(3) + abb[12] * T(3)) + abb[1] * (abb[4] + abb[5] + abb[6] + abb[8] + abb[3] * T(-3) + abb[12] * T(-3) + abb[23] * T(2) + abb[24] * T(4))) + abb[22] * (abb[24] * T(-8) + abb[23] * T(-4) + abb[4] * T(-2) + abb[5] * T(-2) + abb[6] * T(-2) + abb[3] * T(6) + abb[12] * T(6)) + abb[1] * (abb[4] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[8] * (abb[2] + abb[20] + -abb[9] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[23] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[3] * bc<T>[0] * int_to_imaginary<T>(3) + abb[12] * bc<T>[0] * int_to_imaginary<T>(3) + abb[24] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[9] * (-abb[4] + -abb[5] + -abb[6] + abb[24] * T(-4) + abb[23] * T(-2) + abb[3] * T(3) + abb[12] * T(3)) + abb[2] * (abb[4] + abb[5] + abb[6] + abb[3] * T(-3) + abb[12] * T(-3) + abb[23] * T(2) + abb[24] * T(4)) + abb[20] * (abb[4] + abb[5] + abb[6] + abb[3] * T(-3) + abb[12] * T(-3) + abb[23] * T(2) + abb[24] * T(4)) + abb[1] * (abb[24] * T(-8) + abb[23] * T(-4) + abb[4] * T(-2) + abb[5] * T(-2) + abb[6] * T(-2) + abb[8] * T(-2) + abb[3] * T(6) + abb[12] * T(6))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_324_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl18 = DLog_W_18<T>(kin),dl27 = DLog_W_27<T>(kin),dl8 = DLog_W_8<T>(kin),dl22 = DLog_W_22<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl5 = DLog_W_5<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl17 = DLog_W_17<T>(kin),dl16 = DLog_W_16<T>(kin),dl3 = DLog_W_3<T>(kin),dl19 = DLog_W_19<T>(kin),dl20 = DLog_W_20<T>(kin),dl4 = DLog_W_4<T>(kin),spdl23 = SpDLog_f_4_324_W_23<T>(kin),spdl22 = SpDLog_f_4_324_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,42> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[19] / kin_path.W[19]), dl18(t), rlog(kin.W[2] / kin_path.W[2]), rlog(-v_path[4]), f_2_1_7(kin_path), f_2_1_8(kin_path), rlog(kin.W[18] / kin_path.W[18]), dl27(t) / kin_path.SqrtDelta, f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl8(t), rlog(v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), dl22(t), f_2_1_14(kin_path), rlog(kin.W[4] / kin_path.W[4]), -rlog(t), dl2(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl1(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl5(t), dl28(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl17(t), dl16(t), dl3(t), dl19(t), dl20(t), dl4(t)}
;

        auto result = f_4_324_abbreviated(abbr);
        result = result + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_324_abbreviated(const std::array<T,42>& abb)
{
using TR = typename T::value_type;
T z[127];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[7];
z[3] = abb[31];
z[4] = abb[36];
z[5] = abb[37];
z[6] = abb[38];
z[7] = abb[39];
z[8] = abb[40];
z[9] = abb[41];
z[10] = abb[4];
z[11] = abb[5];
z[12] = abb[6];
z[13] = abb[8];
z[14] = abb[12];
z[15] = abb[13];
z[16] = abb[14];
z[17] = abb[15];
z[18] = abb[16];
z[19] = abb[17];
z[20] = abb[32];
z[21] = abb[35];
z[22] = abb[23];
z[23] = abb[24];
z[24] = abb[2];
z[25] = abb[9];
z[26] = abb[19];
z[27] = abb[20];
z[28] = bc<TR>[0];
z[29] = abb[33];
z[30] = abb[34];
z[31] = abb[25];
z[32] = abb[10];
z[33] = abb[11];
z[34] = abb[28];
z[35] = abb[22];
z[36] = abb[26];
z[37] = abb[27];
z[38] = abb[29];
z[39] = abb[30];
z[40] = abb[18];
z[41] = bc<TR>[3];
z[42] = bc<TR>[1];
z[43] = bc<TR>[2];
z[44] = bc<TR>[4];
z[45] = bc<TR>[9];
z[46] = -z[13] + z[14];
z[47] = z[10] + -z[12];
z[48] = z[1] + -z[11];
z[49] = -z[46] + -z[47] + z[48];
z[50] = -(z[31] * z[49]);
z[51] = T(3) * z[13];
z[52] = T(3) * z[14];
z[53] = z[51] + -z[52];
z[54] = (T(7) * z[47]) / T(3) + -z[48] + z[53];
z[54] = z[3] * z[54];
z[55] = z[16] + z[17] + -z[18] + -z[19];
z[56] = z[29] * z[55];
z[57] = z[20] * z[55];
z[58] = z[56] + z[57];
z[59] = z[1] + z[14];
z[60] = z[11] + z[13];
z[61] = z[59] + -z[60];
z[62] = z[47] + z[61];
z[63] = z[2] * z[62];
z[64] = z[21] * z[55];
z[54] = -z[50] + -z[54] + z[58] + z[63] + T(3) * z[64];
z[65] = (T(5) * z[12]) / T(3);
z[66] = T(5) * z[14];
z[67] = T(3) * z[1];
z[68] = T(5) * z[11];
z[69] = T(-19) * z[13] + z[68];
z[69] = z[65] + z[66] + -z[67] + z[69] / T(3);
z[70] = z[10] / T(12) + -z[22] / T(3) + (T(-2) * z[23]) / T(3) + z[42];
z[69] = z[69] / T(4) + -z[70];
z[69] = z[5] * z[69];
z[71] = z[11] + z[12];
z[51] = -z[1] + z[51] + -z[66] + -z[71];
z[51] = z[51] / T(2);
z[66] = (T(3) * z[10]) / T(2);
z[72] = z[22] + z[51] + z[66];
z[72] = z[72] / T(2);
z[73] = -z[23] + z[42];
z[74] = -z[72] + z[73];
z[74] = z[7] * z[74];
z[75] = T(13) * z[11] + z[13];
z[65] = z[14] + -z[65] + -z[67] + z[75] / T(3);
z[65] = z[65] / T(4) + z[70];
z[65] = z[8] * z[65];
z[70] = z[52] + z[67];
z[75] = -z[60] + z[70];
z[76] = (T(25) * z[12]) / T(3) + -z[75];
z[76] = (T(-19) * z[10]) / T(6) + z[22] + z[76] / T(2);
z[77] = T(3) * z[43];
z[76] = -z[73] + z[76] / T(2) + z[77];
z[76] = z[6] * z[76];
z[78] = -z[46] + z[47];
z[79] = T(3) * z[40];
z[78] = z[78] * z[79];
z[61] = -z[47] + z[61];
z[79] = z[34] * z[61];
z[80] = T(3) * z[12];
z[81] = T(5) * z[1] + T(-3) * z[11] + z[13] + z[14] + -z[80];
z[81] = z[81] / T(2);
z[82] = z[10] / T(2);
z[83] = -z[22] + z[81] + z[82];
z[83] = z[73] + z[83] / T(2);
z[83] = z[9] * z[83];
z[75] = z[12] / T(3) + -z[75];
z[75] = (T(5) * z[10]) / T(6) + z[22] + z[75] / T(2);
z[73] = -z[73] + z[75] / T(2);
z[73] = z[4] * z[73];
z[75] = -z[5] + z[7] + z[8];
z[84] = -(z[75] * z[77]);
z[85] = z[30] * z[55];
z[86] = (T(3) * z[85]) / T(4);
z[54] = -z[54] / T(4) + z[65] + z[69] + z[73] + z[74] + z[76] + z[78] + (T(5) * z[79]) / T(4) + z[83] + z[84] + -z[86];
z[54] = z[28] * z[54];
z[65] = z[20] + -z[21] + z[29] + -z[30];
z[69] = z[41] * z[65];
z[54] = z[54] + T(3) * z[69];
z[54] = z[28] * z[54];
z[69] = z[23] + z[72];
z[72] = T(3) * z[7];
z[69] = z[69] * z[72];
z[68] = T(5) * z[12] + z[68];
z[72] = z[13] + z[67];
z[73] = z[52] + z[68] + -z[72];
z[74] = z[22] + T(2) * z[23];
z[76] = (T(7) * z[10]) / T(2) + z[74];
z[73] = z[73] / T(2) + -z[76];
z[73] = z[5] * z[73];
z[83] = T(9) * z[1];
z[84] = T(9) * z[14];
z[87] = z[12] + z[60];
z[88] = z[83] + z[84] + -z[87];
z[89] = T(2) * z[22] + T(4) * z[23];
z[88] = (T(-7) * z[10]) / T(4) + z[88] / T(4) + -z[89];
z[88] = z[3] * z[88];
z[90] = (T(3) * z[63]) / T(4);
z[91] = T(2) * z[12];
z[92] = -z[10] + z[74] + z[91];
z[72] = T(2) * z[11] + -z[72] + z[92];
z[93] = z[8] * z[72];
z[94] = -z[74] + z[82];
z[81] = z[81] + z[94];
z[95] = z[9] * z[81];
z[96] = T(3) * z[95];
z[97] = z[4] * z[72];
z[98] = z[57] / T(2) + -z[64];
z[73] = z[69] + z[73] + z[88] + -z[90] + -z[93] + -z[96] + -z[97] + (T(3) * z[98]) / T(2);
z[88] = prod_pow(z[0], 2);
z[73] = z[73] * z[88];
z[98] = z[4] * z[49];
z[99] = z[50] + z[56];
z[100] = z[8] * z[61];
z[46] = z[1] + z[10] + -z[46] + -z[71];
z[101] = z[6] * z[46];
z[102] = z[3] * z[46];
z[98] = -z[79] + z[85] + -z[98] + -z[99] + z[100] + z[101] + -z[102];
z[98] = (T(3) * z[98]) / T(2);
z[100] = -(z[37] * z[98]);
z[70] = z[70] + -z[87];
z[101] = z[70] / T(2) + -z[82];
z[103] = -z[74] + z[101];
z[104] = z[4] * z[103];
z[105] = (T(3) * z[55]) / T(2);
z[105] = z[30] * z[105];
z[104] = z[104] + z[105];
z[106] = z[8] * z[103];
z[107] = z[104] + z[106];
z[66] = z[66] + z[74];
z[51] = z[51] + z[66];
z[108] = T(3) * z[51];
z[109] = z[7] * z[108];
z[110] = T(2) * z[10] + z[74];
z[111] = T(2) * z[13] + -z[52] + -z[71] + z[110];
z[112] = z[5] * z[111];
z[113] = z[3] * z[103];
z[114] = (T(-3) * z[58]) / T(2) + z[107] + -z[109] + T(2) * z[112] + -z[113];
z[114] = z[0] * z[114];
z[115] = (T(3) * z[79]) / T(2);
z[116] = z[5] * z[103];
z[117] = (T(3) * z[64]) / T(2) + z[116];
z[118] = z[115] + -z[117];
z[48] = z[13] + z[48];
z[48] = T(3) * z[48] + -z[52];
z[119] = z[47] + z[48];
z[120] = z[119] / T(2);
z[121] = z[4] * z[120];
z[48] = -z[47] + z[48];
z[48] = z[6] * z[48];
z[92] = -z[60] + z[92];
z[122] = z[8] * z[92];
z[123] = z[3] * z[47];
z[124] = -z[122] + z[123];
z[121] = -z[48] / T(2) + -z[105] + z[118] + z[121] + z[124];
z[121] = z[25] * z[121];
z[114] = z[114] + z[121];
z[79] = -z[58] + -z[64] + z[79];
z[121] = z[6] + -z[7];
z[125] = z[5] + z[121];
z[51] = z[51] * z[125];
z[80] = -z[59] + -z[60] + z[80];
z[80] = z[80] / T(2) + -z[94];
z[94] = -(z[8] * z[80]);
z[125] = z[3] / T(2);
z[126] = -(z[61] * z[125]);
z[51] = z[51] + z[79] / T(2) + z[94] + z[126];
z[51] = z[39] * z[51];
z[77] = z[42] * z[77];
z[79] = prod_pow(z[42], 2);
z[77] = -z[77] + (T(3) * z[79]) / T(2);
z[79] = z[4] + -z[9];
z[75] = z[75] + -z[79];
z[94] = -(z[75] * z[77]);
z[126] = T(2) * z[111];
z[126] = z[0] * z[126];
z[77] = z[77] + -z[126];
z[77] = z[6] * z[77];
z[65] = -z[65] / T(9) + -z[79] / T(4) + (T(-3) * z[121]) / T(4);
z[121] = prod_pow(z[28], 2);
z[65] = z[65] * z[121];
z[75] = z[6] + -z[75];
z[75] = z[44] * z[75];
z[51] = T(3) * z[51] + z[65] + T(3) * z[75] + z[77] + z[94] + z[100] + -z[114];
z[65] = int_to_imaginary<T>(1) * z[28];
z[51] = z[51] * z[65];
z[60] = -z[22] + z[60];
z[60] = z[12] + z[23] + -z[60] / T(2) + -z[82];
z[60] = z[8] * z[60];
z[75] = -z[22] + z[101];
z[75] = -z[23] + z[75] / T(2);
z[77] = z[5] * z[75];
z[94] = z[119] / T(4);
z[100] = z[4] * z[94];
z[60] = z[60] + z[77] + z[86] + -z[100];
z[77] = z[1] + -z[13] + -z[22];
z[77] = T(-3) * z[23] + z[52] + (T(3) * z[77]) / T(2) + z[82] + -z[91];
z[77] = z[6] * z[77];
z[57] = -z[50] + z[57] + z[64];
z[53] = -z[47] + -z[53];
z[53] = z[53] * z[125];
z[53] = z[53] + (T(3) * z[57]) / T(4) + z[60] + z[69] + z[77] + -z[78] + -z[115];
z[53] = z[26] * z[53];
z[57] = z[64] + z[99];
z[69] = z[3] * z[120];
z[77] = T(2) * z[47];
z[82] = z[4] * z[77];
z[77] = z[6] * z[77];
z[57] = (T(3) * z[57]) / T(2) + z[69] + -z[77] + -z[82] + -z[106] + z[116];
z[69] = z[24] * z[57];
z[78] = z[78] + -z[112];
z[86] = z[4] * z[47];
z[77] = -z[77] + z[78] + z[86] + -z[124];
z[77] = z[65] * z[77];
z[86] = z[6] * z[126];
z[53] = z[53] + z[69] + T(2) * z[77] + z[86] + z[114];
z[53] = z[26] * z[53];
z[69] = T(2) * z[8];
z[69] = z[69] * z[72];
z[69] = z[69] + z[96] + T(2) * z[97] + -z[113] + z[117];
z[72] = z[0] * z[69];
z[77] = z[6] * z[103];
z[86] = z[0] * z[77];
z[72] = z[72] + z[86];
z[69] = -z[69] + -z[77];
z[69] = z[24] * z[69];
z[91] = z[3] * z[92];
z[91] = z[91] + T(2) * z[122];
z[77] = z[77] + z[91] + z[104] + -z[118];
z[65] = z[25] + z[65];
z[92] = -z[26] + z[65];
z[77] = z[77] * z[92];
z[92] = -(z[6] * z[111]);
z[78] = z[78] + z[91] + z[92] + -z[97];
z[78] = z[27] * z[78];
z[69] = z[69] + z[72] + z[77] + z[78];
z[69] = z[27] * z[69];
z[77] = z[36] * z[98];
z[57] = -(z[57] * z[65]);
z[65] = -z[87] + z[110];
z[78] = z[5] * z[65];
z[47] = -(z[6] * z[47]);
z[47] = z[47] + z[78] + z[82] + -z[93] + z[123];
z[47] = z[24] * z[47];
z[47] = z[47] + z[57] + z[72];
z[47] = z[24] * z[47];
z[57] = T(-7) * z[13] + -z[67] + z[68] + z[84];
z[57] = z[57] / T(2) + -z[76];
z[67] = z[5] * z[57];
z[52] = T(-5) * z[13] + z[52] + T(7) * z[71] + -z[83];
z[52] = (T(-5) * z[10]) / T(2) + z[52] / T(2) + z[74];
z[68] = -(z[4] * z[52]);
z[58] = z[58] + -z[64];
z[58] = (T(3) * z[58]) / T(2) + z[67] + z[68] + -z[96] + -z[105] + z[109];
z[58] = z[35] * z[58];
z[50] = z[50] + z[64];
z[67] = z[63] + -z[85];
z[68] = z[5] * z[62];
z[46] = -(z[4] * z[46]);
z[46] = z[46] + z[50] + -z[67] + z[68] + z[102];
z[68] = (T(3) * z[32]) / T(2);
z[46] = z[46] * z[68];
z[71] = z[3] * z[94];
z[48] = z[48] / T(4) + (T(3) * z[50]) / T(4) + z[60] + z[71] + -z[90];
z[48] = z[25] * z[48];
z[50] = z[56] + z[63];
z[60] = -(z[3] * z[65]);
z[50] = (T(3) * z[50]) / T(2) + z[60] + T(-2) * z[78] + -z[107];
z[50] = z[0] * z[50];
z[48] = z[48] + z[50] + -z[86];
z[48] = z[25] * z[48];
z[50] = z[56] + z[64] + z[67];
z[59] = -z[59] + -z[87];
z[59] = z[59] / T(2) + z[66];
z[59] = z[5] * z[59];
z[60] = z[4] * z[81];
z[50] = -z[50] / T(2) + z[59] + z[60] + -z[95];
z[50] = z[33] * z[50];
z[59] = z[38] * z[108];
z[60] = z[45] + z[59];
z[57] = z[35] * z[57];
z[49] = z[49] * z[68];
z[63] = -(z[75] * z[88]);
z[49] = z[49] + z[57] + -z[60] + z[63];
z[49] = z[6] * z[49];
z[57] = z[38] * z[80];
z[52] = -(z[35] * z[52]);
z[63] = T(3) * z[81];
z[63] = z[33] * z[63];
z[64] = (T(85) * z[45]) / T(6);
z[52] = z[52] + T(3) * z[57] + z[63] + z[64];
z[52] = z[8] * z[52];
z[57] = z[20] + z[21];
z[57] = z[55] * z[57];
z[56] = z[56] + z[57];
z[56] = (T(3) * z[56]) / T(2) + -z[115];
z[56] = z[38] * z[56];
z[57] = -z[59] + -z[64];
z[57] = z[5] * z[57];
z[59] = z[7] * z[60];
z[60] = z[38] * z[61];
z[61] = z[33] * z[62];
z[60] = z[60] + z[61];
z[61] = -z[10] + z[70] + -z[89];
z[61] = z[35] * z[61];
z[60] = (T(3) * z[60]) / T(2) + z[61];
z[60] = z[3] * z[60];
z[61] = -z[0] + z[25] / T(2);
z[61] = z[25] * z[61];
z[61] = z[32] + z[33] + z[61] + z[88] / T(2);
z[61] = T(3) * z[61] + z[121] / T(2);
z[55] = z[15] * z[55] * z[61];
z[61] = (T(-5) * z[79]) / T(2);
z[61] = z[45] * z[61];
return z[46] + z[47] + z[48] + z[49] + T(3) * z[50] + z[51] + z[52] + z[53] + z[54] + z[55] / T(2) + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[69] + z[73] + z[77];
}



template IntegrandConstructorType<double> f_4_324_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_324_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_324_construct (const Kin<qd_real>&);
#endif

}