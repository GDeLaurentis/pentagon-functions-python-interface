#include "f_4_216.h"

namespace PentagonFunctions {

template <typename T> T f_4_216_abbreviated (const std::array<T,18>&);



template <typename T> IntegrandConstructorType<T> f_4_216_construct (const Kin<T>& kin) {
    return [&kin, 
            dl13 = DLog_W_13<T>(kin),dl8 = DLog_W_8<T>(kin),dl20 = DLog_W_20<T>(kin),dl1 = DLog_W_1<T>(kin),dl3 = DLog_W_3<T>(kin),dl5 = DLog_W_5<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,18> abbr = 
            {dl13(t), rlog(kin.W[0] / kin_path.W[0]), rlog(v_path[0]), rlog(v_path[2]), f_2_1_1(kin_path), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[12] / kin_path.W[12]), rlog(kin.W[2] / kin_path.W[2]), dl8(t), rlog(kin.W[7] / kin_path.W[7]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl20(t), dl1(t), dl3(t), dl5(t)}
;

        auto result = f_4_216_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_216_abbreviated(const std::array<T,18>& abb)
{
using TR = typename T::value_type;
T z[63];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = bc<TR>[0];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = bc<TR>[1];
z[11] = bc<TR>[2];
z[12] = bc<TR>[4];
z[13] = bc<TR>[7];
z[14] = bc<TR>[8];
z[15] = bc<TR>[9];
z[16] = abb[14];
z[17] = abb[15];
z[18] = abb[16];
z[19] = abb[17];
z[20] = abb[9];
z[21] = abb[11];
z[22] = abb[12];
z[23] = abb[13];
z[24] = abb[10];
z[25] = -z[2] + z[3];
z[26] = z[21] * z[25];
z[27] = -z[22] + z[26];
z[28] = int_to_imaginary<T>(1) * z[4];
z[29] = T(2) * z[28];
z[30] = z[23] * z[29];
z[30] = T(2) * z[27] + z[30];
z[31] = prod_pow(z[3], 2);
z[32] = z[2] * z[29];
z[33] = prod_pow(z[2], 2);
z[34] = T(-2) * z[5] + -z[30] + z[31] + z[32] + -z[33];
z[35] = T(3) * z[19];
z[34] = z[34] * z[35];
z[36] = prod_pow(z[4], 2);
z[26] = T(6) * z[26] + -z[36];
z[37] = T(6) * z[28];
z[38] = z[23] * z[37];
z[38] = T(6) * z[22] + -z[38];
z[39] = z[2] * z[28];
z[40] = T(6) * z[39];
z[41] = z[38] + z[40];
z[42] = z[3] * z[25];
z[42] = -z[26] + z[41] + T(6) * z[42];
z[42] = z[20] * z[42];
z[43] = T(2) * z[2] + -z[3];
z[44] = T(3) * z[3];
z[43] = z[43] * z[44];
z[45] = T(3) * z[33];
z[43] = T(6) * z[5] + z[36] + -z[43] + z[45];
z[46] = z[16] + z[17];
z[43] = z[43] * z[46];
z[46] = z[2] + z[29];
z[47] = (T(3) * z[3]) / T(2) + -z[46];
z[47] = z[3] * z[47];
z[48] = z[33] / T(2);
z[32] = z[5] + z[32] + z[47] + -z[48];
z[47] = T(3) * z[32];
z[49] = z[0] * z[47];
z[50] = T(3) * z[18];
z[32] = z[32] * z[50];
z[34] = z[32] + z[34] + -z[42] + z[43] + -z[49];
z[42] = -z[1] + -z[7];
z[34] = z[34] * z[42];
z[42] = z[23] * z[28];
z[27] = z[5] + z[27] + -z[31] / T(2) + -z[39] + z[42] + z[48];
z[31] = z[27] * z[35];
z[35] = -z[21] + T(2) * z[25];
z[43] = T(3) * z[21];
z[35] = z[35] * z[43];
z[35] = z[35] + -z[41];
z[41] = -z[2] + z[29];
z[49] = z[3] / T(2);
z[50] = z[41] + z[49];
z[50] = z[44] * z[50];
z[51] = (T(7) * z[36]) / T(2);
z[52] = T(3) * z[5];
z[53] = (T(3) * z[33]) / T(2);
z[54] = -z[35] + z[50] + z[51] + -z[52] + -z[53];
z[54] = z[17] * z[54];
z[55] = z[52] + -z[53];
z[26] = -z[26] + z[38] + z[50] + -z[55];
z[26] = z[16] * z[26];
z[38] = -z[22] + z[42];
z[42] = T(3) * z[39];
z[38] = T(-3) * z[38] + z[42];
z[41] = -(z[41] * z[44]);
z[50] = -z[21] + z[25];
z[50] = z[43] * z[50];
z[41] = -z[38] + z[41] + z[50] + -z[51];
z[41] = z[20] * z[41];
z[26] = z[26] + z[31] + z[32] + z[41] + z[54];
z[26] = z[24] * z[26];
z[31] = -z[10] + z[37];
z[31] = z[10] * z[31];
z[31] = z[31] + -z[36];
z[31] = z[10] * z[31];
z[32] = prod_pow(z[11], 2);
z[37] = -z[32] + (T(5) * z[36]) / T(4);
z[37] = z[11] * z[37];
z[41] = T(3) * z[4];
z[41] = z[32] * z[41];
z[50] = prod_pow(z[4], 3);
z[41] = z[41] + z[50];
z[41] = int_to_imaginary<T>(1) * z[41];
z[51] = T(6) * z[13];
z[54] = z[12] * z[28];
z[56] = (T(3) * z[14]) / T(4);
z[31] = (T(-19) * z[15]) / T(8) + z[31] + z[37] + -z[41] + z[51] + T(12) * z[54] + z[56];
z[37] = z[16] * z[31];
z[41] = -z[46] + z[49];
z[49] = z[41] * z[44];
z[57] = (T(5) * z[36]) / T(2);
z[33] = T(9) * z[5] + (T(9) * z[33]) / T(2) + z[35] + z[49] + -z[57];
z[33] = z[6] * z[33];
z[35] = (T(13) * z[36]) / T(4);
z[40] = z[35] + z[40] + -z[45];
z[45] = z[8] + -z[9];
z[40] = z[40] * z[45];
z[31] = z[31] + z[33] + z[40];
z[31] = z[17] * z[31];
z[33] = T(3) * z[28];
z[40] = -(z[11] * z[33]);
z[40] = T(2) * z[36] + z[40];
z[49] = z[10] / T(2);
z[58] = z[10] + T(9) * z[28];
z[58] = z[49] * z[58];
z[59] = T(3) * z[12];
z[40] = T(2) * z[40] + z[58] + z[59];
z[40] = z[10] * z[40];
z[58] = T(-2) * z[32] + (T(-17) * z[36]) / T(4);
z[58] = z[11] * z[58];
z[60] = z[4] * z[32];
z[60] = (T(3) * z[60]) / T(2);
z[61] = -z[50] + z[60];
z[61] = int_to_imaginary<T>(1) * z[61];
z[62] = z[12] * z[33];
z[40] = (T(3) * z[14]) / T(2) + (T(-35) * z[15]) / T(8) + z[40] + z[51] + z[58] + z[61] + z[62];
z[40] = z[20] * z[40];
z[41] = z[3] * z[41];
z[30] = z[30] + z[41] + z[48] + z[52];
z[30] = z[16] * z[30];
z[41] = T(-2) * z[3] + z[46];
z[41] = z[41] * z[44];
z[25] = z[21] + z[25];
z[25] = z[25] * z[43];
z[25] = z[25] + -z[38] + z[41] + z[57];
z[25] = z[20] * z[25];
z[25] = z[25] + T(3) * z[30];
z[25] = z[6] * z[25];
z[30] = z[10] * z[28];
z[30] = (T(3) * z[30]) / T(2) + z[35] + z[59];
z[30] = z[10] * z[30];
z[35] = z[32] + z[36] / T(4);
z[35] = z[11] * z[35];
z[35] = (T(21) * z[15]) / T(8) + z[35] + -z[56];
z[30] = T(3) * z[13] + z[30] + -z[35] / T(2) + -z[62];
z[35] = z[2] / T(2);
z[38] = z[3] / T(4) + -z[28] + z[35];
z[38] = z[3] * z[38];
z[41] = z[5] + z[53];
z[38] = -z[38] + -z[39] + z[41] / T(2);
z[39] = T(3) * z[45];
z[38] = z[38] * z[39];
z[39] = -(z[6] * z[47]);
z[38] = z[30] + z[38] + z[39];
z[38] = z[0] * z[38];
z[27] = z[6] * z[27];
z[29] = z[11] * z[29];
z[29] = -z[12] + z[29] + -z[36];
z[28] = z[10] + T(-21) * z[28];
z[28] = z[28] * z[49];
z[28] = z[28] + T(3) * z[29];
z[28] = z[10] * z[28];
z[29] = z[32] + z[36];
z[29] = z[11] * z[29];
z[29] = T(-4) * z[13] + (T(9) * z[15]) / T(4) + z[29] + -z[56];
z[32] = T(2) * z[50] + z[60];
z[32] = int_to_imaginary<T>(1) * z[32];
z[27] = T(-9) * z[27] + z[28] + T(3) * z[29] + z[32] + T(-15) * z[54];
z[27] = z[19] * z[27];
z[28] = (T(-7) * z[3]) / T(4) + z[33] + z[35];
z[28] = z[28] * z[44];
z[29] = (T(13) * z[36]) / T(2) + -z[55];
z[28] = z[28] + z[29] / T(2) + -z[42];
z[28] = -(z[28] * z[45]);
z[28] = z[28] + -z[30];
z[28] = z[18] * z[28];
return z[25] + z[26] + z[27] + z[28] + z[31] + z[34] + z[37] + z[38] + z[40];
}



template IntegrandConstructorType<double> f_4_216_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_216_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_216_construct (const Kin<qd_real>&);
#endif

}