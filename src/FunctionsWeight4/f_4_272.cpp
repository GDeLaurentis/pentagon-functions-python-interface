#include "f_4_272.h"

namespace PentagonFunctions {

template <typename T> T f_4_272_abbreviated (const std::array<T,42>&);

template <typename T> class SpDLog_f_4_272_W_22 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_272_W_22 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[1] * (T(-6) + T(6) * kin.v[0] + T(3) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[0] * (T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(kin.v[3]) * (kin.v[1] * (T(-6) + T(6) * kin.v[0] + T(3) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[0] * (T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[1] * (T(6) + T(-6) * kin.v[0] + T(-3) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[0] * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[1] * (T(6) + T(-6) * kin.v[0] + T(-3) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[0] * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-3) + prod_pow(abb[1], 2) * (abb[5] * T(-3) + abb[6] * T(-3) + abb[3] * T(3) + abb[4] * T(3)) + prod_pow(abb[2], 2) * (abb[4] * T(-3) + abb[5] * T(3) + abb[6] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_272_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_272_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (T(-6) * kin.v[0] + T(16) * kin.v[2] + T(12) * kin.v[3] + T(-8) * kin.v[4]) + T(-2) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(-6) * kin.v[3] + T(8) * kin.v[4]) + kin.v[2] * (T(-10) * kin.v[2] + T(-16) * kin.v[3] + T(12) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[0] * (T(4) + T(-2) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[3]) + kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(2) * kin.v[4]));
c[1] = kin.v[3] * (T(6) * kin.v[3] + T(-16) * kin.v[4]) + kin.v[2] * (T(2) * kin.v[2] + T(8) * kin.v[3] + T(-12) * kin.v[4]) + T(10) * prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(5) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(3) * kin.v[3] + T(-8) * kin.v[4]) + kin.v[2] * (kin.v[2] + T(4) * kin.v[3] + T(-6) * kin.v[4])) + kin.v[0] * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[0] + T(-8) * kin.v[2] + T(-12) * kin.v[3] + T(16) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] + T(-6) * kin.v[3] + T(8) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (prod_pow(kin.v[4], 2) / T(2) + kin.v[2] * ((T(5) * kin.v[2]) / T(2) + T(4) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-2) * kin.v[4]) + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(-4) * kin.v[2] + T(-3) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (kin.v[0] / T(2) + -kin.v[3] + T(-1) + kin.v[1]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (prod_pow(kin.v[4], 2) / T(2) + kin.v[2] * ((T(5) * kin.v[2]) / T(2) + T(4) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-2) * kin.v[4]) + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(-4) * kin.v[2] + T(-3) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (kin.v[0] / T(2) + -kin.v[3] + T(-1) + kin.v[1]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (prod_pow(kin.v[4], 2) / T(2) + kin.v[2] * ((T(5) * kin.v[2]) / T(2) + T(4) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-2) * kin.v[4]) + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(-4) * kin.v[2] + T(-3) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (kin.v[0] / T(2) + -kin.v[3] + T(-1) + kin.v[1]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (prod_pow(kin.v[4], 2) / T(2) + kin.v[2] * ((T(5) * kin.v[2]) / T(2) + T(4) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-2) * kin.v[4]) + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(-4) * kin.v[2] + T(-3) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (kin.v[0] / T(2) + -kin.v[3] + T(-1) + kin.v[1]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(kin.v[3]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[0] * ((T(-9) * kin.v[0]) / T(2) + T(12) * kin.v[2] + T(9) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[3] * ((T(-9) * kin.v[3]) / T(2) + T(6) * kin.v[4]) + kin.v[2] * ((T(-15) * kin.v[2]) / T(2) + T(-12) * kin.v[3] + T(9) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]))) + rlog(-kin.v[4]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[0] * ((T(-9) * kin.v[0]) / T(2) + T(12) * kin.v[2] + T(9) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[3] * ((T(-9) * kin.v[3]) / T(2) + T(6) * kin.v[4]) + kin.v[2] * ((T(-15) * kin.v[2]) / T(2) + T(-12) * kin.v[3] + T(9) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (prod_pow(kin.v[4], 2) + kin.v[2] * (T(5) * kin.v[2] + T(8) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[3] * (T(3) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[0] * (T(3) * kin.v[0] + T(-8) * kin.v[2] + T(-6) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[0] * (T(-2) + kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(4) + T(8)) * kin.v[0] + T(-8) * kin.v[2] + T(-8) * kin.v[3] + T(8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]);
c[3] = rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[0] + T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(-2) * kin.v[4]) + rlog(kin.v[3]) * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[0] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4])) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[0] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[22] * (abb[14] * (abb[4] * bc<T>[0] * int_to_imaginary<T>(1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(1) + abb[24] * bc<T>[0] * int_to_imaginary<T>(2) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[25] * bc<T>[0] * int_to_imaginary<T>(4) + abb[2] * (-abb[4] + -abb[5] + -abb[6] + -abb[11] + abb[25] * T(-4) + abb[24] * T(-2) + abb[12] * T(3)) + abb[15] * (-abb[4] + -abb[5] + -abb[6] + -abb[11] + abb[25] * T(-4) + abb[24] * T(-2) + abb[12] * T(3)) + abb[8] * (abb[4] + abb[5] + abb[6] + abb[11] + abb[12] * T(-3) + abb[24] * T(2) + abb[25] * T(4)) + abb[14] * (abb[4] + abb[5] + abb[6] + abb[11] + abb[12] * T(-3) + abb[24] * T(2) + abb[25] * T(4))) + abb[23] * (abb[25] * T(-8) + abb[24] * T(-4) + abb[4] * T(-2) + abb[5] * T(-2) + abb[6] * T(-2) + abb[11] * T(-2) + abb[12] * T(6)) + abb[3] * (abb[14] * (abb[8] * T(-3) + abb[14] * T(-3) + bc<T>[0] * int_to_imaginary<T>(-3) + abb[2] * T(3) + abb[15] * T(3)) + abb[23] * T(6)) + abb[1] * (abb[4] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[24] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[12] * bc<T>[0] * int_to_imaginary<T>(3) + abb[25] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[3] * (abb[2] * T(-3) + abb[14] * T(-3) + abb[15] * T(-3) + bc<T>[0] * int_to_imaginary<T>(3) + abb[8] * T(3)) + abb[8] * (-abb[4] + -abb[5] + -abb[6] + -abb[11] + abb[25] * T(-4) + abb[24] * T(-2) + abb[12] * T(3)) + abb[2] * (abb[4] + abb[5] + abb[6] + abb[11] + abb[12] * T(-3) + abb[24] * T(2) + abb[25] * T(4)) + abb[14] * (abb[4] + abb[5] + abb[6] + abb[11] + abb[12] * T(-3) + abb[24] * T(2) + abb[25] * T(4)) + abb[15] * (abb[4] + abb[5] + abb[6] + abb[11] + abb[12] * T(-3) + abb[24] * T(2) + abb[25] * T(4)) + abb[1] * (abb[25] * T(-8) + abb[24] * T(-4) + abb[4] * T(-2) + abb[5] * T(-2) + abb[6] * T(-2) + abb[11] * T(-2) + abb[3] * T(6) + abb[12] * T(6))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_272_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl3 = DLog_W_3<T>(kin),dl24 = DLog_W_24<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl23 = DLog_W_23<T>(kin),dl28 = DLog_W_28<T>(kin),dl19 = DLog_W_19<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl26 = DLog_W_26<T>(kin),dl27 = DLog_W_27<T>(kin),dl20 = DLog_W_20<T>(kin),dl16 = DLog_W_16<T>(kin),dl18 = DLog_W_18<T>(kin),dl5 = DLog_W_5<T>(kin),dl17 = DLog_W_17<T>(kin),dl4 = DLog_W_4<T>(kin),spdl22 = SpDLog_f_4_272_W_22<T>(kin),spdl23 = SpDLog_f_4_272_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,42> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[19] / kin_path.W[19]), dl3(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_14(kin_path), f_2_1_15(kin_path), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[4] / kin_path.W[4]), dl24(t), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl2(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl1(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl23(t), f_2_1_8(kin_path), rlog(kin.W[18] / kin_path.W[18]), -rlog(t), dl28(t) / kin_path.SqrtDelta, f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl19(t), dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl20(t), dl16(t), dl18(t), dl5(t), dl17(t), dl4(t)}
;

        auto result = f_4_272_abbreviated(abbr);
        result = result + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_272_abbreviated(const std::array<T,42>& abb)
{
using TR = typename T::value_type;
T z[133];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[7];
z[3] = abb[31];
z[4] = abb[36];
z[5] = abb[37];
z[6] = abb[38];
z[7] = abb[39];
z[8] = abb[40];
z[9] = abb[41];
z[10] = abb[4];
z[11] = abb[5];
z[12] = abb[6];
z[13] = abb[11];
z[14] = abb[12];
z[15] = abb[24];
z[16] = abb[25];
z[17] = abb[26];
z[18] = abb[27];
z[19] = abb[28];
z[20] = abb[29];
z[21] = abb[30];
z[22] = abb[33];
z[23] = abb[35];
z[24] = abb[2];
z[25] = abb[8];
z[26] = abb[14];
z[27] = abb[15];
z[28] = bc<TR>[0];
z[29] = abb[32];
z[30] = abb[34];
z[31] = abb[16];
z[32] = abb[9];
z[33] = abb[10];
z[34] = abb[19];
z[35] = abb[17];
z[36] = abb[18];
z[37] = abb[20];
z[38] = abb[21];
z[39] = abb[23];
z[40] = abb[13];
z[41] = bc<TR>[3];
z[42] = bc<TR>[1];
z[43] = bc<TR>[2];
z[44] = bc<TR>[4];
z[45] = bc<TR>[9];
z[46] = z[31] / T(4);
z[47] = z[4] / T(4);
z[48] = z[46] + -z[47];
z[49] = z[6] / T(2);
z[50] = T(3) * z[40];
z[51] = z[3] / T(2);
z[52] = (T(-7) * z[5]) / T(3) + -z[8] / T(6) + -z[48] + -z[49] + z[50] + z[51];
z[52] = z[13] * z[52];
z[53] = z[12] + z[13];
z[54] = z[11] + z[53];
z[55] = z[1] + z[14];
z[56] = z[10] + -z[54] + z[55];
z[56] = z[2] * z[56];
z[57] = z[10] + z[13];
z[58] = z[11] + z[12];
z[59] = T(5) * z[1] + z[14] + z[57] + T(-3) * z[58];
z[59] = z[9] * z[59];
z[60] = z[56] + -z[59];
z[61] = T(5) * z[5];
z[62] = (T(3) * z[4]) / T(2);
z[63] = z[3] + -z[6];
z[64] = z[61] + -z[62] + -z[63];
z[64] = z[46] + -z[50] + z[64] / T(2);
z[64] = z[14] * z[64];
z[65] = (T(3) * z[4]) / T(4);
z[66] = z[5] + z[6];
z[67] = z[66] / T(2);
z[68] = -z[8] + -z[46] + -z[65] + -z[67];
z[68] = z[1] * z[68];
z[69] = -z[3] + T(7) * z[6];
z[70] = T(2) * z[5];
z[71] = z[8] + z[70];
z[72] = z[47] + z[69] + z[71];
z[73] = z[46] + z[50];
z[72] = z[72] / T(3) + -z[73];
z[72] = z[11] * z[72];
z[74] = z[8] / T(2);
z[69] = (T(5) * z[4]) / T(4) + (T(-5) * z[5]) / T(2) + -z[69] + -z[74];
z[69] = z[69] / T(3) + z[73];
z[69] = z[10] * z[69];
z[46] = (T(2) * z[5]) / T(3) + (T(5) * z[8]) / T(6) + z[46] + z[47] + z[49];
z[46] = z[12] * z[46];
z[73] = -z[4] + z[9];
z[75] = -z[8] + z[66];
z[76] = -z[7] + z[75];
z[77] = -z[73] + z[76];
z[77] = z[42] * z[77];
z[78] = -z[5] + z[8];
z[79] = -z[73] + z[78] / T(3);
z[80] = z[15] / T(2) + z[16];
z[79] = z[79] * z[80];
z[81] = z[1] + z[11];
z[82] = z[12] + -z[14] + z[57] + -z[81];
z[82] = z[34] * z[82];
z[83] = -z[18] + -z[19] + z[20] + z[21];
z[84] = -(z[23] * z[83]);
z[85] = z[22] * z[83];
z[86] = z[84] + z[85];
z[87] = z[17] * z[83];
z[88] = z[86] + z[87];
z[89] = z[29] * z[83];
z[76] = z[43] * z[76];
z[46] = z[46] + z[52] + -z[60] / T(4) + z[64] + z[68] + z[69] + z[72] + T(3) * z[76] + -z[77] + z[79] + -z[82] + z[88] / T(2) + (T(3) * z[89]) / T(4);
z[46] = z[28] * z[46];
z[52] = -z[4] + z[31];
z[60] = z[8] + z[52];
z[64] = z[60] + z[63];
z[68] = z[10] + -z[11];
z[64] = z[64] * z[68];
z[69] = z[4] + z[8];
z[72] = -z[31] + z[63] + z[69];
z[76] = z[13] + -z[14];
z[72] = z[72] * z[76];
z[60] = z[60] + -z[63];
z[79] = z[12] * z[60];
z[60] = z[1] * z[60];
z[88] = z[84] + z[89];
z[83] = z[30] * z[83];
z[90] = z[83] + -z[87];
z[64] = z[60] + -z[64] + -z[72] + -z[79] + z[82] + -z[88] + z[90];
z[64] = (T(3) * z[64]) / T(2);
z[72] = -(z[36] * z[64]);
z[79] = z[85] + z[88];
z[91] = z[5] + -z[6];
z[92] = z[4] + z[91];
z[92] = z[14] * z[92];
z[93] = -z[4] + z[66];
z[93] = z[1] * z[93];
z[92] = -z[79] + z[92] + z[93];
z[93] = z[4] / T(2);
z[94] = T(2) * z[8];
z[95] = -z[3] + z[91] / T(2) + -z[93] + -z[94];
z[95] = z[11] * z[95];
z[96] = z[67] + z[93];
z[97] = z[3] + z[96];
z[98] = z[8] + z[97];
z[98] = z[10] * z[98];
z[99] = T(3) * z[6];
z[100] = -z[5] + z[99];
z[100] = -z[8] + -z[62] + z[100] / T(2);
z[101] = z[13] * z[100];
z[99] = z[5] + z[99];
z[62] = z[8] + -z[62] + z[99] / T(2);
z[99] = z[12] * z[62];
z[102] = (T(3) * z[82]) / T(2);
z[103] = T(2) * z[16];
z[104] = z[78] * z[103];
z[105] = z[15] * z[78];
z[104] = z[104] + z[105];
z[92] = (T(3) * z[92]) / T(2) + -z[95] + -z[98] + -z[99] + z[101] + z[102] + z[104];
z[95] = z[25] * z[92];
z[98] = -z[3] + z[31];
z[99] = z[78] + z[98];
z[99] = z[1] * z[99];
z[101] = -z[78] + z[98];
z[101] = z[14] * z[101];
z[106] = z[85] + z[90];
z[99] = z[99] + -z[101] + z[106];
z[101] = T(2) * z[6];
z[107] = (T(3) * z[31]) / T(2);
z[108] = T(2) * z[4];
z[109] = -z[51] + z[101] + -z[107] + z[108];
z[110] = z[5] / T(2) + -z[74];
z[111] = z[109] + z[110];
z[111] = z[10] * z[111];
z[109] = z[109] + -z[110];
z[109] = z[11] * z[109];
z[98] = T(-3) * z[98];
z[110] = z[78] + z[98];
z[112] = z[13] / T(2);
z[110] = z[110] * z[112];
z[98] = -z[78] + z[98];
z[113] = z[12] / T(2);
z[98] = z[98] * z[113];
z[98] = z[98] + (T(3) * z[99]) / T(2) + -z[104] + -z[109] + -z[110] + z[111];
z[99] = z[24] * z[98];
z[109] = T(3) * z[8];
z[110] = z[3] + z[109];
z[111] = z[66] + z[110];
z[111] = z[11] * z[111];
z[114] = T(3) * z[66];
z[115] = z[3] + z[8] + z[114];
z[115] = z[57] * z[115];
z[116] = z[84] + z[106];
z[117] = T(5) * z[66];
z[118] = z[3] + -z[8] + z[117];
z[118] = z[14] * z[118];
z[119] = -z[3] + z[75];
z[119] = z[12] * z[119];
z[120] = z[3] + z[75];
z[121] = z[1] * z[120];
z[111] = -z[82] + -z[111] + z[115] + z[116] + -z[118] + -z[119] + -z[121];
z[103] = z[15] + z[103];
z[115] = z[75] * z[103];
z[81] = -z[12] + T(-5) * z[14] + T(3) * z[57] + -z[81];
z[118] = z[81] / T(2) + z[103];
z[119] = z[7] * z[118];
z[111] = z[111] / T(2) + z[115] + -z[119];
z[111] = T(3) * z[111];
z[115] = z[38] * z[111];
z[121] = z[3] + -z[4];
z[122] = z[94] + z[121];
z[101] = z[5] + -z[50] + z[101] + z[122];
z[101] = z[11] * z[101];
z[123] = T(2) * z[66];
z[124] = -z[8] + z[50] + -z[121] + -z[123];
z[124] = z[10] * z[124];
z[71] = z[50] + -z[71];
z[71] = z[13] * z[71];
z[125] = -z[5] + z[40];
z[125] = z[14] * z[125];
z[126] = -(z[12] * z[78]);
z[71] = z[71] + z[101] + z[104] + z[124] + T(-3) * z[125] + z[126];
z[71] = z[27] * z[71];
z[101] = T(2) * z[3];
z[124] = T(4) * z[8];
z[96] = -z[96] + z[101] + z[124];
z[96] = z[11] * z[96];
z[125] = z[4] + z[66];
z[125] = z[55] * z[125];
z[79] = -z[79] + z[125];
z[97] = z[94] + z[97];
z[53] = z[10] + z[53];
z[53] = z[53] * z[97];
z[97] = -z[66] + z[122];
z[97] = z[97] * z[103];
z[53] = -z[53] + (T(3) * z[79]) / T(2) + z[96] + z[97] + z[102];
z[79] = z[26] * z[53];
z[96] = -z[3] + z[69];
z[97] = z[1] * z[96];
z[97] = z[90] + z[97];
z[74] = z[74] + z[93];
z[102] = -z[51] + z[74];
z[122] = z[102] + -z[123];
z[122] = z[14] * z[122];
z[125] = z[89] / T(2);
z[97] = z[97] / T(2) + z[122] + -z[125];
z[96] = z[96] + -z[123];
z[96] = -(z[96] * z[103]);
z[122] = z[102] + z[123];
z[122] = -(z[58] * z[122]);
z[102] = T(4) * z[66] + -z[102];
z[102] = z[57] * z[102];
z[96] = z[96] + T(3) * z[97] + z[102] + z[122];
z[97] = -(z[0] * z[96]);
z[102] = -z[73] + z[75];
z[102] = z[44] * z[102];
z[122] = z[42] / T(2) + -z[43];
z[77] = z[77] * z[122];
z[77] = z[77] + z[102];
z[102] = -z[22] + z[23] + -z[29] + z[30];
z[122] = (T(3) * z[6]) / T(4);
z[123] = (T(3) * z[7]) / T(4) + z[9] / T(4) + -z[47] + z[102] / T(9) + -z[122];
z[123] = prod_pow(z[28], 2) * z[123];
z[126] = z[0] * z[118];
z[126] = -z[44] + z[126];
z[127] = T(3) * z[7];
z[126] = z[126] * z[127];
z[71] = T(2) * z[71] + z[72] + T(3) * z[77] + z[79] + z[95] + z[97] + z[99] + z[115] + z[123] + z[126];
z[71] = int_to_imaginary<T>(1) * z[71];
z[72] = -(z[41] * z[102]);
z[46] = z[46] + z[71] + T(3) * z[72];
z[46] = z[28] * z[46];
z[71] = z[84] + -z[87];
z[72] = -z[5] + z[49];
z[77] = (T(3) * z[3]) / T(2);
z[79] = -z[72] + z[77];
z[79] = z[14] * z[79];
z[71] = z[71] / T(2) + z[79] + z[85];
z[79] = (T(3) * z[3]) / T(4);
z[95] = -z[5] + -z[49];
z[95] = z[69] + z[79] + z[95] / T(2);
z[95] = z[1] * z[95];
z[71] = z[71] / T(2) + z[95];
z[61] = z[49] + z[61];
z[95] = z[94] + z[108];
z[97] = z[3] / T(4);
z[61] = z[61] / T(2) + -z[95] + -z[97];
z[61] = z[58] * z[61];
z[102] = z[69] + z[72] / T(2) + -z[97];
z[102] = z[13] * z[102];
z[115] = T(3) * z[9];
z[123] = z[69] + z[101] + -z[115];
z[72] = z[72] + -z[123];
z[72] = z[15] * z[72];
z[126] = (T(3) * z[56]) / T(4);
z[128] = (T(3) * z[59]) / T(2);
z[129] = T(-7) * z[5] + z[49];
z[129] = (T(-7) * z[3]) / T(4) + z[69] + z[129] / T(2);
z[129] = z[10] * z[129];
z[130] = T(-4) * z[3] + z[6] + T(6) * z[9] + -z[70] + -z[95];
z[130] = z[16] * z[130];
z[61] = z[61] + T(3) * z[71] + z[72] + z[102] + -z[126] + -z[128] + z[129] + z[130];
z[61] = z[0] * z[61];
z[71] = z[27] * z[96];
z[61] = z[61] + z[71];
z[61] = z[0] * z[61];
z[71] = T(3) * z[4];
z[72] = z[5] + z[71] + z[110];
z[72] = -(z[58] * z[72]);
z[102] = z[78] + -z[121];
z[102] = z[13] * z[102];
z[110] = z[3] + z[4] + z[78];
z[110] = z[14] * z[110];
z[129] = T(5) * z[8];
z[130] = T(5) * z[4] + z[129];
z[131] = z[3] + -z[5] + z[130];
z[131] = z[1] * z[131];
z[132] = z[3] + T(3) * z[5] + z[69];
z[132] = z[10] * z[132];
z[59] = z[56] + z[59] + -z[72] + z[89] + -z[102] + -z[106] + -z[110] + -z[131] + -z[132];
z[72] = z[73] + -z[78];
z[72] = z[72] * z[103];
z[59] = -z[59] / T(2) + z[72];
z[59] = z[32] * z[59];
z[72] = -(z[27] * z[92]);
z[92] = -z[31] + z[121];
z[102] = z[66] + z[92];
z[106] = z[1] * z[102];
z[85] = z[85] + -z[87];
z[87] = -z[85] + -z[88] + z[106];
z[92] = -z[91] + z[92];
z[106] = -(z[14] * z[92]);
z[106] = z[87] + z[106];
z[110] = (T(3) * z[31]) / T(4);
z[91] = z[8] + z[47] + -z[91] / T(4) + -z[97] + -z[110];
z[91] = z[11] * z[91];
z[51] = -z[51] + z[67];
z[67] = -z[8] + -z[51] + -z[93] + z[107];
z[67] = z[10] * z[67];
z[67] = z[67] + z[105];
z[77] = z[77] + -z[107];
z[62] = -z[62] + -z[77];
z[62] = z[62] * z[113];
z[77] = z[77] + z[100];
z[77] = z[77] * z[112];
z[93] = z[16] * z[78];
z[62] = z[62] + z[67] / T(2) + z[77] + z[91] + z[93] + (T(3) * z[106]) / T(4) + -z[126];
z[62] = z[25] * z[62];
z[67] = z[6] + z[69];
z[55] = -(z[55] * z[67]);
z[55] = z[55] + z[56] + -z[83] + z[88];
z[67] = z[63] + z[70];
z[77] = z[67] + -z[69];
z[77] = -(z[77] * z[103]);
z[49] = z[49] + z[74];
z[74] = z[3] + z[49] + z[70];
z[54] = z[54] * z[74];
z[49] = T(-4) * z[5] + z[49] + -z[101];
z[49] = z[10] * z[49];
z[49] = z[49] + z[54] + (T(3) * z[55]) / T(2) + z[77];
z[49] = z[0] * z[49];
z[49] = z[49] + z[62] + z[72] + z[99];
z[49] = z[25] * z[49];
z[54] = -z[66] + z[129];
z[54] = z[54] / T(4);
z[55] = -z[50] + z[97];
z[62] = -z[4] + z[54] + z[55];
z[62] = -(z[57] * z[62]);
z[72] = z[4] + -z[75] / T(4) + -z[97];
z[72] = z[1] * z[72];
z[74] = -z[40] + -z[120] / T(4);
z[74] = z[14] * z[74];
z[72] = z[72] + z[74] + z[116] / T(4);
z[74] = T(7) * z[8];
z[75] = z[66] + z[74];
z[75] = (T(5) * z[3]) / T(4) + -z[50] + z[75] / T(4) + -z[108];
z[75] = z[11] * z[75];
z[54] = -z[54] + -z[97] + -z[108];
z[54] = z[12] * z[54];
z[77] = z[8] + z[66];
z[88] = z[77] + z[101] + -z[108];
z[88] = z[16] * z[88];
z[82] = (T(3) * z[82]) / T(4);
z[77] = z[77] / T(2) + z[121];
z[77] = z[15] * z[77];
z[80] = z[80] + z[81] / T(4);
z[81] = -(z[80] * z[127]);
z[54] = z[54] + z[62] + T(3) * z[72] + z[75] + z[77] + z[81] + -z[82] + z[88];
z[54] = z[26] * z[54];
z[62] = z[51] + -z[95];
z[62] = z[1] * z[62];
z[72] = -z[3] + z[66];
z[75] = z[14] * z[72];
z[75] = z[75] + -z[84] + -z[85];
z[62] = z[62] + z[75] / T(2);
z[75] = z[51] + z[95];
z[75] = -(z[57] * z[75]);
z[72] = z[72] + -z[95] + z[115];
z[72] = z[72] * z[103];
z[51] = T(4) * z[4] + -z[51] + z[124];
z[51] = z[51] * z[58];
z[51] = z[51] + T(3) * z[62] + -z[72] + z[75] + z[128];
z[62] = z[0] + -z[24];
z[62] = z[51] * z[62];
z[53] = z[25] * z[53];
z[72] = T(3) * z[119];
z[75] = z[72] + -z[96];
z[75] = z[27] * z[75];
z[53] = z[53] + z[54] + z[62] + z[75];
z[53] = z[26] * z[53];
z[54] = z[60] + -z[84] + -z[89] + z[90];
z[60] = z[8] / T(4);
z[62] = -z[5] + z[60];
z[48] = -z[6] / T(4) + z[40] + -z[48] + z[62] + z[97];
z[48] = z[14] * z[48];
z[48] = z[48] + z[54] / T(4);
z[54] = z[65] + z[79] + -z[110] + -z[122];
z[62] = z[54] + z[62];
z[62] = z[12] * z[62];
z[60] = z[60] + z[70];
z[50] = -z[50] + -z[54] + z[60];
z[50] = z[13] * z[50];
z[47] = (T(11) * z[6]) / T(4) + -z[47] + z[55] + -z[110];
z[54] = -z[5] + (T(-5) * z[8]) / T(4) + -z[47];
z[54] = z[11] * z[54];
z[47] = z[47] + z[60];
z[47] = z[10] * z[47];
z[47] = z[47] + T(3) * z[48] + z[50] + z[54] + z[62] + z[82] + -z[104];
z[47] = prod_pow(z[27], 2) * z[47];
z[48] = -(z[27] * z[98]);
z[50] = z[0] * z[51];
z[51] = z[5] + z[63];
z[54] = -z[51] + -z[95];
z[54] = z[11] * z[54];
z[55] = z[8] + z[67] + z[108];
z[55] = z[10] * z[55];
z[60] = z[1] * z[109];
z[62] = -z[5] + -z[94];
z[62] = z[12] * z[62];
z[63] = z[13] * z[78];
z[54] = z[54] + z[55] + z[60] + z[62] + z[63] + -z[104];
z[54] = z[24] * z[54];
z[48] = z[48] + z[50] + z[54];
z[48] = z[24] * z[48];
z[50] = z[35] * z[64];
z[54] = -z[66] + z[71] + z[109];
z[54] = z[3] + z[54] / T(2);
z[54] = z[1] * z[54];
z[55] = -z[83] + z[86];
z[60] = -z[69] + z[114];
z[60] = z[3] + z[60] / T(2);
z[60] = z[14] * z[60];
z[54] = z[54] + z[55] / T(2) + z[60] + z[125];
z[55] = z[66] + z[123];
z[55] = -(z[55] * z[103]);
z[60] = T(7) * z[4] + z[74] + -z[117];
z[60] = z[3] + z[60] / T(2);
z[58] = -(z[58] * z[60]);
z[60] = T(7) * z[66] + -z[130];
z[60] = z[3] + z[60] / T(2);
z[57] = -(z[57] * z[60]);
z[54] = T(3) * z[54] + z[55] + z[57] + z[58] + z[72] + -z[128];
z[54] = z[39] * z[54];
z[55] = z[37] * z[111];
z[51] = z[51] + z[52];
z[51] = z[51] * z[68];
z[52] = z[76] * z[92];
z[57] = -(z[12] * z[102]);
z[51] = z[51] + z[52] + -z[56] + z[57] + z[87];
z[51] = z[33] * z[51];
z[52] = -(z[27] * z[118]);
z[56] = z[0] * z[80];
z[52] = z[52] + z[56];
z[52] = z[0] * z[52];
z[52] = z[45] + T(3) * z[52];
z[52] = z[7] * z[52];
z[56] = -z[6] + (T(5) * z[73]) / T(2) + (T(85) * z[78]) / T(6);
z[56] = z[45] * z[56];
return z[46] + z[47] + z[48] + z[49] + z[50] + (T(3) * z[51]) / T(2) + z[52] + z[53] + z[54] + z[55] + z[56] + T(3) * z[59] + z[61];
}



template IntegrandConstructorType<double> f_4_272_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_272_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_272_construct (const Kin<qd_real>&);
#endif

}