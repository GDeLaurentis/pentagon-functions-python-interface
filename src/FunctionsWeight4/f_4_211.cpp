#include "f_4_211.h"

namespace PentagonFunctions {

template <typename T> T f_4_211_abbreviated (const std::array<T,42>&);

template <typename T> class SpDLog_f_4_211_W_10 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_211_W_10 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(-6) + T(3) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[2] * (T(-6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(-6) + T(3) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[1] * (T(6) + T(-3) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[1] * (T(6) + T(-3) * kin.v[1] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[2] * (T(6) + T(-3) * kin.v[2] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-3) + prod_pow(abb[1], 2) * (abb[5] * T(-3) + abb[6] * T(-3) + abb[3] * T(3) + abb[4] * T(3)) + prod_pow(abb[2], 2) * (abb[4] * T(-3) + abb[5] * T(3) + abb[6] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_211_W_22 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_211_W_22 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[1] * (T(-6) + T(6) * kin.v[0] + T(3) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[0] * (T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[1] * (T(-6) + T(6) * kin.v[0] + T(3) * kin.v[1] + T(-6) * kin.v[3]) + kin.v[3] * (T(6) + T(3) * kin.v[3]) + kin.v[0] * (T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[1] * (T(6) + T(-6) * kin.v[0] + T(-3) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[0] * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4])) + rlog(kin.v[3]) * (kin.v[3] * (T(-6) + T(-3) * kin.v[3]) + kin.v[1] * (T(6) + T(-6) * kin.v[0] + T(-3) * kin.v[1] + T(6) * kin.v[3]) + kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[0] * (T(-6) * kin.v[2] + T(6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[7] * (prod_pow(abb[9], 2) * abb[10] * T(3) + prod_pow(abb[9], 2) * (abb[5] * T(-3) + abb[11] * T(-3) + abb[4] * T(3)) + prod_pow(abb[8], 2) * (abb[4] * T(-3) + abb[10] * T(-3) + abb[5] * T(3) + abb[11] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_211_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_211_W_23 (const Kin<T>& kin) {
        c[0] = T(-2) * prod_pow(kin.v[3], 2) + kin.v[2] * (T(10) * kin.v[2] + T(8) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + T(2) * prod_pow(kin.v[4], 2) + kin.v[0] * (T(2) * kin.v[0] + T(-4) * kin.v[1] + T(-12) * kin.v[2] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[0] * (T(4) + T(-2) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[3]) + kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(2) * kin.v[4]));
c[1] = T(-2) * prod_pow(kin.v[3], 2) + kin.v[2] * (T(10) * kin.v[2] + T(8) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + T(2) * prod_pow(kin.v[4], 2) + kin.v[0] * ((bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[0] + T(-4) * kin.v[1] + T(-12) * kin.v[2] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[1] + T(6) * kin.v[2] + T(-2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (-prod_pow(kin.v[4], 2) + prod_pow(kin.v[3], 2) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[2] * (T(-5) * kin.v[2] + T(-4) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (-prod_pow(kin.v[4], 2) / T(2) + prod_pow(kin.v[3], 2) / T(2) + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[4] + kin.v[1] + T(3) * kin.v[2]) + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * ((T(-5) * kin.v[2]) / T(2) + T(-2) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (kin.v[0] / T(2) + -kin.v[3] + T(-1) + kin.v[1]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (-prod_pow(kin.v[4], 2) / T(2) + prod_pow(kin.v[3], 2) / T(2) + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[4] + kin.v[1] + T(3) * kin.v[2]) + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * ((T(-5) * kin.v[2]) / T(2) + T(-2) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (kin.v[0] / T(2) + -kin.v[3] + T(-1) + kin.v[1]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(-kin.v[4]) * (-prod_pow(kin.v[4], 2) / T(2) + prod_pow(kin.v[3], 2) / T(2) + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[4] + kin.v[1] + T(3) * kin.v[2]) + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * ((T(-5) * kin.v[2]) / T(2) + T(-2) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (kin.v[0] / T(2) + -kin.v[3] + T(-1) + kin.v[1]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (-prod_pow(kin.v[4], 2) / T(2) + prod_pow(kin.v[3], 2) / T(2) + kin.v[0] * (-kin.v[0] / T(2) + -kin.v[4] + kin.v[1] + T(3) * kin.v[2]) + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * ((T(-5) * kin.v[2]) / T(2) + T(-2) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (kin.v[0] / T(2) + -kin.v[3] + T(-1) + kin.v[1]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[1] * (-kin.v[2] + -kin.v[3] + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(1) + kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-3) * prod_pow(kin.v[3], 2)) / T(2) + (T(3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[2] * ((T(15) * kin.v[2]) / T(2) + T(6) * kin.v[3] + T(-9) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(-3) * kin.v[1] + T(-9) * kin.v[2] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]))) + rlog(kin.v[3]) * ((T(-3) * prod_pow(kin.v[3], 2)) / T(2) + (T(3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[2] * ((T(15) * kin.v[2]) / T(2) + T(6) * kin.v[3] + T(-9) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[0] * ((T(3) * kin.v[0]) / T(2) + T(-3) * kin.v[1] + T(-9) * kin.v[2] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[0] * ((T(-3) * kin.v[0]) / T(2) + T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]))) + rlog(kin.v[2]) * (-prod_pow(kin.v[4], 2) + prod_pow(kin.v[3], 2) + kin.v[0] * (-kin.v[0] + T(2) * kin.v[1] + T(6) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[2] * (T(-5) * kin.v[2] + T(-4) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[0] * (T(-2) + kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4])));
c[2] = (T(-8) + bc<T>[0] * int_to_imaginary<T>(4)) * kin.v[0] + T(8) * kin.v[2] + T(8) * kin.v[3] + T(-8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]);
c[3] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(2) * kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(2) * kin.v[4]) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(2) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(-1) + T(2)) * kin.v[0] + T(-2) * kin.v[2] + T(-2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + kin.v[2] + kin.v[3]) + T(2) * kin.v[4]) + rlog(kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(-2) + T(4)) * kin.v[0] + T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[0] + T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4])) + rlog(kin.v[3]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[0] + T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[12] * (abb[13] * (abb[1] * abb[2] * T(2) + abb[2] * (abb[2] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2) + abb[9] * T(2)) + abb[15] * T(4)) + abb[1] * abb[2] * (abb[3] + abb[4] + abb[5] + abb[11] + abb[6] * T(-3) + abb[10] * T(-3) + abb[16] * T(4)) + abb[2] * (abb[3] * bc<T>[0] * int_to_imaginary<T>(1) + abb[4] * bc<T>[0] * int_to_imaginary<T>(1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[16] * bc<T>[0] * int_to_imaginary<T>(4) + abb[2] * (-abb[3] + -abb[4] + -abb[5] + -abb[11] + abb[16] * T(-4) + abb[6] * T(3) + abb[10] * T(3)) + abb[9] * (abb[3] + abb[4] + abb[5] + abb[11] + abb[6] * T(-3) + abb[10] * T(-3) + abb[16] * T(4))) + abb[14] * (abb[2] * abb[13] * T(-2) + abb[2] * (-abb[3] + -abb[4] + -abb[5] + -abb[11] + abb[16] * T(-4) + abb[6] * T(3) + abb[10] * T(3)) + abb[8] * (abb[3] + abb[4] + abb[5] + abb[11] + abb[6] * T(-3) + abb[10] * T(-3) + abb[13] * T(2) + abb[16] * T(4))) + abb[15] * (abb[6] * T(-6) + abb[10] * T(-6) + abb[3] * T(2) + abb[4] * T(2) + abb[5] * T(2) + abb[11] * T(2) + abb[16] * T(8)) + abb[8] * (abb[3] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[13] * (abb[1] * T(-2) + abb[2] * T(-2) + abb[9] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) + abb[6] * bc<T>[0] * int_to_imaginary<T>(3) + abb[10] * bc<T>[0] * int_to_imaginary<T>(3) + abb[16] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[1] * (-abb[3] + -abb[4] + -abb[5] + -abb[11] + abb[16] * T(-4) + abb[6] * T(3) + abb[10] * T(3)) + abb[2] * (-abb[3] + -abb[4] + -abb[5] + -abb[11] + abb[16] * T(-4) + abb[6] * T(3) + abb[10] * T(3)) + abb[9] * (-abb[3] + -abb[4] + -abb[5] + -abb[11] + abb[16] * T(-4) + abb[6] * T(3) + abb[10] * T(3)) + abb[8] * (abb[6] * T(-6) + abb[10] * T(-6) + abb[3] * T(2) + abb[4] * T(2) + abb[5] * T(2) + abb[11] * T(2) + abb[13] * T(4) + abb[16] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_211_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl22 = DLog_W_22<T>(kin),dl23 = DLog_W_23<T>(kin),dl19 = DLog_W_19<T>(kin),dl28 = DLog_W_28<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl3 = DLog_W_3<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl27 = DLog_W_27<T>(kin),dl26 = DLog_W_26<T>(kin),dl17 = DLog_W_17<T>(kin),dl4 = DLog_W_4<T>(kin),dl5 = DLog_W_5<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),dl16 = DLog_W_16<T>(kin),spdl10 = SpDLog_f_4_211_W_10<T>(kin),spdl22 = SpDLog_f_4_211_W_22<T>(kin),spdl23 = SpDLog_f_4_211_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,42> abbr = 
            {dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[17] / kin_path.W[17]), dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[19] / kin_path.W[19]), dl23(t), rlog(kin.W[2] / kin_path.W[2]), rlog(v_path[2]), f_2_1_8(kin_path), -rlog(t), dl19(t), f_2_1_13(kin_path), f_2_1_14(kin_path), rlog(v_path[0] + v_path[4]), dl28(t) / kin_path.SqrtDelta, f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), dl2(t), f_2_1_5(kin_path), f_2_1_7(kin_path), rlog(-v_path[4] + v_path[2]), dl1(t), dl3(t), dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl17(t), dl4(t), dl5(t), dl18(t), dl20(t), dl16(t)}
;

        auto result = f_4_211_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_211_abbreviated(const std::array<T,42>& abb)
{
using TR = typename T::value_type;
T z[108];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[26];
z[3] = abb[31];
z[4] = abb[36];
z[5] = abb[38];
z[6] = abb[39];
z[7] = abb[41];
z[8] = abb[4];
z[9] = abb[40];
z[10] = abb[5];
z[11] = abb[6];
z[12] = abb[10];
z[13] = abb[11];
z[14] = abb[13];
z[15] = abb[16];
z[16] = abb[22];
z[17] = abb[33];
z[18] = abb[34];
z[19] = abb[35];
z[20] = abb[23];
z[21] = abb[24];
z[22] = abb[25];
z[23] = abb[2];
z[24] = abb[32];
z[25] = abb[8];
z[26] = abb[9];
z[27] = abb[30];
z[28] = abb[14];
z[29] = bc<TR>[0];
z[30] = abb[37];
z[31] = abb[17];
z[32] = abb[15];
z[33] = abb[18];
z[34] = abb[19];
z[35] = abb[20];
z[36] = abb[27];
z[37] = abb[28];
z[38] = abb[29];
z[39] = abb[21];
z[40] = bc<TR>[3];
z[41] = bc<TR>[1];
z[42] = bc<TR>[2];
z[43] = bc<TR>[4];
z[44] = bc<TR>[9];
z[45] = z[16] + z[20] + -z[21] + -z[22];
z[46] = z[19] * z[45];
z[47] = z[17] * z[45];
z[48] = z[46] + z[47];
z[49] = T(3) * z[48];
z[50] = z[1] + z[13];
z[51] = -z[11] + -z[12] + z[50];
z[52] = z[8] + -z[10];
z[53] = z[51] + -z[52];
z[54] = z[31] * z[53];
z[55] = z[39] * z[45];
z[54] = -z[54] + z[55];
z[55] = z[51] + z[52];
z[56] = z[2] * z[55];
z[57] = z[1] + -z[11] + z[12] + -z[13];
z[58] = -z[52] / T(3) + T(5) * z[57];
z[58] = z[3] * z[58];
z[59] = z[18] * z[45];
z[45] = z[24] * z[45];
z[58] = z[45] + z[49] + z[54] + z[56] + -z[58] + -z[59];
z[60] = (T(5) * z[14]) / T(3);
z[61] = z[8] / T(6);
z[62] = z[12] / T(2);
z[63] = T(3) * z[11];
z[64] = -z[1] / T(3) + (T(-13) * z[13]) / T(3) + -z[63];
z[64] = (T(11) * z[10]) / T(6) + z[60] + -z[61] + z[62] + z[64] / T(2);
z[65] = (T(5) * z[15]) / T(3);
z[64] = z[64] / T(2) + z[65];
z[64] = z[4] * z[64];
z[66] = z[10] / T(6);
z[67] = (T(3) * z[12]) / T(2);
z[68] = (T(13) * z[1]) / T(3) + -z[11] + z[13] / T(3);
z[60] = (T(-11) * z[8]) / T(6) + -z[60] + z[66] + z[67] + z[68] / T(2);
z[60] = z[60] / T(2) + -z[65];
z[60] = z[7] * z[60];
z[65] = -z[1] + -z[11] + T(3) * z[13];
z[65] = z[65] / T(2);
z[68] = z[14] + z[65];
z[69] = (T(5) * z[12]) / T(2);
z[66] = (T(-5) * z[8]) / T(6) + -z[66] + -z[68] + z[69];
z[66] = -z[15] + z[66] / T(2);
z[66] = z[9] * z[66];
z[70] = -z[52] + z[57];
z[71] = z[27] * z[70];
z[72] = z[8] / T(2);
z[73] = (T(3) * z[10]) / T(2) + -z[72];
z[69] = -z[69] + z[73];
z[68] = z[68] + z[69];
z[68] = z[15] + z[68] / T(2);
z[68] = z[30] * z[68];
z[74] = T(3) * z[1] + T(-5) * z[11] + -z[13];
z[74] = z[74] / T(2);
z[75] = z[14] + -z[62] + z[74];
z[61] = (T(5) * z[10]) / T(6) + z[61] + z[75];
z[61] = z[15] + z[61] / T(2);
z[61] = z[5] * z[61];
z[76] = z[10] / T(2);
z[77] = (T(-3) * z[8]) / T(2) + z[62] + z[76];
z[74] = -z[74] + z[77];
z[78] = -z[14] + z[74];
z[78] = -z[15] + z[78] / T(2);
z[79] = z[6] * z[78];
z[58] = -z[58] / T(4) + z[60] + z[61] + z[64] + z[66] + z[68] + (T(-5) * z[71]) / T(4) + z[79];
z[58] = z[29] * z[58];
z[60] = -z[17] + z[18] + z[19] + -z[24];
z[61] = -(z[40] * z[60]);
z[58] = z[58] + T(3) * z[61];
z[58] = z[29] * z[58];
z[61] = -z[50] + z[63];
z[64] = z[61] / T(2) + z[67] + -z[72] + -z[76];
z[66] = -z[14] + z[64];
z[66] = -z[15] + z[66] / T(2);
z[68] = z[5] * z[66];
z[76] = z[14] + T(2) * z[15];
z[65] = z[65] + z[69] + z[76];
z[69] = z[30] * z[65];
z[79] = T(3) * z[69];
z[80] = (T(5) * z[10]) / T(2);
z[67] = (T(7) * z[8]) / T(2) + z[67] + z[76] + -z[80];
z[81] = T(5) * z[13];
z[82] = z[1] + -z[63] + -z[81];
z[82] = z[67] + z[82] / T(2);
z[82] = z[7] * z[82];
z[83] = T(3) * z[6];
z[78] = z[78] * z[83];
z[84] = T(3) * z[12];
z[85] = -z[8] + T(2) * z[10] + z[76];
z[86] = z[1] + T(-2) * z[13] + z[84] + -z[85];
z[87] = z[4] * z[86];
z[88] = z[9] * z[86];
z[89] = z[54] + z[59];
z[89] = -z[47] + z[89] / T(2);
z[90] = T(9) * z[11];
z[91] = z[10] + z[50] + -z[90];
z[92] = T(2) * z[14] + T(4) * z[15];
z[91] = (T(7) * z[8]) / T(4) + (T(-9) * z[12]) / T(4) + z[91] / T(4) + z[92];
z[91] = z[3] * z[91];
z[68] = z[68] + z[78] + -z[79] + z[82] + -z[87] + -z[88] + (T(3) * z[89]) / T(2) + z[91];
z[68] = z[25] * z[68];
z[64] = -z[64] + z[76];
z[82] = z[9] * z[64];
z[89] = (T(3) * z[45]) / T(2);
z[82] = z[82] + z[89];
z[91] = z[4] * z[64];
z[93] = z[82] + z[91];
z[94] = z[5] * z[64];
z[95] = -z[46] + z[54];
z[96] = T(2) * z[8] + -z[10] + z[76];
z[97] = -z[50] + z[96];
z[98] = z[3] * z[97];
z[97] = z[7] * z[97];
z[95] = -z[93] + -z[94] + (T(-3) * z[95]) / T(2) + T(2) * z[97] + z[98];
z[98] = int_to_imaginary<T>(1) * z[29];
z[99] = -z[28] + z[98];
z[95] = -(z[95] * z[99]);
z[100] = z[7] * z[64];
z[94] = (T(3) * z[47]) / T(2) + z[94] + z[100];
z[64] = z[3] * z[64];
z[101] = T(2) * z[4];
z[86] = z[86] * z[101];
z[86] = -z[64] + z[79] + z[86] + T(2) * z[88] + z[94];
z[102] = z[23] + z[26];
z[102] = z[86] * z[102];
z[96] = T(2) * z[1] + -z[13] + -z[63] + z[96];
z[103] = z[5] + z[7];
z[96] = z[96] * z[103];
z[104] = z[46] + z[59];
z[74] = z[74] + -z[76];
z[83] = -(z[74] * z[83]);
z[64] = -z[64] + z[83] + z[93] + T(-2) * z[96] + (T(-3) * z[104]) / T(2);
z[64] = z[0] * z[64];
z[64] = z[64] + z[68] + z[95] + z[102];
z[64] = z[25] * z[64];
z[68] = z[52] + z[57];
z[83] = z[3] * z[68];
z[83] = -z[71] + z[83];
z[93] = z[5] * z[70];
z[95] = z[7] * z[53];
z[102] = z[9] * z[68];
z[93] = -z[45] + -z[47] + -z[54] + z[83] + z[93] + -z[95] + -z[102];
z[93] = (T(3) * z[93]) / T(2);
z[95] = z[35] * z[93];
z[70] = z[9] * z[70];
z[102] = z[4] * z[55];
z[68] = z[5] * z[68];
z[46] = z[45] + -z[46] + -z[56] + -z[68] + z[70] + z[83] + z[102];
z[46] = (T(3) * z[46]) / T(2);
z[68] = z[38] * z[46];
z[70] = z[52] + T(3) * z[57];
z[70] = z[3] * z[70];
z[49] = -z[49] + z[70];
z[70] = (T(3) * z[71]) / T(2);
z[71] = T(2) * z[52];
z[83] = z[5] * z[71];
z[71] = z[9] * z[71];
z[49] = z[49] / T(2) + -z[70] + -z[71] + -z[83] + z[91] + -z[100];
z[83] = -(z[26] * z[49]);
z[85] = -z[50] + z[85];
z[91] = z[3] + z[101];
z[91] = z[85] * z[91];
z[82] = (T(3) * z[56]) / T(2) + -z[82] + z[91] + -z[94];
z[82] = z[23] * z[82];
z[94] = z[5] + -z[9] + z[30];
z[100] = -z[6] + z[94];
z[101] = -z[4] + z[7];
z[60] = z[60] / T(9) + (T(-3) * z[100]) / T(4) + z[101] / T(2);
z[102] = prod_pow(z[29], 2);
z[60] = z[60] * z[102];
z[105] = z[3] * z[52];
z[106] = z[5] * z[52];
z[97] = z[97] + z[105] + -z[106];
z[85] = z[4] * z[85];
z[105] = z[9] * z[52];
z[85] = z[85] + -z[97] + z[105];
z[85] = z[28] * z[85];
z[100] = -z[100] + z[101];
z[107] = z[43] * z[100];
z[60] = z[60] + z[68] + z[82] + z[83] + z[85] + z[95] + T(-3) * z[107];
z[60] = z[60] * z[98];
z[46] = -(z[36] * z[46]);
z[68] = -(z[23] * z[86]);
z[83] = z[28] * z[49];
z[71] = -z[71] + -z[87] + -z[97];
z[71] = z[26] * z[71];
z[68] = z[68] + z[71] + z[83];
z[68] = z[26] * z[68];
z[71] = -z[26] + -z[99];
z[49] = z[49] * z[71];
z[66] = -(z[66] * z[101]);
z[66] = z[66] + z[105];
z[59] = z[48] + -z[56] + z[59];
z[51] = T(3) * z[51] + -z[52];
z[51] = z[3] * z[51];
z[51] = z[51] + T(3) * z[59];
z[71] = z[72] + T(3) * z[75] + z[80];
z[71] = T(3) * z[15] + z[71] / T(2);
z[71] = z[5] * z[71];
z[51] = z[51] / T(4) + z[66] + z[71] + z[78];
z[51] = z[0] * z[51];
z[49] = z[49] + z[51] + z[82];
z[49] = z[0] * z[49];
z[51] = -(z[33] * z[93]);
z[53] = z[3] * z[53];
z[45] = z[45] + -z[48] + z[53] + z[54];
z[53] = z[4] + z[9];
z[65] = z[53] * z[65];
z[50] = z[11] + z[50];
z[50] = z[50] / T(2) + -z[76];
z[71] = z[50] + z[77];
z[71] = z[7] * z[71];
z[45] = z[45] / T(2) + z[65] + -z[69] + z[71];
z[45] = z[34] * z[45];
z[55] = z[3] * z[55];
z[55] = z[55] + z[59];
z[50] = z[50] + z[62] + -z[73];
z[50] = z[4] * z[50];
z[50] = z[50] + z[55] / T(2);
z[50] = z[37] * z[50];
z[55] = z[37] * z[74];
z[59] = -(z[55] * z[103]);
z[45] = z[45] + z[50] + z[59];
z[50] = T(-7) * z[1] + z[81] + z[90];
z[50] = z[50] / T(2) + -z[67];
z[50] = -(z[50] * z[103]);
z[59] = z[8] + z[10] + -z[61] + -z[84] + z[92];
z[59] = z[3] * z[59];
z[61] = T(5) * z[1] + T(-7) * z[13] + -z[63];
z[61] = (T(5) * z[8]) / T(2) + (T(-7) * z[10]) / T(2) + (T(9) * z[12]) / T(2) + z[61] / T(2) + -z[76];
z[53] = -(z[53] * z[61]);
z[47] = -z[47] + z[104];
z[47] = (T(3) * z[47]) / T(2) + z[50] + z[53] + z[59] + -z[79] + -z[89];
z[47] = z[32] * z[47];
z[48] = z[48] + z[54] + z[56];
z[50] = -z[52] + (T(-3) * z[57]) / T(2);
z[50] = z[3] * z[50];
z[48] = (T(3) * z[48]) / T(4) + z[50] + z[66] + z[70] + z[106];
z[48] = z[28] * z[48];
z[48] = z[48] + -z[82];
z[48] = z[28] * z[48];
z[50] = -z[88] + -z[91] + z[96];
z[50] = prod_pow(z[23], 2) * z[50];
z[52] = z[32] * z[74];
z[52] = z[52] + z[55];
z[52] = (T(5) * z[44]) / T(2) + T(3) * z[52];
z[52] = z[6] * z[52];
z[53] = z[42] * z[98];
z[54] = z[41] * z[98];
z[53] = T(3) * z[53] + (T(-3) * z[54]) / T(2) + z[102];
z[53] = z[41] * z[53] * z[100];
z[54] = (T(-5) * z[94]) / T(2) + (T(11) * z[101]) / T(3);
z[54] = z[44] * z[54];
return T(3) * z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + z[58] + z[60] + z[64] + z[68];
}



template IntegrandConstructorType<double> f_4_211_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_211_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_211_construct (const Kin<qd_real>&);
#endif

}