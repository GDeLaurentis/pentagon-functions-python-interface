#include "f_4_256.h"

namespace PentagonFunctions {

template <typename T> T f_4_256_abbreviated (const std::array<T,39>&);

template <typename T> class SpDLog_f_4_256_W_23 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_256_W_23 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (T(-21) / T(2) + (T(15) * kin.v[1]) / T(2) + (T(9) * kin.v[2]) / T(4) + (T(-9) * kin.v[3]) / T(4) + (T(-9) * kin.v[4]) / T(4) + (T(21) / T(8) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3])) + (T(-21) / T(2) + (T(-39) * kin.v[4]) / T(8)) * kin.v[4] + (T(21) / T(2) + (T(-3) * kin.v[3]) / T(8) + (T(21) * kin.v[4]) / T(4)) * kin.v[3] + ((T(-15) * kin.v[2]) / T(2) + (T(-15) * kin.v[3]) / T(2) + (T(15) * kin.v[4]) / T(2)) * kin.v[1] + (T(21) / T(2) + (T(-39) * kin.v[2]) / T(8) + (T(-21) * kin.v[3]) / T(4) + (T(39) * kin.v[4]) / T(4)) * kin.v[2] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]));
c[1] = (T(21) / T(2) + (T(15) * kin.v[3]) / T(2) + (T(-21) * kin.v[4]) / T(2)) * kin.v[3] + kin.v[2] * (T(21) / T(2) + (T(21) * kin.v[3]) / T(2) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[4] * (T(-21) / T(2) + T(3) * kin.v[4]) + ((T(-15) * kin.v[2]) / T(2) + (T(-15) * kin.v[3]) / T(2) + (T(15) * kin.v[4]) / T(2)) * kin.v[1] + kin.v[0] * (T(-21) / T(2) + (T(15) * kin.v[1]) / T(2) + (T(-27) * kin.v[2]) / T(2) + (T(27) * kin.v[4]) / T(2) + (T(21) / T(2) + bc<T>[0] * int_to_imaginary<T>(6)) * kin.v[0] + T(-18) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-9) + T(6) * kin.v[1] + T(-6) * kin.v[2] + T(-9) * kin.v[3] + T(6) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[2] * (T(9) + T(3) * kin.v[3]) + T(-9) * kin.v[4] + kin.v[3] * (T(9) + T(3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[0] * (T(6) + (T(-3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[0] + T(-3) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3])) + ((T(3) * kin.v[4]) / T(2) + T(6)) * kin.v[4] + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-6) + T(-3) * kin.v[4]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-6) + T(3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[0] * (T(-6) + (T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[0] + T(3) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3])) + ((T(-3) * kin.v[4]) / T(2) + T(-6)) * kin.v[4] + kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(6) + T(3) * kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(6) + T(-3) * kin.v[3] + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (((T(-9) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2)) * kin.v[1] + (T(15) / T(2) + (T(-21) * kin.v[2]) / T(8) + (T(-15) * kin.v[3]) / T(4) + (T(21) * kin.v[4]) / T(4)) * kin.v[2] + (T(15) / T(2) + (T(-9) * kin.v[3]) / T(8) + (T(15) * kin.v[4]) / T(4)) * kin.v[3] + kin.v[0] * (T(-15) / T(2) + (T(9) * kin.v[1]) / T(2) + (T(3) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(4) + (T(15) / T(8) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3])) + (T(-15) / T(2) + (T(-21) * kin.v[4]) / T(8)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]))) + rlog(kin.v[3]) * (((T(-15) * kin.v[2]) / T(4) + (T(-9) * kin.v[3]) / T(2) + (T(15) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + ((T(-3) * kin.v[3]) / T(4) + (T(9) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + kin.v[0] * ((T(3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-9) + (T(9) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[0] + T(6) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3])) + ((T(-15) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (((T(-15) * kin.v[2]) / T(4) + (T(-9) * kin.v[3]) / T(2) + (T(15) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + ((T(-3) * kin.v[3]) / T(4) + (T(9) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + kin.v[0] * ((T(3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-9) + (T(9) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[0] + T(6) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3])) + ((T(-15) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4])));
c[2] = (T(-3) / T(2) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + (T(3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + (T(-3) * kin.v[4]) / T(2);
c[3] = rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(9) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + (T(-9) / T(2) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4])) + rlog(kin.v[3]) * ((T(-3) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-3) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4])) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[0] + T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[25] * (abb[14] * (abb[14] * ((abb[26] * T(-3)) / T(2) + (abb[24] * T(9)) / T(2) + abb[7] * T(-3)) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[24] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[7] * bc<T>[0] * int_to_imaginary<T>(3) + abb[26] * bc<T>[0] * int_to_imaginary<T>(3) + abb[27] * bc<T>[0] * int_to_imaginary<T>(3)) + abb[8] * (abb[16] * T(-3) + abb[14] * (abb[14] * T(-3) + bc<T>[0] * int_to_imaginary<T>(3))) + abb[13] * (abb[7] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[26] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[27] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[9] * bc<T>[0] * int_to_imaginary<T>(3) + abb[24] * bc<T>[0] * int_to_imaginary<T>(3) + abb[13] * ((abb[24] * T(-3)) / T(2) + (abb[26] * T(-3)) / T(2) + abb[27] * T(-3) + abb[9] * T(3)) + abb[8] * (bc<T>[0] * int_to_imaginary<T>(-3) + abb[14] * T(3)) + abb[14] * (abb[9] * T(-3) + abb[24] * T(-3) + abb[7] * T(3) + abb[26] * T(3) + abb[27] * T(3))) + abb[3] * (abb[8] * abb[14] * T(-3) + abb[14] * (abb[7] * T(-3) + abb[26] * T(-3) + abb[27] * T(-3) + abb[9] * T(3) + abb[24] * T(3)) + abb[13] * (abb[9] * T(-3) + abb[24] * T(-3) + abb[7] * T(3) + abb[8] * T(3) + abb[26] * T(3) + abb[27] * T(3))) + abb[16] * ((abb[26] * T(-9)) / T(2) + (abb[24] * T(3)) / T(2) + abb[27] * T(-6) + abb[7] * T(-3) + abb[9] * T(6)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_256_construct (const Kin<T>& kin) {
    return [&kin, 
            dl3 = DLog_W_3<T>(kin),dl11 = DLog_W_11<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl20 = DLog_W_20<T>(kin),dl23 = DLog_W_23<T>(kin),dl19 = DLog_W_19<T>(kin),dl2 = DLog_W_2<T>(kin),dl26 = DLog_W_26<T>(kin),dl1 = DLog_W_1<T>(kin),dl5 = DLog_W_5<T>(kin),dl17 = DLog_W_17<T>(kin),dl4 = DLog_W_4<T>(kin),dl24 = DLog_W_24<T>(kin),dl18 = DLog_W_18<T>(kin),dl16 = DLog_W_16<T>(kin),spdl23 = SpDLog_f_4_256_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,39> abbr = 
            {dl3(t), rlog(kin.W[0] / kin_path.W[0]), rlog(v_path[0]), rlog(v_path[3]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_2(kin_path), f_2_1_9(kin_path), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[4] / kin_path.W[4]), dl11(t), dl29(t) / kin_path.SqrtDelta, f_1_3_1(kin) - f_1_3_1(kin_path), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_6(kin_path), f_2_1_8(kin_path), rlog(v_path[0] + v_path[1]), dl30(t) / kin_path.SqrtDelta, f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl27(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl20(t), -rlog(t), dl23(t), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[17] / kin_path.W[17]), dl19(t), rlog(kin.W[23] / kin_path.W[23]), dl2(t), dl26(t) / kin_path.SqrtDelta, dl1(t), dl5(t), dl17(t), dl4(t), dl24(t), dl18(t), dl16(t)}
;

        auto result = f_4_256_abbreviated(abbr);
        result = result + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_256_abbreviated(const std::array<T,39>& abb)
{
using TR = typename T::value_type;
T z[157];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[28];
z[13] = abb[30];
z[14] = abb[32];
z[15] = abb[34];
z[16] = abb[35];
z[17] = abb[37];
z[18] = abb[38];
z[19] = abb[36];
z[20] = abb[13];
z[21] = abb[14];
z[22] = abb[23];
z[23] = abb[33];
z[24] = abb[15];
z[25] = abb[16];
z[26] = abb[17];
z[27] = abb[19];
z[28] = abb[20];
z[29] = abb[12];
z[30] = abb[18];
z[31] = abb[22];
z[32] = abb[31];
z[33] = abb[26];
z[34] = abb[27];
z[35] = abb[29];
z[36] = abb[21];
z[37] = abb[11];
z[38] = abb[24];
z[39] = bc<TR>[1];
z[40] = bc<TR>[3];
z[41] = bc<TR>[5];
z[42] = bc<TR>[2];
z[43] = bc<TR>[4];
z[44] = bc<TR>[9];
z[45] = bc<TR>[7];
z[46] = bc<TR>[8];
z[47] = z[20] / T(2);
z[48] = z[21] / T(2);
z[49] = z[47] + -z[48];
z[50] = T(2) * z[3];
z[51] = int_to_imaginary<T>(1) * z[7];
z[52] = T(2) * z[51];
z[53] = z[2] + z[52];
z[54] = z[50] + -z[53];
z[55] = z[4] / T(2);
z[56] = z[49] + -z[54] + -z[55];
z[56] = z[4] * z[56];
z[57] = z[2] / T(2);
z[58] = z[52] + z[57];
z[58] = z[2] * z[58];
z[56] = z[56] + -z[58];
z[59] = T(4) * z[51];
z[60] = z[57] + z[59];
z[61] = T(4) * z[20];
z[62] = T(-4) * z[3] + T(-2) * z[21] + z[60] + z[61];
z[62] = z[21] * z[62];
z[63] = T(5) * z[51];
z[64] = -z[2] + T(5) * z[3] + (T(-5) * z[20]) / T(2) + -z[63];
z[64] = z[47] * z[64];
z[64] = (T(-9) * z[25]) / T(2) + z[64];
z[65] = prod_pow(z[7], 2);
z[66] = (T(3) * z[65]) / T(2);
z[67] = z[26] * z[51];
z[68] = z[66] + -z[67];
z[69] = (T(3) * z[5]) / T(2);
z[70] = (T(3) * z[24]) / T(2);
z[71] = z[69] + -z[70];
z[72] = T(2) * z[53];
z[73] = (T(5) * z[3]) / T(4);
z[74] = -z[72] + z[73];
z[74] = z[3] * z[74];
z[75] = (T(3) * z[6]) / T(2);
z[74] = -z[56] + -z[62] + -z[64] + (T(-3) * z[68]) / T(2) + z[71] + z[74] + z[75];
z[74] = z[16] * z[74];
z[76] = -z[3] + z[20];
z[77] = -z[21] + z[76];
z[78] = -z[2] + z[51];
z[79] = T(2) * z[78];
z[80] = -z[55] + -z[77] + z[79];
z[80] = z[4] * z[80];
z[81] = -z[57] + z[59];
z[81] = z[2] * z[81];
z[82] = -z[2] + z[3];
z[83] = -z[52] + z[82];
z[84] = z[3] * z[83];
z[85] = T(2) * z[65];
z[86] = z[84] + z[85];
z[81] = z[80] + z[81] + z[86];
z[87] = -z[48] + z[76];
z[88] = z[78] + z[87];
z[89] = z[48] * z[88];
z[90] = z[28] * z[51];
z[90] = z[27] + z[90];
z[91] = (T(3) * z[90]) / T(2);
z[92] = (T(3) * z[25]) / T(2);
z[89] = z[89] + z[91] + -z[92];
z[93] = z[52] + -z[57];
z[94] = z[3] / T(2);
z[95] = z[93] + -z[94];
z[96] = z[20] / T(4);
z[97] = z[95] + z[96];
z[97] = z[20] * z[97];
z[97] = -z[89] + z[97];
z[69] = z[69] + z[75];
z[98] = z[69] + z[81] + z[97];
z[98] = z[17] * z[98];
z[99] = z[2] + z[48];
z[100] = T(3) * z[21];
z[99] = z[99] * z[100];
z[101] = -z[2] + z[52];
z[102] = -z[55] + z[101];
z[103] = T(3) * z[4];
z[104] = z[102] * z[103];
z[105] = T(3) * z[51];
z[106] = z[2] * z[105];
z[107] = z[28] * z[105];
z[107] = T(3) * z[27] + z[107];
z[106] = z[106] + -z[107];
z[108] = T(4) * z[65];
z[99] = z[99] + z[104] + z[106] + z[108];
z[109] = z[19] * z[99];
z[110] = z[47] + z[51];
z[110] = z[20] * z[110];
z[111] = prod_pow(z[21], 2);
z[112] = z[111] / T(2);
z[113] = -z[20] + z[21];
z[114] = z[4] * z[113];
z[110] = -z[25] + z[90] + -z[110] + z[112] + -z[114];
z[110] = z[23] * z[110];
z[115] = (T(3) * z[110]) / T(2);
z[98] = z[98] + -z[109] + z[115];
z[109] = z[51] / T(2);
z[116] = -z[3] + z[47];
z[117] = T(2) * z[2];
z[118] = z[109] + -z[116] + -z[117];
z[118] = z[20] * z[118];
z[119] = z[3] / T(4) + -z[93];
z[119] = z[3] * z[119];
z[120] = z[71] + z[119];
z[118] = z[118] + z[120];
z[121] = -z[47] + z[94];
z[122] = z[21] / T(4);
z[109] = -z[109] + z[121] + z[122];
z[123] = T(4) * z[2];
z[124] = z[109] + -z[123];
z[124] = z[21] * z[124];
z[121] = z[48] + z[121];
z[125] = (T(5) * z[2]) / T(2);
z[126] = -z[4] + z[59] + z[121] + -z[125];
z[126] = z[4] * z[126];
z[126] = -z[75] + (T(-9) * z[90]) / T(2) + z[126];
z[127] = z[58] + (T(-3) * z[67]) / T(2) + z[85] + -z[118] + -z[124] + z[126];
z[127] = z[14] * z[127];
z[128] = -z[55] + z[113];
z[95] = z[95] + z[128];
z[95] = z[4] * z[95];
z[89] = -z[89] + z[95];
z[95] = -z[51] + z[57];
z[129] = z[2] * z[95];
z[68] = T(3) * z[68] + z[129];
z[60] = -z[60] + z[73];
z[60] = z[3] * z[60];
z[68] = -z[60] + z[68] / T(2);
z[73] = -z[54] + z[96];
z[73] = z[20] * z[73];
z[71] = z[68] + -z[71] + z[73] + z[89];
z[71] = z[18] * z[71];
z[73] = prod_pow(z[2], 2);
z[130] = T(3) * z[5];
z[131] = T(3) * z[73] + z[130];
z[132] = z[82] * z[103];
z[133] = T(3) * z[6];
z[134] = z[65] / T(2);
z[135] = z[133] + -z[134];
z[136] = T(3) * z[2];
z[137] = z[3] * z[136];
z[137] = -z[131] + -z[132] + -z[135] + z[137];
z[137] = z[0] * z[137];
z[95] = z[95] * z[136];
z[95] = z[95] + z[107];
z[138] = -z[2] + z[48];
z[138] = z[100] * z[138];
z[139] = z[95] + z[135] + z[138];
z[140] = z[12] * z[139];
z[141] = z[137] + z[140];
z[141] = z[141] / T(2);
z[60] = z[60] + z[126];
z[142] = z[51] + z[125];
z[142] = z[2] * z[142];
z[143] = z[26] * z[105];
z[144] = -z[134] + z[142] + z[143];
z[64] = z[64] + z[70];
z[78] = z[76] + z[78];
z[145] = T(-4) * z[78] + -z[122];
z[145] = z[21] * z[145];
z[144] = z[60] + -z[64] + z[144] / T(2) + z[145];
z[144] = z[15] * z[144];
z[145] = z[20] * z[82];
z[146] = -z[24] + z[67];
z[147] = prod_pow(z[3], 2);
z[145] = -z[5] + -z[129] + -z[145] + -z[146] + z[147] / T(2);
z[148] = (T(3) * z[13]) / T(2);
z[145] = z[145] * z[148];
z[148] = prod_pow(z[20], 2);
z[149] = T(3) * z[24] + -z[143];
z[150] = T(3) * z[25];
z[148] = (T(3) * z[148]) / T(2) + -z[149] + z[150];
z[151] = -z[134] + -z[148];
z[76] = z[51] + z[76];
z[152] = z[76] * z[100];
z[147] = (T(3) * z[147]) / T(2);
z[152] = z[147] + z[151] + z[152];
z[153] = z[22] * z[152];
z[154] = -z[145] + z[153] / T(2);
z[74] = z[71] + z[74] + z[98] + z[127] + z[141] + z[144] + z[154];
z[74] = z[8] * z[74];
z[123] = -z[20] + z[50] + (T(5) * z[51]) / T(2) + -z[123];
z[123] = z[20] * z[123];
z[127] = T(3) * z[65] + z[67];
z[144] = T(8) * z[51];
z[125] = -z[125] + z[144];
z[125] = z[2] * z[125];
z[155] = (T(9) * z[5]) / T(2);
z[60] = z[60] + -z[70] + z[123] + -z[124] + z[125] + (T(3) * z[127]) / T(2) + z[155];
z[60] = z[14] * z[60];
z[123] = -z[3] + z[51];
z[124] = z[47] + z[123];
z[125] = -z[2] + z[124];
z[127] = z[47] * z[125];
z[70] = -z[70] + z[92] + z[127];
z[127] = (T(7) * z[65]) / T(2);
z[142] = z[127] + z[142] + -z[143];
z[156] = z[76] + z[117];
z[122] = -z[122] + T(2) * z[156];
z[122] = z[21] * z[122];
z[119] = -z[70] + -z[119] + z[122] + z[126] + z[142] / T(2);
z[119] = z[15] * z[119];
z[122] = T(2) * z[20];
z[126] = -z[21] + -z[50] + z[93] + z[122];
z[126] = z[21] * z[126];
z[142] = (T(-11) * z[65]) / T(2) + -z[143];
z[72] = (T(11) * z[3]) / T(4) + -z[72];
z[72] = z[3] * z[72];
z[56] = -z[56] + z[69] + -z[70] + z[72] + z[126] + z[142] / T(2);
z[56] = z[16] * z[56];
z[70] = -z[3] + z[52];
z[70] = z[3] * z[70];
z[72] = z[2] * z[101];
z[70] = z[70] + -z[72];
z[126] = T(3) * z[11];
z[70] = z[70] * z[126];
z[70] = z[70] + z[141] + -z[154];
z[126] = z[66] + z[67];
z[126] = T(3) * z[126] + T(-5) * z[129];
z[96] = z[3] + z[79] + z[96];
z[96] = z[20] * z[96];
z[89] = z[89] + z[96] + z[120] + z[126] / T(2);
z[89] = z[18] * z[89];
z[56] = z[56] + z[60] + z[70] + z[89] + z[98] + z[119];
z[56] = z[1] * z[56];
z[49] = -z[4] + -z[49] + T(-2) * z[54];
z[49] = z[4] * z[49];
z[60] = z[67] + z[127];
z[59] = -z[2] + -z[59];
z[59] = z[2] * z[59];
z[67] = (T(-19) * z[3]) / T(4) + T(4) * z[53];
z[67] = z[3] * z[67];
z[49] = z[49] + z[59] + (T(3) * z[60]) / T(2) + -z[62] + -z[64] + z[67] + -z[75] + -z[155];
z[49] = z[16] * z[49];
z[59] = z[55] + -z[93] + z[121];
z[59] = z[4] * z[59];
z[59] = z[59] + z[75] + z[91];
z[60] = -z[109] + -z[117];
z[60] = z[21] * z[60];
z[62] = T(-5) * z[65] + -z[143];
z[67] = -(z[101] * z[117]);
z[60] = z[59] + z[60] + z[62] / T(2) + z[67] + -z[118];
z[60] = z[14] * z[60];
z[62] = -z[54] + z[122];
z[67] = (T(11) * z[21]) / T(4) + T(-2) * z[62];
z[67] = z[21] * z[67];
z[59] = z[59] + -z[64] + z[67] + -z[68];
z[59] = z[15] * z[59];
z[64] = -z[54] + z[128];
z[64] = z[4] * z[64];
z[50] = z[50] * z[83];
z[67] = (T(5) * z[65]) / T(2);
z[50] = -z[50] + -z[58] + z[64] + z[67];
z[58] = z[50] + -z[69] + z[97];
z[58] = z[17] * z[58];
z[49] = z[49] + z[58] + z[59] + z[60] + -z[70] + z[71] + z[115];
z[49] = z[9] * z[49];
z[58] = T(3) * z[110];
z[59] = z[2] + z[124];
z[60] = T(3) * z[20];
z[59] = z[59] * z[60];
z[59] = z[59] + z[135] + z[150];
z[64] = z[2] + z[77];
z[64] = z[64] * z[103];
z[68] = -z[2] + z[94];
z[69] = T(3) * z[3];
z[68] = z[68] * z[69];
z[69] = z[68] + -z[107];
z[64] = z[64] + -z[69] + -z[149];
z[57] = z[51] + z[57];
z[57] = z[57] * z[136];
z[70] = z[57] + z[59] + -z[64] + (T(-3) * z[111]) / T(2);
z[71] = -(z[15] * z[70]);
z[71] = -z[58] + z[71] + -z[137] + z[140] + z[153];
z[75] = z[10] / T(2);
z[71] = z[71] * z[75];
z[83] = z[48] + z[78];
z[83] = z[83] * z[100];
z[89] = z[2] + z[3];
z[91] = -z[4] + z[89];
z[91] = z[91] * z[103];
z[68] = -z[67] + z[68] + z[83] + z[91] + z[95] + z[130] + -z[148];
z[68] = z[68] * z[75];
z[83] = (T(7) * z[39]) / T(6) + -z[52];
z[83] = z[39] * z[83];
z[91] = (T(7) * z[65]) / T(4);
z[95] = z[42] * z[105];
z[83] = z[83] + -z[91] + z[95];
z[83] = z[39] * z[83];
z[96] = T(2) * z[42];
z[97] = (T(3) * z[51]) / T(2);
z[98] = z[96] + z[97];
z[98] = z[42] * z[98];
z[101] = z[65] / T(4);
z[109] = z[98] + -z[101];
z[109] = z[42] * z[109];
z[110] = (T(3) * z[46]) / T(2);
z[111] = T(6) * z[45] + z[110];
z[109] = z[109] + -z[111];
z[115] = int_to_imaginary<T>(1) * prod_pow(z[7], 3);
z[68] = (T(259) * z[44]) / T(24) + z[68] + -z[83] + z[109] + (T(-11) * z[115]) / T(12);
z[68] = z[18] * z[68];
z[87] = z[51] + z[87];
z[118] = z[87] + z[117];
z[118] = z[21] * z[118];
z[79] = -z[79] + z[116];
z[79] = z[20] * z[79];
z[79] = -z[79] + z[81] + -z[107] + z[118] + z[130];
z[79] = z[14] * z[79];
z[62] = -z[21] + z[62];
z[62] = z[21] * z[62];
z[54] = -z[20] + z[54];
z[54] = z[20] * z[54];
z[54] = z[54] + -z[150];
z[50] = z[50] + -z[54] + -z[62] + -z[130];
z[50] = z[16] * z[50];
z[50] = z[50] + z[79];
z[62] = z[2] + z[51];
z[62] = z[2] * z[62];
z[54] = z[54] + -z[62] + -z[80] + -z[84] + z[107];
z[62] = T(2) * z[78];
z[48] = -z[48] + -z[62];
z[48] = z[21] * z[48];
z[48] = z[48] + -z[54] + -z[101];
z[48] = z[15] * z[48];
z[78] = z[21] * z[88];
z[72] = -z[72] + z[78];
z[79] = z[72] + z[84];
z[80] = z[20] * z[125];
z[77] = T(2) * z[4] + -z[53] + z[77];
z[77] = z[4] * z[77];
z[77] = (T(5) * z[65]) / T(4) + z[77] + -z[79] + z[80] + z[92];
z[77] = z[18] * z[77];
z[48] = z[48] + z[50] + z[77];
z[48] = z[33] * z[48];
z[62] = (T(-7) * z[21]) / T(2) + -z[62];
z[62] = z[21] * z[62];
z[54] = -z[54] + z[62];
z[54] = z[15] * z[54];
z[61] = T(5) * z[4] + T(-4) * z[21] + z[61] + -z[89] + -z[144];
z[61] = z[4] * z[61];
z[62] = -z[53] + z[116];
z[62] = z[20] * z[62];
z[61] = z[61] + z[62] + -z[72] + -z[86] + z[107];
z[62] = z[18] * z[61];
z[50] = z[50] + z[54] + -z[58] + z[62];
z[50] = z[34] * z[50];
z[54] = T(11) * z[4] + T(-20) * z[51] + -z[89] + T(-10) * z[113];
z[54] = z[4] * z[54];
z[58] = -z[2] + z[116];
z[62] = z[58] + -z[144];
z[62] = z[20] * z[62];
z[54] = z[54] + z[62] + (T(-17) * z[65]) / T(2) + -z[79] + T(9) * z[90] + -z[150];
z[54] = z[34] * z[54];
z[61] = z[33] * z[61];
z[58] = -(z[20] * z[58]);
z[58] = -z[5] + z[6] + z[58] + z[78] + -z[90];
z[62] = z[25] / T(2);
z[58] = z[58] / T(2) + -z[62] + z[114];
z[58] = z[10] * z[58];
z[72] = z[2] * z[93];
z[53] = (T(3) * z[4]) / T(2) + -z[53];
z[77] = -(z[4] * z[53]);
z[77] = -z[6] + -z[72] + z[77];
z[77] = z[35] * z[77];
z[58] = z[58] + z[77];
z[77] = (T(11) * z[65]) / T(4) + z[98];
z[77] = z[42] * z[77];
z[78] = T(-3) * z[45] + (T(2) * z[115]) / T(3);
z[54] = (T(643) * z[44]) / T(24) + z[54] + T(3) * z[58] + z[61] + z[77] + T(2) * z[78] + -z[83] + -z[110];
z[54] = z[17] * z[54];
z[58] = z[2] + z[87];
z[61] = z[58] * z[100];
z[57] = z[57] + z[61] + z[69] + z[130] + z[132] + z[151];
z[57] = z[30] * z[57];
z[61] = z[31] * z[139];
z[69] = z[32] * z[70];
z[70] = z[37] * z[152];
z[47] = z[47] + -z[82];
z[47] = z[20] * z[47];
z[58] = -(z[21] * z[58]);
z[47] = -z[5] + z[6] + z[25] + z[47] + z[58] + z[90];
z[47] = z[36] * z[47];
z[47] = T(3) * z[47] + z[57] + z[61] + z[69] + z[70];
z[47] = z[29] * z[47];
z[57] = -(z[87] * z[100]);
z[58] = -(z[20] * z[105]);
z[57] = z[57] + z[58] + z[64] + z[65] + -z[131] + -z[133];
z[57] = z[57] * z[75];
z[58] = z[39] / T(6) + z[51];
z[58] = z[39] * z[58];
z[58] = z[58] + -z[95] + z[108];
z[58] = z[39] * z[58];
z[61] = z[42] * z[51];
z[64] = z[61] + -z[67];
z[64] = z[42] * z[64];
z[64] = (T(-115) * z[44]) / T(12) + T(3) * z[64] + (T(-5) * z[115]) / T(3);
z[57] = z[57] + z[58] + z[64] / T(2);
z[57] = z[14] * z[57];
z[58] = z[21] + -z[117];
z[58] = z[58] * z[100];
z[58] = z[58] + (T(-3) * z[73]) / T(2) + -z[85] + T(6) * z[90] + -z[104] + z[133];
z[58] = z[15] * z[58];
z[64] = -(z[4] * z[102]);
z[67] = z[28] * z[52];
z[69] = -(z[21] * z[117]);
z[64] = z[6] + T(2) * z[27] + z[64] + -z[66] + z[67] + z[69] + -z[72];
z[64] = z[14] * z[64];
z[58] = z[58] + T(3) * z[64] + -z[140];
z[58] = z[35] * z[58];
z[55] = -z[2] + z[55];
z[55] = z[55] * z[103];
z[55] = z[55] + z[65] + z[106] + -z[138];
z[64] = z[10] + -z[33];
z[55] = z[55] * z[64];
z[64] = z[35] * z[99];
z[66] = -z[2] + (T(3) * z[21]) / T(2);
z[66] = z[66] * z[100];
z[53] = -(z[53] * z[103]);
z[53] = z[53] + z[66] + z[85] + -z[106];
z[53] = z[34] * z[53];
z[66] = -z[85] + z[95];
z[67] = z[39] / T(2);
z[69] = -z[39] + T(-9) * z[51];
z[69] = z[67] * z[69];
z[66] = T(2) * z[66] + z[69];
z[66] = z[39] * z[66];
z[69] = z[96] + -z[97];
z[69] = z[42] * z[69];
z[69] = (T(17) * z[65]) / T(4) + z[69];
z[69] = z[42] * z[69];
z[69] = z[69] + -z[111];
z[53] = (T(35) * z[44]) / T(8) + z[53] + z[55] + z[64] + z[66] + z[69] + z[115];
z[53] = z[19] * z[53];
z[55] = z[147] + z[149];
z[64] = -(z[103] * z[113]);
z[66] = z[2] * z[100];
z[59] = -z[55] + -z[59] + z[64] + z[66] + z[130];
z[59] = z[59] * z[75];
z[64] = prod_pow(z[42], 2);
z[64] = T(2) * z[64] + z[134];
z[64] = z[42] * z[64];
z[66] = (T(-4) * z[39]) / T(3) + z[51];
z[66] = z[39] * z[66];
z[66] = (T(-9) * z[65]) / T(4) + z[66];
z[66] = z[39] * z[66];
z[59] = (T(-5) * z[44]) / T(12) + z[59] + z[64] + z[66] + -z[111] + (T(-7) * z[115]) / T(3);
z[59] = z[16] * z[59];
z[64] = -z[52] + z[94];
z[64] = z[3] * z[64];
z[64] = z[64] + (T(-3) * z[65]) / T(4) + z[146];
z[65] = -(z[20] * z[124]);
z[66] = -z[21] + T(2) * z[76];
z[70] = z[21] * z[66];
z[65] = T(-2) * z[25] + -z[64] + z[65] + z[70];
z[65] = z[15] * z[65];
z[70] = -(z[20] * z[123]);
z[62] = -z[62] + z[64] + z[70] + -z[112];
z[62] = z[18] * z[62];
z[62] = z[62] + z[65];
z[60] = -(z[60] * z[124]);
z[64] = z[66] * z[100];
z[55] = T(-6) * z[25] + z[55] + z[60] + z[64] + -z[134];
z[55] = z[16] * z[55];
z[55] = z[55] + T(3) * z[62] + -z[153];
z[55] = z[38] * z[55];
z[60] = z[10] * z[145];
z[62] = z[15] + -z[19];
z[64] = z[39] + z[51];
z[62] = z[62] * z[64];
z[64] = -z[17] + -z[18] + z[23];
z[64] = z[39] * z[64];
z[52] = z[39] + -z[52];
z[52] = z[14] * z[52];
z[65] = -z[39] + z[51];
z[65] = z[16] * z[65];
z[52] = z[52] + z[62] + z[64] + T(2) * z[65];
z[52] = z[43] * z[52];
z[62] = (T(7) * z[39]) / T(3) + z[63];
z[62] = z[62] * z[67];
z[61] = T(-6) * z[61] + z[62] + z[91];
z[61] = z[39] * z[61];
z[61] = (T(-25) * z[44]) / T(24) + z[61] + -z[69] + z[115] / T(6);
z[61] = z[15] * z[61];
z[62] = prod_pow(z[39], 2);
z[62] = z[62] / T(2) + z[95] + z[134];
z[62] = z[39] * z[62];
z[62] = (T(-113) * z[44]) / T(8) + z[62] + -z[109] + -z[115] / T(4);
z[62] = z[23] * z[62];
z[63] = int_to_imaginary<T>(1) * z[41];
z[63] = T(36) * z[63] + (T(-11) * z[115]) / T(36);
z[64] = int_to_imaginary<T>(1) * z[40];
z[51] = z[39] * z[51];
z[51] = -z[51] / T(20) + z[64];
z[51] = z[39] * z[51];
z[64] = -(z[7] * z[40]);
z[51] = T(3) * z[51] + z[63] / T(5) + z[64];
z[51] = z[32] * z[51];
return z[47] / T(2) + z[48] + z[49] + z[50] + z[51] + T(3) * z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[68] + z[71] + z[74];
}



template IntegrandConstructorType<double> f_4_256_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_256_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_256_construct (const Kin<qd_real>&);
#endif

}