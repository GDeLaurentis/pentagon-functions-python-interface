#include "f_4_319.h"

namespace PentagonFunctions {

template <typename T> T f_4_319_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_319_W_10 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_319_W_10 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[1]) * (((T(3) * kin.v[1]) / T(4) + (T(3) * kin.v[2]) / T(2) + -kin.v[4] + T(-1)) * kin.v[1] + ((T(3) * kin.v[2]) / T(4) + -kin.v[4] + T(-1)) * kin.v[2]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (((T(3) * kin.v[1]) / T(4) + (T(3) * kin.v[2]) / T(2) + -kin.v[4] + T(-1)) * kin.v[1] + ((T(3) * kin.v[2]) / T(4) + -kin.v[4] + T(-1)) * kin.v[2]) + rlog(kin.v[2]) * (kin.v[1] * ((T(-3) * kin.v[1]) / T(4) + (T(-3) * kin.v[2]) / T(2) + T(1) + kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(4) + T(1) + kin.v[4])) + rlog(-kin.v[4]) * (kin.v[1] * ((T(-3) * kin.v[1]) / T(4) + (T(-3) * kin.v[2]) / T(2) + T(1) + kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(4) + T(1) + kin.v[4]));
c[1] = (-kin.v[1] + -kin.v[2]) * rlog(kin.v[2]) + (-kin.v[1] + -kin.v[2]) * rlog(-kin.v[4]) + rlog(-kin.v[1]) * (kin.v[1] + kin.v[2]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[1] + kin.v[2]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[4] * (prod_pow(abb[2], 2) + -abb[3]) + prod_pow(abb[2], 2) * (abb[7] + -abb[5] + -abb[6]) + abb[3] * (abb[5] + abb[6] + -abb[7]) + prod_pow(abb[1], 2) * (abb[5] + abb[6] + -abb[4] + -abb[7]));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_319_construct (const Kin<T>& kin) {
    return [&kin, 
            dl10 = DLog_W_10<T>(kin),dl15 = DLog_W_15<T>(kin),dl17 = DLog_W_17<T>(kin),dl2 = DLog_W_2<T>(kin),dl3 = DLog_W_3<T>(kin),dl5 = DLog_W_5<T>(kin),spdl10 = SpDLog_f_4_319_W_10<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,17> abbr = 
            {dl10(t), rlog(-v_path[4]), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_7(kin_path), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[16] / kin_path.W[16]), dl15(t), rlog(v_path[2]), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl17(t), -rlog(t), dl2(t), dl3(t), dl5(t)}
;

        auto result = f_4_319_abbreviated(abbr);
        result = result + spdl10(t, abbr);

        return result;
    };
}

template <typename T> T f_4_319_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[42];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[8];
z[3] = abb[12];
z[4] = abb[14];
z[5] = abb[16];
z[6] = abb[5];
z[7] = abb[15];
z[8] = abb[6];
z[9] = abb[7];
z[10] = abb[13];
z[11] = abb[2];
z[12] = abb[9];
z[13] = bc<TR>[0];
z[14] = abb[3];
z[15] = abb[10];
z[16] = abb[11];
z[17] = bc<TR>[2];
z[18] = bc<TR>[9];
z[19] = z[1] + z[6];
z[20] = T(4) * z[10];
z[21] = z[8] + T(3) * z[9] + z[19] + -z[20];
z[21] = z[5] * z[21];
z[22] = z[6] + z[9];
z[23] = -z[20] + T(2) * z[22];
z[24] = -z[1] + T(3) * z[8];
z[25] = z[23] + z[24];
z[26] = z[3] * z[25];
z[27] = T(2) * z[10];
z[19] = -z[9] + -z[19] + z[27];
z[28] = T(2) * z[7];
z[29] = z[19] * z[28];
z[30] = z[4] * z[25];
z[31] = -z[6] + z[9];
z[32] = z[1] + -z[8];
z[33] = -z[31] + z[32];
z[33] = z[2] * z[33];
z[21] = z[21] + z[26] + z[29] + -z[30] + -z[33];
z[26] = -(z[15] * z[21]);
z[29] = z[9] / T(3);
z[34] = (T(2) * z[10]) / T(3);
z[35] = z[1] + z[8];
z[36] = z[6] + -z[29] + -z[34] + z[35] / T(6);
z[36] = z[5] * z[36];
z[37] = z[1] / T(3) + -z[8];
z[29] = -z[6] / T(3) + -z[29] + z[34] + z[37] / T(2);
z[29] = z[4] * z[29];
z[23] = z[23] + z[35];
z[34] = z[7] * z[23];
z[37] = -z[3] + z[7];
z[38] = z[4] + -z[5];
z[39] = z[37] + z[38];
z[40] = z[17] * z[39];
z[41] = z[1] + -z[8] / T(3) + (T(-4) * z[10]) / T(3) + (T(2) * z[22]) / T(3);
z[41] = z[3] * z[41];
z[29] = z[29] + (T(-4) * z[33]) / T(3) + -z[34] / T(3) + z[36] + (T(-5) * z[40]) / T(4) + z[41];
z[34] = prod_pow(z[13], 2);
z[29] = z[29] * z[34];
z[36] = z[3] * z[23];
z[22] = z[22] + -z[27];
z[40] = z[8] + z[22];
z[41] = z[28] * z[40];
z[31] = z[5] * z[31];
z[36] = z[31] + -z[33] + z[36] + -z[41];
z[36] = z[0] * z[36];
z[23] = z[5] * z[23];
z[28] = z[28] * z[32];
z[41] = T(2) * z[32];
z[41] = z[3] * z[41];
z[23] = -z[23] + z[28] + z[30] + z[41];
z[23] = z[11] * z[23];
z[28] = -z[23] + z[36];
z[36] = T(3) * z[6] + z[9] + z[35];
z[27] = -z[27] + z[36] / T(2);
z[27] = z[5] * z[27];
z[24] = z[22] + z[24] / T(2);
z[24] = z[4] * z[24];
z[24] = -z[24] + z[27];
z[22] = z[22] + z[35] / T(2);
z[22] = z[3] * z[22];
z[27] = -(z[7] * z[19]);
z[27] = -z[22] + -z[24] + z[27] + (T(3) * z[33]) / T(2);
z[27] = z[12] * z[27];
z[27] = z[27] + z[28];
z[27] = z[12] * z[27];
z[21] = z[16] * z[21];
z[35] = z[3] * z[32];
z[35] = -z[31] + -z[33] + z[35];
z[35] = z[12] * z[35];
z[39] = prod_pow(z[17], 2) * z[39];
z[41] = z[37] + z[38] / T(2);
z[34] = z[34] * z[41];
z[21] = z[21] + -z[28] + z[34] / T(6) + T(2) * z[35] + -z[39] / T(2);
z[21] = int_to_imaginary<T>(1) * z[13] * z[21];
z[19] = z[3] * z[19];
z[20] = -z[20] + z[36];
z[20] = z[5] * z[20];
z[25] = z[7] * z[25];
z[19] = T(2) * z[19] + z[20] + z[25] + -z[30];
z[19] = z[14] * z[19];
z[20] = z[7] * z[40];
z[20] = z[20] + -z[22] + z[24] + -z[33] / T(2);
z[20] = z[0] * z[20];
z[20] = z[20] + z[23];
z[20] = z[0] * z[20];
z[22] = T(21) * z[37] + z[38];
z[22] = z[18] * z[22];
z[23] = z[7] * z[32];
z[23] = -z[23] + -z[31];
z[23] = prod_pow(z[11], 2) * z[23];
return z[19] + z[20] + z[21] + z[22] / T(8) + z[23] + z[26] + z[27] + z[29];
}



template IntegrandConstructorType<double> f_4_319_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_319_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_319_construct (const Kin<qd_real>&);
#endif

}