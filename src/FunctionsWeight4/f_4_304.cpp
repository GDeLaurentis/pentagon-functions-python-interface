#include "f_4_304.h"

namespace PentagonFunctions {

template <typename T> T f_4_304_abbreviated (const std::array<T,19>&);



template <typename T> IntegrandConstructorType<T> f_4_304_construct (const Kin<T>& kin) {
    return [&kin, 
            dl9 = DLog_W_9<T>(kin),dl14 = DLog_W_14<T>(kin),dl1 = DLog_W_1<T>(kin),dl4 = DLog_W_4<T>(kin),dl2 = DLog_W_2<T>(kin),dl16 = DLog_W_16<T>(kin),dl11 = DLog_W_11<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,19> abbr = 
            {dl9(t), rlog(kin.W[1] / kin_path.W[1]), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[0] / kin_path.W[0]), rlog(kin.W[15] / kin_path.W[15]), dl14(t), rlog(-v_path[1]), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl1(t), rlog(kin.W[10] / kin_path.W[10]), dl4(t), dl2(t), dl16(t), dl11(t)}
;

        auto result = f_4_304_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_304_abbreviated(const std::array<T,19>& abb)
{
using TR = typename T::value_type;
T z[59];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[13];
z[12] = abb[15];
z[13] = abb[16];
z[14] = abb[17];
z[15] = abb[10];
z[16] = abb[11];
z[17] = abb[12];
z[18] = abb[14];
z[19] = bc<TR>[1];
z[20] = bc<TR>[2];
z[21] = bc<TR>[4];
z[22] = bc<TR>[9];
z[23] = abb[18];
z[24] = T(2) * z[15];
z[25] = int_to_imaginary<T>(1) * z[3];
z[26] = z[24] * z[25];
z[27] = -z[2] + z[25];
z[28] = z[15] + z[27];
z[29] = z[4] * z[28];
z[30] = z[26] + T(2) * z[29];
z[31] = z[6] * z[25];
z[31] = -z[5] + z[31];
z[32] = T(2) * z[31];
z[33] = z[30] + -z[32];
z[34] = z[17] * z[25];
z[34] = z[16] + z[34];
z[35] = prod_pow(z[15], 2);
z[36] = T(2) * z[34] + z[35];
z[37] = prod_pow(z[2], 2);
z[38] = prod_pow(z[3], 2);
z[39] = -z[33] + z[36] + -z[37] + (T(4) * z[38]) / T(3);
z[40] = z[11] * z[39];
z[27] = z[2] * z[27];
z[41] = prod_pow(z[4], 2);
z[42] = (T(5) * z[38]) / T(6);
z[27] = z[27] + z[31] + z[41] + z[42];
z[27] = z[0] * z[27];
z[28] = T(2) * z[28];
z[41] = z[4] + z[28];
z[41] = z[4] * z[41];
z[41] = z[26] + z[41];
z[43] = T(-3) * z[31];
z[44] = z[41] + z[43];
z[45] = -z[35] + -z[38];
z[46] = (T(3) * z[2]) / T(2) + -z[15];
z[46] = z[2] * z[46];
z[45] = -z[34] + z[44] + (T(3) * z[45]) / T(2) + z[46];
z[45] = z[12] * z[45];
z[24] = -z[2] + z[24];
z[46] = z[2] * z[24];
z[47] = z[38] / T(3);
z[32] = z[32] + -z[36] + z[46] + z[47];
z[46] = -(z[13] * z[32]);
z[48] = z[2] / T(2);
z[49] = z[15] + z[48];
z[50] = -z[25] + z[49];
z[50] = z[2] * z[50];
z[51] = z[34] + (T(2) * z[38]) / T(3);
z[52] = (T(3) * z[35]) / T(2);
z[50] = z[50] + -z[51] + -z[52];
z[50] = z[10] * z[50];
z[45] = -z[27] + z[40] + z[45] + z[46] + -z[50];
z[45] = z[1] * z[45];
z[46] = z[13] + -z[14];
z[46] = z[32] * z[46];
z[39] = z[12] * z[39];
z[39] = z[39] + -z[40] + z[46];
z[39] = z[18] * z[39];
z[46] = z[20] * z[25];
z[53] = z[46] + z[47];
z[54] = z[19] * z[25];
z[53] = T(2) * z[53] + -z[54];
z[55] = T(2) * z[19];
z[55] = z[53] * z[55];
z[56] = z[21] * z[25];
z[57] = int_to_imaginary<T>(1) * prod_pow(z[3], 3);
z[55] = (T(14) * z[22]) / T(3) + -z[55] + T(4) * z[56] + -z[57];
z[58] = z[23] * z[55];
z[39] = z[39] + z[58];
z[58] = z[15] * z[25];
z[29] = z[29] + z[58];
z[34] = T(3) * z[34];
z[43] = T(-3) * z[29] + z[34] + (T(-3) * z[37]) / T(2) + T(2) * z[38] + -z[43] + z[52];
z[43] = z[11] * z[43];
z[52] = z[35] / T(2);
z[29] = z[29] + -z[31] + z[37] / T(2) + -z[51] + -z[52];
z[37] = z[12] * z[29];
z[37] = -z[27] + z[37] + z[43];
z[37] = z[7] * z[37];
z[35] = -z[35] + (T(-7) * z[38]) / T(3);
z[43] = z[2] * z[49];
z[28] = -z[4] + z[28];
z[28] = z[4] * z[28];
z[26] = z[26] + z[28] + -z[31] + -z[34] + z[35] / T(2) + z[43];
z[26] = z[12] * z[26];
z[26] = z[26] + z[27] + z[40];
z[26] = z[9] * z[26];
z[28] = z[11] + z[12];
z[28] = z[28] * z[29];
z[29] = -(z[2] * z[25]);
z[29] = z[29] + z[31] + z[36] + z[38] / T(2) + -z[41];
z[29] = z[13] * z[29];
z[27] = z[27] + z[28] + z[29] + -z[50];
z[27] = z[8] * z[27];
z[28] = -z[15] + z[48];
z[29] = z[25] + -z[28];
z[29] = z[2] * z[29];
z[29] = z[29] + z[30] + -z[34] + -z[47] + -z[52];
z[29] = z[7] * z[29];
z[30] = z[1] + z[9];
z[30] = z[30] * z[32];
z[28] = -z[25] + -z[28];
z[28] = z[2] * z[28];
z[28] = z[28] + -z[33] + z[51] + -z[52];
z[28] = z[8] * z[28];
z[28] = z[28] + z[29] + z[30] + -z[55];
z[28] = z[14] * z[28];
z[29] = -(z[9] * z[32]);
z[24] = -z[24] + z[25];
z[24] = z[2] * z[24];
z[24] = z[24] + -z[42] + z[44];
z[24] = z[7] * z[24];
z[24] = z[24] + z[29] + -z[55];
z[24] = z[13] * z[24];
z[29] = (T(3) * z[38]) / T(2) + z[46];
z[29] = z[20] * z[29];
z[30] = z[38] + T(3) * z[46];
z[30] = T(2) * z[30] + T(-3) * z[54];
z[30] = z[19] * z[30];
z[30] = (T(3) * z[22]) / T(4) + -z[29] + z[30] + T(-6) * z[56] + z[57];
z[30] = z[12] * z[30];
z[31] = -(z[19] * z[53]);
z[25] = T(2) * z[25];
z[25] = z[21] * z[25];
z[25] = (T(-65) * z[22]) / T(12) + z[25] + z[29] + z[31];
z[25] = z[11] * z[25];
z[29] = z[7] + z[9];
z[29] = z[29] * z[50];
return z[24] + z[25] + z[26] + z[27] + z[28] + z[29] + z[30] + z[37] + T(2) * z[39] + z[45];
}



template IntegrandConstructorType<double> f_4_304_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_304_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_304_construct (const Kin<qd_real>&);
#endif

}