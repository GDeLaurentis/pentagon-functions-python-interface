#include "f_4_26.h"

namespace PentagonFunctions {

template <typename T> T f_4_26_abbreviated (const std::array<T,41>&);

template <typename T> class SpDLog_f_4_26_W_7 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_26_W_7 (const Kin<T>& kin) {
        c[0] = (T(-8) * kin.v[0] * kin.v[4]) / T(3) + T(-4) * kin.v[1] * kin.v[4] + T(4) * kin.v[2] * kin.v[4] + T(2) * prod_pow(kin.v[4], 2) + kin.v[3] * ((T(-8) * kin.v[0]) / T(3) + T(-4) * kin.v[1] + T(4) * kin.v[2] + (bc<T>[1] * T(-2) + T(2)) * kin.v[3] + T(4) * kin.v[4] + bc<T>[1] * (T(4) + T(4) * kin.v[1] + T(-4) * kin.v[4])) + bc<T>[1] * (T(4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(4) + T(-2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] * kin.v[4] + kin.v[3] * (T(4) + T(4) * kin.v[1] + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(-2) * kin.v[4]));
c[1] = (T(-8) * kin.v[0] * kin.v[4]) / T(3) + T(-4) * kin.v[1] * kin.v[4] + T(4) * kin.v[2] * kin.v[4] + T(2) * prod_pow(kin.v[4], 2) + kin.v[3] * ((T(-8) * kin.v[0]) / T(3) + T(-4) * kin.v[1] + T(4) * kin.v[2] + T(2) * kin.v[3] + T(4) * kin.v[4]) + rlog(-kin.v[4]) * ((T(3) * prod_pow(kin.v[4], 2)) / T(2) + T(-2) * kin.v[0] * kin.v[4] + T(-3) * kin.v[1] * kin.v[4] + T(3) * kin.v[2] * kin.v[4] + bc<T>[1] * (((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + T(3) * kin.v[1] * kin.v[4]) + kin.v[3] * (T(-2) * kin.v[0] + T(-3) * kin.v[1] + T(3) * kin.v[2] + ((bc<T>[1] * T(-3)) / T(2) + T(3) / T(2)) * kin.v[3] + T(3) * kin.v[4] + bc<T>[1] * (T(3) + T(3) * kin.v[1] + T(-3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + T(3) * kin.v[1] * kin.v[4] + kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(3) + T(3) * kin.v[1] + T(-3) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(3) * prod_pow(kin.v[4], 2)) / T(2) + T(-2) * kin.v[0] * kin.v[4] + T(-3) * kin.v[1] * kin.v[4] + T(3) * kin.v[2] * kin.v[4] + bc<T>[1] * (((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + T(3) * kin.v[1] * kin.v[4]) + kin.v[3] * (T(-2) * kin.v[0] + T(-3) * kin.v[1] + T(3) * kin.v[2] + ((bc<T>[1] * T(-3)) / T(2) + T(3) / T(2)) * kin.v[3] + T(3) * kin.v[4] + bc<T>[1] * (T(3) + T(3) * kin.v[1] + T(-3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + T(3) * kin.v[1] * kin.v[4] + kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(3) + T(3) * kin.v[1] + T(-3) * kin.v[4]))) + bc<T>[0] * int_to_imaginary<T>(1) * ((T(-4) * kin.v[0] * kin.v[4]) / T(3) + T(-2) * kin.v[1] * kin.v[4] + T(2) * kin.v[2] * kin.v[4] + prod_pow(kin.v[4], 2) + kin.v[3] * ((T(-4) * kin.v[0]) / T(3) + T(-2) * kin.v[1] + T(2) * kin.v[2] + kin.v[3] + T(2) * kin.v[4]) + bc<T>[1] * ((-kin.v[4] + T(2)) * kin.v[4] + T(2) * kin.v[1] * kin.v[4] + kin.v[3] * (-kin.v[3] + T(2) + T(2) * kin.v[1] + T(-2) * kin.v[4]))) + bc<T>[1] * ((T(-8) * kin.v[0] * kin.v[4]) / T(3) + T(-4) * kin.v[1] * kin.v[4] + T(4) * kin.v[2] * kin.v[4] + T(2) * prod_pow(kin.v[4], 2) + kin.v[3] * ((T(-8) * kin.v[0]) / T(3) + T(-4) * kin.v[1] + T(4) * kin.v[2] + T(2) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[1] * ((-kin.v[4] + T(2)) * kin.v[4] + T(2) * kin.v[1] * kin.v[4] + kin.v[3] * (-kin.v[3] + T(2) + T(2) * kin.v[1] + T(-2) * kin.v[4]))) + rlog(-kin.v[1]) * (-prod_pow(kin.v[4], 2) / T(2) + (T(2) * kin.v[0] * kin.v[4]) / T(3) + -(kin.v[2] * kin.v[4]) + kin.v[1] * kin.v[4] + bc<T>[1] * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4]) + kin.v[3] * ((T(2) * kin.v[0]) / T(3) + -kin.v[2] + -kin.v[4] + kin.v[1] + (bc<T>[1] / T(2) + T(-1) / T(2)) * kin.v[3] + bc<T>[1] * (-kin.v[1] + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + -kin.v[1] + T(-1) + kin.v[4]))) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (-prod_pow(kin.v[4], 2) / T(2) + (T(2) * kin.v[0] * kin.v[4]) / T(3) + -(kin.v[2] * kin.v[4]) + kin.v[1] * kin.v[4] + bc<T>[1] * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4]) + kin.v[3] * ((T(2) * kin.v[0]) / T(3) + -kin.v[2] + -kin.v[4] + kin.v[1] + (bc<T>[1] / T(2) + T(-1) / T(2)) * kin.v[3] + bc<T>[1] * (-kin.v[1] + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + -kin.v[1] + T(-1) + kin.v[4]))) + rlog(kin.v[3]) * (-prod_pow(kin.v[4], 2) / T(2) + (T(2) * kin.v[0] * kin.v[4]) / T(3) + -(kin.v[2] * kin.v[4]) + kin.v[1] * kin.v[4] + bc<T>[1] * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4]) + kin.v[3] * ((T(2) * kin.v[0]) / T(3) + -kin.v[2] + -kin.v[4] + kin.v[1] + (bc<T>[1] / T(2) + T(-1) / T(2)) * kin.v[3] + bc<T>[1] * (-kin.v[1] + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + -kin.v[1] + T(-1) + kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (-prod_pow(kin.v[4], 2) / T(2) + (T(2) * kin.v[0] * kin.v[4]) / T(3) + -(kin.v[2] * kin.v[4]) + kin.v[1] * kin.v[4] + bc<T>[1] * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4]) + kin.v[3] * ((T(2) * kin.v[0]) / T(3) + -kin.v[2] + -kin.v[4] + kin.v[1] + (bc<T>[1] / T(2) + T(-1) / T(2)) * kin.v[3] + bc<T>[1] * (-kin.v[1] + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + -kin.v[1] + T(-1) + kin.v[4]))) + rlog(kin.v[0]) * ((T(4) * kin.v[0] * kin.v[4]) / T(3) + -prod_pow(kin.v[4], 2) + T(2) * kin.v[1] * kin.v[4] + T(-2) * kin.v[2] * kin.v[4] + bc<T>[1] * (T(-2) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-2) + kin.v[4])) + kin.v[3] * ((T(4) * kin.v[0]) / T(3) + T(2) * kin.v[1] + T(-2) * kin.v[2] + (bc<T>[1] + T(-1)) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[1] * (T(-2) + T(-2) * kin.v[1] + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[3] * (T(-2) + T(-2) * kin.v[1] + kin.v[3] + T(2) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(4) + bc<T>[1] * T(4) + T(8)) * kin.v[3] + T(8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4] + bc<T>[1] * T(4) * kin.v[4];
c[3] = rlog(kin.v[0]) * ((T(-4) + bc<T>[1] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[1] * T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[4]) + rlog(-kin.v[1]) * (-(bc<T>[1] * kin.v[4]) + (-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (-(bc<T>[1] * kin.v[4]) + (-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(kin.v[3]) * (-(bc<T>[1] * kin.v[4]) + (-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (-(bc<T>[1] * kin.v[4]) + (-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(3) + bc<T>[1] * T(3) + T(6)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4] + bc<T>[1] * T(3) * kin.v[4] + T(6) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(3) + bc<T>[1] * T(3) + T(6)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4] + bc<T>[1] * T(3) * kin.v[4] + T(6) * kin.v[4]) + bc<T>[1] * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[4] + T(4) * kin.v[4] + bc<T>[1] * (T(2) * kin.v[3] + T(2) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[16] * (abb[15] * (abb[1] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[7] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(3) + abb[9] * bc<T>[0] * int_to_imaginary<T>(3) + abb[19] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[15] * (-abb[1] + -abb[7] + -abb[10] + -abb[11] + abb[19] * T(-4) + abb[8] * T(3) + abb[9] * T(3))) + abb[17] * (abb[15] * (abb[15] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) + abb[3] * abb[15] * T(2) + abb[4] * abb[15] * T(2) + abb[18] * T(4)) + abb[3] * abb[15] * (abb[1] + abb[7] + abb[10] + abb[11] + abb[8] * T(-3) + abb[9] * T(-3) + abb[19] * T(4)) + abb[4] * abb[15] * (abb[1] + abb[7] + abb[10] + abb[11] + abb[8] * T(-3) + abb[9] * T(-3) + abb[19] * T(4)) + abb[2] * (abb[15] * abb[17] * T(-2) + abb[15] * (-abb[1] + -abb[7] + -abb[10] + -abb[11] + abb[19] * T(-4) + abb[8] * T(3) + abb[9] * T(3)) + abb[13] * (abb[1] + abb[7] + abb[10] + abb[11] + abb[8] * T(-3) + abb[9] * T(-3) + abb[17] * T(2) + abb[19] * T(4))) + abb[18] * (abb[8] * T(-6) + abb[9] * T(-6) + abb[1] * T(2) + abb[7] * T(2) + abb[10] * T(2) + abb[11] * T(2) + abb[19] * T(8)) + abb[13] * (abb[1] * bc<T>[0] * int_to_imaginary<T>(1) + abb[7] * bc<T>[0] * int_to_imaginary<T>(1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(1) + abb[17] * (abb[3] * T(-2) + abb[4] * T(-2) + abb[15] * T(-2) + bc<T>[0] * int_to_imaginary<T>(2)) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[19] * bc<T>[0] * int_to_imaginary<T>(4) + abb[3] * (-abb[1] + -abb[7] + -abb[10] + -abb[11] + abb[19] * T(-4) + abb[8] * T(3) + abb[9] * T(3)) + abb[4] * (-abb[1] + -abb[7] + -abb[10] + -abb[11] + abb[19] * T(-4) + abb[8] * T(3) + abb[9] * T(3)) + abb[15] * (-abb[1] + -abb[7] + -abb[10] + -abb[11] + abb[19] * T(-4) + abb[8] * T(3) + abb[9] * T(3)) + abb[13] * (abb[8] * T(-6) + abb[9] * T(-6) + abb[1] * T(2) + abb[7] * T(2) + abb[10] * T(2) + abb[11] * T(2) + abb[17] * T(4) + abb[19] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_26_construct (const Kin<T>& kin) {
    return [&kin, 
            dl3 = DLog_W_3<T>(kin),dl14 = DLog_W_14<T>(kin),dl25 = DLog_W_25<T>(kin),dl7 = DLog_W_7<T>(kin),dl16 = DLog_W_16<T>(kin),dl30 = DLog_W_30<T>(kin),dl1 = DLog_W_1<T>(kin),dl26 = DLog_W_26<T>(kin),dl17 = DLog_W_17<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),dl5 = DLog_W_5<T>(kin),dl18 = DLog_W_18<T>(kin),dl19 = DLog_W_19<T>(kin),dl20 = DLog_W_20<T>(kin),dl4 = DLog_W_4<T>(kin),dl2 = DLog_W_2<T>(kin),spdl7 = SpDLog_f_4_26_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,41> abbr = 
            {dl3(t), rlog(kin.W[1] / kin_path.W[1]), rlog(v_path[0]), rlog(v_path[3]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_2(kin_path), f_2_1_9(kin_path), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), dl14(t), rlog(-v_path[1]), dl25(t), rlog(-v_path[1] + v_path[3] + v_path[4]), dl7(t), rlog(kin.W[0] / kin_path.W[0]), f_2_1_11(kin_path), -rlog(t), dl16(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl30(t) / kin_path.SqrtDelta, f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl1(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl26(t) / kin_path.SqrtDelta, dl17(t), dl27(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl5(t), dl18(t), dl19(t), dl20(t), dl4(t), dl2(t)}
;

        auto result = f_4_26_abbreviated(abbr);
        result = result + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_26_abbreviated(const std::array<T,41>& abb)
{
using TR = typename T::value_type;
T z[127];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[20];
z[14] = abb[27];
z[15] = abb[31];
z[16] = abb[36];
z[17] = abb[37];
z[18] = abb[39];
z[19] = abb[40];
z[20] = abb[13];
z[21] = abb[15];
z[22] = abb[12];
z[23] = abb[35];
z[24] = abb[14];
z[25] = abb[38];
z[26] = abb[18];
z[27] = abb[21];
z[28] = abb[22];
z[29] = abb[28];
z[30] = abb[29];
z[31] = abb[17];
z[32] = abb[19];
z[33] = abb[23];
z[34] = abb[24];
z[35] = abb[25];
z[36] = abb[26];
z[37] = abb[30];
z[38] = abb[34];
z[39] = abb[32];
z[40] = abb[33];
z[41] = bc<TR>[1];
z[42] = bc<TR>[3];
z[43] = bc<TR>[5];
z[44] = bc<TR>[2];
z[45] = bc<TR>[4];
z[46] = bc<TR>[7];
z[47] = bc<TR>[8];
z[48] = bc<TR>[9];
z[49] = -z[1] + z[12];
z[50] = z[8] + -z[10];
z[51] = z[9] + z[50];
z[52] = -z[11] + z[51];
z[53] = -z[49] + z[52];
z[54] = z[0] * z[53];
z[55] = z[9] + z[10];
z[56] = z[8] + z[11];
z[57] = z[55] + -z[56];
z[58] = -z[49] + z[57];
z[59] = z[13] * z[58];
z[60] = z[54] + -z[59];
z[61] = z[34] + z[35] + z[36];
z[62] = z[40] * z[61];
z[63] = z[38] * z[61];
z[64] = z[62] + z[63];
z[65] = z[39] * z[61];
z[66] = z[64] + -z[65];
z[67] = z[33] * z[61];
z[68] = z[60] + z[66] + z[67];
z[69] = z[17] + -z[19];
z[70] = z[18] + -z[23] + z[25];
z[71] = -z[16] + z[70];
z[72] = z[71] / T(2);
z[73] = (T(-7) * z[69]) / T(3) + -z[72];
z[73] = z[41] * z[73];
z[74] = T(-13) * z[19] + T(7) * z[71];
z[74] = z[44] * z[74];
z[73] = z[73] + z[74];
z[74] = z[49] + z[50];
z[74] = z[22] * z[74];
z[75] = T(3) * z[74];
z[76] = z[1] + z[11];
z[77] = -z[12] + z[76];
z[78] = -z[9] + z[77];
z[79] = T(3) * z[24];
z[78] = z[78] * z[79];
z[79] = z[8] + T(5) * z[9] + z[10] + T(-3) * z[11];
z[80] = T(3) * z[1];
z[81] = z[79] + -z[80];
z[81] = z[81] / T(2);
z[82] = z[12] / T(2);
z[83] = -z[31] + z[81] + z[82];
z[83] = -z[32] + z[83] / T(2);
z[84] = -(z[25] * z[83]);
z[85] = (T(2) * z[31]) / T(3) + (T(4) * z[32]) / T(3);
z[86] = (T(19) * z[1]) / T(3) + (T(-11) * z[8]) / T(3) + T(-7) * z[9] + z[10] + (T(13) * z[11]) / T(3);
z[86] = (T(-11) * z[12]) / T(12) + z[85] + z[86] / T(4);
z[86] = z[19] * z[86];
z[50] = (T(-7) * z[49]) / T(3) + -z[50];
z[87] = z[14] / T(2);
z[50] = z[50] * z[87];
z[88] = (T(-25) * z[8]) / T(3) + -z[9] + T(11) * z[10] + (T(11) * z[76]) / T(3);
z[85] = (T(-19) * z[12]) / T(12) + (T(13) * z[44]) / T(2) + -z[85] + z[88] / T(4);
z[85] = z[17] * z[85];
z[88] = z[38] + z[39] + z[40];
z[89] = -z[71] + (T(-11) * z[88]) / T(90);
z[89] = z[69] + z[89] / T(2);
z[90] = int_to_imaginary<T>(1) * z[7];
z[89] = z[89] * z[90];
z[91] = z[18] * z[49];
z[92] = (T(13) * z[1]) / T(3) + z[79];
z[92] = (T(-19) * z[12]) / T(6) + -z[31] + z[92] / T(2);
z[92] = -z[32] + z[92] / T(2);
z[92] = z[16] * z[92];
z[50] = z[50] + z[68] / T(4) + z[73] / T(2) + z[75] + -z[78] + z[84] + z[85] + z[86] + z[89] + (T(-11) * z[91]) / T(6) + z[92];
z[50] = z[7] * z[50];
z[68] = T(3) * z[9];
z[73] = z[8] + z[76];
z[84] = T(3) * z[10];
z[85] = z[68] + -z[73] + z[84];
z[86] = -z[82] + z[85] / T(2);
z[89] = z[31] + T(2) * z[32];
z[92] = z[86] + -z[89];
z[93] = z[18] * z[92];
z[94] = z[17] * z[92];
z[95] = z[93] + z[94];
z[82] = -z[82] + z[89];
z[81] = z[81] + -z[82];
z[96] = z[25] * z[81];
z[97] = T(3) * z[96];
z[98] = z[14] * z[92];
z[99] = T(2) * z[1] + -z[12] + z[89];
z[100] = -z[8] + T(2) * z[11] + -z[68] + z[99];
z[101] = T(2) * z[100];
z[101] = z[16] * z[101];
z[102] = z[19] * z[100];
z[101] = (T(-3) * z[64]) / T(2) + z[95] + z[97] + -z[98] + z[101] + T(2) * z[102];
z[101] = z[20] * z[101];
z[103] = T(2) * z[12] + z[89];
z[104] = T(-2) * z[8] + z[76] + z[84] + -z[103];
z[105] = z[17] * z[104];
z[99] = -z[56] + z[99];
z[106] = z[19] * z[99];
z[107] = z[16] * z[49];
z[108] = z[14] * z[49];
z[109] = T(2) * z[49];
z[110] = z[18] * z[109];
z[75] = -z[75] + -z[105] + -z[106] + -z[107] + z[108] + z[110];
z[75] = z[3] * z[75];
z[106] = z[12] + z[55] + -z[73];
z[111] = z[15] * z[106];
z[61] = z[37] * z[61];
z[112] = z[61] + z[111];
z[113] = -z[66] + z[112];
z[114] = z[16] + z[19];
z[81] = z[81] * z[114];
z[115] = z[55] + z[73];
z[116] = (T(3) * z[12]) / T(2) + z[89];
z[115] = z[115] / T(2) + -z[116];
z[115] = z[17] * z[115];
z[117] = z[87] * z[106];
z[81] = -z[81] + z[96] + z[113] / T(2) + z[115] + -z[117];
z[81] = T(3) * z[81];
z[96] = -(z[30] * z[81]);
z[113] = -z[63] + z[67];
z[115] = -z[59] + z[113];
z[55] = -z[55] + -z[56] + z[80];
z[55] = z[55] / T(2) + z[82];
z[55] = z[19] * z[55];
z[80] = z[58] * z[87];
z[82] = T(-3) * z[8] + z[9] + T(5) * z[10] + z[76];
z[82] = z[82] / T(2) + -z[116];
z[116] = z[17] + z[18];
z[117] = z[82] * z[116];
z[82] = z[23] * z[82];
z[55] = z[55] + z[80] + -z[82] + z[115] / T(2) + z[117];
z[55] = T(3) * z[55];
z[80] = -(z[28] * z[55]);
z[115] = z[19] * z[92];
z[109] = z[16] * z[109];
z[94] = -z[94] + z[109] + z[110] + z[115];
z[110] = (T(3) * z[59]) / T(2);
z[111] = (T(3) * z[111]) / T(2);
z[117] = (T(3) * z[61]) / T(2) + z[111];
z[118] = -z[94] + -z[108] + -z[110] + (T(3) * z[113]) / T(2) + z[117];
z[118] = z[2] * z[118];
z[119] = T(2) * z[19];
z[120] = T(2) * z[17];
z[121] = (T(-3) * z[88]) / T(20) + z[119] + -z[120];
z[121] = z[41] * z[121];
z[122] = z[44] * z[69];
z[121] = z[121] + T(3) * z[122];
z[121] = z[41] * z[121];
z[122] = -(z[45] * z[69]);
z[80] = T(-2) * z[75] + z[80] + z[96] + z[101] + z[118] + z[121] + T(3) * z[122];
z[80] = int_to_imaginary<T>(1) * z[80];
z[96] = z[42] * z[88];
z[50] = z[50] + z[80] + -z[96];
z[50] = z[7] * z[50];
z[80] = T(9) * z[10];
z[118] = T(9) * z[9];
z[121] = T(7) * z[1];
z[56] = z[12] + z[56] + -z[80] + -z[118] + z[121];
z[122] = T(2) * z[31] + T(4) * z[32];
z[56] = z[56] / T(4) + z[122];
z[56] = z[14] * z[56];
z[123] = z[18] * z[104];
z[62] = -z[62] + -z[65] + -z[67];
z[62] = z[62] / T(4) + z[74];
z[74] = -z[31] + z[86];
z[74] = -z[32] + z[74] / T(2);
z[86] = z[16] * z[74];
z[124] = (T(5) * z[12]) / T(2);
z[125] = -z[89] + z[124];
z[84] = T(5) * z[8] + -z[84] + -z[121];
z[121] = z[11] + -z[68] + -z[84];
z[121] = z[121] / T(2) + -z[125];
z[121] = z[19] * z[121];
z[126] = T(3) * z[25];
z[83] = z[83] * z[126];
z[59] = (T(3) * z[59]) / T(4);
z[82] = T(3) * z[82];
z[56] = z[56] + z[59] + T(3) * z[62] + z[82] + z[83] + z[86] + -z[105] + z[121] + -z[123];
z[56] = prod_pow(z[20], 2) * z[56];
z[62] = z[69] * z[74];
z[62] = z[62] + -z[91];
z[74] = (T(3) * z[112]) / T(4);
z[79] = z[1] + T(-3) * z[79];
z[79] = T(3) * z[31] + z[79] / T(2) + z[124];
z[79] = T(3) * z[32] + z[79] / T(2);
z[79] = z[16] * z[79];
z[57] = z[49] + T(-3) * z[57];
z[57] = z[14] * z[57];
z[57] = z[57] / T(4) + z[62] + (T(-3) * z[66]) / T(4) + z[74] + z[78] + z[79] + z[83];
z[57] = z[4] * z[57];
z[66] = z[78] + -z[102];
z[73] = -z[73] + z[103];
z[78] = -(z[17] * z[73]);
z[78] = -z[66] + z[78] + z[91] + -z[108] + -z[109];
z[78] = z[78] * z[90];
z[79] = z[16] * z[92];
z[83] = (T(-3) * z[65]) / T(2) + z[79] + z[115];
z[86] = z[14] + z[120];
z[73] = z[73] * z[86];
z[86] = z[73] + z[83] + z[93] + -z[117];
z[91] = z[21] * z[86];
z[92] = z[49] + T(3) * z[52];
z[87] = z[87] * z[92];
z[92] = (T(3) * z[54]) / T(2);
z[87] = (T(3) * z[63]) / T(2) + -z[87] + z[92] + z[94];
z[93] = z[2] + -z[3];
z[93] = z[87] * z[93];
z[57] = z[57] + T(2) * z[78] + z[91] + z[93] + -z[101];
z[57] = z[4] * z[57];
z[51] = -z[51] + z[77];
z[77] = -z[14] + z[16];
z[77] = z[51] * z[77];
z[78] = z[18] * z[53];
z[91] = z[17] * z[106];
z[54] = -z[54] + -z[61] + -z[63] + -z[65] + z[77] + z[78] + z[91];
z[54] = (T(3) * z[54]) / T(2) + -z[111];
z[54] = z[6] * z[54];
z[61] = -z[63] + -z[67];
z[49] = z[49] + (T(3) * z[52]) / T(2);
z[49] = z[14] * z[49];
z[49] = z[49] + z[59] + (T(3) * z[61]) / T(4) + z[62] + -z[74] + -z[92] + -z[107];
z[49] = z[2] * z[49];
z[52] = z[14] + z[119];
z[52] = z[52] * z[99];
z[59] = -z[65] + z[113];
z[52] = z[52] + (T(3) * z[59]) / T(2) + z[79] + z[95] + -z[110];
z[52] = z[20] * z[52];
z[49] = z[49] + z[52];
z[49] = z[2] * z[49];
z[52] = T(2) * z[104];
z[52] = z[52] * z[116];
z[52] = z[52] + -z[82] + -z[83] + z[98];
z[59] = -z[3] + z[20] + z[90];
z[59] = z[52] * z[59];
z[61] = -(z[2] * z[86]);
z[62] = z[16] * z[100];
z[62] = z[62] + -z[66] + -z[73] + -z[123];
z[62] = z[21] * z[62];
z[59] = z[59] + z[61] + z[62];
z[59] = z[21] * z[59];
z[61] = T(-7) * z[8] + -z[68] + T(5) * z[76] + z[80];
z[61] = (T(-7) * z[12]) / T(2) + z[61] / T(2) + -z[89];
z[61] = -(z[61] * z[116]);
z[62] = T(-7) * z[11] + z[84] + z[118];
z[62] = z[62] / T(2) + z[125];
z[62] = -(z[62] * z[114]);
z[63] = z[12] + -z[85] + z[122];
z[63] = z[14] * z[63];
z[64] = -z[64] + -z[65];
z[61] = z[61] + z[62] + z[63] + (T(3) * z[64]) / T(2) + z[82] + z[97];
z[61] = z[26] * z[61];
z[62] = -z[14] + z[18];
z[51] = z[51] * z[62];
z[53] = z[16] * z[53];
z[58] = -(z[19] * z[58]);
z[51] = z[51] + z[53] + z[58] + -z[60] + z[65] + -z[67];
z[51] = z[5] * z[51];
z[53] = -z[19] + z[71];
z[58] = z[17] + z[53];
z[60] = z[47] * z[58];
z[51] = -z[51] + z[60];
z[55] = -(z[27] * z[55]);
z[52] = z[20] * z[52];
z[60] = z[2] * z[87];
z[52] = z[52] + z[60] + z[75];
z[52] = z[3] * z[52];
z[60] = z[29] * z[81];
z[62] = -z[70] + z[114];
z[62] = z[46] * z[62];
z[63] = prod_pow(z[44], 3);
z[53] = z[53] * z[63];
z[63] = T(-3) * z[46] + z[63];
z[63] = z[17] * z[63];
z[53] = z[53] + T(3) * z[62] + z[63];
z[62] = (T(-2) * z[69]) / T(3) + -z[72];
z[62] = prod_pow(z[41], 2) * z[62];
z[58] = -(z[45] * z[58]);
z[58] = T(3) * z[58] + z[62];
z[58] = z[41] * z[58];
z[62] = z[43] * z[88];
z[63] = z[41] * z[96];
z[62] = (T(12) * z[62]) / T(5) + z[63];
z[62] = int_to_imaginary<T>(1) * z[62];
z[63] = (T(-149) * z[69]) / T(3) + T(31) * z[71];
z[63] = z[48] * z[63];
return z[49] + z[50] + (T(-3) * z[51]) / T(2) + z[52] + T(2) * z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + T(3) * z[62] + z[63] / T(4);
}



template IntegrandConstructorType<double> f_4_26_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_26_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_26_construct (const Kin<qd_real>&);
#endif

}