#include "f_4_173.h"

namespace PentagonFunctions {

template <typename T> T f_4_173_abbreviated (const std::array<T,47>&);

template <typename T> class SpDLog_f_4_173_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_173_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (-kin.v[3] + T(-2) * kin.v[4]) + -prod_pow(kin.v[4], 2) + kin.v[1] * (-kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((-kin.v[1] / T(4) + -kin.v[4] / T(2) + kin.v[2] / T(2) + kin.v[3] / T(2) + -kin.v[0] + T(1)) * kin.v[1] + (-kin.v[3] / T(2) + (T(3) * kin.v[2]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(1)) * kin.v[2] + (-kin.v[3] / T(4) + kin.v[4] / T(2) + T(-1)) * kin.v[3] + ((T(3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(kin.v[2]) * ((-kin.v[2] / T(2) + -kin.v[3] / T(2) + kin.v[1] / T(4) + kin.v[4] / T(2) + T(-1) + kin.v[0]) * kin.v[1] + ((T(-3) * kin.v[2]) / T(4) + kin.v[3] / T(2) + (T(3) * kin.v[4]) / T(2) + T(-1)) * kin.v[2] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (-kin.v[4] / T(2) + kin.v[3] / T(4) + T(1)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(4) + T(1)) * kin.v[4]);
c[1] = rlog(kin.v[2]) * (-kin.v[3] + -kin.v[4] + kin.v[1] + kin.v[2]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[5] + abb[4] * (abb[3] + -prod_pow(abb[2], 2)) + prod_pow(abb[1], 2) * (abb[4] + -abb[5]) + -(abb[3] * abb[5]));
    }
};
template <typename T> class SpDLog_f_4_173_W_21 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_173_W_21 (const Kin<T>& kin) {
        c[0] = rlog(kin.v[2]) * (kin.v[0] * (-kin.v[0] + T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[0] * (-kin.v[0] + T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) + kin.v[0] + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(6) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) + T(3) * kin.v[1] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[3] * (T(-6) + T(3) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[1]) * (kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[3] * (T(6) + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) + T(-3) * kin.v[1] + T(6) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[3] * (T(6) + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) + T(-3) * kin.v[1] + T(6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[6] * (prod_pow(abb[2], 2) * abb[8] * T(3) + prod_pow(abb[7], 2) * (abb[4] + abb[5] + -abb[9] + abb[8] * T(-3) + abb[11] * T(-3) + abb[10] * T(3)) + prod_pow(abb[2], 2) * (abb[9] + -abb[4] + -abb[5] + abb[10] * T(-3) + abb[11] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_173_W_7 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_173_W_7 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (T(4) * kin.v[2] + T(-6) * kin.v[3] + T(-8) * kin.v[4]) + T(4) * kin.v[2] * kin.v[4] + T(-2) * prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] * kin.v[4] + kin.v[3] * (T(4) + T(4) * kin.v[1] + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(-2) * kin.v[4]));
c[1] = T(10) * prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(5) * prod_pow(kin.v[4], 2) + kin.v[2] * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[4] + T(4) * kin.v[4]) + kin.v[3] * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[4] + T(16) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + T(3) * kin.v[2] * kin.v[4] + kin.v[3] * ((T(-9) * kin.v[3]) / T(2) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + T(3) * kin.v[1] * kin.v[4] + kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(3) + T(3) * kin.v[1] + T(-3) * kin.v[4]))) + rlog(-kin.v[4]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + T(3) * kin.v[2] * kin.v[4] + kin.v[3] * ((T(-9) * kin.v[3]) / T(2) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + T(3) * kin.v[1] * kin.v[4] + kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(3) + T(3) * kin.v[1] + T(-3) * kin.v[4]))) + rlog(-kin.v[1]) * (prod_pow(kin.v[4], 2) / T(2) + -(kin.v[2] * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + -kin.v[2] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + -kin.v[1] + T(-1) + kin.v[4]))) + rlog(kin.v[3]) * (prod_pow(kin.v[4], 2) / T(2) + -(kin.v[2] * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + -kin.v[2] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + -kin.v[1] + T(-1) + kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (prod_pow(kin.v[4], 2) / T(2) + -(kin.v[2] * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + -kin.v[2] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + -kin.v[1] + T(-1) + kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (prod_pow(kin.v[4], 2) / T(2) + -(kin.v[2] * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + -kin.v[2] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + -kin.v[1] + T(-1) + kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (T(-2) * kin.v[2] * kin.v[4] + prod_pow(kin.v[4], 2) + kin.v[3] * (T(-2) * kin.v[2] + T(3) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[3] * (T(-2) + T(-2) * kin.v[1] + kin.v[3] + T(2) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(4) + T(8)) * kin.v[3] + T(8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4];
c[3] = rlog(-kin.v[1]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(kin.v[3]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4] + T(6) * kin.v[4]) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4] + T(6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[20] * (abb[8] * (abb[7] * abb[19] + -(abb[1] * abb[7]) + abb[7] * (abb[2] + -abb[7] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[21] * T(2)) + abb[1] * abb[7] * (-abb[5] + -abb[11] + -abb[14] + abb[22] * T(-4) + abb[9] * T(-2) + abb[10] * T(3) + abb[15] * T(3)) + abb[7] * abb[19] * (abb[5] + abb[11] + abb[14] + abb[10] * T(-3) + abb[15] * T(-3) + abb[9] * T(2) + abb[22] * T(4)) + abb[7] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[14] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[10] * bc<T>[0] * int_to_imaginary<T>(3) + abb[15] * bc<T>[0] * int_to_imaginary<T>(3) + abb[22] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[7] * (-abb[5] + -abb[11] + -abb[14] + abb[22] * T(-4) + abb[9] * T(-2) + abb[10] * T(3) + abb[15] * T(3)) + abb[2] * (abb[5] + abb[11] + abb[14] + abb[10] * T(-3) + abb[15] * T(-3) + abb[9] * T(2) + abb[22] * T(4))) + abb[21] * (abb[10] * T(-6) + abb[15] * T(-6) + abb[5] * T(2) + abb[11] * T(2) + abb[14] * T(2) + abb[9] * T(4) + abb[22] * T(8)) + abb[18] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(1) + abb[14] * bc<T>[0] * int_to_imaginary<T>(1) + abb[8] * (abb[1] + -abb[2] + -abb[7] + -abb[19] + bc<T>[0] * int_to_imaginary<T>(1)) + abb[9] * bc<T>[0] * int_to_imaginary<T>(2) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[15] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[22] * bc<T>[0] * int_to_imaginary<T>(4) + abb[2] * (-abb[5] + -abb[11] + -abb[14] + abb[22] * T(-4) + abb[9] * T(-2) + abb[10] * T(3) + abb[15] * T(3)) + abb[7] * (-abb[5] + -abb[11] + -abb[14] + abb[22] * T(-4) + abb[9] * T(-2) + abb[10] * T(3) + abb[15] * T(3)) + abb[19] * (-abb[5] + -abb[11] + -abb[14] + abb[22] * T(-4) + abb[9] * T(-2) + abb[10] * T(3) + abb[15] * T(3)) + abb[1] * (abb[5] + abb[11] + abb[14] + abb[10] * T(-3) + abb[15] * T(-3) + abb[9] * T(2) + abb[22] * T(4)) + abb[18] * (abb[10] * T(-6) + abb[15] * T(-6) + abb[5] * T(2) + abb[8] * T(2) + abb[11] * T(2) + abb[14] * T(2) + abb[9] * T(4) + abb[22] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_173_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl21 = DLog_W_21<T>(kin),dl3 = DLog_W_3<T>(kin),dl14 = DLog_W_14<T>(kin),dl7 = DLog_W_7<T>(kin),dl17 = DLog_W_17<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl9 = DLog_W_9<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),dl2 = DLog_W_2<T>(kin),dl20 = DLog_W_20<T>(kin),dl16 = DLog_W_16<T>(kin),dl19 = DLog_W_19<T>(kin),dl4 = DLog_W_4<T>(kin),dl5 = DLog_W_5<T>(kin),dl18 = DLog_W_18<T>(kin),dl1 = DLog_W_1<T>(kin),spdl22 = SpDLog_f_4_173_W_22<T>(kin),spdl21 = SpDLog_f_4_173_W_21<T>(kin),spdl7 = SpDLog_f_4_173_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,47> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[18] / kin_path.W[18]), dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[19] / kin_path.W[19]), dl3(t), f_2_1_15(kin_path), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[4] / kin_path.W[4]), dl14(t), rlog(kin.W[0] / kin_path.W[0]), rlog(-v_path[1]), rlog(v_path[3]), dl7(t), f_2_1_11(kin_path), -rlog(t), dl17(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl26(t) / kin_path.SqrtDelta, f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl30(t) / kin_path.SqrtDelta, f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl9(t), dl27(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl2(t), rlog(kin.W[13] / kin_path.W[13]), dl20(t), -rlog(t), dl16(t), dl19(t), dl4(t), dl5(t), dl18(t), dl1(t)}
;

        auto result = f_4_173_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_173_abbreviated(const std::array<T,47>& abb)
{
using TR = typename T::value_type;
T z[129];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[12];
z[3] = abb[41];
z[4] = abb[5];
z[5] = abb[23];
z[6] = abb[37];
z[7] = abb[39];
z[8] = abb[42];
z[9] = abb[43];
z[10] = abb[8];
z[11] = abb[33];
z[12] = abb[9];
z[13] = abb[10];
z[14] = abb[11];
z[15] = abb[14];
z[16] = abb[15];
z[17] = abb[17];
z[18] = abb[22];
z[19] = abb[26];
z[20] = abb[27];
z[21] = abb[28];
z[22] = abb[29];
z[23] = abb[30];
z[24] = abb[35];
z[25] = abb[36];
z[26] = abb[2];
z[27] = abb[7];
z[28] = abb[18];
z[29] = abb[46];
z[30] = abb[19];
z[31] = bc<TR>[0];
z[32] = abb[40];
z[33] = abb[45];
z[34] = abb[34];
z[35] = abb[3];
z[36] = abb[13];
z[37] = abb[44];
z[38] = abb[16];
z[39] = abb[21];
z[40] = abb[24];
z[41] = abb[25];
z[42] = abb[31];
z[43] = abb[32];
z[44] = abb[38];
z[45] = bc<TR>[3];
z[46] = bc<TR>[1];
z[47] = bc<TR>[2];
z[48] = bc<TR>[4];
z[49] = bc<TR>[9];
z[50] = z[14] / T(2);
z[51] = z[10] / T(2);
z[52] = z[50] + z[51];
z[53] = z[13] + z[16];
z[54] = T(3) * z[53];
z[55] = -z[15] + z[54];
z[56] = z[55] / T(2);
z[57] = T(2) * z[18];
z[58] = z[12] + z[57];
z[59] = z[4] / T(2);
z[60] = z[52] + -z[56] + z[58] + z[59];
z[61] = z[27] * z[60];
z[62] = z[4] + z[15];
z[63] = z[14] + -z[17] + z[62];
z[64] = -z[57] + z[63];
z[65] = z[0] * z[64];
z[65] = -z[61] + z[65];
z[66] = (T(3) * z[4]) / T(2);
z[67] = (T(3) * z[14]) / T(2);
z[68] = z[66] + z[67];
z[69] = z[15] + z[53];
z[69] = T(-6) * z[18] + (T(3) * z[69]) / T(2);
z[51] = z[12] + z[51];
z[70] = T(-2) * z[17] + -z[51] + z[68] + z[69];
z[71] = z[43] * z[70];
z[72] = -z[15] + z[53];
z[73] = (T(3) * z[72]) / T(2);
z[68] = -z[17] + z[51] + z[68] + -z[73];
z[74] = z[41] * z[68];
z[75] = T(2) * z[64];
z[76] = z[28] * z[75];
z[75] = -(z[30] * z[75]);
z[77] = prod_pow(z[46], 2);
z[78] = z[48] + z[77] / T(2);
z[79] = z[47] / T(2);
z[80] = -z[46] + z[79];
z[80] = z[47] * z[80];
z[71] = -z[65] + z[71] + z[74] + z[75] + -z[76] + z[78] + z[80];
z[74] = int_to_imaginary<T>(1) * z[31];
z[71] = z[71] * z[74];
z[75] = z[10] + -z[13] + z[16];
z[80] = T(5) * z[15];
z[75] = z[14] + T(3) * z[75] + -z[80];
z[81] = T(2) * z[44];
z[82] = (T(5) * z[4]) / T(2);
z[75] = -z[17] + z[57] + z[75] / T(2) + z[81] + -z[82];
z[75] = z[28] * z[75];
z[75] = z[61] + z[75];
z[75] = z[28] * z[75];
z[83] = z[15] + z[44];
z[84] = (T(2) * z[10]) / T(3);
z[85] = z[18] / T(3);
z[86] = z[12] / T(3);
z[87] = z[4] / T(6);
z[83] = -z[13] + (T(5) * z[14]) / T(6) + -z[46] / T(3) + z[47] / T(4) + -z[83] / T(6) + z[84] + z[85] + z[86] + -z[87];
z[88] = prod_pow(z[31], 2);
z[83] = z[83] * z[88];
z[89] = z[28] * z[60];
z[90] = z[50] + z[59];
z[91] = z[15] + z[54];
z[91] = z[91] / T(2);
z[92] = -z[17] + -z[51] + z[90] + z[91];
z[92] = -z[57] + z[92] / T(2);
z[92] = z[30] * z[92];
z[65] = z[65] + z[89] + z[92];
z[65] = z[30] * z[65];
z[89] = z[10] + z[14];
z[92] = T(3) * z[13];
z[93] = z[58] + -z[62] + T(2) * z[89] + -z[92];
z[94] = -z[27] + z[28];
z[95] = -z[26] + T(-2) * z[94];
z[95] = z[93] * z[95];
z[96] = -z[0] + z[30] + -z[74];
z[97] = z[60] * z[96];
z[95] = z[95] + z[97];
z[95] = z[26] * z[95];
z[92] = -z[16] + z[92];
z[92] = z[80] + T(3) * z[92];
z[82] = -z[58] + z[82] + (T(-7) * z[89]) / T(2) + z[92] / T(2);
z[92] = -(z[39] * z[82]);
z[63] = z[18] + -z[63] / T(2);
z[63] = z[0] * z[63];
z[61] = z[61] + z[63] + -z[76];
z[61] = z[0] * z[61];
z[63] = -(z[40] * z[68]);
z[51] = -z[51] + z[56] + -z[90];
z[51] = -z[18] + z[51] / T(2);
z[76] = prod_pow(z[27], 2);
z[97] = z[51] * z[76];
z[70] = z[42] * z[70];
z[98] = (T(3) * z[10]) / T(2);
z[67] = z[67] + z[98];
z[99] = T(5) * z[13] + z[15] + z[16];
z[99] = -z[67] + z[99] / T(2);
z[100] = z[59] + z[99];
z[101] = -z[58] + z[100];
z[102] = T(3) * z[101];
z[103] = z[36] * z[102];
z[61] = (T(-35) * z[49]) / T(24) + z[61] + z[63] + z[65] + z[70] + z[71] + z[75] + z[83] + z[92] + z[95] + z[97] + z[103];
z[61] = z[6] * z[61];
z[63] = z[1] / T(3);
z[65] = z[52] + -z[63] + -z[72] / T(2) + z[86] + z[87];
z[65] = z[2] * z[65];
z[70] = z[15] + -z[16];
z[71] = z[13] + -z[14] + z[70];
z[75] = z[4] + -z[10];
z[83] = z[71] + -z[75];
z[92] = z[5] * z[83];
z[95] = z[20] + z[21] + z[22];
z[97] = z[19] * z[95];
z[92] = -z[92] + z[97];
z[97] = z[34] * z[95];
z[103] = z[92] + -z[97];
z[104] = z[24] * z[95];
z[105] = (T(3) * z[104]) / T(2);
z[106] = z[25] * z[95];
z[95] = z[23] * z[95];
z[65] = z[65] + z[95] + z[103] / T(2) + z[105] + z[106];
z[103] = (T(2) * z[12]) / T(3);
z[107] = T(5) * z[16];
z[108] = -z[13] + z[107];
z[63] = (T(-11) * z[4]) / T(6) + (T(2) * z[14]) / T(3) + (T(-7) * z[15]) / T(3) + z[32] / T(6) + -z[46] + z[63] + z[84] + -z[85] + -z[103] + z[108] / T(2);
z[63] = z[8] * z[63];
z[84] = (T(2) * z[46]) / T(3);
z[85] = z[10] / T(3);
z[72] = -z[4] + -z[14] + (T(2) * z[17]) / T(3) + z[72] + z[84] + -z[85] + -z[103];
z[72] = z[29] * z[72];
z[86] = z[70] + -z[85] + z[86];
z[84] = z[1] / T(6) + z[17] / T(3) + -z[84] + z[86] / T(2) + -z[87];
z[84] = z[3] * z[84];
z[86] = -z[12] + z[99];
z[87] = z[18] + -z[46];
z[99] = z[4] / T(4);
z[86] = z[86] / T(2) + -z[87] + z[99];
z[86] = z[33] * z[86];
z[50] = z[10] / T(6) + z[12] + -z[32] / T(3) + z[50] + -z[56];
z[50] = (T(7) * z[4]) / T(12) + z[50] / T(2) + z[87];
z[50] = z[7] * z[50];
z[56] = -z[71] + z[85];
z[56] = (T(-7) * z[4]) / T(3) + (T(13) * z[44]) / T(6) + -z[46] + z[56] / T(2);
z[56] = z[9] * z[56];
z[85] = -z[9] + z[37];
z[87] = T(3) * z[8];
z[103] = z[3] + (T(-9) * z[29]) / T(4) + T(-3) * z[85] + z[87];
z[103] = z[47] * z[103];
z[70] = z[4] + z[70];
z[70] = -z[10] + z[12] + -z[17] + T(3) * z[70];
z[70] = z[38] * z[70];
z[108] = -z[10] + z[17];
z[108] = z[11] * z[108];
z[109] = z[37] * z[46];
z[50] = z[50] + z[56] + z[63] + z[65] / T(2) + z[70] + z[72] + z[84] + z[86] + z[103] + (T(-4) * z[108]) / T(3) + z[109];
z[50] = z[31] * z[50];
z[56] = z[24] + z[25] + z[34];
z[63] = z[45] * z[56];
z[50] = z[50] + T(-3) * z[63];
z[50] = z[31] * z[50];
z[63] = T(3) * z[16];
z[62] = -z[58] + T(-2) * z[62] + z[63] + z[89];
z[65] = T(2) * z[62];
z[72] = -z[8] + -z[9];
z[65] = z[65] * z[72];
z[72] = z[13] + T(-3) * z[15] + z[107];
z[72] = -z[52] + z[58] + z[66] + -z[72] / T(2);
z[84] = z[37] * z[72];
z[86] = T(3) * z[84];
z[103] = z[3] * z[60];
z[107] = z[7] * z[60];
z[109] = z[95] + z[104];
z[65] = z[65] + -z[86] + z[103] + -z[107] + (T(-3) * z[109]) / T(2);
z[110] = z[27] * z[65];
z[111] = z[8] * z[60];
z[112] = T(3) * z[71];
z[113] = -z[75] + z[112];
z[113] = z[9] * z[113];
z[114] = z[75] + z[112];
z[115] = z[7] * z[114];
z[113] = z[113] + -z[115];
z[116] = z[104] + z[106];
z[117] = -z[113] + T(3) * z[116];
z[118] = z[29] * z[68];
z[119] = z[108] + z[118];
z[120] = z[4] + -z[17];
z[120] = z[3] * z[120];
z[117] = z[111] + z[117] / T(2) + -z[119] + z[120];
z[117] = z[0] * z[117];
z[117] = z[110] + z[117];
z[60] = z[9] * z[60];
z[121] = z[60] + z[111];
z[64] = z[3] * z[64];
z[64] = z[64] + z[107] + (T(3) * z[116]) / T(2) + -z[118] + z[121];
z[64] = z[28] * z[64];
z[122] = z[9] * z[72];
z[95] = -z[95] + z[106];
z[84] = -z[84] + z[95] / T(2) + z[122];
z[72] = T(3) * z[72];
z[72] = z[8] * z[72];
z[68] = z[3] * z[68];
z[68] = z[68] + z[72] + T(3) * z[84] + -z[118];
z[72] = z[43] * z[68];
z[84] = z[8] * z[62];
z[106] = z[70] + z[84];
z[122] = T(2) * z[9];
z[123] = -z[4] + z[44];
z[123] = z[122] * z[123];
z[124] = z[7] * z[75];
z[120] = z[106] + -z[108] + -z[120] + z[123] + z[124];
z[120] = z[30] * z[120];
z[123] = z[92] + z[109];
z[124] = z[7] * z[83];
z[125] = z[71] + z[75];
z[126] = z[9] * z[125];
z[124] = z[123] + z[124] + -z[126];
z[112] = -z[10] + z[112];
z[112] = z[112] / T(2);
z[66] = -z[17] + z[66] + z[112];
z[66] = z[3] * z[66];
z[66] = z[66] + -z[119] + (T(3) * z[124]) / T(2);
z[119] = z[41] * z[66];
z[124] = -z[7] + z[85];
z[126] = T(3) * z[124];
z[87] = T(2) * z[3] + z[87];
z[127] = T(3) * z[33];
z[128] = T(2) * z[29] + -z[87] + z[126] + z[127];
z[128] = z[46] * z[128];
z[79] = -(z[29] * z[79]);
z[79] = z[79] + z[128];
z[79] = z[47] * z[79];
z[128] = -z[7] + z[33];
z[56] = -z[3] / T(3) + z[29] / T(2) + (T(-5) * z[38]) / T(6) + z[56] / T(9) + (T(3) * z[85]) / T(4) + z[128] / T(4);
z[56] = z[56] * z[88];
z[85] = z[3] + (T(3) * z[8]) / T(2) + (T(-3) * z[124]) / T(2);
z[85] = z[77] * z[85];
z[87] = z[87] + -z[126];
z[87] = z[48] * z[87];
z[78] = -(z[78] * z[127]);
z[77] = T(-2) * z[48] + -z[77];
z[77] = z[29] * z[77];
z[56] = z[56] + -z[64] + z[72] + z[77] + z[78] + z[79] + z[85] + z[87] + -z[117] + z[119] + T(2) * z[120];
z[56] = z[56] * z[74];
z[62] = z[9] * z[62];
z[72] = z[7] * z[51];
z[62] = z[62] + -z[72];
z[72] = z[118] / T(2);
z[53] = z[15] + T(-9) * z[53];
z[53] = T(3) * z[12] + z[17] + z[53] / T(2) + z[90] + z[98];
z[74] = T(4) * z[18];
z[53] = z[53] / T(2) + z[74];
z[53] = z[3] * z[53];
z[77] = -z[12] + z[100];
z[77] = -z[18] + z[77] / T(2);
z[77] = z[77] * z[127];
z[78] = -z[97] + -z[109];
z[53] = z[53] + -z[62] + z[70] + -z[72] + z[77] + (T(3) * z[78]) / T(4) + -z[84] + -z[86];
z[53] = z[28] * z[53];
z[53] = z[53] + -z[110];
z[53] = z[28] * z[53];
z[65] = -(z[28] * z[65]);
z[70] = -z[115] + T(-3) * z[123];
z[71] = -z[10] + z[71];
z[71] = T(3) * z[71];
z[78] = T(11) * z[4] + z[71];
z[78] = z[78] / T(4) + -z[81];
z[78] = z[9] * z[78];
z[79] = -z[17] + z[59] + -z[112];
z[81] = z[3] / T(2);
z[79] = z[79] * z[81];
z[70] = z[70] / T(4) + z[72] + z[78] + z[79] + -z[106] + (T(3) * z[108]) / T(2);
z[70] = z[30] * z[70];
z[65] = z[65] + z[70] + z[117];
z[65] = z[30] * z[65];
z[59] = z[12] + z[59];
z[52] = -z[1] + z[52] + -z[59] + z[91];
z[52] = z[52] / T(2) + -z[57];
z[52] = z[3] * z[52];
z[70] = z[95] + z[97];
z[72] = z[70] + -z[104];
z[54] = -z[54] + z[80];
z[78] = (T(5) * z[89]) / T(2);
z[54] = z[12] + z[54] / T(2) + z[78];
z[54] = z[1] + -z[18] + T(-2) * z[32] + z[54] / T(2) + z[99];
z[54] = z[8] * z[54];
z[52] = z[52] + z[54] + -z[62] + (T(3) * z[72]) / T(4) + -z[77];
z[52] = z[52] * z[76];
z[54] = -z[1] + z[15] + -z[57] + z[89];
z[57] = z[3] + T(2) * z[8];
z[57] = z[54] * z[57];
z[57] = z[57] + z[60] + z[105] + z[107];
z[57] = z[27] * z[57];
z[60] = z[92] + -z[95];
z[62] = z[60] + -z[104];
z[77] = T(3) * z[62] + z[113];
z[71] = z[4] + z[71];
z[71] = z[1] + z[71] / T(2);
z[79] = z[3] * z[71];
z[77] = z[77] / T(2) + z[79] + -z[108];
z[51] = z[8] * z[51];
z[51] = z[51] + z[77] / T(2);
z[51] = z[0] * z[51];
z[51] = z[51] + z[57] + -z[64];
z[51] = z[0] * z[51];
z[57] = z[42] * z[68];
z[64] = T(2) * z[7];
z[68] = z[64] * z[93];
z[77] = z[101] * z[127];
z[68] = -z[68] + (T(3) * z[70]) / T(2) + -z[77] + -z[103] + z[121];
z[68] = z[68] * z[94];
z[70] = -z[64] + -z[122];
z[70] = z[70] * z[75];
z[79] = z[81] * z[114];
z[60] = (T(3) * z[60]) / T(2) + z[70] + z[79] + -z[111];
z[60] = z[60] * z[96];
z[70] = -z[10] + z[32];
z[64] = z[64] * z[70];
z[54] = -(z[8] * z[54]);
z[70] = -(z[9] * z[75]);
z[75] = z[1] + -z[10];
z[75] = z[3] * z[75];
z[54] = z[54] + z[64] + z[70] + z[75];
z[54] = z[26] * z[54];
z[54] = z[54] + z[60] + z[68];
z[54] = z[26] * z[54];
z[60] = -(z[7] * z[82]);
z[63] = -z[13] + z[63];
z[63] = T(-7) * z[15] + T(3) * z[63];
z[58] = (T(-7) * z[4]) / T(2) + -z[58] + z[63] / T(2) + z[78];
z[63] = -(z[9] * z[58]);
z[64] = -z[97] + -z[116];
z[60] = z[60] + z[63] + (T(3) * z[64]) / T(2) + -z[86];
z[60] = z[39] * z[60];
z[63] = z[7] * z[101];
z[63] = z[63] + z[72] / T(2);
z[64] = -z[1] + z[59] + z[67] + -z[73];
z[68] = -(z[3] * z[64]);
z[59] = T(2) * z[1] + z[59] + -z[67] + -z[69];
z[59] = z[8] * z[59];
z[59] = z[59] + T(3) * z[63] + z[68] + -z[77];
z[59] = z[36] * z[59];
z[63] = -(z[35] * z[64]);
z[58] = -(z[39] * z[58]);
z[58] = (T(-7) * z[49]) / T(2) + z[58] + z[63];
z[58] = z[8] * z[58];
z[63] = -(z[40] * z[66]);
z[66] = -(z[7] * z[125]);
z[62] = z[62] + z[66];
z[62] = z[35] * z[62];
z[66] = z[7] * z[49];
z[62] = T(3) * z[62] + T(-5) * z[66];
z[55] = z[4] + T(2) * z[12] + -z[55] + z[74] + z[89];
z[55] = z[39] * z[55];
z[66] = z[35] * z[71];
z[55] = (T(-7) * z[49]) / T(6) + z[55] + z[66];
z[55] = z[3] * z[55];
z[66] = z[0] / T(2) + -z[27];
z[66] = z[0] * z[66];
z[66] = z[35] + z[36] + z[66] + z[76] / T(2);
z[64] = z[2] * z[64] * z[66];
z[66] = z[35] * z[83];
z[66] = -z[49] + (T(3) * z[66]) / T(2);
z[66] = z[9] * z[66];
z[67] = z[39] * z[102];
z[67] = (T(5) * z[49]) / T(2) + z[67];
z[67] = z[33] * z[67];
z[68] = (T(91) * z[29]) / T(24) + z[37];
z[68] = z[49] * z[68];
return z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] / T(2) + z[63] + z[64] + z[65] + z[66] + z[67] + z[68];
}



template IntegrandConstructorType<double> f_4_173_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_173_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_173_construct (const Kin<qd_real>&);
#endif

}