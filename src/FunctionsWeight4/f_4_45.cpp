#include "f_4_45.h"

namespace PentagonFunctions {

template <typename T> T f_4_45_abbreviated (const std::array<T,43>&);

template <typename T> class SpDLog_f_4_45_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_45_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(12) + T(6) * kin.v[1]) + kin.v[4] * (T(-12) + T(-6) * kin.v[4]);
c[1] = kin.v[1] * (T(-12) + T(-6) * kin.v[1]) + kin.v[4] * (T(12) + T(6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-6) + prod_pow(abb[2], 2) * abb[4] * T(6) + prod_pow(abb[1], 2) * (abb[4] * T(-6) + abb[3] * T(6)));
    }
};
template <typename T> class SpDLog_f_4_45_W_7 {
    using TC = std::complex<T>;
    std::array<TC,3> c;

public:
    SpDLog_f_4_45_W_7 (const Kin<T>& kin) {
        c[0] = T(-12) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[3] * (T(-12) + T(-12) * kin.v[1] + T(6) * kin.v[3] + T(12) * kin.v[4]);
c[1] = T(-12) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[3] * (T(-12) + T(-12) * kin.v[1] + T(6) * kin.v[3] + T(12) * kin.v[4]);
c[2] = T(-12) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[3] * (T(-12) + T(-12) * kin.v[1] + T(6) * kin.v[3] + T(12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[7] * c[0] + abb[4] * c[1] + abb[8] * c[2]);
        }

        return abb[5] * (prod_pow(abb[1], 2) * (abb[4] * T(-6) + abb[7] * T(-6) + abb[8] * T(-6)) + prod_pow(abb[6], 2) * abb[7] * T(6) + prod_pow(abb[6], 2) * (abb[4] * T(6) + abb[8] * T(6)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_45_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl7 = DLog_W_7<T>(kin),dl13 = DLog_W_13<T>(kin),dl6 = DLog_W_6<T>(kin),dl15 = DLog_W_15<T>(kin),dl25 = DLog_W_25<T>(kin),dl3 = DLog_W_3<T>(kin),dl5 = DLog_W_5<T>(kin),dl18 = DLog_W_18<T>(kin),dl19 = DLog_W_19<T>(kin),dl2 = DLog_W_2<T>(kin),dl1 = DLog_W_1<T>(kin),dl4 = DLog_W_4<T>(kin),dl20 = DLog_W_20<T>(kin),dl17 = DLog_W_17<T>(kin),dl31 = DLog_W_31<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),dl27 = DLog_W_27<T>(kin),dl26 = DLog_W_26<T>(kin),spdl12 = SpDLog_f_4_45_W_12<T>(kin),spdl7 = SpDLog_f_4_45_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,43> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl7(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl13(t), rlog(v_path[0]), rlog(v_path[2]), dl6(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl15(t), dl25(t), dl3(t), f_2_1_1(kin_path), f_2_1_9(kin_path), f_2_1_4(kin_path), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl5(t), f_2_1_11(kin_path), dl18(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl19(t), dl2(t), dl1(t), dl4(t), f_2_2_1(kin_path), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[30] / kin_path.W[30]), dl20(t), dl17(t), dl31(t), dl29(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta}
;

        auto result = f_4_45_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_45_abbreviated(const std::array<T,43>& abb)
{
using TR = typename T::value_type;
T z[107];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[22];
z[3] = abb[27];
z[4] = abb[28];
z[5] = abb[30];
z[6] = abb[36];
z[7] = abb[4];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[37];
z[11] = abb[32];
z[12] = abb[39];
z[13] = abb[40];
z[14] = abb[42];
z[15] = abb[33];
z[16] = abb[34];
z[17] = abb[35];
z[18] = abb[2];
z[19] = abb[6];
z[20] = abb[13];
z[21] = abb[24];
z[22] = bc<TR>[0];
z[23] = abb[11];
z[24] = abb[16];
z[25] = abb[41];
z[26] = abb[14];
z[27] = abb[29];
z[28] = abb[10];
z[29] = abb[15];
z[30] = abb[12];
z[31] = abb[17];
z[32] = abb[18];
z[33] = abb[19];
z[34] = abb[20];
z[35] = abb[21];
z[36] = abb[23];
z[37] = abb[25];
z[38] = abb[26];
z[39] = abb[9];
z[40] = abb[31];
z[41] = bc<TR>[1];
z[42] = bc<TR>[3];
z[43] = bc<TR>[5];
z[44] = abb[38];
z[45] = bc<TR>[2];
z[46] = bc<TR>[4];
z[47] = bc<TR>[7];
z[48] = bc<TR>[8];
z[49] = bc<TR>[9];
z[50] = z[9] * z[10];
z[51] = z[1] + z[8];
z[52] = z[6] * z[51];
z[53] = z[50] + -z[52];
z[54] = -z[7] + -z[8];
z[54] = z[5] * z[54];
z[54] = z[53] + z[54];
z[55] = z[1] + -z[9];
z[56] = z[8] + z[55];
z[57] = -z[7] + z[56];
z[57] = z[29] * z[57];
z[58] = z[26] * z[55];
z[59] = z[57] + z[58];
z[60] = -z[12] + z[14];
z[61] = z[11] + z[15] + z[16] + T(-2) * z[17];
z[62] = T(2) * z[61];
z[63] = T(15) * z[45] + -z[62];
z[63] = z[60] * z[63];
z[64] = T(4) * z[9];
z[65] = z[7] / T(2);
z[66] = (T(-9) * z[8]) / T(2) + z[64] + -z[65];
z[66] = z[4] * z[66];
z[67] = T(2) * z[1];
z[64] = (T(17) * z[8]) / T(2) + -z[64] + z[67];
z[64] = z[24] * z[64];
z[65] = (T(-13) * z[8]) / T(2) + T(-6) * z[55] + -z[65];
z[65] = z[27] * z[65];
z[68] = T(4) * z[25];
z[69] = T(2) * z[45] + -z[61];
z[69] = z[68] * z[69];
z[70] = z[13] + T(-13) * z[25];
z[70] = T(-7) * z[60] + z[70] / T(2);
z[70] = z[41] * z[70];
z[71] = z[1] * z[4];
z[72] = T(2) * z[8] + -z[9];
z[72] = T(4) * z[1] + (T(13) * z[7]) / T(2) + T(2) * z[72];
z[72] = z[21] * z[72];
z[67] = T(2) * z[9] + -z[67];
z[73] = T(2) * z[7] + z[8] / T(2) + z[67];
z[73] = z[3] * z[73];
z[62] = T(-7) * z[45] + -z[62];
z[62] = z[13] * z[62];
z[67] = (T(5) * z[7]) / T(2) + z[67];
z[67] = z[2] * z[67];
z[54] = T(2) * z[54] + T(6) * z[59] + z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + z[69] + z[70] + T(-4) * z[71] + z[72] + z[73];
z[54] = z[22] * z[54];
z[59] = z[5] + z[10];
z[62] = T(2) * z[44];
z[63] = -z[6] + -z[59] + z[62];
z[64] = z[42] * z[63];
z[54] = z[54] + T(-16) * z[64];
z[54] = z[22] * z[54];
z[65] = z[24] * z[56];
z[66] = z[21] * z[56];
z[67] = z[65] + z[66];
z[69] = z[5] * z[56];
z[70] = z[67] + -z[69];
z[72] = z[7] + z[9];
z[73] = z[3] * z[72];
z[74] = z[27] * z[72];
z[73] = z[73] + z[74];
z[75] = z[10] * z[72];
z[76] = z[73] + -z[75];
z[77] = z[13] * z[61];
z[78] = z[76] + -z[77];
z[79] = z[25] * z[61];
z[80] = z[14] * z[61];
z[81] = z[79] + z[80];
z[82] = -z[70] + -z[78] + z[81];
z[82] = z[32] * z[82];
z[55] = -z[7] + z[55];
z[83] = z[5] * z[55];
z[84] = z[2] * z[55];
z[85] = z[83] + -z[84];
z[55] = z[3] * z[55];
z[86] = -z[55] + z[85];
z[87] = z[4] * z[9];
z[88] = z[9] * z[24];
z[89] = z[87] + z[88];
z[90] = -z[50] + z[89];
z[91] = z[77] + -z[80] + -z[86] + z[90];
z[91] = z[33] * z[91];
z[92] = z[1] * z[2];
z[93] = z[1] * z[27];
z[92] = z[92] + z[93];
z[94] = z[1] * z[6];
z[95] = z[92] + -z[94];
z[96] = z[12] * z[61];
z[97] = -z[80] + z[96];
z[98] = -z[90] + z[95] + z[97];
z[99] = -(z[34] * z[98]);
z[100] = -z[52] + z[97];
z[101] = z[21] * z[51];
z[101] = z[71] + z[101];
z[102] = -z[79] + z[101];
z[103] = z[4] * z[8];
z[104] = z[102] + z[103];
z[78] = z[78] + -z[100] + -z[104];
z[105] = z[37] * z[78];
z[106] = z[77] + z[96];
z[86] = z[52] + -z[86] + -z[104] + z[106];
z[86] = z[36] * z[86];
z[70] = -z[70] + z[79] + z[95] + -z[96];
z[70] = z[31] * z[70];
z[59] = -z[6] + -z[59] + z[62];
z[59] = z[40] * z[59] * z[61];
z[59] = z[59] + z[70] + z[82] + z[86] + z[91] + z[99] + z[105];
z[61] = -z[8] + z[9];
z[62] = z[4] * z[61];
z[62] = -z[53] + z[62] + z[88] + -z[97] + -z[102];
z[70] = int_to_imaginary<T>(1) * z[22];
z[62] = z[62] * z[70];
z[52] = -z[52] + -z[81] + z[101] + z[103];
z[52] = z[20] * z[52];
z[55] = z[55] + z[77];
z[81] = z[55] + z[97];
z[82] = z[81] + -z[85];
z[85] = z[19] * z[82];
z[82] = -(z[18] * z[82]);
z[86] = -z[90] + z[96];
z[86] = z[23] * z[86];
z[52] = z[52] + z[62] + z[82] + -z[85] + z[86];
z[62] = z[4] * z[72];
z[53] = -z[53] + z[62] + -z[71] + z[81] + T(-2) * z[83] + z[84];
z[53] = z[0] * z[53];
z[52] = T(2) * z[52] + z[53];
z[52] = z[0] * z[52];
z[53] = z[76] + -z[106];
z[53] = z[19] * z[53];
z[62] = z[67] + -z[79] + z[97];
z[67] = z[62] + -z[69];
z[72] = z[28] * z[67];
z[67] = -(z[23] * z[67]);
z[67] = -z[53] + z[67] + z[72];
z[65] = z[57] + z[65];
z[51] = z[7] + z[51];
z[76] = z[30] * z[51];
z[75] = -z[75] + z[76];
z[51] = -(z[21] * z[51]);
z[51] = z[51] + -z[65] + z[75] + z[79] + -z[100];
z[51] = z[20] * z[51];
z[51] = z[51] + T(2) * z[67];
z[51] = z[20] * z[51];
z[67] = -(z[3] * z[8]);
z[57] = z[57] + z[67] + z[74] + z[84] + -z[104];
z[57] = prod_pow(z[19], 2) * z[57];
z[66] = z[58] + z[66] + -z[79];
z[67] = z[39] * z[61];
z[74] = z[67] + -z[94];
z[61] = z[24] * z[61];
z[50] = -z[50] + z[61] + -z[66] + -z[74] + -z[97];
z[50] = z[23] * z[50];
z[50] = z[50] + T(2) * z[72];
z[50] = z[23] * z[50];
z[61] = -(z[2] * z[7]);
z[55] = z[55] + z[58] + z[61] + z[89] + -z[93];
z[55] = z[18] * z[55];
z[58] = -z[80] + z[95];
z[61] = z[23] + -z[28];
z[58] = z[58] * z[61];
z[58] = z[58] + z[85];
z[55] = z[55] + T(2) * z[58];
z[55] = z[18] * z[55];
z[56] = z[27] * z[56];
z[56] = -z[56] + -z[74] + z[75];
z[58] = -z[56] + -z[62] + T(2) * z[69];
z[58] = z[28] * z[58];
z[53] = T(2) * z[53] + z[58];
z[53] = z[28] * z[53];
z[58] = z[25] + z[60];
z[61] = prod_pow(z[41], 3) * z[58];
z[50] = z[50] + z[51] + z[52] + z[53] + z[55] + z[57] + -z[61];
z[51] = z[7] * z[21];
z[51] = z[51] + z[65] + -z[71] + z[73] + -z[76] + -z[77] + -z[103];
z[51] = z[20] * z[51];
z[52] = z[8] * z[24];
z[52] = z[52] + z[66] + z[67] + z[87] + -z[92];
z[52] = z[23] * z[52];
z[53] = z[35] * z[98];
z[55] = -(z[38] * z[78]);
z[56] = z[56] + -z[62];
z[56] = z[28] * z[56];
z[51] = z[51] + z[52] + z[53] + z[55] + z[56];
z[52] = T(3) * z[25];
z[53] = T(-3) * z[60];
z[55] = T(6) * z[13] + z[52] + z[53] + T(-2) * z[63];
z[55] = z[41] * z[55];
z[56] = -z[13] + z[60];
z[56] = z[45] * z[56];
z[55] = z[55] + T(6) * z[56];
z[55] = z[41] * z[55];
z[51] = T(6) * z[51] + z[55];
z[52] = T(-3) * z[13] + -z[52] + T(-4) * z[63];
z[52] = prod_pow(z[22], 2) * z[52];
z[51] = T(2) * z[51] + z[52];
z[51] = z[22] * z[51];
z[52] = z[43] * z[63];
z[55] = z[41] * z[64];
z[52] = T(4) * z[52] + z[55];
z[51] = z[51] + T(48) * z[52];
z[51] = int_to_imaginary<T>(1) * z[51];
z[52] = -z[13] + T(2) * z[25];
z[53] = -z[52] + z[53];
z[55] = T(12) * z[47] + T(3) * z[48];
z[53] = z[53] * z[55];
z[55] = prod_pow(z[45], 3);
z[56] = (T(91) * z[49]) / T(2) + T(12) * z[55];
z[56] = z[56] * z[60];
z[52] = z[52] * z[55];
z[55] = z[13] + z[25];
z[55] = z[55] * z[70];
z[57] = -(z[41] * z[58]);
z[55] = z[55] + z[57];
z[55] = z[46] * z[55];
z[57] = (T(-5) * z[13]) / T(2) + z[68];
z[57] = z[49] * z[57];
return T(6) * z[50] + z[51] + T(4) * z[52] + z[53] + z[54] + T(24) * z[55] + z[56] + T(7) * z[57] + T(12) * z[59];
}



template IntegrandConstructorType<double> f_4_45_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_45_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_45_construct (const Kin<qd_real>&);
#endif

}