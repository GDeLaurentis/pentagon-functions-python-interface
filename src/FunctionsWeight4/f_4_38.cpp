#include "f_4_38.h"

namespace PentagonFunctions {

template <typename T> T f_4_38_abbreviated (const std::array<T,29>&);

template <typename T> class SpDLog_f_4_38_W_7 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_38_W_7 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (-kin.v[3] + T(-2) * kin.v[4]) + -prod_pow(kin.v[4], 2) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (((T(-3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(1) + kin.v[1]) * kin.v[3] + ((T(-3) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (-(kin.v[1] * kin.v[4]) + ((T(3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + -kin.v[1] + T(-1)) * kin.v[3] + ((T(3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4]);
c[1] = (-kin.v[3] + -kin.v[4]) * rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[3] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[5] + abb[4] * (abb[3] + -prod_pow(abb[2], 2)) + prod_pow(abb[1], 2) * (abb[4] + -abb[5]) + -(abb[3] * abb[5]));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_38_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl19 = DLog_W_19<T>(kin),dl4 = DLog_W_4<T>(kin),dl25 = DLog_W_25<T>(kin),dl14 = DLog_W_14<T>(kin),dl11 = DLog_W_11<T>(kin),dl20 = DLog_W_20<T>(kin),dl2 = DLog_W_2<T>(kin),dl18 = DLog_W_18<T>(kin),dl1 = DLog_W_1<T>(kin),dl16 = DLog_W_16<T>(kin),spdl7 = SpDLog_f_4_38_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,29> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[19] / kin_path.W[19]), dl19(t), rlog(kin.W[24] / kin_path.W[24]), dl4(t), rlog(kin.W[10] / kin_path.W[10]), rlog(v_path[3]), rlog(kin.W[0] / kin_path.W[0]), dl25(t), rlog(kin.W[1] / kin_path.W[1]), rlog(-v_path[2] + -v_path[3] + v_path[0]), dl14(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), rlog(kin.W[15] / kin_path.W[15]), dl11(t), rlog(v_path[0]), dl20(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl2(t), f_2_1_2(kin_path), dl18(t), dl1(t), dl16(t)}
;

        auto result = f_4_38_abbreviated(abbr);
        result = result + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_38_abbreviated(const std::array<T,29>& abb)
{
using TR = typename T::value_type;
T z[64];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[21];
z[3] = abb[5];
z[4] = abb[24];
z[5] = abb[11];
z[6] = abb[15];
z[7] = abb[28];
z[8] = abb[13];
z[9] = abb[18];
z[10] = abb[14];
z[11] = abb[26];
z[12] = bc<TR>[0];
z[13] = abb[20];
z[14] = abb[27];
z[15] = abb[2];
z[16] = abb[6];
z[17] = abb[12];
z[18] = abb[7];
z[19] = abb[3];
z[20] = abb[22];
z[21] = abb[23];
z[22] = abb[8];
z[23] = abb[9];
z[24] = abb[10];
z[25] = abb[19];
z[26] = abb[16];
z[27] = abb[17];
z[28] = abb[25];
z[29] = bc<TR>[1];
z[30] = bc<TR>[2];
z[31] = bc<TR>[4];
z[32] = bc<TR>[7];
z[33] = bc<TR>[8];
z[34] = bc<TR>[9];
z[35] = -z[8] + z[9];
z[36] = -z[35] / T(2);
z[37] = T(2) * z[5];
z[38] = (T(11) * z[1]) / T(2) + (T(-5) * z[3]) / T(2) + z[36] + -z[37];
z[38] = z[4] * z[38];
z[39] = z[1] + -z[18];
z[39] = z[16] * z[39];
z[36] = z[5] + z[36];
z[40] = z[7] * z[36];
z[38] = z[38] + -z[39] / T(2) + -z[40];
z[41] = T(2) * z[1];
z[42] = -z[3] + z[8];
z[43] = z[41] + z[42];
z[44] = z[17] * z[43];
z[45] = -z[5] + z[9];
z[46] = z[6] * z[45];
z[47] = z[8] + z[37];
z[48] = (T(-3) * z[9]) / T(2) + (T(13) * z[23]) / T(6) + (T(-2) * z[47]) / T(3);
z[48] = z[14] * z[48];
z[49] = T(7) * z[4];
z[50] = (T(17) * z[7]) / T(2) + (T(-19) * z[14]) / T(2) + -z[49];
z[50] = z[30] * z[50];
z[42] = z[1] + z[42] / T(2);
z[51] = z[30] + z[42] / T(3);
z[51] = z[2] * z[51];
z[52] = -z[5] + z[23];
z[53] = z[22] * z[52];
z[54] = T(11) * z[1] + z[3] + (T(11) * z[8]) / T(2) + (T(-13) * z[18]) / T(2);
z[54] = T(-2) * z[30] + z[54] / T(3);
z[54] = z[11] * z[54];
z[55] = T(-3) * z[4] + (T(-17) * z[7]) / T(3);
z[55] = T(-3) * z[14] + (T(13) * z[25]) / T(3) + z[55] / T(2);
z[55] = z[29] * z[55];
z[56] = -z[2] / T(4) + (T(-5) * z[4]) / T(4) + z[7];
z[56] = (T(-3) * z[11]) / T(2) + (T(5) * z[17]) / T(6) + z[56] / T(3);
z[56] = int_to_imaginary<T>(1) * z[12] * z[56];
z[38] = z[38] / T(3) + -z[44] + (T(7) * z[46]) / T(6) + z[48] + z[50] / T(6) + z[51] + (T(-13) * z[53]) / T(6) + z[54] + z[55] / T(2) + z[56];
z[38] = z[12] * z[38];
z[48] = T(2) * z[22];
z[48] = z[48] * z[52];
z[35] = z[35] + -z[37];
z[37] = z[14] * z[35];
z[45] = z[4] * z[45];
z[45] = z[37] + z[45];
z[50] = z[25] * z[35];
z[48] = z[45] + -z[46] + z[48] + -z[50];
z[51] = z[24] * z[48];
z[52] = z[4] * z[43];
z[53] = T(2) * z[11];
z[54] = z[8] + z[41];
z[55] = -z[18] + z[54];
z[55] = z[53] * z[55];
z[52] = -z[44] + z[52] + z[55];
z[52] = z[10] * z[52];
z[51] = z[51] + -z[52];
z[52] = z[2] * z[43];
z[55] = z[43] * z[53];
z[41] = -z[3] + -z[5] + z[41];
z[41] = z[4] * z[41];
z[37] = z[37] + z[41] + -z[46] + -z[52] + z[55];
z[37] = z[0] * z[37];
z[41] = T(2) * z[25];
z[55] = -z[7] + z[41];
z[56] = z[4] + -z[55];
z[56] = z[35] * z[56];
z[47] = z[9] + T(-2) * z[23] + z[47];
z[57] = T(2) * z[14];
z[58] = -(z[47] * z[57]);
z[56] = z[56] + z[58];
z[56] = z[13] * z[56];
z[58] = z[7] * z[35];
z[45] = -z[45] + -z[46] + z[58];
z[58] = -(z[27] * z[45]);
z[59] = z[4] + -z[7];
z[60] = z[57] + z[59];
z[61] = -(z[30] * z[60]);
z[62] = z[4] + T(-5) * z[7];
z[41] = z[14] + z[41] + z[62] / T(2);
z[41] = z[29] * z[41];
z[41] = z[41] + z[61];
z[41] = z[29] * z[41];
z[53] = -z[2] + z[4] + z[53];
z[53] = z[43] * z[53];
z[61] = -(z[21] * z[53]);
z[62] = z[31] * z[60];
z[63] = z[7] + -z[14];
z[63] = prod_pow(z[30], 2) * z[63];
z[37] = z[37] + z[41] + T(-2) * z[51] + z[56] + z[58] + z[61] + T(3) * z[62] + z[63] / T(2);
z[37] = int_to_imaginary<T>(1) * z[37];
z[37] = z[37] + z[38];
z[37] = z[12] * z[37];
z[38] = prod_pow(z[24], 2) * z[48];
z[36] = z[4] * z[36];
z[41] = z[14] * z[47];
z[36] = z[36] + z[40] + z[41] + z[50];
z[36] = prod_pow(z[13], 2) * z[36];
z[41] = z[2] * z[42];
z[42] = -(z[4] * z[42]);
z[47] = -z[3] + T(2) * z[18] + -z[54];
z[47] = z[11] * z[47];
z[42] = -z[41] + z[42] + z[44] + z[47];
z[42] = prod_pow(z[10], 2) * z[42];
z[47] = -z[3] + z[9];
z[47] = z[4] * z[47];
z[40] = z[40] + -z[41] + z[46] + z[47] / T(2);
z[40] = z[0] * z[40];
z[41] = z[4] + z[57];
z[46] = -z[7] + z[41];
z[35] = z[35] * z[46];
z[46] = -(z[13] * z[35]);
z[47] = -(z[10] * z[53]);
z[40] = z[40] + z[46] + z[47];
z[40] = z[0] * z[40];
z[43] = z[11] * z[43];
z[46] = z[1] + -z[3];
z[46] = z[4] * z[46];
z[43] = z[43] + z[46];
z[39] = T(2) * z[39] + z[43] + -z[44];
z[39] = prod_pow(z[15], 2) * z[39];
z[44] = -(z[26] * z[45]);
z[43] = z[43] + -z[52];
z[43] = z[19] * z[43];
z[45] = prod_pow(z[30], 3);
z[45] = T(2) * z[32] + (T(-2) * z[45]) / T(3);
z[45] = z[45] * z[60];
z[41] = z[41] + -z[55];
z[41] = prod_pow(z[29], 3) * z[41];
z[35] = z[28] * z[35];
z[46] = z[20] * z[53];
z[47] = (T(5) * z[7]) / T(2) + -z[49];
z[47] = (T(41) * z[14]) / T(2) + (T(7) * z[47]) / T(3);
z[47] = (T(-32) * z[11]) / T(3) + z[47] / T(4);
z[47] = z[34] * z[47];
z[48] = z[14] + z[59] / T(2);
z[48] = z[33] * z[48];
return z[35] + z[36] + z[37] + z[38] + z[39] + z[40] + z[41] / T(3) + z[42] + z[43] + z[44] + z[45] + z[46] + z[47] + z[48];
}



template IntegrandConstructorType<double> f_4_38_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_38_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_38_construct (const Kin<qd_real>&);
#endif

}