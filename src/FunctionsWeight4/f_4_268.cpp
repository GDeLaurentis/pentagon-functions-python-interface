#include "f_4_268.h"

namespace PentagonFunctions {

template <typename T> T f_4_268_abbreviated (const std::array<T,15>&);

template <typename T> class SpDLog_f_4_268_W_12 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_268_W_12 (const Kin<T>& kin) {
        c[0] = rlog(kin.v[3]) * (kin.v[1] * (T(6) + T(3) * kin.v[1]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[1] * (T(-6) + T(-3) * kin.v[1]) + kin.v[4] * (T(6) + T(3) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-3) + prod_pow(abb[2], 2) * abb[4] * T(3) + prod_pow(abb[1], 2) * (abb[4] * T(-3) + abb[3] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_268_W_7 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_268_W_7 (const Kin<T>& kin) {
        c[0] = (T(-3) / T(2) + (T(-3) * kin.v[1]) / T(2) + (T(3) * kin.v[3]) / T(8) + (T(3) * kin.v[4]) / T(4)) * kin.v[3] + (T(-3) * kin.v[1] * kin.v[4]) / T(2) + (T(-3) / T(2) + (T(3) * kin.v[4]) / T(8)) * kin.v[4];
c[1] = kin.v[3] * (T(-3) / T(2) + (T(-3) * kin.v[1]) / T(2) + (T(-3) * kin.v[3]) / T(2) + T(-3) * kin.v[4]) + (T(-3) * kin.v[1] * kin.v[4]) / T(2) + (T(-3) / T(2) + (T(-3) * kin.v[4]) / T(2)) * kin.v[4] + rlog(kin.v[3]) * (((T(-21) * kin.v[3]) / T(4) + (T(-21) * kin.v[4]) / T(2) + T(9) + T(9) * kin.v[1]) * kin.v[3] + ((T(-21) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + T(9) * kin.v[1] * kin.v[4]) + rlog(-kin.v[4]) * (((T(21) * kin.v[3]) / T(4) + (T(21) * kin.v[4]) / T(2) + T(-9) + T(-9) * kin.v[1]) * kin.v[3] + ((T(21) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + T(-9) * kin.v[1] * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-21) * kin.v[1] * kin.v[4]) / T(2) + (T(-21) / T(2) + (T(-21) * kin.v[1]) / T(2) + (T(45) * kin.v[3]) / T(8) + (T(45) * kin.v[4]) / T(4)) * kin.v[3] + (T(-21) / T(2) + (T(45) * kin.v[4]) / T(8)) * kin.v[4]);
c[2] = (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2);
c[3] = rlog(-kin.v[4]) * (T(-3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * rlog(-kin.v[1] + kin.v[3] + kin.v[4]) + rlog(kin.v[3]) * (T(3) * kin.v[3] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[5] * (prod_pow(abb[6], 2) * ((abb[9] * T(-3)) / T(4) + (abb[8] * T(9)) / T(2) + (abb[4] * T(21)) / T(4)) + abb[3] * ((prod_pow(abb[6], 2) * T(-9)) / T(2) + abb[7] * T(-3)) + abb[1] * (abb[1] * ((abb[4] * T(-15)) / T(4) + (abb[8] * T(-3)) / T(2) + (abb[3] * T(3)) / T(2) + (abb[9] * T(9)) / T(4)) + abb[6] * ((abb[4] * T(-3)) / T(2) + (abb[9] * T(-3)) / T(2) + abb[8] * T(-3)) + abb[3] * abb[6] * T(3)) + abb[7] * ((abb[4] * T(3)) / T(2) + (abb[9] * T(3)) / T(2) + abb[8] * T(3)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_268_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl7 = DLog_W_7<T>(kin),dl2 = DLog_W_2<T>(kin),dl19 = DLog_W_19<T>(kin),dl5 = DLog_W_5<T>(kin),dl4 = DLog_W_4<T>(kin),spdl12 = SpDLog_f_4_268_W_12<T>(kin),spdl7 = SpDLog_f_4_268_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,15> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[18] / kin_path.W[18]), dl7(t), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), rlog(kin.W[4] / kin_path.W[4]), -rlog(t), dl2(t), dl19(t), f_2_1_4(kin_path), dl5(t), dl4(t)}
;

        auto result = f_4_268_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_268_abbreviated(const std::array<T,15>& abb)
{
using TR = typename T::value_type;
T z[30];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[11];
z[3] = abb[13];
z[4] = abb[14];
z[5] = abb[4];
z[6] = abb[10];
z[7] = abb[8];
z[8] = abb[9];
z[9] = abb[2];
z[10] = abb[6];
z[11] = abb[7];
z[12] = abb[12];
z[13] = bc<TR>[0];
z[14] = bc<TR>[9];
z[15] = T(2) * z[7];
z[16] = z[1] + -z[5];
z[17] = -z[15] + T(3) * z[16];
z[18] = z[2] * z[17];
z[19] = z[4] * z[17];
z[20] = T(2) * z[3];
z[20] = z[16] * z[20];
z[18] = -z[18] + z[19] + -z[20];
z[18] = z[9] * z[18];
z[21] = -z[7] + (T(3) * z[16]) / T(2);
z[22] = z[3] * z[21];
z[23] = -z[5] + z[8];
z[23] = z[6] * z[23];
z[24] = z[8] / T(4);
z[25] = T(2) * z[1] + (T(-7) * z[5]) / T(4) + (T(-3) * z[7]) / T(2) + -z[24];
z[25] = z[2] * z[25];
z[19] = -z[19] + z[22] + -z[23] + z[25];
z[19] = z[0] * z[19];
z[19] = z[18] + z[19];
z[19] = z[0] * z[19];
z[25] = -z[3] + z[4];
z[25] = z[17] * z[25];
z[26] = (T(7) * z[5]) / T(2);
z[27] = T(3) * z[1] + -z[26];
z[28] = T(3) * z[7];
z[29] = z[8] / T(2) + z[27] + -z[28];
z[29] = z[2] * z[29];
z[25] = z[25] + -z[29];
z[29] = z[0] * z[25];
z[26] = (T(7) * z[8]) / T(2) + -z[26] + -z[28];
z[26] = z[2] * z[26];
z[22] = z[22] + z[26] / T(2);
z[22] = z[10] * z[22];
z[18] = -z[18] + z[22] + z[29];
z[18] = z[10] * z[18];
z[22] = z[11] * z[25];
z[25] = z[2] + -z[4];
z[17] = z[17] * z[25];
z[17] = z[17] + z[20];
z[17] = z[12] * z[17];
z[15] = z[3] * z[15];
z[20] = z[2] * z[16];
z[15] = z[15] + z[20];
z[15] = prod_pow(z[9], 2) * z[15];
z[15] = z[15] + z[17] + z[18] + z[19] + -z[22];
z[17] = z[7] + -z[24] + -z[27] / T(2);
z[17] = z[2] * z[17];
z[16] = -z[7] / T(2) + -z[16];
z[16] = z[3] * z[16];
z[18] = z[4] * z[21];
z[16] = z[16] + z[17] + z[18] + z[23] / T(4);
z[16] = prod_pow(z[13], 2) * z[16];
z[17] = z[3] + T(-3) * z[25];
z[17] = z[14] * z[17];
return T(3) * z[15] + z[16] + T(4) * z[17];
}



template IntegrandConstructorType<double> f_4_268_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_268_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_268_construct (const Kin<qd_real>&);
#endif

}