#include "f_4_243.h"

namespace PentagonFunctions {

template <typename T> T f_4_243_abbreviated (const std::array<T,29>&);

template <typename T> class SpDLog_f_4_243_W_23 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_243_W_23 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[4]) * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[0] * (-kin.v[0] + T(2) + T(-2) * kin.v[1] + T(2) * kin.v[3]) + kin.v[4] * (T(2) + kin.v[4]) + kin.v[2] * (T(-2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) * kin.v[2] + T(2) * kin.v[3] + T(-2) * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[0] * (T(-2) + kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(-2)) * kin.v[4] + kin.v[2] * (-kin.v[2] + T(2) + T(2) * kin.v[4]) + kin.v[1] * (T(-2) * kin.v[2] + T(-2) * kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[0] * (T(-4) + T(2) * kin.v[0] + T(4) * kin.v[1] + T(-4) * kin.v[3]) + kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[4] * (T(-4) + T(-2) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] + prod_pow(abb[2], 2) * (-abb[5] + abb[4] * T(-2)) + prod_pow(abb[1], 2) * (abb[5] + -abb[3] + abb[4] * T(2)));
    }
};
template <typename T> class SpDLog_f_4_243_W_21 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_243_W_21 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[0] * (-kin.v[0] + T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[1]) * (kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) + kin.v[0] + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[0] * (T(4) + T(-2) * kin.v[0] + T(4) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) + T(2) * kin.v[1] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[3] * (T(-4) + T(2) * kin.v[3] + T(4) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[6] * (prod_pow(abb[8], 2) * abb[9] + prod_pow(abb[8], 2) * (-abb[5] + abb[10] * T(-2)) + prod_pow(abb[7], 2) * (abb[5] + -abb[9] + abb[10] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_243_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl21 = DLog_W_21<T>(kin),dl17 = DLog_W_17<T>(kin),dl19 = DLog_W_19<T>(kin),dl24 = DLog_W_24<T>(kin),dl25 = DLog_W_25<T>(kin),dl18 = DLog_W_18<T>(kin),dl16 = DLog_W_16<T>(kin),dl2 = DLog_W_2<T>(kin),dl20 = DLog_W_20<T>(kin),dl5 = DLog_W_5<T>(kin),spdl23 = SpDLog_f_4_243_W_23<T>(kin),spdl21 = SpDLog_f_4_243_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,29> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[17] / kin_path.W[17]), dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[19] / kin_path.W[19]), dl17(t), -rlog(t), dl19(t), -rlog(t), dl24(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl25(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl18(t), f_2_1_8(kin_path), f_2_1_15(kin_path), dl16(t), dl2(t), dl20(t), dl5(t)}
;

        auto result = f_4_243_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_243_abbreviated(const std::array<T,29>& abb)
{
using TR = typename T::value_type;
T z[55];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[22];
z[3] = abb[25];
z[4] = abb[28];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[12];
z[8] = abb[16];
z[9] = bc<TR>[0];
z[10] = abb[2];
z[11] = abb[15];
z[12] = abb[11];
z[13] = abb[17];
z[14] = abb[18];
z[15] = abb[23];
z[16] = abb[7];
z[17] = abb[26];
z[18] = abb[8];
z[19] = abb[27];
z[20] = abb[20];
z[21] = abb[21];
z[22] = abb[24];
z[23] = abb[9];
z[24] = abb[19];
z[25] = abb[10];
z[26] = abb[13];
z[27] = abb[14];
z[28] = bc<TR>[2];
z[29] = bc<TR>[9];
z[30] = z[6] + -z[23];
z[31] = T(2) * z[25];
z[32] = z[30] + z[31];
z[33] = z[19] * z[32];
z[34] = -z[1] + z[6];
z[35] = T(2) * z[5];
z[36] = z[34] + z[35];
z[37] = z[3] * z[36];
z[38] = -z[23] + z[25];
z[39] = z[24] * z[38];
z[40] = -z[1] + z[5];
z[41] = z[11] * z[40];
z[42] = z[5] + -z[25];
z[42] = z[2] * z[42];
z[42] = -z[33] + z[37] + z[39] + -z[41] + z[42];
z[42] = z[8] * z[42];
z[43] = z[4] * z[36];
z[40] = z[2] * z[40];
z[37] = z[37] + z[40] + z[41] + -z[43];
z[43] = -(z[14] * z[37]);
z[44] = z[17] * z[32];
z[38] = z[2] * z[38];
z[33] = -z[33] + -z[38] + -z[39] + z[44];
z[38] = -(z[21] * z[33]);
z[44] = T(2) * z[19];
z[45] = z[2] + -z[17] + z[44];
z[45] = z[32] * z[45];
z[46] = z[18] * z[45];
z[47] = T(2) * z[3];
z[48] = z[2] + -z[4] + z[47];
z[36] = z[36] * z[48];
z[48] = z[0] * z[36];
z[49] = z[3] + -z[19];
z[50] = z[4] + -z[17];
z[51] = z[49] + -z[50];
z[52] = prod_pow(z[28], 2) * z[51];
z[53] = -z[49] + -z[50] / T(4);
z[54] = prod_pow(z[9], 2);
z[53] = z[53] * z[54];
z[38] = z[38] + z[42] + z[43] + -z[46] + z[48] + -z[52] / T(2) + z[53] / T(3);
z[38] = int_to_imaginary<T>(1) * z[9] * z[38];
z[42] = z[8] * z[37];
z[40] = z[40] + T(-3) * z[41];
z[43] = T(-2) * z[7] + z[35];
z[43] = z[12] * z[43];
z[34] = -z[5] + -z[34] / T(2);
z[48] = z[4] * z[34];
z[52] = z[3] * z[34];
z[40] = z[40] / T(2) + z[43] + -z[48] + -z[52];
z[40] = z[10] * z[40];
z[40] = z[40] + z[42];
z[40] = z[10] * z[40];
z[42] = z[25] * z[26];
z[43] = z[5] * z[12];
z[42] = z[42] + -z[43];
z[43] = T(5) * z[5];
z[53] = -z[23] + z[43];
z[53] = -z[31] + z[53] / T(2);
z[53] = z[2] * z[53];
z[30] = z[25] + z[30] / T(2);
z[30] = z[17] * z[30];
z[43] = T(-2) * z[1] + (T(5) * z[6]) / T(2) + z[43];
z[43] = z[3] * z[43];
z[42] = -z[30] + T(-2) * z[41] + z[42] / T(2) + z[43] + z[53];
z[43] = z[19] + -z[26];
z[43] = z[27] * z[43];
z[53] = -z[3] + z[12];
z[53] = z[7] * z[53];
z[43] = z[43] + z[53];
z[51] = z[28] * z[51];
z[53] = (T(-2) * z[6]) / T(3) + z[23] / T(2) + (T(-4) * z[25]) / T(3);
z[53] = z[19] * z[53];
z[42] = (T(5) * z[39]) / T(6) + z[42] / T(3) + z[43] / T(6) + (T(-3) * z[51]) / T(4) + z[53];
z[42] = z[42] * z[54];
z[37] = -(z[13] * z[37]);
z[33] = z[20] * z[33];
z[30] = z[30] + -z[39];
z[39] = -z[5] + z[23];
z[39] = z[2] * z[39];
z[39] = z[39] + z[41];
z[39] = z[30] + z[39] / T(2) + z[52];
z[39] = prod_pow(z[8], 2) * z[39];
z[34] = z[2] * z[34];
z[35] = -z[1] + -z[6] + -z[35];
z[35] = z[3] * z[35];
z[34] = z[34] + z[35] + z[48];
z[34] = z[0] * z[34];
z[35] = -(z[8] * z[36]);
z[34] = z[34] + z[35];
z[34] = z[0] * z[34];
z[35] = z[6] + -z[27] + z[31];
z[35] = z[35] * z[44];
z[32] = z[2] * z[32];
z[32] = z[32] + z[35];
z[32] = z[18] * z[32];
z[35] = z[8] * z[45];
z[32] = z[32] + z[35];
z[32] = z[18] * z[32];
z[31] = T(2) * z[27] + -z[31];
z[31] = z[26] * z[31];
z[35] = z[6] + z[23];
z[35] = z[2] * z[35];
z[30] = -z[30] + z[31] + z[35] / T(2);
z[30] = z[16] * z[30];
z[30] = z[30] + -z[46];
z[30] = z[16] * z[30];
z[31] = (T(-151) * z[49]) / T(3) + T(-7) * z[50];
z[31] = z[29] * z[31];
z[35] = z[22] * z[45];
z[36] = z[15] * z[36];
z[41] = prod_pow(z[0], 2) * z[7] * z[47];
return z[30] + z[31] / T(8) + z[32] + z[33] + z[34] + z[35] + z[36] + z[37] + z[38] + z[39] + z[40] + z[41] + z[42];
}



template IntegrandConstructorType<double> f_4_243_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_243_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_243_construct (const Kin<qd_real>&);
#endif

}