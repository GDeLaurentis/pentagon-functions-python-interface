#include "f_4_240.h"

namespace PentagonFunctions {

template <typename T> T f_4_240_abbreviated (const std::array<T,17>&);



template <typename T> IntegrandConstructorType<T> f_4_240_construct (const Kin<T>& kin) {
    return [&kin, 
            dl24 = DLog_W_24<T>(kin),dl18 = DLog_W_18<T>(kin),dl1 = DLog_W_1<T>(kin),dl17 = DLog_W_17<T>(kin),dl19 = DLog_W_19<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,17> abbr = 
            {dl24(t), rlog(kin.W[17] / kin_path.W[17]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), rlog(kin.W[0] / kin_path.W[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(kin.W[23] / kin_path.W[23]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[18] / kin_path.W[18]), dl18(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl1(t), dl17(t), dl19(t)}
;

        auto result = f_4_240_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_240_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[49];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = bc<TR>[0];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[8];
z[9] = abb[9];
z[10] = abb[10];
z[11] = abb[7];
z[12] = bc<TR>[1];
z[13] = bc<TR>[2];
z[14] = bc<TR>[4];
z[15] = bc<TR>[9];
z[16] = abb[11];
z[17] = abb[14];
z[18] = abb[15];
z[19] = abb[16];
z[20] = abb[12];
z[21] = abb[13];
z[22] = z[9] + z[10];
z[23] = T(2) * z[7];
z[24] = z[8] / T(2);
z[25] = z[1] / T(2);
z[26] = T(3) * z[22] + -z[23] + -z[24] + -z[25];
z[26] = z[0] * z[26];
z[27] = z[22] + z[25];
z[28] = -z[7] + -z[24] + z[27];
z[28] = z[16] * z[28];
z[29] = -z[8] + z[22];
z[30] = T(2) * z[18];
z[29] = z[29] * z[30];
z[31] = T(2) * z[22];
z[32] = -z[7] + z[31];
z[33] = -z[1] + T(2) * z[8];
z[34] = -z[32] + z[33];
z[34] = z[19] * z[34];
z[35] = -z[1] + z[8];
z[36] = z[17] * z[35];
z[26] = z[26] + -z[28] + -z[29] + z[34] + -z[36];
z[28] = z[6] * z[26];
z[29] = z[7] + -z[8];
z[34] = z[21] * z[29];
z[37] = prod_pow(z[12], 2);
z[38] = T(2) * z[12];
z[39] = -z[13] / T(4) + z[38];
z[39] = z[13] * z[39];
z[39] = z[34] + -z[37] + z[39];
z[39] = z[16] * z[39];
z[40] = -(z[0] * z[38]);
z[41] = z[0] / T(4) + z[17];
z[42] = -(z[13] * z[41]);
z[40] = z[40] + z[42];
z[40] = z[13] * z[40];
z[42] = prod_pow(z[13], 2);
z[42] = (T(3) * z[42]) / T(2);
z[43] = z[21] * z[35];
z[43] = z[42] + z[43];
z[43] = z[18] * z[43];
z[34] = -z[34] + z[42];
z[34] = z[19] * z[34];
z[42] = -(z[21] * z[36]);
z[37] = z[0] * z[37];
z[28] = z[28] + z[34] + z[37] + z[39] + z[40] + z[42] + z[43];
z[32] = z[8] + -z[32];
z[32] = z[0] * z[32];
z[32] = z[32] + z[36];
z[34] = T(2) * z[19];
z[37] = z[30] + z[34];
z[31] = z[1] + z[31];
z[39] = T(3) * z[8] + -z[31];
z[37] = z[37] * z[39];
z[40] = T(3) * z[7];
z[31] = -z[31] + z[40];
z[42] = z[16] * z[31];
z[32] = T(-3) * z[32] + z[37] + z[42];
z[37] = -z[2] + -z[11];
z[37] = z[32] * z[37];
z[42] = z[18] + z[19];
z[43] = -(z[39] * z[42]);
z[44] = (T(3) * z[8]) / T(2);
z[45] = z[27] + -z[44];
z[46] = T(3) * z[0];
z[47] = z[16] + -z[46];
z[47] = z[45] * z[47];
z[43] = z[43] + z[47];
z[43] = z[3] * z[43];
z[47] = z[0] + -z[16];
z[47] = z[14] * z[47];
z[28] = T(3) * z[28] + z[37] + z[43] + T(6) * z[47];
z[28] = int_to_imaginary<T>(1) * z[28];
z[37] = (T(3) * z[13]) / T(2);
z[43] = (T(-11) * z[1]) / T(2) + z[22];
z[43] = z[37] + z[43] / T(3) + z[44];
z[47] = z[18] / T(2);
z[43] = z[43] * z[47];
z[23] = -z[1] / T(12) + (T(-7) * z[8]) / T(4) + (T(-9) * z[13]) / T(8) + -z[22] / T(6) + z[23] + z[38];
z[23] = z[16] * z[23];
z[33] = -z[7] + z[33] + -z[38];
z[33] = z[0] * z[33];
z[38] = (T(5) * z[0]) / T(4) + -z[17];
z[37] = z[37] * z[38];
z[38] = -z[1] + z[22];
z[48] = (T(3) * z[13]) / T(4) + -z[29] + z[38] / T(6);
z[48] = z[19] * z[48];
z[41] = (T(3) * z[16]) / T(4) + z[19] / T(2) + -z[41] + z[47];
z[41] = int_to_imaginary<T>(1) * z[4] * z[41];
z[23] = z[23] + z[33] + T(-2) * z[36] + z[37] + z[41] + z[43] + z[48];
z[23] = z[4] * z[23];
z[23] = z[23] + z[28];
z[23] = z[4] * z[23];
z[24] = z[7] + -z[22] + -z[24] + z[25];
z[24] = z[24] * z[46];
z[25] = z[30] * z[38];
z[28] = z[16] * z[45];
z[30] = -(z[19] * z[31]);
z[24] = z[24] + z[25] + z[28] + z[30];
z[24] = z[3] * z[24];
z[25] = z[11] * z[32];
z[28] = -z[27] + z[40] + -z[44];
z[30] = z[16] / T(2);
z[28] = z[28] * z[30];
z[31] = (T(3) * z[7]) / T(2) + -z[27];
z[31] = z[19] * z[31];
z[32] = z[18] * z[38];
z[31] = z[31] + -z[32];
z[22] = (T(-3) * z[1]) / T(2) + (T(5) * z[8]) / T(2) + z[22];
z[22] = -z[7] + z[22] / T(2);
z[22] = z[0] * z[22];
z[22] = z[22] + -z[36] / T(2);
z[22] = T(3) * z[22] + z[28] + z[31];
z[22] = z[2] * z[22];
z[22] = z[22] + z[24] + -z[25];
z[22] = z[2] * z[22];
z[24] = z[5] * z[26];
z[26] = -z[16] + z[19];
z[26] = z[26] * z[29];
z[28] = -(z[18] * z[35]);
z[26] = z[26] + z[28] + z[36];
z[26] = z[20] * z[26];
z[24] = z[24] + z[26];
z[26] = z[0] * z[45];
z[26] = z[26] + z[36];
z[27] = (T(9) * z[8]) / T(2) + -z[27] + -z[40];
z[27] = z[27] * z[30];
z[26] = (T(3) * z[26]) / T(2) + z[27] + z[31];
z[26] = z[3] * z[26];
z[25] = z[25] + z[26];
z[25] = z[3] * z[25];
z[26] = z[16] + -z[18];
z[26] = z[26] * z[39];
z[27] = z[34] * z[38];
z[26] = z[26] + z[27];
z[26] = prod_pow(z[11], 2) * z[26];
z[27] = (T(-119) * z[0]) / T(4) + (T(-157) * z[16]) / T(12) + T(39) * z[17] + (T(23) * z[42]) / T(6);
z[27] = z[15] * z[27];
return z[22] + z[23] + T(3) * z[24] + z[25] + z[26] + z[27] / T(4);
}



template IntegrandConstructorType<double> f_4_240_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_240_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_240_construct (const Kin<qd_real>&);
#endif

}