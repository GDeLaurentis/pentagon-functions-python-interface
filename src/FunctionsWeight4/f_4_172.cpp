#include "f_4_172.h"

namespace PentagonFunctions {

template <typename T> T f_4_172_abbreviated (const std::array<T,41>&);

template <typename T> class SpDLog_f_4_172_W_21 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_172_W_21 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(6) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) + T(3) * kin.v[1] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[3] * (T(-6) + T(3) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(6) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) + T(3) * kin.v[1] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[3] * (T(-6) + T(3) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[1]) * (kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[3] * (T(6) + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) + T(-3) * kin.v[1] + T(6) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[3] * (T(6) + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) + T(-3) * kin.v[1] + T(6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(3) + prod_pow(abb[1], 2) * (abb[3] * T(-3) + abb[6] * T(-3) + abb[4] * T(3) + abb[5] * T(3)) + prod_pow(abb[2], 2) * (abb[4] * T(-3) + abb[5] * T(-3) + abb[6] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_172_W_7 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_172_W_7 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (T(4) * kin.v[2] + T(-6) * kin.v[3] + T(-8) * kin.v[4]) + T(4) * kin.v[2] * kin.v[4] + T(-2) * prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[1] * kin.v[4] + kin.v[3] * (T(4) + T(4) * kin.v[1] + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(-2) * kin.v[4]));
c[1] = T(10) * prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(5) * prod_pow(kin.v[4], 2) + kin.v[2] * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[4] + T(4) * kin.v[4]) + kin.v[3] * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(8) * kin.v[4] + T(16) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + T(3) * kin.v[2] * kin.v[4] + kin.v[3] * ((T(-9) * kin.v[3]) / T(2) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + T(3) * kin.v[1] * kin.v[4] + kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(3) + T(3) * kin.v[1] + T(-3) * kin.v[4]))) + rlog(-kin.v[4]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + T(3) * kin.v[2] * kin.v[4] + kin.v[3] * ((T(-9) * kin.v[3]) / T(2) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + T(3) * kin.v[1] * kin.v[4] + kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(3) + T(3) * kin.v[1] + T(-3) * kin.v[4]))) + rlog(-kin.v[1]) * (prod_pow(kin.v[4], 2) / T(2) + -(kin.v[2] * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + -kin.v[2] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + -kin.v[1] + T(-1) + kin.v[4]))) + rlog(kin.v[3]) * (prod_pow(kin.v[4], 2) / T(2) + -(kin.v[2] * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + -kin.v[2] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + -kin.v[1] + T(-1) + kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (prod_pow(kin.v[4], 2) / T(2) + -(kin.v[2] * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + -kin.v[2] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + -kin.v[1] + T(-1) + kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (prod_pow(kin.v[4], 2) / T(2) + -(kin.v[2] * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + -kin.v[2] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-(kin.v[1] * kin.v[4]) + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + -kin.v[1] + T(-1) + kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (T(-2) * kin.v[2] * kin.v[4] + prod_pow(kin.v[4], 2) + kin.v[3] * (T(-2) * kin.v[2] + T(3) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[3] * (T(-2) + T(-2) * kin.v[1] + kin.v[3] + T(2) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(4) + T(8)) * kin.v[3] + T(8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4];
c[3] = rlog(-kin.v[1]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(kin.v[3]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[3] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4] + T(6) * kin.v[4]) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4] + T(6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[16] * (abb[3] * (abb[1] * abb[15] + -(abb[1] * abb[8]) + abb[1] * (abb[2] + -abb[1] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[17] * T(2)) + abb[1] * abb[8] * (-abb[5] + -abb[6] + -abb[11] + abb[18] * T(-4) + abb[19] * T(-2) + abb[4] * T(3) + abb[12] * T(3)) + abb[1] * abb[15] * (abb[5] + abb[6] + abb[11] + abb[4] * T(-3) + abb[12] * T(-3) + abb[19] * T(2) + abb[18] * T(4)) + abb[1] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[19] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[4] * bc<T>[0] * int_to_imaginary<T>(3) + abb[12] * bc<T>[0] * int_to_imaginary<T>(3) + abb[18] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[1] * (-abb[5] + -abb[6] + -abb[11] + abb[18] * T(-4) + abb[19] * T(-2) + abb[4] * T(3) + abb[12] * T(3)) + abb[2] * (abb[5] + abb[6] + abb[11] + abb[4] * T(-3) + abb[12] * T(-3) + abb[19] * T(2) + abb[18] * T(4))) + abb[17] * (abb[4] * T(-6) + abb[12] * T(-6) + abb[5] * T(2) + abb[6] * T(2) + abb[11] * T(2) + abb[19] * T(4) + abb[18] * T(8)) + abb[14] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(1) + abb[3] * (abb[8] + -abb[1] + -abb[2] + -abb[15] + bc<T>[0] * int_to_imaginary<T>(1)) + abb[19] * bc<T>[0] * int_to_imaginary<T>(2) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[18] * bc<T>[0] * int_to_imaginary<T>(4) + abb[1] * (-abb[5] + -abb[6] + -abb[11] + abb[18] * T(-4) + abb[19] * T(-2) + abb[4] * T(3) + abb[12] * T(3)) + abb[2] * (-abb[5] + -abb[6] + -abb[11] + abb[18] * T(-4) + abb[19] * T(-2) + abb[4] * T(3) + abb[12] * T(3)) + abb[15] * (-abb[5] + -abb[6] + -abb[11] + abb[18] * T(-4) + abb[19] * T(-2) + abb[4] * T(3) + abb[12] * T(3)) + abb[8] * (abb[5] + abb[6] + abb[11] + abb[4] * T(-3) + abb[12] * T(-3) + abb[19] * T(2) + abb[18] * T(4)) + abb[14] * (abb[4] * T(-6) + abb[12] * T(-6) + abb[3] * T(2) + abb[5] * T(2) + abb[6] * T(2) + abb[11] * T(2) + abb[19] * T(4) + abb[18] * T(8))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_172_construct (const Kin<T>& kin) {
    return [&kin, 
            dl21 = DLog_W_21<T>(kin),dl3 = DLog_W_3<T>(kin),dl14 = DLog_W_14<T>(kin),dl7 = DLog_W_7<T>(kin),dl17 = DLog_W_17<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl1 = DLog_W_1<T>(kin),dl16 = DLog_W_16<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),dl20 = DLog_W_20<T>(kin),dl19 = DLog_W_19<T>(kin),dl4 = DLog_W_4<T>(kin),dl5 = DLog_W_5<T>(kin),dl2 = DLog_W_2<T>(kin),dl18 = DLog_W_18<T>(kin),spdl21 = SpDLog_f_4_172_W_21<T>(kin),spdl7 = SpDLog_f_4_172_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);

        std::array<std::complex<T>,41> abbr = 
            {dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl3(t), rlog(-v_path[3] + v_path[0] + v_path[1]), f_2_1_14(kin_path), f_2_1_15(kin_path), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[4] / kin_path.W[4]), dl14(t), rlog(-v_path[1]), rlog(v_path[3]), dl7(t), f_2_1_11(kin_path), -rlog(t), rlog(kin.W[15] / kin_path.W[15]), dl17(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl26(t) / kin_path.SqrtDelta, f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl30(t) / kin_path.SqrtDelta, f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl1(t), dl16(t), dl27(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl20(t), dl19(t), dl4(t), dl5(t), dl2(t), dl18(t)}
;

        auto result = f_4_172_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_172_abbreviated(const std::array<T,41>& abb)
{
using TR = typename T::value_type;
T z[116];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[7];
z[3] = abb[31];
z[4] = abb[35];
z[5] = abb[36];
z[6] = abb[37];
z[7] = abb[39];
z[8] = abb[40];
z[9] = abb[4];
z[10] = abb[5];
z[11] = abb[6];
z[12] = abb[11];
z[13] = abb[12];
z[14] = abb[18];
z[15] = abb[19];
z[16] = abb[24];
z[17] = abb[27];
z[18] = abb[32];
z[19] = abb[33];
z[20] = abb[34];
z[21] = abb[25];
z[22] = abb[26];
z[23] = abb[2];
z[24] = abb[8];
z[25] = abb[14];
z[26] = abb[38];
z[27] = abb[15];
z[28] = bc<TR>[0];
z[29] = abb[20];
z[30] = abb[23];
z[31] = abb[9];
z[32] = abb[10];
z[33] = abb[30];
z[34] = abb[13];
z[35] = abb[17];
z[36] = abb[21];
z[37] = abb[22];
z[38] = abb[28];
z[39] = abb[29];
z[40] = bc<TR>[3];
z[41] = bc<TR>[1];
z[42] = bc<TR>[2];
z[43] = bc<TR>[4];
z[44] = bc<TR>[9];
z[45] = z[1] + -z[10];
z[46] = -z[41] + T(3) * z[42];
z[47] = z[9] / T(2) + -z[46];
z[48] = -z[12] + z[13];
z[49] = -z[48] / T(2);
z[50] = z[11] / T(2);
z[51] = (T(7) * z[45]) / T(3) + -z[47] + -z[49] + z[50];
z[51] = z[6] * z[51];
z[52] = z[10] + z[12];
z[53] = z[14] / T(3) + z[15] / T(6);
z[54] = z[1] / T(3);
z[55] = -z[9] + (T(5) * z[11]) / T(6) + -z[46] + -z[52] / T(6) + z[53] + z[54];
z[55] = z[7] * z[55];
z[56] = z[16] + z[21] + z[22];
z[57] = z[17] + z[30] / T(2);
z[57] = z[56] * z[57];
z[58] = -z[9] + z[11] + -z[45] + z[48];
z[59] = z[29] * z[58];
z[60] = z[20] * z[56];
z[57] = z[57] + z[59] / T(2) + z[60];
z[61] = z[1] + z[11];
z[47] = (T(-5) * z[10]) / T(6) + (T(-7) * z[12]) / T(3) + (T(5) * z[13]) / T(2) + -z[47] + -z[53] + (T(2) * z[61]) / T(3);
z[47] = z[5] * z[47];
z[53] = T(3) * z[1];
z[62] = T(3) * z[11];
z[63] = T(5) * z[9] + z[13] + z[52] + -z[53] + -z[62];
z[64] = z[14] + z[15] / T(2);
z[65] = z[63] / T(4) + -z[64];
z[66] = z[41] + z[65];
z[66] = z[8] * z[66];
z[67] = -z[9] + z[61];
z[48] = -z[10] + -z[48] + z[67];
z[68] = z[2] * z[48];
z[69] = z[18] * z[56];
z[70] = -z[68] + z[69];
z[71] = -z[13] + z[52];
z[72] = -z[1] + z[71];
z[73] = T(3) * z[34];
z[72] = z[72] * z[73];
z[73] = z[9] + z[13];
z[74] = z[11] + z[52];
z[75] = z[1] + z[73] + -z[74];
z[76] = z[33] * z[75];
z[49] = z[10] / T(3) + z[49] + -z[54];
z[49] = z[3] * z[49];
z[77] = T(3) * z[9];
z[78] = T(3) * z[13];
z[79] = z[77] + z[78];
z[54] = (T(5) * z[10]) / T(3) + z[11] + z[12] + z[54] + -z[79];
z[54] = -z[41] + z[54] / T(4) + z[64];
z[54] = z[4] * z[54];
z[80] = z[19] * z[56];
z[81] = (T(3) * z[80]) / T(4);
z[46] = -(z[26] * z[46]);
z[46] = z[46] + z[47] + z[49] + z[51] + z[54] + z[55] + z[57] / T(2) + z[66] + -z[70] / T(4) + z[72] + z[76] + z[81];
z[46] = z[28] * z[46];
z[47] = z[18] + z[19] + z[20];
z[49] = -(z[40] * z[47]);
z[46] = z[46] + T(3) * z[49];
z[46] = z[28] * z[46];
z[49] = T(9) * z[9];
z[51] = T(9) * z[13];
z[54] = T(7) * z[1];
z[55] = -z[49] + -z[51] + z[54] + z[74];
z[57] = T(4) * z[14] + T(2) * z[15];
z[55] = z[55] / T(4) + z[57];
z[55] = z[3] * z[55];
z[66] = T(2) * z[14] + z[15];
z[70] = -z[61] + z[66];
z[82] = T(2) * z[52] + z[70] + -z[78];
z[83] = z[6] * z[82];
z[83] = -z[81] + z[83];
z[84] = T(3) * z[8];
z[65] = z[65] * z[84];
z[65] = -z[65] + (T(3) * z[69]) / T(4);
z[85] = z[5] * z[82];
z[54] = T(5) * z[52] + -z[54];
z[86] = z[77] + -z[78];
z[87] = z[11] + -z[54] + -z[86];
z[87] = z[66] + z[87] / T(2);
z[87] = z[7] * z[87];
z[88] = z[52] + z[61] + -z[79];
z[89] = z[64] + z[88] / T(4);
z[90] = -(z[4] * z[89]);
z[91] = (T(3) * z[76]) / T(4);
z[92] = z[9] + T(5) * z[13] + T(-3) * z[52] + z[61];
z[92] = -z[66] + z[92] / T(2);
z[93] = T(3) * z[92];
z[94] = z[26] * z[93];
z[95] = z[17] * z[56];
z[55] = z[55] + -z[65] + z[72] + z[83] + z[85] + z[87] + z[90] + z[91] + z[94] + (T(-3) * z[95]) / T(4);
z[55] = z[25] * z[55];
z[87] = z[66] + z[88] / T(2);
z[90] = z[5] * z[87];
z[96] = (T(3) * z[76]) / T(2) + z[90];
z[97] = z[6] * z[87];
z[98] = z[4] * z[87];
z[99] = (T(3) * z[80]) / T(2);
z[98] = z[98] + z[99];
z[100] = T(2) * z[1] + z[66];
z[101] = -z[74] + z[100];
z[102] = z[3] * z[101];
z[101] = z[7] * z[101];
z[103] = (T(3) * z[60]) / T(2);
z[102] = -z[96] + -z[97] + -z[98] + T(2) * z[101] + z[102] + -z[103];
z[104] = int_to_imaginary<T>(1) * z[28];
z[105] = z[102] * z[104];
z[55] = z[55] + z[105];
z[55] = z[25] * z[55];
z[105] = z[17] + z[30];
z[105] = z[56] * z[105];
z[59] = z[59] + z[105];
z[105] = z[59] + -z[60];
z[106] = z[7] * z[87];
z[62] = T(3) * z[12] + -z[62] + z[86];
z[86] = -z[45] + z[62];
z[107] = z[86] / T(2);
z[108] = z[3] * z[107];
z[109] = T(2) * z[45];
z[110] = z[6] * z[109];
z[109] = z[4] * z[109];
z[108] = -z[90] + (T(3) * z[105]) / T(2) + z[106] + z[108] + z[109] + z[110];
z[111] = z[23] * z[108];
z[112] = T(-11) * z[45] + z[62];
z[112] = z[6] * z[112];
z[62] = z[45] + z[62];
z[113] = -(z[3] * z[62]);
z[112] = T(-3) * z[59] + z[112] + z[113];
z[72] = -z[72] + z[85];
z[85] = T(5) * z[1];
z[113] = z[74] + z[79] + -z[85];
z[113] = -z[66] + z[113] / T(4);
z[113] = z[7] * z[113];
z[114] = z[4] / T(4);
z[115] = z[86] * z[114];
z[81] = z[81] + z[115];
z[91] = z[72] + -z[81] + -z[91] + z[112] / T(4) + z[113];
z[91] = z[27] * z[91];
z[98] = z[98] + z[106];
z[82] = T(2) * z[82];
z[106] = z[5] + z[6];
z[82] = z[82] * z[106];
z[87] = z[3] * z[87];
z[82] = z[82] + z[87] + z[94] + (T(-3) * z[95]) / T(2) + -z[98];
z[94] = z[25] * z[82];
z[62] = z[6] * z[62];
z[112] = T(3) * z[60] + -z[62];
z[107] = z[4] * z[107];
z[113] = z[3] * z[45];
z[101] = z[101] + z[113];
z[96] = z[96] + z[99] + -z[101] + z[107] + z[112] / T(2);
z[107] = z[24] * z[96];
z[112] = z[0] * z[82];
z[115] = -(z[4] * z[45]);
z[72] = -z[72] + z[101] + z[110] + z[115];
z[72] = z[72] * z[104];
z[72] = T(2) * z[72] + z[91] + -z[94] + z[107] + z[111] + z[112];
z[72] = z[27] * z[72];
z[91] = z[4] + z[7];
z[101] = -(z[89] * z[91]);
z[107] = (T(3) * z[68]) / T(4);
z[60] = -z[60] + z[95];
z[61] = z[12] + z[61];
z[95] = T(-5) * z[10] + z[61] + z[79];
z[95] = -z[66] + z[95] / T(4);
z[95] = z[3] * z[95];
z[85] = T(5) * z[11] + z[85];
z[79] = T(-7) * z[10] + T(5) * z[12] + -z[79] + z[85];
z[79] = -z[64] + z[79] / T(4);
z[79] = z[5] * z[79];
z[65] = (T(-3) * z[60]) / T(4) + z[65] + z[79] + z[83] + z[95] + z[101] + z[107];
z[65] = z[0] * z[65];
z[70] = T(2) * z[10] + -z[12] + z[70];
z[79] = z[5] * z[70];
z[70] = -(z[3] * z[70]);
z[70] = (T(-3) * z[68]) / T(2) + z[70] + T(-2) * z[79] + z[97] + z[98];
z[70] = z[24] * z[70];
z[82] = -(z[82] * z[104]);
z[65] = z[65] + z[70] + z[82] + -z[94];
z[65] = z[0] * z[65];
z[68] = z[68] + -z[80];
z[67] = z[67] + -z[71];
z[70] = z[3] * z[67];
z[71] = -(z[6] * z[58]);
z[82] = -(z[5] * z[48]);
z[83] = z[4] * z[67];
z[71] = z[68] + -z[70] + z[71] + z[82] + z[83] + z[105];
z[71] = z[31] * z[71];
z[82] = z[25] * z[102];
z[83] = z[3] * z[86];
z[62] = z[62] + z[83] + T(3) * z[105];
z[83] = -(z[5] * z[89]);
z[50] = z[1] + -z[50] + -z[52] / T(2) + z[64];
z[50] = z[7] * z[50];
z[50] = z[50] + z[62] / T(4) + -z[81] + z[83] + z[107];
z[50] = z[24] * z[50];
z[62] = -(z[96] * z[104]);
z[50] = z[50] + z[62] + z[82];
z[50] = z[24] * z[50];
z[62] = T(2) * z[11] + -z[52] + -z[77] + z[100];
z[64] = T(2) * z[62];
z[64] = z[64] * z[91];
z[63] = z[63] / T(2) + -z[66];
z[81] = z[63] * z[84];
z[56] = (T(3) * z[56]) / T(2);
z[56] = z[18] * z[56];
z[56] = z[56] + -z[81];
z[64] = -z[56] + (T(3) * z[60]) / T(2) + z[64] + z[87] + -z[90] + -z[97];
z[81] = z[0] + -z[25];
z[64] = z[64] * z[81];
z[81] = -z[24] + -z[104];
z[81] = z[81] * z[108];
z[62] = -(z[7] * z[62]);
z[45] = z[6] * z[45];
z[45] = z[45] + z[62] + z[79] + -z[109] + -z[113];
z[45] = z[23] * z[45];
z[45] = z[45] + z[64] + z[81];
z[45] = z[23] * z[45];
z[62] = z[7] * z[75];
z[64] = z[6] * z[67];
z[59] = z[59] + -z[62] + z[64] + -z[70] + z[76] + z[80];
z[62] = -(z[4] * z[58]);
z[62] = z[59] + z[62];
z[62] = z[37] * z[62];
z[64] = z[60] + -z[76];
z[53] = z[53] + -z[73] + -z[74];
z[53] = z[53] / T(2) + z[66];
z[53] = z[7] * z[53];
z[67] = z[3] / T(2);
z[67] = z[67] * z[75];
z[53] = z[53] + z[64] / T(2) + z[67];
z[64] = -z[26] + z[106];
z[67] = -(z[64] * z[92]);
z[67] = -z[53] + z[67];
z[67] = z[39] * z[67];
z[64] = z[4] + -z[7] + -z[8] + z[64];
z[70] = z[41] / T(2) + -z[42];
z[70] = z[41] * z[70];
z[70] = z[43] + z[70];
z[64] = z[64] * z[70];
z[64] = z[64] + z[67];
z[67] = -z[6] + z[26];
z[47] = z[8] / T(4) + z[47] / T(9) + (T(3) * z[67]) / T(4) + -z[114];
z[47] = prod_pow(z[28], 2) * z[47];
z[47] = z[47] + (T(3) * z[62]) / T(2) + T(3) * z[64];
z[47] = z[47] * z[104];
z[51] = z[51] + T(-7) * z[52] + -z[77] + z[85];
z[51] = z[51] / T(2) + -z[66];
z[51] = -(z[51] * z[106]);
z[52] = z[57] + z[88];
z[52] = z[3] * z[52];
z[49] = T(-7) * z[11] + z[49] + z[54] + -z[78];
z[49] = z[49] / T(2) + -z[66];
z[54] = -(z[7] * z[49]);
z[51] = z[51] + z[52] + z[54] + -z[56] + -z[99] + -z[103];
z[51] = z[35] * z[51];
z[52] = (T(3) * z[36]) / T(2);
z[54] = -(z[52] * z[59]);
z[48] = -(z[3] * z[48]);
z[48] = z[48] + -z[60] + z[68] + z[69];
z[56] = z[7] * z[63];
z[57] = T(3) * z[10] + -z[61] + -z[73];
z[57] = z[57] / T(2) + z[66];
z[57] = z[5] * z[57];
z[48] = z[48] / T(2) + z[56] + z[57];
z[56] = T(3) * z[32];
z[48] = z[48] * z[56];
z[57] = -(z[92] * z[106]);
z[53] = -z[53] + z[57];
z[53] = z[38] * z[53];
z[56] = z[56] * z[63];
z[56] = (T(5) * z[44]) / T(2) + -z[56];
z[49] = -(z[35] * z[49]);
z[52] = z[52] * z[58];
z[49] = z[49] + z[52] + -z[56];
z[49] = z[4] * z[49];
z[52] = z[8] * z[56];
z[56] = z[35] + z[38];
z[56] = z[56] * z[93];
z[56] = z[44] + z[56];
z[56] = z[26] * z[56];
z[57] = -z[5] + z[7];
z[57] = -z[6] + (T(85) * z[57]) / T(6);
z[57] = z[44] * z[57];
return z[45] + z[46] + z[47] + z[48] + z[49] + z[50] + z[51] + z[52] + T(3) * z[53] + z[54] + z[55] + z[56] + z[57] + z[65] + (T(3) * z[71]) / T(2) + z[72];
}



template IntegrandConstructorType<double> f_4_172_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_172_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_172_construct (const Kin<qd_real>&);
#endif

}