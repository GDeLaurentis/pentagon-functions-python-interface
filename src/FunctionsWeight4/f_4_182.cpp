#include "f_4_182.h"

namespace PentagonFunctions {

template <typename T> T f_4_182_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_182_W_7 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_182_W_7 (const Kin<T>& kin) {
        c[0] = -(kin.v[1] * kin.v[4]) + ((T(3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + ((T(3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + -kin.v[1] + T(-1)) * kin.v[3];
c[1] = kin.v[3] + kin.v[4];
c[2] = -(kin.v[1] * kin.v[4]) + ((T(3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + ((T(3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + -kin.v[1] + T(-1)) * kin.v[3];
c[3] = kin.v[3] + kin.v[4];
c[4] = -(kin.v[1] * kin.v[4]) + ((T(3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + ((T(3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + -kin.v[1] + T(-1)) * kin.v[3];
c[5] = kin.v[3] + kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[4] * (t * c[0] + c[1]) + abb[5] * (t * c[2] + c[3]) + abb[6] * (t * c[4] + c[5]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * (abb[5] + abb[6]) + abb[4] * (prod_pow(abb[2], 2) + -abb[3]) + abb[3] * (-abb[5] + -abb[6]) + prod_pow(abb[1], 2) * (-abb[4] + -abb[5] + -abb[6]));
    }
};
template <typename T> class SpDLog_f_4_182_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_182_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) + kin.v[0] + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4]);
c[1] = kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) + kin.v[0] + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4]);
c[2] = kin.v[0] * (-kin.v[0] + T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4]);
c[3] = kin.v[0] * (-kin.v[0] + T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[6] * c[0] + abb[9] * c[1] + abb[4] * c[2] + abb[5] * c[3]);
        }

        return abb[7] * (prod_pow(abb[8], 2) * (abb[6] + abb[9] + -abb[5]) + -(abb[4] * prod_pow(abb[8], 2)) + prod_pow(abb[2], 2) * (abb[4] + abb[5] + -abb[6] + -abb[9]));
    }
};
template <typename T> class SpDLog_f_4_182_W_22 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_182_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(4) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (-kin.v[4] + T(4) + T(-3) * kin.v[0] + (T(-3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[1] + kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[0] + T(1) + kin.v[3])) + ((T(5) * kin.v[4]) / T(2) + T(-4)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + T(-4) + T(3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]));
c[1] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + -kin.v[4] + kin.v[2]) + T(-2) * kin.v[4];
c[2] = kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(4) + T(-2) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[1] * (-kin.v[4] + T(4) + T(-3) * kin.v[0] + (T(-3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[1] + kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[0] + T(1) + kin.v[3])) + ((T(5) * kin.v[4]) / T(2) + T(-4)) * kin.v[4] + kin.v[3] * (kin.v[3] / T(2) + T(-4) + T(3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[2] / T(2) + -kin.v[4] + T(1)) * kin.v[2] + (-kin.v[3] / T(2) + T(-1)) * kin.v[3] + (kin.v[4] / T(2) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]));
c[3] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + -kin.v[4] + kin.v[2]) + T(-2) * kin.v[4];
c[4] = kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[3] * (-kin.v[3] / T(2) + T(4) + T(-3) * kin.v[4]) + ((T(-5) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + kin.v[1] * (-kin.v[3] + T(-4) + T(3) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + T(-1) + kin.v[0]) + (T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[1] + kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-4) + T(2) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]));
c[5] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4]);
c[6] = kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + kin.v[3] * (-kin.v[3] / T(2) + T(4) + T(-3) * kin.v[4]) + ((T(-5) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + kin.v[1] * (-kin.v[3] + T(-4) + T(3) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[3] + T(-1) + kin.v[0]) + (T(3) / T(2) + bc<T>[0] * (int_to_imaginary<T>(1) / T(2))) * kin.v[1] + kin.v[4]) + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-4) + T(2) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]));
c[7] = (T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[5] * (t * c[0] + c[1]) + abb[6] * (t * c[2] + c[3]) + abb[21] * (t * c[4] + c[5]) + abb[9] * (t * c[6] + c[7]);
        }

        return abb[25] * (abb[2] * abb[8] * (abb[5] + abb[6] + -abb[9]) + abb[8] * abb[23] * (abb[5] + abb[6] + -abb[9]) + abb[1] * (abb[8] * abb[21] + abb[8] * (abb[9] + -abb[5] + -abb[6]) + abb[24] * (abb[5] + abb[6] + -abb[9] + -abb[21])) + abb[24] * (abb[2] * (abb[9] + -abb[5] + -abb[6]) + abb[8] * (abb[9] + -abb[5] + -abb[6]) + abb[23] * (abb[9] + -abb[5] + -abb[6]) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[21] * (abb[2] + abb[8] + abb[23] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[8] * (abb[8] * (abb[5] + abb[6] + -abb[9]) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[21] * (-(abb[2] * abb[8]) + -(abb[8] * abb[23]) + abb[26] * T(-2) + abb[8] * (-abb[8] + bc<T>[0] * int_to_imaginary<T>(1))) + abb[26] * (abb[9] * T(-2) + abb[5] * T(2) + abb[6] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_182_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl21 = DLog_W_21<T>(kin),dl27 = DLog_W_27<T>(kin),dl18 = DLog_W_18<T>(kin),dl9 = DLog_W_9<T>(kin),dl22 = DLog_W_22<T>(kin),dl14 = DLog_W_14<T>(kin),dl5 = DLog_W_5<T>(kin),dl1 = DLog_W_1<T>(kin),dl31 = DLog_W_31<T>(kin),dl3 = DLog_W_3<T>(kin),dl17 = DLog_W_17<T>(kin),dl2 = DLog_W_2<T>(kin),dl4 = DLog_W_4<T>(kin),dl16 = DLog_W_16<T>(kin),dl20 = DLog_W_20<T>(kin),dl19 = DLog_W_19<T>(kin),dl30 = DLog_W_30<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),dl26 = DLog_W_26<T>(kin),spdl7 = SpDLog_f_4_182_W_7<T>(kin),spdl21 = SpDLog_f_4_182_W_21<T>(kin),spdl22 = SpDLog_f_4_182_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,46> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_11(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl21(t), rlog(-v_path[2] + v_path[0] + v_path[4]), f_1_3_5(kin) - f_1_3_5(kin_path), dl27(t) / kin_path.SqrtDelta, rlog(kin.W[2] / kin_path.W[2]), f_2_1_15(kin_path), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl18(t), f_1_3_1(kin) - f_1_3_1(kin_path), dl9(t), rlog(v_path[3]), rlog(-v_path[3] + v_path[0] + v_path[1]), dl22(t), f_2_1_14(kin_path), dl14(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl5(t), dl1(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl31(t), dl3(t), dl17(t), dl2(t), dl4(t), dl16(t), dl20(t), dl19(t), dl30(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta}
;

        auto result = f_4_182_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_182_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[142];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[20];
z[3] = abb[37];
z[4] = abb[5];
z[5] = abb[27];
z[6] = abb[30];
z[7] = abb[31];
z[8] = abb[39];
z[9] = abb[40];
z[10] = abb[6];
z[11] = abb[38];
z[12] = abb[41];
z[13] = abb[9];
z[14] = abb[10];
z[15] = abb[11];
z[16] = abb[13];
z[17] = abb[14];
z[18] = abb[15];
z[19] = abb[16];
z[20] = abb[17];
z[21] = abb[18];
z[22] = abb[19];
z[23] = abb[42];
z[24] = abb[43];
z[25] = abb[21];
z[26] = abb[2];
z[27] = abb[8];
z[28] = abb[23];
z[29] = abb[24];
z[30] = bc<TR>[0];
z[31] = abb[44];
z[32] = abb[35];
z[33] = abb[3];
z[34] = abb[12];
z[35] = abb[36];
z[36] = abb[26];
z[37] = abb[28];
z[38] = abb[29];
z[39] = abb[32];
z[40] = abb[33];
z[41] = abb[22];
z[42] = abb[45];
z[43] = abb[34];
z[44] = bc<TR>[3];
z[45] = bc<TR>[1];
z[46] = bc<TR>[2];
z[47] = bc<TR>[4];
z[48] = bc<TR>[9];
z[49] = (T(3) * z[40]) / T(2);
z[50] = T(2) * z[28];
z[51] = z[49] + -z[50];
z[52] = z[26] / T(2);
z[53] = (T(3) * z[38]) / T(2);
z[54] = z[51] + -z[52] + z[53];
z[55] = z[27] / T(2);
z[56] = z[0] + z[29];
z[57] = z[54] + -z[55] + -z[56];
z[58] = int_to_imaginary<T>(1) * z[30];
z[57] = z[57] * z[58];
z[59] = z[29] / T(2);
z[60] = -z[0] + z[28];
z[61] = z[59] + -z[60];
z[62] = z[52] + z[61];
z[62] = z[29] * z[62];
z[57] = z[57] + -z[62];
z[62] = z[28] / T(2);
z[63] = z[0] + -z[26];
z[64] = z[62] + -z[63];
z[65] = z[62] * z[64];
z[52] = -z[0] + z[52];
z[52] = z[0] * z[52];
z[66] = prod_pow(z[26], 2);
z[67] = z[66] / T(4);
z[52] = z[52] + -z[65] + -z[67];
z[65] = z[33] / T(2);
z[68] = -z[28] + z[29];
z[69] = z[27] * z[68];
z[70] = -z[65] + z[69] / T(2);
z[71] = (T(3) * z[39]) / T(2);
z[72] = z[34] / T(2);
z[73] = (T(3) * z[37]) / T(2);
z[74] = prod_pow(z[30], 2);
z[75] = z[74] / T(6);
z[76] = z[36] + -z[52] + z[57] + -z[70] + -z[71] + z[72] + z[73] + -z[75];
z[76] = z[3] * z[76];
z[77] = T(2) * z[26];
z[78] = -z[0] + z[77];
z[78] = z[0] * z[78];
z[79] = (T(3) * z[33]) / T(2);
z[80] = -z[73] + z[78] + -z[79];
z[81] = T(2) * z[63];
z[82] = -z[28] + z[81];
z[82] = z[28] * z[82];
z[83] = z[80] + z[82];
z[84] = z[0] / T(2);
z[77] = z[77] + -z[84];
z[85] = z[29] / T(4);
z[86] = z[62] + z[77] + -z[85];
z[86] = z[29] * z[86];
z[87] = z[55] + -z[59];
z[53] = -z[53] + z[77];
z[77] = z[50] + z[53];
z[88] = z[77] + z[87];
z[88] = z[58] * z[88];
z[68] = z[63] + z[68];
z[89] = -z[27] + z[68] / T(2);
z[89] = z[27] * z[89];
z[90] = (T(5) * z[36]) / T(2);
z[89] = z[89] + -z[90];
z[91] = (T(3) * z[34]) / T(2);
z[86] = (T(-3) * z[66]) / T(4) + (T(5) * z[74]) / T(6) + z[83] + z[86] + z[88] + z[89] + -z[91];
z[86] = z[12] * z[86];
z[80] = -z[66] + z[80];
z[88] = -z[28] + z[63];
z[92] = z[59] + z[88];
z[93] = z[59] * z[92];
z[94] = z[39] / T(2);
z[95] = z[36] / T(2);
z[96] = z[27] * z[63];
z[97] = -z[28] / T(4) + z[81];
z[97] = z[28] * z[97];
z[97] = z[80] + -z[93] + -z[94] + -z[95] + z[96] / T(2) + z[97];
z[97] = z[11] * z[97];
z[98] = prod_pow(z[28], 2);
z[99] = z[98] / T(2);
z[100] = prod_pow(z[29], 2);
z[101] = z[36] + -z[39] + z[100] / T(2);
z[102] = z[69] + z[99] + -z[101];
z[103] = z[35] * z[102];
z[64] = z[28] * z[64];
z[84] = -z[26] + z[84];
z[84] = z[0] * z[84];
z[104] = z[66] / T(2);
z[105] = z[34] + z[104];
z[106] = z[64] + z[84] + z[105];
z[92] = z[29] * z[92];
z[107] = z[92] + z[106];
z[108] = z[74] / T(2);
z[109] = z[33] + z[36] + z[39] + z[107] + -z[108];
z[110] = z[29] + -z[40] + z[63];
z[111] = -(z[58] * z[110]);
z[111] = -z[109] + z[111];
z[111] = z[9] * z[111];
z[111] = z[103] + z[111];
z[112] = T(3) * z[39];
z[64] = -z[64] + z[112];
z[113] = T(3) * z[34] + -z[104];
z[84] = z[64] + -z[84] + z[113];
z[114] = z[74] / T(3);
z[115] = z[73] + -z[114];
z[88] = T(2) * z[88];
z[116] = (T(-5) * z[29]) / T(4) + -z[88];
z[116] = z[29] * z[116];
z[89] = -z[84] / T(2) + z[89] + z[115] + z[116];
z[89] = z[8] * z[89];
z[116] = z[40] / T(2) + z[53] + -z[59];
z[116] = z[11] * z[116];
z[56] = -z[38] + z[56];
z[117] = -z[40] + z[56];
z[118] = z[7] * z[117];
z[119] = z[41] * z[50];
z[120] = -z[27] + z[40];
z[121] = z[35] * z[120];
z[116] = z[116] + (T(3) * z[118]) / T(2) + z[119] + -z[121] / T(2);
z[116] = z[30] * z[116];
z[122] = T(2) * z[29];
z[54] = -z[0] + z[54] + z[55] + -z[122];
z[55] = z[8] * z[30];
z[54] = z[54] * z[55];
z[116] = z[54] + z[116];
z[116] = int_to_imaginary<T>(1) * z[116];
z[123] = -z[26] + z[38];
z[124] = z[58] * z[123];
z[125] = z[28] * z[63];
z[126] = z[0] * z[63];
z[124] = -z[33] + -z[37] + -z[124] + z[125] + -z[126];
z[124] = z[6] * z[124];
z[125] = (T(3) * z[124]) / T(2);
z[127] = z[29] * z[60];
z[99] = z[39] + z[99];
z[128] = prod_pow(z[0], 2);
z[127] = z[37] + -z[99] + z[127] + z[128] / T(2);
z[129] = z[7] * z[127];
z[130] = -z[66] + z[128];
z[131] = z[33] + -z[34];
z[132] = -z[96] + z[130] / T(2) + z[131];
z[133] = z[75] + z[132];
z[134] = z[2] / T(2);
z[135] = z[133] * z[134];
z[136] = z[98] + -z[100];
z[137] = z[41] * z[136];
z[138] = -z[7] + z[11] / T(6) + -z[35] / T(12) + z[41];
z[138] = z[74] * z[138];
z[139] = -z[26] + z[59];
z[139] = z[29] * z[139];
z[139] = z[105] + z[139];
z[140] = z[36] + z[139];
z[140] = z[108] + T(3) * z[140];
z[141] = z[32] / T(2);
z[140] = z[140] * z[141];
z[76] = z[76] + z[86] + z[89] + z[97] + z[111] / T(2) + z[116] + -z[125] + (T(-3) * z[129]) / T(2) + z[135] + -z[137] + z[138] + z[140];
z[76] = z[10] * z[76];
z[62] = z[62] + z[63];
z[86] = z[27] + -z[59] + z[62];
z[86] = z[27] * z[86];
z[86] = z[36] + z[86];
z[89] = -z[26] + z[29];
z[97] = -(z[59] * z[89]);
z[111] = -z[120] + z[123];
z[116] = z[111] / T(2);
z[138] = z[58] * z[116];
z[140] = z[37] / T(2);
z[52] = -z[52] + z[79] + -z[86] + -z[91] + z[94] + z[97] + z[138] + z[140];
z[52] = z[3] * z[52];
z[77] = z[77] + -z[87];
z[77] = z[58] * z[77];
z[68] = z[27] * z[68];
z[87] = z[68] / T(2) + -z[95];
z[91] = z[59] * z[61];
z[67] = -z[67] + -z[72] + z[74] + z[77] + z[83] + -z[87] + z[91];
z[67] = z[12] * z[67];
z[72] = z[37] + z[68];
z[77] = z[72] + -z[101] + z[106];
z[83] = z[77] + -z[114];
z[91] = z[8] / T(2);
z[83] = z[83] * z[91];
z[91] = z[7] / T(2);
z[94] = z[91] * z[127];
z[95] = -z[11] / T(3) + z[35] / T(4);
z[97] = -z[7] / T(3) + -z[41] + z[95];
z[97] = z[74] * z[97];
z[83] = z[83] + -z[94] + z[97] + (T(-3) * z[103]) / T(2) + z[137];
z[94] = z[63] / T(2);
z[97] = -z[27] + -z[50] + z[94] + z[122];
z[97] = z[27] * z[97];
z[81] = (T(-3) * z[28]) / T(4) + z[81];
z[81] = z[28] * z[81];
z[71] = -z[71] + z[80] + z[81] + z[90] + z[93] + -z[97];
z[71] = z[11] * z[71];
z[80] = T(5) * z[36];
z[81] = z[80] + z[92];
z[84] = -z[81] + z[84];
z[90] = T(2) * z[27];
z[59] = -z[59] + z[90];
z[51] = -z[51] + z[59] + -z[94];
z[51] = z[51] * z[58];
z[68] = T(2) * z[68];
z[79] = z[51] + z[68] + (T(7) * z[74]) / T(12) + -z[79] + z[84] / T(2);
z[79] = z[9] * z[79];
z[84] = -z[0] + z[38];
z[50] = z[50] + z[84];
z[50] = z[50] * z[58];
z[50] = z[37] + z[50] + (T(7) * z[74]) / T(6) + -z[98] + z[128];
z[50] = z[5] * z[50];
z[92] = z[108] + T(3) * z[132];
z[92] = z[92] * z[134];
z[50] = z[50] + z[79] + z[92];
z[79] = z[91] * z[117];
z[79] = z[79] + -z[119] + (T(3) * z[121]) / T(2);
z[53] = z[49] + z[53] + -z[59];
z[53] = z[11] * z[53];
z[53] = z[53] + -z[79];
z[53] = z[30] * z[53];
z[59] = z[55] * z[116];
z[53] = z[53] + -z[59];
z[53] = int_to_imaginary<T>(1) * z[53];
z[91] = z[36] + z[75];
z[92] = z[91] + z[139];
z[92] = z[92] * z[141];
z[52] = -z[50] + z[52] + z[53] + z[67] + z[71] + -z[83] + z[92] + -z[125];
z[52] = z[13] * z[52];
z[53] = z[29] * z[61];
z[53] = z[36] + z[53] + -z[72] + -z[104] + z[131];
z[56] = z[27] + -z[56];
z[61] = z[56] * z[58];
z[61] = -z[53] + z[61] + z[114];
z[67] = z[12] / T(2);
z[61] = z[61] * z[67];
z[67] = -z[37] + -z[81] + T(3) * z[99];
z[67] = z[65] + z[67] / T(2) + z[97];
z[67] = z[11] * z[67];
z[61] = z[61] + z[67] + -z[92] + z[124] / T(2);
z[64] = -z[64] + z[113] + z[126];
z[57] = z[57] + z[64] / T(2) + -z[65] + z[86] + z[115];
z[57] = z[3] * z[57];
z[64] = -z[103] + -z[129];
z[67] = z[106] + -z[112];
z[60] = T(2) * z[60];
z[71] = z[60] + -z[85];
z[71] = z[29] * z[71];
z[67] = z[67] / T(2) + z[71] + z[73] + z[87];
z[67] = z[8] * z[67];
z[71] = z[29] + z[84];
z[49] = z[49] + z[71] / T(2) + -z[90];
z[49] = z[11] * z[49];
z[71] = z[118] + z[121];
z[71] = -z[49] + (T(3) * z[71]) / T(2);
z[71] = z[30] * z[71];
z[54] = z[54] + z[71];
z[54] = int_to_imaginary<T>(1) * z[54];
z[71] = -z[7] + z[95];
z[71] = z[71] * z[74];
z[50] = z[50] + z[54] + z[57] + z[61] + (T(3) * z[64]) / T(2) + z[67] + z[71];
z[50] = z[4] * z[50];
z[54] = z[80] + z[107] + -z[112];
z[57] = T(2) * z[69];
z[51] = -z[51] + z[54] / T(2) + -z[57] + z[65] + (T(-11) * z[74]) / T(12);
z[51] = z[9] * z[51];
z[54] = z[28] * z[62];
z[62] = -z[120] + -z[123];
z[58] = z[58] * z[62];
z[62] = -(z[29] * z[89]);
z[54] = z[39] + z[54] + z[58] + z[62] + -z[105] + -z[126];
z[54] = z[54] / T(2) + z[70] + -z[91] + -z[140];
z[54] = z[3] * z[54];
z[49] = z[49] + -z[79];
z[49] = z[30] * z[49];
z[49] = z[49] + -z[59];
z[49] = int_to_imaginary<T>(1) * z[49];
z[49] = z[49] + z[51] + z[54] + -z[61] + -z[83] + -z[135];
z[49] = z[25] * z[49];
z[51] = z[31] * z[53];
z[53] = z[24] * z[109];
z[54] = z[42] * z[102];
z[58] = z[14] * z[132];
z[59] = z[23] * z[77];
z[51] = z[51] + z[53] + z[54] + z[58] + z[59];
z[53] = z[23] + z[31];
z[54] = -z[14] + z[42];
z[58] = -z[53] + -z[54] / T(2);
z[59] = z[58] * z[114];
z[59] = z[51] + z[59];
z[61] = z[15] + z[16];
z[61] = z[61] / T(2);
z[62] = z[59] * z[61];
z[64] = z[23] * z[111];
z[65] = z[42] * z[120];
z[56] = z[31] * z[56];
z[56] = z[56] + -z[64] + z[65];
z[64] = z[30] * z[56];
z[65] = z[24] * z[30];
z[67] = z[65] * z[110];
z[64] = z[64] + -z[67];
z[67] = int_to_imaginary<T>(1) * z[64];
z[51] = z[51] + -z[67];
z[58] = z[58] * z[74];
z[51] = T(3) * z[51] + z[58];
z[58] = z[19] + z[20] + z[21] + z[22];
z[58] = -z[58] / T(2);
z[51] = z[51] * z[58];
z[58] = -z[59] + z[67];
z[58] = z[18] * z[58];
z[59] = T(2) * z[39];
z[67] = T(2) * z[34];
z[69] = -z[59] + -z[66] + -z[67] + z[78] + z[82];
z[70] = T(2) * z[36];
z[68] = T(-2) * z[37] + -z[68] + z[70];
z[71] = -z[68] + -z[69] + -z[100];
z[71] = z[23] * z[71];
z[72] = T(2) * z[33];
z[67] = -z[67] + z[72];
z[60] = z[29] + -z[60];
z[60] = z[29] * z[60];
z[60] = z[60] + -z[66] + z[67] + z[68];
z[60] = z[31] * z[60];
z[57] = z[57] + z[59] + -z[70] + z[136];
z[57] = z[42] * z[57];
z[59] = z[67] + T(-2) * z[96] + z[130];
z[59] = z[14] * z[59];
z[57] = z[57] + z[59] + z[60] + z[71];
z[57] = z[17] * z[57];
z[59] = T(2) * z[17];
z[56] = -(z[56] * z[59]);
z[60] = prod_pow(z[45], 2);
z[60] = z[47] + z[60] / T(2);
z[67] = z[46] / T(2);
z[68] = z[45] + -z[67];
z[68] = z[46] * z[68];
z[68] = -z[60] + z[68];
z[68] = z[23] * z[68];
z[67] = T(-3) * z[45] + z[67];
z[67] = z[46] * z[67];
z[67] = T(3) * z[60] + z[67];
z[67] = z[31] * z[67];
z[71] = z[45] * z[46];
z[60] = -z[60] + z[71];
z[71] = z[42] * z[60];
z[73] = z[35] + T(4) * z[43];
z[77] = z[11] + -z[73] / T(3);
z[77] = (T(-5) * z[31]) / T(4) + -z[42] / T(4) + z[77] / T(3);
z[77] = z[77] * z[114];
z[56] = z[56] + z[67] + z[68] + z[71] + z[77];
z[56] = z[30] * z[56];
z[61] = -(z[61] * z[64]);
z[59] = z[59] * z[110];
z[59] = z[59] + T(-3) * z[60] + -z[108];
z[59] = z[59] * z[65];
z[60] = prod_pow(z[30], 3);
z[64] = z[8] * z[60];
z[56] = z[56] + z[59] + z[61] + z[64] / T(9);
z[56] = int_to_imaginary<T>(1) * z[56];
z[59] = z[29] + z[88];
z[59] = z[29] * z[59];
z[59] = z[59] + -z[69] + z[70] + z[72];
z[59] = z[17] * z[59];
z[61] = -z[17] + -z[45] + z[46];
z[61] = z[61] * z[74];
z[59] = -z[48] / T(2) + z[59] + z[61];
z[59] = z[24] * z[59];
z[53] = T(-2) * z[53] + -z[54];
z[53] = z[17] * z[53];
z[54] = z[45] / T(3);
z[61] = -z[46] / T(4) + z[54];
z[61] = z[23] * z[61];
z[64] = -z[45] + (T(5) * z[46]) / T(4);
z[64] = z[31] * z[64];
z[54] = z[46] + z[54];
z[54] = z[42] * z[54];
z[53] = z[53] / T(3) + z[54] + z[61] + z[64];
z[53] = z[53] * z[74];
z[54] = -z[66] + -z[128];
z[61] = z[27] + z[63];
z[61] = z[27] * z[61];
z[54] = z[54] / T(2) + z[61] + -z[75] + -z[131];
z[54] = z[1] * z[54];
z[61] = z[30] * z[44];
z[60] = int_to_imaginary<T>(1) * z[60];
z[54] = z[54] + (T(2) * z[60]) / T(27) + T(-2) * z[61];
z[54] = z[3] * z[54];
z[63] = z[1] * z[2] * z[133];
z[64] = z[34] + z[96];
z[64] = -z[33] + T(2) * z[64] + -z[114];
z[64] = z[1] * z[64];
z[65] = z[60] / T(9);
z[64] = z[64] + z[65];
z[64] = z[9] * z[64];
z[66] = z[9] + z[11];
z[66] = T(-3) * z[66] + z[73];
z[66] = z[30] * z[66];
z[55] = T(-3) * z[55] + z[66];
z[55] = z[44] * z[55];
z[60] = -z[60] / T(27) + z[61];
z[60] = z[32] * z[60];
z[61] = T(-3) * z[61] + z[65];
z[61] = z[12] * z[61];
z[65] = (T(-61) * z[23]) / T(3) + T(-9) * z[31];
z[65] = (T(-8) * z[42]) / T(3) + z[65] / T(8);
z[65] = z[48] * z[65];
return z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + z[76];
}



template IntegrandConstructorType<double> f_4_182_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_182_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_182_construct (const Kin<qd_real>&);
#endif

}