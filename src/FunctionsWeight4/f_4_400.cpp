#include "f_4_400.h"

namespace PentagonFunctions {

template <typename T> T f_4_400_abbreviated (const std::array<T,47>&);

template <typename T> class SpDLog_f_4_400_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_400_W_12 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(12) + T(6) * kin.v[1]) + kin.v[4] * (T(-12) + T(-6) * kin.v[4]);
c[1] = kin.v[1] * (T(-12) + T(-6) * kin.v[1]) + kin.v[4] * (T(12) + T(6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[4] * c[0] + abb[3] * c[1]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[4] * T(-6) + prod_pow(abb[2], 2) * abb[3] * T(6) + prod_pow(abb[1], 2) * (abb[3] * T(-6) + abb[4] * T(6)));
    }
};
template <typename T> class SpDLog_f_4_400_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_400_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (T(-12) + T(12) * kin.v[0] + T(6) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[0] * (T(12) * kin.v[2] + T(-12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(-6) * kin.v[4]) + kin.v[2] * (T(-12) + T(-6) * kin.v[2] + T(12) * kin.v[4]);
c[1] = kin.v[1] * (T(-12) + T(12) * kin.v[0] + T(6) * kin.v[1] + T(-12) * kin.v[3]) + kin.v[3] * (T(12) + T(6) * kin.v[3]) + kin.v[0] * (T(12) * kin.v[2] + T(-12) * kin.v[3] + T(-12) * kin.v[4]) + kin.v[4] * (T(12) + T(-6) * kin.v[4]) + kin.v[2] * (T(-12) + T(-6) * kin.v[2] + T(12) * kin.v[4]);
c[2] = kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[1] * (T(12) + T(-12) * kin.v[0] + T(-6) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[0] * (T(-12) * kin.v[2] + T(12) * kin.v[3] + T(12) * kin.v[4]);
c[3] = kin.v[3] * (T(-12) + T(-6) * kin.v[3]) + kin.v[1] * (T(12) + T(-12) * kin.v[0] + T(-6) * kin.v[1] + T(12) * kin.v[3]) + kin.v[2] * (T(12) + T(6) * kin.v[2] + T(-12) * kin.v[4]) + kin.v[4] * (T(-12) + T(6) * kin.v[4]) + kin.v[0] * (T(-12) * kin.v[2] + T(12) * kin.v[3] + T(12) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[4] * c[0] + abb[8] * c[1] + abb[3] * c[2] + abb[9] * c[3]);
        }

        return abb[5] * (abb[3] * prod_pow(abb[7], 2) * T(6) + prod_pow(abb[6], 2) * (abb[3] * T(-6) + abb[9] * T(-6) + abb[4] * T(6) + abb[8] * T(6)) + prod_pow(abb[7], 2) * (abb[4] * T(-6) + abb[8] * T(-6) + abb[9] * T(6)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_400_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl22 = DLog_W_22<T>(kin),dl15 = DLog_W_15<T>(kin),dl8 = DLog_W_8<T>(kin),dl9 = DLog_W_9<T>(kin),dl14 = DLog_W_14<T>(kin),dl4 = DLog_W_4<T>(kin),dl28 = DLog_W_28<T>(kin),dl3 = DLog_W_3<T>(kin),dl26 = DLog_W_26<T>(kin),dl2 = DLog_W_2<T>(kin),dl16 = DLog_W_16<T>(kin),dl5 = DLog_W_5<T>(kin),dl20 = DLog_W_20<T>(kin),dl31 = DLog_W_31<T>(kin),dl19 = DLog_W_19<T>(kin),dl17 = DLog_W_17<T>(kin),dl1 = DLog_W_1<T>(kin),dl30 = DLog_W_30<T>(kin),dl29 = DLog_W_29<T>(kin),spdl12 = SpDLog_f_4_400_W_12<T>(kin),spdl22 = SpDLog_f_4_400_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,47> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl15(t), rlog(v_path[2]), dl8(t), dl9(t), rlog(v_path[3]), dl14(t), dl4(t), f_2_1_6(kin_path), f_2_1_14(kin_path), rlog(v_path[0] + v_path[1]), f_2_1_3(kin_path), f_2_1_4(kin_path), rlog(-v_path[1] + v_path[3]), dl28(t) / kin_path.SqrtDelta, rlog(kin.W[0] / kin_path.W[0]), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[30] / kin_path.W[30]), dl3(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl26(t) / kin_path.SqrtDelta, dl2(t), dl16(t), dl5(t), dl20(t), dl31(t), f_2_2_6(kin_path), f_1_3_4(kin_path), f_1_3_5(kin_path), dl19(t), dl17(t), dl1(t), dl30(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta}
;

        auto result = f_4_400_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_400_abbreviated(const std::array<T,47>& abb)
{
using TR = typename T::value_type;
T z[117];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[16];
z[3] = abb[34];
z[4] = abb[36];
z[5] = abb[42];
z[6] = abb[44];
z[7] = abb[4];
z[8] = abb[15];
z[9] = abb[43];
z[10] = abb[9];
z[11] = abb[24];
z[12] = abb[33];
z[13] = abb[27];
z[14] = abb[28];
z[15] = abb[29];
z[16] = abb[2];
z[17] = abb[6];
z[18] = abb[35];
z[19] = abb[14];
z[20] = bc<TR>[0];
z[21] = abb[11];
z[22] = abb[30];
z[23] = abb[23];
z[24] = abb[45];
z[25] = abb[10];
z[26] = abb[37];
z[27] = abb[8];
z[28] = abb[46];
z[29] = abb[7];
z[30] = abb[13];
z[31] = abb[20];
z[32] = abb[21];
z[33] = abb[22];
z[34] = abb[17];
z[35] = abb[18];
z[36] = abb[19];
z[37] = abb[25];
z[38] = abb[26];
z[39] = abb[31];
z[40] = abb[32];
z[41] = abb[12];
z[42] = abb[38];
z[43] = abb[39];
z[44] = abb[40];
z[45] = abb[41];
z[46] = bc<TR>[3];
z[47] = bc<TR>[1];
z[48] = bc<TR>[2];
z[49] = bc<TR>[4];
z[50] = bc<TR>[9];
z[51] = z[1] + z[10];
z[52] = z[5] * z[51];
z[53] = -z[1] + z[7];
z[54] = -z[10] + z[53];
z[55] = z[3] * z[54];
z[56] = z[52] + z[55];
z[57] = T(2) * z[15];
z[58] = z[11] + z[13] + z[14] + -z[57];
z[59] = z[12] * z[58];
z[60] = T(2) * z[59];
z[61] = z[22] * z[51];
z[62] = z[24] * z[58];
z[63] = z[23] * z[58];
z[64] = z[6] * z[54];
z[65] = T(2) * z[51];
z[66] = z[7] + -z[65];
z[66] = z[18] * z[66];
z[66] = z[56] + z[60] + -z[61] + T(2) * z[62] + -z[63] + -z[64] + z[66];
z[66] = z[17] * z[66];
z[67] = z[59] + -z[64];
z[54] = z[18] * z[54];
z[54] = z[54] + z[62];
z[68] = T(2) * z[7];
z[69] = -z[51] + z[68];
z[69] = z[3] * z[69];
z[70] = T(2) * z[63];
z[71] = z[7] * z[22];
z[72] = z[7] * z[9];
z[69] = z[54] + z[67] + z[69] + -z[70] + z[71] + -z[72];
z[69] = z[0] * z[69];
z[73] = z[27] + z[53];
z[74] = z[6] * z[73];
z[75] = z[59] + -z[74];
z[76] = z[9] * z[27];
z[76] = z[72] + z[76];
z[77] = z[7] + z[27];
z[78] = z[2] * z[77];
z[79] = -z[76] + z[78];
z[80] = z[28] * z[58];
z[81] = z[4] * z[73];
z[82] = z[1] + T(-2) * z[27] + -z[68];
z[82] = z[26] * z[82];
z[82] = z[70] + -z[75] + -z[79] + T(2) * z[80] + -z[81] + z[82];
z[82] = z[29] * z[82];
z[83] = z[27] * z[41];
z[84] = z[10] * z[41];
z[83] = z[83] + z[84];
z[85] = z[7] * z[41];
z[86] = z[62] + z[83] + z[85];
z[87] = z[26] * z[73];
z[88] = z[81] + z[87];
z[89] = -z[80] + z[88];
z[90] = -z[1] + z[27];
z[90] = z[25] * z[90];
z[91] = z[3] * z[7];
z[92] = z[18] * z[51];
z[93] = z[10] + z[27];
z[94] = -(z[22] * z[93]);
z[94] = z[86] + -z[89] + z[90] + z[91] + -z[92] + z[94];
z[94] = z[21] * z[94];
z[51] = z[27] + z[51];
z[95] = z[30] * z[51];
z[96] = z[7] + -z[10];
z[97] = z[8] * z[96];
z[98] = z[95] + -z[97];
z[99] = z[1] * z[4];
z[100] = z[98] + z[99];
z[54] = z[54] + z[55];
z[55] = z[26] * z[77];
z[93] = -(z[2] * z[93]);
z[93] = z[54] + -z[55] + z[80] + z[93] + z[100];
z[93] = z[19] * z[93];
z[55] = z[55] + z[79];
z[54] = z[54] + -z[64];
z[79] = z[54] + -z[55] + z[59] + z[80];
z[101] = -(z[36] * z[79]);
z[71] = z[71] + z[91];
z[91] = z[71] + -z[72];
z[89] = -z[75] + -z[89] + z[91];
z[102] = -(z[40] * z[89]);
z[61] = z[61] + z[92];
z[92] = -z[52] + z[61] + -z[62];
z[103] = z[63] + z[80];
z[88] = -z[88] + z[103];
z[104] = z[74] + z[88] + -z[92];
z[105] = -(z[38] * z[104]);
z[106] = -z[5] + -z[9] + T(2) * z[42];
z[107] = z[11] * z[106];
z[106] = -z[6] + z[106];
z[108] = z[13] * z[106];
z[109] = z[14] * z[106];
z[107] = z[107] + z[108] + z[109];
z[57] = z[57] * z[106];
z[108] = z[6] * z[11];
z[57] = z[57] + -z[107] + z[108];
z[108] = z[44] + z[45];
z[108] = T(-2) * z[108];
z[108] = z[57] * z[108];
z[109] = z[1] * z[5];
z[110] = z[1] * z[2];
z[111] = z[109] + -z[110];
z[112] = -z[99] + z[111];
z[113] = z[54] + -z[63] + -z[112];
z[114] = -(z[33] * z[113]);
z[115] = z[24] + z[28];
z[116] = -z[47] + T(2) * z[48];
z[116] = z[47] * z[116];
z[116] = T(-2) * z[49] + z[116];
z[116] = -(z[115] * z[116]);
z[66] = z[66] + z[69] + z[82] + z[93] + z[94] + z[101] + z[102] + z[105] + z[108] + z[114] + z[116];
z[69] = int_to_imaginary<T>(1) * z[20];
z[66] = z[66] * z[69];
z[82] = -z[59] + z[63];
z[93] = -z[82] + z[91] + z[112];
z[93] = z[32] * z[93];
z[89] = z[39] * z[89];
z[79] = z[34] * z[79];
z[92] = -z[59] + z[92];
z[55] = z[55] + -z[103];
z[94] = z[55] + -z[92];
z[94] = z[35] * z[94];
z[101] = z[37] * z[104];
z[57] = -(z[43] * z[57]);
z[102] = -(z[31] * z[113]);
z[57] = z[57] + z[66] + z[79] + z[89] + z[93] + z[94] + z[101] + z[102];
z[66] = z[1] * z[3];
z[64] = z[59] + z[64] + z[66] + -z[72] + -z[97] + -z[99] + T(2) * z[109] + -z[110];
z[64] = z[0] * z[64];
z[54] = z[54] + -z[82];
z[66] = T(2) * z[17];
z[72] = z[54] * z[66];
z[64] = z[64] + z[72];
z[64] = z[0] * z[64];
z[72] = -z[9] + -z[41];
z[72] = z[7] * z[72];
z[73] = z[22] * z[73];
z[72] = z[52] + -z[59] + z[72] + z[73] + T(2) * z[74] + -z[83] + z[88] + -z[90];
z[72] = z[21] * z[72];
z[73] = z[75] + -z[88];
z[75] = z[29] * z[73];
z[79] = z[17] * z[92];
z[63] = z[63] + -z[91];
z[63] = z[0] * z[63];
z[63] = z[63] + z[75] + z[79];
z[63] = T(2) * z[63] + z[72];
z[63] = z[21] * z[63];
z[72] = T(2) * z[1];
z[77] = z[72] + -z[77];
z[77] = z[4] * z[77];
z[60] = -z[60] + z[74] + z[77] + -z[87] + z[103] + -z[111];
z[60] = z[60] * z[69];
z[69] = z[21] * z[73];
z[59] = z[59] + z[112];
z[73] = z[0] * z[59];
z[59] = z[19] * z[59];
z[59] = z[59] + z[60] + z[69] + -z[73] + -z[75];
z[60] = -(z[4] * z[27]);
z[60] = z[60] + z[71] + z[80] + -z[87] + z[90] + -z[110];
z[60] = z[16] * z[60];
z[59] = T(2) * z[59] + z[60];
z[59] = z[16] * z[59];
z[51] = -(z[18] * z[51]);
z[51] = z[51] + z[56] + z[62] + -z[76] + -z[82] + z[95];
z[51] = prod_pow(z[17], 2) * z[51];
z[56] = -(z[10] * z[26]);
z[56] = z[56] + -z[61] + z[78] + -z[81] + z[86];
z[56] = z[29] * z[56];
z[60] = -(z[55] * z[66]);
z[56] = z[56] + z[60];
z[56] = z[29] * z[56];
z[54] = -(z[17] * z[54]);
z[55] = z[29] * z[55];
z[54] = z[54] + z[55] + -z[73];
z[55] = -(z[2] * z[96]);
z[55] = z[55] + z[67] + z[76] + -z[100];
z[55] = z[19] * z[55];
z[54] = T(2) * z[54] + z[55];
z[54] = z[19] * z[54];
z[51] = z[51] + z[54] + z[56] + z[59] + z[63] + z[64];
z[54] = z[84] + z[85];
z[55] = T(-4) * z[47] + T(9) * z[48];
z[56] = z[55] + z[58];
z[56] = z[24] * z[56];
z[58] = -z[9] + T(3) * z[41];
z[58] = z[27] * z[58];
z[53] = T(-4) * z[10] + T(-8) * z[11] + T(-5) * z[27] + -z[53];
z[53] = z[6] * z[53];
z[59] = z[15] * z[106];
z[55] = z[28] * z[55];
z[52] = -z[52] + z[53] + T(3) * z[54] + z[55] + z[56] + z[58] + T(-16) * z[59] + T(8) * z[107];
z[53] = z[90] + z[98];
z[54] = (T(5) * z[10]) / T(2) + z[68] + z[72];
z[54] = z[3] * z[54];
z[55] = (T(-9) * z[10]) / T(2) + (T(-13) * z[27]) / T(2) + -z[68] + z[72];
z[55] = z[22] * z[55];
z[56] = T(-13) * z[10] + T(-9) * z[27];
z[56] = z[56] / T(2) + z[68];
z[56] = z[2] * z[56];
z[58] = z[27] / T(2) + -z[65] + -z[68];
z[58] = z[18] * z[58];
z[59] = T(-4) * z[1] + z[10] / T(2);
z[59] = z[26] * z[59];
z[60] = T(4) * z[7] + (T(9) * z[27]) / T(2) + z[72];
z[60] = z[4] * z[60];
z[52] = T(2) * z[52] + T(6) * z[53] + z[54] + z[55] + z[56] + z[58] + z[59] + z[60] + -z[70];
z[52] = z[20] * z[52];
z[53] = z[46] * z[106];
z[52] = z[52] + T(96) * z[53];
z[52] = z[20] * z[52];
z[53] = z[50] * z[115];
return T(6) * z[51] + z[52] + T(-35) * z[53] + T(12) * z[57];
}



template IntegrandConstructorType<double> f_4_400_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_400_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_400_construct (const Kin<qd_real>&);
#endif

}