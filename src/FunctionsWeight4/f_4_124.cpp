#include "f_4_124.h"

namespace PentagonFunctions {

template <typename T> T f_4_124_abbreviated (const std::array<T,16>&);



template <typename T> IntegrandConstructorType<T> f_4_124_construct (const Kin<T>& kin) {
    return [&kin, 
            dl20 = DLog_W_20<T>(kin),dl16 = DLog_W_16<T>(kin),dl3 = DLog_W_3<T>(kin),dl19 = DLog_W_19<T>(kin),dl8 = DLog_W_8<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,16> abbr = 
            {dl20(t), rlog(kin.W[15] / kin_path.W[15]), rlog(v_path[2]), rlog(-v_path[1] + v_path[3] + v_path[4]), f_2_1_13(kin_path), f_2_1_15(kin_path), rlog(v_path[0] + v_path[4]), rlog(kin.W[7] / kin_path.W[7]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[18] / kin_path.W[18]), dl16(t), dl3(t), dl19(t), dl8(t)}
;

        auto result = f_4_124_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_124_abbreviated(const std::array<T,16>& abb)
{
using TR = typename T::value_type;
T z[44];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = bc<TR>[0];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[9];
z[10] = abb[10];
z[11] = abb[11];
z[12] = abb[8];
z[13] = bc<TR>[2];
z[14] = bc<TR>[9];
z[15] = abb[14];
z[16] = abb[15];
z[17] = abb[12];
z[18] = abb[13];
z[19] = z[0] + z[16];
z[20] = (T(3) * z[8]) / T(2);
z[19] = z[19] * z[20];
z[21] = z[0] / T(2);
z[22] = z[15] + z[21];
z[23] = (T(3) * z[16]) / T(2);
z[24] = T(2) * z[18] + -z[22] + -z[23];
z[24] = z[9] * z[24];
z[25] = z[10] + z[11];
z[26] = z[15] + z[18];
z[27] = T(2) * z[26];
z[28] = z[0] + z[27];
z[29] = T(3) * z[16] + -z[28];
z[30] = z[25] * z[29];
z[31] = -z[15] + z[16];
z[32] = T(3) * z[1];
z[31] = z[31] * z[32];
z[19] = z[19] + z[24] + z[30] + -z[31];
z[24] = z[12] * z[19];
z[33] = -z[16] + z[27];
z[34] = T(3) * z[8];
z[33] = z[33] * z[34];
z[35] = T(2) * z[30];
z[36] = -z[0] + z[16];
z[37] = z[32] * z[36];
z[28] = z[9] * z[28];
z[38] = -z[8] + z[9];
z[39] = z[17] * z[38];
z[28] = -z[28] + z[33] + z[35] + -z[37] + T(3) * z[39];
z[33] = z[3] * z[28];
z[37] = z[16] / T(2);
z[27] = z[21] + z[27] + -z[37];
z[27] = z[8] * z[27];
z[40] = z[22] + z[37];
z[40] = z[9] * z[40];
z[41] = T(2) * z[16];
z[42] = z[0] + z[15];
z[43] = z[41] + -z[42];
z[43] = z[1] * z[43];
z[27] = -z[27] + -z[30] + -z[39] + z[40] + z[43];
z[27] = T(3) * z[27];
z[30] = z[7] * z[27];
z[40] = z[9] + -z[34];
z[29] = z[29] * z[40];
z[29] = z[29] + z[35];
z[29] = z[2] * z[29];
z[37] = z[21] + z[37];
z[40] = T(-3) * z[26] + z[37];
z[40] = T(3) * z[17] + (T(3) * z[40]) / T(2);
z[40] = prod_pow(z[13], 2) * z[40];
z[37] = -z[26] + z[37];
z[43] = prod_pow(z[4], 2);
z[37] = z[37] * z[43];
z[24] = z[24] + z[29] + z[30] + z[33] + z[37] / T(2) + z[40];
z[24] = int_to_imaginary<T>(1) * z[4] * z[24];
z[28] = -(z[2] * z[28]);
z[29] = z[26] + -z[36];
z[29] = z[29] * z[34];
z[30] = T(2) * z[15];
z[33] = z[0] + -z[18];
z[34] = -z[30] + -z[33];
z[34] = z[9] * z[34];
z[29] = z[29] + -z[31] + z[34] + z[35];
z[29] = z[12] * z[29];
z[31] = z[0] + -z[15];
z[31] = z[1] * z[31];
z[31] = z[31] + z[39];
z[34] = -z[0] + z[26];
z[35] = z[25] * z[34];
z[33] = z[15] + z[33];
z[33] = z[20] * z[33];
z[30] = -z[0] + -z[18] / T(2) + -z[30];
z[30] = z[9] * z[30];
z[30] = z[30] + (T(3) * z[31]) / T(2) + z[33] + T(2) * z[35];
z[30] = z[3] * z[30];
z[28] = z[28] + z[29] + z[30];
z[28] = z[3] * z[28];
z[22] = (T(-17) * z[18]) / T(2) + -z[22];
z[22] = z[22] / T(6) + z[41];
z[22] = z[9] * z[22];
z[29] = T(3) * z[15];
z[30] = -z[0] + -z[29];
z[30] = z[30] / T(2) + z[41];
z[30] = z[1] * z[30];
z[29] = (T(3) * z[0]) / T(2) + (T(5) * z[18]) / T(2) + z[29];
z[29] = T(-4) * z[16] + z[29] / T(2);
z[29] = z[8] * z[29];
z[33] = (T(5) * z[0]) / T(2) + (T(-11) * z[16]) / T(2) + z[26];
z[33] = z[13] * z[33];
z[36] = T(3) * z[13] + -z[38];
z[36] = z[17] * z[36];
z[22] = z[22] + z[29] + z[30] + (T(3) * z[33]) / T(4) + z[35] / T(6) + z[36] / T(2);
z[22] = z[22] * z[43];
z[19] = -(z[2] * z[19]);
z[21] = -z[21] + z[23] + -z[26];
z[20] = z[9] / T(2) + -z[20] + z[25];
z[20] = z[12] * z[20] * z[21];
z[19] = z[19] + z[20];
z[19] = z[12] * z[19];
z[20] = -(z[5] * z[27]);
z[21] = z[21] * z[25];
z[23] = -z[16] + z[42] / T(2);
z[23] = z[23] * z[32];
z[25] = -z[0] + T(5) * z[16];
z[25] = z[8] * z[25];
z[27] = -z[0] / T(4) + -z[15] / T(2) + (T(-9) * z[16]) / T(4) + z[18];
z[27] = z[9] * z[27];
z[21] = z[21] + z[23] + (T(3) * z[25]) / T(4) + z[27];
z[23] = prod_pow(z[2], 2);
z[21] = z[21] * z[23];
z[25] = z[8] * z[34];
z[27] = -(z[9] * z[18]);
z[25] = z[25] + z[27] + z[31];
z[25] = z[6] * z[25];
z[23] = z[23] * z[38];
z[23] = -z[14] / T(2) + z[23];
z[23] = z[17] * z[23];
z[26] = (T(-325) * z[0]) / T(2) + T(23) * z[26];
z[26] = (T(105) * z[16]) / T(2) + z[26] / T(3);
z[26] = z[14] * z[26];
return z[19] + z[20] + z[21] + z[22] + (T(3) * z[23]) / T(2) + z[24] + T(3) * z[25] + z[26] / T(8) + z[28];
}



template IntegrandConstructorType<double> f_4_124_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_124_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_124_construct (const Kin<qd_real>&);
#endif

}