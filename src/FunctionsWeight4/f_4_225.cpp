#include "f_4_225.h"

namespace PentagonFunctions {

template <typename T> T f_4_225_abbreviated (const std::array<T,39>&);

template <typename T> class SpDLog_f_4_225_W_22 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_225_W_22 (const Kin<T>& kin) {
        c[0] = ((T(9) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(-3)) * kin.v[2] + ((T(9) * kin.v[1]) / T(4) + (T(9) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(-3)) * kin.v[1] + ((T(-3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[3] + ((T(9) * kin.v[4]) / T(4) + T(3)) * kin.v[4];
c[1] = T(-3) * kin.v[1] + T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4];
c[2] = ((T(3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[3] + ((T(-9) * kin.v[4]) / T(4) + T(-3)) * kin.v[4] + ((T(-9) * kin.v[1]) / T(4) + (T(-9) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2) + T(3)) * kin.v[1] + ((T(-9) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2) + T(3)) * kin.v[2];
c[3] = T(3) * kin.v[1] + T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4];
c[4] = ((T(3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[3] + ((T(-9) * kin.v[4]) / T(4) + T(-3)) * kin.v[4] + ((T(-9) * kin.v[1]) / T(4) + (T(-9) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2) + T(3)) * kin.v[1] + ((T(-9) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2) + T(3)) * kin.v[2];
c[5] = T(3) * kin.v[1] + T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4];
c[6] = ((T(9) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(-3)) * kin.v[2] + ((T(9) * kin.v[1]) / T(4) + (T(9) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(-3)) * kin.v[1] + ((T(-3) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[3] + ((T(9) * kin.v[4]) / T(4) + T(3)) * kin.v[4];
c[7] = T(-3) * kin.v[1] + T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[5] * (t * c[0] + c[1]) + abb[6] * (t * c[2] + c[3]) + abb[7] * (t * c[4] + c[5]) + abb[8] * (t * c[6] + c[7]);
        }

        return abb[0] * (abb[5] * (abb[2] * abb[3] * T(-3) + abb[4] * T(-3)) + abb[2] * abb[3] * (abb[8] * T(-3) + abb[6] * T(3) + abb[7] * T(3)) + abb[4] * (abb[8] * T(-3) + abb[6] * T(3) + abb[7] * T(3)) + abb[1] * (abb[5] * (abb[2] * T(3) + abb[3] * T(3)) + abb[1] * (abb[5] * T(-3) + abb[8] * T(-3) + abb[6] * T(3) + abb[7] * T(3)) + abb[2] * (abb[6] * T(-3) + abb[7] * T(-3) + abb[8] * T(3)) + abb[3] * (abb[6] * T(-3) + abb[7] * T(-3) + abb[8] * T(3))));
    }
};
template <typename T> class SpDLog_f_4_225_W_23 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_225_W_23 (const Kin<T>& kin) {
        c[0] = ((T(15) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + ((T(15) * kin.v[2]) / T(4) + (T(15) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + ((T(-9) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + kin.v[0] * ((T(-21) * kin.v[2]) / T(2) + (T(-21) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2) + T(-9) + (T(27) / T(4) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[0] + T(6) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3])) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]));
c[1] = (T(-9) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[0] + T(9) * kin.v[2] + T(9) * kin.v[3] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]);
c[2] = ((T(15) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + ((T(15) * kin.v[2]) / T(4) + (T(15) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + ((T(-9) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + kin.v[0] * ((T(-21) * kin.v[2]) / T(2) + (T(-21) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2) + T(-9) + (T(27) / T(4) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[0] + T(6) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3])) + kin.v[1] * (T(-6) * kin.v[2] + T(-6) * kin.v[3] + T(6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]));
c[3] = (T(-9) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[0] + T(9) * kin.v[2] + T(9) * kin.v[3] + T(-9) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]);
c[4] = kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * ((T(21) * kin.v[2]) / T(2) + (T(21) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(9) + (T(-27) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[0] + T(-6) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3])) + ((T(-15) * kin.v[2]) / T(4) + (T(-15) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + ((T(-15) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + ((T(9) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]));
c[5] = (bc<T>[0] * int_to_imaginary<T>(-3) + T(9)) * kin.v[0] + T(-9) * kin.v[2] + T(-9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + T(9) * kin.v[4];
c[6] = kin.v[1] * (T(6) * kin.v[2] + T(6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * ((T(21) * kin.v[2]) / T(2) + (T(21) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(9) + (T(-27) / T(4) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[0] + T(-6) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3])) + ((T(-15) * kin.v[2]) / T(4) + (T(-15) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + ((T(-15) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + ((T(9) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]));
c[7] = (bc<T>[0] * int_to_imaginary<T>(-3) + T(9)) * kin.v[0] + T(-9) * kin.v[2] + T(-9) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + T(9) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[5] * (t * c[0] + c[1]) + abb[13] * (t * c[2] + c[3]) + abb[7] * (t * c[4] + c[5]) + abb[8] * (t * c[6] + c[7]);
        }

        return abb[9] * (abb[5] * (abb[12] * T(-9) + abb[2] * abb[11] * T(-3) + abb[2] * (abb[3] * T(-3) + bc<T>[0] * int_to_imaginary<T>(-3))) + abb[2] * abb[11] * (abb[13] * T(-3) + abb[7] * T(3) + abb[8] * T(3)) + abb[2] * (abb[13] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[7] * bc<T>[0] * int_to_imaginary<T>(3) + abb[8] * bc<T>[0] * int_to_imaginary<T>(3) + abb[3] * (abb[13] * T(-3) + abb[7] * T(3) + abb[8] * T(3))) + abb[10] * (abb[2] * abb[5] * T(3) + abb[1] * (abb[5] * T(-3) + abb[13] * T(-3) + abb[7] * T(3) + abb[8] * T(3)) + abb[2] * (abb[7] * T(-3) + abb[8] * T(-3) + abb[13] * T(3))) + abb[1] * (abb[7] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[13] * bc<T>[0] * int_to_imaginary<T>(3) + abb[1] * (abb[5] * T(-3) + abb[13] * T(-3) + abb[7] * T(3) + abb[8] * T(3)) + abb[5] * (bc<T>[0] * int_to_imaginary<T>(3) + abb[2] * T(3) + abb[3] * T(3) + abb[11] * T(3)) + abb[2] * (abb[7] * T(-3) + abb[8] * T(-3) + abb[13] * T(3)) + abb[3] * (abb[7] * T(-3) + abb[8] * T(-3) + abb[13] * T(3)) + abb[11] * (abb[7] * T(-3) + abb[8] * T(-3) + abb[13] * T(3))) + abb[12] * (abb[13] * T(-9) + abb[7] * T(9) + abb[8] * T(9)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_225_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl23 = DLog_W_23<T>(kin),dl20 = DLog_W_20<T>(kin),dl2 = DLog_W_2<T>(kin),dl5 = DLog_W_5<T>(kin),dl1 = DLog_W_1<T>(kin),dl8 = DLog_W_8<T>(kin),dl17 = DLog_W_17<T>(kin),dl3 = DLog_W_3<T>(kin),dl19 = DLog_W_19<T>(kin),dl16 = DLog_W_16<T>(kin),dl18 = DLog_W_18<T>(kin),dl4 = DLog_W_4<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl29 = DLog_W_29<T>(kin),spdl22 = SpDLog_f_4_225_W_22<T>(kin),spdl23 = SpDLog_f_4_225_W_23<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,39> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl23(t), rlog(v_path[2]), rlog(-v_path[4]), f_2_1_8(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), dl20(t), f_2_1_5(kin_path), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), rlog(-v_path[4] + v_path[2]), dl2(t), f_2_1_7(kin_path), dl5(t), dl1(t), dl8(t), dl17(t), dl3(t), dl19(t), dl16(t), dl18(t), dl4(t), dl27(t) / kin_path.SqrtDelta, rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[17] / kin_path.W[17]), dl28(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta}
;

        auto result = f_4_225_abbreviated(abbr);
        result = result + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_225_abbreviated(const std::array<T,39>& abb)
{
using TR = typename T::value_type;
T z[91];
z[0] = abb[1];
z[1] = abb[5];
z[2] = abb[27];
z[3] = abb[29];
z[4] = abb[6];
z[5] = abb[21];
z[6] = abb[25];
z[7] = abb[26];
z[8] = abb[28];
z[9] = abb[7];
z[10] = abb[8];
z[11] = abb[13];
z[12] = abb[30];
z[13] = abb[31];
z[14] = abb[32];
z[15] = abb[33];
z[16] = abb[34];
z[17] = abb[35];
z[18] = abb[37];
z[19] = abb[2];
z[20] = abb[3];
z[21] = abb[10];
z[22] = abb[11];
z[23] = bc<TR>[0];
z[24] = abb[14];
z[25] = abb[24];
z[26] = abb[36];
z[27] = abb[38];
z[28] = abb[19];
z[29] = abb[22];
z[30] = abb[23];
z[31] = abb[4];
z[32] = abb[12];
z[33] = abb[15];
z[34] = abb[16];
z[35] = abb[17];
z[36] = abb[18];
z[37] = abb[20];
z[38] = bc<TR>[3];
z[39] = bc<TR>[1];
z[40] = bc<TR>[2];
z[41] = bc<TR>[4];
z[42] = bc<TR>[9];
z[43] = z[21] / T(2);
z[44] = z[20] + z[22];
z[45] = z[43] + -z[44];
z[45] = z[21] * z[45];
z[46] = z[22] / T(2);
z[47] = z[20] + z[46];
z[47] = z[22] * z[47];
z[47] = z[37] + z[47];
z[48] = z[45] + z[47];
z[49] = z[31] + -z[33];
z[50] = z[48] + -z[49];
z[50] = z[25] * z[50];
z[51] = z[19] * z[28];
z[52] = z[21] + -z[22];
z[53] = -(z[51] * z[52]);
z[54] = prod_pow(z[21], 2);
z[54] = z[54] / T(2);
z[55] = -z[7] + z[28];
z[56] = z[54] * z[55];
z[57] = z[31] + z[34];
z[57] = z[7] * z[57];
z[58] = -z[33] + z[37];
z[59] = z[28] * z[58];
z[50] = z[50] + -z[53] + -z[56] + z[57] + z[59];
z[53] = z[7] * z[21];
z[56] = z[25] * z[52];
z[53] = z[53] + -z[56];
z[56] = z[0] / T(2);
z[57] = -z[7] + -z[8];
z[57] = z[56] * z[57];
z[59] = z[8] * z[22];
z[57] = z[53] + z[57] + z[59];
z[57] = z[0] * z[57];
z[60] = -z[0] + z[19];
z[52] = -(z[52] * z[60]);
z[61] = T(3) * z[32] + -z[33];
z[62] = z[31] + -z[34];
z[63] = -z[52] + -z[61] + z[62];
z[63] = z[24] * z[63];
z[64] = -z[20] + z[21] + z[56];
z[64] = z[0] * z[64];
z[65] = prod_pow(z[22], 2);
z[65] = z[65] / T(2);
z[66] = -z[21] + z[44];
z[67] = -z[0] + z[66];
z[68] = z[19] * z[67];
z[64] = z[64] + -z[65] + z[68];
z[69] = z[34] + -z[37] + z[64];
z[70] = z[32] + -z[33];
z[71] = -z[69] + -z[70];
z[71] = z[5] * z[71];
z[72] = z[20] * z[22];
z[66] = z[21] * z[66];
z[72] = -z[66] + z[72];
z[73] = z[33] + z[34] + z[72];
z[74] = z[29] * z[73];
z[75] = z[8] * z[37];
z[74] = z[74] + z[75];
z[75] = z[0] + -z[20];
z[76] = z[0] * z[75];
z[76] = z[32] + z[76];
z[75] = z[19] * z[75];
z[75] = -z[31] + z[75];
z[77] = -z[75] + z[76];
z[77] = z[3] * z[77];
z[78] = -z[8] + -z[28];
z[78] = z[65] * z[78];
z[79] = z[8] + T(3) * z[25];
z[80] = -(z[32] * z[79]);
z[57] = -z[50] + z[57] + z[63] + z[71] + -z[74] + z[77] + z[78] + z[80];
z[57] = z[10] * z[57];
z[63] = -z[13] + -z[14] + z[15] + z[16];
z[43] = z[43] * z[63];
z[71] = z[20] * z[63];
z[78] = z[22] * z[63];
z[80] = z[71] + z[78];
z[43] = z[43] + -z[80];
z[43] = z[21] * z[43];
z[81] = z[46] * z[63];
z[81] = z[71] + z[81];
z[81] = z[22] * z[81];
z[43] = z[43] + z[81];
z[81] = z[34] * z[63];
z[82] = z[32] * z[63];
z[83] = z[81] + -z[82];
z[84] = z[0] * z[63];
z[71] = -z[71] + z[84];
z[71] = z[0] * z[71];
z[85] = z[80] + -z[84];
z[86] = z[21] * z[63];
z[87] = z[85] + -z[86];
z[87] = z[19] * z[87];
z[88] = z[37] * z[63];
z[89] = z[31] * z[63];
z[71] = -z[43] + z[71] + -z[83] + z[87] + -z[88] + z[89];
z[71] = z[18] * z[71];
z[87] = z[32] + -z[34];
z[49] = -z[49] + z[52] + z[87];
z[49] = z[24] * z[49];
z[52] = z[7] + -z[8];
z[90] = z[52] * z[56];
z[53] = -z[53] + z[59] + z[90];
z[53] = z[0] * z[53];
z[59] = -z[8] + z[28];
z[59] = z[59] * z[65];
z[90] = -z[8] + z[25];
z[90] = z[32] * z[90];
z[49] = z[49] + z[50] + z[53] + z[59] + -z[74] + z[90];
z[50] = z[61] + z[69];
z[50] = z[5] * z[50];
z[50] = z[49] + z[50] + z[77];
z[50] = z[11] * z[50];
z[53] = -z[58] + z[64] + z[87];
z[53] = z[5] * z[53];
z[49] = z[49] + z[53] + -z[77];
z[49] = z[4] * z[49];
z[53] = -(z[54] * z[63]);
z[54] = z[56] * z[63];
z[56] = -z[54] + z[86];
z[56] = z[0] * z[56];
z[53] = z[53] + z[56] + -z[81] + -z[89];
z[53] = z[17] * z[53];
z[56] = z[82] + z[88];
z[59] = -(z[63] * z[65]);
z[54] = -z[54] + z[78];
z[54] = z[0] * z[54];
z[54] = z[54] + -z[56] + z[59];
z[54] = z[12] * z[54];
z[49] = z[49] + z[50] + z[53] + z[54] + z[57] + z[71];
z[48] = z[48] + -z[62] + -z[68] + -z[76];
z[48] = z[10] * z[48];
z[46] = -z[20] + z[46];
z[46] = z[22] * z[46];
z[45] = -z[37] + z[45] + -z[46] + -z[62];
z[45] = z[45] / T(2);
z[50] = z[68] / T(2);
z[53] = z[20] / T(2);
z[54] = z[0] + -z[22] + -z[53];
z[54] = z[0] * z[54];
z[54] = (T(3) * z[32]) / T(2) + -z[45] + z[50] + z[54];
z[54] = z[11] * z[54];
z[53] = -z[21] + z[53];
z[53] = z[0] * z[53];
z[45] = -z[32] / T(2) + -z[45] + -z[50] + z[53];
z[45] = z[4] * z[45];
z[50] = z[0] * z[67];
z[50] = T(2) * z[32] + -z[50] + z[68];
z[53] = -z[1] + z[9];
z[50] = -(z[50] * z[53]);
z[45] = z[45] + z[48] / T(2) + z[50] + z[54];
z[45] = z[2] * z[45];
z[48] = -z[78] + z[86];
z[50] = z[0] * z[48];
z[54] = z[33] * z[63];
z[50] = z[50] + z[54] + -z[89];
z[48] = -(z[19] * z[48]);
z[48] = z[48] + z[50] + -z[83];
z[54] = z[27] / T(2);
z[48] = z[48] * z[54];
z[57] = (T(3) * z[0]) / T(2) + -z[44];
z[57] = z[0] * z[57];
z[57] = z[57] + -z[75];
z[59] = -z[34] + z[66];
z[46] = z[46] + z[57] + z[58] + z[59];
z[46] = z[32] + z[46] / T(2);
z[58] = -z[10] + z[11];
z[46] = -(z[46] * z[58]);
z[47] = -z[33] + -z[47] + -z[57] + z[59];
z[47] = z[4] * z[47];
z[57] = -(z[1] * z[32]);
z[46] = z[46] + z[47] / T(2) + z[57];
z[46] = z[6] * z[46];
z[47] = z[32] + -z[73];
z[47] = z[6] * z[47];
z[57] = -z[34] + -z[70];
z[57] = z[5] * z[57];
z[47] = z[47] + z[57] + -z[77];
z[47] = z[9] * z[47];
z[57] = z[5] * z[32];
z[57] = z[57] + z[77];
z[57] = z[1] * z[57];
z[59] = T(2) * z[34] + z[72];
z[61] = z[4] + z[10];
z[62] = z[9] + z[61];
z[62] = z[30] * z[62];
z[59] = z[59] * z[62];
z[45] = z[45] + z[46] + z[47] + z[48] + z[49] / T(2) + z[57] + z[59];
z[46] = -z[36] + z[44];
z[46] = z[25] * z[46];
z[47] = -z[7] + z[25];
z[47] = z[0] * z[47];
z[48] = z[7] * z[35];
z[46] = z[46] + -z[47] + -z[48] + -z[51];
z[47] = -z[35] + z[36];
z[48] = z[47] + z[60];
z[49] = z[5] * z[48];
z[49] = z[46] + z[49];
z[48] = -(z[24] * z[48]);
z[51] = -z[35] + z[44];
z[57] = z[29] * z[51];
z[59] = -z[28] + z[29];
z[59] = z[36] * z[59];
z[48] = z[48] + -z[49] + -z[57] + z[59];
z[48] = z[10] * z[48];
z[59] = z[47] + -z[60];
z[64] = z[24] * z[59];
z[65] = z[28] + z[29];
z[65] = z[36] * z[65];
z[57] = -z[57] + -z[64] + z[65];
z[49] = z[49] + z[57];
z[49] = z[11] * z[49];
z[59] = -(z[5] * z[59]);
z[46] = z[46] + z[57] + z[59];
z[46] = z[4] * z[46];
z[57] = z[19] + z[35];
z[59] = z[57] * z[63];
z[59] = z[59] + -z[80];
z[59] = z[18] * z[59];
z[64] = z[35] * z[63];
z[64] = z[64] + -z[84];
z[64] = z[17] * z[64];
z[57] = -z[36] + z[57];
z[57] = z[57] * z[63];
z[57] = z[57] + -z[84];
z[57] = z[27] * z[57];
z[65] = -z[36] + z[51];
z[65] = z[6] * z[65];
z[61] = z[11] + -z[61];
z[61] = z[61] * z[65];
z[46] = z[46] + z[48] + z[49] + z[57] + z[59] + z[61] + z[64];
z[48] = -(z[53] * z[60]);
z[49] = -z[19] + z[51];
z[49] = -(z[49] * z[58]);
z[51] = z[19] + z[51];
z[51] = z[0] + -z[51] / T(2);
z[51] = z[4] * z[51];
z[48] = z[48] + z[49] / T(2) + z[51];
z[48] = z[2] * z[48];
z[49] = prod_pow(z[39], 2);
z[49] = -z[41] + -z[49] / T(2);
z[51] = z[17] + z[18];
z[57] = z[12] + z[51];
z[49] = z[49] * z[57];
z[47] = -(z[5] * z[47]);
z[47] = z[47] + -z[65];
z[47] = z[9] * z[47];
z[51] = z[27] + z[51];
z[51] = z[40] * z[51];
z[57] = z[39] * z[57];
z[59] = -z[51] + z[57];
z[59] = z[40] * z[59];
z[44] = T(2) * z[35] + -z[44];
z[44] = -(z[44] * z[62]);
z[44] = z[44] + z[46] / T(2) + z[47] + z[48] + z[49] + z[59];
z[44] = int_to_imaginary<T>(1) * z[44];
z[46] = z[2] + -z[3] + z[52];
z[47] = z[38] * z[46];
z[44] = z[44] + z[47];
z[47] = z[24] + z[55];
z[48] = -z[5] + T(5) * z[29];
z[49] = z[47] + z[48] + -z[79];
z[52] = -z[3] + z[49];
z[52] = z[11] * z[52];
z[49] = z[3] + z[49];
z[49] = z[4] * z[49];
z[55] = -z[12] + z[17] + T(3) * z[18];
z[55] = z[55] * z[63];
z[47] = -z[3] + -z[8] + T(5) * z[25] + -z[47] + z[48];
z[47] = z[10] * z[47];
z[47] = z[47] + z[49] + z[52] + z[55];
z[48] = z[54] * z[63];
z[49] = T(3) * z[4];
z[52] = z[49] + T(-5) * z[58];
z[52] = z[1] + z[52] / T(2);
z[52] = z[6] * z[52];
z[49] = T(-3) * z[10] + T(5) * z[11] + z[49];
z[49] = z[49] / T(2) + -z[53];
z[49] = z[2] * z[49];
z[53] = -z[3] + -z[5];
z[53] = z[1] * z[53];
z[47] = z[47] / T(2) + z[48] + z[49] + z[52] + z[53];
z[48] = -z[17] + z[18];
z[46] = (T(3) * z[12]) / T(4) + -z[46] / T(9) + -z[48] / T(4) + -z[54];
z[48] = int_to_imaginary<T>(1) * z[23];
z[46] = z[46] * z[48];
z[49] = z[3] / T(2) + T(2) * z[6];
z[49] = z[9] * z[49];
z[46] = z[46] + z[47] / T(2) + z[49] + (T(-3) * z[51]) / T(2) + z[57] + T(-2) * z[62];
z[46] = z[23] * z[46];
z[44] = T(3) * z[44] + z[46];
z[44] = z[23] * z[44];
z[43] = -z[43] + -z[50] + -z[56];
z[43] = z[42] + z[43] / T(2);
z[46] = z[36] * z[63];
z[46] = z[46] + -z[85];
z[46] = int_to_imaginary<T>(1) * z[46];
z[47] = -z[48] + (T(3) * z[63]) / T(2);
z[47] = z[23] * z[47];
z[46] = T(3) * z[46] + z[47];
z[46] = z[23] * z[46];
z[43] = T(3) * z[43] + z[46] / T(2);
z[43] = z[26] * z[43];
z[46] = T(-19) * z[12] + (T(-11) * z[17]) / T(2) + (T(13) * z[18]) / T(2) + (T(27) * z[27]) / T(2);
z[46] = z[42] * z[46];
return z[43] + z[44] + T(3) * z[45] + z[46] / T(2);
}



template IntegrandConstructorType<double> f_4_225_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_225_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_225_construct (const Kin<qd_real>&);
#endif

}