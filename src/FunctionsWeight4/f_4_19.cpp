#include "f_4_19.h"

namespace PentagonFunctions {

template <typename T> T f_4_19_abbreviated (const std::array<T,16>&);



template <typename T> IntegrandConstructorType<T> f_4_19_construct (const Kin<T>& kin) {
    return [&kin, 
            dl15 = DLog_W_15<T>(kin),dl3 = DLog_W_3<T>(kin),dl13 = DLog_W_13<T>(kin),dl20 = DLog_W_20<T>(kin),dl5 = DLog_W_5<T>(kin),dl1 = DLog_W_1<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,16> abbr = 
            {dl15(t), rlog(kin.W[0] / kin_path.W[0]), rlog(v_path[2]), rlog(-v_path[4]), rlog(kin.W[19] / kin_path.W[19]), dl3(t), rlog(kin.W[12] / kin_path.W[12]), dl13(t), rlog(v_path[0]), f_2_1_1(kin_path), rlog(kin.W[4] / kin_path.W[4]), dl20(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl5(t), dl1(t)}
;

        auto result = f_4_19_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_19_abbreviated(const std::array<T,16>& abb)
{
using TR = typename T::value_type;
T z[47];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[7];
z[8] = abb[11];
z[9] = abb[14];
z[10] = abb[15];
z[11] = abb[8];
z[12] = abb[9];
z[13] = abb[12];
z[14] = abb[13];
z[15] = abb[6];
z[16] = abb[10];
z[17] = bc<TR>[1];
z[18] = bc<TR>[2];
z[19] = bc<TR>[4];
z[20] = bc<TR>[7];
z[21] = bc<TR>[8];
z[22] = bc<TR>[9];
z[23] = z[15] / T(2);
z[24] = z[7] + -z[10];
z[25] = z[23] * z[24];
z[26] = z[7] + -z[8];
z[27] = z[16] * z[26];
z[28] = -z[1] + z[15];
z[28] = z[6] * z[28];
z[29] = T(3) * z[8];
z[30] = z[10] + -z[29];
z[30] = z[0] + z[7] + z[30] / T(2);
z[30] = z[5] * z[30];
z[31] = (T(3) * z[26]) / T(2);
z[32] = -z[0] + -z[31];
z[32] = z[1] * z[32];
z[25] = z[25] + -z[27] + -z[28] + z[30] + z[32];
z[25] = prod_pow(z[2], 2) * z[25];
z[30] = T(3) * z[10];
z[32] = -z[7] + z[30];
z[33] = z[23] * z[32];
z[34] = z[8] + -z[10];
z[35] = -z[7] + (T(3) * z[34]) / T(2);
z[35] = z[5] * z[35];
z[36] = z[1] * z[31];
z[27] = z[27] + z[33] + z[35] + z[36];
z[27] = prod_pow(z[11], 2) * z[27];
z[33] = z[7] / T(2);
z[35] = T(-3) * z[9] + (T(-7) * z[10]) / T(2) + z[29] + z[33];
z[36] = -(z[1] * z[35]);
z[37] = T(2) * z[9];
z[30] = z[30] + z[37];
z[38] = T(2) * z[8];
z[39] = z[7] + -z[30] + z[38];
z[40] = -(z[16] * z[39]);
z[41] = -z[7] + -z[10];
z[23] = z[23] * z[41];
z[41] = -z[9] + z[34];
z[41] = z[7] + T(3) * z[41];
z[41] = z[5] * z[41];
z[23] = z[23] + z[36] + z[40] + z[41];
z[23] = z[12] * z[23];
z[29] = -z[29] + z[30];
z[36] = z[1] + -z[5];
z[36] = z[29] * z[36];
z[40] = T(2) * z[16];
z[41] = z[34] * z[40];
z[36] = z[36] + -z[41];
z[41] = z[2] + -z[11];
z[41] = z[36] * z[41];
z[42] = z[0] + -z[10];
z[43] = z[5] * z[42];
z[44] = z[1] * z[42];
z[37] = z[16] * z[37];
z[37] = z[37] + -z[43] + z[44];
z[37] = z[4] * z[37];
z[37] = z[37] + z[41];
z[37] = z[4] * z[37];
z[41] = -(z[13] * z[36]);
z[35] = z[20] * z[35];
z[33] = -z[9] + z[33];
z[44] = z[8] + (T(-3) * z[10]) / T(2) + z[33];
z[44] = z[21] * z[44];
z[45] = T(5) * z[8];
z[46] = (T(-7) * z[7]) / T(2) + T(31) * z[9] + (T(17) * z[10]) / T(2) + -z[45];
z[46] = z[22] * z[46];
z[23] = z[23] + z[25] + z[27] + z[35] + z[37] + z[41] + z[44] / T(2) + z[46] / T(4);
z[25] = -z[7] + z[9] + -z[42];
z[25] = z[5] * z[25];
z[25] = z[25] + z[28];
z[27] = -z[15] + z[40];
z[24] = z[24] * z[27];
z[27] = T(2) * z[0] + T(3) * z[7] + -z[30];
z[27] = z[1] * z[27];
z[24] = z[24] + T(2) * z[25] + z[27];
z[24] = z[2] * z[24];
z[25] = T(-3) * z[1] + -z[40];
z[25] = z[25] * z[26];
z[26] = T(2) * z[7] + T(-3) * z[34];
z[26] = z[5] * z[26];
z[27] = -(z[15] * z[32]);
z[25] = z[25] + z[26] + z[27];
z[25] = z[11] * z[25];
z[26] = z[14] * z[36];
z[27] = z[18] * z[29];
z[29] = -z[7] + T(4) * z[8] + -z[30];
z[29] = z[17] * z[29];
z[27] = z[27] + z[29];
z[27] = z[17] * z[27];
z[29] = z[7] + T(-2) * z[30] + z[45];
z[29] = z[19] * z[29];
z[24] = z[24] + z[25] + z[26] + z[27] + z[29];
z[25] = prod_pow(z[3], 2);
z[26] = z[9] + T(2) * z[10] + -z[38];
z[26] = z[25] * z[26];
z[24] = T(3) * z[24] + z[26];
z[24] = int_to_imaginary<T>(1) * z[3] * z[24];
z[26] = z[9] * z[16];
z[27] = T(-7) * z[8] + (T(15) * z[10]) / T(2) + -z[33];
z[27] = z[18] * z[27];
z[29] = (T(-13) * z[7]) / T(2) + (T(15) * z[8]) / T(2) + -z[9] + -z[10];
z[29] = z[17] * z[29];
z[26] = z[26] + -z[27] + -z[29];
z[27] = z[10] * z[15];
z[27] = -z[27] + z[28];
z[28] = T(3) * z[0] + z[10] / T(4);
z[28] = z[1] * z[28];
z[26] = -z[26] / T(2) + (T(13) * z[27]) / T(4) + z[28] + T(-3) * z[43];
z[25] = z[25] * z[26];
z[26] = -(prod_pow(z[18], 3) * z[39]);
z[27] = prod_pow(z[17], 3) * z[31];
return T(3) * z[23] + z[24] + z[25] + z[26] + z[27];
}



template IntegrandConstructorType<double> f_4_19_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_19_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_19_construct (const Kin<qd_real>&);
#endif

}