#include "f_4_131.h"

namespace PentagonFunctions {

template <typename T> T f_4_131_abbreviated (const std::array<T,48>&);

template <typename T> class SpDLog_f_4_131_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_131_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(16) * kin.v[0]) / T(3) + (bc<T>[1] * T(-2) + T(2)) * kin.v[1] + T(-4) * kin.v[2] + T(-8) * kin.v[3] + bc<T>[1] * (T(4) + T(-4) * kin.v[0] + T(4) * kin.v[3])) + ((T(16) * kin.v[2]) / T(3) + (T(-16) * kin.v[3]) / T(3) + (T(-16) * kin.v[4]) / T(3)) * kin.v[0] + T(-2) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(6) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * (T(-6) * kin.v[2] + T(8) * kin.v[4]) + bc<T>[1] * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[1] * (T(4) + T(-4) * kin.v[0] + T(-2) * kin.v[1] + T(4) * kin.v[3]) + kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]));
c[1] = kin.v[1] * ((T(16) * kin.v[0]) / T(3) + T(2) * kin.v[1] + T(-4) * kin.v[2] + T(-8) * kin.v[3]) + ((T(16) * kin.v[2]) / T(3) + (T(-16) * kin.v[3]) / T(3) + (T(-16) * kin.v[4]) / T(3)) * kin.v[0] + T(-2) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(6) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * (T(-6) * kin.v[2] + T(8) * kin.v[4]) + rlog(kin.v[2]) * (prod_pow(kin.v[4], 2) / T(2) + ((T(-4) * kin.v[2]) / T(3) + (T(4) * kin.v[3]) / T(3) + (T(4) * kin.v[4]) / T(3)) * kin.v[0] + ((T(-3) * kin.v[3]) / T(2) + -kin.v[4]) * kin.v[3] + kin.v[1] * ((T(-4) * kin.v[0]) / T(3) + bc<T>[1] * (-kin.v[3] + T(-1) + kin.v[0]) + (bc<T>[1] / T(2) + T(-1) / T(2)) * kin.v[1] + kin.v[2] + T(2) * kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-2) * kin.v[4]) + bc<T>[1] * (kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + T(-1) + kin.v[0]) * kin.v[1] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]))) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (prod_pow(kin.v[4], 2) / T(2) + ((T(-4) * kin.v[2]) / T(3) + (T(4) * kin.v[3]) / T(3) + (T(4) * kin.v[4]) / T(3)) * kin.v[0] + ((T(-3) * kin.v[3]) / T(2) + -kin.v[4]) * kin.v[3] + kin.v[1] * ((T(-4) * kin.v[0]) / T(3) + bc<T>[1] * (-kin.v[3] + T(-1) + kin.v[0]) + (bc<T>[1] / T(2) + T(-1) / T(2)) * kin.v[1] + kin.v[2] + T(2) * kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-2) * kin.v[4]) + bc<T>[1] * (kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + T(-1) + kin.v[0]) * kin.v[1] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]))) + rlog(kin.v[3]) * (prod_pow(kin.v[4], 2) / T(2) + ((T(-4) * kin.v[2]) / T(3) + (T(4) * kin.v[3]) / T(3) + (T(4) * kin.v[4]) / T(3)) * kin.v[0] + ((T(-3) * kin.v[3]) / T(2) + -kin.v[4]) * kin.v[3] + kin.v[1] * ((T(-4) * kin.v[0]) / T(3) + bc<T>[1] * (-kin.v[3] + T(-1) + kin.v[0]) + (bc<T>[1] / T(2) + T(-1) / T(2)) * kin.v[1] + kin.v[2] + T(2) * kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-2) * kin.v[4]) + bc<T>[1] * (kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + T(-1) + kin.v[0]) * kin.v[1] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (prod_pow(kin.v[4], 2) / T(2) + ((T(-4) * kin.v[2]) / T(3) + (T(4) * kin.v[3]) / T(3) + (T(4) * kin.v[4]) / T(3)) * kin.v[0] + ((T(-3) * kin.v[3]) / T(2) + -kin.v[4]) * kin.v[3] + kin.v[1] * ((T(-4) * kin.v[0]) / T(3) + bc<T>[1] * (-kin.v[3] + T(-1) + kin.v[0]) + (bc<T>[1] / T(2) + T(-1) / T(2)) * kin.v[1] + kin.v[2] + T(2) * kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-2) * kin.v[4]) + bc<T>[1] * (kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + -kin.v[3] + T(-1) + kin.v[0]) * kin.v[1] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (kin.v[3] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[2] * (-kin.v[2] / T(2) + T(-1) + kin.v[4]))) + rlog(kin.v[0]) * (((T(-8) * kin.v[2]) / T(3) + (T(8) * kin.v[3]) / T(3) + (T(8) * kin.v[4]) / T(3)) * kin.v[0] + kin.v[1] * ((T(-8) * kin.v[0]) / T(3) + (bc<T>[1] + T(-1)) * kin.v[1] + T(2) * kin.v[2] + T(4) * kin.v[3] + bc<T>[1] * (T(-2) + T(2) * kin.v[0] + T(-2) * kin.v[3])) + prod_pow(kin.v[4], 2) + kin.v[2] * (T(3) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[3] * (T(-3) * kin.v[3] + T(-2) * kin.v[4]) + bc<T>[1] * (kin.v[3] * (T(2) + kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[3] * (T(2) + kin.v[3]) + kin.v[1] * (T(-2) + T(2) * kin.v[0] + kin.v[1] + T(-2) * kin.v[3]) + (-kin.v[4] + T(2)) * kin.v[4] + kin.v[0] * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]))) + bc<T>[0] * int_to_imaginary<T>(1) * (-prod_pow(kin.v[4], 2) + ((T(8) * kin.v[2]) / T(3) + (T(-8) * kin.v[3]) / T(3) + (T(-8) * kin.v[4]) / T(3)) * kin.v[0] + kin.v[1] * ((T(8) * kin.v[0]) / T(3) + kin.v[1] + T(-2) * kin.v[2] + T(-4) * kin.v[3]) + kin.v[3] * (T(3) * kin.v[3] + T(2) * kin.v[4]) + kin.v[2] * (T(-3) * kin.v[2] + T(4) * kin.v[4]) + bc<T>[1] * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3]) + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]))) + bc<T>[1] * (((T(16) * kin.v[2]) / T(3) + (T(-16) * kin.v[3]) / T(3) + (T(-16) * kin.v[4]) / T(3)) * kin.v[0] + kin.v[1] * ((T(16) * kin.v[0]) / T(3) + T(2) * kin.v[1] + T(-4) * kin.v[2] + T(-8) * kin.v[3]) + T(-2) * prod_pow(kin.v[4], 2) + kin.v[3] * (T(6) * kin.v[3] + T(4) * kin.v[4]) + kin.v[2] * (T(-6) * kin.v[2] + T(8) * kin.v[4]) + bc<T>[1] * ((-kin.v[3] + T(-2)) * kin.v[3] + kin.v[1] * (-kin.v[1] + T(2) + T(-2) * kin.v[0] + T(2) * kin.v[3]) + kin.v[4] * (T(-2) + kin.v[4]) + kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]))) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[1] * (T(4) * kin.v[0] + ((bc<T>[1] * T(-3)) / T(2) + T(3) / T(2)) * kin.v[1] + T(-3) * kin.v[2] + T(-6) * kin.v[3] + bc<T>[1] * (T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3])) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[3] * ((T(9) * kin.v[3]) / T(2) + T(3) * kin.v[4]) + kin.v[2] * ((T(-9) * kin.v[2]) / T(2) + T(6) * kin.v[4]) + bc<T>[1] * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]))) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + kin.v[1] * (T(4) * kin.v[0] + ((bc<T>[1] * T(-3)) / T(2) + T(3) / T(2)) * kin.v[1] + T(-3) * kin.v[2] + T(-6) * kin.v[3] + bc<T>[1] * (T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3])) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[3] * ((T(9) * kin.v[3]) / T(2) + T(3) * kin.v[4]) + kin.v[2] * ((T(-9) * kin.v[2]) / T(2) + T(6) * kin.v[4]) + bc<T>[1] * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3]) + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4])));
c[2] = (bc<T>[0] * int_to_imaginary<T>(4) + bc<T>[1] * T(4) + T(8)) * kin.v[1] + T(8) * kin.v[2] + T(-8) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + bc<T>[1] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + T(-8) * kin.v[4];
c[3] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(3) + bc<T>[1] * T(3) + T(6)) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[1] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(3) + bc<T>[1] * T(3) + T(6)) * kin.v[1] + T(6) * kin.v[2] + T(-6) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[1] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4])) + bc<T>[1] * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + bc<T>[1] * (T(2) * kin.v[1] + T(2) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4])) + rlog(kin.v[2]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[1] * (-kin.v[2] + kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[1] * (-kin.v[2] + kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(kin.v[3]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[1] * (-kin.v[2] + kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((-bc<T>[1] + T(-2) + bc<T>[0] * int_to_imaginary<T>(-1)) * kin.v[1] + T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4] + bc<T>[1] * (-kin.v[2] + kin.v[3] + kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(kin.v[0]) * ((T(-4) + bc<T>[1] * T(-2) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4] + bc<T>[1] * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-2) * kin.v[2] + T(2) * kin.v[3] + T(2) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[26] * (abb[20] * (abb[27] * T(-4) + abb[3] * abb[18] * T(-2) + abb[4] * abb[18] * T(-2) + abb[18] * (bc<T>[0] * int_to_imaginary<T>(2) + abb[18] * T(2))) + abb[3] * abb[18] * (-abb[1] + -abb[8] + -abb[9] + -abb[10] + abb[28] * T(-4) + abb[7] * T(3) + abb[11] * T(3)) + abb[4] * abb[18] * (-abb[1] + -abb[8] + -abb[9] + -abb[10] + abb[28] * T(-4) + abb[7] * T(3) + abb[11] * T(3)) + abb[18] * (abb[1] * bc<T>[0] * int_to_imaginary<T>(1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(1) + abb[7] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[28] * bc<T>[0] * int_to_imaginary<T>(4) + abb[18] * (abb[1] + abb[8] + abb[9] + abb[10] + abb[7] * T(-3) + abb[11] * T(-3) + abb[28] * T(4))) + abb[2] * (abb[18] * abb[20] * T(2) + abb[23] * (-abb[1] + -abb[8] + -abb[9] + -abb[10] + abb[28] * T(-4) + abb[20] * T(-2) + abb[7] * T(3) + abb[11] * T(3)) + abb[18] * (abb[1] + abb[8] + abb[9] + abb[10] + abb[7] * T(-3) + abb[11] * T(-3) + abb[28] * T(4))) + abb[27] * (abb[28] * T(-8) + abb[1] * T(-2) + abb[8] * T(-2) + abb[9] * T(-2) + abb[10] * T(-2) + abb[7] * T(6) + abb[11] * T(6)) + abb[23] * (abb[1] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[10] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[7] * bc<T>[0] * int_to_imaginary<T>(3) + abb[11] * bc<T>[0] * int_to_imaginary<T>(3) + abb[28] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[20] * (bc<T>[0] * int_to_imaginary<T>(-2) + abb[3] * T(2) + abb[4] * T(2) + abb[18] * T(2)) + abb[3] * (abb[1] + abb[8] + abb[9] + abb[10] + abb[7] * T(-3) + abb[11] * T(-3) + abb[28] * T(4)) + abb[4] * (abb[1] + abb[8] + abb[9] + abb[10] + abb[7] * T(-3) + abb[11] * T(-3) + abb[28] * T(4)) + abb[18] * (abb[1] + abb[8] + abb[9] + abb[10] + abb[7] * T(-3) + abb[11] * T(-3) + abb[28] * T(4)) + abb[23] * (abb[28] * T(-8) + abb[20] * T(-4) + abb[1] * T(-2) + abb[8] * T(-2) + abb[9] * T(-2) + abb[10] * T(-2) + abb[7] * T(6) + abb[11] * T(6))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_131_construct (const Kin<T>& kin) {
    return [&kin, 
            dl18 = DLog_W_18<T>(kin),dl27 = DLog_W_27<T>(kin),dl8 = DLog_W_8<T>(kin),dl13 = DLog_W_13<T>(kin),dl9 = DLog_W_9<T>(kin),dl11 = DLog_W_11<T>(kin),dl22 = DLog_W_22<T>(kin),dl1 = DLog_W_1<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl28 = DLog_W_28<T>(kin),dl29 = DLog_W_29<T>(kin),dl19 = DLog_W_19<T>(kin),dl4 = DLog_W_4<T>(kin),dl16 = DLog_W_16<T>(kin),dl5 = DLog_W_5<T>(kin),dl2 = DLog_W_2<T>(kin),dl20 = DLog_W_20<T>(kin),dl17 = DLog_W_17<T>(kin),dl3 = DLog_W_3<T>(kin),spdl22 = SpDLog_f_4_131_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,48> abbr = 
            {dl18(t), rlog(kin.W[2] / kin_path.W[2]), rlog(v_path[0]), rlog(v_path[2]), rlog(v_path[3]), f_2_1_1(kin_path), f_2_1_2(kin_path), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[18] / kin_path.W[18]), dl27(t) / kin_path.SqrtDelta, f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl8(t), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[0] / kin_path.W[0]), dl13(t), dl9(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(kin.W[1] / kin_path.W[1]), dl11(t), dl22(t), f_2_1_14(kin_path), -rlog(t), dl1(t), f_2_1_6(kin_path), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), rlog(v_path[0] + v_path[1]), dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl19(t), dl4(t), rlog(kin.W[8] / kin_path.W[8]), dl16(t), dl5(t), dl2(t), dl20(t), rlog(kin.W[7] / kin_path.W[7]), dl17(t), dl3(t)}
;

        auto result = f_4_131_abbreviated(abbr);
        result = result + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_131_abbreviated(const std::array<T,48>& abb)
{
using TR = typename T::value_type;
T z[146];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = abb[3];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[6];
z[7] = bc<TR>[0];
z[8] = abb[7];
z[9] = abb[8];
z[10] = abb[9];
z[11] = abb[10];
z[12] = abb[11];
z[13] = abb[29];
z[14] = abb[41];
z[15] = abb[42];
z[16] = abb[43];
z[17] = abb[44];
z[18] = abb[18];
z[19] = abb[39];
z[20] = abb[47];
z[21] = abb[23];
z[22] = abb[17];
z[23] = abb[38];
z[24] = abb[46];
z[25] = abb[27];
z[26] = abb[30];
z[27] = abb[31];
z[28] = abb[32];
z[29] = abb[33];
z[30] = abb[21];
z[31] = abb[25];
z[32] = abb[12];
z[33] = abb[13];
z[34] = abb[14];
z[35] = abb[15];
z[36] = abb[16];
z[37] = abb[35];
z[38] = abb[37];
z[39] = abb[19];
z[40] = abb[20];
z[41] = abb[24];
z[42] = abb[28];
z[43] = abb[36];
z[44] = abb[45];
z[45] = abb[22];
z[46] = abb[34];
z[47] = abb[40];
z[48] = bc<TR>[1];
z[49] = bc<TR>[3];
z[50] = bc<TR>[5];
z[51] = bc<TR>[2];
z[52] = bc<TR>[4];
z[53] = bc<TR>[7];
z[54] = bc<TR>[8];
z[55] = bc<TR>[9];
z[56] = z[8] + -z[10];
z[57] = -z[12] + z[56];
z[58] = z[1] + z[57];
z[59] = z[9] + -z[11];
z[60] = z[58] + z[59];
z[61] = z[0] * z[60];
z[62] = z[33] + z[34] + -z[35] + -z[36];
z[63] = z[43] * z[62];
z[64] = z[32] * z[62];
z[61] = -z[61] + z[63] + -z[64];
z[64] = T(3) * z[9];
z[65] = -z[11] + z[64];
z[66] = -z[40] + z[41];
z[67] = T(3) * z[56] + -z[65] + z[66];
z[67] = z[45] * z[67];
z[68] = z[38] * z[62];
z[69] = z[37] * z[62];
z[70] = z[68] + z[69];
z[71] = T(-5) * z[8] + T(3) * z[10];
z[72] = z[1] + z[11];
z[73] = z[12] + z[72];
z[74] = z[71] + -z[73];
z[75] = -z[64] + -z[74];
z[76] = z[40] / T(2);
z[77] = z[42] + z[76];
z[78] = z[75] / T(4) + -z[77];
z[78] = z[24] * z[78];
z[79] = z[46] * z[62];
z[78] = z[78] + z[79] / T(4);
z[80] = z[8] + T(5) * z[12];
z[81] = T(3) * z[1];
z[82] = z[80] + -z[81];
z[83] = z[9] + z[10];
z[84] = T(3) * z[11] + -z[82] + -z[83];
z[85] = z[77] + z[84] / T(4);
z[85] = z[23] * z[85];
z[86] = -z[39] + z[40];
z[87] = -z[12] + z[72];
z[88] = -z[9] + z[86] + T(3) * z[87];
z[88] = z[22] * z[88];
z[89] = (T(13) * z[11]) / T(3);
z[90] = -z[58] + -z[89];
z[91] = -z[39] + z[41];
z[90] = (T(13) * z[9]) / T(12) + z[90] / T(4) + -z[91];
z[90] = z[13] * z[90];
z[82] = (T(-7) * z[9]) / T(3) + -z[10] + -z[82] + -z[89];
z[82] = (T(13) * z[44]) / T(6) + z[77] + z[82] / T(4);
z[82] = z[20] * z[82];
z[71] = z[1] + (T(13) * z[9]) / T(3) + (T(7) * z[11]) / T(3) + z[12] + -z[71];
z[71] = (T(-13) * z[47]) / T(6) + z[71] / T(4) + -z[77];
z[71] = z[19] * z[71];
z[89] = z[15] / T(3);
z[92] = z[20] + -z[23] + z[24];
z[93] = z[19] + -z[92];
z[94] = z[89] + z[93];
z[95] = z[51] * z[94];
z[96] = z[40] + z[42];
z[96] = (T(7) * z[48]) / T(6) + (T(-13) * z[51]) / T(3) + z[96] / T(3);
z[97] = (T(-4) * z[1]) / T(3) + -z[8] / T(2) + (T(-7) * z[11]) / T(6) + (T(3) * z[12]) / T(2) + z[47] / T(6) + (T(2) * z[83]) / T(3) + -z[96];
z[97] = z[14] * z[97];
z[96] = (T(-3) * z[8]) / T(2) + (T(7) * z[9]) / T(6) + (T(4) * z[10]) / T(3) + z[12] / T(2) + -z[44] / T(6) + (T(-2) * z[72]) / T(3) + z[96];
z[96] = z[17] * z[96];
z[98] = (T(13) * z[15]) / T(12) + z[93] / T(4);
z[98] = z[48] * z[98];
z[99] = (T(-13) * z[48]) / T(2) + T(7) * z[51];
z[99] = z[16] * z[99];
z[61] = -z[61] / T(4) + z[67] + -z[70] / T(2) + z[71] + -z[78] + z[82] + -z[85] + z[88] + z[90] + (T(-7) * z[95]) / T(2) + z[96] + z[97] + z[98] + z[99] / T(6);
z[61] = z[7] * z[61];
z[70] = z[38] + z[43];
z[71] = z[37] + z[46];
z[82] = -z[70] + z[71];
z[90] = z[49] * z[82];
z[61] = z[61] + -z[90];
z[61] = z[7] * z[61];
z[95] = T(2) * z[42];
z[96] = z[40] + z[95];
z[97] = T(3) * z[8];
z[98] = -z[72] + T(2) * z[83] + z[96] + -z[97];
z[99] = z[17] * z[98];
z[100] = z[1] + z[83];
z[101] = -z[41] + -z[95] + z[100];
z[102] = z[14] * z[101];
z[103] = T(2) * z[19];
z[104] = z[9] + -z[47];
z[104] = z[103] * z[104];
z[105] = z[20] * z[59];
z[106] = -z[11] + z[41];
z[106] = z[31] * z[106];
z[107] = z[9] + -z[41];
z[107] = z[13] * z[107];
z[99] = z[67] + z[99] + z[102] + z[104] + -z[105] + z[106] + z[107];
z[99] = z[4] * z[99];
z[104] = T(3) * z[12];
z[107] = T(2) * z[72] + -z[83] + z[96] + -z[104];
z[108] = z[14] * z[107];
z[109] = z[19] * z[59];
z[110] = -z[88] + z[109];
z[95] = -z[10] + -z[72] + z[95];
z[111] = z[39] + z[95];
z[112] = z[17] * z[111];
z[113] = z[9] + -z[39];
z[113] = z[30] * z[113];
z[114] = -z[11] + z[39];
z[114] = z[13] * z[114];
z[115] = -z[11] + z[44];
z[115] = z[20] * z[115];
z[112] = -z[108] + -z[110] + z[112] + z[113] + z[114] + T(2) * z[115];
z[112] = z[3] * z[112];
z[99] = z[99] + z[112];
z[112] = z[97] + z[104];
z[114] = z[72] + z[83] + -z[112];
z[115] = z[96] + z[114] / T(2);
z[116] = z[17] * z[115];
z[117] = (T(3) * z[62]) / T(2);
z[118] = z[38] * z[117];
z[116] = z[116] + z[118];
z[119] = z[14] * z[115];
z[120] = z[37] * z[117];
z[119] = z[119] + -z[120];
z[121] = z[19] + z[20];
z[122] = T(2) * z[59];
z[122] = z[121] * z[122];
z[122] = z[116] + -z[119] + z[122];
z[123] = z[12] + z[56];
z[124] = -z[72] + z[123];
z[125] = z[9] / T(2);
z[86] = -z[86] + (T(3) * z[124]) / T(2) + -z[125];
z[124] = z[15] * z[86];
z[126] = z[106] + z[113];
z[123] = -z[81] + T(3) * z[123];
z[64] = -z[11] + -z[64] + z[123];
z[64] = z[64] / T(2) + z[66];
z[66] = z[16] * z[64];
z[127] = z[59] + -z[91];
z[127] = z[13] * z[127];
z[126] = z[66] + z[122] + -z[124] + T(-2) * z[126] + z[127];
z[126] = z[2] * z[126];
z[107] = z[20] * z[107];
z[107] = z[107] + z[108];
z[108] = z[19] * z[115];
z[116] = z[108] + z[116];
z[127] = z[13] * z[115];
z[84] = z[84] / T(2) + z[96];
z[128] = T(3) * z[84];
z[129] = z[23] * z[128];
z[130] = z[43] * z[117];
z[131] = z[129] + z[130];
z[132] = T(-2) * z[107] + z[116] + -z[127] + z[131];
z[132] = z[21] * z[132];
z[133] = z[13] * z[86];
z[133] = z[120] + z[133];
z[134] = z[20] * z[128];
z[129] = -z[129] + -z[133] + z[134];
z[129] = z[28] * z[129];
z[134] = z[13] * z[64];
z[75] = z[75] / T(2) + -z[96];
z[135] = T(3) * z[75];
z[136] = z[19] * z[135];
z[75] = z[24] * z[75];
z[75] = z[75] + z[79] / T(2);
z[79] = T(3) * z[75];
z[134] = z[79] + z[120] + -z[134] + -z[136];
z[136] = -(z[29] * z[134]);
z[137] = T(-5) * z[22] + (T(-11) * z[46]) / T(30);
z[137] = (T(-11) * z[37]) / T(90) + z[93] + z[137] / T(3);
z[138] = -z[14] + z[17];
z[139] = -z[16] + z[138];
z[70] = (T(5) * z[45]) / T(6) + (T(11) * z[70]) / T(180) + -z[89] + z[137] / T(2) + -z[139] / T(3);
z[70] = prod_pow(z[7], 2) * z[70];
z[89] = -z[28] + -z[29];
z[89] = z[62] * z[89];
z[137] = prod_pow(z[48], 2);
z[139] = z[137] / T(10);
z[89] = z[89] + z[139];
z[89] = z[38] * z[89];
z[62] = -(z[28] * z[62]);
z[62] = z[62] + z[139];
z[62] = z[43] * z[62];
z[62] = z[62] + z[89];
z[89] = z[28] * z[86];
z[139] = z[48] * z[51];
z[140] = T(2) * z[137] + -z[139];
z[89] = z[89] + z[140];
z[89] = z[15] * z[89];
z[141] = z[8] + z[10];
z[142] = z[12] + z[141];
z[142] = z[65] + z[81] + T(3) * z[142];
z[143] = z[40] + T(6) * z[42];
z[142] = T(-2) * z[41] + z[142] / T(2) + -z[143];
z[144] = -(z[29] * z[142]);
z[145] = z[28] * z[128];
z[144] = z[139] + z[144] + z[145];
z[144] = z[14] * z[144];
z[73] = z[73] + z[141];
z[73] = T(2) * z[39] + (T(-3) * z[73]) / T(2) + z[125] + z[143];
z[141] = -(z[28] * z[73]);
z[143] = z[29] * z[135];
z[139] = -z[139] + z[141] + z[143];
z[139] = z[17] * z[139];
z[141] = -(z[29] * z[64]);
z[140] = -z[140] + z[141];
z[140] = z[16] * z[140];
z[71] = z[71] * z[137];
z[137] = -z[15] + z[16];
z[137] = T(-3) * z[137] + -z[138];
z[137] = z[52] * z[137];
z[62] = (T(3) * z[62]) / T(2) + z[70] + (T(-3) * z[71]) / T(20) + z[89] + T(2) * z[99] + z[126] + z[129] + z[132] + z[136] + z[137] + z[139] + z[140] + z[144];
z[62] = z[7] * z[62];
z[70] = z[48] * z[90];
z[71] = z[50] * z[82];
z[70] = z[70] + (T(12) * z[71]) / T(5);
z[62] = z[62] + T(3) * z[70];
z[62] = int_to_imaginary<T>(1) * z[62];
z[70] = z[77] + z[114] / T(4);
z[71] = z[70] * z[121];
z[77] = z[17] * z[70];
z[68] = (T(3) * z[68]) / T(4) + z[77];
z[77] = -z[78] + z[85];
z[82] = -z[11] + z[100] + z[112];
z[89] = z[40] + z[41];
z[90] = T(4) * z[42];
z[82] = z[82] / T(2) + -z[89] + -z[90];
z[82] = z[13] * z[82];
z[97] = T(-7) * z[11] + T(9) * z[12] + -z[97];
z[99] = z[1] + T(13) * z[83] + z[97];
z[100] = T(2) * z[47];
z[76] = T(-5) * z[42] + -z[76] + z[99] / T(4) + -z[100];
z[76] = z[14] * z[76];
z[63] = (T(3) * z[63]) / T(4);
z[69] = (T(3) * z[69]) / T(4);
z[71] = z[63] + z[67] + z[68] + -z[69] + z[71] + z[76] + T(3) * z[77] + z[82];
z[71] = z[21] * z[71];
z[76] = z[20] * z[115];
z[77] = z[13] * z[101];
z[77] = z[66] + z[76] + z[77] + T(2) * z[102] + z[116];
z[82] = z[2] + -z[4];
z[77] = z[77] * z[82];
z[71] = z[71] + z[77];
z[71] = z[21] * z[71];
z[60] = (T(3) * z[60]) / T(2);
z[77] = z[0] * z[60];
z[99] = z[32] * z[117];
z[77] = z[77] + z[99];
z[57] = T(3) * z[57] + z[81];
z[81] = z[57] + -z[65];
z[81] = z[41] + z[81] / T(2);
z[81] = z[13] * z[81];
z[99] = z[20] * z[60];
z[101] = z[8] + -z[83] + z[87];
z[101] = (T(3) * z[101]) / T(2);
z[102] = -(z[19] * z[101]);
z[64] = z[14] * z[64];
z[64] = z[64] + -z[66] + -z[77] + z[81] + z[99] + z[102] + z[106] + z[120];
z[64] = z[6] * z[64];
z[56] = z[56] + z[87];
z[81] = z[39] + z[125];
z[56] = (T(3) * z[56]) / T(2) + -z[81];
z[56] = z[13] * z[56];
z[87] = -(z[20] * z[101]);
z[60] = z[19] * z[60];
z[56] = z[56] + z[60] + -z[77] + z[87] + z[113] + z[118] + z[124];
z[56] = z[5] * z[56];
z[60] = z[14] * z[70];
z[60] = z[60] + -z[68] + -z[69];
z[68] = z[60] + -z[105];
z[69] = z[9] + T(3) * z[74];
z[70] = (T(3) * z[40]) / T(2) + T(3) * z[42];
z[69] = z[69] / T(4) + z[70] + z[100];
z[69] = z[19] * z[69];
z[74] = z[66] / T(2) + -z[106];
z[87] = -z[59] + -z[123];
z[87] = z[87] / T(2) + z[89];
z[89] = z[13] / T(2);
z[87] = z[87] * z[89];
z[67] = -z[67] + -z[68] + z[69] + z[74] + T(3) * z[78] + z[87];
z[67] = prod_pow(z[4], 2) * z[67];
z[69] = z[113] + z[124] / T(2);
z[78] = z[11] + z[123];
z[78] = -z[40] + z[78] / T(2) + -z[81];
z[78] = z[78] * z[89];
z[80] = z[10] + z[80];
z[65] = T(-9) * z[1] + z[65] + T(3) * z[80];
z[65] = T(-2) * z[44] + z[65] / T(4) + -z[70];
z[65] = z[20] * z[65];
z[60] = -z[60] + z[63] + z[65] + -z[69] + z[78] + T(3) * z[85] + z[110];
z[60] = z[3] * z[60];
z[57] = z[57] + -z[59];
z[57] = z[57] * z[89];
z[57] = z[57] + -z[77] + z[122];
z[63] = -(z[57] * z[82]);
z[60] = z[60] + z[63] + -z[132];
z[60] = z[3] * z[60];
z[63] = z[76] + z[119];
z[65] = T(2) * z[17];
z[70] = z[65] * z[111];
z[76] = z[13] * z[111];
z[70] = z[63] + -z[70] + -z[76] + z[108] + z[124];
z[78] = -z[2] + z[3];
z[70] = z[70] * z[78];
z[78] = -(z[19] * z[98]);
z[80] = z[44] + z[95];
z[80] = z[65] * z[80];
z[76] = z[76] + z[78] + z[80] + z[88] + -z[107];
z[76] = z[18] * z[76];
z[65] = z[65] + z[103];
z[65] = z[65] * z[98];
z[63] = -z[63] + z[65] + z[79] + z[127];
z[65] = int_to_imaginary<T>(1) * z[7];
z[65] = -z[4] + z[21] + z[65];
z[63] = z[63] * z[65];
z[63] = z[63] + z[70] + z[76];
z[63] = z[18] * z[63];
z[65] = z[23] * z[84];
z[65] = z[65] + -z[75];
z[70] = T(-2) * z[40] + -z[90] + -z[114];
z[70] = z[13] * z[70];
z[75] = T(-7) * z[1] + T(5) * z[83] + z[97];
z[75] = z[75] / T(2) + -z[96];
z[76] = z[20] * z[75];
z[65] = T(3) * z[65] + z[70] + z[76] + z[118] + -z[120] + z[130];
z[65] = z[25] * z[65];
z[58] = (T(3) * z[58]) / T(2) + -z[59] + z[91] / T(2);
z[58] = z[13] * z[58];
z[58] = z[58] + -z[68] + z[69] + -z[74] + -z[77] + z[109];
z[58] = z[2] * z[58];
z[57] = -(z[4] * z[57]);
z[57] = z[57] + z[58];
z[57] = z[2] * z[57];
z[58] = z[17] * z[73];
z[59] = -z[14] + -z[20];
z[59] = z[59] * z[128];
z[58] = z[58] + z[59] + z[118] + -z[124] + z[131] + z[133];
z[58] = z[27] * z[58];
z[59] = z[14] * z[142];
z[68] = -(z[17] * z[135]);
z[59] = z[59] + z[66] + z[68] + z[118] + z[134];
z[59] = z[26] * z[59];
z[66] = T(-9) * z[8] + T(-5) * z[72] + T(7) * z[83] + z[104];
z[66] = z[66] / T(2) + z[96];
z[66] = z[25] * z[66];
z[68] = -(z[5] * z[86]);
z[69] = prod_pow(z[51], 3);
z[70] = prod_pow(z[48], 3);
z[72] = T(5) * z[53] + z[54] + (T(7) * z[55]) / T(6) + (T(-4) * z[69]) / T(3) + (T(2) * z[70]) / T(3);
z[68] = -z[66] + z[68] + z[72];
z[68] = z[17] * z[68];
z[73] = T(6) * z[53] + (T(3) * z[54]) / T(2) + (T(-31) * z[55]) / T(4);
z[66] = -z[66] + z[73];
z[66] = z[19] * z[66];
z[74] = z[25] * z[75];
z[72] = -z[72] + z[74];
z[72] = z[14] * z[72];
z[73] = -(z[73] * z[92]);
z[74] = z[70] * z[93];
z[75] = z[69] * z[94];
z[70] = -z[53] + -z[54] / T(2) + (T(7) * z[55]) / T(12) + z[70] / T(6);
z[69] = (T(2) * z[69]) / T(3) + z[70];
z[69] = z[16] * z[69];
z[76] = z[93] + z[138];
z[76] = z[48] * z[52] * z[76];
z[70] = -(z[15] * z[70]);
return z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] + z[66] + z[67] + z[68] + z[69] + z[70] + z[71] + z[72] + z[73] + z[74] / T(2) + T(-2) * z[75] + T(3) * z[76];
}



template IntegrandConstructorType<double> f_4_131_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_131_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_131_construct (const Kin<qd_real>&);
#endif

}