#include "f_4_271.h"

namespace PentagonFunctions {

template <typename T> T f_4_271_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_271_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_271_W_12 (const Kin<T>& kin) {
        c[0] = (-kin.v[1] + T(-2)) * kin.v[1] + kin.v[4] * (T(2) + kin.v[4]);
c[1] = kin.v[1] * (T(2) + kin.v[1]) + (-kin.v[4] + T(-2)) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[4] * c[0] + abb[3] * c[1]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[4] + -(prod_pow(abb[2], 2) * abb[3]) + prod_pow(abb[1], 2) * (abb[3] + -abb[4]));
    }
};
template <typename T> class SpDLog_f_4_271_W_10 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_271_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[1] * (-kin.v[1] / T(4) + -kin.v[2] / T(2) + T(1) + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(4) + T(1) + kin.v[4]);
c[1] = -kin.v[1] + -kin.v[2];
c[2] = kin.v[1] * (-kin.v[1] / T(4) + -kin.v[2] / T(2) + T(1) + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(4) + T(1) + kin.v[4]);
c[3] = -kin.v[1] + -kin.v[2];
c[4] = kin.v[1] * (-kin.v[1] / T(4) + -kin.v[2] / T(2) + T(1) + kin.v[4]) + kin.v[2] * (-kin.v[2] / T(4) + T(1) + kin.v[4]);
c[5] = -kin.v[1] + -kin.v[2];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[3] * (t * c[0] + c[1]) + abb[8] * (t * c[2] + c[3]) + abb[9] * (t * c[4] + c[5]);
        }

        return abb[5] * (abb[7] * (abb[8] + abb[9]) + abb[3] * (abb[7] + -prod_pow(abb[6], 2) / T(2)) + prod_pow(abb[6], 2) * (-abb[8] / T(2) + -abb[9] / T(2)) + abb[2] * (abb[2] * ((abb[3] * T(3)) / T(2) + (abb[8] * T(3)) / T(2) + (abb[9] * T(3)) / T(2)) + -(abb[3] * abb[6]) + abb[6] * (-abb[8] + -abb[9])));
    }
};
template <typename T> class SpDLog_f_4_271_W_7 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_271_W_7 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (-kin.v[0] + T(4) + T(4) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(1) + kin.v[1]) + T(2) * kin.v[2] + (T(-1) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[3] + T(-2) * kin.v[4]) + -(kin.v[0] * kin.v[4]) + T(4) * kin.v[1] * kin.v[4] + T(2) * kin.v[2] * kin.v[4] + ((T(-3) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4]);
c[1] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4];
c[2] = kin.v[3] * (-kin.v[0] + T(4) + T(4) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(1) + kin.v[1]) + T(2) * kin.v[2] + (T(-1) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[3] + T(-2) * kin.v[4]) + -(kin.v[0] * kin.v[4]) + T(4) * kin.v[1] * kin.v[4] + T(2) * kin.v[2] * kin.v[4] + ((T(-3) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4]);
c[3] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4];
c[4] = kin.v[3] * (-kin.v[0] + T(4) + T(4) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(1) + kin.v[1]) + T(2) * kin.v[2] + (T(-1) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[3] + T(-2) * kin.v[4]) + -(kin.v[0] * kin.v[4]) + T(4) * kin.v[1] * kin.v[4] + T(2) * kin.v[2] * kin.v[4] + ((T(-3) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4]);
c[5] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[8] * (t * c[0] + c[1]) + abb[4] * (t * c[2] + c[3]) + abb[12] * (t * c[4] + c[5]);
        }

        return abb[16] * (abb[6] * (abb[4] + abb[12]) * abb[15] + abb[14] * abb[15] * (-abb[4] + -abb[12]) + abb[2] * (abb[15] * (-abb[4] + -abb[12]) + -(abb[8] * abb[15])) + abb[17] * (abb[4] * T(-2) + abb[12] * T(-2)) + abb[1] * (abb[2] * (abb[4] + abb[8] + abb[12]) + (abb[4] + abb[12]) * abb[14] + (abb[4] + abb[12]) * abb[15] + abb[6] * (-abb[4] + -abb[12]) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[8] * (abb[14] + abb[15] + -abb[6] + bc<T>[0] * int_to_imaginary<T>(-1))) + abb[15] * (abb[15] * (-abb[4] + -abb[12]) + abb[4] * bc<T>[0] * int_to_imaginary<T>(1) + abb[12] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[8] * (abb[6] * abb[15] + -(abb[14] * abb[15]) + abb[17] * T(-2) + abb[15] * (-abb[15] + bc<T>[0] * int_to_imaginary<T>(1))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_271_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl10 = DLog_W_10<T>(kin),dl3 = DLog_W_3<T>(kin),dl25 = DLog_W_25<T>(kin),dl7 = DLog_W_7<T>(kin),dl24 = DLog_W_24<T>(kin),dl1 = DLog_W_1<T>(kin),dl16 = DLog_W_16<T>(kin),dl19 = DLog_W_19<T>(kin),dl2 = DLog_W_2<T>(kin),dl18 = DLog_W_18<T>(kin),dl5 = DLog_W_5<T>(kin),dl17 = DLog_W_17<T>(kin),dl4 = DLog_W_4<T>(kin),dl20 = DLog_W_20<T>(kin),dl31 = DLog_W_31<T>(kin),dl27 = DLog_W_27<T>(kin),dl30 = DLog_W_30<T>(kin),dl29 = DLog_W_29<T>(kin),dl26 = DLog_W_26<T>(kin),dl28 = DLog_W_28<T>(kin),spdl12 = SpDLog_f_4_271_W_12<T>(kin),spdl10 = SpDLog_f_4_271_W_10<T>(kin),spdl7 = SpDLog_f_4_271_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,46> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl10(t), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_7(kin_path), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl3(t), f_2_1_4(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl25(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), dl7(t), f_2_1_11(kin_path), dl24(t), f_2_1_10(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), dl1(t), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl16(t), dl19(t), dl2(t), dl18(t), dl5(t), dl17(t), dl4(t), dl20(t), dl31(t), dl27(t) / kin_path.SqrtDelta, rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), dl30(t) / kin_path.SqrtDelta, dl29(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta}
;

        auto result = f_4_271_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_271_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[131];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[25];
z[3] = abb[26];
z[4] = abb[28];
z[5] = abb[30];
z[6] = abb[31];
z[7] = abb[4];
z[8] = abb[10];
z[9] = abb[27];
z[10] = abb[29];
z[11] = abb[8];
z[12] = abb[12];
z[13] = abb[33];
z[14] = abb[34];
z[15] = abb[35];
z[16] = abb[36];
z[17] = abb[37];
z[18] = abb[38];
z[19] = abb[39];
z[20] = abb[40];
z[21] = abb[41];
z[22] = abb[43];
z[23] = abb[44];
z[24] = abb[2];
z[25] = abb[14];
z[26] = abb[15];
z[27] = bc<TR>[0];
z[28] = abb[6];
z[29] = abb[45];
z[30] = abb[24];
z[31] = abb[9];
z[32] = abb[42];
z[33] = abb[18];
z[34] = abb[7];
z[35] = abb[11];
z[36] = abb[13];
z[37] = abb[17];
z[38] = abb[19];
z[39] = abb[20];
z[40] = abb[22];
z[41] = abb[23];
z[42] = abb[21];
z[43] = bc<TR>[3];
z[44] = abb[32];
z[45] = bc<TR>[1];
z[46] = bc<TR>[2];
z[47] = bc<TR>[4];
z[48] = bc<TR>[9];
z[49] = z[14] / T(2) + T(2) * z[16] + -z[17];
z[50] = z[29] * z[49];
z[51] = z[13] * z[49];
z[52] = z[12] / T(2);
z[53] = -z[11] + z[52];
z[53] = z[10] * z[53];
z[53] = z[50] + -z[51] + z[53];
z[54] = z[7] + z[11];
z[55] = -z[1] + z[54];
z[55] = z[33] * z[55];
z[56] = -z[4] + z[33];
z[57] = z[31] * z[56];
z[57] = z[55] + -z[57];
z[58] = z[19] + z[20];
z[58] = -z[58] / T(2);
z[59] = -z[13] + z[22] / T(2) + z[29];
z[58] = z[58] * z[59];
z[60] = z[7] + z[12];
z[61] = z[1] + z[11];
z[62] = -z[60] + z[61];
z[62] = z[36] * z[62];
z[63] = T(2) * z[1];
z[64] = T(7) * z[7] + -z[11] + T(11) * z[12];
z[64] = -z[63] + z[64] / T(4);
z[65] = z[3] / T(3);
z[64] = z[64] * z[65];
z[52] = -z[7] / T(2) + -z[52] + -z[61];
z[66] = z[2] / T(3);
z[52] = z[52] * z[66];
z[67] = z[1] / T(3);
z[68] = z[12] / T(3);
z[69] = z[7] / T(3) + z[68];
z[70] = z[11] + -z[69];
z[70] = z[67] + z[70] / T(4);
z[70] = z[5] * z[70];
z[68] = z[54] + z[68];
z[68] = z[8] * z[68];
z[71] = z[11] / T(3);
z[72] = z[60] + -z[71];
z[67] = -z[67] + z[72] / T(4);
z[67] = z[6] * z[67];
z[72] = -z[23] + z[29];
z[73] = T(9) * z[13] + z[72];
z[73] = (T(-5) * z[32]) / T(4) + z[73] / T(4);
z[73] = z[46] * z[73];
z[74] = z[14] / T(4) + z[16] + -z[17] / T(2);
z[75] = -z[46] + z[74] / T(3);
z[75] = z[22] * z[75];
z[71] = (T(2) * z[1]) / T(3) + -z[7] + z[12] / T(6) + -z[71];
z[71] = z[4] * z[71];
z[76] = z[11] / T(2) + -z[60];
z[76] = z[1] / T(2) + z[76] / T(3);
z[76] = z[9] * z[76];
z[77] = z[22] + z[29];
z[78] = -z[13] + -z[77] / T(3);
z[78] = z[45] * z[78];
z[69] = -z[11] + -z[69];
z[69] = z[42] * z[69];
z[52] = z[52] + z[53] / T(3) + (T(7) * z[57]) / T(6) + z[58] + z[62] + z[64] + z[67] + z[68] / T(4) + z[69] + z[70] + z[71] + z[73] + z[75] + z[76] + z[78];
z[52] = z[27] * z[52];
z[53] = T(3) * z[7];
z[57] = -z[11] + z[53];
z[58] = z[12] + z[57];
z[64] = z[58] / T(2) + -z[63];
z[67] = z[30] * z[64];
z[68] = z[32] * z[49];
z[67] = -z[67] + z[68];
z[68] = z[23] * z[49];
z[69] = -z[50] + z[68];
z[70] = z[67] + z[69];
z[71] = z[10] + z[30];
z[73] = T(2) * z[9];
z[75] = z[71] + -z[73];
z[76] = -z[4] + -z[33] + z[75];
z[76] = z[31] * z[76];
z[78] = (T(3) * z[19]) / T(2);
z[79] = (T(3) * z[20]) / T(2);
z[80] = z[78] + z[79];
z[81] = -z[32] + z[72];
z[82] = z[13] + z[81];
z[83] = z[80] * z[82];
z[84] = z[4] * z[64];
z[85] = T(3) * z[11];
z[86] = z[60] + z[85];
z[87] = z[86] / T(2);
z[88] = z[2] * z[87];
z[89] = -z[7] + z[11];
z[90] = z[12] + z[89];
z[91] = z[10] / T(2);
z[92] = z[90] * z[91];
z[87] = z[42] * z[87];
z[53] = -z[11] + z[12] + -z[53];
z[53] = z[1] + z[53] / T(2);
z[93] = z[9] * z[53];
z[76] = z[51] + -z[55] + -z[70] + -z[76] + -z[83] + -z[84] + z[87] + -z[88] + -z[92] + z[93];
z[83] = -(z[39] * z[76]);
z[49] = z[22] * z[49];
z[84] = z[49] + -z[69];
z[88] = -z[1] + z[60];
z[92] = z[73] * z[88];
z[93] = T(3) * z[12];
z[57] = z[57] + z[93];
z[94] = z[57] / T(2) + -z[63];
z[95] = z[6] * z[94];
z[96] = z[11] + z[60];
z[97] = z[91] * z[96];
z[98] = z[3] * z[88];
z[99] = z[96] / T(2);
z[100] = z[4] * z[99];
z[101] = z[2] * z[99];
z[92] = -z[84] + z[92] + -z[95] + -z[97] + T(2) * z[98] + z[100] + z[101];
z[102] = z[0] * z[92];
z[103] = z[3] * z[99];
z[104] = -z[51] + z[103];
z[105] = z[4] / T(2);
z[106] = -z[12] + z[54];
z[107] = z[105] * z[106];
z[108] = z[2] * z[11];
z[109] = z[10] * z[11];
z[110] = -z[11] + z[60];
z[111] = -z[1] + z[110] / T(2);
z[112] = z[9] * z[111];
z[107] = -z[55] + -z[104] + z[107] + -z[108] + -z[109] + z[112];
z[107] = z[28] * z[107];
z[112] = z[86] * z[91];
z[84] = z[51] + -z[84] + -z[112];
z[112] = z[3] + z[9];
z[113] = -(z[94] * z[112]);
z[85] = -z[60] + z[85];
z[85] = z[85] / T(2);
z[114] = z[2] * z[85];
z[95] = -z[84] + z[95] + z[113] + z[114];
z[95] = z[41] * z[95];
z[113] = T(2) * z[4];
z[73] = z[73] + z[113];
z[114] = z[1] + -z[7];
z[73] = z[73] * z[114];
z[115] = z[91] * z[106];
z[70] = -z[70] + z[73] + -z[101] + z[103] + z[115];
z[73] = z[24] * z[70];
z[103] = z[22] + -z[23] + z[32];
z[115] = z[5] + z[6];
z[116] = z[4] + z[9];
z[65] = (T(-2) * z[10]) / T(9) + (T(-7) * z[13]) / T(4) + (T(4) * z[44]) / T(9) + -z[65] + -z[66] + z[103] / T(4) + z[115] / T(9) + -z[116] / T(3);
z[66] = prod_pow(z[27], 2);
z[65] = z[65] * z[66];
z[103] = z[13] * z[26];
z[117] = z[24] * z[81];
z[103] = z[103] + z[117];
z[117] = z[22] + z[72];
z[118] = z[0] * z[117];
z[119] = -z[13] + z[117];
z[120] = z[41] * z[119];
z[121] = z[13] * z[28];
z[118] = -z[103] + z[118] + -z[120] + -z[121];
z[80] = z[80] * z[118];
z[120] = z[39] * z[82];
z[118] = z[118] + z[120];
z[120] = z[18] + z[21];
z[122] = z[15] / T(2);
z[123] = (T(3) * z[120]) / T(2) + -z[122];
z[118] = z[118] * z[123];
z[99] = z[9] * z[99];
z[104] = z[99] + z[104];
z[123] = T(2) * z[2];
z[124] = z[11] * z[123];
z[124] = z[100] + z[104] + z[109] + z[124];
z[125] = -(z[26] * z[124]);
z[114] = z[4] * z[114];
z[114] = z[55] + z[62] + z[114];
z[98] = z[98] + -z[109];
z[108] = z[98] + -z[108] + z[114];
z[109] = T(2) * z[25];
z[108] = z[108] * z[109];
z[109] = z[28] + -z[109];
z[109] = z[56] * z[109];
z[75] = z[75] + -z[113];
z[126] = -(z[24] * z[75]);
z[109] = z[109] + z[126];
z[109] = z[31] * z[109];
z[126] = z[45] / T(2) + -z[46];
z[127] = T(3) * z[13] + z[77];
z[126] = z[45] * z[126] * z[127];
z[128] = z[42] / T(2);
z[128] = z[86] * z[128];
z[129] = z[26] + z[28] + -z[41];
z[129] = z[128] * z[129];
z[130] = prod_pow(z[46], 2) * z[82];
z[127] = z[47] * z[127];
z[65] = z[65] / T(3) + z[73] + z[80] + z[83] + z[95] + z[102] + z[107] + z[108] + z[109] + z[118] + z[125] + z[126] + z[127] + z[129] + z[130] / T(2);
z[65] = int_to_imaginary<T>(1) * z[65];
z[73] = z[2] + z[3] + z[116];
z[80] = z[10] + T(-2) * z[44];
z[73] = T(3) * z[73] + T(2) * z[80] + -z[115];
z[73] = z[43] * z[73];
z[52] = z[52] + z[65] + z[73];
z[52] = z[27] * z[52];
z[65] = -z[29] + z[32];
z[65] = z[65] * z[74];
z[73] = z[10] / T(4);
z[80] = z[73] * z[90];
z[83] = z[105] * z[111];
z[58] = -z[1] + z[58] / T(4);
z[58] = z[30] * z[58];
z[90] = z[9] * z[106];
z[95] = z[90] / T(4);
z[102] = z[3] * z[54];
z[107] = z[23] * z[74];
z[108] = z[13] * z[74];
z[109] = z[96] / T(4);
z[115] = z[2] * z[109];
z[55] = z[55] + -z[58] + z[65] + z[80] + z[83] + z[95] + z[102] / T(2) + z[107] + -z[108] + z[115];
z[65] = prod_pow(z[28], 2);
z[55] = z[55] * z[65];
z[76] = -(z[38] * z[76]);
z[80] = z[2] + z[4];
z[83] = z[61] * z[80];
z[107] = z[107] + z[108];
z[108] = z[63] + z[85];
z[115] = z[5] * z[108];
z[116] = -z[54] + -z[93];
z[116] = z[73] * z[116];
z[89] = -z[89] + z[93];
z[89] = -z[1] + z[89] / T(2);
z[89] = z[3] * z[89];
z[57] = -z[1] + z[57] / T(4);
z[93] = -(z[6] * z[57]);
z[118] = T(3) * z[54];
z[125] = z[12] + z[118];
z[126] = z[8] * z[125];
z[127] = -(z[22] * z[74]);
z[129] = z[9] * z[109];
z[83] = z[83] + z[89] + z[93] + -z[107] + -z[115] + z[116] + z[126] / T(4) + z[127] + z[129];
z[83] = z[0] * z[83];
z[89] = -(z[25] * z[92]);
z[50] = z[50] + z[51];
z[54] = z[10] * z[54];
z[54] = z[50] + -z[54] + -z[101];
z[92] = z[126] / T(2);
z[93] = -z[54] + -z[92] + z[99] + z[100] + T(2) * z[102];
z[93] = z[28] * z[93];
z[99] = z[113] + z[123];
z[99] = z[61] * z[99];
z[97] = -z[68] + -z[97] + z[99] + z[104] + -z[115];
z[99] = -z[24] + -z[26];
z[99] = z[97] * z[99];
z[83] = z[83] + z[89] + z[93] + z[99];
z[83] = z[0] * z[83];
z[89] = z[13] + z[23];
z[93] = z[22] + z[89];
z[99] = z[0] / T(2);
z[93] = z[93] * z[99];
z[99] = z[26] * z[89];
z[100] = z[25] * z[117];
z[101] = z[13] + z[29];
z[101] = z[28] * z[101];
z[104] = z[24] * z[89];
z[93] = z[93] + -z[99] + -z[100] + z[101] + -z[104];
z[93] = z[0] * z[93];
z[81] = z[28] * z[81];
z[99] = z[25] * z[119];
z[81] = -z[81] + z[99] / T(2) + z[103];
z[81] = z[25] * z[81];
z[99] = -z[104] + z[121];
z[99] = z[26] * z[99];
z[100] = z[24] / T(2);
z[89] = z[32] + z[89];
z[103] = z[89] * z[100];
z[101] = -z[101] + z[103];
z[101] = z[24] * z[101];
z[103] = z[65] / T(2);
z[104] = z[82] * z[103];
z[116] = z[13] + z[77];
z[116] = z[37] * z[116];
z[117] = z[40] * z[119];
z[89] = z[34] * z[89];
z[81] = z[81] + z[89] + z[93] + -z[99] + z[101] + z[104] + z[116] + z[117];
z[89] = z[35] * z[72];
z[89] = -z[81] + z[89];
z[82] = z[38] * z[82];
z[82] = z[82] + -z[89];
z[59] = z[59] * z[66];
z[66] = -z[59] + T(3) * z[82];
z[93] = z[120] / T(2);
z[66] = z[66] * z[93];
z[93] = z[53] * z[105];
z[99] = -(z[32] * z[74]);
z[101] = -(z[10] * z[109]);
z[104] = z[3] * z[106];
z[105] = z[2] * z[61];
z[58] = z[58] + z[93] + z[95] + z[99] + z[101] + -z[104] / T(4) + z[105] + -z[107];
z[58] = z[24] * z[58];
z[93] = -(z[4] * z[111]);
z[54] = z[54] + -z[90] / T(2) + z[93] + -z[102];
z[54] = z[28] * z[54];
z[54] = z[54] + z[58];
z[54] = z[24] * z[54];
z[58] = -(z[74] * z[77]);
z[74] = z[2] + -z[3];
z[74] = z[74] * z[109];
z[73] = -(z[73] * z[110]);
z[77] = z[9] * z[57];
z[86] = z[42] * z[86];
z[58] = z[58] + z[73] + z[74] + z[77] + z[86] / T(4) + z[107] + -z[114];
z[58] = z[25] * z[58];
z[73] = z[24] + -z[28];
z[70] = -(z[70] * z[73]);
z[74] = -z[87] + z[124];
z[74] = z[26] * z[74];
z[58] = z[58] + z[70] + z[74];
z[58] = z[25] * z[58];
z[70] = z[79] * z[81];
z[74] = -(z[78] * z[89]);
z[59] = z[59] / T(3) + -z[82];
z[59] = z[59] * z[122];
z[77] = -z[12] + z[118];
z[78] = z[77] * z[91];
z[64] = -(z[9] * z[64]);
z[81] = z[3] / T(2);
z[82] = z[81] * z[125];
z[53] = z[4] * z[53];
z[51] = -z[51] + z[53] + z[64] + -z[67] + -z[68] + z[78] + z[82];
z[51] = z[34] * z[51];
z[53] = z[80] * z[108];
z[64] = -(z[91] * z[125]);
z[67] = -(z[77] * z[81]);
z[68] = -(z[72] * z[79]);
z[53] = z[53] + z[64] + z[67] + z[68] + -z[69] + z[92] + -z[115];
z[53] = z[35] * z[53];
z[64] = T(5) * z[11] + z[60];
z[64] = z[63] + z[64] / T(2);
z[67] = z[4] * z[64];
z[68] = -(z[10] * z[96]);
z[49] = -z[49] + -z[50] + z[67] + z[68] + -z[115];
z[49] = z[37] * z[49];
z[50] = z[24] * z[97];
z[67] = -(z[28] * z[124]);
z[61] = z[4] * z[61];
z[68] = z[9] * z[88];
z[61] = z[61] + z[68] + z[98];
z[61] = z[26] * z[61];
z[50] = z[50] + z[61] + z[67];
z[50] = z[26] * z[50];
z[61] = z[9] / T(2);
z[67] = -z[33] + -z[61] + z[71] / T(2);
z[65] = z[65] * z[67];
z[61] = z[4] + -z[30] / T(2) + -z[61] + -z[91];
z[61] = z[24] * z[61];
z[67] = z[9] * z[28];
z[61] = z[61] + z[67];
z[61] = z[24] * z[61];
z[67] = z[9] + -z[71] + z[113];
z[67] = z[34] * z[67];
z[68] = z[73] * z[75];
z[56] = z[25] * z[56];
z[56] = z[56] + z[68];
z[56] = z[25] * z[56];
z[56] = z[56] + z[61] + z[65] + z[67];
z[56] = z[31] * z[56];
z[61] = z[40] * z[94];
z[65] = -(z[37] * z[94]);
z[57] = -(prod_pow(z[25], 2) * z[57]);
z[57] = z[57] + -z[61] + z[65];
z[57] = z[6] * z[57];
z[60] = z[11] + T(5) * z[60];
z[60] = z[60] / T(2) + -z[63];
z[60] = z[37] * z[60];
z[60] = z[60] + z[61];
z[60] = z[60] * z[112];
z[61] = z[40] * z[84];
z[63] = z[37] * z[64];
z[64] = -(z[40] * z[85]);
z[63] = z[63] + z[64];
z[63] = z[2] * z[63];
z[64] = z[26] * z[28];
z[64] = z[40] + z[64] + -z[103];
z[64] = z[64] * z[128];
z[62] = prod_pow(z[26], 2) * z[62];
z[65] = z[28] + -z[100];
z[65] = z[24] * z[65];
z[65] = -z[34] + z[65];
z[65] = z[8] * z[65] * z[125];
z[67] = z[13] + -z[23];
z[67] = (T(61) * z[29]) / T(3) + z[32] + T(-5) * z[67];
z[67] = (T(8) * z[22]) / T(3) + z[67] / T(8);
z[67] = z[48] * z[67];
return z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + z[65] / T(2) + z[66] + z[67] + z[70] + z[74] + z[76] + z[83];
}



template IntegrandConstructorType<double> f_4_271_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_271_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_271_construct (const Kin<qd_real>&);
#endif

}