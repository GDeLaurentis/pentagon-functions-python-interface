#include "f_4_290.h"

namespace PentagonFunctions {

template <typename T> T f_4_290_abbreviated (const std::array<T,55>&);

template <typename T> class SpDLog_f_4_290_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_290_W_12 (const Kin<T>& kin) {
        c[0] = ((T(-3) * kin.v[1]) / T(2) + T(-3)) * kin.v[1] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4];
c[1] = ((T(3) * kin.v[1]) / T(2) + T(3)) * kin.v[1] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1]);
        }

        return abb[0] * ((prod_pow(abb[2], 2) * abb[4] * T(-3)) / T(2) + (prod_pow(abb[2], 2) * abb[3] * T(3)) / T(2) + prod_pow(abb[1], 2) * ((abb[3] * T(-3)) / T(2) + (abb[4] * T(3)) / T(2)));
    }
};
template <typename T> class SpDLog_f_4_290_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_290_W_22 (const Kin<T>& kin) {
        c[0] = ((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]);
c[1] = kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(-3) + T(3) * kin.v[0] + T(-3) * kin.v[3]) + ((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]);
c[2] = kin.v[1] * ((T(3) * kin.v[1]) / T(2) + T(-3) + T(3) * kin.v[0] + T(-3) * kin.v[3]) + ((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + kin.v[0] * (T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(-3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(-3) + T(3) * kin.v[4]);
c[3] = ((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(3) + T(-3) * kin.v[0] + T(3) * kin.v[3]) + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(3) + T(-3) * kin.v[4]) + ((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[0] * (T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1] + abb[8] * c[2] + abb[9] * c[3]);
        }

        return abb[5] * ((abb[3] * prod_pow(abb[7], 2) * T(3)) / T(2) + prod_pow(abb[6], 2) * ((abb[3] * T(-3)) / T(2) + (abb[9] * T(-3)) / T(2) + (abb[4] * T(3)) / T(2) + (abb[8] * T(3)) / T(2)) + prod_pow(abb[7], 2) * ((abb[4] * T(-3)) / T(2) + (abb[8] * T(-3)) / T(2) + (abb[9] * T(3)) / T(2)));
    }
};
template <typename T> class SpDLog_f_4_290_W_21 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_290_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[1] * ((T(15) * kin.v[2]) / T(2) + T(7) + kin.v[3] + T(-7) * kin.v[4]) + kin.v[0] * ((T(-15) * kin.v[1]) / T(2) + (T(17) * kin.v[3]) / T(2) + bc<T>[0] * (T(9) / T(2) + (T(9) * kin.v[2]) / T(2) + (T(-9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) + T(7) + (T(-15) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(4))) * kin.v[0] + T(15) * kin.v[2] + T(-7) * kin.v[4]) + kin.v[2] * ((T(-15) * kin.v[2]) / T(2) + (T(-17) * kin.v[3]) / T(2) + T(-7) + T(7) * kin.v[4]) + kin.v[3] * (-kin.v[3] + T(-7) + T(7) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((T(9) / T(2) + (T(9) * kin.v[1]) / T(4) + (T(-9) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2)) * kin.v[1] + (T(-9) / T(2) + (T(-9) * kin.v[2]) / T(4) + (T(9) * kin.v[4]) / T(2)) * kin.v[2] + (T(-9) / T(2) + (T(9) * kin.v[3]) / T(4) + (T(9) * kin.v[4]) / T(2)) * kin.v[3]);
c[1] = (bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + T(4)) * kin.v[0] + T(4) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * ((T(9) * kin.v[1]) / T(2) + (T(-9) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2)) * int_to_imaginary<T>(1) + T(-4) * kin.v[3];
c[2] = kin.v[1] * ((T(15) * kin.v[2]) / T(2) + T(7) + kin.v[3] + T(-7) * kin.v[4]) + kin.v[0] * ((T(-15) * kin.v[1]) / T(2) + (T(17) * kin.v[3]) / T(2) + bc<T>[0] * (T(9) / T(2) + (T(9) * kin.v[2]) / T(2) + (T(-9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) + T(7) + (T(-15) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(4))) * kin.v[0] + T(15) * kin.v[2] + T(-7) * kin.v[4]) + kin.v[2] * ((T(-15) * kin.v[2]) / T(2) + (T(-17) * kin.v[3]) / T(2) + T(-7) + T(7) * kin.v[4]) + kin.v[3] * (-kin.v[3] + T(-7) + T(7) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((T(9) / T(2) + (T(9) * kin.v[1]) / T(4) + (T(-9) * kin.v[3]) / T(2) + (T(-9) * kin.v[4]) / T(2)) * kin.v[1] + (T(-9) / T(2) + (T(-9) * kin.v[2]) / T(4) + (T(9) * kin.v[4]) / T(2)) * kin.v[2] + (T(-9) / T(2) + (T(9) * kin.v[3]) / T(4) + (T(9) * kin.v[4]) / T(2)) * kin.v[3]);
c[3] = (bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + T(4)) * kin.v[0] + T(4) * kin.v[1] + T(-4) * kin.v[2] + bc<T>[0] * ((T(9) * kin.v[1]) / T(2) + (T(-9) * kin.v[2]) / T(2) + (T(-9) * kin.v[3]) / T(2)) * int_to_imaginary<T>(1) + T(-4) * kin.v[3];
c[4] = kin.v[3] * (T(7) + kin.v[3] + T(-7) * kin.v[4]) + kin.v[2] * ((T(15) * kin.v[2]) / T(2) + (T(17) * kin.v[3]) / T(2) + T(7) + T(-7) * kin.v[4]) + kin.v[1] * ((T(-15) * kin.v[2]) / T(2) + -kin.v[3] + T(-7) + T(7) * kin.v[4]) + kin.v[0] * ((T(15) * kin.v[1]) / T(2) + (T(-17) * kin.v[3]) / T(2) + T(-7) + bc<T>[0] * (T(-9) / T(2) + (T(-9) * kin.v[2]) / T(2) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) + (T(15) / T(2) + bc<T>[0] * (int_to_imaginary<T>(9) / T(4))) * kin.v[0] + T(-15) * kin.v[2] + T(7) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((T(-9) / T(2) + (T(-9) * kin.v[1]) / T(4) + (T(9) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2)) * kin.v[1] + (T(9) / T(2) + (T(9) * kin.v[2]) / T(4) + (T(-9) * kin.v[4]) / T(2)) * kin.v[2] + (T(9) / T(2) + (T(-9) * kin.v[3]) / T(4) + (T(-9) * kin.v[4]) / T(2)) * kin.v[3]);
c[5] = (T(-4) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2))) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3] + bc<T>[0] * ((T(-9) * kin.v[1]) / T(2) + (T(9) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2)) * int_to_imaginary<T>(1);
c[6] = kin.v[3] * (T(7) + kin.v[3] + T(-7) * kin.v[4]) + kin.v[2] * ((T(15) * kin.v[2]) / T(2) + (T(17) * kin.v[3]) / T(2) + T(7) + T(-7) * kin.v[4]) + kin.v[1] * ((T(-15) * kin.v[2]) / T(2) + -kin.v[3] + T(-7) + T(7) * kin.v[4]) + kin.v[0] * ((T(15) * kin.v[1]) / T(2) + (T(-17) * kin.v[3]) / T(2) + T(-7) + bc<T>[0] * (T(-9) / T(2) + (T(-9) * kin.v[2]) / T(2) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) + (T(15) / T(2) + bc<T>[0] * (int_to_imaginary<T>(9) / T(4))) * kin.v[0] + T(-15) * kin.v[2] + T(7) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((T(-9) / T(2) + (T(-9) * kin.v[1]) / T(4) + (T(9) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2)) * kin.v[1] + (T(9) / T(2) + (T(9) * kin.v[2]) / T(4) + (T(-9) * kin.v[4]) / T(2)) * kin.v[2] + (T(9) / T(2) + (T(-9) * kin.v[3]) / T(4) + (T(-9) * kin.v[4]) / T(2)) * kin.v[3]);
c[7] = (T(-4) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2))) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3] + bc<T>[0] * ((T(-9) * kin.v[1]) / T(2) + (T(9) * kin.v[2]) / T(2) + (T(9) * kin.v[3]) / T(2)) * int_to_imaginary<T>(1);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[8] * (t * c[0] + c[1]) + abb[9] * (t * c[2] + c[3]) + abb[11] * (t * c[4] + c[5]) + abb[4] * (t * c[6] + c[7]);
        }

        return abb[10] * (abb[7] * abb[12] * (abb[8] / T(2) + abb[9] / T(2) + -abb[4] / T(2)) + abb[6] * abb[7] * (abb[4] / T(2) + -abb[8] / T(2) + -abb[9] / T(2)) + abb[7] * abb[14] * ((abb[8] * T(-9)) / T(2) + (abb[9] * T(-9)) / T(2) + (abb[4] * T(9)) / T(2)) + abb[7] * (abb[7] * ((abb[8] * T(-3)) / T(2) + (abb[9] * T(-3)) / T(2) + (abb[4] * T(3)) / T(2)) + abb[4] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[8] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[9] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2))) + abb[13] * (abb[6] * (abb[8] / T(2) + abb[9] / T(2) + -abb[4] / T(2)) + abb[7] * (abb[8] / T(2) + abb[9] / T(2) + -abb[4] / T(2)) + abb[12] * (abb[4] / T(2) + -abb[8] / T(2) + -abb[9] / T(2)) + abb[14] * ((abb[4] * T(-9)) / T(2) + (abb[8] * T(9)) / T(2) + (abb[9] * T(9)) / T(2)) + abb[13] * (abb[8] + abb[9] + -abb[4] + -abb[11]) + abb[8] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[9] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[4] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[11] * (abb[12] / T(2) + -abb[6] / T(2) + -abb[7] / T(2) + (abb[14] * T(-9)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)))) + abb[15] * (abb[8] * T(-4) + abb[9] * T(-4) + abb[4] * T(4)) + abb[11] * ((abb[6] * abb[7]) / T(2) + -(abb[7] * abb[12]) / T(2) + (abb[7] * abb[14] * T(9)) / T(2) + abb[7] * ((abb[7] * T(3)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2))) + abb[15] * T(4)) + abb[1] * (abb[7] * abb[11] * T(-4) + abb[7] * (abb[4] * T(-4) + abb[8] * T(4) + abb[9] * T(4)) + abb[13] * (abb[8] * T(-4) + abb[9] * T(-4) + abb[4] * T(4) + abb[11] * T(4))));
    }
};
template <typename T> class SpDLog_f_4_290_W_10 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_290_W_10 (const Kin<T>& kin) {
        c[0] = kin.v[0] * kin.v[2] + kin.v[2] * (T(-13) / T(2) + kin.v[2] / T(8) + (T(-13) * kin.v[4]) / T(2) + T(-2) * kin.v[3]) + kin.v[1] * (T(-13) / T(2) + (T(5) * kin.v[2]) / T(4) + (T(-13) * kin.v[4]) / T(2) + kin.v[0] + (T(9) / T(8) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[1] + T(-2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[2] + T(-3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]);
c[1] = (T(-7) / T(2) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1] + (T(-7) * kin.v[2]) / T(2) + bc<T>[0] * int_to_imaginary<T>(-3) * kin.v[2];
c[2] = kin.v[0] * kin.v[2] + kin.v[2] * (T(-13) / T(2) + kin.v[2] / T(8) + (T(-13) * kin.v[4]) / T(2) + T(-2) * kin.v[3]) + kin.v[1] * (T(-13) / T(2) + (T(5) * kin.v[2]) / T(4) + (T(-13) * kin.v[4]) / T(2) + kin.v[0] + (T(9) / T(8) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[1] + T(-2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[2] + T(-3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]);
c[3] = (T(-7) / T(2) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1] + (T(-7) * kin.v[2]) / T(2) + bc<T>[0] * int_to_imaginary<T>(-3) * kin.v[2];
c[4] = kin.v[0] * kin.v[2] + kin.v[2] * (T(-13) / T(2) + kin.v[2] / T(8) + (T(-13) * kin.v[4]) / T(2) + T(-2) * kin.v[3]) + kin.v[1] * (T(-13) / T(2) + (T(5) * kin.v[2]) / T(4) + (T(-13) * kin.v[4]) / T(2) + kin.v[0] + (T(9) / T(8) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[1] + T(-2) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[2] + T(-3) * kin.v[4])) + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]);
c[5] = (T(-7) / T(2) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1] + (T(-7) * kin.v[2]) / T(2) + bc<T>[0] * int_to_imaginary<T>(-3) * kin.v[2];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[3] * (t * c[0] + c[1]) + abb[11] * (t * c[2] + c[3]) + abb[9] * (t * c[4] + c[5]);
        }

        return abb[39] * (abb[17] * ((abb[9] * T(7)) / T(2) + (abb[11] * T(7)) / T(2)) + abb[1] * (abb[3] * abb[12] + (abb[9] + abb[11]) * abb[12] + abb[2] * (-abb[3] + -abb[9] + -abb[11])) + abb[6] * abb[12] * (abb[9] * T(-2) + abb[11] * T(-2)) + abb[2] * ((abb[9] + abb[11]) * abb[13] + abb[2] * (abb[3] / T(4) + abb[9] / T(4) + abb[11] / T(4)) + abb[12] * ((abb[9] * T(-5)) / T(2) + (abb[11] * T(-5)) / T(2)) + abb[14] * (abb[9] * T(-3) + abb[11] * T(-3)) + abb[9] * bc<T>[0] * int_to_imaginary<T>(3) + abb[11] * bc<T>[0] * int_to_imaginary<T>(3) + abb[3] * (abb[13] + (abb[12] * T(-5)) / T(2) + abb[14] * T(-3) + bc<T>[0] * int_to_imaginary<T>(3) + abb[6] * T(2)) + abb[6] * (abb[9] * T(2) + abb[11] * T(2))) + abb[12] * (abb[12] * ((abb[9] * T(9)) / T(4) + (abb[11] * T(9)) / T(4)) + abb[13] * (-abb[9] + -abb[11]) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[14] * (abb[9] * T(3) + abb[11] * T(3))) + abb[3] * ((abb[17] * T(7)) / T(2) + abb[6] * abb[12] * T(-2) + abb[12] * ((abb[12] * T(9)) / T(4) + -abb[13] + bc<T>[0] * int_to_imaginary<T>(-3) + abb[14] * T(3))));
    }
};
template <typename T> class SpDLog_f_4_290_W_23 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_290_W_23 (const Kin<T>& kin) {
        c[0] = (T(13) / T(2) + (T(51) * kin.v[3]) / T(8) + (T(-21) * kin.v[4]) / T(4)) * kin.v[3] + (T(13) / T(2) + (T(7) * kin.v[2]) / T(8) + (T(29) * kin.v[3]) / T(4) + kin.v[4] / T(4)) * kin.v[2] + (T(-13) / T(2) + (T(-9) * kin.v[4]) / T(8)) * kin.v[4] + kin.v[0] * (T(-13) / T(2) + (T(13) * kin.v[1]) / T(2) + (T(-29) * kin.v[2]) / T(4) + (T(-51) * kin.v[3]) / T(4) + (T(21) * kin.v[4]) / T(4) + (T(51) / T(8) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3])) + ((T(-13) * kin.v[2]) / T(2) + (T(-13) * kin.v[3]) / T(2) + (T(13) * kin.v[4]) / T(2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]));
c[1] = (T(13) / T(2) + (T(51) * kin.v[3]) / T(8) + (T(-21) * kin.v[4]) / T(4)) * kin.v[3] + (T(13) / T(2) + (T(7) * kin.v[2]) / T(8) + (T(29) * kin.v[3]) / T(4) + kin.v[4] / T(4)) * kin.v[2] + (T(-13) / T(2) + (T(-9) * kin.v[4]) / T(8)) * kin.v[4] + kin.v[0] * (T(-13) / T(2) + (T(13) * kin.v[1]) / T(2) + (T(-29) * kin.v[2]) / T(4) + (T(-51) * kin.v[3]) / T(4) + (T(21) * kin.v[4]) / T(4) + (T(51) / T(8) + bc<T>[0] * (int_to_imaginary<T>(3) / T(2))) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) + T(3) * kin.v[1] + T(-3) * kin.v[3])) + ((T(-13) * kin.v[2]) / T(2) + (T(-13) * kin.v[3]) / T(2) + (T(13) * kin.v[4]) / T(2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[3]) / T(2) + T(3)) * kin.v[3] + ((T(-3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + kin.v[2] * ((T(-3) * kin.v[2]) / T(2) + T(3) + T(3) * kin.v[4]) + kin.v[1] * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]));
c[2] = ((T(13) * kin.v[2]) / T(2) + (T(13) * kin.v[3]) / T(2) + (T(-13) * kin.v[4]) / T(2)) * kin.v[1] + kin.v[0] * (T(13) / T(2) + (T(-13) * kin.v[1]) / T(2) + (T(29) * kin.v[2]) / T(4) + (T(51) * kin.v[3]) / T(4) + (T(-21) * kin.v[4]) / T(4) + (T(-51) / T(8) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3])) + (-kin.v[4] / T(4) + T(-13) / T(2) + (T(-7) * kin.v[2]) / T(8) + (T(-29) * kin.v[3]) / T(4)) * kin.v[2] + (T(13) / T(2) + (T(9) * kin.v[4]) / T(8)) * kin.v[4] + (T(-13) / T(2) + (T(-51) * kin.v[3]) / T(8) + (T(21) * kin.v[4]) / T(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]));
c[3] = ((T(13) * kin.v[2]) / T(2) + (T(13) * kin.v[3]) / T(2) + (T(-13) * kin.v[4]) / T(2)) * kin.v[1] + kin.v[0] * (T(13) / T(2) + (T(-13) * kin.v[1]) / T(2) + (T(29) * kin.v[2]) / T(4) + (T(51) * kin.v[3]) / T(4) + (T(-21) * kin.v[4]) / T(4) + (T(-51) / T(8) + bc<T>[0] * (int_to_imaginary<T>(-3) / T(2))) * kin.v[0] + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) + T(-3) * kin.v[1] + T(3) * kin.v[3])) + (-kin.v[4] / T(4) + T(-13) / T(2) + (T(-7) * kin.v[2]) / T(8) + (T(-29) * kin.v[3]) / T(4)) * kin.v[2] + (T(13) / T(2) + (T(9) * kin.v[4]) / T(8)) * kin.v[4] + (T(-13) / T(2) + (T(-51) * kin.v[3]) / T(8) + (T(21) * kin.v[4]) / T(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[3]) / T(2) + T(-3)) * kin.v[3] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4] + kin.v[2] * ((T(3) * kin.v[2]) / T(2) + T(-3) + T(-3) * kin.v[4]) + kin.v[1] * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]));
c[4] = (T(-7) / T(2) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + (T(7) * kin.v[2]) / T(2) + (T(7) * kin.v[3]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + (T(-7) * kin.v[4]) / T(2);
c[5] = (T(-7) / T(2) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[0] + (T(7) * kin.v[2]) / T(2) + (T(7) * kin.v[3]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(3) * kin.v[2] + T(3) * kin.v[3] + T(-3) * kin.v[4]) + (T(-7) * kin.v[4]) / T(2);
c[6] = (T(7) / T(2) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[0] + (T(-7) * kin.v[2]) / T(2) + (T(-7) * kin.v[3]) / T(2) + (T(7) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]);
c[7] = (T(7) / T(2) + bc<T>[0] * int_to_imaginary<T>(3)) * kin.v[0] + (T(-7) * kin.v[2]) / T(2) + (T(-7) * kin.v[3]) / T(2) + (T(7) * kin.v[4]) / T(2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-3) * kin.v[2] + T(-3) * kin.v[3] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[11] * c[1] + abb[8] * c[2] + abb[9] * c[3]) + abb[3] * c[4] + abb[11] * c[5] + abb[8] * c[6] + abb[9] * c[7];
        }

        return abb[40] * (abb[18] * ((abb[11] * T(-7)) / T(2) + (abb[8] * T(7)) / T(2) + (abb[9] * T(7)) / T(2)) + abb[3] * ((abb[18] * T(-7)) / T(2) + abb[12] * (abb[13] + (abb[12] * T(-9)) / T(4) + -abb[7] + abb[14] * T(-3) + bc<T>[0] * int_to_imaginary<T>(3))) + abb[2] * (abb[3] * abb[12] * T(2) + abb[6] * (abb[3] * T(-2) + abb[11] * T(-2) + abb[8] * T(2) + abb[9] * T(2)) + abb[12] * (abb[8] * T(-2) + abb[9] * T(-2) + abb[11] * T(2))) + abb[12] * (abb[12] * ((abb[11] * T(-9)) / T(4) + (abb[8] * T(9)) / T(4) + (abb[9] * T(9)) / T(4)) + abb[13] * (abb[11] + -abb[8] + -abb[9]) + abb[7] * (abb[8] + abb[9] + -abb[11]) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[9] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[11] * bc<T>[0] * int_to_imaginary<T>(3) + abb[14] * (abb[11] * T(-3) + abb[8] * T(3) + abb[9] * T(3))) + abb[6] * (abb[6] * (abb[8] / T(4) + abb[9] / T(4) + -abb[3] / T(4) + -abb[11] / T(4)) + abb[12] * ((abb[8] * T(-5)) / T(2) + (abb[9] * T(-5)) / T(2) + (abb[11] * T(5)) / T(2)) + abb[7] * (abb[11] + -abb[8] + -abb[9]) + abb[13] * (abb[8] + abb[9] + -abb[11]) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[8] * bc<T>[0] * int_to_imaginary<T>(3) + abb[9] * bc<T>[0] * int_to_imaginary<T>(3) + abb[14] * (abb[8] * T(-3) + abb[9] * T(-3) + abb[11] * T(3)) + abb[3] * (abb[7] + (abb[12] * T(5)) / T(2) + -abb[13] + bc<T>[0] * int_to_imaginary<T>(-3) + abb[14] * T(3))));
    }
};
template <typename T> class SpDLog_f_4_290_W_7 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_290_W_7 (const Kin<T>& kin) {
        c[0] = T(-7) * kin.v[4] + (kin.v[0] * kin.v[4]) / T(2) + T(-7) * kin.v[1] * kin.v[4] + -(kin.v[2] * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((T(-9) * kin.v[1] * kin.v[4]) / T(2) + (T(-9) / T(2) + (T(9) * kin.v[4]) / T(4)) * kin.v[4]) + kin.v[3] * (-kin.v[4] / T(2) + kin.v[0] / T(2) + -kin.v[2] + T(-7) + bc<T>[0] * (T(-9) / T(2) + (T(-9) * kin.v[1]) / T(2) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) + T(-7) * kin.v[1] + (T(-1) / T(2) + bc<T>[0] * (int_to_imaginary<T>(9) / T(4))) * kin.v[3]);
c[1] = (T(-4) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2))) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) * kin.v[4];
c[2] = T(-7) * kin.v[4] + (kin.v[0] * kin.v[4]) / T(2) + T(-7) * kin.v[1] * kin.v[4] + -(kin.v[2] * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((T(-9) * kin.v[1] * kin.v[4]) / T(2) + (T(-9) / T(2) + (T(9) * kin.v[4]) / T(4)) * kin.v[4]) + kin.v[3] * (-kin.v[4] / T(2) + kin.v[0] / T(2) + -kin.v[2] + T(-7) + bc<T>[0] * (T(-9) / T(2) + (T(-9) * kin.v[1]) / T(2) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) + T(-7) * kin.v[1] + (T(-1) / T(2) + bc<T>[0] * (int_to_imaginary<T>(9) / T(4))) * kin.v[3]);
c[3] = (T(-4) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2))) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) * kin.v[4];
c[4] = T(-7) * kin.v[4] + (kin.v[0] * kin.v[4]) / T(2) + T(-7) * kin.v[1] * kin.v[4] + -(kin.v[2] * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((T(-9) * kin.v[1] * kin.v[4]) / T(2) + (T(-9) / T(2) + (T(9) * kin.v[4]) / T(4)) * kin.v[4]) + kin.v[3] * (-kin.v[4] / T(2) + kin.v[0] / T(2) + -kin.v[2] + T(-7) + bc<T>[0] * (T(-9) / T(2) + (T(-9) * kin.v[1]) / T(2) + (T(9) * kin.v[4]) / T(2)) * int_to_imaginary<T>(1) + T(-7) * kin.v[1] + (T(-1) / T(2) + bc<T>[0] * (int_to_imaginary<T>(9) / T(4))) * kin.v[3]);
c[5] = (T(-4) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2))) * kin.v[3] + T(-4) * kin.v[4] + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[11] * (t * c[0] + c[1]) + abb[4] * (t * c[2] + c[3]) + abb[8] * (t * c[4] + c[5]);
        }

        return abb[41] * (abb[2] * abb[13] * (abb[4] / T(2) + abb[8] / T(2)) + abb[12] * abb[13] * (-abb[4] / T(2) + -abb[8] / T(2)) + abb[13] * abb[14] * ((abb[4] * T(9)) / T(2) + (abb[8] * T(9)) / T(2)) + abb[13] * (abb[7] * (abb[4] * T(-4) + abb[8] * T(-4)) + abb[4] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[8] * bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[13] * (abb[4] * T(3) + abb[8] * T(3))) + abb[19] * (abb[4] * T(4) + abb[8] * T(4)) + abb[11] * ((abb[2] * abb[13]) / T(2) + -(abb[12] * abb[13]) / T(2) + (abb[13] * abb[14] * T(9)) / T(2) + abb[13] * (abb[7] * T(-4) + bc<T>[0] * (int_to_imaginary<T>(-9) / T(2)) + abb[13] * T(3)) + abb[19] * T(4)) + abb[1] * (abb[12] * (abb[4] / T(2) + abb[8] / T(2)) + abb[1] * (abb[4] / T(2) + abb[8] / T(2) + abb[11] / T(2)) + abb[2] * (-abb[4] / T(2) + -abb[8] / T(2)) + abb[14] * ((abb[4] * T(-9)) / T(2) + (abb[8] * T(-9)) / T(2)) + abb[13] * ((abb[4] * T(-7)) / T(2) + (abb[8] * T(-7)) / T(2)) + abb[4] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[8] * bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[11] * (abb[12] / T(2) + -abb[2] / T(2) + (abb[14] * T(-9)) / T(2) + (abb[13] * T(-7)) / T(2) + bc<T>[0] * (int_to_imaginary<T>(9) / T(2)) + abb[7] * T(4)) + abb[7] * (abb[4] * T(4) + abb[8] * T(4))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_290_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl22 = DLog_W_22<T>(kin),dl21 = DLog_W_21<T>(kin),dl27 = DLog_W_27<T>(kin),dl1 = DLog_W_1<T>(kin),dl28 = DLog_W_28<T>(kin),dl10 = DLog_W_10<T>(kin),dl23 = DLog_W_23<T>(kin),dl7 = DLog_W_7<T>(kin),dl26 = DLog_W_26<T>(kin),dl31 = DLog_W_31<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),dl2 = DLog_W_2<T>(kin),dl17 = DLog_W_17<T>(kin),dl19 = DLog_W_19<T>(kin),dl16 = DLog_W_16<T>(kin),dl5 = DLog_W_5<T>(kin),dl4 = DLog_W_4<T>(kin),dl3 = DLog_W_3<T>(kin),dl29 = DLog_W_29<T>(kin),dl30 = DLog_W_30<T>(kin),spdl12 = SpDLog_f_4_290_W_12<T>(kin),spdl22 = SpDLog_f_4_290_W_22<T>(kin),spdl21 = SpDLog_f_4_290_W_21<T>(kin),spdl10 = SpDLog_f_4_290_W_10<T>(kin),spdl23 = SpDLog_f_4_290_W_23<T>(kin),spdl7 = SpDLog_f_4_290_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[9] = t * (kin.v[1] + kin.v[2]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,55> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl21(t), f_1_3_2(kin) - f_1_3_2(kin_path), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + -v_path[3] + v_path[0]), f_2_1_15(kin_path), dl27(t) / kin_path.SqrtDelta, f_2_1_7(kin_path), f_2_1_8(kin_path), f_2_1_11(kin_path), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[18] / kin_path.W[18]), f_2_2_7(kin_path), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[1] / kin_path.W[1]), f_2_1_10(kin_path), f_2_1_12(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[19] / kin_path.W[19]), dl1(t), dl28(t) / kin_path.SqrtDelta, f_2_1_4(kin_path), f_2_1_14(kin_path), dl10(t), dl23(t), dl7(t), dl26(t) / kin_path.SqrtDelta, dl31(t), dl18(t), dl20(t), dl2(t), dl17(t), dl19(t), dl16(t), dl5(t), dl4(t), dl3(t), dl29(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta}
;

        auto result = f_4_290_abbreviated(abbr);
        result = result + spdl10(t, abbr) + spdl12(t, abbr) + spdl21(t, abbr) + spdl22(t, abbr) + spdl23(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_290_abbreviated(const std::array<T,55>& abb)
{
using TR = typename T::value_type;
T z[215];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[44];
z[3] = abb[45];
z[4] = abb[46];
z[5] = abb[47];
z[6] = abb[48];
z[7] = abb[50];
z[8] = abb[51];
z[9] = abb[52];
z[10] = abb[4];
z[11] = abb[8];
z[12] = abb[9];
z[13] = abb[11];
z[14] = abb[16];
z[15] = abb[20];
z[16] = abb[21];
z[17] = abb[22];
z[18] = abb[24];
z[19] = abb[25];
z[20] = abb[30];
z[21] = abb[31];
z[22] = abb[32];
z[23] = abb[33];
z[24] = abb[34];
z[25] = abb[42];
z[26] = abb[53];
z[27] = abb[2];
z[28] = abb[49];
z[29] = abb[7];
z[30] = abb[12];
z[31] = abb[13];
z[32] = abb[14];
z[33] = bc<TR>[0];
z[34] = abb[36];
z[35] = abb[54];
z[36] = abb[6];
z[37] = abb[15];
z[38] = abb[23];
z[39] = abb[17];
z[40] = abb[18];
z[41] = abb[19];
z[42] = abb[26];
z[43] = abb[27];
z[44] = abb[28];
z[45] = abb[29];
z[46] = abb[37];
z[47] = abb[38];
z[48] = abb[35];
z[49] = abb[43];
z[50] = bc<TR>[3];
z[51] = bc<TR>[1];
z[52] = bc<TR>[2];
z[53] = bc<TR>[4];
z[54] = bc<TR>[9];
z[55] = z[12] / T(2);
z[56] = -z[13] + z[55];
z[57] = z[1] / T(2);
z[58] = z[10] / T(2);
z[59] = z[56] + -z[57] + -z[58];
z[60] = z[3] * z[59];
z[61] = -z[20] + z[23];
z[62] = -z[21] + (T(-3) * z[61]) / T(2);
z[63] = z[22] / T(2);
z[62] = (T(3) * z[19]) / T(4) + z[62] / T(2) + z[63];
z[64] = z[15] + -z[16] / T(4) + -z[18];
z[65] = (T(5) * z[17]) / T(4);
z[66] = z[64] + z[65];
z[67] = z[24] / T(4);
z[68] = z[62] + z[66] + z[67];
z[69] = T(3) * z[14];
z[69] = z[68] * z[69];
z[70] = z[11] / T(2);
z[71] = z[57] + -z[70];
z[72] = (T(5) * z[10]) / T(2);
z[73] = T(2) * z[13];
z[74] = -z[71] + z[72] + z[73];
z[75] = z[28] * z[74];
z[76] = T(3) * z[1];
z[77] = (T(3) * z[11]) / T(2);
z[78] = z[76] + -z[77];
z[79] = (T(5) * z[13]) / T(2);
z[80] = -z[58] + z[78] + z[79];
z[81] = z[12] + z[80] / T(2);
z[81] = z[7] * z[81];
z[82] = z[1] + z[77];
z[83] = z[12] + z[82];
z[84] = (T(9) * z[13]) / T(2);
z[85] = (T(11) * z[10]) / T(2) + z[84];
z[86] = z[83] + -z[85];
z[87] = z[9] / T(2);
z[86] = z[86] * z[87];
z[88] = (T(7) * z[10]) / T(2);
z[89] = z[84] + z[88];
z[90] = z[1] + z[89];
z[91] = (T(7) * z[11]) / T(2);
z[92] = -z[90] + z[91];
z[93] = T(2) * z[12];
z[92] = z[92] / T(2) + z[93];
z[92] = z[2] * z[92];
z[94] = z[10] + z[13];
z[95] = T(5) * z[94];
z[96] = z[11] + z[95];
z[97] = T(3) * z[12];
z[98] = z[96] + z[97];
z[99] = z[5] / T(2);
z[98] = z[98] * z[99];
z[100] = z[1] + T(7) * z[13];
z[101] = T(3) * z[10];
z[100] = z[100] / T(2) + z[101];
z[102] = -z[70] + z[100];
z[102] = z[4] * z[102];
z[103] = (T(11) * z[13]) / T(2);
z[104] = (T(9) * z[10]) / T(2);
z[105] = z[103] + z[104];
z[106] = (T(5) * z[11]) / T(2);
z[107] = -z[1] + z[106];
z[108] = z[105] + -z[107];
z[108] = -z[12] + z[108] / T(2);
z[108] = z[6] * z[108];
z[75] = z[60] + z[69] + z[75] + z[81] + z[86] + z[92] + z[98] + z[102] + z[108];
z[75] = z[27] * z[75];
z[81] = (T(3) * z[21]) / T(2) + z[61];
z[81] = -z[19] / T(4) + (T(-3) * z[22]) / T(4) + z[81] / T(2);
z[92] = (T(3) * z[24]) / T(4);
z[98] = z[66] + -z[81] + z[92];
z[102] = T(3) * z[98];
z[108] = z[14] * z[102];
z[109] = z[55] + z[106];
z[110] = z[90] + z[109];
z[111] = z[2] / T(2);
z[110] = z[110] * z[111];
z[112] = (T(3) * z[12]) / T(2);
z[80] = -z[80] + z[112];
z[113] = z[28] / T(2);
z[80] = z[80] * z[113];
z[114] = (T(5) * z[12]) / T(2);
z[115] = -z[82] + -z[105] + z[114];
z[116] = z[6] / T(2);
z[115] = z[115] * z[116];
z[117] = z[72] + -z[114];
z[118] = -z[57] + z[73] + z[117];
z[119] = -(z[7] * z[118]);
z[120] = z[13] + z[58] + z[71];
z[120] = z[4] * z[120];
z[121] = T(3) * z[13];
z[122] = T(2) * z[10] + z[121];
z[123] = z[1] + z[122];
z[124] = -z[93] + z[123];
z[124] = z[9] * z[124];
z[95] = -z[11] + z[95];
z[125] = T(4) * z[12];
z[126] = -z[95] / T(2) + z[125];
z[126] = z[5] * z[126];
z[100] = z[97] + -z[100];
z[100] = z[3] * z[100];
z[80] = z[80] + z[100] + -z[108] + z[110] + z[115] + z[119] + z[120] + z[124] + z[126];
z[80] = z[36] * z[80];
z[100] = (T(3) * z[10]) / T(2);
z[79] = z[79] + z[100];
z[110] = -z[1] + z[70];
z[115] = z[79] + -z[110];
z[119] = -z[12] + z[115];
z[126] = z[4] * z[119];
z[115] = -z[55] + z[115];
z[127] = -(z[3] * z[115]);
z[128] = z[13] / T(2);
z[129] = z[100] + z[128];
z[130] = z[110] + z[129];
z[131] = z[55] + z[130];
z[131] = z[28] * z[131];
z[132] = z[11] + z[12];
z[133] = z[5] * z[132];
z[134] = z[11] + z[55];
z[135] = z[48] * z[134];
z[136] = z[2] * z[134];
z[126] = z[126] + z[127] + z[131] + T(3) * z[133] + -z[135] + z[136];
z[126] = int_to_imaginary<T>(1) * z[126];
z[127] = -z[21] + z[61];
z[63] = -z[19] + z[24] + z[63] + z[127] / T(2);
z[127] = int_to_imaginary<T>(1) * z[63];
z[131] = -(z[14] * z[127]);
z[136] = z[6] * z[132];
z[137] = int_to_imaginary<T>(1) * z[136];
z[126] = z[126] + z[131] + z[137];
z[126] = z[33] * z[126];
z[131] = z[4] / T(2);
z[138] = z[119] * z[131];
z[139] = z[3] / T(2);
z[140] = z[115] * z[139];
z[141] = z[78] + z[128];
z[142] = -z[72] + z[141];
z[142] = z[113] * z[142];
z[143] = z[12] / T(4);
z[144] = z[6] * z[143];
z[145] = -(z[2] * z[132]);
z[146] = (T(3) * z[12]) / T(4);
z[147] = z[11] + z[146];
z[148] = -(z[5] * z[147]);
z[142] = -z[138] + z[140] + z[142] + -z[144] + z[145] + z[148];
z[142] = z[32] * z[142];
z[141] = z[117] + -z[141];
z[145] = z[32] / T(2);
z[141] = z[141] * z[145];
z[130] = z[12] + -z[130] / T(2);
z[148] = int_to_imaginary<T>(1) * z[33];
z[130] = z[130] * z[148];
z[130] = z[130] + z[141];
z[130] = z[7] * z[130];
z[126] = z[126] / T(2) + z[130] + z[142];
z[130] = z[73] + z[100];
z[71] = z[71] + z[130];
z[141] = z[28] * z[71];
z[60] = z[60] + -z[141];
z[142] = z[60] + -z[69];
z[74] = -(z[4] * z[74]);
z[149] = z[89] + -z[110];
z[150] = -z[12] + z[149];
z[151] = z[2] * z[150];
z[95] = -z[12] + -z[95];
z[95] = z[5] * z[95];
z[95] = z[95] + z[151];
z[151] = z[105] + -z[110];
z[152] = -(z[116] * z[151]);
z[153] = (T(3) * z[11]) / T(4);
z[154] = -z[1] + z[153];
z[155] = (T(7) * z[13]) / T(4);
z[156] = (T(3) * z[10]) / T(4) + z[155];
z[157] = z[154] + -z[156];
z[157] = z[7] * z[157];
z[74] = z[74] + -z[86] + z[95] / T(2) + z[142] + z[152] + z[157];
z[74] = z[0] * z[74];
z[86] = (T(7) * z[13]) / T(2);
z[95] = z[1] + z[70];
z[152] = z[72] + z[86] + z[95] + -z[97];
z[152] = z[4] * z[152];
z[157] = z[55] + z[72];
z[158] = -z[1] + z[77];
z[86] = -z[86] + -z[157] + z[158];
z[86] = z[3] * z[86];
z[159] = -z[95] + z[129];
z[160] = z[112] + z[159];
z[160] = z[28] * z[160];
z[161] = z[2] * z[55];
z[161] = z[135] + z[161];
z[86] = z[86] + T(7) * z[133] + z[152] + z[160] + T(-3) * z[161];
z[152] = (T(3) * z[14]) / T(2);
z[162] = -(z[63] * z[152]);
z[129] = z[129] + z[158];
z[163] = z[97] + -z[129];
z[164] = z[7] / T(2);
z[163] = z[163] * z[164];
z[86] = z[86] / T(2) + T(3) * z[136] + z[162] + z[163];
z[86] = z[31] * z[86];
z[162] = z[27] * z[68];
z[163] = z[36] * z[98];
z[165] = -(z[0] * z[68]);
z[166] = z[63] / T(2);
z[167] = -(z[32] * z[166]);
z[165] = z[162] + -z[163] + z[165] + z[167];
z[167] = T(3) * z[34];
z[165] = z[165] * z[167];
z[168] = z[2] * z[147];
z[168] = z[135] / T(2) + -z[168];
z[169] = (T(13) * z[12]) / T(2);
z[170] = T(5) * z[11];
z[171] = -z[169] + -z[170];
z[171] = z[99] * z[171];
z[172] = -z[1] + z[11];
z[173] = -z[89] + z[172];
z[173] = z[4] * z[173];
z[174] = (T(7) * z[12]) / T(2);
z[175] = z[90] + -z[174];
z[175] = z[3] * z[175];
z[171] = T(3) * z[168] + z[171] + z[173] + z[175];
z[121] = -z[1] + z[121];
z[121] = z[10] + z[121] / T(4);
z[173] = (T(11) * z[12]) / T(8);
z[175] = z[121] + -z[153] + -z[173];
z[175] = z[7] * z[175];
z[176] = (T(3) * z[63]) / T(4);
z[177] = -z[25] + z[34];
z[178] = z[14] + z[177];
z[179] = z[176] * z[178];
z[180] = (T(3) * z[12]) / T(8);
z[121] = -z[11] + -z[121] + -z[180];
z[121] = z[28] * z[121];
z[173] = z[11] + z[173];
z[173] = z[6] * z[173];
z[121] = z[121] + z[171] / T(2) + z[173] + z[175] + z[179];
z[121] = z[30] * z[121];
z[171] = z[32] * z[98];
z[81] = -z[64] + z[81];
z[173] = -z[81] + z[92];
z[173] = int_to_imaginary<T>(1) * z[173];
z[175] = int_to_imaginary<T>(1) * z[65];
z[173] = z[173] + z[175];
z[173] = z[33] * z[173];
z[171] = -z[171] + z[173];
z[179] = z[31] * z[98];
z[181] = -z[171] + -z[179];
z[182] = T(3) * z[181];
z[183] = -(z[26] * z[182]);
z[184] = z[25] * z[32] * z[63];
z[74] = z[74] + z[75] + z[80] + z[86] + z[121] + T(3) * z[126] + z[165] + z[183] + (T(3) * z[184]) / T(2);
z[74] = z[30] * z[74];
z[75] = (T(13) * z[13]) / T(2);
z[80] = z[75] + z[88];
z[86] = z[78] + z[80];
z[88] = z[86] / T(2) + -z[93];
z[88] = z[5] * z[88];
z[121] = T(4) * z[13] + -z[112];
z[126] = (T(5) * z[1]) / T(2);
z[165] = z[100] + z[121] + z[126];
z[165] = z[7] * z[165];
z[183] = (T(13) * z[11]) / T(2);
z[184] = T(7) * z[12] + -z[90] + z[183];
z[184] = z[111] * z[184];
z[185] = T(3) * z[11] + T(7) * z[94];
z[185] = -z[97] + z[185] / T(2);
z[185] = z[131] * z[185];
z[186] = (T(3) * z[1]) / T(2) + z[13] + z[55] + -z[58];
z[186] = z[3] * z[186];
z[187] = z[91] + z[112];
z[188] = z[1] + z[13];
z[189] = T(2) * z[188];
z[190] = z[187] + z[189];
z[190] = z[6] * z[190];
z[88] = z[69] + z[88] + -z[124] + -z[141] + z[165] + z[184] + z[185] + z[186] + z[190];
z[88] = z[31] * z[88];
z[141] = z[70] + -z[112] + z[189];
z[141] = z[6] * z[141];
z[165] = (T(7) * z[10]) / T(4);
z[184] = (T(11) * z[13]) / T(4) + z[165];
z[185] = -z[1] + z[11] / T(4);
z[186] = -z[112] + z[184] + -z[185];
z[186] = z[4] * z[186];
z[80] = z[76] + z[80];
z[190] = z[70] + z[80];
z[191] = -z[12] + z[190] / T(2);
z[191] = z[5] * z[191];
z[126] = z[13] + -z[100] + z[126];
z[192] = z[112] + z[126];
z[192] = z[7] * z[192];
z[150] = z[111] * z[150];
z[141] = -z[124] + z[141] + -z[142] + -z[150] + z[186] + z[191] + z[192];
z[142] = z[27] * z[141];
z[150] = T(7) * z[1];
z[100] = -z[100] + z[103] + z[150];
z[103] = z[77] + z[97];
z[186] = -z[100] + z[103];
z[186] = z[3] * z[186];
z[90] = z[90] + z[91] + -z[97];
z[90] = z[2] * z[90];
z[91] = z[97] + -z[190];
z[91] = z[5] * z[91];
z[90] = z[90] + z[91] + z[186];
z[91] = z[68] * z[152];
z[190] = (T(3) * z[13]) / T(4);
z[165] = z[1] + (T(-7) * z[11]) / T(4) + z[112] + -z[165] + -z[190];
z[165] = z[4] * z[165];
z[191] = -z[77] + -z[188];
z[191] = z[6] * z[191];
z[192] = -z[58] + z[128];
z[193] = z[95] + z[192];
z[194] = -z[12] + z[193];
z[195] = (T(3) * z[9]) / T(4);
z[196] = z[194] * z[195];
z[197] = -(z[7] * z[188]);
z[90] = z[90] / T(4) + -z[91] + z[165] + z[191] + z[196] + z[197];
z[90] = z[0] * z[90];
z[165] = z[99] * z[119];
z[191] = -z[110] + z[192];
z[196] = -z[12] + z[191];
z[197] = z[139] * z[196];
z[198] = -z[1] + z[10];
z[199] = z[11] + z[198];
z[200] = z[4] * z[199];
z[201] = z[2] * z[199];
z[165] = -z[165] + z[197] + z[200] + z[201];
z[197] = int_to_imaginary<T>(-1) * z[165];
z[83] = z[79] + z[83];
z[202] = int_to_imaginary<T>(1) * z[116];
z[203] = -(z[83] * z[202]);
z[197] = z[197] + z[203];
z[197] = z[33] * z[197];
z[83] = z[83] * z[116];
z[83] = z[83] + z[165];
z[83] = z[32] * z[83];
z[165] = -z[32] + z[148];
z[203] = z[164] * z[165];
z[204] = z[119] * z[203];
z[83] = z[83] + z[197] + -z[204];
z[83] = T(3) * z[83] + z[88] + z[90] + z[142];
z[83] = z[0] * z[83];
z[88] = -z[146] + z[156] + -z[185];
z[88] = z[28] * z[88];
z[88] = z[88] + -z[124];
z[57] = z[57] + -z[112] + z[130];
z[57] = z[7] * z[57];
z[90] = z[1] + z[79];
z[90] = T(3) * z[90];
z[109] = -z[90] + z[109];
z[109] = z[99] * z[109];
z[130] = (T(9) * z[12]) / T(2);
z[90] = -z[70] + z[90] + -z[130];
z[90] = z[90] * z[116];
z[142] = -z[12] + z[170] + T(3) * z[198];
z[142] = z[2] * z[142];
z[156] = T(2) * z[1];
z[155] = -z[10] / T(4) + (T(-5) * z[11]) / T(4) + (T(9) * z[12]) / T(4) + z[155] + z[156];
z[155] = z[4] * z[155];
z[117] = (T(-7) * z[1]) / T(2) + -z[13] + z[117];
z[117] = z[3] * z[117];
z[90] = z[57] + z[88] + z[90] + z[109] + z[117] + z[142] + z[155];
z[90] = z[31] * z[90];
z[57] = z[57] + z[120];
z[109] = z[57] + z[108];
z[117] = z[3] * z[118];
z[96] = z[96] / T(2) + -z[97];
z[96] = z[5] * z[96];
z[118] = -z[130] + z[151];
z[118] = z[116] * z[118];
z[120] = -z[114] + z[149];
z[142] = z[111] * z[120];
z[88] = z[88] + z[96] + z[109] + z[117] + z[118] + -z[142];
z[96] = -(z[36] * z[88]);
z[117] = z[34] * z[102];
z[88] = z[88] + z[117];
z[88] = z[30] * z[88];
z[117] = z[99] * z[115];
z[118] = z[112] + z[191];
z[142] = z[118] * z[131];
z[149] = z[113] * z[115];
z[151] = -z[12] + z[198];
z[155] = z[3] * z[151];
z[185] = z[2] * z[151];
z[117] = -z[117] + z[142] + z[149] + z[155] + z[185];
z[142] = int_to_imaginary<T>(1) * z[117];
z[197] = -z[79] + z[107] + z[114];
z[205] = -(z[197] * z[202]);
z[142] = z[142] + z[205];
z[142] = z[33] * z[142];
z[197] = z[116] * z[197];
z[117] = -z[117] + z[197];
z[117] = z[32] * z[117];
z[117] = z[117] + z[142];
z[57] = z[57] + z[60];
z[60] = -z[93] + -z[170];
z[60] = z[2] * z[60];
z[142] = -z[11] + z[12];
z[142] = z[99] * z[142];
z[197] = T(-7) * z[11] + -z[97];
z[197] = z[116] * z[197];
z[60] = -z[57] + z[60] + z[124] + z[142] + z[197];
z[60] = z[0] * z[60];
z[142] = z[25] * z[182];
z[197] = z[163] + z[181];
z[167] = -(z[167] * z[197]);
z[205] = z[6] + z[28];
z[206] = -z[13] + z[172];
z[205] = z[205] * z[206];
z[207] = -z[12] + z[94];
z[208] = z[5] * z[207];
z[205] = -z[185] + z[205] + z[208];
z[205] = z[29] * z[205];
z[60] = z[60] + z[88] + z[90] + z[96] + T(3) * z[117] + z[142] + z[167] + (T(3) * z[205]) / T(2);
z[60] = z[29] * z[60];
z[62] = z[62] + z[64] + z[67];
z[62] = int_to_imaginary<T>(1) * z[62];
z[62] = z[62] + z[175];
z[62] = z[33] * z[62];
z[64] = z[32] * z[68];
z[62] = z[62] + -z[64];
z[67] = z[31] * z[68];
z[88] = z[68] / T(2);
z[88] = z[0] * z[88];
z[90] = z[62] + z[67] + -z[88] + z[162];
z[90] = z[0] * z[90];
z[96] = z[62] + z[162] / T(2);
z[67] = -z[67] + -z[96];
z[67] = z[27] * z[67];
z[117] = -(z[36] * z[181]);
z[142] = z[40] + -z[47];
z[162] = z[98] * z[142];
z[167] = -z[39] + -z[46];
z[167] = z[68] * z[167];
z[175] = z[33] / T(2);
z[181] = z[127] * z[175];
z[205] = -(z[44] * z[181]);
z[208] = z[63] / T(4);
z[209] = prod_pow(z[32], 2);
z[210] = -(z[208] * z[209]);
z[67] = z[67] + z[90] + z[117] + z[162] + z[167] + z[205] + z[210];
z[67] = z[25] * z[67];
z[90] = z[11] + z[112];
z[117] = z[2] * z[90];
z[162] = z[5] * z[90];
z[117] = -z[117] + -z[135] + z[162];
z[117] = z[117] / T(2);
z[167] = z[143] + z[191];
z[191] = z[7] + -z[28];
z[191] = z[167] * z[191];
z[178] = -z[35] + z[178];
z[178] = z[166] * z[178];
z[205] = -(z[116] * z[134]);
z[178] = -z[117] + z[178] + z[191] + z[205];
z[178] = z[42] * z[178];
z[191] = z[5] * z[134];
z[191] = -z[161] + z[191];
z[205] = z[3] + -z[4];
z[205] = z[167] * z[205];
z[191] = z[191] / T(2) + -z[205];
z[205] = int_to_imaginary<T>(-1) * z[191];
z[210] = int_to_imaginary<T>(1) * z[6];
z[211] = -(z[143] * z[210]);
z[205] = z[205] + z[211];
z[205] = z[33] * z[205];
z[177] = -z[14] + z[26] + z[177];
z[211] = -(z[177] * z[181]);
z[205] = z[205] + z[211];
z[205] = z[45] * z[205];
z[177] = z[166] * z[177];
z[144] = z[144] + z[177] + z[191];
z[144] = z[43] * z[144];
z[67] = z[67] + z[144] + z[178] + z[205];
z[144] = z[11] + z[114];
z[144] = z[5] * z[144];
z[177] = (T(13) * z[11]) / T(4);
z[178] = z[12] + z[177];
z[178] = z[6] * z[178];
z[144] = z[144] + z[178];
z[178] = z[12] + z[70];
z[111] = z[111] * z[178];
z[128] = (T(4) * z[1]) / T(3) + (T(-11) * z[10]) / T(6) + -z[128];
z[178] = (T(-11) * z[11]) / T(6) + z[128] + z[180];
z[178] = z[4] * z[178];
z[128] = (T(-35) * z[12]) / T(24) + -z[70] + -z[128];
z[128] = z[3] * z[128];
z[180] = (T(-19) * z[11]) / T(2) + T(-11) * z[198];
z[180] = -z[55] + z[180] / T(3);
z[180] = z[113] * z[180];
z[191] = z[10] + z[110];
z[205] = z[146] + -z[191];
z[205] = z[87] * z[205];
z[211] = z[3] + z[4];
z[212] = z[9] + -z[28];
z[211] = -z[2] + T(4) * z[5] + T(2) * z[211] + (T(-5) * z[212]) / T(2);
z[212] = int_to_imaginary<T>(1) * z[211];
z[210] = T(5) * z[210] + z[212];
z[210] = z[33] * z[210];
z[111] = z[111] + z[128] + z[135] + z[144] / T(3) + z[178] + z[180] + z[205] + z[210] / T(9);
z[111] = z[33] * z[111];
z[128] = -(z[28] * z[167]);
z[117] = -z[117] + z[128];
z[117] = int_to_imaginary<T>(1) * z[117];
z[128] = -(z[134] * z[202]);
z[117] = z[117] + z[128];
z[128] = T(3) * z[44];
z[117] = z[117] * z[128];
z[135] = T(-5) * z[6] + (T(-5) * z[7]) / T(2) + -z[211];
z[144] = T(3) * z[50];
z[135] = z[135] * z[144];
z[111] = z[111] + z[117] + z[135];
z[111] = z[33] * z[111];
z[117] = z[171] + z[179] / T(2);
z[135] = T(3) * z[31];
z[117] = z[117] * z[135];
z[81] = z[37] * z[81];
z[135] = (T(15) * z[17]) / T(4) + (T(9) * z[24]) / T(4);
z[135] = z[37] * z[135];
z[117] = T(-3) * z[81] + z[117] + z[135];
z[135] = z[36] * z[182];
z[88] = -z[62] + -z[88];
z[144] = T(3) * z[0];
z[88] = z[88] * z[144];
z[178] = z[41] * z[68];
z[180] = z[53] * z[148];
z[182] = z[178] + z[180];
z[142] = -(z[102] * z[142]);
z[205] = -z[22] + z[148];
z[61] = T(7) * z[21] + T(3) * z[61];
z[61] = -z[24] + z[51] + z[61] / T(8) + -z[66] + (T(7) * z[205]) / T(8);
z[61] = z[33] * z[61];
z[66] = z[51] / T(2);
z[205] = -z[52] + z[66];
z[205] = int_to_imaginary<T>(1) * z[51] * z[205];
z[61] = z[61] + T(-3) * z[205];
z[61] = z[33] * z[61];
z[176] = z[176] * z[209];
z[61] = (T(77) * z[54]) / T(4) + z[61] + z[88] + z[117] + z[135] + z[142] + z[176] + T(-3) * z[182];
z[61] = z[26] * z[61];
z[72] = -z[72] + z[84] + z[150];
z[88] = (T(-33) * z[11]) / T(2) + z[72] + -z[169];
z[88] = z[2] * z[88];
z[135] = T(5) * z[12];
z[142] = z[94] + z[135] + z[170];
z[142] = z[5] * z[142];
z[170] = (T(3) * z[13]) / T(2);
z[82] = z[82] + -z[157] + -z[170];
z[82] = z[3] * z[82];
z[104] = (T(23) * z[13]) / T(2) + z[104] + z[150];
z[157] = (T(11) * z[11]) / T(2);
z[182] = -z[104] + z[112] + z[157];
z[209] = z[6] * z[182];
z[210] = z[1] + z[58] + z[170];
z[210] = T(7) * z[210];
z[211] = (T(9) * z[11]) / T(2);
z[212] = -z[55] + z[210] + -z[211];
z[213] = z[9] * z[212];
z[105] = z[95] + z[105];
z[214] = -z[105] + -z[130];
z[214] = z[4] * z[214];
z[82] = z[82] + z[88] + z[142] + z[160] + z[209] + z[213] + z[214];
z[82] = z[82] / T(2) + -z[108];
z[88] = z[7] * z[59];
z[82] = z[82] / T(2) + T(3) * z[88];
z[82] = z[31] * z[82];
z[88] = -z[55] + z[159];
z[88] = z[88] * z[139];
z[139] = -z[12] + z[129];
z[139] = z[131] * z[139];
z[142] = z[99] * z[132];
z[88] = z[88] + -z[139] + -z[142] + z[149] + z[168];
z[139] = int_to_imaginary<T>(-1) * z[88];
z[139] = T(2) * z[137] + z[139];
z[139] = z[33] * z[139];
z[142] = z[14] * z[166];
z[88] = z[88] + T(-2) * z[136] + z[142];
z[88] = z[32] * z[88];
z[136] = -(z[14] * z[181]);
z[88] = z[88] + z[136] + z[139] + z[204];
z[82] = z[82] + T(3) * z[88];
z[82] = z[31] * z[82];
z[88] = -(z[31] * z[141]);
z[136] = z[79] + z[95];
z[136] = z[99] * z[136];
z[139] = z[113] * z[193];
z[136] = -z[136] + -z[138] + z[139] + z[185];
z[138] = int_to_imaginary<T>(1) * z[136];
z[139] = z[119] * z[202];
z[138] = z[138] + z[139];
z[138] = z[33] * z[138];
z[119] = -(z[116] * z[119]);
z[119] = z[119] + -z[136];
z[119] = z[32] * z[119];
z[136] = z[7] * z[151] * z[165];
z[119] = z[119] + z[136] + z[138];
z[86] = -z[86] + z[97];
z[86] = z[4] * z[86];
z[136] = (T(19) * z[10]) / T(2);
z[75] = z[75] + -z[76] + z[136];
z[138] = z[75] + -z[77];
z[139] = z[28] * z[138];
z[141] = (T(21) * z[13]) / T(2) + z[136];
z[135] = -z[135] + z[141] + -z[158];
z[149] = z[9] * z[135];
z[160] = -z[77] + -z[80];
z[160] = z[5] * z[160];
z[86] = -z[86] + z[139] + -z[149] + -z[160];
z[139] = z[89] + -z[158];
z[139] = z[12] + z[139] / T(4);
z[139] = z[2] * z[139];
z[149] = z[11] + z[13];
z[149] = (T(19) * z[10]) / T(8) + (T(-11) * z[12]) / T(4) + (T(3) * z[149]) / T(8) + -z[156];
z[149] = z[7] * z[149];
z[56] = -z[1] + z[56];
z[56] = z[6] * z[56];
z[56] = z[56] + -z[86] / T(4) + -z[91] + z[139] + z[149];
z[56] = z[27] * z[56];
z[56] = z[56] + z[88] + T(3) * z[119];
z[56] = z[27] * z[56];
z[80] = z[80] + -z[187];
z[80] = z[5] * z[80];
z[86] = -(z[2] * z[120]);
z[80] = z[80] + z[86];
z[86] = -z[143] + -z[154] + z[184];
z[86] = z[3] * z[86];
z[88] = -z[106] + z[112] + z[189];
z[88] = z[6] * z[88];
z[91] = T(5) * z[1];
z[58] = -z[58] + z[84] + z[91];
z[119] = -z[58] + -z[174] + z[211];
z[119] = z[87] * z[119];
z[120] = -z[106] + z[126];
z[120] = z[28] * z[120];
z[80] = z[80] / T(2) + z[86] + z[88] + z[109] + z[119] + z[120];
z[80] = z[31] * z[80];
z[86] = (T(15) * z[12]) / T(2);
z[88] = z[86] + -z[89] + -z[95];
z[88] = z[2] * z[88];
z[89] = (T(19) * z[12]) / T(2);
z[75] = -z[75] + z[89] + -z[211];
z[109] = z[7] * z[75];
z[119] = (T(11) * z[12]) / T(2);
z[105] = z[105] + -z[119];
z[105] = z[6] * z[105];
z[88] = z[88] + z[105] + -z[109];
z[105] = z[98] * z[152];
z[109] = -z[93] + (T(5) * z[94]) / T(4);
z[109] = z[5] * z[109];
z[120] = -z[90] + (T(-5) * z[198]) / T(2);
z[120] = z[113] * z[120];
z[114] = -z[114] + z[129];
z[126] = -(z[114] * z[195]);
z[139] = z[3] * z[207];
z[88] = z[88] / T(4) + z[105] + z[109] + z[120] + z[126] + (T(5) * z[139]) / T(4);
z[88] = z[36] * z[88];
z[79] = -z[79] + z[112] + z[158];
z[79] = z[79] * z[99];
z[105] = z[28] * z[199];
z[79] = z[79] + z[105] + -z[140] + z[201];
z[109] = int_to_imaginary<T>(-1) * z[79];
z[120] = -(z[115] * z[202]);
z[109] = z[109] + z[120];
z[109] = z[33] * z[109];
z[115] = z[115] * z[116];
z[79] = z[79] + z[115];
z[79] = z[32] * z[79];
z[115] = z[158] + -z[192];
z[120] = -z[55] + z[115];
z[120] = z[120] * z[203];
z[79] = z[79] + z[109] + z[120];
z[109] = -z[11] + -z[97];
z[109] = z[99] * z[109];
z[120] = z[11] + -z[97];
z[116] = z[116] * z[120];
z[120] = -z[11] + -z[125];
z[120] = z[2] * z[120];
z[57] = -z[57] + z[109] + z[116] + z[120] + -z[124];
z[57] = z[27] * z[57];
z[57] = z[57] + T(3) * z[79] + z[80] + z[88];
z[57] = z[36] * z[57];
z[79] = z[97] + z[106];
z[80] = -z[79] + z[210];
z[80] = z[41] * z[80];
z[88] = (T(5) * z[148]) / T(9);
z[106] = -z[88] + -z[143] + z[191];
z[106] = prod_pow(z[33], 2) * z[106];
z[95] = -z[95] + z[130] + -z[141];
z[95] = z[40] * z[95];
z[58] = z[58] + -z[70] + -z[97];
z[109] = z[27] * z[31] * z[58];
z[116] = z[0] + -z[27];
z[120] = -z[31] + z[116];
z[58] = z[0] * z[58] * z[120];
z[112] = -z[112] + z[115];
z[115] = T(3) * z[47];
z[120] = -(z[112] * z[115]);
z[124] = z[12] + z[159];
z[125] = T(3) * z[46];
z[126] = -(z[124] * z[125]);
z[58] = z[58] + z[80] + z[95] + z[106] + z[109] + z[120] + z[126];
z[80] = z[85] + z[107] + -z[130];
z[80] = z[80] / T(2);
z[85] = z[36] * z[80];
z[80] = -(z[30] * z[80]);
z[95] = z[122] + -z[172];
z[106] = z[0] + -z[31];
z[106] = -(z[95] * z[106]);
z[80] = z[80] + z[85] + z[106];
z[80] = z[29] * z[80];
z[106] = z[27] + -z[31];
z[106] = z[95] * z[106];
z[106] = -z[85] + z[106];
z[106] = z[36] * z[106];
z[95] = z[95] * z[116];
z[85] = z[85] + z[95];
z[85] = z[30] * z[85];
z[95] = z[33] * z[50];
z[58] = z[58] / T(2) + z[80] + z[85] + (T(15) * z[95]) / T(2) + z[106];
z[58] = z[8] * z[58];
z[80] = z[72] + z[119] + -z[211];
z[80] = z[2] * z[80];
z[85] = -z[100] + -z[130] + z[157];
z[85] = z[4] * z[85];
z[100] = (T(17) * z[10]) / T(2) + -z[150] + z[170];
z[106] = z[77] + -z[100] + z[119];
z[106] = z[3] * z[106];
z[107] = z[28] * z[182];
z[109] = z[94] + -z[132];
z[109] = z[5] * z[109];
z[116] = (T(23) * z[11]) / T(2) + z[86] + -z[104];
z[116] = z[6] * z[116];
z[80] = z[80] + z[85] + z[106] + z[107] + z[109] + z[116];
z[80] = z[37] * z[80];
z[85] = z[5] * z[112];
z[106] = -(z[3] * z[118]);
z[107] = -z[55] + z[129];
z[107] = z[28] * z[107];
z[109] = z[6] * z[114];
z[85] = z[85] + z[106] + z[107] + z[109];
z[85] = z[85] * z[115];
z[72] = z[72] + -z[79];
z[72] = z[2] * z[72];
z[79] = -z[97] + -z[104] + -z[211];
z[79] = z[6] * z[79];
z[100] = (T(-17) * z[11]) / T(2) + z[97] + -z[100];
z[100] = z[4] * z[100];
z[106] = z[11] + z[94];
z[106] = z[5] * z[106];
z[72] = z[72] + z[79] + z[100] + z[106] + z[186];
z[72] = z[41] * z[72];
z[79] = -z[5] + z[9];
z[79] = z[79] * z[194];
z[100] = -(z[4] * z[196]);
z[106] = z[6] * z[124];
z[79] = z[79] + z[100] + z[106];
z[79] = z[79] * z[125];
z[100] = z[37] * z[212];
z[106] = -(z[114] * z[115]);
z[100] = z[100] + z[106];
z[100] = z[9] * z[100];
z[72] = z[72] + z[79] + z[80] + z[85] + z[100];
z[79] = -z[84] + z[91] + -z[136];
z[80] = -z[77] + -z[79];
z[80] = -z[12] + z[80] / T(2);
z[80] = z[2] * z[80];
z[84] = -(z[113] * z[138]);
z[85] = z[87] * z[135];
z[87] = (T(25) * z[13]) / T(2) + z[136];
z[76] = z[76] + z[87];
z[77] = -z[76] + -z[77];
z[77] = z[77] * z[99];
z[78] = z[78] + z[87];
z[87] = -z[78] + z[97];
z[87] = z[87] * z[131];
z[91] = z[12] + z[188];
z[91] = z[6] * z[91];
z[69] = -z[69] + z[77] + z[80] + z[84] + z[85] + z[87] + z[91];
z[69] = z[39] * z[69];
z[77] = -(z[30] * z[208]);
z[64] = -z[64] + z[77] + z[163] + z[173] + z[179];
z[64] = z[30] * z[64];
z[77] = -(z[30] * z[98]);
z[77] = z[77] + z[197];
z[77] = z[29] * z[77];
z[80] = z[39] * z[68];
z[64] = -z[64] + -z[77] + z[80];
z[77] = T(3) * z[27];
z[80] = -(z[77] * z[96]);
z[84] = -prod_pow(z[36], 2);
z[84] = -z[40] + -z[47] + z[84];
z[84] = z[84] * z[102];
z[66] = z[66] + -z[98] + (T(3) * z[148]) / T(4);
z[66] = z[33] * z[66];
z[85] = -(z[44] * z[127]);
z[85] = z[85] + -z[205];
z[66] = z[66] + (T(3) * z[85]) / T(2);
z[66] = z[33] * z[66];
z[64] = (T(76) * z[54]) / T(3) + T(-3) * z[64] + z[66] + z[80] + z[84] + z[117] + (T(-3) * z[180]) / T(2);
z[64] = z[35] * z[64];
z[66] = z[103] + -z[104];
z[66] = z[41] * z[66];
z[80] = -z[12] + z[159];
z[80] = z[80] * z[125];
z[66] = z[66] + z[80];
z[70] = (T(-25) * z[12]) / T(6) + z[70] + z[88] + (T(11) * z[198]) / T(3);
z[70] = z[70] * z[175];
z[80] = int_to_imaginary<T>(1) * z[128] * z[167];
z[70] = z[70] + z[80];
z[70] = z[33] * z[70];
z[80] = -z[145] + z[148];
z[84] = T(3) * z[32];
z[80] = z[80] * z[84] * z[151];
z[85] = T(4) * z[1] + (T(-19) * z[10]) / T(4) + -z[190];
z[87] = -z[85] + -z[119] + z[153];
z[87] = z[39] * z[87];
z[66] = z[66] / T(2) + z[70] + z[80] + z[87];
z[66] = z[7] * z[66];
z[70] = z[79] + z[86] + -z[183];
z[70] = z[2] * z[70];
z[76] = z[76] + -z[89] + -z[211];
z[76] = z[5] * z[76];
z[78] = z[78] + -z[169];
z[78] = z[3] * z[78];
z[70] = z[70] + z[76] + z[78];
z[76] = z[85] + -z[146] + -z[177];
z[76] = z[28] * z[76];
z[75] = -(z[75] * z[164]);
z[78] = z[12] + z[206];
z[78] = z[6] * z[78];
z[70] = z[70] / T(2) + z[75] + z[76] + z[78] + z[108];
z[70] = z[40] * z[70];
z[65] = z[65] + z[92];
z[65] = z[37] * z[65];
z[65] = z[65] + -z[81] + z[178];
z[75] = z[163] / T(2) + -z[171];
z[75] = z[36] * z[75];
z[76] = z[47] * z[98];
z[68] = z[46] * z[68];
z[68] = -z[65] + z[68] + z[75] + z[76];
z[75] = z[77] + -z[144];
z[62] = z[62] * z[75];
z[63] = z[33] * z[63];
z[75] = z[127] * z[128];
z[76] = -z[63] + z[75];
z[76] = z[76] * z[175];
z[77] = prod_pow(z[31], 2) * z[98];
z[62] = z[62] + T(3) * z[68] + z[76] + (T(-3) * z[77]) / T(2) + z[176];
z[62] = z[34] * z[62];
z[55] = z[10] + -z[55] + z[73] + -z[110];
z[55] = z[25] * z[55];
z[68] = z[101] + -z[110] + z[121];
z[68] = z[34] * z[68];
z[59] = z[26] * z[59];
z[71] = -(z[35] * z[71]);
z[73] = -z[11] + -z[93] + T(2) * z[123];
z[73] = z[14] * z[73];
z[55] = z[55] + z[59] + z[68] + z[71] + z[73];
z[55] = z[38] * z[55];
z[59] = -z[105] + z[133] + z[155] + -z[200];
z[59] = int_to_imaginary<T>(1) * z[59];
z[59] = z[59] + z[137];
z[59] = z[33] * z[59];
z[68] = z[94] + z[134];
z[68] = z[4] * z[68];
z[71] = z[90] + -z[94];
z[71] = z[3] * z[71];
z[68] = z[68] + z[71] + -z[161] + -z[162];
z[71] = -(z[6] * z[147]);
z[68] = z[68] / T(2) + z[71] + z[105] + -z[142];
z[68] = z[68] * z[145];
z[59] = z[59] + z[68];
z[59] = z[59] * z[84];
z[63] = z[63] + z[75];
z[63] = z[63] * z[175];
z[63] = z[63] + T(-3) * z[65];
z[63] = z[14] * z[63];
z[65] = int_to_imaginary<T>(1) * prod_pow(z[33], 3);
z[65] = -z[65] / T(9) + T(3) * z[95];
z[65] = z[49] * z[65];
return z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[63] + z[64] + T(4) * z[65] + z[66] + T(3) * z[67] + z[69] + z[70] + z[72] / T(2) + z[74] + z[82] + z[83] + z[111];
}



template IntegrandConstructorType<double> f_4_290_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_290_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_290_construct (const Kin<qd_real>&);
#endif

}