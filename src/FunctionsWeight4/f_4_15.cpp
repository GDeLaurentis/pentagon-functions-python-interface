#include "f_4_15.h"

namespace PentagonFunctions {

template <typename T> T f_4_15_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_15_W_12 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_15_W_12 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[1] * (T(4) + T(2) * kin.v[1]) + kin.v[4] * (T(-4) + T(-2) * kin.v[4])) + rlog(-kin.v[1]) * (kin.v[1] * (T(-4) + T(-2) * kin.v[1]) + kin.v[4] * (T(4) + T(2) * kin.v[4])) + rlog(kin.v[2]) * (kin.v[1] * (T(-4) + T(-2) * kin.v[1]) + kin.v[4] * (T(4) + T(2) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(2) + prod_pow(abb[2], 2) * (abb[5] * T(-2) + abb[4] * T(2)) + prod_pow(abb[1], 2) * (abb[3] * T(-2) + abb[4] * T(-2) + abb[5] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_15_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl5 = DLog_W_5<T>(kin),dl15 = DLog_W_15<T>(kin),dl3 = DLog_W_3<T>(kin),dl2 = DLog_W_2<T>(kin),dl17 = DLog_W_17<T>(kin),spdl12 = SpDLog_f_4_15_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,17> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[16] / kin_path.W[16]), dl5(t), -rlog(t), rlog(kin.W[14] / kin_path.W[14]), dl15(t), rlog(v_path[2]), dl3(t), f_2_1_4(kin_path), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl2(t), dl17(t)}
;

        auto result = f_4_15_abbreviated(abbr);
        result = result + spdl12(t, abbr);

        return result;
    };
}

template <typename T> T f_4_15_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[31];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[15];
z[3] = abb[16];
z[4] = abb[4];
z[5] = abb[5];
z[6] = abb[7];
z[7] = abb[10];
z[8] = abb[11];
z[9] = bc<TR>[0];
z[10] = abb[2];
z[11] = abb[6];
z[12] = abb[9];
z[13] = abb[8];
z[14] = abb[12];
z[15] = abb[13];
z[16] = abb[14];
z[17] = bc<TR>[2];
z[18] = bc<TR>[9];
z[19] = (T(-2) * z[3]) / T(3) + T(2) * z[12];
z[20] = z[1] + z[4];
z[21] = -z[5] + z[20];
z[19] = z[19] * z[21];
z[20] = z[6] + z[13] + -z[20];
z[20] = z[11] * z[20];
z[22] = z[1] + -z[5];
z[23] = (T(-3) * z[4]) / T(2) + z[6] / T(6) + (T(-4) * z[22]) / T(3);
z[23] = z[2] * z[23];
z[24] = -z[4] + z[5];
z[25] = (T(13) * z[13]) / T(2) + T(-2) * z[24];
z[25] = (T(-3) * z[1]) / T(2) + z[25] / T(3);
z[25] = z[8] * z[25];
z[26] = z[3] + -z[8];
z[27] = -z[2] + z[26];
z[28] = z[17] * z[27];
z[19] = z[19] + -z[20] / T(6) + z[23] + z[25] + T(-2) * z[28];
z[23] = prod_pow(z[9], 2);
z[19] = z[19] * z[23];
z[25] = z[21] * z[27];
z[28] = z[0] * z[25];
z[29] = z[3] + -z[12];
z[29] = z[21] * z[29];
z[24] = -z[13] + z[24];
z[24] = z[8] * z[24];
z[24] = z[24] + z[29];
z[24] = z[7] * z[24];
z[24] = z[24] + T(-2) * z[28];
z[24] = z[7] * z[24];
z[22] = -z[6] + -z[22];
z[22] = z[2] * z[22];
z[29] = z[3] * z[21];
z[22] = z[22] + z[29];
z[22] = prod_pow(z[0], 2) * z[22];
z[29] = -z[2] + z[12];
z[30] = -z[8] + z[29];
z[30] = z[21] * z[30];
z[20] = z[20] + z[30];
z[20] = prod_pow(z[10], 2) * z[20];
z[20] = z[20] + z[22] + z[24];
z[22] = -(z[16] * z[25]);
z[21] = z[21] * z[29];
z[24] = z[1] + -z[13];
z[24] = -(z[8] * z[24]);
z[21] = z[21] + z[24];
z[21] = z[7] * z[21];
z[21] = z[21] + z[22] + z[28];
z[22] = z[2] / T(2) + (T(-5) * z[12]) / T(6) + z[26] / T(3);
z[22] = z[22] * z[23];
z[21] = T(4) * z[21] + z[22];
z[21] = int_to_imaginary<T>(1) * z[9] * z[21];
z[22] = T(4) * z[25];
z[23] = z[14] + z[15];
z[22] = z[22] * z[23];
z[23] = z[18] * z[27];
return z[19] + T(2) * z[20] + z[21] + z[22] + T(3) * z[23];
}



template IntegrandConstructorType<double> f_4_15_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_15_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_15_construct (const Kin<qd_real>&);
#endif

}