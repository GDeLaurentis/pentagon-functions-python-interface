#include "f_4_119.h"

namespace PentagonFunctions {

template <typename T> T f_4_119_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_119_W_22 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_119_W_22 (const Kin<T>& kin) {
        c[0] = rlog(kin.v[2]) * ((-kin.v[4] / T(2) + (T(3) * kin.v[1]) / T(4) + kin.v[2] / T(2) + (T(-3) * kin.v[3]) / T(2) + T(-1) + kin.v[0]) * kin.v[1] + (-kin.v[2] / T(4) + -kin.v[3] / T(2) + kin.v[4] / T(2) + T(-1)) * kin.v[2] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + ((T(3) * kin.v[3]) / T(4) + kin.v[4] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(4) + T(1)) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((-kin.v[4] / T(2) + (T(3) * kin.v[1]) / T(4) + kin.v[2] / T(2) + (T(-3) * kin.v[3]) / T(2) + T(-1) + kin.v[0]) * kin.v[1] + (-kin.v[2] / T(4) + -kin.v[3] / T(2) + kin.v[4] / T(2) + T(-1)) * kin.v[2] + kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + ((T(3) * kin.v[3]) / T(4) + kin.v[4] / T(2) + T(1)) * kin.v[3] + (-kin.v[4] / T(4) + T(1)) * kin.v[4]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * ((-kin.v[2] / T(2) + (T(-3) * kin.v[1]) / T(4) + (T(3) * kin.v[3]) / T(2) + kin.v[4] / T(2) + -kin.v[0] + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + kin.v[2] / T(4) + kin.v[3] / T(2) + T(1)) * kin.v[2] + (-kin.v[4] / T(2) + (T(-3) * kin.v[3]) / T(4) + T(-1)) * kin.v[3] + (kin.v[4] / T(4) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((-kin.v[2] / T(2) + (T(-3) * kin.v[1]) / T(4) + (T(3) * kin.v[3]) / T(2) + kin.v[4] / T(2) + -kin.v[0] + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + kin.v[2] / T(4) + kin.v[3] / T(2) + T(1)) * kin.v[2] + (-kin.v[4] / T(2) + (T(-3) * kin.v[3]) / T(4) + T(-1)) * kin.v[3] + (kin.v[4] / T(4) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]));
c[1] = rlog(kin.v[2]) * (-kin.v[3] + -kin.v[4] + kin.v[1] + kin.v[2]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (-kin.v[3] + -kin.v[4] + kin.v[1] + kin.v[2]) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[4] * (abb[3] + -prod_pow(abb[2], 2) / T(2)) + prod_pow(abb[2], 2) * (abb[5] / T(2) + abb[6] / T(2) + -abb[7] / T(2)) + abb[3] * (abb[7] + -abb[5] + -abb[6]) + abb[1] * (abb[1] * ((abb[5] * T(-3)) / T(2) + (abb[6] * T(-3)) / T(2) + (abb[4] * T(3)) / T(2) + (abb[7] * T(3)) / T(2)) + -(abb[2] * abb[4]) + abb[2] * (abb[5] + abb[6] + -abb[7])));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_119_construct (const Kin<T>& kin) {
    return [&kin, 
            dl22 = DLog_W_22<T>(kin),dl8 = DLog_W_8<T>(kin),dl3 = DLog_W_3<T>(kin),dl20 = DLog_W_20<T>(kin),dl16 = DLog_W_16<T>(kin),dl19 = DLog_W_19<T>(kin),spdl22 = SpDLog_f_4_119_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,17> abbr = 
            {dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_14(kin_path), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), dl8(t), rlog(v_path[2]), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl3(t), -rlog(t), dl20(t), dl16(t), dl19(t)}
;

        auto result = f_4_119_abbreviated(abbr);
        result = result + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_119_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[45];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[12];
z[3] = abb[14];
z[4] = abb[15];
z[5] = abb[16];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[7];
z[9] = abb[13];
z[10] = abb[2];
z[11] = abb[9];
z[12] = bc<TR>[0];
z[13] = abb[8];
z[14] = abb[3];
z[15] = abb[10];
z[16] = abb[11];
z[17] = bc<TR>[2];
z[18] = bc<TR>[9];
z[19] = -z[7] + T(3) * z[8];
z[20] = z[1] + z[6];
z[21] = T(2) * z[20];
z[22] = T(4) * z[9];
z[23] = -z[19] + -z[21] + z[22];
z[24] = z[4] * z[23];
z[25] = z[5] * z[23];
z[26] = z[1] + z[7];
z[27] = T(3) * z[6] + z[8] + z[26];
z[28] = -z[22] + z[27];
z[28] = z[3] * z[28];
z[29] = T(2) * z[9];
z[30] = -z[6] + -z[26] + z[29];
z[31] = T(2) * z[2];
z[32] = z[30] * z[31];
z[33] = z[6] + z[8];
z[26] = -z[26] + z[33];
z[26] = z[13] * z[26];
z[24] = -z[24] + z[25] + z[26] + z[28] + z[32];
z[28] = -(z[16] * z[24]);
z[32] = z[7] + z[8];
z[22] = -z[22] + z[32];
z[21] = z[21] + z[22];
z[34] = z[4] * z[21];
z[33] = z[1] + -z[29] + z[33];
z[31] = z[31] * z[33];
z[31] = z[26] + -z[31] + z[34];
z[31] = z[10] * z[31];
z[34] = -z[7] + z[8];
z[35] = z[2] * z[34];
z[34] = z[4] * z[34];
z[35] = z[34] + z[35];
z[36] = z[25] + T(2) * z[35];
z[36] = z[0] * z[36];
z[31] = z[31] + z[36];
z[36] = -z[1] + z[6];
z[37] = z[3] * z[36];
z[34] = -z[26] + z[34] + z[37];
z[34] = z[11] * z[34];
z[37] = z[2] + -z[4];
z[38] = z[5] + z[37];
z[39] = prod_pow(z[17], 2);
z[39] = z[39] / T(2);
z[40] = z[38] * z[39];
z[41] = z[0] * z[21];
z[36] = z[10] * z[36];
z[41] = z[36] + z[41];
z[39] = -z[39] + z[41];
z[39] = z[3] * z[39];
z[42] = z[3] + -z[5];
z[42] = -z[37] + z[42] / T(2);
z[43] = prod_pow(z[12], 2);
z[42] = z[42] * z[43];
z[28] = z[28] + z[31] + T(2) * z[34] + z[39] + z[40] + z[42] / T(6);
z[28] = int_to_imaginary<T>(1) * z[12] * z[28];
z[34] = z[20] + -z[29];
z[19] = z[19] / T(2) + z[34];
z[19] = z[5] * z[19];
z[34] = z[32] / T(2) + z[34];
z[39] = z[4] * z[34];
z[40] = z[2] * z[30];
z[42] = T(3) * z[1] + z[6];
z[44] = z[32] + z[42];
z[44] = -z[29] + z[44] / T(2);
z[44] = z[3] * z[44];
z[39] = -z[19] + (T(3) * z[26]) / T(2) + z[39] + z[40] + z[44];
z[39] = z[11] * z[39];
z[40] = -(z[3] * z[41]);
z[31] = -z[31] + z[39] + z[40];
z[31] = z[11] * z[31];
z[39] = z[6] / T(3);
z[40] = (T(2) * z[9]) / T(3);
z[41] = (T(5) * z[17]) / T(4);
z[32] = -z[1] + -z[32] / T(6) + z[39] + z[40] + -z[41];
z[32] = z[3] * z[32];
z[44] = -z[7] / T(3) + z[8];
z[39] = z[1] / T(3) + z[39] + -z[40] + z[44] / T(2);
z[39] = z[5] * z[39];
z[21] = z[2] * z[21];
z[38] = z[38] * z[41];
z[20] = -z[7] + z[8] / T(3) + (T(4) * z[9]) / T(3) + (T(-2) * z[20]) / T(3);
z[20] = z[4] * z[20];
z[20] = z[20] + z[21] / T(3) + (T(-4) * z[26]) / T(3) + z[32] + z[38] + z[39];
z[20] = z[20] * z[43];
z[24] = z[15] * z[24];
z[23] = -(z[2] * z[23]);
z[30] = z[4] * z[30];
z[22] = z[22] + z[42];
z[22] = z[3] * z[22];
z[22] = z[22] + z[23] + z[25] + T(2) * z[30];
z[22] = z[14] * z[22];
z[23] = z[2] * z[34];
z[19] = -z[19] + z[23] + z[30];
z[19] = z[0] * z[19];
z[23] = z[4] * z[33];
z[21] = -z[21] + T(2) * z[23];
z[21] = z[10] * z[21];
z[19] = z[19] + z[21];
z[19] = z[0] * z[19];
z[21] = -z[26] + z[35];
z[21] = prod_pow(z[10], 2) * z[21];
z[23] = -z[5] / T(4) + (T(-21) * z[37]) / T(4);
z[23] = z[18] * z[23];
z[21] = z[21] + z[23];
z[23] = z[27] / T(2) + -z[29];
z[23] = z[0] * z[23];
z[23] = z[23] + z[36];
z[23] = z[0] * z[23];
z[23] = z[18] / T(8) + z[23];
z[23] = z[3] * z[23];
return z[19] + z[20] + z[21] / T(2) + z[22] + z[23] + z[24] + z[28] + z[31];
}



template IntegrandConstructorType<double> f_4_119_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_119_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_119_construct (const Kin<qd_real>&);
#endif

}