#include "f_4_323.h"

namespace PentagonFunctions {

template <typename T> T f_4_323_abbreviated (const std::array<T,40>&);

template <typename T> class SpDLog_f_4_323_W_7 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_323_W_7 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[1]) * (T(6) * kin.v[1] * kin.v[4] + kin.v[3] * (T(6) + T(6) * kin.v[1] + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4])) + rlog(kin.v[3]) * (T(6) * kin.v[1] * kin.v[4] + kin.v[3] * (T(6) + T(6) * kin.v[1] + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[4] * (T(6) + T(-3) * kin.v[4])) + rlog(-kin.v[4]) * (T(-6) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[3] * (T(-6) + T(-6) * kin.v[1] + T(3) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (T(-6) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-6) + T(3) * kin.v[4]) + kin.v[3] * (T(-6) + T(-6) * kin.v[1] + T(3) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-3) + prod_pow(abb[1], 2) * (abb[5] * T(-3) + abb[6] * T(-3) + abb[3] * T(3) + abb[4] * T(3)) + prod_pow(abb[2], 2) * (abb[4] * T(-3) + abb[5] * T(3) + abb[6] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_323_W_12 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_323_W_12 (const Kin<T>& kin) {
        c[0] = T(4) * kin.v[0] * kin.v[4] + T(-8) * kin.v[2] * kin.v[4] + T(-4) * kin.v[3] * kin.v[4] + T(-2) * prod_pow(kin.v[4], 2) + kin.v[1] * (T(-4) * kin.v[0] + T(-2) * kin.v[1] + T(8) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(-4) + T(-2) * kin.v[1]) + kin.v[4] * (T(4) + T(2) * kin.v[4]));
c[1] = kin.v[1] * ((bc<T>[0] * int_to_imaginary<T>(5) + T(10)) * kin.v[1] + T(8) * kin.v[2] + T(4) * kin.v[3] + T(-20) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(4) * kin.v[2] + T(2) * kin.v[3] + T(-10) * kin.v[4])) + T(-8) * kin.v[2] * kin.v[4] + T(-4) * kin.v[3] * kin.v[4] + T(10) * prod_pow(kin.v[4], 2) + kin.v[0] * ((T(-4) + bc<T>[0] * int_to_imaginary<T>(-2)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[4] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] * kin.v[4] + T(-2) * kin.v[3] * kin.v[4] + T(5) * prod_pow(kin.v[4], 2)) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (T(-2) * kin.v[0] * kin.v[4] + T(4) * kin.v[2] * kin.v[4] + T(2) * kin.v[3] * kin.v[4] + prod_pow(kin.v[4], 2) + kin.v[1] * (T(2) * kin.v[0] + kin.v[1] + T(-4) * kin.v[2] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (kin.v[1] * (T(2) + kin.v[1]) + (-kin.v[4] + T(-2)) * kin.v[4])) + rlog(-kin.v[1]) * (prod_pow(kin.v[4], 2) / T(2) + -(kin.v[0] * kin.v[4]) + kin.v[1] * (kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + kin.v[0] + T(-2) * kin.v[2]) + T(2) * kin.v[2] * kin.v[4] + kin.v[3] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4])) + rlog(kin.v[2]) * (prod_pow(kin.v[4], 2) / T(2) + -(kin.v[0] * kin.v[4]) + kin.v[1] * (kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + kin.v[0] + T(-2) * kin.v[2]) + T(2) * kin.v[2] * kin.v[4] + kin.v[3] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4])) + rlog(-kin.v[4]) * (prod_pow(kin.v[4], 2) / T(2) + -(kin.v[0] * kin.v[4]) + kin.v[1] * (kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + kin.v[0] + T(-2) * kin.v[2]) + T(2) * kin.v[2] * kin.v[4] + kin.v[3] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (prod_pow(kin.v[4], 2) / T(2) + -(kin.v[0] * kin.v[4]) + kin.v[1] * (kin.v[1] / T(2) + -kin.v[3] + -kin.v[4] + kin.v[0] + T(-2) * kin.v[2]) + T(2) * kin.v[2] * kin.v[4] + kin.v[3] * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((kin.v[1] / T(2) + T(1)) * kin.v[1] + (-kin.v[4] / T(2) + T(-1)) * kin.v[4])) + rlog(kin.v[3]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + T(3) * kin.v[0] * kin.v[4] + T(-6) * kin.v[2] * kin.v[4] + T(-3) * kin.v[3] * kin.v[4] + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-3) * kin.v[0] + T(6) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[1]) / T(2) + T(-3)) * kin.v[1] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + T(3) * kin.v[0] * kin.v[4] + T(-6) * kin.v[2] * kin.v[4] + T(-3) * kin.v[3] * kin.v[4] + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-3) * kin.v[0] + T(6) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(-3) * kin.v[1]) / T(2) + T(-3)) * kin.v[1] + ((T(3) * kin.v[4]) / T(2) + T(3)) * kin.v[4]));
c[2] = (T(-8) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[1] + T(8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(4) * kin.v[4];
c[3] = rlog(-kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[1] + T(-2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-1) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[1] + T(-4) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-2) * kin.v[4]) + rlog(kin.v[3]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4] + T(6) * kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(3) * kin.v[4] + T(6) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[19] * (abb[3] * (-(abb[17] * abb[18]) + abb[20] * T(-2) + abb[18] * (abb[9] + abb[18] + -abb[2] + bc<T>[0] * int_to_imaginary<T>(1))) + abb[17] * abb[18] * (-abb[5] + -abb[6] + -abb[8] + abb[21] * T(-4) + abb[22] * T(-2) + abb[4] * T(3) + abb[12] * T(3)) + abb[18] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(1) + abb[22] * bc<T>[0] * int_to_imaginary<T>(2) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[21] * bc<T>[0] * int_to_imaginary<T>(4) + abb[2] * (-abb[5] + -abb[6] + -abb[8] + abb[21] * T(-4) + abb[22] * T(-2) + abb[4] * T(3) + abb[12] * T(3)) + abb[9] * (abb[5] + abb[6] + abb[8] + abb[4] * T(-3) + abb[12] * T(-3) + abb[22] * T(2) + abb[21] * T(4)) + abb[18] * (abb[5] + abb[6] + abb[8] + abb[4] * T(-3) + abb[12] * T(-3) + abb[22] * T(2) + abb[21] * T(4))) + abb[20] * (abb[21] * T(-8) + abb[22] * T(-4) + abb[5] * T(-2) + abb[6] * T(-2) + abb[8] * T(-2) + abb[4] * T(6) + abb[12] * T(6)) + abb[1] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[8] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[3] * (abb[2] + abb[17] + abb[18] + -abb[9] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[22] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[4] * bc<T>[0] * int_to_imaginary<T>(3) + abb[12] * bc<T>[0] * int_to_imaginary<T>(3) + abb[21] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[9] * (-abb[5] + -abb[6] + -abb[8] + abb[21] * T(-4) + abb[22] * T(-2) + abb[4] * T(3) + abb[12] * T(3)) + abb[2] * (abb[5] + abb[6] + abb[8] + abb[4] * T(-3) + abb[12] * T(-3) + abb[22] * T(2) + abb[21] * T(4)) + abb[17] * (abb[5] + abb[6] + abb[8] + abb[4] * T(-3) + abb[12] * T(-3) + abb[22] * T(2) + abb[21] * T(4)) + abb[18] * (abb[5] + abb[6] + abb[8] + abb[4] * T(-3) + abb[12] * T(-3) + abb[22] * T(2) + abb[21] * T(4)) + abb[1] * (abb[21] * T(-8) + abb[22] * T(-4) + abb[3] * T(-2) + abb[5] * T(-2) + abb[6] * T(-2) + abb[8] * T(-2) + abb[4] * T(6) + abb[12] * T(6))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_323_construct (const Kin<T>& kin) {
    return [&kin, 
            dl7 = DLog_W_7<T>(kin),dl18 = DLog_W_18<T>(kin),dl27 = DLog_W_27<T>(kin),dl15 = DLog_W_15<T>(kin),dl12 = DLog_W_12<T>(kin),dl16 = DLog_W_16<T>(kin),dl30 = DLog_W_30<T>(kin),dl20 = DLog_W_20<T>(kin),dl29 = DLog_W_29<T>(kin),dl1 = DLog_W_1<T>(kin),dl28 = DLog_W_28<T>(kin),dl26 = DLog_W_26<T>(kin),dl19 = DLog_W_19<T>(kin),dl2 = DLog_W_2<T>(kin),dl3 = DLog_W_3<T>(kin),dl17 = DLog_W_17<T>(kin),dl5 = DLog_W_5<T>(kin),dl4 = DLog_W_4<T>(kin),spdl7 = SpDLog_f_4_323_W_7<T>(kin),spdl12 = SpDLog_f_4_323_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,40> abbr = 
            {dl7(t), rlog(-v_path[1]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[18] / kin_path.W[18]), dl18(t), rlog(kin.W[2] / kin_path.W[2]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_11(kin_path), f_2_1_15(kin_path), rlog(kin.W[16] / kin_path.W[16]), dl27(t) / kin_path.SqrtDelta, f_1_3_1(kin) - f_1_3_1(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), dl15(t), rlog(v_path[2]), rlog(-v_path[4]), dl12(t), f_2_1_4(kin_path), -rlog(t), rlog(kin.W[19] / kin_path.W[19]), dl16(t), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), dl30(t) / kin_path.SqrtDelta, dl20(t), f_2_1_5(kin_path), rlog(-v_path[4] + v_path[2]), dl29(t) / kin_path.SqrtDelta, dl1(t), dl28(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl19(t), dl2(t), dl3(t), dl17(t), dl5(t), dl4(t)}
;

        auto result = f_4_323_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_323_abbreviated(const std::array<T,40>& abb)
{
using TR = typename T::value_type;
T z[121];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[7];
z[3] = abb[27];
z[4] = abb[34];
z[5] = abb[35];
z[6] = abb[36];
z[7] = abb[37];
z[8] = abb[38];
z[9] = abb[39];
z[10] = abb[4];
z[11] = abb[5];
z[12] = abb[6];
z[13] = abb[8];
z[14] = abb[12];
z[15] = abb[13];
z[16] = abb[14];
z[17] = abb[15];
z[18] = abb[30];
z[19] = abb[33];
z[20] = abb[21];
z[21] = abb[22];
z[22] = abb[2];
z[23] = abb[9];
z[24] = abb[17];
z[25] = abb[18];
z[26] = bc<TR>[0];
z[27] = abb[32];
z[28] = abb[23];
z[29] = abb[26];
z[30] = abb[10];
z[31] = abb[11];
z[32] = abb[31];
z[33] = abb[16];
z[34] = abb[20];
z[35] = abb[24];
z[36] = abb[25];
z[37] = abb[28];
z[38] = abb[29];
z[39] = bc<TR>[3];
z[40] = bc<TR>[1];
z[41] = bc<TR>[2];
z[42] = bc<TR>[4];
z[43] = bc<TR>[9];
z[44] = -z[1] + z[11];
z[45] = z[10] + z[14];
z[46] = z[12] + z[13];
z[47] = -z[44] + z[45] + -z[46];
z[48] = z[2] * z[47];
z[49] = z[10] + -z[14];
z[50] = -z[12] + z[13] + z[44] + z[49];
z[51] = z[28] * z[50];
z[52] = -z[16] + z[17];
z[53] = z[18] * z[52];
z[54] = z[48] + z[51] + -z[53];
z[55] = T(3) * z[13];
z[56] = T(3) * z[14];
z[57] = z[55] + -z[56];
z[58] = -z[10] + z[12] + (T(-7) * z[44]) / T(3) + z[57];
z[58] = z[3] * z[58];
z[59] = z[15] * z[52];
z[60] = z[19] * z[52];
z[61] = z[29] * z[52];
z[58] = -z[54] + z[58] + -z[59] + z[60] + z[61];
z[62] = z[11] + z[12];
z[63] = T(3) * z[1];
z[64] = T(5) * z[14];
z[55] = z[10] + -z[55] + z[62] + -z[63] + z[64];
z[55] = z[55] / T(2);
z[65] = -z[21] + z[55];
z[65] = -z[20] + z[65] / T(2);
z[66] = z[7] * z[65];
z[67] = T(3) * z[11];
z[68] = T(3) * z[12];
z[69] = z[1] + z[13];
z[70] = T(-5) * z[10] + -z[14] + z[67] + z[68] + -z[69];
z[70] = z[70] / T(2);
z[71] = -z[21] + -z[70];
z[71] = -z[20] + z[71] / T(2);
z[71] = z[9] * z[71];
z[72] = z[1] / T(3) + (T(-5) * z[11]) / T(3);
z[73] = T(3) * z[10];
z[64] = (T(5) * z[12]) / T(3) + (T(-19) * z[13]) / T(3) + z[64] + -z[72] + -z[73];
z[74] = (T(2) * z[20]) / T(3) + z[21] / T(3);
z[64] = z[64] / T(4) + z[74];
z[64] = z[5] * z[64];
z[75] = -z[6] + z[7];
z[76] = -z[5] + z[8];
z[77] = z[75] + z[76];
z[78] = -z[4] + z[9];
z[79] = z[77] + z[78];
z[80] = z[40] * z[79];
z[81] = z[11] + z[45];
z[82] = z[12] + z[69];
z[83] = z[81] + -z[82];
z[84] = z[32] * z[83];
z[85] = z[56] + z[73];
z[86] = -z[46] + z[85];
z[87] = (T(5) * z[1]) / T(3) + z[11] / T(3) + -z[86];
z[87] = z[21] + z[87] / T(2);
z[87] = z[20] + z[87] / T(2);
z[87] = z[4] * z[87];
z[86] = (T(-19) * z[1]) / T(3) + (T(25) * z[11]) / T(3) + -z[86];
z[86] = z[21] + z[86] / T(2);
z[86] = z[20] + z[86] / T(2);
z[86] = z[6] * z[86];
z[72] = (T(13) * z[12]) / T(3) + z[13] / T(3) + z[14] + z[72] + -z[73];
z[72] = z[72] / T(4) + -z[74];
z[72] = z[8] * z[72];
z[74] = T(3) * z[41];
z[77] = -(z[74] * z[77]);
z[88] = -z[11] + -z[14] + z[69];
z[88] = z[33] * z[88];
z[89] = T(3) * z[88];
z[90] = z[27] * z[52];
z[91] = (T(3) * z[90]) / T(4);
z[58] = z[58] / T(4) + z[64] + z[66] + z[71] + z[72] + z[77] + z[80] + (T(5) * z[84]) / T(4) + z[86] + z[87] + z[89] + z[91];
z[58] = z[26] * z[58];
z[64] = T(2) * z[20] + z[21];
z[55] = z[55] + -z[64];
z[66] = z[5] + z[6];
z[71] = z[55] * z[66];
z[60] = z[60] + z[84];
z[72] = z[3] / T(2);
z[77] = z[72] * z[83];
z[86] = z[53] / T(2);
z[60] = -z[60] / T(2) + z[71] + z[77] + z[86];
z[71] = -(z[38] * z[60]);
z[77] = z[38] * z[55];
z[77] = -z[42] + z[77];
z[77] = z[7] * z[77];
z[45] = -z[45] + z[67] + -z[82];
z[45] = z[45] / T(2) + z[64];
z[67] = -(z[38] * z[45]);
z[67] = -z[42] + z[67];
z[67] = z[8] * z[67];
z[87] = z[66] + -z[78];
z[87] = z[42] * z[87];
z[79] = prod_pow(z[40], 2) * z[79];
z[67] = z[67] + z[71] + z[77] + -z[79] / T(2) + z[87];
z[71] = z[62] + z[69] + -z[85];
z[77] = z[71] / T(2);
z[79] = z[64] + z[77];
z[87] = z[6] * z[79];
z[92] = z[5] * z[79];
z[93] = z[87] + z[92];
z[94] = T(2) * z[11];
z[95] = z[64] + z[94];
z[82] = -z[82] + z[95];
z[96] = z[3] * z[82];
z[82] = z[8] * z[82];
z[96] = T(2) * z[82] + z[96];
z[97] = z[4] * z[79];
z[98] = (T(3) * z[52]) / T(2);
z[99] = z[27] * z[98];
z[97] = z[97] + z[99];
z[100] = (T(3) * z[84]) / T(2);
z[101] = -z[93] + z[96] + -z[97] + -z[100];
z[101] = z[25] * z[101];
z[102] = T(2) * z[1] + z[64];
z[103] = T(2) * z[13] + -z[56] + -z[62] + z[102];
z[104] = z[5] * z[103];
z[89] = z[89] + -z[104];
z[105] = z[3] * z[44];
z[82] = z[82] + z[105];
z[106] = T(2) * z[44];
z[107] = z[6] * z[106];
z[108] = -(z[4] * z[44]);
z[108] = z[82] + z[89] + z[107] + z[108];
z[108] = z[24] * z[108];
z[109] = -z[19] + z[27];
z[110] = (T(3) * z[75]) / T(4) + z[78] / T(4) + -z[109] / T(9);
z[110] = prod_pow(z[26], 2) * z[110];
z[74] = z[74] * z[80];
z[67] = T(3) * z[67] + z[74] + z[101] + T(2) * z[108] + z[110];
z[67] = int_to_imaginary<T>(1) * z[67];
z[74] = z[39] * z[109];
z[58] = z[58] + z[67] + T(3) * z[74];
z[58] = z[26] * z[58];
z[67] = T(3) * z[7];
z[65] = z[65] * z[67];
z[68] = z[57] + -z[68] + z[73];
z[74] = -z[44] + z[68];
z[80] = z[4] * z[74];
z[108] = -z[10] + z[13] + z[21];
z[94] = z[1] / T(2) + T(-3) * z[20] + z[56] + -z[94] + (T(-3) * z[108]) / T(2);
z[94] = z[6] * z[94];
z[77] = z[21] + z[77];
z[77] = z[20] + z[77] / T(2);
z[108] = -(z[5] * z[77]);
z[109] = -z[12] + z[21];
z[109] = -z[20] + -z[109] / T(2);
z[110] = -z[11] + z[69] / T(2) + z[109];
z[110] = z[8] * z[110];
z[88] = z[51] / T(4) + -z[88];
z[57] = z[44] + -z[57];
z[57] = z[57] * z[72];
z[111] = (T(3) * z[61]) / T(4);
z[52] = (T(3) * z[52]) / T(4);
z[112] = -(z[19] * z[52]);
z[57] = z[57] + -z[65] + -z[80] / T(4) + T(3) * z[88] + -z[91] + z[94] + -z[100] + z[108] + -z[110] + -z[111] + z[112];
z[57] = z[24] * z[57];
z[57] = z[57] + -z[101];
z[57] = z[24] * z[57];
z[88] = z[48] + z[53];
z[94] = T(2) * z[12] + -z[69] + -z[73] + z[95];
z[95] = z[4] + z[8];
z[108] = -(z[94] * z[95]);
z[70] = z[64] + z[70];
z[112] = z[9] * z[70];
z[113] = T(3) * z[112];
z[77] = z[6] * z[77];
z[114] = T(7) * z[1];
z[115] = T(9) * z[14] + -z[114];
z[116] = T(9) * z[10];
z[117] = z[11] + z[46];
z[118] = z[115] + z[116] + -z[117];
z[119] = T(4) * z[20] + T(2) * z[21];
z[118] = z[118] / T(4) + -z[119];
z[118] = z[3] * z[118];
z[73] = T(5) * z[62] + -z[73];
z[114] = -z[13] + z[56] + z[73] + -z[114];
z[114] = -z[64] + z[114] / T(2);
z[114] = z[5] * z[114];
z[120] = -z[15] + -z[19];
z[120] = z[52] * z[120];
z[65] = -z[65] + z[77] + (T(-3) * z[88]) / T(4) + z[108] + z[113] + z[114] + z[118] + z[120];
z[65] = z[0] * z[65];
z[77] = z[6] * z[103];
z[88] = z[77] + z[104];
z[67] = z[55] * z[67];
z[103] = z[19] * z[98];
z[67] = z[67] + z[103];
z[103] = z[8] * z[79];
z[104] = z[18] * z[98];
z[108] = z[103] + z[104];
z[79] = z[3] * z[79];
z[88] = z[67] + z[79] + T(2) * z[88] + -z[97] + -z[108];
z[114] = int_to_imaginary<T>(1) * z[26];
z[118] = -z[24] + z[114];
z[88] = -(z[88] * z[118]);
z[120] = T(2) * z[94];
z[120] = z[95] * z[120];
z[79] = z[79] + -z[93] + z[104] + -z[113] + z[120];
z[93] = z[22] + z[25];
z[93] = z[79] * z[93];
z[65] = z[65] + z[88] + z[93];
z[65] = z[0] * z[65];
z[88] = z[72] * z[74];
z[93] = z[29] * z[98];
z[104] = z[4] * z[106];
z[88] = (T(3) * z[51]) / T(2) + -z[88] + z[92] + -z[93] + -z[104] + -z[107] + -z[108];
z[88] = z[88] * z[118];
z[79] = -(z[25] * z[79]);
z[74] = z[6] * z[74];
z[93] = T(-11) * z[44] + z[68];
z[93] = z[4] * z[93];
z[68] = z[44] + z[68];
z[104] = -(z[3] * z[68]);
z[54] = T(3) * z[54] + -z[74] + z[93] + z[104];
z[93] = T(5) * z[1];
z[85] = -z[85] + z[93] + -z[117];
z[85] = z[64] + z[85] / T(4);
z[85] = z[5] * z[85];
z[104] = -(z[8] * z[94]);
z[52] = z[15] * z[52];
z[52] = z[52] + z[54] / T(4) + z[85] + z[91] + z[104] + -z[111];
z[52] = z[22] * z[52];
z[52] = z[52] + z[79] + z[88];
z[52] = z[22] * z[52];
z[54] = z[15] * z[98];
z[54] = z[54] + z[103];
z[79] = -z[102] + z[117];
z[85] = z[5] * z[79];
z[79] = z[3] * z[79];
z[79] = (T(3) * z[48]) / T(2) + z[54] + z[79] + T(2) * z[85] + z[87] + z[97];
z[79] = z[0] * z[79];
z[87] = z[6] * z[68];
z[80] = -z[80] + z[87];
z[80] = -z[80] / T(2) + -z[82] + z[92] + z[99] + z[100];
z[80] = -(z[80] * z[118]);
z[68] = -(z[4] * z[68]);
z[68] = T(-3) * z[48] + z[68] + z[74];
z[54] = -z[54] + z[68] / T(2) + -z[85] + -z[99] + -z[105];
z[54] = z[22] * z[54];
z[68] = -z[4] + -z[6];
z[44] = z[44] * z[68];
z[44] = z[44] + z[105];
z[68] = z[11] + z[13];
z[68] = -z[1] + z[68] / T(2) + z[109];
z[68] = z[5] * z[68];
z[44] = z[44] / T(2) + z[68] + -z[110];
z[44] = z[23] * z[44];
z[44] = z[44] + z[54] + z[79] + z[80] + z[101];
z[44] = z[23] * z[44];
z[54] = -(z[4] * z[70]);
z[68] = z[47] * z[72];
z[46] = -z[46] + z[63] + -z[81];
z[46] = z[46] / T(2) + z[64];
z[46] = z[5] * z[46];
z[46] = z[46] + -z[48] / T(2) + z[54] + z[68] + -z[86] + z[112];
z[46] = z[30] * z[46];
z[49] = z[49] + -z[62] + z[69];
z[54] = z[3] * z[49];
z[51] = -z[51] + z[53] + z[54];
z[53] = z[51] + z[61];
z[54] = z[4] * z[49];
z[47] = -(z[5] * z[47]);
z[63] = -(z[6] * z[50]);
z[47] = z[47] + z[48] + -z[53] + z[54] + z[63] + z[90];
z[47] = z[31] * z[47];
z[48] = z[6] * z[49];
z[49] = z[4] * z[50];
z[48] = -z[48] + z[49] + z[84];
z[49] = -z[48] + -z[51];
z[49] = z[35] * z[49];
z[50] = -z[30] + -z[35];
z[50] = z[50] * z[90];
z[47] = z[47] + z[49] + z[50];
z[49] = z[7] * z[55];
z[49] = -z[49] + z[60];
z[49] = z[37] * z[49];
z[45] = z[37] * z[45];
z[50] = -(z[30] * z[70]);
z[51] = z[35] / T(2);
z[54] = z[51] * z[83];
z[45] = z[45] + z[50] + z[54];
z[45] = z[8] * z[45];
z[50] = -(z[51] * z[61]);
z[45] = z[45] + z[46] + z[47] / T(2) + z[49] + z[50];
z[46] = -(z[4] * z[94]);
z[46] = z[46] + -z[77] + z[89] + z[96];
z[46] = prod_pow(z[25], 2) * z[46];
z[47] = T(-5) * z[13] + z[56] + T(7) * z[62] + -z[93] + -z[116];
z[47] = z[47] / T(2) + z[64];
z[47] = -(z[47] * z[95]);
z[49] = -z[71] + -z[119];
z[49] = z[3] * z[49];
z[50] = T(-7) * z[13] + z[73] + z[115];
z[50] = z[50] / T(2) + -z[64];
z[50] = z[50] * z[66];
z[47] = z[47] + z[49] + z[50] + -z[67] + z[99] + z[113];
z[47] = z[34] * z[47];
z[49] = -(z[8] * z[83]);
z[48] = z[48] + z[49] + z[53] + z[90];
z[48] = z[36] * z[48] * z[114];
z[49] = -z[30] + z[31];
z[49] = z[49] * z[59];
z[48] = z[48] + z[49];
z[49] = z[75] + (T(85) * z[76]) / T(6) + (T(5) * z[78]) / T(2);
z[49] = z[43] * z[49];
return z[44] + T(3) * z[45] + z[46] + z[47] + (T(3) * z[48]) / T(2) + z[49] + z[52] + z[57] + z[58] + z[65];
}



template IntegrandConstructorType<double> f_4_323_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_323_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_323_construct (const Kin<qd_real>&);
#endif

}