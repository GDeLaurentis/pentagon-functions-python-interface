#include "f_4_125.h"

namespace PentagonFunctions {

template <typename T> T f_4_125_abbreviated (const std::array<T,18>&);

template <typename T> class SpDLog_f_4_125_W_21 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_125_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(-2) * kin.v[2] + T(-4) * kin.v[3]) + T(-2) * prod_pow(kin.v[3], 2) + kin.v[0] * (T(-2) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3]) + kin.v[1] * (T(-2) * kin.v[1] + T(4) * kin.v[2] + T(4) * kin.v[3]) + rlog(kin.v[2]) * (kin.v[0] * (-kin.v[0] / T(2) + -kin.v[3] + T(2) + kin.v[1] + kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * ((T(3) * kin.v[1]) / T(2) + -kin.v[2] + T(2) + T(-3) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-2) + T(2) * kin.v[4]) + kin.v[2] * (-kin.v[2] / T(2) + T(-2) + kin.v[3] + T(2) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[3] * ((T(-3) * kin.v[3]) / T(2) + T(2) + T(-2) * kin.v[4]) + kin.v[2] * (kin.v[2] / T(2) + -kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[0] * (kin.v[0] / T(2) + -kin.v[1] + -kin.v[2] + T(-2) + kin.v[3] + T(2) * kin.v[4]) + kin.v[1] * ((T(-3) * kin.v[1]) / T(2) + T(-2) + kin.v[2] + T(3) * kin.v[3] + T(2) * kin.v[4]));
c[1] = rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (T(2) * kin.v[0] + T(2) * kin.v[1] + T(-2) * kin.v[2] + T(-2) * kin.v[3]) + rlog(kin.v[2]) * (T(-2) * kin.v[0] + T(-2) * kin.v[1] + T(2) * kin.v[2] + T(2) * kin.v[3]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[5] + abb[3] * abb[5] * T(-2) + abb[4] * (-prod_pow(abb[2], 2) + abb[3] * T(2)) + abb[1] * (abb[2] * abb[4] * T(-2) + abb[2] * abb[5] * T(2) + abb[1] * (abb[5] * T(-3) + abb[4] * T(3))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_125_construct (const Kin<T>& kin) {
    return [&kin, 
            dl21 = DLog_W_21<T>(kin),dl16 = DLog_W_16<T>(kin),dl3 = DLog_W_3<T>(kin),dl8 = DLog_W_8<T>(kin),dl19 = DLog_W_19<T>(kin),dl20 = DLog_W_20<T>(kin),spdl21 = SpDLog_f_4_125_W_21<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,18> abbr = 
            {dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_2_1_15(kin_path), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[15] / kin_path.W[15]), dl16(t), rlog(v_path[2]), f_2_1_13(kin_path), rlog(v_path[0] + v_path[4]), rlog(kin.W[18] / kin_path.W[18]), -rlog(t), rlog(kin.W[7] / kin_path.W[7]), rlog(kin.W[19] / kin_path.W[19]), dl3(t), dl8(t), dl19(t), dl20(t)}
;

        auto result = f_4_125_abbreviated(abbr);
        result = result + spdl21(t, abbr);

        return result;
    };
}

template <typename T> T f_4_125_abbreviated(const std::array<T,18>& abb)
{
using TR = typename T::value_type;
T z[57];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[6];
z[3] = abb[16];
z[4] = abb[17];
z[5] = abb[5];
z[6] = abb[14];
z[7] = abb[10];
z[8] = abb[11];
z[9] = abb[12];
z[10] = abb[13];
z[11] = abb[2];
z[12] = abb[15];
z[13] = abb[7];
z[14] = bc<TR>[0];
z[15] = abb[3];
z[16] = abb[8];
z[17] = abb[9];
z[18] = bc<TR>[2];
z[19] = bc<TR>[9];
z[20] = T(3) * z[1];
z[21] = T(3) * z[7];
z[22] = T(2) * z[8];
z[23] = z[9] + -z[20] + -z[21] + z[22];
z[24] = T(3) * z[5];
z[25] = T(3) * z[10];
z[23] = T(2) * z[23] + z[24] + -z[25];
z[23] = z[3] * z[23];
z[26] = (T(3) * z[10]) / T(2);
z[21] = z[21] + z[26];
z[27] = z[9] / T(2);
z[28] = z[5] + z[27];
z[29] = T(4) * z[8];
z[30] = -z[1] + -z[21] + -z[28] + z[29];
z[30] = z[4] * z[30];
z[31] = z[1] + z[7];
z[32] = T(2) * z[5];
z[33] = z[9] + z[32];
z[34] = -z[22] + -z[31] + z[33];
z[35] = T(2) * z[6];
z[34] = z[34] * z[35];
z[36] = -z[29] + T(4) * z[31];
z[37] = z[25] + -z[33] + z[36];
z[38] = z[2] * z[37];
z[39] = -z[1] + z[27];
z[40] = -z[7] + z[26] + T(3) * z[39];
z[40] = z[12] * z[40];
z[23] = z[23] + z[30] + z[34] + z[38] + -z[40];
z[30] = z[17] * z[23];
z[34] = -z[22] + z[31];
z[40] = z[5] + z[10] + T(2) * z[34];
z[41] = z[4] * z[40];
z[42] = T(2) * z[7];
z[43] = T(2) * z[1];
z[44] = z[42] + z[43];
z[45] = z[10] + z[44];
z[46] = -z[33] + z[45];
z[47] = z[35] * z[46];
z[46] = z[3] * z[46];
z[46] = T(2) * z[46];
z[48] = z[5] + z[9];
z[49] = -z[44] + z[48];
z[49] = z[12] * z[49];
z[41] = -z[38] + z[41] + z[46] + z[47] + z[49];
z[47] = z[0] * z[41];
z[42] = z[10] + z[42];
z[50] = -z[9] + z[43];
z[51] = z[42] + -z[50];
z[51] = z[4] * z[51];
z[25] = T(4) * z[5] + T(-6) * z[7] + -z[25] + -z[50];
z[25] = z[12] * z[25];
z[45] = T(-3) * z[9] + z[45];
z[45] = z[35] * z[45];
z[25] = z[25] + z[45] + z[46] + z[51];
z[25] = z[13] * z[25];
z[45] = z[7] + z[39];
z[46] = -z[5] + z[26] + z[45];
z[46] = z[12] * z[46];
z[50] = z[10] / T(2);
z[45] = z[45] + z[50];
z[45] = z[4] * z[45];
z[40] = z[3] * z[40];
z[51] = z[10] + z[34];
z[35] = z[35] * z[51];
z[35] = -z[35] + z[40] + z[45] + z[46];
z[40] = z[11] * z[35];
z[45] = z[3] + z[6];
z[46] = z[2] + z[45];
z[52] = z[4] / T(2);
z[46] = (T(11) * z[12]) / T(6) + -z[46] / T(3) + -z[52];
z[53] = prod_pow(z[14], 2);
z[46] = z[46] * z[53];
z[54] = z[4] + z[12];
z[54] = z[3] + T(3) * z[6] + (T(-3) * z[54]) / T(2);
z[54] = prod_pow(z[18], 2) * z[54];
z[46] = z[46] + z[54];
z[25] = z[25] + z[30] + z[40] + z[46] / T(2) + z[47];
z[25] = int_to_imaginary<T>(1) * z[14] * z[25];
z[30] = -(z[13] * z[41]);
z[40] = T(2) * z[10];
z[41] = (T(3) * z[5]) / T(2);
z[46] = z[22] + -z[27] + z[40] + -z[41] + -z[43];
z[46] = z[3] * z[46];
z[44] = -z[22] + z[44];
z[26] = z[26] + -z[28] + z[44];
z[26] = z[2] * z[26];
z[47] = z[5] / T(2);
z[54] = -z[22] + z[47];
z[55] = -z[39] + z[42] + z[54];
z[55] = z[4] * z[55];
z[56] = -z[22] + z[28] + z[50];
z[56] = z[6] * z[56];
z[46] = -z[26] + z[46] + z[55] + z[56];
z[46] = z[0] * z[46];
z[30] = z[30] + z[46];
z[30] = z[0] * z[30];
z[46] = z[7] + z[20] + -z[27];
z[55] = z[10] / T(4);
z[46] = z[46] / T(2) + z[54] + z[55];
z[46] = z[4] * z[46];
z[34] = z[34] + z[47] + z[50];
z[34] = z[3] * z[34];
z[47] = T(5) * z[7] + (T(9) * z[10]) / T(2) + z[39];
z[47] = -z[32] + z[47] / T(2);
z[47] = z[12] * z[47];
z[51] = T(2) * z[9] + -z[32] + -z[51];
z[51] = z[6] * z[51];
z[26] = -z[26] + z[34] + z[46] + z[47] + z[51];
z[26] = prod_pow(z[13], 2) * z[26];
z[34] = -(z[13] * z[35]);
z[35] = z[36] + z[40] + -z[48];
z[35] = z[3] * z[35];
z[33] = -z[10] + z[29] + -z[33];
z[33] = z[6] * z[33];
z[36] = -z[9] + z[42];
z[36] = z[4] * z[36];
z[33] = z[33] + z[35] + z[36] + z[49];
z[33] = z[0] * z[33];
z[31] = z[31] + z[50];
z[28] = -z[28] + z[31];
z[28] = z[28] * z[45];
z[31] = (T(-3) * z[9]) / T(2) + z[31];
z[31] = z[31] * z[52];
z[21] = -z[21] + z[39];
z[21] = z[5] + z[21] / T(2);
z[21] = z[12] * z[21];
z[21] = z[21] + z[28] + z[31];
z[21] = z[11] * z[21];
z[21] = z[21] + z[33] + z[34];
z[21] = z[11] * z[21];
z[28] = z[7] + T(7) * z[39];
z[22] = z[22] + z[28] / T(2) + z[55];
z[22] = -z[5] / T(6) + (T(-15) * z[18]) / T(8) + z[22] / T(3);
z[22] = z[4] * z[22];
z[28] = (T(4) * z[8]) / T(3);
z[31] = z[1] + -z[9];
z[31] = z[7] / T(3) + z[10] / T(6) + (T(-11) * z[18]) / T(4) + z[28] + z[31] / T(2) + -z[41];
z[31] = z[3] * z[31];
z[33] = (T(-4) * z[7]) / T(3) + -z[10];
z[32] = z[18] / T(8) + z[32] + T(2) * z[33];
z[32] = z[12] * z[32];
z[27] = -z[5] + -z[27] + z[44];
z[27] = T(2) * z[18] + z[27] / T(3) + z[50];
z[27] = z[2] * z[27];
z[20] = (T(11) * z[7]) / T(3) + (T(-31) * z[9]) / T(6) + z[20];
z[20] = (T(2) * z[5]) / T(3) + (T(17) * z[10]) / T(12) + -z[18] / T(4) + z[20] / T(2) + -z[28];
z[20] = z[6] * z[20];
z[20] = z[20] + z[22] + z[27] + z[31] + z[32];
z[20] = z[20] * z[53];
z[22] = -(z[16] * z[23]);
z[23] = -z[29] + z[43] + z[48];
z[23] = z[4] * z[23];
z[27] = z[6] * z[37];
z[24] = -z[9] + -z[24] + z[29];
z[24] = z[3] * z[24];
z[23] = z[23] + z[24] + z[27] + -z[38];
z[23] = z[15] * z[23];
z[24] = T(27) * z[3] + (T(325) * z[4]) / T(6) + (T(-149) * z[6]) / T(3) + (T(7) * z[12]) / T(2);
z[24] = z[19] * z[24];
return z[20] + z[21] + z[22] + z[23] + z[24] / T(8) + z[25] + z[26] + z[30];
}



template IntegrandConstructorType<double> f_4_125_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_125_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_125_construct (const Kin<qd_real>&);
#endif

}