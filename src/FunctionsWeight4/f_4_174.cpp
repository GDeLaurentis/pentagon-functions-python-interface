#include "f_4_174.h"

namespace PentagonFunctions {

template <typename T> T f_4_174_abbreviated (const std::array<T,46>&);

template <typename T> class SpDLog_f_4_174_W_21 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_174_W_21 (const Kin<T>& kin) {
        c[0] = kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) + kin.v[0] + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4]);
c[1] = kin.v[2] * (T(2) + kin.v[2] + T(-2) * kin.v[4]) + kin.v[3] * (-kin.v[3] + T(2) + T(-2) * kin.v[4]) + kin.v[0] * (T(-2) + kin.v[0] + T(-2) * kin.v[2] + T(2) * kin.v[4]) + kin.v[1] * (-kin.v[1] + T(-2) + T(2) * kin.v[3] + T(2) * kin.v[4]);
c[2] = kin.v[0] * (-kin.v[0] + T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4]);
c[3] = kin.v[0] * (-kin.v[0] + T(2) + T(2) * kin.v[2] + T(-2) * kin.v[4]) + kin.v[1] * (T(2) + kin.v[1] + T(-2) * kin.v[3] + T(-2) * kin.v[4]) + kin.v[2] * (-kin.v[2] + T(-2) + T(2) * kin.v[4]) + kin.v[3] * (T(-2) + kin.v[3] + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * (abb[3] * c[0] + abb[4] * c[1] + abb[5] * c[2] + abb[6] * c[3]);
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] + prod_pow(abb[1], 2) * (abb[5] + abb[6] + -abb[3] + -abb[4]) + prod_pow(abb[2], 2) * (abb[4] + -abb[5] + -abb[6]));
    }
};
template <typename T> class SpDLog_f_4_174_W_22 {
    using TC = std::complex<T>;
    std::array<TC,8> c;

public:
    SpDLog_f_4_174_W_22 (const Kin<T>& kin) {
        c[0] = kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (-kin.v[4] / T(2) + kin.v[3] / T(4) + T(1)) * kin.v[3] + (-kin.v[2] / T(2) + -kin.v[3] / T(2) + kin.v[1] / T(4) + kin.v[4] / T(2) + T(-1) + kin.v[0]) * kin.v[1] + ((T(-3) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + ((T(-3) * kin.v[2]) / T(4) + kin.v[3] / T(2) + (T(3) * kin.v[4]) / T(2) + T(-1)) * kin.v[2];
c[1] = kin.v[1] + kin.v[2] + -kin.v[3] + -kin.v[4];
c[2] = kin.v[0] * (-kin.v[3] + -kin.v[4] + kin.v[2]) + (-kin.v[4] / T(2) + kin.v[3] / T(4) + T(1)) * kin.v[3] + (-kin.v[2] / T(2) + -kin.v[3] / T(2) + kin.v[1] / T(4) + kin.v[4] / T(2) + T(-1) + kin.v[0]) * kin.v[1] + ((T(-3) * kin.v[4]) / T(4) + T(1)) * kin.v[4] + ((T(-3) * kin.v[2]) / T(4) + kin.v[3] / T(2) + (T(3) * kin.v[4]) / T(2) + T(-1)) * kin.v[2];
c[3] = kin.v[1] + kin.v[2] + -kin.v[3] + -kin.v[4];
c[4] = (-kin.v[3] / T(2) + (T(3) * kin.v[2]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(1)) * kin.v[2] + (-kin.v[1] / T(4) + -kin.v[4] / T(2) + kin.v[2] / T(2) + kin.v[3] / T(2) + -kin.v[0] + T(1)) * kin.v[1] + (-kin.v[3] / T(4) + kin.v[4] / T(2) + T(-1)) * kin.v[3] + ((T(3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]);
c[5] = -kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4];
c[6] = (-kin.v[3] / T(2) + (T(3) * kin.v[2]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(1)) * kin.v[2] + (-kin.v[1] / T(4) + -kin.v[4] / T(2) + kin.v[2] / T(2) + kin.v[3] / T(2) + -kin.v[0] + T(1)) * kin.v[1] + (-kin.v[3] / T(4) + kin.v[4] / T(2) + T(-1)) * kin.v[3] + ((T(3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4] + kin.v[0] * (-kin.v[2] + kin.v[3] + kin.v[4]);
c[7] = -kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[4] * (t * c[0] + c[1]) + abb[5] * (t * c[2] + c[3]) + abb[10] * (t * c[4] + c[5]) + abb[6] * (t * c[6] + c[7]);
        }

        return abb[7] * (prod_pow(abb[2], 2) * (abb[6] + -abb[4] + -abb[5]) + abb[9] * (abb[4] + abb[5] + -abb[6]) + abb[10] * (prod_pow(abb[2], 2) + -abb[9]) + prod_pow(abb[8], 2) * (abb[4] + abb[5] + -abb[6] + -abb[10]));
    }
};
template <typename T> class SpDLog_f_4_174_W_7 {
    using TC = std::complex<T>;
    std::array<TC,6> c;

public:
    SpDLog_f_4_174_W_7 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (T(4) + T(4) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(1) + kin.v[1]) + kin.v[2] + (T(-7) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[3] + T(-6) * kin.v[4]) + T(4) * kin.v[1] * kin.v[4] + kin.v[2] * kin.v[4] + ((T(-5) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4]);
c[1] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4];
c[2] = kin.v[3] * (T(4) + T(4) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(1) + kin.v[1]) + kin.v[2] + (T(-7) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[3] + T(-6) * kin.v[4]) + T(4) * kin.v[1] * kin.v[4] + kin.v[2] * kin.v[4] + ((T(-5) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4]);
c[3] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4];
c[4] = kin.v[3] * (T(4) + T(4) * kin.v[1] + bc<T>[0] * int_to_imaginary<T>(1) * (-kin.v[4] + T(1) + kin.v[1]) + kin.v[2] + (T(-7) / T(2) + bc<T>[0] * (int_to_imaginary<T>(-1) / T(2))) * kin.v[3] + T(-6) * kin.v[4]) + T(4) * kin.v[1] * kin.v[4] + kin.v[2] * kin.v[4] + ((T(-5) * kin.v[4]) / T(2) + T(4)) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4]);
c[5] = (bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[3] + T(2) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4];

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return abb[3] * (t * c[0] + c[1]) + abb[4] * (t * c[2] + c[3]) + abb[5] * (t * c[4] + c[5]);
        }

        return abb[16] * (abb[1] * (abb[4] + abb[5]) * abb[8] + abb[15] * (-(abb[1] * abb[3]) + abb[1] * (-abb[4] + -abb[5])) + abb[17] * (abb[4] * T(-2) + abb[5] * T(-2)) + abb[14] * (abb[1] * (abb[4] + abb[5]) + abb[2] * (abb[4] + abb[5]) + (abb[3] + abb[4] + abb[5]) * abb[15] + abb[8] * (-abb[4] + -abb[5]) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[3] * (abb[1] + abb[2] + -abb[8] + bc<T>[0] * int_to_imaginary<T>(-1))) + abb[1] * (abb[1] * (-abb[4] + -abb[5]) + abb[2] * (-abb[4] + -abb[5]) + abb[4] * bc<T>[0] * int_to_imaginary<T>(1) + abb[5] * bc<T>[0] * int_to_imaginary<T>(1)) + abb[3] * (abb[1] * abb[8] + abb[17] * T(-2) + abb[1] * (-abb[1] + -abb[2] + bc<T>[0] * int_to_imaginary<T>(1))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_174_construct (const Kin<T>& kin) {
    return [&kin, 
            dl21 = DLog_W_21<T>(kin),dl22 = DLog_W_22<T>(kin),dl3 = DLog_W_3<T>(kin),dl14 = DLog_W_14<T>(kin),dl7 = DLog_W_7<T>(kin),dl17 = DLog_W_17<T>(kin),dl1 = DLog_W_1<T>(kin),dl9 = DLog_W_9<T>(kin),dl2 = DLog_W_2<T>(kin),dl4 = DLog_W_4<T>(kin),dl20 = DLog_W_20<T>(kin),dl19 = DLog_W_19<T>(kin),dl16 = DLog_W_16<T>(kin),dl18 = DLog_W_18<T>(kin),dl5 = DLog_W_5<T>(kin),dl31 = DLog_W_31<T>(kin),dl27 = DLog_W_27<T>(kin),dl29 = DLog_W_29<T>(kin),dl28 = DLog_W_28<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),spdl21 = SpDLog_f_4_174_W_21<T>(kin),spdl22 = SpDLog_f_4_174_W_22<T>(kin),spdl7 = SpDLog_f_4_174_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,46> abbr = 
            {dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), f_1_3_5(kin) - f_1_3_5(kin_path), dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), f_2_1_14(kin_path), f_1_3_1(kin) - f_1_3_1(kin_path), dl3(t), f_2_1_15(kin_path), dl14(t), rlog(-v_path[1]), rlog(v_path[3]), dl7(t), f_2_1_11(kin_path), dl17(t), f_2_1_6(kin_path), rlog(v_path[0] + v_path[1]), dl1(t), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl9(t), dl2(t), dl4(t), dl20(t), dl19(t), dl16(t), dl18(t), dl5(t), dl31(t), dl27(t) / kin_path.SqrtDelta, rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[18] / kin_path.W[18]), rlog(kin.W[19] / kin_path.W[19]), rlog(kin.W[15] / kin_path.W[15]), rlog(kin.W[30] / kin_path.W[30]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[17] / kin_path.W[17]), dl29(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl26(t) / kin_path.SqrtDelta, dl30(t) / kin_path.SqrtDelta}
;

        auto result = f_4_174_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_174_abbreviated(const std::array<T,46>& abb)
{
using TR = typename T::value_type;
T z[112];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[11];
z[3] = abb[25];
z[4] = abb[27];
z[5] = abb[28];
z[6] = abb[29];
z[7] = abb[30];
z[8] = abb[4];
z[9] = abb[5];
z[10] = abb[26];
z[11] = abb[6];
z[12] = abb[33];
z[13] = abb[34];
z[14] = abb[35];
z[15] = abb[36];
z[16] = abb[37];
z[17] = abb[38];
z[18] = abb[39];
z[19] = abb[40];
z[20] = abb[41];
z[21] = abb[42];
z[22] = abb[43];
z[23] = abb[45];
z[24] = abb[2];
z[25] = abb[8];
z[26] = abb[14];
z[27] = abb[31];
z[28] = abb[15];
z[29] = bc<TR>[0];
z[30] = abb[10];
z[31] = abb[18];
z[32] = abb[44];
z[33] = abb[21];
z[34] = abb[9];
z[35] = abb[12];
z[36] = abb[17];
z[37] = abb[19];
z[38] = abb[20];
z[39] = abb[22];
z[40] = abb[23];
z[41] = abb[13];
z[42] = abb[24];
z[43] = bc<TR>[3];
z[44] = abb[32];
z[45] = bc<TR>[1];
z[46] = bc<TR>[2];
z[47] = bc<TR>[4];
z[48] = bc<TR>[9];
z[49] = z[11] * z[31];
z[50] = z[1] + z[8];
z[51] = -z[11] + z[50];
z[52] = z[3] * z[51];
z[53] = z[8] / T(2) + z[9];
z[53] = z[6] * z[53];
z[54] = z[8] + z[9];
z[55] = -z[1] + z[54];
z[56] = -z[30] + z[55] / T(2);
z[57] = z[10] * z[56];
z[53] = z[49] + z[52] + z[53] + z[57];
z[57] = z[9] / T(3);
z[58] = z[1] / T(3);
z[59] = -z[8] + z[57] + -z[58];
z[59] = z[31] * z[59];
z[60] = -z[8] / T(3) + -z[9] + -z[58];
z[60] = z[2] * z[60];
z[59] = z[59] + z[60];
z[60] = z[13] + z[14] + z[15] + z[16];
z[61] = z[19] + z[20];
z[62] = z[17] / T(3) + z[60] / T(2) + -z[61] / T(6);
z[63] = (T(2) * z[18]) / T(3) + -z[62];
z[64] = z[22] + z[23];
z[63] = z[63] * z[64];
z[62] = z[18] / T(3) + -z[62] / T(2);
z[64] = -z[12] + z[32];
z[62] = z[62] * z[64];
z[60] = z[17] + (T(3) * z[60]) / T(2) + -z[61] / T(2);
z[61] = -z[18] + z[60] / T(2);
z[64] = z[21] * z[61];
z[58] = z[54] + z[58];
z[58] = z[33] * z[58];
z[65] = z[11] / T(3);
z[66] = (T(5) * z[1]) / T(3) + (T(-7) * z[8]) / T(3) + -z[9];
z[66] = z[30] + z[65] + z[66] / T(4);
z[66] = z[4] * z[66];
z[57] = -z[50] + z[57];
z[57] = z[57] / T(4) + z[65];
z[57] = z[7] * z[57];
z[65] = z[21] + z[32];
z[67] = z[23] / T(4);
z[68] = -z[22] + (T(-5) * z[65]) / T(4) + -z[67];
z[68] = z[46] * z[68];
z[69] = z[5] + z[10];
z[70] = -z[6] + T(2) * z[44];
z[70] = T(2) * z[70];
z[71] = z[27] + z[70];
z[71] = -z[3] + -z[69] + z[71] / T(3);
z[67] = -z[67] + z[71] / T(3);
z[71] = z[12] + z[32];
z[67] = -z[4] / T(9) + z[7] / T(27) + (T(5) * z[21]) / T(12) + z[22] / T(2) + z[67] / T(3) + z[71] / T(12);
z[71] = int_to_imaginary<T>(1) * z[29];
z[67] = z[67] * z[71];
z[72] = -z[8] + z[11];
z[73] = z[41] * z[72];
z[74] = z[9] + z[11];
z[75] = z[30] + z[74];
z[75] = z[42] * z[75];
z[76] = z[6] + z[31];
z[76] = z[30] * z[76];
z[77] = T(5) * z[9] + z[50];
z[78] = -z[11] + -z[77] / T(6);
z[78] = z[5] * z[78];
z[79] = z[21] + z[22];
z[80] = z[12] / T(3) + z[79];
z[80] = z[45] * z[80];
z[53] = z[53] / T(3) + z[57] + z[58] + z[59] / T(4) + z[62] + z[63] + -z[64] + z[66] + z[67] + z[68] + z[73] + (T(-4) * z[75]) / T(3) + z[76] / T(6) + z[78] + z[80];
z[53] = z[29] * z[53];
z[57] = T(-2) * z[18] + z[60];
z[58] = z[32] * z[57];
z[59] = T(2) * z[11];
z[60] = z[31] * z[59];
z[60] = z[60] + z[76];
z[58] = z[58] + -z[60];
z[62] = z[4] + z[10];
z[63] = z[30] + z[72];
z[66] = T(2) * z[63];
z[66] = z[62] * z[66];
z[67] = z[6] * z[55];
z[68] = T(3) * z[8];
z[72] = -z[9] + z[68];
z[78] = z[1] + z[72];
z[80] = z[31] * z[78];
z[67] = z[67] + z[80];
z[81] = z[23] * z[57];
z[82] = z[1] + z[54];
z[83] = z[82] / T(2);
z[84] = z[3] * z[83];
z[85] = z[81] + z[84];
z[86] = z[22] * z[57];
z[87] = z[5] * z[83];
z[87] = z[86] + z[87];
z[66] = z[58] + z[66] + z[67] / T(2) + z[85] + -z[87];
z[66] = z[24] * z[66];
z[67] = T(2) * z[74];
z[67] = z[67] * z[69];
z[88] = z[21] * z[57];
z[89] = z[4] * z[83];
z[89] = z[88] + z[89];
z[90] = T(3) * z[9];
z[91] = -z[50] + z[90];
z[92] = z[59] + z[91] / T(2);
z[93] = z[27] * z[92];
z[94] = z[6] * z[83];
z[67] = z[67] + z[85] + z[89] + -z[93] + -z[94];
z[85] = z[0] * z[67];
z[95] = z[66] + -z[85];
z[96] = z[5] * z[74];
z[97] = z[6] * z[54];
z[98] = z[3] * z[54];
z[99] = z[4] * z[63];
z[99] = z[73] + -z[75] + -z[96] + z[97] + z[98] + z[99];
z[99] = z[28] * z[99];
z[92] = z[69] * z[92];
z[100] = T(3) * z[54];
z[101] = z[1] + z[100];
z[102] = z[6] * z[101];
z[103] = z[33] * z[101];
z[102] = z[102] + -z[103];
z[100] = -z[1] + z[100];
z[104] = z[3] / T(2);
z[105] = z[100] * z[104];
z[92] = -z[92] + z[93] + z[102] / T(2) + z[105];
z[102] = z[81] + -z[86] + -z[92];
z[102] = z[40] * z[102];
z[105] = z[80] + -z[103];
z[100] = z[6] * z[100];
z[100] = z[100] + z[105];
z[101] = z[101] * z[104];
z[68] = z[1] + -z[9] + -z[68];
z[68] = z[11] + z[68] / T(2);
z[106] = T(2) * z[30] + z[68];
z[107] = z[10] * z[106];
z[100] = z[100] / T(2) + z[101] + z[107];
z[58] = z[58] + z[81];
z[101] = z[75] + z[88];
z[78] = -z[30] + -z[59] + z[78] / T(2);
z[107] = z[4] * z[78];
z[107] = -z[58] + -z[100] + -z[101] + z[107];
z[107] = z[38] * z[107];
z[97] = -z[87] + -z[97] + z[103] / T(2);
z[83] = z[10] * z[83];
z[89] = z[83] + z[89];
z[108] = z[89] + -z[97] + T(2) * z[98];
z[108] = z[26] * z[108];
z[109] = prod_pow(z[45], 2);
z[109] = -z[47] + -z[109] / T(2);
z[79] = z[12] + T(3) * z[79];
z[109] = z[79] * z[109];
z[79] = z[45] * z[79];
z[65] = -z[23] + -z[65];
z[65] = z[46] * z[65];
z[65] = z[65] / T(2) + z[79];
z[65] = z[46] * z[65];
z[65] = z[65] + z[95] + T(2) * z[99] + z[102] + z[107] + z[108] + z[109];
z[65] = int_to_imaginary<T>(1) * z[65];
z[62] = z[3] + z[5] + z[62];
z[62] = -z[7] + -z[27] + T(3) * z[62] + -z[70];
z[62] = z[43] * z[62];
z[53] = z[53] + z[62] + z[65];
z[53] = z[29] * z[53];
z[62] = -z[9] + z[50];
z[65] = -z[11] + z[62] / T(2);
z[65] = z[10] * z[65];
z[70] = z[65] + -z[75];
z[49] = z[49] + z[76] / T(2);
z[76] = z[8] + -z[9];
z[79] = z[1] + -z[76];
z[79] = z[6] * z[79];
z[79] = z[79] + -z[80];
z[80] = z[50] + z[90];
z[90] = z[2] * z[80];
z[99] = z[90] / T(4);
z[102] = z[22] + -z[32];
z[102] = z[61] * z[102];
z[107] = z[23] * z[61];
z[104] = z[54] * z[104];
z[56] = z[4] * z[56];
z[109] = z[56] / T(2);
z[110] = z[82] / T(4);
z[111] = z[5] * z[110];
z[70] = z[49] + z[64] + z[70] / T(2) + z[79] / T(4) + -z[99] + z[102] + z[104] + -z[107] + z[109] + z[111];
z[70] = z[25] * z[70];
z[56] = -z[56] + -z[65] + z[97] + -z[98] + -z[101];
z[65] = z[28] + -z[71];
z[56] = z[56] * z[65];
z[65] = z[90] / T(2);
z[71] = T(-2) * z[5] + -z[6];
z[71] = z[9] * z[71];
z[71] = z[65] + z[71] + -z[84] + -z[89];
z[71] = z[0] * z[71];
z[56] = z[56] + z[66] + z[70] + z[71] + z[108];
z[56] = z[25] * z[56];
z[66] = z[6] * z[82];
z[55] = z[3] * z[55];
z[55] = -z[55] + -z[66] + z[105];
z[64] = z[64] + z[107];
z[70] = z[64] + z[96];
z[71] = z[32] * z[61];
z[68] = z[30] + z[68] / T(2);
z[68] = z[10] * z[68];
z[49] = -z[49] + z[55] / T(4) + z[68] + z[70] + z[71] + -z[73] + (T(3) * z[75]) / T(2) + z[109];
z[49] = z[28] * z[49];
z[55] = -(z[26] * z[67]);
z[49] = z[49] + z[55] + -z[95];
z[49] = z[28] * z[49];
z[55] = z[86] + z[88];
z[67] = z[5] / T(2);
z[68] = z[67] * z[80];
z[71] = z[10] * z[78];
z[84] = -(z[4] * z[106]);
z[58] = z[55] + -z[58] + -z[65] + z[68] + z[71] + z[79] / T(2) + z[84];
z[58] = z[34] * z[58];
z[51] = T(2) * z[51];
z[51] = z[4] * z[51];
z[51] = -z[51] + T(-2) * z[52] + z[81] + -z[83] + -z[87] + z[94];
z[68] = -(z[24] * z[51]);
z[71] = z[5] * z[91];
z[62] = z[6] * z[62];
z[79] = z[3] * z[82];
z[62] = z[62] + z[71] + z[79];
z[71] = z[10] * z[74];
z[74] = z[4] * z[110];
z[71] = z[71] + z[74];
z[74] = -(z[22] * z[61]);
z[62] = z[62] / T(4) + z[64] + z[71] + z[74] + -z[99];
z[62] = z[0] * z[62];
z[62] = z[62] + z[68];
z[62] = z[0] * z[62];
z[64] = T(3) * z[1];
z[54] = -z[54] + -z[64];
z[54] = z[6] * z[54];
z[54] = z[54] + z[103];
z[68] = z[64] + z[76];
z[68] = -z[11] + z[68] / T(2);
z[68] = z[3] * z[68];
z[74] = z[12] * z[61];
z[54] = z[54] / T(4) + z[68] + z[70] + z[71] + z[73] + z[74] + -z[93];
z[54] = z[26] * z[54];
z[68] = -(z[12] * z[57]);
z[51] = z[51] + z[68];
z[51] = z[24] * z[51];
z[51] = z[51] + z[54] + -z[85];
z[51] = z[26] * z[51];
z[54] = z[6] * z[80];
z[64] = z[64] + z[72];
z[68] = -z[59] + z[64] / T(2);
z[70] = -(z[3] * z[68]);
z[67] = z[67] * z[91];
z[54] = z[54] / T(2) + -z[65] + z[67] + z[70];
z[54] = z[35] * z[54];
z[65] = -(z[39] * z[92]);
z[50] = z[9] + T(5) * z[50];
z[50] = z[50] / T(2) + -z[59];
z[67] = z[3] + z[4];
z[50] = z[50] * z[67];
z[59] = z[59] + z[77] / T(2);
z[59] = z[59] * z[69];
z[50] = z[50] + z[55] + z[59] + -z[66] + -z[93];
z[50] = z[36] * z[50];
z[55] = z[24] * z[68];
z[59] = -z[11] + z[64] / T(4);
z[64] = z[0] * z[59];
z[64] = -z[55] + z[64];
z[64] = z[0] * z[64];
z[59] = -(z[26] * z[59]);
z[55] = z[55] + z[59];
z[55] = z[26] * z[55];
z[59] = z[35] * z[68];
z[66] = -(z[36] * z[68]);
z[55] = z[55] + z[59] + z[64] + z[66];
z[55] = z[7] * z[55];
z[64] = z[24] * z[57];
z[61] = -(z[0] * z[61]);
z[61] = z[61] + z[64];
z[61] = z[0] * z[61];
z[64] = z[35] * z[57];
z[66] = z[36] * z[57];
z[61] = (T(-19) * z[48]) / T(6) + z[61] + -z[64] + z[66];
z[61] = z[12] * z[61];
z[60] = -z[60] + z[75] + z[100];
z[60] = z[37] * z[60];
z[66] = z[39] * z[57];
z[66] = z[64] + z[66];
z[57] = z[37] * z[57];
z[67] = (T(5) * z[48]) / T(8) + z[57] + z[66];
z[67] = z[23] * z[67];
z[66] = z[48] / T(2) + -z[66];
z[66] = z[22] * z[66];
z[64] = (T(9) * z[48]) / T(8) + z[57] + z[64];
z[64] = z[21] * z[64];
z[68] = -(z[37] * z[78]);
z[59] = -z[59] + z[68];
z[59] = z[4] * z[59];
z[68] = z[5] + z[6];
z[68] = z[9] * z[68];
z[63] = -(z[10] * z[63]);
z[52] = -z[52] + z[63] + z[68];
z[52] = prod_pow(z[24], 2) * z[52];
z[57] = z[48] / T(8) + z[57];
z[57] = z[32] * z[57];
return z[49] + z[50] + z[51] + z[52] + z[53] + z[54] + z[55] + z[56] + z[57] + z[58] + z[59] + z[60] + z[61] + z[62] + z[64] + z[65] + z[66] + z[67];
}



template IntegrandConstructorType<double> f_4_174_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_174_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_174_construct (const Kin<qd_real>&);
#endif

}