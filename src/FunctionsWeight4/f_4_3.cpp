#include "f_4_3.h"

namespace PentagonFunctions {

template <typename T> T f_4_3_abbreviated (const std::array<T,17>&);

template <typename T> class SpDLog_f_4_3_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_3_W_12 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[1]) * ((kin.v[1] / T(4) + kin.v[4] / T(2) + T(1)) * kin.v[1] + ((T(-3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4]) + rlog(kin.v[3]) * ((kin.v[1] / T(4) + kin.v[4] / T(2) + T(1)) * kin.v[1] + ((T(-3) * kin.v[4]) / T(4) + T(-1)) * kin.v[4]) + rlog(-kin.v[4]) * ((-kin.v[1] / T(4) + -kin.v[4] / T(2) + T(-1)) * kin.v[1] + ((T(3) * kin.v[4]) / T(4) + T(1)) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((-kin.v[1] / T(4) + -kin.v[4] / T(2) + T(-1)) * kin.v[1] + ((T(3) * kin.v[4]) / T(4) + T(1)) * kin.v[4]);
c[1] = rlog(-kin.v[4]) * (-kin.v[4] + kin.v[1]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (-kin.v[4] + kin.v[1]) + rlog(-kin.v[1]) * (-kin.v[1] + kin.v[4]) + rlog(kin.v[3]) * (-kin.v[1] + kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (abb[4] * (abb[3] + -prod_pow(abb[2], 2)) + prod_pow(abb[2], 2) * (abb[6] + abb[7] + -abb[5]) + abb[3] * (abb[5] + -abb[6] + -abb[7]) + prod_pow(abb[1], 2) * (abb[4] + abb[5] + -abb[6] + -abb[7]));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_3_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl14 = DLog_W_14<T>(kin),dl5 = DLog_W_5<T>(kin),dl19 = DLog_W_19<T>(kin),dl4 = DLog_W_4<T>(kin),dl2 = DLog_W_2<T>(kin),spdl12 = SpDLog_f_4_3_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,17> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), f_2_1_4(kin_path), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[18] / kin_path.W[18]), dl14(t), rlog(v_path[3]), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), dl5(t), -rlog(t), dl19(t), dl4(t), dl2(t)}
;

        auto result = f_4_3_abbreviated(abbr);
        result = result + spdl12(t, abbr);

        return result;
    };
}

template <typename T> T f_4_3_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[41];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[8];
z[3] = abb[12];
z[4] = abb[14];
z[5] = abb[15];
z[6] = abb[16];
z[7] = abb[5];
z[8] = abb[6];
z[9] = abb[7];
z[10] = abb[13];
z[11] = abb[2];
z[12] = bc<TR>[0];
z[13] = abb[9];
z[14] = abb[3];
z[15] = abb[10];
z[16] = abb[11];
z[17] = bc<TR>[2];
z[18] = bc<TR>[9];
z[19] = z[3] + -z[4];
z[20] = z[7] + z[8] + T(-2) * z[10];
z[21] = T(2) * z[20];
z[22] = T(3) * z[1] + -z[9] + z[21];
z[19] = z[19] * z[22];
z[23] = z[9] + z[20];
z[24] = T(2) * z[5];
z[25] = z[23] * z[24];
z[26] = -z[7] + z[8];
z[27] = z[1] + -z[9];
z[28] = z[26] + z[27];
z[28] = z[2] * z[28];
z[29] = z[1] + z[9];
z[30] = T(-4) * z[10] + z[29];
z[31] = -z[7] + T(-3) * z[8] + -z[30];
z[31] = z[6] * z[31];
z[19] = -z[19] + z[25] + -z[28] + z[31];
z[25] = -(z[16] * z[19]);
z[21] = z[21] + z[29];
z[29] = z[6] * z[21];
z[31] = z[4] * z[22];
z[29] = -z[29] + z[31];
z[31] = z[24] * z[27];
z[32] = z[3] * z[27];
z[31] = z[29] + -z[31] + T(-2) * z[32];
z[33] = z[11] * z[31];
z[26] = z[6] * z[26];
z[34] = -z[26] + z[28] + -z[32];
z[34] = z[13] * z[34];
z[35] = -z[3] + z[5];
z[36] = -z[4] + z[6];
z[37] = z[35] + -z[36];
z[38] = prod_pow(z[17], 2) * z[37];
z[25] = z[25] + z[33] + T(2) * z[34] + -z[38] / T(2);
z[25] = int_to_imaginary<T>(1) * z[25];
z[38] = T(-5) * z[26] + T(7) * z[28];
z[39] = (T(-5) * z[1]) / T(2) + (T(7) * z[9]) / T(2) + z[20];
z[39] = z[3] * z[39];
z[20] = z[1] + z[20];
z[40] = -(z[5] * z[20]);
z[38] = z[38] / T(2) + z[39] + z[40];
z[37] = z[17] * z[37];
z[39] = z[35] + -z[36] / T(2);
z[40] = int_to_imaginary<T>(1) * z[12];
z[39] = z[39] * z[40];
z[37] = (T(-5) * z[37]) / T(4) + z[38] / T(3) + z[39] / T(6);
z[37] = z[12] * z[37];
z[25] = z[25] + z[37];
z[25] = z[12] * z[25];
z[21] = -(z[3] * z[21]);
z[20] = z[20] * z[24];
z[20] = z[20] + z[21] + -z[26] + -z[28];
z[20] = z[20] * z[40];
z[21] = z[5] * z[27];
z[24] = z[21] + z[28] + -z[29] + z[32];
z[24] = z[0] * z[24];
z[27] = z[11] + z[13];
z[27] = z[27] * z[31];
z[20] = z[20] + z[24] + z[27];
z[20] = z[0] * z[20];
z[19] = -(z[15] * z[19]);
z[24] = -z[33] + -z[34];
z[24] = z[13] * z[24];
z[27] = -z[4] + z[5];
z[22] = z[22] * z[27];
z[27] = T(2) * z[3];
z[23] = -(z[23] * z[27]);
z[27] = T(3) * z[7] + z[8] + z[30];
z[27] = z[6] * z[27];
z[22] = z[22] + z[23] + z[27];
z[22] = z[14] * z[22];
z[21] = z[21] + -z[26];
z[21] = prod_pow(z[11], 2) * z[21];
z[23] = T(21) * z[35] + -z[36];
z[23] = z[18] * z[23];
return z[19] + z[20] + z[21] + z[22] + z[23] / T(8) + z[24] + z[25];
}



template IntegrandConstructorType<double> f_4_3_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_3_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_3_construct (const Kin<qd_real>&);
#endif

}