#include "f_4_178.h"

namespace PentagonFunctions {

template <typename T> T f_4_178_abbreviated (const std::array<T,15>&);

template <typename T> class SpDLog_f_4_178_W_21 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_178_W_21 (const Kin<T>& kin) {
        c[0] = rlog(kin.v[2]) * (kin.v[0] * (T(6) + T(-3) * kin.v[0] + T(6) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(6) + T(3) * kin.v[1] + T(-6) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[2] * (T(-6) + T(-3) * kin.v[2] + T(6) * kin.v[4]) + kin.v[3] * (T(-6) + T(3) * kin.v[3] + T(6) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (kin.v[2] * (T(6) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[3] * (T(6) + T(-3) * kin.v[3] + T(-6) * kin.v[4]) + kin.v[0] * (T(-6) + T(3) * kin.v[0] + T(-6) * kin.v[2] + T(6) * kin.v[4]) + kin.v[1] * (T(-6) + T(-3) * kin.v[1] + T(6) * kin.v[3] + T(6) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-3) + prod_pow(abb[2], 2) * abb[4] * T(3) + prod_pow(abb[1], 2) * (abb[4] * T(-3) + abb[3] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_178_W_22 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_178_W_22 (const Kin<T>& kin) {
        c[0] = (T(3) / T(2) + (T(9) * kin.v[2]) / T(8) + (T(-3) * kin.v[3]) / T(4) + (T(-9) * kin.v[4]) / T(4)) * kin.v[2] + (T(3) / T(2) + (T(-3) * kin.v[0]) / T(2) + (T(-3) * kin.v[1]) / T(8) + (T(3) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(4)) * kin.v[1] + (T(-3) / T(2) + (T(-3) * kin.v[3]) / T(8) + (T(3) * kin.v[4]) / T(4)) * kin.v[3] + (T(-3) / T(2) + (T(9) * kin.v[4]) / T(8)) * kin.v[4] + ((T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2)) * kin.v[0];
c[1] = kin.v[2] * (T(3) / T(2) + (T(-9) * kin.v[3]) / T(2) + T(3) * kin.v[2] + T(-6) * kin.v[4]) + kin.v[1] * (T(3) / T(2) + (T(-3) * kin.v[0]) / T(2) + (T(3) * kin.v[1]) / T(2) + (T(9) * kin.v[2]) / T(2) + (T(-9) * kin.v[4]) / T(2) + T(-3) * kin.v[3]) + ((T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2)) * kin.v[0] + kin.v[4] * (T(-3) / T(2) + T(3) * kin.v[4]) + (T(-3) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(9) * kin.v[4]) / T(2)) * kin.v[3] + rlog(kin.v[2]) * (((T(21) * kin.v[1]) / T(4) + (T(3) * kin.v[2]) / T(2) + (T(-21) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2) + T(-9) + T(9) * kin.v[0]) * kin.v[1] + ((T(-15) * kin.v[2]) / T(4) + (T(-3) * kin.v[3]) / T(2) + (T(15) * kin.v[4]) / T(2) + T(-9)) * kin.v[2] + ((T(21) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(2) + T(9)) * kin.v[3] + ((T(-15) * kin.v[4]) / T(4) + T(9)) * kin.v[4] + kin.v[0] * (T(9) * kin.v[2] + T(-9) * kin.v[3] + T(-9) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (((T(-21) * kin.v[1]) / T(4) + (T(-3) * kin.v[2]) / T(2) + (T(21) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2) + T(9) + T(-9) * kin.v[0]) * kin.v[1] + ((T(15) * kin.v[2]) / T(4) + (T(3) * kin.v[3]) / T(2) + (T(-15) * kin.v[4]) / T(2) + T(9)) * kin.v[2] + ((T(-21) * kin.v[3]) / T(4) + (T(-3) * kin.v[4]) / T(2) + T(-9)) * kin.v[3] + ((T(15) * kin.v[4]) / T(4) + T(-9)) * kin.v[4] + kin.v[0] * (T(-9) * kin.v[2] + T(9) * kin.v[3] + T(9) * kin.v[4])) + rlog(-kin.v[3] + kin.v[0] + kin.v[1]) * (((T(-21) * kin.v[2]) / T(2) + (T(21) * kin.v[3]) / T(2) + (T(21) * kin.v[4]) / T(2)) * kin.v[0] + (T(21) / T(2) + (T(-21) * kin.v[0]) / T(2) + (T(-45) * kin.v[1]) / T(8) + (T(-3) * kin.v[2]) / T(4) + (T(45) * kin.v[3]) / T(4) + (T(3) * kin.v[4]) / T(4)) * kin.v[1] + (T(21) / T(2) + (T(39) * kin.v[2]) / T(8) + (T(3) * kin.v[3]) / T(4) + (T(-39) * kin.v[4]) / T(4)) * kin.v[2] + (T(-21) / T(2) + (T(-45) * kin.v[3]) / T(8) + (T(-3) * kin.v[4]) / T(4)) * kin.v[3] + (T(-21) / T(2) + (T(39) * kin.v[4]) / T(8)) * kin.v[4]);
c[2] = (T(-3) * kin.v[1]) / T(2) + (T(-3) * kin.v[2]) / T(2) + (T(3) * kin.v[3]) / T(2) + (T(3) * kin.v[4]) / T(2);
c[3] = rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (T(3) * kin.v[1] + T(3) * kin.v[2] + T(-3) * kin.v[3] + T(-3) * kin.v[4]) + ((T(3) * kin.v[1]) / T(2) + (T(3) * kin.v[2]) / T(2) + (T(-3) * kin.v[3]) / T(2) + (T(-3) * kin.v[4]) / T(2)) * rlog(-kin.v[3] + kin.v[0] + kin.v[1]) + rlog(kin.v[2]) * (T(-3) * kin.v[1] + T(-3) * kin.v[2] + T(3) * kin.v[3] + T(3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[5] * (abb[3] * (prod_pow(abb[2], 2) * T(-3) + abb[7] * T(-3)) + prod_pow(abb[6], 2) * ((abb[4] * T(-9)) / T(2) + (abb[9] * T(3)) / T(2) + abb[8] * T(-3) + abb[3] * T(3)) + abb[7] * ((abb[4] * T(3)) / T(2) + (abb[9] * T(3)) / T(2) + abb[8] * T(3)) + prod_pow(abb[2], 2) * ((abb[9] * T(-3)) / T(2) + (abb[4] * T(9)) / T(2) + abb[8] * T(3)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_178_construct (const Kin<T>& kin) {
    return [&kin, 
            dl21 = DLog_W_21<T>(kin),dl22 = DLog_W_22<T>(kin),dl20 = DLog_W_20<T>(kin),dl3 = DLog_W_3<T>(kin),dl19 = DLog_W_19<T>(kin),dl16 = DLog_W_16<T>(kin),spdl21 = SpDLog_f_4_178_W_21<T>(kin),spdl22 = SpDLog_f_4_178_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[20] = t * (-kin.v[0] + -kin.v[1] + kin.v[2] + kin.v[3]);
kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);

        std::array<std::complex<T>,15> abbr = 
            {dl21(t), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(-v_path[2] + v_path[0] + v_path[4]), rlog(kin.W[2] / kin_path.W[2]), rlog(kin.W[15] / kin_path.W[15]), dl22(t), rlog(-v_path[3] + v_path[0] + v_path[1]), f_2_1_14(kin_path), rlog(kin.W[18] / kin_path.W[18]), -rlog(t), dl20(t), dl3(t), f_2_1_15(kin_path), dl19(t), dl16(t)}
;

        auto result = f_4_178_abbreviated(abbr);
        result = result + spdl21(t, abbr) + spdl22(t, abbr);

        return result;
    };
}

template <typename T> T f_4_178_abbreviated(const std::array<T,15>& abb)
{
using TR = typename T::value_type;
T z[26];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[11];
z[3] = abb[13];
z[4] = abb[14];
z[5] = abb[4];
z[6] = abb[8];
z[7] = abb[6];
z[8] = abb[2];
z[9] = abb[10];
z[10] = abb[9];
z[11] = abb[7];
z[12] = abb[12];
z[13] = bc<TR>[0];
z[14] = bc<TR>[9];
z[15] = z[5] + -z[10];
z[15] = z[9] * z[15];
z[16] = z[1] + -z[5];
z[17] = z[3] * z[16];
z[18] = T(-3) * z[5] + z[10];
z[18] = z[1] + -z[6] + z[18] / T(2);
z[18] = z[4] * z[18];
z[17] = -z[15] + z[17] + z[18];
z[17] = prod_pow(z[8], 2) * z[17];
z[18] = T(2) * z[6];
z[19] = z[16] + -z[18];
z[19] = z[3] * z[19];
z[20] = -z[6] + (T(3) * z[16]) / T(2);
z[20] = z[2] * z[20];
z[21] = -z[6] + z[16] / T(2);
z[21] = z[4] * z[21];
z[19] = z[19] + -z[20] + z[21];
z[19] = prod_pow(z[0], 2) * z[19];
z[18] = T(3) * z[16] + -z[18];
z[21] = z[2] + -z[4];
z[22] = z[18] * z[21];
z[23] = T(2) * z[3];
z[23] = z[16] * z[23];
z[22] = z[22] + -z[23];
z[23] = z[0] * z[22];
z[24] = z[1] + -z[10];
z[24] = z[4] * z[24];
z[24] = -z[20] + (T(3) * z[24]) / T(2);
z[24] = z[7] * z[24];
z[23] = z[23] + z[24];
z[23] = z[7] * z[23];
z[22] = z[12] * z[22];
z[24] = -z[2] + z[3];
z[18] = z[18] * z[24];
z[24] = T(7) * z[5] + -z[10];
z[24] = T(-3) * z[1] + z[24] / T(2);
z[25] = T(-3) * z[6] + -z[24];
z[25] = z[4] * z[25];
z[18] = z[18] + z[25];
z[18] = z[11] * z[18];
z[17] = z[17] + z[18] + z[19] + -z[22] + z[23];
z[16] = z[6] / T(2) + z[16];
z[16] = z[3] * z[16];
z[18] = -z[6] + -z[24] / T(2);
z[18] = z[4] * z[18];
z[15] = z[15] / T(4) + z[16] + z[18] + -z[20];
z[15] = prod_pow(z[13], 2) * z[15];
z[16] = -z[3] + T(-3) * z[21];
z[16] = z[14] * z[16];
return z[15] + T(4) * z[16] + T(3) * z[17];
}



template IntegrandConstructorType<double> f_4_178_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_178_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_178_construct (const Kin<qd_real>&);
#endif

}