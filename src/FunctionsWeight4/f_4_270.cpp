#include "f_4_270.h"

namespace PentagonFunctions {

template <typename T> T f_4_270_abbreviated (const std::array<T,41>&);

template <typename T> class SpDLog_f_4_270_W_12 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_270_W_12 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[1]) * (kin.v[1] * (T(6) + T(3) * kin.v[1]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4])) + rlog(kin.v[3]) * (kin.v[1] * (T(6) + T(3) * kin.v[1]) + kin.v[4] * (T(-6) + T(-3) * kin.v[4])) + rlog(-kin.v[4]) * (kin.v[1] * (T(-6) + T(-3) * kin.v[1]) + kin.v[4] * (T(6) + T(3) * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (kin.v[1] * (T(-6) + T(-3) * kin.v[1]) + kin.v[4] * (T(6) + T(3) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[3] * T(-3) + prod_pow(abb[1], 2) * (abb[5] * T(-3) + abb[6] * T(-3) + abb[3] * T(3) + abb[4] * T(3)) + prod_pow(abb[2], 2) * (abb[4] * T(-3) + abb[5] * T(3) + abb[6] * T(3)));
    }
};
template <typename T> class SpDLog_f_4_270_W_7 {
    using TC = std::complex<T>;
    std::array<TC,4> c;

public:
    SpDLog_f_4_270_W_7 (const Kin<T>& kin) {
        c[0] = kin.v[3] * (T(4) * kin.v[0] + T(-8) * kin.v[2] + T(-6) * kin.v[3] + T(-8) * kin.v[4]) + T(4) * kin.v[0] * kin.v[4] + T(-8) * kin.v[2] * kin.v[4] + T(-2) * prod_pow(kin.v[4], 2) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[1] * kin.v[4] + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[3] * (T(-4) + T(-4) * kin.v[1] + T(2) * kin.v[3] + T(4) * kin.v[4]));
c[1] = T(-8) * kin.v[2] * kin.v[4] + T(10) * prod_pow(kin.v[4], 2) + kin.v[0] * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[4] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] * kin.v[4] + T(5) * prod_pow(kin.v[4], 2)) + kin.v[3] * (T(-8) * kin.v[2] + (bc<T>[0] * int_to_imaginary<T>(3) + T(6)) * kin.v[3] + T(16) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(1) * (T(-4) * kin.v[2] + T(8) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (T(-2) * kin.v[0] * kin.v[4] + T(4) * kin.v[2] * kin.v[4] + prod_pow(kin.v[4], 2) + kin.v[3] * (T(-2) * kin.v[0] + T(4) * kin.v[2] + T(3) * kin.v[3] + T(4) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[4] + T(2)) * kin.v[4] + T(2) * kin.v[1] * kin.v[4] + kin.v[3] * (-kin.v[3] + T(2) + T(2) * kin.v[1] + T(-2) * kin.v[4]))) + rlog(-kin.v[1]) * (prod_pow(kin.v[4], 2) / T(2) + -(kin.v[0] * kin.v[4]) + T(2) * kin.v[2] * kin.v[4] + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + -kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] / T(2) + -kin.v[4] + T(1) + kin.v[1]) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4])) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * (prod_pow(kin.v[4], 2) / T(2) + -(kin.v[0] * kin.v[4]) + T(2) * kin.v[2] * kin.v[4] + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + -kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] / T(2) + -kin.v[4] + T(1) + kin.v[1]) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4])) + rlog(-kin.v[4]) * (prod_pow(kin.v[4], 2) / T(2) + -(kin.v[0] * kin.v[4]) + T(2) * kin.v[2] * kin.v[4] + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + -kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] / T(2) + -kin.v[4] + T(1) + kin.v[1]) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4])) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (prod_pow(kin.v[4], 2) / T(2) + -(kin.v[0] * kin.v[4]) + T(2) * kin.v[2] * kin.v[4] + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + -kin.v[0] + T(2) * kin.v[2] + T(2) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * ((-kin.v[3] / T(2) + -kin.v[4] + T(1) + kin.v[1]) * kin.v[3] + (-kin.v[4] / T(2) + T(1)) * kin.v[4] + kin.v[1] * kin.v[4])) + rlog(kin.v[3]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + T(3) * kin.v[0] * kin.v[4] + T(-6) * kin.v[2] * kin.v[4] + kin.v[3] * ((T(-9) * kin.v[3]) / T(2) + T(3) * kin.v[0] + T(-6) * kin.v[2] + T(-6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + T(-3) * kin.v[1] * kin.v[4] + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(-3) * kin.v[1] + T(3) * kin.v[4]))) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-3) * prod_pow(kin.v[4], 2)) / T(2) + T(3) * kin.v[0] * kin.v[4] + T(-6) * kin.v[2] * kin.v[4] + kin.v[3] * ((T(-9) * kin.v[3]) / T(2) + T(3) * kin.v[0] + T(-6) * kin.v[2] + T(-6) * kin.v[4]) + bc<T>[0] * int_to_imaginary<T>(1) * (((T(3) * kin.v[4]) / T(2) + T(-3)) * kin.v[4] + T(-3) * kin.v[1] * kin.v[4] + kin.v[3] * ((T(3) * kin.v[3]) / T(2) + T(-3) + T(-3) * kin.v[1] + T(3) * kin.v[4])));
c[2] = (T(-8) + bc<T>[0] * int_to_imaginary<T>(-4)) * kin.v[3] + T(-8) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-4) * kin.v[4];
c[3] = rlog(-kin.v[1]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] + T(2) * kin.v[4]) + rlog(-kin.v[2] + -kin.v[3] + kin.v[0]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] + T(2) * kin.v[4]) + rlog(-kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] + T(2) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * ((bc<T>[0] * int_to_imaginary<T>(1) + T(2)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(1) * kin.v[4] + T(2) * kin.v[4]) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * ((bc<T>[0] * int_to_imaginary<T>(2) + T(4)) * kin.v[3] + bc<T>[0] * int_to_imaginary<T>(2) * kin.v[4] + T(4) * kin.v[4]) + rlog(kin.v[3]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-3) * kin.v[4]) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * ((T(-6) + bc<T>[0] * int_to_imaginary<T>(-3)) * kin.v[3] + T(-6) * kin.v[4] + bc<T>[0] * int_to_imaginary<T>(-3) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return c[3] + c[2] * rlog(t) + t * (c[1] + c[0] * rlog(t));
        }

        return abb[16] * (abb[3] * (abb[8] * abb[15] + -(abb[2] * abb[15]) + -(abb[14] * abb[15]) + abb[17] * T(-2) + abb[15] * (abb[15] + bc<T>[0] * int_to_imaginary<T>(1))) + abb[2] * abb[15] * (-abb[5] + -abb[6] + -abb[11] + abb[18] * T(-4) + abb[19] * T(-2) + abb[4] * T(3) + abb[12] * T(3)) + abb[14] * abb[15] * (-abb[5] + -abb[6] + -abb[11] + abb[18] * T(-4) + abb[19] * T(-2) + abb[4] * T(3) + abb[12] * T(3)) + abb[8] * abb[15] * (abb[5] + abb[6] + abb[11] + abb[4] * T(-3) + abb[12] * T(-3) + abb[19] * T(2) + abb[18] * T(4)) + abb[15] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(1) + abb[19] * bc<T>[0] * int_to_imaginary<T>(2) + abb[4] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[12] * bc<T>[0] * int_to_imaginary<T>(-3) + abb[18] * bc<T>[0] * int_to_imaginary<T>(4) + abb[15] * (abb[5] + abb[6] + abb[11] + abb[4] * T(-3) + abb[12] * T(-3) + abb[19] * T(2) + abb[18] * T(4))) + abb[17] * (abb[18] * T(-8) + abb[19] * T(-4) + abb[5] * T(-2) + abb[6] * T(-2) + abb[11] * T(-2) + abb[4] * T(6) + abb[12] * T(6)) + abb[1] * (abb[5] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[6] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[11] * bc<T>[0] * int_to_imaginary<T>(-1) + abb[3] * (abb[2] + abb[14] + abb[15] + -abb[8] + bc<T>[0] * int_to_imaginary<T>(-1)) + abb[19] * bc<T>[0] * int_to_imaginary<T>(-2) + abb[4] * bc<T>[0] * int_to_imaginary<T>(3) + abb[12] * bc<T>[0] * int_to_imaginary<T>(3) + abb[18] * bc<T>[0] * int_to_imaginary<T>(-4) + abb[8] * (-abb[5] + -abb[6] + -abb[11] + abb[18] * T(-4) + abb[19] * T(-2) + abb[4] * T(3) + abb[12] * T(3)) + abb[2] * (abb[5] + abb[6] + abb[11] + abb[4] * T(-3) + abb[12] * T(-3) + abb[19] * T(2) + abb[18] * T(4)) + abb[14] * (abb[5] + abb[6] + abb[11] + abb[4] * T(-3) + abb[12] * T(-3) + abb[19] * T(2) + abb[18] * T(4)) + abb[15] * (abb[5] + abb[6] + abb[11] + abb[4] * T(-3) + abb[12] * T(-3) + abb[19] * T(2) + abb[18] * T(4)) + abb[1] * (abb[18] * T(-8) + abb[19] * T(-4) + abb[3] * T(-2) + abb[5] * T(-2) + abb[6] * T(-2) + abb[11] * T(-2) + abb[4] * T(6) + abb[12] * T(6))));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_270_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl3 = DLog_W_3<T>(kin),dl25 = DLog_W_25<T>(kin),dl7 = DLog_W_7<T>(kin),dl1 = DLog_W_1<T>(kin),dl17 = DLog_W_17<T>(kin),dl26 = DLog_W_26<T>(kin),dl30 = DLog_W_30<T>(kin),dl16 = DLog_W_16<T>(kin),dl29 = DLog_W_29<T>(kin),dl27 = DLog_W_27<T>(kin),dl28 = DLog_W_28<T>(kin),dl5 = DLog_W_5<T>(kin),dl2 = DLog_W_2<T>(kin),dl18 = DLog_W_18<T>(kin),dl20 = DLog_W_20<T>(kin),dl19 = DLog_W_19<T>(kin),dl4 = DLog_W_4<T>(kin),spdl12 = SpDLog_f_4_270_W_12<T>(kin),spdl7 = SpDLog_f_4_270_W_7<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,41> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[4] / kin_path.W[4]), rlog(kin.W[18] / kin_path.W[18]), dl3(t), rlog(-v_path[4] + v_path[1] + v_path[2]), f_2_1_4(kin_path), f_2_1_7(kin_path), rlog(kin.W[17] / kin_path.W[17]), rlog(kin.W[19] / kin_path.W[19]), dl25(t), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), dl7(t), f_2_1_11(kin_path), -rlog(t), rlog(kin.W[16] / kin_path.W[16]), dl1(t), f_2_1_10(kin_path), f_2_1_12(kin_path), rlog(-v_path[3] + -v_path[4] + v_path[0] + v_path[1]), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl17(t), dl26(t) / kin_path.SqrtDelta, f_1_3_2(kin) - f_1_3_2(kin_path), f_1_3_3(kin) - f_1_3_3(kin_path), f_1_3_4(kin) - f_1_3_4(kin_path), dl30(t) / kin_path.SqrtDelta, dl16(t), dl29(t) / kin_path.SqrtDelta, dl27(t) / kin_path.SqrtDelta, dl28(t) / kin_path.SqrtDelta, dl5(t), dl2(t), dl18(t), dl20(t), dl19(t), dl4(t)}
;

        auto result = f_4_270_abbreviated(abbr);
        result = result + spdl12(t, abbr) + spdl7(t, abbr);

        return result;
    };
}

template <typename T> T f_4_270_abbreviated(const std::array<T,41>& abb)
{
using TR = typename T::value_type;
T z[119];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[7];
z[3] = abb[25];
z[4] = abb[35];
z[5] = abb[36];
z[6] = abb[37];
z[7] = abb[38];
z[8] = abb[39];
z[9] = abb[40];
z[10] = abb[4];
z[11] = abb[5];
z[12] = abb[6];
z[13] = abb[11];
z[14] = abb[12];
z[15] = abb[18];
z[16] = abb[19];
z[17] = abb[26];
z[18] = abb[27];
z[19] = abb[28];
z[20] = abb[29];
z[21] = abb[32];
z[22] = abb[33];
z[23] = abb[2];
z[24] = abb[8];
z[25] = abb[14];
z[26] = abb[15];
z[27] = bc<TR>[0];
z[28] = abb[34];
z[29] = abb[31];
z[30] = abb[30];
z[31] = abb[9];
z[32] = abb[10];
z[33] = abb[20];
z[34] = abb[13];
z[35] = abb[17];
z[36] = abb[21];
z[37] = abb[22];
z[38] = abb[23];
z[39] = abb[24];
z[40] = bc<TR>[3];
z[41] = bc<TR>[1];
z[42] = bc<TR>[2];
z[43] = bc<TR>[4];
z[44] = bc<TR>[9];
z[45] = z[11] + z[12];
z[46] = -z[10] + z[45];
z[47] = -z[13] + z[14];
z[48] = z[1] + -z[46] + z[47];
z[49] = z[2] * z[48];
z[50] = z[18] + z[19] + z[20];
z[51] = z[21] * z[50];
z[52] = z[49] + z[51];
z[53] = z[28] * z[50];
z[54] = z[22] * z[50];
z[52] = -z[52] / T(2) + -z[53] + z[54];
z[55] = T(5) * z[11];
z[56] = -z[1] + z[55];
z[56] = z[56] / T(3);
z[57] = z[12] / T(3);
z[58] = -z[10] + z[56] + -z[57];
z[59] = (T(2) * z[15]) / T(3) + z[16] / T(3);
z[60] = z[14] / T(2);
z[58] = -z[13] / T(6) + z[58] / T(2) + -z[59] + z[60];
z[58] = z[8] * z[58];
z[61] = z[11] / T(2);
z[62] = -z[60] + z[61];
z[63] = z[13] / T(2);
z[64] = z[10] / T(2) + -z[63];
z[57] = z[1] / T(3) + -z[57] + z[62] + -z[64];
z[57] = z[3] * z[57];
z[65] = T(3) * z[11];
z[66] = -z[1] + z[65];
z[67] = T(3) * z[12];
z[68] = T(5) * z[10] + -z[66] + -z[67];
z[68] = z[68] / T(2);
z[60] = z[60] + z[63];
z[69] = -z[16] + z[60] + z[68];
z[69] = -z[15] + z[69] / T(2);
z[69] = z[9] * z[69];
z[70] = -z[6] + z[7];
z[71] = -z[4] + z[9];
z[72] = z[21] + z[22] + z[28];
z[73] = (T(3) * z[70]) / T(4) + z[71] / T(4) + -z[72] / T(9);
z[74] = int_to_imaginary<T>(1) * z[27];
z[73] = z[73] * z[74];
z[75] = z[5] + -z[8];
z[76] = -z[70] + z[75];
z[77] = -z[71] + z[76];
z[78] = z[41] * z[77];
z[79] = -z[1] + z[12];
z[80] = z[47] + z[79];
z[81] = T(3) * z[34];
z[80] = z[80] * z[81];
z[81] = z[1] + z[11];
z[82] = -z[10] + -z[12] + z[81];
z[83] = -z[47] + z[82];
z[84] = z[33] * z[83];
z[85] = z[15] + z[16] / T(2);
z[64] = (T(-4) * z[1]) / T(3) + (T(11) * z[12]) / T(6) + -z[14] + -z[64] + z[85];
z[64] = z[6] * z[64];
z[62] = (T(2) * z[1]) / T(3) + -z[10] + -z[12] / T(6) + z[62] + z[85];
z[62] = z[4] * z[62];
z[86] = T(3) * z[10];
z[56] = (T(5) * z[12]) / T(3) + z[56] + -z[86];
z[56] = (T(-19) * z[13]) / T(12) + (T(5) * z[14]) / T(4) + z[56] / T(4) + z[59];
z[56] = z[5] * z[56];
z[59] = T(3) * z[1] + -z[10] + -z[45];
z[59] = z[59] / T(2);
z[87] = (T(3) * z[13]) / T(2) + (T(-5) * z[14]) / T(2) + z[59];
z[88] = z[16] + z[87];
z[88] = z[15] + z[88] / T(2);
z[89] = -(z[7] * z[88]);
z[52] = z[52] / T(2) + z[56] + z[57] + z[58] + z[62] + z[64] + z[69] + z[73] + -z[78] + -z[80] + -z[84] + z[89];
z[52] = z[27] * z[52];
z[47] = z[47] + z[82];
z[56] = z[29] * z[47];
z[57] = z[30] * z[50];
z[56] = -z[56] + z[57];
z[50] = z[17] * z[50];
z[57] = -z[50] + z[53];
z[58] = z[56] + -z[57];
z[62] = -z[54] + z[84];
z[64] = z[58] + z[62];
z[69] = z[8] * z[83];
z[73] = z[4] * z[47];
z[69] = -z[64] + z[69] + -z[73];
z[73] = z[1] + z[13];
z[46] = -z[14] + -z[46] + z[73];
z[82] = -(z[6] * z[46]);
z[82] = z[69] + z[82];
z[82] = z[38] * z[82];
z[67] = -z[10] + z[67] + -z[81];
z[84] = T(2) * z[15] + z[16];
z[60] = z[60] + -z[84];
z[67] = -z[60] + z[67] / T(2);
z[67] = z[8] * z[67];
z[89] = z[51] + z[57];
z[90] = z[62] + z[89];
z[67] = z[67] + z[90] / T(2);
z[87] = z[84] + z[87];
z[91] = z[5] + z[6];
z[92] = z[87] * z[91];
z[92] = -z[67] + z[92];
z[92] = z[39] * z[92];
z[93] = z[38] * z[46];
z[94] = z[39] * z[83];
z[93] = z[93] + z[94];
z[94] = z[3] / T(2);
z[93] = z[93] * z[94];
z[95] = z[6] + -z[71] + z[75];
z[95] = z[43] * z[95];
z[96] = -(z[39] * z[87]);
z[96] = -z[43] + z[96];
z[96] = z[7] * z[96];
z[82] = z[82] / T(2) + z[92] + z[93] + z[95] + z[96];
z[82] = int_to_imaginary<T>(1) * z[82];
z[78] = int_to_imaginary<T>(-1) * z[78];
z[76] = z[27] * z[76];
z[76] = z[76] + z[78];
z[76] = z[42] * z[76];
z[72] = z[40] * z[72];
z[72] = z[72] + z[76] + z[82];
z[76] = (T(3) * z[14]) / T(2);
z[78] = -z[63] + z[76];
z[82] = z[78] + -z[84];
z[92] = -z[45] + z[86];
z[93] = -z[1] + z[92];
z[93] = z[93] / T(2);
z[95] = z[82] + z[93];
z[96] = z[6] * z[95];
z[97] = z[5] * z[95];
z[98] = z[96] + z[97];
z[99] = z[4] * z[95];
z[100] = T(2) * z[12] + z[84];
z[81] = -z[13] + -z[81] + z[100];
z[101] = T(2) * z[8];
z[102] = z[3] + z[101];
z[102] = z[81] * z[102];
z[103] = (T(3) * z[62]) / T(2) + z[98] + z[99] + z[102];
z[103] = z[26] * z[103];
z[81] = z[8] * z[81];
z[104] = z[3] * z[79];
z[81] = z[81] + z[104];
z[105] = z[4] * z[79];
z[105] = z[80] + z[105];
z[106] = T(2) * z[1] + -z[45] + z[84];
z[107] = T(3) * z[14];
z[108] = T(2) * z[13] + z[106] + -z[107];
z[109] = z[5] * z[108];
z[110] = T(2) * z[79];
z[111] = z[6] * z[110];
z[112] = z[81] + -z[105] + -z[109] + z[111];
z[112] = z[25] * z[112];
z[65] = z[65] + -z[86];
z[113] = T(3) * z[13] + -z[107];
z[114] = -z[65] + z[113];
z[115] = z[79] + z[114];
z[116] = z[6] * z[115];
z[117] = -z[79] + z[114];
z[118] = -(z[4] * z[117]);
z[62] = T(3) * z[62] + z[116] + z[118];
z[62] = z[62] / T(2) + z[81] + z[97];
z[62] = z[24] * z[62];
z[77] = prod_pow(z[41], 2) * z[77];
z[62] = z[62] + (T(3) * z[77]) / T(2) + z[103] + T(2) * z[112];
z[62] = int_to_imaginary<T>(1) * z[62];
z[52] = z[52] + z[62] + T(3) * z[72];
z[52] = z[27] * z[52];
z[62] = -z[16] + z[78] + z[93];
z[62] = -z[15] + z[62] / T(2);
z[72] = z[8] * z[62];
z[77] = z[5] * z[62];
z[65] = -z[65] + -z[79] + -z[113];
z[78] = z[3] / T(4);
z[65] = z[65] * z[78];
z[81] = T(3) * z[7];
z[88] = z[81] * z[88];
z[93] = T(5) * z[12] + -z[86];
z[66] = z[66] + -z[93];
z[66] = (T(-9) * z[13]) / T(2) + (T(15) * z[14]) / T(2) + T(-3) * z[16] + z[66] / T(2);
z[66] = T(-3) * z[15] + z[66] / T(2);
z[66] = z[6] * z[66];
z[65] = z[65] + z[66] + -z[72] + z[77] + z[88] + (T(3) * z[90]) / T(4) + z[105];
z[65] = z[25] * z[65];
z[65] = z[65] + -z[103];
z[65] = z[25] * z[65];
z[66] = T(2) * z[11] + -z[73] + -z[86] + z[100];
z[77] = z[4] + z[8];
z[90] = -(z[66] * z[77]);
z[68] = z[60] + z[68];
z[100] = z[9] * z[68];
z[105] = T(3) * z[100];
z[50] = z[50] + z[54];
z[112] = -z[49] + z[50] + z[51];
z[62] = -(z[6] * z[62]);
z[113] = T(7) * z[1];
z[55] = -z[55] + -z[93] + z[113];
z[55] = z[55] / T(2);
z[82] = -z[55] + z[82];
z[82] = z[5] * z[82];
z[93] = T(9) * z[10];
z[113] = -z[45] + z[93] + -z[113];
z[116] = T(4) * z[15] + T(2) * z[16];
z[118] = z[13] / T(4);
z[113] = (T(9) * z[14]) / T(4) + z[113] / T(4) + -z[116] + -z[118];
z[113] = z[3] * z[113];
z[62] = z[62] + z[82] + z[88] + z[90] + -z[105] + (T(3) * z[112]) / T(4) + z[113];
z[62] = z[0] * z[62];
z[82] = z[66] * z[101];
z[88] = T(2) * z[66];
z[88] = z[4] * z[88];
z[90] = z[3] * z[95];
z[82] = (T(3) * z[50]) / T(2) + -z[82] + -z[88] + z[90] + -z[98] + -z[105];
z[88] = -(z[26] * z[82]);
z[95] = z[8] * z[95];
z[98] = z[95] + z[99];
z[99] = z[6] * z[108];
z[99] = z[99] + z[109];
z[81] = z[81] * z[87];
z[89] = z[81] + (T(3) * z[89]) / T(2) + z[90] + -z[98] + T(-2) * z[99];
z[90] = -z[25] + z[74];
z[89] = z[89] * z[90];
z[53] = z[53] + z[54];
z[54] = z[49] + z[53];
z[90] = -z[13] + z[106];
z[101] = -z[3] + T(-2) * z[5];
z[101] = z[90] * z[101];
z[96] = (T(3) * z[54]) / T(2) + -z[96] + -z[98] + z[101];
z[96] = z[24] * z[96];
z[62] = z[62] + z[88] + z[89] + z[96];
z[62] = z[0] * z[62];
z[88] = -z[0] + z[26];
z[82] = z[82] * z[88];
z[88] = z[94] * z[117];
z[89] = z[4] * z[110];
z[58] = (T(3) * z[58]) / T(2) + -z[88] + -z[89] + z[95] + -z[97] + -z[111];
z[88] = z[25] * z[58];
z[58] = z[58] * z[74];
z[50] = z[49] + z[50] + z[56];
z[56] = z[6] * z[117];
z[74] = T(-11) * z[79] + z[114];
z[74] = z[4] * z[74];
z[74] = T(3) * z[50] + -z[56] + z[74];
z[79] = -(z[8] * z[66]);
z[89] = T(5) * z[1];
z[86] = -z[45] + -z[86] + z[89];
z[86] = (T(-3) * z[14]) / T(4) + z[84] + z[86] / T(4) + -z[118];
z[86] = z[5] * z[86];
z[96] = -(z[78] * z[115]);
z[74] = z[74] / T(4) + z[79] + z[86] + z[96];
z[74] = z[23] * z[74];
z[79] = z[4] * z[115];
z[56] = -z[56] + z[79];
z[54] = T(-3) * z[54] + -z[56];
z[79] = z[5] * z[90];
z[54] = z[54] / T(2) + z[79] + z[95] + -z[104];
z[54] = z[24] * z[54];
z[54] = z[54] + z[58] + z[74] + z[82] + -z[88];
z[54] = z[23] * z[54];
z[58] = -z[73] + z[92] + z[107] + -z[116];
z[58] = z[3] * z[58];
z[45] = T(7) * z[45] + -z[89] + -z[93];
z[45] = (T(-5) * z[13]) / T(2) + z[45] / T(2) + z[76] + z[84];
z[45] = -(z[45] * z[77]);
z[55] = (T(7) * z[13]) / T(2) + (T(-9) * z[14]) / T(2) + z[55] + z[84];
z[55] = -(z[55] * z[91]);
z[51] = z[51] + z[53];
z[45] = z[45] + (T(3) * z[51]) / T(2) + z[55] + z[58] + z[81] + -z[105];
z[45] = z[35] * z[45];
z[51] = z[56] + T(-3) * z[64];
z[53] = -z[1] + z[12] / T(2) + z[61] + z[63] + -z[85];
z[53] = z[5] * z[53];
z[55] = z[78] * z[117];
z[51] = z[51] / T(4) + z[53] + z[55] + -z[72];
z[51] = z[24] * z[51];
z[51] = z[51] + z[88] + z[103];
z[51] = z[24] * z[51];
z[53] = z[68] * z[77];
z[49] = -z[49] + -z[57];
z[55] = z[59] + -z[60];
z[55] = z[5] * z[55];
z[56] = z[48] * z[94];
z[49] = z[49] / T(2) + z[53] + z[55] + z[56] + -z[100];
z[49] = z[31] * z[49];
z[53] = -z[5] + z[70];
z[53] = z[53] * z[87];
z[55] = -(z[83] * z[94]);
z[53] = z[53] + z[55] + z[67];
z[53] = z[37] * z[53];
z[49] = z[49] + z[53];
z[53] = -z[3] + z[4];
z[53] = z[46] * z[53];
z[47] = z[6] * z[47];
z[48] = -(z[5] * z[48]);
z[47] = z[47] + z[48] + z[50] + z[53];
z[47] = z[32] * z[47];
z[48] = z[3] + -z[6];
z[46] = z[46] * z[48];
z[46] = z[46] + z[69];
z[46] = z[36] * z[46];
z[46] = z[46] + z[47];
z[47] = -(z[4] * z[66]);
z[47] = z[47] + -z[80] + -z[99] + z[102];
z[47] = prod_pow(z[26], 2) * z[47];
z[48] = z[70] + (T(5) * z[71]) / T(2) + (T(-85) * z[75]) / T(6);
z[48] = z[44] * z[48];
return z[45] + (T(3) * z[46]) / T(2) + z[47] + z[48] + T(3) * z[49] + z[51] + z[52] + z[54] + z[62] + z[65];
}



template IntegrandConstructorType<double> f_4_270_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_270_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_270_construct (const Kin<qd_real>&);
#endif

}