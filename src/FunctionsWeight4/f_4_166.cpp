#include "f_4_166.h"

namespace PentagonFunctions {

template <typename T> T f_4_166_abbreviated (const std::array<T,16>&);

template <typename T> class SpDLog_f_4_166_W_23 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_166_W_23 (const Kin<T>& kin) {
        c[0] = rlog(kin.v[3]) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[0] * (T(4) + T(-2) * kin.v[0] + T(-4) * kin.v[1] + T(4) * kin.v[3]) + kin.v[2] * (T(-4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[1] * (T(4) * kin.v[2] + T(4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(2) * kin.v[4])) + rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[0] * (T(-4) + T(2) * kin.v[0] + T(4) * kin.v[1] + T(-4) * kin.v[3]) + kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[4] * (T(-4) + T(-2) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[0] * (T(-4) + T(2) * kin.v[0] + T(4) * kin.v[1] + T(-4) * kin.v[3]) + kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[4] * (T(-4) + T(-2) * kin.v[4]) + kin.v[2] * (T(4) + T(-2) * kin.v[2] + T(4) * kin.v[4]) + kin.v[1] * (T(-4) * kin.v[2] + T(-4) * kin.v[3] + T(4) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[0] * (prod_pow(abb[2], 2) * (abb[4] * T(-2) + abb[5] * T(-2)) + prod_pow(abb[2], 2) * abb[3] * T(2) + prod_pow(abb[1], 2) * (abb[3] * T(-2) + abb[4] * T(2) + abb[5] * T(2)));
    }
};
template <typename T> class SpDLog_f_4_166_W_22 {
    using TC = std::complex<T>;
    std::array<TC,1> c;

public:
    SpDLog_f_4_166_W_22 (const Kin<T>& kin) {
        c[0] = rlog(-kin.v[4] + kin.v[1] + kin.v[2]) * (kin.v[1] * (T(-4) + T(4) * kin.v[0] + T(2) * kin.v[1] + T(-4) * kin.v[3]) + kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(-2) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4])) + rlog(-kin.v[2] + kin.v[0] + kin.v[4]) * (kin.v[1] * (T(-4) + T(4) * kin.v[0] + T(2) * kin.v[1] + T(-4) * kin.v[3]) + kin.v[3] * (T(4) + T(2) * kin.v[3]) + kin.v[0] * (T(4) * kin.v[2] + T(-4) * kin.v[3] + T(-4) * kin.v[4]) + kin.v[4] * (T(4) + T(-2) * kin.v[4]) + kin.v[2] * (T(-4) + T(-2) * kin.v[2] + T(4) * kin.v[4])) + rlog(kin.v[3]) * (kin.v[3] * (T(-4) + T(-2) * kin.v[3]) + kin.v[1] * (T(4) + T(-4) * kin.v[0] + T(-2) * kin.v[1] + T(4) * kin.v[3]) + kin.v[2] * (T(4) + T(2) * kin.v[2] + T(-4) * kin.v[4]) + kin.v[4] * (T(-4) + T(2) * kin.v[4]) + kin.v[0] * (T(-4) * kin.v[2] + T(4) * kin.v[3] + T(4) * kin.v[4]));

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0];
        }

        return abb[6] * (prod_pow(abb[7], 2) * (abb[4] * T(-2) + abb[5] * T(-2)) + abb[3] * prod_pow(abb[7], 2) * T(2) + prod_pow(abb[1], 2) * (abb[3] * T(-2) + abb[4] * T(2) + abb[5] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_166_construct (const Kin<T>& kin) {
    return [&kin, 
            dl23 = DLog_W_23<T>(kin),dl22 = DLog_W_22<T>(kin),dl16 = DLog_W_16<T>(kin),dl4 = DLog_W_4<T>(kin),dl20 = DLog_W_20<T>(kin),dl17 = DLog_W_17<T>(kin),spdl23 = SpDLog_f_4_166_W_23<T>(kin),spdl22 = SpDLog_f_4_166_W_22<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[21] = t * (-kin.v[1] + -kin.v[2] + kin.v[3] + kin.v[4]);
kin_path.W[22] = t * (-kin.v[2] + -kin.v[3] + kin.v[0] + kin.v[4]);

        std::array<std::complex<T>,16> abbr = 
            {dl23(t), rlog(-v_path[3] + v_path[0] + v_path[1]), rlog(-v_path[4] + v_path[1] + v_path[2]), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[19] / kin_path.W[19]), dl22(t), rlog(-v_path[2] + v_path[0] + v_path[4]), dl16(t), -rlog(t), -rlog(t), dl4(t), f_2_1_8(kin_path), f_2_1_14(kin_path), dl20(t), dl17(t)}
;

        auto result = f_4_166_abbreviated(abbr);
        result = result + spdl22(t, abbr) + spdl23(t, abbr);

        return result;
    };
}

template <typename T> T f_4_166_abbreviated(const std::array<T,16>& abb)
{
using TR = typename T::value_type;
T z[26];
z[0] = abb[1];
z[1] = abb[3];
z[2] = abb[11];
z[3] = abb[14];
z[4] = abb[15];
z[5] = abb[4];
z[6] = abb[8];
z[7] = abb[5];
z[8] = abb[9];
z[9] = abb[10];
z[10] = abb[2];
z[11] = abb[7];
z[12] = abb[12];
z[13] = abb[13];
z[14] = bc<TR>[0];
z[15] = bc<TR>[9];
z[16] = z[4] * z[9];
z[17] = z[5] + z[7];
z[18] = -z[1] + z[17];
z[19] = z[3] * z[18];
z[20] = z[4] * z[7];
z[19] = -z[16] + z[19] + z[20];
z[19] = z[10] * z[19];
z[20] = z[3] + z[4];
z[21] = -z[2] + z[20];
z[22] = z[18] * z[21];
z[23] = T(2) * z[22];
z[24] = -(z[0] * z[23]);
z[19] = z[19] + z[24];
z[19] = z[10] * z[19];
z[24] = -z[0] + z[10];
z[22] = z[22] * z[24];
z[24] = -z[5] + z[8];
z[24] = -(z[3] * z[24]);
z[25] = z[4] * z[18];
z[24] = z[24] + z[25];
z[24] = z[11] * z[24];
z[22] = T(2) * z[22] + z[24];
z[22] = z[11] * z[22];
z[24] = T(2) * z[2];
z[20] = z[20] + -z[24];
z[20] = z[18] * z[20];
z[17] = z[8] + z[9] + -z[17];
z[17] = z[6] * z[17];
z[20] = z[17] + z[20];
z[20] = prod_pow(z[0], 2) * z[20];
z[25] = z[13] * z[18];
z[25] = z[15] + z[25];
z[21] = z[21] * z[25];
z[23] = z[12] * z[23];
z[19] = z[19] + z[20] + T(2) * z[21] + z[22] + z[23];
z[16] = -z[16] + z[17];
z[17] = z[18] * z[24];
z[18] = T(2) * z[1];
z[20] = (T(-5) * z[5]) / T(2) + T(-2) * z[7] + z[8] / T(2) + z[18];
z[20] = z[3] * z[20];
z[18] = T(-2) * z[5] + (T(-5) * z[7]) / T(2) + z[18];
z[18] = z[4] * z[18];
z[16] = -z[16] / T(2) + z[17] + z[18] + z[20];
z[16] = prod_pow(z[14], 2) * z[16];
return z[16] / T(3) + T(2) * z[19];
}



template IntegrandConstructorType<double> f_4_166_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_166_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_166_construct (const Kin<qd_real>&);
#endif

}