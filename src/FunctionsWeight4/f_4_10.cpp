#include "f_4_10.h"

namespace PentagonFunctions {

template <typename T> T f_4_10_abbreviated (const std::array<T,17>&);



template <typename T> IntegrandConstructorType<T> f_4_10_construct (const Kin<T>& kin) {
    return [&kin, 
            dl25 = DLog_W_25<T>(kin),dl18 = DLog_W_18<T>(kin),dl6 = DLog_W_6<T>(kin),dl1 = DLog_W_1<T>(kin),dl19 = DLog_W_19<T>(kin),dl17 = DLog_W_17<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        
        std::array<std::complex<T>,17> abbr = 
            {dl25(t), rlog(kin.W[0] / kin_path.W[0]), rlog(-v_path[2] + -v_path[3] + v_path[0]), rlog(-v_path[1] + v_path[3] + v_path[4]), rlog(kin.W[16] / kin_path.W[16]), rlog(kin.W[18] / kin_path.W[18]), dl18(t), rlog(kin.W[5] / kin_path.W[5]), rlog(kin.W[24] / kin_path.W[24]), dl6(t), rlog(v_path[0]), dl1(t), f_2_1_9(kin_path), f_2_1_12(kin_path), rlog(-v_path[1] + -v_path[2] + v_path[0] + v_path[4]), dl19(t), dl17(t)}
;

        auto result = f_4_10_abbreviated(abbr);
        
        return result;
    };
}

template <typename T> T f_4_10_abbreviated(const std::array<T,17>& abb)
{
using TR = typename T::value_type;
T z[41];
z[0] = abb[0];
z[1] = abb[1];
z[2] = abb[2];
z[3] = bc<TR>[0];
z[4] = abb[3];
z[5] = abb[4];
z[6] = abb[5];
z[7] = abb[6];
z[8] = abb[9];
z[9] = abb[16];
z[10] = abb[11];
z[11] = abb[15];
z[12] = abb[10];
z[13] = abb[12];
z[14] = abb[13];
z[15] = abb[14];
z[16] = abb[7];
z[17] = abb[8];
z[18] = bc<TR>[1];
z[19] = bc<TR>[2];
z[20] = bc<TR>[4];
z[21] = bc<TR>[7];
z[22] = bc<TR>[8];
z[23] = bc<TR>[9];
z[24] = -z[1] + -z[6] + z[16] + z[17];
z[24] = z[7] * z[24];
z[25] = z[10] * z[16];
z[26] = z[24] + -z[25];
z[27] = z[11] * z[17];
z[28] = z[9] + -z[10];
z[29] = -z[11] + z[28];
z[30] = z[19] * z[29];
z[31] = -z[0] + z[10];
z[32] = z[5] * z[31];
z[33] = -z[11] / T(6) + T(2) * z[31];
z[33] = z[1] * z[33];
z[34] = T(-2) * z[0] + -z[10] / T(6);
z[34] = z[6] * z[34];
z[35] = (T(13) * z[8]) / T(2) + z[11] / T(2) + T(-7) * z[28];
z[35] = z[18] * z[35];
z[36] = T(5) * z[0] + T(-11) * z[10];
z[36] = z[9] + -z[11] + z[36] / T(6);
z[36] = int_to_imaginary<T>(1) * z[3] * z[36];
z[26] = (T(-13) * z[26]) / T(6) + z[27] / T(6) + (T(7) * z[30]) / T(3) + T(-2) * z[32] + z[33] + z[34] + z[35] / T(3) + z[36];
z[26] = z[3] * z[26];
z[33] = -z[1] + z[5];
z[34] = -z[8] + z[28];
z[35] = z[33] * z[34];
z[36] = z[8] + -z[9];
z[37] = z[6] * z[36];
z[25] = z[25] + z[35] + z[37];
z[25] = z[12] * z[25];
z[33] = -z[6] + z[33];
z[35] = -z[0] + z[10] + z[11];
z[37] = z[8] + -z[35];
z[37] = z[33] * z[37];
z[37] = -z[24] + z[37];
z[37] = z[2] * z[37];
z[38] = z[29] * z[33];
z[39] = -(z[15] * z[38]);
z[40] = T(2) * z[29];
z[40] = -(z[20] * z[40]);
z[37] = z[25] + z[37] + z[39] + z[40];
z[28] = z[8] + T(2) * z[11] + T(-3) * z[28];
z[28] = z[18] * z[28];
z[28] = z[28] + T(2) * z[30];
z[28] = z[18] * z[28];
z[28] = z[28] + T(2) * z[37];
z[28] = int_to_imaginary<T>(1) * z[28];
z[26] = z[26] + T(2) * z[28];
z[26] = z[3] * z[26];
z[28] = z[1] * z[35];
z[30] = z[6] * z[31];
z[27] = -z[27] + z[28] + z[30] + -z[32];
z[27] = prod_pow(z[4], 2) * z[27];
z[28] = T(2) * z[4];
z[28] = z[28] * z[38];
z[25] = -z[25] + z[28];
z[25] = z[12] * z[25];
z[30] = z[0] + z[36];
z[30] = -(z[30] * z[33]);
z[24] = z[24] + z[30];
z[24] = z[2] * z[24];
z[24] = z[24] + -z[28];
z[24] = z[2] * z[24];
z[28] = z[13] + -z[14];
z[28] = T(-2) * z[28];
z[28] = z[28] * z[38];
z[30] = prod_pow(z[19], 3);
z[30] = T(-2) * z[21] + (T(2) * z[30]) / T(3);
z[30] = z[29] * z[30];
z[31] = prod_pow(z[18], 3) * z[34];
z[24] = z[24] + z[25] + z[27] + z[28] + z[30] + z[31] / T(3);
z[25] = -z[22] + (T(11) * z[23]) / T(6);
z[25] = z[25] * z[29];
return T(2) * z[24] + z[25] + z[26];
}



template IntegrandConstructorType<double> f_4_10_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_10_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_10_construct (const Kin<qd_real>&);
#endif

}