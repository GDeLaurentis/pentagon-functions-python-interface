#include "f_4_21.h"

namespace PentagonFunctions {

template <typename T> T f_4_21_abbreviated (const std::array<T,18>&);

template <typename T> class SpDLog_f_4_21_W_12 {
    using TC = std::complex<T>;
    std::array<TC,2> c;

public:
    SpDLog_f_4_21_W_12 (const Kin<T>& kin) {
        c[0] = T(-2) * prod_pow(kin.v[4], 2) + kin.v[1] * (T(-2) * kin.v[1] + T(4) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (((T(-3) * kin.v[4]) / T(2) + T(-2)) * kin.v[4] + kin.v[1] * (kin.v[1] / T(2) + T(2) + kin.v[4])) + rlog(kin.v[3]) * ((-kin.v[1] / T(2) + -kin.v[4] + T(-2)) * kin.v[1] + ((T(3) * kin.v[4]) / T(2) + T(2)) * kin.v[4]);
c[1] = rlog(kin.v[3]) * (T(2) * kin.v[1] + T(-2) * kin.v[4]) + rlog(-kin.v[1] + kin.v[3] + kin.v[4]) * (T(-2) * kin.v[1] + T(2) * kin.v[4]);

    }

    template <typename TABB> TC operator()(T t, const TABB& abb) const {

        if (t < detail::SpDLogZeroThreshold<T>) {
            return t * c[0] + c[1];
        }

        return abb[0] * (prod_pow(abb[2], 2) * abb[5] * T(-2) + abb[3] * abb[5] * T(2) + abb[4] * (abb[3] * T(-2) + prod_pow(abb[2], 2) * T(2)) + prod_pow(abb[1], 2) * (abb[4] * T(-2) + abb[5] * T(2)));
    }
};

template <typename T> IntegrandConstructorType<T> f_4_21_construct (const Kin<T>& kin) {
    return [&kin, 
            dl12 = DLog_W_12<T>(kin),dl19 = DLog_W_19<T>(kin),dl4 = DLog_W_4<T>(kin),dl14 = DLog_W_14<T>(kin),dl5 = DLog_W_5<T>(kin),dl2 = DLog_W_2<T>(kin),spdl12 = SpDLog_f_4_21_W_12<T>(kin)
        ](T t) {

        std::array<T,5> v_path = {
                T(3)*(T(1) - t) + t*kin.v[0],
                T(-1) + t + t*kin.v[1],
                T(1) - t + t*kin.v[2],
                T(1) - t + t*kin.v[3],
                T(-1)+ t + t*kin.v[4]
        };

        Kin<T> kin_path(v_path);
        kin_path.W[6] = t * (kin.v[3] + kin.v[4]);
kin_path.W[11] = t * (-kin.v[4] + kin.v[1]);

        std::array<std::complex<T>,18> abbr = 
            {dl12(t), rlog(-v_path[1]), rlog(-v_path[4]), f_2_1_4(kin_path), rlog(kin.W[3] / kin_path.W[3]), rlog(kin.W[18] / kin_path.W[18]), dl19(t), rlog(kin.W[13] / kin_path.W[13]), rlog(v_path[3]), f_2_1_3(kin_path), rlog(-v_path[1] + v_path[3]), rlog(kin.W[1] / kin_path.W[1]), rlog(kin.W[4] / kin_path.W[4]), -rlog(t), dl4(t), dl14(t), dl5(t), dl2(t)}
;

        auto result = f_4_21_abbreviated(abbr);
        result = result + spdl12(t, abbr);

        return result;
    };
}

template <typename T> T f_4_21_abbreviated(const std::array<T,18>& abb)
{
using TR = typename T::value_type;
T z[51];
z[0] = abb[1];
z[1] = abb[4];
z[2] = abb[6];
z[3] = abb[14];
z[4] = abb[15];
z[5] = abb[16];
z[6] = abb[17];
z[7] = abb[5];
z[8] = abb[7];
z[9] = abb[11];
z[10] = abb[12];
z[11] = abb[13];
z[12] = abb[2];
z[13] = abb[8];
z[14] = bc<TR>[0];
z[15] = abb[3];
z[16] = abb[9];
z[17] = abb[10];
z[18] = bc<TR>[2];
z[19] = bc<TR>[9];
z[20] = T(2) * z[7];
z[21] = z[8] + z[20];
z[22] = T(3) * z[9];
z[23] = T(4) * z[11];
z[24] = z[1] + z[10];
z[25] = -z[21] + z[22] + -z[23] + T(4) * z[24];
z[26] = z[2] * z[25];
z[27] = T(3) * z[10];
z[28] = (T(3) * z[9]) / T(2);
z[29] = z[27] + z[28];
z[30] = -z[7] + z[23];
z[31] = z[8] / T(2);
z[32] = z[1] + z[29] + -z[30] + z[31];
z[32] = z[6] * z[32];
z[33] = T(2) * z[11];
z[21] = z[21] + -z[24] + -z[33];
z[34] = T(2) * z[3];
z[21] = z[21] * z[34];
z[35] = T(3) * z[7];
z[36] = T(2) * z[24];
z[37] = z[9] + z[36];
z[38] = T(2) * z[8] + z[23] + z[35] + T(-3) * z[37];
z[38] = z[5] * z[38];
z[39] = (T(3) * z[8]) / T(2);
z[40] = T(3) * z[1] + z[10] + -z[28] + -z[39];
z[40] = z[4] * z[40];
z[21] = z[21] + z[26] + -z[32] + z[38] + z[40];
z[32] = z[17] * z[21];
z[31] = -z[1] + z[31];
z[38] = z[10] + z[31];
z[28] = -z[7] + z[28] + z[38];
z[28] = z[4] * z[28];
z[40] = z[9] / T(2);
z[38] = z[38] + z[40];
z[38] = z[6] * z[38];
z[41] = -z[30] + z[37];
z[42] = z[5] * z[41];
z[43] = -z[9] + -z[24] + z[33];
z[43] = z[34] * z[43];
z[42] = z[28] + z[38] + z[42] + z[43];
z[42] = z[0] * z[42];
z[43] = z[8] + -z[9];
z[44] = z[20] + -z[36] + z[43];
z[45] = T(2) * z[5];
z[46] = z[44] * z[45];
z[27] = z[1] + z[27];
z[22] = T(4) * z[7] + z[8] + -z[22] + T(-2) * z[27];
z[22] = z[4] * z[22];
z[27] = T(3) * z[8] + -z[37];
z[37] = -(z[27] * z[34]);
z[47] = z[1] + -z[10];
z[47] = z[8] + z[9] + T(-2) * z[47];
z[47] = z[6] * z[47];
z[22] = z[22] + z[37] + -z[46] + z[47];
z[22] = z[13] * z[22];
z[37] = z[4] + z[6];
z[37] = T(3) * z[3] + z[5] + (T(-3) * z[37]) / T(2);
z[37] = prod_pow(z[18], 2) * z[37];
z[22] = z[22] + z[32] + z[37] / T(2) + z[42];
z[22] = int_to_imaginary<T>(1) * z[22];
z[32] = z[9] + z[11];
z[32] = (T(3) * z[1]) / T(2) + (T(-5) * z[8]) / T(6) + (T(4) * z[10]) / T(3) + -z[20] + (T(2) * z[32]) / T(3);
z[32] = z[5] * z[32];
z[37] = (T(-7) * z[9]) / T(2) + (T(-17) * z[10]) / T(3) + z[31];
z[37] = z[20] + z[37] / T(2);
z[37] = z[4] * z[37];
z[42] = T(11) * z[1] + (T(17) * z[9]) / T(2) + T(13) * z[10];
z[42] = (T(-35) * z[8]) / T(4) + -z[33] + z[42] / T(2);
z[47] = z[3] / T(3);
z[42] = z[42] * z[47];
z[48] = -z[2] + -z[5];
z[47] = (T(11) * z[4]) / T(6) + -z[6] / T(2) + -z[47] + z[48] / T(3);
z[48] = int_to_imaginary<T>(1) * z[14];
z[47] = z[47] * z[48];
z[49] = z[8] + z[10];
z[49] = -z[1] + z[9] / T(3) + (T(2) * z[49]) / T(3);
z[49] = z[6] * z[49];
z[50] = T(2) * z[2] + -z[3] / T(4) + z[4] / T(8) + (T(-11) * z[5]) / T(4) + (T(-15) * z[6]) / T(8);
z[50] = z[18] * z[50];
z[32] = z[32] + z[37] + z[42] + z[47] / T(2) + z[49] + z[50];
z[32] = z[14] * z[32];
z[22] = z[22] + z[32];
z[22] = z[14] * z[22];
z[24] = -z[24] + z[30] + -z[39] + -z[40];
z[24] = z[6] * z[24];
z[30] = z[5] * z[44];
z[32] = z[3] * z[44];
z[24] = z[24] + z[26] + -z[28] + z[30] + z[32];
z[24] = prod_pow(z[0], 2) * z[24];
z[28] = z[34] * z[44];
z[34] = z[6] * z[41];
z[37] = z[7] + z[8];
z[36] = -z[36] + z[37];
z[36] = z[4] * z[36];
z[28] = -z[26] + -z[28] + z[34] + z[36] + -z[46];
z[34] = z[0] * z[28];
z[20] = -z[20] + z[29] + -z[31];
z[20] = z[4] * z[20];
z[27] = z[3] * z[27];
z[20] = z[20] + z[27] + z[30] + -z[38];
z[20] = z[13] * z[20];
z[20] = z[20] + z[34];
z[20] = z[13] * z[20];
z[21] = z[16] * z[21];
z[27] = z[0] + -z[13] + z[48];
z[27] = z[27] * z[28];
z[28] = -z[1] + z[9];
z[28] = z[28] * z[45];
z[29] = T(2) * z[10] + -z[43];
z[29] = z[6] * z[29];
z[28] = z[28] + z[29] + z[32];
z[28] = z[12] * z[28];
z[27] = z[27] + z[28];
z[27] = z[12] * z[27];
z[25] = -(z[3] * z[25]);
z[23] = z[8] + -z[23] + z[35];
z[23] = z[5] * z[23];
z[28] = -z[1] + z[33];
z[28] = T(2) * z[28] + -z[37];
z[28] = z[6] * z[28];
z[23] = z[23] + z[25] + z[26] + z[28];
z[23] = z[15] * z[23];
z[25] = (T(-149) * z[3]) / T(3) + (T(7) * z[4]) / T(2) + T(27) * z[5] + (T(325) * z[6]) / T(6);
z[25] = z[19] * z[25];
return z[20] + z[21] + z[22] + z[23] + z[24] + z[25] / T(8) + z[27];
}



template IntegrandConstructorType<double> f_4_21_construct (const Kin<double>&);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template IntegrandConstructorType<dd_real> f_4_21_construct (const Kin<dd_real>&);
template IntegrandConstructorType<qd_real> f_4_21_construct (const Kin<qd_real>&);
#endif

}