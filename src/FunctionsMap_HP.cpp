#include "FunctionID.h"
#include "Integrate.h"
#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "FunctionsMap.hpp"

namespace PentagonFunctions {

template <> std::unordered_map<FunID<KinType::m0>, FunctionObjectType<dd_real,KinType::m0>> functions_map<dd_real,KinType::m0> = {};

template void detail::fill_function_map<dd_real, KinType::m0>();

template FunctionObjectType<dd_real,KinType::m0> FunID<KinType::m0>::get_evaluator<dd_real>() const;

#ifdef PENTAGON_FUNCTIONS_M1_ENABLED 
template <> std::unordered_map<FunID<KinType::m1>, FunctionObjectType<dd_real,KinType::m1>> functions_map<dd_real,KinType::m1> = {};

template void detail::fill_function_map<dd_real, KinType::m1>();

template FunctionObjectType<dd_real,KinType::m1> FunID<KinType::m1>::get_evaluator<dd_real>() const;
#endif // PENTAGON_FUNCTIONS_M1_ENABLED

} // namespace PentagonFunctions

