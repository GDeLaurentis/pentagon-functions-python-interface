#pragma once

#include "Kin.h"
#include <Li2++.h>
#include <type_traits>

namespace PentagonFunctions {

using Li2pp::pi;

/**
 * is_complex checks if the type is complex
 */
template <typename T> struct is_complex : std::false_type {};
template <typename T> struct is_complex<std::complex<T>> : std::true_type {};

template <typename T> std::enable_if_t<!is_complex<T>::value, std::complex<T>> int_to_imaginary(int d) { return {T(0), static_cast<T>(d)}; }
template <typename T> std::enable_if_t<is_complex<T>::value, T> int_to_imaginary(int d) {
    using TR = typename T::value_type;
    return T{TR(0), static_cast<TR>(d)};
}

template <typename T> int sign(const T& x) {
    if (x > 0) return 1;
    if (x < 0) return -1;
    return 0;
}

/**
 * Real log of nonnegative argument
 */
template <typename T> T rlog(const T& t) {
    using std::log;
    return log(t);
}

/**
 * Log of a phase (returns the imaginary part).
 * The phase will be in the interval (0, 2 Pi)
 */
template <typename T> T log_iphi_im(const std::complex<T>& t) {
    using std::arg;
    T a = arg(t); // arg in range (-Pi, Pi)
    if (a < 0) a += (2 * pi<T>());
    return a;
}

#ifdef PENTAGON_FUNCTIONS_M1_ENABLED 

/**
 * Either real or standard log of I sqrt(r) with sqrt(r) positive
 */
template <typename T> std::complex<T> clog(const std::complex<T>& t) {
    using std::fabs;

    T re = rlog(fabs(t));

    if(t.imag() != T{}) {
        return {re, pi<T>()/2};
    }
    else {
        return re;
    }
}

template <typename T> T log2_re(const std::complex<T>& t) {
    using std::arg;

    T re = t.real();
    T im = t.imag();

    if (im == T(0) && re >= T(0)) return rlog(t.real());
    
    return 0;
}

template <typename T> T log2_im(const std::complex<T>& t) {
    T re = t.real();
    T im = t.imag();

    if (im == T(0)) {
        if (re >= T(1)) {
            return 0;
        }
        else if (re >= T(0)) return 2*pi<T>();
    }

    return log_iphi_im(t);
}

template <typename T> std::complex<T> log2(const std::complex<T>& t) {return {log2_re(t), log2_im(t)};}

template <typename T> T log3_re(const std::complex<T>& t) {
    T re = t.real();
    T im = t.imag();

    if (im == T(0) && re > T(1)) return rlog(t.real());

    return 0;
}


template <typename T> T log3_im(const std::complex<T>& t) {
    T re = t.real();
    T im = t.imag();

    if (im == T(0) && re >= T(1)) return 0;

    using std::arg;

    /* The phase needs to be in the interval (-2Pi, 0) */
    T a = arg(t); // arg in range (-Pi, Pi)
    if (a > 0) a -= (2 * pi<T>());
    return a;
}

template <typename T> std::complex<T> log3(const std::complex<T>& t) {return {log3_re(t), log3_im(t)};}

#endif // PENTAGON_FUNCTIONS_M1_ENABLED


template <typename T> inline T rLi2(T t) { return Li2pp::ReLi2(t); }
template <typename T> T SV_Li2(const std::complex<T>& t) {using std::arg; return Li2pp::Cl2(arg(t)); }

} // namespace PentagonFunctions
