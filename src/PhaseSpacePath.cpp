#include "PhaseSpacePath.h"
#include "Constants.h"
#include "PhaseSpaceGen.h"
#include "PolyLog.h"
#include "Integrator/tanh_sinh_detail.hpp"
#include "ChangePrecision.h"

#include <algorithm>
#include <mutex>
#include <optional>

#include <iostream>

#ifdef PENTAGON_FUNCTIONS_M1_ENABLED 
#include "m1_set/DLog.h"
#endif // PENTAGON_FUNCTIONS_M1_ENABLED




namespace PentagonFunctions {

namespace {

template <typename T> std::tuple<int, T, T> get_segment_bounds(int Nsegments, T t) {
    using boost::math::quadrature::detail::to_int;
    using std::floor;
    int current_segement_start = to_int(floor(Nsegments * t));

    T tbegin(current_segement_start);
    tbegin /= Nsegments;
    T tend(current_segement_start + 1);
    tend /= Nsegments;

    return {current_segement_start, tbegin, tend};
}

/**
 * Improve quality of the root x0 with Newton iterations.
 *
 * Aggressively until f(x0) does not decrease further 
 */
template <typename F1, typename F2, typename T> T newton_iterate_aggressive(F1 f, F2 fder, T x0) {
    using std::fabs;
    constexpr size_t n_max = 1000;

    static_assert(std::is_same_v<T, decltype(f(std::declval<T>()))>);
    static_assert(std::is_same_v<T, decltype(fder(std::declval<T>()))>);

    T last_val =  f(x0);

    // if the root is so good that the function value is already as small as it can be, just retun it
    if (fabs(last_val) <= std::numeric_limits<T>::epsilon()) return x0;

    T x = x0;
    T x_before_increasing = x;

    size_t n_iterations = 0;
    size_t n_not_decreasing = 0;

    while (n_iterations <= n_max) {
        ++n_iterations;

        x = x - f(x)/fder(x);

        T new_val = f(x);

        if (fabs(new_val) <= std::numeric_limits<T>::epsilon()) return x;

        if (fabs(new_val) >= fabs(last_val)){
            ++n_not_decreasing;
        }
        else {
            x_before_increasing = x;
            n_not_decreasing = 0;
        }
        if(n_not_decreasing >= 5) return x_before_increasing;

        last_val = new_val;
    }

    if (fabs(x-x0) > T(0.1)) {
        std::cerr << "WARNING: initial guess for Newton iterations was found to be very bad!" << std::endl;
    }

    if (n_iterations == n_max) {
        std::cerr << "WARNING: maximal number " << n_max << " of Newton iterations reached!" << std::endl;
    }

    return x;
}


template <typename T> std::optional<std::pair<T,T>> solve_quadratic_equation(T a, T b, T c) {
    if (a == T{}) throw std::invalid_argument("a should not be 0!");

    auto f = [=](auto x) { return a * x * x + b * x + c; };
    auto fder = [=](auto x) { return 2*a * x + b ; };

    b /= a;
    c /= a;

    T discriminant = b * b - T(4) * c;

    if (discriminant < 0) return {};

    using std::sqrt;
    T root = sqrt(discriminant);
    T solution1 = (b > 0) ? (-b - root) / 2 : (-b + root) / 2;
    T solution2 = c/solution1;

    solution1 = newton_iterate_aggressive(f, fder, solution1);
    solution2 = newton_iterate_aggressive(f, fder, solution2);

    if (solution1 < solution2)
        return {{solution1, solution2}};
    else
        return {{solution2, solution1}};
}

template <typename T> std::array<std::complex<T>,2> solve_quadratic_equation_c(T a, T b, T c) {

    using C = std::complex<T>;

    if (a == T{}) throw std::invalid_argument("a should not be 0!");

    b /= a;
    c /= a;

    T discriminant = b * b - T(4) * c;

    using std::sqrt;
    C root = sqrt(static_cast<C>(discriminant));
    C solution1 = (b > 0) ? (-b - root) / T{2} : (-b + root) / T{2};
    C solution2 = c/solution1;

    return {solution1, solution2};
}


/**
 * Returns a vector of real roots within (tbegin, tend)
 * Coefficients are of expansion around t = 0
 */
template <typename T> std::vector<T> solve_quartic(const std::array<T,5>& c_in, const T& tbegin, const T& tend) {
    using std::sqrt;
    using std::pow;
    using C = std::complex<T>;

    auto a = c_in[3]/c_in[4];
    const auto& b = c_in[2]/c_in[4];
    const auto& c = c_in[1]/c_in[4];
    const auto& d = c_in[0]/c_in[4];

    std::array<C,2> vi;

    {
        T bc = prod_pow(c, 2) * T(-27) + prod_pow(a, 2) * d * T(-27) + b * (prod_pow(b, 2) * T(-2) + a * c * T(9) + d * T(72));
        T cc = prod_pow(prod_pow(b, 2) + a * c * T(-3) + d * T(12), 3);

        vi = solve_quadratic_equation_c(T{1}, bc, cc);

        vi[0] = pow(vi[0], T(1)/T(3));
    }

    const auto& v13 = vi[0];

    auto u = prod_pow(a, 2) / T(4) + (b * T(-2)) / T(3) + (v13 + (prod_pow(b, 2) + a * c * T(-3) + d * T(12)) / v13) / T(3);

    const auto r11 = (-4*u-8*b+3*a*a)/T(16);

    u = sqrt(u);

    const auto r22 = (-a*a*a+ 4*a*b - 8*c)/(16*u);

    u /= T(2);
    a /= -4;

    std::vector<C> roots_C {
        a + u + sqrt(r11 + r22),
        a + u - sqrt(r11 + r22),
        a - u + sqrt(r11 - r22),
        a - u - sqrt(r11 - r22),
    };

    // At this point we have to put a threshold for when the root is considered real.

    std::vector<T> roots;

    for (auto& ri : roots_C) {
        using std::fabs;
        auto re = ri.real();
        auto im = ri.imag();

        if (fabs(im) < ::PentagonFunctions::detail::VanishingDLogThreshold<T> and re >= T(0)) {
            if (re <= tend-tbegin) {

                auto f = [&](T t) {
                    return c_in[4]*t*t*t*t + c_in[3]*t*t*t + c_in[2]*t*t + c_in[1]*t + c_in[0];
                };
                auto fder = [&](T t) {
                    return 4*c_in[4]*t*t*t + 3*c_in[3]*t*t + 2*c_in[2]*t + c_in[1];
                };

                // if the analytic solution was unstable, we correct it by performing aggressive Newton iterations
                re = newton_iterate_aggressive(f,fder,re);

                re += tbegin;
                roots.push_back(re);
            }
        }
    }

    std::sort(roots.begin(), roots.end());
    
    return roots;
}

}

#ifdef PENTAGON_FUNCTIONS_M1_ENABLED

namespace m1_set {

constexpr size_t max_number_of_MC_attempts = 900000;
static constexpr std::array spurious_linear_letters = {16,17,19,20}; // 1-based indices here
static constexpr std::array check_small_letters = {1,2,3,4,5,6,7,8,9,10,11,16,17,19,20}; // 1-based indices here
static const std::set sigma5_letters = {199, 200, 201, 202, 203, 204}; // 1-based indices here


#include "m1_set/derivatives.incl.hpp"
#include "m1_set/gram5_PS_boundary.incl.hpp"

template <typename Tkin> std::vector<Tkin> construct_line_segments(const Tkin& vb, const Tkin& ve, double threshold_factor) {
    std::vector<Tkin> path_points;

    if (line_lies_within_physical_phase_space(vb, ve)) {
        path_points.push_back(vb);
        path_points.push_back(ve);
    }
    else {
        using T = typename Tkin::value_type;

        if (vb == ve) throw std::runtime_error("construct_line_segments received equal start and end points");

        using std::sqrt;

        static thread_local std::shared_ptr<std::mt19937> gen = std::make_shared<std::mt19937>(42);

        auto [a,b] = std::minmax({sqrt(vb[4]),sqrt(ve[4])});
        std::uniform_real_distribution<double> distribution{to_double(a),to_double(b)};

        PhaseSpaceGen<T> ps(6, T{distribution(*gen)}, {4, 5});
        ps.set_random_generator(gen);

        auto get_signs = [](const auto& kin) {
            constexpr size_t number_spurious_letters_at_origin = spurious_linear_letters.size();
            std::array<int, number_spurious_letters_at_origin> signs;
            for (size_t i = 0; i < number_spurious_letters_at_origin; ++i) { signs[i] = sign(kin.W[spurious_linear_letters[i] - 1].real()); }
            return signs;
        };

        Kin<T, KinType::m1> kend{ve};
        auto signs_end = get_signs(kend);

        size_t n_attempts = 1;
        for (; n_attempts < max_number_of_MC_attempts; ++n_attempts) {
            using std::fabs;
            ps.set_energy(distribution(*gen));
            ps.generate();

            // 4 5 -> 1 2 3 Channel
            // 4 5 - > 1 2 3 6   map {{1,2}->1, 3->2, 6->3, 4->4, 5->5}
            std::array<T, 6> v = {ps.s({1, 2}), ps.s({1, 2, 3}), ps.s({3, 6}), ps.s({6, 4}), ps.s({4, 5}), ps.s({1, 2, 5})};

            Kin<T, KinType::m1> kin(v);

            auto signs = get_signs(kin);

            // drop the point if it would result in crossing vanishing linear letters
            // TODO: special treatment of paths that go along zero linear letters
            if (signs != signs_end) continue;

            // do not accept points which are too close to physical singularities or the boundary of the physical region
            // the threshold is of course arbitrary
            if (prod_pow(fabs(kin.W[197]), 2) < T(1e-2/threshold_factor)) { continue; }
            for (auto wi : check_small_letters) {
                if (fabs(kin.W[wi - 1]) < T(1e-2/threshold_factor)) continue;
            }
            for (auto wi : sigma5_letters) {
                if (fabs(kin.W[wi - 1]) < T(1e-1/threshold_factor)) continue;
            }

            if (line_lies_within_physical_phase_space(vb, v) and line_lies_within_physical_phase_space(v, ve)) {
                path_points = {vb, v, ve};
                break;
            }
        }

        if (n_attempts == max_number_of_MC_attempts) {
            if (threshold_factor > 1/(100*std::numeric_limits<T>::epsilon())) {
                std::cerr << "Failed to find a suitable piecewise linear path within phase space to reach a point that is not directly visible from the base "
                             "point after " + std::to_string(max_number_of_MC_attempts) + " attempts!\n";
                std::cerr << "In the last attempt the thresholds were relaxed by factor " << threshold_factor;
                std::cerr << ".\nCheck if the renormalization scale is set to a characeristic scale of the process.\n";
                std::cerr << "If this is already the case, something unexpected has happened.";

                std::cerr << "Please report to the developers the start and end points\n";

                std::cerr.precision(17);

                std::cerr << "{";
                for (auto& it : vb) { std::cerr << it << ","; }
                std::cerr << "}\n{";
                for (auto& it : ve) { std::cerr << it << ","; }
                std::cerr << "}" << std::endl;

                throw std::runtime_error("Monte-Carlo path finding failed!");
            }
            else {
                // attempt again after relaxing thresholds
                return construct_line_segments(vb, ve, threshold_factor *= 10);
            }
        }
    }

    return path_points;
}

template <typename Tkin> std::vector<Tkin> construct_line_segments(const Tkin& vb, const Tkin& ve) {
    // We have a GLOBAL cache of the last call here.
    // It is lazily populated in a thread-safe way.
    static std::pair<Tkin,Tkin> cached_points;
    static std::vector<Tkin> cached_result;

    static std::mutex mut;
    std::lock_guard<std::mutex> lock(mut);

    if(cached_points.first == vb and cached_points.second == ve) {
        return cached_result;
    }
    else {
        std::vector<Tkin> path_points = construct_line_segments(vb, ve, 1);

        cached_result = path_points;
        cached_points.first = vb;
        cached_points.second = ve;
        return cached_result;
    }
}

} // namespace m1_set

template <typename T> PhaseSpacePath<KinType::m1, T>::PhaseSpacePath(const Ptype& start, const Ptype& end) {
    path_points = m1_set::construct_line_segments(start, end);
}

template <typename T> std::tuple<Kin<T,KinType::m1>, std::array<T, 6>, std::array<T, 6>, std::array<T, 10>> PhaseSpacePath<KinType::m1, T>::operator()(T t) const {
    const int Nsegments = path_points.size()-1;

    if (t < T{0} or t >= T{1}) { throw std::invalid_argument("Endpoints of the path should never be reached exactly"); }

    auto [current_segement_start, tbegin, tend] = get_segment_bounds(Nsegments,t);

    const auto& vbegin = path_points.at(current_segement_start);
    const auto& vend = path_points.at(current_segement_start+1);

    Ptype vs;
    Ptype dv;
    Ptype dl;

    for (size_t i = 0; i < vbegin.size(); ++i) {
        T tdiff = tend-tbegin;
        vs[i] = (t-tbegin)/tdiff * vend[i] + (tend-t)/tdiff * vbegin[i];
        dv[i] = (vend[i]-vbegin[i])/tdiff;
        dl[i] = dv[i]/vs[i];
    }

    Kin<T,KinType::m1> kin(vs);

    std::array<T, Nroots> dlr = m1_set::dlog_roots(vs, kin, dl);

    return {kin, dv, dl, dlr};
}

template <typename T> std::vector<T> PhaseSpacePath<KinType::m1, T>::get_path_break_points(const std::vector<size_t>& letters_to_check)  {
    const size_t Nsegments = path_points.size()-1;

    vanishing_letters_per_segment.clear();
    vanishing_letters_per_segment.resize(Nsegments);

    for (int si = 0; si < (int)path_points.size() - 1; ++si) {
        const auto& vbegin = path_points.at(si);
        const auto& vend = path_points.at(si+1);

        T tbegin(si);
        tbegin/=Nsegments;
        T tend(si+1);
        tend/=Nsegments;

        t_break_points.push_back(tbegin);

        if (!letters_to_check.empty()) {
            Ptype dv;

            for (size_t i = 0; i < vbegin.size(); ++i) { dv[i] = (vend[i] - vbegin[i])/(tend-tbegin); }

            for (auto wi : letters_to_check) {
                if (m1_set::sigma5_letters.count(wi+1) > 0) {
                    auto coefficients = m1_set::get_sigma5_taylor_series_coefficients(wi, vbegin, dv);
                    auto roots = solve_quartic(coefficients, tbegin, tend);
                    for(auto ti : roots) {
                        vanishing_letters_per_segment[si][wi].insert(ti);
                        t_break_points.push_back(ti);
                    }
                }
                else {
                    auto [b, a] = m1_set::derivatives_on_line_W(wi, vbegin, dv);

                    if (a == T{}) {
                        // the path degenerated to a line
                        auto s1 = -Kin<T, KinType::m1>{vbegin}.W[wi].real() / b;
                        s1 += tbegin;
                        if (s1 > tbegin and s1 < tend) {
                            t_break_points.push_back(s1);
                            vanishing_letters_per_segment[si][wi].insert(s1);
                        }
                        else if (s1 == tbegin) {
                            vanishing_letters_per_segment[si][wi].insert(s1);
                        }
                    }
                    else {
                        auto sol = solve_quadratic_equation(a / 2, b, Kin<T, KinType::m1>{vbegin}.W[wi].real());

                        if (sol) {
                            auto [s1, s2] = sol.value();
                            s1 += tbegin;
                            s2 += tbegin;
                            if (s1 > tbegin and s1 < tend) {
                                t_break_points.push_back(s1);
                                vanishing_letters_per_segment[si][wi].insert(s1);
                            }
                            else if (s1 == tbegin) {
                                vanishing_letters_per_segment[si][wi].insert(s1);
                            }
                            if (s2 > tbegin and s2 < tend) {
                                t_break_points.push_back(s2);
                                vanishing_letters_per_segment[si][wi].insert(s2);
                            }
                            else if (s2 == tbegin) {
                                vanishing_letters_per_segment[si][wi].insert(s2);
                            }
                        }
                    }
                }
            }
        }
    }
    t_break_points.emplace_back(1);

    {
        bool empty = true;
        for(const auto& it : vanishing_letters_per_segment){
            if(!it.empty()) {
                empty=false;
                break;
            }
        }

        if(empty){
            vanishing_letters_per_segment.clear();
        }
    }


    std::sort(t_break_points.begin(),t_break_points.end());

    return t_break_points;
}

template <typename T> void PhaseSpacePath<KinType::m1, T>::set_divergent_letters(T t, Kin<T, KinType::m1>& k) {
    if (!vanishing_letters_per_segment.empty()) {
        const size_t Nsegments = path_points.size() - 1;

        auto [current_segement_start, tbegin, tend] = get_segment_bounds(Nsegments, t);
        const auto& vbegin = path_points.at(current_segement_start);
        const auto& vend = path_points.at(current_segement_start + 1);

        for (auto& [wi, tlist] : vanishing_letters_per_segment[current_segement_start]) {
            T t0;
            {
                using std::fabs;
                std::set<std::pair<T,T>> distances;
                for (auto& ti : tlist) distances.emplace(fabs(ti - t),ti);
                t0 = (*distances.begin()).second;
            }

            Ptype vsing;
            Ptype dv;
            {
                T tdiff = tend - tbegin;
                for (size_t i = 0; i < vbegin.size(); ++i) {
                    vsing[i] = (t0 - tbegin) / tdiff * vend[i] + (tend - t0) / tdiff * vbegin[i];
                    dv[i] = (vend[i] - vbegin[i]) / tdiff;
                }
            }


            auto difft = t - t0;

            if (m1_set::sigma5_letters.count(wi+1) > 0) {
                auto cs = m1_set::get_sigma5_taylor_series_coefficients(wi, vsing, dv);

                T r{cs[1]}; // the constant is assumed to be zero exactly

                auto tp = difft;
                for (size_t i = 2; i <= 4; ++i) {
                    cs[i] *= tp;
                    r += cs[i];
                    tp *= difft;
                }

                k.W[wi] = sqrt(static_cast<std::complex<T>>(difft*r));
                // other letters get affected by this change, so we need to recompute them
                k.recompute_sigma5_letters();
            }
            else {
                auto [c1, c2] = m1_set::derivatives_on_line_W(wi, vsing, dv);
                c2 /= 2;
                k.W[wi] = difft * (c1 + c2 * difft);
            }
        }
    }
}


template <typename T> void PhaseSpacePath<KinType::m1, T>::set_divergent_letters(T t, T tc, Kin<T, KinType::m1>& k) {
    if (!vanishing_letters_per_segment.empty()) {
        const size_t Nsegments = path_points.size() - 1;

        auto [current_segement_start, tbegin, tend] = get_segment_bounds(Nsegments, t);
        const auto& vbegin = path_points.at(current_segement_start);
        const auto& vend = path_points.at(current_segement_start + 1);

        for (auto& [wi, tlist] : vanishing_letters_per_segment[current_segement_start]) {
            T t0;
            {
                using std::fabs;
                std::set<std::pair<T,T>> distances;
                for (auto& ti : tlist) distances.emplace(fabs(ti - t),ti);
                /*t0 = tlist.at(std::distance(distances.begin(),std::min_element(distances.begin(), distances.end())));*/
                t0 = (*distances.begin()).second;
            }

            // where the subsegement begins and ends
            T t_sub_begin{}, t_sub_end{}; 
            {
                for (size_t i = 0; i < t_break_points.size()-1; ++i) {
                    if (t_break_points.at(i+1) > t){
                        t_sub_end = t_break_points.at(i+1);
                        t_sub_begin = t_break_points.at(i);
                        break;
                    }
                }

                if (t_sub_begin == t_sub_end) { throw std::runtime_error("Inconsistency in construction of integration subsegements!"); }
            }

            Ptype vsing;
            Ptype dv;
            {
                T tdiff = tend - tbegin;
                for (size_t i = 0; i < vbegin.size(); ++i) {
                    vsing[i] = (t0 - tbegin) / tdiff * vend[i] + (tend - t0) / tdiff * vbegin[i];
                    dv[i] = (vend[i] - vbegin[i]) / tdiff;
                }
            }

            auto dtt0 = t - t0;

            if ((t0 == t_sub_begin and tc < T(0)) or (t0 == t_sub_end and tc > T(0))) { dtt0 = -tc; }

            if (m1_set::sigma5_letters.count(wi+1) > 0) {
                auto cs = m1_set::get_sigma5_taylor_series_coefficients(wi, vsing, dv);

                T r{cs[1]}; // the constant is assumed to be zero exactly

                auto tp = dtt0;
                for (size_t i = 2; i <= 4; ++i) {
                    cs[i] *= tp;
                    r += cs[i];
                    tp *= dtt0;
                }

                k.W[wi] = sqrt(static_cast<std::complex<T>>(dtt0*r));
                // other letters get affected by this change, so we need to recompute them
                k.recompute_sigma5_letters();
            }
            else {
                auto [c1, c2] = m1_set::derivatives_on_line_W(wi, vsing, dv);
                c2 /= 2;
                k.W[wi] = dtt0 * (c1 + c2 * dtt0);
            }
        }
    }
}

template <typename T> bool PhaseSpacePath<KinType::m1, T>::can_letters_vanish_Q(const std::vector<size_t>& letters_to_check)  {
    if (letters_to_check.size() == 0) return false;

    for (auto& si : vanishing_letters_per_segment) {
        for (auto& wi : letters_to_check) {
            if (si.count(wi) > 0 ) return true;
        }
    }

    return false;
}

template struct PhaseSpacePath<KinType::m1, double>;

#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
template struct PhaseSpacePath<KinType::m1, dd_real>;
template struct PhaseSpacePath<KinType::m1, qd_real>;
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED


namespace  m1_set {
template std::optional<int> number_of_gram5_zeroes_on_path(const std::array<double,6>& vb, const std::array<double,6>& ve);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
template std::optional<int> number_of_gram5_zeroes_on_path(const std::array<dd_real,6>& vb, const std::array<dd_real,6>& ve);
template std::optional<int> number_of_gram5_zeroes_on_path(const std::array<qd_real,6>& vb, const std::array<qd_real,6>& ve);
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
} //  m1_set

#endif



} // namespace PentagonFunctions

