#pragma once

#include "Kin.h"

#include "PentagonFunctions_config.h"

#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
#include <qd/qd_real.h>
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED

#include <array>
#include <functional>

#include <functional>
#include <unordered_map>

#include <iosfwd>
#include <memory>

// forward declarations
namespace std {
    template <typename T> struct complex;
} // std


namespace PentagonFunctions {

/**
 * Struct which decsribes return of integrator
 *
 */
template <typename T> struct integration_result  {
    std::complex<T> result{};
    double abs_error{};
};

/**
 * The type of function objects which correspond to an individual function
 */
template <typename T, KinType Tk = KinType::m0> using FunctionObjectType = std::function<std::complex<T>(const Kin<T, Tk>&)>;
/**
 * The type of function objects which correspond to an individual function and its error
 */
template <typename T, KinType Tk> using FunctionObjectTypeError = std::function<integration_result<T>(const Kin<T, KinType::m1>&)>;

template <KinType Tk = KinType::m0> struct FunID {
    const int w;    //! Weight
    const int n;    //! Primary index
    const int i{0}; //! Secondary index

    /**
     * Default constructor, which corresponds to F[set_weight,set_n,set_i]
     * The last argument is optional and is require only for some of the functions.
     */
    FunID(int set_weight, int set_n, int set_i = 0);

    FunID(const std::array<int, 2>&);

    FunID(const std::array<int, 3>&);

    std::array<int, 3> indices() const noexcept { return {w, n, i}; }

    KinType get_KinType() const noexcept {return Tk;}

    /**
     * Get a function object which evaluates the function corresponding to *this in numerical type T
     */
    template <typename T> FunctionObjectType<T,Tk> get_evaluator() const;
};

template <KinType Tk1, KinType Tk2> bool constexpr operator==(const FunID<Tk1>& F1, const FunID<Tk2>& F2) {
    if (Tk1 != Tk2) { return false; }
    else {
        return F1.w == F2.w && F1.n == F2.n && F1.i == F2.i;
    }
}

template <KinType Tk1, KinType Tk2> bool constexpr operator<(const FunID<Tk1>& F1, const FunID<Tk2>& F2) {
    return std::make_tuple(Tk1, F1.w, F1.n, F1.i) < std::make_tuple(Tk2, F2.w, F2.n, F2.i);
}
template <KinType Tk1, KinType Tk2> bool constexpr operator>(const FunID<Tk1>& F1, const FunID<Tk2>& F2) {
    return std::make_tuple(Tk1, F1.w, F1.n, F1.i) > std::make_tuple(Tk2, F2.w, F2.n, F2.i);
}



template <KinType Tk1, KinType Tk2> bool constexpr operator!=(const FunID<Tk1>& F1, const FunID<Tk2>& F2) { return !(F1 == F2); }

template <KinType Tk> std::ostream& operator<< (std::ostream&, const FunID<Tk>&);


/**
 * Definition of FunctionID type as an alias for backwards compatibility.
 */
using FunctionID = FunID<KinType::m0>;


/**
 * The map giving a function object corresponding to a particular FunID  
 */
template <typename T, KinType Tk = KinType::m0> extern std::unordered_map<FunID<Tk>, FunctionObjectType<T, Tk>> functions_map;

extern template FunctionObjectType<double> FunID<KinType::m0>::get_evaluator<double>() const;
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
extern template FunctionObjectType<dd_real> FunID<KinType::m0>::get_evaluator<dd_real>() const;
extern template FunctionObjectType<qd_real> FunID<KinType::m0>::get_evaluator<qd_real>() const;
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED

#ifdef PENTAGON_FUNCTIONS_M1_ENABLED 

extern template FunctionObjectType<double,KinType::m1> FunID<KinType::m1>::get_evaluator<double>() const;
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
extern template FunctionObjectType<dd_real,KinType::m1> FunID<KinType::m1>::get_evaluator<dd_real>() const;
extern template FunctionObjectType<qd_real,KinType::m1> FunID<KinType::m1>::get_evaluator<qd_real>() const;
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED

#endif // PENTAGON_FUNCTIONS_M1_ENABLED

} // PentagonFunctions

namespace std {

template <PentagonFunctions::KinType Tk> struct hash<PentagonFunctions::FunID<Tk>> { size_t operator()(const PentagonFunctions::FunID<Tk>& k) const; };


} // namespace std
