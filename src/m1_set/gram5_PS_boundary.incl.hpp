template <typename Tkin, typename T = typename Tkin::value_type> std::array<T,5> gram5_poly_on_path(const Tkin& vb, const Tkin& ve) {
    T z[66];
    z[0] = vb[0] + vb[2];
    z[1] = 2 * z[0] - vb[4];
    z[1] = vb[4] * z[1];
    z[2] = 2 * vb[2];
    z[3] = z[2] - vb[0];
    z[3] = vb[0] * z[3];
    z[4] = prod_pow(vb[2], 2);
    z[1] = (z[1] + z[3]) - z[4];
    z[3] = vb[3] * z[1];
    z[2] = z[2] - vb[5];
    z[4] = vb[0] * z[2];
    z[5] = vb[2] * vb[5];
    z[4] = z[4] - z[5];
    z[6] = vb[4] * vb[5];
    z[7] = z[4] + z[6];
    z[7] = vb[4] * z[7];
    z[8] = vb[0] - vb[2];
    z[9] = vb[2] - vb[5];
    z[10] = -z[8] * z[9];
    z[11] = vb[2] + vb[5];
    z[12] = vb[4] * z[11];
    z[10] = z[10] - z[12];
    z[12] = vb[1] * z[10];
    z[7] = z[7] + z[12];
    z[12] = z[3] + 2 * z[7];
    z[12] = vb[3] * z[12];
    z[13] = prod_pow(vb[5], 2);
    z[5] = z[5] - z[13];
    z[14] = 2 * vb[4];
    z[15] = z[5] * z[14];
    z[16] = 2 * vb[5];
    z[17] = z[16] - vb[2];
    z[18] = vb[2] * z[17];
    z[18] = z[18] - z[13];
    z[19] = vb[1] * z[18];
    z[15] = z[15] - z[19];
    z[15] = vb[1] * z[15];
    z[20] = prod_pow(vb[4], 2);
    z[21] = z[13] * z[20];
    z[12] = (z[15] + z[21]) - z[12];
    z[15] = 2 * z[12];
    z[21] = vb[4] * z[5];
    z[22] = vb[3] * z[10];
    z[19] = (z[19] + z[22]) - z[21];
    z[21] = -ve[1] * z[19];
    z[3] = z[3] + z[7];
    z[7] = -ve[3] * z[3];
    z[22] = vb[1] * z[11];
    z[23] = vb[4] * z[16];
    z[4] = (z[4] + z[23]) - z[22];
    z[22] = vb[4] - vb[0];
    z[24] = z[22] - vb[2];
    z[25] = vb[3] * z[24];
    z[25] = z[25] - z[4];
    z[25] = vb[3] * z[25];
    z[26] = vb[1] * z[5];
    z[27] = vb[4] * z[13];
    z[25] = z[25] + z[26] + z[27];
    z[26] = ve[4] * z[25];
    z[27] = vb[4] * z[17];
    z[28] = vb[1] * z[9];
    z[29] = z[27] + z[28];
    z[29] = vb[1] * z[29];
    z[22] = vb[2] + z[22];
    z[30] = vb[1] * z[22];
    z[31] = vb[4] * z[24];
    z[30] = z[30] - z[31];
    z[31] = vb[3] * z[30];
    z[32] = vb[5] * z[20];
    z[29] = z[29] - (z[31] + z[32]);
    z[31] = -ve[5] * z[29];
    z[32] = z[6] + z[28];
    z[32] = vb[1] * z[32];
    z[33] = z[2] - (vb[0] + vb[4]);
    z[34] = vb[1] * z[33];
    z[35] = 2 * vb[0] - vb[5];
    z[36] = vb[4] * z[35];
    z[34] = z[34] + z[36];
    z[8] = vb[4] + z[8];
    z[36] = vb[3] * z[8];
    z[36] = z[34] + z[36];
    z[36] = vb[3] * z[36];
    z[32] = z[32] - z[36];
    z[36] = ve[2] * z[32];
    z[37] = vb[4] * z[2];
    z[37] = z[37] - z[28];
    z[38] = vb[3] * z[22];
    z[39] = z[37] + z[38];
    z[40] = vb[3] * ve[0];
    z[41] = -z[39] * z[40];
    z[7] = (z[7] + z[21] + z[26] + z[31] + z[36] + z[41]) - z[15];
    z[7] = 2 * z[7];
    z[21] = 3 * z[32];
    z[26] = vb[3] * z[33];
    z[28] = 2 * z[28];
    z[6] = z[26] - (z[6] + z[28]);
    z[26] = -ve[1] * z[6];
    z[31] = 2 * vb[3];
    z[36] = z[8] * z[31];
    z[34] = z[34] + z[36];
    z[36] = -ve[3] * z[34];
    z[14] = z[14] - vb[1];
    z[41] = vb[3] + z[14];
    z[42] = z[40] * z[41];
    z[43] = vb[3] - vb[1];
    z[44] = z[35] + z[43];
    z[44] = vb[3] * z[44];
    z[45] = vb[1] * vb[5];
    z[44] = z[44] - z[45];
    z[45] = -ve[4] * z[44];
    z[46] = vb[1] - vb[4];
    z[47] = vb[1] * z[46];
    z[48] = vb[1] + vb[4];
    z[49] = vb[3] * z[48];
    z[47] = z[47] - z[49];
    z[49] = -ve[5] * z[47];
    z[26] = (z[26] + z[36] + z[45] + z[49]) - (z[21] + z[42]);
    z[36] = 2 * vb[1] - vb[3];
    z[45] = vb[3] * z[36];
    z[49] = prod_pow(vb[1], 2);
    z[45] = z[45] - z[49];
    z[49] = -ve[2] * z[45];
    z[26] = 2 * z[26] + z[49];
    z[26] = ve[2] * z[26];
    z[49] = 3 * z[3];
    z[50] = z[22] * z[31];
    z[37] = z[37] + z[50];
    z[50] = -ve[0] * z[37];
    z[51] = z[24] * z[31];
    z[4] = z[51] - z[4];
    z[51] = ve[4] * z[4];
    z[52] = -ve[1] * z[10];
    z[53] = ve[5] * z[30];
    z[50] = z[49] + z[50] + z[51] + z[52] + z[53];
    z[51] = -ve[3] * z[1];
    z[50] = 2 * z[50] + z[51];
    z[50] = ve[3] * z[50];
    z[51] = 3 * z[29];
    z[27] = (z[27] + z[28]) - z[38];
    z[28] = -ve[1] * z[27];
    z[0] = z[14] - z[0];
    z[38] = vb[3] * z[0];
    z[52] = vb[1] * z[17];
    z[23] = (z[38] + z[52]) - z[23];
    z[38] = -ve[4] * z[23];
    z[52] = z[40] * z[46];
    z[28] = (z[28] + z[38] + z[51]) - z[52];
    z[38] = vb[1] * z[14];
    z[20] = z[38] - z[20];
    z[38] = -ve[5] * z[20];
    z[28] = 2 * z[28] + z[38];
    z[28] = ve[5] * z[28];
    z[38] = 3 * z[19];
    z[53] = z[9] * z[40];
    z[54] = z[38] + z[53];
    z[18] = ve[1] * z[18];
    z[54] = 2 * z[54] - z[18];
    z[54] = ve[1] * z[54];
    z[55] = vb[3] + z[2];
    z[56] = z[40] * z[55];
    z[57] = vb[3] * z[11];
    z[5] = z[5] + z[57];
    z[5] = ve[1] * z[5];
    z[56] = z[56] - z[5];
    z[57] = 3 * z[25];
    z[58] = -(z[56] + z[57]);
    z[16] = z[16] - vb[3];
    z[16] = vb[3] * z[16];
    z[13] = z[16] - z[13];
    z[13] = ve[4] * z[13];
    z[16] = 2 * z[58] - z[13];
    z[16] = ve[4] * z[16];
    z[58] = vb[3] * z[39];
    z[59] = prod_pow(vb[3], 2) * ve[0];
    z[60] = 6 * z[58] + z[59];
    z[60] = ve[0] * z[60];
    z[16] = 6 * z[12] + z[16] + z[26] + z[28] + z[50] + z[54] + z[60];
    z[14] = z[14] + z[31];
    z[14] = ve[0] * z[14];
    z[26] = (z[31] + z[35]) - vb[1];
    z[28] = -ve[4] * z[26];
    z[35] = -ve[1] * z[33];
    z[50] = ve[5] * z[48];
    z[54] = -ve[3] * z[8];
    z[28] = (z[28] + 2 * z[34] + z[35] + z[50] + z[54]) - z[14];
    z[28] = ve[3] * z[28];
    z[35] = ve[1] * z[9];
    z[35] = z[35] + z[40];
    z[50] = 2 * z[6] + z[35];
    z[50] = ve[1] * z[50];
    z[36] = z[36] - vb[4];
    z[54] = -ve[1] * z[36];
    z[60] = vb[1] + vb[3];
    z[61] = ve[4] * z[60];
    z[54] = 2 * z[47] + z[54] + z[61];
    z[54] = ve[5] * z[54];
    z[61] = ve[3] - ve[1];
    z[61] = z[43] * z[61];
    z[61] = z[45] + z[61];
    z[61] = ve[2] * z[61];
    z[62] = 2 * z[40];
    z[41] = z[41] * z[62];
    z[63] = z[44] - z[40];
    z[64] = vb[3] + vb[5];
    z[64] = ve[1] * z[64];
    z[63] = 2 * z[63] + z[64];
    z[63] = ve[4] * z[63];
    z[21] = z[21] + z[28] + z[41] + z[50] + z[54] + z[61] + z[63];
    z[21] = ve[2] * z[21];
    z[2] = z[2] + z[31];
    z[2] = ve[0] * z[2];
    z[11] = ve[1] * z[11];
    z[28] = vb[3] - vb[5];
    z[28] = ve[4] * z[28];
    z[2] = z[2] - (z[11] + z[28]);
    z[11] = -(z[2] + 2 * z[4]);
    z[11] = ve[4] * z[11];
    z[41] = -ve[4] * z[0];
    z[50] = ve[0] * z[46];
    z[54] = ve[1] * z[22];
    z[41] = (z[41] + z[54]) - (2 * z[30] + z[50]);
    z[41] = ve[5] * z[41];
    z[54] = 2 * z[37] + z[40];
    z[54] = ve[0] * z[54];
    z[61] = -ve[0] * z[22];
    z[63] = ve[4] * z[24];
    z[61] = z[1] + z[61] + z[63];
    z[61] = ve[3] * z[61];
    z[63] = ve[0] * z[9];
    z[65] = 2 * z[10] + z[63];
    z[65] = ve[1] * z[65];
    z[11] = (z[11] + z[41] + z[54] + z[61] + z[65]) - z[49];
    z[11] = ve[3] * z[11];
    z[17] = z[17] - vb[3];
    z[17] = ve[1] * z[17];
    z[17] = (z[17] + z[28]) - z[40];
    z[28] = 2 * z[23] - z[17];
    z[28] = ve[4] * z[28];
    z[41] = 2 * z[27] - z[35];
    z[41] = ve[1] * z[41];
    z[49] = ve[4] - ve[1];
    z[54] = -z[46] * z[49];
    z[54] = z[20] + z[54];
    z[54] = ve[5] * z[54];
    z[61] = z[46] * z[62];
    z[28] = (z[28] + z[41] + z[54] + z[61]) - z[51];
    z[28] = ve[5] * z[28];
    z[41] = z[55] * z[62];
    z[5] = (z[13] + z[41] + z[57]) - 2 * z[5];
    z[5] = ve[4] * z[5];
    z[9] = -z[9] * z[62];
    z[9] = (z[9] + z[18]) - z[38];
    z[9] = ve[1] * z[9];
    z[38] = -(3 * z[58] + z[59]);
    z[38] = ve[0] * z[38];
    z[5] = (z[5] + z[9] + z[11] + z[21] + z[28] + z[38]) - z[15];
    z[5] = 2 * z[5];
    z[9] = (ve[1] + z[26]) - 2 * ve[0];
    z[9] = ve[4] * z[9];
    z[11] = ve[0] + z[33];
    z[11] = ve[1] * z[11];
    z[15] = (ve[1] + ve[4]) - z[48];
    z[15] = ve[5] * z[15];
    z[8] = z[8] - (ve[0] + ve[4]);
    z[8] = ve[3] * z[8];
    z[8] = (z[8] + z[9] + z[11] + z[14] + z[15]) - z[34];
    z[8] = ve[3] * z[8];
    z[6] = -(z[6] + z[35]);
    z[6] = ve[1] * z[6];
    z[9] = z[62] - (z[44] + z[64]);
    z[9] = ve[4] * z[9];
    z[11] = z[36] - ve[1];
    z[11] = ve[1] * z[11];
    z[14] = ve[1] - z[60];
    z[14] = ve[4] * z[14];
    z[11] = (z[11] + z[14]) - z[47];
    z[11] = ve[5] * z[11];
    z[6] = (z[6] + z[8] + z[9] + z[11]) - (z[32] + z[42]);
    z[8] = ve[1] + 2 * z[43];
    z[8] = ve[1] * z[8];
    z[9] = -(ve[1] + z[43]);
    z[9] = ve[3] + 2 * z[9];
    z[9] = ve[3] * z[9];
    z[8] = (z[8] + z[9]) - z[45];
    z[8] = ve[2] * z[8];
    z[6] = 2 * z[6] + z[8];
    z[6] = ve[2] * z[6];
    z[2] = z[2] + z[4];
    z[2] = ve[4] * z[2];
    z[0] = (ve[0] + z[0]) - z[49];
    z[0] = ve[4] * z[0];
    z[4] = -(ve[0] + z[22]);
    z[4] = ve[1] * z[4];
    z[0] = z[0] + z[4] + z[30] + z[50];
    z[0] = ve[5] * z[0];
    z[4] = -(z[37] + z[40]);
    z[4] = ve[0] * z[4];
    z[8] = -(z[10] + z[63]);
    z[8] = ve[1] * z[8];
    z[0] = z[0] + z[2] + z[3] + z[4] + z[8];
    z[2] = ve[0] + 2 * z[22];
    z[2] = ve[0] * z[2];
    z[3] = -(ve[0] + z[24]);
    z[3] = ve[4] + 2 * z[3];
    z[3] = ve[4] * z[3];
    z[1] = (z[2] + z[3]) - z[1];
    z[1] = ve[3] * z[1];
    z[0] = 2 * z[0] + z[1];
    z[0] = ve[3] * z[0];
    z[1] = z[17] - z[23];
    z[1] = ve[4] * z[1];
    z[2] = z[35] - z[27];
    z[2] = ve[1] * z[2];
    z[1] = (z[1] + z[2] + z[29]) - z[52];
    z[2] = ve[1] - 2 * z[46];
    z[2] = ve[1] * z[2];
    z[3] = z[46] - ve[1];
    z[3] = ve[4] + 2 * z[3];
    z[3] = ve[4] * z[3];
    z[2] = (z[2] + z[3]) - z[20];
    z[2] = ve[5] * z[2];
    z[1] = 2 * z[1] + z[2];
    z[1] = ve[5] * z[1];
    z[2] = z[19] + z[53];
    z[2] = 2 * z[2] - z[18];
    z[2] = ve[1] * z[2];
    z[3] = -(z[25] + z[56]);
    z[3] = 2 * z[3] - z[13];
    z[3] = ve[4] * z[3];
    z[4] = z[31] * z[39];
    z[4] = z[4] + z[59];
    z[4] = ve[0] * z[4];
    z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[6] + z[12];
    return {z[12], z[7], z[16], z[5], z[0]};
}

/**
 * Hard-coded Sturm chain for a generic polynomial of degree 4, given its coefficients.
 * Assumes c[4] == 1.
 * Returns the chain evaluated at {t=0, t=1} limits
 */
template <typename T> std::optional<std::array<T,10>> Sturm_chain_degree4(const std::array<T,5>& c) {

    std::vector<std::vector<T>> coeffs = {
        {c[0]},
        {c[1]},
        {-c[0], (c[1] * c[3]) / T(16)},
        {c[0] * c[1] * T(-768), c[0] * c[2] * c[3] * T(512), c[0] * prod_pow(c[3], 3) * T(-144), prod_pow(c[1], 2) * c[3] * T(48),
         c[1] * prod_pow(c[2], 2) * T(-64), c[1] * c[2] * prod_pow(c[3], 2) * T(16)},
        {prod_pow(c[0], 3) * T(4), prod_pow(c[0], 2) * c[1] * c[3] * T(-3), prod_pow(c[0], 2) * prod_pow(c[2], 2) * T(-2),
         (prod_pow(c[0], 2) * c[2] * prod_pow(c[3], 2) * T(9)) / T(4), (prod_pow(c[0], 2) * prod_pow(c[3], 4) * T(-27)) / T(64),
         (c[0] * prod_pow(c[1], 2) * c[2] * T(9)) / T(4), (c[0] * prod_pow(c[1], 2) * prod_pow(c[3], 2) * T(-3)) / T(32),
         (c[0] * c[1] * prod_pow(c[2], 2) * c[3] * T(-5)) / T(4), (c[0] * c[1] * c[2] * prod_pow(c[3], 3) * T(9)) / T(32), (c[0] * prod_pow(c[2], 4)) / T(4),
         -(c[0] * prod_pow(c[2], 3) * prod_pow(c[3], 2)) / T(16), (prod_pow(c[1], 4) * T(-27)) / T(64), (prod_pow(c[1], 3) * c[2] * c[3] * T(9)) / T(32),
         -(prod_pow(c[1], 3) * prod_pow(c[3], 3)) / T(16), -(prod_pow(c[1], 2) * prod_pow(c[2], 3)) / T(16),
         (prod_pow(c[1], 2) * prod_pow(c[2], 2) * prod_pow(c[3], 2)) / T(64)},
        {c[0], c[1], c[2], c[3], T(1)},
        {c[1], c[2] * T(2), c[3] * T(3), T(4)},
        {-c[0], (c[1] * c[3]) / T(16), (c[1] * T(-3)) / T(4), (c[2] * c[3]) / T(8), -c[2] / T(2), (prod_pow(c[3], 2) * T(3)) / T(16)},
        {c[0] * c[1] * T(-768), c[0] * c[2] * c[3] * T(512), c[0] * c[2] * T(512), c[0] * prod_pow(c[3], 3) * T(-144), c[0] * prod_pow(c[3], 2) * T(-192),
         prod_pow(c[1], 2) * c[3] * T(48), prod_pow(c[1], 2) * T(-576), c[1] * prod_pow(c[2], 2) * T(-64), c[1] * c[2] * prod_pow(c[3], 2) * T(16),
         c[1] * c[2] * c[3] * T(448), c[1] * prod_pow(c[3], 3) * T(-96), prod_pow(c[2], 3) * T(-128), prod_pow(c[2], 2) * prod_pow(c[3], 2) * T(32)}};

    // Neumaier summation (for improved numerical stability) [doi:10.1002/zamm.19740540106]
    auto sum_coeff = [](const auto& values) -> std::pair<T,T> {
        // trivial cases 
        switch (values.size()) {
            case 0: return {T(0),T(0)};
            case 1: return {values[0],T(0)};
            case 2: return {values[0] + values[1], (fabs(values[0]) + fabs(values[1]))*(T(2)*std::numeric_limits<T>::epsilon())};
            default: break;
        }

        T sum{0};
        T c{0};

        T abserr{0};

        for (const auto& si : values) {
            using std::fabs;
            auto t = sum + si;
            abserr += fabs(si);
            if (fabs(sum) >= fabs(si)) { c += ((sum - t) + si); }
            else {
                c += ((si - t) + sum);
            }
            sum = t;
        }
        sum += c;

        abserr *= (T(2)*std::numeric_limits<T>::epsilon());

        /*if (abserr > fabs(sum)) {*/
            /*std::cerr << "WARNING: bad-conditioned coefficients in the Sturm chain. Number of roots might be calculated incorrectly!\n";*/
            /*std::cerr << "c = " << sum << " +- " << abserr << std::endl;*/
        /*}*/

        return {sum,abserr};
    };

    std::array<T,10> result;

    for (size_t i = 0; i < 9; ++i) {
        using std::fabs;
        auto [sum, err] = sum_coeff(coeffs[i]);
        // ill-conditioned question, no reliable answer about number of roots is possible
        if(err > fabs(sum)){
            return {};
        }
        result[i] = sum;
    }

    result[9] = result[4];

    return result;
}




template <typename Tkin, typename T> std::optional<int> number_of_gram5_zeroes_on_path(const Tkin& vb, const Tkin& ve) {
    auto coeffs_t_poly = gram5_poly_on_path(vb,ve);

    // we make the leading coefficent to be one always
    for (size_t i = 0; i < 5; ++i) {
        coeffs_t_poly.at(i) /= coeffs_t_poly.at(4);
    }

    auto sturm_chain_coeffs_on_limits = Sturm_chain_degree4(coeffs_t_poly);

    if(!sturm_chain_coeffs_on_limits) return {};

    // if the coefficients of the Sturm chain were ill-conditioned, we cannot answer the question reliably

    int n_sign_flips = 0; 

    int last_sign;

    last_sign = sign(sturm_chain_coeffs_on_limits.value()[0]);
    for (int i = 1; i < 5; ++i) {
        int s1 = sign(sturm_chain_coeffs_on_limits.value()[i]);
        if(last_sign != s1){
            last_sign = s1;
            ++n_sign_flips;
        }
    }

    last_sign = sign(sturm_chain_coeffs_on_limits.value()[5]);
    for (size_t i = 6; i < 10; ++i) {
        int s1 = sign(sturm_chain_coeffs_on_limits.value()[i]);
        if(last_sign != s1){
            last_sign = s1;
            --n_sign_flips;
        }
    }

    return n_sign_flips;
}



