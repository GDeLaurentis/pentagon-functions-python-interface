#include "DLog.h"

namespace PentagonFunctions {
namespace m1_set {

#include "derivatives.incl.hpp"


template <typename T> T dlog_W1 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[2];
z[0] = 1 / (k.W[0]).real();
return dl[0] * k.v[0] * z[0];
}

template <typename T> T dlog_W2 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[2];
z[0] = 1 / (k.W[1]).real();
return dl[1] * k.v[1] * z[0];
}

template <typename T> T dlog_W3 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[2]).real();
z[1] = dl[0] * k.v[0];
z[2] = dl[4] * k.v[4];
z[3] = -dl[2] * k.v[2];
z[4] = -dl[1] * k.v[1];
z[1] = z[1] + z[2] + z[3] + z[4];
return z[0] * z[1];
}

template <typename T> T dlog_W4 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[3]).real();
z[1] = dl[0] * k.v[0];
z[2] = -dl[5] * k.v[5];
z[3] = -dl[4] * k.v[4];
z[4] = dl[2] * k.v[2];
z[1] = z[1] + z[2] + z[3] + z[4];
return z[0] * z[1];
}

template <typename T> T dlog_W5 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[2];
z[0] = 1 / (k.W[4]).real();
return dl[5] * k.v[5] * z[0];
}

template <typename T> T dlog_W6 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[2];
z[0] = 1 / (k.W[5]).real();
return dl[2] * k.v[2] * z[0];
}

template <typename T> T dlog_W7 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[4];
z[0] = 1 / (k.W[6]).real();
z[1] = -dl[2] * k.v[2];
z[2] = -dl[3] * k.v[3];
z[3] = dl[5] * k.v[5];
z[1] = z[1] + z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W8 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[7]).real();
z[1] = dl[0] * k.v[0];
z[2] = -dl[5] * k.v[5];
z[3] = dl[3] * k.v[3];
z[4] = -dl[1] * k.v[1];
z[1] = z[1] + z[2] + z[3] + z[4];
return z[0] * z[1];
}

template <typename T> T dlog_W9 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[2];
z[0] = 1 / (k.W[8]).real();
return dl[3] * k.v[3] * z[0];
}

template <typename T> T dlog_W10 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[4];
z[0] = 1 / (k.W[9]).real();
z[1] = dl[1] * k.v[1];
z[2] = -dl[3] * k.v[3];
z[3] = -dl[4] * k.v[4];
z[1] = z[1] + z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W11 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[2];
z[0] = 1 / (k.W[10]).real();
return dl[4] * k.v[4] * z[0];
}

template <typename T> T dlog_W12 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[11]).real();
z[1] = -dl[0] * k.v[0];
z[2] = dl[1] * k.v[1];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W13 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[4];
z[0] = 1 / (k.W[12]).real();
z[1] = -dl[1] * k.v[1];
z[2] = -dl[2] * k.v[2];
z[3] = dl[4] * k.v[4];
z[1] = z[1] + z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W14 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[4];
z[0] = 1 / (k.W[13]).real();
z[1] = dl[2] * k.v[2];
z[2] = -dl[4] * k.v[4];
z[3] = -dl[5] * k.v[5];
z[1] = z[1] + z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W15 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[14]).real();
z[1] = -dl[0] * k.v[0];
z[2] = dl[5] * k.v[5];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W16 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[15]).real();
z[1] = -dl[3] * k.v[3];
z[2] = dl[5] * k.v[5];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W17 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[16]).real();
z[1] = dl[3] * k.v[3];
z[2] = -dl[5] * k.v[5];
z[3] = -dl[1] * k.v[1];
z[4] = dl[2] * k.v[2];
z[5] = dl[0] * k.v[0];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
return z[0] * z[1];
}

template <typename T> T dlog_W18 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[4];
z[0] = 1 / (k.W[17]).real();
z[1] = dl[0] * k.v[0];
z[2] = -dl[1] * k.v[1];
z[3] = -dl[2] * k.v[2];
z[1] = z[1] + z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W19 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[18]).real();
z[1] = -dl[2] * k.v[2];
z[2] = -dl[3] * k.v[3];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W20 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[19]).real();
z[1] = -dl[1] * k.v[1];
z[2] = dl[4] * k.v[4];
z[3] = dl[3] * k.v[3];
z[4] = -dl[2] * k.v[2];
z[1] = z[1] + z[2] + z[3] + z[4];
return z[0] * z[1];
}

template <typename T> T dlog_W21 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[20]).real();
z[1] = -dl[1] * k.v[1];
z[2] = dl[4] * k.v[4];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W22 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[21]).real();
z[1] = dl[2] * k.v[2];
z[2] = -dl[5] * k.v[5];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W23 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[22]).real();
z[1] = dl[2] * k.v[2];
z[2] = -dl[5] * k.v[5];
z[3] = -dl[4] * k.v[4];
z[4] = dl[3] * k.v[3];
z[1] = z[1] + z[2] + z[3] + z[4];
return z[0] * z[1];
}

template <typename T> T dlog_W24 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[23]).real();
z[1] = -dl[3] * k.v[3];
z[2] = -dl[4] * k.v[4];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W25 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[4];
z[0] = 1 / (k.W[24]).real();
z[1] = dl[0] * k.v[0];
z[2] = -dl[4] * k.v[4];
z[3] = -dl[5] * k.v[5];
z[1] = z[1] + z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W26 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[25]).real();
z[1] = dl[4] * k.v[4];
z[2] = -dl[5] * k.v[5];
z[3] = -dl[1] * k.v[1];
z[4] = dl[3] * k.v[3];
z[5] = dl[0] * k.v[0];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
return z[0] * z[1];
}

template <typename T> T dlog_W27 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[26]).real();
z[1] = dl[1] * k.v[1];
z[2] = -dl[3] * k.v[3];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W28 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[27]).real();
z[1] = -dl[0] + -dl[3];
z[1] = k.v[0] * k.v[3] * z[1];
z[2] = dl[1] + dl[5];
z[2] = k.v[1] * k.v[5] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W29 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[28]).real();
z[1] = dl[2] * k.v[2];
z[2] = -dl[4] * k.v[4];
z[3] = -dl[5] * k.v[5];
z[4] = k.v[2] + -k.v[4] + -k.v[5];
z[4] = dl[1] * z[4];
z[1] = z[1] + z[2] + z[3] + z[4];
z[1] = k.v[1] * z[1];
z[2] = dl[0] * k.v[0];
z[3] = dl[3] * k.v[0];
z[3] = z[2] + z[3];
z[3] = k.v[3] * z[3];
z[4] = dl[4] * k.v[0];
z[2] = z[2] + z[4];
z[2] = k.v[4] * z[2];
z[1] = z[1] + z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W30 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[4];
z[0] = 1 / (k.W[29]).real();
z[1] = dl[0] + dl[1];
z[1] = k.v[0] * z[1];
z[2] = -dl[1] + -dl[2];
z[2] = k.v[2] * z[2];
z[3] = dl[1] * k.v[1];
z[1] = z[1] + z[2] + -2 * z[3];
z[1] = k.v[1] * z[1];
z[2] = -dl[0] + -dl[4];
z[2] = k.v[0] * z[2];
z[3] = dl[1] + dl[4];
z[3] = k.v[1] * z[3];
z[2] = z[2] + z[3];
z[2] = k.v[4] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W31 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[30]).real();
z[1] = -dl[1] * k.v[1];
z[2] = dl[4] * k.v[4];
z[3] = -k.v[1] + k.v[4];
z[3] = dl[5] * z[3];
z[4] = -dl[2] + -dl[5];
z[4] = k.v[2] * z[4];
z[1] = z[1] + z[2] + z[3] + z[4];
z[1] = k.v[5] * z[1];
z[2] = dl[0] + dl[3];
z[2] = k.v[3] * z[2];
z[3] = dl[0] + dl[2];
z[3] = k.v[2] * z[3];
z[2] = z[2] + z[3];
z[2] = k.v[0] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W32 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[31]).real();
z[1] = dl[2] * k.v[2];
z[2] = dl[1] * k.v[1];
z[3] = -dl[4] * k.v[4];
z[4] = k.v[2] + -k.v[4];
z[5] = k.v[1] + z[4];
z[5] = dl[5] * z[5];
z[3] = z[1] + z[2] + z[3] + z[5];
z[3] = k.v[5] * z[3];
z[5] = -k.v[1] + -2 * z[4];
z[1] = z[1] * z[5];
z[2] = -z[2] * z[4];
z[4] = prod_pow(k.v[4], 2);
z[5] = k.v[2] * k.v[4];
z[4] = -z[4] + z[5];
z[5] = k.v[1] * k.v[4];
z[4] = 2 * z[4] + z[5];
z[4] = dl[4] * z[4];
z[5] = -dl[0] + -dl[3];
z[5] = k.v[0] * k.v[3] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
return z[0] * z[1];
}

template <typename T> T dlog_W33 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[4];
z[0] = 1 / (k.W[32]).real();
z[1] = -dl[4] + -dl[5];
z[1] = k.v[4] * z[1];
z[2] = dl[0] + dl[5];
z[2] = k.v[0] * z[2];
z[3] = dl[5] * k.v[5];
z[1] = z[1] + z[2] + -2 * z[3];
z[1] = k.v[5] * z[1];
z[2] = -dl[0] + -dl[2];
z[2] = k.v[0] * z[2];
z[3] = dl[2] + dl[5];
z[3] = k.v[5] * z[3];
z[2] = z[2] + z[3];
z[2] = k.v[2] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W34 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[33]).real();
z[1] = -dl[1] + -dl[4];
z[1] = k.v[1] * z[1];
z[2] = dl[0] + dl[4];
z[2] = k.v[0] * z[2];
z[1] = z[1] + z[2];
z[1] = k.v[4] * z[1];
z[2] = dl[1] + dl[2];
z[2] = k.v[1] * k.v[2] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W35 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[34]).real();
z[1] = k.v[0] + -2 * k.v[1] + -k.v[2] + k.v[4] + k.v[5];
z[1] = dl[1] * z[1];
z[2] = dl[4] * k.v[4];
z[3] = dl[5] * k.v[5];
z[4] = dl[0] * k.v[0];
z[5] = -dl[2] * k.v[2];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
z[1] = k.v[1] * z[1];
z[3] = -dl[0] * k.v[4];
z[4] = -dl[0] + -dl[3];
z[4] = k.v[3] * z[4];
z[2] = -z[2] + z[3] + z[4];
z[2] = k.v[0] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W36 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[4];
z[0] = 1 / (k.W[35]).real();
z[1] = -dl[1] + -dl[5];
z[1] = k.v[5] * z[1];
z[2] = dl[1] * k.v[1];
z[1] = z[1] + -2 * z[2];
z[1] = k.v[1] * z[1];
z[2] = dl[0] + dl[1];
z[2] = k.v[1] * z[2];
z[3] = dl[0] + dl[3];
z[3] = k.v[3] * z[3];
z[2] = z[2] + z[3];
z[2] = k.v[0] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W37 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[36]).real();
z[1] = 2 * k.v[2];
z[2] = k.v[0] + -z[1];
z[2] = dl[2] * z[2];
z[3] = dl[0] * k.v[0];
z[4] = -dl[1] + -dl[2];
z[4] = k.v[1] * z[4];
z[2] = z[2] + z[3] + z[4];
z[2] = k.v[2] * z[2];
z[3] = dl[2] + dl[4];
z[1] = z[1] * z[3];
z[3] = dl[1] + dl[4];
z[3] = k.v[1] * z[3];
z[4] = dl[4] * k.v[4];
z[1] = z[1] + z[3] + -2 * z[4];
z[1] = k.v[4] * z[1];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W38 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[37]).real();
z[1] = k.v[1] + k.v[3] + -k.v[4];
z[1] = dl[0] * z[1];
z[2] = dl[4] * k.v[4];
z[3] = dl[1] * k.v[1];
z[4] = dl[3] * k.v[3];
z[5] = dl[0] + dl[2];
z[5] = k.v[2] * z[5];
z[1] = z[1] + -z[2] + z[3] + z[4] + z[5];
z[1] = k.v[0] * z[1];
z[4] = dl[2] * k.v[2];
z[2] = -z[2] + z[4];
z[4] = k.v[2] + -k.v[4];
z[5] = -k.v[1] + -z[4];
z[5] = dl[5] * z[5];
z[5] = -z[2] + -z[3] + z[5];
z[5] = k.v[5] * z[5];
z[2] = -k.v[1] * z[2];
z[4] = -2 * k.v[1] + -z[4];
z[3] = z[3] * z[4];
z[1] = z[1] + z[2] + z[3] + z[5];
return z[0] * z[1];
}

template <typename T> T dlog_W39 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[7];
z[0] = 1 / (k.W[38]).real();
z[1] = dl[4] * k.v[4];
z[2] = dl[1] * k.v[1];
z[3] = -dl[3] * k.v[3];
z[4] = -k.v[1] + k.v[4];
z[5] = -k.v[3] + -z[4];
z[5] = dl[0] * z[5];
z[3] = -z[1] + z[2] + z[3] + z[5];
z[3] = k.v[0] * z[3];
z[5] = dl[2] * k.v[2];
z[2] = z[2] + z[5];
z[5] = -dl[2] * k.v[1];
z[5] = -z[2] + z[5];
z[5] = k.v[2] * z[5];
z[1] = -z[1] + z[2];
z[2] = k.v[1] + k.v[2];
z[2] = dl[4] * z[2];
z[2] = z[1] + z[2];
z[2] = k.v[4] * z[2];
z[6] = -prod_pow(k.v[1], 2) * dl[1];
z[2] = z[2] + z[5] + z[6];
z[4] = k.v[2] + -z[4];
z[4] = dl[5] * z[4];
z[1] = z[1] + z[4];
z[1] = k.v[5] * z[1];
z[1] = z[1] + 2 * z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W40 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[8];
z[0] = 1 / (k.W[39]).real();
z[1] = k.v[2] + -k.v[4];
z[2] = k.v[5] * z[1];
z[3] = prod_pow(k.v[5], 2);
z[2] = z[2] + -z[3];
z[2] = dl[5] * z[2];
z[3] = dl[4] * k.v[4];
z[1] = -k.v[5] + z[1];
z[1] = z[1] * z[3];
z[4] = k.v[2] + -k.v[5];
z[5] = -k.v[4] + z[4];
z[6] = dl[2] * k.v[2];
z[7] = -z[5] * z[6];
z[1] = z[1] + z[2] + z[7];
z[2] = dl[5] * k.v[5];
z[2] = z[2] + -z[6];
z[6] = -dl[3] * k.v[3];
z[4] = -k.v[3] + -z[4];
z[4] = dl[0] * z[4];
z[4] = z[2] + z[4] + z[6];
z[4] = k.v[0] * z[4];
z[5] = -dl[1] * z[5];
z[2] = z[2] + z[3] + z[5];
z[2] = k.v[1] * z[2];
z[1] = 2 * z[1] + z[2] + z[4];
return z[0] * z[1];
}

template <typename T> T dlog_W41 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[7];
z[0] = 1 / (k.W[40]).real();
z[1] = dl[1] * k.v[1];
z[2] = dl[0] * k.v[0];
z[1] = z[1] + -z[2];
z[3] = -k.v[0] + k.v[1];
z[4] = k.v[2] + -k.v[4] + -2 * k.v[5] + -z[3];
z[4] = dl[5] * z[4];
z[5] = -dl[4] * k.v[4];
z[6] = dl[2] * k.v[2];
z[4] = -z[1] + z[4] + z[5] + z[6];
z[4] = k.v[5] * z[4];
z[5] = -dl[4] * z[3];
z[5] = -z[1] + z[5];
z[5] = k.v[4] * z[5];
z[3] = dl[2] * z[3];
z[1] = z[1] + z[3];
z[1] = k.v[2] * z[1];
z[3] = dl[3] * k.v[0];
z[2] = z[2] + z[3];
z[2] = k.v[3] * z[2];
z[1] = z[1] + z[2] + z[4] + z[5];
return z[0] * z[1];
}

template <typename T> T dlog_W42 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[41]).real();
z[1] = 2 * k.v[4];
z[2] = k.v[0] + -z[1];
z[2] = dl[4] * z[2];
z[3] = dl[0] * k.v[0];
z[4] = -dl[4] + -dl[5];
z[4] = k.v[5] * z[4];
z[2] = z[2] + z[3] + z[4];
z[2] = k.v[4] * z[2];
z[3] = dl[2] + dl[4];
z[1] = z[1] * z[3];
z[3] = dl[2] + dl[5];
z[3] = k.v[5] * z[3];
z[4] = dl[2] * k.v[2];
z[1] = z[1] + z[3] + -2 * z[4];
z[1] = k.v[2] * z[1];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W43 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[4];
z[0] = 1 / (k.W[42]).real();
z[1] = -dl[1] + -dl[5];
z[1] = k.v[1] * z[1];
z[2] = dl[0] + dl[5];
z[2] = k.v[0] * z[2];
z[3] = dl[5] * k.v[5];
z[1] = z[1] + z[2] + -2 * z[3];
z[1] = k.v[5] * z[1];
z[2] = dl[0] + dl[3];
z[2] = k.v[0] * k.v[3] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W44 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[43]).real();
z[1] = k.v[1] + -k.v[4] + -2 * k.v[5];
z[1] = dl[5] * z[1];
z[2] = -dl[4] * k.v[4];
z[3] = dl[1] * k.v[1];
z[4] = dl[2] + dl[5];
z[4] = k.v[2] * z[4];
z[5] = dl[0] + dl[5];
z[5] = k.v[0] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
z[1] = k.v[5] * z[1];
z[2] = -dl[0] + -dl[3];
z[2] = k.v[3] * z[2];
z[3] = -dl[0] + -dl[2];
z[3] = k.v[2] * z[3];
z[2] = z[2] + z[3];
z[2] = k.v[0] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W45 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[44]).real();
z[1] = -dl[2] + -dl[5];
z[1] = k.v[5] * z[1];
z[2] = dl[0] + dl[2];
z[2] = k.v[0] * z[2];
z[1] = z[1] + z[2];
z[1] = k.v[2] * z[1];
z[2] = dl[4] + dl[5];
z[2] = k.v[4] * k.v[5] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W46 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[45]).real();
z[1] = -dl[0] + -dl[5];
z[1] = k.v[0] * z[1];
z[2] = dl[4] + dl[5];
z[2] = k.v[4] * z[2];
z[1] = z[1] + z[2];
z[1] = k.v[5] * z[1];
z[2] = dl[0] + dl[2];
z[2] = k.v[0] * k.v[2] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W47 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[46]).real();
z[1] = 2 * k.v[4];
z[2] = k.v[2] + -z[1];
z[2] = dl[4] * z[2];
z[3] = dl[2] * k.v[2];
z[4] = -dl[4] + -dl[5];
z[4] = k.v[5] * z[4];
z[2] = z[2] + z[3] + z[4];
z[2] = k.v[4] * z[2];
z[3] = dl[0] + dl[4];
z[1] = z[1] * z[3];
z[3] = dl[0] + dl[5];
z[3] = k.v[5] * z[3];
z[4] = dl[0] * k.v[0];
z[1] = z[1] + z[3] + -2 * z[4];
z[1] = k.v[0] * z[1];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W48 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[47]).real();
z[1] = dl[3] * k.v[3];
z[2] = -dl[4] * k.v[4];
z[3] = -k.v[3] + -k.v[4];
z[3] = dl[5] * z[3];
z[4] = dl[1] + dl[5];
z[4] = k.v[1] * z[4];
z[2] = -z[1] + z[2] + z[3] + z[4];
z[2] = k.v[5] * z[2];
z[3] = -dl[0] * k.v[3];
z[4] = -dl[0] + -dl[2];
z[4] = k.v[2] * z[4];
z[1] = -z[1] + z[3] + z[4];
z[1] = k.v[0] * z[1];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W49 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[7];
z[0] = 1 / (k.W[48]).real();
z[1] = dl[4] * k.v[4];
z[2] = -dl[3] * k.v[4];
z[3] = dl[1] + dl[3];
z[3] = k.v[1] * z[3];
z[4] = -dl[0] + -dl[3];
z[4] = k.v[0] * z[4];
z[2] = -z[1] + z[2] + z[3] + 2 * z[4];
z[2] = k.v[3] * z[2];
z[3] = dl[1] * k.v[1];
z[1] = -z[1] + z[3];
z[3] = dl[1] * k.v[4];
z[3] = -z[1] + z[3];
z[3] = k.v[1] * z[3];
z[4] = k.v[1] + -k.v[4];
z[5] = -k.v[0] + z[4];
z[5] = dl[0] * z[5];
z[5] = z[1] + z[5];
z[5] = k.v[0] * z[5];
z[6] = -prod_pow(k.v[4], 2) * dl[4];
z[3] = z[3] + z[5] + z[6];
z[4] = -dl[2] * z[4];
z[5] = dl[2] + dl[3];
z[5] = k.v[3] * z[5];
z[1] = -z[1] + z[4] + z[5];
z[1] = k.v[2] * z[1];
z[4] = dl[0] + dl[5];
z[4] = k.v[0] * k.v[5] * z[4];
z[1] = z[1] + z[2] + 2 * z[3] + z[4];
return z[0] * z[1];
}

template <typename T> T dlog_W50 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[49]).real();
z[1] = -dl[5] * k.v[5];
z[2] = dl[2] * k.v[2];
z[3] = k.v[2] + -k.v[5];
z[3] = dl[3] * z[3];
z[4] = -dl[3] + -dl[4];
z[4] = k.v[4] * z[4];
z[1] = z[1] + z[2] + z[3] + z[4];
z[1] = k.v[3] * z[1];
z[2] = -dl[0] + -dl[1];
z[2] = k.v[1] * z[2];
z[3] = -dl[0] + -dl[2];
z[3] = k.v[2] * z[3];
z[4] = dl[0] + dl[3];
z[4] = k.v[3] * z[4];
z[5] = dl[0] + dl[4];
z[5] = k.v[4] * z[5];
z[2] = z[2] + z[3] + 2 * z[4] + z[5];
z[2] = k.v[0] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W51 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[50]).real();
z[1] = -dl[0] + -dl[5];
z[1] = k.v[5] * z[1];
z[2] = dl[0] + dl[2];
z[2] = k.v[2] * z[2];
z[3] = dl[0] + dl[3];
z[3] = k.v[3] * z[3];
z[1] = z[1] + z[2] + 2 * z[3];
z[1] = k.v[0] * z[1];
z[2] = -dl[1] + -dl[3];
z[2] = k.v[1] * z[2];
z[3] = -dl[2] + -dl[3];
z[3] = k.v[2] * z[3];
z[2] = z[2] + z[3];
z[2] = k.v[3] * z[2];
z[3] = dl[3] + dl[4];
z[3] = k.v[3] * z[3];
z[4] = -dl[0] + -dl[4];
z[4] = k.v[0] * z[4];
z[3] = z[3] + z[4];
z[3] = k.v[4] * z[3];
z[1] = z[1] + z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W52 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[4];
z[0] = 1 / (k.W[51]).real();
z[1] = -k.v[1] + k.v[3] + -2 * k.v[5];
z[1] = dl[5] * z[1];
z[2] = dl[3] * k.v[3];
z[3] = -dl[1] * k.v[1];
z[1] = z[1] + z[2] + z[3];
z[1] = k.v[5] * z[1];
z[3] = dl[0] * k.v[3];
z[2] = z[2] + z[3];
z[2] = k.v[0] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W53 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[4];
z[0] = 1 / (k.W[52]).real();
z[1] = -2 * k.v[1] + k.v[3] + -k.v[5];
z[1] = dl[1] * z[1];
z[2] = dl[3] * k.v[3];
z[3] = -dl[5] * k.v[5];
z[1] = z[1] + z[2] + z[3];
z[1] = k.v[1] * z[1];
z[3] = dl[0] * k.v[3];
z[2] = z[2] + z[3];
z[2] = k.v[0] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W54 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[8];
z[0] = 1 / (k.W[53]).real();
z[1] = dl[5] * k.v[5];
z[2] = dl[0] * k.v[0];
z[3] = dl[2] * k.v[2];
z[4] = dl[4] * k.v[4];
z[5] = k.v[2] + -k.v[5];
z[6] = -2 * k.v[0] + k.v[4] + -z[5];
z[6] = dl[3] * z[6];
z[4] = z[1] + -2 * z[2] + -z[3] + z[4] + z[6];
z[4] = k.v[3] * z[4];
z[6] = dl[0] + dl[5];
z[6] = k.v[5] * z[6];
z[7] = -dl[0] * k.v[2];
z[2] = -z[2] + -z[3] + z[6] + z[7];
z[2] = k.v[0] * z[2];
z[5] = -z[3] * z[5];
z[6] = -prod_pow(k.v[5], 2) * dl[5];
z[1] = k.v[2] * z[1];
z[1] = z[1] + z[2] + z[5] + z[6];
z[2] = -dl[4] + -dl[5];
z[2] = k.v[5] * z[2];
z[5] = dl[4] * k.v[2];
z[2] = z[2] + z[3] + z[5];
z[2] = k.v[4] * z[2];
z[3] = dl[0] + dl[1];
z[3] = k.v[0] * k.v[1] * z[3];
z[1] = 2 * z[1] + z[2] + z[3] + z[4];
return z[0] * z[1];
}

template <typename T> T dlog_W55 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[54]).real();
z[1] = -dl[1] + -dl[2];
z[1] = k.v[2] * z[1];
z[2] = dl[3] * k.v[3];
z[3] = -dl[1] * k.v[3];
z[4] = dl[1] + dl[5];
z[4] = k.v[5] * z[4];
z[1] = z[1] + -z[2] + z[3] + z[4];
z[1] = k.v[1] * z[1];
z[3] = -dl[4] * k.v[4];
z[4] = -k.v[3] + -k.v[4];
z[4] = dl[0] * z[4];
z[2] = -z[2] + z[3] + z[4];
z[2] = k.v[0] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W56 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[55]).real();
z[1] = 2 * k.v[0];
z[2] = dl[0] + dl[2];
z[2] = z[1] * z[2];
z[3] = -dl[1] + -dl[2];
z[3] = k.v[1] * z[3];
z[4] = dl[2] + dl[4];
z[4] = k.v[4] * z[4];
z[5] = dl[2] * k.v[2];
z[2] = z[2] + z[3] + z[4] + -2 * z[5];
z[2] = k.v[2] * z[2];
z[3] = dl[0] + dl[1];
z[3] = k.v[1] * z[3];
z[1] = -dl[0] * z[1];
z[1] = z[1] + z[3];
z[1] = k.v[0] * z[1];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W57 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[56]).real();
z[1] = -dl[0] + -dl[1];
z[1] = k.v[0] * z[1];
z[2] = dl[1] + dl[2];
z[2] = k.v[2] * z[2];
z[1] = z[1] + z[2];
z[1] = k.v[1] * z[1];
z[2] = dl[0] + dl[4];
z[2] = k.v[0] * k.v[4] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W58 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[4];
z[0] = 1 / (k.W[57]).real();
z[1] = dl[0] + dl[2];
z[1] = k.v[2] * z[1];
z[2] = dl[0] + dl[1];
z[2] = k.v[1] * z[2];
z[3] = dl[0] * k.v[0];
z[1] = z[1] + z[2] + -2 * z[3];
z[1] = k.v[0] * z[1];
z[2] = -dl[1] + -dl[4];
z[2] = k.v[1] * z[2];
z[3] = dl[0] + dl[4];
z[3] = k.v[0] * z[3];
z[2] = z[2] + z[3];
z[2] = k.v[4] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W59 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[8];
z[0] = 1 / (k.W[58]).real();
z[1] = 2 * k.v[1];
z[2] = dl[0] + dl[1];
z[2] = z[1] * z[2];
z[3] = -2 * k.v[0] + -k.v[2] + k.v[5];
z[3] = dl[0] * z[3];
z[4] = -dl[2] * k.v[2];
z[5] = -dl[0] + -dl[4];
z[5] = k.v[4] * z[5];
z[6] = dl[5] * k.v[5];
z[7] = -dl[0] + -dl[3];
z[7] = k.v[3] * z[7];
z[2] = z[2] + z[3] + z[4] + z[5] + z[6] + 2 * z[7];
z[2] = k.v[0] * z[2];
z[3] = dl[1] + dl[4];
z[3] = k.v[4] * z[3];
z[1] = -dl[1] * z[1];
z[4] = dl[1] + dl[3];
z[4] = k.v[3] * z[4];
z[1] = z[1] + z[3] + z[4];
z[1] = k.v[1] * z[1];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W60 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[59]).real();
z[1] = -dl[0] + -dl[5];
z[1] = k.v[5] * z[1];
z[2] = dl[0] + dl[3];
z[2] = k.v[3] * z[2];
z[1] = z[1] + 2 * z[2];
z[1] = k.v[0] * z[1];
z[2] = -dl[1] + -dl[3];
z[2] = k.v[1] * k.v[3] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W61 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[4];
z[0] = 1 / (k.W[60]).real();
z[1] = k.v[2] + -2 * k.v[4];
z[1] = dl[4] * z[1];
z[2] = dl[2] * k.v[2];
z[3] = dl[0] + dl[4];
z[3] = k.v[0] * z[3];
z[1] = z[1] + z[2] + z[3];
z[1] = k.v[4] * z[1];
z[2] = -dl[0] + -dl[1];
z[2] = k.v[0] * z[2];
z[3] = dl[1] + dl[4];
z[3] = k.v[4] * z[3];
z[2] = z[2] + z[3];
z[2] = k.v[1] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W62 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[61]).real();
z[1] = k.v[0] + k.v[1] + -k.v[4];
z[1] = dl[3] * z[1];
z[2] = dl[4] * k.v[4];
z[3] = dl[1] * k.v[1];
z[4] = dl[0] * k.v[0];
z[5] = dl[2] + dl[3];
z[5] = k.v[2] * z[5];
z[1] = z[1] + -z[2] + z[3] + z[4] + z[5];
z[1] = k.v[3] * z[1];
z[4] = dl[2] * k.v[2];
z[2] = -z[2] + z[4];
z[4] = k.v[2] + -k.v[4];
z[5] = -k.v[1] + -z[4];
z[5] = dl[5] * z[5];
z[5] = -z[2] + -z[3] + z[5];
z[5] = k.v[5] * z[5];
z[2] = -k.v[1] * z[2];
z[4] = -2 * k.v[1] + -z[4];
z[3] = z[3] * z[4];
z[1] = z[1] + z[2] + z[3] + z[5];
return z[0] * z[1];
}

template <typename T> T dlog_W63 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[7];
z[0] = 1 / (k.W[62]).real();
z[1] = dl[1] * k.v[1];
z[2] = dl[4] * k.v[4];
z[1] = z[1] + -z[2];
z[2] = k.v[1] + -k.v[4];
z[3] = k.v[2] + z[2];
z[3] = dl[5] * z[3];
z[4] = dl[2] * k.v[2];
z[3] = z[1] + z[3] + z[4];
z[3] = k.v[5] * z[3];
z[2] = k.v[0] + z[2];
z[5] = -dl[3] * z[2];
z[5] = -z[1] + z[5];
z[5] = k.v[3] * z[5];
z[6] = -dl[3] * k.v[3];
z[1] = -z[1] + z[6];
z[1] = k.v[2] * z[1];
z[2] = -2 * k.v[2] + -k.v[3] + -z[2];
z[2] = z[2] * z[4];
z[4] = -k.v[2] + -k.v[3];
z[4] = dl[0] * k.v[0] * z[4];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
return z[0] * z[1];
}

template <typename T> T dlog_W64 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[8];
z[0] = 1 / (k.W[63]).real();
z[1] = dl[5] * k.v[5];
z[2] = dl[2] * k.v[2];
z[1] = z[1] + -z[2];
z[3] = dl[4] * k.v[4];
z[4] = dl[1] * k.v[1];
z[5] = -k.v[2] + k.v[5];
z[6] = k.v[1] + -z[5];
z[6] = dl[4] * z[6];
z[7] = -dl[0] + -dl[4];
z[7] = k.v[0] * z[7];
z[4] = -z[1] + -2 * z[3] + z[4] + z[6] + z[7];
z[4] = k.v[4] * z[4];
z[6] = -dl[0] * k.v[0];
z[5] = -k.v[0] + -k.v[4] + -z[5];
z[5] = dl[3] * z[5];
z[1] = -z[1] + -z[3] + z[5] + z[6];
z[1] = k.v[3] * z[1];
z[3] = -dl[1] * k.v[2];
z[5] = dl[1] + dl[5];
z[5] = k.v[5] * z[5];
z[2] = -z[2] + z[3] + z[5];
z[2] = k.v[1] * z[2];
z[1] = z[1] + z[2] + z[4];
return z[0] * z[1];
}

template <typename T> T dlog_W65 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[64]).real();
z[1] = dl[2] * k.v[2];
z[2] = dl[4] * k.v[4];
z[1] = z[1] + -z[2];
z[2] = -k.v[2] + k.v[4];
z[3] = k.v[0] + z[2];
z[3] = dl[3] * z[3];
z[4] = dl[0] * k.v[0];
z[5] = dl[3] + dl[5];
z[5] = k.v[5] * z[5];
z[3] = -z[1] + z[3] + z[4] + z[5];
z[3] = k.v[3] * z[3];
z[4] = -dl[1] * z[2];
z[5] = -dl[1] + -dl[5];
z[5] = k.v[5] * z[5];
z[4] = z[1] + z[4] + z[5];
z[4] = k.v[1] * z[4];
z[2] = -2 * k.v[5] + -z[2];
z[2] = dl[5] * z[2];
z[1] = z[1] + z[2];
z[1] = k.v[5] * z[1];
z[1] = z[1] + z[3] + z[4];
return z[0] * z[1];
}

template <typename T> T dlog_W66 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[65]).real();
z[1] = dl[0] + dl[2];
z[1] = k.v[0] * z[1];
z[2] = dl[2] + dl[5];
z[2] = k.v[5] * z[2];
z[3] = dl[2] + dl[4];
z[3] = k.v[4] * z[3];
z[4] = dl[2] * k.v[2];
z[1] = z[1] + z[2] + z[3] + -2 * z[4];
z[1] = k.v[2] * z[1];
z[2] = -dl[0] + -dl[5];
z[2] = k.v[0] * k.v[5] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W67 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[66]).real();
z[1] = dl[0] + dl[3];
z[1] = k.v[3] * z[1];
z[2] = -dl[0] + -dl[1];
z[2] = k.v[1] * z[2];
z[1] = 2 * z[1] + z[2];
z[1] = k.v[0] * z[1];
z[2] = -dl[3] + -dl[5];
z[2] = k.v[3] * k.v[5] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W68 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[8];
z[0] = 1 / (k.W[67]).real();
z[1] = 2 * k.v[5];
z[2] = dl[0] + dl[5];
z[2] = z[1] * z[2];
z[3] = -2 * k.v[0] + k.v[1] + -k.v[2] + -k.v[4];
z[3] = dl[0] * z[3];
z[4] = -dl[2] * k.v[2];
z[5] = -dl[0] + -dl[3];
z[5] = k.v[3] * z[5];
z[6] = -dl[4] * k.v[4];
z[7] = dl[1] * k.v[1];
z[2] = z[2] + z[3] + z[4] + 2 * z[5] + z[6] + z[7];
z[2] = k.v[0] * z[2];
z[3] = dl[2] + dl[5];
z[3] = k.v[2] * z[3];
z[4] = dl[3] + dl[5];
z[4] = k.v[3] * z[4];
z[1] = -dl[5] * z[1];
z[1] = z[1] + z[3] + z[4];
z[1] = k.v[5] * z[1];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W69 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[4];
z[0] = 1 / (k.W[68]).real();
z[1] = dl[0] + dl[4];
z[1] = k.v[4] * z[1];
z[2] = dl[0] + dl[2];
z[2] = k.v[2] * z[2];
z[3] = dl[0] * k.v[0];
z[1] = z[1] + z[2] + -2 * z[3];
z[1] = k.v[0] * z[1];
z[2] = -dl[2] + -dl[5];
z[2] = k.v[2] * z[2];
z[3] = dl[0] + dl[5];
z[3] = k.v[0] * z[3];
z[2] = z[2] + z[3];
z[2] = k.v[5] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W70 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[69]).real();
z[1] = dl[1] + dl[5];
z[1] = k.v[5] * z[1];
z[2] = -dl[1] + -dl[2];
z[2] = k.v[2] * z[2];
z[1] = z[1] + z[2];
z[1] = k.v[1] * z[1];
z[2] = -dl[0] + -dl[3];
z[2] = k.v[0] * k.v[3] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W71 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[70]).real();
z[1] = dl[1] * k.v[1];
z[2] = dl[0] * k.v[0];
z[3] = k.v[0] + -k.v[1];
z[3] = dl[4] * z[3];
z[3] = -z[1] + z[2] + z[3];
z[3] = k.v[4] * z[3];
z[4] = -dl[5] * k.v[1];
z[1] = -z[1] + z[4];
z[1] = k.v[5] * z[1];
z[4] = dl[3] * k.v[0];
z[2] = z[2] + z[4];
z[2] = k.v[3] * z[2];
z[1] = z[1] + z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W72 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[71]).real();
z[1] = dl[1] + dl[3];
z[1] = k.v[3] * z[1];
z[2] = dl[1] + dl[2];
z[2] = k.v[2] * z[2];
z[1] = z[1] + z[2];
z[1] = k.v[1] * z[1];
z[2] = -dl[0] + -dl[3];
z[2] = k.v[0] * k.v[3] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W73 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[72]).real();
z[1] = k.v[0] + -2 * k.v[1] + k.v[3] + k.v[4] + -k.v[5];
z[1] = dl[1] * z[1];
z[2] = -dl[5] * k.v[5];
z[3] = dl[4] * k.v[4];
z[4] = dl[0] * k.v[0];
z[5] = dl[3] * k.v[3];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
z[1] = k.v[1] * z[1];
z[2] = -dl[0] + -dl[4];
z[2] = k.v[0] * k.v[4] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W74 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[73]).real();
z[1] = dl[1] * k.v[1];
z[2] = -dl[0] * k.v[1];
z[2] = -z[1] + z[2];
z[2] = k.v[0] * z[2];
z[3] = dl[0] * k.v[0];
z[1] = -z[1] + z[3];
z[3] = k.v[0] + -k.v[1];
z[4] = dl[3] * z[3];
z[4] = z[1] + z[4];
z[4] = k.v[3] * z[4];
z[3] = dl[4] * z[3];
z[1] = z[1] + z[3];
z[1] = k.v[4] * z[1];
z[3] = prod_pow(k.v[1], 2) * dl[1];
z[5] = dl[1] + dl[2];
z[5] = k.v[1] * k.v[2] * z[5];
z[1] = z[1] + z[2] + 2 * z[3] + z[4] + z[5];
return z[0] * z[1];
}

template <typename T> T dlog_W75 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[74]).real();
z[1] = -k.v[2] + -k.v[3];
z[1] = dl[1] * z[1];
z[2] = -dl[2] * k.v[2];
z[3] = -dl[3] * k.v[3];
z[4] = dl[1] + dl[5];
z[4] = k.v[5] * z[4];
z[1] = z[1] + z[2] + z[3] + z[4];
z[1] = k.v[1] * z[1];
z[2] = -dl[0] + -dl[4];
z[2] = k.v[0] * z[2];
z[3] = dl[1] + dl[4];
z[3] = k.v[1] * z[3];
z[2] = z[2] + z[3];
z[2] = k.v[4] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W76 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[75]).real();
z[1] = dl[1] + dl[2];
z[1] = k.v[1] * z[1];
z[2] = -dl[2] + -dl[4];
z[2] = k.v[4] * z[2];
z[3] = -dl[2] + -dl[5];
z[3] = k.v[5] * z[3];
z[4] = dl[2] * k.v[2];
z[1] = z[1] + z[2] + z[3] + 2 * z[4];
z[1] = k.v[2] * z[1];
z[2] = -dl[1] + -dl[5];
z[2] = k.v[1] * z[2];
z[3] = dl[4] + dl[5];
z[3] = k.v[4] * z[3];
z[2] = z[2] + z[3];
z[2] = k.v[5] * z[2];
z[3] = dl[0] + dl[3];
z[3] = k.v[0] * k.v[3] * z[3];
z[1] = z[1] + z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W77 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[76]).real();
z[1] = dl[2] * k.v[2];
z[2] = dl[1] * k.v[1];
z[1] = z[1] + z[2];
z[2] = k.v[1] + k.v[2];
z[3] = -2 * k.v[4] + -k.v[5] + z[2];
z[3] = dl[4] * z[3];
z[3] = z[1] + z[3];
z[3] = k.v[4] * z[3];
z[4] = -dl[0] + -dl[3];
z[4] = k.v[3] * z[4];
z[5] = -dl[0] + -dl[2];
z[5] = k.v[2] * z[5];
z[4] = z[4] + z[5];
z[4] = k.v[0] * z[4];
z[2] = -k.v[4] + z[2];
z[2] = dl[5] * z[2];
z[1] = z[1] + z[2];
z[1] = k.v[5] * z[1];
z[1] = z[1] + z[3] + z[4];
return z[0] * z[1];
}

template <typename T> T dlog_W78 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[77]).real();
z[1] = dl[4] + dl[5];
z[1] = k.v[4] * z[1];
z[2] = -dl[1] + -dl[5];
z[2] = k.v[1] * z[2];
z[3] = -dl[2] + -dl[5];
z[3] = k.v[2] * z[3];
z[1] = z[1] + z[2] + z[3];
z[1] = k.v[5] * z[1];
z[2] = -dl[3] + -dl[4];
z[2] = k.v[4] * z[2];
z[3] = dl[1] + dl[3];
z[3] = k.v[1] * z[3];
z[2] = z[2] + z[3];
z[2] = k.v[3] * z[2];
z[3] = dl[0] + dl[2];
z[3] = k.v[0] * z[3];
z[4] = dl[2] + dl[3];
z[4] = k.v[3] * z[4];
z[3] = z[3] + z[4];
z[3] = k.v[2] * z[3];
z[1] = z[1] + z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W79 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[78]).real();
z[1] = dl[1] + dl[4];
z[1] = k.v[4] * z[1];
z[2] = dl[0] + dl[1];
z[2] = k.v[0] * z[2];
z[3] = -dl[1] + -dl[2];
z[3] = k.v[2] * z[3];
z[4] = dl[1] + dl[3];
z[4] = k.v[3] * z[4];
z[5] = dl[1] * k.v[1];
z[1] = z[1] + z[2] + z[3] + z[4] + -2 * z[5];
z[1] = k.v[1] * z[1];
z[2] = -dl[3] + -dl[4];
z[2] = k.v[4] * z[2];
z[3] = -dl[0] + -dl[3];
z[3] = k.v[0] * z[3];
z[4] = dl[2] + dl[3];
z[4] = k.v[2] * z[4];
z[2] = z[2] + z[3] + z[4];
z[2] = k.v[3] * z[2];
z[3] = -dl[0] + -dl[4];
z[3] = k.v[0] * k.v[4] * z[3];
z[1] = z[1] + z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W80 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[79]).real();
z[1] = dl[3] * k.v[3];
z[2] = dl[5] * k.v[5];
z[1] = z[1] + -z[2];
z[2] = -k.v[3] + k.v[5];
z[3] = 2 * k.v[1] + z[2];
z[3] = dl[1] * z[3];
z[4] = -dl[0] + -dl[1];
z[4] = k.v[0] * z[4];
z[5] = -dl[1] + -dl[4];
z[5] = k.v[4] * z[5];
z[3] = -z[1] + z[3] + z[4] + z[5];
z[3] = k.v[1] * z[3];
z[4] = dl[4] * k.v[4];
z[2] = -2 * k.v[2] + k.v[4] + z[2];
z[2] = dl[2] * z[2];
z[1] = -z[1] + z[2] + z[4];
z[1] = k.v[2] * z[1];
z[2] = dl[3] + dl[4];
z[2] = k.v[3] * z[2];
z[4] = -dl[4] + -dl[5];
z[4] = k.v[5] * z[4];
z[5] = dl[0] + dl[4];
z[5] = k.v[0] * z[5];
z[2] = z[2] + z[4] + z[5];
z[2] = k.v[4] * z[2];
z[1] = z[1] + z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W81 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[80]).real();
z[1] = dl[2] * k.v[2];
z[2] = dl[1] * k.v[1];
z[1] = z[1] + z[2];
z[2] = k.v[1] + k.v[2];
z[3] = -dl[3] * z[2];
z[4] = dl[0] + dl[3];
z[4] = k.v[0] * z[4];
z[3] = -z[1] + z[3] + z[4];
z[3] = k.v[3] * z[3];
z[2] = 2 * k.v[4] + -z[2];
z[2] = dl[4] * z[2];
z[4] = dl[3] + dl[4];
z[4] = k.v[3] * z[4];
z[1] = -z[1] + z[2] + z[4];
z[1] = k.v[4] * z[1];
z[1] = z[1] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W82 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[81]).real();
z[1] = dl[5] * k.v[5];
z[2] = dl[4] * k.v[4];
z[1] = z[1] + z[2];
z[2] = dl[0] * k.v[0];
z[3] = k.v[4] + k.v[5];
z[4] = k.v[0] + -z[3];
z[4] = dl[3] * z[4];
z[5] = dl[2] + dl[3];
z[5] = k.v[2] * z[5];
z[2] = -z[1] + z[2] + z[4] + z[5];
z[2] = k.v[3] * z[2];
z[3] = 2 * k.v[2] + -z[3];
z[3] = dl[2] * z[3];
z[1] = -z[1] + z[3];
z[1] = k.v[2] * z[1];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W83 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[82]).real();
z[1] = dl[3] * k.v[3];
z[2] = dl[2] * k.v[3];
z[3] = -dl[1] + -dl[2];
z[3] = k.v[1] * z[3];
z[4] = dl[2] + dl[4];
z[4] = k.v[4] * z[4];
z[2] = z[1] + z[2] + z[3] + z[4];
z[2] = k.v[2] * z[2];
z[3] = dl[1] * k.v[1];
z[1] = -z[1] + z[3];
z[3] = k.v[1] + -k.v[3];
z[4] = -k.v[2] + 2 * k.v[5] + z[3];
z[4] = dl[5] * z[4];
z[5] = -dl[2] * k.v[2];
z[4] = z[1] + z[4] + z[5];
z[4] = k.v[5] * z[4];
z[3] = -2 * k.v[4] + z[3];
z[3] = dl[4] * z[3];
z[1] = z[1] + z[3];
z[1] = k.v[4] * z[1];
z[3] = dl[0] + dl[2];
z[3] = k.v[2] * z[3];
z[5] = -dl[0] + -dl[5];
z[5] = k.v[5] * z[5];
z[3] = z[3] + z[5];
z[3] = k.v[0] * z[3];
z[1] = z[1] + z[2] + z[3] + z[4];
return z[0] * z[1];
}

template <typename T> T dlog_W84 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[83]).real();
z[1] = dl[3] * k.v[3];
z[2] = dl[5] * k.v[5];
z[1] = z[1] + -z[2];
z[3] = -k.v[3] + k.v[5];
z[4] = dl[0] * z[3];
z[5] = -dl[0] + -dl[2];
z[5] = k.v[2] * z[5];
z[4] = -z[1] + z[4] + z[5];
z[4] = k.v[0] * z[4];
z[5] = dl[3] + dl[5];
z[5] = k.v[3] * z[5];
z[2] = -2 * z[2] + z[5];
z[2] = k.v[5] * z[2];
z[5] = -dl[4] * z[3];
z[5] = z[1] + z[5];
z[5] = k.v[4] * z[5];
z[3] = dl[2] * z[3];
z[1] = -z[1] + z[3];
z[1] = k.v[2] * z[1];
z[1] = z[1] + z[2] + z[4] + z[5];
return z[0] * z[1];
}

template <typename T> T dlog_W85 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[84]).real();
z[1] = dl[3] * k.v[3];
z[2] = dl[1] * k.v[1];
z[1] = z[1] + -z[2];
z[2] = -k.v[1] + k.v[3];
z[3] = k.v[0] + z[2];
z[3] = dl[4] * z[3];
z[4] = dl[0] * k.v[0];
z[3] = z[1] + z[3] + z[4];
z[3] = k.v[4] * z[3];
z[4] = -dl[2] * z[2];
z[4] = -z[1] + z[4];
z[4] = k.v[2] * z[4];
z[2] = dl[5] * z[2];
z[1] = z[1] + z[2];
z[1] = k.v[5] * z[1];
z[1] = z[1] + z[3] + z[4];
return z[0] * z[1];
}

template <typename T> T dlog_W86 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[8];
z[0] = 1 / (k.W[85]).real();
z[1] = dl[4] * k.v[4];
z[2] = -dl[3] * k.v[3];
z[3] = -k.v[3] + -k.v[4];
z[3] = dl[0] * z[3];
z[2] = -z[1] + z[2] + z[3];
z[2] = k.v[0] * z[2];
z[3] = dl[2] * k.v[2];
z[4] = -dl[1] + -dl[2];
z[4] = k.v[1] * z[4];
z[4] = -2 * z[3] + z[4];
z[4] = k.v[2] * z[4];
z[5] = dl[1] * k.v[1];
z[3] = z[3] + z[5];
z[5] = k.v[4] * z[3];
z[6] = k.v[1] + k.v[2];
z[7] = dl[5] * z[6];
z[3] = z[3] + z[7];
z[3] = k.v[5] * z[3];
z[1] = z[1] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
return z[0] * z[1];
}

template <typename T> T dlog_W87 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[86]).real();
z[1] = dl[4] + dl[5];
z[1] = k.v[5] * z[1];
z[2] = dl[4] * k.v[4];
z[3] = -dl[2] + -dl[4];
z[3] = k.v[2] * z[3];
z[1] = z[1] + 2 * z[2] + z[3];
z[1] = k.v[4] * z[1];
z[2] = -dl[1] + -dl[5];
z[2] = k.v[5] * z[2];
z[3] = -dl[1] + -dl[4];
z[3] = k.v[4] * z[3];
z[4] = dl[1] + dl[2];
z[4] = k.v[2] * z[4];
z[2] = z[2] + z[3] + z[4];
z[2] = k.v[1] * z[2];
z[3] = dl[0] + dl[3];
z[3] = k.v[0] * k.v[3] * z[3];
z[1] = z[1] + z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W88 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[5];
z[0] = 1 / (k.W[87]).real();
z[1] = dl[1] + dl[5];
z[1] = k.v[1] * z[1];
z[2] = dl[2] + dl[5];
z[2] = k.v[2] * z[2];
z[3] = -dl[4] + -dl[5];
z[3] = k.v[4] * z[3];
z[4] = -dl[3] + -dl[5];
z[4] = k.v[3] * z[4];
z[1] = z[1] + z[2] + z[3] + z[4];
z[1] = k.v[5] * z[1];
z[2] = -dl[0] + -dl[2];
z[2] = k.v[0] * k.v[2] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W89 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[7];
z[0] = 1 / (k.W[88]).real();
z[1] = k.v[4] * k.v[5];
z[2] = -k.v[0] + 2 * k.v[5];
z[2] = k.v[5] * z[2];
z[2] = z[1] + z[2];
z[2] = dl[5] * z[2];
z[3] = dl[5] * k.v[5];
z[4] = dl[0] * k.v[0];
z[3] = z[3] + -z[4];
z[5] = -k.v[0] + k.v[5];
z[6] = -dl[3] * z[5];
z[6] = -z[3] + z[6];
z[6] = k.v[3] * z[6];
z[5] = -dl[2] * z[5];
z[3] = -z[3] + z[5];
z[3] = k.v[2] * z[3];
z[4] = -k.v[5] * z[4];
z[1] = dl[4] * z[1];
z[1] = z[1] + z[2] + z[3] + z[4] + z[6];
return z[0] * z[1];
}

template <typename T> T dlog_W90 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[89]).real();
z[1] = k.v[0] + -k.v[1] + k.v[2] + k.v[3] + -2 * k.v[5];
z[1] = dl[5] * z[1];
z[2] = dl[3] * k.v[3];
z[3] = dl[2] * k.v[2];
z[4] = dl[0] * k.v[0];
z[5] = -dl[1] * k.v[1];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
z[1] = k.v[5] * z[1];
z[2] = -dl[0] + -dl[2];
z[2] = k.v[0] * k.v[2] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W91 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[90]).real();
z[1] = -dl[0] + -dl[3];
z[1] = k.v[0] * z[1];
z[2] = dl[3] + dl[5];
z[2] = k.v[5] * z[2];
z[1] = z[1] + z[2];
z[1] = k.v[3] * z[1];
z[2] = dl[4] + dl[5];
z[2] = k.v[4] * k.v[5] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W92 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[4];
z[0] = 1 / (k.W[91]).real();
z[1] = -dl[1] + -dl[5];
z[1] = k.v[1] * z[1];
z[2] = -dl[2] + -dl[5];
z[2] = k.v[2] * z[2];
z[1] = z[1] + z[2];
z[1] = k.v[5] * z[1];
z[2] = dl[0] + dl[3];
z[2] = k.v[3] * z[2];
z[3] = dl[0] + dl[2];
z[3] = k.v[2] * z[3];
z[2] = z[2] + z[3];
z[2] = k.v[0] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W93 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[3];
z[0] = 1 / (k.W[92]).real();
z[1] = dl[1] + dl[5];
z[1] = k.v[1] * z[1];
z[2] = -dl[4] + -dl[5];
z[2] = k.v[4] * z[2];
z[1] = z[1] + z[2];
z[1] = k.v[5] * z[1];
z[2] = -dl[0] + -dl[3];
z[2] = k.v[0] * k.v[3] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W94 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[7];
z[0] = 1 / (k.W[93]).real();
z[1] = dl[4] + dl[5];
z[2] = -dl[0] + -z[1];
z[2] = k.v[4] * z[2];
z[3] = dl[0] + dl[1];
z[4] = -dl[5] + -z[3];
z[4] = k.v[1] * z[4];
z[5] = 2 * dl[0];
z[6] = dl[5] + z[5];
z[6] = k.v[0] * z[6];
z[2] = z[2] + z[4] + z[6];
z[2] = k.v[0] * z[2];
z[1] = dl[1] + z[1];
z[1] = k.v[1] * k.v[4] * z[1];
z[1] = z[1] + z[2];
z[1] = k.v[5] * z[1];
z[2] = -dl[0] + -dl[2] + -dl[3];
z[2] = k.v[3] * z[2];
z[3] = dl[2] + z[3];
z[3] = k.v[1] * z[3];
z[4] = -dl[2] + -z[5];
z[4] = k.v[0] * z[4];
z[2] = z[2] + z[3] + z[4];
z[2] = k.v[0] * k.v[2] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W95 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[10];
z[0] = 1 / (k.W[94]).real();
z[1] = dl[1] * k.v[1];
z[2] = dl[0] * k.v[0];
z[1] = z[1] + -z[2];
z[1] = k.v[4] * z[1];
z[3] = dl[0] + dl[1];
z[4] = k.v[1] * z[3];
z[4] = 2 * z[2] + -z[4];
z[5] = -k.v[0] * z[4];
z[6] = dl[4] * k.v[4];
z[7] = k.v[0] + -k.v[1];
z[8] = z[6] * z[7];
z[9] = -k.v[0] + k.v[4];
z[7] = z[7] * z[9];
z[9] = dl[5] * z[7];
z[5] = -z[1] + z[5] + z[8] + z[9];
z[5] = k.v[5] * z[5];
z[8] = 2 * k.v[0];
z[4] = -z[4] * z[8];
z[1] = -z[1] + z[4];
z[1] = k.v[4] * z[1];
z[4] = dl[1] + dl[2];
z[4] = k.v[4] * z[4];
z[4] = z[4] + z[6];
z[4] = k.v[1] * z[4];
z[3] = -dl[2] + -z[3];
z[3] = k.v[1] * z[3];
z[8] = dl[0] + dl[2] + dl[3];
z[8] = k.v[3] * z[8];
z[3] = z[3] + z[8];
z[3] = k.v[0] * z[3];
z[3] = z[3] + z[4];
z[3] = k.v[2] * z[3];
z[4] = -2 * dl[0] + -dl[1];
z[4] = k.v[1] * z[4];
z[2] = 3 * z[2] + z[4];
z[2] = prod_pow(k.v[0], 2) * z[2];
z[4] = z[6] * z[7];
z[1] = z[1] + z[2] + z[3] + 2 * z[4] + z[5];
return z[0] * z[1];
}

template <typename T> T dlog_W96 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[8];
z[0] = 1 / (k.W[95]).real();
z[1] = k.v[2] + k.v[3];
z[1] = dl[0] * z[1];
z[2] = dl[3] * k.v[3];
z[3] = dl[2] + dl[3];
z[3] = k.v[2] * z[3];
z[3] = z[1] + 2 * z[2] + z[3];
z[3] = k.v[3] * z[3];
z[4] = dl[2] * k.v[2];
z[2] = 2 * z[1] + z[2] + z[4];
z[2] = k.v[0] * z[2];
z[4] = dl[0] + dl[4] + dl[5];
z[5] = k.v[4] * k.v[5];
z[4] = z[4] * z[5];
z[2] = z[2] + z[3] + z[4];
z[2] = k.v[0] * z[2];
z[3] = dl[1] + dl[5];
z[4] = -dl[3] + -z[3];
z[4] = k.v[3] * z[4];
z[6] = -dl[0] + -z[3];
z[6] = k.v[0] * z[6];
z[7] = 2 * dl[1] + dl[5];
z[7] = k.v[1] * z[7];
z[4] = z[4] + z[6] + z[7];
z[4] = k.v[5] * z[4];
z[6] = -dl[1] + -dl[2];
z[6] = k.v[2] * z[6];
z[7] = dl[1] + dl[3];
z[7] = -k.v[3] * z[7];
z[1] = -z[1] + z[6] + z[7];
z[1] = k.v[0] * z[1];
z[3] = -dl[4] + -z[3];
z[3] = z[3] * z[5];
z[1] = z[1] + z[3] + z[4];
z[1] = k.v[1] * z[1];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W97 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[15];
z[0] = 1 / (k.W[96]).real();
z[1] = 3 * k.v[1];
z[2] = k.v[2] + z[1];
z[3] = 2 * k.v[0];
z[4] = -z[2] + z[3];
z[4] = k.v[0] * z[4];
z[5] = prod_pow(k.v[1], 2);
z[6] = k.v[1] * k.v[2];
z[7] = -k.v[1] + z[3];
z[8] = k.v[4] * z[7];
z[4] = z[4] + z[5] + z[6] + z[8];
z[4] = dl[3] * z[4];
z[8] = k.v[1] * k.v[4];
z[8] = -z[6] + z[8];
z[9] = k.v[0] * z[1];
z[10] = 2 * z[5];
z[11] = -z[8] + -z[9] + z[10];
z[11] = dl[1] * z[11];
z[12] = dl[4] * k.v[4];
z[7] = z[7] * z[12];
z[13] = 4 * k.v[0];
z[2] = -z[2] + z[13];
z[2] = k.v[0] * z[2];
z[14] = k.v[4] * z[3];
z[2] = z[2] + z[14];
z[2] = dl[0] * z[2];
z[14] = -k.v[0] * k.v[2];
z[14] = z[6] + z[14];
z[14] = dl[2] * z[14];
z[2] = z[2] + z[4] + z[7] + z[11] + z[14];
z[2] = k.v[3] * z[2];
z[4] = 4 * k.v[1] + k.v[5];
z[3] = z[3] + -z[4];
z[3] = k.v[0] * z[3];
z[7] = k.v[0] + -k.v[1];
z[7] = k.v[4] * z[7];
z[3] = z[3] + z[6] + 2 * z[7] + z[10];
z[3] = z[3] * z[12];
z[7] = k.v[1] * k.v[5];
z[11] = z[6] + z[7];
z[9] = 6 * z[5] + -z[9] + z[11];
z[9] = k.v[0] * z[9];
z[12] = k.v[0] * k.v[1];
z[12] = -z[5] + z[12];
z[8] = -z[8] + -4 * z[12];
z[8] = k.v[4] * z[8];
z[12] = prod_pow(k.v[1], 3);
z[10] = -k.v[2] * z[10];
z[8] = z[8] + z[9] + z[10] + -3 * z[12];
z[8] = dl[1] * z[8];
z[1] = -k.v[5] + -z[1];
z[1] = 3 * k.v[0] + 2 * z[1];
z[1] = k.v[0] * z[1];
z[1] = z[1] + 3 * z[5] + z[11];
z[1] = k.v[0] * z[1];
z[4] = -z[4] + z[13];
z[4] = k.v[0] * z[4];
z[9] = k.v[0] * k.v[4];
z[4] = z[4] + z[9];
z[4] = k.v[4] * z[4];
z[1] = z[1] + z[4];
z[1] = dl[0] * z[1];
z[4] = k.v[0] + k.v[4];
z[4] = z[4] * z[6];
z[5] = k.v[2] * z[5];
z[4] = z[4] + -z[5];
z[4] = dl[2] * z[4];
z[5] = -k.v[0] * k.v[5];
z[5] = z[5] + z[7];
z[5] = k.v[0] * z[5];
z[6] = -k.v[5] * z[9];
z[5] = z[5] + z[6];
z[5] = dl[5] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[8];
return z[0] * z[1];
}

template <typename T> T dlog_W98 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[97]).real();
z[1] = dl[0] + dl[3];
z[2] = -dl[2] + -z[1];
z[2] = k.v[2] * z[2];
z[1] = dl[4] + z[1];
z[1] = k.v[4] * z[1];
z[3] = dl[0] + 2 * dl[3];
z[3] = k.v[3] * z[3];
z[1] = 2 * z[1] + z[2] + z[3];
z[1] = k.v[3] * z[1];
z[2] = 2 * dl[0];
z[3] = dl[2] + z[2];
z[3] = k.v[2] * z[3];
z[2] = -dl[3] + -z[2];
z[2] = k.v[3] * z[2];
z[2] = z[2] + z[3];
z[2] = k.v[0] * z[2];
z[1] = z[1] + z[2];
z[1] = k.v[0] * z[1];
z[2] = dl[1] + dl[3];
z[3] = dl[2] + z[2];
z[3] = k.v[2] * z[3];
z[4] = -dl[4] + -z[2];
z[4] = k.v[4] * z[4];
z[3] = z[3] + z[4];
z[3] = k.v[3] * z[3];
z[4] = -dl[0] + -dl[1] + -dl[2];
z[4] = k.v[0] * k.v[2] * z[4];
z[3] = z[3] + z[4];
z[3] = k.v[1] * z[3];
z[2] = -dl[5] + -z[2];
z[2] = k.v[3] * z[2];
z[4] = dl[0] + dl[5];
z[5] = dl[1] + z[4];
z[5] = k.v[0] * z[5];
z[2] = z[2] + z[5];
z[2] = k.v[1] * z[2];
z[4] = -dl[4] + -z[4];
z[4] = k.v[0] * k.v[4] * z[4];
z[2] = z[2] + z[4];
z[2] = k.v[5] * z[2];
z[1] = z[1] + z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W99 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[7];
z[0] = 1 / (k.W[98]).real();
z[1] = dl[0] + dl[4];
z[2] = k.v[4] * k.v[5];
z[1] = z[1] * z[2];
z[3] = dl[0] + dl[1];
z[4] = -k.v[5] * z[3];
z[3] = dl[2] + z[3];
z[3] = k.v[2] * z[3];
z[3] = z[3] + z[4];
z[3] = k.v[1] * z[3];
z[4] = -k.v[1] * k.v[5];
z[2] = z[2] + z[4];
z[2] = dl[5] * z[2];
z[4] = 2 * dl[0];
z[5] = dl[5] + z[4];
z[5] = k.v[5] * z[5];
z[6] = -dl[2] + -z[4];
z[6] = k.v[2] * z[6];
z[5] = z[5] + z[6];
z[5] = k.v[0] * z[5];
z[1] = z[1] + z[2] + z[3] + z[5];
z[1] = k.v[0] * z[1];
z[2] = dl[1] + dl[3];
z[3] = dl[4] + z[2];
z[3] = k.v[4] * z[3];
z[2] = -dl[2] + -z[2];
z[2] = k.v[2] * z[2];
z[5] = -2 * dl[1] + -dl[3];
z[5] = k.v[1] * z[5];
z[2] = z[2] + z[3] + z[5];
z[2] = k.v[1] * z[2];
z[3] = dl[0] + dl[3];
z[5] = -dl[4] + -z[3];
z[5] = k.v[4] * z[5];
z[4] = -dl[3] + -z[4];
z[4] = k.v[0] * z[4];
z[4] = z[4] + z[5];
z[5] = dl[2] + z[3];
z[5] = k.v[2] * z[5];
z[3] = dl[1] + z[3];
z[3] = k.v[1] * z[3];
z[3] = 3 * z[3] + 2 * z[4] + z[5];
z[3] = k.v[0] * z[3];
z[2] = z[2] + z[3];
z[2] = k.v[3] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W100 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[7];
z[0] = 1 / (k.W[99]).real();
z[1] = dl[1] * k.v[1];
z[2] = dl[4] * k.v[4];
z[3] = -dl[4] * k.v[1];
z[3] = -z[1] + 2 * z[2] + z[3];
z[3] = k.v[4] * z[3];
z[4] = dl[5] * k.v[4];
z[5] = -dl[2] * k.v[4];
z[5] = -z[2] + -z[4] + z[5];
z[5] = k.v[2] * z[5];
z[6] = -k.v[1] + k.v[4];
z[4] = z[4] * z[6];
z[3] = z[3] + z[4] + z[5];
z[3] = k.v[5] * z[3];
z[1] = z[1] + -z[2];
z[2] = dl[0] + dl[2];
z[4] = z[2] * z[6];
z[2] = dl[3] + z[2];
z[2] = k.v[3] * z[2];
z[2] = -z[1] + z[2] + z[4];
z[2] = k.v[2] * z[2];
z[4] = -dl[0] + -dl[5];
z[4] = z[4] * z[6];
z[1] = z[1] + z[4];
z[1] = k.v[5] * z[1];
z[1] = z[1] + z[2];
z[1] = k.v[0] * z[1];
z[1] = z[1] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W101 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[10];
z[0] = 1 / (k.W[100]).real();
z[1] = 2 * k.v[2];
z[2] = 2 * dl[4];
z[3] = dl[2] + z[2];
z[3] = z[1] * z[3];
z[4] = dl[4] * k.v[5];
z[5] = dl[5] * k.v[5];
z[6] = dl[1] + z[2];
z[6] = k.v[1] * z[6];
z[7] = dl[4] * k.v[4];
z[3] = z[3] + -2 * z[4] + -z[5] + z[6] + -3 * z[7];
z[3] = k.v[4] * z[3];
z[4] = z[4] + z[5];
z[6] = dl[2] * k.v[5];
z[7] = dl[1] + dl[4];
z[8] = -dl[2] + -z[7];
z[8] = k.v[1] * z[8];
z[9] = -2 * dl[2] + -dl[4];
z[9] = k.v[2] * z[9];
z[6] = z[4] + z[6] + z[8] + z[9];
z[6] = k.v[2] * z[6];
z[8] = dl[1] * k.v[5];
z[9] = z[4] + z[8];
z[9] = k.v[1] * z[9];
z[3] = z[3] + z[6] + z[9];
z[3] = k.v[4] * z[3];
z[6] = -dl[0] + -z[7];
z[6] = k.v[1] * z[6];
z[2] = dl[0] + z[2];
z[2] = k.v[4] * z[2];
z[2] = z[2] + z[6];
z[6] = dl[0] + dl[2];
z[7] = -dl[4] + -z[6];
z[1] = z[1] * z[7];
z[7] = dl[0] * k.v[5];
z[1] = z[1] + 2 * z[2] + z[4] + z[7];
z[1] = k.v[4] * z[1];
z[2] = -z[5] + -z[7] + -z[8];
z[2] = k.v[1] * z[2];
z[4] = dl[1] + z[6];
z[4] = k.v[1] * z[4];
z[5] = -dl[3] + -z[6];
z[5] = k.v[3] * z[5];
z[4] = z[4] + z[5];
z[4] = k.v[2] * z[4];
z[5] = 2 * dl[0];
z[6] = dl[1] + z[5];
z[6] = k.v[1] * z[6];
z[5] = -dl[4] + -z[5];
z[5] = k.v[4] * z[5];
z[5] = z[5] + z[6];
z[5] = k.v[0] * z[5];
z[1] = z[1] + z[2] + z[4] + z[5];
z[1] = k.v[0] * z[1];
z[1] = z[1] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W102 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[11];
z[0] = 1 / (k.W[101]).real();
z[1] = dl[1] * k.v[1];
z[2] = 2 * k.v[1] + k.v[2] + -k.v[4];
z[2] = z[1] * z[2];
z[3] = dl[4] * k.v[4];
z[1] = -z[1] + z[3];
z[4] = k.v[3] * z[1];
z[5] = dl[2] * k.v[2];
z[6] = -k.v[1] + k.v[3];
z[7] = -z[5] * z[6];
z[5] = -z[1] + z[5];
z[5] = k.v[5] * z[5];
z[6] = 2 * k.v[5] + -z[6];
z[8] = k.v[1] + -k.v[4];
z[9] = k.v[2] + z[8];
z[6] = z[6] * z[9];
z[10] = k.v[0] * k.v[3];
z[6] = z[6] + -z[10];
z[6] = dl[5] * z[6];
z[3] = -k.v[1] * z[3];
z[2] = z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
z[2] = k.v[5] * z[2];
z[3] = 2 * k.v[3] + -z[8];
z[3] = z[3] * z[10];
z[4] = -k.v[3] * z[9];
z[4] = z[4] + -z[10];
z[4] = k.v[5] * z[4];
z[3] = z[3] + z[4];
z[3] = dl[3] * z[3];
z[4] = k.v[3] + -k.v[5] + -z[8];
z[4] = dl[0] * z[4];
z[1] = z[1] + z[4];
z[1] = z[1] * z[10];
z[1] = z[1] + z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W103 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[8];
z[0] = 1 / (k.W[102]).real();
z[1] = k.v[0] * k.v[3];
z[2] = -k.v[3] + k.v[5];
z[3] = -k.v[4] * z[2];
z[4] = -k.v[4] + z[2];
z[4] = 3 * k.v[1] + 2 * z[4];
z[4] = k.v[1] * z[4];
z[5] = 2 * k.v[1] + z[2];
z[5] = k.v[2] * z[5];
z[3] = -z[1] + z[3] + z[4] + z[5];
z[3] = dl[1] * z[3];
z[4] = dl[0] * k.v[3];
z[5] = dl[3] * k.v[3];
z[4] = z[4] + z[5];
z[4] = k.v[0] * z[4];
z[6] = dl[5] * k.v[5];
z[5] = -z[5] + z[6];
z[6] = -dl[4] * z[2];
z[6] = -z[5] + z[6];
z[6] = k.v[4] * z[6];
z[7] = -dl[4] * k.v[4];
z[7] = z[5] + z[7];
z[7] = k.v[1] * z[7];
z[3] = z[3] + -z[4] + z[6] + z[7];
z[3] = k.v[1] * z[3];
z[5] = k.v[1] * z[5];
z[2] = k.v[1] + z[2];
z[2] = k.v[1] * z[2];
z[1] = -z[1] + z[2];
z[1] = dl[2] * z[1];
z[1] = z[1] + -z[4] + z[5];
z[1] = k.v[2] * z[1];
z[1] = z[1] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W104 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[19];
z[0] = 1 / (k.W[103]).real();
z[1] = -k.v[1] + 2 * k.v[4];
z[2] = 2 * k.v[0];
z[3] = k.v[5] + z[1] + -z[2];
z[4] = k.v[2] + -z[3];
z[4] = k.v[2] * z[4];
z[5] = -k.v[1] + k.v[4];
z[6] = k.v[4] * z[5];
z[7] = k.v[5] * z[5];
z[8] = k.v[4] + k.v[5];
z[9] = k.v[0] + -z[8];
z[9] = k.v[0] * z[9];
z[10] = k.v[3] * z[2];
z[4] = z[4] + z[6] + z[7] + z[9] + z[10];
z[4] = dl[3] * z[4];
z[9] = dl[0] * z[2];
z[10] = dl[4] * k.v[4];
z[8] = -dl[0] * z[8];
z[11] = dl[0] * k.v[3];
z[8] = z[8] + z[9] + -z[10] + z[11];
z[8] = k.v[0] * z[8];
z[11] = z[1] * z[10];
z[12] = dl[1] * k.v[1];
z[13] = k.v[4] * z[12];
z[11] = z[11] + -z[13];
z[13] = -z[10] + z[12];
z[13] = k.v[5] * z[13];
z[14] = z[11] + -z[13];
z[9] = -z[9] + 2 * z[10] + -z[12];
z[15] = k.v[2] * z[9];
z[16] = k.v[0] * k.v[5];
z[17] = -k.v[2] * k.v[5];
z[17] = z[7] + -z[16] + z[17];
z[17] = dl[5] * z[17];
z[3] = 2 * k.v[2] + -z[3];
z[18] = dl[2] * k.v[2];
z[3] = z[3] * z[18];
z[3] = z[3] + z[4] + z[8] + z[14] + -z[15] + z[17];
z[3] = k.v[3] * z[3];
z[4] = 2 * k.v[5];
z[1] = z[1] + z[4];
z[8] = -dl[0] * z[1];
z[8] = z[8] + -z[9];
z[8] = k.v[0] * z[8];
z[9] = 3 * z[10] + -2 * z[12];
z[9] = k.v[5] * z[9];
z[8] = z[8] + z[9] + z[11] + -z[15];
z[8] = k.v[2] * z[8];
z[9] = 2 * k.v[1] + -3 * k.v[4];
z[10] = -z[2] + z[4] + -z[9];
z[10] = k.v[5] * z[10];
z[11] = -k.v[2] * z[4];
z[10] = z[10] + z[11];
z[10] = k.v[2] * z[10];
z[4] = -z[4] * z[5];
z[4] = z[4] + -z[6];
z[4] = k.v[5] * z[4];
z[5] = z[5] * z[16];
z[4] = z[4] + z[5] + z[10];
z[4] = dl[5] * z[4];
z[5] = k.v[0] + -z[1];
z[5] = k.v[0] * z[5];
z[1] = -z[1] + z[2];
z[1] = 3 * k.v[2] + 2 * z[1];
z[1] = k.v[2] * z[1];
z[2] = k.v[5] + -z[9];
z[2] = k.v[5] * z[2];
z[1] = z[1] + z[2] + z[5] + z[6];
z[1] = z[1] * z[18];
z[2] = -k.v[5] * z[14];
z[5] = dl[0] * z[7];
z[5] = z[5] + -z[13];
z[5] = k.v[0] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[8];
return z[0] * z[1];
}

template <typename T> T dlog_W105 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[10];
z[0] = 1 / (k.W[104]).real();
z[1] = dl[5] * k.v[5];
z[2] = dl[3] * k.v[3];
z[1] = z[1] + -z[2];
z[2] = dl[2] * k.v[2];
z[3] = dl[4] * k.v[4];
z[4] = k.v[3] + -k.v[5];
z[5] = -k.v[4] + z[4];
z[5] = dl[2] * z[5];
z[5] = -z[1] + 2 * z[2] + -z[3] + z[5];
z[5] = k.v[2] * z[5];
z[6] = k.v[0] * k.v[3];
z[7] = -k.v[4] * z[4];
z[8] = k.v[2] + z[4];
z[9] = -k.v[4] + z[8];
z[9] = k.v[2] * z[9];
z[7] = z[6] + z[7] + z[9];
z[7] = dl[1] * z[7];
z[8] = dl[1] * z[8];
z[2] = -z[1] + z[2] + 2 * z[8];
z[2] = k.v[1] * z[2];
z[8] = dl[0] + dl[3];
z[8] = z[6] * z[8];
z[1] = k.v[4] * z[1];
z[4] = -z[3] * z[4];
z[1] = z[1] + z[2] + z[4] + z[5] + z[7] + z[8];
z[1] = k.v[1] * z[1];
z[2] = k.v[0] * k.v[4];
z[2] = z[2] + z[6];
z[4] = dl[0] + dl[2];
z[2] = z[2] * z[4];
z[4] = dl[3] * z[6];
z[3] = k.v[0] * z[3];
z[2] = z[2] + z[3] + z[4];
z[2] = k.v[2] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W106 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[10];
z[0] = 1 / (k.W[105]).real();
z[1] = dl[1] * k.v[1];
z[2] = dl[3] * k.v[3];
z[1] = z[1] + -z[2];
z[2] = dl[4] * k.v[4];
z[3] = dl[2] * k.v[2];
z[4] = -k.v[1] + k.v[3];
z[5] = -k.v[2] + z[4];
z[5] = dl[4] * z[5];
z[5] = -z[1] + 2 * z[2] + -z[3] + z[5];
z[5] = k.v[4] * z[5];
z[6] = k.v[0] * k.v[3];
z[7] = -k.v[2] * z[4];
z[8] = k.v[4] + z[4];
z[9] = -k.v[2] + z[8];
z[9] = k.v[4] * z[9];
z[7] = z[6] + z[7] + z[9];
z[7] = dl[5] * z[7];
z[8] = dl[5] * z[8];
z[2] = -z[1] + z[2] + 2 * z[8];
z[2] = k.v[5] * z[2];
z[8] = dl[0] + dl[3];
z[8] = z[6] * z[8];
z[1] = k.v[2] * z[1];
z[4] = -z[3] * z[4];
z[1] = z[1] + z[2] + z[4] + z[5] + z[7] + z[8];
z[1] = k.v[5] * z[1];
z[2] = k.v[0] * k.v[2];
z[2] = z[2] + z[6];
z[4] = dl[0] + dl[4];
z[2] = z[2] * z[4];
z[4] = dl[3] * z[6];
z[3] = k.v[0] * z[3];
z[2] = z[2] + z[3] + z[4];
z[2] = k.v[4] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W107 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[19];
z[0] = 1 / (k.W[106]).real();
z[1] = 2 * k.v[2] + -k.v[5];
z[2] = 2 * k.v[0];
z[3] = k.v[1] + z[1] + -z[2];
z[4] = k.v[4] + -z[3];
z[4] = k.v[4] * z[4];
z[5] = k.v[2] + -k.v[5];
z[6] = k.v[2] * z[5];
z[7] = k.v[1] * z[5];
z[8] = k.v[1] + k.v[2];
z[9] = k.v[0] + -z[8];
z[9] = k.v[0] * z[9];
z[10] = k.v[3] * z[2];
z[4] = z[4] + z[6] + z[7] + z[9] + z[10];
z[4] = dl[3] * z[4];
z[9] = dl[0] * z[2];
z[10] = dl[2] * k.v[2];
z[8] = -dl[0] * z[8];
z[11] = dl[0] * k.v[3];
z[8] = z[8] + z[9] + -z[10] + z[11];
z[8] = k.v[0] * z[8];
z[11] = z[1] * z[10];
z[12] = dl[5] * k.v[5];
z[13] = k.v[2] * z[12];
z[11] = z[11] + -z[13];
z[13] = -z[10] + z[12];
z[13] = k.v[1] * z[13];
z[14] = z[11] + -z[13];
z[9] = -z[9] + 2 * z[10] + -z[12];
z[15] = k.v[4] * z[9];
z[16] = k.v[0] * k.v[1];
z[17] = -k.v[1] * k.v[4];
z[17] = z[7] + -z[16] + z[17];
z[17] = dl[1] * z[17];
z[3] = 2 * k.v[4] + -z[3];
z[18] = dl[4] * k.v[4];
z[3] = z[3] * z[18];
z[3] = z[3] + z[4] + z[8] + z[14] + -z[15] + z[17];
z[3] = k.v[3] * z[3];
z[4] = 2 * k.v[1];
z[1] = z[1] + z[4];
z[8] = -dl[0] * z[1];
z[8] = z[8] + -z[9];
z[8] = k.v[0] * z[8];
z[9] = 3 * z[10] + -2 * z[12];
z[9] = k.v[1] * z[9];
z[8] = z[8] + z[9] + z[11] + -z[15];
z[8] = k.v[4] * z[8];
z[9] = -3 * k.v[2] + 2 * k.v[5];
z[10] = -z[2] + z[4] + -z[9];
z[10] = k.v[1] * z[10];
z[11] = -k.v[4] * z[4];
z[10] = z[10] + z[11];
z[10] = k.v[4] * z[10];
z[4] = -z[4] * z[5];
z[4] = z[4] + -z[6];
z[4] = k.v[1] * z[4];
z[5] = z[5] * z[16];
z[4] = z[4] + z[5] + z[10];
z[4] = dl[1] * z[4];
z[5] = k.v[0] + -z[1];
z[5] = k.v[0] * z[5];
z[1] = -z[1] + z[2];
z[1] = 3 * k.v[4] + 2 * z[1];
z[1] = k.v[4] * z[1];
z[2] = k.v[1] + -z[9];
z[2] = k.v[1] * z[2];
z[1] = z[1] + z[2] + z[5] + z[6];
z[1] = z[1] * z[18];
z[2] = -k.v[1] * z[14];
z[5] = dl[0] * z[7];
z[5] = z[5] + -z[13];
z[5] = k.v[0] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[8];
return z[0] * z[1];
}

template <typename T> T dlog_W108 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[8];
z[0] = 1 / (k.W[107]).real();
z[1] = dl[2] * k.v[2];
z[2] = dl[4] * k.v[4];
z[1] = z[1] + -z[2];
z[3] = 2 * dl[5];
z[4] = k.v[2] + -k.v[4];
z[5] = -z[3] * z[4];
z[6] = dl[1] + z[3];
z[6] = k.v[1] * z[6];
z[3] = -dl[3] + -z[3];
z[3] = k.v[3] * z[3];
z[7] = dl[5] * k.v[5];
z[3] = -z[1] + z[3] + z[5] + z[6] + 3 * z[7];
z[3] = k.v[5] * z[3];
z[5] = dl[5] * z[4];
z[1] = z[1] + z[5];
z[5] = dl[3] * z[4];
z[6] = dl[0] + dl[3];
z[7] = -dl[5] + -z[6];
z[7] = k.v[0] * z[7];
z[5] = z[1] + z[5] + z[7];
z[5] = k.v[3] * z[5];
z[4] = -dl[1] * z[4];
z[1] = -z[1] + z[4];
z[1] = k.v[1] * z[1];
z[1] = z[1] + z[3] + z[5];
z[1] = k.v[5] * z[1];
z[3] = -k.v[4] * z[6];
z[2] = -z[2] + z[3];
z[2] = k.v[0] * k.v[3] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W109 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[11];
z[0] = 1 / (k.W[108]).real();
z[1] = dl[5] * k.v[5];
z[2] = -k.v[2] + k.v[4] + 2 * k.v[5];
z[2] = z[1] * z[2];
z[3] = dl[2] * k.v[2];
z[1] = -z[1] + z[3];
z[4] = k.v[3] * z[1];
z[5] = dl[4] * k.v[4];
z[6] = k.v[3] + -k.v[5];
z[7] = -z[5] * z[6];
z[5] = -z[1] + z[5];
z[5] = k.v[1] * z[5];
z[6] = 2 * k.v[1] + -z[6];
z[8] = -k.v[2] + k.v[5];
z[9] = k.v[4] + z[8];
z[6] = z[6] * z[9];
z[10] = k.v[0] * k.v[3];
z[6] = z[6] + -z[10];
z[6] = dl[1] * z[6];
z[3] = -k.v[5] * z[3];
z[2] = z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
z[2] = k.v[1] * z[2];
z[3] = 2 * k.v[3] + -z[8];
z[3] = z[3] * z[10];
z[4] = -k.v[3] * z[9];
z[4] = z[4] + -z[10];
z[4] = k.v[1] * z[4];
z[3] = z[3] + z[4];
z[3] = dl[3] * z[3];
z[4] = -k.v[1] + k.v[3] + -z[8];
z[4] = dl[0] * z[4];
z[1] = z[1] + z[4];
z[1] = z[1] * z[10];
z[1] = z[1] + z[2] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W110 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[15];
z[0] = 1 / (k.W[109]).real();
z[1] = k.v[1] + k.v[2];
z[1] = k.v[2] * z[1];
z[2] = 2 * k.v[2];
z[3] = k.v[1] + z[2];
z[4] = k.v[0] + -z[3];
z[4] = k.v[0] * z[4];
z[5] = k.v[0] + -k.v[2];
z[6] = k.v[4] * z[5];
z[1] = z[1] + z[4] + z[6];
z[1] = dl[5] * z[1];
z[4] = dl[1] * k.v[1];
z[6] = -z[4] * z[5];
z[7] = k.v[0] * z[2];
z[8] = k.v[2] * k.v[4];
z[9] = z[7] + z[8];
z[10] = k.v[2] * z[3];
z[11] = -z[9] + z[10];
z[11] = dl[2] * z[11];
z[12] = k.v[0] * k.v[4];
z[3] = 2 * k.v[0] + -z[3];
z[3] = k.v[0] * z[3];
z[3] = z[3] + z[12];
z[3] = dl[0] * z[3];
z[1] = z[1] + z[3] + z[6] + z[11];
z[1] = k.v[5] * z[1];
z[3] = prod_pow(k.v[2], 2);
z[6] = -2 * k.v[1] + -3 * k.v[2];
z[6] = z[3] * z[6];
z[11] = k.v[1] + 4 * k.v[2];
z[11] = k.v[2] * z[11];
z[13] = k.v[0] * k.v[2];
z[14] = z[11] + -z[13];
z[14] = k.v[0] * z[14];
z[9] = -z[9] + z[11];
z[9] = k.v[4] * z[9];
z[6] = z[6] + z[9] + z[14];
z[6] = dl[2] * z[6];
z[9] = k.v[3] + z[2];
z[11] = -k.v[0] * z[9];
z[2] = -k.v[4] * z[2];
z[5] = k.v[5] * z[5];
z[2] = z[2] + z[5] + z[10] + z[11];
z[2] = dl[4] * k.v[4] * z[2];
z[3] = -z[3] + z[8] + z[13];
z[3] = z[3] * z[4];
z[4] = -z[7] + z[10];
z[4] = k.v[0] * z[4];
z[5] = -z[9] * z[12];
z[4] = z[4] + z[5];
z[4] = dl[0] * z[4];
z[5] = -dl[3] * k.v[3] * z[12];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
return z[0] * z[1];
}

template <typename T> T dlog_W111 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[7];
z[0] = 1 / (k.W[110]).real();
z[1] = dl[5] * k.v[5];
z[2] = dl[2] * k.v[2];
z[3] = -dl[2] * k.v[5];
z[3] = -z[1] + 2 * z[2] + z[3];
z[3] = k.v[2] * z[3];
z[4] = dl[1] * k.v[2];
z[5] = -dl[4] * k.v[2];
z[5] = -z[2] + -z[4] + z[5];
z[5] = k.v[4] * z[5];
z[6] = k.v[2] + -k.v[5];
z[4] = z[4] * z[6];
z[3] = z[3] + z[4] + z[5];
z[3] = k.v[1] * z[3];
z[1] = z[1] + -z[2];
z[2] = dl[0] + dl[4];
z[4] = z[2] * z[6];
z[2] = dl[3] + z[2];
z[2] = k.v[3] * z[2];
z[2] = -z[1] + z[2] + z[4];
z[2] = k.v[4] * z[2];
z[4] = -dl[0] + -dl[1];
z[4] = z[4] * z[6];
z[1] = z[1] + z[4];
z[1] = k.v[1] * z[1];
z[1] = z[1] + z[2];
z[1] = k.v[0] * z[1];
z[1] = z[1] + z[3];
return z[0] * z[1];
}

template <typename T> T dlog_W112 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[7];
z[0] = 1 / (k.W[111]).real();
z[1] = dl[0] + dl[2];
z[2] = k.v[1] * k.v[2];
z[1] = z[1] * z[2];
z[3] = dl[0] + dl[5];
z[4] = -k.v[1] * z[3];
z[3] = dl[4] + z[3];
z[3] = k.v[4] * z[3];
z[3] = z[3] + z[4];
z[3] = k.v[5] * z[3];
z[4] = -k.v[1] * k.v[5];
z[2] = z[2] + z[4];
z[2] = dl[1] * z[2];
z[4] = 2 * dl[0];
z[5] = dl[1] + z[4];
z[5] = k.v[1] * z[5];
z[6] = -dl[4] + -z[4];
z[6] = k.v[4] * z[6];
z[5] = z[5] + z[6];
z[5] = k.v[0] * z[5];
z[1] = z[1] + z[2] + z[3] + z[5];
z[1] = k.v[0] * z[1];
z[2] = dl[3] + dl[5];
z[3] = dl[2] + z[2];
z[3] = k.v[2] * z[3];
z[2] = -dl[4] + -z[2];
z[2] = k.v[4] * z[2];
z[5] = -dl[3] + -2 * dl[5];
z[5] = k.v[5] * z[5];
z[2] = z[2] + z[3] + z[5];
z[2] = k.v[5] * z[2];
z[3] = dl[0] + dl[3];
z[5] = -dl[2] + -z[3];
z[5] = k.v[2] * z[5];
z[4] = -dl[3] + -z[4];
z[4] = k.v[0] * z[4];
z[4] = z[4] + z[5];
z[5] = dl[4] + z[3];
z[5] = k.v[4] * z[5];
z[3] = dl[5] + z[3];
z[3] = k.v[5] * z[3];
z[3] = 3 * z[3] + 2 * z[4] + z[5];
z[3] = k.v[0] * z[3];
z[2] = z[2] + z[3];
z[2] = k.v[3] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W113 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[112]).real();
z[1] = -dl[0] + -dl[3] + -dl[4];
z[1] = k.v[4] * z[1];
z[2] = dl[0] + dl[2];
z[3] = dl[3] + z[2];
z[3] = k.v[2] * z[3];
z[4] = dl[0] + 2 * dl[3];
z[4] = k.v[3] * z[4];
z[1] = z[1] + 2 * z[3] + z[4];
z[1] = k.v[3] * z[1];
z[3] = -dl[0] + -dl[4] + -dl[5];
z[3] = k.v[4] * z[3];
z[4] = dl[0] + dl[1] + dl[5];
z[4] = k.v[1] * z[4];
z[3] = z[3] + z[4];
z[3] = k.v[5] * z[3];
z[4] = 2 * dl[0];
z[5] = dl[4] + z[4];
z[5] = k.v[4] * z[5];
z[4] = -dl[3] + -z[4];
z[4] = k.v[3] * z[4];
z[4] = z[4] + z[5];
z[4] = k.v[0] * z[4];
z[2] = -dl[1] + -z[2];
z[2] = k.v[1] * k.v[2] * z[2];
z[1] = z[1] + z[2] + z[3] + z[4];
z[1] = k.v[0] * z[1];
z[2] = dl[3] + dl[5];
z[3] = dl[4] + z[2];
z[3] = k.v[4] * z[3];
z[4] = -dl[1] + -z[2];
z[4] = k.v[1] * z[4];
z[2] = -dl[2] + -z[2];
z[2] = k.v[2] * z[2];
z[2] = z[2] + z[3] + z[4];
z[2] = k.v[3] * k.v[5] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

template <typename T> T dlog_W114 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[15];
z[0] = 1 / (k.W[113]).real();
z[1] = 3 * k.v[5];
z[2] = k.v[4] + z[1];
z[3] = 2 * k.v[0];
z[4] = -z[2] + z[3];
z[4] = k.v[0] * z[4];
z[5] = prod_pow(k.v[5], 2);
z[6] = k.v[4] * k.v[5];
z[7] = -k.v[5] + z[3];
z[8] = k.v[2] * z[7];
z[4] = z[4] + z[5] + z[6] + z[8];
z[4] = dl[3] * z[4];
z[8] = k.v[2] * k.v[5];
z[8] = -z[6] + z[8];
z[9] = k.v[0] * z[1];
z[10] = 2 * z[5];
z[11] = -z[8] + -z[9] + z[10];
z[11] = dl[5] * z[11];
z[12] = dl[2] * k.v[2];
z[7] = z[7] * z[12];
z[13] = 4 * k.v[0];
z[2] = -z[2] + z[13];
z[2] = k.v[0] * z[2];
z[14] = k.v[2] * z[3];
z[2] = z[2] + z[14];
z[2] = dl[0] * z[2];
z[14] = -k.v[0] * k.v[4];
z[14] = z[6] + z[14];
z[14] = dl[4] * z[14];
z[2] = z[2] + z[4] + z[7] + z[11] + z[14];
z[2] = k.v[3] * z[2];
z[4] = k.v[1] + 4 * k.v[5];
z[3] = z[3] + -z[4];
z[3] = k.v[0] * z[3];
z[7] = k.v[0] + -k.v[5];
z[7] = k.v[2] * z[7];
z[3] = z[3] + z[6] + 2 * z[7] + z[10];
z[3] = z[3] * z[12];
z[7] = k.v[1] * k.v[5];
z[11] = z[6] + z[7];
z[9] = 6 * z[5] + -z[9] + z[11];
z[9] = k.v[0] * z[9];
z[12] = k.v[0] * k.v[5];
z[12] = -z[5] + z[12];
z[8] = -z[8] + -4 * z[12];
z[8] = k.v[2] * z[8];
z[12] = prod_pow(k.v[5], 3);
z[10] = -k.v[4] * z[10];
z[8] = z[8] + z[9] + z[10] + -3 * z[12];
z[8] = dl[5] * z[8];
z[1] = -k.v[1] + -z[1];
z[1] = 3 * k.v[0] + 2 * z[1];
z[1] = k.v[0] * z[1];
z[1] = z[1] + 3 * z[5] + z[11];
z[1] = k.v[0] * z[1];
z[4] = -z[4] + z[13];
z[4] = k.v[0] * z[4];
z[9] = k.v[0] * k.v[2];
z[4] = z[4] + z[9];
z[4] = k.v[2] * z[4];
z[1] = z[1] + z[4];
z[1] = dl[0] * z[1];
z[4] = k.v[0] + k.v[2];
z[4] = z[4] * z[6];
z[5] = k.v[4] * z[5];
z[4] = z[4] + -z[5];
z[4] = dl[4] * z[4];
z[5] = -k.v[0] * k.v[1];
z[5] = z[5] + z[7];
z[5] = k.v[0] * z[5];
z[6] = -k.v[1] * z[9];
z[5] = z[5] + z[6];
z[5] = dl[1] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[8];
return z[0] * z[1];
}

template <typename T> T dlog_W115 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[10];
z[0] = 1 / (k.W[114]).real();
z[1] = dl[0] * k.v[5];
z[2] = dl[5] * k.v[5];
z[1] = z[1] + z[2];
z[3] = dl[0] * k.v[0];
z[4] = -z[1] + 2 * z[3];
z[4] = k.v[0] * z[4];
z[5] = k.v[0] + -k.v[5];
z[6] = dl[3] * k.v[0];
z[7] = z[5] * z[6];
z[8] = -dl[1] + -dl[3];
z[8] = k.v[5] * z[8];
z[8] = -z[2] + z[8];
z[8] = k.v[1] * z[8];
z[9] = z[3] + 2 * z[6];
z[9] = k.v[3] * z[9];
z[4] = z[4] + z[7] + z[8] + z[9];
z[4] = k.v[3] * z[4];
z[7] = dl[1] + dl[2];
z[5] = z[5] * z[7];
z[2] = -z[2] + z[3] + z[5];
z[2] = k.v[2] * z[2];
z[3] = -k.v[0] * z[1];
z[5] = prod_pow(k.v[5], 2);
z[7] = -k.v[0] * k.v[5];
z[7] = z[5] + z[7];
z[7] = dl[1] * z[7];
z[5] = dl[5] * z[5];
z[2] = z[2] + z[3] + 2 * z[5] + z[7];
z[2] = k.v[1] * z[2];
z[3] = -dl[4] * k.v[5];
z[5] = 2 * dl[0] + dl[4];
z[5] = k.v[0] * z[5];
z[1] = -z[1] + z[3] + z[5];
z[1] = k.v[0] * z[1];
z[3] = dl[0] + dl[4];
z[3] = k.v[0] * z[3];
z[3] = z[3] + z[6];
z[3] = k.v[3] * z[3];
z[1] = z[1] + z[3];
z[1] = k.v[4] * z[1];
z[1] = z[1] + z[2] + z[4];
return z[0] * z[1];
}

template <typename T> T dlog_W116 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[13];
z[0] = 1 / (k.W[115]).real();
z[1] = k.v[5] * z[0];
z[2] = dl[0] * z[1];
z[3] = k.v[0] * z[0];
z[4] = dl[0] * z[3];
z[5] = dl[5] * z[1];
z[6] = z[2] + -2 * z[4] + z[5];
z[7] = 2 * k.v[0];
z[7] = z[6] * z[7];
z[8] = z[4] + -z[5];
z[9] = k.v[2] * z[8];
z[10] = -z[1] + z[3];
z[11] = -k.v[0] + k.v[2];
z[11] = z[10] * z[11];
z[12] = k.v[4] * z[1];
z[12] = 2 * z[11] + z[12];
z[12] = dl[2] * z[12];
z[7] = z[7] + z[9] + z[12];
z[7] = k.v[2] * z[7];
z[9] = dl[2] * z[10];
z[8] = z[8] + z[9];
z[8] = k.v[2] * z[8];
z[6] = k.v[0] * z[6];
z[9] = dl[1] * z[11];
z[6] = z[6] + z[8] + z[9];
z[6] = k.v[1] * z[6];
z[8] = dl[0] + dl[4];
z[9] = -dl[5] + -z[8];
z[9] = z[1] * z[9];
z[8] = k.v[3] * z[0] * z[8];
z[8] = z[8] + z[9];
z[8] = k.v[0] * z[8];
z[1] = dl[4] * z[1];
z[1] = z[1] + z[5];
z[1] = k.v[2] * z[1];
z[3] = dl[3] * k.v[3] * z[3];
z[1] = z[1] + z[3] + z[8];
z[1] = k.v[4] * z[1];
z[2] = -2 * z[2] + 3 * z[4] + -z[5];
z[2] = prod_pow(k.v[0], 2) * z[2];
return z[1] + z[2] + z[6] + z[7];
}

template <typename T> T dlog_W117 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dl) {
T z[6];
z[0] = 1 / (k.W[116]).real();
z[1] = dl[0] + dl[4];
z[2] = dl[5] + z[1];
z[2] = k.v[4] * z[2];
z[3] = dl[0] + dl[1];
z[4] = -dl[5] + -z[3];
z[4] = k.v[1] * z[4];
z[2] = z[2] + z[4];
z[2] = k.v[5] * z[2];
z[4] = 2 * dl[0];
z[5] = -dl[4] + -z[4];
z[5] = k.v[4] * z[5];
z[4] = dl[1] + z[4];
z[4] = k.v[1] * z[4];
z[4] = z[4] + z[5];
z[4] = k.v[0] * z[4];
z[1] = -dl[3] + -z[1];
z[1] = k.v[3] * k.v[4] * z[1];
z[1] = z[1] + z[2] + z[4];
z[1] = k.v[0] * z[1];
z[2] = dl[2] * k.v[1];
z[4] = dl[1] + dl[5];
z[4] = k.v[1] * z[4];
z[4] = z[2] + z[4];
z[4] = k.v[5] * z[4];
z[3] = -k.v[1] * z[3];
z[2] = -z[2] + z[3];
z[2] = k.v[0] * z[2];
z[2] = z[2] + z[4];
z[2] = k.v[2] * z[2];
z[1] = z[1] + z[2];
return z[0] * z[1];
}


template <typename T> T dlog_W118_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[2];
z[0] = k.v[0] + -k.v[2] + k.v[4];
z[1] = dv[0] + -dv[2] + dv[4];
a = {z[0], z[1]};
}


    T s1 = (k.W[194]*k.W[194]).real();

    T ds1 = get_sqrt_derivatives(194, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W118 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W118_re(k,dv);
	return re/((k.W[194]).real());
}


template <typename T> T dlog_W119_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[2];
z[0] = k.v[0] + k.v[1] + k.v[2] + -k.v[4] + -k.v[5];
z[1] = dv[0] + dv[1] + dv[2] + -dv[4] + -dv[5];
a = {z[0], z[1]};
}


    T s1 = (k.W[195]*k.W[195]).real();

    T ds1 = get_sqrt_derivatives(195, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W119 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W119_re(k,dv);
	return re/((k.W[195]).real());
}


template <typename T> T dlog_W120_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[2];
z[0] = k.v[1] + k.v[5];
z[1] = dv[1] + dv[5];
a = {z[0], z[1]};
}


    T s1 = (k.W[196]*k.W[196]).real();

    T ds1 = get_sqrt_derivatives(196, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W120 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W120_re(k,dv);
	return re/((k.W[196]).real());
}


template <typename T> T dlog_W121_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[2];
z[0] = 2 * k.v[0] + -k.v[1] + -k.v[5];
z[1] = 2 * dv[0] + -dv[1] + -dv[5];
a = {z[0], z[1]};
}


    T s1 = (k.W[196]*k.W[196]).real();

    T ds1 = get_sqrt_derivatives(196, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W121 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W121_re(k,dv);
	return re/((k.W[196]).real());
}


template <typename T> T dlog_W122_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[2];
z[0] = k.v[0] + -k.v[1] + -k.v[2] + k.v[4] + k.v[5];
z[1] = dv[0] + -dv[1] + -dv[2] + dv[4] + dv[5];
a = {z[0], z[1]};
}


    T s1 = (k.W[195]*k.W[195]).real();

    T ds1 = get_sqrt_derivatives(195, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W122 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W122_re(k,dv);
	return re/((k.W[195]).real());
}


template <typename T> T dlog_W123_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[2];
z[0] = k.v[0] + k.v[2] + -k.v[4];
z[1] = dv[0] + dv[2] + -dv[4];
a = {z[0], z[1]};
}


    T s1 = (k.W[194]*k.W[194]).real();

    T ds1 = get_sqrt_derivatives(194, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W123 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W123_re(k,dv);
	return re/((k.W[194]).real());
}


template <typename T> T dlog_W124_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[2];
z[0] = -k.v[0] + 2 * k.v[1] + k.v[2] + -k.v[4];
z[1] = -dv[0] + 2 * dv[1] + dv[2] + -dv[4];
a = {z[0], z[1]};
}


    T s1 = (k.W[194]*k.W[194]).real();

    T ds1 = get_sqrt_derivatives(194, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W124 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W124_re(k,dv);
	return re/((k.W[194]).real());
}


template <typename T> T dlog_W125_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[2];
z[0] = -k.v[0] + k.v[1] + -k.v[2] + k.v[4] + k.v[5];
z[1] = -dv[0] + dv[1] + -dv[2] + dv[4] + dv[5];
a = {z[0], z[1]};
}


    T s1 = (k.W[195]*k.W[195]).real();

    T ds1 = get_sqrt_derivatives(195, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W125 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W125_re(k,dv);
	return re/((k.W[195]).real());
}


template <typename T> T dlog_W126_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[2];
z[0] = k.v[1] + -k.v[5];
z[1] = dv[1] + -dv[5];
a = {z[0], z[1]};
}


    T s1 = (k.W[196]*k.W[196]).real();

    T ds1 = get_sqrt_derivatives(196, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W126 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W126_re(k,dv);
	return re/((k.W[196]).real());
}


template <typename T> T dlog_W127_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[2];
z[0] = -k.v[2] + k.v[4];
z[0] = -k.v[1] + k.v[5] + 2 * z[0];
z[1] = -dv[2] + dv[4];
z[1] = -dv[1] + dv[5] + 2 * z[1];
a = {z[0], z[1]};
}


    T s1 = (k.W[196]*k.W[196]).real();

    T ds1 = get_sqrt_derivatives(196, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W127 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W127_re(k,dv);
	return re/((k.W[196]).real());
}


template <typename T> T dlog_W128_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[2];
z[0] = k.v[0] + -k.v[1] + -k.v[2] + k.v[4] + -k.v[5];
z[1] = dv[0] + -dv[1] + -dv[2] + dv[4] + -dv[5];
a = {z[0], z[1]};
}


    T s1 = (k.W[195]*k.W[195]).real();

    T ds1 = get_sqrt_derivatives(195, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W128 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W128_re(k,dv);
	return re/((k.W[195]).real());
}


template <typename T> T dlog_W129_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[2];
z[0] = -k.v[0] + -k.v[2] + k.v[4] + 2 * k.v[5];
z[1] = -dv[0] + -dv[2] + dv[4] + 2 * dv[5];
a = {z[0], z[1]};
}


    T s1 = (k.W[194]*k.W[194]).real();

    T ds1 = get_sqrt_derivatives(194, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W129 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W129_re(k,dv);
	return re/((k.W[194]).real());
}


template <typename T> T dlog_W130_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[4] * k.v[5];
z[1] = -k.v[2] + k.v[5];
z[2] = k.v[1] * z[1];
z[3] = k.v[0] + k.v[2] + -k.v[4];
z[4] = -k.v[3] * z[3];
z[0] = z[0] + z[2] + z[4];
z[2] = -dv[4] * k.v[5];
z[1] = dv[1] * z[1];
z[4] = -dv[2] * k.v[1];
z[5] = k.v[1] + -k.v[4];
z[5] = dv[5] * z[5];
z[3] = -dv[3] * z[3];
z[6] = -dv[0] + -dv[2] + dv[4];
z[6] = k.v[3] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[0], z[1]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W130 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W130_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W131_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = k.v[4] * k.v[5];
z[1] = k.v[2] + k.v[5];
z[2] = -k.v[1] * z[1];
z[3] = k.v[0] + k.v[2] + -k.v[4];
z[4] = k.v[3] * z[3];
z[0] = z[0] + z[2] + z[4];
z[2] = dv[4] * k.v[5];
z[1] = -dv[1] * z[1];
z[4] = -dv[2] * k.v[1];
z[5] = -k.v[1] + k.v[4];
z[5] = dv[5] * z[5];
z[3] = dv[3] * z[3];
z[6] = dv[0] + dv[2] + -dv[4];
z[6] = k.v[3] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[0], z[1]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W131 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W131_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W132_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = k.v[1] * k.v[2];
z[1] = -k.v[1] + k.v[4];
z[2] = k.v[5] * z[1];
z[3] = k.v[0] + -2 * k.v[1] + -k.v[2] + k.v[4];
z[4] = -k.v[3] * z[3];
z[0] = z[0] + z[2] + z[4];
z[1] = dv[5] * z[1];
z[2] = dv[1] * k.v[2];
z[4] = -dv[1] + dv[4];
z[4] = k.v[5] * z[4];
z[3] = -dv[3] * z[3];
z[5] = dv[2] * k.v[1];
z[6] = -dv[0] + 2 * dv[1] + dv[2] + -dv[4];
z[6] = k.v[3] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[0], z[1]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W132 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W132_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W133_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[4] * k.v[5];
z[1] = -k.v[2] + k.v[5];
z[2] = -k.v[1] * z[1];
z[3] = k.v[0] + -k.v[2] + k.v[4];
z[4] = k.v[3] * z[3];
z[0] = z[0] + z[2] + z[4];
z[2] = -dv[4] * k.v[5];
z[1] = -dv[1] * z[1];
z[4] = dv[2] * k.v[1];
z[5] = -k.v[1] + -k.v[4];
z[5] = dv[5] * z[5];
z[3] = dv[3] * z[3];
z[6] = dv[0] + -dv[2] + dv[4];
z[6] = k.v[3] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[0], z[1]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W133 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W133_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W134_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = 2 * k.v[0];
z[1] = 2 * k.v[1];
z[2] = z[0] + -z[1];
z[3] = k.v[2] + -2 * k.v[4] + k.v[5];
z[4] = -z[2] + z[3];
z[4] = k.v[1] * z[4];
z[5] = -k.v[5] + z[0];
z[5] = k.v[4] * z[5];
z[1] = -k.v[0] + k.v[2] + -k.v[4] + z[1];
z[6] = -k.v[3] * z[1];
z[4] = z[4] + z[5] + z[6];
z[2] = k.v[3] + -k.v[5] + z[2];
z[2] = dv[4] * z[2];
z[0] = 4 * k.v[1] + -2 * k.v[3] + -z[0] + z[3];
z[0] = dv[1] * z[0];
z[3] = k.v[1] + -k.v[3];
z[3] = dv[2] * z[3];
z[5] = k.v[1] + -k.v[4];
z[6] = dv[5] * z[5];
z[1] = -dv[3] * z[1];
z[5] = k.v[3] + -2 * z[5];
z[5] = dv[0] * z[5];
z[0] = z[0] + z[1] + z[2] + z[3] + z[5] + z[6];
a = {z[4], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W134 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W134_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W135_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[1] + 2 * k.v[4];
z[1] = 2 * k.v[2];
z[2] = -z[0] + z[1];
z[2] = k.v[2] * z[2];
z[3] = -k.v[1] + k.v[4] + -z[1];
z[4] = k.v[5] * z[3];
z[5] = k.v[0] + k.v[2] + -k.v[4];
z[6] = k.v[3] * z[5];
z[2] = z[2] + z[4] + z[6];
z[3] = dv[5] * z[3];
z[0] = 4 * k.v[2] + -2 * k.v[5] + -z[0];
z[0] = dv[2] * z[0];
z[1] = k.v[5] + -z[1];
z[1] = dv[4] * z[1];
z[4] = k.v[2] + -k.v[5];
z[4] = dv[1] * z[4];
z[6] = dv[0] + dv[2] + -dv[4];
z[6] = k.v[3] * z[6];
z[5] = dv[3] * z[5];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6];
a = {z[2], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W135 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W135_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W136_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[0] + k.v[4];
z[1] = k.v[3] * z[0];
z[2] = -k.v[1] + k.v[4];
z[3] = -k.v[5] * z[2];
z[4] = -k.v[0] + k.v[5];
z[4] = -k.v[1] + k.v[3] + -2 * z[4];
z[5] = -k.v[2] * z[4];
z[1] = z[1] + z[3] + z[5];
z[0] = dv[3] * z[0];
z[3] = -dv[0] + dv[4];
z[3] = k.v[3] * z[3];
z[2] = -dv[5] * z[2];
z[5] = dv[1] + -dv[4];
z[5] = k.v[5] * z[5];
z[6] = -dv[0] + dv[5];
z[6] = dv[1] + -dv[3] + 2 * z[6];
z[6] = k.v[2] * z[6];
z[4] = -dv[2] * z[4];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W136 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W136_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W137_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[8];
z[0] = 2 * k.v[0];
z[1] = 2 * k.v[5];
z[2] = z[0] + -z[1];
z[3] = k.v[1] + -2 * k.v[2] + k.v[4];
z[4] = -z[2] + z[3];
z[4] = k.v[5] * z[4];
z[5] = -k.v[1] + z[0];
z[5] = k.v[2] * z[5];
z[1] = k.v[0] + k.v[2] + -k.v[4] + -z[1];
z[6] = k.v[3] * z[1];
z[4] = z[4] + z[5] + z[6];
z[2] = -k.v[1] + k.v[3] + z[2];
z[2] = dv[2] * z[2];
z[0] = -2 * k.v[3] + 4 * k.v[5] + -z[0] + z[3];
z[0] = dv[5] * z[0];
z[3] = -dv[1] * k.v[2];
z[5] = dv[1] + dv[4];
z[5] = k.v[5] * z[5];
z[6] = -dv[4] * k.v[3];
z[7] = k.v[2] + -k.v[5];
z[7] = k.v[3] + 2 * z[7];
z[7] = dv[0] * z[7];
z[1] = dv[3] * z[1];
z[0] = z[0] + z[1] + z[2] + z[3] + z[5] + z[6] + z[7];
a = {z[4], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W137 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W137_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W138_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = 2 * k.v[0] + -k.v[5];
z[1] = -k.v[4] * z[0];
z[2] = k.v[0] + -k.v[2] + k.v[4];
z[3] = -k.v[3] * z[2];
z[4] = k.v[2] + -z[0];
z[5] = -k.v[1] * z[4];
z[1] = z[1] + z[3] + z[5];
z[3] = 2 * dv[0] + -dv[5];
z[5] = -k.v[4] * z[3];
z[0] = -dv[4] * z[0];
z[2] = -dv[3] * z[2];
z[6] = -dv[0] + dv[2] + -dv[4];
z[6] = k.v[3] * z[6];
z[3] = -dv[2] + z[3];
z[3] = k.v[1] * z[3];
z[4] = -dv[1] * z[4];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W138 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W138_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W139_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[4] * k.v[5];
z[1] = -k.v[2] + k.v[5];
z[2] = k.v[1] * z[1];
z[3] = k.v[0] + -k.v[2] + k.v[4];
z[4] = k.v[3] * z[3];
z[0] = z[0] + z[2] + z[4];
z[2] = -dv[4] * k.v[5];
z[1] = dv[1] * z[1];
z[4] = -dv[2] * k.v[1];
z[5] = k.v[1] + -k.v[4];
z[5] = dv[5] * z[5];
z[3] = dv[3] * z[3];
z[6] = dv[0] + -dv[2] + dv[4];
z[6] = k.v[3] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[0], z[1]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W139 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W139_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W140_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = 2 * k.v[0] + -k.v[5];
z[1] = k.v[4] * z[0];
z[2] = k.v[0] + -k.v[2] + k.v[4];
z[3] = k.v[3] * z[2];
z[4] = k.v[2] + -k.v[5];
z[5] = k.v[1] * z[4];
z[1] = z[1] + z[3] + z[5];
z[3] = k.v[1] + -k.v[3];
z[3] = dv[2] * z[3];
z[4] = dv[1] * z[4];
z[0] = k.v[3] + z[0];
z[0] = dv[4] * z[0];
z[5] = k.v[3] + 2 * k.v[4];
z[5] = dv[0] * z[5];
z[6] = -k.v[1] + -k.v[4];
z[6] = dv[5] * z[6];
z[2] = dv[3] * z[2];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W140 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W140_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W141_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = k.v[1] * k.v[2];
z[1] = -k.v[1] + k.v[4];
z[2] = k.v[5] * z[1];
z[3] = k.v[0] + 2 * k.v[1] + k.v[2] + -k.v[4];
z[4] = k.v[3] * z[3];
z[0] = z[0] + z[2] + z[4];
z[1] = dv[5] * z[1];
z[2] = dv[1] * k.v[2];
z[4] = -dv[1] + dv[4];
z[4] = k.v[5] * z[4];
z[3] = dv[3] * z[3];
z[5] = dv[2] * k.v[1];
z[6] = dv[0] + 2 * dv[1] + dv[2] + -dv[4];
z[6] = k.v[3] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[0], z[1]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W141 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W141_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W142_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[2] + 2 * k.v[4];
z[1] = k.v[1] * z[0];
z[2] = -k.v[0] + k.v[2] + -k.v[4];
z[3] = k.v[3] * z[2];
z[4] = k.v[1] + k.v[4];
z[5] = k.v[5] * z[4];
z[1] = z[1] + z[3] + z[5];
z[2] = dv[3] * z[2];
z[3] = -dv[2] + 2 * dv[4];
z[3] = k.v[1] * z[3];
z[4] = dv[5] * z[4];
z[5] = -dv[0] + dv[2] + -dv[4];
z[5] = k.v[3] * z[5];
z[6] = dv[4] * k.v[5];
z[0] = k.v[5] + z[0];
z[0] = dv[1] * z[0];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W142 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W142_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W143_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[2] + 2 * k.v[4];
z[1] = 2 * k.v[1];
z[2] = -z[0] + z[1];
z[2] = k.v[1] * z[2];
z[3] = k.v[0] + k.v[2] + -k.v[4] + z[1];
z[4] = -k.v[3] * z[3];
z[5] = k.v[1] + -k.v[4];
z[6] = k.v[5] * z[5];
z[2] = z[2] + z[4] + z[6];
z[1] = k.v[3] + -k.v[5] + -z[1];
z[1] = dv[4] * z[1];
z[4] = k.v[1] + -k.v[3];
z[4] = dv[2] * z[4];
z[3] = -dv[3] * z[3];
z[6] = -dv[0] * k.v[3];
z[5] = dv[5] * z[5];
z[0] = 4 * k.v[1] + -2 * k.v[3] + k.v[5] + -z[0];
z[0] = dv[1] * z[0];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6];
a = {z[2], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W143 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W143_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W144_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[8];
z[0] = 2 * k.v[0];
z[1] = k.v[2] + k.v[5];
z[2] = k.v[0] + -z[1];
z[2] = z[0] * z[2];
z[1] = -z[0] + z[1];
z[3] = k.v[1] * z[1];
z[4] = 2 * k.v[2] + -k.v[4];
z[5] = k.v[5] * z[4];
z[6] = k.v[0] + -k.v[2] + k.v[4];
z[7] = k.v[3] * z[6];
z[2] = z[2] + z[3] + z[5] + z[7];
z[0] = k.v[1] + -z[0] + z[4];
z[0] = dv[5] * z[0];
z[3] = dv[1] * z[1];
z[1] = -k.v[1] + -z[1];
z[1] = k.v[3] + 2 * z[1];
z[1] = dv[0] * z[1];
z[4] = k.v[3] + -k.v[5];
z[4] = dv[4] * z[4];
z[5] = dv[3] * z[6];
z[6] = k.v[0] + -k.v[5];
z[6] = k.v[1] + -k.v[3] + -2 * z[6];
z[6] = dv[2] * z[6];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6];
a = {z[2], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W144 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W144_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W145_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[9];
z[0] = 2 * k.v[2];
z[1] = k.v[3] + z[0];
z[2] = k.v[0] + k.v[4];
z[3] = -k.v[2] + z[2];
z[4] = -z[1] * z[3];
z[5] = 2 * k.v[0] + k.v[4] + -z[0];
z[6] = k.v[5] * z[5];
z[7] = -k.v[2] + k.v[5];
z[8] = -k.v[1] * z[7];
z[4] = z[4] + z[6] + z[8];
z[2] = z[0] + -z[2];
z[6] = -k.v[3] + 2 * k.v[5];
z[2] = k.v[1] + 2 * z[2] + -z[6];
z[2] = dv[2] * z[2];
z[0] = -z[0] + z[6];
z[0] = dv[0] * z[0];
z[1] = k.v[5] + -z[1];
z[1] = dv[4] * z[1];
z[5] = -k.v[1] + z[5];
z[5] = dv[5] * z[5];
z[6] = -dv[1] * z[7];
z[3] = -dv[3] * z[3];
z[0] = z[0] + z[1] + z[2] + z[3] + z[5] + z[6];
a = {z[4], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W145 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W145_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W146_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = 2 * k.v[0] + -k.v[5];
z[1] = k.v[4] * z[0];
z[2] = k.v[0] + -k.v[2] + k.v[4];
z[3] = -k.v[3] * z[2];
z[4] = k.v[2] + -k.v[5];
z[5] = -k.v[1] * z[4];
z[1] = z[1] + z[3] + z[5];
z[3] = -k.v[1] + k.v[3];
z[3] = dv[2] * z[3];
z[4] = -dv[1] * z[4];
z[0] = -k.v[3] + z[0];
z[0] = dv[4] * z[0];
z[5] = -k.v[3] + 2 * k.v[4];
z[5] = dv[0] * z[5];
z[6] = k.v[1] + -k.v[4];
z[6] = dv[5] * z[6];
z[2] = -dv[3] * z[2];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W146 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W146_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W147_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = 2 * k.v[0] + -k.v[1];
z[1] = k.v[4] + z[0];
z[2] = k.v[5] * z[1];
z[3] = -k.v[2] * z[0];
z[4] = 3 * k.v[0] + -2 * k.v[1] + k.v[4];
z[5] = k.v[2] + -z[4];
z[5] = k.v[3] * z[5];
z[2] = z[2] + z[3] + z[5];
z[3] = -dv[3] * z[4];
z[0] = -dv[2] * z[0];
z[4] = 2 * dv[0] + -dv[1];
z[5] = dv[4] + z[4];
z[5] = k.v[5] * z[5];
z[1] = dv[5] * z[1];
z[4] = dv[3] + -z[4];
z[4] = k.v[2] * z[4];
z[6] = -3 * dv[0] + 2 * dv[1] + dv[2] + -dv[4];
z[6] = k.v[3] * z[6];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6];
a = {z[2], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W147 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W147_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W148_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[0] + k.v[2];
z[1] = -k.v[5] + 2 * z[0];
z[2] = 2 * k.v[4];
z[3] = -z[1] + z[2];
z[3] = k.v[4] * z[3];
z[4] = -k.v[2] + k.v[5] + z[2];
z[5] = -k.v[1] * z[4];
z[0] = -k.v[4] + z[0];
z[6] = -k.v[3] * z[0];
z[3] = z[3] + z[5] + z[6];
z[0] = -dv[3] * z[0];
z[2] = k.v[3] + z[2];
z[5] = k.v[1] + -z[2];
z[5] = dv[2] * z[5];
z[6] = -k.v[1] + k.v[4];
z[6] = dv[5] * z[6];
z[2] = dv[0] * z[2];
z[1] = -2 * k.v[1] + k.v[3] + 4 * k.v[4] + -z[1];
z[1] = dv[4] * z[1];
z[4] = -dv[1] * z[4];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + z[6];
a = {z[3], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W148 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W148_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W149_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[9];
z[0] = prod_pow(k.v[0], 2);
z[1] = k.v[0] * k.v[4];
z[0] = z[0] + z[1];
z[1] = 2 * k.v[0];
z[2] = k.v[4] + z[1];
z[3] = -k.v[2] + -k.v[5] + 2 * z[2];
z[4] = 2 * k.v[1];
z[5] = -z[3] + z[4];
z[5] = k.v[1] * z[5];
z[6] = -k.v[5] * z[2];
z[7] = -3 * k.v[0] + k.v[2] + -k.v[4] + z[4];
z[8] = -k.v[3] * z[7];
z[0] = 2 * z[0] + z[5] + z[6] + z[8];
z[4] = k.v[5] + z[4];
z[5] = z[2] + -z[4];
z[5] = dv[0] * z[5];
z[1] = k.v[3] + z[1] + -z[4];
z[1] = dv[4] * z[1];
z[2] = -dv[5] * z[2];
z[4] = dv[2] + dv[5];
z[4] = k.v[1] * z[4];
z[3] = 4 * k.v[1] + -z[3];
z[3] = dv[1] * z[3];
z[6] = 3 * dv[0] + -2 * dv[1] + -dv[2];
z[6] = k.v[3] * z[6];
z[7] = -dv[3] * z[7];
z[1] = z[1] + z[2] + z[3] + z[4] + 2 * z[5] + z[6] + z[7];
a = {z[0], z[1]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W149 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W149_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W150_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[9];
z[0] = -k.v[2] + k.v[5];
z[1] = k.v[5] * z[0];
z[2] = -k.v[2] + 2 * k.v[5];
z[3] = k.v[0] + -z[2];
z[3] = k.v[0] * z[3];
z[1] = z[1] + z[3];
z[3] = 2 * k.v[0];
z[0] = z[0] + -z[3];
z[4] = k.v[1] * z[0];
z[5] = -3 * k.v[0] + z[2];
z[6] = -k.v[3] * z[5];
z[7] = k.v[3] + -k.v[5];
z[8] = -k.v[4] * z[7];
z[1] = 2 * z[1] + z[4] + z[6] + z[8];
z[2] = z[2] + -z[3];
z[3] = k.v[1] + z[2];
z[3] = 3 * k.v[3] + -2 * z[3];
z[3] = dv[0] * z[3];
z[0] = dv[1] * z[0];
z[2] = -k.v[3] + z[2];
z[2] = k.v[1] + k.v[4] + 2 * z[2];
z[2] = dv[5] * z[2];
z[4] = -k.v[4] + -z[5];
z[4] = dv[3] * z[4];
z[5] = -dv[4] * z[7];
z[6] = k.v[0] + -k.v[5];
z[6] = -k.v[1] + k.v[3] + 2 * z[6];
z[6] = dv[2] * z[6];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W150 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W150_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W151_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[0] + k.v[4];
z[1] = -k.v[1] + 2 * z[0];
z[2] = 2 * k.v[2];
z[3] = -z[1] + z[2];
z[3] = k.v[2] * z[3];
z[0] = -k.v[2] + z[0];
z[4] = -k.v[3] * z[0];
z[5] = k.v[1] + -k.v[4] + z[2];
z[6] = -k.v[5] * z[5];
z[3] = z[3] + z[4] + z[6];
z[4] = k.v[2] + -k.v[5];
z[4] = dv[1] * z[4];
z[5] = -dv[5] * z[5];
z[0] = -dv[3] * z[0];
z[2] = k.v[3] + z[2];
z[6] = dv[0] * z[2];
z[1] = 4 * k.v[2] + k.v[3] + -2 * k.v[5] + -z[1];
z[1] = dv[2] * z[1];
z[2] = k.v[5] + -z[2];
z[2] = dv[4] * z[2];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + z[6];
a = {z[3], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W151 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W151_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W152_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = 2 * k.v[0] + -k.v[5];
z[1] = -k.v[2] + -z[0];
z[2] = -k.v[1] * z[1];
z[3] = -k.v[4] * z[0];
z[4] = -3 * k.v[0] + -k.v[2] + k.v[4] + 2 * k.v[5];
z[5] = k.v[3] * z[4];
z[2] = z[2] + z[3] + z[5];
z[3] = 2 * dv[0] + -dv[5];
z[5] = dv[2] + z[3];
z[5] = k.v[1] * z[5];
z[3] = -k.v[4] * z[3];
z[0] = -dv[4] * z[0];
z[6] = -3 * dv[0] + -dv[2] + dv[4] + 2 * dv[5];
z[6] = k.v[3] * z[6];
z[4] = dv[3] * z[4];
z[1] = -dv[1] * z[1];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6];
a = {z[2], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W152 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W152_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W153_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[0] + k.v[4];
z[1] = k.v[3] * z[0];
z[2] = -k.v[1] + k.v[4];
z[3] = -k.v[5] * z[2];
z[4] = -2 * k.v[0] + k.v[1] + k.v[3];
z[5] = -k.v[2] * z[4];
z[1] = z[1] + z[3] + z[5];
z[2] = -dv[5] * z[2];
z[3] = 2 * k.v[2] + -k.v[3];
z[3] = dv[0] * z[3];
z[4] = -dv[2] * z[4];
z[0] = -k.v[2] + z[0];
z[0] = dv[3] * z[0];
z[5] = -k.v[2] + k.v[5];
z[5] = dv[1] * z[5];
z[6] = k.v[3] + -k.v[5];
z[6] = dv[4] * z[6];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W153 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W153_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W154_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[9];
z[0] = 2 * k.v[4];
z[1] = k.v[3] + z[0];
z[2] = k.v[0] + k.v[2];
z[3] = -k.v[4] + z[2];
z[4] = -z[1] * z[3];
z[5] = 2 * k.v[0] + k.v[2] + -z[0];
z[6] = k.v[1] * z[5];
z[7] = k.v[1] + -k.v[4];
z[8] = -k.v[5] * z[7];
z[4] = z[4] + z[6] + z[8];
z[2] = z[0] + -z[2];
z[6] = 2 * k.v[1] + -k.v[3];
z[2] = k.v[5] + 2 * z[2] + -z[6];
z[2] = dv[4] * z[2];
z[0] = -z[0] + z[6];
z[0] = dv[0] * z[0];
z[1] = k.v[1] + -z[1];
z[1] = dv[2] * z[1];
z[5] = -k.v[5] + z[5];
z[5] = dv[1] * z[5];
z[6] = -dv[5] * z[7];
z[3] = -dv[3] * z[3];
z[0] = z[0] + z[1] + z[2] + z[3] + z[5] + z[6];
a = {z[4], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W154 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W154_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W155_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[8];
z[0] = 2 * k.v[0];
z[1] = k.v[0] + -k.v[5];
z[1] = z[0] * z[1];
z[2] = -k.v[5] + z[0];
z[3] = -k.v[4] * z[2];
z[4] = -k.v[0] + -k.v[2] + k.v[4];
z[5] = -k.v[3] * z[4];
z[6] = -k.v[2] + 2 * k.v[4] + -z[2];
z[7] = k.v[1] * z[6];
z[1] = z[1] + z[3] + z[5] + z[7];
z[3] = 2 * k.v[1] + -k.v[3];
z[5] = -z[2] + z[3];
z[5] = dv[4] * z[5];
z[2] = -k.v[4] + z[2];
z[2] = 2 * z[2] + -z[3];
z[2] = dv[0] * z[2];
z[3] = -dv[3] * z[4];
z[4] = -k.v[1] + k.v[3];
z[4] = dv[2] * z[4];
z[0] = k.v[1] + k.v[4] + -z[0];
z[0] = dv[5] * z[0];
z[6] = dv[1] * z[6];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W155 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W155_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W156_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = k.v[2] + k.v[3];
z[0] = k.v[1] + k.v[4] + -2 * z[0];
z[1] = 2 * k.v[5];
z[2] = z[0] + z[1];
z[2] = k.v[5] * z[2];
z[3] = k.v[0] + -k.v[2] + k.v[4];
z[4] = -k.v[3] * z[3];
z[5] = -k.v[1] * k.v[2];
z[2] = z[2] + z[4] + z[5];
z[3] = -z[1] + -z[3];
z[3] = dv[3] * z[3];
z[4] = -dv[1] * k.v[2];
z[5] = -dv[0] + -dv[4];
z[5] = k.v[3] * z[5];
z[6] = dv[1] + dv[4];
z[6] = k.v[5] * z[6];
z[0] = 4 * k.v[5] + z[0];
z[0] = dv[5] * z[0];
z[1] = -k.v[1] + k.v[3] + -z[1];
z[1] = dv[2] * z[1];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6];
a = {z[2], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W156 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W156_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W157_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = k.v[1] * k.v[2];
z[1] = -k.v[1] + -2 * k.v[2] + k.v[4];
z[2] = -k.v[5] * z[1];
z[3] = -k.v[0] + -k.v[2] + k.v[4];
z[4] = k.v[3] * z[3];
z[0] = z[0] + z[2] + z[4];
z[2] = -dv[4] * k.v[5];
z[1] = -dv[5] * z[1];
z[4] = k.v[1] + 2 * k.v[5];
z[4] = dv[2] * z[4];
z[3] = dv[3] * z[3];
z[5] = k.v[2] + k.v[5];
z[5] = dv[1] * z[5];
z[6] = -dv[0] + -dv[2] + dv[4];
z[6] = k.v[3] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[0], z[1]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W157 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W157_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W158_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = k.v[1] * k.v[2];
z[1] = -k.v[1] + k.v[4];
z[2] = k.v[5] * z[1];
z[3] = k.v[0] + -k.v[2] + k.v[4] + 2 * k.v[5];
z[4] = k.v[3] * z[3];
z[0] = z[0] + z[2] + z[4];
z[2] = dv[2] * k.v[1];
z[1] = dv[5] * z[1];
z[3] = dv[3] * z[3];
z[4] = dv[0] + -dv[2] + 2 * dv[5];
z[4] = k.v[3] * z[4];
z[5] = k.v[2] + -k.v[5];
z[5] = dv[1] * z[5];
z[6] = k.v[3] + k.v[5];
z[6] = dv[4] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[0], z[1]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W158 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W158_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W159_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[0] + k.v[4];
z[1] = -k.v[3] * z[0];
z[2] = -k.v[1] + k.v[4];
z[3] = k.v[5] * z[2];
z[4] = 2 * k.v[0] + -k.v[1] + k.v[3];
z[5] = k.v[2] * z[4];
z[1] = z[1] + z[3] + z[5];
z[2] = dv[5] * z[2];
z[3] = 2 * k.v[2] + k.v[3];
z[3] = dv[0] * z[3];
z[4] = dv[2] * z[4];
z[0] = k.v[2] + -z[0];
z[0] = dv[3] * z[0];
z[5] = -k.v[2] + -k.v[5];
z[5] = dv[1] * z[5];
z[6] = -k.v[3] + k.v[5];
z[6] = dv[4] * z[6];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W159 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W159_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W160_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[4] * k.v[5];
z[1] = -k.v[2] + k.v[5];
z[2] = k.v[1] * z[1];
z[3] = k.v[0] + k.v[2] + -k.v[4];
z[4] = k.v[3] * z[3];
z[0] = z[0] + z[2] + z[4];
z[2] = -dv[4] * k.v[5];
z[1] = dv[1] * z[1];
z[4] = -dv[2] * k.v[1];
z[5] = k.v[1] + -k.v[4];
z[5] = dv[5] * z[5];
z[3] = dv[3] * z[3];
z[6] = dv[0] + dv[2] + -dv[4];
z[6] = k.v[3] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[0], z[1]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W160 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W160_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W161_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = k.v[0] + -k.v[4];
z[1] = -k.v[3] * z[0];
z[2] = 2 * k.v[0] + -k.v[1];
z[3] = k.v[4] + -z[2];
z[4] = -k.v[5] * z[3];
z[2] = k.v[3] + z[2];
z[5] = -k.v[2] * z[2];
z[1] = z[1] + z[4] + z[5];
z[4] = 2 * dv[0] + -dv[1];
z[5] = -dv[4] + z[4];
z[5] = k.v[5] * z[5];
z[6] = -dv[0] + dv[4];
z[6] = k.v[3] * z[6];
z[0] = -dv[3] * z[0];
z[3] = -dv[5] * z[3];
z[2] = -dv[2] * z[2];
z[4] = -dv[3] + -z[4];
z[4] = k.v[2] * z[4];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> T dlog_W161 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W161_re(k,dv);
	return re/(-(k.W[197]).imag());
}


template <typename T> T dlog_W162_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[6];
z[0] = k.v[2] * k.v[3];
z[1] = k.v[2] + -k.v[5];
z[2] = k.v[1] * z[1];
z[3] = k.v[3] + -k.v[5];
z[4] = -k.v[4] * z[3];
z[0] = z[0] + z[2] + z[4];
z[2] = k.v[1] + k.v[3];
z[2] = dv[2] * z[2];
z[1] = dv[1] * z[1];
z[3] = -dv[4] * z[3];
z[4] = k.v[2] + -k.v[4];
z[4] = dv[3] * z[4];
z[5] = -k.v[1] + k.v[4];
z[5] = dv[5] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
a = {z[0], z[1]};
}


    T s1 = (k.W[198]*k.W[198]).real();

    T ds1 = get_sqrt_derivatives(198, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W162 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W162_re(k,dv);
	return re/(k.W[198]);
}


template <typename T> T dlog_W163_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[2] * k.v[3];
z[1] = -k.v[0] + k.v[5];
z[2] = -k.v[3] + z[1];
z[3] = -k.v[4] * z[2];
z[1] = k.v[2] + z[1];
z[4] = k.v[1] * z[1];
z[0] = z[0] + z[3] + z[4];
z[3] = -dv[2] * k.v[3];
z[4] = -dv[3] * k.v[2];
z[1] = dv[1] * z[1];
z[5] = -dv[0] + dv[5];
z[6] = dv[3] + -z[5];
z[6] = k.v[4] * z[6];
z[2] = -dv[4] * z[2];
z[5] = dv[2] + z[5];
z[5] = k.v[1] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[0], z[1]};
}


    T s1 = (k.W[199]*k.W[199]).real();

    T ds1 = get_sqrt_derivatives(199, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W163 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W163_re(k,dv);
	return re/(k.W[199]);
}


template <typename T> T dlog_W164_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[6];
z[0] = -k.v[4] * k.v[5];
z[1] = -k.v[2] + k.v[5];
z[2] = k.v[1] * z[1];
z[3] = 2 * k.v[1] + k.v[2] + -k.v[4];
z[4] = -k.v[3] * z[3];
z[0] = z[0] + z[2] + z[4];
z[2] = -dv[3] * z[3];
z[3] = k.v[1] + -k.v[4];
z[3] = dv[5] * z[3];
z[4] = -k.v[1] + -k.v[3];
z[4] = dv[2] * z[4];
z[5] = k.v[3] + -k.v[5];
z[5] = dv[4] * z[5];
z[1] = -2 * k.v[3] + z[1];
z[1] = dv[1] * z[1];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
a = {z[0], z[1]};
}


    T s1 = (k.W[198]*k.W[198]).real();

    T ds1 = get_sqrt_derivatives(198, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W164 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W164_re(k,dv);
	return re/(k.W[198]);
}


template <typename T> T dlog_W165_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[2] + k.v[5];
z[1] = k.v[1] * z[0];
z[2] = -k.v[0] + k.v[2];
z[3] = k.v[3] * z[2];
z[4] = k.v[0] + k.v[3] + -k.v[5];
z[5] = -k.v[4] * z[4];
z[1] = z[1] + z[3] + z[5];
z[3] = -dv[0] + dv[2];
z[3] = k.v[3] * z[3];
z[5] = -dv[2] + dv[5];
z[5] = k.v[1] * z[5];
z[0] = dv[1] * z[0];
z[2] = dv[3] * z[2];
z[6] = -dv[0] + -dv[3] + dv[5];
z[6] = k.v[4] * z[6];
z[4] = -dv[4] * z[4];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T s1 = (k.W[200]*k.W[200]).real();

    T ds1 = get_sqrt_derivatives(200, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W165 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W165_re(k,dv);
	return re/(k.W[200]);
}


template <typename T> T dlog_W166_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = k.v[0] + -k.v[5];
z[1] = 2 * k.v[1];
z[2] = z[0] + -z[1];
z[3] = -k.v[2] + z[2];
z[3] = k.v[1] * z[3];
z[2] = k.v[3] + z[2];
z[4] = -k.v[4] * z[2];
z[1] = k.v[2] + z[1];
z[5] = k.v[3] * z[1];
z[3] = z[3] + z[4] + z[5];
z[4] = -dv[0] + dv[5];
z[5] = 2 * dv[1];
z[6] = z[4] + z[5];
z[6] = k.v[4] * z[6];
z[2] = -dv[4] * z[2];
z[4] = -dv[2] + -z[4];
z[4] = k.v[1] * z[4];
z[0] = -4 * k.v[1] + -k.v[2] + z[0];
z[0] = dv[1] * z[0];
z[5] = dv[2] + z[5];
z[5] = k.v[3] * z[5];
z[1] = -k.v[4] + z[1];
z[1] = dv[3] * z[1];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + z[6];
a = {z[3], z[0]};
}


    T s1 = (k.W[199]*k.W[199]).real();

    T ds1 = get_sqrt_derivatives(199, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W166 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W166_re(k,dv);
	return re/(k.W[199]);
}


template <typename T> T dlog_W167_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[8];
z[0] = k.v[2] + -k.v[5];
z[1] = k.v[1] * z[0];
z[2] = -k.v[0] + 2 * k.v[1] + k.v[5];
z[3] = -k.v[4] * z[2];
z[4] = k.v[0] + -k.v[2] + k.v[4];
z[5] = k.v[3] * z[4];
z[1] = z[1] + z[3] + z[5];
z[0] = dv[1] * z[0];
z[3] = -dv[5] * k.v[1];
z[5] = -2 * dv[1] + -dv[5];
z[5] = k.v[4] * z[5];
z[4] = dv[3] * z[4];
z[6] = k.v[1] + -k.v[3];
z[6] = dv[2] * z[6];
z[2] = k.v[3] + -z[2];
z[2] = dv[4] * z[2];
z[7] = k.v[3] + k.v[4];
z[7] = dv[0] * z[7];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
a = {z[1], z[0]};
}


    T s1 = (k.W[200]*k.W[200]).real();

    T ds1 = get_sqrt_derivatives(200, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W167 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W167_re(k,dv);
	return re/(k.W[200]);
}


template <typename T> T dlog_W168_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[6];
z[0] = k.v[0] + -k.v[1];
z[1] = 2 * k.v[2];
z[2] = z[0] + -z[1];
z[3] = -k.v[3] + z[2];
z[3] = k.v[2] * z[3];
z[2] = k.v[4] + z[2];
z[4] = -k.v[5] * z[2];
z[1] = k.v[3] + z[1];
z[5] = k.v[4] * z[1];
z[3] = z[3] + z[4] + z[5];
z[4] = -dv[0] + dv[1];
z[5] = -k.v[2] + k.v[5];
z[4] = z[4] * z[5];
z[5] = k.v[4] + k.v[5];
z[0] = -4 * k.v[2] + -k.v[3] + z[0] + 2 * z[5];
z[0] = dv[2] * z[0];
z[2] = -dv[5] * z[2];
z[1] = -k.v[5] + z[1];
z[1] = dv[4] * z[1];
z[5] = -k.v[2] + k.v[4];
z[5] = dv[3] * z[5];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5];
a = {z[3], z[0]};
}


    T s1 = (k.W[201]*k.W[201]).real();

    T ds1 = get_sqrt_derivatives(201, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W168 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W168_re(k,dv);
	return re/(k.W[201]);
}


template <typename T> T dlog_W169_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[9];
z[0] = -k.v[0] + k.v[1];
z[1] = k.v[0] + -k.v[5];
z[0] = z[0] * z[1];
z[2] = 2 * k.v[0];
z[3] = k.v[1] + -k.v[3] + 2 * k.v[5] + -z[2];
z[4] = -k.v[2] * z[3];
z[5] = k.v[3] + -k.v[5];
z[6] = -k.v[4] * z[5];
z[0] = z[0] + z[4] + z[6];
z[4] = -dv[4] * z[5];
z[5] = dv[5] * k.v[0];
z[2] = k.v[5] + -z[2];
z[2] = dv[0] * z[2];
z[6] = dv[0] + -dv[5];
z[7] = k.v[1] * z[6];
z[3] = -dv[2] * z[3];
z[6] = dv[3] + 2 * z[6];
z[6] = k.v[2] * z[6];
z[1] = -k.v[2] + z[1];
z[1] = dv[1] * z[1];
z[8] = -dv[3] + dv[5];
z[8] = k.v[4] * z[8];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8];
a = {z[0], z[1]};
}


    T s1 = (k.W[202]*k.W[202]).real();

    T ds1 = get_sqrt_derivatives(202, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W169 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W169_re(k,dv);
	return re/(k.W[202]);
}


template <typename T> T dlog_W170_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = k.v[0] + -k.v[1];
z[1] = k.v[4] + z[0];
z[2] = -k.v[5] * z[1];
z[3] = k.v[2] * z[0];
z[4] = -k.v[2] + k.v[4] + 2 * z[0];
z[5] = k.v[3] * z[4];
z[2] = z[2] + z[3] + z[5];
z[3] = k.v[2] + -k.v[5];
z[5] = dv[0] + -dv[1];
z[3] = z[3] * z[5];
z[0] = dv[2] * z[0];
z[4] = dv[3] * z[4];
z[5] = -dv[2] + 2 * z[5];
z[5] = k.v[3] * z[5];
z[1] = -dv[5] * z[1];
z[6] = k.v[3] + -k.v[5];
z[6] = dv[4] * z[6];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6];
a = {z[2], z[0]};
}


    T s1 = (k.W[201]*k.W[201]).real();

    T ds1 = get_sqrt_derivatives(201, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W170 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W170_re(k,dv);
	return re/(k.W[201]);
}


template <typename T> T dlog_W171_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[2] + k.v[5];
z[1] = -k.v[1] * z[0];
z[2] = -k.v[0] + k.v[2];
z[3] = -k.v[3] * z[2];
z[4] = -k.v[0] + k.v[3] + k.v[5];
z[5] = k.v[4] * z[4];
z[1] = z[1] + z[3] + z[5];
z[3] = dv[0] + -dv[2];
z[3] = k.v[3] * z[3];
z[5] = dv[2] + -dv[5];
z[5] = k.v[1] * z[5];
z[0] = -dv[1] * z[0];
z[2] = -dv[3] * z[2];
z[6] = -dv[0] + dv[3] + dv[5];
z[6] = k.v[4] * z[6];
z[4] = dv[4] * z[4];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T s1 = (k.W[200]*k.W[200]).real();

    T ds1 = get_sqrt_derivatives(200, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W171 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W171_re(k,dv);
	return re/(k.W[200]);
}


template <typename T> T dlog_W172_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[9];
z[0] = k.v[3] + k.v[4];
z[0] = 3 * k.v[0] + -k.v[2] + -k.v[5] + 2 * z[0];
z[1] = 2 * k.v[1];
z[2] = z[0] + -z[1];
z[2] = k.v[1] * z[2];
z[3] = -k.v[0] + k.v[5];
z[3] = k.v[0] * z[3];
z[4] = 2 * k.v[0];
z[5] = -k.v[5] + z[4];
z[6] = -k.v[4] * z[5];
z[4] = -k.v[2] + k.v[4] + z[4];
z[7] = -k.v[3] * z[4];
z[2] = z[2] + z[3] + z[6] + z[7];
z[3] = -k.v[3] + z[1] + -z[5];
z[3] = dv[4] * z[3];
z[1] = z[1] + -z[4];
z[1] = dv[3] * z[1];
z[4] = dv[5] * k.v[0];
z[5] = -dv[0] * z[5];
z[6] = 2 * dv[0];
z[7] = dv[5] + -z[6];
z[7] = k.v[4] * z[7];
z[6] = dv[2] + -z[6];
z[6] = k.v[3] * z[6];
z[8] = 3 * dv[0] + -dv[2] + -dv[5];
z[8] = k.v[1] * z[8];
z[0] = -4 * k.v[1] + z[0];
z[0] = dv[1] * z[0];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8];
a = {z[2], z[0]};
}


    T s1 = (k.W[202]*k.W[202]).real();

    T ds1 = get_sqrt_derivatives(202, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W172 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W172_re(k,dv);
	return re/(k.W[202]);
}


template <typename T> T dlog_W173_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = k.v[1] + k.v[2];
z[0] = -k.v[0] + -k.v[3] + -k.v[5] + 2 * z[0];
z[1] = 2 * k.v[4];
z[2] = z[0] + -z[1];
z[2] = k.v[4] * z[2];
z[3] = k.v[0] + -k.v[2];
z[4] = -k.v[3] * z[3];
z[5] = -k.v[2] + k.v[5];
z[6] = k.v[1] * z[5];
z[2] = z[2] + z[4] + z[6];
z[4] = z[1] + z[5];
z[4] = dv[1] * z[4];
z[3] = -k.v[4] + -z[3];
z[3] = dv[3] * z[3];
z[1] = -k.v[1] + k.v[3] + z[1];
z[1] = dv[2] * z[1];
z[5] = k.v[1] + -k.v[4];
z[5] = dv[5] * z[5];
z[6] = -k.v[3] + -k.v[4];
z[6] = dv[0] * z[6];
z[0] = -4 * k.v[4] + z[0];
z[0] = dv[4] * z[0];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6];
a = {z[2], z[0]};
}


    T s1 = (k.W[200]*k.W[200]).real();

    T ds1 = get_sqrt_derivatives(200, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W173 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W173_re(k,dv);
	return re/(k.W[200]);
}


template <typename T> T dlog_W174_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[8];
z[0] = k.v[4] + k.v[5];
z[0] = -k.v[0] + -k.v[1] + -k.v[3] + 2 * z[0];
z[1] = 2 * k.v[2];
z[2] = z[0] + -z[1];
z[2] = k.v[2] * z[2];
z[3] = k.v[1] + -k.v[4];
z[4] = k.v[5] * z[3];
z[5] = -k.v[0] + k.v[4];
z[6] = k.v[3] * z[5];
z[2] = z[2] + z[4] + z[6];
z[3] = z[1] + z[3];
z[3] = dv[5] * z[3];
z[4] = -dv[0] * k.v[3];
z[5] = dv[3] * z[5];
z[6] = dv[1] * k.v[5];
z[7] = -dv[0] + -dv[1] + -dv[3];
z[7] = k.v[2] * z[7];
z[1] = k.v[3] + -k.v[5] + z[1];
z[1] = dv[4] * z[1];
z[0] = -4 * k.v[2] + z[0];
z[0] = dv[2] * z[0];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6] + z[7];
a = {z[2], z[0]};
}


    T s1 = (k.W[203]*k.W[203]).real();

    T ds1 = get_sqrt_derivatives(203, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W174 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W174_re(k,dv);
	return re/(k.W[203]);
}


template <typename T> T dlog_W175_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[9];
z[0] = k.v[2] + k.v[3];
z[0] = 3 * k.v[0] + -k.v[1] + -k.v[4] + 2 * z[0];
z[1] = 2 * k.v[5];
z[2] = z[0] + -z[1];
z[2] = k.v[5] * z[2];
z[3] = prod_pow(k.v[0], 2);
z[4] = 2 * k.v[0];
z[5] = -k.v[4] + z[4];
z[5] = -k.v[3] * z[5];
z[6] = k.v[3] + z[4];
z[6] = -k.v[2] * z[6];
z[7] = k.v[0] + k.v[2];
z[8] = k.v[1] * z[7];
z[2] = z[2] + -z[3] + z[5] + z[6] + z[8];
z[1] = z[1] + -z[4];
z[3] = k.v[1] + -k.v[3] + z[1];
z[3] = dv[2] * z[3];
z[1] = -k.v[2] + k.v[4] + z[1];
z[1] = dv[3] * z[1];
z[4] = -k.v[5] + z[7];
z[4] = dv[1] * z[4];
z[0] = -4 * k.v[5] + z[0];
z[0] = dv[5] * z[0];
z[5] = k.v[3] + -k.v[5];
z[5] = dv[4] * z[5];
z[6] = -k.v[3] + -z[7];
z[6] = k.v[1] + 3 * k.v[5] + 2 * z[6];
z[6] = dv[0] * z[6];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6];
a = {z[2], z[0]};
}


    T s1 = (k.W[202]*k.W[202]).real();

    T ds1 = get_sqrt_derivatives(202, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W175 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W175_re(k,dv);
	return re/(k.W[202]);
}


template <typename T> T dlog_W176_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[8];
z[0] = k.v[1] + -k.v[4];
z[1] = -k.v[5] * z[0];
z[2] = -k.v[0] + k.v[4];
z[3] = -k.v[3] * z[2];
z[4] = -k.v[0] + k.v[1] + k.v[3];
z[5] = k.v[2] * z[4];
z[1] = z[1] + z[3] + z[5];
z[2] = -dv[3] * z[2];
z[3] = -dv[1] + dv[4];
z[3] = k.v[5] * z[3];
z[5] = -dv[4] * k.v[3];
z[6] = dv[1] + dv[3];
z[6] = k.v[2] * z[6];
z[7] = -k.v[2] + k.v[3];
z[7] = dv[0] * z[7];
z[4] = dv[2] * z[4];
z[0] = -dv[5] * z[0];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
a = {z[1], z[0]};
}


    T s1 = (k.W[203]*k.W[203]).real();

    T ds1 = get_sqrt_derivatives(203, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W176 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W176_re(k,dv);
	return re/(k.W[203]);
}


template <typename T> T dlog_W177_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[0] + k.v[5];
z[1] = -k.v[4] * z[0];
z[2] = -k.v[2] + k.v[4] + 2 * z[0];
z[3] = -k.v[3] * z[2];
z[4] = -k.v[2] + z[0];
z[5] = k.v[1] * z[4];
z[1] = z[1] + z[3] + z[5];
z[3] = dv[1] * z[4];
z[0] = -dv[4] * z[0];
z[4] = -dv[0] + dv[5];
z[5] = -k.v[4] * z[4];
z[2] = -dv[3] * z[2];
z[6] = dv[2] + -dv[4] + -2 * z[4];
z[6] = k.v[3] * z[6];
z[4] = -dv[2] + z[4];
z[4] = k.v[1] * z[4];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T s1 = (k.W[199]*k.W[199]).real();

    T ds1 = get_sqrt_derivatives(199, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W177 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W177_re(k,dv);
	return re/(k.W[199]);
}


template <typename T> T dlog_W178_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[0] + k.v[5];
z[1] = k.v[0] + -k.v[1];
z[0] = z[0] * z[1];
z[2] = k.v[3] + -k.v[5] + 2 * z[1];
z[3] = k.v[4] * z[2];
z[4] = -k.v[1] + k.v[3];
z[5] = -k.v[2] * z[4];
z[0] = z[0] + z[3] + z[5];
z[2] = dv[4] * z[2];
z[1] = -k.v[4] + z[1];
z[1] = dv[5] * z[1];
z[3] = -k.v[2] + k.v[4];
z[3] = dv[3] * z[3];
z[5] = 2 * k.v[4] + k.v[5];
z[6] = k.v[0] + k.v[2] + -z[5];
z[6] = dv[1] * z[6];
z[5] = -2 * k.v[0] + k.v[1] + z[5];
z[5] = dv[0] * z[5];
z[4] = -dv[2] * z[4];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[0], z[1]};
}


    T s1 = (k.W[202]*k.W[202]).real();

    T ds1 = get_sqrt_derivatives(202, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W178 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W178_re(k,dv);
	return re/(k.W[202]);
}


template <typename T> T dlog_W179_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[6];
z[0] = k.v[0] + -k.v[5];
z[1] = 2 * k.v[4];
z[2] = z[0] + -z[1];
z[3] = -k.v[3] + z[2];
z[3] = k.v[4] * z[3];
z[2] = k.v[2] + z[2];
z[4] = -k.v[1] * z[2];
z[1] = k.v[3] + z[1];
z[5] = k.v[2] * z[1];
z[3] = z[3] + z[4] + z[5];
z[4] = -dv[0] + dv[5];
z[5] = k.v[1] + -k.v[4];
z[4] = z[4] * z[5];
z[5] = k.v[1] + k.v[2];
z[0] = -k.v[3] + -4 * k.v[4] + z[0] + 2 * z[5];
z[0] = dv[4] * z[0];
z[2] = -dv[1] * z[2];
z[1] = -k.v[1] + z[1];
z[1] = dv[2] * z[1];
z[5] = k.v[2] + -k.v[4];
z[5] = dv[3] * z[5];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5];
a = {z[3], z[0]};
}


    T s1 = (k.W[199]*k.W[199]).real();

    T ds1 = get_sqrt_derivatives(199, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W179 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W179_re(k,dv);
	return re/(k.W[199]);
}


template <typename T> T dlog_W180_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = k.v[0] + -k.v[1];
z[1] = k.v[2] * z[0];
z[2] = k.v[1] + 2 * k.v[2] + -k.v[4];
z[3] = -k.v[5] * z[2];
z[4] = k.v[0] + k.v[2] + -k.v[4];
z[5] = k.v[3] * z[4];
z[1] = z[1] + z[3] + z[5];
z[0] = dv[2] * z[0];
z[3] = dv[0] + -dv[1];
z[3] = k.v[2] * z[3];
z[5] = -dv[1] + -2 * dv[2] + dv[4];
z[5] = k.v[5] * z[5];
z[4] = dv[3] * z[4];
z[6] = dv[0] + dv[2] + -dv[4];
z[6] = k.v[3] * z[6];
z[2] = -dv[5] * z[2];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[1], z[0]};
}


    T s1 = (k.W[203]*k.W[203]).real();

    T ds1 = get_sqrt_derivatives(203, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W180 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W180_re(k,dv);
	return re/(k.W[203]);
}


template <typename T> T dlog_W181_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[6];
z[0] = k.v[0] + -k.v[1];
z[1] = 2 * k.v[5];
z[2] = z[0] + -z[1];
z[3] = -k.v[4] + z[2];
z[3] = k.v[5] * z[3];
z[2] = k.v[3] + z[2];
z[4] = -k.v[2] * z[2];
z[1] = k.v[4] + z[1];
z[5] = k.v[3] * z[1];
z[3] = z[3] + z[4] + z[5];
z[4] = -dv[0] + dv[1];
z[5] = k.v[2] + -k.v[5];
z[4] = z[4] * z[5];
z[5] = k.v[2] + k.v[3];
z[0] = -k.v[4] + -4 * k.v[5] + z[0] + 2 * z[5];
z[0] = dv[5] * z[0];
z[2] = -dv[2] * z[2];
z[1] = -k.v[2] + z[1];
z[1] = dv[3] * z[1];
z[5] = k.v[3] + -k.v[5];
z[5] = dv[4] * z[5];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5];
a = {z[3], z[0]};
}


    T s1 = (k.W[201]*k.W[201]).real();

    T ds1 = get_sqrt_derivatives(201, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W181 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W181_re(k,dv);
	return re/(k.W[201]);
}


template <typename T> T dlog_W182_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[8];
z[0] = k.v[1] + -k.v[4];
z[1] = k.v[5] * z[0];
z[2] = -k.v[0] + k.v[4];
z[3] = k.v[3] * z[2];
z[4] = k.v[0] + -k.v[1] + k.v[3];
z[5] = -k.v[2] * z[4];
z[1] = z[1] + z[3] + z[5];
z[2] = dv[3] * z[2];
z[3] = dv[1] + -dv[4];
z[3] = k.v[5] * z[3];
z[5] = dv[4] * k.v[3];
z[6] = dv[1] + -dv[3];
z[6] = k.v[2] * z[6];
z[7] = -k.v[2] + -k.v[3];
z[7] = dv[0] * z[7];
z[4] = -dv[2] * z[4];
z[0] = dv[5] * z[0];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
a = {z[1], z[0]};
}


    T s1 = (k.W[203]*k.W[203]).real();

    T ds1 = get_sqrt_derivatives(203, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W182 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W182_re(k,dv);
	return re/(k.W[203]);
}


template <typename T> T dlog_W183_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[6];
z[0] = -k.v[1] * k.v[2];
z[1] = k.v[2] + -k.v[4];
z[2] = k.v[3] * z[1];
z[3] = k.v[1] + -2 * k.v[3] + -k.v[4];
z[4] = k.v[5] * z[3];
z[0] = z[0] + z[2] + z[4];
z[2] = dv[5] * z[3];
z[3] = -k.v[3] + -k.v[5];
z[3] = dv[4] * z[3];
z[4] = -k.v[1] + k.v[3];
z[4] = dv[2] * z[4];
z[1] = -2 * k.v[5] + z[1];
z[1] = dv[3] * z[1];
z[5] = -k.v[2] + k.v[5];
z[5] = dv[1] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
a = {z[0], z[1]};
}


    T s1 = (k.W[198]*k.W[198]).real();

    T ds1 = get_sqrt_derivatives(198, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W183 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W183_re(k,dv);
	return re/(k.W[198]);
}


template <typename T> T dlog_W184_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[7];
z[0] = -k.v[3] * k.v[4];
z[1] = k.v[0] + -k.v[1];
z[2] = k.v[3] + z[1];
z[3] = k.v[2] * z[2];
z[1] = -k.v[4] + z[1];
z[4] = -k.v[5] * z[1];
z[0] = z[0] + z[3] + z[4];
z[3] = -dv[4] * k.v[3];
z[4] = -dv[3] * k.v[4];
z[1] = -dv[5] * z[1];
z[2] = dv[2] * z[2];
z[5] = -dv[0] + dv[1];
z[6] = dv[3] + -z[5];
z[6] = k.v[2] * z[6];
z[5] = dv[4] + z[5];
z[5] = k.v[5] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
a = {z[0], z[1]};
}


    T s1 = (k.W[201]*k.W[201]).real();

    T ds1 = get_sqrt_derivatives(201, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W184 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W184_re(k,dv);
	return re/(k.W[201]);
}


template <typename T> T dlog_W185_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[6];
z[0] = -k.v[2] * k.v[3];
z[1] = k.v[2] + -k.v[5];
z[2] = k.v[1] * z[1];
z[3] = k.v[3] + k.v[5];
z[4] = k.v[4] * z[3];
z[0] = z[0] + z[2] + z[4];
z[2] = k.v[1] + -k.v[3];
z[2] = dv[2] * z[2];
z[1] = dv[1] * z[1];
z[3] = dv[4] * z[3];
z[4] = -k.v[2] + k.v[4];
z[4] = dv[3] * z[4];
z[5] = -k.v[1] + k.v[4];
z[5] = dv[5] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
a = {z[0], z[1]};
}


    T s1 = (k.W[198]*k.W[198]).real();

    T ds1 = get_sqrt_derivatives(198, k.v, dv);

    return get_dlog_sqrt_1(a[0], s1, a[1], ds1);
}
template <typename T> std::complex<T> dlog_W185 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W185_re(k,dv);
	return re/(k.W[198]);
}


template <typename T> T dlog_W186_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[11];
z[0] = -k.v[0] + k.v[2];
z[1] = -k.v[5] + z[0];
z[1] = k.v[2] * z[1];
z[2] = k.v[2] + k.v[5];
z[3] = k.v[4] * z[2];
z[4] = k.v[0] * k.v[5];
z[1] = -z[1] + z[3] + -z[4];
z[3] = k.v[1] * z[1];
z[5] = k.v[0] + k.v[2];
z[6] = -k.v[4] + 2 * z[5];
z[6] = k.v[4] * z[6];
z[7] = 2 * k.v[0];
z[8] = -k.v[2] + z[7];
z[8] = k.v[2] * z[8];
z[9] = prod_pow(k.v[0], 2);
z[6] = z[6] + z[8] + -z[9];
z[8] = -k.v[3] * z[6];
z[7] = -k.v[5] + z[7];
z[9] = k.v[2] * z[7];
z[4] = -z[4] + z[9];
z[9] = k.v[4] * k.v[5];
z[10] = -z[4] + -z[9];
z[10] = k.v[4] * z[10];
z[3] = z[3] + z[8] + z[10];
z[1] = dv[1] * z[1];
z[2] = k.v[1] * z[2];
z[2] = z[2] + -z[4] + -2 * z[9];
z[2] = dv[4] * z[2];
z[4] = -dv[0] + dv[2];
z[4] = z[0] * z[4];
z[8] = -dv[0] + -dv[2];
z[8] = k.v[4] * z[8];
z[5] = -k.v[4] + z[5];
z[9] = -dv[4] * z[5];
z[4] = z[4] + z[8] + z[9];
z[4] = k.v[3] * z[4];
z[6] = -dv[3] * z[6];
z[7] = -dv[2] * z[7];
z[8] = 2 * k.v[2] + -k.v[5];
z[9] = -dv[0] * z[8];
z[7] = z[7] + z[9];
z[7] = k.v[4] * z[7];
z[8] = k.v[0] + k.v[4] + -z[8];
z[8] = dv[2] * z[8];
z[9] = k.v[2] + -k.v[5];
z[9] = dv[0] * z[9];
z[8] = z[8] + z[9];
z[8] = k.v[1] * z[8];
z[5] = k.v[4] * z[5];
z[0] = k.v[4] + z[0];
z[0] = k.v[1] * z[0];
z[0] = z[0] + z[5];
z[0] = dv[5] * z[0];
z[0] = z[0] + z[1] + z[2] + 2 * z[4] + z[6] + z[7] + z[8];
a = {z[3], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();
    T s2 = (k.W[194]*k.W[194]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);
    T ds2 = get_sqrt_derivatives(194, k.v, dv);

    return get_dlog_sqrt_2(a[0], s1, s2, a[1], ds1, ds2);
}
template <typename T> T dlog_W186 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W186_re(k,dv);
	return re/((k.W[194]).real() * -(k.W[197]).imag());
}


template <typename T> T dlog_W187_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[16];
z[0] = -k.v[0] + k.v[2];
z[1] = 2 * k.v[5];
z[2] = z[0] + -z[1];
z[3] = k.v[3] * z[2];
z[0] = k.v[2] * z[0];
z[4] = -k.v[2] + k.v[3];
z[5] = k.v[4] * z[4];
z[6] = -k.v[0] + k.v[5];
z[6] = k.v[5] * z[6];
z[0] = z[0] + -z[3] + z[5] + -z[6];
z[3] = k.v[2] + -k.v[5];
z[5] = k.v[1] * z[3];
z[7] = z[0] + z[5];
z[7] = k.v[1] * z[7];
z[8] = k.v[0] * k.v[5];
z[9] = k.v[2] * z[3];
z[10] = prod_pow(k.v[0], 2);
z[8] = z[8] + -z[9] + -z[10];
z[9] = 2 * k.v[0];
z[10] = -k.v[3] * z[9];
z[10] = z[8] + z[10];
z[10] = k.v[3] * z[10];
z[9] = -k.v[5] + z[9];
z[11] = k.v[2] * z[9];
z[12] = 2 * k.v[2];
z[13] = -k.v[5] + z[12];
z[14] = k.v[3] * z[13];
z[6] = z[6] + z[11] + z[14];
z[11] = k.v[3] + -k.v[5];
z[11] = k.v[4] * z[11];
z[15] = z[6] + -z[11];
z[15] = k.v[4] * z[15];
z[7] = z[7] + z[10] + z[15];
z[2] = k.v[4] + -z[2];
z[2] = k.v[1] * z[2];
z[10] = k.v[0] * k.v[3];
z[15] = -k.v[4] + z[13];
z[15] = k.v[4] * z[15];
z[2] = z[2] + z[8] + -4 * z[10] + z[15];
z[2] = dv[3] * z[2];
z[0] = z[0] + 2 * z[5];
z[0] = dv[1] * z[0];
z[5] = 2 * k.v[3];
z[8] = z[5] + z[9];
z[9] = -k.v[3] * z[8];
z[10] = k.v[4] * z[13];
z[3] = k.v[3] + -z[3];
z[3] = k.v[1] * z[3];
z[3] = z[3] + z[9] + z[10];
z[3] = dv[0] * z[3];
z[1] = -k.v[0] + z[1];
z[5] = -k.v[1] + -z[1] + z[5];
z[5] = k.v[1] * z[5];
z[9] = k.v[0] + k.v[2];
z[9] = k.v[3] * z[9];
z[1] = -k.v[2] + -k.v[3] + k.v[4] + z[1];
z[1] = k.v[4] * z[1];
z[1] = z[1] + z[5] + z[9];
z[1] = dv[5] * z[1];
z[4] = k.v[1] * z[4];
z[4] = z[4] + z[6] + -2 * z[11];
z[4] = dv[4] * z[4];
z[5] = k.v[4] * z[8];
z[6] = -k.v[0] + k.v[1] + -k.v[3] + -k.v[4] + z[12];
z[6] = k.v[1] * z[6];
z[5] = z[5] + z[6] + -z[14];
z[5] = dv[2] * z[5];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5];
a = {z[7], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();
    T s2 = (k.W[195]*k.W[195]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);
    T ds2 = get_sqrt_derivatives(195, k.v, dv);

    return get_dlog_sqrt_2(a[0], s1, s2, a[1], ds1, ds2);
}
template <typename T> T dlog_W187 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W187_re(k,dv);
	return re/((k.W[195]).real() * -(k.W[197]).imag());
}


template <typename T> T dlog_W188_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[14];
z[0] = k.v[2] + k.v[4];
z[1] = -k.v[1] + 2 * z[0];
z[2] = 2 * k.v[3];
z[3] = z[1] + z[2];
z[4] = -k.v[0] * z[3];
z[5] = k.v[2] + -k.v[4];
z[6] = k.v[1] * z[5];
z[4] = z[4] + -z[6];
z[4] = k.v[3] * z[4];
z[7] = 2 * k.v[1];
z[8] = k.v[0] + z[5] + z[7];
z[9] = k.v[3] * z[8];
z[10] = -k.v[1] + z[0];
z[10] = k.v[1] * z[10];
z[9] = z[9] + z[10];
z[10] = k.v[1] + -k.v[4];
z[10] = k.v[5] * z[10];
z[11] = z[9] + -z[10];
z[11] = k.v[5] * z[11];
z[12] = prod_pow(k.v[1], 2);
z[13] = k.v[2] * z[12];
z[4] = z[4] + z[11] + z[13];
z[8] = k.v[5] * z[8];
z[1] = -4 * k.v[3] + -z[1];
z[1] = k.v[0] * z[1];
z[1] = z[1] + -z[6] + z[8];
z[1] = dv[3] * z[1];
z[6] = 2 * k.v[0];
z[8] = -k.v[1] + -z[6];
z[8] = k.v[3] * z[8];
z[11] = k.v[1] + k.v[3];
z[11] = k.v[5] * z[11];
z[8] = z[8] + z[11] + z[12];
z[8] = dv[2] * z[8];
z[0] = -k.v[5] + z[0] + z[2] + -z[7];
z[0] = k.v[5] * z[0];
z[2] = k.v[2] * z[7];
z[5] = k.v[0] + -z[5];
z[5] = k.v[3] * z[5];
z[0] = z[0] + z[2] + z[5];
z[0] = dv[1] * z[0];
z[2] = k.v[1] + -z[6];
z[2] = k.v[3] * z[2];
z[5] = k.v[1] + -k.v[3] + k.v[5];
z[5] = k.v[5] * z[5];
z[2] = z[2] + z[5];
z[2] = dv[4] * z[2];
z[5] = z[9] + -2 * z[10];
z[5] = dv[5] * z[5];
z[3] = k.v[5] + -z[3];
z[3] = dv[0] * k.v[3] * z[3];
z[0] = z[0] + z[1] + z[2] + z[3] + z[5] + z[8];
a = {z[4], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();
    T s2 = (k.W[196]*k.W[196]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);
    T ds2 = get_sqrt_derivatives(196, k.v, dv);

    return get_dlog_sqrt_2(a[0], s1, s2, a[1], ds1, ds2);
}
template <typename T> T dlog_W188 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W188_re(k,dv);
	return re/((k.W[196]).real() * -(k.W[197]).imag());
}


template <typename T> T dlog_W189_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[3];
z[0] = k.v[0] * k.v[3];
z[1] = dv[3] * k.v[0];
z[2] = dv[0] * k.v[3];
z[1] = z[1] + z[2];
a = {z[0], z[1]};
}


    T s1 = (k.W[197]*k.W[197]).real();
    T s2 = (k.W[198]*k.W[198]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);
    T ds2 = get_sqrt_derivatives(198, k.v, dv);

    return get_dlog_sqrt_3(a[0], s1, s2, a[1], ds1, ds2);
}
template <typename T> std::complex<T> dlog_W189 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W189_re(k,dv);
	return re/(k.W[197] * k.W[198]);
}


template <typename T> T dlog_W190_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[3];
z[0] = k.v[1] + -k.v[3] + -k.v[4];
z[1] = -k.v[0] * z[0];
z[2] = -dv[1] + dv[3] + dv[4];
z[2] = k.v[0] * z[2];
z[0] = -dv[0] * z[0];
z[0] = z[0] + z[2];
a = {z[1], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();
    T s2 = (k.W[199]*k.W[199]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);
    T ds2 = get_sqrt_derivatives(199, k.v, dv);

    return get_dlog_sqrt_3(a[0], s1, s2, a[1], ds1, ds2);
}
template <typename T> std::complex<T> dlog_W190 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W190_re(k,dv);
	return re/(k.W[197] * k.W[199]);
}


template <typename T> T dlog_W191_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[3];
z[0] = k.v[0] * k.v[4];
z[1] = dv[4] * k.v[0];
z[2] = dv[0] * k.v[4];
z[1] = z[1] + z[2];
a = {z[0], z[1]};
}


    T s1 = (k.W[197]*k.W[197]).real();
    T s2 = (k.W[200]*k.W[200]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);
    T ds2 = get_sqrt_derivatives(200, k.v, dv);

    return get_dlog_sqrt_3(a[0], s1, s2, a[1], ds1, ds2);
}
template <typename T> std::complex<T> dlog_W191 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W191_re(k,dv);
	return re/(k.W[197] * k.W[200]);
}


template <typename T> T dlog_W192_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[3];
z[0] = k.v[2] + k.v[3] + -k.v[5];
z[1] = k.v[0] * z[0];
z[2] = dv[2] + dv[3] + -dv[5];
z[2] = k.v[0] * z[2];
z[0] = dv[0] * z[0];
z[0] = z[0] + z[2];
a = {z[1], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();
    T s2 = (k.W[201]*k.W[201]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);
    T ds2 = get_sqrt_derivatives(201, k.v, dv);

    return get_dlog_sqrt_3(a[0], s1, s2, a[1], ds1, ds2);
}
template <typename T> std::complex<T> dlog_W192 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W192_re(k,dv);
	return re/(k.W[197] * k.W[201]);
}


template <typename T> T dlog_W193_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[3];
z[0] = k.v[1] + -k.v[3] + k.v[5];
z[1] = k.v[0] + -z[0];
z[1] = k.v[0] * z[1];
z[2] = -dv[1] + dv[3] + -dv[5];
z[2] = k.v[0] * z[2];
z[0] = 2 * k.v[0] + -z[0];
z[0] = dv[0] * z[0];
z[0] = z[0] + z[2];
a = {z[1], z[0]};
}


    T s1 = (k.W[197]*k.W[197]).real();
    T s2 = (k.W[202]*k.W[202]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);
    T ds2 = get_sqrt_derivatives(202, k.v, dv);

    return get_dlog_sqrt_3(a[0], s1, s2, a[1], ds1, ds2);
}
template <typename T> std::complex<T> dlog_W193 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W193_re(k,dv);
	return re/(k.W[197] * k.W[202]);
}


template <typename T> T dlog_W194_re (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
    using std::abs;

    std::array<T,2> a;

    {
T z[3];
z[0] = k.v[0] * k.v[2];
z[1] = dv[2] * k.v[0];
z[2] = dv[0] * k.v[2];
z[1] = z[1] + z[2];
a = {z[0], z[1]};
}


    T s1 = (k.W[197]*k.W[197]).real();
    T s2 = (k.W[203]*k.W[203]).real();

    T ds1 = get_sqrt_derivatives(197, k.v, dv);
    T ds2 = get_sqrt_derivatives(203, k.v, dv);

    return get_dlog_sqrt_3(a[0], s1, s2, a[1], ds1, ds2);
}
template <typename T> std::complex<T> dlog_W194 (const Kin<T,KinType::m1>& k, const std::array<T,6>& dv) {
	auto re = dlog_W194_re(k,dv);
	return re/(k.W[197] * k.W[203]);
}

template double dlog_W1 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W1 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W1 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W2 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W2 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W2 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W3 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W3 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W3 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W4 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W4 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W4 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W5 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W5 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W5 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W6 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W6 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W6 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W7 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W7 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W7 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W8 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W8 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W8 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W9 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W9 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W9 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W10 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W10 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W10 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W11 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W11 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W11 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W12 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W12 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W12 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W13 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W13 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W13 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W14 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W14 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W14 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W15 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W15 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W15 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W16 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W16 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W16 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W17 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W17 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W17 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W18 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W18 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W18 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W19 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W19 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W19 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W20 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W20 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W20 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W21 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W21 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W21 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W22 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W22 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W22 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W23 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W23 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W23 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W24 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W24 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W24 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W25 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W25 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W25 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W26 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W26 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W26 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W27 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W27 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W27 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W28 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W28 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W28 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W29 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W29 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W29 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W30 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W30 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W30 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W31 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W31 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W31 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W32 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W32 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W32 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W33 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W33 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W33 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W34 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W34 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W34 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W35 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W35 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W35 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W36 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W36 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W36 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W37 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W37 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W37 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W38 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W38 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W38 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W39 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W39 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W39 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W40 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W40 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W40 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W41 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W41 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W41 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W42 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W42 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W42 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W43 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W43 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W43 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W44 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W44 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W44 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W45 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W45 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W45 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W46 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W46 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W46 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W47 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W47 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W47 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W48 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W48 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W48 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W49 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W49 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W49 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W50 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W50 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W50 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W51 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W51 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W51 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W52 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W52 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W52 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W53 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W53 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W53 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W54 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W54 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W54 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W55 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W55 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W55 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W56 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W56 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W56 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W57 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W57 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W57 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W58 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W58 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W58 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W59 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W59 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W59 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W60 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W60 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W60 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W61 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W61 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W61 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W62 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W62 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W62 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W63 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W63 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W63 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W64 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W64 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W64 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W65 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W65 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W65 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W66 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W66 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W66 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W67 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W67 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W67 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W68 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W68 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W68 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W69 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W69 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W69 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W70 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W70 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W70 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W71 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W71 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W71 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W72 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W72 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W72 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W73 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W73 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W73 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W74 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W74 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W74 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W75 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W75 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W75 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W76 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W76 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W76 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W77 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W77 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W77 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W78 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W78 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W78 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W79 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W79 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W79 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W80 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W80 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W80 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W81 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W81 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W81 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W82 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W82 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W82 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W83 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W83 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W83 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W84 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W84 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W84 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W85 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W85 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W85 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W86 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W86 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W86 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W87 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W87 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W87 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W88 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W88 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W88 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W89 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W89 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W89 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W90 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W90 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W90 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W91 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W91 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W91 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W92 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W92 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W92 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W93 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W93 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W93 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W94 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W94 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W94 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W95 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W95 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W95 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W96 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W96 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W96 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W97 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W97 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W97 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W98 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W98 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W98 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W99 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W99 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W99 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W100 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W100 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W100 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W101 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W101 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W101 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W102 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W102 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W102 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W103 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W103 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W103 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W104 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W104 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W104 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W105 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W105 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W105 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W106 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W106 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W106 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W107 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W107 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W107 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W108 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W108 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W108 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W109 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W109 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W109 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W110 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W110 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W110 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W111 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W111 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W111 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W112 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W112 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W112 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W113 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W113 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W113 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W114 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W114 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W114 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W115 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W115 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W115 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W116 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W116 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W116 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W117 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dl);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W117 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dl);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W117 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dl);
#endif
template double dlog_W118 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W118 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W118 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W119 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W119 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W119 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W120 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W120 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W120 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W121 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W121 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W121 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W122 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W122 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W122 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W123 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W123 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W123 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W124 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W124 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W124 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W125 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W125 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W125 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W126 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W126 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W126 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W127 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W127 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W127 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W128 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W128 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W128 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W129 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W129 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W129 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W130 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W130 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W130 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W131 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W131 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W131 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W132 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W132 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W132 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W133 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W133 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W133 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W134 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W134 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W134 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W135 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W135 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W135 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W136 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W136 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W136 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W137 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W137 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W137 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W138 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W138 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W138 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W139 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W139 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W139 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W140 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W140 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W140 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W141 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W141 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W141 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W142 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W142 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W142 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W143 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W143 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W143 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W144 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W144 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W144 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W145 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W145 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W145 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W146 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W146 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W146 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W147 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W147 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W147 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W148 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W148 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W148 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W149 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W149 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W149 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W150 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W150 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W150 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W151 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W151 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W151 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W152 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W152 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W152 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W153 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W153 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W153 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W154 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W154 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W154 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W155 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W155 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W155 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W156 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W156 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W156 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W157 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W157 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W157 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W158 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W158 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W158 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W159 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W159 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W159 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W160 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W160 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W160 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W161 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W161 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W161 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W162 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W162 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W162 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W163 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W163 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W163 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W164 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W164 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W164 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W165 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W165 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W165 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W166 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W166 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W166 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W167 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W167 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W167 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W168 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W168 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W168 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W169 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W169 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W169 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W170 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W170 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W170 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W171 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W171 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W171 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W172 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W172 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W172 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W173 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W173 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W173 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W174 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W174 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W174 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W175 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W175 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W175 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W176 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W176 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W176 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W177 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W177 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W177 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W178 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W178 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W178 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W179 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W179 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W179 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W180 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W180 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W180 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W181 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W181 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W181 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W182 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W182 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W182 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W183 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W183 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W183 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W184 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W184 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W184 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W185 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W185 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W185 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W186 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W186 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W186 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W187 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W187 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W187 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template double dlog_W188 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template dd_real dlog_W188 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template qd_real dlog_W188 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W189 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W189 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W189 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W190 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W190 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W190 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W191 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W191 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W191 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W192 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W192 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W192 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W193 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W193 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W193 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif
template std::complex<double> dlog_W194 (const Kin<double,KinType::m1>& k, const std::array<double,6>& dv);
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<dd_real> dlog_W194 (const Kin<dd_real,KinType::m1>& k, const std::array<dd_real,6>& dv);
#endif
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template std::complex<qd_real> dlog_W194 (const Kin<qd_real,KinType::m1>& k, const std::array<qd_real,6>& dv);
#endif

} // namespace m1_set
} // namespace PentagonFunctions
