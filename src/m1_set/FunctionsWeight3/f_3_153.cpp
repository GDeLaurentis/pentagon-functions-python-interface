/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_153.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_153_abbreviated (const std::array<T,21>& abb) {
T z[24];
z[0] = abb[13] * abb[14];
z[1] = prod_pow(abb[14], 2);
z[0] = z[0] + -z[1];
z[2] = 2 * z[0];
z[3] = -abb[11] + abb[15];
z[4] = abb[13] + -abb[14];
z[5] = z[3] + z[4];
z[6] = 2 * abb[15];
z[7] = -z[5] * z[6];
z[8] = 4 * abb[16];
z[9] = 2 * abb[17];
z[10] = 2 * abb[20];
z[11] = -abb[11] + 2 * abb[13];
z[11] = abb[11] * z[11];
z[12] = 2 * z[4];
z[13] = -abb[12] + -z[12];
z[13] = abb[12] * z[13];
z[7] = -4 * abb[18] + z[2] + z[7] + -z[8] + -z[9] + z[10] + z[11] + z[13];
z[7] = abb[3] * z[7];
z[11] = -abb[11] + z[12];
z[11] = abb[11] * z[11];
z[13] = 3 * abb[19];
z[14] = 2 * abb[18];
z[15] = prod_pow(abb[13], 2);
z[16] = -abb[11] + z[4];
z[17] = 2 * z[16];
z[18] = 3 * abb[15] + z[17];
z[18] = abb[15] * z[18];
z[19] = prod_pow(abb[12], 2);
z[15] = abb[16] + -abb[17] + abb[20] + -z[1] + -z[11] + z[13] + -z[14] + z[15] + z[18] + -z[19];
z[15] = abb[4] * z[15];
z[18] = -abb[12] + z[4];
z[19] = 2 * abb[11];
z[20] = z[18] + z[19];
z[21] = abb[12] * z[20];
z[21] = abb[16] + z[21];
z[22] = 3 * abb[20];
z[23] = abb[11] + z[12];
z[23] = abb[11] * z[23];
z[17] = -abb[15] + -z[17];
z[17] = abb[15] * z[17];
z[2] = 4 * abb[17] + z[2] + -z[14] + z[17] + -2 * z[21] + -z[22] + z[23];
z[2] = abb[9] * z[2];
z[5] = abb[15] * z[5];
z[5] = 3 * abb[16] + 4 * z[5];
z[17] = abb[11] * z[4];
z[17] = -3 * abb[18] + z[5] + -4 * z[17] + z[22];
z[17] = abb[6] * z[17];
z[21] = abb[11] * abb[14];
z[4] = abb[11] + z[4];
z[4] = abb[12] * z[4];
z[4] = -z[0] + z[4] + -z[21];
z[23] = -abb[16] + abb[20];
z[4] = 11 * abb[18] + 4 * z[4] + -5 * z[23];
z[4] = abb[8] * z[4];
z[23] = -abb[13] + 2 * abb[14];
z[23] = abb[13] * z[23];
z[1] = -z[1] + z[23];
z[5] = z[1] + -z[5] + z[11];
z[5] = abb[2] * z[5];
z[22] = 3 * abb[17] + -z[22];
z[23] = -abb[12] + z[12];
z[23] = abb[12] * z[23];
z[11] = -z[11] + -z[22] + z[23];
z[11] = abb[5] * z[11];
z[23] = abb[15] * z[16];
z[0] = -z[0] + z[21] + z[23];
z[0] = 2 * z[0] + -z[13] + -z[22];
z[0] = abb[7] * z[0];
z[9] = -abb[20] + z[9];
z[9] = abb[10] * z[9];
z[13] = abb[2] + -abb[9];
z[13] = -abb[3] + abb[10] + -2 * z[13];
z[13] = -abb[0] + -3 * abb[5] + 2 * z[13];
z[13] = abb[19] * z[13];
z[0] = z[0] + z[2] + z[4] + z[5] + z[7] + z[9] + z[11] + z[13] + z[15] + z[17];
z[1] = 2 * z[1];
z[2] = -abb[11] + 4 * abb[13];
z[2] = abb[11] * z[2];
z[4] = prod_pow(abb[15], 2);
z[5] = 5 * abb[12];
z[7] = 6 * abb[11] + -z[5];
z[7] = abb[12] * z[7];
z[2] = -10 * abb[17] + -z[1] + z[2] + -2 * z[4] + z[7] + -z[8] + z[14];
z[2] = abb[0] * z[2];
z[4] = -3 * abb[11] + -z[12];
z[4] = 2 * z[4] + z[5];
z[4] = abb[12] * z[4];
z[5] = prod_pow(abb[11], 2);
z[1] = 8 * abb[17] + -6 * abb[18] + z[1] + z[4] + -z[5] + z[10];
z[1] = abb[1] * z[1];
z[0] = 2 * z[0] + z[1] + z[2];
z[1] = -abb[0] + abb[1] + abb[2] + -abb[4];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[0] = 2 * z[0] + (T(13) / T(3)) * z[1];
z[0] = 4 * z[0];
z[1] = abb[6] * z[3];
z[2] = abb[12] + -abb[14];
z[2] = -abb[8] * z[2];
z[1] = z[1] + -z[2];
z[2] = z[6] + z[16];
z[2] = abb[2] * z[2];
z[3] = abb[1] * z[20];
z[4] = abb[12] + abb[15];
z[5] = -abb[13] + z[4] + -z[19];
z[5] = abb[0] * z[5];
z[6] = -z[6] + -z[18];
z[6] = abb[4] * z[6];
z[4] = -abb[11] + -abb[14] + z[4];
z[4] = abb[3] * z[4];
z[7] = -abb[11] + abb[12];
z[7] = abb[5] * z[7];
z[8] = -abb[14] + abb[15];
z[8] = abb[7] * z[8];
z[1] = -2 * z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8];
z[1] = 32 * m1_set::bc<T>[0] * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_153_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_153_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_153_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("5.047076865273833317856577548568822397288647406593707418399830384"),stof<T>("-46.597393294026146015217746397500806486883094290685608813499393055")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,21> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), dlog_W58(k,dl), f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k)};

                    
            return f_3_153_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_153_DLogXconstant_part(base_point<T>, kend);
	value += f_3_153_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_153_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_153_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_153_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_153_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_153_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_153_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
