/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_185.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_185_abbreviated (const std::array<T,48>& abb) {
T z[43];
z[0] = 3 * abb[1];
z[1] = abb[6] + abb[8];
z[2] = -abb[5] + z[1];
z[3] = abb[3] + abb[11];
z[4] = abb[4] + -abb[13];
z[5] = z[0] + -z[2] + z[3] + z[4];
z[5] = abb[24] * z[5];
z[6] = 4 * abb[12];
z[7] = 2 * abb[7];
z[8] = 3 * abb[5];
z[0] = abb[6] + -abb[8] + z[0] + -z[6] + -z[7] + z[8];
z[0] = abb[25] * z[0];
z[9] = 2 * abb[3];
z[10] = abb[0] + -abb[4];
z[11] = z[9] + -z[10];
z[12] = -abb[2] + z[11];
z[13] = 2 * abb[12];
z[14] = -abb[5] + z[13];
z[15] = 2 * abb[1];
z[16] = z[12] + -z[14] + z[15];
z[16] = abb[19] * z[16];
z[17] = 4 * abb[1];
z[18] = z[4] + z[17];
z[19] = -abb[3] + z[13];
z[20] = -z[8] + -z[18] + z[19];
z[20] = abb[18] * z[20];
z[12] = abb[5] + -z[12];
z[12] = abb[21] * z[12];
z[21] = abb[18] + -abb[21];
z[21] = z[7] * z[21];
z[22] = -abb[1] + abb[8];
z[23] = -abb[6] + z[14] + z[22];
z[24] = abb[22] * z[23];
z[11] = abb[1] + abb[7] + -abb[12] + z[11];
z[11] = abb[23] * z[11];
z[25] = -abb[1] + -abb[11] + z[1];
z[25] = abb[20] * z[25];
z[0] = z[0] + z[5] + z[11] + z[12] + z[16] + z[20] + z[21] + z[24] + z[25];
z[5] = 2 * abb[23];
z[0] = z[0] * z[5];
z[11] = -abb[1] + abb[10];
z[12] = z[2] + -z[11];
z[12] = abb[19] * z[12];
z[16] = -abb[25] * z[23];
z[20] = abb[10] + -z[15];
z[20] = abb[18] * z[20];
z[2] = abb[1] + -z[2];
z[2] = abb[20] * z[2];
z[11] = -abb[22] * z[11];
z[2] = z[2] + z[11] + z[12] + z[16] + z[20];
z[2] = abb[22] * z[2];
z[11] = abb[3] + z[10];
z[12] = -z[8] + z[11];
z[16] = 2 * abb[6];
z[20] = abb[1] + abb[8] + -z[12] + -z[16];
z[21] = -abb[19] + abb[20];
z[20] = z[20] * z[21];
z[21] = 2 * abb[5];
z[23] = -abb[11] + z[21];
z[18] = z[18] + z[23];
z[18] = abb[18] * z[18];
z[24] = z[15] + z[21];
z[25] = abb[11] + z[11] + -z[24];
z[25] = abb[24] * z[25];
z[26] = abb[3] * abb[21];
z[18] = z[18] + z[20] + z[25] + -z[26];
z[18] = abb[24] * z[18];
z[20] = abb[0] + abb[10];
z[25] = 2 * abb[8];
z[27] = -z[17] + z[20] + -z[25];
z[28] = abb[42] * z[27];
z[12] = z[12] + -z[25];
z[29] = abb[6] + abb[11] + z[12];
z[30] = abb[41] * z[29];
z[31] = abb[5] + -abb[12];
z[32] = 2 * z[31];
z[33] = -abb[39] * z[32];
z[34] = abb[5] + z[10];
z[35] = -abb[2] + 3 * abb[3] + -z[34];
z[35] = abb[27] * z[35];
z[36] = abb[2] + -abb[3];
z[37] = abb[28] * z[36];
z[38] = abb[15] + abb[17];
z[39] = abb[45] * z[38];
z[2] = -z[2] + -z[18] + -z[28] + z[30] + -z[33] + -z[35] + z[37] + -z[39];
z[14] = -5 * abb[1] + -z[1] + z[14] + z[20];
z[14] = abb[19] * z[14];
z[1] = -7 * abb[1] + z[1] + -2 * z[4] + -z[8];
z[1] = abb[24] * z[1];
z[8] = -abb[12] + z[4] + z[21];
z[8] = 12 * abb[1] + 2 * z[8] + -z[20];
z[8] = abb[18] * z[8];
z[18] = -abb[18] + abb[24];
z[7] = z[7] * z[18];
z[1] = z[1] + z[7] + z[8] + z[14];
z[7] = z[16] + -z[25];
z[8] = 4 * abb[5];
z[14] = -abb[4] + abb[10] + 5 * abb[12] + -z[8];
z[14] = abb[0] + -16 * abb[1] + 7 * abb[7] + -z[7] + 2 * z[14];
z[14] = abb[25] * z[14];
z[1] = 2 * z[1] + z[14];
z[1] = abb[25] * z[1];
z[14] = abb[18] * abb[24];
z[14] = -abb[27] + z[14];
z[18] = 4 * abb[29];
z[20] = prod_pow(abb[18], 2);
z[28] = prod_pow(abb[21], 2);
z[14] = -abb[26] + -4 * z[14] + z[18] + -3 * z[20] + 2 * z[28];
z[14] = abb[7] * z[14];
z[20] = 2 * abb[13];
z[28] = 6 * abb[1];
z[30] = abb[3] + abb[4];
z[33] = 2 * abb[0] + -abb[5] + z[20] + -z[28] + -z[30];
z[33] = abb[18] * z[33];
z[30] = abb[5] + z[15] + -z[30];
z[30] = abb[19] * z[30];
z[30] = 2 * z[30] + z[33];
z[30] = abb[18] * z[30];
z[22] = -z[16] + z[21] + z[22];
z[22] = abb[19] * z[22];
z[33] = -abb[5] + abb[11];
z[33] = abb[18] * z[33];
z[22] = z[22] + z[33];
z[33] = 4 * abb[6];
z[12] = -2 * abb[11] + z[12] + z[33];
z[12] = abb[20] * z[12];
z[12] = z[12] + 2 * z[22];
z[12] = abb[20] * z[12];
z[4] = z[4] + z[13] + z[15];
z[22] = -abb[6] + z[4];
z[35] = abb[43] * z[22];
z[37] = -abb[1] + -z[31];
z[18] = z[18] * z[37];
z[36] = abb[19] * z[36];
z[37] = abb[3] * abb[18];
z[36] = z[36] + z[37];
z[34] = -2 * abb[2] + abb[3] + -z[34];
z[34] = abb[21] * z[34];
z[34] = z[34] + 2 * z[36];
z[34] = abb[21] * z[34];
z[20] = -abb[4] + z[20] + -z[21];
z[20] = 3 * abb[0] + -18 * abb[1] + 2 * z[20];
z[20] = abb[26] * z[20];
z[36] = -abb[22] + 2 * abb[25];
z[37] = abb[18] + -z[36];
z[39] = abb[18] + -abb[19];
z[37] = z[37] * z[39];
z[39] = abb[26] + abb[42];
z[37] = z[37] + 2 * z[39];
z[37] = abb[14] * z[37];
z[39] = -abb[0] + abb[3] + 3 * abb[4] + -abb[5];
z[39] = prod_pow(abb[19], 2) * z[39];
z[40] = -abb[5] + abb[6];
z[41] = abb[40] * z[40];
z[42] = abb[16] * abb[44];
z[0] = z[0] + z[1] + -2 * z[2] + z[12] + z[14] + z[18] + z[20] + z[30] + z[34] + -4 * z[35] + z[37] + z[39] + -6 * z[41] + z[42];
z[1] = -abb[10] + abb[11];
z[2] = abb[2] + z[1];
z[12] = abb[4] + -16 * abb[12] + 4 * abb[13];
z[12] = 6 * abb[5] + (T(1) / T(3)) * z[12];
z[2] = abb[0] * (T(-13) / T(3)) + abb[6] * (T(-10) / T(3)) + abb[14] * (T(4) / T(3)) + abb[1] * (T(7) / T(3)) + (T(2) / T(3)) * z[2] + -z[9] + 2 * z[12];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[0] = abb[46] + 8 * abb[47] + 4 * z[0] + 2 * z[2];
z[2] = 5 * abb[7];
z[1] = -abb[0] + abb[3] + -abb[4] + -z[1] + z[2] + z[13] + -z[21] + -z[28];
z[1] = abb[18] * z[1];
z[9] = 3 * abb[6] + -abb[10] + -z[10] + z[17] + -z[19] + -z[21];
z[9] = abb[19] * z[9];
z[8] = abb[11] + z[8] + -z[11] + z[25] + -z[33];
z[8] = abb[20] * z[8];
z[10] = abb[0] + abb[3] + abb[6] + -abb[13] + z[15] + -z[23] + -z[25];
z[10] = abb[24] * z[10];
z[6] = abb[13] + -z[6] + z[24];
z[2] = -abb[10] + -z[2] + 2 * z[6] + z[7];
z[2] = abb[25] * z[2];
z[6] = -abb[10] + z[25] + -z[32];
z[6] = abb[22] * z[6];
z[1] = z[1] + z[2] + z[6] + z[8] + z[9] + z[10] + -z[26];
z[2] = z[3] + z[4] + -z[16];
z[2] = z[2] * z[5];
z[3] = -abb[19] + z[36];
z[3] = abb[14] * z[3];
z[1] = 2 * z[1] + z[2] + z[3];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[16] * abb[35];
z[1] = z[1] + z[2];
z[2] = -abb[34] * z[22];
z[3] = -abb[30] * z[31];
z[2] = z[2] + z[3];
z[3] = abb[14] + z[27];
z[3] = abb[33] * z[3];
z[4] = abb[32] * z[29];
z[5] = abb[36] * z[38];
z[2] = abb[38] + 2 * z[2] + z[3] + -z[4] + z[5];
z[3] = abb[31] * z[40];
z[1] = abb[37] + 4 * z[1] + 8 * z[2] + -24 * z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_185_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {stof<T>("-315.82734083485947580270371199603683633003838103170530004522718004")};
	
	std::vector<C> intdlogs = {(log2(k.W[172]) - log2(kbase.W[172]))};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_185_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_3_185_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(2)) * (v[2] + v[3]) * (24 + 8 * v[0] + 4 * v[1] + -5 * v[2] + -9 * v[3] + -4 * v[4] + 6 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 12 * v[5])) / prod_pow(tend, 2);
c[1] = (-4 * (-1 + 3 * m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[2];
z[0] = abb[18] + -abb[24];
z[1] = -abb[23] + z[0];
z[1] = abb[23] * z[1];
z[0] = abb[21] + -z[0];
z[0] = abb[21] * z[0];
z[0] = -abb[27] + z[0] + z[1];
return 8 * abb[9] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_185_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (-4 * m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = abb[21] + -abb[23];
return 8 * abb[9] * m1_set::bc<T>[0] * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_185_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-31.737775073355003260632849371930008327564453074960916425721120264"),stof<T>("-127.230279665719475680056474229810324087015406742971023028044421874")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18, 200});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,48> abb = {dl[0], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W19(k,dl), dlog_W23(k,dl), dlog_W24(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W122(k,dv), dlog_W127(k,dv), dlog_W128(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_16(k), f_2_19(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), f_2_25_im(k), T{0}, T{0}, f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k), f_2_25_re(k), T{0}, T{0}};
abb[37] = SpDLog_f_3_185_W_19_Im(t, path, abb);
abb[46] = SpDLog_f_3_185_W_19_Re(t, path, abb);
{
auto c = dlog_W173(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[47] = c.real();
abb[38] = c.imag();
SpDLog_Sigma5<T,200,172>(k, dv, abb[47], abb[38], f_2_32_series_coefficients<T>);
}

                    
            return f_3_185_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_185_DLogXconstant_part(base_point<T>, kend);
	value += f_3_185_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_185_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_185_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_185_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_185_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_185_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_185_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
