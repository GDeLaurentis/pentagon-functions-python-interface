/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_170.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_170_abbreviated (const std::array<T,52>& abb) {
T z[63];
z[0] = 3 * abb[6];
z[1] = 4 * abb[4] + -z[0];
z[2] = 3 * abb[14];
z[3] = 2 * abb[9];
z[4] = abb[7] + z[3];
z[5] = 5 * abb[8] + -abb[15];
z[6] = 2 * abb[11];
z[7] = -4 * abb[1] + -z[1] + z[2] + -2 * z[4] + z[5] + -z[6];
z[8] = abb[44] * z[7];
z[9] = abb[1] + -abb[12];
z[10] = abb[7] + abb[15];
z[11] = -abb[8] + z[10];
z[12] = 3 * z[11];
z[13] = 3 * abb[3];
z[14] = 3 * abb[5];
z[15] = z[9] + z[12] + z[13] + -z[14];
z[16] = abb[49] * z[15];
z[17] = abb[7] + abb[9];
z[17] = 2 * z[17];
z[18] = 4 * abb[11];
z[5] = -z[5] + z[17] + z[18];
z[19] = abb[3] + abb[4];
z[20] = -z[5] + -z[19];
z[20] = abb[46] * z[20];
z[21] = 2 * abb[0];
z[22] = 3 * abb[13];
z[23] = z[21] + -z[22];
z[24] = 4 * abb[10];
z[25] = z[14] + z[24];
z[26] = 3 * abb[4];
z[27] = z[25] + z[26];
z[28] = 2 * abb[16];
z[29] = z[27] + -z[28];
z[30] = abb[7] + abb[8];
z[31] = z[23] + z[29] + -z[30];
z[32] = abb[48] * z[31];
z[23] = z[23] + z[24];
z[33] = 3 * abb[15];
z[34] = abb[14] + z[33];
z[35] = -z[30] + z[34];
z[36] = -z[0] + -z[23] + z[35];
z[36] = abb[34] * z[36];
z[37] = 2 * abb[8];
z[10] = -abb[9] + -abb[11] + -z[10] + -z[19] + z[37];
z[10] = abb[14] + z[0] + 2 * z[10];
z[38] = abb[50] * z[10];
z[39] = abb[7] + z[21];
z[39] = -z[22] + 3 * z[39];
z[39] = abb[31] * z[39];
z[40] = -abb[31] + abb[33];
z[2] = z[2] * z[40];
z[40] = abb[46] + -z[40];
z[40] = z[0] * z[40];
z[41] = 3 * abb[9];
z[42] = abb[7] + z[41];
z[42] = -abb[8] + -z[33] + 2 * z[42];
z[43] = 4 * abb[14] + -z[28];
z[44] = z[42] + -z[43];
z[45] = abb[35] * z[44];
z[12] = -abb[0] + -6 * abb[2] + abb[10] + -z[12] + z[22];
z[46] = abb[45] * z[12];
z[47] = abb[0] + abb[3];
z[48] = -abb[1] + -abb[2] + z[47];
z[49] = -prod_pow(abb[28], 2) * z[48];
z[50] = 2 * abb[1];
z[51] = -abb[3] + z[50];
z[52] = abb[5] + -abb[7] + -z[51];
z[52] = abb[33] * z[52];
z[53] = -abb[18] + -abb[20] + abb[22];
z[53] = -abb[17] + -abb[19] + abb[21] + 3 * z[53];
z[54] = abb[43] * z[53];
z[2] = z[2] + z[8] + -z[16] + z[20] + z[32] + z[36] + z[38] + z[39] + z[40] + -z[45] + z[46] + z[49] + 3 * z[52] + -z[54];
z[8] = 2 * abb[7];
z[16] = z[0] + z[8] + -z[14] + -z[26] + z[28] + -z[34] + z[37];
z[16] = abb[26] * z[16];
z[20] = 2 * abb[2];
z[32] = 4 * abb[0];
z[36] = z[20] + -z[32];
z[22] = z[22] + z[28];
z[37] = z[0] + z[50];
z[38] = 2 * abb[4];
z[11] = 4 * abb[9] + -abb[14] + z[11] + z[22] + -z[24] + z[36] + -z[37] + -z[38];
z[11] = abb[23] * z[11];
z[24] = 4 * abb[15];
z[39] = -abb[3] + z[0];
z[40] = abb[4] + -abb[7];
z[23] = z[6] + z[23] + -z[24] + z[39] + -z[40];
z[23] = abb[24] * z[23];
z[45] = -abb[14] + z[3];
z[46] = abb[15] + -abb[16] + z[40] + -z[45] + z[51];
z[49] = abb[30] * z[46];
z[51] = 2 * abb[28];
z[48] = z[48] * z[51];
z[11] = z[11] + z[16] + z[23] + z[48] + 2 * z[49];
z[16] = z[0] + z[32];
z[23] = -4 * abb[2] + abb[3] + 6 * abb[13] + -z[16] + -z[25] + z[35] + z[50];
z[23] = abb[27] * z[23];
z[11] = 2 * z[11] + z[23];
z[11] = abb[27] * z[11];
z[23] = 2 * abb[10];
z[25] = -z[23] + z[28] + -z[36];
z[32] = 4 * abb[8];
z[36] = 2 * abb[3];
z[34] = z[4] + -z[25] + -z[32] + z[34] + -z[36] + z[50];
z[34] = abb[23] * z[34];
z[48] = 2 * abb[15];
z[49] = -z[40] + z[48];
z[52] = -z[23] + z[49];
z[54] = abb[0] + abb[2] + -abb[9];
z[55] = abb[1] + -z[52] + -z[54];
z[55] = abb[28] * z[55];
z[34] = z[34] + 4 * z[55];
z[34] = abb[23] * z[34];
z[55] = 2 * abb[12];
z[56] = abb[11] + -z[36] + z[55];
z[57] = abb[1] + -z[49] + z[56];
z[57] = z[51] * z[57];
z[58] = abb[23] * z[10];
z[59] = 2 * abb[14];
z[9] = -z[9] + z[17] + -z[59];
z[9] = abb[30] * z[9];
z[14] = -z[14] + z[37];
z[17] = 4 * abb[12] + -z[13];
z[60] = z[14] + z[17];
z[35] = z[35] + -z[60];
z[35] = abb[26] * z[35];
z[61] = -abb[15] + z[6];
z[19] = abb[8] + -abb[14] + z[19] + -z[61];
z[19] = abb[27] * z[19];
z[9] = z[9] + z[19] + z[35] + z[57] + z[58];
z[19] = abb[1] + 5 * abb[12];
z[35] = 6 * abb[11];
z[57] = 6 * abb[3];
z[32] = abb[7] + z[32];
z[58] = abb[14] + z[19] + -z[32] + -z[33] + z[35] + -z[57];
z[58] = abb[25] * z[58];
z[9] = 2 * z[9] + z[58];
z[9] = abb[25] * z[9];
z[58] = abb[12] + -abb[15];
z[62] = 3 * abb[7];
z[1] = 9 * abb[1] + -10 * abb[9] + z[1] + z[36] + z[43] + -7 * z[58] + -z[62];
z[1] = abb[30] * z[1];
z[46] = -abb[23] * z[46];
z[49] = abb[3] + -abb[9] + z[49] + -z[55];
z[49] = abb[28] * z[49];
z[46] = z[46] + z[49];
z[1] = z[1] + 4 * z[46];
z[1] = abb[30] * z[1];
z[33] = abb[7] + z[33];
z[33] = 12 * abb[2] + -10 * abb[10] + -z[0] + -z[21] + 3 * z[33] + -z[35];
z[33] = abb[24] * z[33];
z[35] = 3 * abb[2] + -abb[11] + -z[47] + z[52];
z[46] = -abb[23] + abb[28];
z[46] = z[35] * z[46];
z[33] = z[33] + 4 * z[46];
z[33] = abb[24] * z[33];
z[13] = -6 * abb[1] + 7 * abb[14] + z[13] + -z[26] + -z[28] + -z[42];
z[13] = abb[26] * z[13];
z[4] = z[4] + -z[48];
z[4] = -5 * abb[14] + 3 * z[4] + z[28] + z[60];
z[4] = abb[30] * z[4];
z[26] = -abb[23] * z[44];
z[4] = z[4] + z[26];
z[4] = 2 * z[4] + z[13];
z[4] = abb[26] * z[4];
z[13] = -6 * abb[9] + z[16] + -z[29] + z[32];
z[13] = abb[29] * z[13];
z[16] = abb[27] * z[31];
z[26] = -abb[5] + abb[6];
z[29] = abb[4] + -abb[8] + -z[26] + z[45];
z[31] = 3 * z[29];
z[32] = abb[26] * z[31];
z[16] = z[16] + z[32];
z[16] = z[13] + 2 * z[16];
z[16] = abb[29] * z[16];
z[29] = abb[47] * z[29];
z[32] = abb[3] + -abb[6] + abb[13] + -z[20] + z[40];
z[32] = abb[32] * z[32];
z[29] = z[29] + z[32];
z[32] = abb[0] + abb[10] + abb[14] + -abb[15];
z[32] = 2 * abb[6] + -abb[7] + -8 * abb[11] + abb[12] * (T(-16) / T(3)) + abb[4] * (T(-4) / T(3)) + abb[2] * (T(-2) / T(3)) + abb[8] * (T(22) / T(3)) + (T(1) / T(3)) * z[32] + -z[50] + z[57];
z[32] = prod_pow(m1_set::bc<T>[0], 2) * z[32];
z[1] = z[1] + 2 * z[2] + z[4] + z[9] + z[11] + z[16] + 6 * z[29] + z[32] + z[33] + z[34];
z[1] = 8 * z[1];
z[2] = z[8] + -z[23] + z[24] + -z[38] + -z[50] + z[54] + -z[56];
z[2] = z[2] * z[51];
z[4] = abb[8] + -z[40] + -z[48];
z[4] = 3 * z[4] + z[17] + 6 * z[26] + z[28] + z[50] + -z[59];
z[4] = abb[26] * z[4];
z[9] = z[38] + -z[59];
z[11] = -abb[15] + -z[3];
z[6] = -abb[3] + abb[8] + z[6] + -z[9] + 2 * z[11] + z[14] + -z[20] + z[21];
z[6] = abb[27] * z[6];
z[0] = 8 * abb[3] + 5 * abb[15] + -z[0] + z[3] + z[9] + -z[18] + -z[19] + z[62];
z[0] = abb[25] * z[0];
z[3] = 4 * abb[3] + abb[7] + z[9] + z[25] + -z[37] + z[61];
z[3] = abb[23] * z[3];
z[8] = abb[4] + abb[15] + -z[8] + -z[41];
z[8] = 5 * abb[1] + -abb[12] + 2 * z[8] + -z[36] + z[43];
z[8] = abb[30] * z[8];
z[9] = abb[24] * z[35];
z[0] = z[0] + z[2] + z[3] + z[4] + z[6] + z[8] + 2 * z[9] + -z[13];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[36] * z[7];
z[3] = z[21] + -z[22] + z[27] + -z[30];
z[3] = abb[40] * z[3];
z[4] = -abb[41] * z[15];
z[5] = -abb[4] + -z[5] + z[39];
z[5] = abb[38] * z[5];
z[6] = abb[42] * z[10];
z[7] = abb[39] * z[31];
z[8] = abb[37] * z[12];
z[9] = abb[51] * z[53];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9];
z[0] = 16 * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_3_170_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_170_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_170_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-109.30485304357035631750849465705909903802702019986781261025097151"),stof<T>("-560.14570667504861489173407788892849518419006415974927713276798773")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,52> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W83(k,dl), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_13(k), f_2_14(k), f_2_19(k), f_2_20(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_10_im(k), f_2_11_im(k), f_2_12_im(k), f_2_22_im(k), f_2_26_im(k), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_10_re(k), f_2_11_re(k), f_2_12_re(k), f_2_22_re(k), f_2_26_re(k)};

                    
            return f_3_170_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_170_DLogXconstant_part(base_point<T>, kend);
	value += f_3_170_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_170_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_170_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_170_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_170_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_170_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_170_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
