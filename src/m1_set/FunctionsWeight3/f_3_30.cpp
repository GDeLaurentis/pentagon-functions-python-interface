/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_30.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_30_abbreviated (const std::array<T,35>& abb) {
T z[37];
z[0] = abb[6] + abb[11];
z[1] = -abb[4] + abb[12] + abb[7] * (T(1) / T(2));
z[2] = 2 * abb[3];
z[3] = 7 * abb[5] + abb[2] * (T(-5) / T(2)) + abb[0] * (T(3) / T(2)) + -z[0] + z[1] + z[2];
z[3] = abb[17] * z[3];
z[4] = abb[20] + -abb[21];
z[5] = -abb[16] + z[4];
z[6] = 2 * abb[11];
z[7] = z[5] * z[6];
z[8] = 3 * abb[0];
z[9] = 2 * abb[4];
z[10] = z[8] + -z[9];
z[11] = 4 * abb[8];
z[12] = z[10] + -z[11];
z[13] = abb[12] + z[12];
z[13] = abb[16] * z[13];
z[14] = 2 * abb[12];
z[12] = -abb[7] + -z[12] + -z[14];
z[12] = abb[19] * z[12];
z[15] = -abb[16] + abb[19];
z[16] = -z[2] * z[15];
z[17] = abb[12] * z[4];
z[18] = abb[20] + abb[21];
z[19] = abb[0] * z[18];
z[20] = 2 * abb[21];
z[21] = -abb[6] * z[20];
z[22] = abb[16] + abb[20];
z[22] = -6 * abb[19] + -abb[21] + 3 * z[22];
z[22] = abb[5] * z[22];
z[23] = 2 * abb[20];
z[24] = abb[19] + -z[23];
z[24] = abb[2] * z[24];
z[3] = z[3] + -z[7] + z[12] + z[13] + z[16] + z[17] + z[19] + z[21] + z[22] + z[24];
z[3] = abb[17] * z[3];
z[12] = 7 * abb[20];
z[13] = z[12] + -z[20];
z[13] = abb[10] * z[13];
z[16] = abb[8] * z[23];
z[19] = abb[13] * z[4];
z[16] = z[16] + -z[19];
z[13] = z[13] + 2 * z[16];
z[16] = 2 * abb[8] + -abb[13];
z[8] = abb[12] + z[8];
z[21] = 2 * abb[10];
z[22] = -z[8] + 2 * z[16] + z[21];
z[22] = abb[16] * z[22];
z[24] = abb[0] * (T(1) / T(2));
z[1] = abb[10] * (T(-7) / T(2)) + z[1] + -z[16] + z[24];
z[1] = abb[19] * z[1];
z[25] = abb[0] * z[4];
z[25] = -z[17] + z[25];
z[26] = 2 * abb[6];
z[27] = abb[16] + -abb[21];
z[28] = -abb[20] + z[27];
z[29] = z[26] * z[28];
z[1] = z[1] + z[13] + z[22] + z[25] + z[29];
z[1] = abb[19] * z[1];
z[22] = abb[1] + -abb[11];
z[29] = abb[4] + -abb[7];
z[30] = -abb[0] + abb[5];
z[31] = abb[12] + z[30];
z[32] = -5 * z[22] + -z[29] + -z[31];
z[32] = abb[18] * z[32];
z[33] = abb[1] * z[5];
z[7] = -z[7] + 2 * z[33];
z[33] = abb[0] + -abb[12];
z[33] = -abb[16] * z[33];
z[5] = -abb[5] * z[5];
z[34] = abb[3] + -abb[6] + z[6];
z[35] = -abb[4] + abb[5] + z[34];
z[35] = 2 * z[35];
z[36] = -abb[17] * z[35];
z[5] = z[5] + -z[7] + z[25] + z[32] + z[33] + z[36];
z[5] = abb[18] * z[5];
z[25] = abb[0] + -abb[10];
z[25] = z[23] * z[25];
z[16] = 2 * abb[0] + -z[16] + z[29];
z[16] = abb[16] * z[16];
z[29] = -abb[20] * z[11];
z[16] = z[16] + z[17] + z[19] + z[25] + z[29];
z[16] = abb[16] * z[16];
z[17] = 4 * abb[24];
z[25] = prod_pow(abb[21], 2);
z[29] = abb[20] * (T(5) / T(2)) + -z[20];
z[29] = abb[20] * z[29];
z[32] = -abb[16] + 2 * z[18];
z[32] = abb[16] * z[32];
z[12] = 3 * abb[19] + -z[12];
z[12] = abb[19] * z[12];
z[12] = z[12] + -z[17] + -z[25] + z[29] + z[32];
z[12] = abb[2] * z[12];
z[29] = abb[5] + -abb[12];
z[32] = 3 * abb[2];
z[24] = abb[7] + -z[9] + z[24] + (T(1) / T(2)) * z[29] + -z[32] + z[34];
z[24] = abb[25] * z[24];
z[29] = prod_pow(abb[20], 2);
z[33] = -abb[16] + z[20];
z[33] = abb[16] * z[33];
z[29] = -z[25] + z[29] + z[33];
z[29] = abb[6] * z[29];
z[18] = abb[20] * z[18];
z[4] = -abb[16] + -z[4];
z[4] = abb[16] * z[4];
z[27] = -abb[19] + 2 * z[27];
z[27] = abb[19] * z[27];
z[4] = z[4] + z[18] + z[27];
z[4] = abb[3] * z[4];
z[18] = z[15] * z[28];
z[17] = z[17] + z[18] + -z[25];
z[17] = abb[5] * z[17];
z[18] = -abb[7] + abb[12];
z[18] = -3 * abb[1] + z[9] + 2 * z[18] + -z[30];
z[18] = abb[22] * z[18];
z[27] = abb[20] * (T(-7) / T(2)) + z[20];
z[27] = abb[10] * z[27];
z[19] = z[19] + z[27];
z[19] = abb[20] * z[19];
z[27] = abb[21] + -z[23];
z[27] = abb[20] * z[27];
z[25] = z[25] + z[27];
z[25] = abb[0] * z[25];
z[10] = abb[7] + z[10] + -z[14];
z[10] = abb[24] * z[10];
z[0] = abb[2] + -abb[4] + 2 * abb[5] + -abb[13] + z[0] + z[2];
z[0] = 2 * z[0];
z[14] = abb[31] * z[0];
z[27] = abb[0] + -abb[13] + z[21];
z[27] = abb[3] + 2 * z[27] + -z[32];
z[30] = abb[33] * z[27];
z[32] = abb[14] + abb[15];
z[32] = abb[26] * z[32];
z[33] = 6 * abb[8] + abb[0] * (T(-61) / T(12)) + abb[2] * (T(-19) / T(12)) + abb[5] * (T(-11) / T(12)) + abb[11] * (T(-8) / T(3)) + abb[6] * (T(-7) / T(6)) + abb[3] * (T(-3) / T(2)) + abb[13] * (T(-1) / T(3)) + abb[1] * (T(13) / T(4)) + abb[10] * (T(17) / T(6));
z[33] = prod_pow(m1_set::bc<T>[0], 2) * z[33];
z[34] = -abb[1] + abb[6];
z[34] = abb[23] * z[34];
z[36] = -abb[32] * z[35];
z[1] = abb[34] + z[1] + z[3] + z[4] + z[5] + z[10] + z[12] + z[14] + z[16] + z[17] + z[18] + z[19] + z[24] + z[25] + z[29] + z[30] + z[32] + z[33] + 2 * z[34] + z[36];
z[3] = 5 * abb[0] + abb[12];
z[4] = -abb[13] + z[11];
z[5] = abb[7] + z[4];
z[5] = -z[3] + 2 * z[5] + -z[9] + z[21];
z[5] = abb[16] * z[5];
z[4] = -7 * abb[10] + -2 * z[4] + z[8] + z[26];
z[4] = abb[19] * z[4];
z[3] = 2 * abb[2] + -3 * abb[5] + -z[3] + z[6] + z[11];
z[3] = abb[17] * z[3];
z[6] = -2 * abb[7] + z[9] + 8 * z[22] + z[31];
z[6] = abb[18] * z[6];
z[2] = z[2] * z[28];
z[8] = -abb[0] * z[20];
z[9] = -abb[6] * z[23];
z[10] = abb[5] * z[15];
z[11] = -2 * abb[16] + 7 * abb[19] + -5 * abb[20] + z[20];
z[11] = abb[2] * z[11];
z[2] = z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[13];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = abb[29] * z[27];
z[0] = abb[27] * z[0];
z[4] = -abb[28] * z[35];
z[0] = abb[30] + z[0] + z[2] + z[3] + z[4];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_3_30_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_30_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_3_30_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(32)) * (-v[3] + v[5]) * (-32 + -72 * v[0] + 24 * v[1] + 22 * v[3] + -38 * v[5] + -8 * m1_set::bc<T>[1] * (4 + v[3] + v[5]) + 3 * m1_set::bc<T>[2] * (12 * v[0] + -4 * v[1] + 3 * (8 + v[3] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = ((8 + 4 * m1_set::bc<T>[1] + -9 * m1_set::bc<T>[2]) * (T(-1) / T(4)) * (-v[3] + v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[3];
z[0] = -abb[22] + abb[25];
z[1] = prod_pow(abb[19], 2);
z[2] = abb[19] + abb[17] * (T(-3) / T(2));
z[2] = abb[17] * z[2];
z[0] = 4 * abb[24] + 3 * z[0] + (T(1) / T(2)) * z[1] + z[2];
return abb[9] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_30_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_30_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("40.689318351523977089944445633098122714382276123641635477406171239"),stof<T>("53.431952432766875735928060532817539370162872015423822741101514222")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,35> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W8(k,dl), dl[3], dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W22(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W33(k,dl), dlog_W123(k,dv), dlog_W129(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), T{0}, f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), T{0}};
abb[30] = SpDLog_f_3_30_W_16_Im(t, path, abb);
abb[34] = SpDLog_f_3_30_W_16_Re(t, path, abb);

                    
            return f_3_30_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_30_DLogXconstant_part(base_point<T>, kend);
	value += f_3_30_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_30_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_30_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_30_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_30_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_30_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_30_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
