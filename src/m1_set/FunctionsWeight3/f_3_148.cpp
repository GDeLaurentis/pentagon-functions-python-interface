/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_148.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_148_abbreviated (const std::array<T,26>& abb) {
T z[30];
z[0] = 2 * abb[4];
z[1] = abb[9] + z[0];
z[2] = 2 * abb[3];
z[3] = -abb[0] + z[2];
z[4] = 3 * abb[1];
z[5] = 3 * abb[6];
z[1] = -11 * abb[8] + 2 * z[1] + z[3] + z[4] + z[5];
z[6] = abb[24] * z[1];
z[7] = 3 * abb[5] + z[0];
z[8] = 4 * abb[9];
z[9] = 3 * abb[7];
z[10] = -2 * abb[10] + z[7] + -z[8] + z[9];
z[11] = 3 * abb[3];
z[12] = abb[0] + 4 * abb[2] + z[10] + -z[11];
z[13] = abb[23] * z[12];
z[7] = abb[1] + -3 * abb[9] + -abb[10] + z[5] + z[7] + z[9];
z[9] = 5 * abb[8];
z[14] = abb[3] + z[7] + -z[9];
z[14] = abb[25] * z[14];
z[15] = 2 * abb[9];
z[5] = 2 * abb[0] + 3 * abb[2] + 4 * abb[4] + -z[5] + z[15];
z[16] = -abb[3] + z[5] + -z[9];
z[16] = abb[22] * z[16];
z[6] = z[6] + z[13] + z[14] + z[16];
z[13] = 4 * abb[8];
z[4] = 3 * abb[0] + -z[4] + -z[8] + z[13];
z[8] = abb[11] * z[4];
z[14] = 5 * abb[0];
z[16] = -abb[4] + -abb[5] + z[15];
z[16] = 5 * abb[1] + -z[2] + -z[14] + 2 * z[16];
z[16] = abb[15] * z[16];
z[8] = 2 * z[8] + z[16];
z[8] = abb[15] * z[8];
z[16] = 2 * abb[8];
z[17] = abb[4] + abb[9];
z[18] = abb[1] + -abb[5] + -z[16] + z[17];
z[18] = abb[15] * z[18];
z[19] = abb[2] + -abb[5];
z[20] = 2 * abb[6];
z[21] = abb[3] + z[20];
z[22] = z[19] + -z[21];
z[23] = abb[0] + z[17] + z[22];
z[23] = abb[11] * z[23];
z[23] = -z[18] + z[23];
z[24] = -abb[0] + abb[1];
z[25] = abb[2] + z[24];
z[26] = abb[3] + -z[25];
z[26] = abb[14] * z[26];
z[23] = 2 * z[23] + z[26];
z[26] = 2 * abb[14];
z[23] = z[23] * z[26];
z[17] = -abb[7] + z[17];
z[27] = 2 * abb[2];
z[21] = z[17] + -z[21] + z[27];
z[28] = abb[11] + -abb[14];
z[28] = z[21] * z[28];
z[27] = -abb[4] + -z[27];
z[11] = -abb[0] + 4 * abb[6] + -abb[9] + z[11] + 2 * z[27];
z[11] = abb[12] * z[11];
z[11] = z[11] + 2 * z[28];
z[27] = 2 * abb[12];
z[11] = z[11] * z[27];
z[28] = -abb[9] + z[19];
z[29] = -abb[4] + -z[28];
z[3] = -abb[1] + z[3] + 2 * z[29];
z[3] = prod_pow(abb[11], 2) * z[3];
z[3] = z[3] + 2 * z[6] + z[8] + z[11] + z[23];
z[6] = -z[16] + z[17] + z[25];
z[6] = abb[14] * z[6];
z[8] = abb[12] * z[21];
z[11] = abb[7] + -abb[9] + -z[16] + -z[22];
z[11] = abb[11] * z[11];
z[6] = z[6] + z[8] + z[11] + z[18];
z[8] = 2 * abb[7];
z[11] = z[8] + -z[15];
z[17] = -abb[3] + -z[0] + z[11] + z[13] + -z[25];
z[17] = abb[13] * z[17];
z[6] = 2 * z[6] + z[17];
z[6] = abb[13] * z[6];
z[10] = -4 * abb[1] + abb[3] + z[10] + z[14];
z[14] = abb[21] * z[10];
z[6] = z[6] + z[14];
z[14] = abb[4] + 4 * abb[5];
z[14] = 24 * abb[6] + 12 * abb[7] + abb[9] * (T(-76) / T(3)) + abb[8] * (T(-40) / T(3)) + abb[10] * (T(-16) / T(3)) + abb[2] * (T(-1) / T(3)) + abb[3] * (T(13) / T(3)) + 4 * z[14] + 11 * z[24];
z[14] = prod_pow(m1_set::bc<T>[0], 2) * z[14];
z[3] = 2 * z[3] + 4 * z[6] + z[14];
z[3] = 4 * z[3];
z[2] = -z[2] + -z[8] + z[13] + z[24] + 2 * z[28];
z[2] = abb[11] * z[2];
z[6] = z[19] + -z[20];
z[0] = -abb[0] + abb[7] + -z[0] + -z[6] + -z[15] + z[16];
z[0] = z[0] * z[26];
z[4] = -abb[15] * z[4];
z[8] = -z[21] * z[27];
z[6] = abb[4] + z[6] + -z[11];
z[6] = abb[13] * z[6];
z[0] = z[0] + z[2] + z[4] + 2 * z[6] + z[8];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[19] * z[1];
z[2] = abb[20] * z[7];
z[4] = abb[16] * z[10];
z[6] = abb[18] * z[12];
z[5] = abb[17] * z[5];
z[7] = -abb[17] + -abb[20];
z[7] = z[7] * z[9];
z[8] = -abb[17] + abb[20];
z[8] = abb[3] * z[8];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + z[6] + z[7] + z[8];
z[0] = 16 * z[0];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_3_148_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_148_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_148_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("1079.82781143089988753789681845678522904872351811224701539303743536"),stof<T>("23.59355695184630573673583798045486863905681931657837527033867001")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,26> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), dlog_W66(k,dl), f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_11(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_3_148_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_148_DLogXconstant_part(base_point<T>, kend);
	value += f_3_148_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_148_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_148_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_148_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_148_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_148_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_148_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
