/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_12.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_12_abbreviated (const std::array<T,33>& abb) {
T z[34];
z[0] = 2 * abb[1];
z[1] = 2 * abb[2];
z[2] = z[0] + -z[1];
z[3] = abb[12] * (T(1) / T(2));
z[4] = abb[3] * (T(3) / T(2)) + z[3];
z[5] = abb[6] * (T(1) / T(2));
z[6] = abb[0] + abb[4];
z[7] = 2 * abb[8];
z[8] = abb[11] * (T(1) / T(2));
z[9] = z[2] + z[4] + z[5] + z[6] + -z[7] + -z[8];
z[9] = abb[21] * z[9];
z[10] = -abb[10] + z[0] + z[1];
z[11] = -z[3] + z[10];
z[12] = abb[3] * (T(1) / T(2));
z[13] = -z[8] + z[12];
z[14] = 2 * abb[4];
z[15] = abb[0] + z[14];
z[16] = z[13] + z[15];
z[17] = abb[6] * (T(-5) / T(2)) + z[7] + -z[11] + -z[16];
z[17] = abb[17] * z[17];
z[18] = abb[4] + -abb[10];
z[19] = abb[6] + z[18];
z[20] = 2 * abb[3];
z[21] = z[19] + z[20];
z[7] = -4 * abb[1] + abb[11] + z[7];
z[22] = -z[7] + z[21];
z[23] = abb[19] * z[22];
z[1] = abb[6] + z[1];
z[1] = 2 * z[1] + z[18];
z[18] = -abb[3] + -abb[12] + z[1];
z[24] = abb[20] * z[18];
z[25] = 2 * abb[0];
z[26] = 2 * abb[5];
z[27] = abb[3] + -3 * abb[4] + -6 * abb[6] + abb[10] + -z[25] + z[26];
z[27] = abb[18] * z[27];
z[28] = abb[1] + -abb[2];
z[16] = -abb[5] + 3 * abb[8] + abb[6] * (T(-7) / T(2)) + -z[16] + -z[28];
z[16] = abb[22] * z[16];
z[9] = z[9] + z[16] + z[17] + z[23] + z[24] + z[27];
z[9] = abb[22] * z[9];
z[16] = abb[1] + abb[7];
z[17] = (T(1) / T(2)) * z[16];
z[27] = abb[6] * (T(3) / T(2));
z[29] = -abb[10] + z[20];
z[30] = abb[4] * (T(1) / T(2));
z[31] = abb[0] + abb[5] + -abb[8] + -2 * abb[9] + z[8] + -z[17] + z[27] + -z[29] + z[30];
z[31] = abb[19] * z[31];
z[32] = -abb[6] + abb[10];
z[33] = -abb[9] + z[32];
z[6] = -z[6] + -z[16] + z[26] + 2 * z[33];
z[33] = -abb[18] * z[6];
z[22] = -abb[21] * z[22];
z[22] = z[22] + z[31] + z[33];
z[22] = abb[19] * z[22];
z[5] = -abb[0] + z[5] + z[11] + z[13];
z[5] = abb[21] * z[5];
z[11] = z[3] + z[28];
z[8] = -abb[3] + abb[7] + z[8] + -z[11];
z[8] = abb[17] * z[8];
z[13] = -abb[0] + -abb[3] + -z[19];
z[13] = abb[18] * z[13];
z[5] = z[5] + z[8] + z[13] + z[23];
z[5] = abb[17] * z[5];
z[8] = -abb[31] * z[6];
z[13] = z[14] + z[25];
z[19] = 3 * abb[3];
z[7] = 5 * abb[6] + z[7] + z[13] + -z[19] + -z[26];
z[7] = abb[24] * z[7];
z[11] = abb[4] + -z[11] + z[12] + z[27];
z[11] = prod_pow(abb[21], 2) * z[11];
z[12] = abb[0] * (T(1) / T(2));
z[23] = z[12] + z[30];
z[17] = -z[17] + -z[23] + z[32];
z[17] = abb[18] * z[17];
z[21] = abb[21] * z[21];
z[17] = z[17] + z[21];
z[17] = abb[18] * z[17];
z[21] = -abb[6] + abb[9];
z[23] = -abb[3] + abb[10] * (T(3) / T(2)) + -z[16] + -z[21] + z[23];
z[23] = abb[25] * z[23];
z[1] = -abb[7] + z[1] + -z[4] + z[12];
z[1] = abb[20] * z[1];
z[4] = -abb[17] + -abb[21];
z[4] = z[4] * z[18];
z[1] = z[1] + z[4];
z[1] = abb[20] * z[1];
z[4] = 2 * abb[7] + -abb[12];
z[12] = abb[4] + abb[6];
z[18] = 3 * abb[2] + abb[9] + -2 * abb[10] + z[4] + z[12];
z[27] = -abb[30] * z[18];
z[0] = abb[0] + -abb[3] + abb[11] + -z[0];
z[0] = abb[23] * z[0];
z[19] = 4 * abb[2] + abb[6] + -abb[12] + -z[19];
z[30] = -abb[32] * z[19];
z[3] = -5 * abb[3] + 7 * abb[6] + -abb[7] + abb[1] * (T(-35) / T(4)) + abb[2] * (T(1) / T(4)) + abb[9] * (T(11) / T(2)) + abb[4] * (T(13) / T(4)) + abb[0] * (T(17) / T(4)) + z[3];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[31] = 3 * abb[13] + abb[14] + abb[15] + -abb[16];
z[31] = abb[26] * z[31];
z[0] = z[0] + z[1] + (T(1) / T(3)) * z[3] + z[5] + z[7] + z[8] + z[9] + z[11] + z[17] + z[22] + z[23] + z[27] + z[30] + (T(1) / T(2)) * z[31];
z[1] = -z[4] + -z[10] + z[12] + z[25];
z[1] = abb[17] * z[1];
z[3] = -z[13] + z[29];
z[4] = 2 * abb[6] + -abb[9];
z[4] = -z[3] + 2 * z[4] + -z[16];
z[4] = abb[18] * z[4];
z[5] = 4 * abb[8] + -z[26];
z[3] = -3 * abb[1] + abb[7] + z[3] + z[5] + 4 * z[21];
z[3] = abb[19] * z[3];
z[7] = 4 * abb[6] + z[15] + -z[28];
z[5] = -abb[12] + -z[5] + 2 * z[7] + -z[20];
z[5] = abb[22] * z[5];
z[2] = -abb[3] + -3 * abb[6] + abb[12] + z[2] + -z[14];
z[2] = abb[21] * z[2];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[24];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[28] * z[6];
z[3] = -abb[27] * z[18];
z[4] = -abb[29] * z[19];
z[1] = z[1] + z[2] + z[3] + z[4];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_12_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_12_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_12_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("45.870626012292646463824900536416937873678067981044247971074025182"),stof<T>("8.115727860305059152635211273068611508744075810219363293614958643")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,33> abb = {dl[0], dl[1], dl[5], dl[2], dl[3], dlog_W10(k,dl), dl[4], dlog_W15(k,dl), dlog_W21(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k)};

                    
            return f_3_12_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_12_DLogXconstant_part(base_point<T>, kend);
	value += f_3_12_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_12_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_12_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_12_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_12_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_12_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_12_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
