/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_198.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_198_abbreviated (const std::array<T,50>& abb) {
T z[33];
z[0] = 2 * abb[4];
z[1] = -abb[6] + z[0];
z[2] = -abb[0] + abb[3];
z[3] = 2 * abb[1];
z[4] = abb[5] + z[1] + z[2] + z[3];
z[4] = abb[21] * z[4];
z[5] = abb[18] + -abb[23];
z[5] = abb[5] * z[5];
z[6] = abb[2] * abb[23];
z[5] = z[5] + z[6];
z[7] = 2 * abb[2];
z[8] = -abb[7] + z[7];
z[9] = 2 * abb[5];
z[10] = -abb[1] + -z[8] + z[9];
z[10] = abb[20] * z[10];
z[11] = 3 * abb[5];
z[12] = -abb[4] + z[11];
z[8] = -abb[1] + z[8] + -z[12];
z[13] = -z[2] + z[8];
z[13] = abb[19] * z[13];
z[14] = -abb[4] + z[3];
z[15] = -abb[3] + z[14];
z[15] = abb[18] * z[15];
z[16] = abb[7] * abb[23];
z[17] = abb[1] * abb[23];
z[18] = -abb[4] + abb[6];
z[19] = abb[24] * z[18];
z[20] = abb[2] + abb[7];
z[21] = abb[0] + -5 * abb[1] + -abb[5] + -z[20];
z[21] = abb[25] * z[21];
z[4] = z[4] + z[5] + z[10] + z[13] + z[15] + z[16] + z[17] + z[19] + z[21];
z[10] = abb[0] + abb[5];
z[13] = 3 * abb[3] + abb[4] + -z[10];
z[13] = abb[22] * z[13];
z[4] = 2 * z[4] + z[13];
z[4] = abb[22] * z[4];
z[13] = z[16] + -z[17];
z[6] = -z[6] + z[13];
z[15] = 2 * abb[14];
z[16] = 2 * abb[8];
z[19] = z[15] + z[16];
z[21] = -abb[0] + 12 * abb[1] + -z[19];
z[22] = 2 * abb[3];
z[23] = z[21] + z[22];
z[23] = abb[18] * z[23];
z[24] = 4 * abb[18] + abb[23];
z[24] = abb[5] * z[24];
z[23] = -z[6] + z[23] + z[24];
z[24] = 2 * abb[7];
z[7] = abb[0] + -16 * abb[1] + -8 * abb[5] + 7 * abb[8] + -z[7] + -z[22] + z[24];
z[7] = abb[25] * z[7];
z[7] = z[7] + 2 * z[23];
z[7] = abb[25] * z[7];
z[23] = -abb[0] + 4 * abb[1];
z[25] = z[23] + z[24];
z[25] = abb[29] * z[25];
z[26] = abb[1] * prod_pow(abb[23], 2);
z[27] = abb[43] * z[18];
z[25] = abb[49] + z[25] + z[26] + z[27];
z[26] = 3 * abb[1];
z[27] = -abb[0] + z[26];
z[28] = 3 * abb[8];
z[15] = -z[15] + 2 * z[27] + z[28];
z[27] = -abb[4] + -z[15];
z[27] = abb[18] * z[27];
z[17] = -4 * z[17] + z[27];
z[17] = abb[18] * z[17];
z[27] = abb[12] * abb[18];
z[29] = abb[4] * abb[18];
z[29] = -z[27] + z[29];
z[10] = z[10] + -z[16];
z[30] = 2 * abb[6] + z[10];
z[31] = abb[4] + 2 * abb[12] + -z[30];
z[31] = abb[24] * z[31];
z[29] = 2 * z[29] + z[31];
z[29] = abb[24] * z[29];
z[5] = -z[5] + -z[13];
z[12] = z[12] + z[24];
z[13] = abb[0] + 4 * abb[2] + -z[12];
z[13] = abb[20] * z[13];
z[5] = 2 * z[5] + z[13];
z[5] = abb[20] * z[5];
z[13] = prod_pow(abb[18], 2);
z[24] = prod_pow(abb[24], 2);
z[31] = prod_pow(abb[20], 2);
z[24] = -z[13] + z[24] + -z[31];
z[24] = abb[3] * z[24];
z[31] = 6 * abb[27];
z[13] = 4 * abb[26] + -z[13] + -z[31];
z[13] = abb[5] * z[13];
z[31] = abb[2] * z[31];
z[32] = -abb[16] * abb[45];
z[4] = z[4] + z[5] + z[7] + z[13] + z[17] + z[24] + 2 * z[25] + z[29] + z[31] + z[32];
z[5] = abb[2] + -abb[7] + z[11] + -z[16] + z[26];
z[5] = abb[25] * z[5];
z[1] = -z[1] + z[10];
z[1] = abb[24] * z[1];
z[7] = -abb[12] + z[2];
z[10] = abb[1] + abb[8] + z[0] + z[7];
z[10] = abb[21] * z[10];
z[13] = -abb[8] + z[3];
z[13] = 2 * z[13];
z[17] = -abb[14] + z[13];
z[24] = -abb[4] + -z[17];
z[24] = abb[18] * z[24];
z[25] = -3 * abb[18] + -abb[23];
z[25] = abb[5] * z[25];
z[29] = -abb[1] + z[20];
z[29] = abb[20] * z[29];
z[31] = -abb[18] + -abb[24];
z[31] = abb[3] * z[31];
z[1] = z[1] + z[5] + z[6] + z[10] + z[24] + z[25] + z[27] + z[29] + z[31];
z[1] = abb[21] * z[1];
z[5] = 7 * abb[1];
z[6] = -z[5] + -z[11] + z[19] + z[20] + -z[22];
z[6] = abb[25] * z[6];
z[10] = -abb[4] + abb[12];
z[11] = abb[3] + -abb[14];
z[19] = abb[5] + -z[10] + z[11] + -z[20] + z[26];
z[19] = abb[21] * z[19];
z[14] = -z[2] + -z[9] + -z[14];
z[14] = abb[19] * z[14];
z[17] = z[9] + z[17];
z[17] = abb[18] * z[17];
z[8] = -abb[0] + -z[8];
z[8] = abb[20] * z[8];
z[20] = abb[24] * z[10];
z[24] = abb[18] + abb[20];
z[25] = abb[3] * z[24];
z[6] = z[6] + z[8] + z[14] + z[17] + z[19] + z[20] + z[25];
z[6] = abb[19] * z[6];
z[2] = abb[2] + -z[2] + -z[12];
z[2] = abb[30] * z[2];
z[1] = z[1] + z[2] + z[6];
z[2] = abb[5] + abb[6];
z[6] = 3 * abb[4];
z[7] = z[2] + -z[6] + -z[7] + -z[16];
z[7] = 8 * z[7];
z[8] = abb[42] * z[7];
z[12] = abb[2] + abb[12];
z[5] = -2 * abb[0] + -4 * abb[4] + abb[14] * (T(-8) / T(3)) + abb[6] * (T(10) / T(3)) + abb[5] * (T(14) / T(3)) + z[5] + (T(-2) / T(3)) * z[12] + z[28];
z[12] = prod_pow(m1_set::bc<T>[0], 2);
z[5] = z[5] * z[12];
z[14] = -abb[18] + abb[22];
z[16] = -abb[23] + 2 * abb[25];
z[17] = -abb[18] + z[16];
z[14] = z[14] * z[17];
z[17] = abb[29] + abb[41];
z[12] = (T(-2) / T(3)) * z[12] + z[14] + -2 * z[17];
z[14] = 4 * abb[13];
z[12] = z[12] * z[14];
z[17] = -abb[2] + z[3] + z[11];
z[17] = abb[28] * z[17];
z[19] = -abb[1] + -abb[5] + abb[8];
z[19] = abb[31] * z[19];
z[17] = z[17] + z[19];
z[19] = -abb[0] + 6 * abb[1];
z[20] = 4 * abb[5] + z[22];
z[19] = -abb[8] + 4 * abb[14] + -3 * z[19] + -z[20];
z[19] = 4 * z[19];
z[25] = -abb[41] * z[19];
z[26] = -abb[15] + abb[17];
z[26] = 8 * z[26];
z[28] = -abb[44] * z[26];
z[1] = abb[46] + abb[47] + abb[48] + 8 * z[1] + 4 * z[4] + 2 * z[5] + z[8] + z[12] + 16 * z[17] + z[25] + z[28];
z[4] = -abb[6] + -abb[12] + -abb[14] + z[6] + z[9] + z[22] + z[23];
z[4] = abb[21] * z[4];
z[5] = -z[9] + -z[10] + -z[11] + -z[13];
z[5] = abb[19] * z[5];
z[6] = abb[18] * z[15];
z[0] = -abb[12] + -z[0] + z[30];
z[0] = abb[24] * z[0];
z[8] = -z[20] + -z[21];
z[8] = abb[25] * z[8];
z[9] = -abb[1] + abb[4];
z[2] = abb[3] + -z[2] + 2 * z[9];
z[2] = abb[22] * z[2];
z[9] = abb[5] * z[24];
z[3] = abb[23] * z[3];
z[10] = abb[18] + -abb[24];
z[10] = abb[3] * z[10];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[8] + z[9] + z[10] + z[27];
z[0] = m1_set::bc<T>[0] * z[0];
z[0] = abb[40] + z[0];
z[2] = abb[33] * z[7];
z[3] = -abb[32] * z[19];
z[4] = -2 * abb[18] + abb[22] + z[16];
z[4] = m1_set::bc<T>[0] * z[4];
z[4] = -2 * abb[32] + z[4];
z[4] = z[4] * z[14];
z[5] = 8 * z[18];
z[5] = abb[34] * z[5];
z[6] = abb[16] * abb[36];
z[7] = -abb[35] * z[26];
z[0] = abb[37] + abb[38] + abb[39] + 8 * z[0] + z[2] + z[3] + z[4] + z[5] + -4 * z[6] + z[7];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_3_198_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_198_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_3_198_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(2)) * (-v[3] + v[5]) * (-12 * v[0] + 4 * v[1] + 6 * m1_set::bc<T>[1] * (4 + v[3] + v[5]) + -3 * (8 + 3 * v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = (4 * (-1 + 3 * m1_set::bc<T>[1]) * (-v[3] + v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[2];
z[0] = abb[18] + -abb[21];
z[1] = -abb[20] + z[0];
z[1] = abb[20] * z[1];
z[0] = abb[19] + -z[0];
z[0] = abb[19] * z[0];
z[0] = abb[30] + z[0] + z[1];
return 8 * abb[9] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_198_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (4 * m1_set::bc<T>[0] * (-v[3] + v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = abb[19] + -abb[20];
return 8 * abb[9] * m1_set::bc<T>[0] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_198_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(2)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (-24 + -15 * v[0] + -5 * v[1] + -3 * v[2] + 9 * v[3] + 12 * v[4] + 6 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + 3 * v[5])) / prod_pow(tend, 2);
c[1] = (-4 * (-1 + 3 * m1_set::bc<T>[1]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[2];
z[0] = abb[18] + -abb[22];
z[1] = abb[25] + -z[0];
z[1] = abb[25] * z[1];
z[0] = -abb[23] + z[0];
z[0] = abb[23] * z[0];
z[0] = -abb[29] + z[0] + z[1];
return 8 * abb[10] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_198_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (-m1_set::bc<T>[0] * (v[0] + -1 * v[1] + v[2] + v[3] + -1 * v[5]) * (4 + v[0] + v[1] + v[2] + -1 * v[3] + -2 * v[4] + -1 * v[5])) / prod_pow(tend, 2);
c[1] = (-4 * m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = -abb[23] + abb[25];
return 8 * abb[10] * m1_set::bc<T>[0] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_198_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4]) * (2 * (4 * v[0] + -11 * v[1] + -7 * v[2] + 3 * v[3] + 7 * v[4] + -6 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5])) + m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]))) / prod_pow(tend, 2);
c[1] = (-4 * (4 + 6 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[3];
z[0] = -abb[26] + 2 * abb[28] + abb[31];
z[1] = -abb[18] + abb[22] + -abb[23];
z[2] = -abb[21] + -2 * z[1];
z[2] = abb[21] * z[2];
z[1] = -2 * abb[21] + z[1];
z[1] = 5 * abb[25] + 2 * z[1];
z[1] = abb[25] * z[1];
z[0] = 2 * z[0] + z[1] + z[2];
return 8 * abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_198_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (2 * m1_set::bc<T>[0] * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (8 * m1_set::bc<T>[0] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = -abb[21] + abb[25];
return 16 * abb[11] * m1_set::bc<T>[0] * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_198_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.940313008484691411040722732728746671583986051814330128150943921"),stof<T>("48.773029441727147112869838373396201907101315918547307019843048692")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 16, 19, 203});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,50> abb = {dl[0], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W16(k,dl), dlog_W17(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W29(k,dl), dlog_W32(k,dl), dlog_W121(k,dv), dlog_W125(k,dv), dlog_W127(k,dv), f_1_1(k), f_1_2(k), f_1_4(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_13(k), f_2_14(k), f_2_15(k), f_2_16(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, T{0}, f_2_2_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k), f_2_25_re(k), T{0}, T{0}, T{0}, T{0}};
abb[37] = SpDLog_f_3_198_W_16_Im(t, path, abb);
abb[38] = SpDLog_f_3_198_W_17_Im(t, path, abb);
abb[39] = SpDLog_f_3_198_W_20_Im(t, path, abb);
abb[46] = SpDLog_f_3_198_W_16_Re(t, path, abb);
abb[47] = SpDLog_f_3_198_W_17_Re(t, path, abb);
abb[48] = SpDLog_f_3_198_W_20_Re(t, path, abb);
{
auto c = dlog_W176(k,dv) * f_2_35(k);
abb[49] = c.real();
abb[40] = c.imag();
SpDLog_Sigma5<T,203,175>(k, dv, abb[49], abb[40], f_2_35_series_coefficients<T>);
}

                    
            return f_3_198_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_198_DLogXconstant_part(base_point<T>, kend);
	value += f_3_198_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_198_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_198_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_198_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_198_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_198_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_198_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
