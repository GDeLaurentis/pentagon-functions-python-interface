/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_31.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_31_abbreviated (const std::array<T,23>& abb) {
T z[21];
z[0] = abb[2] + abb[4];
z[1] = 2 * abb[5] + z[0];
z[2] = 3 * abb[7];
z[3] = z[1] + -z[2];
z[4] = -2 * abb[1] + -z[3];
z[4] = abb[14] * z[4];
z[5] = abb[5] + abb[8];
z[6] = 2 * z[0] + z[5];
z[7] = 3 * abb[3];
z[8] = -z[6] + z[7];
z[8] = abb[10] * z[8];
z[3] = 3 * abb[1] + -abb[8] + z[3];
z[9] = -abb[12] * z[3];
z[10] = abb[1] + abb[5];
z[11] = abb[3] * (T(-3) / T(2)) + (T(1) / T(2)) * z[0] + z[10];
z[11] = abb[13] * z[11];
z[4] = z[4] + z[8] + z[9] + z[11];
z[4] = abb[13] * z[4];
z[8] = abb[0] + abb[1] + -abb[8];
z[9] = abb[13] + -abb[14];
z[11] = z[8] * z[9];
z[5] = -3 * abb[0] + -z[0] + z[5];
z[12] = abb[11] * z[5];
z[13] = -abb[10] + abb[12];
z[14] = -abb[5] + z[0];
z[15] = 2 * abb[8] + z[14];
z[13] = z[13] * z[15];
z[16] = -abb[0] + -abb[8] + -z[14];
z[16] = abb[1] + (T(1) / T(2)) * z[16];
z[16] = abb[9] * z[16];
z[11] = z[11] + -z[12] + z[13] + z[16];
z[11] = abb[9] * z[11];
z[13] = abb[8] * abb[15];
z[16] = abb[5] * abb[16];
z[17] = -abb[3] + z[0];
z[18] = abb[8] + z[17];
z[19] = -abb[17] * z[18];
z[13] = z[13] + z[16] + z[19];
z[16] = abb[10] * z[18];
z[18] = abb[3] + abb[5] + -abb[8];
z[18] = abb[1] + -abb[7] + (T(1) / T(2)) * z[18];
z[18] = abb[12] * z[18];
z[16] = z[16] + z[18];
z[3] = abb[14] * z[3];
z[3] = z[3] + 3 * z[16];
z[3] = abb[12] * z[3];
z[16] = prod_pow(abb[14], 2);
z[18] = -abb[15] + -abb[16];
z[19] = abb[14] + abb[13] * (T(-1) / T(2));
z[19] = abb[13] * z[19];
z[18] = (T(-1) / T(2)) * z[16] + 3 * z[18] + z[19];
z[18] = abb[0] * z[18];
z[5] = z[5] * z[9];
z[5] = z[5] + (T(3) / T(2)) * z[12];
z[5] = abb[11] * z[5];
z[19] = abb[1] + abb[5] * (T(-1) / T(2)) + -z[0];
z[16] = z[16] * z[19];
z[19] = -abb[8] + z[1];
z[19] = -abb[14] * z[19];
z[20] = abb[8] + z[0];
z[20] = abb[10] * z[20];
z[19] = z[19] + (T(-3) / T(2)) * z[20];
z[19] = abb[10] * z[19];
z[10] = -abb[7] + z[10] + z[17];
z[10] = 3 * z[10];
z[17] = -abb[21] * z[10];
z[20] = -abb[18] * z[15];
z[0] = 5 * abb[5] + (T(23) / T(2)) * z[0];
z[0] = abb[7] * (T(-7) / T(2)) + abb[1] * (T(-5) / T(6)) + abb[3] * (T(1) / T(2)) + abb[0] * (T(13) / T(6)) + (T(1) / T(3)) * z[0];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[0] = abb[22] + z[0] + z[3] + z[4] + z[5] + z[11] + 3 * z[13] + z[16] + z[17] + z[18] + z[19] + z[20];
z[3] = abb[0] * z[9];
z[4] = abb[9] * z[8];
z[3] = -z[3] + z[4] + z[12];
z[4] = -abb[1] + -z[2] + z[7] + z[14];
z[4] = abb[13] * z[4];
z[1] = abb[1] + 2 * z[1] + -z[2];
z[1] = abb[14] * z[1];
z[2] = 2 * z[6] + -z[7];
z[2] = abb[10] * z[2];
z[5] = -abb[12] * z[15];
z[1] = z[1] + z[2] + -2 * z[3] + z[4] + z[5];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[19] * z[10];
z[1] = abb[20] + z[1] + z[2];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_31_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_31_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_3_31_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(32)) * (-v[3] + v[5]) * (4 * m1_set::bc<T>[1] * (4 + v[3] + v[5]) + -4 * (12 + 2 * v[0] + -2 * v[1] + v[3] + 3 * v[5]) + m1_set::bc<T>[2] * (12 * v[0] + -4 * v[1] + 3 * (8 + v[3] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = ((-2 + 2 * m1_set::bc<T>[1] + 3 * m1_set::bc<T>[2]) * (T(3) / T(4)) * (-v[3] + v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[2];
z[0] = abb[9] + abb[10];
z[1] = abb[10] + -abb[12];
z[0] = z[0] * z[1];
z[0] = -abb[15] + abb[17] + abb[18] + z[0];
return 3 * abb[6] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_31_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-3) / T(8)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(3) / T(2)) * (v[3] + -v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = -abb[10] + abb[12];
return 3 * abb[6] * m1_set::bc<T>[0] * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_31_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("49.524956236407311254082823283016140611008461205871124774759687584"),stof<T>("-73.841783123950370409383066140526278607337617840009044329562912174")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,23> abb = {dl[1], dl[5], dl[2], dlog_W7(k,dl), dl[3], dl[4], dlog_W16(k,dl), dlog_W22(k,dl), dlog_W28(k,dl), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_7_im(k), T{0}, f_2_7_re(k), T{0}};
abb[20] = SpDLog_f_3_31_W_16_Im(t, path, abb);
abb[22] = SpDLog_f_3_31_W_16_Re(t, path, abb);

                    
            return f_3_31_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_31_DLogXconstant_part(base_point<T>, kend);
	value += f_3_31_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_31_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_31_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_31_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_31_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_31_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_31_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
