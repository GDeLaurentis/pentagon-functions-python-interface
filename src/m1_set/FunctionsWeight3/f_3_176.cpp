/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_176.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_176_abbreviated (const std::array<T,45>& abb) {
T z[51];
z[0] = 2 * abb[0];
z[1] = 3 * abb[13];
z[2] = z[0] + -z[1];
z[3] = 3 * abb[8];
z[4] = 4 * abb[9];
z[5] = 2 * abb[16];
z[6] = 3 * abb[7];
z[7] = -abb[3] + -abb[6] + z[2] + z[3] + z[4] + -z[5] + z[6];
z[7] = abb[29] * z[7];
z[8] = 3 * abb[4];
z[2] = z[2] + z[8];
z[9] = 4 * abb[1];
z[10] = 3 * abb[3];
z[11] = abb[5] + abb[8];
z[12] = 2 * abb[15];
z[13] = z[2] + z[9] + z[10] + -z[11] + -z[12];
z[13] = abb[30] * z[13];
z[14] = -abb[6] + 2 * abb[10];
z[15] = abb[7] + abb[8];
z[16] = -abb[12] + z[14] + z[15];
z[16] = abb[42] * z[16];
z[17] = abb[3] + -abb[12];
z[18] = abb[5] + abb[7] + -z[17];
z[18] = abb[32] * z[18];
z[19] = -abb[18] * abb[36];
z[16] = z[16] + z[18] + z[19];
z[18] = 3 * abb[14];
z[19] = z[17] + -z[18];
z[20] = abb[6] + z[4];
z[2] = -z[2] + -z[19] + -z[20];
z[2] = abb[35] * z[2];
z[21] = 2 * abb[12];
z[22] = -abb[16] + z[21];
z[23] = -abb[3] + z[22];
z[24] = 6 * abb[10];
z[25] = -abb[6] + z[24];
z[23] = z[18] + 2 * z[23] + -z[25];
z[26] = abb[33] * z[23];
z[27] = -abb[32] + -abb[42];
z[27] = z[8] * z[27];
z[28] = 6 * abb[2];
z[29] = -abb[32] * z[28];
z[2] = z[2] + z[7] + z[13] + 3 * z[16] + z[26] + z[27] + z[29];
z[7] = -abb[12] + z[12];
z[13] = z[5] + z[7];
z[16] = -z[8] + z[24];
z[26] = z[1] + z[16];
z[27] = 2 * abb[1];
z[29] = abb[3] + z[27];
z[30] = 4 * abb[0];
z[31] = 4 * abb[2];
z[20] = -4 * abb[8] + -abb[14] + z[13] + -z[20] + z[26] + z[29] + -z[30] + -z[31];
z[20] = abb[19] * z[20];
z[32] = abb[2] + -abb[10];
z[33] = 2 * abb[14];
z[34] = 2 * abb[8];
z[32] = -abb[5] + -abb[15] + -abb[16] + -z[17] + 3 * z[32] + z[33] + z[34];
z[35] = 2 * abb[26];
z[32] = z[32] * z[35];
z[36] = 2 * abb[6];
z[37] = z[3] + -z[36];
z[18] = z[5] + -z[18];
z[38] = 2 * abb[3];
z[39] = z[18] + z[38];
z[8] = z[6] + -z[8];
z[40] = -abb[12] + -z[8] + -z[37] + z[39];
z[40] = abb[24] * z[40];
z[41] = -abb[6] + abb[14];
z[11] = z[7] + -z[11] + -z[41];
z[11] = abb[23] * z[11];
z[42] = -abb[8] + abb[14];
z[43] = abb[3] + z[42];
z[44] = 2 * abb[9] + -z[43];
z[45] = abb[5] + -z[27] + z[44];
z[45] = abb[25] * z[45];
z[46] = 2 * z[45];
z[47] = 2 * abb[21];
z[48] = -abb[0] + abb[2];
z[49] = abb[1] + -abb[5] + z[48];
z[47] = z[47] * z[49];
z[11] = z[11] + z[20] + z[32] + z[40] + z[46] + -z[47];
z[20] = -z[34] + z[36];
z[40] = 2 * abb[5];
z[50] = z[7] + z[40];
z[18] = z[0] + z[18] + z[20] + -z[29] + -z[31] + z[50];
z[29] = -abb[22] * z[18];
z[11] = 2 * z[11] + z[29];
z[11] = abb[22] * z[11];
z[29] = 5 * abb[14] + z[6];
z[7] = -4 * abb[5] + -z[7] + -z[16] + -z[28] + z[29] + -z[37] + -z[38];
z[7] = abb[23] * z[7];
z[31] = 2 * abb[2];
z[37] = z[10] + z[31];
z[12] = abb[5] + z[12] + -z[21] + z[25] + -z[29] + z[37];
z[12] = z[12] * z[35];
z[7] = z[7] + z[12];
z[7] = abb[23] * z[7];
z[12] = -abb[19] * z[18];
z[18] = 4 * abb[14];
z[16] = z[16] + -z[18] + -z[20] + z[50];
z[20] = -abb[23] * z[16];
z[21] = abb[24] * z[23];
z[20] = z[20] + z[21] + -z[32] + z[46];
z[12] = z[12] + 2 * z[20];
z[12] = abb[19] * z[12];
z[20] = 3 * abb[5];
z[21] = 7 * abb[12] + -z[3] + z[20] + -z[25] + -z[28] + -z[39];
z[21] = abb[24] * z[21];
z[20] = z[8] + z[20];
z[5] = -5 * abb[12] + -6 * abb[14] + z[5] + -z[20] + z[24] + z[37];
z[5] = abb[26] * z[5];
z[19] = -abb[6] + -z[19] + z[20] + -z[31];
z[19] = abb[23] * z[19];
z[5] = z[5] + z[19];
z[5] = 2 * z[5] + z[21];
z[5] = abb[24] * z[5];
z[19] = 3 * abb[6] + -z[26];
z[20] = 6 * abb[0];
z[21] = z[19] + z[20];
z[23] = -abb[1] + abb[9];
z[24] = -abb[3] + abb[8];
z[25] = abb[5] + z[24];
z[18] = z[18] + z[21] + -8 * z[23] + -4 * z[25];
z[18] = abb[25] * z[18];
z[14] = abb[4] + -z[14];
z[26] = -abb[5] + z[14] + z[42];
z[29] = 6 * z[26];
z[32] = -abb[23] * z[29];
z[18] = z[18] + z[32];
z[18] = abb[25] * z[18];
z[32] = abb[12] + z[14];
z[35] = -abb[14] + z[31] + -z[32] + z[34];
z[37] = abb[41] * z[35];
z[0] = -abb[13] + z[0];
z[17] = abb[4] + z[0] + z[17];
z[17] = abb[28] * z[17];
z[17] = -z[17] + z[37];
z[3] = -abb[15] + z[3] + -z[10] + z[22];
z[22] = abb[2] + abb[14];
z[3] = -abb[5] + abb[6] + -12 * abb[10] + 2 * z[3] + z[8] + 8 * z[22];
z[3] = prod_pow(abb[26], 2) * z[3];
z[8] = -abb[43] * z[29];
z[2] = 2 * z[2] + z[3] + z[5] + z[7] + z[8] + z[11] + z[12] + -6 * z[17] + z[18];
z[3] = -abb[23] + abb[26];
z[5] = abb[5] + z[43];
z[3] = z[3] * z[5];
z[5] = -abb[1] + z[44] + z[48];
z[5] = abb[19] * z[5];
z[5] = z[3] + z[5] + -z[45];
z[7] = abb[21] * z[49];
z[5] = 2 * z[5] + z[7];
z[5] = abb[21] * z[5];
z[7] = z[10] + z[41];
z[1] = -z[1] + z[7] + z[20] + -3 * z[23];
z[1] = abb[27] * z[1];
z[8] = abb[34] * z[16];
z[6] = z[6] + -z[7] + -z[28];
z[6] = abb[31] * z[6];
z[7] = abb[17] * abb[36];
z[1] = -z[1] + -z[5] + -z[6] + z[7] + z[8];
z[5] = abb[3] + -z[13] + z[34];
z[5] = 7 * abb[5] + 2 * z[5] + z[36];
z[6] = -abb[0] + abb[1];
z[7] = abb[4] + -abb[7];
z[5] = abb[2] + -4 * abb[10] + (T(1) / T(3)) * z[5] + (T(-13) / T(3)) * z[6] + 2 * z[7] + z[33];
z[5] = prod_pow(m1_set::bc<T>[0], 2) * z[5];
z[0] = z[0] + z[14];
z[0] = abb[20] * z[0];
z[6] = z[15] + -z[32];
z[7] = abb[24] * z[6];
z[7] = z[0] + 2 * z[7];
z[7] = abb[20] * z[7];
z[1] = abb[44] + -16 * z[1] + 8 * z[2] + 4 * z[5] + 24 * z[7];
z[2] = -z[4] + z[33];
z[4] = -z[2] + -z[9] + -z[21] + 2 * z[25];
z[4] = abb[25] * z[4];
z[5] = z[19] + z[27];
z[2] = 8 * abb[0] + z[2] + z[5] + -2 * z[24] + -z[31];
z[2] = abb[19] * z[2];
z[5] = z[5] + z[30] + z[31] + -z[40];
z[5] = abb[22] * z[5];
z[0] = -3 * z[0] + z[2] + -2 * z[3] + z[4] + z[5] + -z[47];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = -abb[37] * z[35];
z[3] = -abb[39] * z[26];
z[4] = abb[38] * z[6];
z[2] = z[2] + z[3] + z[4];
z[0] = z[0] + 3 * z[2];
z[0] = abb[40] + 16 * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_3_176_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_176_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_3_176_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((v[1] + v[2] + -v[3] + -v[4]) * (-56 + -28 * v[0] + 27 * v[1] + -v[2] + -27 * v[3] + v[4] + 8 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 28 * v[5])) / prod_pow(tend, 2);
c[1] = (8 * (-3 + 4 * m1_set::bc<T>[1]) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[2];
z[0] = abb[21] + -abb[24];
z[0] = 2 * z[0];
z[1] = -abb[26] + -z[0];
z[1] = abb[26] * z[1];
z[0] = abb[23] + z[0];
z[0] = abb[23] * z[0];
z[0] = z[0] + z[1];
z[0] = 3 * abb[31] + 2 * z[0];
return 16 * abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_176_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (8 * m1_set::bc<T>[0] * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (32 * m1_set::bc<T>[0] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = -abb[23] + abb[26];
return 64 * abb[11] * m1_set::bc<T>[0] * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_176_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-81.38735113289923039824140361415409872186364832624223321233762375"),stof<T>("201.43905565506692712566916940894645015861396124229108664963793791")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,45> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W20(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W86(k,dl), dlog_W87(k,dl), dlog_W130(k,dv), dlog_W135(k,dv), f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_20(k), f_2_21(k), f_2_29_im(k), f_2_2_im(k), f_2_9_im(k), f_2_10_im(k), T{0}, f_2_2_re(k), f_2_9_re(k), f_2_10_re(k), T{0}};
abb[40] = SpDLog_f_3_176_W_20_Im(t, path, abb);
abb[44] = SpDLog_f_3_176_W_20_Re(t, path, abb);

                    
            return f_3_176_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_176_DLogXconstant_part(base_point<T>, kend);
	value += f_3_176_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_176_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_176_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_176_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_176_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_176_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_176_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
