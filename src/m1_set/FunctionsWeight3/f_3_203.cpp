/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_203.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_203_abbreviated (const std::array<T,30>& abb) {
T z[20];
z[0] = 2 * abb[12];
z[1] = -abb[14] + abb[15];
z[2] = 2 * abb[13] + -z[0] + -z[1];
z[3] = abb[2] + abb[4];
z[2] = -z[2] * z[3];
z[4] = abb[6] * abb[13];
z[5] = abb[9] + abb[12];
z[6] = abb[11] + -z[5];
z[6] = abb[6] * z[6];
z[6] = z[4] + z[6];
z[2] = z[2] + 2 * z[6];
z[2] = m1_set::bc<T>[0] * z[2];
z[6] = abb[1] + abb[3];
z[7] = -2 * z[6];
z[8] = abb[9] + -abb[15];
z[9] = abb[10] + -abb[12] + z[8];
z[9] = m1_set::bc<T>[0] * z[9];
z[9] = abb[22] + z[9];
z[7] = z[7] * z[9];
z[9] = 2 * abb[20];
z[10] = -abb[6] + -z[6];
z[10] = z[9] * z[10];
z[11] = 2 * abb[11];
z[12] = z[1] + z[11];
z[13] = -2 * abb[9] + z[12];
z[13] = m1_set::bc<T>[0] * z[13];
z[9] = -z[9] + z[13];
z[9] = abb[5] * z[9];
z[3] = -abb[6] + z[3] + z[6];
z[3] = abb[21] * z[3];
z[13] = -abb[8] * abb[23];
z[14] = 2 * abb[10];
z[15] = abb[14] + abb[15] + -z[14];
z[15] = m1_set::bc<T>[0] * z[15];
z[15] = -2 * abb[22] + z[15];
z[15] = abb[0] * z[15];
z[2] = -abb[29] + z[2] + 2 * z[3] + z[7] + z[9] + z[10] + z[13] + z[15];
z[3] = -abb[10] + abb[11];
z[0] = abb[13] + -z[0] + z[3] + z[8];
z[0] = abb[13] * z[0];
z[7] = -abb[9] + abb[12] + 2 * abb[15] + -z[14];
z[7] = abb[9] * z[7];
z[8] = abb[14] + z[3];
z[9] = -abb[12] + z[8];
z[9] = abb[12] * z[9];
z[10] = abb[11] * abb[14];
z[8] = abb[15] * z[8];
z[13] = 2 * abb[26];
z[15] = 2 * abb[17] + -z[13];
z[16] = abb[10] * abb[14];
z[17] = 2 * abb[25];
z[18] = prod_pow(m1_set::bc<T>[0], 2);
z[0] = 2 * abb[27] + z[0] + z[7] + -z[8] + -z[9] + z[10] + z[15] + z[16] + z[17] + z[18];
z[0] = z[0] * z[6];
z[6] = -abb[9] + -z[1] + z[14];
z[6] = abb[9] * z[6];
z[6] = -2 * abb[16] + z[6];
z[7] = -abb[14] + z[11];
z[7] = abb[15] * z[7];
z[7] = -2 * abb[18] + z[7];
z[8] = (T(1) / T(3)) * z[18];
z[9] = z[7] + -z[8];
z[10] = prod_pow(abb[11], 2);
z[14] = prod_pow(abb[10], 2);
z[19] = abb[12] + -2 * z[3];
z[19] = abb[12] * z[19];
z[3] = -abb[12] + z[3];
z[3] = abb[13] + 2 * z[3];
z[3] = abb[13] * z[3];
z[3] = z[3] + -z[6] + -z[9] + z[10] + z[14] + z[15] + z[19];
z[3] = abb[2] * z[3];
z[14] = -abb[12] + z[11];
z[14] = abb[12] * z[14];
z[10] = -z[10] + z[13] + z[14];
z[5] = z[5] + -z[11];
z[11] = -abb[9] * z[5];
z[8] = z[8] + z[10] + z[11] + z[17];
z[8] = abb[6] * z[8];
z[11] = (T(2) / T(3)) * z[18];
z[13] = abb[27] + z[16];
z[14] = -abb[14] * abb[15];
z[6] = -z[6] + z[11] + 2 * z[13] + z[14];
z[6] = abb[0] * z[6];
z[13] = -abb[13] * z[5];
z[1] = abb[12] + z[1];
z[1] = abb[9] * z[1];
z[1] = z[1] + -z[9] + -z[10] + z[13];
z[1] = abb[4] * z[1];
z[9] = -abb[9] + z[12];
z[9] = abb[9] * z[9];
z[7] = -z[7] + z[9] + z[11] + z[17];
z[7] = abb[5] * z[7];
z[4] = z[4] * z[5];
z[5] = abb[8] * abb[28];
z[9] = -abb[7] * abb[19];
z[0] = -abb[24] + z[0] + z[1] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_3_203_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_203_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_203_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-5.1156137591021159324521833062370106254853630667671669020404408748"),stof<T>("8.1590970221738957400031467760741852677013306533114953925001605429")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({198});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,30> abb = {dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_9(k), f_2_3(k), f_2_5(k), f_2_13(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_9_im(k), f_2_25_im(k), T{0}, f_2_4_re(k), f_2_7_re(k), f_2_9_re(k), f_2_25_re(k), T{0}};
{
auto c = dlog_W189(k,dv) * f_2_30(k);
abb[29] = c.real();
abb[24] = c.imag();
SpDLog_Sigma5<T,198,188>(k, dv, abb[29], abb[24], f_2_30_series_coefficients<T>);
}

                    
            return f_3_203_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_203_DLogXconstant_part(base_point<T>, kend);
	value += f_3_203_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_203_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_203_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_203_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_203_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_203_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_203_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
