/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_157.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_157_abbreviated (const std::array<T,48>& abb) {
T z[48];
z[0] = 3 * abb[6];
z[1] = 2 * abb[2];
z[2] = z[0] + z[1];
z[3] = 3 * abb[13];
z[4] = 3 * abb[3];
z[5] = z[3] + -z[4];
z[6] = 4 * abb[9];
z[7] = abb[5] + -abb[7];
z[8] = z[2] + z[5] + -z[6] + z[7];
z[9] = -abb[46] * z[8];
z[10] = 2 * abb[4];
z[11] = z[4] + z[10];
z[12] = abb[2] + -abb[7];
z[13] = -abb[10] + z[12];
z[14] = 3 * abb[11];
z[13] = abb[5] + z[0] + -z[11] + 2 * z[13] + z[14];
z[15] = abb[30] * z[13];
z[16] = 2 * abb[10];
z[6] = z[6] + -z[16];
z[17] = -abb[7] + -z[3] + z[6] + z[14];
z[18] = abb[41] * z[17];
z[19] = 3 * abb[7];
z[20] = 4 * abb[10] + -z[19];
z[1] = abb[6] + z[1];
z[21] = 5 * abb[5] + z[1];
z[14] = -z[14] + z[20] + z[21];
z[22] = abb[42] * z[14];
z[23] = 2 * abb[8];
z[24] = 4 * abb[1] + -abb[12] + -z[5] + z[19] + -z[23];
z[25] = abb[40] * z[24];
z[26] = 3 * abb[28];
z[27] = abb[31] + z[26];
z[28] = 3 * abb[26];
z[29] = 3 * abb[29];
z[30] = -abb[41] + abb[42] + abb[46] + -z[27] + z[28] + -z[29];
z[30] = abb[4] * z[30];
z[31] = abb[1] + abb[7] + -abb[9];
z[32] = 7 * abb[2] + abb[6];
z[3] = -z[3] + 3 * z[31] + z[32];
z[31] = -abb[4] + z[3];
z[31] = abb[45] * z[31];
z[33] = 4 * abb[8];
z[21] = 7 * abb[7] + -abb[12] + z[4] + -z[21] + -z[33];
z[34] = abb[44] * z[21];
z[35] = abb[7] * abb[27];
z[36] = abb[26] + abb[27];
z[36] = abb[0] * z[36];
z[35] = z[35] + -z[36];
z[36] = abb[5] + abb[6];
z[37] = -z[23] + z[36];
z[38] = abb[31] * z[37];
z[39] = -abb[2] + abb[6];
z[26] = -z[26] * z[39];
z[39] = abb[3] + -abb[11];
z[40] = -abb[12] + z[39];
z[28] = z[28] * z[40];
z[39] = abb[2] + -abb[5] + -z[39];
z[29] = z[29] * z[39];
z[39] = abb[3] + -abb[13];
z[40] = -abb[2] + z[7] + -z[39];
z[40] = 3 * z[40];
z[41] = -abb[43] * z[40];
z[27] = abb[12] * z[27];
z[42] = -abb[28] * z[4];
z[43] = abb[14] + abb[16];
z[43] = abb[15] + abb[17] + -2 * z[43];
z[44] = -abb[39] * z[43];
z[9] = z[9] + z[15] + z[18] + z[22] + z[25] + z[26] + z[27] + z[28] + z[29] + z[30] + z[31] + -z[34] + -6 * z[35] + z[38] + z[41] + z[42] + z[44];
z[15] = 5 * abb[6];
z[18] = 7 * abb[5];
z[22] = abb[4] + z[4] + -4 * z[12] + -z[15] + -z[18];
z[22] = abb[19] * z[22];
z[25] = -abb[20] * z[21];
z[26] = 2 * abb[12];
z[27] = z[23] + z[26];
z[28] = -abb[5] + z[27];
z[29] = z[2] + z[19] + -z[28];
z[29] = abb[23] * z[29];
z[28] = 3 * abb[2] + -4 * abb[6] + -z[11] + z[28];
z[28] = abb[22] * z[28];
z[30] = -abb[4] + abb[12] + z[37];
z[30] = abb[18] * z[30];
z[31] = abb[3] + abb[4];
z[34] = -abb[2] + z[31];
z[35] = abb[6] + -abb[12] + z[34];
z[35] = abb[25] * z[35];
z[22] = z[22] + z[25] + z[28] + z[29] + z[30] + 3 * z[35];
z[18] = -abb[2] + -abb[4] + -8 * abb[6] + -z[18] + z[19] + z[27];
z[18] = abb[24] * z[18];
z[18] = z[18] + 2 * z[22];
z[18] = abb[24] * z[18];
z[13] = abb[19] * z[13];
z[8] = -abb[4] + z[8];
z[22] = 2 * abb[0];
z[25] = 2 * abb[1];
z[28] = -z[8] + -z[22] + -z[25] + z[26];
z[28] = abb[22] * z[28];
z[29] = 2 * abb[9];
z[30] = abb[2] + abb[7];
z[35] = abb[6] + -z[29] + z[30];
z[37] = -abb[12] + z[25] + z[35];
z[38] = abb[25] * z[37];
z[38] = 2 * z[38];
z[41] = abb[8] + abb[10];
z[42] = 2 * abb[6];
z[44] = z[41] + -z[42];
z[45] = 3 * abb[0] + abb[2] + -abb[12] + z[7] + -z[44];
z[45] = abb[20] * z[45];
z[46] = -abb[1] + abb[12];
z[47] = abb[0] + -z[46];
z[47] = abb[23] * z[47];
z[47] = -z[45] + z[47];
z[13] = z[13] + z[28] + z[38] + 2 * z[47];
z[19] = -z[19] + z[22] + -z[25];
z[10] = z[10] + z[19] + z[36];
z[22] = abb[18] * z[10];
z[13] = 2 * z[13] + z[22];
z[13] = abb[18] * z[13];
z[22] = abb[4] + z[14];
z[22] = abb[20] * z[22];
z[28] = abb[7] + -z[16] + z[36];
z[28] = abb[23] * z[28];
z[22] = z[22] + z[28];
z[11] = z[11] + -z[16];
z[15] = -10 * abb[5] + 6 * abb[11] + -z[11] + -z[15] + -z[30];
z[15] = abb[19] * z[15];
z[15] = z[15] + 2 * z[22];
z[15] = abb[19] * z[15];
z[22] = -abb[0] + abb[1] + z[35];
z[22] = abb[23] * z[22];
z[22] = z[22] + z[45];
z[28] = 2 * abb[7];
z[30] = abb[2] + z[28];
z[16] = -abb[4] + -4 * abb[5] + -z[0] + z[16] + z[30];
z[16] = abb[19] * z[16];
z[16] = z[16] + 2 * z[22] + z[38];
z[10] = abb[22] * z[10];
z[10] = z[10] + 2 * z[16];
z[10] = abb[22] * z[10];
z[2] = abb[5] + z[2] + z[19];
z[2] = abb[23] * z[2];
z[2] = z[2] + -4 * z[45];
z[2] = abb[23] * z[2];
z[16] = 4 * z[37];
z[19] = 3 * abb[4];
z[5] = -z[5] + z[19];
z[22] = -z[5] + -z[16];
z[22] = abb[25] * z[22];
z[16] = -abb[23] * z[16];
z[16] = z[16] + z[22];
z[16] = abb[25] * z[16];
z[22] = z[30] + -5 * z[41];
z[4] = -abb[4] + z[4];
z[22] = 18 * abb[0] + -abb[5] + 7 * abb[6] + -4 * abb[12] + z[4] + 2 * z[22];
z[22] = prod_pow(abb[20], 2) * z[22];
z[30] = abb[5] + -abb[11] + z[34];
z[30] = abb[19] * z[30];
z[31] = abb[13] + -z[31];
z[31] = abb[21] * z[31];
z[30] = 2 * z[30] + z[31];
z[30] = abb[21] * z[30];
z[2] = z[2] + 2 * z[9] + z[10] + z[13] + z[15] + z[16] + z[18] + z[22] + 3 * z[30];
z[9] = abb[7] * (T(-1) / T(2)) + z[29];
z[9] = -27 * abb[2] + abb[6] * (T(-139) / T(6)) + abb[0] * (T(-52) / T(3)) + abb[1] * (T(-1) / T(3)) + abb[5] * (T(23) / T(6)) + (T(47) / T(3)) * z[9] + 4 * z[39] + (T(32) / T(3)) * z[41];
z[9] = prod_pow(m1_set::bc<T>[0], 2) * z[9];
z[2] = 2 * z[2] + z[9];
z[9] = 4 * abb[0];
z[1] = -z[1] + z[6] + z[7] + z[9] + -z[23] + -z[25];
z[1] = abb[23] * z[1];
z[6] = z[20] + z[33];
z[7] = 3 * abb[5];
z[4] = -12 * abb[0] + -z[0] + -z[4] + 2 * z[6] + z[7] + z[26];
z[4] = abb[20] * z[4];
z[6] = -5 * abb[2] + -z[7] + z[11] + z[28] + -z[42];
z[6] = abb[19] * z[6];
z[7] = abb[2] + -abb[9];
z[0] = z[0] + 2 * z[7] + -z[41] + -z[46];
z[7] = 2 * abb[5] + z[9];
z[0] = 2 * z[0] + -z[5] + z[7];
z[0] = abb[18] * z[0];
z[9] = abb[3] + -abb[5];
z[9] = 4 * abb[7] + 6 * z[9] + z[19] + -z[27] + -z[32];
z[9] = abb[24] * z[9];
z[10] = abb[1] + -z[12] + z[44];
z[5] = -z[5] + -z[7] + 2 * z[10];
z[5] = abb[22] * z[5];
z[0] = z[0] + z[1] + z[4] + z[5] + z[6] + z[9] + -z[38];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = -abb[38] * z[8];
z[4] = abb[34] * z[14];
z[3] = abb[37] * z[3];
z[5] = -abb[4] + z[17];
z[5] = abb[33] * z[5];
z[6] = abb[32] * z[24];
z[7] = -abb[36] * z[21];
z[8] = -abb[35] * z[40];
z[9] = abb[47] * z[43];
z[10] = abb[34] + -abb[37];
z[10] = abb[4] * z[10];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10];
z[0] = 4 * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_3_157_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_157_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_157_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-7.51248222655846273724437061564510622645001589519470063777217179"),stof<T>("675.8712729906897502283737695621190500758504689017568104844749918")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,48> abb = {dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W24(k,dl), dlog_W25(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W33(k,dl), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_16(k), f_2_17(k), f_2_18(k), f_2_2_im(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_9_im(k), f_2_12_im(k), f_2_22_im(k), f_2_27_im(k), f_2_2_re(k), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_9_re(k), f_2_12_re(k), f_2_22_re(k), f_2_27_re(k)};

                    
            return f_3_157_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_157_DLogXconstant_part(base_point<T>, kend);
	value += f_3_157_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_157_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_157_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_157_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_157_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_157_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_157_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
