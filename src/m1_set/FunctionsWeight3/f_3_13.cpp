/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_13.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_13_abbreviated (const std::array<T,34>& abb) {
T z[33];
z[0] = abb[17] + -abb[21];
z[1] = 2 * abb[19];
z[2] = -4 * abb[22] + -z[0] + z[1];
z[2] = abb[6] * z[2];
z[3] = abb[19] * (T(3) / T(2));
z[4] = 2 * abb[22];
z[5] = -abb[17] + z[3] + -z[4];
z[5] = abb[4] * z[5];
z[2] = z[2] + z[5];
z[5] = -abb[4] + abb[10];
z[5] = (T(3) / T(2)) * z[5];
z[6] = -abb[3] + abb[8];
z[7] = abb[7] + z[6];
z[8] = -z[5] + z[7];
z[8] = abb[6] + (T(1) / T(2)) * z[8];
z[8] = abb[18] * z[8];
z[9] = -abb[10] * z[3];
z[10] = -abb[17] + abb[19];
z[11] = abb[3] * z[10];
z[12] = abb[21] + abb[22];
z[13] = abb[0] * z[12];
z[14] = -abb[8] * abb[19];
z[8] = -z[2] + z[8] + z[9] + z[11] + z[13] + z[14];
z[8] = abb[18] * z[8];
z[9] = prod_pow(abb[22], 2);
z[11] = -abb[21] * z[12];
z[11] = 5 * z[9] + z[11];
z[13] = abb[19] * (T(1) / T(2));
z[14] = abb[21] + -abb[22];
z[15] = -abb[17] + z[14];
z[16] = -z[13] + z[15];
z[16] = abb[19] * z[16];
z[17] = abb[31] + abb[33];
z[18] = abb[17] + (T(-1) / T(2)) * z[14];
z[18] = abb[17] * z[18];
z[11] = (T(1) / T(2)) * z[11] + z[16] + -z[17] + z[18];
z[11] = abb[6] * z[11];
z[16] = abb[17] + z[14];
z[18] = abb[12] * z[16];
z[19] = -abb[0] * z[16];
z[20] = abb[3] + -abb[7];
z[21] = abb[18] * z[20];
z[20] = -abb[0] + -abb[12] + -z[20];
z[20] = abb[8] + (T(1) / T(2)) * z[20];
z[20] = abb[20] * z[20];
z[19] = z[18] + z[19] + z[20] + z[21];
z[19] = abb[20] * z[19];
z[20] = abb[21] + -3 * abb[22];
z[20] = abb[21] * z[20];
z[21] = 3 * abb[21] + abb[22];
z[21] = abb[17] * z[21];
z[20] = -z[9] + z[20] + z[21];
z[21] = prod_pow(m1_set::bc<T>[0], 2);
z[22] = abb[17] + -abb[22];
z[22] = abb[18] * z[22];
z[23] = abb[20] * (T(1) / T(2)) + -z[16];
z[23] = abb[20] * z[23];
z[20] = -abb[23] + abb[24] + -abb[33] + (T(1) / T(2)) * z[20] + (T(-1) / T(6)) * z[21] + z[22] + z[23];
z[20] = abb[2] * z[20];
z[22] = abb[21] * abb[22];
z[22] = -z[9] + z[22];
z[23] = prod_pow(abb[19], 2);
z[12] = abb[17] * (T(-1) / T(2)) + z[12];
z[12] = abb[17] * z[12];
z[12] = z[12] + -z[22] + (T(-3) / T(4)) * z[23];
z[12] = abb[4] * z[12];
z[3] = -z[3] + -z[15];
z[3] = abb[19] * z[3];
z[14] = abb[21] * z[14];
z[24] = 2 * abb[17];
z[25] = abb[21] * z[24];
z[3] = -abb[31] + z[3] + z[14] + z[25];
z[3] = abb[0] * z[3];
z[0] = -z[0] * z[4];
z[25] = abb[19] + -2 * z[15];
z[25] = abb[19] * z[25];
z[0] = z[0] + -3 * z[9] + z[25];
z[0] = abb[9] * z[0];
z[25] = abb[4] + abb[10];
z[26] = 2 * abb[6];
z[6] = z[6] + (T(3) / T(2)) * z[25] + z[26];
z[25] = -abb[32] * z[6];
z[27] = -abb[17] * z[15];
z[22] = -z[22] + z[27];
z[13] = z[13] + z[15];
z[13] = abb[19] * z[13];
z[13] = abb[23] + abb[24] + z[13] + (T(1) / T(2)) * z[22];
z[13] = abb[11] * z[13];
z[15] = abb[19] + -abb[22];
z[22] = abb[18] * z[15];
z[22] = abb[24] + abb[32] + z[22];
z[9] = z[9] + 2 * z[22] + -z[23];
z[9] = abb[5] * z[9];
z[22] = -3 * abb[1] + abb[10] * (T(9) / T(4));
z[22] = z[22] * z[23];
z[27] = prod_pow(abb[17], 2);
z[28] = -z[23] + z[27];
z[29] = abb[25] + -z[28];
z[29] = abb[7] * z[29];
z[28] = -abb[25] + (T(1) / T(2)) * z[28];
z[28] = abb[3] * z[28];
z[30] = abb[3] + -abb[4];
z[31] = 2 * abb[7];
z[32] = -abb[0] + z[30] + -z[31];
z[32] = abb[23] * z[32];
z[23] = 2 * abb[31] + (T(1) / T(2)) * z[23] + -z[27];
z[23] = abb[8] * z[23];
z[16] = -abb[17] * z[16];
z[14] = -z[14] + z[16];
z[14] = (T(1) / T(2)) * z[14] + z[17];
z[14] = abb[12] * z[14];
z[16] = 13 * abb[1] + -5 * abb[4];
z[16] = -2 * abb[10] + abb[0] * (T(-11) / T(6)) + abb[6] * (T(-7) / T(6)) + abb[8] * (T(1) / T(3)) + abb[12] * (T(1) / T(6)) + (T(1) / T(4)) * z[16];
z[16] = z[16] * z[21];
z[17] = -abb[31] * z[30];
z[7] = -abb[0] + z[7];
z[7] = abb[26] * z[7];
z[21] = -abb[0] + -abb[4] + -abb[9];
z[21] = -3 * abb[6] + 2 * z[21];
z[21] = abb[24] * z[21];
z[27] = -3 * abb[13] + -abb[14] + abb[15] + -abb[16];
z[27] = abb[27] * z[27];
z[0] = z[0] + z[3] + z[7] + z[8] + z[9] + z[11] + z[12] + z[13] + z[14] + z[16] + z[17] + z[19] + z[20] + z[21] + z[22] + z[23] + z[25] + (T(1) / T(2)) * z[27] + z[28] + z[29] + z[32];
z[3] = abb[3] + -z[31];
z[3] = z[3] * z[10];
z[5] = -2 * abb[0] + abb[8] + z[5] + -z[26];
z[5] = abb[18] * z[5];
z[7] = 6 * abb[1] + abb[10] * (T(-9) / T(2));
z[7] = abb[19] * z[7];
z[1] = -3 * abb[17] + -2 * abb[21] + z[1];
z[1] = abb[0] * z[1];
z[8] = abb[9] * z[15];
z[9] = -abb[19] + z[24];
z[9] = abb[8] * z[9];
z[1] = z[1] + z[2] + z[3] + z[5] + z[7] + -4 * z[8] + z[9] + z[18];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[29] * z[6];
z[3] = -abb[6] + abb[12];
z[5] = -abb[0] + 2 * abb[8] + z[3] + -z[30];
z[5] = abb[28] * z[5];
z[6] = abb[20] * m1_set::bc<T>[0];
z[7] = abb[0] + -abb[12];
z[7] = z[6] * z[7];
z[4] = -abb[21] + z[4] + -z[24];
z[4] = m1_set::bc<T>[0] * z[4];
z[4] = -abb[30] + z[4] + z[6];
z[4] = abb[2] * z[4];
z[3] = abb[30] * z[3];
z[6] = m1_set::bc<T>[0] * z[15];
z[6] = abb[29] + z[6];
z[6] = abb[5] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + 2 * z[6] + z[7];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_13_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_13_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_13_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-9.6539870796848114599008910212301228695003103703982736841311235467"),stof<T>("-0.6642569533075198199319515826006161342830392903264259500639777148")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,34> abb = {dl[0], dl[1], dl[2], dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W21(k,dl), dlog_W27(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k)};

                    
            return f_3_13_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_13_DLogXconstant_part(base_point<T>, kend);
	value += f_3_13_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_13_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_13_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_13_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_13_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_13_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_13_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
