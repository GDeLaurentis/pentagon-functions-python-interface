/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_192.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_192_abbreviated (const std::array<T,46>& abb) {
T z[45];
z[0] = 2 * abb[20];
z[1] = 2 * abb[23];
z[2] = abb[18] + -abb[24];
z[3] = -z[0] + z[1] + -z[2];
z[3] = abb[4] * z[3];
z[4] = abb[20] + -abb[23];
z[5] = -z[2] + -z[4];
z[5] = abb[6] * z[5];
z[6] = abb[0] + abb[5];
z[6] = z[4] * z[6];
z[7] = abb[21] + -abb[22];
z[8] = -abb[24] + abb[25];
z[9] = -z[7] + z[8];
z[9] = abb[7] * z[9];
z[10] = abb[22] + -abb[23];
z[11] = 3 * abb[18];
z[12] = z[10] + z[11];
z[13] = 3 * abb[21];
z[14] = abb[20] + abb[24] + -z[12] + z[13];
z[14] = abb[3] * z[14];
z[15] = 2 * abb[8];
z[16] = abb[20] + abb[21];
z[17] = abb[18] + -z[16];
z[17] = z[15] * z[17];
z[7] = z[7] + z[8];
z[7] = abb[2] * z[7];
z[18] = abb[12] * z[2];
z[19] = -abb[11] * z[8];
z[20] = abb[0] + -abb[6] + abb[12];
z[21] = 2 * abb[4] + abb[8] + -z[20];
z[21] = abb[19] * z[21];
z[3] = z[3] + z[5] + z[6] + z[7] + z[9] + z[14] + z[17] + z[18] + z[19] + z[21];
z[5] = 2 * abb[19];
z[3] = z[3] * z[5];
z[6] = abb[23] + abb[24];
z[7] = 2 * abb[24];
z[6] = z[6] * z[7];
z[9] = -abb[25] + z[7];
z[14] = abb[25] * z[9];
z[6] = -z[6] + z[14];
z[14] = 2 * abb[40];
z[17] = prod_pow(abb[20], 2);
z[19] = z[14] + -z[17];
z[21] = 2 * abb[21];
z[22] = abb[18] + z[10];
z[23] = abb[21] + -z[22];
z[23] = z[21] * z[23];
z[24] = prod_pow(abb[23], 2);
z[25] = prod_pow(abb[18], 2);
z[26] = prod_pow(abb[22], 2);
z[23] = -z[6] + z[19] + z[23] + -z[24] + 2 * z[25] + z[26];
z[23] = abb[0] * z[23];
z[25] = -3 * abb[23] + -z[7];
z[25] = abb[24] * z[25];
z[26] = 3 * abb[24];
z[27] = -abb[18] + -abb[23] + z[26];
z[27] = abb[25] * z[27];
z[25] = 2 * abb[26] + 3 * abb[28] + z[24] + z[25] + z[27];
z[26] = 4 * abb[18] + -z[26];
z[27] = 4 * abb[21];
z[28] = z[10] + z[26] + -z[27];
z[28] = z[21] * z[28];
z[29] = 4 * abb[30];
z[30] = -abb[23] + abb[25];
z[31] = abb[22] * z[30];
z[31] = 2 * z[31];
z[32] = abb[23] + z[7];
z[33] = 2 * z[32];
z[34] = -abb[18] + z[33];
z[34] = abb[18] * z[34];
z[25] = z[19] + 2 * z[25] + z[28] + -z[29] + z[31] + z[34];
z[25] = abb[3] * z[25];
z[26] = abb[19] + -abb[22] + -abb[25] + z[1] + z[13] + -z[26];
z[26] = abb[19] * z[26];
z[28] = -z[11] + z[33];
z[28] = abb[18] * z[28];
z[33] = 2 * abb[18];
z[34] = abb[25] + -z[10] + -z[33];
z[34] = abb[22] * z[34];
z[32] = -abb[24] * z[32];
z[35] = -abb[23] + abb[24];
z[36] = abb[25] * z[35];
z[37] = 12 * abb[18] + -10 * abb[21] + -7 * abb[24] + 5 * z[10];
z[37] = abb[21] * z[37];
z[26] = -2 * abb[30] + 4 * abb[41] + z[26] + z[28] + z[32] + z[34] + z[36] + z[37];
z[28] = 2 * abb[1];
z[26] = z[26] * z[28];
z[32] = abb[24] + z[10];
z[34] = -abb[21] + z[32];
z[34] = z[21] * z[34];
z[36] = 3 * z[24];
z[37] = abb[23] * abb[24];
z[38] = abb[23] + -z[7];
z[38] = abb[25] + 2 * z[38];
z[38] = abb[25] * z[38];
z[31] = -6 * abb[28] + -z[31] + z[34] + -z[36] + 4 * z[37] + z[38];
z[31] = abb[2] * z[31];
z[34] = prod_pow(abb[24], 2);
z[38] = -abb[18] * abb[24];
z[39] = abb[18] + -abb[25];
z[39] = abb[25] * z[39];
z[34] = z[34] + z[38] + z[39];
z[34] = abb[11] * z[34];
z[38] = abb[12] * abb[40];
z[39] = -abb[15] + abb[16];
z[40] = abb[43] * z[39];
z[41] = 2 * abb[7];
z[42] = -abb[0] + z[41];
z[43] = -abb[10] + -abb[14] + z[42];
z[44] = abb[41] * z[43];
z[34] = abb[45] + z[34] + z[38] + -z[40] + z[44];
z[35] = -abb[18] + 2 * z[35];
z[35] = abb[18] * z[35];
z[14] = z[6] + -z[14] + z[35] + z[36];
z[14] = abb[6] * z[14];
z[35] = -abb[24] + z[30];
z[36] = -abb[25] * z[35];
z[10] = abb[25] + z[10];
z[38] = -abb[22] * z[10];
z[32] = abb[21] * z[32];
z[32] = z[32] + z[36] + -z[37] + z[38];
z[32] = z[32] * z[41];
z[36] = -4 * abb[24] + -z[11];
z[36] = abb[18] * z[36];
z[37] = 7 * abb[21] + -4 * z[2];
z[37] = abb[21] * z[37];
z[17] = -4 * abb[40] + 2 * z[17] + z[29] + z[36] + z[37];
z[17] = abb[8] * z[17];
z[29] = -abb[18] + -z[1];
z[29] = abb[18] * z[29];
z[36] = 2 * z[2];
z[37] = abb[20] * z[36];
z[6] = -6 * abb[40] + -z[6] + z[29] + z[37];
z[6] = abb[4] * z[6];
z[29] = abb[18] + -abb[23];
z[37] = abb[18] * z[29];
z[38] = abb[22] * z[22];
z[37] = z[37] + z[38];
z[37] = abb[14] * z[37];
z[38] = abb[14] * z[22];
z[36] = abb[6] * z[36];
z[36] = z[36] + -z[38];
z[40] = 2 * abb[6] + -abb[14];
z[40] = abb[21] * z[40];
z[36] = 2 * z[36] + -z[40];
z[36] = abb[21] * z[36];
z[44] = abb[6] + 2 * abb[12];
z[44] = abb[20] * z[44];
z[44] = -2 * z[18] + z[44];
z[44] = abb[20] * z[44];
z[13] = z[13] + -2 * z[22];
z[13] = abb[21] * z[13];
z[22] = -abb[22] + 2 * z[29];
z[22] = abb[22] * z[22];
z[13] = z[13] + z[22];
z[13] = abb[10] * z[13];
z[19] = z[19] + z[24];
z[19] = abb[5] * z[19];
z[22] = abb[17] * abb[31];
z[3] = z[3] + z[6] + z[13] + z[14] + z[17] + z[19] + z[22] + z[23] + z[25] + z[26] + z[31] + z[32] + 2 * z[34] + z[36] + z[37] + z[44];
z[6] = abb[4] + abb[11];
z[13] = abb[2] + -abb[6];
z[14] = -3 * abb[3] + z[6] + z[13] + -z[42];
z[14] = 8 * z[14];
z[17] = -abb[42] * z[14];
z[19] = abb[0] + -abb[10];
z[22] = -abb[6] + z[41];
z[23] = 4 * abb[3];
z[19] = abb[11] + -abb[12] + -abb[14] + -4 * z[19] + z[22] + z[23];
z[19] = 3 * abb[8] + abb[1] * (T(13) / T(3)) + (T(2) / T(3)) * z[19];
z[24] = prod_pow(m1_set::bc<T>[0], 2);
z[19] = z[19] * z[24];
z[13] = -z[13] + z[28];
z[13] = abb[29] * z[13];
z[25] = -abb[19] + z[21];
z[26] = abb[18] + -z[25];
z[26] = z[2] * z[26];
z[24] = -2 * abb[29] + (T(-2) / T(3)) * z[24] + z[26];
z[26] = 8 * abb[13];
z[24] = z[24] * z[26];
z[31] = -abb[6] + abb[14];
z[23] = -3 * abb[0] + 18 * abb[1] + abb[8] + -4 * abb[13] + z[23] + -2 * z[31];
z[23] = 4 * z[23];
z[31] = abb[39] * z[23];
z[32] = -abb[4] + abb[5];
z[32] = abb[27] * z[32];
z[3] = abb[44] + 4 * z[3] + 16 * z[13] + z[17] + 2 * z[19] + z[24] + z[31] + 8 * z[32];
z[0] = -z[0] + z[7] + z[11] + z[21];
z[0] = abb[8] * z[0];
z[1] = -z[1] + z[8] + z[33];
z[1] = abb[6] * z[1];
z[8] = -z[10] + z[16] + -z[33];
z[8] = abb[0] * z[8];
z[9] = abb[20] + -z[9] + -z[27] + z[29];
z[9] = abb[3] * z[9];
z[5] = -6 * abb[21] + z[5] + -z[7] + z[12];
z[5] = z[5] * z[28];
z[7] = z[10] * z[41];
z[6] = 2 * abb[3] + -abb[12] + z[6] + -z[22];
z[6] = abb[19] * z[6];
z[2] = abb[25] + -z[2];
z[2] = abb[11] * z[2];
z[10] = -abb[6] + -abb[12];
z[10] = abb[20] * z[10];
z[11] = abb[21] + abb[22] + -z[29];
z[11] = abb[10] * z[11];
z[12] = abb[18] + -abb[20] + -z[35];
z[12] = abb[4] * z[12];
z[13] = -abb[2] * z[30];
z[4] = abb[5] * z[4];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + z[18] + -z[38] + -z[40];
z[1] = abb[24] + z[25] + -z[33];
z[1] = z[1] * z[26];
z[0] = 8 * z[0] + z[1];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[3] + -3 * abb[4] + abb[5] + -z[15] + z[20];
z[1] = abb[33] * z[1];
z[2] = abb[34] * z[43];
z[4] = -abb[36] * z[39];
z[1] = abb[38] + z[1] + z[2] + z[4];
z[2] = -abb[35] * z[14];
z[4] = abb[32] * z[23];
z[5] = abb[1] * abb[34];
z[0] = abb[37] + z[0] + 8 * z[1] + z[2] + z[4] + 32 * z[5];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_3_192_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_192_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_3_192_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(14)) * (-v[3] + v[5]) * (7 * (4 * (-2 + 3 * m1_set::bc<T>[2]) * v[0] + -4 * m1_set::bc<T>[4] * (v[3] + v[5]) + m1_set::bc<T>[2] * (-4 * v[1] + 3 * v[3] + 9 * v[5])) + 2 * (-112 * m1_set::bc<T>[1] + 84 * m1_set::bc<T>[2] + -56 * m1_set::bc<T>[4] + 28 * v[1] + 28 * v[2] + 21 * v[3] + -8 * v[4] + 7 * (v[5] + -4 * m1_set::bc<T>[1] * (v[3] + v[5]))))) / prod_pow(tend, 2);
c[1] = (-4 * (4 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2] + 2 * m1_set::bc<T>[4]) * (-v[3] + v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[3];
z[0] = -abb[26] + 2 * abb[29] + abb[30];
z[1] = abb[18] + abb[22] + -abb[23];
z[2] = 5 * abb[21] + -2 * z[1];
z[2] = abb[21] * z[2];
z[1] = -2 * abb[21] + z[1];
z[1] = -abb[19] + 2 * z[1];
z[1] = abb[19] * z[1];
z[0] = 2 * z[0] + z[1] + z[2];
return 8 * abb[9] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_192_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (-2 * m1_set::bc<T>[0] * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (8 * m1_set::bc<T>[0] * (v[3] + -v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = -abb[19] + abb[21];
return 16 * abb[9] * m1_set::bc<T>[0] * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_192_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-55.599466336862840895947729581561863840716166685773996729621081368"),stof<T>("51.219747214480611570610758475011899739850558685514477065621386162")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 201});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,46> abb = {dl[0], dl[5], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W22(k,dl), dlog_W23(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W33(k,dl), dlog_W120(k,dv), dlog_W126(k,dv), dlog_W129(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_10_im(k), f_2_24_im(k), T{0}, T{0}, f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_10_re(k), f_2_24_re(k), T{0}, T{0}};
abb[37] = SpDLog_f_3_192_W_16_Im(t, path, abb);
abb[44] = SpDLog_f_3_192_W_16_Re(t, path, abb);
{
auto c = dlog_W181(k,dv) * f_2_33(k);
abb[45] = c.real();
abb[38] = c.imag();
SpDLog_Sigma5<T,201,180>(k, dv, abb[45], abb[38], f_2_33_series_coefficients<T>);
}

                    
            return f_3_192_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_192_DLogXconstant_part(base_point<T>, kend);
	value += f_3_192_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_192_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_192_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_192_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_192_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_192_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_192_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
