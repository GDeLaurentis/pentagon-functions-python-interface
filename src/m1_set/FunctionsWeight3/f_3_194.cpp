/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_194.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_194_abbreviated (const std::array<T,48>& abb) {
T z[41];
z[0] = 4 * abb[4];
z[1] = 2 * abb[12];
z[2] = z[0] + -z[1];
z[3] = 2 * abb[13];
z[4] = 2 * abb[2];
z[5] = z[3] + -z[4];
z[6] = abb[0] + abb[11];
z[7] = 2 * abb[7];
z[8] = 12 * abb[1] + z[2] + -z[5] + -z[6] + -z[7];
z[8] = abb[18] * z[8];
z[9] = 3 * abb[4];
z[10] = -z[7] + z[9];
z[11] = abb[6] + abb[8];
z[5] = -7 * abb[1] + z[5] + -z[10] + z[11];
z[5] = abb[24] * z[5];
z[12] = -5 * abb[1] + z[6] + -z[11];
z[13] = abb[22] + -abb[23];
z[12] = z[12] * z[13];
z[14] = z[1] * z[13];
z[15] = 3 * abb[1];
z[16] = abb[6] + -abb[8] + -4 * abb[12] + z[10] + z[15];
z[16] = abb[19] * z[16];
z[17] = abb[4] * z[13];
z[18] = -abb[18] + z[13];
z[19] = abb[14] * z[18];
z[5] = z[5] + z[8] + z[12] + z[14] + z[16] + -z[17] + z[19];
z[8] = 2 * abb[6];
z[12] = 2 * abb[0] + -20 * abb[1] + -8 * abb[4] + 7 * abb[7] + 3 * abb[11] + 10 * abb[12] + abb[14] + -z[4] + -z[8];
z[12] = abb[20] * z[12];
z[5] = 2 * z[5] + z[12];
z[5] = abb[20] * z[5];
z[12] = abb[2] + -abb[13];
z[16] = 4 * abb[1];
z[19] = z[12] + z[16];
z[20] = 2 * abb[4];
z[21] = -z[7] + z[19] + z[20];
z[21] = abb[24] * z[21];
z[10] = z[1] + -z[10] + -z[19];
z[10] = abb[19] * z[10];
z[19] = abb[22] + -abb[25];
z[19] = abb[4] * z[19];
z[22] = 2 * abb[1];
z[23] = abb[11] + -z[22];
z[23] = abb[23] * z[23];
z[24] = -abb[2] + z[22];
z[24] = abb[22] * z[24];
z[25] = -abb[21] + abb[22];
z[26] = abb[19] + z[25];
z[27] = -abb[5] * z[26];
z[10] = z[10] + z[19] + z[21] + z[23] + z[24] + z[27];
z[21] = 6 * abb[1];
z[23] = abb[0] + abb[13];
z[23] = -abb[2] + -abb[4] + -abb[5] + -3 * abb[7] + -z[21] + 2 * z[23];
z[23] = abb[18] * z[23];
z[10] = 2 * z[10] + z[23];
z[10] = abb[18] * z[10];
z[23] = -abb[0] + abb[2];
z[24] = abb[1] + z[23];
z[27] = abb[8] + -z[8] + z[9] + z[24];
z[28] = abb[24] * z[27];
z[29] = abb[19] + z[13];
z[30] = -abb[1] + z[11];
z[29] = z[29] * z[30];
z[17] = -z[17] + z[28] + z[29];
z[28] = 2 * abb[8];
z[29] = -abb[6] + z[23] + z[28];
z[30] = -abb[25] * z[29];
z[17] = 2 * z[17] + z[30];
z[17] = abb[25] * z[17];
z[30] = prod_pow(abb[23], 2);
z[31] = -abb[22] * abb[23];
z[18] = -abb[18] * z[18];
z[18] = 2 * abb[26] + z[18] + z[30] + z[31];
z[18] = abb[14] * z[18];
z[31] = abb[0] + -abb[11] + -z[22] + -z[28];
z[30] = z[30] * z[31];
z[7] = z[7] + z[23];
z[31] = prod_pow(abb[21], 2);
z[32] = z[7] * z[31];
z[33] = 2 * abb[23];
z[34] = abb[1] + -abb[11] + z[11];
z[34] = z[33] * z[34];
z[35] = 3 * abb[6];
z[36] = -abb[3] + z[35];
z[37] = -abb[0] + 3 * abb[2] + -z[36];
z[37] = abb[22] * z[37];
z[34] = z[34] + z[37];
z[34] = abb[22] * z[34];
z[37] = abb[24] + z[25];
z[37] = abb[24] * z[37];
z[26] = abb[24] + 2 * z[26];
z[26] = abb[19] * z[26];
z[26] = abb[42] + z[26] + z[37];
z[37] = -2 * abb[24] + abb[25];
z[37] = abb[25] * z[37];
z[26] = 2 * z[26] + z[37];
z[26] = abb[5] * z[26];
z[37] = 3 * abb[0] + -18 * abb[1] + -abb[7] + 4 * abb[13] + -z[4];
z[37] = abb[26] * z[37];
z[38] = abb[16] * abb[31];
z[39] = abb[15] * abb[44];
z[40] = -2 * abb[42] + -z[31];
z[40] = abb[3] * z[40];
z[5] = z[5] + z[10] + z[17] + z[18] + z[26] + z[30] + z[32] + z[34] + z[37] + z[38] + 2 * z[39] + z[40];
z[10] = abb[4] + -z[11] + z[12] + z[15];
z[10] = abb[24] * z[10];
z[7] = -abb[3] + z[7];
z[11] = -abb[21] * z[7];
z[15] = z[22] + z[23];
z[17] = -abb[3] + z[15];
z[17] = abb[22] * z[17];
z[18] = abb[1] + abb[6];
z[23] = abb[8] + -z[18];
z[23] = abb[23] * z[23];
z[26] = abb[21] + z[13];
z[26] = abb[4] * z[26];
z[24] = abb[7] + -abb[12] + z[24];
z[24] = abb[19] * z[24];
z[10] = z[10] + z[11] + -z[14] + z[17] + z[23] + z[24] + z[26];
z[10] = abb[19] * z[10];
z[11] = -abb[22] * z[27];
z[15] = -z[15] + -z[20];
z[15] = abb[24] * z[15];
z[11] = z[11] + z[15];
z[11] = abb[24] * z[11];
z[6] = abb[14] + z[6] + -z[16] + -z[28];
z[6] = abb[27] * z[6];
z[7] = -abb[4] + 3 * abb[5] + z[7];
z[7] = abb[29] * z[7];
z[9] = abb[5] + -z[9] + -z[29];
z[9] = abb[28] * z[9];
z[15] = abb[17] * abb[44];
z[6] = abb[47] + z[6] + z[7] + z[9] + z[10] + z[11] + z[15];
z[7] = -13 * abb[0] + 23 * abb[1] + -8 * z[12];
z[7] = abb[12] * (T(-32) / T(3)) + abb[3] * (T(-2) / T(3)) + abb[5] * (T(8) / T(3)) + z[0] + (T(1) / T(3)) * z[7] + z[8];
z[7] = prod_pow(m1_set::bc<T>[0], 2) * z[7];
z[9] = -abb[6] + z[1] + z[12] + z[22];
z[9] = 16 * z[9];
z[10] = -abb[41] * z[9];
z[11] = -abb[4] + abb[12];
z[12] = -abb[1] + abb[7] + z[11];
z[12] = abb[30] * z[12];
z[15] = abb[40] * z[11];
z[12] = z[12] + z[15];
z[13] = abb[22] * z[13];
z[13] = -4 * abb[26] + 2 * z[13] + -z[31];
z[13] = z[0] * z[13];
z[15] = abb[4] + -abb[6];
z[17] = abb[43] * z[15];
z[5] = abb[45] + abb[46] + 4 * z[5] + 8 * z[6] + 2 * z[7] + z[10] + 16 * z[12] + z[13] + -24 * z[17];
z[6] = -abb[1] + abb[12];
z[7] = 5 * abb[7];
z[0] = -abb[0] + z[0];
z[3] = -2 * abb[11] + z[0] + z[3] + -8 * z[6] + -z[7] + z[8];
z[3] = abb[20] * z[3];
z[0] = -abb[13] + z[0] + z[4] + z[16] + -z[35];
z[0] = abb[24] * z[0];
z[2] = -abb[0] + 2 * abb[5] + -z[2] + z[7] + -z[21];
z[2] = abb[18] * z[2];
z[4] = abb[11] + -z[18];
z[4] = z[4] * z[33];
z[6] = abb[1] + -abb[2];
z[6] = 2 * z[6] + z[36];
z[6] = abb[22] * z[6];
z[1] = abb[0] + abb[3] + -abb[13] + z[1];
z[1] = abb[19] * z[1];
z[7] = -abb[19] + -abb[24] + z[25];
z[7] = abb[5] * z[7];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[6] + z[7] + -z[14] + -z[19];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[15] + abb[17];
z[1] = abb[36] * z[1];
z[2] = abb[35] * z[15];
z[3] = abb[32] * z[11];
z[4] = -abb[3] + abb[5];
z[4] = abb[34] * z[4];
z[0] = abb[39] + z[0] + z[1] + -3 * z[2] + 2 * z[3] + z[4];
z[1] = -abb[33] * z[9];
z[0] = abb[37] + abb[38] + 8 * z[0] + z[1];
return {z[5], z[0]};
}


template <typename T> std::complex<T> f_3_194_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_194_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_3_194_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(2)) * (-v[3] + v[5]) * (-12 * v[0] + 4 * v[1] + 6 * m1_set::bc<T>[1] * (4 + v[3] + v[5]) + -3 * (8 + 3 * v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = (-4 * (-1 + 3 * m1_set::bc<T>[1]) * (-v[3] + v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[2];
z[0] = abb[18] + -abb[24];
z[1] = abb[21] + -z[0];
z[1] = abb[21] * z[1];
z[0] = -abb[19] + z[0];
z[0] = abb[19] * z[0];
z[0] = -abb[29] + z[0] + z[1];
return 8 * abb[9] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_194_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (-m1_set::bc<T>[0] * (-1 * v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (4 * m1_set::bc<T>[0] * (v[3] + -v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = -abb[19] + abb[21];
return 8 * abb[9] * m1_set::bc<T>[0] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_194_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4]) * (-24 + -20 * v[0] + 5 * v[1] + -7 * v[2] + -9 * v[3] + 7 * v[4] + 6 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 12 * v[5])) / prod_pow(tend, 2);
c[1] = (4 * (-1 + 3 * m1_set::bc<T>[1]) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[2];
z[0] = abb[18] + -abb[19];
z[1] = -abb[25] + z[0];
z[1] = abb[25] * z[1];
z[0] = abb[24] + -z[0];
z[0] = abb[24] * z[0];
z[0] = abb[28] + z[0] + z[1];
return 8 * abb[10] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_194_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (-m1_set::bc<T>[0] * (v[1] + v[2] + -1 * v[3] + -1 * v[4]) * (-4 + -2 * v[0] + v[1] + -1 * v[2] + -1 * v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (4 * m1_set::bc<T>[0] * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = abb[24] + -abb[25];
return 8 * abb[10] * m1_set::bc<T>[0] * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_194_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("86.670611932228881023669958296932132789513209652157082377650804404"),stof<T>("-10.999762635158051008359231406928728086634337474702834641183860091")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 19, 199});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,48> abb = {dl[0], dl[1], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dlog_W12(k,dl), dlog_W13(k,dl), dlog_W16(k,dl), dlog_W20(k,dl), dlog_W21(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W120(k,dv), dlog_W124(k,dv), dlog_W126(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_14(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_12_im(k), f_2_24_im(k), T{0}, T{0}, T{0}, f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_12_re(k), f_2_24_re(k), T{0}, T{0}, T{0}};
abb[37] = SpDLog_f_3_194_W_16_Im(t, path, abb);
abb[38] = SpDLog_f_3_194_W_20_Im(t, path, abb);
abb[45] = SpDLog_f_3_194_W_16_Re(t, path, abb);
abb[46] = SpDLog_f_3_194_W_20_Re(t, path, abb);
{
auto c = dlog_W166(k,dv) * f_2_31(k);
abb[47] = c.real();
abb[39] = c.imag();
SpDLog_Sigma5<T,199,165>(k, dv, abb[47], abb[39], f_2_31_series_coefficients<T>);
}

                    
            return f_3_194_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_194_DLogXconstant_part(base_point<T>, kend);
	value += f_3_194_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_194_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_194_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_194_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_194_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_194_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_194_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
