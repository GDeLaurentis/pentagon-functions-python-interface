/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_110.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_110_abbreviated (const std::array<T,26>& abb) {
T z[26];
z[0] = -abb[2] + abb[4];
z[1] = 2 * abb[7];
z[2] = z[0] + -z[1];
z[3] = abb[1] + z[2];
z[4] = 2 * abb[8];
z[5] = 3 * abb[5];
z[6] = z[3] + -z[4] + z[5];
z[7] = abb[11] * z[6];
z[8] = 3 * abb[0];
z[3] = abb[8] + z[3] + -z[8];
z[3] = abb[14] * z[3];
z[9] = abb[1] + -abb[8];
z[10] = abb[6] + z[9];
z[10] = abb[10] * z[10];
z[3] = z[3] + z[10];
z[11] = abb[12] + -abb[13];
z[12] = abb[6] * z[11];
z[13] = abb[8] * z[11];
z[12] = z[12] + -z[13];
z[14] = -abb[0] + 3 * abb[6];
z[15] = abb[1] + -z[5] + z[14];
z[15] = abb[9] * z[15];
z[16] = abb[0] * z[11];
z[7] = -z[3] + z[7] + z[12] + (T(1) / T(2)) * z[15] + z[16];
z[7] = abb[9] * z[7];
z[15] = abb[2] + 2 * abb[4];
z[15] = abb[13] * z[15];
z[0] = abb[12] * z[0];
z[16] = 3 * abb[3];
z[17] = abb[13] * z[16];
z[0] = z[0] + z[15] + -z[17];
z[15] = z[1] * z[11];
z[13] = -z[0] + -z[13] + z[15];
z[15] = 2 * abb[1];
z[17] = z[11] * z[15];
z[18] = abb[1] + -abb[5];
z[19] = -abb[2] + abb[3];
z[19] = -abb[7] + (T(1) / T(2)) * z[19];
z[18] = (T(1) / T(2)) * z[18] + -z[19];
z[18] = abb[11] * z[18];
z[17] = z[13] + z[17] + 3 * z[18];
z[17] = abb[11] * z[17];
z[18] = abb[12] + 2 * abb[13];
z[18] = abb[1] * z[18];
z[20] = -abb[12] * z[8];
z[19] = -abb[0] + abb[8] * (T(1) / T(2)) + z[19];
z[19] = abb[14] * z[19];
z[13] = -z[13] + z[18] + 3 * z[19] + z[20];
z[13] = abb[14] * z[13];
z[18] = abb[5] + -abb[8];
z[19] = abb[21] * z[18];
z[9] = abb[5] + z[9];
z[9] = abb[23] * z[9];
z[20] = abb[1] + abb[4];
z[21] = abb[0] + abb[3] + -z[20];
z[21] = abb[15] * z[21];
z[9] = z[9] + z[19] + z[21];
z[19] = 2 * abb[2] + 4 * abb[7];
z[16] = -z[16] + z[19];
z[5] = -abb[8] + z[5];
z[20] = -z[5] + z[16] + z[20];
z[21] = -abb[24] * z[20];
z[22] = -abb[1] * z[11];
z[10] = (T(-1) / T(2)) * z[10] + -z[12] + z[22];
z[10] = abb[10] * z[10];
z[12] = abb[25] * z[6];
z[22] = -abb[13] + abb[12] * (T(1) / T(2));
z[22] = abb[12] * z[22];
z[23] = prod_pow(abb[13], 2);
z[22] = z[22] + -z[23];
z[23] = abb[1] * z[22];
z[22] = 3 * abb[21] + -z[22];
z[22] = abb[0] * z[22];
z[5] = -abb[6] + z[2] + z[5];
z[24] = abb[22] * z[5];
z[25] = abb[1] + -abb[4];
z[19] = 4 * abb[5] + abb[6] * (T(-4) / T(3)) + abb[8] * (T(-2) / T(3)) + abb[3] * (T(1) / T(2)) + abb[0] * (T(1) / T(6)) + -z[19] + (T(-3) / T(2)) * z[25];
z[19] = prod_pow(m1_set::bc<T>[0], 2) * z[19];
z[7] = z[7] + 3 * z[9] + z[10] + z[12] + z[13] + z[17] + z[19] + z[21] + z[22] + z[23] + z[24];
z[9] = -z[4] + z[15];
z[2] = -z[2] + -z[9] + -z[14];
z[2] = abb[9] * z[2];
z[1] = -abb[0] + -abb[6] + -z[1] + -z[9];
z[1] = z[1] * z[11];
z[4] = -4 * abb[1] + -abb[4] + z[4] + -z[16];
z[4] = abb[11] * z[4];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = -abb[19] * z[20];
z[2] = abb[20] * z[6];
z[3] = abb[17] * z[5];
z[4] = abb[16] + abb[18];
z[4] = z[4] * z[18];
z[5] = abb[1] * abb[18];
z[4] = z[4] + z[5];
z[5] = abb[16] * z[8];
z[0] = z[0] + z[1] + z[2] + z[3] + 3 * z[4] + z[5];
return {z[7], z[0]};
}


template <typename T> std::complex<T> f_3_110_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_110_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_110_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-4.262265169913628654295379489958629188151132392251783412781500992"),stof<T>("52.458930299853228537316550805892197671105966515254623505216716107")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,26> abb = {dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dlog_W10(k,dl), dl[4], dlog_W15(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_3_110_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_110_DLogXconstant_part(base_point<T>, kend);
	value += f_3_110_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_110_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_110_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_110_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_110_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_110_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_110_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
