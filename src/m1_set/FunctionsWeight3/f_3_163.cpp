/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_163.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_163_abbreviated (const std::array<T,48>& abb) {
T z[47];
z[0] = -abb[2] + abb[6];
z[1] = abb[5] + z[0];
z[2] = -abb[4] + abb[12];
z[3] = z[1] + -z[2];
z[4] = -abb[29] * z[3];
z[5] = 2 * abb[7];
z[6] = -abb[27] * z[5];
z[7] = -abb[6] + abb[12];
z[8] = abb[5] + -abb[11] + -z[7];
z[8] = abb[26] * z[8];
z[1] = abb[3] + -abb[11] + z[1];
z[9] = abb[28] * z[1];
z[4] = z[4] + z[6] + z[8] + -z[9];
z[6] = abb[7] + z[0];
z[8] = 3 * abb[5];
z[9] = -abb[3] + z[8];
z[10] = 3 * abb[11];
z[11] = 3 * abb[4];
z[6] = 2 * z[6] + z[9] + -z[10] + -z[11];
z[12] = -abb[30] * z[6];
z[13] = -abb[1] + abb[9];
z[14] = 7 * abb[2] + abb[4];
z[15] = 3 * abb[7];
z[16] = -abb[6] + z[15];
z[17] = 3 * abb[13];
z[13] = 3 * z[13] + -z[14] + -z[16] + z[17];
z[18] = abb[45] * z[13];
z[19] = prod_pow(abb[21], 2);
z[20] = abb[26] + abb[27];
z[21] = 2 * abb[21];
z[22] = abb[18] + z[21];
z[22] = abb[18] * z[22];
z[23] = abb[18] + abb[21];
z[24] = abb[20] + -2 * z[23];
z[24] = abb[20] * z[24];
z[20] = z[19] + 6 * z[20] + z[22] + z[24];
z[20] = abb[0] * z[20];
z[22] = -z[8] + z[17];
z[24] = 4 * abb[9];
z[25] = -z[22] + z[24];
z[26] = 2 * abb[2];
z[27] = z[11] + z[26];
z[28] = abb[3] + -abb[7];
z[29] = abb[6] + z[25] + -z[27] + -z[28];
z[30] = abb[46] * z[29];
z[17] = abb[6] + abb[7] + -z[10] + z[17] + -z[24];
z[31] = abb[41] * z[17];
z[32] = abb[3] + abb[4];
z[33] = 2 * abb[8];
z[7] = z[7] + z[32] + -z[33];
z[34] = abb[31] * z[7];
z[35] = abb[4] + z[26];
z[36] = 5 * abb[3] + z[35];
z[10] = -z[10] + -z[16] + z[36];
z[37] = abb[44] * z[10];
z[38] = z[22] + z[33];
z[39] = -4 * abb[1] + abb[12] + -z[15] + z[38];
z[40] = -abb[40] * z[39];
z[36] = 7 * abb[7] + -4 * abb[8] + -abb[12] + z[8] + -z[36];
z[41] = -abb[43] * z[36];
z[4] = 3 * z[4] + z[12] + -z[18] + z[20] + z[30] + -z[31] + z[34] + z[37] + z[40] + z[41];
z[12] = abb[2] + abb[7];
z[18] = 2 * abb[1];
z[20] = z[12] + z[18];
z[30] = 2 * abb[9];
z[2] = -z[2] + z[20] + -z[30];
z[31] = abb[25] * z[2];
z[31] = 2 * z[31];
z[34] = 2 * abb[12];
z[37] = -z[18] + z[29] + z[34];
z[37] = abb[18] * z[37];
z[40] = 2 * abb[6];
z[41] = z[8] + z[40];
z[42] = z[33] + z[34];
z[43] = 4 * abb[4];
z[44] = 3 * abb[2] + -abb[3] + -z[41] + z[42] + -z[43];
z[44] = abb[22] * z[44];
z[30] = abb[1] + abb[4] + z[12] + -z[30];
z[21] = z[21] * z[30];
z[21] = z[21] + z[31] + z[37] + z[44];
z[30] = z[15] + z[18];
z[37] = z[30] + -z[32] + -z[40];
z[40] = -abb[20] * z[37];
z[21] = 2 * z[21] + z[40];
z[21] = abb[20] * z[21];
z[6] = -abb[18] * z[6];
z[40] = -abb[2] + abb[7];
z[44] = 5 * abb[4];
z[45] = 7 * abb[3];
z[46] = abb[6] + z[8] + 4 * z[40] + -z[44] + -z[45];
z[46] = abb[22] * z[46];
z[0] = -4 * abb[3] + -z[0] + z[5] + -z[11];
z[0] = abb[20] * z[0];
z[32] = abb[7] + z[32];
z[32] = abb[21] * z[32];
z[0] = z[0] + z[6] + z[32] + z[46];
z[6] = -10 * abb[3] + 6 * abb[11] + -z[12] + -z[41] + -z[44];
z[6] = abb[23] * z[6];
z[0] = 2 * z[0] + z[6];
z[0] = abb[23] * z[0];
z[6] = 3 * abb[6];
z[12] = z[6] + z[43];
z[32] = 4 * abb[12];
z[20] = 8 * abb[9] + -z[12] + -4 * z[20] + z[22] + z[32];
z[20] = abb[25] * z[20];
z[22] = abb[18] + -abb[21];
z[2] = z[2] * z[22];
z[2] = 4 * z[2] + z[20];
z[2] = abb[25] * z[2];
z[20] = abb[3] + z[27];
z[15] = z[15] + z[20] + -z[42];
z[15] = abb[21] * z[15];
z[7] = abb[18] * z[7];
z[3] = abb[25] * z[3];
z[3] = 3 * z[3] + z[7] + z[15];
z[7] = -abb[2] + -8 * abb[4] + z[16] + z[42] + -z[45];
z[7] = abb[22] * z[7];
z[3] = 2 * z[3] + z[7];
z[3] = abb[22] * z[3];
z[7] = abb[23] * z[10];
z[15] = 2 * abb[4];
z[16] = abb[2] + -abb[8] + -abb[12] + z[15] + z[28];
z[22] = -abb[20] + z[23];
z[23] = 2 * z[22];
z[16] = -z[16] * z[23];
z[27] = -abb[22] * z[36];
z[43] = abb[0] * z[22];
z[7] = z[7] + z[16] + z[27] + -6 * z[43];
z[16] = abb[2] + z[5];
z[9] = 18 * abb[0] + 7 * abb[4] + -abb[6] + -10 * abb[8] + z[9] + 2 * z[16] + -z[32];
z[9] = abb[24] * z[9];
z[7] = 2 * z[7] + z[9];
z[7] = abb[24] * z[7];
z[9] = z[20] + -z[30];
z[9] = z[9] * z[19];
z[16] = -abb[18] * z[37];
z[19] = abb[1] + -abb[12];
z[19] = abb[21] * z[19];
z[16] = z[16] + 4 * z[19];
z[16] = abb[18] * z[16];
z[19] = -abb[5] + abb[13];
z[20] = -abb[2] + z[19] + z[28];
z[27] = abb[42] * z[20];
z[30] = abb[14] + abb[16];
z[32] = 6 * abb[15] + 4 * z[30];
z[32] = abb[39] * z[32];
z[0] = z[0] + z[2] + z[3] + 2 * z[4] + z[7] + z[9] + z[16] + z[21] + -6 * z[27] + z[32];
z[2] = 2 * abb[23];
z[3] = z[2] + z[22];
z[3] = -5 * abb[24] + 2 * z[3];
z[3] = abb[24] * z[3];
z[4] = prod_pow(m1_set::bc<T>[0], 2);
z[7] = -abb[30] + -abb[41];
z[9] = abb[23] + -z[23];
z[9] = abb[23] * z[9];
z[3] = 4 * abb[44] + z[3] + (T(8) / T(3)) * z[4] + 2 * z[7] + z[9];
z[3] = abb[10] * z[3];
z[7] = -27 * abb[2] + abb[4] * (T(-139) / T(6)) + abb[0] * (T(-52) / T(3)) + abb[7] * (T(-47) / T(6)) + abb[1] * (T(-1) / T(3)) + abb[3] * (T(23) / T(6)) + abb[8] * (T(32) / T(3)) + abb[9] * (T(94) / T(3)) + -4 * z[19];
z[4] = z[4] * z[7];
z[1] = z[1] * z[2];
z[2] = -abb[6] + z[19];
z[2] = abb[19] * z[2];
z[1] = z[1] + z[2];
z[1] = abb[19] * z[1];
z[2] = abb[17] * abb[39];
z[0] = 2 * z[0] + 6 * z[1] + 12 * z[2] + 4 * z[3] + z[4];
z[1] = -z[18] + z[24] + z[28] + -z[33] + -z[35];
z[1] = abb[21] * z[1];
z[2] = 2 * abb[3];
z[3] = abb[1] + z[26];
z[3] = 6 * abb[4] + z[2] + 2 * z[3] + -z[6] + -z[25] + -z[42];
z[3] = abb[18] * z[3];
z[4] = abb[3] + -abb[5];
z[4] = 4 * abb[7] + -6 * z[4] + z[6] + -z[14] + -z[42];
z[4] = abb[22] * z[4];
z[6] = abb[1] + z[40];
z[2] = -z[2] + 2 * z[6] + -z[12] + z[38];
z[2] = abb[20] * z[2];
z[6] = 3 * abb[3];
z[5] = -5 * abb[2] + z[5] + -z[6] + -z[15] + z[41];
z[5] = abb[23] * z[5];
z[6] = -12 * abb[0] + abb[6] + -6 * abb[7] + 8 * abb[8] + z[6] + -z[8] + -z[11] + z[34];
z[6] = abb[24] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + -z[31] + 4 * z[43];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[37] * z[13];
z[3] = -abb[33] * z[17];
z[4] = abb[38] * z[29];
z[5] = abb[36] * z[10];
z[6] = abb[15] + abb[17];
z[6] = -3 * z[6] + -2 * z[30];
z[6] = abb[47] * z[6];
z[7] = -abb[32] * z[39];
z[8] = abb[34] * z[20];
z[9] = -abb[35] * z[36];
z[10] = -abb[23] + 4 * abb[24] + -z[22];
z[10] = m1_set::bc<T>[0] * z[10];
z[10] = -abb[33] + 2 * abb[36] + z[10];
z[10] = abb[10] * z[10];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + -3 * z[8] + z[9] + 2 * z[10];
z[1] = 4 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_163_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_163_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_163_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-7.51248222655846273724437061564510622645001589519470063777217179"),stof<T>("675.8712729906897502283737695621190500758504689017568104844749918")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,48> abb = {dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W23(k,dl), dlog_W25(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_8(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_14(k), f_2_19(k), f_2_20(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_10_im(k), f_2_11_im(k), f_2_12_im(k), f_2_22_im(k), f_2_26_im(k), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_10_re(k), f_2_11_re(k), f_2_12_re(k), f_2_22_re(k), f_2_26_re(k)};

                    
            return f_3_163_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_163_DLogXconstant_part(base_point<T>, kend);
	value += f_3_163_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_163_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_163_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_163_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_163_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_163_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_163_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
