/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_196.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_196_abbreviated (const std::array<T,48>& abb) {
T z[39];
z[0] = 4 * abb[24];
z[1] = -abb[20] + abb[21];
z[2] = z[0] + -z[1];
z[3] = 2 * abb[24];
z[4] = -z[2] * z[3];
z[5] = abb[30] + abb[40];
z[6] = 2 * abb[20];
z[7] = abb[21] * z[6];
z[7] = 6 * abb[43] + z[7];
z[8] = prod_pow(abb[19], 2);
z[9] = prod_pow(abb[20], 2);
z[10] = -3 * abb[29] + z[9];
z[11] = 2 * z[1];
z[12] = abb[25] * z[11];
z[4] = z[4] + -4 * z[5] + -z[7] + -z[8] + 2 * z[10] + z[12];
z[4] = abb[6] * z[4];
z[10] = 4 * abb[42];
z[12] = 3 * z[9] + -z[10];
z[13] = -abb[24] + z[1];
z[13] = z[3] * z[13];
z[14] = 2 * abb[29];
z[15] = abb[25] + -z[11];
z[15] = abb[25] * z[15];
z[7] = z[7] + -z[12] + z[13] + z[14] + z[15];
z[7] = abb[4] * z[7];
z[15] = prod_pow(abb[25], 2);
z[15] = z[14] + z[15];
z[16] = -z[8] + z[15];
z[17] = prod_pow(abb[21], 2);
z[13] = -z[9] + -z[13] + z[16] + z[17];
z[13] = abb[0] * z[13];
z[17] = -abb[18] + z[3];
z[18] = -z[1] + z[17];
z[19] = -abb[18] * z[18];
z[20] = abb[26] + abb[27];
z[21] = abb[21] * z[1];
z[22] = abb[24] + -z[11];
z[22] = abb[24] * z[22];
z[19] = z[19] + 2 * z[20] + z[21] + z[22];
z[19] = abb[13] * z[19];
z[20] = prod_pow(abb[24], 2);
z[12] = z[12] + -z[16] + -2 * z[20];
z[12] = abb[2] * z[12];
z[16] = 2 * abb[30];
z[22] = z[8] + z[16];
z[23] = 3 * abb[18];
z[24] = -z[0] + -z[23];
z[24] = abb[18] * z[24];
z[20] = -abb[26] + 7 * z[20] + 2 * z[22] + z[24];
z[20] = abb[8] * z[20];
z[8] = -z[8] + z[9];
z[8] = abb[5] * z[8];
z[9] = abb[3] * z[15];
z[15] = 3 * abb[24];
z[22] = -z[11] + z[15];
z[22] = abb[24] * z[22];
z[24] = -abb[21] + -z[6];
z[24] = abb[21] * z[24];
z[22] = z[22] + z[24];
z[22] = abb[10] * z[22];
z[24] = abb[3] + -abb[5];
z[25] = abb[41] * z[24];
z[25] = abb[47] + z[25];
z[26] = -abb[16] * abb[31];
z[27] = 2 * abb[6];
z[28] = abb[2] + z[27];
z[29] = 3 * abb[0] + -2 * z[28];
z[29] = abb[26] * z[29];
z[4] = z[4] + z[7] + z[8] + z[9] + z[12] + z[13] + z[19] + z[20] + z[22] + 2 * z[25] + z[26] + z[29];
z[7] = 4 * abb[18];
z[8] = -abb[21] + -abb[25] + z[6] + -z[7] + z[15];
z[8] = abb[1] * z[8];
z[9] = abb[0] + -abb[2];
z[12] = abb[5] + z[9];
z[13] = abb[19] + -abb[20];
z[19] = z[12] * z[13];
z[15] = abb[19] + -z[1] + z[15];
z[15] = abb[6] * z[15];
z[20] = 3 * abb[6];
z[22] = abb[2] + abb[3];
z[25] = -z[20] + -z[22];
z[25] = abb[18] * z[25];
z[26] = 2 * abb[8];
z[29] = abb[18] + -abb[24];
z[30] = -abb[19] + z[29];
z[30] = z[26] * z[30];
z[13] = abb[3] * z[13];
z[31] = -abb[21] + abb[24];
z[32] = abb[25] + -z[31];
z[32] = abb[7] * z[32];
z[33] = abb[25] + z[31];
z[33] = abb[4] * z[33];
z[22] = abb[6] + z[22];
z[34] = 3 * abb[1] + -abb[4] + -abb[7] + z[22];
z[34] = abb[23] * z[34];
z[35] = -abb[18] + abb[23];
z[36] = -abb[14] * z[35];
z[37] = abb[1] + 2 * abb[3] + abb[8] + -z[9];
z[37] = abb[22] * z[37];
z[8] = z[8] + -2 * z[13] + z[15] + z[19] + z[25] + z[30] + z[32] + z[33] + z[34] + z[36] + z[37];
z[8] = abb[22] * z[8];
z[15] = -abb[3] + z[27];
z[19] = 2 * abb[1];
z[9] = z[9] + -z[15] + -z[19];
z[9] = abb[23] * z[9];
z[25] = -abb[20] + abb[25];
z[30] = -z[3] + z[25];
z[30] = abb[2] * z[30];
z[32] = -abb[24] + z[25];
z[20] = z[20] * z[32];
z[32] = -7 * abb[24] + z[7] + z[25];
z[32] = abb[1] * z[32];
z[33] = -z[26] * z[29];
z[34] = -abb[19] + -z[25];
z[34] = abb[3] * z[34];
z[36] = -abb[0] * z[25];
z[28] = abb[18] * z[28];
z[37] = abb[24] + z[25];
z[37] = abb[7] * z[37];
z[38] = abb[24] + -2 * z[25];
z[38] = abb[4] * z[38];
z[9] = z[9] + z[20] + z[28] + z[30] + z[32] + z[33] + z[34] + z[36] + z[37] + z[38];
z[9] = abb[23] * z[9];
z[20] = z[1] + -z[3];
z[28] = 5 * abb[24];
z[20] = z[20] * z[28];
z[30] = 6 * abb[24] + -z[1];
z[30] = -z[23] + 2 * z[30];
z[30] = abb[18] * z[30];
z[32] = abb[25] * z[1];
z[16] = -9 * abb[26] + -z[10] + -z[16] + z[20] + -z[21] + z[30] + z[32];
z[16] = abb[1] * z[16];
z[11] = -z[11] + z[28];
z[11] = abb[24] * z[11];
z[18] = -abb[22] + -2 * z[18];
z[18] = abb[22] * z[18];
z[20] = -abb[18] * z[3];
z[5] = 2 * z[5] + -z[10] + z[11] + z[18] + z[20];
z[5] = abb[12] * z[5];
z[10] = -abb[25] + -z[1];
z[10] = abb[25] * z[10];
z[1] = abb[24] * z[1];
z[1] = z[1] + z[10] + -z[14] + -z[21];
z[1] = abb[7] * z[1];
z[10] = 3 * abb[3] + -abb[6] + -z[12] + z[26];
z[10] = abb[28] * z[10];
z[11] = z[17] * z[35];
z[12] = abb[26] + abb[42];
z[11] = z[11] + 2 * z[12];
z[11] = abb[14] * z[11];
z[12] = -abb[0] + 4 * abb[1];
z[14] = -2 * abb[7] + abb[10] + -z[12];
z[14] = abb[27] * z[14];
z[1] = z[1] + z[5] + z[8] + z[9] + z[10] + z[11] + z[14] + z[16];
z[5] = z[0] + -z[25];
z[5] = abb[6] * z[5];
z[8] = abb[0] * abb[24];
z[9] = abb[10] * z[31];
z[10] = -abb[20] + z[3];
z[10] = abb[2] * z[10];
z[5] = z[5] + -z[8] + -z[9] + z[10] + z[13];
z[10] = 2 * abb[0] + -z[22];
z[10] = abb[18] * z[10];
z[5] = 2 * z[5] + z[10];
z[5] = z[5] * z[7];
z[7] = abb[2] + -abb[3];
z[7] = -abb[5] + -4 * z[7];
z[7] = abb[4] + (T(1) / T(3)) * z[7] + z[27];
z[7] = abb[12] * (T(-32) / T(3)) + abb[0] * (T(-13) / T(3)) + abb[14] * (T(8) / T(3)) + abb[1] * (T(23) / T(3)) + 2 * z[7];
z[7] = prod_pow(m1_set::bc<T>[0], 2) * z[7];
z[10] = abb[15] + abb[17];
z[10] = 8 * z[10];
z[11] = abb[44] * z[10];
z[1] = abb[45] + abb[46] + 8 * z[1] + 4 * z[4] + z[5] + 2 * z[7] + z[11];
z[4] = abb[18] + abb[22] + -z[2];
z[4] = abb[12] * z[4];
z[4] = -z[4] + z[9];
z[0] = z[0] + z[25];
z[0] = abb[6] * z[0];
z[2] = z[2] + -z[23];
z[2] = z[2] * z[19];
z[5] = -abb[5] * abb[20];
z[6] = -abb[2] * z[6];
z[7] = -abb[0] + -2 * z[15];
z[7] = abb[18] * z[7];
z[9] = abb[8] * z[29];
z[11] = 3 * abb[20] + -2 * abb[21] + z[3];
z[11] = abb[4] * z[11];
z[12] = 2 * abb[2] + -abb[3] + -3 * abb[4] + 4 * abb[6] + z[12];
z[12] = abb[23] * z[12];
z[3] = -abb[23] + z[3];
z[3] = abb[14] * z[3];
z[14] = abb[0] + -abb[14] + -z[24];
z[14] = abb[22] * z[14];
z[0] = z[0] + z[2] + z[3] + -2 * z[4] + z[5] + z[6] + z[7] + -z[8] + 5 * z[9] + z[11] + z[12] + -z[13] + z[14];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[33] * z[24];
z[3] = -2 * abb[32] + -3 * abb[35];
z[3] = abb[6] * z[3];
z[0] = abb[39] + z[0] + z[2] + z[3];
z[2] = -abb[2] + abb[4] + abb[14] + -z[19];
z[2] = abb[34] * z[2];
z[3] = abb[32] + -2 * abb[34];
z[3] = abb[12] * z[3];
z[2] = z[2] + z[3];
z[3] = abb[4] * abb[35];
z[4] = abb[36] * z[10];
z[0] = abb[37] + abb[38] + 8 * z[0] + 16 * z[2] + 24 * z[3] + z[4];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_3_196_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_196_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_3_196_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(2)) * (24 + 15 * v[0] + 5 * v[1] + 3 * v[2] + -9 * v[3] + -12 * v[4] + -6 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -3 * v[5]) * (-v[0] + v[1] + -v[2] + -v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (4 * (-1 + 3 * m1_set::bc<T>[1]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[2];
z[0] = abb[18] + -abb[22];
z[1] = -abb[25] + z[0];
z[1] = abb[25] * z[1];
z[0] = abb[23] + -z[0];
z[0] = abb[23] * z[0];
z[0] = abb[29] + z[0] + z[1];
return 8 * abb[9] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_196_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (4 * m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = abb[23] + -abb[25];
return 8 * abb[9] * m1_set::bc<T>[0] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_196_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(2)) * (v[2] + v[3]) * (24 + 8 * v[0] + 4 * v[1] + -5 * v[2] + -9 * v[3] + -4 * v[4] + 6 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 12 * v[5])) / prod_pow(tend, 2);
c[1] = (-4 * (-1 + 3 * m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[2];
z[0] = abb[18] + -abb[23];
z[1] = -abb[22] + z[0];
z[1] = abb[22] * z[1];
z[0] = abb[19] + -z[0];
z[0] = abb[19] * z[0];
z[0] = -abb[28] + z[0] + z[1];
return 8 * abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_196_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (-4 * m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = abb[19] + -abb[22];
return 8 * abb[11] * m1_set::bc<T>[0] * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_196_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("86.670611932228881023669958296932132789513209652157082377650804404"),stof<T>("-10.999762635158051008359231406928728086634337474702834641183860091")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 18, 202});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,48> abb = {dl[0], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dlog_W12(k,dl), dlog_W13(k,dl), dlog_W17(k,dl), dlog_W18(k,dl), dlog_W19(k,dl), dlog_W26(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W122(k,dv), dlog_W124(k,dv), dlog_W128(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_15(k), f_2_19(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_11_im(k), f_2_12_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, f_2_4_re(k), f_2_7_re(k), f_2_11_re(k), f_2_12_re(k), f_2_25_re(k), T{0}, T{0}, T{0}};
abb[37] = SpDLog_f_3_196_W_17_Im(t, path, abb);
abb[38] = SpDLog_f_3_196_W_19_Im(t, path, abb);
abb[45] = SpDLog_f_3_196_W_17_Re(t, path, abb);
abb[46] = SpDLog_f_3_196_W_19_Re(t, path, abb);
{
auto c = dlog_W172(k,dv) * f_2_34(k);
abb[47] = c.real();
abb[39] = c.imag();
SpDLog_Sigma5<T,202,171>(k, dv, abb[47], abb[39], f_2_34_series_coefficients<T>);
}

                    
            return f_3_196_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_196_DLogXconstant_part(base_point<T>, kend);
	value += f_3_196_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_196_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_196_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_196_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_196_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_196_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_196_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
