/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_123.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_123_abbreviated (const std::array<T,31>& abb) {
T z[27];
z[0] = abb[3] + abb[5];
z[1] = abb[7] + -abb[11];
z[2] = -abb[9] + z[1];
z[3] = -abb[4] + abb[6];
z[2] = z[0] + 2 * z[2] + z[3];
z[4] = -abb[27] * z[2];
z[5] = -abb[7] + 2 * abb[11];
z[3] = -abb[9] + z[3];
z[6] = abb[1] + abb[5];
z[7] = -z[3] + z[5] + -z[6];
z[8] = abb[25] * z[7];
z[9] = -abb[0] + -z[0];
z[9] = abb[18] * z[9];
z[4] = z[4] + z[8] + z[9];
z[8] = -abb[5] + z[1];
z[9] = abb[13] * z[8];
z[10] = abb[1] * abb[13];
z[9] = z[9] + -z[10];
z[11] = 2 * abb[7];
z[12] = 3 * abb[11];
z[13] = z[11] + -z[12];
z[14] = -abb[4] + z[13];
z[15] = 2 * abb[10];
z[14] = -3 * abb[1] + abb[3] + 2 * z[14] + z[15];
z[14] = abb[15] * z[14];
z[16] = z[1] + -z[6];
z[17] = 2 * abb[12];
z[16] = z[16] * z[17];
z[8] = abb[3] * (T(-3) / T(2)) + abb[1] * (T(5) / T(2)) + -z[8];
z[8] = abb[17] * z[8];
z[8] = z[8] + -2 * z[9] + z[14] + z[16];
z[8] = abb[17] * z[8];
z[12] = abb[4] + z[12];
z[14] = 3 * abb[9];
z[16] = abb[6] + abb[1] * (T(-1) / T(2)) + abb[3] * (T(5) / T(2)) + z[12] + -z[14] + -z[15];
z[16] = abb[15] * z[16];
z[18] = abb[9] + -abb[11];
z[0] = z[0] + -z[18];
z[0] = abb[13] * z[0];
z[16] = -2 * z[0] + z[16];
z[16] = abb[15] * z[16];
z[3] = z[3] + z[13];
z[13] = -abb[15] * z[3];
z[9] = z[9] + z[13];
z[6] = abb[0] + -abb[8] + z[6];
z[13] = abb[6] + -abb[7] + z[6];
z[13] = abb[12] * z[13];
z[9] = 2 * z[9] + z[13];
z[9] = abb[12] * z[9];
z[13] = abb[0] + -abb[2] + z[18];
z[19] = -abb[12] * z[13];
z[0] = z[0] + z[19];
z[19] = abb[4] + 4 * abb[11];
z[14] = 3 * abb[0] + -4 * abb[2] + abb[8] + z[14] + -z[19];
z[14] = abb[14] * z[14];
z[0] = 2 * z[0] + z[14];
z[0] = abb[14] * z[0];
z[14] = -abb[12] + abb[17];
z[14] = z[1] * z[14];
z[20] = abb[1] + -abb[3];
z[18] = -z[18] + -z[20];
z[18] = abb[15] * z[18];
z[13] = abb[14] * z[13];
z[14] = z[13] + z[14] + z[18];
z[18] = 2 * abb[16];
z[14] = z[14] * z[18];
z[15] = 5 * abb[1] + -z[15];
z[21] = abb[6] + -abb[11];
z[21] = 2 * z[21];
z[22] = 3 * abb[3] + -z[15] + -z[21];
z[23] = abb[28] * z[22];
z[20] = -abb[0] + z[20];
z[24] = prod_pow(abb[13], 2);
z[20] = z[20] * z[24];
z[3] = 2 * z[3];
z[25] = -abb[29] * z[3];
z[21] = -5 * abb[0] + 2 * abb[8] + -z[21];
z[26] = abb[26] * z[21];
z[24] = 2 * abb[18] + 3 * abb[26] + z[24];
z[24] = abb[2] * z[24];
z[11] = 4 * abb[9] + abb[11] + abb[3] * (T(-35) / T(12)) + abb[6] * (T(-8) / T(3)) + abb[0] * (T(-5) / T(12)) + abb[2] * (T(-1) / T(4)) + abb[4] * (T(1) / T(3)) + abb[8] * (T(2) / T(3)) + abb[10] * (T(5) / T(3)) + abb[1] * (T(5) / T(4)) + -z[11];
z[11] = prod_pow(m1_set::bc<T>[0], 2) * z[11];
z[0] = abb[30] + z[0] + 2 * z[4] + z[8] + z[9] + z[11] + z[14] + z[16] + z[20] + z[23] + z[24] + z[25] + z[26];
z[4] = abb[3] + -abb[9] + z[5];
z[5] = 2 * abb[5] + z[4];
z[5] = abb[13] * z[5];
z[5] = z[5] + z[10] + z[13];
z[1] = -abb[4] + -abb[9] + 3 * z[1] + -z[6];
z[1] = z[1] * z[17];
z[6] = abb[5] + -3 * abb[7] + z[19];
z[6] = -abb[3] + 2 * z[6] + z[15];
z[6] = abb[17] * z[6];
z[4] = abb[1] + -z[4];
z[4] = z[4] * z[18];
z[8] = abb[7] + abb[9] + abb[10] + -z[12];
z[8] = abb[1] + -5 * abb[3] + 4 * z[8];
z[8] = abb[15] * z[8];
z[1] = z[1] + z[4] + 2 * z[5] + z[6] + z[8];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[21] * z[2];
z[4] = abb[19] * z[7];
z[2] = z[2] + z[4];
z[3] = -abb[23] * z[3];
z[4] = abb[22] * z[22];
z[5] = 3 * abb[2] + z[21];
z[5] = abb[20] * z[5];
z[1] = abb[24] + z[1] + 2 * z[2] + z[3] + z[4] + z[5];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_123_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_123_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_123_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.317884321949236500199637749323032675410117420057837651023166462"),stof<T>("22.754951790478186162352393914322666310916554322791790456633971681")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,31> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W8(k,dl), dl[3], dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), f_1_1(k), f_1_2(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_11(k), f_2_16(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), T{0}, f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), T{0}};
abb[24] = SpDLogQ_W_84(k,dl,dlr).imag();
abb[30] = SpDLogQ_W_84(k,dl,dlr).real();

                    
            return f_3_123_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_123_DLogXconstant_part(base_point<T>, kend);
	value += f_3_123_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_123_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_123_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_123_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_123_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_123_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_123_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
