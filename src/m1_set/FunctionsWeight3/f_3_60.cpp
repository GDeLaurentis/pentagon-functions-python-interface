/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_60.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_60_abbreviated (const std::array<T,22>& abb) {
T z[23];
z[0] = abb[2] + abb[5];
z[1] = abb[1] + abb[4];
z[2] = 3 * abb[7];
z[3] = abb[0] + -z[0] + -2 * z[1] + z[2];
z[3] = abb[13] * z[3];
z[4] = -abb[11] + abb[14];
z[5] = 3 * abb[3];
z[6] = z[4] * z[5];
z[7] = abb[10] + z[4];
z[8] = abb[4] * z[7];
z[3] = z[3] + -z[6] + z[8];
z[6] = -abb[9] + z[7];
z[6] = abb[8] * z[6];
z[9] = abb[0] + abb[1];
z[10] = abb[9] * z[9];
z[6] = z[6] + z[10];
z[10] = -abb[10] + 2 * z[4];
z[11] = z[0] * z[10];
z[12] = -abb[4] + z[2] + -z[9];
z[12] = -z[0] + (T(1) / T(2)) * z[12];
z[12] = abb[12] * z[12];
z[13] = 3 * abb[0];
z[14] = abb[10] * z[13];
z[11] = z[3] + z[6] + z[11] + z[12] + -z[14];
z[11] = abb[12] * z[11];
z[12] = abb[9] * (T(1) / T(2));
z[15] = -z[7] + z[12];
z[15] = abb[9] * z[15];
z[16] = 3 * abb[16];
z[17] = 3 * abb[21];
z[18] = prod_pow(abb[10], 2);
z[19] = (T(3) / T(2)) * z[18];
z[15] = abb[17] + z[15] + -z[16] + -z[17] + z[19];
z[15] = -z[0] * z[15];
z[20] = -abb[6] * z[4];
z[21] = abb[4] * (T(1) / T(2));
z[22] = abb[1] + abb[0] * (T(-1) / T(2)) + z[21];
z[22] = abb[9] * z[22];
z[8] = -z[8] + z[14] + 3 * z[20] + z[22];
z[8] = abb[9] * z[8];
z[10] = z[10] + -z[12];
z[10] = abb[9] * z[10];
z[12] = 3 * abb[15];
z[10] = -2 * abb[17] + z[10] + z[12] + z[16] + z[19];
z[10] = abb[8] * z[10];
z[16] = 2 * abb[4] + z[0];
z[7] = z[7] * z[16];
z[9] = z[9] + -z[16];
z[9] = abb[13] * z[9];
z[2] = 3 * abb[1] + -z[2];
z[2] = abb[14] * z[2];
z[7] = z[2] + -z[6] + z[7] + z[9];
z[7] = abb[13] * z[7];
z[9] = prod_pow(abb[11], 2);
z[16] = prod_pow(abb[14], 2);
z[9] = (T(-1) / T(2)) * z[9] + (T(1) / T(2)) * z[16];
z[19] = -abb[16] + abb[17] + -z[9];
z[19] = abb[6] * z[19];
z[22] = -abb[0] + abb[4];
z[22] = abb[20] * z[22];
z[19] = z[19] + z[22];
z[22] = -abb[1] + abb[7];
z[22] = (T(-3) / T(2)) * z[22];
z[16] = z[16] * z[22];
z[13] = -z[13] * z[18];
z[18] = -abb[0] + -abb[6];
z[12] = z[12] * z[18];
z[9] = -abb[16] + z[9];
z[5] = z[5] * z[9];
z[9] = abb[3] + abb[7];
z[18] = z[1] + -z[9];
z[17] = z[17] * z[18];
z[18] = -10 * abb[1] + abb[0] * (T(7) / T(2)) + z[21];
z[18] = 2 * abb[7] + (T(4) / T(3)) * z[0] + (T(1) / T(3)) * z[18];
z[18] = prod_pow(m1_set::bc<T>[0], 2) * z[18];
z[21] = abb[4] * abb[17];
z[5] = z[5] + z[7] + z[8] + z[10] + z[11] + z[12] + z[13] + z[15] + z[16] + z[17] + z[18] + 3 * z[19] + z[21];
z[4] = 2 * abb[10] + -z[4];
z[4] = z[0] * z[4];
z[1] = 2 * abb[0] + z[0] + -z[1];
z[1] = abb[12] * z[1];
z[1] = z[1] + -z[2] + z[3] + z[4] + -2 * z[6] + z[14] + -6 * z[20];
z[1] = m1_set::bc<T>[0] * z[1];
z[0] = abb[1] + z[0] + -z[9];
z[0] = abb[19] * z[0];
z[2] = abb[18] + abb[19];
z[2] = abb[4] * z[2];
z[3] = -abb[0] * abb[18];
z[0] = z[0] + z[2] + z[3];
z[0] = 3 * z[0] + z[1];
return {z[5], z[0]};
}


template <typename T> std::complex<T> f_3_60_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_60_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_60_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("17.615507745935394102840818345096835064136201461388195528260961886"),stof<T>("-8.584324298882360639391283163367212198296924741406416600296859453")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,22> abb = {dl[1], dlog_W3(k,dl), dlog_W7(k,dl), dlog_W8(k,dl), dlog_W10(k,dl), dl[4], dlog_W18(k,dl), dlog_W26(k,dl), dlog_W30(k,dl), f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_3(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_9_re(k), f_2_11_re(k)};

                    
            return f_3_60_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_60_DLogXconstant_part(base_point<T>, kend);
	value += f_3_60_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_60_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_60_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_60_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_60_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_60_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_60_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
