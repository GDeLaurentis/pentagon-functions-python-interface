/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_147.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_147_abbreviated (const std::array<T,17>& abb) {
T z[17];
z[0] = 2 * abb[4];
z[1] = -abb[5] + z[0];
z[2] = -abb[0] + abb[2];
z[3] = z[1] + -z[2];
z[4] = abb[7] + -abb[11];
z[4] = z[3] * z[4];
z[5] = 2 * abb[0];
z[6] = -2 * abb[1] + 6 * abb[6];
z[7] = -3 * abb[3] + z[6];
z[8] = 5 * abb[4] + -abb[5] + z[5] + -z[7];
z[8] = abb[8] * z[8];
z[4] = z[4] + z[8];
z[8] = abb[0] + abb[2];
z[9] = 3 * abb[4];
z[10] = z[7] + -z[8] + -z[9];
z[11] = abb[9] * z[10];
z[4] = 2 * z[4] + z[11];
z[4] = abb[9] * z[4];
z[11] = abb[15] + -abb[16];
z[11] = 2 * z[11];
z[11] = z[10] * z[11];
z[12] = 2 * abb[3] + -abb[5] + z[2];
z[13] = abb[7] * z[12];
z[12] = abb[10] * z[12];
z[13] = -z[12] + z[13];
z[3] = abb[8] * z[3];
z[3] = z[3] + z[13];
z[14] = 2 * abb[11];
z[3] = z[3] * z[14];
z[1] = -6 * abb[3] + -3 * z[1] + z[6] + -z[8];
z[1] = abb[12] * z[1];
z[8] = 3 * abb[0];
z[15] = abb[2] + -7 * abb[4] + 2 * abb[5] + z[7] + -z[8];
z[15] = abb[8] * z[15];
z[15] = 2 * z[12] + z[15];
z[15] = abb[8] * z[15];
z[16] = -abb[3] + -abb[4] + abb[5];
z[16] = abb[8] * z[16];
z[13] = -z[13] + 2 * z[16];
z[13] = abb[7] * z[13];
z[16] = 5 * abb[2] + 7 * abb[3] + -abb[4] + -z[6] + -z[8];
z[16] = prod_pow(m1_set::bc<T>[0], 2) * z[16];
z[1] = z[1] + z[3] + z[4] + z[11] + 2 * z[13] + z[15] + z[16];
z[1] = 4 * z[1];
z[3] = -abb[3] + -abb[5] + -z[5] + z[6] + -z[9];
z[3] = abb[8] * z[3];
z[0] = 3 * abb[2] + 4 * abb[3] + -abb[5] + -z[0] + -z[8];
z[0] = abb[7] * z[0];
z[2] = -abb[3] + abb[4] + -z[2];
z[2] = z[2] * z[14];
z[4] = 2 * abb[2] + abb[4] + abb[5] + -z[7];
z[4] = abb[9] * z[4];
z[0] = z[0] + z[2] + z[3] + z[4] + -z[12];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[13] + -abb[14];
z[2] = z[2] * z[10];
z[0] = z[0] + z[2];
z[0] = 8 * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_3_147_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_147_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_147_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-50.30330900885967422934096524925954292177499271695971172138598057"),stof<T>("274.49261402741422057663537217397065643668827985985975469989461928")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,17> abb = {dl[0], dlog_W8(k,dl), dl[3], dlog_W15(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W67(k,dl), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_7(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_4_re(k), f_2_6_re(k)};

                    
            return f_3_147_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_147_DLogXconstant_part(base_point<T>, kend);
	value += f_3_147_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_147_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_147_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_147_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_147_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_147_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_147_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
