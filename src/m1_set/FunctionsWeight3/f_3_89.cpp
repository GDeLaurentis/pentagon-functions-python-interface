/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_89.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_89_abbreviated (const std::array<T,30>& abb) {
T z[24];
z[0] = -abb[10] + abb[3] * (T(1) / T(2));
z[1] = abb[9] * (T(1) / T(2));
z[2] = 2 * abb[2];
z[3] = abb[1] + abb[4];
z[4] = abb[0] + abb[6] * (T(-3) / T(2)) + z[0] + -z[1] + z[2] + z[3];
z[4] = abb[13] * z[4];
z[5] = -abb[3] + abb[6];
z[6] = 2 * abb[8];
z[7] = z[5] + -z[6];
z[8] = abb[0] * (T(1) / T(2)) + z[1];
z[9] = abb[4] * (T(1) / T(2));
z[10] = -z[7] + -z[8] + -z[9];
z[10] = abb[17] * z[10];
z[11] = -abb[8] + abb[10];
z[12] = -abb[1] + z[11];
z[8] = abb[4] * (T(-3) / T(2)) + z[5] + z[8] + 2 * z[12];
z[8] = abb[14] * z[8];
z[11] = -z[3] + z[11];
z[11] = abb[9] + z[5] + 2 * z[11];
z[11] = abb[18] * z[11];
z[13] = -abb[3] + z[2];
z[14] = -abb[10] + z[6];
z[15] = -abb[7] + z[13] + -z[14];
z[16] = -abb[16] * z[15];
z[17] = abb[7] + -abb[8];
z[18] = abb[0] + z[17];
z[18] = abb[15] * z[18];
z[16] = z[16] + -z[18];
z[4] = z[4] + z[8] + z[10] + z[11] + 2 * z[16];
z[4] = abb[13] * z[4];
z[8] = -abb[1] + -z[17];
z[8] = abb[16] * z[8];
z[8] = z[8] + z[18];
z[7] = z[3] + z[7];
z[7] = abb[18] * z[7];
z[10] = abb[0] + -abb[1];
z[16] = abb[17] * (T(1) / T(2));
z[18] = z[10] * z[16];
z[7] = z[7] + 2 * z[8] + z[18];
z[7] = abb[17] * z[7];
z[8] = abb[5] + -abb[10];
z[17] = -z[8] + z[17];
z[18] = -abb[15] + abb[16];
z[17] = z[17] * z[18];
z[18] = -abb[0] + -abb[4] + abb[9];
z[16] = z[16] * z[18];
z[10] = -abb[14] * z[10];
z[10] = z[10] + -z[11] + z[16] + 2 * z[17];
z[10] = abb[14] * z[10];
z[11] = abb[0] + -abb[8];
z[11] = abb[20] * z[11];
z[16] = abb[19] + abb[20];
z[16] = abb[2] * z[16];
z[17] = abb[1] + abb[2] + -abb[8];
z[17] = abb[22] * z[17];
z[11] = z[11] + z[16] + z[17];
z[16] = 2 * abb[7];
z[13] = abb[5] + -abb[8] + z[13] + -z[16];
z[13] = abb[21] * z[13];
z[15] = abb[23] * z[15];
z[17] = abb[0] + z[8];
z[19] = abb[27] * z[17];
z[13] = z[13] + -z[15] + z[19];
z[15] = 3 * abb[7] + -z[8];
z[2] = abb[8] + z[2] + -z[15];
z[2] = prod_pow(abb[16], 2) * z[2];
z[0] = abb[6] * (T(1) / T(2)) + abb[1] * (T(3) / T(2)) + z[0] + z[9];
z[0] = abb[18] * z[0];
z[12] = -abb[3] + z[12];
z[12] = abb[16] * z[12];
z[0] = z[0] + 2 * z[12];
z[0] = abb[18] * z[0];
z[12] = abb[4] + -abb[9] + abb[1] * (T(11) / T(2));
z[19] = abb[0] * (T(3) / T(2));
z[12] = (T(1) / T(3)) * z[12] + -z[19];
z[12] = prod_pow(m1_set::bc<T>[0], 2) * z[12];
z[20] = -abb[11] + abb[12];
z[21] = (T(-1) / T(2)) * z[20];
z[21] = abb[29] * z[21];
z[22] = -abb[7] + -abb[8] + z[3];
z[22] = -abb[3] + -abb[6] + -abb[9] + 2 * z[22];
z[22] = abb[19] * z[22];
z[3] = -abb[9] + z[3];
z[23] = abb[28] * z[3];
z[15] = 2 * abb[0] + -3 * abb[8] + z[15];
z[15] = prod_pow(abb[15], 2) * z[15];
z[0] = z[0] + z[2] + z[4] + z[7] + z[10] + 4 * z[11] + z[12] + 2 * z[13] + z[15] + z[21] + z[22] + z[23];
z[2] = -z[6] + -z[8] + z[16];
z[4] = abb[1] + z[2];
z[4] = abb[16] * z[4];
z[2] = -abb[0] + -z[2];
z[2] = abb[15] * z[2];
z[5] = z[5] + -z[14];
z[6] = abb[13] * z[5];
z[2] = z[2] + z[4] + z[6];
z[1] = abb[1] + z[1] + -z[9] + -z[19];
z[1] = abb[17] * z[1];
z[4] = z[3] + -2 * z[5];
z[4] = abb[18] * z[4];
z[5] = abb[14] * z[18];
z[1] = z[1] + 2 * z[2] + z[4] + (T(1) / T(2)) * z[5];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[24] * z[17];
z[4] = -abb[26] * z[20];
z[3] = abb[25] * z[3];
z[1] = z[1] + 2 * z[2] + z[3] + (T(1) / T(2)) * z[4];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_89_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_89_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_89_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("17.228850235920250016981920766407842360472218689139040843374050265"),stof<T>("11.11583715686923448479530091659175115018679881593546836060900538")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,30> abb = {dl[0], dlog_W3(k,dl), dl[2], dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dlog_W13(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W79(k,dl), dlog_W120(k,dv), dlog_W127(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_3_89_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_89_DLogXconstant_part(base_point<T>, kend);
	value += f_3_89_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_89_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_89_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_89_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_89_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_89_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_89_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
