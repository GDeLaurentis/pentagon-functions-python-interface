/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_179.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_179_abbreviated (const std::array<T,43>& abb) {
T z[37];
z[0] = 4 * abb[1];
z[1] = 2 * abb[10];
z[2] = z[0] + -z[1];
z[3] = -abb[4] + abb[14];
z[4] = -abb[6] + abb[9];
z[5] = z[3] + z[4];
z[6] = 2 * abb[7];
z[7] = 3 * abb[2];
z[8] = z[6] + -z[7];
z[9] = -z[2] + z[5] + z[8];
z[9] = abb[18] * z[9];
z[10] = 2 * abb[4];
z[11] = 2 * abb[14];
z[12] = abb[5] + abb[8];
z[8] = -7 * abb[1] + z[8] + -z[10] + z[11] + z[12];
z[13] = -abb[20] * z[8];
z[14] = 2 * abb[6];
z[15] = abb[0] + -abb[4];
z[16] = z[14] + -z[15];
z[17] = 2 * abb[1];
z[18] = -abb[3] + z[17];
z[19] = -abb[2] + z[1];
z[20] = z[16] + z[18] + -z[19];
z[20] = abb[23] * z[20];
z[21] = -abb[1] + abb[8];
z[22] = -abb[5] + z[19] + z[21];
z[22] = abb[19] * z[22];
z[23] = -abb[11] + z[12];
z[5] = 3 * abb[1] + abb[2] + -z[5] + -z[23];
z[5] = abb[21] * z[5];
z[24] = abb[24] * z[4];
z[23] = -abb[1] + z[23];
z[25] = abb[25] * z[23];
z[5] = z[5] + z[9] + z[13] + z[20] + z[22] + -z[24] + z[25];
z[9] = -abb[0] + -abb[4] + abb[6];
z[13] = 2 * abb[5];
z[18] = abb[2] + -abb[9] + -6 * abb[10] + z[9] + z[11] + z[13] + -z[18];
z[18] = abb[22] * z[18];
z[5] = 2 * z[5] + z[18];
z[5] = abb[22] * z[5];
z[18] = abb[2] + abb[3] + -3 * abb[6] + abb[9] + -z[6] + z[15];
z[18] = abb[29] * z[18];
z[20] = -abb[3] + abb[6];
z[25] = -abb[39] * z[20];
z[26] = abb[15] + abb[16];
z[26] = abb[32] * z[26];
z[27] = abb[7] + abb[10];
z[28] = -abb[1] + -abb[2] + z[27];
z[28] = abb[31] * z[28];
z[29] = -abb[1] + abb[12];
z[30] = -prod_pow(abb[19], 2) * z[29];
z[18] = abb[42] + z[18] + z[25] + z[26] + 2 * z[28] + z[30];
z[25] = 5 * abb[1];
z[26] = abb[0] + abb[12];
z[19] = -z[12] + z[19] + -z[25] + z[26];
z[19] = abb[20] * z[19];
z[15] = abb[6] + z[15];
z[28] = -abb[1] + -abb[8] + -z[7] + z[13] + z[15];
z[28] = abb[21] * z[28];
z[12] = -abb[2] + z[12];
z[29] = z[12] + -z[29];
z[29] = abb[19] * z[29];
z[30] = -abb[2] + abb[6];
z[31] = -abb[4] + z[17] + -z[30];
z[31] = abb[18] * z[31];
z[32] = -abb[24] * z[20];
z[19] = z[19] + z[28] + z[29] + z[31] + z[32];
z[28] = -abb[0] + 3 * abb[4] + z[30];
z[28] = abb[23] * z[28];
z[19] = 2 * z[19] + z[28];
z[19] = abb[23] * z[19];
z[8] = abb[21] * z[8];
z[28] = 2 * abb[2];
z[29] = abb[4] + z[28];
z[30] = -abb[14] + z[29];
z[31] = 6 * abb[1];
z[27] = -z[27] + z[30] + z[31];
z[27] = -z[26] + 2 * z[27];
z[27] = abb[18] * z[27];
z[8] = z[8] + -z[22] + z[27];
z[22] = -z[25] + -z[29];
z[22] = abb[8] + 3 * abb[10] + abb[14] + 2 * z[22];
z[25] = 2 * abb[12];
z[22] = abb[0] + 7 * abb[7] + 2 * z[22] + z[25];
z[22] = abb[20] * z[22];
z[8] = 2 * z[8] + z[22];
z[8] = abb[20] * z[8];
z[21] = -z[13] + z[21] + z[28];
z[21] = abb[23] * z[21];
z[12] = abb[1] + -z[12];
z[12] = abb[19] * z[12];
z[22] = -abb[21] * z[23];
z[23] = -abb[2] + abb[11];
z[27] = abb[18] * z[23];
z[12] = z[12] + z[21] + z[22] + z[27];
z[21] = abb[5] + -abb[11];
z[21] = abb[25] * z[21];
z[12] = 2 * z[12] + 3 * z[21];
z[12] = abb[25] * z[12];
z[1] = abb[5] + -z[1] + z[3] + -z[17];
z[1] = abb[28] * z[1];
z[3] = -abb[2] + abb[10];
z[3] = abb[26] * z[3];
z[1] = -z[1] + z[3];
z[3] = abb[11] + z[15];
z[21] = 2 * abb[8];
z[22] = -abb[5] + z[21];
z[27] = abb[2] + z[0] + -z[3] + -z[22];
z[32] = -prod_pow(abb[21], 2) * z[27];
z[6] = -abb[11] + z[0] + -z[6] + z[30];
z[6] = abb[21] * z[6];
z[30] = -abb[12] + z[17];
z[33] = abb[19] * z[30];
z[6] = z[6] + -z[24] + -z[33];
z[24] = 2 * abb[0] + -abb[2] + -abb[4] + -abb[6] + -3 * abb[7] + z[11] + -z[31];
z[24] = abb[18] * z[24];
z[6] = 2 * z[6] + z[24];
z[6] = abb[18] * z[6];
z[24] = -abb[18] + abb[23];
z[34] = -abb[19] + 2 * abb[20];
z[35] = -abb[18] + z[34];
z[24] = z[24] * z[35];
z[35] = 2 * abb[38];
z[36] = prod_pow(m1_set::bc<T>[0], 2);
z[24] = 2 * abb[27] + z[24] + z[35] + (T(2) / T(3)) * z[36];
z[24] = abb[13] * z[24];
z[4] = abb[21] * z[4];
z[14] = -abb[3] + 3 * abb[9] + -z[14];
z[14] = abb[24] * z[14];
z[4] = 2 * z[4] + z[14];
z[4] = abb[24] * z[4];
z[14] = abb[8] + z[17];
z[14] = 2 * z[14] + -z[26];
z[17] = -z[14] * z[35];
z[11] = -9 * abb[1] + z[11] + -z[29];
z[11] = 3 * abb[0] + -abb[7] + 2 * z[11];
z[11] = abb[27] * z[11];
z[26] = abb[17] * abb[41];
z[35] = -abb[2] + abb[5];
z[35] = abb[30] * z[35];
z[1] = -4 * z[1] + z[4] + z[5] + z[6] + z[8] + z[11] + z[12] + z[17] + 2 * z[18] + z[19] + z[24] + z[26] + z[32] + 6 * z[35];
z[4] = 5 * abb[2] + -abb[8] + z[10];
z[4] = -11 * abb[0] + 23 * abb[1] + -14 * abb[6] + -8 * abb[11] + 4 * z[4] + z[13] + -z[25];
z[4] = z[4] * z[36];
z[3] = z[3] + -z[7] + -z[22];
z[5] = abb[40] * z[3];
z[1] = 2 * z[1] + (T(1) / T(3)) * z[4] + 4 * z[5];
z[1] = 2 * z[1];
z[4] = -abb[3] + 2 * abb[9];
z[0] = 4 * abb[10] + -abb[11] + -z[0] + z[4] + -z[16] + z[21] + -z[28];
z[0] = abb[22] * z[0];
z[5] = 5 * abb[7] + abb[12];
z[6] = abb[11] + z[5] + z[9] + -z[28] + -z[31];
z[6] = abb[18] * z[6];
z[7] = -z[15] + z[22] + z[28] + z[30];
z[7] = abb[23] * z[7];
z[8] = abb[21] * z[27];
z[2] = -abb[8] + z[2] + z[29];
z[2] = 2 * z[2] + -z[5];
z[2] = abb[20] * z[2];
z[4] = abb[6] + -z[4];
z[4] = abb[24] * z[4];
z[5] = -abb[25] * z[23];
z[0] = z[0] + z[2] + z[4] + z[5] + z[6] + z[7] + z[8] + z[33];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[35] * z[3];
z[3] = -abb[33] * z[14];
z[4] = -abb[34] * z[20];
z[0] = abb[37] + z[0] + z[2] + z[3] + z[4];
z[2] = abb[17] * abb[36];
z[3] = -abb[23] + z[34];
z[3] = m1_set::bc<T>[0] * z[3];
z[3] = 2 * abb[33] + z[3];
z[3] = abb[13] * z[3];
z[0] = 2 * z[0] + z[2] + z[3];
z[0] = 4 * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_3_179_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_179_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_179_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("9.81128692593616145166810725220076368607416695431390552624287996"),stof<T>("-231.89047480199404580354954487794200553541977627822741870117052118")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({199});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,43> abb = {dl[0], dl[1], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W25(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W118(k,dv), dlog_W124(k,dv), dlog_W126(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_14(k), f_2_21(k), f_2_23(k), f_2_6_im(k), f_2_10_im(k), f_2_12_im(k), f_2_24_im(k), T{0}, f_2_6_re(k), f_2_10_re(k), f_2_12_re(k), f_2_24_re(k), T{0}};
{
auto c = dlog_W163(k,dv) * f_2_31(k);
abb[42] = c.real();
abb[37] = c.imag();
SpDLog_Sigma5<T,199,162>(k, dv, abb[42], abb[37], f_2_31_series_coefficients<T>);
}

                    
            return f_3_179_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_179_DLogXconstant_part(base_point<T>, kend);
	value += f_3_179_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_179_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_179_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_179_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_179_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_179_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_179_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
