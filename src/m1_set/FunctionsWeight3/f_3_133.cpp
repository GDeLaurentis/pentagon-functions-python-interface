/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_133.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_133_abbreviated (const std::array<T,24>& abb) {
T z[20];
z[0] = abb[5] + abb[7];
z[1] = abb[12] * z[0];
z[2] = abb[8] * abb[15];
z[3] = z[1] + z[2];
z[4] = abb[11] * (T(1) / T(2));
z[5] = 3 * abb[5];
z[6] = -abb[6] + -abb[7] + abb[8] + -z[5];
z[6] = z[4] * z[6];
z[7] = abb[5] + abb[8];
z[8] = 3 * abb[6];
z[9] = abb[7] + -z[7] + -z[8];
z[10] = abb[14] * (T(1) / T(2));
z[9] = z[9] * z[10];
z[11] = -abb[5] * abb[13];
z[12] = abb[13] + -abb[15];
z[13] = -abb[6] * z[12];
z[14] = abb[7] + abb[8];
z[15] = abb[10] * z[14];
z[6] = z[3] + z[6] + z[9] + z[11] + z[13] + (T(-1) / T(2)) * z[15];
z[6] = abb[10] * z[6];
z[5] = z[5] + z[8] + z[14];
z[4] = z[4] * z[5];
z[8] = abb[14] + z[12];
z[8] = abb[5] * z[8];
z[1] = -z[1] + z[4] + z[8];
z[1] = abb[14] * z[1];
z[8] = -abb[12] + z[12];
z[9] = abb[11] + z[8];
z[9] = abb[6] * z[9];
z[2] = -z[2] + z[9];
z[2] = abb[11] * z[2];
z[9] = prod_pow(abb[15], 2);
z[9] = -abb[22] + (T(1) / T(2)) * z[9];
z[9] = abb[5] * z[9];
z[11] = -abb[8] * abb[22];
z[13] = -abb[7] * abb[17];
z[15] = prod_pow(abb[12], 2);
z[15] = abb[17] + -abb[22] + (T(1) / T(2)) * z[15];
z[15] = abb[6] * z[15];
z[16] = -abb[6] + -z[0];
z[16] = abb[21] * z[16];
z[14] = -abb[5] + -abb[6] + -z[14];
z[14] = prod_pow(m1_set::bc<T>[0], 2) * z[14];
z[17] = abb[5] + -abb[8];
z[17] = abb[16] * z[17];
z[18] = -abb[0] + -abb[1] + abb[3] + abb[4];
z[18] = -abb[2] + -2 * abb[9] + (T(1) / T(2)) * z[18];
z[19] = -abb[23] * z[18];
z[1] = z[1] + z[2] + z[6] + z[9] + z[11] + z[13] + (T(1) / T(3)) * z[14] + z[15] + z[16] + z[17] + z[19];
z[2] = z[5] * z[10];
z[5] = abb[5] * z[12];
z[6] = abb[6] * z[8];
z[2] = z[2] + -z[3] + z[4] + z[5] + z[6];
z[2] = m1_set::bc<T>[0] * z[2];
z[0] = -abb[18] * z[0];
z[3] = -abb[19] * z[7];
z[4] = -abb[18] + -abb[19];
z[4] = abb[6] * z[4];
z[5] = -abb[20] * z[18];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_3_133_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_133_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_133_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("7.7143564516654021477305372678719799821591283604003080270983573661"),stof<T>("7.2516632689748497845188292938738884372507784963632055609905540863")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,24> abb = {dlog_W8(k,dl), dl[3], dl[4], dlog_W28(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_3_133_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_133_DLogXconstant_part(base_point<T>, kend);
	value += f_3_133_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_133_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_133_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_133_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_133_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_133_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_133_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
