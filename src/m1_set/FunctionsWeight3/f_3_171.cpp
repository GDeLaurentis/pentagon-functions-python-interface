/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_171.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_171_abbreviated (const std::array<T,52>& abb) {
T z[60];
z[0] = 4 * abb[11];
z[1] = 5 * abb[8];
z[2] = 3 * abb[4];
z[3] = -abb[3] + z[2];
z[4] = 2 * abb[5];
z[5] = abb[6] + abb[15];
z[6] = 2 * abb[9];
z[7] = z[0] + -z[1] + -z[3] + z[4] + z[5] + z[6];
z[8] = abb[47] * z[7];
z[9] = 2 * abb[11];
z[10] = -z[2] + z[9];
z[11] = abb[5] + z[5];
z[12] = 4 * abb[8];
z[13] = 2 * abb[3];
z[14] = -abb[14] + z[6];
z[11] = z[10] + 2 * z[11] + -z[12] + z[13] + z[14];
z[15] = abb[50] * z[11];
z[16] = 2 * abb[6];
z[17] = abb[5] + z[16];
z[18] = 4 * abb[9];
z[19] = abb[15] + z[18];
z[1] = -4 * abb[1] + 3 * abb[14] + z[1] + -z[10] + -2 * z[17] + -z[19];
z[17] = abb[44] * z[1];
z[20] = abb[5] + abb[15];
z[21] = 3 * abb[8];
z[20] = 3 * z[20] + -z[21];
z[22] = -abb[1] + abb[12];
z[23] = 3 * abb[3];
z[24] = 3 * abb[7];
z[25] = z[20] + -z[22] + z[23] + -z[24];
z[26] = abb[49] * z[25];
z[27] = -abb[5] + abb[14];
z[28] = 3 * abb[15];
z[29] = z[27] + z[28];
z[30] = abb[8] + z[2];
z[31] = z[29] + -z[30];
z[32] = 3 * abb[13];
z[33] = 4 * abb[10];
z[34] = z[32] + -z[33];
z[35] = 2 * abb[0];
z[36] = z[34] + -z[35];
z[37] = z[31] + z[36];
z[37] = abb[34] * z[37];
z[38] = z[4] + -z[28];
z[39] = 6 * abb[9];
z[40] = 2 * abb[16];
z[41] = z[39] + z[40];
z[42] = -abb[8] + z[38] + z[41];
z[43] = 4 * abb[14];
z[44] = z[42] + -z[43];
z[45] = abb[35] * z[44];
z[20] = -abb[0] + -6 * abb[2] + abb[10] + -z[20] + z[32];
z[32] = abb[45] * z[20];
z[46] = abb[17] + abb[18] + abb[19] + abb[20] + abb[21] + -3 * abb[22];
z[47] = abb[43] * z[46];
z[8] = z[8] + z[15] + -z[17] + z[26] + -z[32] + -z[37] + z[45] + -z[47];
z[15] = 2 * abb[1];
z[17] = z[15] + -z[40];
z[26] = 2 * abb[2];
z[32] = 4 * abb[0];
z[37] = z[26] + -z[32];
z[45] = abb[5] + z[28];
z[47] = 2 * abb[10];
z[48] = abb[14] + z[6] + -z[12] + -z[13] + z[17] + z[37] + z[45] + z[47];
z[48] = abb[23] * z[48];
z[37] = -z[16] + z[37];
z[17] = -z[17] + z[19] + -z[27] + -z[30] + z[34] + z[37];
z[17] = abb[24] * z[17];
z[17] = 2 * z[17] + z[48];
z[17] = abb[23] * z[17];
z[19] = 3 * abb[6];
z[30] = z[19] + -z[40];
z[34] = -abb[5] + z[30];
z[39] = -z[2] + -z[12] + -z[32] + z[33] + z[34] + z[39];
z[48] = prod_pow(abb[25], 2);
z[49] = -z[39] * z[48];
z[29] = -4 * abb[2] + -abb[8] + 6 * abb[13] + -z[3] + z[15] + -z[24] + z[29] + -z[32] + -z[33];
z[29] = abb[24] * z[29];
z[32] = -abb[8] + z[34] + -z[36];
z[33] = abb[25] * z[32];
z[34] = abb[25] * z[24];
z[33] = z[33] + z[34];
z[29] = z[29] + 2 * z[33];
z[29] = abb[24] * z[29];
z[33] = abb[12] + -abb[15];
z[50] = 3 * abb[5];
z[33] = 9 * abb[1] + 4 * abb[6] + -10 * abb[9] + -z[2] + z[13] + -7 * z[33] + -z[40] + z[43] + -z[50];
z[33] = abb[30] * z[33];
z[43] = -abb[3] + z[15];
z[51] = -abb[5] + abb[6];
z[52] = abb[15] + -abb[16] + -z[14] + z[43] + z[51];
z[53] = -abb[23] + abb[24];
z[52] = z[52] * z[53];
z[33] = z[33] + 4 * z[52];
z[33] = abb[30] * z[33];
z[52] = abb[1] + 5 * abb[12];
z[53] = 6 * abb[3];
z[54] = 6 * abb[11];
z[12] = -z[12] + z[27] + -z[28] + z[52] + -z[53] + z[54];
z[12] = abb[27] * z[12];
z[28] = -abb[23] * z[11];
z[22] = z[6] + z[22] + -2 * z[27];
z[22] = abb[30] * z[22];
z[55] = -abb[14] + z[5];
z[56] = abb[3] + abb[8] + -z[9] + z[55];
z[56] = abb[24] * z[56];
z[22] = z[22] + z[28] + z[56];
z[12] = z[12] + 2 * z[22];
z[12] = abb[27] * z[12];
z[22] = 2 * abb[8] + -abb[14] + z[2] + -z[24] + -z[30] + z[38];
z[22] = abb[24] * z[22];
z[28] = 4 * abb[12] + z[15] + -z[23];
z[30] = -z[24] + z[28];
z[38] = 2 * abb[15];
z[56] = abb[5] + -z[38];
z[56] = -5 * abb[14] + z[2] + z[30] + z[41] + 3 * z[56];
z[56] = abb[30] * z[56];
z[44] = -abb[23] * z[44];
z[30] = -z[30] + z[31];
z[30] = abb[27] * z[30];
z[31] = -abb[4] + abb[7];
z[14] = abb[6] + -abb[8] + z[14] + z[31];
z[57] = abb[25] * z[14];
z[22] = z[22] + z[30] + z[44] + z[56] + 3 * z[57];
z[19] = -6 * abb[1] + 7 * abb[14] + -z[19] + z[23] + -z[42];
z[19] = abb[29] * z[19];
z[19] = z[19] + 2 * z[22];
z[19] = abb[29] * z[19];
z[22] = abb[48] * z[14];
z[23] = -abb[4] + z[27];
z[27] = -abb[13] + -z[23] + z[35];
z[27] = abb[31] * z[27];
z[30] = abb[3] + -abb[4] + abb[13] + -z[26] + z[51];
z[30] = abb[33] * z[30];
z[23] = abb[7] + z[23] + -z[43];
z[23] = abb[32] * z[23];
z[22] = z[22] + z[23] + z[27] + z[30];
z[23] = z[38] + -z[51];
z[27] = 2 * abb[12];
z[30] = abb[1] + abb[11] + -z[13] + -z[23] + z[27];
z[30] = abb[27] * z[30];
z[42] = -z[23] + z[47];
z[43] = abb[0] + abb[3];
z[44] = 3 * abb[2] + -abb[11] + -z[42] + -z[43];
z[56] = abb[26] * z[44];
z[57] = abb[0] + abb[2];
z[42] = abb[1] + abb[9] + z[42] + -z[57];
z[42] = abb[23] * z[42];
z[23] = abb[3] + z[23];
z[27] = abb[9] + z[27];
z[58] = z[23] + -z[27];
z[58] = abb[30] * z[58];
z[43] = -abb[1] + -abb[2] + z[43];
z[59] = abb[24] * z[43];
z[30] = z[30] + z[42] + z[56] + z[58] + z[59];
z[42] = -abb[28] * z[43];
z[30] = 2 * z[30] + z[42];
z[42] = 2 * abb[28];
z[30] = z[30] * z[42];
z[43] = 12 * abb[2] + -10 * abb[10] + -z[2] + -z[35] + 3 * z[45] + -z[54];
z[43] = abb[26] * z[43];
z[3] = z[3] + z[9];
z[9] = -4 * abb[15] + z[3] + -z[36] + -z[51];
z[9] = abb[24] * z[9];
z[36] = 2 * z[44];
z[36] = -abb[23] * z[36];
z[9] = z[9] + z[36];
z[9] = 2 * z[9] + z[43];
z[9] = abb[26] * z[9];
z[36] = 2 * abb[46];
z[43] = z[32] * z[36];
z[44] = abb[0] + abb[10] + abb[14] + -abb[15];
z[44] = 2 * abb[4] + -abb[5] + -8 * abb[11] + abb[12] * (T(-16) / T(3)) + abb[6] * (T(-4) / T(3)) + abb[2] * (T(-2) / T(3)) + abb[8] * (T(22) / T(3)) + -z[15] + (T(1) / T(3)) * z[44] + z[53];
z[44] = prod_pow(m1_set::bc<T>[0], 2) * z[44];
z[36] = z[36] + -z[48];
z[36] = z[24] * z[36];
z[8] = -2 * z[8] + z[9] + z[12] + z[17] + z[19] + 6 * z[22] + z[29] + z[30] + z[33] + z[36] + z[43] + z[44] + z[49];
z[8] = 8 * z[8];
z[9] = 2 * abb[14];
z[0] = 8 * abb[3] + 5 * abb[15] + -z[0] + -z[2] + z[6] + -z[9] + z[16] + z[50] + -z[52];
z[0] = abb[27] * z[0];
z[2] = abb[25] * z[39];
z[6] = abb[1] + -z[55];
z[3] = abb[8] + z[3] + 2 * z[6] + -z[18] + -z[24] + -z[26] + z[35];
z[3] = abb[24] * z[3];
z[6] = z[9] + -z[40];
z[10] = 4 * abb[3] + abb[5] + -abb[15] + -z[6] + z[10] + -z[15] + -z[37] + -z[47];
z[10] = abb[23] * z[10];
z[12] = -z[38] + -z[51];
z[6] = -z[6] + 3 * z[12] + z[21] + z[28] + -6 * z[31];
z[6] = abb[29] * z[6];
z[4] = -z[4] + z[5] + z[9];
z[4] = 5 * abb[1] + -abb[12] + 2 * z[4] + -z[13] + -z[41];
z[4] = abb[30] * z[4];
z[5] = -abb[1] + -abb[10] + z[23];
z[5] = -abb[11] + 2 * z[5] + -z[27] + z[57];
z[5] = z[5] * z[42];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[10] + z[34] + 2 * z[56];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = -abb[39] * z[7];
z[3] = -abb[42] * z[11];
z[4] = -abb[41] * z[25];
z[1] = abb[36] * z[1];
z[5] = z[24] + z[32];
z[5] = abb[38] * z[5];
z[6] = abb[40] * z[14];
z[7] = abb[37] * z[20];
z[9] = -abb[51] * z[46];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + 3 * z[6] + z[7] + z[9];
z[0] = 16 * z[0];
return {z[8], z[0]};
}


template <typename T> std::complex<T> f_3_171_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_171_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_171_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-109.30485304357035631750849465705909903802702019986781261025097151"),stof<T>("-560.14570667504861489173407788892849518419006415974927713276798773")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,52> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W33(k,dl), dlog_W85(k,dl), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_15(k), f_2_16(k), f_2_17(k), f_2_18(k), f_2_2_im(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_9_im(k), f_2_12_im(k), f_2_22_im(k), f_2_27_im(k), f_2_2_re(k), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_9_re(k), f_2_12_re(k), f_2_22_re(k), f_2_27_re(k)};

                    
            return f_3_171_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_171_DLogXconstant_part(base_point<T>, kend);
	value += f_3_171_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_171_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_171_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_171_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_171_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_171_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_171_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
