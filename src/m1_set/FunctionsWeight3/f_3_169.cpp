/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_169.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_169_abbreviated (const std::array<T,44>& abb) {
T z[37];
z[0] = 4 * abb[4] + -9 * abb[8];
z[1] = 5 * abb[3];
z[2] = 4 * abb[5];
z[3] = 2 * abb[9];
z[4] = 4 * abb[1] + 2 * abb[2];
z[5] = z[0] + z[1] + z[2] + z[3] + -z[4];
z[6] = -abb[40] * z[5];
z[7] = 2 * abb[7];
z[8] = 2 * abb[12];
z[9] = 2 * abb[3];
z[10] = -abb[10] + -z[4] + z[7] + z[8] + z[9];
z[11] = -abb[4] + abb[5] + 6 * abb[6] + -5 * abb[11] + z[10];
z[12] = abb[34] * z[11];
z[13] = 2 * abb[5];
z[14] = -z[9] + z[13];
z[14] = abb[41] * z[14];
z[15] = -abb[4] + abb[10];
z[16] = abb[31] * z[15];
z[14] = z[14] + z[16];
z[16] = 2 * abb[6];
z[17] = 3 * abb[30] + -abb[31];
z[17] = z[16] * z[17];
z[18] = abb[11] + z[15];
z[19] = -abb[5] + z[18];
z[20] = 2 * abb[0];
z[21] = z[19] + -z[20];
z[21] = abb[33] * z[21];
z[22] = -z[9] + z[18];
z[23] = -abb[5] + z[16] + -z[22];
z[24] = abb[32] * z[23];
z[25] = -abb[13] * abb[43];
z[26] = abb[5] + -abb[11];
z[27] = abb[30] * z[26];
z[28] = -abb[11] + abb[12];
z[28] = prod_pow(abb[25], 2) * z[28];
z[29] = -abb[0] + -abb[6] + 2 * z[15];
z[29] = prod_pow(abb[26], 2) * z[29];
z[30] = abb[0] + abb[5] + -abb[12];
z[30] = 2 * z[30];
z[31] = abb[42] * z[30];
z[32] = -abb[16] + -abb[17];
z[32] = abb[35] * z[32];
z[33] = abb[14] * abb[43];
z[6] = -z[6] + z[12] + -2 * z[14] + -z[17] + -z[21] + z[24] + -z[25] + -6 * z[27] + -z[28] + -z[29] + -z[31] + -z[32] + z[33];
z[11] = -abb[22] * z[11];
z[12] = 8 * abb[1] + 4 * abb[2];
z[14] = -abb[9] + z[12];
z[17] = -5 * abb[0] + abb[3] + 4 * abb[6] + z[8] + -z[14] + z[19];
z[17] = abb[23] * z[17];
z[21] = abb[0] + z[26];
z[21] = abb[25] * z[21];
z[24] = -abb[0] + z[15];
z[24] = abb[26] * z[24];
z[21] = z[21] + -z[24];
z[23] = -abb[24] * z[23];
z[11] = z[11] + z[17] + 2 * z[21] + z[23];
z[3] = z[3] + z[8];
z[17] = -4 * abb[0] + -z[3] + z[12] + z[16] + z[19];
z[17] = abb[19] * z[17];
z[11] = 2 * z[11] + z[17];
z[11] = abb[19] * z[11];
z[17] = 2 * abb[1] + abb[2];
z[19] = 3 * abb[8];
z[23] = z[17] + -z[19];
z[25] = abb[4] + -abb[11];
z[27] = -abb[7] + z[9] + z[13] + z[23] + z[25];
z[27] = abb[22] * z[27];
z[28] = -abb[10] + z[17];
z[29] = 2 * abb[4];
z[31] = -abb[8] + z[28] + z[29];
z[31] = abb[24] * z[31];
z[21] = -z[21] + z[27] + z[31];
z[27] = 2 * abb[11];
z[32] = -abb[0] + z[27];
z[33] = abb[3] + -abb[9] + z[4];
z[34] = 2 * abb[10];
z[35] = z[16] + -z[32] + z[33] + -z[34];
z[35] = abb[19] * z[35];
z[36] = abb[0] + z[13];
z[33] = z[29] + -z[33] + z[36];
z[33] = abb[23] * z[33];
z[21] = 2 * z[21] + z[33] + z[35];
z[19] = z[4] + -z[19];
z[1] = -z[1] + z[2] + -z[19] + z[20];
z[1] = abb[20] * z[1];
z[1] = z[1] + 2 * z[21];
z[1] = abb[20] * z[1];
z[21] = -z[19] + -z[29] + z[34];
z[29] = abb[3] + z[16];
z[2] = -z[2] + z[7] + z[21] + z[27] + -z[29];
z[2] = abb[20] * z[2];
z[14] = abb[7] + abb[12] + -z[14] + z[29];
z[34] = abb[19] + -abb[23];
z[14] = z[14] * z[34];
z[34] = abb[10] + abb[11];
z[35] = -abb[3] + -abb[4] + 3 * abb[6] + abb[12] + -z[19] + -z[34];
z[35] = abb[22] * z[35];
z[26] = -abb[3] + abb[6] + abb[8] + z[26];
z[26] = abb[24] * z[26];
z[14] = z[14] + z[26] + z[35];
z[2] = z[2] + 2 * z[14];
z[14] = 3 * abb[3];
z[26] = z[13] + z[14];
z[27] = z[26] + -z[27];
z[35] = 6 * abb[7];
z[0] = 28 * abb[1] + 14 * abb[2] + -10 * abb[6] + z[0] + -z[8] + z[27] + -z[35];
z[0] = abb[21] * z[0];
z[0] = z[0] + 2 * z[2];
z[0] = abb[21] * z[0];
z[2] = z[7] + -z[9] + z[17] + z[25];
z[2] = abb[29] * z[2];
z[17] = abb[2] + -abb[11];
z[17] = 6 * abb[1] + -abb[9] + -abb[10] + 3 * z[17] + z[29];
z[17] = abb[28] * z[17];
z[29] = abb[0] + abb[7] + -abb[11];
z[29] = abb[27] * z[29];
z[2] = z[2] + z[17] + z[29];
z[17] = 3 * abb[5];
z[16] = z[9] + -z[16] + -z[17] + z[18];
z[16] = abb[24] * z[16];
z[18] = abb[6] + -z[15];
z[18] = abb[26] * z[18];
z[16] = z[16] + 4 * z[18];
z[16] = abb[24] * z[16];
z[18] = 4 * abb[3];
z[4] = -3 * abb[4] + -z[4] + -z[17] + z[18] + z[34];
z[4] = abb[24] * z[4];
z[17] = -z[17] + z[22];
z[8] = -z[8] + z[17] + z[35];
z[8] = abb[22] * z[8];
z[4] = 2 * z[4] + z[8];
z[4] = abb[22] * z[4];
z[8] = -5 * abb[5] + z[10] + -z[25];
z[8] = abb[22] * z[8];
z[10] = -abb[11] + -abb[12] + z[13] + z[20];
z[10] = abb[25] * z[10];
z[10] = z[10] + -z[24];
z[9] = -abb[5] + -abb[11] + z[9] + z[15];
z[9] = abb[24] * z[9];
z[8] = z[8] + z[9] + 2 * z[10];
z[9] = -6 * abb[0] + z[12] + z[17];
z[9] = abb[23] * z[9];
z[8] = 2 * z[8] + z[9];
z[8] = abb[23] * z[8];
z[9] = abb[2] + abb[4];
z[9] = -abb[0] + -6 * abb[8] + abb[9] * (T(-4) / T(3)) + abb[5] * (T(7) / T(3)) + abb[1] * (T(10) / T(3)) + (T(5) / T(3)) * z[9] + z[14] + (T(1) / T(3)) * z[34];
z[9] = prod_pow(m1_set::bc<T>[0], 2) * z[9];
z[10] = -abb[15] + -abb[18];
z[12] = 2 * abb[35];
z[10] = z[10] * z[12];
z[0] = z[0] + z[1] + 4 * z[2] + z[4] + -2 * z[6] + z[8] + z[9] + z[10] + z[11] + z[16];
z[0] = 4 * z[0];
z[1] = abb[4] + abb[12] + -z[7] + z[23] + z[27];
z[1] = abb[22] * z[1];
z[2] = -abb[12] + z[32];
z[4] = -z[2] + z[28];
z[4] = abb[19] * z[4];
z[2] = -abb[5] + z[2];
z[2] = abb[25] * z[2];
z[1] = z[1] + z[2] + z[4] + z[24] + z[31];
z[2] = abb[7] + abb[11];
z[2] = 4 * z[2] + -z[3] + z[21] + -z[26];
z[2] = abb[21] * z[2];
z[3] = abb[9] + -z[18] + -z[19] + z[36];
z[3] = abb[20] * z[3];
z[1] = 2 * z[1] + z[2] + z[3] + z[33];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[36] * z[5];
z[3] = -abb[13] + -abb[14];
z[3] = abb[39] * z[3];
z[4] = -abb[3] + abb[5];
z[4] = abb[37] * z[4];
z[5] = abb[38] * z[30];
z[1] = z[1] + z[2] + z[3] + 4 * z[4] + z[5];
z[1] = 8 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_169_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_169_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_169_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("74.154641676792018346869118468696445371683790093427244811691426156"),stof<T>("55.547468842577761992396315371334888965067282070012368742228760553")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,44> abb = {dl[0], dl[1], dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W21(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W75(k,dl), dlog_W120(k,dv), dlog_W126(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_15(k), f_2_18(k), f_2_20(k), f_2_21(k), f_2_29_im(k), f_2_6_im(k), f_2_9_im(k), f_2_10_im(k), f_2_24_im(k), f_2_6_re(k), f_2_9_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_3_169_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_169_DLogXconstant_part(base_point<T>, kend);
	value += f_3_169_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_169_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_169_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_169_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_169_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_169_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_169_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
