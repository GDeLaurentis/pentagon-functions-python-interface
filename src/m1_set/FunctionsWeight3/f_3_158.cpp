/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_158.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_158_abbreviated (const std::array<T,44>& abb) {
T z[39];
z[0] = 2 * abb[12];
z[1] = 2 * abb[8];
z[2] = z[0] + z[1];
z[3] = 3 * abb[7];
z[4] = 2 * abb[3];
z[5] = z[3] + z[4];
z[6] = abb[5] + z[5];
z[7] = 3 * abb[6];
z[8] = z[6] + z[7];
z[9] = -z[2] + z[8];
z[9] = abb[24] * z[9];
z[10] = 2 * abb[4];
z[11] = 3 * abb[2];
z[12] = z[10] + z[11];
z[2] = 3 * abb[3] + -abb[5] + -4 * abb[7] + z[2] + -z[12];
z[2] = abb[23] * z[2];
z[13] = abb[5] + abb[7];
z[14] = -abb[4] + abb[12];
z[15] = -z[1] + z[13] + z[14];
z[16] = abb[18] * z[15];
z[17] = -abb[3] + abb[4];
z[18] = 2 * abb[6];
z[19] = z[17] + -z[18];
z[20] = 2 * abb[7];
z[21] = -abb[5] + 3 * abb[8] + z[19] + -z[20];
z[21] = abb[22] * z[21];
z[2] = z[2] + z[9] + z[16] + z[21];
z[2] = abb[22] * z[2];
z[9] = -abb[12] + z[11];
z[16] = 3 * abb[13];
z[1] = -4 * abb[1] + z[1] + -z[7] + -z[9] + z[16];
z[1] = abb[26] * z[1];
z[4] = 5 * abb[5] + abb[7] + z[4];
z[21] = 4 * abb[10];
z[22] = -abb[4] + z[7];
z[23] = 3 * abb[11];
z[24] = z[4] + z[21] + -z[22] + -z[23];
z[25] = abb[41] * z[24];
z[26] = 4 * abb[9];
z[27] = -z[16] + z[26];
z[11] = abb[4] + z[11];
z[28] = -abb[5] + abb[6];
z[5] = z[5] + -z[11] + -z[27] + -z[28];
z[29] = abb[33] * z[5];
z[30] = -abb[7] + z[16];
z[31] = 7 * abb[3];
z[32] = 3 * abb[9];
z[33] = -3 * abb[1] + -z[22] + z[30] + -z[31] + z[32];
z[34] = abb[43] * z[33];
z[35] = 4 * abb[8];
z[4] = z[4] + -z[9] + z[35];
z[4] = abb[28] * z[4];
z[9] = 2 * abb[10];
z[27] = -abb[4] + -z[9] + z[27];
z[36] = abb[40] * z[27];
z[37] = -abb[15] + -3 * abb[16] + -abb[17];
z[37] = abb[35] * z[37];
z[38] = -7 * abb[28] + -abb[40];
z[38] = abb[6] * z[38];
z[1] = z[1] + z[2] + z[4] + z[25] + -z[29] + z[34] + z[36] + z[37] + z[38];
z[2] = z[6] + -z[9];
z[4] = -z[2] + z[12] + z[18] + -z[23];
z[25] = -abb[18] * z[4];
z[3] = -4 * abb[5] + -z[3] + z[9] + -z[19];
z[3] = abb[23] * z[3];
z[19] = 5 * abb[7];
z[29] = 4 * abb[6];
z[34] = -4 * abb[3] + -7 * abb[5] + z[11] + -z[19] + z[29];
z[34] = abb[22] * z[34];
z[36] = -abb[6] + z[9] + -z[13];
z[37] = -abb[24] * z[36];
z[3] = z[3] + z[25] + z[34] + z[37];
z[25] = 6 * abb[11];
z[34] = abb[3] + abb[6];
z[9] = -10 * abb[5] + z[9] + -z[12] + -z[19] + z[25] + -z[34];
z[9] = abb[19] * z[9];
z[3] = 2 * z[3] + z[9];
z[3] = abb[19] * z[3];
z[9] = 5 * abb[3];
z[12] = -abb[5] + z[9] + -z[30];
z[19] = -abb[1] + z[32];
z[30] = 2 * abb[0];
z[11] = z[7] + z[11] + 2 * z[12] + -z[19] + z[30];
z[11] = abb[23] * z[11];
z[12] = abb[1] + -abb[9];
z[32] = abb[6] + -z[12];
z[9] = abb[4] + abb[7] + -z[0] + -z[9] + z[16] + -z[32];
z[9] = abb[25] * z[9];
z[16] = 2 * abb[9];
z[34] = -abb[7] + z[16] + -z[34];
z[37] = -abb[0] + abb[1] + -z[34];
z[37] = abb[24] * z[37];
z[9] = z[9] + 2 * z[37];
z[9] = 2 * z[9] + z[11];
z[9] = abb[23] * z[9];
z[11] = 2 * abb[1];
z[0] = z[0] + -z[5] + -z[11] + -z[30];
z[0] = abb[23] * z[0];
z[5] = abb[12] + -z[11] + z[34];
z[34] = -abb[25] * z[5];
z[37] = abb[0] + abb[1] + -abb[12];
z[37] = abb[24] * z[37];
z[34] = z[34] + z[37];
z[0] = z[0] + 2 * z[34];
z[7] = -z[7] + -z[11] + z[30];
z[13] = z[7] + z[10] + z[13];
z[13] = abb[18] * z[13];
z[0] = 2 * z[0] + z[13];
z[0] = abb[18] * z[0];
z[13] = abb[19] * z[24];
z[20] = abb[3] + z[20];
z[34] = -abb[8] + -abb[10] + -abb[12] + z[20];
z[37] = 3 * abb[0] + -z[28] + z[34];
z[38] = -abb[18] + abb[23];
z[37] = z[37] * z[38];
z[34] = -abb[5] + -z[18] + -z[34];
z[34] = abb[24] * z[34];
z[34] = z[34] + z[37];
z[13] = z[13] + 2 * z[34];
z[20] = abb[5] + z[20];
z[34] = -5 * abb[10] + 2 * z[20];
z[22] = 12 * abb[0] + -6 * abb[8] + -3 * abb[12] + z[22] + 2 * z[34];
z[22] = abb[20] * z[22];
z[13] = 2 * z[13] + z[22];
z[13] = abb[20] * z[13];
z[22] = 4 * abb[0];
z[8] = z[8] + -z[11] + -z[22];
z[8] = prod_pow(abb[24], 2) * z[8];
z[5] = abb[24] * z[5];
z[12] = -abb[6] + -5 * z[12] + z[14];
z[12] = abb[25] * z[12];
z[5] = 4 * z[5] + z[12];
z[5] = abb[25] * z[5];
z[12] = abb[2] + z[17];
z[34] = abb[7] + -abb[12] + z[12];
z[34] = abb[29] * z[34];
z[12] = abb[5] + -abb[11] + z[12];
z[37] = abb[31] * z[12];
z[34] = z[34] + -z[37];
z[25] = abb[40] * z[25];
z[37] = -abb[0] + abb[6];
z[38] = abb[42] * z[37];
z[0] = z[0] + 2 * z[1] + z[3] + z[5] + z[8] + z[9] + z[13] + z[25] + 6 * z[34] + -12 * z[38];
z[1] = abb[32] * z[4];
z[3] = abb[34] * z[15];
z[1] = z[1] + -z[3];
z[3] = abb[2] + -abb[11] + -z[14] + z[30];
z[3] = abb[27] * z[3];
z[4] = abb[14] * abb[35];
z[5] = abb[2] + abb[3] + -abb[13] + z[28];
z[8] = abb[30] * z[5];
z[3] = -z[3] + z[4] + -z[8];
z[4] = -28 * abb[0] + abb[1] + 32 * abb[10] + abb[7] * (T(5) / T(2)) + abb[5] * (T(19) / T(2)) + z[10] + z[26] + -z[31];
z[4] = abb[6] * (T(-9) / T(2)) + (T(1) / T(3)) * z[4];
z[4] = prod_pow(m1_set::bc<T>[0], 2) * z[4];
z[5] = -abb[23] * z[5];
z[8] = abb[19] * z[12];
z[5] = z[5] + z[8];
z[8] = -z[17] + z[28];
z[8] = abb[21] * z[8];
z[5] = 2 * z[5] + z[8];
z[5] = abb[21] * z[5];
z[0] = 2 * z[0] + -4 * z[1] + -12 * z[3] + z[4] + 6 * z[5];
z[1] = abb[3] + abb[7];
z[3] = abb[10] + z[1];
z[3] = 2 * z[3] + z[18] + -z[19] + -z[22];
z[3] = abb[23] * z[3];
z[4] = abb[4] + abb[12];
z[5] = -abb[10] + z[16];
z[5] = -abb[6] + -z[4] + 2 * z[5] + -z[11] + z[22];
z[5] = abb[18] * z[5];
z[2] = z[2] + -z[7] + -z[26];
z[2] = abb[24] * z[2];
z[1] = -2 * z[1] + z[4] + -z[32];
z[1] = abb[25] * z[1];
z[4] = z[4] + z[35];
z[6] = -z[4] + z[6] + z[29];
z[6] = abb[22] * z[6];
z[7] = -z[20] + z[21];
z[4] = -6 * abb[0] + -5 * abb[6] + z[4] + 2 * z[7];
z[4] = abb[20] * z[4];
z[7] = -abb[19] * z[36];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[39] * z[33];
z[3] = abb[37] * z[24];
z[4] = -abb[6] + z[23] + z[27];
z[4] = abb[36] * z[4];
z[5] = abb[38] * z[37];
z[1] = z[1] + z[2] + z[3] + z[4] + -6 * z[5];
z[1] = 4 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_158_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_158_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_158_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("63.326364531143290699528102245036865465482108191882495582449464273"),stof<T>("28.247091478452608822937013829452508951426840828444132665594561723")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,44> abb = {dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W21(k,dl), dlog_W26(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_19(k), f_2_21(k), f_2_28_im(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_11_im(k), f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_11_re(k)};

                    
            return f_3_158_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_158_DLogXconstant_part(base_point<T>, kend);
	value += f_3_158_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_158_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_158_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_158_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_158_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_158_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_158_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
