/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_187.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_187_abbreviated (const std::array<T,48>& abb) {
T z[45];
z[0] = -abb[18] + abb[21];
z[1] = -abb[19] + z[0];
z[2] = 2 * abb[10];
z[1] = z[1] * z[2];
z[3] = abb[3] * abb[19];
z[4] = z[1] + z[3];
z[5] = abb[0] + abb[13];
z[6] = z[0] * z[5];
z[7] = abb[18] + -abb[22];
z[8] = abb[14] * z[7];
z[9] = 3 * abb[22];
z[10] = 4 * abb[18] + -z[9];
z[11] = -abb[21] + z[10];
z[11] = abb[3] * z[11];
z[12] = 2 * z[7];
z[13] = abb[4] * z[12];
z[14] = abb[19] + abb[21];
z[15] = abb[22] + -z[14];
z[15] = abb[8] * z[15];
z[16] = abb[19] + -abb[21] + abb[22];
z[16] = abb[6] * z[16];
z[17] = 12 * abb[18] + abb[19] + -5 * abb[21] + -7 * abb[22];
z[17] = abb[1] * z[17];
z[6] = z[4] + z[6] + -2 * z[8] + z[11] + z[13] + z[15] + z[16] + z[17];
z[11] = abb[6] + -abb[8];
z[13] = -8 * abb[1] + -4 * abb[3] + -abb[4] + 5 * abb[10] + -z[11];
z[13] = abb[0] + 2 * z[13];
z[13] = abb[20] * z[13];
z[6] = 2 * z[6] + z[13];
z[6] = abb[20] * z[6];
z[13] = 2 * abb[24];
z[9] = abb[18] + -z[9] + z[13];
z[9] = -abb[21] + 2 * z[9];
z[9] = abb[21] * z[9];
z[15] = 2 * abb[22];
z[16] = -abb[24] + z[15];
z[17] = abb[24] * z[16];
z[18] = prod_pow(abb[25], 2);
z[19] = prod_pow(abb[22], 2);
z[16] = -abb[18] + 2 * z[16];
z[16] = abb[18] * z[16];
z[9] = z[9] + z[16] + 3 * z[17] + -z[18] + -4 * z[19];
z[9] = abb[3] * z[9];
z[16] = 4 * abb[22];
z[20] = 3 * abb[18];
z[21] = z[16] + -z[20];
z[21] = abb[18] * z[21];
z[22] = 2 * abb[18] + -abb[24];
z[23] = z[14] + -z[22];
z[23] = abb[19] * z[23];
z[24] = 2 * z[19];
z[25] = abb[22] * abb[24];
z[22] = -abb[22] + z[22];
z[22] = abb[21] * z[22];
z[21] = z[21] + z[22] + z[23] + -z[24] + z[25];
z[22] = 2 * abb[1];
z[21] = z[21] * z[22];
z[16] = -z[16] + -z[20];
z[16] = abb[18] * z[16];
z[23] = 2 * abb[23];
z[25] = -abb[18] + abb[20];
z[26] = -abb[25] + -z[25];
z[26] = abb[23] + 2 * z[26];
z[26] = z[23] * z[26];
z[27] = abb[28] + abb[29];
z[28] = 7 * abb[20] + -4 * z[7];
z[28] = abb[20] * z[28];
z[16] = -abb[26] + z[16] + 2 * z[18] + z[26] + 4 * z[27] + z[28];
z[16] = abb[7] * z[16];
z[17] = z[17] + -z[24];
z[24] = prod_pow(abb[18], 2);
z[12] = abb[25] + z[12];
z[12] = abb[25] * z[12];
z[26] = -abb[25] + -z[7];
z[26] = abb[21] + 2 * z[26];
z[26] = abb[21] * z[26];
z[12] = z[12] + -z[17] + -z[24] + z[26];
z[12] = abb[5] * z[12];
z[26] = abb[5] + abb[11];
z[27] = abb[0] + -abb[4];
z[28] = 3 * abb[3];
z[29] = 2 * abb[8];
z[30] = -abb[6] + -z[26] + -z[27] + z[28] + z[29];
z[30] = 2 * z[30];
z[31] = abb[43] * z[30];
z[32] = prod_pow(abb[24], 2);
z[33] = -abb[22] + abb[24];
z[34] = abb[18] * z[33];
z[19] = z[19] + -z[32] + z[34];
z[19] = abb[11] * z[19];
z[32] = abb[18] * z[8];
z[34] = -abb[3] + 3 * abb[5] + -z[27];
z[34] = abb[28] * z[34];
z[19] = abb[47] + z[19] + z[32] + z[34];
z[17] = z[17] + z[18];
z[32] = abb[18] + abb[22];
z[34] = 3 * abb[21];
z[35] = -2 * z[32] + z[34];
z[35] = abb[21] * z[35];
z[36] = -abb[18] + z[15];
z[36] = abb[18] * z[36];
z[35] = z[17] + z[35] + z[36];
z[35] = abb[4] * z[35];
z[36] = -abb[18] * abb[21];
z[37] = -abb[19] * z[0];
z[36] = z[24] + z[36] + z[37];
z[36] = abb[13] * z[36];
z[15] = -abb[21] + z[15];
z[15] = abb[21] * z[15];
z[15] = z[15] + -z[17] + 2 * z[24];
z[15] = abb[0] * z[15];
z[17] = abb[21] + -abb[24];
z[3] = 2 * z[3];
z[24] = -z[3] * z[17];
z[37] = -z[17] * z[33];
z[17] = abb[19] * z[17];
z[38] = z[17] + -z[37];
z[38] = z[29] * z[38];
z[17] = z[17] + 2 * z[37];
z[37] = 2 * abb[6];
z[17] = z[17] * z[37];
z[2] = abb[4] + -abb[14] + z[2] + z[22];
z[39] = -abb[6] + z[2];
z[39] = 4 * z[39];
z[40] = -abb[41] * z[39];
z[41] = abb[15] + abb[16];
z[41] = 2 * z[41];
z[42] = abb[45] * z[41];
z[43] = abb[17] * abb[44];
z[44] = -abb[3] + abb[14];
z[44] = -9 * abb[1] + -abb[4] + abb[13] + 2 * z[44];
z[44] = 3 * abb[0] + 2 * z[44];
z[44] = abb[26] * z[44];
z[6] = z[6] + z[9] + z[12] + z[15] + z[16] + z[17] + 2 * z[19] + z[21] + z[24] + z[31] + z[35] + z[36] + z[38] + z[40] + z[42] + z[43] + z[44];
z[9] = -abb[0] + -abb[2];
z[12] = abb[21] + -abb[25];
z[9] = z[9] * z[12];
z[15] = 2 * abb[21];
z[16] = -2 * abb[25] + -z[7] + z[15];
z[16] = abb[5] * z[16];
z[10] = -abb[19] + -abb[24] + -z[10] + z[15];
z[10] = abb[1] * z[10];
z[11] = 4 * abb[10] + -z[11];
z[17] = 3 * abb[1] + -z[11] + z[28];
z[17] = abb[20] * z[17];
z[19] = -abb[22] + z[20];
z[20] = abb[21] + abb[25] + -z[19];
z[20] = abb[3] * z[20];
z[21] = -abb[11] * z[33];
z[24] = abb[22] + -abb[25] + z[0];
z[24] = abb[4] * z[24];
z[31] = abb[19] + z[33];
z[35] = abb[8] * z[31];
z[36] = -abb[19] + z[33];
z[36] = abb[6] * z[36];
z[27] = abb[1] + 2 * abb[5] + -abb[10] + -z[27];
z[27] = abb[23] * z[27];
z[4] = -z[4] + z[8] + z[9] + z[10] + z[16] + z[17] + z[20] + z[21] + z[24] + z[27] + z[35] + z[36];
z[4] = abb[23] * z[4];
z[8] = abb[21] * abb[25];
z[8] = -abb[28] + z[8] + -z[18];
z[8] = abb[2] * z[8];
z[9] = abb[8] + z[22];
z[5] = -z[5] + 2 * z[9];
z[9] = abb[40] * z[5];
z[10] = abb[19] + z[0];
z[10] = -abb[19] * z[10];
z[16] = abb[20] + z[0];
z[17] = abb[20] * z[16];
z[10] = abb[40] + z[10] + z[17];
z[10] = abb[12] * z[10];
z[17] = -abb[2] + abb[5];
z[17] = abb[27] * z[17];
z[4] = z[4] + z[8] + -z[9] + z[10] + z[17];
z[8] = abb[4] + abb[11];
z[9] = abb[14] * (T(2) / T(3)) + z[28];
z[8] = -abb[5] + abb[10] * (T(-16) / T(3)) + abb[6] * (T(-5) / T(3)) + (T(1) / T(3)) * z[8] + 2 * z[9];
z[9] = -abb[2] + abb[12];
z[8] = abb[0] * (T(-13) / T(3)) + abb[13] * (T(4) / T(3)) + abb[1] * (T(7) / T(3)) + 2 * z[8] + (T(-2) / T(3)) * z[9];
z[8] = prod_pow(m1_set::bc<T>[0], 2) * z[8];
z[9] = -abb[3] + abb[10];
z[10] = -abb[1] + z[9];
z[10] = abb[29] * z[10];
z[9] = 16 * z[9];
z[17] = abb[39] * z[9];
z[18] = -abb[3] + abb[6];
z[20] = abb[42] * z[18];
z[4] = abb[46] + 8 * z[4] + 4 * z[6] + 2 * z[8] + 16 * z[10] + z[17] + -24 * z[20];
z[6] = 2 * abb[3];
z[8] = abb[14] + z[6] + -z[11] + z[22];
z[8] = abb[13] + 2 * z[8];
z[8] = abb[20] * z[8];
z[10] = -abb[21] + z[13] + -z[32];
z[6] = z[6] * z[10];
z[10] = abb[18] + z[12] + -z[33];
z[10] = abb[5] * z[10];
z[11] = z[29] * z[31];
z[12] = z[15] + -z[19];
z[12] = z[12] * z[22];
z[13] = -abb[14] * abb[22];
z[15] = abb[24] + -z[7];
z[15] = abb[11] * z[15];
z[0] = abb[24] + z[0];
z[0] = abb[4] * z[0];
z[17] = abb[22] + -4 * abb[24] + z[34];
z[17] = abb[6] * z[17];
z[7] = -abb[21] + -abb[24] + -z[7];
z[7] = abb[0] * z[7];
z[0] = z[0] + -z[1] + -z[3] + z[6] + z[7] + z[8] + z[10] + z[11] + z[12] + z[13] + z[15] + z[17];
z[1] = z[2] + z[26] + -z[37];
z[1] = z[1] * z[23];
z[2] = 2 * abb[12];
z[3] = -abb[19] + -z[16];
z[3] = z[2] * z[3];
z[6] = -abb[13] * z[14];
z[0] = 2 * z[0] + z[1] + z[3] + z[6];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[34] * z[30];
z[2] = z[2] + -2 * z[5];
z[2] = abb[31] * z[2];
z[3] = -abb[32] * z[39];
z[5] = abb[17] * abb[35];
z[6] = abb[36] * z[41];
z[7] = abb[33] * z[18];
z[0] = 2 * abb[38] + z[0] + z[1] + z[2] + z[3] + z[5] + z[6] + -6 * z[7];
z[1] = abb[7] * m1_set::bc<T>[0] * z[25];
z[2] = abb[30] * z[9];
z[0] = abb[37] + 4 * z[0] + -40 * z[1] + z[2];
return {z[4], z[0]};
}


template <typename T> std::complex<T> f_3_187_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {stof<T>("-315.82734083485947580270371199603683633003838103170530004522718004")};
	
	std::vector<C> intdlogs = {(log2(k.W[164]) - log2(kbase.W[164]))};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_187_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_3_187_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(2)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (-24 + -15 * v[0] + -5 * v[1] + -3 * v[2] + 9 * v[3] + 12 * v[4] + 6 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + 3 * v[5])) / prod_pow(tend, 2);
c[1] = (-4 * (-1 + 3 * m1_set::bc<T>[1]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[2];
z[0] = abb[18] + -abb[22];
z[1] = abb[25] + -z[0];
z[1] = abb[25] * z[1];
z[0] = -abb[23] + z[0];
z[0] = abb[23] * z[0];
z[0] = -abb[28] + z[0] + z[1];
return 8 * abb[9] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_187_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (-m1_set::bc<T>[0] * (v[0] + -1 * v[1] + v[2] + v[3] + -1 * v[5]) * (4 + v[0] + v[1] + v[2] + -1 * v[3] + -2 * v[4] + -1 * v[5])) / prod_pow(tend, 2);
c[1] = (-4 * m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = -abb[23] + abb[25];
return 8 * abb[9] * m1_set::bc<T>[0] * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_187_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-31.737775073355003260632849371930008327564453074960916425721120264"),stof<T>("-127.230279665719475680056474229810324087015406742971023028044421874")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 200});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,48> abb = {dl[0], dl[1], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W17(k,dl), dlog_W24(k,dl), dlog_W26(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W119(k,dv), dlog_W125(k,dv), dlog_W126(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_3(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), f_2_25_im(k), T{0}, T{0}, f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k), f_2_25_re(k), T{0}, T{0}};
abb[37] = SpDLog_f_3_187_W_17_Im(t, path, abb);
abb[46] = SpDLog_f_3_187_W_17_Re(t, path, abb);
{
auto c = dlog_W165(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[47] = c.real();
abb[38] = c.imag();
SpDLog_Sigma5<T,200,164>(k, dv, abb[47], abb[38], f_2_32_series_coefficients<T>);
}

                    
            return f_3_187_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_187_DLogXconstant_part(base_point<T>, kend);
	value += f_3_187_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_187_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_187_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_187_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_187_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_187_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_187_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
