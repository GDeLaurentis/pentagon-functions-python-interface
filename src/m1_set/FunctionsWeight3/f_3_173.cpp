/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_173.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_173_abbreviated (const std::array<T,25>& abb) {
T z[28];
z[0] = 3 * abb[6];
z[1] = 2 * abb[4];
z[2] = z[0] + z[1];
z[3] = abb[1] + z[2];
z[4] = 6 * abb[9];
z[5] = 3 * abb[5];
z[6] = z[4] + -z[5];
z[7] = 2 * abb[2] + -3 * abb[8];
z[8] = 3 * abb[3];
z[9] = 2 * abb[0];
z[10] = z[3] + -z[6] + z[7] + -z[8] + z[9];
z[11] = prod_pow(abb[13], 2);
z[10] = z[10] * z[11];
z[12] = 5 * abb[8];
z[3] = abb[3] + -3 * abb[9] + z[3] + z[5] + -z[12];
z[3] = abb[18] * z[3];
z[13] = 4 * abb[9];
z[14] = 2 * abb[10];
z[5] = -z[5] + z[13] + z[14];
z[8] = -abb[0] + -z[1] + z[8];
z[15] = 4 * abb[2] + -z[5] + -z[8];
z[15] = abb[17] * z[15];
z[3] = z[3] + z[15];
z[15] = -abb[2] + abb[3];
z[16] = 2 * abb[9];
z[17] = -abb[0] + z[16];
z[18] = 2 * abb[5];
z[19] = abb[1] + z[1];
z[20] = 2 * z[15] + z[17] + z[18] + -z[19];
z[20] = abb[11] * z[20];
z[21] = -abb[2] + abb[6];
z[22] = abb[4] + abb[9];
z[21] = abb[3] + 2 * z[21] + -z[22];
z[21] = abb[15] * z[21];
z[23] = 2 * abb[6];
z[24] = 2 * abb[8];
z[25] = z[23] + -z[24];
z[26] = z[15] + z[25];
z[27] = abb[5] + -abb[9] + z[26];
z[27] = abb[13] * z[27];
z[27] = -z[21] + z[27];
z[20] = z[20] + 4 * z[27];
z[20] = abb[11] * z[20];
z[27] = -abb[2] + z[23];
z[8] = -z[6] + z[8] + 4 * z[27];
z[8] = abb[15] * z[8];
z[6] = -abb[0] + abb[3] + -4 * abb[6] + z[6];
z[27] = 2 * abb[13];
z[6] = z[6] * z[27];
z[6] = z[6] + z[8];
z[6] = abb[15] * z[6];
z[8] = -abb[15] + z[27];
z[8] = abb[15] * z[8];
z[8] = -abb[18] + z[8] + -z[11];
z[8] = z[8] * z[14];
z[3] = 2 * z[3] + z[6] + z[8] + z[10] + z[20];
z[6] = 4 * abb[4];
z[8] = -3 * abb[2] + abb[3] + z[0] + -z[6] + -z[9] + z[12] + -z[16];
z[9] = abb[22] * z[8];
z[10] = abb[0] + -abb[5] + -z[15] + z[22] + -z[23];
z[10] = abb[11] * z[10];
z[11] = -z[22] + z[24];
z[12] = abb[0] + -abb[1];
z[14] = abb[2] + -z[11] + -z[12];
z[14] = abb[13] * z[14];
z[10] = z[10] + z[14] + z[21];
z[14] = z[12] + z[15];
z[14] = abb[12] * z[14];
z[10] = 2 * z[10] + z[14];
z[10] = abb[12] * z[10];
z[1] = -5 * abb[0] + 4 * abb[1] + -abb[3] + -z[1] + z[5];
z[1] = abb[16] * z[1];
z[1] = -z[1] + z[9] + -z[10];
z[5] = (T(-1) / T(3)) * z[12] + (T(-7) / T(3)) * z[15] + -z[25];
z[5] = prod_pow(m1_set::bc<T>[0], 2) * z[5];
z[1] = -4 * z[1] + 2 * z[3] + z[5];
z[3] = 11 * abb[8];
z[5] = 2 * abb[3];
z[9] = 3 * abb[1];
z[0] = -z[0] + z[3] + -z[5] + -z[6] + -z[9] + -z[17];
z[6] = abb[23] * z[0];
z[10] = 7 * abb[8] + -z[2];
z[5] = -z[5] + z[10] + z[12] + -z[18];
z[5] = abb[13] * z[5];
z[14] = 3 * abb[0];
z[9] = 4 * abb[8] + -z[9] + -z[13] + z[14];
z[9] = abb[11] * z[9];
z[11] = abb[5] + z[11];
z[13] = -abb[1] + z[11];
z[20] = 2 * abb[12];
z[13] = z[13] * z[20];
z[5] = z[5] + z[9] + z[13];
z[2] = -6 * abb[0] + 8 * abb[1] + z[2] + -z[3] + z[4] + -z[18];
z[2] = abb[14] * z[2];
z[2] = z[2] + 2 * z[5];
z[2] = abb[14] * z[2];
z[1] = abb[24] + 4 * z[1] + 8 * z[2] + 16 * z[6];
z[0] = abb[20] * z[0];
z[2] = -5 * abb[1] + z[10] + z[14] + -z[16] + z[18];
z[2] = abb[14] * z[2];
z[3] = -abb[0] + 2 * abb[1] + -z[11] + -z[15];
z[3] = abb[11] * z[3];
z[3] = z[3] + z[21];
z[4] = -abb[6] + z[7] + z[17] + z[19];
z[4] = abb[13] * z[4];
z[5] = -z[12] + z[26];
z[5] = z[5] * z[20];
z[2] = z[2] + 2 * z[3] + z[4] + z[5];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = -abb[19] * z[8];
z[0] = z[0] + z[2] + z[3];
z[0] = abb[21] + 16 * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_3_173_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_173_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_3_173_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4]) * (3 * m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]) + -8 * (10 + 7 * v[0] + v[1] + 6 * v[2] + -6 * v[4] + -5 * v[5]))) / prod_pow(tend, 2);
c[1] = (12 * (-2 + 3 * m1_set::bc<T>[2]) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[3];
z[0] = -abb[16] + abb[17] + abb[18];
z[1] = abb[11] + -abb[12];
z[2] = 7 * abb[13] + 4 * z[1];
z[2] = abb[13] * z[2];
z[1] = -5 * abb[13] + -2 * z[1];
z[1] = 3 * abb[15] + 2 * z[1];
z[1] = abb[15] * z[1];
z[0] = 6 * z[0] + z[1] + z[2];
return 8 * abb[7] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_3_173_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (4 * m1_set::bc<T>[0] * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (16 * m1_set::bc<T>[0] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return t * c[0] + c[1];
	}
	{
T z[1];
z[0] = -abb[13] + abb[15];
return 32 * abb[7] * m1_set::bc<T>[0] * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_173_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-5.771634444810701331808733572703257485691286571769691187447890765"),stof<T>("-24.969459559618275138399237733011339683276915727151721666349331211")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,25> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dlog_W8(k,dl), dl[3], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W32(k,dl), dlog_W62(k,dl), f_1_1(k), f_1_2(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), T{0}, f_2_2_re(k), f_2_10_re(k), T{0}};
abb[21] = SpDLog_f_3_173_W_20_Im(t, path, abb);
abb[24] = SpDLog_f_3_173_W_20_Re(t, path, abb);

                    
            return f_3_173_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_173_DLogXconstant_part(base_point<T>, kend);
	value += f_3_173_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_173_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_173_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_173_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_173_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_173_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_173_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
