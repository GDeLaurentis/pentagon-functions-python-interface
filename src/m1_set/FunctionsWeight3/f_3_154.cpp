/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_154.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_154_abbreviated (const std::array<T,46>& abb) {
T z[48];
z[0] = 4 * abb[18];
z[1] = 2 * abb[21];
z[2] = -abb[16] + abb[20];
z[3] = z[0] + -z[1] + 5 * z[2];
z[4] = 2 * abb[18];
z[3] = z[3] * z[4];
z[5] = 2 * abb[16];
z[6] = -abb[20] + z[5];
z[6] = abb[20] * z[6];
z[7] = prod_pow(abb[16], 2);
z[8] = z[6] + -z[7];
z[9] = -z[1] * z[2];
z[10] = abb[18] + -abb[21];
z[11] = z[2] + z[10];
z[12] = abb[17] + 2 * z[11];
z[13] = -abb[17] * z[12];
z[11] = abb[17] + z[11];
z[11] = abb[22] + 2 * z[11];
z[14] = -abb[22] * z[11];
z[15] = abb[40] + abb[42];
z[16] = 2 * abb[28];
z[17] = prod_pow(m1_set::bc<T>[0], 2);
z[18] = 2 * abb[29];
z[3] = 6 * abb[24] + z[3] + -3 * z[8] + z[9] + z[13] + z[14] + 2 * z[15] + z[16] + (T(-11) / T(3)) * z[17] + z[18];
z[9] = 2 * abb[2];
z[3] = z[3] * z[9];
z[13] = 3 * abb[18];
z[14] = -z[1] + -z[2] + z[13];
z[19] = abb[17] + -2 * z[14];
z[19] = abb[17] * z[19];
z[20] = 2 * abb[17];
z[14] = -z[14] + z[20];
z[14] = abb[22] + 2 * z[14];
z[14] = abb[22] * z[14];
z[21] = z[7] + z[18];
z[6] = -z[6] + z[21];
z[22] = -abb[21] + z[2];
z[23] = -z[0] * z[22];
z[24] = prod_pow(abb[21], 2);
z[25] = 12 * abb[25];
z[26] = 4 * abb[42];
z[14] = -z[6] + z[14] + z[19] + z[23] + -4 * z[24] + -z[25] + -z[26];
z[19] = 2 * abb[5];
z[14] = z[14] * z[19];
z[23] = 3 * abb[22];
z[27] = abb[16] + abb[20];
z[28] = abb[21] + -z[27];
z[29] = z[23] + -z[28];
z[30] = -abb[4] + abb[11];
z[29] = -z[29] * z[30];
z[31] = abb[6] * z[23];
z[32] = -abb[0] * z[28];
z[29] = z[29] + z[31] + z[32];
z[31] = abb[0] + -3 * abb[6] + 2 * z[30];
z[31] = abb[23] * z[31];
z[29] = 2 * z[29] + z[31];
z[29] = abb[23] * z[29];
z[31] = z[4] + z[22];
z[32] = 2 * abb[9];
z[33] = z[31] * z[32];
z[34] = abb[9] * abb[17];
z[33] = z[33] + z[34];
z[33] = abb[17] * z[33];
z[34] = abb[3] + -abb[10];
z[35] = -z[30] + z[34];
z[36] = abb[44] * z[35];
z[37] = abb[15] * abb[37];
z[38] = 5 * abb[18] + 2 * z[22];
z[38] = abb[18] * z[38];
z[39] = -abb[9] * z[38];
z[40] = abb[14] * abb[37];
z[29] = z[29] + z[33] + -z[36] + z[37] + z[39] + z[40];
z[33] = 3 * abb[25] + -5 * z[8];
z[36] = 4 * abb[17];
z[12] = -z[12] * z[36];
z[37] = 4 * abb[22];
z[11] = -z[11] * z[37];
z[15] = abb[28] + abb[29] + z[15];
z[39] = 8 * z[2];
z[40] = abb[21] + -z[39];
z[40] = abb[21] * z[40];
z[41] = -11 * abb[21] + 20 * z[2];
z[41] = 41 * abb[18] + 2 * z[41];
z[41] = abb[18] * z[41];
z[11] = 24 * abb[24] + z[11] + z[12] + 8 * z[15] + 2 * z[33] + z[40] + z[41];
z[11] = abb[1] * z[11];
z[12] = abb[21] * z[0];
z[15] = 4 * abb[21];
z[33] = abb[16] * z[15];
z[5] = abb[20] + z[5];
z[5] = abb[20] * z[5];
z[12] = z[5] + z[12] + -z[33];
z[33] = -5 * abb[16] + -abb[20] + z[0] + z[1];
z[40] = 9 * abb[17];
z[41] = -2 * z[33] + z[40];
z[41] = abb[17] * z[41];
z[4] = z[2] + z[4];
z[42] = abb[17] + -z[4];
z[42] = abb[22] + 2 * z[42];
z[42] = abb[22] * z[42];
z[41] = z[12] + z[21] + z[41] + z[42];
z[41] = abb[10] * z[41];
z[42] = 2 * abb[20] + -abb[21];
z[42] = z[1] * z[42];
z[5] = -z[5] + -z[25] + z[42];
z[25] = -z[7] + z[26];
z[15] = -abb[16] + -5 * abb[20] + z[15];
z[40] = 2 * z[15] + -z[40];
z[40] = abb[17] * z[40];
z[42] = z[1] + -z[2];
z[43] = 3 * abb[17];
z[44] = z[42] + -z[43];
z[23] = -z[23] + 2 * z[44];
z[23] = abb[22] * z[23];
z[23] = z[5] + z[18] + z[23] + z[25] + z[40];
z[23] = abb[3] * z[23];
z[40] = 2 * z[42] + -z[43];
z[40] = abb[17] * z[40];
z[15] = z[15] + -z[43];
z[42] = 9 * abb[22];
z[15] = 2 * z[15] + -z[42];
z[15] = abb[22] * z[15];
z[5] = -4 * abb[43] + z[5] + z[15] + -z[21] + z[40];
z[5] = abb[4] * z[5];
z[4] = abb[17] + -2 * z[4];
z[4] = abb[17] * z[4];
z[15] = abb[17] + -z[33];
z[15] = 2 * z[15] + z[42];
z[15] = abb[22] * z[15];
z[4] = 10 * abb[29] + 4 * abb[38] + z[4] + z[12] + z[15] + -z[25];
z[4] = abb[11] * z[4];
z[12] = abb[4] + abb[11];
z[15] = 2 * abb[12];
z[21] = abb[3] + z[15];
z[25] = 6 * abb[6];
z[33] = 5 * abb[10] + z[12] + -z[19] + -z[21] + -z[25] + -z[32];
z[16] = z[16] * z[33];
z[32] = abb[4] + -abb[10] + z[32];
z[33] = -z[19] + z[32];
z[33] = abb[40] * z[33];
z[40] = -z[28] + z[43];
z[40] = z[34] * z[40];
z[42] = abb[19] * z[34];
z[40] = z[40] + -z[42];
z[40] = abb[19] * z[40];
z[43] = -abb[9] + abb[10];
z[44] = abb[39] * z[43];
z[45] = -abb[3] + abb[12];
z[46] = -abb[0] + z[45];
z[46] = abb[41] * z[46];
z[33] = z[33] + z[40] + z[44] + z[46];
z[31] = abb[22] + 2 * z[31];
z[31] = abb[22] * z[31];
z[18] = -2 * abb[38] + -z[18] + z[26] + z[31] + -z[38];
z[26] = 2 * abb[8];
z[18] = z[18] * z[26];
z[31] = abb[16] * abb[20];
z[38] = abb[18] + z[2];
z[40] = -abb[18] * z[38];
z[31] = abb[24] + -abb[29] + z[31] + z[40];
z[40] = -abb[16] + abb[18];
z[44] = 2 * z[40];
z[46] = -abb[17] + z[44];
z[46] = abb[17] * z[46];
z[44] = -abb[22] + z[44];
z[44] = abb[22] * z[44];
z[20] = -abb[19] + z[20];
z[20] = abb[19] * z[20];
z[20] = z[20] + 2 * z[31] + z[44] + z[46];
z[20] = z[20] * z[25];
z[31] = abb[10] + abb[11];
z[31] = abb[24] * z[31];
z[44] = -abb[6] + z[30];
z[44] = abb[26] * z[44];
z[34] = -abb[6] + -z[34];
z[34] = abb[27] * z[34];
z[31] = z[31] + -z[34] + -z[44];
z[34] = 2 * z[2];
z[44] = abb[18] + z[34];
z[44] = abb[18] * z[44];
z[46] = 2 * z[38];
z[47] = -abb[17] + z[46];
z[47] = abb[17] * z[47];
z[8] = z[8] + -z[44] + z[47];
z[8] = z[8] * z[15];
z[15] = -abb[22] + z[46];
z[15] = abb[22] * z[15];
z[6] = 2 * abb[43] + -z[6] + z[15] + -z[44];
z[15] = 2 * abb[13];
z[6] = z[6] * z[15];
z[44] = -3 * abb[16] + -abb[20];
z[44] = abb[20] * z[44];
z[46] = abb[21] * z[27];
z[7] = -abb[38] + -abb[39] + -abb[43] + -abb[44] + -z[7] + z[44] + z[46];
z[28] = abb[19] + -2 * z[28];
z[28] = abb[19] * z[28];
z[7] = 2 * z[7] + z[28];
z[28] = 2 * abb[0];
z[7] = z[7] * z[28];
z[36] = z[36] + z[37];
z[36] = z[10] * z[36];
z[1] = -abb[18] + -z[1];
z[1] = abb[18] * z[1];
z[1] = 10 * abb[25] + z[1] + 3 * z[24] + z[36];
z[24] = 3 * abb[7];
z[1] = z[1] * z[24];
z[36] = abb[8] + abb[9];
z[12] = -abb[5] + abb[10] + z[12] + z[21] + 16 * z[36];
z[12] = -19 * abb[1] + abb[13] * (T(2) / T(3)) + abb[0] * (T(16) / T(3)) + (T(1) / T(3)) * z[12];
z[12] = z[12] * z[17];
z[1] = z[1] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[11] + z[12] + z[14] + z[16] + z[18] + z[20] + z[23] + 2 * z[29] + -12 * z[31] + 4 * z[33] + z[41];
z[1] = 4 * z[1];
z[0] = z[0] + z[22];
z[3] = -abb[17] + z[0];
z[3] = abb[9] * z[3];
z[0] = -abb[22] + z[0];
z[0] = abb[8] * z[0];
z[2] = z[2] + -z[10];
z[2] = abb[17] + abb[22] + 2 * z[2];
z[2] = abb[5] * z[2];
z[4] = z[25] * z[40];
z[5] = z[10] * z[24];
z[6] = abb[18] + abb[20];
z[7] = abb[22] + -z[6];
z[7] = abb[10] * z[7];
z[8] = -abb[17] + abb[22];
z[10] = abb[16] + -z[8];
z[10] = abb[3] * z[10];
z[8] = abb[16] + z[8];
z[8] = abb[4] * z[8];
z[6] = abb[17] + -z[6];
z[6] = abb[11] * z[6];
z[11] = -abb[17] + z[38];
z[11] = abb[12] * z[11];
z[12] = -15 * abb[18] + 5 * abb[21] + -z[39];
z[12] = abb[1] * z[12];
z[14] = -abb[22] + z[38];
z[14] = abb[13] * z[14];
z[16] = -abb[21] + 2 * z[27];
z[16] = -abb[19] + 2 * z[16];
z[16] = abb[0] * z[16];
z[17] = -abb[0] + z[30];
z[17] = abb[23] * z[17];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[10] + z[11] + z[12] + z[14] + z[16] + z[17] + -z[42];
z[2] = 2 * m1_set::bc<T>[0];
z[0] = z[0] * z[2];
z[3] = 2 * abb[1] + abb[3] + -abb[11] + -z[19] + z[26];
z[3] = abb[34] * z[3];
z[4] = abb[31] * z[43];
z[5] = -abb[4] * abb[35];
z[6] = abb[33] * z[45];
z[7] = -abb[0] + -abb[8] + abb[11];
z[7] = abb[30] * z[7];
z[3] = z[3] + z[4] + z[5] + z[6] + z[7];
z[4] = abb[1] + -abb[5];
z[4] = 4 * z[4] + 2 * z[32];
z[4] = abb[32] * z[4];
z[5] = -abb[36] * z[35];
z[6] = -abb[31] + -abb[33] + -abb[35] + -abb[36];
z[6] = z[6] * z[28];
z[7] = abb[21] + -z[13] + -z[34];
z[2] = z[2] * z[7];
z[2] = abb[32] + abb[34] + z[2];
z[2] = z[2] * z[9];
z[7] = -abb[14] + -abb[15];
z[7] = abb[45] * z[7];
z[8] = abb[35] * z[15];
z[0] = z[0] + z[2] + 2 * z[3] + z[4] + z[5] + z[6] + z[7] + z[8];
z[0] = 8 * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_3_154_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_154_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_154_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-219.79938189925117073934360115235950825900716817480459513278814313"),stof<T>("1049.97603205975223713086781883187679401294229794465264088020437439")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,46> abb = {dl[0], dl[1], dl[2], dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W70(k,dl), dlog_W71(k,dl), dlog_W130(k,dv), dlog_W131(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_16(k), f_2_17(k), f_2_18(k), f_2_2_im(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_9_im(k), f_2_12_im(k), f_2_22_im(k), f_2_27_im(k), f_2_2_re(k), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_9_re(k), f_2_12_re(k), f_2_22_re(k), f_2_27_re(k)};

                    
            return f_3_154_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_154_DLogXconstant_part(base_point<T>, kend);
	value += f_3_154_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_154_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_154_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_154_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_154_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_154_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_154_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
