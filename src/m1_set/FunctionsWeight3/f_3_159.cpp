/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_159.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_159_abbreviated (const std::array<T,44>& abb) {
T z[47];
z[0] = 3 * abb[7];
z[1] = 2 * abb[4];
z[2] = z[0] + z[1];
z[3] = abb[6] + z[2];
z[4] = 2 * abb[10];
z[5] = z[3] + -z[4];
z[6] = 2 * abb[3];
z[7] = 3 * abb[2];
z[8] = z[6] + z[7];
z[9] = 2 * abb[5];
z[10] = 3 * abb[11];
z[11] = -z[5] + z[8] + z[9] + -z[10];
z[12] = abb[32] * z[11];
z[1] = 5 * abb[6] + abb[7] + z[1];
z[13] = 4 * abb[10];
z[14] = 3 * abb[5];
z[15] = -abb[3] + z[14];
z[16] = z[1] + -z[10] + z[13] + -z[15];
z[17] = abb[42] * z[16];
z[18] = 4 * abb[9];
z[19] = 3 * abb[13];
z[20] = z[18] + -z[19];
z[21] = abb[3] + z[7];
z[22] = abb[5] + -abb[6];
z[2] = z[2] + -z[20] + -z[21] + -z[22];
z[23] = abb[33] * z[2];
z[24] = abb[3] + -abb[12];
z[25] = -abb[11] + z[24];
z[25] = abb[27] * z[25];
z[26] = abb[4] + abb[5];
z[27] = -abb[6] + z[26];
z[28] = -abb[13] + z[27];
z[29] = abb[30] * z[28];
z[30] = abb[27] + abb[30];
z[30] = abb[2] * z[30];
z[25] = z[25] + z[29] + z[30];
z[7] = -abb[12] + z[7];
z[29] = 4 * abb[8];
z[1] = -7 * abb[5] + z[1] + -z[7] + z[29];
z[1] = abb[28] * z[1];
z[30] = 3 * abb[8];
z[31] = abb[4] + 2 * abb[7];
z[32] = abb[6] + z[31];
z[33] = abb[3] + -z[9] + z[30] + -z[32];
z[33] = prod_pow(abb[21], 2) * z[33];
z[10] = abb[3] + abb[5] + z[4] + -z[10] + -z[20];
z[20] = abb[40] * z[10];
z[34] = 2 * abb[8];
z[35] = -abb[6] + z[34];
z[36] = -abb[7] + z[24] + z[35];
z[37] = -abb[34] * z[36];
z[38] = abb[15] + abb[17];
z[38] = -3 * abb[14] + -2 * z[38];
z[38] = abb[35] * z[38];
z[1] = z[1] + -z[12] + z[17] + -z[20] + -z[23] + 3 * z[25] + z[33] + z[37] + z[38];
z[12] = abb[3] + -abb[4];
z[0] = -4 * abb[6] + -z[0] + z[4] + z[9] + -z[12];
z[0] = abb[23] * z[0];
z[17] = abb[1] + -abb[9];
z[20] = -abb[5] + z[17];
z[23] = 5 * abb[4];
z[25] = 2 * abb[12];
z[33] = abb[3] + abb[7] + z[19] + z[20] + -z[23] + -z[25];
z[33] = abb[24] * z[33];
z[37] = 2 * abb[0];
z[38] = 2 * abb[1];
z[2] = -z[2] + z[25] + -z[37] + -z[38];
z[2] = abb[18] * z[2];
z[39] = 3 * abb[4] + -4 * abb[7] + -z[8] + z[25] + z[35];
z[39] = abb[21] * z[39];
z[40] = -abb[8] + -abb[10] + -abb[12] + z[31];
z[22] = -z[22] + z[40];
z[41] = 2 * abb[20];
z[22] = z[22] * z[41];
z[42] = 6 * abb[0];
z[43] = abb[20] * z[42];
z[22] = z[22] + z[43];
z[44] = 2 * abb[9];
z[45] = -abb[7] + -z[26] + z[44];
z[46] = -abb[0] + abb[1] + -z[45];
z[46] = abb[19] * z[46];
z[28] = -abb[2] + -z[28];
z[28] = abb[25] * z[28];
z[0] = z[0] + z[2] + z[22] + 3 * z[28] + z[33] + z[39] + 2 * z[46];
z[2] = -abb[7] + z[19];
z[23] = -abb[6] + -z[2] + z[23];
z[28] = 3 * abb[9];
z[33] = -abb[1] + z[28];
z[23] = z[14] + z[21] + 2 * z[23] + -z[33] + z[37];
z[23] = abb[22] * z[23];
z[0] = 2 * z[0] + z[23];
z[0] = abb[22] * z[0];
z[23] = abb[20] * z[16];
z[39] = 5 * abb[7];
z[46] = 4 * abb[5];
z[21] = -4 * abb[4] + -7 * abb[6] + z[21] + -z[39] + z[46];
z[21] = abb[21] * z[21];
z[21] = z[21] + z[23];
z[23] = -2 * abb[6] + -abb[7];
z[8] = 6 * abb[11] + z[4] + -z[8] + 5 * z[23] + -z[26];
z[8] = abb[23] * z[8];
z[8] = z[8] + 2 * z[21];
z[8] = abb[23] * z[8];
z[11] = -abb[23] * z[11];
z[21] = abb[12] + -z[38] + z[45];
z[23] = 2 * abb[24];
z[21] = z[21] * z[23];
z[23] = -abb[21] * z[36];
z[11] = z[11] + -z[21] + -z[22] + z[23];
z[22] = -z[14] + z[37] + -z[38];
z[23] = abb[6] + abb[7];
z[6] = z[6] + z[22] + z[23];
z[6] = abb[18] * z[6];
z[6] = z[6] + 2 * z[11];
z[6] = abb[18] * z[6];
z[11] = z[3] + z[14];
z[25] = z[11] + -z[25] + -z[34];
z[25] = abb[21] * z[25];
z[4] = -abb[5] + z[4] + -z[23];
z[4] = abb[23] * z[4];
z[23] = -abb[6] + -z[9] + -z[40];
z[23] = z[23] * z[41];
z[26] = abb[0] + abb[1] + -abb[12];
z[26] = abb[18] * z[26];
z[21] = -z[4] + z[21] + z[23] + z[25] + 2 * z[26];
z[23] = 4 * abb[0];
z[11] = z[11] + -z[23] + -z[38];
z[11] = abb[19] * z[11];
z[11] = z[11] + 2 * z[21];
z[11] = abb[19] * z[11];
z[21] = prod_pow(abb[20], 2);
z[25] = abb[27] + z[21];
z[25] = abb[0] * z[25];
z[26] = abb[0] + -abb[5];
z[26] = abb[41] * z[26];
z[25] = z[25] + z[26];
z[26] = -5 * abb[10] + -z[30] + 2 * z[32];
z[26] = -3 * abb[12] + z[15] + 2 * z[26];
z[21] = z[21] * z[26];
z[17] = -abb[5] + -5 * z[17] + -z[24];
z[17] = prod_pow(abb[24], 2) * z[17];
z[0] = z[0] + 2 * z[1] + z[6] + z[8] + z[11] + z[17] + z[21] + 12 * z[25];
z[1] = 7 * abb[4];
z[2] = -3 * abb[1] + -z[1] + z[2] + -z[15] + z[28];
z[6] = abb[43] * z[2];
z[7] = -4 * abb[1] + -z[7] + -z[14] + z[19] + z[34];
z[7] = abb[26] * z[7];
z[6] = z[6] + z[7];
z[7] = abb[2] + z[12];
z[8] = abb[6] + -abb[11] + z[7];
z[11] = abb[31] * z[8];
z[12] = abb[16] * abb[35];
z[7] = abb[7] + -abb[12] + z[7];
z[7] = abb[29] * z[7];
z[7] = -z[7] + z[11] + z[12];
z[11] = 19 * abb[6] + z[39];
z[1] = abb[1] + 32 * abb[10] + -z[1] + (T(1) / T(2)) * z[11] + z[18];
z[1] = abb[0] * (T(-28) / T(3)) + abb[5] * (T(-9) / T(2)) + abb[3] * (T(2) / T(3)) + (T(1) / T(3)) * z[1];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[8] = abb[23] * z[8];
z[11] = -abb[3] + z[27];
z[11] = abb[25] * z[11];
z[8] = 2 * z[8] + z[11];
z[8] = abb[25] * z[8];
z[0] = 2 * z[0] + z[1] + 4 * z[6] + -12 * z[7] + 6 * z[8];
z[1] = abb[3] + abb[12];
z[3] = -z[1] + z[3] + -z[29] + z[46];
z[3] = abb[21] * z[3];
z[6] = -abb[10] + z[44];
z[6] = -abb[5] + -z[1] + 2 * z[6] + z[23] + -z[38];
z[6] = abb[18] * z[6];
z[7] = abb[4] + abb[7];
z[8] = abb[10] + z[7];
z[8] = 2 * z[8] + z[9] + -z[23] + -z[33];
z[8] = abb[22] * z[8];
z[9] = z[13] + -z[31] + z[35];
z[9] = -5 * abb[5] + z[1] + 2 * z[9];
z[9] = abb[20] * z[9];
z[1] = z[1] + -2 * z[7] + z[20];
z[1] = abb[24] * z[1];
z[5] = z[5] + -z[18] + -z[22];
z[5] = abb[19] * z[5];
z[1] = z[1] + z[3] + -z[4] + z[5] + z[6] + z[8] + z[9] + -z[43];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = abb[38] * z[16];
z[2] = abb[39] * z[2];
z[4] = -abb[36] * z[10];
z[5] = -6 * abb[5] + z[42];
z[5] = abb[37] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
z[1] = 4 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_159_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_159_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_159_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("63.326364531143290699528102245036865465482108191882495582449464273"),stof<T>("28.247091478452608822937013829452508951426840828444132665594561723")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,44> abb = {dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W21(k,dl), dlog_W23(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_20(k), f_2_21(k), f_2_29_im(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_10_im(k), f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_10_re(k)};

                    
            return f_3_159_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_159_DLogXconstant_part(base_point<T>, kend);
	value += f_3_159_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_159_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_159_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_159_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_159_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_159_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_159_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
