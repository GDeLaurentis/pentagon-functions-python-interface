/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_145.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_145_abbreviated (const std::array<T,17>& abb) {
T z[9];
z[0] = abb[5] + -abb[6];
z[1] = 2 * abb[11];
z[2] = z[0] * z[1];
z[3] = abb[5] * (T(3) / T(2));
z[4] = -abb[6] + z[3];
z[5] = -abb[10] * z[4];
z[3] = abb[9] * z[3];
z[3] = z[2] + z[3] + z[5];
z[3] = abb[9] * z[3];
z[5] = -abb[12] + abb[15];
z[6] = prod_pow(abb[11], 2);
z[1] = abb[10] * z[1];
z[1] = z[1] + 2 * z[5] + -z[6];
z[1] = abb[4] * z[1];
z[5] = abb[9] + -abb[10];
z[4] = z[4] * z[5];
z[2] = z[2] + z[4];
z[4] = abb[4] + abb[6];
z[5] = -abb[8] * z[4];
z[5] = -z[2] + z[5];
z[5] = abb[8] * z[5];
z[6] = abb[6] * abb[15];
z[0] = -abb[12] * z[0];
z[0] = z[0] + z[6];
z[6] = abb[2] + abb[1] * (T(1) / T(2));
z[6] = -abb[3] + 6 * abb[7] + abb[0] * (T(-3) / T(2)) + -3 * z[6];
z[7] = -abb[16] * z[6];
z[8] = abb[5] + 2 * z[4];
z[8] = prod_pow(m1_set::bc<T>[0], 2) * z[8];
z[0] = 2 * z[0] + z[1] + z[3] + z[5] + z[7] + (T(1) / T(3)) * z[8];
z[1] = m1_set::bc<T>[0] * z[2];
z[2] = abb[8] * m1_set::bc<T>[0];
z[2] = abb[13] + z[2];
z[2] = 2 * z[2];
z[2] = z[2] * z[4];
z[3] = -abb[14] * z[6];
z[1] = z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_3_145_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_145_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_145_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-5.1308037300470366547383700732721051365847964024862086651021948251"),stof<T>("-6.3404151149454952501397502564644908152841443413141669639254008115")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,17> abb = {dl[0], dlog_W8(k,dl), dl[3], dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W127(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_2_im(k), f_2_24_im(k), f_2_2_re(k), f_2_24_re(k)};

                    
            return f_3_145_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_145_DLogXconstant_part(base_point<T>, kend);
	value += f_3_145_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_145_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_145_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_145_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_145_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_145_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_145_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
