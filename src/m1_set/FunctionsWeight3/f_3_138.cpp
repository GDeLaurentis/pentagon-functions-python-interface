/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_3_138.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_3_138_abbreviated (const std::array<T,31>& abb) {
T z[31];
z[0] = abb[5] + -abb[9];
z[1] = 2 * abb[7];
z[2] = abb[4] + z[1];
z[3] = z[0] + -z[2];
z[3] = abb[28] * z[3];
z[4] = 2 * abb[3];
z[5] = abb[19] + -abb[22] + abb[28];
z[5] = z[4] * z[5];
z[6] = 2 * abb[9];
z[7] = -abb[10] + z[6];
z[8] = abb[4] + abb[7] + z[7];
z[8] = abb[22] * z[8];
z[9] = abb[0] + -abb[9];
z[10] = abb[19] * z[9];
z[11] = abb[5] + -abb[10];
z[12] = abb[0] + z[11];
z[13] = abb[29] * z[12];
z[3] = z[3] + z[5] + z[8] + 2 * z[10] + z[13];
z[5] = abb[4] + abb[6];
z[8] = abb[1] * (T(1) / T(2));
z[10] = abb[2] * (T(1) / T(2));
z[5] = -z[4] + (T(1) / T(2)) * z[5] + z[7] + -z[8] + z[10];
z[5] = prod_pow(abb[18], 2) * z[5];
z[13] = abb[4] + -abb[6];
z[14] = z[6] + z[13];
z[15] = abb[8] * (T(1) / T(2));
z[16] = abb[0] * (T(1) / T(2)) + z[15];
z[17] = -z[10] + z[14] + -z[16];
z[17] = abb[13] * z[17];
z[18] = abb[1] + abb[2];
z[19] = -z[14] + z[18];
z[19] = abb[18] * z[19];
z[20] = 2 * abb[14];
z[21] = abb[7] + -abb[9];
z[22] = -abb[1] + -z[21];
z[22] = z[20] * z[22];
z[23] = abb[0] + -abb[1];
z[24] = abb[16] * (T(1) / T(2));
z[25] = z[23] * z[24];
z[17] = z[17] + z[19] + z[22] + z[25];
z[17] = abb[16] * z[17];
z[19] = 2 * abb[1];
z[22] = 2 * abb[2] + -abb[8] + z[19];
z[25] = 4 * abb[3];
z[2] = abb[6] + z[2] + z[6] + -z[22] + -z[25];
z[26] = abb[27] * z[2];
z[27] = -abb[4] + 3 * abb[6];
z[28] = abb[0] + -abb[10] + z[4] + -z[15] + z[18] + (T(-1) / T(2)) * z[27];
z[28] = abb[13] * z[28];
z[29] = 2 * abb[10];
z[22] = z[22] + -z[29];
z[30] = z[14] + z[22];
z[30] = abb[18] * z[30];
z[28] = z[28] + -z[30];
z[28] = abb[13] * z[28];
z[11] = z[11] + -z[21];
z[20] = z[11] * z[20];
z[20] = -z[20] + z[30];
z[16] = abb[2] * (T(3) / T(2)) + -z[16] + z[19];
z[14] = -z[14] + -z[16] + z[29];
z[14] = abb[13] * z[14];
z[29] = -abb[0] + -abb[2] + abb[8];
z[24] = z[24] * z[29];
z[23] = -abb[15] * z[23];
z[14] = z[14] + z[20] + z[23] + z[24];
z[14] = abb[15] * z[14];
z[4] = -abb[4] + z[4];
z[0] = z[0] + -z[1] + z[4];
z[23] = abb[14] * z[0];
z[24] = -abb[13] + abb[16];
z[21] = abb[0] + z[21];
z[21] = z[21] * z[24];
z[11] = abb[15] * z[11];
z[11] = z[11] + z[21] + z[23];
z[21] = 2 * abb[5];
z[23] = z[4] + z[21];
z[7] = 2 * abb[0] + 5 * abb[7] + -z[7] + -z[23];
z[7] = abb[17] * z[7];
z[7] = z[7] + 2 * z[11];
z[7] = abb[17] * z[7];
z[4] = abb[10] + z[4];
z[11] = abb[7] + -z[4] + z[6];
z[11] = abb[13] * z[11];
z[24] = abb[1] + -3 * abb[9] + z[4];
z[24] = abb[18] * z[24];
z[11] = z[11] + z[24];
z[4] = -abb[7] + 4 * abb[9] + -z[4] + -z[19];
z[4] = abb[14] * z[4];
z[4] = z[4] + 2 * z[11];
z[4] = abb[14] * z[4];
z[11] = z[1] + z[13];
z[8] = 5 * abb[5] + abb[8] + -z[8] + -4 * z[11];
z[13] = abb[0] * (T(3) / T(2));
z[8] = abb[2] * (T(-2) / T(3)) + (T(1) / T(3)) * z[8] + -z[13];
z[8] = prod_pow(m1_set::bc<T>[0], 2) * z[8];
z[19] = -abb[1] + -abb[3] + abb[9];
z[19] = abb[20] * z[19];
z[24] = abb[11] + abb[12];
z[29] = abb[30] * z[24];
z[18] = abb[8] + -z[18];
z[18] = abb[21] * z[18];
z[3] = 2 * z[3] + z[4] + z[5] + z[7] + z[8] + z[14] + z[17] + z[18] + 4 * z[19] + z[26] + z[28] + (T(1) / T(2)) * z[29];
z[1] = z[1] + -z[6] + -z[22] + -z[25] + z[27];
z[1] = abb[13] * z[1];
z[4] = z[10] + -z[11] + -z[13] + z[15];
z[4] = abb[16] * z[4];
z[5] = z[11] + z[16] + -z[21];
z[5] = abb[15] * z[5];
z[6] = -4 * abb[7] + -abb[10] + -z[9] + z[23];
z[6] = abb[17] * z[6];
z[1] = z[1] + z[4] + z[5] + 2 * z[6] + z[20];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[23] * z[2];
z[0] = abb[24] * z[0];
z[4] = abb[25] * z[12];
z[0] = z[0] + z[4];
z[4] = (T(1) / T(2)) * z[24];
z[4] = abb[26] * z[4];
z[0] = 2 * z[0] + z[1] + z[2] + z[4];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_3_138_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {};
	
	std::vector<C> intdlogs = {};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_3_138_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_3_138_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("5.623083711066501554553611767788908602404321195464896363705457306"),stof<T>("35.581845883757260604044516958014370861146856902287448626101218264")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,31> abb = {dl[0], dlog_W4(k,dl), dlog_W7(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W23(k,dl), dlog_W29(k,dl), dlog_W32(k,dl), dlog_W83(k,dl), dlog_W122(k,dv), dlog_W125(k,dv), f_1_1(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_15(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), f_2_11_im(k), f_2_25_im(k), f_2_2_re(k), f_2_10_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_3_138_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_3_138_DLogXconstant_part(base_point<T>, kend);
	value += f_3_138_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_3_138_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_3_138_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_3_138_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_3_138_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_3_138_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_3_138_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
