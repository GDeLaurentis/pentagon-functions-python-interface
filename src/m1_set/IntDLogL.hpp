#pragma once

#include "Constants.h"
#include "Kin.h"
#include "PolyLog.h"

namespace PentagonFunctions {
namespace m1_set {
template <typename T> T IntDLogL_W_16(T t, const Kin<T, KinType::m1>& k, const Kin<T, KinType::m1>& kend, const std::array<T, 6>& dl) {
    if (t < detail::SpDLogZeroThreshold<T>) {
        T c = -dl[3] * k.v[3] + dl[5] * k.v[5];
        return rlog(kend.W[15].real() / c) - rlog(t);
    }
    else {
        return rlog(kend.W[15].real() / k.W[15].real());
    }
}
template <typename T> T IntDLogL_W_17(T t, const Kin<T, KinType::m1>& k, const Kin<T, KinType::m1>& kend, const std::array<T, 6>& dl) {
    if (t < detail::SpDLogZeroThreshold<T>) {
        T c = dl[0] * k.v[0] + -dl[1] * k.v[1] + dl[2] * k.v[2] + dl[3] * k.v[3] + -dl[5] * k.v[5];
        return rlog(kend.W[16].real() / c) - rlog(t);
    }
    else {
        return rlog(kend.W[16].real() / k.W[16].real());
    }
}
template <typename T> T IntDLogL_W_19(T t, const Kin<T, KinType::m1>& k, const Kin<T, KinType::m1>& kend, const std::array<T, 6>& dl) {
    if (t < detail::SpDLogZeroThreshold<T>) {
        T c = -dl[2] * k.v[2] + -dl[3] * k.v[3];
        return rlog(kend.W[18].real() / c) - rlog(t);
    }
    else {
        return rlog(kend.W[18].real() / k.W[18].real());
    }
}
template <typename T> T IntDLogL_W_20(T t, const Kin<T, KinType::m1>& k, const Kin<T, KinType::m1>& kend, const std::array<T, 6>& dl) {
    if (t < detail::SpDLogZeroThreshold<T>) {
        T c = -dl[1] * k.v[1] + -dl[2] * k.v[2] + dl[3] * k.v[3] + dl[4] * k.v[4];
        return rlog(kend.W[19].real() / c) - rlog(t);
    }
    else {
        return rlog(kend.W[19].real() / k.W[19].real());
    }
}

} // namespace m1_set
} // namespace PentagonFunctions

