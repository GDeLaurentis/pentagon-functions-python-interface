/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_598.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_598_abbreviated (const std::array<T,69>& abb) {
T z[167];
z[0] = -abb[49] + abb[55];
z[1] = abb[51] + z[0];
z[2] = abb[50] + z[1];
z[3] = -abb[56] + abb[57];
z[4] = -abb[48] + abb[54];
z[5] = 2 * abb[47];
z[6] = -abb[52] + z[5];
z[7] = abb[46] + -z[2] + z[3] + z[4] + -z[6];
z[7] = abb[34] * z[7];
z[8] = abb[52] * (T(3) / T(2));
z[9] = abb[51] * (T(1) / T(2));
z[10] = z[8] + -z[9];
z[11] = 2 * abb[50];
z[12] = abb[46] + z[11];
z[13] = 2 * abb[56];
z[14] = -abb[58] + z[13];
z[15] = abb[54] * (T(1) / T(2));
z[16] = -abb[57] + z[10] + -z[12] + z[14] + z[15];
z[16] = abb[31] * z[16];
z[17] = 2 * abb[48];
z[18] = -abb[54] + z[17];
z[19] = abb[51] + z[18];
z[20] = z[6] + z[19];
z[21] = z[11] + z[20];
z[22] = abb[49] + abb[56];
z[23] = -abb[58] + z[22];
z[24] = -z[21] + 2 * z[23];
z[24] = abb[36] * z[24];
z[25] = 2 * abb[58];
z[2] = -abb[52] + -3 * abb[56] + abb[57] + z[2] + z[25];
z[2] = abb[33] * z[2];
z[26] = abb[53] + -abb[55];
z[27] = abb[46] + z[26];
z[28] = abb[32] * z[27];
z[29] = 2 * z[28];
z[2] = z[2] + z[7] + z[16] + -z[24] + z[29];
z[2] = abb[31] * z[2];
z[7] = abb[52] * (T(1) / T(2));
z[16] = z[7] + z[9];
z[30] = -abb[57] + z[16];
z[31] = 2 * abb[49];
z[17] = -abb[47] + -abb[58] + z[15] + -z[17] + -z[30] + z[31];
z[32] = prod_pow(abb[36], 2);
z[17] = z[17] * z[32];
z[33] = 2 * abb[57];
z[34] = -abb[49] + abb[56];
z[21] = z[21] + -z[33] + 2 * z[34];
z[21] = abb[36] * z[21];
z[34] = abb[49] + z[3];
z[35] = abb[46] * (T(1) / T(2));
z[36] = z[34] + -z[35];
z[37] = abb[48] * (T(3) / T(2)) + -z[15];
z[38] = abb[50] + -abb[55];
z[39] = abb[47] + z[38];
z[40] = z[36] + -z[37] + -z[39];
z[40] = abb[34] * z[40];
z[21] = z[21] + -z[29] + z[40];
z[21] = abb[34] * z[21];
z[40] = z[5] + z[18];
z[34] = -z[34] + z[38] + z[40];
z[34] = abb[34] * z[34];
z[41] = abb[50] + -abb[56];
z[42] = abb[58] + z[41];
z[43] = -abb[55] + z[42];
z[35] = z[35] + z[43];
z[44] = abb[48] * (T(1) / T(2));
z[45] = -z[15] + -z[35] + z[44];
z[45] = abb[33] * z[45];
z[24] = z[24] + z[29] + z[34] + z[45];
z[24] = abb[33] * z[24];
z[29] = abb[49] + abb[55];
z[3] = z[3] + z[29];
z[34] = 3 * abb[48] + -abb[54];
z[45] = 3 * abb[47];
z[3] = 2 * z[3] + -z[34] + -z[45];
z[3] = abb[38] * z[3];
z[46] = 5 * abb[53];
z[47] = 2 * abb[55];
z[48] = abb[49] + z[47];
z[49] = -abb[51] + z[11];
z[50] = -3 * abb[46] + -z[46] + z[48] + z[49];
z[51] = prod_pow(abb[32], 2);
z[50] = z[50] * z[51];
z[52] = 2 * abb[53];
z[53] = z[1] + z[52];
z[54] = 2 * abb[39];
z[53] = -z[53] * z[54];
z[55] = -abb[52] + z[40];
z[56] = abb[53] + abb[55];
z[56] = abb[51] + -z[55] + 2 * z[56];
z[57] = abb[37] * z[56];
z[58] = 2 * abb[37];
z[59] = -abb[38] + z[54] + -z[58];
z[59] = z[11] * z[59];
z[60] = abb[56] + -abb[58];
z[61] = abb[50] + -abb[53] + -z[1] + z[60];
z[62] = 2 * abb[40];
z[63] = z[61] * z[62];
z[64] = -z[0] + z[41];
z[65] = z[5] + z[64];
z[66] = 2 * abb[67];
z[67] = -z[65] * z[66];
z[43] = abb[46] + z[43];
z[68] = 2 * abb[66];
z[69] = z[43] * z[68];
z[70] = abb[59] + abb[60];
z[71] = abb[41] * z[70];
z[2] = z[2] + z[3] + z[17] + z[21] + z[24] + z[50] + z[53] + z[57] + z[59] + z[63] + z[67] + z[69] + -z[71];
z[2] = abb[8] * z[2];
z[3] = 3 * abb[58];
z[17] = z[3] + -z[13];
z[21] = -abb[48] + z[15];
z[24] = -abb[49] + z[17] + z[21];
z[50] = -z[33] + z[45];
z[53] = abb[51] * (T(3) / T(2));
z[57] = 3 * abb[53];
z[8] = z[8] + -z[24] + z[47] + z[50] + z[53] + z[57];
z[8] = abb[9] * z[8];
z[59] = 2 * abb[46];
z[63] = z[59] + z[64];
z[64] = abb[17] * z[63];
z[67] = -abb[53] + z[42];
z[69] = abb[4] * z[67];
z[72] = -z[64] + z[69];
z[73] = abb[55] + -abb[58];
z[74] = 2 * z[73];
z[75] = z[20] + z[74];
z[76] = abb[1] * z[75];
z[27] = abb[2] * z[27];
z[76] = -z[27] + -z[72] + -z[76];
z[77] = abb[57] + z[41];
z[10] = z[0] + -z[10] + z[77];
z[21] = z[10] + -z[21];
z[78] = abb[15] * z[21];
z[79] = 3 * abb[55];
z[80] = -z[22] + z[79];
z[81] = 2 * abb[51];
z[82] = z[57] + z[80] + z[81];
z[83] = abb[7] * z[82];
z[84] = abb[51] + z[79];
z[85] = 2 * abb[52];
z[86] = -abb[47] + z[85];
z[87] = -abb[48] + z[13] + -z[84] + z[86];
z[87] = abb[0] * z[87];
z[88] = z[1] + -z[13];
z[89] = z[25] + z[88];
z[89] = abb[18] * z[89];
z[90] = abb[51] + abb[55];
z[91] = -abb[58] + z[90];
z[40] = -abb[46] + abb[57] + -z[40] + -z[91];
z[40] = abb[3] * z[40];
z[92] = abb[46] + z[38];
z[92] = abb[10] * z[92];
z[93] = 4 * z[92];
z[94] = abb[0] * abb[46];
z[95] = 4 * abb[7];
z[96] = -5 * abb[9] + z[95];
z[96] = abb[50] * z[96];
z[97] = abb[26] + -abb[28];
z[98] = abb[64] * z[97];
z[8] = z[8] + z[40] + z[76] + z[78] + -z[83] + z[87] + z[89] + -z[93] + z[94] + z[96] + z[98];
z[8] = abb[31] * z[8];
z[40] = 4 * abb[58];
z[87] = abb[46] * (T(1) / T(4));
z[96] = 4 * abb[47];
z[53] = abb[49] + abb[54] * (T(-5) / T(4)) + abb[48] * (T(11) / T(4)) + -z[7] + -z[40] + z[53] + -z[77] + z[79] + -z[87] + z[96];
z[53] = abb[33] * z[53];
z[77] = abb[48] * (T(1) / T(4));
z[98] = abb[54] * (T(1) / T(4));
z[99] = z[77] + z[98];
z[29] = abb[46] * (T(5) / T(4)) + z[25] + -z[29] + z[30] + z[41] + z[99];
z[29] = abb[34] * z[29];
z[30] = abb[36] * z[75];
z[100] = abb[32] * z[67];
z[101] = z[30] + z[100];
z[29] = z[29] + z[53] + 2 * z[101];
z[29] = abb[3] * z[29];
z[53] = abb[48] * (T(7) / T(4)) + z[5] + -z[13] + z[16] + -z[98];
z[53] = abb[0] * z[53];
z[98] = (T(3) / T(4)) * z[94];
z[101] = z[78] + z[98];
z[102] = -z[64] + z[101];
z[56] = abb[9] * z[56];
z[103] = abb[16] * z[61];
z[104] = 2 * z[103];
z[105] = 4 * abb[50];
z[106] = abb[9] * z[105];
z[106] = -z[104] + z[106];
z[56] = z[56] + -z[106];
z[107] = -abb[52] + z[73];
z[108] = -abb[47] + z[107];
z[109] = abb[57] + z[108];
z[110] = 2 * abb[1];
z[111] = z[109] * z[110];
z[112] = abb[20] * z[70];
z[113] = (T(3) / T(4)) * z[112];
z[114] = 2 * z[27];
z[53] = z[53] + z[56] + z[102] + -z[111] + z[113] + z[114];
z[53] = abb[34] * z[53];
z[115] = -abb[56] + z[0];
z[116] = 3 * abb[52];
z[115] = -z[19] + -z[33] + -2 * z[115] + z[116];
z[117] = -z[11] + z[115];
z[117] = abb[15] * z[117];
z[33] = -abb[52] + z[33];
z[118] = -z[19] + z[33] + -z[96];
z[119] = -abb[55] + z[17];
z[120] = -abb[49] + z[119];
z[121] = -abb[53] + z[120];
z[121] = z[118] + 2 * z[121];
z[121] = abb[9] * z[121];
z[122] = 2 * z[89];
z[106] = z[106] + z[121] + -z[122];
z[19] = abb[57] + z[19] + 3 * z[73] + -z[86];
z[73] = z[19] * z[110];
z[86] = 2 * z[69];
z[73] = z[73] + z[86] + z[106] + z[117];
z[73] = abb[36] * z[73];
z[121] = -z[16] + 2 * z[60] + z[99];
z[121] = abb[0] * z[121];
z[123] = -abb[49] + abb[58];
z[124] = -abb[46] + z[123];
z[124] = abb[13] * z[124];
z[125] = 2 * z[124];
z[121] = (T(1) / T(4)) * z[112] + -z[114] + z[121] + z[125];
z[75] = z[75] * z[110];
z[102] = z[75] + -z[102] + z[106] + z[121];
z[102] = abb[33] * z[102];
z[106] = z[14] + -z[90];
z[126] = abb[0] * z[106];
z[126] = -z[94] + z[124] + z[126];
z[72] = -z[72] + -z[103] + z[126];
z[72] = abb[32] * z[72];
z[127] = abb[32] * z[27];
z[127] = 2 * z[72] + -6 * z[127];
z[128] = abb[23] + abb[25];
z[129] = abb[26] + z[128];
z[130] = abb[27] + z[129];
z[130] = abb[36] * z[130];
z[131] = abb[27] + abb[28];
z[131] = abb[32] * z[131];
z[132] = z[130] + -z[131];
z[133] = 2 * abb[27] + abb[28];
z[134] = z[129] + z[133];
z[135] = -abb[33] + abb[34];
z[134] = z[134] * z[135];
z[132] = 2 * z[132] + -z[134];
z[134] = -abb[64] * z[132];
z[135] = abb[34] * z[70];
z[136] = abb[33] * z[70];
z[135] = z[135] + z[136];
z[135] = (T(1) / T(2)) * z[135];
z[137] = abb[19] * z[135];
z[8] = z[8] + z[29] + z[53] + z[73] + z[102] + -z[127] + z[134] + z[137];
z[8] = abb[31] * z[8];
z[29] = -z[5] + z[42] + z[47] + -z[57];
z[29] = abb[4] * z[29];
z[53] = 3 * abb[51] + -abb[58];
z[102] = -z[22] + -5 * z[38] + z[53] + z[57];
z[102] = abb[16] * z[102];
z[134] = z[83] + z[89];
z[120] = abb[9] * z[120];
z[137] = -z[120] + z[134];
z[106] = -abb[47] + z[106];
z[106] = abb[1] * z[106];
z[0] = abb[56] + z[0];
z[138] = z[0] + -z[5];
z[139] = abb[5] * z[138];
z[140] = abb[9] + z[95];
z[141] = abb[5] + z[140];
z[141] = abb[50] * z[141];
z[29] = z[29] + z[102] + z[106] + -z[137] + -z[139] + z[141];
z[29] = abb[36] * z[29];
z[102] = abb[5] * abb[50];
z[102] = z[102] + -z[139];
z[141] = abb[47] + z[26];
z[142] = 2 * abb[4];
z[143] = z[141] * z[142];
z[106] = -z[102] + z[103] + -z[106] + z[143];
z[106] = abb[34] * z[106];
z[143] = -z[11] + z[52];
z[1] = z[1] + z[143];
z[1] = abb[32] * z[1];
z[144] = abb[50] + z[23];
z[145] = -abb[47] + abb[51] + z[47] + -z[144];
z[145] = abb[36] * z[145];
z[61] = abb[31] * z[61];
z[141] = abb[34] * z[141];
z[146] = abb[33] * z[67];
z[1] = z[1] + z[61] + z[141] + z[145] + z[146];
z[1] = abb[8] * z[1];
z[61] = abb[51] + -abb[55];
z[141] = abb[56] + -z[57] + -z[61] + -3 * z[123];
z[141] = abb[6] * z[141];
z[120] = -z[83] + z[120] + z[141];
z[145] = abb[6] + z[140];
z[146] = abb[50] * z[145];
z[146] = z[120] + z[146];
z[147] = -abb[53] + z[3];
z[22] = 3 * abb[50] + -z[22] + -z[84] + z[147];
z[148] = -abb[16] * z[22];
z[31] = z[31] + z[47];
z[149] = -abb[53] + -z[3] + z[31];
z[150] = -z[41] + z[149];
z[151] = -abb[4] * z[150];
z[148] = z[146] + z[148] + z[151];
z[148] = abb[32] * z[148];
z[151] = abb[6] * abb[50];
z[89] = z[89] + z[151];
z[151] = z[89] + z[141];
z[26] = z[26] + z[123];
z[152] = z[26] * z[142];
z[152] = z[103] + z[151] + z[152];
z[153] = -abb[33] * z[152];
z[154] = abb[24] + abb[26];
z[155] = abb[27] + z[154];
z[155] = abb[32] * z[155];
z[156] = abb[27] * abb[34];
z[157] = abb[24] + z[129];
z[158] = abb[27] + z[157];
z[159] = abb[33] * z[158];
z[160] = abb[27] + z[128];
z[161] = abb[36] * z[160];
z[155] = z[155] + z[156] + -z[159] + -z[161];
z[156] = abb[64] * z[155];
z[140] = abb[50] * z[140];
z[137] = -z[137] + z[140];
z[140] = abb[64] * z[128];
z[140] = -z[137] + z[140];
z[159] = abb[53] + -abb[58];
z[49] = -z[47] + z[49] + -z[159];
z[162] = abb[16] * z[49];
z[163] = -abb[3] * z[67];
z[163] = -z[69] + z[140] + 2 * z[162] + z[163];
z[163] = abb[31] * z[163];
z[164] = -abb[32] * z[150];
z[26] = 2 * z[26];
z[165] = -abb[33] * z[26];
z[166] = abb[34] * z[67];
z[164] = z[164] + z[165] + z[166];
z[164] = abb[3] * z[164];
z[1] = z[1] + z[29] + z[106] + z[148] + z[153] + z[156] + z[163] + z[164];
z[29] = -4 * abb[55] + -z[42] + z[46] + z[96];
z[29] = abb[4] * z[29];
z[42] = -abb[51] + z[5];
z[46] = abb[50] + z[42] + -z[80] + z[159];
z[46] = abb[8] * z[46];
z[80] = abb[47] + z[91];
z[80] = z[80] * z[110];
z[91] = abb[3] * z[150];
z[39] = abb[14] * z[39];
z[106] = 4 * z[39];
z[29] = z[29] + z[46] + z[80] + z[91] + -z[106] + z[140] + 4 * z[162];
z[29] = abb[35] * z[29];
z[1] = 2 * z[1] + z[29];
z[1] = abb[35] * z[1];
z[29] = abb[48] + abb[52];
z[46] = abb[47] + z[29];
z[46] = 7 * abb[55] + 8 * abb[58] + z[9] + -z[13] + (T(-13) / T(2)) * z[46];
z[46] = abb[0] * z[46];
z[35] = abb[47] * (T(1) / T(2)) + z[35];
z[35] = abb[8] * z[35];
z[35] = -z[35] + -z[46] + z[64] + z[89];
z[46] = abb[47] + -abb[53];
z[80] = abb[56] * (T(1) / T(3));
z[89] = abb[50] * (T(1) / T(3));
z[25] = -abb[55] + abb[49] * (T(-5) / T(3)) + z[25] + (T(-2) / T(3)) * z[46] + -z[80] + z[89];
z[25] = abb[4] * z[25];
z[61] = abb[53] + (T(1) / T(3)) * z[61] + -z[80] + z[123];
z[61] = abb[6] * z[61];
z[31] = -abb[58] + z[31];
z[80] = -abb[56] + z[31];
z[91] = abb[46] + -abb[47];
z[80] = -abb[53] + (T(1) / T(3)) * z[80] + z[89] + (T(-13) / T(6)) * z[91];
z[80] = abb[3] * z[80];
z[89] = -z[39] + z[92];
z[91] = abb[58] * (T(-7) / T(3)) + abb[51] * (T(1) / T(6)) + abb[47] * (T(9) / T(2)) + (T(13) / T(6)) * z[29] + -z[47];
z[91] = abb[1] * z[91];
z[123] = abb[0] * z[59];
z[140] = -abb[28] + z[157];
z[148] = (T(1) / T(3)) * z[140];
z[150] = -abb[64] * z[148];
z[25] = z[25] + (T(-2) / T(3)) * z[27] + (T(-1) / T(3)) * z[35] + z[61] + z[80] + (T(11) / T(3)) * z[89] + z[91] + -z[123] + (T(-8) / T(3)) * z[124] + z[150];
z[35] = prod_pow(m1_set::bc<T>[0], 2);
z[25] = z[25] * z[35];
z[61] = abb[38] * z[115];
z[57] = -z[41] + z[57];
z[31] = z[31] + -z[57];
z[80] = z[5] + z[31] + -z[59];
z[80] = abb[33] * z[80];
z[87] = z[10] + z[87];
z[89] = abb[54] * (T(3) / T(4));
z[91] = abb[48] * (T(5) / T(4)) + z[87] + -z[89];
z[91] = abb[34] * z[91];
z[30] = z[30] + -z[100];
z[30] = 2 * z[30];
z[91] = -z[30] + -z[80] + z[91];
z[91] = abb[33] * z[91];
z[20] = -abb[56] + -z[20] + -z[47] + z[147];
z[20] = z[20] * z[58];
z[115] = -z[19] * z[32];
z[87] = abb[47] + -z[87] + -z[99];
z[87] = abb[34] * z[87];
z[109] = abb[36] * z[109];
z[100] = -z[100] + z[109];
z[87] = z[87] + 2 * z[100];
z[87] = abb[34] * z[87];
z[100] = -2 * z[51] + -z[62];
z[100] = z[67] * z[100];
z[109] = abb[56] + z[149];
z[147] = z[54] * z[109];
z[149] = abb[37] + -abb[39];
z[150] = -abb[38] + z[149];
z[150] = z[11] * z[150];
z[20] = z[20] + z[61] + (T(-3) / T(4)) * z[71] + z[87] + z[91] + z[100] + z[115] + z[147] + z[150];
z[20] = abb[3] * z[20];
z[61] = z[134] + z[139];
z[16] = -z[16] + -z[24] + z[46];
z[16] = abb[9] * z[16];
z[24] = z[4] + -z[13];
z[33] = abb[55] + z[33];
z[40] = z[24] + -z[33] + z[40];
z[40] = abb[1] * z[40];
z[71] = -abb[47] + -abb[49] + z[14] + z[143];
z[71] = abb[4] * z[71];
z[87] = abb[5] + z[95];
z[91] = abb[9] + -z[87];
z[91] = abb[50] * z[91];
z[16] = z[16] + z[40] + z[61] + z[71] + z[78] + z[91] + z[104] + z[106];
z[16] = z[16] * z[32];
z[40] = z[13] + -z[53] + -z[79];
z[40] = abb[0] * z[40];
z[53] = z[86] + 3 * z[94];
z[40] = -9 * z[27] + z[40] + -z[53] + z[64] + z[93] + -z[104] + -z[124] + -z[146];
z[40] = z[40] * z[51];
z[71] = -z[82] + z[105];
z[71] = abb[16] * z[71];
z[71] = z[71] + z[83];
z[63] = abb[8] * z[63];
z[79] = 3 * z[27];
z[82] = -z[64] + z[79];
z[91] = abb[50] * z[95];
z[93] = abb[28] + z[160];
z[100] = abb[64] * z[93];
z[90] = -abb[56] + z[90];
z[104] = abb[0] * z[90];
z[63] = -z[63] + -z[71] + -z[82] + z[91] + -z[92] + -z[100] + -2 * z[104] + -z[123];
z[91] = 2 * abb[65];
z[91] = -z[63] * z[91];
z[100] = abb[38] + abb[40];
z[100] = z[100] * z[128];
z[104] = abb[38] + z[149];
z[104] = abb[26] * z[104];
z[105] = abb[24] * abb[39];
z[106] = abb[27] * z[149];
z[100] = z[100] + z[104] + -z[105] + z[106];
z[104] = z[130] + z[131];
z[104] = 2 * z[104];
z[105] = -abb[28] + z[129];
z[105] = abb[34] * z[105];
z[106] = abb[33] * z[140];
z[105] = -z[104] + z[105] + -z[106];
z[105] = abb[33] * z[105];
z[115] = 2 * z[160];
z[123] = abb[26] + z[115];
z[32] = z[32] * z[123];
z[123] = z[133] + z[154];
z[51] = z[51] * z[123];
z[123] = abb[34] * z[129];
z[104] = -z[104] + z[123];
z[104] = abb[34] * z[104];
z[123] = abb[29] * abb[41];
z[115] = abb[67] * z[115];
z[32] = z[32] + -z[51] + 2 * z[100] + z[104] + -z[105] + -z[115] + -z[123];
z[51] = abb[31] * z[128];
z[51] = z[51] + z[155];
z[100] = abb[35] * z[128];
z[51] = 2 * z[51] + z[100];
z[51] = abb[35] * z[51];
z[97] = abb[31] * z[97];
z[97] = z[97] + -z[132];
z[97] = abb[31] * z[97];
z[100] = z[68] * z[140];
z[35] = z[35] * z[148];
z[104] = 2 * z[93];
z[105] = abb[65] * z[104];
z[35] = z[32] + -z[35] + z[51] + z[97] + -z[100] + z[105];
z[51] = abb[61] + abb[62] + abb[63];
z[35] = z[35] * z[51];
z[7] = -abb[47] + -abb[55] + z[7] + -z[9] + -z[77] + z[89];
z[7] = abb[0] * z[7];
z[5] = z[5] + -z[24] + -z[33];
z[5] = abb[1] * z[5];
z[9] = abb[49] + z[41] + z[46];
z[24] = -abb[4] * z[9];
z[5] = z[5] + z[7] + z[24] + -z[27] + z[101] + z[102] + -z[113];
z[5] = abb[34] * z[5];
z[7] = -abb[48] + -abb[58] + z[47] + -z[50] + -z[85];
z[7] = z[7] * z[110];
z[9] = z[9] * z[142];
z[7] = z[7] + z[9] + -z[56] + z[117];
z[7] = abb[36] * z[7];
z[5] = z[5] + z[7] + z[127];
z[5] = abb[34] * z[5];
z[7] = -z[64] + -z[78] + z[86] + z[98] + z[111] + -z[121];
z[7] = abb[34] * z[7];
z[6] = z[6] + z[37] + -z[119];
z[6] = abb[0] * z[6];
z[6] = z[6] + z[76] + (T(-3) / T(2)) * z[94] + 3 * z[124] + z[151];
z[6] = abb[33] * z[6];
z[6] = z[6] + z[7] + -z[73] + -z[127];
z[6] = abb[33] * z[6];
z[7] = abb[64] * z[32];
z[9] = -abb[47] + z[48] + -z[57];
z[9] = abb[4] * z[9];
z[24] = abb[47] + z[90];
z[24] = z[24] * z[110];
z[27] = abb[50] * z[87];
z[9] = -z[9] + z[24] + -z[27] + z[39] + z[71] + z[139];
z[24] = -z[9] * z[66];
z[10] = abb[46] * (T(3) / T(4)) + -z[10] + -z[99];
z[10] = abb[20] * z[10];
z[21] = -abb[21] * z[21];
z[0] = abb[50] + -z[0] + z[59];
z[0] = abb[22] * z[0];
z[27] = (T(-1) / T(4)) * z[70];
z[27] = abb[0] * z[27];
z[32] = abb[30] * z[70];
z[0] = z[0] + z[10] + z[21] + z[27] + z[32];
z[0] = abb[41] * z[0];
z[10] = abb[3] + abb[4];
z[10] = z[10] * z[31];
z[21] = abb[64] * z[140];
z[10] = -z[10] + z[21] + -z[82] + z[126] + z[151];
z[21] = -z[10] * z[68];
z[27] = -z[54] * z[120];
z[31] = z[52] + -z[118];
z[31] = abb[9] * z[31];
z[31] = z[31] + -2 * z[83];
z[31] = abb[37] * z[31];
z[32] = abb[38] * z[138];
z[33] = abb[39] * z[109];
z[37] = -abb[38] + -abb[39];
z[37] = abb[50] * z[37];
z[41] = -abb[40] * z[67];
z[32] = z[32] + z[33] + z[37] + z[41];
z[32] = z[32] * z[142];
z[22] = -z[22] * z[149];
z[33] = z[49] * z[62];
z[22] = z[22] + z[33];
z[22] = abb[16] * z[22];
z[33] = -abb[55] + abb[56] + -abb[57];
z[33] = abb[54] + 4 * z[33] + z[42] + z[116];
z[33] = abb[38] * z[33];
z[19] = -z[19] * z[58];
z[19] = z[19] + z[33];
z[19] = abb[1] * z[19];
z[33] = abb[37] + abb[38];
z[33] = -z[33] * z[117];
z[15] = -z[15] + z[36] + -z[38] + -z[44];
z[15] = abb[41] * z[15];
z[36] = -abb[33] * z[135];
z[15] = z[15] + z[36];
z[15] = abb[19] * z[15];
z[36] = -abb[39] * z[145];
z[37] = -abb[9] + z[95];
z[37] = abb[37] * z[37];
z[38] = abb[5] * abb[38];
z[36] = z[36] + z[37] + z[38];
z[36] = z[11] * z[36];
z[37] = -z[62] * z[137];
z[38] = 2 * z[139];
z[41] = -abb[38] * z[38];
z[42] = abb[34] + -abb[36];
z[44] = -abb[47] + z[4];
z[42] = z[42] * z[44];
z[42] = 2 * z[42];
z[46] = abb[34] * z[42];
z[44] = abb[38] * z[44];
z[44] = z[44] + z[46];
z[44] = abb[11] * z[44];
z[0] = abb[68] + z[0] + z[1] + z[2] + z[5] + z[6] + z[7] + z[8] + z[15] + z[16] + z[19] + z[20] + z[21] + 2 * z[22] + z[24] + z[25] + z[27] + z[31] + z[32] + z[33] + z[35] + z[36] + z[37] + z[40] + z[41] + z[44] + z[91];
z[1] = z[3] + z[52] + -z[55] + z[88];
z[1] = abb[9] * z[1];
z[1] = z[1] + -z[103];
z[2] = -z[14] + z[84] + -z[85];
z[2] = abb[0] * z[2];
z[3] = 3 * abb[9];
z[5] = z[3] + -z[95];
z[5] = abb[50] * z[5];
z[2] = -z[1] + z[2] + z[5] + -z[94] + -z[124] + z[134];
z[5] = z[34] + z[96];
z[6] = abb[52] + -z[74];
z[6] = abb[46] + -z[5] + 2 * z[6] + -z[81];
z[6] = abb[3] * z[6];
z[7] = 2 * z[64] + -z[114];
z[8] = -abb[19] * z[70];
z[14] = abb[64] * z[104];
z[2] = 2 * z[2] + z[6] + -z[7] + z[8] + z[14] + -z[75] + 8 * z[92] + -z[112];
z[2] = abb[31] * z[2];
z[6] = abb[52] + -abb[55] + -z[17] + z[18] + z[45];
z[6] = abb[1] * z[6];
z[3] = -z[3] + z[87];
z[3] = abb[50] * z[3];
z[1] = z[1] + z[3] + z[6] + -z[61] + z[69];
z[1] = abb[36] * z[1];
z[3] = abb[28] * abb[34];
z[3] = z[3] + z[106] + z[131] + z[161];
z[6] = abb[64] * z[3];
z[8] = abb[3] * z[26];
z[14] = abb[64] * z[158];
z[15] = -abb[8] * z[67];
z[8] = z[8] + z[14] + z[15] + z[152];
z[8] = abb[35] * z[8];
z[14] = -abb[32] * z[79];
z[1] = z[1] + -z[6] + z[8] + z[14] + z[72];
z[6] = -z[7] + z[53];
z[7] = z[13] + z[107];
z[8] = abb[48] + abb[54];
z[7] = 2 * z[7] + -z[8];
z[7] = abb[0] * z[7];
z[13] = abb[54] + -z[13] + -z[108];
z[13] = z[13] * z[110];
z[14] = -abb[5] * z[11];
z[7] = -z[6] + z[7] + z[13] + z[14] + z[38] + z[112] + z[125];
z[7] = abb[34] * z[7];
z[13] = abb[52] + z[119];
z[5] = -z[5] + 2 * z[13];
z[5] = abb[0] * z[5];
z[11] = -abb[6] * z[11];
z[5] = z[5] + z[6] + z[11] + z[75] + -z[122] + -6 * z[124] + -2 * z[141];
z[5] = abb[33] * z[5];
z[6] = -abb[55] + -z[60];
z[6] = z[4] + 2 * z[6] + z[12];
z[6] = abb[33] * z[6];
z[11] = -abb[52] + z[23];
z[8] = -z[8] + 2 * z[11] + z[12];
z[8] = abb[31] * z[8];
z[11] = z[29] + -z[144];
z[11] = abb[36] * z[11];
z[11] = z[11] + -z[28];
z[6] = z[6] + z[8] + 2 * z[11];
z[6] = abb[8] * z[6];
z[4] = -abb[46] + z[4];
z[4] = abb[34] * z[4];
z[4] = z[4] + z[30] + 2 * z[80];
z[4] = abb[3] * z[4];
z[8] = -abb[11] * z[42];
z[11] = abb[19] * z[136];
z[12] = abb[36] * z[39];
z[1] = 2 * z[1] + z[2] + z[4] + z[5] + z[6] + z[7] + z[8] + z[11] + -8 * z[12];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[42] * z[63];
z[4] = -abb[8] * z[65];
z[5] = -abb[64] * z[160];
z[4] = z[4] + z[5] + -z[9];
z[4] = abb[44] * z[4];
z[2] = z[2] + -z[4];
z[4] = abb[8] * z[43];
z[4] = -z[4] + z[10];
z[4] = -2 * z[4];
z[4] = abb[43] * z[4];
z[5] = abb[31] * z[93];
z[6] = abb[35] * z[158];
z[3] = -z[3] + z[5] + z[6];
z[3] = m1_set::bc<T>[0] * z[3];
z[5] = abb[44] * z[160];
z[6] = abb[43] * z[140];
z[7] = abb[42] * z[93];
z[3] = z[3] + -z[5] + -z[6] + z[7];
z[5] = 2 * z[51];
z[3] = z[3] * z[5];
z[1] = abb[45] + z[1] + -2 * z[2] + z[3] + z[4];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_598_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("33.706053055515298288839095570101245313636647942428658327598171961"),stof<T>("50.961377933537765307332419140777549464780828639273489602082343258")}, std::complex<T>{stof<T>("11.858026132483910657718379006669116221305018847510705623791277394"),stof<T>("-56.879646427640951417939862551058491074130978128676838193480500277")}, std::complex<T>{stof<T>("15.906370797905423851752454953211023491375675565932195527849166268"),stof<T>("11.057996405110180876423471423951151950785385011025780364855342733")}, std::complex<T>{stof<T>("19.951921924348071445516545983985641398493729237037885813901714986"),stof<T>("-9.195139706723453282066128734824871446144474898744586122859910221")}, std::complex<T>{stof<T>("-26.838331748098920589511380876061648957629331972514306536496090407"),stof<T>("-11.156613126976248734663096518086661797552330618153893688362247169")}, std::complex<T>{stof<T>("3.99214115123710515555086421751400831887046925024364274524650727"),stof<T>("-18.091150631019225221082721300622652058801548881111807164189096765")}, std::complex<T>{stof<T>("32.551977062607529083248990298121337283831124709239946400888151237"),stof<T>("21.466638565505560250781031719302091742362819897581158019157754403")}, std::complex<T>{stof<T>("-0.2763918106676341814796327240858328421747738605893962837744060502"),stof<T>("1.9232461890621132253448052434278699448465971300673805737648824541")}, std::complex<T>{stof<T>("-1.721504163271503338186745204545680007331323486481997119145553561"),stof<T>("-28.401176069548536737200656501838082003612038160539071494984603999")}, std::complex<T>{stof<T>("6.7353799965619372045429879339872034198064024037499747569895055434"),stof<T>("-1.3422343308323370821987211391889269365807527714048069895205015157")}, stof<T>("-8.1467639364098125700497265545092086142014652425291793387481963645"), std::complex<T>{stof<T>("-38.162658122975090905833493810102835172170037353845123221464291653"),stof<T>("15.647282074402721483579294507257428545894794410168039337949283315")}, std::complex<T>{stof<T>("1.7314772302804613735427175946745870918310611219429528881459093378"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("1.7314772302804613735427175946745870918310611219429528881459093378"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("-3.1923472445362258158409460442271726429200038553757622486786770783")}, std::complex<T>{stof<T>("2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("-3.1923472445362258158409460442271726429200038553757622486786770783")}, std::complex<T>{stof<T>("2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("-3.1923472445362258158409460442271726429200038553757622486786770783")}, std::complex<T>{stof<T>("2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("-3.1923472445362258158409460442271726429200038553757622486786770783")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[36].real()/kbase.W[36].real()), rlog(k.W[75].real()/kbase.W[75].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[123].real()/kbase.W[123].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_598_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_598_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(112)) * (v[2] + v[3]) * (56 * (-4 * m1_set::bc<T>[1] + -2 * m1_set::bc<T>[4] + m1_set::bc<T>[2] * (3 + v[0])) + -42 * v[2] + -42 * v[3] + 40 * v[4] + 56 * (m1_set::bc<T>[1] * (v[2] + v[3]) + v[5] + -2 * m1_set::bc<T>[1] * v[5]) + 7 * (4 * (-2 + m1_set::bc<T>[2]) * v[1] + 4 * m1_set::bc<T>[4] * (v[2] + v[3] + -2 * v[5]) + m1_set::bc<T>[2] * (v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5])))) / prod_pow(tend, 2);
c[1] = ((4 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2] + 2 * m1_set::bc<T>[4]) * (v[2] + v[3])) / tend;


		return (abb[49] + 2 * abb[50] + -abb[51] + -2 * abb[53] + -abb[55]) * (2 * t * c[0] + -c[1]) * (T(1) / T(2));
	}
	{
T z[5];
z[0] = -abb[37] + 2 * abb[39];
z[1] = prod_pow(abb[32], 2);
z[0] = -2 * abb[40] + -2 * z[0] + -5 * z[1];
z[1] = -abb[49] + -2 * abb[50] + abb[51] + 2 * abb[53] + abb[55];
z[0] = z[0] * z[1];
z[2] = -abb[32] * z[1];
z[1] = -abb[35] * z[1];
z[3] = -z[1] + z[2];
z[4] = abb[31] + abb[33] + -abb[34];
z[4] = -2 * z[4];
z[3] = z[3] * z[4];
z[1] = -z[1] + -4 * z[2];
z[1] = abb[35] * z[1];
z[0] = z[0] + z[1] + z[3];
return abb[12] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_598_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[49] + 2 * abb[50] + -abb[51] + -2 * abb[53] + -abb[55]) * (t * c[0] + -c[1]);
	}
	{
T z[3];
z[0] = abb[50] + -abb[53];
z[1] = abb[32] + -abb[35];
z[2] = 2 * z[1];
z[0] = z[0] * z[2];
z[2] = abb[49] + -abb[51] + -abb[55];
z[1] = z[1] * z[2];
z[0] = z[0] + z[1];
return 2 * abb[12] * m1_set::bc<T>[0] * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_598_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-43.837623605525059103782776619003555742200251039657810455806317352"),stof<T>("-34.859701539291110108982035290351984755759745157931530015992864135")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,69> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W15(k,dl), dlog_W18(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W26(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W76(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_19(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_11_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[36].real()/k.W[36].real()), rlog(kend.W[75].real()/k.W[75].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real()), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_4_re(k), f_2_7_re(k), f_2_11_re(k), T{0}};
abb[45] = SpDLog_f_4_598_W_19_Im(t, path, abb);
abb[68] = SpDLog_f_4_598_W_19_Re(t, path, abb);

                    
            return f_4_598_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_598_DLogXconstant_part(base_point<T>, kend);
	value += f_4_598_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_598_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_598_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_598_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_598_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_598_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_598_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
