/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_532.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_532_abbreviated (const std::array<T,19>& abb) {
T z[23];
z[0] = abb[14] + -abb[15];
z[1] = 2 * z[0];
z[2] = abb[12] + abb[13];
z[3] = z[1] + -z[2];
z[4] = abb[3] * z[3];
z[5] = 5 * z[0];
z[6] = 2 * abb[12];
z[7] = 6 * abb[13] + -z[5] + -z[6];
z[8] = abb[2] * z[7];
z[9] = abb[12] * (T(1) / T(2));
z[10] = -abb[16] + abb[13] * (T(-15) / T(2)) + 7 * z[0] + z[9];
z[10] = abb[5] * z[10];
z[11] = 2 * abb[14];
z[12] = -abb[15] + z[11];
z[13] = abb[16] * (T(3) / T(2)) + -z[12];
z[14] = z[9] + z[13];
z[14] = abb[0] * z[14];
z[15] = 6 * abb[14] + abb[15];
z[16] = abb[12] + abb[16] * (T(-17) / T(2)) + abb[13] * (T(11) / T(2)) + z[15];
z[16] = abb[1] * z[16];
z[10] = -z[4] + z[8] + z[10] + z[14] + z[16];
z[10] = prod_pow(abb[6], 2) * z[10];
z[14] = abb[12] * (T(2) / T(3));
z[16] = 2 * abb[13];
z[17] = (T(5) / T(3)) * z[0] + z[14] + -z[16];
z[17] = abb[2] * z[17];
z[18] = -abb[14] + abb[15] * (T(-8) / T(3));
z[9] = 9 * abb[16] + abb[13] * (T(-55) / T(6)) + -z[9] + 2 * z[18];
z[9] = abb[1] * z[9];
z[5] = abb[13] * (T(-16) / T(3)) + z[5] + z[14];
z[5] = abb[3] * z[5];
z[14] = abb[12] * (T(-1) / T(3)) + abb[16] * (T(11) / T(12)) + abb[13] * (T(29) / T(4)) + (T(-20) / T(3)) * z[0];
z[14] = abb[5] * z[14];
z[18] = abb[14] + abb[15];
z[18] = abb[16] * (T(-25) / T(12)) + abb[13] * (T(5) / T(4)) + (T(2) / T(3)) * z[18];
z[18] = abb[0] * z[18];
z[5] = z[5] + z[9] + z[14] + z[17] + z[18];
z[5] = prod_pow(m1_set::bc<T>[0], 2) * z[5];
z[9] = -abb[12] + abb[13] * (T(3) / T(2));
z[14] = abb[16] * (T(1) / T(2));
z[17] = -z[1] + z[9] + z[14];
z[17] = abb[5] * z[17];
z[18] = abb[13] * (T(23) / T(2)) + -12 * z[0] + -z[14];
z[18] = abb[1] * z[18];
z[19] = 3 * abb[13];
z[20] = -abb[12] + -z[1] + z[19];
z[21] = abb[3] * z[20];
z[22] = 2 * abb[2];
z[22] = z[7] * z[22];
z[1] = -abb[13] + z[1];
z[1] = -abb[12] + abb[16] + 2 * z[1];
z[1] = abb[0] * z[1];
z[1] = z[1] + z[17] + z[18] + 4 * z[21] + -z[22];
z[17] = abb[17] * z[1];
z[11] = -3 * abb[15] + abb[12] * (T(5) / T(2)) + z[11] + z[14] + -z[19];
z[11] = abb[0] * z[11];
z[9] = z[9] + z[13];
z[9] = abb[1] * z[9];
z[13] = abb[12] * (T(-3) / T(2)) + abb[13] * (T(7) / T(2)) + -3 * z[0];
z[13] = abb[5] * z[13];
z[4] = -z[4] + -z[8] + z[9] + z[11] + z[13];
z[4] = abb[8] * z[4];
z[9] = abb[15] + -abb[16] + z[16];
z[11] = 2 * abb[1];
z[9] = z[9] * z[11];
z[11] = 3 * abb[16];
z[13] = abb[13] + abb[15];
z[13] = abb[12] + -z[11] + 2 * z[13];
z[13] = abb[0] * z[13];
z[14] = 2 * abb[3];
z[16] = z[3] * z[14];
z[18] = -abb[13] + z[0];
z[19] = -abb[12] + -abb[16] + 4 * z[18];
z[19] = abb[5] * z[19];
z[9] = z[9] + z[13] + -z[16] + z[19];
z[13] = -abb[6] * z[9];
z[4] = z[4] + z[13];
z[4] = abb[8] * z[4];
z[3] = abb[0] * z[3];
z[13] = z[14] * z[20];
z[19] = abb[12] + -7 * abb[13] + 6 * z[0];
z[19] = abb[1] * z[19];
z[21] = abb[5] * z[0];
z[3] = -z[3] + z[8] + -z[13] + z[19] + z[21];
z[8] = -abb[6] + abb[8];
z[8] = z[3] * z[8];
z[13] = abb[1] * z[20];
z[19] = -abb[12] + abb[13];
z[14] = z[14] * z[19];
z[19] = -abb[0] * z[2];
z[13] = z[13] + z[14] + z[19];
z[13] = abb[7] * z[13];
z[8] = z[8] + z[13];
z[13] = 2 * abb[7];
z[8] = z[8] * z[13];
z[14] = abb[1] + -abb[2];
z[7] = z[7] * z[14];
z[2] = z[0] + -z[2];
z[2] = abb[0] * z[2];
z[14] = -abb[12] + -5 * z[18];
z[14] = abb[3] * z[14];
z[2] = 2 * z[2] + z[7] + z[14];
z[2] = abb[9] * z[2];
z[2] = abb[18] + 2 * z[2] + z[4] + z[5] + z[8] + z[10] + z[17];
z[4] = -11 * abb[13] + 17 * abb[16] + -z[6] + -2 * z[15];
z[4] = abb[1] * z[4];
z[5] = -abb[12] + -z[11] + 2 * z[12];
z[5] = abb[0] * z[5];
z[0] = -abb[12] + 15 * abb[13] + 2 * abb[16] + -14 * z[0];
z[0] = abb[5] * z[0];
z[0] = z[0] + z[4] + z[5] + z[16] + -z[22];
z[0] = abb[6] * z[0];
z[3] = z[3] * z[13];
z[4] = abb[8] * z[9];
z[0] = z[0] + z[3] + z[4];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[10] * z[1];
z[0] = abb[11] + z[0] + z[1];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_532_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-3.6756231802737492070749810551109064302499865424455147221346925815"),stof<T>("4.4130189505897218903879246308024838571594931643980745090031768856")}, std::complex<T>{stof<T>("-18.878370835837848538649938923107459941779247221287072881038958847"),stof<T>("79.082651372755930187787464754257656126373749482094282514297654142")}, std::complex<T>{stof<T>("-41.514945012895204287658570427224459402033918851551928848621247693"),stof<T>("12.254074969965383646783960709058353157382751733080941158077351382")}, std::complex<T>{stof<T>("0.468414229752505018573695503499882974235244562424447756676456602"),stof<T>("64.391778875152151710287856426830044636193578336617131839474114508")}, std::complex<T>{stof<T>("54.271288944576727700548038691902744276323460764530039722613567944"),stof<T>("-96.504439122771283863817477974499575150794049309489408288065761875")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[24].real()/kbase.W[24].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_532_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_532_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (24 + 5 * v[0] + 7 * v[1] + 5 * v[2] + -7 * v[3] + -12 * v[4] + -8 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -5 * v[5]) * (-v[0] + v[1] + -v[2] + -v[3] + v[5])) / prod_pow(tend, 2);
c[1] = ((1 + 4 * m1_set::bc<T>[1]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[12] + -abb[13] + abb[14] + -abb[15]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -prod_pow(abb[8], 2);
z[1] = prod_pow(abb[7], 2);
z[0] = z[0] + z[1];
z[0] = -abb[9] + 2 * z[0];
z[1] = abb[12] + -abb[13] + abb[14] + -abb[15];
return 2 * abb[4] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_532_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_532_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("27.113351616561778918705793892114765261806508857813368626000585203"),stof<T>("-14.230293883394392108381521562964687631640501191524168955242110771")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,19> abb = {dlog_W4(k,dl), dl[2], dlog_W8(k,dl), dlog_W10(k,dl), dlog_W17(k,dl), dlog_W25(k,dl), f_1_5(k), f_1_9(k), f_1_11(k), f_2_15(k), f_2_12_im(k), T{0}, rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[24].real()/k.W[24].real()), f_2_12_re(k), T{0}};
abb[11] = SpDLog_f_4_532_W_17_Im(t, path, abb);
abb[18] = SpDLog_f_4_532_W_17_Re(t, path, abb);

                    
            return f_4_532_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_532_DLogXconstant_part(base_point<T>, kend);
	value += f_4_532_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_532_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_532_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_532_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_532_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_532_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_532_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
