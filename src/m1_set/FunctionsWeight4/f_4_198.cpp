/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_198.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_198_abbreviated (const std::array<T,16>& abb) {
T z[19];
z[0] = abb[1] * abb[6];
z[1] = abb[2] * abb[6];
z[2] = z[0] + -z[1];
z[3] = abb[11] + -abb[12];
z[4] = 2 * abb[13];
z[5] = z[3] + z[4];
z[2] = z[2] * z[5];
z[6] = abb[2] * z[5];
z[7] = abb[1] + abb[5];
z[8] = -abb[12] + abb[13];
z[7] = z[7] * z[8];
z[9] = z[6] + -z[7];
z[9] = abb[8] * z[9];
z[10] = abb[3] * z[5];
z[11] = 2 * abb[6] + -abb[8];
z[11] = z[10] * z[11];
z[12] = abb[5] * z[8];
z[13] = abb[11] + abb[13];
z[13] = abb[1] * z[13];
z[14] = z[12] + -z[13];
z[3] = -abb[13] + (T(-1) / T(2)) * z[3];
z[15] = -abb[3] * z[3];
z[15] = (T(-1) / T(2)) * z[14] + z[15];
z[15] = abb[7] * z[15];
z[2] = z[2] + z[9] + z[11] + z[15];
z[2] = abb[7] * z[2];
z[9] = -abb[13] + abb[14];
z[9] = abb[0] * z[9];
z[11] = -4 * abb[12] + -5 * abb[13] + abb[11] * (T(-5) / T(2)) + abb[14] * (T(13) / T(2));
z[11] = abb[3] * z[11];
z[11] = (T(-13) / T(2)) * z[9] + z[11] + -4 * z[12] + (T(-5) / T(2)) * z[13];
z[11] = prod_pow(m1_set::bc<T>[0], 2) * z[11];
z[12] = -abb[1] + -abb[2];
z[13] = prod_pow(abb[6], 2);
z[12] = z[3] * z[12] * z[13];
z[15] = -abb[1] + 3 * abb[5];
z[8] = z[8] * z[15];
z[15] = abb[2] * z[3];
z[15] = (T(1) / T(2)) * z[8] + 2 * z[9] + z[15];
z[16] = prod_pow(abb[8], 2);
z[15] = z[15] * z[16];
z[4] = abb[11] + abb[12] + -2 * abb[14] + z[4];
z[17] = z[4] * z[13];
z[3] = z[3] * z[16];
z[3] = z[3] + z[17];
z[3] = abb[3] * z[3];
z[17] = -abb[2] + abb[3];
z[17] = z[5] * z[17];
z[7] = z[7] + z[17];
z[17] = -abb[15] * z[7];
z[18] = abb[4] * z[5];
z[13] = -z[13] + z[16];
z[13] = z[13] * z[18];
z[16] = -abb[1] + abb[2] + -2 * abb[3];
z[16] = abb[9] * z[5] * z[16];
z[2] = z[2] + z[3] + (T(1) / T(3)) * z[11] + z[12] + z[13] + z[15] + z[16] + z[17];
z[0] = -z[0] + -z[1];
z[0] = z[0] * z[5];
z[1] = z[6] + -z[8] + -4 * z[9];
z[1] = abb[8] * z[1];
z[3] = -z[10] + z[14];
z[3] = abb[7] * z[3];
z[5] = abb[8] * z[5];
z[4] = -abb[6] * z[4];
z[4] = 2 * z[4] + z[5];
z[4] = abb[3] * z[4];
z[5] = abb[6] + -abb[8];
z[5] = z[5] * z[18];
z[0] = z[0] + z[1] + z[3] + z[4] + 2 * z[5];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = -abb[10] * z[7];
z[0] = z[0] + z[1];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_198_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-10.3158474499474235812904333284176338175975803272889389813487477108"),stof<T>("7.8931180814845717449640795253122374900794762907837552942142767117")}, std::complex<T>{stof<T>("-2.9912777436305789428978528457383932968371718357453958911947530096"),stof<T>("-5.4116768818873198712108339919089631647125985835247459481623587089")}, std::complex<T>{stof<T>("-21.415075639426640868783506906055062794963546612808839102053731135"),stof<T>("-2.903418851856143217487264823193268905204814573308695354534522105")}, std::complex<T>{stof<T>("14.090505933109796230390926423375822274203138121265296011899736434"),stof<T>("16.208213815228034833662178340414469559996889447617196596911157526")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[17].real()/kbase.W[17].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_198_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_198_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("12.403640638573959220084956348643139735955429812387611616833464871"),stof<T>("10.151857958599264605363044384573144179692783209098171058694436592")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,16> abb = {dlog_W3(k,dl), dlog_W7(k,dl), dlog_W8(k,dl), dl[4], dlog_W18(k,dl), dlog_W26(k,dl), f_1_6(k), f_1_8(k), f_1_10(k), f_2_8(k), f_2_11_im(k), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[17].real()/k.W[17].real()), f_2_11_re(k)};

                    
            return f_4_198_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_198_DLogXconstant_part(base_point<T>, kend);
	value += f_4_198_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_198_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_198_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_198_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_198_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_198_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_198_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
