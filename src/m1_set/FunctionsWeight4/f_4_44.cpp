/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_44.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_44_abbreviated (const std::array<T,29>& abb) {
T z[60];
z[0] = 2 * abb[6];
z[1] = abb[1] + -abb[3] + 4 * abb[4] + -z[0];
z[1] = abb[24] * z[1];
z[2] = abb[2] * abb[24];
z[3] = abb[0] * abb[24];
z[1] = z[1] + -z[2] + -z[3];
z[4] = abb[3] + -abb[6];
z[5] = abb[1] * (T(1) / T(2));
z[6] = (T(-1) / T(3)) * z[4] + -z[5];
z[7] = abb[0] * (T(1) / T(6));
z[8] = abb[4] * (T(4) / T(3));
z[9] = z[6] + z[7] + -z[8];
z[9] = abb[22] * z[9];
z[10] = abb[1] + z[0];
z[11] = abb[3] * (T(1) / T(2));
z[12] = z[10] + z[11];
z[13] = abb[0] + abb[2];
z[8] = -z[8] + (T(1) / T(3)) * z[12] + (T(1) / T(2)) * z[13];
z[8] = abb[21] * z[8];
z[12] = abb[2] * (T(1) / T(6));
z[6] = z[6] + z[12];
z[6] = abb[23] * z[6];
z[5] = abb[6] + z[5];
z[5] = (T(1) / T(3)) * z[5] + -z[11];
z[7] = abb[2] * (T(-7) / T(6)) + -z[5] + -z[7];
z[7] = abb[20] * z[7];
z[10] = abb[3] + 2 * z[10] + (T(11) / T(2)) * z[13];
z[10] = abb[4] * (T(-7) / T(2)) + (T(1) / T(3)) * z[10];
z[10] = abb[19] * z[10];
z[5] = abb[0] * (T(-7) / T(6)) + abb[4] * (T(13) / T(6)) + -z[5] + -z[12];
z[5] = abb[18] * z[5];
z[11] = abb[21] + abb[23] + -abb[24];
z[12] = 4 * z[11];
z[14] = abb[20] * (T(13) / T(2)) + -z[12];
z[14] = abb[19] * (T(-7) / T(2)) + (T(1) / T(3)) * z[14];
z[14] = abb[5] * z[14];
z[1] = (T(1) / T(3)) * z[1] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[14];
z[5] = prod_pow(m1_set::bc<T>[0], 2);
z[1] = z[1] * z[5];
z[6] = 2 * abb[4];
z[7] = abb[0] + z[4] + z[6];
z[7] = abb[22] * z[7];
z[8] = 2 * abb[3];
z[9] = -z[0] + z[8];
z[10] = 5 * abb[1] + z[9];
z[14] = -3 * abb[0] + z[6] + z[10];
z[14] = abb[21] * z[14];
z[15] = 3 * abb[1];
z[16] = z[4] + z[15];
z[17] = 2 * abb[0];
z[18] = z[16] + -z[17];
z[18] = abb[20] * z[18];
z[9] = z[9] + z[15];
z[9] = abb[24] * z[9];
z[19] = abb[1] + z[4];
z[20] = abb[23] * z[19];
z[21] = abb[24] * z[6];
z[7] = -z[3] + -z[7] + z[9] + -z[14] + z[18] + -z[20] + z[21];
z[14] = -abb[6] + z[15];
z[14] = 5 * abb[3] + 4 * z[14];
z[15] = -abb[2] + z[14];
z[15] = 9 * abb[0] + -3 * abb[4] + -2 * z[15];
z[15] = abb[19] * z[15];
z[8] = 4 * abb[1] + -abb[6] + z[8];
z[18] = -abb[2] + z[8];
z[18] = abb[0] + -abb[4] + 2 * z[18];
z[18] = abb[18] * z[18];
z[7] = 2 * z[7] + z[15] + z[18];
z[15] = abb[27] * z[7];
z[18] = abb[11] + abb[13];
z[20] = -abb[14] + z[18];
z[22] = 2 * z[20];
z[23] = -abb[12] + z[22];
z[23] = abb[12] * z[23];
z[24] = abb[13] * z[18];
z[25] = abb[14] * z[18];
z[26] = prod_pow(abb[11], 2);
z[27] = z[23] + -z[24] + z[25] + -z[26];
z[28] = abb[6] * z[27];
z[29] = 2 * abb[11];
z[30] = abb[13] + z[29];
z[31] = abb[13] * z[30];
z[31] = z[26] + z[31];
z[32] = abb[12] + -z[20];
z[33] = abb[12] * z[32];
z[34] = z[31] + 4 * z[33];
z[35] = 2 * z[18];
z[36] = -abb[14] + z[35];
z[36] = abb[14] * z[36];
z[37] = z[34] + -z[36];
z[38] = 2 * abb[1];
z[39] = z[37] * z[38];
z[39] = z[28] + z[39];
z[37] = abb[3] * z[37];
z[37] = z[37] + z[39];
z[40] = abb[8] + abb[9];
z[41] = abb[15] * z[40];
z[41] = z[37] + -z[41];
z[42] = 2 * abb[13];
z[43] = abb[11] + z[42];
z[44] = 2 * abb[14];
z[45] = z[43] + -z[44];
z[45] = abb[14] * z[45];
z[33] = 3 * z[33];
z[46] = 2 * z[26];
z[47] = abb[11] * abb[13];
z[45] = -z[33] + z[45] + -z[46] + -z[47];
z[45] = abb[0] * z[45];
z[30] = z[30] + -z[44];
z[30] = abb[14] * z[30];
z[43] = -abb[13] * z[43];
z[30] = z[30] + -z[33] + z[43];
z[30] = abb[2] * z[30];
z[33] = 3 * abb[12];
z[43] = -z[20] + z[33];
z[43] = abb[12] * z[43];
z[44] = abb[11] * abb[14];
z[44] = z[44] + -z[47];
z[48] = -z[43] + z[44] + z[46];
z[48] = abb[4] * z[48];
z[30] = z[30] + 2 * z[41] + z[45] + z[48];
z[30] = abb[19] * z[30];
z[41] = -abb[11] + abb[14];
z[45] = z[41] + z[42];
z[45] = abb[13] * z[45];
z[43] = -z[43] + z[45];
z[43] = abb[19] * z[43];
z[45] = prod_pow(abb[13], 2);
z[48] = prod_pow(abb[12], 2);
z[49] = z[45] + -z[48];
z[49] = z[11] * z[49];
z[50] = 2 * abb[12];
z[51] = -z[20] + z[50];
z[52] = abb[12] * z[51];
z[53] = -abb[11] + abb[13];
z[54] = -abb[14] + -z[53];
z[54] = abb[13] * z[54];
z[54] = z[52] + z[54];
z[54] = abb[20] * z[54];
z[43] = z[43] + z[49] + z[54];
z[43] = abb[5] * z[43];
z[30] = z[30] + z[43];
z[43] = abb[9] * z[27];
z[49] = abb[7] + z[40];
z[54] = abb[28] * z[49];
z[55] = abb[7] + -abb[9];
z[56] = abb[27] * z[55];
z[54] = z[54] + -z[56];
z[45] = -z[26] + z[45];
z[45] = abb[7] * z[45];
z[56] = abb[14] * z[20];
z[23] = z[23] + (T(3) / T(2)) * z[56];
z[57] = abb[13] + abb[11] * (T(3) / T(2));
z[57] = abb[13] * z[57];
z[57] = -z[23] + z[57];
z[58] = abb[8] * z[57];
z[5] = z[5] * z[40];
z[13] = 3 * abb[3] + abb[6] + -6 * abb[10] + (T(3) / T(2)) * z[13];
z[13] = abb[15] * z[13];
z[5] = (T(-1) / T(3)) * z[5] + z[13] + -z[43] + z[45] + -2 * z[54] + z[58];
z[13] = abb[25] + abb[26];
z[5] = -z[5] * z[13];
z[43] = -z[0] * z[27];
z[45] = 3 * abb[11];
z[54] = -z[42] + -z[45];
z[54] = abb[13] * z[54];
z[58] = 5 * abb[12];
z[59] = 6 * z[20] + -z[58];
z[59] = abb[12] * z[59];
z[25] = 3 * z[25] + -z[46] + z[54] + z[59];
z[25] = abb[3] * z[25];
z[31] = -z[31] + z[36];
z[36] = -7 * abb[12] + 10 * z[20];
z[36] = abb[12] * z[36];
z[36] = 3 * z[31] + z[36];
z[36] = abb[1] * z[36];
z[25] = z[25] + z[36] + z[43];
z[25] = abb[24] * z[25];
z[27] = abb[3] * z[27];
z[36] = abb[12] + z[22];
z[36] = abb[12] * z[36];
z[31] = z[31] + z[36];
z[31] = abb[1] * z[31];
z[27] = z[27] + -z[28] + z[31];
z[28] = z[47] + -z[56];
z[31] = -z[28] + 2 * z[48];
z[36] = abb[0] * z[31];
z[43] = -z[26] + z[48];
z[46] = -z[6] * z[43];
z[36] = -z[27] + z[36] + z[46];
z[36] = abb[22] * z[36];
z[46] = z[32] * z[50];
z[24] = -z[24] + -z[46] + z[56];
z[24] = abb[2] * z[24];
z[28] = -z[26] + -z[28] + -z[46];
z[28] = abb[0] * z[28];
z[46] = -abb[4] * z[43];
z[24] = z[24] + z[28] + z[37] + z[46];
z[28] = 2 * abb[21];
z[24] = z[24] * z[28];
z[31] = abb[2] * z[31];
z[27] = -z[27] + z[31];
z[27] = abb[23] * z[27];
z[31] = abb[14] * z[22];
z[31] = z[31] + -z[34];
z[31] = abb[3] * z[31];
z[31] = z[31] + -z[39];
z[34] = 3 * abb[14];
z[37] = abb[13] + z[34] + -z[45];
z[39] = abb[14] * (T(1) / T(2));
z[37] = z[37] * z[39];
z[45] = abb[11] * (T(1) / T(2));
z[46] = abb[13] + -z[45];
z[46] = abb[13] * z[46];
z[37] = z[37] + z[46] + -z[48];
z[37] = abb[2] * z[37];
z[22] = -z[22] + z[33];
z[22] = abb[12] * z[22];
z[22] = z[22] + (T(-1) / T(2)) * z[56];
z[46] = (T(1) / T(2)) * z[47];
z[48] = z[22] + z[26] + z[46];
z[48] = abb[0] * z[48];
z[54] = abb[8] * (T(3) / T(2));
z[56] = abb[9] + z[54];
z[56] = abb[15] * z[56];
z[37] = z[31] + z[37] + z[48] + z[56];
z[37] = abb[20] * z[37];
z[48] = abb[11] + -3 * abb[13] + z[34];
z[39] = z[39] * z[48];
z[39] = z[39] + -z[43] + -z[46];
z[39] = abb[0] * z[39];
z[44] = -z[26] + -z[44] + z[52];
z[6] = z[6] * z[44];
z[44] = abb[13] + z[45];
z[44] = abb[13] * z[44];
z[22] = z[22] + z[44];
z[22] = abb[2] * z[22];
z[44] = abb[9] + abb[8] * (T(1) / T(2));
z[44] = abb[15] * z[44];
z[6] = z[6] + z[22] + z[31] + z[39] + z[44];
z[6] = abb[18] * z[6];
z[10] = 3 * abb[2] + -z[10];
z[10] = abb[21] * z[10];
z[4] = abb[2] + z[4];
z[4] = abb[23] * z[4];
z[19] = abb[22] * z[19];
z[4] = z[2] + z[4] + -z[9] + -z[10] + z[19];
z[9] = -9 * abb[2] + 2 * z[14] + -z[17];
z[9] = abb[19] * z[9];
z[8] = abb[2] + 2 * z[8] + -z[17];
z[8] = abb[20] * z[8];
z[10] = 2 * abb[2] + -z[16];
z[14] = 2 * abb[18];
z[10] = z[10] * z[14];
z[12] = 3 * abb[19] + abb[20] + z[12];
z[12] = abb[5] * z[12];
z[4] = -2 * z[4] + z[8] + -z[9] + -z[10] + -z[12];
z[8] = abb[28] * z[4];
z[9] = -abb[23] + -z[28];
z[9] = z[9] * z[40];
z[10] = 2 * abb[9] + z[54];
z[10] = abb[24] * z[10];
z[12] = -abb[9] * abb[22];
z[14] = -abb[18] + abb[20] + abb[22] + -abb[23];
z[14] = abb[7] * z[14];
z[9] = z[9] + z[10] + z[12] + z[14];
z[9] = abb[15] * z[9];
z[10] = -z[23] + z[26] + (T(3) / T(2)) * z[47];
z[10] = z[3] * z[10];
z[12] = z[2] * z[57];
z[14] = z[21] * z[43];
z[1] = z[1] + z[5] + z[6] + z[8] + z[9] + z[10] + z[12] + z[14] + z[15] + z[24] + z[25] + z[27] + 2 * z[30] + z[36] + z[37];
z[5] = abb[3] * z[51];
z[6] = z[38] * z[51];
z[8] = abb[6] * z[32];
z[6] = z[6] + -z[8];
z[9] = z[5] + z[6];
z[10] = 4 * abb[13];
z[12] = z[10] + -z[33] + z[41];
z[12] = abb[2] * z[12];
z[14] = 4 * abb[11];
z[15] = -abb[13] + abb[14];
z[16] = z[14] + z[15] + -z[33];
z[16] = abb[0] * z[16];
z[17] = abb[12] + z[15];
z[14] = -z[14] + -z[17];
z[14] = abb[4] * z[14];
z[9] = 4 * z[9] + z[12] + z[14] + z[16];
z[9] = abb[19] * z[9];
z[12] = -z[33] + -z[34] + z[35];
z[12] = abb[3] * z[12];
z[0] = z[0] * z[32];
z[14] = 3 * z[20] + -z[58];
z[14] = abb[1] * z[14];
z[16] = abb[4] * z[29];
z[0] = z[0] + z[12] + z[14] + z[16];
z[0] = abb[24] * z[0];
z[12] = abb[12] + -abb[13];
z[14] = abb[2] * z[12];
z[14] = -z[6] + z[14];
z[19] = -abb[11] + abb[12];
z[20] = abb[0] * z[19];
z[21] = -abb[4] * abb[11];
z[5] = z[5] + -z[14] + -z[20] + z[21];
z[5] = z[5] * z[28];
z[18] = -z[18] + z[50];
z[18] = abb[3] * z[18];
z[21] = -abb[13] + -z[41];
z[21] = abb[2] * z[21];
z[6] = -z[6] + -z[18] + z[20] + z[21];
z[6] = abb[20] * z[6];
z[17] = z[17] + z[29];
z[17] = abb[4] * z[17];
z[15] = -abb[11] + -z[15];
z[15] = abb[0] * z[15];
z[14] = z[14] + z[15] + z[17] + -z[18];
z[14] = abb[18] * z[14];
z[15] = abb[12] + z[41];
z[17] = z[15] + z[42];
z[17] = abb[20] * z[17];
z[10] = -z[10] + -z[15];
z[10] = abb[19] * z[10];
z[11] = -abb[13] * z[11];
z[10] = z[10] + 2 * z[11] + z[17];
z[10] = abb[5] * z[10];
z[2] = z[2] * z[12];
z[3] = z[3] * z[19];
z[11] = abb[1] + abb[3];
z[11] = z[11] * z[32];
z[8] = -z[8] + z[11];
z[11] = z[8] + -z[16];
z[11] = abb[22] * z[11];
z[8] = abb[23] * z[8];
z[0] = z[0] + z[2] + z[3] + z[5] + z[6] + z[8] + z[9] + z[10] + z[11] + z[14];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[16] * z[7];
z[3] = abb[17] * z[4];
z[4] = abb[8] * z[12];
z[5] = abb[9] * z[32];
z[6] = abb[7] * z[53];
z[4] = z[4] + z[5] + -z[6];
z[4] = m1_set::bc<T>[0] * z[4];
z[5] = abb[17] * z[49];
z[6] = abb[16] * z[55];
z[4] = z[4] + -z[5] + z[6];
z[5] = -2 * z[13];
z[4] = z[4] * z[5];
z[0] = 2 * z[0] + z[2] + z[3] + z[4];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_44_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("7.499370481393428529613643864909206776407606765853323308413651831"),stof<T>("30.925185260231911121207769558381019047413615432257003277117091769")}, std::complex<T>{stof<T>("42.216715266864030124334445014411318774298620193757042889861416277"),stof<T>("-56.431647100018905478157875709729794450535405067495488775056624084")}, std::complex<T>{stof<T>("3.128120631044279442516169200329558015994916039926816086680606328"),stof<T>("39.184038852243588931132430335434875260433171862298276964317354233")}, std::complex<T>{stof<T>("23.730277588438377338269960790123016598083898396478660062078145007"),stof<T>("-15.281833559914695864347906795049705272452430732263689371594978874")}, std::complex<T>{stof<T>("-26.880210816872341450909897200596016269818166498766283936203828411"),stof<T>("-4.65572360152186764833772650702743528282549532619367129579613118")}, std::complex<T>{stof<T>("-28.047142876499479648595544777175144846893072748278675484952211386"),stof<T>("-13.006971778289777983726806804056996473558405155221492303361916016")}, std::complex<T>{stof<T>("1.0415736570350415015205919490605387750050481237538885681001826802"),stof<T>("1.9925591836775255289411045634991659495362591271776860663901126389")}, std::complex<T>{stof<T>("-4.5968061348999354552581905392268209704183843451340513010916107703"),stof<T>("-6.9160590414200418020718042420585732451309314383448089228716819912")}, std::complex<T>{stof<T>("-4.5968061348999354552581905392268209704183843451340513010916107703"),stof<T>("-6.9160590414200418020718042420585732451309314383448089228716819912")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_44_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_44_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("42.701588759907823751466344002883850371580380878801311685890182373"),stof<T>("-40.466838422179643029303309779121212607863054988428644863630012494")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dl[5], dl[2], dl[4], dlog_W15(k,dl), dlog_W22(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_4_re(k), f_2_7_re(k)};

                    
            return f_4_44_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_44_DLogXconstant_part(base_point<T>, kend);
	value += f_4_44_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_44_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_44_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_44_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_44_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_44_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_44_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
