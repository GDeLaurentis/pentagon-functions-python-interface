/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_291.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_291_abbreviated (const std::array<T,31>& abb) {
T z[39];
z[0] = 2 * abb[22];
z[1] = -abb[23] + z[0];
z[2] = abb[21] * (T(1) / T(2));
z[3] = -abb[20] + abb[25] * (T(1) / T(2)) + z[2];
z[4] = abb[24] + abb[19] * (T(1) / T(2));
z[5] = z[1] + -z[3] + -z[4];
z[5] = abb[3] * z[5];
z[6] = abb[20] + abb[22];
z[7] = -abb[23] + z[4];
z[2] = abb[25] * (T(-3) / T(2)) + z[2] + z[6] + -z[7];
z[2] = abb[9] * z[2];
z[8] = 2 * abb[24];
z[9] = abb[19] + z[8];
z[0] = -z[0] + z[9];
z[10] = 2 * abb[23];
z[11] = -abb[21] + abb[25];
z[12] = z[0] + z[10] + -z[11];
z[13] = abb[5] * z[12];
z[14] = abb[19] + -abb[22];
z[8] = z[8] + z[14];
z[15] = -abb[20] + abb[25];
z[16] = -z[8] + -z[10] + 2 * z[15];
z[17] = abb[7] * z[16];
z[18] = 2 * z[11];
z[8] = z[8] + -z[10] + z[18];
z[8] = abb[4] * z[8];
z[19] = -abb[22] + z[4];
z[20] = -abb[23] + z[3] + -z[19];
z[20] = abb[0] * z[20];
z[21] = 2 * abb[20];
z[22] = -abb[21] + -abb[25] + z[21];
z[23] = z[9] + z[22];
z[23] = abb[2] * z[23];
z[24] = -abb[23] + z[11];
z[25] = abb[1] * z[24];
z[26] = 2 * z[25];
z[5] = 3 * z[2] + -z[5] + z[8] + z[13] + z[17] + z[20] + z[23] + z[26];
z[20] = abb[30] * z[5];
z[27] = abb[23] * (T(3) / T(2));
z[28] = z[19] + z[27];
z[29] = -z[11] + z[28];
z[29] = abb[5] * z[29];
z[30] = -abb[22] + z[11];
z[31] = abb[8] * z[30];
z[6] = -abb[25] + z[6];
z[32] = abb[6] * z[6];
z[29] = -z[29] + z[31] + z[32];
z[33] = -abb[23] + -z[14] + z[15];
z[33] = -abb[24] + (T(1) / T(2)) * z[33];
z[33] = abb[7] * z[33];
z[34] = -z[3] + z[4];
z[34] = abb[2] * z[34];
z[35] = -abb[22] + -abb[23] + abb[24];
z[21] = abb[21] + z[21];
z[36] = -abb[19] + z[21];
z[35] = -abb[25] + (T(-2) / T(3)) * z[35] + (T(1) / T(3)) * z[36];
z[35] = abb[9] * z[35];
z[36] = abb[23] * (T(5) / T(2));
z[37] = abb[20] + abb[25] * (T(-7) / T(2)) + abb[21] * (T(5) / T(2));
z[37] = (T(1) / T(2)) * z[37];
z[38] = -8 * abb[22] + -z[37];
z[38] = abb[24] * (T(-5) / T(6)) + abb[19] * (T(-5) / T(12)) + z[36] + (T(1) / T(3)) * z[38];
z[38] = abb[3] * z[38];
z[37] = 4 * abb[22] + -z[10] + -z[37];
z[37] = abb[24] * (T(-5) / T(2)) + abb[19] * (T(-5) / T(4)) + (T(1) / T(3)) * z[37];
z[37] = abb[0] * z[37];
z[29] = (T(4) / T(3)) * z[8] + (T(13) / T(4)) * z[25] + (T(-5) / T(2)) * z[29] + 5 * z[33] + (T(7) / T(2)) * z[34] + 4 * z[35] + z[37] + z[38];
z[29] = prod_pow(m1_set::bc<T>[0], 2) * z[29];
z[21] = 3 * abb[25] + z[0] + -z[10] + -z[21];
z[21] = abb[9] * z[21];
z[8] = -z[8] + z[21];
z[26] = z[8] + -z[26];
z[18] = -z[18] + z[19] + z[36];
z[18] = abb[5] * z[18];
z[12] = abb[3] * z[12];
z[19] = z[22] + z[28];
z[19] = abb[0] * z[19];
z[12] = z[12] + z[17] + z[18] + -z[19] + -z[26];
z[17] = abb[26] * z[12];
z[18] = z[8] + -z[23];
z[16] = abb[0] * z[16];
z[19] = -2 * abb[19] + -4 * abb[24] + z[1] + z[15];
z[19] = abb[7] * z[19];
z[1] = z[1] + -z[15];
z[1] = abb[3] * z[1];
z[1] = -z[1] + z[13] + z[16] + -z[18] + z[19];
z[13] = abb[28] * z[1];
z[16] = abb[23] * (T(1) / T(2));
z[19] = -z[4] + z[16] + -z[30];
z[19] = abb[5] * z[19];
z[28] = abb[22] * (T(1) / T(2));
z[4] = -z[4] + -z[24] + z[28];
z[4] = abb[4] * z[4];
z[27] = -z[15] + z[27] + -z[28];
z[27] = abb[0] * z[27];
z[28] = abb[22] + -abb[23];
z[28] = abb[3] * z[28];
z[27] = z[4] + -z[19] + z[27] + z[28] + -z[32];
z[27] = prod_pow(abb[10], 2) * z[27];
z[28] = -abb[23] + z[9];
z[33] = -z[6] + z[28];
z[33] = abb[7] * z[33];
z[34] = abb[3] * z[6];
z[14] = abb[24] + (T(1) / T(2)) * z[14];
z[14] = abb[0] * z[14];
z[4] = z[4] + z[14] + z[31] + z[33] + z[34];
z[4] = abb[12] * z[4];
z[0] = -z[0] + z[22];
z[0] = abb[3] * z[0];
z[14] = -abb[22] + z[10];
z[34] = -z[14] + -z[22];
z[34] = abb[0] * z[34];
z[8] = -z[0] + -z[8] + z[34];
z[8] = abb[10] * z[8];
z[4] = z[4] + z[8];
z[4] = abb[12] * z[4];
z[8] = abb[0] * z[30];
z[34] = 2 * z[31];
z[35] = 2 * z[32];
z[0] = z[0] + z[8] + z[26] + z[34] + z[35];
z[8] = -abb[29] * z[0];
z[26] = -z[3] + -z[7];
z[26] = abb[3] * z[26];
z[19] = -z[2] + z[19] + z[25] + z[26];
z[25] = prod_pow(abb[13], 2);
z[19] = z[19] * z[25];
z[26] = abb[19] + -z[11];
z[26] = abb[24] + (T(1) / T(2)) * z[26];
z[26] = abb[3] * z[26];
z[2] = -z[2] + -z[23] + z[26] + -z[33];
z[23] = prod_pow(abb[11], 2);
z[2] = z[2] * z[23];
z[14] = z[14] + -z[15];
z[14] = abb[0] * z[14];
z[15] = 2 * z[6];
z[26] = abb[3] * z[15];
z[14] = z[14] + z[18] + z[26] + z[32];
z[18] = -abb[27] * z[14];
z[3] = -z[3] + z[16];
z[3] = z[3] * z[25];
z[7] = -z[7] + (T(-1) / T(2)) * z[11];
z[7] = z[7] * z[23];
z[3] = z[3] + z[7];
z[3] = abb[0] * z[3];
z[6] = z[6] * z[23];
z[7] = -abb[30] * z[15];
z[6] = z[6] + z[7];
z[6] = abb[6] * z[6];
z[7] = -abb[27] + -abb[30] + -z[25];
z[7] = z[7] * z[31];
z[2] = z[2] + z[3] + z[4] + z[6] + z[7] + z[8] + z[13] + z[17] + z[18] + z[19] + z[20] + z[27] + z[29];
z[3] = z[5] + -z[31] + -z[35];
z[3] = abb[18] * z[3];
z[4] = abb[14] * z[12];
z[1] = abb[16] * z[1];
z[5] = z[9] + -z[10] + -z[22];
z[6] = -abb[3] * z[5];
z[7] = -z[28] + -2 * z[30];
z[7] = abb[5] * z[7];
z[8] = abb[0] * z[24];
z[6] = z[6] + z[7] + z[8] + z[21] + z[35];
z[6] = abb[10] * z[6];
z[5] = -abb[0] * z[5];
z[7] = -z[9] + z[11];
z[7] = abb[3] * z[7];
z[5] = z[5] + z[7] + z[21] + -2 * z[33] + -z[34];
z[5] = abb[12] * z[5];
z[5] = z[5] + z[6];
z[5] = m1_set::bc<T>[0] * z[5];
z[0] = -abb[17] * z[0];
z[6] = -z[14] + -z[31];
z[6] = abb[15] * z[6];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_291_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-2.803135703716962412489849754716839063995836503017974977015828895"),stof<T>("25.011831899435466457799590123704629285262241814443182881711012903")}, std::complex<T>{stof<T>("-32.318245777209719371883693797952253647752971249484800311062996166"),stof<T>("-5.724517080325883961316843887146793206757856374984065277392731427")}, std::complex<T>{stof<T>("-1.4285480644788474698631322466362858945674834219365712300813161187"),stof<T>("5.073599168998984956694123062407283758768589175952851156371630066")}, std::complex<T>{stof<T>("-9.2659612808264862622894029064565854058437101921504607056427546701"),stof<T>("-0.184515602961859341353340983873565076462023095554941325534180607")}, std::complex<T>{stof<T>("-21.67769685714511816696757338341511507248090797625293585848572872"),stof<T>("-25.478234207800506121068969964570573656789485917919455677197933657")}, std::complex<T>{stof<T>("-5.606271407433924824979699509433678127991673006035949954031657791"),stof<T>("50.023663798870932915599180247409258570524483628886365763422025806")}, std::complex<T>{stof<T>("33.746793841688566841746826044588539542320454671421371541144312285"),stof<T>("0.650917911326899004622720824739509447989267199031214121021101361")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[32].real()/kbase.W[32].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_291_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_291_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("20.5305705716977113028088202882915616670524433776354996556183145"),stof<T>("53.288419071553157385558895855863440883916999286918416843766612291")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,31> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), f_1_1(k), f_1_4(k), f_1_5(k), f_1_11(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[32].real()/k.W[32].real()), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_4_291_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_291_DLogXconstant_part(base_point<T>, kend);
	value += f_4_291_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_291_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_291_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_291_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_291_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_291_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_291_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
