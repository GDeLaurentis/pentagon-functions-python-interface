/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_821.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_821_abbreviated (const std::array<T,87>& abb) {
T z[151];
z[0] = 3 * abb[74];
z[1] = 3 * abb[72];
z[2] = 3 * abb[70];
z[3] = abb[71] + abb[73];
z[4] = 2 * z[3];
z[5] = z[0] + -z[1] + -z[2] + -z[4];
z[5] = abb[6] * z[5];
z[6] = abb[70] + abb[72];
z[7] = -abb[74] + z[6];
z[8] = z[4] + z[7];
z[8] = abb[17] * z[8];
z[9] = 2 * abb[61];
z[10] = -abb[63] + z[9];
z[11] = -abb[62] + abb[64] + -abb[65];
z[12] = abb[67] + z[11];
z[13] = z[10] + -z[12];
z[14] = abb[31] * z[13];
z[15] = abb[30] * z[13];
z[16] = abb[21] * abb[76];
z[17] = abb[25] * abb[76];
z[18] = z[16] + z[17];
z[19] = abb[29] * z[13];
z[20] = abb[70] + abb[73];
z[21] = -abb[74] + z[20];
z[22] = abb[71] + abb[72];
z[23] = z[21] + z[22];
z[23] = abb[14] * z[23];
z[24] = abb[5] * z[7];
z[25] = -abb[72] + z[3];
z[26] = abb[8] * z[25];
z[8] = -z[5] + z[8] + z[14] + -z[15] + z[18] + -z[19] + -6 * z[23] + z[24] + z[26];
z[14] = abb[27] * z[13];
z[24] = abb[28] * z[13];
z[26] = abb[4] * z[25];
z[24] = z[24] + z[26];
z[27] = abb[20] * abb[76];
z[28] = z[24] + -z[27];
z[14] = -z[8] + z[14] + z[28];
z[29] = 2 * abb[41];
z[30] = -z[14] * z[29];
z[31] = 2 * abb[75];
z[32] = abb[71] + abb[74];
z[33] = z[20] + z[31] + z[32];
z[33] = abb[0] * z[33];
z[34] = -z[16] + z[33];
z[35] = -abb[75] + z[6];
z[36] = 2 * abb[74];
z[37] = z[35] + -z[36];
z[38] = abb[6] * z[37];
z[39] = -abb[61] + z[11];
z[40] = abb[69] + z[39];
z[41] = abb[28] * z[40];
z[42] = abb[70] + abb[75];
z[43] = abb[72] + z[42];
z[44] = abb[15] * z[43];
z[45] = z[41] + z[44];
z[46] = abb[30] * z[40];
z[47] = z[45] + z[46];
z[48] = z[38] + z[47];
z[49] = z[3] + z[42];
z[50] = 2 * abb[72];
z[51] = z[49] + z[50];
z[52] = abb[7] * z[51];
z[53] = abb[11] * z[42];
z[54] = 2 * z[53];
z[52] = z[52] + -z[54];
z[55] = 2 * z[23];
z[56] = abb[72] + z[3];
z[57] = abb[8] * z[56];
z[57] = -z[52] + z[55] + z[57];
z[58] = abb[74] + abb[75];
z[59] = z[1] + z[3] + -z[58];
z[59] = abb[5] * z[59];
z[19] = 2 * z[19];
z[60] = 2 * abb[67];
z[61] = -abb[69] + z[60];
z[62] = z[11] + z[61];
z[63] = 2 * abb[63];
z[64] = 3 * abb[61] + -z[62] + -z[63];
z[65] = abb[27] + -abb[31];
z[66] = z[64] * z[65];
z[67] = -z[4] + -z[37];
z[67] = abb[17] * z[67];
z[68] = 2 * z[17];
z[57] = z[19] + -z[34] + -z[48] + 2 * z[57] + z[59] + z[66] + z[67] + -z[68];
z[57] = abb[40] * z[57];
z[59] = abb[61] + -abb[63];
z[66] = abb[67] + -abb[69];
z[67] = z[59] + -z[66];
z[69] = abb[31] * z[67];
z[70] = abb[17] * z[58];
z[69] = z[17] + z[33] + z[69] + -z[70];
z[70] = abb[71] + z[20];
z[71] = abb[74] + -abb[75];
z[70] = z[50] + 2 * z[70] + -z[71];
z[70] = abb[6] * z[70];
z[72] = abb[32] * z[67];
z[73] = abb[18] * z[49];
z[72] = z[72] + -z[73];
z[73] = abb[28] * z[67];
z[73] = -z[72] + z[73];
z[74] = abb[30] * z[67];
z[75] = abb[8] * z[49];
z[70] = -z[55] + z[69] + z[70] + -z[73] + -z[74] + z[75];
z[74] = abb[27] * z[67];
z[76] = abb[29] * z[67];
z[74] = z[74] + z[76];
z[77] = z[27] + z[70] + -z[74];
z[78] = -abb[35] * z[77];
z[79] = z[4] + z[43];
z[80] = abb[8] * z[79];
z[81] = abb[7] * z[79];
z[54] = z[54] + -z[81];
z[81] = -abb[72] + z[42];
z[82] = abb[5] * z[81];
z[82] = -z[54] + z[82];
z[83] = abb[29] * z[40];
z[84] = abb[6] * z[43];
z[85] = z[83] + -z[84];
z[47] = -z[47] + z[80] + -z[82] + -z[85];
z[80] = -abb[37] * z[47];
z[86] = abb[3] * z[49];
z[87] = z[72] + z[86];
z[88] = abb[5] * z[58];
z[69] = -z[16] + z[69] + z[87] + z[88];
z[69] = abb[42] * z[69];
z[78] = z[69] + z[78] + z[80];
z[80] = -z[41] + z[84];
z[4] = abb[8] * z[4];
z[84] = abb[5] * z[56];
z[4] = z[4] + z[26] + -z[46] + z[54] + z[72] + z[80] + z[84];
z[54] = 2 * abb[39];
z[4] = z[4] * z[54];
z[84] = abb[8] * z[43];
z[83] = -z[55] + z[83] + -z[84];
z[84] = abb[17] * z[79];
z[34] = z[34] + -z[84];
z[84] = 2 * abb[70];
z[71] = -z[56] + z[71] + -z[84];
z[88] = abb[5] * z[71];
z[65] = -z[40] * z[65];
z[48] = z[34] + -z[48] + z[65] + -2 * z[83] + z[88];
z[48] = abb[36] * z[48];
z[65] = z[3] + -z[42];
z[65] = abb[8] * z[65];
z[83] = abb[5] * z[51];
z[65] = -z[52] + z[65] + z[74] + z[83] + z[87];
z[74] = 2 * z[65];
z[83] = -abb[38] * z[74];
z[4] = z[4] + z[30] + z[48] + z[57] + 2 * z[78] + z[83];
z[4] = m1_set::bc<T>[0] * z[4];
z[8] = z[8] + -z[24];
z[8] = abb[53] * z[8];
z[24] = -z[70] + z[76];
z[24] = abb[51] * z[24];
z[30] = -abb[51] + abb[53];
z[30] = z[27] * z[30];
z[8] = z[8] + z[24] + z[30];
z[24] = abb[21] * z[71];
z[30] = abb[20] * z[37];
z[48] = abb[25] * z[79];
z[57] = -abb[0] + abb[5];
z[57] = abb[76] * z[57];
z[70] = abb[24] * z[43];
z[71] = abb[34] * z[40];
z[24] = -z[24] + z[30] + z[48] + z[57] + z[70] + -z[71];
z[30] = abb[55] * z[24];
z[48] = -abb[54] * z[74];
z[47] = 2 * z[47];
z[57] = -abb[52] * z[47];
z[70] = -abb[53] * z[13];
z[71] = abb[51] * z[67];
z[70] = z[70] + z[71];
z[71] = 2 * abb[27];
z[70] = z[70] * z[71];
z[76] = -abb[58] + -abb[59];
z[78] = -abb[73] + z[42];
z[76] = z[76] * z[78];
z[79] = 2 * abb[66] + -abb[68];
z[83] = z[66] + z[79];
z[88] = abb[86] * z[83];
z[89] = abb[73] + z[42];
z[90] = z[50] + z[89];
z[91] = 2 * abb[71];
z[92] = z[90] + z[91];
z[93] = abb[57] * z[92];
z[94] = abb[56] * z[89];
z[4] = z[4] + 2 * z[8] + z[30] + z[48] + z[57] + z[70] + z[76] + z[88] + z[93] + z[94];
z[8] = abb[30] * z[83];
z[30] = abb[27] * z[83];
z[48] = z[8] + z[30];
z[0] = z[0] + -z[50];
z[57] = 2 * abb[73];
z[70] = z[57] + z[84];
z[76] = abb[71] + z[0] + -z[70];
z[88] = 2 * abb[10];
z[93] = z[76] * z[88];
z[94] = 4 * abb[66] + -2 * abb[68];
z[61] = z[39] + z[61] + z[94];
z[95] = abb[31] * z[61];
z[37] = z[37] + z[57];
z[96] = abb[17] * z[37];
z[95] = -z[68] + z[95] + -z[96];
z[96] = abb[72] + z[20];
z[97] = -abb[74] + z[96];
z[98] = abb[2] * z[97];
z[99] = 8 * z[98];
z[100] = z[95] + z[99];
z[101] = -abb[29] * z[61];
z[102] = abb[71] + -abb[73];
z[102] = -z[81] + 2 * z[102];
z[102] = abb[8] * z[102];
z[103] = z[43] + z[57];
z[103] = abb[3] * z[103];
z[103] = -z[44] + z[103];
z[104] = z[58] + z[91];
z[105] = abb[5] * z[104];
z[106] = -abb[71] + z[20];
z[0] = -abb[75] + -z[0] + 2 * z[106];
z[0] = abb[6] * z[0];
z[106] = 3 * abb[71];
z[107] = -z[90] + -z[106];
z[107] = abb[16] * z[107];
z[108] = abb[9] * z[20];
z[109] = 2 * z[108];
z[0] = z[0] + -z[16] + -z[27] + -2 * z[48] + z[73] + z[93] + z[100] + z[101] + z[102] + z[103] + z[105] + z[107] + -z[109];
z[0] = abb[35] * z[0];
z[48] = 2 * abb[0];
z[73] = z[48] * z[89];
z[35] = abb[16] * z[35];
z[35] = z[35] + z[73] + -z[109];
z[6] = abb[71] + z[6];
z[6] = abb[13] * z[6];
z[101] = 2 * z[6];
z[105] = z[35] + -z[101];
z[107] = -z[43] + -z[91];
z[107] = abb[8] * z[107];
z[85] = z[45] + z[85] + -z[105] + z[107];
z[85] = abb[37] * z[85];
z[69] = z[69] + z[85];
z[0] = z[0] + 2 * z[69];
z[0] = abb[35] * z[0];
z[69] = 2 * abb[69];
z[85] = abb[67] + z[79];
z[107] = z[39] + z[69] + -z[85];
z[109] = abb[28] + abb[30];
z[107] = z[107] * z[109];
z[110] = z[22] + z[42];
z[110] = abb[10] * z[110];
z[111] = abb[7] * z[22];
z[111] = -z[53] + z[110] + z[111];
z[55] = -z[55] + z[111];
z[112] = abb[72] + abb[73];
z[113] = -z[31] + -z[36] + z[112];
z[113] = abb[6] * z[113];
z[114] = abb[69] + 2 * z[11];
z[115] = -z[9] + z[85] + z[114];
z[116] = abb[29] * z[115];
z[117] = -abb[71] + z[21];
z[117] = abb[0] * z[117];
z[118] = abb[1] * z[22];
z[119] = 2 * z[118];
z[120] = z[117] + z[119];
z[121] = -abb[8] * z[90];
z[122] = abb[71] + z[31];
z[123] = -abb[72] + abb[74];
z[124] = abb[70] + z[122] + z[123];
z[124] = abb[5] * z[124];
z[39] = z[39] + z[85];
z[85] = abb[27] * z[39];
z[125] = abb[3] * z[89];
z[55] = -z[16] + z[44] + 2 * z[55] + z[85] + -z[95] + z[107] + z[113] + z[116] + -z[120] + z[121] + z[124] + z[125];
z[55] = abb[36] * z[55];
z[85] = abb[28] + abb[29];
z[95] = -z[85] * z[115];
z[107] = 2 * z[111];
z[113] = abb[72] + -abb[73];
z[116] = abb[16] * z[113];
z[121] = 2 * z[116];
z[124] = z[73] + z[121];
z[126] = -z[89] + z[91];
z[127] = z[50] + z[126];
z[127] = abb[5] * z[127];
z[128] = 4 * abb[71] + z[90];
z[129] = abb[8] * z[128];
z[130] = abb[6] * z[90];
z[95] = z[8] + -z[30] + -2 * z[44] + z[95] + -z[107] + -z[119] + z[124] + z[125] + z[127] + z[129] + z[130];
z[95] = abb[37] * z[95];
z[129] = abb[29] * z[83];
z[30] = z[30] + z[129];
z[130] = abb[8] * z[78];
z[130] = z[30] + z[130];
z[37] = -abb[71] + z[37];
z[37] = abb[10] * z[37];
z[131] = abb[73] + z[43];
z[131] = abb[7] * z[131];
z[37] = z[37] + -z[53] + z[131];
z[9] = -z[9] + z[63];
z[132] = -6 * abb[66] + 3 * abb[68] + z[9] + -z[66];
z[132] = -z[109] * z[132];
z[128] = abb[5] * z[128];
z[133] = z[59] + z[79];
z[134] = abb[31] * z[133];
z[135] = abb[17] * z[91];
z[135] = 2 * z[134] + z[135];
z[136] = abb[75] + z[32];
z[137] = z[48] * z[136];
z[138] = 3 * abb[73];
z[139] = -abb[70] + abb[75] + z[36] + -z[138];
z[139] = abb[6] * z[139];
z[126] = abb[3] * z[126];
z[140] = 2 * z[27];
z[37] = 2 * z[37] + -z[119] + z[126] + -z[128] + z[130] + z[132] + z[135] + z[137] + z[139] + z[140];
z[37] = abb[42] * z[37];
z[126] = z[2] + z[138];
z[132] = z[31] + -z[32] + z[126];
z[132] = abb[0] * z[132];
z[100] = -z[100] + z[121] + z[132];
z[121] = abb[10] * z[97];
z[132] = z[23] + z[121];
z[137] = abb[8] * abb[71];
z[132] = 2 * z[132] + -z[137];
z[86] = z[86] + z[132];
z[63] = abb[61] + abb[69] + z[11] + -z[63] + z[94];
z[139] = abb[27] + abb[30];
z[63] = -z[63] * z[139];
z[141] = -abb[73] + z[22];
z[142] = -z[58] + -z[141];
z[142] = abb[5] * z[142];
z[64] = -abb[28] * z[64];
z[143] = -abb[74] + z[3];
z[144] = 5 * abb[72] + abb[75];
z[143] = 5 * abb[70] + 4 * z[143] + z[144];
z[143] = abb[6] * z[143];
z[145] = 2 * z[133];
z[146] = -abb[29] * z[145];
z[63] = z[16] + z[44] + z[63] + z[64] + -2 * z[86] + -z[100] + z[140] + z[142] + z[143] + z[146];
z[63] = abb[35] * z[63];
z[64] = abb[17] * abb[71];
z[64] = z[64] + z[134];
z[86] = abb[28] * z[39];
z[134] = abb[3] * abb[71];
z[140] = abb[5] * z[91];
z[142] = -abb[8] * z[91];
z[143] = -abb[6] * z[112];
z[86] = -z[26] + z[64] + z[86] + -z[116] + z[119] + z[134] + -z[140] + z[142] + z[143];
z[54] = z[54] * z[86];
z[9] = z[9] + z[66] + -z[79];
z[86] = abb[27] + abb[29];
z[9] = -z[9] * z[86];
z[142] = z[73] + z[135];
z[143] = abb[28] * z[83];
z[146] = abb[6] * z[78];
z[143] = z[119] + z[143] + z[146];
z[125] = z[125] + -z[143];
z[53] = z[53] + z[110];
z[131] = z[53] + -z[131];
z[146] = -z[78] + z[91];
z[146] = abb[8] * z[146];
z[147] = abb[5] * z[92];
z[9] = -z[8] + z[9] + z[125] + 2 * z[131] + -z[142] + z[146] + z[147];
z[9] = abb[38] * z[9];
z[116] = 4 * z[98] + -z[116] + -z[117];
z[131] = z[86] * z[133];
z[146] = abb[30] * z[133];
z[131] = z[131] + z[146];
z[147] = abb[5] * abb[71];
z[64] = z[64] + -z[131] + -z[147];
z[148] = z[88] * z[97];
z[5] = z[5] + 4 * z[23] + z[28] + -z[64] + -z[116] + -z[137] + z[148];
z[5] = z[5] * z[29];
z[133] = abb[28] * z[133];
z[131] = z[131] + z[133] + z[134] + -z[147];
z[21] = abb[6] * z[21];
z[21] = z[21] + -z[27] + z[120] + -z[131] + -z[137] + -z[148];
z[21] = abb[40] * z[21];
z[5] = z[5] + z[9] + z[21] + z[37] + z[54] + z[55] + z[63] + z[95];
z[5] = abb[40] * z[5];
z[9] = abb[71] + z[50];
z[21] = -abb[75] + z[9] + z[126];
z[21] = abb[16] * z[21];
z[37] = z[21] + z[143] + -z[146];
z[10] = -z[10] + z[62] + z[79];
z[10] = z[10] * z[86];
z[54] = -z[22] + z[42] + z[57];
z[54] = abb[8] * z[54];
z[55] = z[57] + z[81];
z[62] = abb[7] * z[55];
z[63] = abb[12] * z[96];
z[95] = -abb[5] * z[106];
z[96] = -abb[71] + -z[81];
z[96] = abb[3] * z[96];
z[10] = z[10] + z[26] + z[37] + z[54] + -z[62] + -6 * z[63] + z[95] + z[96] + -z[101] + 2 * z[110];
z[10] = abb[39] * z[10];
z[54] = 4 * z[108];
z[21] = z[21] + -z[54] + z[142];
z[95] = 4 * z[63] + z[72];
z[96] = z[50] + z[78] + z[106];
z[96] = abb[8] * z[96];
z[110] = z[20] + z[22];
z[120] = abb[10] * z[110];
z[126] = z[6] + z[120];
z[133] = -abb[63] + z[94];
z[134] = abb[61] + -abb[67] + -z[114] + -z[133];
z[146] = abb[30] + z[86];
z[134] = z[134] * z[146];
z[146] = 2 * abb[5];
z[147] = -z[113] * z[146];
z[149] = z[42] + z[138];
z[150] = abb[71] + -z[50] + -z[149];
z[150] = abb[3] * z[150];
z[80] = z[21] + 2 * z[80] + z[95] + z[96] + -4 * z[126] + z[134] + z[147] + z[150];
z[80] = abb[35] * z[80];
z[8] = -z[8] + z[130];
z[96] = z[8] + z[107];
z[107] = z[89] + z[91];
z[107] = abb[3] * z[107];
z[90] = abb[5] * z[90];
z[90] = -2 * z[72] + -z[90] + z[96] + -z[107] + -z[142] + -z[143];
z[90] = abb[42] * z[90];
z[107] = -z[101] + z[111];
z[107] = 2 * z[107];
z[20] = -abb[75] + z[20];
z[111] = 2 * abb[16];
z[130] = z[20] * z[111];
z[8] = z[8] + -z[54] + z[73] + z[107] + -z[125] + -z[127] + z[130];
z[8] = abb[37] * z[8];
z[8] = -z[8] + z[90];
z[54] = abb[71] + z[57];
z[73] = z[42] + z[54];
z[73] = abb[7] * z[73];
z[53] = -z[53] + z[73];
z[73] = abb[30] * z[115];
z[90] = z[78] + z[91];
z[90] = abb[5] * z[90];
z[115] = abb[8] * z[149];
z[30] = -z[30] + 2 * z[53] + z[73] + z[90] + -z[115] + z[124] + z[125];
z[53] = abb[36] * z[30];
z[10] = z[8] + z[10] + z[53] + z[80];
z[10] = abb[39] * z[10];
z[14] = abb[79] * z[14];
z[53] = abb[73] + abb[75];
z[73] = z[50] + z[53] + z[84] + z[91];
z[73] = z[73] * z[88];
z[80] = z[61] * z[85];
z[55] = abb[6] * z[55];
z[55] = z[55] + -z[62] + z[73] + z[80] + z[101] + -z[102] + 4 * z[118];
z[55] = abb[45] * z[55];
z[62] = abb[77] * z[77];
z[73] = 2 * z[63];
z[43] = abb[5] * z[43];
z[77] = abb[27] * z[40];
z[35] = z[35] + z[43] + z[46] + -z[73] + z[77] + z[103];
z[35] = abb[44] * z[35];
z[43] = -abb[73] + z[136];
z[43] = z[43] * z[88];
z[58] = -z[57] + z[58];
z[58] = abb[6] * z[58];
z[43] = z[27] + -z[43] + z[58];
z[58] = z[59] + z[66] + z[94];
z[66] = abb[28] + abb[31];
z[77] = -abb[30] + -z[66];
z[77] = z[58] * z[77];
z[80] = abb[3] + -abb[7];
z[85] = -abb[71] + z[89];
z[80] = z[80] * z[85];
z[90] = abb[5] + -abb[17];
z[90] = z[90] * z[104];
z[77] = -z[16] + z[17] + -z[43] + z[77] + z[80] + z[90];
z[77] = abb[47] * z[77];
z[9] = z[9] + z[70];
z[80] = z[9] + -z[36];
z[80] = abb[6] * z[80];
z[80] = z[80] + 2 * z[98] + -z[131] + -z[132];
z[80] = abb[48] * z[80];
z[90] = -abb[71] + z[112];
z[90] = abb[46] * z[90];
z[94] = -abb[49] * z[112];
z[81] = -abb[45] * z[81];
z[81] = z[81] + z[90] + z[94];
z[81] = abb[3] * z[81];
z[14] = z[14] + z[35] + z[55] + z[62] + z[77] + z[80] + z[81];
z[35] = z[73] + z[99];
z[55] = 8 * z[23];
z[1] = z[1] + z[84];
z[62] = abb[73] + z[1] + z[106];
z[62] = abb[16] * z[62];
z[62] = z[62] + z[117];
z[77] = abb[29] + abb[30];
z[80] = abb[27] + z[77];
z[81] = -z[80] * z[145];
z[90] = -abb[70] + z[123];
z[94] = abb[71] + z[138];
z[90] = -7 * z[90] + 2 * z[94];
z[90] = abb[6] * z[90];
z[28] = -z[28] + z[35] + -z[55] + -z[62] + z[81] + z[90] + z[93] + z[135] + 4 * z[137] + z[140];
z[28] = abb[41] * z[28];
z[81] = z[7] + z[57];
z[90] = abb[17] * z[81];
z[62] = z[62] + z[90];
z[76] = -abb[10] * z[76];
z[76] = z[23] + z[76];
z[12] = z[12] + z[133];
z[90] = -abb[31] + z[80];
z[93] = z[12] * z[90];
z[94] = -z[106] + -z[113];
z[94] = abb[8] * z[94];
z[98] = z[7] + -z[91];
z[98] = abb[5] * z[98];
z[97] = abb[6] * z[97];
z[35] = z[18] + -z[35] + z[62] + 2 * z[76] + z[93] + z[94] + -4 * z[97] + z[98];
z[35] = abb[35] * z[35];
z[76] = -z[39] * z[90];
z[23] = z[23] + -z[121];
z[54] = -z[7] + -z[54];
z[54] = abb[17] * z[54];
z[90] = abb[8] * z[113];
z[7] = -abb[71] + -z[7];
z[7] = abb[5] * z[7];
z[7] = z[7] + -z[18] + 2 * z[23] + z[54] + z[76] + z[90] + z[116];
z[7] = abb[36] * z[7];
z[18] = z[88] * z[110];
z[9] = abb[16] * z[9];
z[9] = z[9] + -z[18] + z[64] + -z[73] + -z[137];
z[18] = -abb[39] * z[9];
z[7] = z[7] + z[18] + z[35];
z[7] = 2 * z[7] + z[28];
z[7] = abb[41] * z[7];
z[18] = z[59] + -z[60] + z[69] + -z[79];
z[18] = z[18] * z[86];
z[23] = abb[71] + -z[31] + -z[84];
z[23] = abb[8] * z[23];
z[28] = z[70] + z[122];
z[28] = abb[3] * z[28];
z[18] = z[18] + z[23] + z[28] + -z[37] + z[95] + -z[107] + z[128];
z[18] = abb[39] * z[18];
z[23] = z[58] * z[80];
z[28] = 2 * z[89];
z[28] = -abb[5] * z[28];
z[21] = -z[21] + z[23] + z[28] + z[75] + -z[87] + 4 * z[120];
z[21] = abb[35] * z[21];
z[23] = -abb[38] * z[65];
z[9] = z[9] * z[29];
z[8] = -z[8] + z[9] + z[18] + z[21] + z[23];
z[8] = abb[38] * z[8];
z[9] = -z[58] * z[109];
z[18] = -abb[7] * z[85];
z[21] = -abb[3] * z[91];
z[9] = z[9] + z[18] + z[21] + -z[33] + -z[43] + -z[72] + -z[135] + z[140];
z[9] = prod_pow(abb[42], 2) * z[9];
z[18] = -abb[37] * z[30];
z[21] = abb[8] * z[89];
z[21] = z[21] + z[129] + z[148];
z[23] = z[61] * z[139];
z[22] = -abb[74] + z[22] + -z[53];
z[22] = abb[5] * z[22];
z[21] = z[16] + 2 * z[21] + z[22] + z[23] + -z[38] + -z[45] + z[100];
z[21] = abb[35] * z[21];
z[20] = z[20] + -z[36] + z[50];
z[20] = abb[5] * z[20];
z[22] = abb[31] * z[83];
z[16] = z[16] + z[22];
z[22] = 2 * abb[17] + -z[48];
z[22] = z[22] * z[136];
z[16] = 2 * z[16] + z[20] + z[22] + -z[68] + -z[96] + -z[125];
z[16] = abb[42] * z[16];
z[16] = z[16] + z[18] + z[21];
z[16] = abb[36] * z[16];
z[12] = z[12] * z[66];
z[18] = abb[10] * z[32];
z[6] = z[6] + z[18];
z[18] = abb[6] * z[81];
z[20] = abb[3] * z[56];
z[6] = 2 * z[6] + z[12] + -z[17] + z[18] + z[20] + z[27] + -z[62];
z[6] = 2 * z[6] + z[99];
z[6] = abb[43] * z[6];
z[12] = -abb[81] * z[24];
z[17] = -abb[5] + abb[6];
z[17] = z[17] * z[113];
z[18] = -abb[28] + -z[77];
z[18] = z[18] * z[39];
z[20] = z[91] + z[113];
z[20] = abb[8] * z[20];
z[17] = z[17] + z[18] + z[20] + z[73] + -z[119] + -2 * z[126];
z[18] = 2 * abb[49];
z[17] = z[17] * z[18];
z[20] = abb[80] * z[74];
z[21] = abb[31] * z[40];
z[21] = -z[21] + -z[34] + z[38] + z[41] + z[44] + z[46];
z[22] = -z[52] + z[55] + z[87];
z[23] = (T(1) / T(3)) * z[3];
z[24] = abb[72] * (T(4) / T(3)) + z[23] + z[42];
z[24] = abb[8] * z[24];
z[27] = abb[67] * (T(1) / T(3));
z[11] = -abb[63] + -4 * z[11];
z[11] = -abb[69] + abb[61] * (T(5) / T(3)) + (T(1) / T(3)) * z[11] + -z[27];
z[11] = abb[29] * z[11];
z[28] = -abb[63] + -z[114];
z[27] = abb[61] + -z[27] + (T(1) / T(3)) * z[28];
z[27] = abb[27] * z[27];
z[23] = -abb[70] + abb[75] * (T(-1) / T(3)) + abb[74] * (T(2) / T(3)) + -z[23];
z[23] = abb[5] * z[23];
z[11] = z[11] + (T(-2) / T(3)) * z[21] + (T(1) / T(3)) * z[22] + z[23] + z[24] + z[27];
z[11] = prod_pow(m1_set::bc<T>[0], 2) * z[11];
z[21] = abb[78] * z[47];
z[22] = -abb[22] + -abb[26];
z[22] = z[22] * z[49];
z[23] = abb[33] * z[67];
z[24] = -abb[23] * z[51];
z[27] = abb[19] * z[85];
z[22] = z[22] + z[23] + z[24] + z[27];
z[22] = abb[50] * z[22];
z[23] = abb[8] * z[141];
z[15] = z[15] + z[23] + -z[26];
z[23] = -z[25] * z[146];
z[15] = -2 * z[15] + -z[19] + z[23] + -12 * z[63];
z[15] = abb[46] * z[15];
z[19] = -abb[73] + -z[91];
z[2] = -z[2] + 2 * z[19] + -z[144];
z[2] = abb[45] * z[2];
z[1] = z[1] + z[3];
z[1] = abb[46] * z[1];
z[3] = z[18] * z[110];
z[1] = z[1] + z[2] + z[3];
z[1] = z[1] * z[111];
z[2] = abb[45] * z[61];
z[3] = -abb[46] * z[13];
z[13] = -abb[49] * z[39];
z[2] = z[2] + z[3] + z[13];
z[2] = z[2] * z[71];
z[3] = -abb[8] * z[57];
z[3] = z[3] + z[46] + z[82] + z[105];
z[3] = prod_pow(abb[37], 2) * z[3];
z[13] = abb[84] + abb[85];
z[13] = z[13] * z[78];
z[18] = -abb[83] * z[92];
z[19] = abb[46] + -abb[49];
z[19] = z[19] * z[108];
z[23] = abb[60] * z[83];
z[24] = -abb[82] * z[89];
z[0] = z[0] + z[1] + z[2] + z[3] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + 2 * z[14] + z[15] + z[16] + z[17] + z[18] + 4 * z[19] + z[20] + z[21] + z[22] + z[23] + z[24];
return {z[4], z[0]};
}


template <typename T> std::complex<T> f_4_821_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.08553765046350073568378678535865044279977537447147275761918782368"),stof<T>("0.6675000416113379518678092547268015533829146060396698635469383943")}, std::complex<T>{stof<T>("2.5150380543193075983841982604391800913427938461478470722106265256"),stof<T>("-2.524847202924887863973136789500371089537089249336092385131738684")}, std::complex<T>{stof<T>("2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("-3.1923472445362258158409460442271726429200038553757622486786770783")}, std::complex<T>{stof<T>("-2.5150380543193075983841982604391800913427938461478470722106265256"),stof<T>("2.524847202924887863973136789500371089537089249336092385131738684")}, std::complex<T>{stof<T>("2.5150380543193075983841982604391800913427938461478470722106265256"),stof<T>("-2.524847202924887863973136789500371089537089249336092385131738684")}, std::complex<T>{T(0),stof<T>("-4.8838051494255641203781278846932830704884750971992815173794895611")}, std::complex<T>{stof<T>("2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("-5.6342498192490078760300099865738141781642414039754030073684218589")}, std::complex<T>{T(0),stof<T>("2.4419025747127820601890639423466415352442375485996407586897447806")}, std::complex<T>{stof<T>("-5.1156137591021159324521833062370106254853630667671669020404408748"),stof<T>("8.1590970221738957400031467760741852677013306533114953925001605429")}, std::complex<T>{stof<T>("-9.0610824991124569698802977505881405211653624530046756647845249626"),stof<T>("-1.9952113053754119600323600750584017912894559016311967253205018102")}, std::complex<T>{stof<T>("-14.315054530607056963448513697961240777061039767368709279071596729"),stof<T>("-7.285374355940702859377663138485538501127179517628973392756640704")}, std::complex<T>{stof<T>("-5.0753839611376320477969755689041253126844733102158994801902001319"),stof<T>("-0.7402534937232493017429716487969404013382630037060589511236579171")}, std::complex<T>{stof<T>("-15.007432774639599258325582342812921313080181506896294265234808902"),stof<T>("-7.285374355940702859377663138485538501127179517628973392756640704")}, std::complex<T>{stof<T>("-12.323207908246221773813562341188589202437833425954166868578306547"),stof<T>("-7.894519493599358122145664592402644890470679602745194895173298888")}, std::complex<T>{stof<T>("-13.842052801351284171667853128630002617836076455602366060010560602"),stof<T>("-9.889730798974770082178024667461046681760135504376391620493800699")}, std::complex<T>{stof<T>("-6.7688445250302535862652480115932426555867764535582311162698881432"),stof<T>("-2.6995108351590209188393898844171947967143503037264621279860156359")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_821_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_821_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-6.1351262803324101184864052140295070491024747970508530365943254878"),stof<T>("3.4838972377980918980257994188126625204329653155375700110102999706")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({199});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,87> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W15(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W23(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_14(k), f_2_20(k), f_2_21(k), f_2_23(k), f_2_2_im(k), f_2_6_im(k), f_2_10_im(k), f_2_12_im(k), f_2_24_im(k), T{0}, T{0}, T{0}, T{0}, T{0}, rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_2_re(k), f_2_6_re(k), f_2_10_re(k), f_2_12_re(k), f_2_24_re(k), T{0}, T{0}, T{0}, T{0}, T{0}};
{
auto c = dlog_W163(k,dv) * f_2_31(k);
abb[82] = c.real();
abb[56] = c.imag();
SpDLog_Sigma5<T,199,162>(k, dv, abb[82], abb[56], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W166(k,dv) * f_2_31(k);
abb[83] = c.real();
abb[57] = c.imag();
SpDLog_Sigma5<T,199,165>(k, dv, abb[83], abb[57], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W177(k,dv) * f_2_31(k);
abb[84] = c.real();
abb[58] = c.imag();
SpDLog_Sigma5<T,199,176>(k, dv, abb[84], abb[58], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W179(k,dv) * f_2_31(k);
abb[85] = c.real();
abb[59] = c.imag();
SpDLog_Sigma5<T,199,178>(k, dv, abb[85], abb[59], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W190(k,dv) * f_2_31(k);
abb[86] = c.real();
abb[60] = c.imag();
SpDLog_Sigma5<T,199,189>(k, dv, abb[86], abb[60], f_2_31_series_coefficients<T>);
}

                    
            return f_4_821_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_821_DLogXconstant_part(base_point<T>, kend);
	value += f_4_821_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_821_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_821_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_821_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_821_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_821_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_821_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
