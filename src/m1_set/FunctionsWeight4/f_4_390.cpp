/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_390.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_390_abbreviated (const std::array<T,35>& abb) {
T z[45];
z[0] = abb[30] + abb[31];
z[1] = abb[10] * z[0];
z[2] = abb[9] * z[0];
z[3] = z[1] + z[2];
z[4] = 2 * abb[29];
z[5] = abb[26] + abb[27];
z[6] = z[4] + -z[5];
z[7] = -abb[7] * z[6];
z[8] = abb[24] * (T(1) / T(4));
z[9] = abb[26] * (T(1) / T(2));
z[10] = abb[28] * (T(-1) / T(3)) + -z[9];
z[10] = abb[25] * (T(-2) / T(3)) + abb[29] * (T(5) / T(12)) + z[8] + (T(1) / T(2)) * z[10];
z[10] = abb[4] * z[10];
z[11] = abb[25] + abb[26];
z[12] = abb[24] + -abb[27];
z[13] = abb[29] * (T(13) / T(2)) + 2 * z[11] + (T(17) / T(2)) * z[12];
z[13] = abb[0] * z[13];
z[14] = abb[26] * (T(2) / T(3));
z[15] = abb[29] * (T(1) / T(3));
z[16] = abb[25] + abb[27] * (T(-1) / T(3)) + z[14] + -z[15];
z[16] = abb[8] * z[16];
z[17] = 2 * abb[25];
z[15] = abb[26] * (T(-7) / T(6)) + abb[27] * (T(5) / T(6)) + z[15] + -z[17];
z[15] = abb[2] * z[15];
z[18] = abb[24] + abb[29];
z[19] = abb[25] + z[9];
z[18] = abb[27] + (T(5) / T(2)) * z[18] + -7 * z[19];
z[18] = abb[3] * z[18];
z[20] = -abb[24] + abb[29];
z[21] = -abb[28] + z[20];
z[21] = abb[1] * z[21];
z[14] = abb[29] * (T(-11) / T(6)) + abb[25] * (T(-1) / T(2)) + abb[27] * (T(7) / T(6)) + z[14];
z[14] = abb[6] * z[14];
z[10] = (T(1) / T(4)) * z[3] + -z[7] + z[10] + (T(1) / T(3)) * z[13] + z[14] + z[15] + z[16] + (T(1) / T(6)) * z[18] + (T(-13) / T(6)) * z[21];
z[10] = prod_pow(m1_set::bc<T>[0], 2) * z[10];
z[13] = abb[24] * (T(1) / T(2));
z[14] = abb[29] * (T(1) / T(2));
z[15] = z[13] + z[14] + -z[19];
z[16] = -abb[0] * z[15];
z[18] = abb[27] * (T(1) / T(2));
z[22] = abb[29] * (T(3) / T(2)) + -z[11] + z[13] + -z[18];
z[22] = abb[3] * z[22];
z[23] = abb[8] * z[6];
z[24] = 2 * z[23];
z[25] = abb[2] * z[6];
z[5] = -abb[29] + (T(1) / T(2)) * z[5];
z[26] = abb[6] * z[5];
z[27] = abb[25] + abb[28] + -abb[29];
z[27] = abb[4] * z[27];
z[1] = (T(1) / T(2)) * z[1] + -z[7] + z[16] + z[22] + -z[24] + z[25] + -z[26] + 2 * z[27];
z[1] = abb[14] * z[1];
z[16] = abb[3] * z[6];
z[6] = abb[6] * z[6];
z[22] = z[6] + z[16];
z[24] = z[22] + -z[24];
z[25] = -abb[16] * z[24];
z[1] = z[1] + z[25];
z[1] = abb[14] * z[1];
z[25] = abb[25] * (T(3) / T(2));
z[27] = abb[26] + -z[14] + -z[18] + z[25];
z[28] = abb[8] * z[27];
z[29] = abb[4] * z[15];
z[3] = (T(1) / T(2)) * z[3] + z[29];
z[30] = -z[11] + -3 * z[12];
z[31] = -abb[29] + (T(1) / T(2)) * z[30];
z[31] = abb[0] * z[31];
z[32] = -z[8] + z[17];
z[33] = -abb[26] + abb[29];
z[33] = z[32] + (T(-7) / T(4)) * z[33];
z[33] = abb[3] * z[33];
z[20] = -abb[26] + z[20];
z[20] = abb[5] * z[20];
z[34] = 3 * abb[25];
z[35] = -2 * abb[26] + abb[27] + abb[29] + -z[34];
z[36] = abb[2] * z[35];
z[31] = (T(-1) / T(2)) * z[3] + z[20] + -z[26] + -z[28] + z[31] + z[33] + -z[36];
z[31] = abb[13] * z[31];
z[33] = z[11] + z[12];
z[33] = abb[0] * z[33];
z[33] = z[3] + z[33];
z[33] = (T(1) / T(2)) * z[33];
z[37] = z[33] + z[36];
z[38] = abb[27] * (T(3) / T(2));
z[39] = abb[29] * (T(5) / T(4));
z[40] = abb[26] * (T(1) / T(4)) + z[32] + -z[38] + z[39];
z[40] = abb[3] * z[40];
z[41] = abb[29] * (T(7) / T(2));
z[25] = abb[26] + abb[27] * (T(5) / T(2)) + -z[25] + -z[41];
z[25] = abb[8] * z[25];
z[25] = z[6] + z[25] + -z[37] + z[40];
z[25] = abb[14] * z[25];
z[40] = 2 * abb[3];
z[42] = -abb[6] + -abb[8] + z[40];
z[42] = abb[16] * z[35] * z[42];
z[43] = 3 * z[36];
z[44] = abb[16] * z[43];
z[42] = z[42] + z[44];
z[25] = z[25] + z[31] + z[42];
z[25] = abb[13] * z[25];
z[31] = 4 * abb[25];
z[8] = abb[26] * (T(-11) / T(4)) + -z[8] + -z[31] + z[38] + z[39];
z[8] = abb[3] * z[8];
z[38] = abb[6] * z[35];
z[39] = 2 * z[36];
z[8] = z[8] + z[28] + -z[33] + -z[38] + z[39];
z[33] = abb[13] * z[8];
z[17] = -abb[27] + z[17];
z[38] = abb[26] * (T(-3) / T(2)) + -z[13] + z[14] + -z[17];
z[38] = abb[0] * z[38];
z[44] = -abb[24] + abb[25];
z[40] = z[40] * z[44];
z[2] = (T(1) / T(2)) * z[2] + z[29] + -z[36] + z[38] + z[40];
z[2] = abb[17] * z[2];
z[29] = abb[26] * (T(-5) / T(4)) + abb[29] * (T(3) / T(4)) + z[18] + -z[32];
z[29] = abb[3] * z[29];
z[28] = z[28] + z[29] + z[37];
z[28] = abb[14] * z[28];
z[28] = z[28] + -z[42];
z[2] = z[2] + z[28] + z[33];
z[2] = abb[17] * z[2];
z[15] = abb[9] * z[15];
z[27] = abb[11] * z[27];
z[9] = -abb[27] + abb[24] * (T(3) / T(2)) + z[9] + z[14];
z[14] = abb[10] * (T(1) / T(2));
z[9] = z[9] * z[14];
z[14] = abb[3] + abb[4];
z[14] = -abb[12] + abb[0] * (T(1) / T(2)) + (T(3) / T(4)) * z[14];
z[0] = z[0] * z[14];
z[0] = z[0] + z[9] + (T(3) / T(2)) * z[15] + z[27];
z[9] = abb[34] * z[0];
z[12] = abb[29] + z[12];
z[12] = abb[0] * z[12];
z[7] = -z[7] + z[12];
z[5] = -abb[3] * z[5];
z[5] = z[5] + -z[7] + -z[20] + 2 * z[21] + -z[26];
z[5] = abb[15] * z[5];
z[14] = -abb[14] * z[24];
z[5] = z[5] + z[14];
z[5] = abb[15] * z[5];
z[12] = -z[12] + -z[16] + z[20] + z[23];
z[12] = abb[18] * z[12];
z[14] = abb[20] + -abb[33];
z[14] = z[14] * z[24];
z[15] = prod_pow(abb[16], 2);
z[16] = abb[26] + z[17];
z[16] = z[15] * z[16];
z[11] = -abb[29] + z[11];
z[11] = abb[19] * z[11];
z[17] = abb[32] * z[35];
z[16] = -z[11] + z[16] + z[17];
z[16] = abb[2] * z[16];
z[17] = 4 * abb[26] + abb[27] + -5 * abb[29] + z[34];
z[23] = abb[32] * z[17];
z[23] = 3 * z[11] + z[23];
z[23] = abb[8] * z[23];
z[18] = -z[18] + z[19];
z[15] = z[15] * z[18];
z[18] = abb[26] + -2 * abb[27] + abb[29] + z[34];
z[19] = abb[32] * z[18];
z[19] = -3 * z[15] + z[19];
z[19] = abb[6] * z[19];
z[11] = -z[11] + z[15];
z[15] = -6 * abb[25] + -5 * abb[26] + abb[27] + 4 * abb[29];
z[26] = abb[32] * z[15];
z[11] = 3 * z[11] + z[26];
z[11] = abb[3] * z[11];
z[1] = z[1] + z[2] + z[5] + z[9] + z[10] + z[11] + z[12] + z[14] + 3 * z[16] + z[19] + z[23] + z[25];
z[2] = abb[26] * (T(-7) / T(2)) + z[13] + -z[31] + z[41];
z[2] = abb[3] * z[2];
z[4] = z[4] + -z[30];
z[4] = abb[0] * z[4];
z[5] = 2 * z[20];
z[9] = -abb[8] * z[35];
z[2] = z[2] + z[3] + z[4] + -z[5] + -z[6] + z[9] + z[39];
z[2] = abb[13] * z[2];
z[3] = -abb[17] * z[8];
z[4] = z[5] + 2 * z[7] + -4 * z[21] + -z[22];
z[4] = abb[15] * z[4];
z[2] = z[2] + z[3] + z[4] + z[28];
z[2] = m1_set::bc<T>[0] * z[2];
z[0] = abb[23] * z[0];
z[3] = abb[8] * z[17];
z[4] = abb[6] * z[18];
z[5] = abb[3] * z[15];
z[3] = z[3] + z[4] + z[5] + z[43];
z[3] = abb[21] * z[3];
z[4] = -abb[22] * z[24];
z[0] = z[0] + z[2] + z[3] + z[4];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_390_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("13.89080176549358099314059959235238098129596881720881596598448881"),stof<T>("-60.437637728212557938425205334794277264659936456790018877794028093")}, std::complex<T>{stof<T>("23.879381808358947358200499996971327984902276670437873584039445088"),stof<T>("-4.964525490228577077160159552622320638799579908595529274791218172")}, std::complex<T>{stof<T>("0.2015533229792085425028727201302613426726795509405163333223935846"),stof<T>("-9.5031409798842799019678613114784397898708661711707674171535287782")}, std::complex<T>{stof<T>("-15.877892395071963112430422521104305911120166048471898901490971519"),stof<T>("37.643942315400290543009241147558098481130285140035957282348822649")}, std::complex<T>{stof<T>("21.690737855801356696407804348089141712405399888234274315210568794"),stof<T>("-18.255079923156564570608262428380059632458365054178823453082894837")}, std::complex<T>{stof<T>("-6.0143987837086021264802545471150971439579133907028917470419908594"),stof<T>("-9.8857214123594460704331174076995990588010539146863664121123990331")}, std::complex<T>{stof<T>("1.8875214468817965659845470056534843067331750307489830546406995373"),stof<T>("-3.073952256592314428553215974881268771197457590719930264362430233")}, std::complex<T>{stof<T>("1.8875214468817965659845470056534843067331750307489830546406995373"),stof<T>("-3.073952256592314428553215974881268771197457590719930264362430233")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[125].real()/kbase.W[125].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_390_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_390_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-61.050504075050102766174370205160398789496951453334777072294893951"),stof<T>("26.095116799996252388387906399154123963584972728326104579889433891")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,35> abb = {dl[0], dl[1], dl[5], dlog_W8(k,dl), dl[3], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_7(k), f_2_3(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[125].real()/k.W[125].real()), f_2_4_re(k), f_2_6_re(k), f_2_24_re(k)};

                    
            return f_4_390_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_390_DLogXconstant_part(base_point<T>, kend);
	value += f_4_390_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_390_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_390_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_390_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_390_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_390_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_390_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
