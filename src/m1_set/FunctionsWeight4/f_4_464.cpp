/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_464.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_464_abbreviated (const std::array<T,65>& abb) {
T z[91];
z[0] = -abb[31] + abb[32];
z[1] = abb[13] * abb[52];
z[2] = z[0] * z[1];
z[2] = (T(1) / T(2)) * z[2];
z[3] = abb[28] + -abb[29];
z[4] = z[1] * z[3];
z[5] = -abb[45] + abb[46] + -abb[47] + abb[48];
z[6] = abb[31] * z[5];
z[7] = -abb[32] * z[5];
z[8] = z[6] + z[7];
z[9] = (T(1) / T(2)) * z[8];
z[10] = -abb[29] * z[5];
z[11] = z[9] + z[10];
z[12] = -abb[28] * z[5];
z[13] = -z[11] + z[12];
z[13] = abb[24] * z[13];
z[14] = abb[49] + abb[52];
z[15] = abb[32] * z[14];
z[16] = abb[31] * z[14];
z[17] = z[15] + -z[16];
z[18] = abb[28] * z[14];
z[19] = (T(1) / T(2)) * z[17] + z[18];
z[19] = abb[12] * z[19];
z[20] = abb[49] + -abb[52];
z[21] = abb[5] * z[20];
z[22] = abb[32] * z[21];
z[4] = -z[2] + z[4] + z[13] + z[19] + z[22];
z[13] = abb[49] + abb[52] * (T(5) / T(4));
z[13] = abb[31] * z[13];
z[19] = abb[52] * (T(1) / T(4));
z[23] = abb[32] * z[19];
z[24] = abb[49] + abb[52] * (T(3) / T(2));
z[25] = -abb[28] * z[24];
z[13] = z[13] + z[23] + z[25];
z[13] = abb[7] * z[13];
z[23] = abb[49] + 3 * abb[52];
z[23] = abb[7] * z[23];
z[25] = abb[12] * z[14];
z[26] = z[23] + -z[25];
z[27] = abb[33] * (T(1) / T(2));
z[26] = z[26] * z[27];
z[23] = -z[21] + -z[23];
z[28] = abb[30] * (T(1) / T(2));
z[23] = z[23] * z[28];
z[4] = (T(1) / T(2)) * z[4] + z[13] + z[23] + z[26];
z[4] = m1_set::bc<T>[0] * z[4];
z[13] = abb[16] * abb[29];
z[23] = abb[16] * abb[31];
z[13] = z[13] + -z[23];
z[26] = abb[31] + abb[32];
z[26] = -abb[28] + (T(1) / T(2)) * z[26];
z[29] = abb[15] * z[26];
z[29] = -z[13] + z[29];
z[29] = m1_set::bc<T>[0] * z[29];
z[30] = abb[7] + abb[13];
z[31] = -abb[27] + (T(1) / T(4)) * z[30];
z[32] = -abb[41] * z[31];
z[33] = abb[15] * (T(1) / T(2));
z[33] = -abb[37] * z[33];
z[34] = abb[29] + (T(1) / T(2)) * z[0];
z[35] = -abb[28] + z[34];
z[35] = m1_set::bc<T>[0] * z[35];
z[35] = -abb[37] + z[35];
z[36] = (T(1) / T(2)) * z[35];
z[37] = abb[18] * z[36];
z[29] = (T(1) / T(2)) * z[29] + z[32] + z[33] + z[37];
z[29] = abb[55] * z[29];
z[32] = abb[49] * (T(1) / T(2));
z[33] = abb[52] + z[32];
z[37] = abb[31] * z[33];
z[38] = abb[32] * z[33];
z[37] = z[37] + -z[38];
z[37] = (T(1) / T(2)) * z[37];
z[3] = -z[3] * z[33];
z[3] = z[3] + -z[37];
z[3] = m1_set::bc<T>[0] * z[3];
z[39] = abb[51] + abb[53];
z[36] = z[36] * z[39];
z[39] = -abb[37] * z[33];
z[40] = abb[55] * (T(1) / T(4));
z[41] = -abb[41] * z[40];
z[3] = z[3] + z[36] + z[39] + z[41];
z[3] = abb[0] * z[3];
z[36] = abb[37] + abb[39];
z[39] = -z[5] * z[36];
z[41] = -abb[38] * z[5];
z[42] = -abb[36] * z[5];
z[41] = z[41] + z[42];
z[39] = z[39] + -z[41];
z[43] = abb[29] * (T(1) / T(2));
z[44] = -z[5] * z[43];
z[45] = z[8] + z[44];
z[46] = -z[5] * z[27];
z[47] = -z[45] + z[46];
z[47] = m1_set::bc<T>[0] * z[47];
z[39] = (T(1) / T(2)) * z[39] + z[47];
z[39] = abb[25] * z[39];
z[47] = z[14] * z[43];
z[48] = z[17] + z[47];
z[49] = z[14] * z[27];
z[50] = -z[48] + z[49];
z[50] = m1_set::bc<T>[0] * z[50];
z[36] = -abb[36] + -abb[38] + z[36];
z[36] = z[14] * z[36];
z[36] = (T(1) / T(2)) * z[36] + z[50];
z[36] = abb[14] * z[36];
z[47] = -z[18] + z[47] + z[49];
z[47] = m1_set::bc<T>[0] * z[47];
z[50] = abb[39] + abb[40];
z[51] = abb[38] + z[50];
z[51] = z[14] * z[51];
z[47] = z[47] + (T(1) / T(2)) * z[51];
z[47] = abb[3] * z[47];
z[3] = z[3] + z[4] + z[29] + z[36] + z[39] + z[47];
z[4] = abb[8] * z[14];
z[29] = -z[1] + z[4];
z[36] = -abb[24] * z[5];
z[39] = abb[7] * abb[52];
z[47] = z[36] + -z[39];
z[51] = -z[29] + z[47];
z[51] = abb[37] * z[51];
z[0] = -abb[13] * z[0];
z[52] = abb[13] * abb[29];
z[0] = (T(1) / T(2)) * z[0] + -z[52];
z[53] = abb[13] * abb[28];
z[26] = abb[7] * z[26];
z[26] = z[0] + z[26] + z[53];
z[26] = m1_set::bc<T>[0] * z[26];
z[53] = abb[7] + -abb[13];
z[54] = abb[37] * z[53];
z[55] = abb[15] * abb[41];
z[26] = z[26] + -z[54] + (T(-1) / T(2)) * z[55];
z[54] = abb[18] * abb[41];
z[56] = abb[29] + -abb[31];
z[57] = -abb[4] * m1_set::bc<T>[0] * z[56];
z[57] = z[26] + (T(1) / T(2)) * z[54] + z[57];
z[57] = abb[53] * z[57];
z[58] = -abb[33] * z[5];
z[58] = -z[12] + z[58];
z[9] = z[9] + -z[58];
z[9] = m1_set::bc<T>[0] * z[9];
z[9] = z[9] + z[42];
z[9] = abb[19] * z[9];
z[42] = z[4] + z[21];
z[59] = abb[7] * z[20];
z[60] = z[42] + -z[59];
z[61] = abb[39] * z[60];
z[62] = abb[7] * z[14];
z[62] = -z[4] + z[62];
z[62] = abb[40] * z[62];
z[63] = -z[4] + z[25];
z[63] = abb[36] * z[63];
z[64] = abb[30] * z[20];
z[65] = abb[31] * z[20];
z[66] = z[64] + -z[65];
z[66] = m1_set::bc<T>[0] * z[66];
z[67] = abb[38] * z[20];
z[66] = z[66] + z[67];
z[66] = abb[6] * z[66];
z[67] = abb[38] * z[4];
z[9] = z[9] + z[51] + z[57] + -z[61] + z[62] + z[63] + z[66] + -z[67];
z[26] = -abb[44] + z[26];
z[51] = abb[29] + 3 * abb[31];
z[57] = abb[30] + -abb[33];
z[51] = -abb[28] + (T(1) / T(4)) * z[51] + -z[57];
z[51] = m1_set::bc<T>[0] * z[51];
z[61] = abb[40] * (T(1) / T(2));
z[51] = z[51] + z[61];
z[51] = abb[4] * z[51];
z[62] = abb[28] + -abb[31];
z[63] = -abb[33] + z[62];
z[66] = z[28] + z[63];
z[67] = abb[32] * (T(1) / T(2));
z[68] = z[66] + z[67];
z[68] = m1_set::bc<T>[0] * z[68];
z[68] = (T(-1) / T(2)) * z[50] + z[68];
z[68] = abb[1] * z[68];
z[69] = m1_set::bc<T>[0] * z[57];
z[69] = abb[39] + z[69];
z[70] = abb[3] * (T(1) / T(2));
z[69] = z[69] * z[70];
z[26] = (T(-1) / T(4)) * z[26] + z[51] + (T(-1) / T(8)) * z[54] + -z[68] + -z[69];
z[51] = abb[51] * z[26];
z[35] = abb[0] * z[35];
z[26] = -z[26] + (T(1) / T(4)) * z[35];
z[26] = abb[50] * z[26];
z[35] = 3 * abb[49];
z[68] = abb[52] + z[35];
z[69] = abb[31] * z[68];
z[71] = 3 * z[15] + -z[69];
z[72] = abb[29] * abb[52];
z[35] = -abb[52] + z[35];
z[35] = abb[28] * z[35];
z[35] = z[35] + z[64] + (T(1) / T(2)) * z[71] + z[72];
z[64] = abb[33] * abb[49];
z[35] = (T(1) / T(4)) * z[35] + -z[64];
z[35] = m1_set::bc<T>[0] * z[35];
z[71] = abb[38] + abb[40];
z[73] = -z[20] * z[71];
z[74] = abb[36] * z[14];
z[73] = z[73] + z[74];
z[35] = z[35] + (T(1) / T(4)) * z[73];
z[35] = abb[4] * z[35];
z[73] = abb[31] * abb[49];
z[74] = abb[28] * abb[49];
z[75] = z[73] + -z[74];
z[76] = z[64] + z[75];
z[77] = z[14] * z[67];
z[78] = z[20] * z[28];
z[79] = -z[76] + z[77] + z[78];
z[79] = m1_set::bc<T>[0] * z[79];
z[80] = -abb[39] * z[14];
z[81] = -abb[40] * abb[49];
z[80] = z[80] + z[81];
z[81] = abb[30] + z[63];
z[81] = m1_set::bc<T>[0] * z[81];
z[61] = -z[61] + z[81];
z[81] = -abb[54] * z[61];
z[79] = z[79] + (T(1) / T(2)) * z[80] + z[81];
z[79] = abb[1] * z[79];
z[80] = -abb[33] + z[28];
z[81] = -z[14] * z[80];
z[81] = (T(1) / T(2)) * z[16] + -z[18] + z[81];
z[81] = m1_set::bc<T>[0] * z[81];
z[71] = z[14] * z[71];
z[71] = (T(1) / T(2)) * z[71] + z[81];
z[71] = abb[9] * z[71];
z[61] = -abb[4] * z[61];
z[61] = abb[44] * (T(1) / T(4)) + z[61];
z[61] = abb[54] * z[61];
z[26] = -z[26] + -z[35] + z[51] + -z[61] + -z[71] + -z[79];
z[35] = 3 * z[6] + z[7];
z[51] = -abb[30] * z[5];
z[35] = (T(1) / T(2)) * z[35] + z[51] + -z[58];
z[35] = m1_set::bc<T>[0] * z[35];
z[35] = z[35] + z[41];
z[51] = abb[21] + abb[23];
z[58] = (T(1) / T(32)) * z[51];
z[35] = z[35] * z[58];
z[50] = abb[37] + z[50];
z[50] = -z[5] * z[50];
z[41] = z[41] + z[50];
z[50] = abb[20] + abb[22];
z[58] = (T(1) / T(32)) * z[50];
z[41] = z[41] * z[58];
z[54] = -z[54] + z[55];
z[54] = abb[44] * (T(1) / T(32)) + (T(-1) / T(64)) * z[54];
z[54] = abb[52] * z[54];
z[55] = abb[16] + abb[17];
z[55] = z[14] * z[55];
z[58] = -abb[26] * z[5];
z[55] = z[55] + z[58];
z[58] = abb[41] * z[55];
z[3] = abb[62] + abb[63] + (T(1) / T(16)) * z[3] + (T(1) / T(32)) * z[9] + (T(-1) / T(8)) * z[26] + z[35] + z[41] + z[54] + (T(1) / T(64)) * z[58];
z[9] = abb[28] * (T(1) / T(2));
z[26] = -z[5] * z[9];
z[11] = -z[11] + z[26];
z[11] = abb[28] * z[11];
z[26] = z[6] * z[67];
z[35] = -abb[57] * z[5];
z[7] = abb[29] * z[7];
z[7] = z[7] + z[11] + z[26] + -z[35];
z[7] = abb[24] * z[7];
z[11] = abb[31] * z[24];
z[24] = abb[52] * z[67];
z[41] = -abb[28] * z[33];
z[11] = z[11] + z[24] + z[41];
z[11] = abb[28] * z[11];
z[24] = abb[31] * z[67];
z[41] = -abb[57] + z[24];
z[54] = -abb[31] + z[43];
z[58] = abb[29] * z[54];
z[61] = -z[41] + z[58];
z[61] = abb[52] * z[61];
z[71] = prod_pow(abb[31], 2);
z[79] = z[32] * z[71];
z[81] = -abb[60] * z[14];
z[11] = z[11] + z[61] + -z[79] + z[81];
z[11] = abb[7] * z[11];
z[29] = -z[9] * z[29];
z[61] = -abb[29] * z[1];
z[2] = -z[2] + z[29] + z[61];
z[2] = abb[28] * z[2];
z[29] = -z[14] * z[24];
z[61] = -z[16] + z[18];
z[81] = z[15] + z[61];
z[81] = z[9] * z[81];
z[82] = abb[56] * z[14];
z[29] = z[29] + z[81] + -z[82];
z[29] = abb[12] * z[29];
z[81] = z[21] + -z[25] + z[59];
z[81] = abb[35] * z[81];
z[83] = abb[7] + -abb[12];
z[83] = z[61] * z[83];
z[21] = (T(1) / T(2)) * z[21];
z[84] = z[21] + -z[39];
z[84] = abb[33] * z[84];
z[83] = -z[22] + z[83] + z[84];
z[83] = abb[33] * z[83];
z[84] = abb[58] + abb[60];
z[85] = z[4] * z[84];
z[86] = abb[57] + z[24];
z[87] = abb[29] * abb[32];
z[88] = -z[86] + z[87];
z[88] = z[1] * z[88];
z[89] = abb[57] * z[14];
z[89] = z[82] + z[89];
z[89] = abb[8] * z[89];
z[60] = abb[59] * z[60];
z[2] = z[2] + z[7] + z[11] + z[29] + z[60] + z[81] + z[83] + z[85] + z[88] + z[89];
z[7] = abb[49] + abb[52] * (T(1) / T(2));
z[7] = abb[33] * z[7];
z[7] = z[7] + -z[15] + z[75];
z[7] = abb[33] * z[7];
z[11] = -abb[52] * z[28];
z[11] = z[11] + z[15] + -z[76];
z[11] = abb[30] * z[11];
z[27] = z[27] + -z[62];
z[27] = abb[33] * z[27];
z[29] = abb[30] * z[66];
z[27] = abb[60] + z[27] + z[29];
z[29] = -abb[31] + z[9];
z[29] = abb[28] * z[29];
z[60] = (T(1) / T(2)) * z[71];
z[66] = z[29] + z[60];
z[76] = prod_pow(m1_set::bc<T>[0], 2);
z[81] = (T(1) / T(3)) * z[76];
z[83] = -z[27] + -z[66] + -z[81];
z[83] = abb[54] * z[83];
z[85] = abb[35] + abb[59];
z[85] = z[14] * z[85];
z[88] = abb[49] * z[9];
z[73] = -z[73] + z[88];
z[73] = abb[28] * z[73];
z[88] = (T(1) / T(2)) * z[76];
z[89] = abb[49] + abb[52] * (T(1) / T(3));
z[89] = z[88] * z[89];
z[90] = abb[49] * abb[60];
z[7] = z[7] + z[11] + z[73] + z[79] + z[83] + z[85] + z[89] + z[90];
z[7] = abb[1] * z[7];
z[11] = -abb[32] + z[62];
z[11] = z[9] * z[11];
z[73] = z[58] + z[60];
z[11] = z[11] + z[41] + z[73];
z[41] = abb[7] * z[11];
z[79] = abb[13] * z[9];
z[0] = z[0] + z[79];
z[0] = abb[28] * z[0];
z[53] = z[53] * z[81];
z[79] = -abb[13] * z[86];
z[30] = abb[34] * z[30];
z[52] = abb[32] * z[52];
z[0] = z[0] + -z[30] + -z[41] + z[52] + z[53] + z[79];
z[30] = z[54] + -z[67];
z[30] = abb[29] * z[30];
z[41] = -abb[29] + z[62];
z[41] = z[9] * z[41];
z[52] = (T(1) / T(4)) * z[76];
z[30] = abb[34] * (T(-3) / T(2)) + -z[24] + z[27] + -z[30] + z[41] + z[52];
z[30] = abb[4] * z[30];
z[41] = abb[34] + z[73];
z[53] = abb[30] * z[80];
z[62] = prod_pow(abb[33], 2);
z[53] = abb[35] + -abb[59] + -z[41] + z[53] + (T(1) / T(2)) * z[62] + -z[81];
z[53] = abb[3] * z[53];
z[62] = abb[32] + z[63];
z[57] = z[57] * z[62];
z[62] = abb[35] + z[88];
z[63] = abb[59] + abb[60];
z[57] = z[57] + z[62] + z[63] + z[66];
z[57] = abb[1] * z[57];
z[66] = -abb[15] + abb[18];
z[67] = abb[61] * (T(1) / T(4));
z[73] = z[66] * z[67];
z[79] = abb[64] * (T(1) / T(2));
z[30] = (T(1) / T(2)) * z[0] + z[30] + z[53] + z[57] + -z[73] + z[79];
z[34] = -z[9] + z[34];
z[34] = abb[28] * z[34];
z[34] = z[34] + z[86];
z[53] = abb[32] + z[54];
z[53] = abb[29] * z[53];
z[57] = (T(1) / T(6)) * z[76];
z[53] = z[34] + -z[53] + z[57] + -z[60];
z[73] = abb[0] * (T(1) / T(2));
z[80] = z[53] * z[73];
z[80] = z[30] + z[80];
z[80] = abb[50] * z[80];
z[83] = -abb[34] + z[87];
z[85] = -abb[31] * abb[32];
z[56] = -abb[28] * z[56];
z[56] = z[56] + z[57] + z[83] + z[85];
z[56] = abb[4] * z[56];
z[0] = z[0] + z[56];
z[41] = z[41] + z[57];
z[56] = -abb[2] * z[41];
z[0] = (T(1) / T(2)) * z[0] + z[56];
z[0] = abb[53] * z[0];
z[56] = z[15] + -z[69];
z[56] = (T(1) / T(2)) * z[56] + z[72] + z[74];
z[56] = abb[28] * z[56];
z[24] = -z[24] + z[84];
z[24] = z[20] * z[24];
z[69] = -abb[52] * z[87];
z[72] = abb[35] * z[14];
z[24] = z[24] + z[56] + z[69] + z[72] + -z[82];
z[56] = abb[33] * z[68];
z[56] = (T(1) / T(4)) * z[56] + z[75] + -z[77];
z[56] = abb[33] * z[56];
z[68] = abb[34] * (T(1) / T(2));
z[69] = z[68] + (T(-5) / T(12)) * z[76];
z[69] = abb[52] * z[69];
z[72] = abb[28] * z[20];
z[72] = z[15] + z[72];
z[64] = -z[64] + (T(1) / T(2)) * z[72];
z[64] = abb[30] * z[64];
z[24] = (T(1) / T(2)) * z[24] + z[56] + z[64] + z[69];
z[24] = abb[4] * z[24];
z[49] = z[49] + -z[61];
z[56] = -abb[33] * z[49];
z[64] = -z[81] + -z[84];
z[64] = z[14] * z[64];
z[69] = -z[9] * z[14];
z[69] = z[16] + z[69];
z[69] = abb[28] * z[69];
z[72] = abb[33] * z[14];
z[72] = -z[18] + z[72];
z[72] = abb[30] * z[72];
z[56] = z[56] + z[64] + z[69] + z[72];
z[56] = abb[9] * z[56];
z[27] = abb[34] + -z[27] + -z[29] + -z[57] + z[58];
z[27] = abb[4] * z[27];
z[29] = -abb[2] + abb[3];
z[29] = z[29] * z[41];
z[27] = z[27] + z[29] + -z[79];
z[27] = abb[54] * z[27];
z[29] = -abb[7] * z[61];
z[58] = abb[8] * z[18];
z[22] = z[22] + z[29] + z[58];
z[29] = -z[39] + (T(-1) / T(2)) * z[42];
z[29] = z[28] * z[29];
z[39] = abb[33] * z[39];
z[22] = (T(1) / T(2)) * z[22] + z[29] + z[39];
z[22] = abb[30] * z[22];
z[29] = -abb[52] * z[66];
z[39] = abb[53] * z[66];
z[29] = z[29] + -z[39] + -z[55];
z[31] = abb[55] * z[31];
z[29] = (T(1) / T(4)) * z[29] + z[31];
z[29] = abb[61] * z[29];
z[31] = -z[1] + -z[47];
z[31] = z[31] * z[68];
z[0] = z[0] + (T(1) / T(2)) * z[2] + z[7] + z[22] + z[24] + z[27] + z[29] + z[31] + z[56] + z[80];
z[2] = abb[51] * (T(1) / T(2));
z[7] = z[2] * z[53];
z[22] = abb[31] * z[32];
z[24] = abb[49] * (T(1) / T(4));
z[27] = -abb[29] * z[24];
z[22] = z[22] + z[27] + -z[38];
z[22] = abb[29] * z[22];
z[27] = -abb[29] + z[9];
z[29] = -z[27] * z[33];
z[29] = z[29] + -z[37];
z[29] = abb[28] * z[29];
z[31] = -abb[32] + z[54];
z[31] = abb[29] * z[31];
z[31] = z[31] + z[34] + z[60];
z[31] = abb[34] + (T(1) / T(2)) * z[31] + z[52];
z[31] = abb[53] * z[31];
z[32] = z[33] * z[86];
z[33] = -z[24] * z[71];
z[24] = abb[52] + z[24];
z[24] = z[24] * z[81];
z[37] = abb[54] * z[41];
z[38] = abb[34] * abb[52];
z[39] = abb[55] * z[67];
z[7] = z[7] + z[22] + z[24] + z[29] + z[31] + z[32] + z[33] + z[37] + z[38] + z[39];
z[7] = z[7] * z[73];
z[22] = abb[29] * z[45];
z[24] = z[8] + z[12];
z[29] = z[24] + -z[46];
z[29] = abb[33] * z[29];
z[31] = -abb[56] * z[5];
z[32] = -abb[58] * z[5];
z[32] = z[31] + z[32];
z[10] = -z[8] + -z[10];
z[10] = abb[28] * z[10];
z[8] = -abb[30] * z[8];
z[33] = abb[59] * z[5];
z[8] = z[8] + z[10] + z[22] + z[29] + z[32] + z[33] + -z[35];
z[8] = abb[25] * z[8];
z[10] = abb[29] * z[48];
z[22] = z[15] + -z[49];
z[22] = abb[33] * z[22];
z[29] = abb[56] + -abb[57] + abb[58] + -abb[59];
z[29] = z[14] * z[29];
z[33] = -abb[29] * z[14];
z[33] = -z[17] + z[33];
z[33] = abb[28] * z[33];
z[17] = -abb[30] * z[17];
z[10] = z[10] + z[17] + z[22] + z[29] + z[33];
z[10] = abb[14] * z[10];
z[17] = -abb[34] + -abb[58] + -z[57];
z[17] = z[17] * z[20];
z[20] = -z[20] * z[43];
z[20] = z[20] + z[65];
z[20] = abb[29] * z[20];
z[22] = -z[65] + z[78];
z[22] = abb[30] * z[22];
z[17] = z[17] + z[20] + z[22];
z[17] = abb[6] * z[17];
z[20] = z[6] + z[12];
z[20] = abb[33] * z[20];
z[9] = z[9] * z[24];
z[9] = -z[9] + z[20] + -z[26];
z[20] = -z[5] * z[81];
z[22] = abb[35] * z[5];
z[22] = -z[9] + -z[20] + z[22] + -z[31];
z[22] = abb[19] * z[22];
z[8] = z[8] + z[10] + z[17] + z[22];
z[10] = -abb[15] * z[11];
z[11] = z[34] + z[81] + -z[83];
z[11] = abb[18] * z[11];
z[13] = -abb[28] * z[13];
z[17] = abb[15] + abb[16] * (T(1) / T(2));
z[17] = z[17] * z[81];
z[22] = -abb[32] * z[23];
z[23] = abb[16] * z[87];
z[24] = -abb[15] + -abb[16];
z[24] = abb[34] * z[24];
z[10] = z[10] + z[11] + z[13] + z[17] + z[22] + z[23] + z[24];
z[10] = z[10] * z[40];
z[2] = z[2] * z[30];
z[11] = abb[34] + z[62];
z[11] = -z[5] * z[11];
z[13] = z[6] + z[44];
z[13] = abb[29] * z[13];
z[17] = -z[5] * z[28];
z[6] = z[6] + z[17];
z[6] = abb[30] * z[6];
z[6] = z[6] + -z[9] + -z[11] + -z[13] + -z[32];
z[9] = (T(1) / T(4)) * z[51];
z[6] = z[6] * z[9];
z[9] = abb[34] + abb[35] + -abb[58] + -z[63];
z[9] = z[9] * z[14];
z[11] = -z[18] * z[27];
z[13] = abb[33] * z[61];
z[22] = z[14] * z[28];
z[18] = -z[18] + z[22];
z[18] = abb[30] * z[18];
z[16] = abb[32] * z[16];
z[15] = -abb[29] * z[15];
z[9] = z[9] + z[11] + z[13] + z[15] + z[16] + z[18];
z[11] = -z[14] * z[81];
z[9] = (T(1) / T(2)) * z[9] + z[11];
z[9] = z[9] * z[70];
z[11] = prod_pow(abb[28], 2);
z[11] = (T(1) / T(2)) * z[11] + -z[63];
z[5] = -z[5] * z[11];
z[11] = -z[12] + z[17];
z[11] = abb[30] * z[11];
z[5] = z[5] + z[11] + -z[32] + -z[35];
z[5] = (T(1) / T(4)) * z[5] + -z[20];
z[5] = z[5] * z[50];
z[1] = z[1] + -z[21] + z[25] + z[36];
z[1] = (T(-1) / T(4)) * z[1] + z[4];
z[1] = (T(1) / T(3)) * z[1] + (T(-1) / T(8)) * z[59];
z[1] = z[1] * z[76];
z[4] = -abb[64] * z[19];
z[0] = (T(1) / T(2)) * z[0] + z[1] + z[2] + z[4] + z[5] + z[6] + z[7] + (T(1) / T(4)) * z[8] + z[9] + z[10];
z[0] = abb[42] + abb[43] + (T(1) / T(8)) * z[0];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_464_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("-0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("-0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("-0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("-0.053288561085580309456759226098342009758950786597486899487371394669"),stof<T>("-0.061842275467620006176704672420265680252001586638264831807137015426")}, std::complex<T>{stof<T>("-0.118663281162297222230451109681332941933695541789799826282352309027"),stof<T>("-0.046715516044768915744719746978202008178539297230429104501961264694")}, std::complex<T>{stof<T>("-0.118663281162297222230451109681332941933695541789799826282352309027"),stof<T>("-0.046715516044768915744719746978202008178539297230429104501961264694")}, std::complex<T>{stof<T>("-0.020898306371438252515741060746933841823397787250839508815725475101"),stof<T>("-0.13574022450088939322201724851925175755513123840040319374065701043")}, std::complex<T>{stof<T>("-0.1351135996422727134439098190793225223541996862176824857816791106"),stof<T>("-0.30254326910386149651592487300032369275821954141726113707824483379")}, std::complex<T>{stof<T>("-0.12914315315118745546968416203968801705472160933480442966735225988"),stof<T>("-0.41731877743565414418514426699660811792460362213415030181661970505")}, std::complex<T>{stof<T>("-0.052678935884231730299906118451924879625719695309925263090767030428"),stof<T>("-0.119129763596743450607089913422324455774389719436512833921156798303")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_464_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_464_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(128)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(-1) / T(32)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[49] + abb[50] + abb[51]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[49] + -abb[50] + -abb[51];
z[1] = abb[32] + -abb[33];
return abb[10] * m1_set::bc<T>[0] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_464_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(128)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (8 + v[0] + v[1] + -v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(16)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[49] + abb[50] + abb[51]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[30] + abb[33];
z[1] = -abb[32] + abb[33];
z[0] = z[0] * z[1];
z[0] = abb[35] + (T(1) / T(2)) * z[0];
z[1] = -abb[49] + -abb[50] + -abb[51];
return abb[10] * (T(1) / T(8)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_464_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(128)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(-1) / T(32)) * (v[2] + v[3])) / tend;


		return (abb[50] + abb[51] + abb[53]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[50] + abb[51] + abb[53];
z[1] = abb[29] + -abb[31];
return abb[11] * m1_set::bc<T>[0] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_464_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(128)) * (v[2] + v[3]) * (-4 * v[0] + -2 * v[1] + -m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 2 * (-4 + v[2] + 2 * v[3] + v[4] + -2 * v[5]))) / prod_pow(tend, 2);
c[1] = ((-2 + m1_set::bc<T>[1]) * (T(1) / T(32)) * (v[2] + v[3])) / tend;


		return (abb[50] + abb[51] + abb[53]) * (t * c[0] + c[1]);
	}
	{
T z[3];
z[0] = -abb[28] + abb[31] + abb[29] * (T(-1) / T(2));
z[0] = abb[29] * z[0];
z[1] = -abb[28] + abb[31] * (T(1) / T(2));
z[1] = abb[31] * z[1];
z[2] = abb[29] + -abb[31];
z[2] = abb[32] * z[2];
z[0] = z[0] + -z[1] + z[2];
z[0] = -abb[34] + (T(1) / T(2)) * z[0];
z[1] = -abb[50] + -abb[51] + -abb[53];
return abb[11] * (T(1) / T(8)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_464_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.36305763688590465716802049969219397096264130247739573151632483067"),stof<T>("-0.23018706837600162262874729035368137348782582526734162686533743354")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,65> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W17(k,dl), dlog_W19(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), T{0}, T{0}, T{0}};
abb[42] = SpDLog_f_4_464_W_17_Im(t, path, abb);
abb[43] = SpDLog_f_4_464_W_19_Im(t, path, abb);
abb[44] = SpDLogQ_W_82(k,dl,dlr).imag();
abb[62] = SpDLog_f_4_464_W_17_Re(t, path, abb);
abb[63] = SpDLog_f_4_464_W_19_Re(t, path, abb);
abb[64] = SpDLogQ_W_82(k,dl,dlr).real();

                    
            return f_4_464_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_464_DLogXconstant_part(base_point<T>, kend);
	value += f_4_464_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_464_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_464_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_464_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_464_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_464_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_464_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
