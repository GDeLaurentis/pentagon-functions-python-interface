/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_322.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_322_abbreviated (const std::array<T,29>& abb) {
T z[81];
z[0] = 5 * abb[19];
z[1] = 2 * abb[21];
z[2] = z[0] + z[1];
z[3] = abb[24] * (T(1) / T(3));
z[4] = abb[18] + abb[20];
z[5] = (T(-2) / T(3)) * z[2] + z[3] + z[4];
z[5] = abb[6] * z[5];
z[6] = abb[22] + abb[23];
z[7] = abb[6] * z[6];
z[5] = z[5] + (T(1) / T(3)) * z[7];
z[8] = 2 * abb[20];
z[9] = 2 * abb[19];
z[10] = abb[21] + z[9];
z[11] = -z[8] + z[10];
z[12] = 3 * abb[23];
z[13] = 2 * abb[18];
z[11] = abb[24] * (T(-13) / T(6)) + abb[22] * (T(-2) / T(3)) + (T(1) / T(3)) * z[11] + z[12] + z[13];
z[11] = abb[3] * z[11];
z[14] = 4 * abb[19] + abb[21] * (T(5) / T(3)) + -z[8];
z[3] = -abb[23] + abb[18] * (T(-11) / T(3)) + z[3] + 2 * z[14];
z[3] = abb[5] * z[3];
z[14] = abb[20] * (T(1) / T(2));
z[15] = -abb[18] + z[14];
z[16] = 5 * abb[21];
z[17] = -8 * abb[19] + z[15] + -z[16];
z[18] = abb[24] * (T(3) / T(2));
z[17] = abb[23] * (T(-1) / T(6)) + (T(1) / T(3)) * z[17] + z[18];
z[17] = abb[2] * z[17];
z[19] = 2 * z[10];
z[20] = z[14] + z[19];
z[20] = abb[24] * (T(-16) / T(3)) + abb[18] * (T(-1) / T(2)) + abb[23] * (T(23) / T(6)) + abb[22] * (T(35) / T(6)) + (T(1) / T(3)) * z[20];
z[20] = abb[1] * z[20];
z[21] = abb[7] + abb[8];
z[22] = abb[25] * z[21];
z[23] = abb[9] * abb[26];
z[24] = abb[7] * abb[26];
z[25] = abb[8] * abb[26];
z[26] = 3 * abb[21];
z[27] = abb[22] * (T(-13) / T(2)) + abb[23] * (T(-11) / T(3)) + abb[20] * (T(-4) / T(3)) + abb[18] * (T(3) / T(2)) + abb[24] * (T(5) / T(3)) + abb[19] * (T(20) / T(3)) + z[26];
z[27] = abb[0] * z[27];
z[3] = z[3] + 2 * z[5] + z[11] + z[17] + z[20] + -z[22] + (T(2) / T(3)) * z[23] + (T(1) / T(6)) * z[24] + (T(1) / T(2)) * z[25] + z[27];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[2] = 2 * z[2];
z[5] = 3 * abb[18];
z[11] = 3 * abb[20];
z[17] = abb[24] + -z[2] + z[5] + z[11];
z[20] = abb[6] * z[17];
z[7] = z[7] + z[20];
z[20] = z[7] + z[25];
z[27] = 4 * z[10];
z[28] = -z[4] + z[27];
z[29] = 5 * abb[24];
z[30] = -z[6] + 2 * z[28] + -z[29];
z[31] = 2 * abb[1];
z[30] = z[30] * z[31];
z[32] = 2 * abb[24];
z[33] = abb[19] + abb[21];
z[34] = 2 * z[33];
z[35] = z[32] + -z[34];
z[36] = abb[18] + abb[23];
z[37] = z[35] + -z[36];
z[37] = abb[0] * z[37];
z[38] = 3 * abb[24];
z[39] = -z[28] + z[38];
z[40] = 2 * abb[3];
z[41] = z[39] * z[40];
z[42] = abb[20] + abb[22];
z[43] = z[35] + -z[42];
z[44] = abb[2] * z[43];
z[19] = z[19] + -z[38];
z[45] = z[19] + z[42];
z[46] = abb[4] * z[45];
z[19] = z[19] + z[36];
z[36] = abb[5] * z[19];
z[30] = z[20] + z[23] + z[30] + -z[36] + z[37] + -z[41] + z[44] + -z[46];
z[30] = abb[14] * z[30];
z[30] = 2 * z[30];
z[37] = abb[24] * (T(5) / T(2));
z[41] = -abb[20] + z[27];
z[44] = 2 * abb[22];
z[47] = -abb[23] + -z[5] + -z[37] + z[41] + z[44];
z[47] = abb[3] * z[47];
z[48] = 5 * abb[22];
z[49] = -z[13] + z[32] + -z[41] + z[48];
z[49] = abb[0] * z[49];
z[50] = z[7] + z[23];
z[51] = (T(3) / T(2)) * z[25];
z[52] = z[50] + z[51];
z[53] = -z[17] + -z[44];
z[53] = abb[4] * z[53];
z[54] = 3 * abb[22];
z[55] = -z[18] + z[54];
z[56] = -z[9] + z[55];
z[57] = -abb[20] + -abb[23] + -z[56];
z[57] = abb[2] * z[57];
z[58] = -z[31] * z[39];
z[22] = z[22] + (T(1) / T(2)) * z[24] + z[47] + z[49] + z[52] + z[53] + z[57] + z[58];
z[22] = abb[11] * z[22];
z[22] = z[22] + -z[30];
z[22] = abb[11] * z[22];
z[47] = 2 * abb[23];
z[49] = abb[24] + -z[34] + z[42] + -z[47];
z[49] = z[31] * z[49];
z[46] = 2 * z[46];
z[35] = 5 * abb[23] + -z[11] + -z[35];
z[35] = abb[5] * z[35];
z[53] = abb[23] + z[44];
z[57] = abb[20] + z[34];
z[58] = z[53] + -z[57];
z[58] = abb[2] * z[58];
z[43] = z[40] * z[43];
z[59] = 2 * abb[0];
z[60] = abb[24] + -z[9];
z[60] = z[59] * z[60];
z[61] = 2 * z[24];
z[35] = z[35] + z[43] + z[46] + z[49] + z[58] + z[60] + z[61];
z[35] = abb[14] * z[35];
z[36] = 2 * z[36];
z[43] = -z[36] + z[52];
z[49] = 2 * z[39];
z[58] = abb[22] + -abb[23];
z[60] = z[49] + z[58];
z[60] = abb[3] * z[60];
z[62] = 4 * abb[1];
z[63] = z[39] * z[62];
z[64] = z[12] + -z[18];
z[65] = abb[18] + -abb[19];
z[65] = z[64] + 2 * z[65];
z[65] = abb[2] * z[65];
z[66] = 3 * abb[19];
z[67] = z[1] + z[66];
z[68] = abb[24] * (T(9) / T(2));
z[69] = 2 * z[67] + -z[68];
z[70] = abb[22] + z[47];
z[71] = z[69] + z[70];
z[71] = abb[0] * z[71];
z[72] = 3 * z[24];
z[73] = 2 * abb[25];
z[74] = -abb[7] * z[73];
z[65] = -z[43] + z[60] + z[63] + z[65] + z[71] + -z[72] + z[74];
z[65] = abb[13] * z[65];
z[26] = 7 * abb[19] + z[26];
z[71] = 2 * z[26];
z[74] = -z[13] + z[71];
z[75] = 4 * abb[23];
z[76] = z[38] + z[42] + -z[74] + z[75];
z[76] = abb[1] * z[76];
z[77] = 13 * abb[19] + abb[23] * (T(-7) / T(2)) + abb[20] * (T(-3) / T(2)) + -z[13] + z[16] + -z[38];
z[77] = abb[5] * z[77];
z[0] = -abb[21] + abb[23] * (T(9) / T(2)) + -z[0] + -z[15];
z[0] = abb[2] * z[0];
z[15] = abb[21] + z[66];
z[66] = 2 * z[15];
z[78] = -z[18] + z[66];
z[79] = abb[18] + z[47] + -z[78];
z[79] = abb[3] * z[79];
z[78] = -z[70] + z[78];
z[78] = abb[0] * z[78];
z[80] = -abb[7] * abb[25];
z[0] = z[0] + (T(-3) / T(2)) * z[24] + z[76] + z[77] + z[78] + z[79] + z[80];
z[0] = abb[12] * z[0];
z[46] = z[46] + z[63];
z[44] = z[9] + -z[44] + -z[64];
z[44] = abb[2] * z[44];
z[63] = 9 * abb[24];
z[64] = z[6] + 8 * z[10] + -z[63];
z[64] = abb[3] * z[64];
z[56] = -z[47] + -z[56];
z[56] = abb[0] * z[56];
z[43] = z[43] + z[44] + -z[46] + z[56] + z[64];
z[43] = abb[11] * z[43];
z[0] = z[0] + z[35] + z[43] + z[65];
z[0] = abb[12] * z[0];
z[21] = z[21] * z[73];
z[35] = z[21] + z[72];
z[43] = z[53] + z[69];
z[43] = abb[2] * z[43];
z[44] = z[49] + -z[58];
z[44] = abb[3] * z[44];
z[49] = -abb[19] + abb[20];
z[49] = 2 * z[49];
z[53] = z[49] + z[55];
z[53] = abb[0] * z[53];
z[43] = z[35] + z[43] + z[44] + z[46] + -z[50] + z[51] + z[53];
z[43] = abb[11] * z[43];
z[44] = abb[0] + abb[2];
z[46] = -z[31] + z[44];
z[39] = z[39] * z[46];
z[46] = abb[8] * abb[25];
z[46] = z[25] + z[46];
z[4] = -z[4] + z[10];
z[4] = abb[3] * z[4];
z[4] = 4 * z[4] + z[39] + -3 * z[46];
z[4] = abb[13] * z[4];
z[4] = z[4] + z[30] + z[43];
z[4] = abb[13] * z[4];
z[10] = 9 * abb[19];
z[30] = z[10] + z[16];
z[30] = -6 * abb[24] + -z[13] + 2 * z[30] + -z[42] + -z[47];
z[30] = abb[3] * z[30];
z[39] = 15 * abb[19] + 7 * abb[21];
z[42] = abb[23] * (T(-3) / T(2)) + abb[20] * (T(9) / T(2)) + z[13] + z[29] + -z[39];
z[42] = abb[5] * z[42];
z[43] = 7 * abb[24];
z[10] = -4 * abb[21] + -z[10];
z[10] = abb[20] + z[5] + 2 * z[10] + z[43];
z[10] = abb[4] * z[10];
z[46] = 4 * abb[18];
z[51] = 17 * abb[19] + 9 * abb[21];
z[51] = -5 * abb[20] + -9 * abb[22] + -6 * abb[23] + -z[29] + -z[46] + 2 * z[51];
z[51] = abb[1] * z[51];
z[14] = abb[22] + abb[23] * (T(1) / T(2)) + -z[14] + -z[33];
z[14] = abb[2] * z[14];
z[33] = -abb[18] + -abb[24] + -z[1] + z[70];
z[33] = abb[0] * z[33];
z[10] = z[10] + z[14] + -z[24] + z[30] + z[33] + z[42] + z[51];
z[10] = prod_pow(abb[14], 2) * z[10];
z[14] = abb[22] + z[11] + -2 * z[39] + z[46] + z[63] + z[75];
z[14] = z[14] * z[31];
z[11] = z[11] + z[13];
z[13] = 4 * abb[24];
z[30] = z[11] + -z[12] + -z[13] + z[66];
z[30] = abb[5] * z[30];
z[23] = 2 * z[23];
z[20] = 2 * z[20] + z[23] + z[61];
z[33] = abb[22] + z[13];
z[11] = -z[11] + -z[33] + z[71];
z[11] = z[11] * z[40];
z[42] = abb[23] + z[13];
z[46] = -z[42] + 3 * z[57];
z[46] = abb[2] * z[46];
z[19] = 2 * z[19];
z[19] = abb[0] * z[19];
z[11] = z[11] + -z[14] + -z[19] + z[20] + -z[30] + -z[46];
z[14] = abb[27] * z[11];
z[17] = z[6] + z[17];
z[17] = abb[9] * z[17];
z[2] = abb[23] + -z[2] + z[68];
z[2] = abb[8] * z[2];
z[19] = z[40] + z[44];
z[19] = z[19] * z[73];
z[44] = 3 * abb[3] + -abb[6] + (T(3) / T(2)) * z[44];
z[44] = abb[26] * z[44];
z[46] = abb[10] * abb[25];
z[51] = abb[7] * z[58];
z[2] = -z[2] + -z[17] + z[19] + z[44] + -4 * z[46] + z[51];
z[17] = -abb[28] * z[2];
z[19] = -abb[2] * z[45];
z[26] = -abb[20] + z[26];
z[26] = -z[5] + 2 * z[26];
z[42] = z[26] + -z[42];
z[42] = abb[3] * z[42];
z[7] = z[7] + z[19] + -z[24] + z[42];
z[15] = -abb[20] + -z[15];
z[15] = -z[5] + z[13] + 2 * z[15] + z[54];
z[15] = abb[4] * z[15];
z[8] = -z[8] + z[39];
z[19] = 4 * abb[22];
z[39] = abb[23] + z[19];
z[5] = -z[5] + 2 * z[8] + -z[39] + -z[63];
z[5] = z[5] * z[31];
z[8] = -abb[18] + -z[34];
z[8] = 3 * z[8] + z[33];
z[8] = abb[0] * z[8];
z[5] = z[5] + 2 * z[7] + z[8] + z[15] + z[23];
z[5] = abb[15] * z[5];
z[0] = z[0] + z[3] + z[4] + z[5] + z[10] + z[14] + z[17] + z[22];
z[3] = z[13] + -z[39] + -z[57];
z[3] = abb[2] * z[3];
z[4] = -abb[22] + -z[9] + -z[12] + z[38];
z[4] = z[4] * z[59];
z[5] = abb[20] + 7 * abb[22] + -z[29] + -z[34] + z[75];
z[5] = z[5] * z[31];
z[7] = z[26] + -z[33];
z[8] = 2 * abb[4];
z[7] = z[7] * z[8];
z[8] = 11 * abb[19] + z[16];
z[8] = -6 * abb[18] + -9 * abb[20] + abb[23] + 2 * z[8] + -z[13];
z[8] = abb[5] * z[8];
z[10] = -z[57] + z[70];
z[10] = z[10] * z[40];
z[3] = z[3] + z[4] + z[5] + z[7] + z[8] + z[10] + z[20];
z[3] = abb[14] * z[3];
z[4] = abb[18] + z[41];
z[4] = 2 * z[4] + -z[19] + -z[43] + z[47];
z[4] = abb[3] * z[4];
z[5] = -z[6] + z[28] + -z[32];
z[5] = z[5] * z[62];
z[5] = z[5] + -z[36];
z[6] = abb[24] + z[19] + -z[27] + z[47];
z[6] = abb[2] * z[6];
z[8] = -abb[20] + z[66];
z[10] = abb[18] + -abb[23] + z[8] + -z[48];
z[10] = z[10] * z[59];
z[4] = z[4] + z[5] + z[6] + -z[7] + z[10] + -z[21] + -z[24] + -z[25];
z[4] = abb[11] * z[4];
z[6] = abb[20] + z[47];
z[1] = z[1] + z[6] + -z[18];
z[1] = abb[2] * z[1];
z[7] = -2 * z[8] + z[29] + z[58];
z[7] = abb[3] * z[7];
z[8] = abb[24] * (T(-7) / T(2)) + z[9] + z[47] + z[54];
z[8] = abb[0] * z[8];
z[6] = -abb[22] + z[6] + z[29] + -z[74];
z[6] = z[6] * z[31];
z[1] = z[1] + z[6] + z[7] + z[8] + z[30] + -z[52] + -z[61];
z[1] = abb[12] * z[1];
z[6] = abb[18] + -abb[20] + z[67];
z[6] = 2 * z[6] + -z[37] + z[47] + -z[54];
z[6] = abb[0] * z[6];
z[7] = -abb[23] + abb[24] * (T(1) / T(2)) + z[49];
z[7] = abb[2] * z[7];
z[5] = -z[5] + z[6] + z[7] + (T(-7) / T(2)) * z[25] + -z[35] + -z[50] + z[60];
z[5] = abb[13] * z[5];
z[1] = z[1] + z[3] + z[4] + z[5];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = abb[16] * z[11];
z[2] = -abb[17] * z[2];
z[1] = z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_322_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-8.879890113217632994777116848266417700521155805884253245216662634"),stof<T>("-28.574104586518136831103715252748351297130386128493667026449820845")}, std::complex<T>{stof<T>("-75.66763303240302585934466224692344987904571729748822494914055995"),stof<T>("27.015649059501431435697774692037262992504376232835195608871243648")}, std::complex<T>{stof<T>("-6.4065828851179997936467990618572374253378364780038385214648987632"),stof<T>("7.5664416278969528957900519244185216690277760096754935004137287372")}, std::complex<T>{stof<T>("-37.671024205524536262308608969406525097371025279937272745332614527"),stof<T>("4.00250915394797535709659329217123003202468561599260996181972812")}, std::complex<T>{stof<T>("65.367554724978659426136107678026866436233506933211636887596667063"),stof<T>("25.942200157832585605905997225371641560037370132754012623692473932")}, std::complex<T>{stof<T>("46.066835959083542291793487452977540656972848749397665674614437845"),stof<T>("23.043085985439189315451432101572075484109883639800328400739312198")}, std::complex<T>{stof<T>("-10.24014232666177153508050848697865104415082305061649292319449178"),stof<T>("-27.496636122076011210870770345325575204274509943285681553633468835")}, std::complex<T>{stof<T>("-7.550085787527186263938188022613937226932700122995932218562798149"),stof<T>("12.295809026369257714212863899525075084789830362879721057449720932")}, std::complex<T>{stof<T>("1.38170156813203563064654091171199458661310179487603999647250399902"),stof<T>("-0.06077927709401665162193682741330718472847426139315312127179326935")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[127].real()/kbase.W[127].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_322_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_322_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("6.722139767473038432095981129120439434026296111827271395967754276"),stof<T>("40.751872273298863623615755019349381825590601083084377415815115553")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dlog_W3(k,dl), dlog_W7(k,dl), dlog_W10(k,dl), dlog_W13(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), dlr[2], f_1_1(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[127].real()/k.W[127].real()), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_322_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_322_DLogXconstant_part(base_point<T>, kend);
	value += f_4_322_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_322_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_322_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_322_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_322_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_322_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_322_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
