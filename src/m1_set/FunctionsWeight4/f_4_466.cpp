/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_466.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_466_abbreviated (const std::array<T,70>& abb) {
T z[105];
z[0] = abb[56] * (T(1) / T(2));
z[1] = abb[57] * (T(1) / T(2));
z[2] = z[0] + -z[1];
z[3] = -abb[59] + z[2];
z[4] = abb[54] * (T(1) / T(2));
z[5] = abb[52] * (T(1) / T(2));
z[6] = -z[3] + -z[4] + -z[5];
z[6] = abb[1] * z[6];
z[7] = abb[10] * abb[58];
z[8] = abb[10] * abb[53];
z[7] = z[7] + z[8];
z[9] = abb[53] + abb[54];
z[10] = abb[56] + z[9];
z[11] = -abb[57] + z[10];
z[12] = abb[15] * z[11];
z[13] = (T(1) / T(8)) * z[12];
z[14] = abb[56] + abb[57] + z[9];
z[15] = abb[5] * z[14];
z[16] = (T(1) / T(4)) * z[15];
z[17] = (T(1) / T(2)) * z[14];
z[18] = abb[58] + z[17];
z[19] = abb[7] * z[18];
z[20] = abb[59] + abb[52] * (T(-1) / T(4)) + abb[57] * (T(3) / T(8)) + (T(-5) / T(8)) * z[10];
z[20] = abb[4] * z[20];
z[6] = z[6] + (T(-1) / T(2)) * z[7] + -z[13] + -z[16] + (T(1) / T(4)) * z[19] + z[20];
z[6] = abb[34] * z[6];
z[20] = (T(1) / T(2)) * z[9];
z[21] = z[0] + z[20];
z[22] = z[1] + z[21];
z[22] = abb[6] * z[22];
z[23] = -abb[59] + z[10];
z[24] = abb[2] * z[23];
z[22] = z[22] + -z[24];
z[25] = abb[55] + abb[59];
z[25] = abb[10] * z[25];
z[26] = abb[53] + abb[55];
z[27] = abb[59] + z[26];
z[27] = abb[14] * z[27];
z[28] = abb[57] + abb[59];
z[29] = abb[1] * z[28];
z[30] = z[22] + z[25] + -z[27] + z[29];
z[31] = abb[59] + abb[57] * (T(1) / T(4));
z[32] = abb[56] * (T(3) / T(4)) + -z[31];
z[33] = (T(3) / T(4)) * z[9] + z[32];
z[34] = abb[4] * z[33];
z[35] = abb[55] + z[23];
z[36] = abb[13] * z[35];
z[34] = -z[34] + (T(1) / T(2)) * z[36];
z[37] = -abb[59] + z[0];
z[4] = z[4] + z[37];
z[4] = abb[3] * z[4];
z[38] = (T(1) / T(4)) * z[10];
z[39] = abb[59] + abb[57] * (T(3) / T(4)) + -z[38];
z[40] = abb[7] * z[39];
z[4] = z[4] + z[16] + (T(-1) / T(2)) * z[30] + -z[34] + -z[40];
z[4] = abb[32] * z[4];
z[30] = abb[2] + abb[10];
z[30] = abb[58] * z[30];
z[41] = abb[2] * abb[59];
z[8] = z[8] + z[30] + z[41];
z[30] = -z[8] + z[25];
z[31] = z[31] + -z[38];
z[31] = abb[3] * z[31];
z[30] = (T(1) / T(2)) * z[30] + z[31] + z[34];
z[30] = abb[31] * z[30];
z[31] = abb[6] * z[10];
z[34] = abb[6] * abb[57];
z[34] = z[31] + z[34];
z[38] = abb[52] + z[17];
z[42] = abb[4] * z[38];
z[43] = z[34] + -z[42];
z[44] = z[20] + z[37];
z[45] = abb[58] * (T(1) / T(2));
z[46] = -z[44] + z[45];
z[46] = abb[2] * z[46];
z[47] = abb[59] + abb[58] * (T(1) / T(4)) + abb[57] * (T(5) / T(8)) + (T(-3) / T(8)) * z[10];
z[47] = abb[7] * z[47];
z[13] = z[13] + (T(1) / T(4)) * z[43] + z[46] + z[47];
z[13] = abb[33] * z[13];
z[43] = abb[54] + abb[56];
z[46] = -abb[59] + z[43];
z[47] = abb[52] + z[46];
z[47] = abb[1] * z[47];
z[48] = -z[27] + z[47];
z[49] = abb[53] + 3 * abb[54];
z[32] = -z[32] + (T(-1) / T(4)) * z[49];
z[32] = abb[3] * z[32];
z[50] = (T(1) / T(4)) * z[12];
z[32] = z[32] + z[40] + (T(1) / T(2)) * z[48] + z[50];
z[32] = abb[35] * z[32];
z[33] = z[5] + z[33];
z[33] = abb[4] * z[33];
z[33] = z[7] + z[33];
z[39] = z[39] + z[45];
z[39] = abb[7] * z[39];
z[39] = z[39] + z[50];
z[20] = z[2] + z[20];
z[40] = -abb[59] + z[20];
z[48] = abb[3] * z[40];
z[51] = z[33] + -z[39] + z[48];
z[51] = abb[30] * z[51];
z[52] = -abb[33] + abb[34];
z[53] = -abb[53] + z[43];
z[54] = -abb[57] + z[53];
z[55] = z[52] * z[54];
z[56] = 3 * abb[53] + -abb[54];
z[2] = abb[55] + -z[2] + (T(1) / T(2)) * z[56];
z[56] = -abb[31] + abb[35];
z[57] = z[2] * z[56];
z[55] = z[55] + z[57];
z[55] = abb[17] * z[55];
z[4] = z[4] + z[6] + z[13] + z[30] + z[32] + z[51] + (T(1) / T(2)) * z[55];
z[4] = m1_set::bc<T>[0] * z[4];
z[6] = abb[3] * (T(1) / T(2));
z[13] = abb[57] + z[53];
z[30] = z[6] * z[13];
z[32] = abb[8] * z[40];
z[51] = z[25] + z[32];
z[53] = abb[5] * z[17];
z[53] = -z[29] + z[53];
z[55] = abb[7] * z[17];
z[30] = z[27] + z[30] + z[51] + z[53] + -z[55];
z[55] = abb[41] * z[30];
z[57] = abb[4] * z[40];
z[58] = z[48] + z[57];
z[59] = abb[7] * z[40];
z[51] = z[51] + -z[58] + -z[59];
z[60] = abb[42] * (T(1) / T(2));
z[61] = z[51] * z[60];
z[62] = -abb[56] + abb[57];
z[9] = z[9] + -z[62];
z[9] = abb[20] * z[9];
z[38] = abb[19] * z[38];
z[9] = (T(1) / T(2)) * z[9] + -z[38];
z[38] = -abb[43] * z[9];
z[63] = -abb[53] + abb[59];
z[64] = abb[47] * z[63];
z[62] = -abb[54] + abb[59] + z[62];
z[62] = abb[46] * z[62];
z[38] = z[38] + z[62] + z[64];
z[62] = z[7] + (T(1) / T(2)) * z[32];
z[19] = (T(1) / T(2)) * z[19];
z[64] = -z[19] + z[25] + z[62];
z[65] = abb[39] * z[64];
z[4] = z[4] + (T(1) / T(4)) * z[38] + (T(1) / T(2)) * z[55] + z[61] + z[65];
z[38] = abb[30] * (T(1) / T(2));
z[55] = abb[24] + abb[26];
z[61] = abb[22] + z[55];
z[65] = abb[27] + z[61];
z[66] = z[38] * z[65];
z[67] = abb[35] * (T(1) / T(2));
z[68] = -abb[28] + z[61];
z[69] = z[67] * z[68];
z[70] = abb[32] * (T(1) / T(2));
z[71] = z[55] * z[70];
z[72] = abb[22] + -abb[27] + 3 * z[55];
z[72] = -abb[28] + (T(1) / T(4)) * z[72];
z[72] = abb[33] * z[72];
z[73] = -abb[27] + z[61];
z[74] = -abb[28] + (T(1) / T(4)) * z[73];
z[74] = abb[34] * z[74];
z[75] = abb[27] + abb[28];
z[76] = abb[31] * z[75];
z[66] = -z[66] + z[69] + -z[71] + z[72] + -z[74] + (T(1) / T(2)) * z[76];
z[66] = m1_set::bc<T>[0] * z[66];
z[69] = abb[23] + abb[25];
z[71] = z[69] + z[75];
z[72] = abb[39] * z[71];
z[74] = z[68] + z[69];
z[75] = abb[38] * z[74];
z[77] = abb[42] * z[69];
z[78] = abb[28] + z[69];
z[79] = abb[41] * z[78];
z[80] = abb[29] * (T(1) / T(2));
z[81] = abb[43] * z[80];
z[72] = z[72] + z[75] + z[77] + z[79] + z[81];
z[66] = -z[66] + (T(1) / T(2)) * z[72];
z[72] = abb[48] * z[66];
z[6] = z[6] * z[11];
z[75] = (T(1) / T(2)) * z[34];
z[77] = -z[24] + z[75];
z[17] = abb[4] * z[17];
z[6] = z[6] + -z[17] + -z[32] + z[36] + z[77];
z[17] = abb[55] + z[20];
z[20] = abb[17] * z[17];
z[79] = -abb[28] + z[55];
z[81] = z[69] + z[79];
z[82] = (T(1) / T(2)) * z[81];
z[83] = abb[48] * z[82];
z[84] = -abb[57] + z[35];
z[85] = -abb[9] * z[84];
z[83] = -z[6] + z[20] + z[83] + z[85];
z[83] = abb[40] * z[83];
z[85] = (T(1) / T(2)) * z[12];
z[86] = -z[32] + -z[42] + z[85];
z[20] = z[20] + -z[86];
z[20] = abb[38] * z[20];
z[20] = z[20] + z[72] + z[83];
z[72] = abb[40] * z[82];
z[66] = z[66] + z[72];
z[72] = abb[49] + -abb[50] + abb[51];
z[82] = (T(-1) / T(8)) * z[72];
z[66] = z[66] * z[82];
z[82] = abb[34] * (T(1) / T(2));
z[83] = abb[33] * (T(1) / T(2));
z[87] = z[82] + -z[83];
z[88] = abb[52] + abb[58] + z[14];
z[88] = -z[87] * z[88];
z[89] = abb[52] + z[23];
z[90] = abb[35] * z[89];
z[91] = abb[58] + z[28];
z[92] = abb[31] * z[91];
z[88] = z[88] + z[90] + -z[92];
z[45] = -z[5] + -z[40] + z[45];
z[93] = abb[30] * z[45];
z[94] = (T(-1) / T(2)) * z[88] + -z[93];
z[94] = m1_set::bc<T>[0] * z[94];
z[95] = -abb[39] * z[91];
z[96] = abb[38] * z[89];
z[95] = z[95] + z[96];
z[94] = z[94] + (T(1) / T(2)) * z[95];
z[95] = abb[0] * (T(1) / T(4));
z[94] = z[94] * z[95];
z[96] = abb[17] * (T(1) / T(8));
z[97] = -abb[39] + -abb[41];
z[97] = z[17] * z[96] * z[97];
z[98] = abb[32] * z[84];
z[99] = abb[52] + abb[57];
z[100] = abb[33] * z[99];
z[98] = z[98] + z[100];
z[5] = abb[57] + z[5] + (T(-1) / T(2)) * z[35];
z[5] = abb[35] * z[5];
z[100] = -abb[30] * z[99];
z[5] = z[5] + (T(1) / T(2)) * z[98] + z[100];
z[5] = m1_set::bc<T>[0] * z[5];
z[60] = -z[60] * z[84];
z[100] = abb[52] + z[35];
z[101] = -abb[38] * z[100];
z[5] = z[5] + z[60] + z[101];
z[60] = abb[9] * (T(1) / T(4));
z[5] = z[5] * z[60];
z[101] = abb[31] + z[87];
z[102] = abb[30] + -z[101];
z[102] = m1_set::bc<T>[0] * z[102];
z[102] = abb[39] + z[102];
z[103] = abb[16] * (T(1) / T(16));
z[102] = z[13] * z[102] * z[103];
z[104] = abb[21] * z[13];
z[18] = abb[18] * z[18];
z[18] = (T(1) / T(16)) * z[18] + (T(-1) / T(32)) * z[104];
z[104] = -abb[43] * z[18];
z[4] = abb[66] + abb[67] + (T(1) / T(4)) * z[4] + z[5] + (T(1) / T(8)) * z[20] + z[66] + z[94] + z[97] + z[102] + z[104];
z[5] = abb[53] + -abb[54];
z[5] = (T(1) / T(2)) * z[5];
z[20] = z[1] + -z[5] + z[37];
z[20] = abb[3] * z[20];
z[37] = abb[7] * z[28];
z[37] = -z[27] + z[37];
z[23] = abb[4] * z[23];
z[23] = z[23] + -z[36];
z[66] = -abb[6] * z[1];
z[20] = z[20] + z[23] + z[24] + (T(-1) / T(2)) * z[31] + z[32] + -z[37] + z[53] + z[66];
z[20] = z[20] * z[70];
z[31] = abb[3] * abb[59];
z[23] = -z[23] + z[25] + z[31];
z[31] = abb[31] * z[23];
z[66] = -z[59] + z[77];
z[77] = abb[33] * z[66];
z[20] = z[20] + z[31] + z[77];
z[20] = abb[32] * z[20];
z[8] = z[8] + z[58];
z[8] = abb[31] * z[8];
z[31] = -abb[32] * z[51];
z[19] = -z[19] + z[47] + z[50];
z[33] = -z[19] + -z[33];
z[33] = abb[34] * z[33];
z[47] = -z[47] + z[48] + z[59] + -z[85];
z[51] = -abb[35] * z[47];
z[58] = abb[4] * z[89];
z[77] = abb[7] * z[91];
z[58] = z[48] + z[58] + -z[77];
z[77] = (T(1) / T(2)) * z[25];
z[50] = -z[50] + (T(1) / T(2)) * z[58] + z[62] + z[77];
z[50] = abb[30] * z[50];
z[58] = abb[2] * abb[58];
z[41] = -z[41] + (T(1) / T(2)) * z[42] + -z[58];
z[39] = z[39] + -z[41];
z[39] = abb[33] * z[39];
z[31] = -z[8] + z[31] + z[33] + z[39] + z[50] + z[51];
z[31] = abb[30] * z[31];
z[6] = abb[62] * z[6];
z[19] = z[19] + z[41] + -z[48];
z[19] = abb[33] * z[19];
z[33] = z[53] + z[57];
z[39] = -abb[32] * z[33];
z[8] = z[8] + z[19] + z[39];
z[8] = abb[34] * z[8];
z[19] = abb[33] * z[47];
z[39] = abb[34] * z[33];
z[41] = abb[3] * z[46];
z[37] = -z[37] + z[41];
z[33] = -z[33] + z[37];
z[33] = z[33] * z[67];
z[37] = -abb[32] * z[37];
z[19] = z[19] + z[33] + z[37] + z[39];
z[19] = abb[35] * z[19];
z[30] = abb[63] * z[30];
z[33] = abb[64] * z[40];
z[3] = -z[3] + z[5];
z[3] = abb[36] * z[3];
z[3] = z[3] + z[33];
z[3] = abb[4] * z[3];
z[37] = abb[36] * abb[58];
z[11] = abb[36] * z[11];
z[11] = (T(-1) / T(2)) * z[11] + z[33] + -z[37];
z[11] = abb[7] * z[11];
z[39] = abb[59] + z[1];
z[40] = abb[56] * (T(-3) / T(2)) + z[39] + (T(-1) / T(2)) * z[49];
z[40] = abb[36] * z[40];
z[33] = z[33] + z[40];
z[33] = abb[3] * z[33];
z[40] = abb[31] * abb[33];
z[41] = z[40] * z[66];
z[42] = abb[60] * z[86];
z[46] = -abb[59] + -z[10];
z[46] = abb[2] * z[46];
z[46] = z[46] + z[75];
z[46] = abb[36] * z[46];
z[47] = abb[64] * z[32];
z[48] = -abb[69] * z[63];
z[25] = -abb[64] * z[25];
z[3] = -z[3] + -z[6] + -z[8] + -z[11] + -z[19] + -z[20] + -z[25] + z[30] + -z[31] + -z[33] + z[41] + -z[42] + -z[46] + z[47] + (T(-1) / T(2)) * z[48];
z[6] = -abb[28] + (T(1) / T(2)) * z[73];
z[6] = z[6] * z[52];
z[8] = z[65] + z[69];
z[8] = z[8] * z[38];
z[11] = abb[32] * z[69];
z[19] = abb[35] * z[68];
z[6] = z[6] + z[8] + -z[11] + -z[19] + -z[76];
z[6] = abb[30] * z[6];
z[8] = z[65] * z[83];
z[11] = abb[28] * abb[32];
z[8] = z[8] + z[11] + -z[76];
z[8] = abb[34] * z[8];
z[11] = z[55] + z[69];
z[11] = z[11] * z[70];
z[19] = abb[33] * z[79];
z[11] = z[11] + -z[19];
z[11] = abb[32] * z[11];
z[20] = abb[34] + -z[67];
z[20] = abb[28] * z[20];
z[25] = abb[33] * z[68];
z[20] = z[20] + z[25];
z[20] = abb[35] * z[20];
z[25] = abb[61] * z[71];
z[30] = abb[60] * z[74];
z[31] = abb[62] * z[81];
z[33] = prod_pow(abb[31], 2);
z[41] = z[33] * z[79];
z[42] = -abb[27] + -z[55];
z[42] = abb[36] * z[42];
z[46] = abb[64] * z[69];
z[47] = abb[37] * z[61];
z[48] = abb[63] * z[78];
z[19] = abb[31] * z[19];
z[49] = abb[65] * z[80];
z[6] = z[6] + -z[8] + z[11] + z[19] + z[20] + -z[25] + -z[30] + -z[31] + (T(-1) / T(2)) * z[41] + z[42] + -z[46] + -z[47] + -z[48] + -z[49];
z[8] = abb[22] + abb[27];
z[8] = (T(1) / T(12)) * z[8] + (T(1) / T(8)) * z[55] + (T(1) / T(3)) * z[69];
z[11] = prod_pow(m1_set::bc<T>[0], 2);
z[8] = z[8] * z[11];
z[6] = (T(1) / T(4)) * z[6] + -z[8];
z[8] = -abb[48] + z[72];
z[8] = (T(-1) / T(4)) * z[8];
z[6] = z[6] * z[8];
z[8] = -z[88] + -z[93];
z[8] = abb[30] * z[8];
z[19] = abb[33] * z[45];
z[19] = z[19] + -z[92];
z[19] = abb[34] * z[19];
z[20] = -abb[37] + -abb[60];
z[20] = z[20] * z[89];
z[25] = abb[36] * z[28];
z[30] = abb[61] * z[91];
z[31] = abb[33] * z[90];
z[8] = z[8] + z[19] + z[20] + z[25] + z[30] + z[31] + z[37];
z[19] = (T(1) / T(3)) * z[11];
z[20] = z[19] * z[45];
z[8] = (T(1) / T(2)) * z[8] + z[20];
z[8] = z[8] * z[95];
z[20] = -abb[31] + abb[32];
z[20] = z[20] * z[82];
z[25] = z[52] + -z[56];
z[25] = z[25] * z[38];
z[20] = z[20] + z[25];
z[20] = z[20] * z[54];
z[25] = z[2] * z[33];
z[30] = z[40] * z[54];
z[25] = z[25] + z[30];
z[30] = -z[54] * z[87];
z[2] = -z[2] * z[67];
z[31] = abb[32] * z[26];
z[2] = z[2] + z[30] + z[31];
z[2] = abb[35] * z[2];
z[30] = -abb[60] + abb[61] + -abb[62] + abb[63];
z[17] = z[17] * z[30];
z[30] = -z[54] * z[83];
z[26] = -abb[31] * z[26];
z[26] = z[26] + z[30];
z[26] = abb[32] * z[26];
z[2] = z[2] + z[17] + z[20] + (T(1) / T(2)) * z[25] + z[26];
z[2] = z[2] * z[96];
z[7] = (T(-1) / T(3)) * z[7] + (T(-1) / T(12)) * z[24] + (T(-1) / T(6)) * z[27] + (T(1) / T(24)) * z[34] + -z[77];
z[17] = abb[57] * (T(-5) / T(6)) + abb[59] * (T(-1) / T(3)) + -z[21];
z[20] = abb[52] * (T(1) / T(3));
z[17] = (T(1) / T(2)) * z[17] + -z[20];
z[17] = abb[4] * z[17];
z[10] = -abb[59] + (T(5) / T(2)) * z[10];
z[10] = z[1] + (T(1) / T(3)) * z[10];
z[10] = abb[58] * (T(1) / T(3)) + (T(1) / T(2)) * z[10];
z[10] = abb[7] * z[10];
z[10] = z[10] + z[17];
z[17] = -abb[53] + abb[57];
z[17] = abb[3] * z[17];
z[17] = -z[17] + z[36];
z[12] = z[12] + z[29];
z[7] = (T(1) / T(2)) * z[7] + (T(1) / T(4)) * z[10] + (T(1) / T(24)) * z[12] + (T(-1) / T(48)) * z[15] + (T(1) / T(12)) * z[17] + (T(-1) / T(3)) * z[32];
z[7] = z[7] * z[11];
z[0] = z[0] + -z[5] + z[39];
z[0] = abb[3] * z[0];
z[5] = -z[1] + -z[44];
z[5] = abb[7] * z[5];
z[10] = abb[52] + (T(1) / T(2)) * z[13];
z[10] = abb[4] * z[10];
z[0] = z[0] + z[5] + z[10] + z[85];
z[1] = abb[52] + abb[59] * (T(-1) / T(2)) + z[1] + z[43];
z[1] = abb[1] * z[1];
z[0] = (T(1) / T(2)) * z[0] + z[1] + -z[16];
z[0] = abb[37] * z[0];
z[1] = abb[61] * z[64];
z[5] = abb[2] * z[37];
z[0] = -z[0] + z[1] + z[5];
z[1] = z[22] + -z[23] + -z[59];
z[1] = z[1] * z[33];
z[5] = -z[28] + z[43];
z[5] = abb[68] * z[5];
z[1] = z[1] + z[5];
z[5] = z[67] * z[84];
z[5] = z[5] + -z[98];
z[5] = abb[35] * z[5];
z[10] = abb[62] + abb[64];
z[10] = z[10] * z[84];
z[5] = z[5] + z[10];
z[10] = abb[35] * z[99];
z[12] = -abb[55] + abb[59] + -z[14];
z[12] = -abb[52] + (T(1) / T(2)) * z[12];
z[12] = abb[30] * z[12];
z[10] = z[10] + z[12] + z[98];
z[10] = z[10] * z[38];
z[12] = abb[57] * (T(-1) / T(3)) + z[35];
z[12] = (T(1) / T(2)) * z[12] + z[20];
z[11] = z[11] * z[12];
z[12] = abb[60] * z[100];
z[5] = (T(1) / T(2)) * z[5] + z[10] + z[11] + z[12];
z[5] = z[5] * z[60];
z[9] = (T(1) / T(16)) * z[9] + z[18];
z[9] = abb[65] * z[9];
z[10] = z[38] + -z[101];
z[10] = abb[30] * z[10];
z[11] = -abb[31] + z[83];
z[11] = -abb[34] * z[11];
z[10] = -abb[36] + -abb[61] + z[10] + z[11] + -z[19];
z[10] = z[10] * z[13] * z[103];
z[0] = abb[44] + abb[45] + (T(-1) / T(4)) * z[0] + (T(1) / T(16)) * z[1] + z[2] + (T(-1) / T(8)) * z[3] + z[5] + z[6] + (T(1) / T(2)) * z[7] + z[8] + z[9] + z[10];
return {z[4], z[0]};
}


template <typename T> std::complex<T> f_4_466_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("0.1680781571205259684783209532609898001018838699352932802170601115")}, std::complex<T>{stof<T>("0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("-0.1680781571205259684783209532609898001018838699352932802170601115")}, std::complex<T>{stof<T>("-0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("0.1680781571205259684783209532609898001018838699352932802170601115")}, std::complex<T>{stof<T>("0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("-0.1680781571205259684783209532609898001018838699352932802170601115")}, std::complex<T>{stof<T>("0.0883298241095633926946418692511844408412068741973559873146708585"),stof<T>("0.44009601786745975038172927061295343418807866948773457276764121692")}, std::complex<T>{stof<T>("0.0417966127428765050314821214938676836467955745016790176314509502"),stof<T>("0.27148044900177878644403449703850351511026247680080638748131402086")}, std::complex<T>{stof<T>("0.15258225933979579846437091687265167261413254825078213605058390699"),stof<T>("0.56468626539569294860296510654471602956976011454291979071394015152")}, std::complex<T>{stof<T>("0.15258225933979579846437091687265167261413254825078213605058390699"),stof<T>("0.56468626539569294860296510654471602956976011454291979071394015152")}, std::complex<T>{stof<T>("-0.0417966127428765050314821214938676836467955745016790176314509502"),stof<T>("-0.27148044900177878644403449703850351511026247680080638748131402086")}, std::complex<T>{stof<T>("-0.0883298241095633926946418692511844408412068741973559873146708585"),stof<T>("-0.44009601786745975038172927061295343418807866948773457276764121692")}, std::complex<T>{stof<T>("-0.30516451867959159692874183374530334522826509650156427210116781398"),stof<T>("-1.12937253079138589720593021308943205913952022908583958142788030304")}};
	
	std::vector<C> intdlogs = {rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_466_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_466_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(64)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(16)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[52] + abb[54] + abb[56]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[52] + abb[54] + abb[56];
z[1] = abb[34] + -abb[35];
return abb[11] * m1_set::bc<T>[0] * (T(1) / T(8)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_466_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(64)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (-8 + -7 * v[0] + -v[1] + -v[2] + 3 * v[3] + 4 * v[4] + m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + v[5])) / prod_pow(tend, 2);
c[1] = ((-2 + m1_set::bc<T>[1]) * (T(-1) / T(16)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[52] + abb[54] + abb[56]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[34] + abb[35];
z[0] = (T(1) / T(2)) * z[0];
z[1] = -abb[30] + abb[33];
z[0] = z[0] * z[1];
z[0] = -abb[37] + z[0];
z[1] = abb[52] + abb[54] + abb[56];
return abb[11] * (T(1) / T(4)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_466_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(64)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(-1) / T(16)) * (v[2] + v[3])) / tend;


		return (abb[54] + abb[56] + abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[54] + abb[56] + abb[58];
z[1] = abb[31] + -abb[33];
return abb[12] * m1_set::bc<T>[0] * (T(1) / T(8)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_466_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(64)) * (v[2] + v[3]) * (-8 + -4 * v[0] + -2 * v[1] + v[2] + 3 * v[3] + 2 * v[4] + -m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -4 * v[5])) / prod_pow(tend, 2);
c[1] = ((-2 + m1_set::bc<T>[1]) * (T(1) / T(16)) * (v[2] + v[3])) / tend;


		return (abb[54] + abb[56] + abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[31] + -abb[33];
z[1] = -abb[30] + abb[34];
z[0] = z[0] * z[1];
z[0] = -abb[36] + (T(1) / T(2)) * z[0];
z[1] = -abb[54] + -abb[56] + -abb[58];
return abb[12] * (T(1) / T(4)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_466_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = T(0);
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,70> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W17(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, T{0}, rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), T{0}, T{0}, T{0}, T{0}};
abb[44] = SpDLog_f_4_466_W_17_Im(t, path, abb);
abb[45] = SpDLog_f_4_466_W_19_Im(t, path, abb);
abb[46] = SpDLogQ_W_82(k,dl,dlr).imag();
abb[47] = SpDLogQ_W_90(k,dl,dlr).imag();
abb[66] = SpDLog_f_4_466_W_17_Re(t, path, abb);
abb[67] = SpDLog_f_4_466_W_19_Re(t, path, abb);
abb[68] = SpDLogQ_W_82(k,dl,dlr).real();
abb[69] = SpDLogQ_W_90(k,dl,dlr).real();

                    
            return f_4_466_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_466_DLogXconstant_part(base_point<T>, kend);
	value += f_4_466_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_466_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_466_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_466_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_466_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_466_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_466_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
