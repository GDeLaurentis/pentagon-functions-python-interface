/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_687.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_687_abbreviated (const std::array<T,36>& abb) {
T z[121];
z[0] = abb[25] + -abb[32];
z[1] = 5 * abb[26];
z[2] = 4 * abb[27];
z[3] = -abb[33] + z[2];
z[4] = 4 * abb[31];
z[5] = -z[0] + z[1] + z[3] + -z[4];
z[6] = 2 * abb[7];
z[5] = z[5] * z[6];
z[7] = 5 * abb[30];
z[8] = 3 * abb[32];
z[9] = z[7] + z[8];
z[10] = 2 * abb[28];
z[11] = 3 * abb[25];
z[12] = z[10] + z[11];
z[13] = 12 * abb[26];
z[14] = z[12] + z[13];
z[15] = 6 * abb[27];
z[16] = -abb[33] + z[15];
z[17] = z[4] + z[9] + -z[14] + -z[16];
z[17] = abb[9] * z[17];
z[18] = 2 * abb[26];
z[19] = abb[28] + z[0];
z[20] = 2 * abb[31];
z[21] = 2 * abb[27];
z[22] = z[18] + z[19] + -z[20] + z[21];
z[22] = abb[4] * z[22];
z[23] = abb[25] + -abb[28];
z[24] = 4 * abb[30];
z[25] = z[23] + -z[24];
z[26] = z[4] + -z[8];
z[27] = 6 * abb[26];
z[28] = z[25] + z[26] + z[27];
z[29] = 2 * abb[2];
z[30] = z[28] * z[29];
z[31] = 3 * abb[30];
z[32] = -abb[33] + z[31];
z[33] = abb[28] + z[21];
z[34] = -z[20] + z[32] + z[33];
z[34] = abb[0] * z[34];
z[35] = abb[28] + -abb[32];
z[36] = abb[30] + z[35];
z[37] = abb[8] * z[36];
z[38] = 2 * z[37];
z[39] = abb[34] + -abb[35];
z[40] = 2 * z[39];
z[41] = abb[13] * z[40];
z[42] = z[38] + -z[41];
z[43] = abb[11] * z[39];
z[22] = z[5] + z[17] + z[22] + z[30] + z[34] + -z[42] + z[43];
z[22] = abb[16] * z[22];
z[17] = z[17] + z[41];
z[34] = z[3] + -z[20];
z[44] = z[8] + -z[27];
z[45] = abb[30] + -z[12] + -z[34] + z[44];
z[45] = abb[0] * z[45];
z[46] = 5 * abb[32];
z[47] = z[10] + z[46];
z[48] = 17 * abb[26];
z[49] = 2 * abb[33];
z[50] = 2 * abb[25];
z[51] = -8 * abb[27] + -abb[31] + z[47] + -z[48] + z[49] + z[50];
z[51] = abb[7] * z[51];
z[52] = abb[2] * z[28];
z[53] = z[1] + -z[11];
z[54] = 2 * z[35];
z[55] = z[53] + z[54];
z[55] = abb[4] * z[55];
z[56] = abb[12] * z[39];
z[45] = -z[17] + -3 * z[37] + z[45] + z[51] + -z[52] + z[55] + z[56];
z[45] = abb[17] * z[45];
z[22] = z[22] + z[45];
z[22] = abb[17] * z[22];
z[45] = -abb[28] + z[2];
z[51] = 2 * abb[32];
z[55] = -z[45] + z[51];
z[57] = 10 * abb[30];
z[58] = 16 * abb[25];
z[59] = 2 * abb[29];
z[55] = abb[31] + -z[13] + 3 * z[55] + z[57] + -z[58] + z[59];
z[55] = abb[0] * z[55];
z[60] = 4 * abb[25];
z[61] = 3 * abb[28];
z[62] = z[60] + z[61];
z[63] = z[15] + z[27];
z[64] = 9 * abb[31];
z[65] = abb[29] + z[64];
z[66] = -abb[32] + z[7] + -z[62] + -z[63] + z[65];
z[67] = 2 * abb[9];
z[68] = -z[66] * z[67];
z[69] = 5 * abb[25];
z[70] = z[7] + -z[10] + z[20] + -z[69];
z[71] = 2 * abb[3];
z[70] = z[70] * z[71];
z[72] = z[18] + z[33];
z[73] = -z[0] + z[72];
z[74] = 6 * abb[31];
z[75] = -z[59] + -z[73] + z[74];
z[75] = abb[4] * z[75];
z[76] = 8 * abb[31];
z[77] = z[0] + z[76];
z[78] = 10 * abb[26];
z[79] = 10 * abb[27] + z[78];
z[80] = abb[28] + -4 * abb[29] + -z[77] + z[79];
z[81] = -abb[7] * z[80];
z[82] = abb[1] * z[19];
z[83] = abb[12] * z[40];
z[55] = -z[41] + z[55] + z[68] + z[70] + z[75] + z[81] + -9 * z[82] + z[83];
z[55] = abb[20] * z[55];
z[68] = 7 * abb[26];
z[70] = 3 * abb[31];
z[75] = -abb[29] + z[2] + -z[11] + z[68] + -z[70];
z[75] = abb[4] * z[75];
z[81] = z[18] + -z[21];
z[84] = abb[25] + abb[28];
z[85] = z[26] + z[59] + z[81] + -z[84];
z[85] = abb[0] * z[85];
z[86] = 8 * abb[30];
z[87] = z[50] + -z[86];
z[88] = 5 * abb[31];
z[89] = -abb[29] + abb[32] + z[13] + z[16] + z[87] + -z[88];
z[89] = z[67] * z[89];
z[90] = z[11] + z[21];
z[91] = -z[59] + z[61];
z[92] = 11 * abb[32];
z[93] = -28 * abb[26] + 16 * abb[30] + -12 * abb[31] + z[49] + -z[90] + z[91] + z[92];
z[93] = abb[2] * z[93];
z[94] = -27 * abb[26] + -22 * abb[27] + 5 * abb[29] + 15 * abb[31] + z[35] + z[49] + z[60];
z[94] = abb[7] * z[94];
z[40] = abb[11] * z[40];
z[95] = z[40] + -z[41];
z[75] = z[75] + z[83] + z[85] + z[89] + z[93] + z[94] + z[95];
z[75] = abb[22] * z[75];
z[66] = -abb[9] * z[66];
z[85] = -abb[28] + z[0];
z[89] = 3 * abb[27];
z[93] = z[85] + -z[89];
z[65] = -z[27] + z[65] + 2 * z[93];
z[65] = abb[4] * z[65];
z[93] = abb[29] + z[10];
z[9] = -8 * abb[25] + z[9] + -z[63] + z[93];
z[9] = abb[0] * z[9];
z[94] = abb[13] * z[39];
z[9] = z[9] + 5 * z[37] + -z[43] + z[65] + z[66] + -z[94];
z[9] = abb[15] * z[9];
z[65] = 5 * abb[28] + -z[8];
z[66] = 2 * abb[30];
z[76] = z[63] + z[65] + -z[66] + z[69] + -z[76];
z[76] = abb[9] * z[76];
z[96] = 7 * abb[25];
z[97] = -z[66] + z[96];
z[98] = -abb[28] + -z[46] + z[63] + z[97];
z[98] = abb[0] * z[98];
z[99] = abb[28] + z[63];
z[77] = z[77] + -z[99];
z[77] = abb[4] * z[77];
z[42] = z[40] + -z[42] + -z[76] + -z[77] + z[98];
z[76] = -abb[16] + abb[17];
z[77] = z[42] * z[76];
z[9] = z[9] + z[77];
z[9] = abb[15] * z[9];
z[77] = -abb[33] + z[4];
z[98] = abb[28] + abb[30];
z[98] = -z[27] + z[50] + -z[77] + 3 * z[98];
z[98] = abb[13] * z[98];
z[100] = 3 * abb[26];
z[101] = -abb[33] + z[100];
z[102] = -abb[28] + z[51];
z[103] = z[20] + z[101] + -z[102];
z[103] = abb[11] * z[103];
z[104] = -abb[0] + abb[4];
z[39] = z[39] * z[104];
z[104] = abb[30] + -abb[32];
z[77] = abb[25] + z[77] + 3 * z[104];
z[77] = abb[12] * z[77];
z[39] = z[39] + z[77] + z[98] + 2 * z[103];
z[39] = abb[24] * z[39];
z[44] = -z[11] + z[24] + -z[33] + z[44];
z[77] = abb[0] * z[44];
z[98] = z[21] + -z[84];
z[103] = -z[26] + z[98];
z[105] = abb[4] * z[103];
z[52] = z[52] + z[77] + z[105];
z[77] = prod_pow(abb[16], 2);
z[105] = -z[52] * z[77];
z[9] = z[9] + z[22] + z[39] + z[55] + z[75] + z[105];
z[22] = z[20] + z[59];
z[39] = 8 * abb[26];
z[45] = -z[8] + z[22] + -z[39] + -z[45] + z[49] + z[97];
z[45] = abb[0] * z[45];
z[55] = z[59] + z[74];
z[75] = 7 * abb[32];
z[25] = -14 * abb[26] + -z[21] + -z[25] + z[49] + -z[55] + z[75];
z[25] = abb[9] * z[25];
z[105] = 15 * abb[26];
z[16] = abb[31] + z[16] + -z[46] + -z[66] + -z[84] + z[105];
z[106] = z[16] * z[71];
z[107] = z[2] + -z[20];
z[108] = abb[25] + abb[29];
z[109] = -abb[32] + z[27] + z[107] + -z[108];
z[109] = z[6] * z[109];
z[110] = 4 * abb[26];
z[111] = -abb[32] + z[110];
z[98] = z[98] + z[111];
z[98] = abb[4] * z[98];
z[95] = z[95] + z[98];
z[98] = 4 * abb[33];
z[112] = z[4] + z[98];
z[113] = 9 * abb[32] + z[7];
z[114] = 15 * abb[27] + -z[10] + -z[113];
z[114] = 69 * abb[26] + -z[96] + -z[112] + 2 * z[114];
z[114] = abb[2] * z[114];
z[115] = abb[27] + -abb[32];
z[116] = -abb[30] + -3 * z[115];
z[48] = -z[11] + -z[48] + 4 * z[116];
z[48] = abb[6] * z[48];
z[25] = z[25] + z[45] + z[48] + -z[95] + z[106] + -z[109] + z[114];
z[25] = abb[18] * z[25];
z[16] = z[16] * z[29];
z[45] = -z[21] + z[23] + z[70] + -z[100];
z[45] = abb[4] * z[45];
z[45] = z[45] + -z[94];
z[48] = 8 * abb[32];
z[32] = abb[28] + z[27] + z[32] + -z[48] + z[55];
z[32] = abb[9] * z[32];
z[55] = z[22] + z[96];
z[106] = abb[30] + -abb[33] + z[18] + z[46] + -z[55];
z[106] = abb[0] * z[106];
z[32] = -z[16] + z[32] + z[38] + -z[43] + 2 * z[45] + -z[83] + z[106] + z[109];
z[32] = abb[17] * z[32];
z[38] = -abb[28] + z[15];
z[45] = 6 * abb[32];
z[58] = -9 * abb[30] + abb[33] + z[38] + -z[45] + z[58] + z[110];
z[58] = abb[0] * z[58];
z[106] = -z[8] + z[23];
z[109] = z[21] + -z[74] + -z[106];
z[109] = abb[4] * z[109];
z[16] = z[16] + z[17] + -z[43] + z[58] + z[109];
z[16] = abb[16] * z[16];
z[13] = z[3] + z[4] + z[10] + z[13] + z[96] + -z[113];
z[13] = abb[16] * z[13];
z[17] = 10 * abb[31];
z[14] = 15 * abb[32] + -z[3] + -z[7] + -z[14] + -z[17];
z[14] = abb[17] * z[14];
z[58] = abb[15] * z[19];
z[13] = z[13] + z[14] + 6 * z[58];
z[13] = abb[3] * z[13];
z[14] = abb[15] * z[42];
z[42] = 4 * z[115];
z[96] = z[1] + z[11] + z[42];
z[109] = 2 * abb[6];
z[96] = z[76] * z[96] * z[109];
z[113] = abb[16] * z[19];
z[114] = abb[17] * z[19];
z[116] = z[113] + -z[114];
z[117] = 2 * z[58] + z[116];
z[118] = 6 * abb[1];
z[117] = z[117] * z[118];
z[119] = -abb[15] + z[76];
z[120] = abb[31] + z[0];
z[119] = abb[5] * z[119] * z[120];
z[13] = z[13] + z[14] + z[16] + z[25] + z[32] + z[96] + z[117] + 8 * z[119];
z[12] = abb[29] + -abb[31] + z[3] + z[12] + -z[31] + -z[46] + z[78];
z[12] = abb[9] * z[12];
z[14] = -abb[29] + abb[30] + z[10] + -z[69] + z[101] + -z[115];
z[14] = abb[0] * z[14];
z[16] = -abb[25] + abb[26];
z[25] = -z[16] + z[70] + -z[93];
z[25] = abb[4] * z[25];
z[12] = z[12] + z[14] + z[25] + -z[41];
z[14] = 39 * abb[26];
z[25] = -9 * abb[27] + abb[28] + -abb[30] + z[48];
z[25] = -z[4] + z[11] + -z[14] + 2 * z[25] + z[98];
z[25] = abb[2] * z[25];
z[32] = -9 * abb[26] + z[31] + -z[34] + z[51] + -z[84];
z[32] = z[32] * z[71];
z[41] = 2 * z[115];
z[48] = z[7] + z[41];
z[48] = 2 * z[48] + -z[53];
z[48] = abb[6] * z[48];
z[53] = z[19] * z[118];
z[84] = -abb[31] + -z[50] + z[102];
z[93] = 2 * abb[5];
z[84] = z[84] * z[93];
z[12] = 2 * z[12] + z[25] + z[32] + z[40] + z[48] + -z[53] + z[83] + z[84];
z[12] = abb[14] * z[12];
z[12] = z[12] + 2 * z[13];
z[12] = abb[14] * z[12];
z[13] = 9 * abb[25];
z[20] = -z[13] + z[20] + z[24] + -z[35] + z[81];
z[20] = abb[0] * z[20];
z[25] = -18 * abb[26] + -z[15] + z[49] + z[86] + -z[106];
z[25] = abb[9] * z[25];
z[28] = z[28] * z[71];
z[32] = -abb[28] + z[89];
z[48] = -4 * abb[32] + -z[31] + z[32];
z[84] = 21 * abb[26];
z[48] = -abb[33] + 2 * z[48] + z[84] + z[88];
z[98] = z[29] * z[48];
z[5] = z[5] + -z[20] + z[25] + z[28] + -z[95] + z[98];
z[5] = z[5] * z[76];
z[5] = z[5] + -z[96];
z[20] = abb[27] + -abb[33] + z[1] + -z[54] + -z[97];
z[20] = abb[0] * z[20];
z[25] = z[34] + z[50];
z[34] = 6 * abb[30];
z[54] = abb[32] + z[34];
z[95] = z[25] + -z[54] + z[78];
z[96] = abb[9] * z[95];
z[98] = -abb[25] + abb[27] + -abb[31] + z[18];
z[98] = abb[4] * z[98];
z[20] = z[20] + -z[94] + z[96] + 2 * z[98];
z[68] = -z[10] + z[64] + z[68] + -z[75];
z[68] = z[6] * z[68];
z[96] = -21 * abb[27] + 4 * abb[28] + 15 * abb[30] + 13 * abb[32];
z[96] = -111 * abb[26] + z[13] + 2 * z[96] + z[112];
z[96] = abb[2] * z[96];
z[98] = -abb[30] + 10 * z[115];
z[14] = z[11] + z[14] + 2 * z[98];
z[14] = abb[6] * z[14];
z[48] = -z[48] * z[71];
z[14] = z[14] + 2 * z[20] + z[40] + z[48] + z[68] + z[96];
z[14] = abb[18] * z[14];
z[5] = 2 * z[5] + z[14];
z[5] = abb[18] * z[5];
z[14] = 15 * abb[25];
z[20] = -z[14] + -z[22] + z[24] + z[92] + -z[99];
z[20] = abb[0] * z[20];
z[40] = 12 * abb[30] + 18 * abb[31] + -z[69] + -z[75] + -z[79] + -z[91];
z[40] = abb[9] * z[40];
z[48] = z[22] + -z[73];
z[48] = abb[4] * z[48];
z[68] = abb[29] + abb[31] + -z[89] + -z[100] + -z[104];
z[68] = abb[2] * z[68];
z[35] = -abb[25] + z[35] + z[66];
z[35] = abb[10] * z[35];
z[66] = z[6] * z[80];
z[73] = abb[26] + abb[27] + z[0];
z[73] = abb[6] * z[73];
z[20] = z[20] + 2 * z[35] + z[40] + z[48] + z[66] + 4 * z[68] + 16 * z[73];
z[20] = abb[23] * z[20];
z[14] = 7 * abb[30] + -z[14] + z[27] + -z[49] + 2 * z[102];
z[14] = abb[0] * z[14];
z[40] = z[67] * z[95];
z[34] = -11 * abb[27] + z[34] + z[47];
z[34] = -53 * abb[26] + 2 * z[34] + z[69] + z[112];
z[34] = abb[2] * z[34];
z[41] = abb[30] + z[41];
z[1] = z[1] + 3 * z[41] + z[60];
z[1] = abb[6] * z[1];
z[35] = z[35] + z[94];
z[16] = abb[4] * z[16];
z[16] = z[16] + -z[35];
z[1] = z[1] + z[14] + 2 * z[16] + z[34] + z[40] + -z[83];
z[14] = 2 * abb[19];
z[1] = z[1] * z[14];
z[3] = abb[25] + -z[3] + z[10] + z[26] + -z[31];
z[3] = abb[16] * z[3];
z[10] = -abb[28] + -abb[33] + -z[8] + z[21] + z[70] + -z[87];
z[10] = abb[17] * z[10];
z[3] = z[3] + z[10];
z[3] = abb[17] * z[3];
z[10] = z[11] + z[99];
z[16] = -z[4] + -z[10] + -z[86] + z[92];
z[16] = abb[23] * z[16];
z[26] = abb[28] + z[86];
z[34] = -abb[26] + -z[8] + z[26] + z[107];
z[34] = abb[22] * z[34];
z[23] = abb[27] + z[23];
z[40] = z[23] * z[77];
z[34] = z[34] + z[40];
z[40] = 11 * abb[26];
z[25] = -z[25] + z[26] + -z[40];
z[14] = z[14] * z[25];
z[25] = z[58] + z[116];
z[25] = abb[15] * z[25];
z[3] = z[3] + z[14] + z[16] + -6 * z[25] + 2 * z[34];
z[3] = z[3] * z[71];
z[14] = -z[19] * z[77];
z[16] = 2 * z[113] + -z[114];
z[16] = abb[17] * z[16];
z[14] = z[14] + z[16] + -4 * z[25];
z[14] = z[14] * z[118];
z[16] = z[55] + z[72] + -z[75];
z[19] = -abb[22] + abb[23];
z[16] = z[16] * z[19];
z[19] = -z[76] * z[120];
z[25] = abb[28] + 10 * z[0] + z[64];
z[25] = abb[15] * z[25];
z[19] = 8 * z[19] + z[25];
z[19] = abb[15] * z[19];
z[25] = 7 * abb[31] + -z[47] + z[69];
z[25] = abb[20] * z[25];
z[16] = z[16] + z[19] + z[25];
z[16] = z[16] * z[93];
z[1] = z[1] + z[3] + z[5] + 2 * z[9] + z[12] + z[14] + z[16] + z[20];
z[2] = -z[2] + z[74];
z[3] = -z[2] + -z[31] + z[62] + z[111];
z[3] = abb[9] * z[3];
z[2] = z[2] + z[85] + -z[110];
z[2] = abb[4] * z[2];
z[5] = -abb[28] + -abb[32];
z[5] = -abb[25] + 3 * z[5] + z[24] + z[74];
z[5] = abb[3] * z[5];
z[9] = abb[0] * z[36];
z[2] = z[2] + z[3] + z[5] + z[9] + -z[35] + z[37] + -z[43] + -z[53] + -z[56] + -4 * z[73];
z[2] = abb[21] * z[2];
z[3] = abb[30] * (T(4) / T(3)) + -z[18];
z[5] = abb[27] * (T(2) / T(3));
z[9] = abb[28] * (T(1) / T(3));
z[12] = -z[0] + z[3] + -z[5] + -z[9];
z[12] = abb[0] * z[12];
z[14] = -abb[32] + abb[25] * (T(1) / T(3)) + abb[31] * (T(4) / T(3));
z[5] = z[5] + -z[9] + -z[14];
z[5] = abb[4] * z[5];
z[3] = -z[3] + -z[9] + z[14];
z[3] = abb[2] * z[3];
z[9] = abb[3] * z[23];
z[3] = z[3] + z[5] + (T(-2) / T(3)) * z[9] + z[12] + z[82];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[1] = 2 * z[1] + 8 * z[2] + 13 * z[3];
z[1] = 4 * z[1];
z[2] = z[22] + z[27] + -z[46] + z[61] + -z[90];
z[3] = abb[4] * z[2];
z[5] = -13 * abb[25] + -z[15] + z[57] + z[75] + -z[78] + -z[91];
z[5] = abb[0] * z[5];
z[9] = z[4] + -z[38] + z[50] + z[54] + -z[105];
z[9] = z[9] * z[29];
z[12] = z[17] + z[59];
z[10] = z[10] + -z[12] + z[46] + -z[86];
z[10] = abb[9] * z[10];
z[14] = -z[31] + z[40] + z[42];
z[14] = z[14] * z[109];
z[14] = z[10] + z[14];
z[15] = z[44] * z[71];
z[16] = -6 * abb[25] + -abb[28] + z[45] + -z[88];
z[16] = z[16] * z[93];
z[3] = z[3] + z[5] + z[9] + z[14] + z[15] + -z[16] + -z[53];
z[3] = abb[14] * z[3];
z[5] = -abb[32] + -z[22] + z[61] + -z[81] + z[97];
z[5] = abb[0] * z[5];
z[4] = z[4] + z[11];
z[9] = -abb[32] + z[4] + -z[33] + -z[110];
z[9] = abb[4] * z[9];
z[7] = -z[7] + z[32] + -z[51];
z[7] = -abb[25] + 2 * z[7] + z[84];
z[7] = z[7] * z[29];
z[8] = z[8] + z[33];
z[8] = abb[26] + 11 * abb[31] + -2 * z[8] + z[108];
z[6] = z[6] * z[8];
z[5] = z[5] + -z[6] + z[7] + z[9] + -z[14] + z[28];
z[5] = abb[18] * z[5];
z[2] = abb[0] * z[2];
z[7] = z[10] + 8 * z[37];
z[4] = z[4] + -z[21] + -z[39] + -z[65];
z[4] = abb[4] * z[4];
z[2] = z[2] + z[4] + z[6] + z[7] + -z[30];
z[2] = abb[17] * z[2];
z[4] = -abb[32] + z[13] + -z[59] + -z[61] + z[63] + -z[86];
z[4] = abb[0] * z[4];
z[0] = -z[0] + z[33];
z[0] = 3 * z[0] + -z[12] + z[27];
z[0] = abb[4] * z[0];
z[0] = z[0] + z[4] + -z[7] + z[16];
z[0] = abb[15] * z[0];
z[4] = 2 * abb[16];
z[6] = z[4] * z[52];
z[7] = abb[17] * z[103];
z[4] = -z[4] * z[23];
z[4] = z[4] + z[7] + 3 * z[58];
z[4] = z[4] * z[71];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[117];
z[0] = 16 * m1_set::bc<T>[0] * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_687_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-244.37300566006329070864959755753957159630835175820259016842433718"),stof<T>("-97.39611495360723787802164301319716354697912002521745314397428377")}, std::complex<T>{stof<T>("563.8163711942118973579775722469827253703238941863211020323346863"),stof<T>("-1156.8187235267090374865077802431694655087468522700780874149346868")}, std::complex<T>{stof<T>("49.85251191333757139777124538652395794920871933704495294772108423"),stof<T>("-314.08272837986695310203234846002186406233785555068720658579423392")}, std::complex<T>{stof<T>("308.82542020037316866964613978570994685331768338800271714456136185"),stof<T>("-555.00268603052221216585177851682940330406058820722806357927231979")}, std::complex<T>{stof<T>("-28.240549396660009698402753265115480102125719254216771579574702074"),stof<T>("95.71967846624625301542642783129893430402463067444315793003585604")}, std::complex<T>{stof<T>("-176.91614773818144299751070592661821796022441492851748956945174693"),stof<T>("762.95587802213775214559433748184204838241367800183009023868186482")}, std::complex<T>{stof<T>("-237.76267683442113811496860853081744683581827530827482916597022606"),stof<T>("596.18950386583134213555496183104961645139937434069386850059782583")}, std::complex<T>{stof<T>("14.8520538069864127587193631971664287763124668233938116497513321"),stof<T>("177.17623207831157011690273731450271661097443874277824373443287184")}, stof<T>("-107.52675968961600501276538657346740664089487498594988099648885006"), stof<T>("-78.039413497218364182385893645573224160329866409059849676046121531"), stof<T>("78.039413497218364182385893645573224160329866409059849676046121531")};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[55].real()/kbase.W[55].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[123].real()/kbase.W[123].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_687_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_687_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-108.00060044758115657960883457496514956939702239478926649817015676"),stof<T>("672.0453727514179875707391177962045413725734887562403262605272941")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,36> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), dlog_W61(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[55].real()/k.W[55].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[123].real()/k.W[123].real())};

                    
            return f_4_687_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_687_DLogXconstant_part(base_point<T>, kend);
	value += f_4_687_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_687_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_687_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_687_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_687_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_687_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_687_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
