/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_837.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_837_abbreviated (const std::array<T,102>& abb) {
T z[187];
z[0] = -abb[76] + abb[80];
z[1] = abb[77] + abb[79];
z[2] = z[0] + -z[1];
z[3] = 4 * abb[78];
z[4] = 2 * abb[82];
z[5] = -z[2] + z[3] + z[4];
z[5] = abb[0] * z[5];
z[6] = abb[78] + z[1];
z[7] = 4 * z[6];
z[8] = abb[11] * z[7];
z[5] = z[5] + z[8];
z[9] = 3 * z[1];
z[10] = 3 * abb[82];
z[11] = 2 * abb[76];
z[12] = -z[9] + z[10] + -z[11];
z[12] = abb[4] * z[12];
z[13] = 2 * z[1];
z[14] = 2 * abb[78];
z[15] = z[13] + z[14];
z[16] = abb[82] + z[0];
z[17] = z[15] + -z[16];
z[17] = abb[5] * z[17];
z[18] = abb[20] * abb[84];
z[17] = z[17] + z[18];
z[19] = -abb[82] + z[1];
z[20] = -abb[78] + z[0];
z[21] = -z[19] + z[20];
z[22] = 2 * z[21];
z[23] = abb[3] * z[22];
z[24] = abb[10] * z[7];
z[25] = z[23] + z[24];
z[26] = abb[9] * z[21];
z[26] = 4 * z[26];
z[27] = 2 * abb[10];
z[28] = -abb[3] + z[27];
z[29] = 2 * abb[1];
z[30] = -abb[4] + z[29];
z[31] = z[28] + z[30];
z[32] = 2 * abb[0];
z[33] = -abb[5] + z[32];
z[34] = -z[31] + z[33];
z[35] = 2 * abb[83];
z[36] = z[34] * z[35];
z[37] = 2 * abb[80];
z[38] = z[30] * z[37];
z[39] = abb[1] * abb[76];
z[40] = -z[38] + 4 * z[39];
z[41] = 2 * abb[7];
z[42] = z[19] * z[41];
z[43] = -abb[69] + abb[74];
z[44] = -abb[70] + z[43];
z[45] = abb[32] * z[44];
z[46] = abb[15] * z[19];
z[47] = z[45] + z[46];
z[48] = z[14] * z[30];
z[49] = abb[12] * z[19];
z[12] = -z[5] + z[12] + 3 * z[17] + z[25] + z[26] + -z[36] + z[40] + z[42] + z[47] + z[48] + -z[49];
z[12] = abb[42] * z[12];
z[48] = z[1] + z[10] + z[11];
z[48] = abb[4] * z[48];
z[50] = z[18] + z[46];
z[51] = abb[21] * abb[84];
z[52] = z[39] + -z[51];
z[53] = -z[0] + z[19];
z[54] = z[41] * z[53];
z[55] = 3 * abb[4] + -z[29];
z[56] = -abb[7] + z[55];
z[56] = z[14] * z[56];
z[57] = 4 * abb[10];
z[58] = z[19] * z[57];
z[59] = abb[3] * z[19];
z[60] = abb[24] * abb[84];
z[61] = 2 * z[60];
z[5] = z[5] + z[38] + z[48] + z[50] + -4 * z[52] + -z[54] + z[56] + z[58] + -2 * z[59] + z[61];
z[5] = abb[39] * z[5];
z[38] = 2 * abb[81];
z[48] = z[35] + z[38];
z[56] = 3 * abb[80];
z[58] = abb[76] + abb[78];
z[62] = -z[1] + z[10] + z[48] + z[56] + -z[58];
z[62] = abb[6] * z[62];
z[63] = abb[3] * z[21];
z[62] = z[62] + -3 * z[63];
z[64] = z[11] + -z[37];
z[65] = z[19] + z[64];
z[66] = abb[7] * z[65];
z[67] = abb[76] * z[29];
z[68] = abb[80] * z[30];
z[69] = z[67] + -z[68];
z[70] = abb[76] + z[19];
z[71] = abb[4] * z[70];
z[71] = -z[69] + z[71];
z[72] = z[66] + -z[71];
z[73] = 3 * abb[76];
z[74] = 3 * abb[78];
z[75] = -z[56] + z[73] + z[74];
z[76] = z[19] + -z[75];
z[76] = abb[14] * z[76];
z[77] = 2 * abb[14];
z[78] = z[31] + z[41] + -z[77];
z[79] = z[33] + z[78];
z[80] = z[35] * z[79];
z[81] = abb[13] * z[21];
z[82] = -abb[73] + z[43];
z[83] = abb[71] + z[82];
z[84] = abb[27] * z[83];
z[81] = z[81] + z[84];
z[85] = abb[76] + z[1];
z[86] = z[14] + 2 * z[85];
z[87] = abb[80] + abb[82];
z[88] = abb[81] + z[86] + -z[87];
z[88] = z[32] * z[88];
z[89] = 2 * abb[5];
z[90] = -z[57] + z[89];
z[90] = z[19] * z[90];
z[91] = 2 * abb[70];
z[92] = 2 * abb[72] + -abb[75];
z[93] = -abb[69] + -abb[71] + -3 * abb[73] + z[91] + -4 * z[92];
z[93] = abb[33] * z[93];
z[94] = z[30] + z[41];
z[95] = z[14] * z[94];
z[96] = abb[33] * abb[74];
z[72] = -z[62] + 2 * z[72] + z[76] + z[80] + z[81] + z[88] + z[90] + z[93] + z[95] + z[96];
z[72] = abb[43] * z[72];
z[76] = abb[12] * z[6];
z[80] = abb[14] * z[20];
z[76] = z[60] + z[76] + z[80];
z[90] = 3 * z[85];
z[3] = z[3] + z[90];
z[93] = -abb[80] + z[3] + z[38];
z[93] = abb[0] * z[93];
z[95] = 2 * z[92];
z[97] = abb[71] + z[95];
z[98] = abb[70] + -abb[73];
z[99] = z[97] + -z[98];
z[100] = abb[33] * z[99];
z[100] = -z[76] + -z[93] + z[100];
z[101] = abb[76] + abb[81];
z[15] = z[15] + z[101];
z[15] = z[15] * z[27];
z[102] = abb[0] + -abb[10];
z[102] = abb[3] + 2 * z[102];
z[103] = -abb[12] + abb[14];
z[104] = -abb[5] + z[102] + -z[103];
z[105] = z[35] * z[104];
z[15] = z[15] + -z[105];
z[106] = z[15] + z[100];
z[107] = abb[73] + z[92];
z[108] = z[43] + -z[107];
z[109] = -2 * z[108];
z[110] = abb[29] + abb[31];
z[109] = z[109] * z[110];
z[111] = abb[4] * abb[78];
z[111] = z[51] + z[111];
z[112] = z[1] + -z[4];
z[112] = abb[4] * z[112];
z[113] = z[4] + -z[13];
z[114] = z[20] + z[113];
z[114] = abb[6] * z[114];
z[115] = -abb[28] * z[99];
z[116] = 2 * z[18];
z[117] = -abb[82] + z[13];
z[118] = abb[78] + z[117];
z[119] = abb[5] * z[118];
z[120] = abb[11] * z[6];
z[121] = 2 * z[120];
z[112] = -z[23] + -z[106] + -z[109] + -z[111] + z[112] + z[114] + z[115] + -z[116] + -2 * z[119] + z[121];
z[112] = abb[38] * z[112];
z[114] = z[0] + z[19];
z[114] = abb[7] * z[114];
z[115] = z[19] + z[20];
z[122] = abb[5] * z[115];
z[123] = -z[71] + -z[114] + z[122];
z[124] = abb[81] + z[1];
z[125] = z[58] + z[124];
z[126] = z[57] * z[125];
z[126] = z[81] + z[126];
z[127] = abb[14] * z[115];
z[127] = z[96] + z[127];
z[128] = abb[69] + abb[73];
z[129] = -abb[71] + z[128];
z[130] = abb[33] * z[129];
z[130] = z[127] + -z[130];
z[131] = abb[7] + z[30];
z[131] = z[14] * z[131];
z[26] = -z[26] + -z[36] + -z[62] + -z[88] + 2 * z[123] + z[126] + z[130] + z[131];
z[26] = abb[44] * z[26];
z[36] = 2 * abb[17];
z[62] = -abb[4] + z[36];
z[88] = -abb[5] + abb[7] + -abb[12] + z[62];
z[88] = z[35] * z[88];
z[118] = z[27] * z[118];
z[88] = z[88] + -z[118];
z[118] = abb[4] * abb[82];
z[123] = z[111] + z[118];
z[132] = z[88] + -z[123];
z[133] = abb[78] + abb[82];
z[134] = abb[12] * z[133];
z[135] = z[60] + z[134];
z[136] = z[81] + z[135];
z[137] = abb[9] * z[22];
z[138] = -z[63] + z[137];
z[139] = -z[0] + z[4] + -z[9];
z[139] = abb[5] * z[139];
z[7] = abb[17] * z[7];
z[140] = -z[7] + z[121];
z[141] = -abb[83] + z[21];
z[142] = 2 * abb[6];
z[143] = z[141] * z[142];
z[139] = -z[18] + z[42] + z[132] + -z[136] + z[138] + z[139] + -z[140] + z[143];
z[143] = 2 * abb[40];
z[139] = z[139] * z[143];
z[144] = abb[82] + z[1];
z[37] = z[37] + z[48] + z[144];
z[48] = abb[6] * z[37];
z[145] = abb[81] + abb[82];
z[146] = z[58] + z[145];
z[146] = z[27] * z[146];
z[131] = z[131] + z[146];
z[31] = abb[7] + -abb[14] + z[31];
z[146] = z[31] * z[35];
z[48] = z[48] + -z[131] + -z[146];
z[146] = z[14] + z[65];
z[147] = abb[5] * z[146];
z[148] = z[44] * z[110];
z[147] = -z[48] + z[147] + -z[148];
z[148] = z[14] + -z[19] + z[64];
z[148] = abb[14] * z[148];
z[149] = 2 * z[107];
z[150] = -abb[70] + z[149];
z[151] = abb[69] + z[150];
z[151] = abb[33] * z[151];
z[148] = -z[96] + z[148] + z[151];
z[151] = abb[7] * z[53];
z[152] = z[71] + -z[151];
z[152] = -z[23] + -z[47] + -z[137] + -z[147] + z[148] + 2 * z[152];
z[153] = 2 * abb[45];
z[152] = z[152] * z[153];
z[154] = -z[82] + z[97];
z[155] = abb[39] * z[154];
z[156] = abb[71] + z[92];
z[157] = -z[143] * z[156];
z[150] = -z[43] + z[150];
z[158] = -abb[43] * z[150];
z[159] = abb[42] * z[44];
z[160] = -abb[70] + z[107];
z[161] = abb[45] * z[160];
z[162] = abb[44] * z[83];
z[155] = z[155] + z[157] + z[158] + -z[159] + 2 * z[161] + z[162];
z[157] = 2 * abb[30];
z[155] = z[155] * z[157];
z[158] = 2 * abb[44] + -z[143];
z[158] = z[83] * z[158];
z[162] = 2 * abb[41];
z[163] = z[99] * z[162];
z[164] = 2 * abb[43];
z[165] = -z[150] * z[164];
z[166] = -abb[39] * z[44];
z[158] = z[158] + -z[159] + 4 * z[161] + z[163] + z[165] + z[166];
z[158] = abb[28] * z[158];
z[91] = -z[43] + z[91];
z[165] = -abb[73] + z[91] + -z[97];
z[165] = abb[43] * z[165];
z[166] = abb[44] * z[154];
z[165] = z[163] + z[165] + z[166];
z[149] = abb[70] + -3 * z[43] + z[149];
z[166] = abb[39] + abb[42];
z[149] = z[149] * z[166];
z[166] = z[143] * z[154];
z[149] = -z[149] + -z[165] + z[166];
z[149] = -z[110] * z[149];
z[166] = abb[3] * z[20];
z[167] = z[18] + z[166];
z[168] = abb[5] * z[6];
z[168] = z[167] + z[168];
z[106] = z[106] + z[168];
z[106] = z[106] * z[162];
z[10] = -z[0] + 4 * z[1] + -z[10] + z[14];
z[169] = abb[5] * abb[39];
z[10] = z[10] * z[169];
z[170] = -z[28] + z[32] + -z[41] + z[55];
z[170] = abb[39] * z[170];
z[171] = abb[12] * abb[39];
z[172] = 2 * z[171];
z[173] = z[169] + z[172];
z[174] = abb[17] * abb[39];
z[170] = z[170] + z[173] + -4 * z[174];
z[170] = z[35] * z[170];
z[175] = -abb[28] + -abb[30] + abb[32];
z[176] = abb[59] + abb[60];
z[177] = -abb[62] + z[176];
z[178] = abb[61] + z[177];
z[175] = z[175] * z[178];
z[178] = abb[34] + -z[110];
z[177] = -abb[61] + z[177];
z[177] = z[177] * z[178];
z[176] = -3 * abb[61] + -abb[62] + -z[176];
z[176] = abb[35] * z[176];
z[178] = abb[15] + -abb[16] + -abb[18] + z[27];
z[179] = abb[5] + z[178];
z[179] = abb[96] * z[179];
z[175] = z[175] + z[176] + z[177] + z[179];
z[175] = m1_set::bc<T>[0] * z[175];
z[176] = abb[83] + z[21];
z[177] = abb[42] * z[176];
z[179] = abb[39] * z[21];
z[180] = z[177] + z[179];
z[181] = abb[39] * abb[83];
z[182] = -z[180] + 3 * z[181];
z[182] = z[142] * z[182];
z[183] = z[14] + z[144];
z[184] = z[171] * z[183];
z[45] = abb[39] * z[45];
z[185] = z[45] + z[184];
z[174] = 8 * z[174];
z[186] = z[6] * z[174];
z[5] = z[5] + z[10] + z[12] + z[26] + z[72] + z[106] + 2 * z[112] + z[139] + z[149] + z[152] + z[155] + z[158] + z[170] + 4 * z[175] + z[182] + z[185] + -z[186];
z[5] = m1_set::bc<T>[0] * z[5];
z[10] = z[18] + z[119];
z[12] = -z[21] + z[35];
z[12] = abb[6] * z[12];
z[26] = z[51] + z[118];
z[72] = -abb[4] + abb[7];
z[112] = abb[78] * z[72];
z[88] = -z[10] + -z[12] + -z[26] + z[88] + z[112] + -z[135] + -z[140] + z[151];
z[88] = abb[53] * z[88];
z[62] = -z[32] + z[62] + z[103];
z[62] = z[35] * z[62];
z[103] = -z[20] + z[35];
z[103] = abb[6] * z[103];
z[101] = z[27] * z[101];
z[112] = abb[4] * z[1];
z[62] = z[62] + z[100] + z[101] + -z[103] + -z[111] + -z[112] + -z[140];
z[100] = abb[52] * z[62];
z[101] = z[11] + z[19];
z[101] = abb[4] * z[101];
z[66] = -z[66] + z[101] + z[148];
z[103] = abb[3] * z[146];
z[40] = z[40] + -z[103];
z[111] = -z[40] + z[66] + -z[131];
z[111] = abb[56] * z[111];
z[118] = abb[7] * abb[78];
z[114] = -z[81] + z[114] + -z[118] + z[138];
z[118] = abb[54] * z[114];
z[119] = abb[4] * z[19];
z[131] = -z[47] + z[119];
z[135] = z[59] + z[131];
z[139] = abb[7] * z[19];
z[140] = z[135] + -z[137] + -z[139];
z[140] = abb[55] * z[140];
z[148] = abb[56] * z[150];
z[149] = abb[54] * z[83];
z[148] = -z[148] + z[149];
z[149] = -abb[52] * z[99];
z[151] = abb[55] * z[44];
z[149] = -z[148] + z[149] + z[151];
z[149] = abb[28] * z[149];
z[88] = -z[88] + z[100] + -z[111] + -z[118] + -z[140] + -z[149];
z[100] = -abb[76] + z[19];
z[111] = abb[78] + -abb[80];
z[118] = z[100] + -z[111];
z[140] = -abb[26] * z[118];
z[149] = abb[22] + abb[23];
z[149] = z[21] * z[149];
z[152] = abb[80] + z[38] + z[58];
z[155] = z[144] + z[152];
z[158] = abb[19] * z[155];
z[129] = -abb[74] + z[129];
z[129] = abb[36] * z[129];
z[129] = z[129] + z[140] + -z[149] + z[158];
z[140] = abb[58] * z[129];
z[149] = -abb[54] * z[115];
z[146] = -abb[55] * z[146];
z[146] = z[146] + z[149];
z[89] = z[89] * z[146];
z[37] = abb[56] * z[37];
z[146] = abb[54] * z[21];
z[37] = z[37] + z[146];
z[37] = z[37] * z[142];
z[142] = abb[53] * z[154];
z[142] = z[142] + -z[151];
z[146] = -z[142] + -z[148];
z[146] = z[146] * z[157];
z[148] = abb[21] + abb[24] + -abb[25];
z[148] = z[19] * z[148];
z[149] = abb[37] * z[44];
z[148] = z[148] + -z[149];
z[149] = -abb[0] + abb[5];
z[149] = abb[84] * z[149];
z[151] = abb[20] * z[16];
z[149] = z[148] + z[149] + z[151];
z[149] = abb[57] * z[149];
z[151] = -2 * z[110];
z[142] = z[142] * z[151];
z[151] = abb[64] + abb[65];
z[158] = abb[67] + -z[151];
z[158] = z[21] * z[158];
z[118] = -abb[66] * z[118];
z[170] = abb[101] * z[108];
z[31] = abb[56] * z[31];
z[31] = 3 * abb[66] + abb[67] + -4 * z[31] + z[151];
z[31] = abb[83] * z[31];
z[5] = z[5] + z[31] + z[37] + -2 * z[88] + z[89] + z[118] + z[140] + z[142] + z[146] + z[149] + z[158] + z[170];
z[31] = abb[2] * abb[76];
z[37] = 7 * z[1];
z[88] = -4 * abb[76] + 13 * abb[82] + -z[37];
z[88] = abb[4] * z[88];
z[89] = 8 * abb[1] + -abb[2];
z[118] = 2 * abb[4] + 5 * abb[7] + z[89];
z[118] = abb[78] * z[118];
z[88] = -z[31] + 8 * z[39] + z[88] + z[118];
z[49] = z[46] + -z[49] + z[81] + z[130];
z[118] = abb[92] + abb[93];
z[130] = 4 * abb[95];
z[140] = 4 * z[118] + -z[130];
z[142] = abb[70] + z[140];
z[146] = abb[71] + 4 * abb[73] + 5 * z[92];
z[149] = 4 * abb[94];
z[146] = (T(-11) / T(3)) * z[43] + z[142] + (T(2) / T(3)) * z[146] + -z[149];
z[146] = z[110] * z[146];
z[151] = abb[82] * (T(13) / T(3));
z[158] = 4 * abb[63];
z[37] = abb[78] * (T(10) / T(3)) + (T(-2) / T(3)) * z[0] + z[37] + -z[151] + z[158];
z[37] = abb[5] * z[37];
z[170] = abb[71] + -abb[73];
z[142] = (T(2) / T(3)) * z[43] + z[142] + z[149] + (T(5) / T(3)) * z[170];
z[142] = abb[28] * z[142];
z[90] = abb[80] * (T(-13) / T(3)) + abb[81] * (T(-4) / T(3)) + abb[83] * (T(2) / T(3)) + z[74] + z[90] + -z[151];
z[90] = abb[6] * z[90];
z[98] = abb[71] + z[98];
z[140] = (T(5) / T(3)) * z[98] + z[140] + z[149];
z[140] = abb[30] * z[140];
z[149] = 3 * abb[94] + z[118];
z[149] = abb[35] * z[149];
z[151] = abb[94] + -z[118];
z[151] = abb[34] * z[151];
z[149] = z[149] + z[151];
z[151] = abb[80] * (T(1) / T(3));
z[89] = 4 * abb[4] + -z[89];
z[89] = z[89] * z[151];
z[11] = z[11] + z[38];
z[38] = 5 * z[6] + z[11];
z[38] = abb[10] * z[38];
z[151] = abb[78] * (T(-4) / T(3)) + abb[81] * (T(-2) / T(3)) + -z[85] + z[151];
z[151] = z[32] * z[151];
z[158] = z[158] * z[178];
z[118] = abb[94] + z[118];
z[118] = (T(1) / T(3)) * z[44] + -2 * z[118];
z[170] = 2 * abb[32];
z[118] = z[118] * z[170];
z[175] = 4 * abb[0];
z[178] = 4 * abb[1] + -5 * abb[3] + abb[4] + 10 * abb[10] + -z[175];
z[36] = abb[5] * (T(5) / T(3)) + -z[36] + (T(1) / T(3)) * z[178];
z[36] = z[35] * z[36];
z[178] = abb[32] + abb[34] + abb[35];
z[130] = z[130] * z[178];
z[178] = 2 * z[51];
z[182] = abb[7] * z[0];
z[36] = -z[7] + (T(8) / T(3)) * z[18] + z[36] + z[37] + (T(4) / T(3)) * z[38] + (T(2) / T(3)) * z[49] + (T(13) / T(3)) * z[63] + (T(1) / T(3)) * z[88] + z[89] + z[90] + z[118] + z[130] + z[140] + z[142] + z[146] + 4 * z[149] + z[151] + z[158] + z[178] + (T(-5) / T(3)) * z[182];
z[36] = prod_pow(m1_set::bc<T>[0], 2) * z[36];
z[37] = z[97] + z[128];
z[38] = abb[33] * z[37];
z[38] = z[38] + -z[127];
z[49] = z[38] + -z[81];
z[81] = abb[12] * z[183];
z[81] = z[61] + z[81];
z[88] = z[125] * z[175];
z[89] = -abb[28] * z[150];
z[90] = abb[80] + z[124];
z[90] = abb[8] * z[90];
z[97] = 2 * z[90];
z[118] = -abb[6] * z[19];
z[88] = -z[10] + -z[15] + z[47] + -z[49] + z[81] + z[88] + z[89] + -z[97] + z[103] + -z[109] + z[118] + -z[123];
z[88] = abb[38] * z[88];
z[3] = z[3] + -z[4] + -z[56];
z[3] = abb[0] * z[3];
z[89] = abb[33] * z[160];
z[103] = 2 * z[80] + -2 * z[89];
z[3] = z[3] + z[103];
z[109] = abb[4] * z[183];
z[118] = -z[8] + z[61];
z[124] = -abb[6] * z[22];
z[18] = z[3] + z[18] + -z[24] + -z[46] + z[109] + z[118] + z[124] + z[178];
z[18] = abb[39] * z[18];
z[3] = -z[3] + z[17] + z[25] + -z[81] + -z[105] + -z[131];
z[3] = abb[42] * z[3];
z[17] = abb[42] * z[150];
z[24] = -abb[39] * z[150];
z[25] = -z[83] * z[143];
z[81] = -z[156] * z[164];
z[109] = abb[44] * z[156];
z[24] = z[17] + z[24] + z[25] + z[81] + 2 * z[109] + z[163];
z[24] = abb[28] * z[24];
z[25] = 3 * abb[81] + z[86] + z[87];
z[25] = z[25] * z[32];
z[81] = 4 * z[90];
z[25] = z[25] + -z[38] + -z[81] + z[105] + -z[126];
z[38] = -abb[5] * z[22];
z[86] = -abb[6] * z[155];
z[38] = -z[25] + z[38] + z[63] + z[86];
z[38] = abb[44] * z[38];
z[20] = abb[5] * z[20];
z[86] = -z[20] + z[119];
z[87] = abb[3] * z[115];
z[90] = -abb[82] + z[9];
z[105] = z[90] + z[152];
z[105] = abb[6] * z[105];
z[25] = z[25] + -2 * z[86] + -z[87] + z[105];
z[25] = abb[43] * z[25];
z[86] = 2 * abb[71];
z[95] = z[44] + z[86] + z[95];
z[95] = abb[39] * z[95];
z[17] = -z[17] + z[95] + -z[165];
z[17] = -z[17] * z[110];
z[95] = z[2] + z[4];
z[95] = abb[0] * z[95];
z[105] = z[95] + -z[136];
z[124] = abb[6] * z[21];
z[10] = -z[10] + z[105] + z[121] + z[124];
z[10] = z[10] * z[143];
z[121] = abb[14] * abb[39];
z[121] = z[121] + -z[171];
z[102] = abb[39] * z[102];
z[102] = -z[102] + z[121] + z[169];
z[124] = -z[35] * z[102];
z[126] = abb[80] + z[145];
z[127] = z[32] * z[126];
z[127] = -z[97] + z[127];
z[130] = abb[69] + abb[70];
z[131] = abb[33] * z[130];
z[131] = -z[96] + -z[127] + z[131];
z[136] = -abb[14] * z[19];
z[135] = z[131] + z[135] + z[136];
z[135] = z[135] * z[153];
z[136] = z[0] + z[117];
z[136] = z[136] * z[169];
z[3] = z[3] + z[10] + z[17] + z[18] + z[24] + z[25] + z[38] + -z[45] + z[88] + z[106] + z[124] + z[135] + z[136] + z[184];
z[3] = abb[38] * z[3];
z[10] = z[54] + z[103] + z[137];
z[17] = 3 * abb[83];
z[18] = -4 * abb[81] + -abb[82] + -z[9] + -z[17] + -z[56] + -z[58];
z[18] = abb[6] * z[18];
z[24] = z[19] + z[75];
z[25] = -abb[3] + abb[5];
z[25] = z[24] * z[25];
z[11] = z[11] + z[183];
z[11] = z[11] * z[27];
z[38] = 3 * z[30] + z[41];
z[33] = -3 * abb[3] + 6 * abb[10] + -z[33] + z[38] + -z[77];
z[33] = abb[83] * z[33];
z[54] = z[32] * z[155];
z[54] = z[54] + -z[81];
z[56] = z[19] + z[73];
z[56] = -abb[4] * z[56];
z[38] = abb[78] * z[38];
z[11] = z[10] + z[11] + z[18] + z[25] + z[33] + z[38] + 6 * z[39] + -z[54] + z[56] + -3 * z[68];
z[11] = abb[43] * z[11];
z[18] = z[21] * z[32];
z[25] = z[19] * z[27];
z[18] = z[18] + z[25];
z[33] = abb[78] * z[94];
z[0] = z[0] * z[41];
z[38] = -abb[4] * z[100];
z[38] = z[0] + z[18] + -z[33] + z[38] + 2 * z[46] + -z[69] + -z[87] + -z[103];
z[38] = abb[39] * z[38];
z[39] = abb[83] * z[79];
z[33] = z[33] + z[39] + -z[71];
z[25] = z[0] + z[25];
z[39] = -z[25] + z[33];
z[56] = -abb[5] * z[24];
z[58] = -z[92] + -z[128];
z[58] = abb[33] * z[58];
z[58] = z[58] + z[96];
z[73] = abb[6] * z[176];
z[75] = z[77] * z[115];
z[54] = z[39] + z[54] + z[56] + 2 * z[58] + -z[73] + z[75] + -z[138];
z[54] = abb[44] * z[54];
z[56] = abb[5] * z[21];
z[33] = -z[10] + z[18] + -z[33] + z[56] + -z[63];
z[33] = abb[42] * z[33];
z[10] = -z[10] + -z[40] + z[101] + z[127] + -z[147];
z[10] = abb[45] * z[10];
z[56] = -abb[43] * z[108];
z[58] = z[91] + -z[107];
z[58] = abb[42] * z[58];
z[75] = -abb[44] * z[108];
z[77] = -abb[39] * z[108];
z[56] = z[56] + z[58] + -z[75] + z[77];
z[56] = z[56] * z[110];
z[24] = z[24] * z[169];
z[78] = -z[32] + -z[78];
z[78] = abb[39] * z[78];
z[78] = z[78] + z[169];
z[78] = abb[83] * z[78];
z[79] = z[179] + z[181];
z[81] = z[79] + -z[177];
z[81] = abb[6] * z[81];
z[88] = abb[39] * z[137];
z[10] = z[10] + z[11] + z[24] + -z[33] + z[38] + 2 * z[45] + z[54] + z[56] + z[78] + z[81] + z[88];
z[10] = abb[45] * z[10];
z[11] = -z[82] + z[86] + 3 * z[92];
z[11] = abb[39] * z[11];
z[24] = abb[40] * z[154];
z[38] = -abb[42] * z[108];
z[11] = z[11] + -z[24] + z[38];
z[11] = abb[40] * z[11];
z[24] = abb[43] * z[160];
z[45] = z[24] + -z[109];
z[54] = -abb[40] * z[108];
z[56] = z[45] + z[54] + z[58];
z[56] = abb[43] * z[56];
z[37] = abb[86] * z[37];
z[58] = abb[86] + abb[88];
z[58] = abb[74] * z[58];
z[78] = abb[88] * z[130];
z[37] = -z[37] + z[58] + -z[78];
z[58] = abb[50] * z[156];
z[81] = abb[47] * z[83];
z[58] = z[37] + z[58] + -z[81];
z[82] = abb[39] * z[156];
z[86] = abb[42] * z[160];
z[91] = z[82] + -z[86];
z[45] = z[45] + z[91];
z[92] = abb[41] * z[99];
z[45] = 2 * z[45] + -z[92];
z[45] = abb[41] * z[45];
z[92] = z[54] + -z[77];
z[92] = abb[44] * z[92];
z[43] = abb[70] + -2 * z[43] + z[107];
z[43] = abb[39] * abb[42] * z[43];
z[94] = prod_pow(abb[39], 2);
z[96] = z[94] * z[156];
z[100] = 2 * abb[49];
z[101] = z[98] * z[100];
z[106] = 2 * abb[46];
z[44] = z[44] * z[106];
z[11] = -z[11] + z[43] + -z[44] + -z[45] + z[56] + 2 * z[58] + -z[92] + z[96] + z[101];
z[11] = -z[11] * z[110];
z[43] = -z[30] + z[41];
z[44] = z[28] + z[32];
z[45] = z[43] + z[44];
z[56] = 2 * abb[12];
z[58] = -abb[5] + z[45] + -z[56];
z[58] = abb[83] * z[58];
z[18] = z[18] + -z[71];
z[92] = -z[18] + z[58];
z[96] = z[122] + -z[138];
z[43] = abb[78] * z[43];
z[22] = abb[13] * z[22];
z[101] = -abb[6] * z[141];
z[0] = -z[0] + z[22] + z[43] + 2 * z[84] + z[92] + z[96] + z[101];
z[0] = abb[40] * z[0];
z[22] = z[32] * z[125];
z[84] = abb[83] + 2 * z[126];
z[84] = abb[6] * z[84];
z[23] = -z[22] + -z[23] + -z[39] + z[49] + z[84] + -z[122] + z[137];
z[23] = abb[44] * z[23];
z[39] = z[27] * z[125];
z[22] = -z[22] + z[39];
z[20] = z[20] + -z[97] + -z[166];
z[13] = z[13] + z[152];
z[39] = abb[83] + z[13];
z[39] = abb[6] * z[39];
z[34] = abb[83] * z[34];
z[49] = abb[4] * abb[76];
z[84] = abb[78] * z[30];
z[34] = z[20] + -z[22] + z[34] + z[39] + z[49] + -z[69] + -z[84];
z[39] = abb[43] * z[34];
z[97] = abb[2] + z[30];
z[97] = z[97] * z[111];
z[49] = z[31] + -z[49] + z[67] + z[80] + -z[89] + z[97];
z[49] = abb[39] * z[49];
z[30] = abb[39] * z[30];
z[30] = z[30] + -z[121];
z[30] = abb[83] * z[30];
z[30] = z[30] + z[49];
z[35] = -abb[39] * z[35];
z[35] = z[35] + z[177];
z[35] = abb[6] * z[35];
z[0] = z[0] + z[23] + 2 * z[30] + z[33] + z[35] + z[39];
z[0] = abb[43] * z[0];
z[23] = z[42] + -z[63];
z[30] = z[32] * z[133];
z[32] = z[14] + z[90];
z[32] = z[27] * z[32];
z[33] = abb[76] + z[144];
z[33] = abb[4] * z[33];
z[35] = abb[78] * z[55];
z[32] = -z[23] + z[30] + z[32] + z[33] + z[35] + -2 * z[52] + z[68] + 8 * z[120];
z[32] = abb[39] * z[32];
z[25] = z[25] + -z[43] + z[118];
z[9] = z[9] + z[74];
z[33] = z[9] + -z[16];
z[33] = abb[5] * z[33];
z[30] = z[25] + -z[30] + z[33] + -z[58] + -z[71] + z[116] + 2 * z[134] + z[138];
z[30] = abb[42] * z[30];
z[33] = -z[42] + z[96];
z[7] = z[7] + -z[8] + -z[12] + -z[33] + -z[95] + z[132];
z[7] = abb[40] * z[7];
z[12] = 5 * abb[4] + -z[29] + -z[41] + z[44];
z[12] = abb[39] * z[12];
z[12] = z[12] + 3 * z[169] + z[172] + -z[174];
z[12] = abb[83] * z[12];
z[29] = z[115] * z[169];
z[29] = z[29] + -z[88];
z[35] = -z[180] + 5 * z[181];
z[35] = abb[6] * z[35];
z[7] = z[7] + z[12] + z[29] + z[30] + z[32] + z[35] + -z[186];
z[7] = abb[40] * z[7];
z[12] = z[6] * z[27];
z[2] = -z[2] + z[14];
z[2] = abb[0] * z[2];
z[27] = -z[2] + z[12] + z[89];
z[30] = abb[83] * z[104];
z[32] = -z[27] + z[30] + z[76] + -z[168];
z[32] = abb[42] * z[32];
z[35] = z[51] + z[112];
z[39] = abb[2] * abb[80];
z[31] = -z[31] + z[39];
z[39] = abb[2] + abb[4];
z[39] = abb[78] * z[39];
z[39] = -z[31] + z[35] + z[39];
z[27] = z[27] + -z[39] + -z[80];
z[27] = abb[39] * z[27];
z[13] = abb[6] * z[13];
z[13] = z[13] + z[20];
z[20] = abb[2] * abb[78];
z[20] = z[13] + z[20] + -z[31] + z[80];
z[22] = -z[22] + z[30];
z[30] = -z[20] + -z[22] + z[89];
z[30] = abb[43] * z[30];
z[31] = abb[83] * z[102];
z[42] = -abb[33] * z[156];
z[22] = z[22] + z[42];
z[22] = abb[44] * z[22];
z[22] = z[22] + z[27] + z[30] + z[31] + z[32];
z[27] = abb[2] * z[64];
z[30] = 2 * abb[2] + abb[4];
z[30] = abb[78] * z[30];
z[13] = z[13] + -z[15] + z[27] + z[30] + z[35] + z[93] + z[103];
z[13] = abb[41] * z[13];
z[13] = z[13] + 2 * z[22];
z[13] = abb[41] * z[13];
z[15] = z[75] + z[86];
z[22] = z[15] + z[24] + -z[77] + -z[161];
z[24] = -abb[45] * z[22];
z[27] = abb[40] * z[156];
z[27] = z[27] + -z[82];
z[30] = -z[27] + z[38];
z[30] = abb[40] * z[30];
z[31] = z[15] + -z[54];
z[31] = abb[43] * z[31];
z[32] = abb[89] * z[150];
z[35] = -abb[42] * z[77];
z[27] = abb[44] * z[27];
z[38] = abb[87] * z[83];
z[24] = z[24] + z[27] + z[30] + z[31] + -z[32] + z[35] + -z[37] + z[38];
z[24] = z[24] * z[157];
z[27] = -z[110] * z[160];
z[27] = z[27] + z[34];
z[27] = abb[51] * z[27];
z[30] = -z[63] + -z[105] + -z[123];
z[30] = abb[47] * z[30];
z[31] = abb[33] + z[110];
z[31] = z[31] * z[98];
z[20] = z[20] + z[31];
z[20] = abb[48] * z[20];
z[20] = z[20] + z[27] + z[30];
z[27] = 2 * abb[85];
z[30] = z[27] * z[62];
z[2] = z[2] + -z[69];
z[31] = abb[1] + -abb[4];
z[31] = z[14] * z[31];
z[34] = -abb[4] * z[85];
z[8] = -z[2] + -z[8] + -z[12] + z[31] + z[34] + -z[51];
z[8] = z[8] * z[94];
z[12] = abb[76] + -z[113];
z[12] = abb[4] * z[12];
z[2] = z[2] + z[12] + -z[25] + -z[50] + z[87];
z[2] = abb[39] * z[2];
z[12] = abb[39] * z[45];
z[12] = z[12] + -z[173];
z[12] = abb[83] * z[12];
z[4] = z[4] + -z[9] + -z[64];
z[4] = z[4] * z[169];
z[2] = z[2] + z[4] + z[12] + -z[88] + -z[185];
z[2] = abb[42] * z[2];
z[4] = -z[18] + z[23] + -z[84];
z[4] = abb[39] * z[4];
z[9] = z[33] + -z[73] + z[84] + -z[92];
z[9] = abb[40] * z[9];
z[18] = abb[6] * z[79];
z[4] = z[4] + z[9] + z[12] + z[18] + -z[29];
z[4] = abb[44] * z[4];
z[9] = abb[74] * abb[88];
z[9] = z[9] + -z[78];
z[12] = -abb[49] * z[98];
z[12] = z[9] + -z[12] + z[32] + -z[38] + -z[81];
z[18] = -z[22] * z[153];
z[22] = abb[40] * z[83];
z[15] = z[15] + z[22] + -z[82];
z[15] = z[15] * z[164];
z[22] = -abb[41] + abb[43];
z[22] = z[22] * z[156];
z[22] = z[22] + z[91] + -z[109];
z[22] = z[22] * z[162];
z[23] = z[27] * z[99];
z[25] = abb[39] * z[159];
z[12] = -2 * z[12] + z[15] + z[18] + z[22] + z[23] + z[25];
z[12] = abb[28] * z[12];
z[15] = abb[86] * z[72];
z[18] = -abb[4] + -z[28];
z[18] = abb[50] * z[18];
z[18] = -2 * z[15] + z[18];
z[22] = -z[44] + -z[55];
z[22] = z[22] * z[94];
z[23] = 2 * abb[86];
z[25] = -abb[50] + z[23];
z[27] = -z[25] + z[94];
z[28] = 4 * abb[17];
z[27] = z[27] * z[28];
z[25] = 2 * z[25];
z[29] = z[25] + -z[94];
z[29] = abb[5] * z[29];
z[31] = abb[12] * abb[86];
z[18] = 2 * z[18] + z[22] + z[27] + z[29] + 4 * z[31];
z[18] = abb[83] * z[18];
z[22] = z[40] + -z[48] + -z[66];
z[27] = 2 * abb[89];
z[22] = z[22] * z[27];
z[1] = abb[86] * z[1];
z[27] = abb[78] * abb[86];
z[1] = z[1] + z[27];
z[29] = abb[11] * z[1];
z[31] = -abb[10] * z[6];
z[31] = z[31] + -z[120];
z[31] = abb[50] * z[31];
z[32] = -abb[78] + -z[53];
z[32] = abb[9] * abb[88] * z[32];
z[29] = z[29] + z[31] + z[32];
z[31] = z[46] + -z[59] + -z[119] + z[139];
z[32] = 2 * abb[88];
z[31] = z[31] * z[32];
z[33] = 2 * abb[87];
z[34] = -z[33] * z[114];
z[35] = abb[88] * z[65];
z[37] = abb[86] * z[117];
z[32] = abb[86] + z[32];
z[32] = abb[78] * z[32];
z[32] = z[32] + z[35] + z[37];
z[35] = z[6] * z[100];
z[33] = z[33] * z[115];
z[38] = -abb[84] * abb[90];
z[32] = 2 * z[32] + z[33] + z[35] + z[38];
z[32] = abb[5] * z[32];
z[21] = -abb[87] * z[21];
z[33] = -abb[80] + z[70];
z[33] = abb[86] * z[33];
z[21] = z[21] + z[27] + z[33];
z[25] = z[25] + -3 * z[94];
z[25] = abb[83] * z[25];
z[35] = abb[42] * z[79];
z[21] = 2 * z[21] + z[25] + z[35];
z[21] = abb[6] * z[21];
z[25] = -abb[5] + -abb[6] + abb[14];
z[19] = z[19] * z[25];
z[19] = z[19] + z[47] + -z[131];
z[19] = z[19] * z[106];
z[25] = -abb[91] * z[129];
z[35] = z[39] + -z[60] + z[167];
z[35] = z[35] * z[100];
z[38] = abb[50] + z[94];
z[38] = z[6] * z[38];
z[1] = -2 * z[1] + z[38];
z[1] = z[1] * z[28];
z[28] = abb[82] * abb[86];
z[6] = -abb[49] * z[6];
z[6] = z[6] + z[27] + z[28];
z[6] = z[6] * z[56];
z[28] = abb[97] + abb[98];
z[28] = z[28] * z[141];
z[38] = abb[68] * z[108];
z[39] = abb[0] * abb[84];
z[39] = z[39] + -z[148];
z[39] = abb[90] * z[39];
z[26] = z[23] * z[26];
z[33] = -z[33] * z[41];
z[14] = -z[14] * z[15];
z[15] = z[27] + z[37];
z[15] = z[15] * z[57];
z[23] = abb[84] * z[23];
z[16] = -abb[90] * z[16];
z[16] = z[16] + z[23];
z[16] = abb[20] * z[16];
z[9] = z[9] * z[170];
z[23] = -abb[100] * z[176];
z[17] = -z[17] + z[115];
z[17] = abb[99] * z[17];
z[27] = abb[86] * z[61];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + z[17] + z[18] + z[19] + 2 * z[20] + z[21] + z[22] + z[23] + z[24] + z[25] + z[26] + z[27] + z[28] + 4 * z[29] + z[30] + z[31] + z[32] + z[33] + z[34] + z[35] + z[36] + z[38] + z[39];
return {z[5], z[0]};
}


template <typename T> std::complex<T> f_4_837_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("21.320386546457785579583542635939367404963811062742711707591249931"),stof<T>("41.907621805331554588925262824653324268568311782159758858123251945")}, std::complex<T>{stof<T>("7.716189463884924266520168352034841159627932287386486731870255224"),stof<T>("-0.5198668473063908739104816218943657540319412722144541130253495268")}, std::complex<T>{stof<T>("-7.716189463884924266520168352034841159627932287386486731870255224"),stof<T>("0.5198668473063908739104816218943657540319412722144541130253495268")}, std::complex<T>{stof<T>("11.776015237375874093086411863739370171415892975939476487701478967"),stof<T>("85.894710999888672673492452136884111553264388653177334168347901997")}, std::complex<T>{stof<T>("13.604197082572861313063374283904526245335878775356224975720994707"),stof<T>("42.427488652637945462835744446547690022600253054374212971148601472")}, std::complex<T>{stof<T>("-21.320386546457785579583542635939367404963811062742711707591249931"),stof<T>("-41.907621805331554588925262824653324268568311782159758858123251945")}, std::complex<T>{stof<T>("-5.888007618687937046543205931869685085707946487969738243850739483"),stof<T>("-42.947355499944336336746226068442055776632194326588667084173950998")}, std::complex<T>{stof<T>("26.298598948493034954678133242847087493236563599818267750289240659"),stof<T>("18.769623101993836816775779262739977968370521864467720371580540326")}, std::complex<T>{stof<T>("-11.744594664539802681323952526440144182537686854997323502521521271"),stof<T>("34.920447561433775146995720967763072623054772025276781345406222811")}, std::complex<T>{stof<T>("0.920971866576962337941148155750115482473514385517602325172084214"),stof<T>("46.892406539014338283026912180790505745106726566388320338748512867")}, std::complex<T>{stof<T>("-11.744594664539802681323952526440144182537686854997323502521521271"),stof<T>("34.920447561433775146995720967763072623054772025276781345406222811")}, std::complex<T>{stof<T>("-12.665566531116765019265100682190259665011201240514925827693605485"),stof<T>("-11.971958977580563136031191213027433122051954541111538993342290055")}, std::complex<T>{stof<T>("13.633032417376269935413032560656827828225362359303341922595635174"),stof<T>("6.797664124413273680744588049712544846318567323356181378238250271")}, std::complex<T>{stof<T>("31.115275361457826513670449515504352313048456280500870869500513354"),stof<T>("94.334644807388216797982693524221762234388740403884208578572287819")}, std::complex<T>{stof<T>("28.906423433909449776319138026216768471607120310154062862567903952"),stof<T>("-27.027666238563836864442354724890052789972501232523597615027963347")}};
	
	std::vector<C> intdlogs = {rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[112].real()/kbase.W[112].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[145]) - log_iphi_im(kbase.W[145]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_837_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_837_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-45.026063215807729003053257926354577418913717468185901161133767849"),stof<T>("-11.303732369445695951231871699928045315626257971877383059408242016")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({200});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,102> abb = {dl[0], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W15(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W67(k,dl), dlog_W113(k,dl), dlog_W119(k,dv), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W146(k,dv), dlog_W187(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_19(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), f_2_25_im(k), (log2_im(k.W[164]) - log2_im(base_point<T>.W[164])), (log2_im(k.W[166]) - log2_im(base_point<T>.W[166])), (log2_im(k.W[170]) - log2_im(base_point<T>.W[170])), (log2_im(k.W[172]) - log2_im(base_point<T>.W[172])), (log3_im(k.W[190]) - log3_im(base_point<T>.W[190])), T{0}, T{0}, T{0}, T{0}, T{0}, rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[112].real()/k.W[112].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[145]) - log_iphi_im(k.W[145])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k), f_2_25_re(k), (log2_re(k.W[164]) - log2_re(base_point<T>.W[164])), (log2_re(k.W[166]) - log2_re(base_point<T>.W[166])), (log2_re(k.W[170]) - log2_re(base_point<T>.W[170])), (log2_re(k.W[172]) - log2_re(base_point<T>.W[172])), (log3_re(k.W[190]) - log3_re(base_point<T>.W[190])), T{0}, T{0}, T{0}, T{0}, T{0}};
{
auto c = dlog_W165(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[97] = c.real();
abb[64] = c.imag();
SpDLog_Sigma5<T,200,164>(k, dv, abb[97], abb[64], f_2_32_series_coefficients<T>);
}
{
auto c = dlog_W167(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[98] = c.real();
abb[65] = c.imag();
SpDLog_Sigma5<T,200,166>(k, dv, abb[98], abb[65], f_2_32_series_coefficients<T>);
}
{
auto c = dlog_W171(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[99] = c.real();
abb[66] = c.imag();
SpDLog_Sigma5<T,200,170>(k, dv, abb[99], abb[66], f_2_32_series_coefficients<T>);
}
{
auto c = dlog_W173(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[100] = c.real();
abb[67] = c.imag();
SpDLog_Sigma5<T,200,172>(k, dv, abb[100], abb[67], f_2_32_series_coefficients<T>);
}
{
auto c = dlog_W191(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[101] = c.real();
abb[68] = c.imag();
SpDLog_Sigma5<T,200,190>(k, dv, abb[101], abb[68], f_2_32_series_coefficients<T>);
}

                    
            return f_4_837_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_837_DLogXconstant_part(base_point<T>, kend);
	value += f_4_837_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_837_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_837_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_837_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_837_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_837_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_837_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
