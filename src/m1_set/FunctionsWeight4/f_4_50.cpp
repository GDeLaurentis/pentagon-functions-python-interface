/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_50.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_50_abbreviated (const std::array<T,27>& abb) {
T z[61];
z[0] = 3 * abb[18];
z[1] = abb[23] + z[0];
z[2] = 4 * abb[20];
z[3] = 8 * abb[19] + z[2];
z[4] = abb[24] * (T(5) / T(2)) + -z[3];
z[5] = 2 * abb[22];
z[6] = -abb[21] + -z[1] + -z[4] + z[5];
z[6] = abb[2] * z[6];
z[7] = 2 * abb[24];
z[8] = -z[3] + z[7];
z[9] = 2 * abb[18];
z[10] = -abb[21] + z[9];
z[11] = 5 * abb[22];
z[12] = z[8] + -z[10] + z[11];
z[12] = abb[0] * z[12];
z[13] = abb[18] + abb[21];
z[3] = z[3] + -z[13];
z[14] = 3 * abb[24];
z[15] = z[3] + -z[14];
z[16] = 2 * abb[1];
z[17] = z[15] * z[16];
z[2] = 10 * abb[19] + z[2] + -3 * z[13];
z[18] = abb[22] + abb[23];
z[19] = abb[24] + z[18];
z[20] = -z[2] + z[19];
z[21] = abb[6] * z[20];
z[22] = abb[9] * abb[26];
z[23] = z[21] + z[22];
z[24] = abb[24] * (T(3) / T(2));
z[25] = 2 * abb[19];
z[26] = z[24] + z[25];
z[27] = 3 * abb[22];
z[28] = z[26] + -z[27];
z[29] = -abb[21] + -abb[23] + z[28];
z[29] = abb[3] * z[29];
z[2] = -abb[24] + z[2];
z[30] = z[2] + -z[5];
z[30] = abb[4] * z[30];
z[31] = abb[8] * abb[26];
z[32] = (T(1) / T(2)) * z[31];
z[33] = abb[7] + abb[8];
z[34] = abb[25] * z[33];
z[35] = abb[7] * abb[26];
z[36] = (T(3) / T(2)) * z[35];
z[6] = z[6] + z[12] + z[17] + z[23] + z[29] + z[30] + z[32] + z[34] + z[36];
z[6] = abb[11] * z[6];
z[12] = 4 * abb[1];
z[29] = z[12] * z[15];
z[29] = z[23] + z[29];
z[30] = -abb[19] + abb[21];
z[30] = -z[24] + z[27] + 2 * z[30];
z[30] = abb[0] * z[30];
z[37] = 3 * z[31] + z[36];
z[38] = 2 * abb[20];
z[39] = 4 * abb[19] + z[38];
z[14] = -z[14] + z[39];
z[40] = abb[21] + abb[22];
z[41] = z[14] + z[40];
z[42] = abb[4] * z[41];
z[43] = 2 * z[42];
z[44] = 2 * abb[25];
z[33] = z[33] * z[44];
z[45] = 2 * z[15];
z[46] = -abb[22] + abb[23];
z[47] = -z[45] + z[46];
z[47] = abb[2] * z[47];
z[48] = 3 * abb[19] + z[38];
z[49] = abb[24] * (T(9) / T(2));
z[48] = 2 * z[48] + -z[49];
z[50] = abb[23] + z[5];
z[51] = z[48] + z[50];
z[51] = abb[3] * z[51];
z[30] = -z[29] + z[30] + z[33] + z[37] + z[43] + z[47] + z[51];
z[30] = abb[13] * z[30];
z[33] = z[25] + z[38];
z[47] = -z[7] + z[33];
z[51] = z[40] + z[47];
z[51] = abb[3] * z[51];
z[52] = abb[18] + abb[23];
z[47] = z[47] + z[52];
z[47] = abb[0] * z[47];
z[53] = -5 * abb[24] + 2 * z[3] + -z[18];
z[54] = z[16] * z[53];
z[55] = 2 * abb[2];
z[56] = z[15] * z[55];
z[23] = z[23] + z[35];
z[14] = z[14] + z[52];
z[57] = abb[5] * z[14];
z[42] = -z[23] + z[42] + z[47] + z[51] + -z[54] + -z[56] + z[57];
z[47] = abb[12] * z[42];
z[29] = -z[29] + 2 * z[57];
z[51] = 9 * abb[24];
z[54] = abb[20] + z[25];
z[56] = z[18] + -z[51] + 8 * z[54];
z[56] = abb[2] * z[56];
z[57] = 3 * abb[23];
z[26] = z[26] + -z[57];
z[58] = -z[5] + z[26];
z[58] = abb[3] * z[58];
z[59] = 2 * abb[23];
z[28] = z[28] + -z[59];
z[28] = abb[0] * z[28];
z[28] = z[28] + -z[29] + z[36] + -z[43] + z[56] + z[58];
z[28] = abb[14] * z[28];
z[6] = z[6] + z[28] + z[30] + 2 * z[47];
z[6] = abb[11] * z[6];
z[28] = -abb[13] + abb[14];
z[28] = z[28] * z[42];
z[30] = 7 * abb[24];
z[36] = 5 * abb[23];
z[42] = 4 * z[3] + -z[11] + -z[30] + -z[36];
z[42] = z[16] * z[42];
z[30] = 18 * abb[19] + 8 * abb[20] + -z[30];
z[43] = 3 * abb[21];
z[47] = abb[18] + -z[30] + z[43];
z[47] = abb[5] * z[47];
z[53] = z[53] * z[55];
z[56] = abb[21] + z[33];
z[58] = abb[24] + -z[56];
z[58] = z[18] + 2 * z[58];
z[58] = abb[3] * z[58];
z[33] = abb[18] + z[33];
z[60] = abb[24] + -z[33];
z[60] = z[18] + 2 * z[60];
z[60] = abb[0] * z[60];
z[30] = abb[21] + z[0] + -z[30];
z[30] = abb[4] * z[30];
z[30] = z[23] + z[30] + z[42] + z[47] + z[53] + z[58] + z[60];
z[30] = abb[12] * z[30];
z[28] = 2 * z[28] + z[30];
z[28] = abb[12] * z[28];
z[30] = abb[22] + z[43];
z[4] = -abb[18] + -z[4] + -z[30] + z[59];
z[4] = abb[2] * z[4];
z[42] = 2 * abb[21];
z[47] = -abb[18] + z[42];
z[8] = z[8] + z[36] + -z[47];
z[8] = abb[3] * z[8];
z[2] = z[2] + -z[59];
z[2] = abb[5] * z[2];
z[26] = -abb[18] + -abb[22] + z[26];
z[26] = abb[0] * z[26];
z[53] = -abb[8] * abb[25];
z[2] = z[2] + z[4] + z[8] + z[17] + z[23] + z[26] + -z[32] + z[53];
z[2] = abb[14] * z[2];
z[4] = abb[18] + -abb[19];
z[4] = 2 * z[4] + -z[24] + z[57];
z[4] = abb[3] * z[4];
z[8] = -z[45] + -z[46];
z[8] = abb[2] * z[8];
z[17] = abb[22] + z[59];
z[23] = z[17] + z[48];
z[23] = abb[0] * z[23];
z[24] = abb[8] * z[44];
z[4] = z[4] + z[8] + z[23] + -z[24] + z[29] + -z[37];
z[4] = abb[13] * z[4];
z[2] = z[2] + z[4];
z[2] = abb[14] * z[2];
z[4] = z[21] + z[31] + z[35];
z[8] = 6 * abb[19] + z[38];
z[23] = 4 * abb[24];
z[26] = z[8] + -z[23];
z[9] = z[9] + z[43];
z[29] = -z[9] + -z[26] + z[57];
z[29] = abb[5] * z[29];
z[32] = 30 * abb[19] + 14 * abb[20] + -z[51];
z[30] = -z[30] + z[32] + -4 * z[52];
z[30] = z[16] * z[30];
z[37] = 14 * abb[19] + 6 * abb[20] + -z[23];
z[9] = z[9] + -z[37];
z[43] = -abb[22] + -z[9];
z[43] = z[43] * z[55];
z[45] = abb[23] + z[23] + -3 * z[56];
z[45] = abb[3] * z[45];
z[48] = 2 * abb[0];
z[14] = -z[14] * z[48];
z[22] = 2 * z[22];
z[4] = 2 * z[4] + z[14] + z[22] + z[29] + z[30] + z[43] + z[45];
z[4] = abb[16] * z[4];
z[14] = -5 * abb[19] + -z[38];
z[14] = abb[23] + 2 * z[14] + z[49];
z[14] = abb[7] * z[14];
z[20] = abb[9] * z[20];
z[29] = abb[8] * z[46];
z[30] = abb[6] * abb[26];
z[38] = 4 * abb[25];
z[43] = abb[10] * z[38];
z[14] = z[14] + z[20] + z[29] + z[30] + z[43];
z[14] = abb[17] * z[14];
z[1] = -z[1] + z[32] + -4 * z[40];
z[1] = abb[1] * z[1];
z[0] = z[0] + z[42];
z[20] = z[0] + -z[37];
z[29] = -abb[23] + -z[20];
z[29] = abb[2] * z[29];
z[30] = -abb[3] * z[41];
z[1] = z[1] + z[21] + z[29] + z[30] + -z[31];
z[0] = -z[0] + -z[26] + z[27];
z[0] = abb[4] * z[0];
z[21] = abb[22] + z[23] + -3 * z[33];
z[21] = abb[0] * z[21];
z[0] = z[0] + 2 * z[1] + z[21] + z[22];
z[0] = abb[15] * z[0];
z[1] = -56 * abb[19] + -26 * abb[20] + z[7] + (T(13) / T(2)) * z[13];
z[21] = abb[22] * (T(13) / T(2));
z[22] = abb[23] * (T(13) / T(2));
z[1] = (T(1) / T(3)) * z[1] + z[21] + z[22];
z[1] = abb[1] * z[1];
z[23] = z[13] + -z[25];
z[23] = -11 * abb[20] + 13 * z[23];
z[23] = abb[2] * z[23];
z[23] = z[23] + z[35];
z[25] = 28 * abb[19] + 13 * abb[20] + -abb[24];
z[26] = -abb[18] + abb[21] * (T(1) / T(2));
z[26] = z[25] + 13 * z[26];
z[22] = -z[22] + (T(1) / T(3)) * z[26];
z[22] = abb[3] * z[22];
z[26] = -abb[21] + abb[18] * (T(1) / T(2));
z[25] = z[25] + 13 * z[26];
z[21] = -z[21] + (T(1) / T(3)) * z[25];
z[21] = abb[0] * z[21];
z[1] = z[1] + z[21] + z[22] + (T(1) / T(3)) * z[23];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[21] = abb[0] + abb[3];
z[22] = z[16] + -z[21];
z[15] = z[15] * z[22];
z[22] = abb[7] * abb[25];
z[23] = z[22] + z[35];
z[13] = -z[13] + z[54];
z[25] = abb[2] * z[13];
z[15] = z[15] + -3 * z[23] + 4 * z[25];
z[15] = prod_pow(abb[13], 2) * z[15];
z[23] = abb[17] * z[44];
z[25] = abb[17] * abb[26];
z[23] = z[23] + (T(3) / T(2)) * z[25];
z[21] = -z[21] * z[23];
z[23] = -abb[17] * z[38];
z[23] = z[23] + -3 * z[25];
z[23] = abb[2] * z[23];
z[0] = z[0] + z[1] + z[2] + z[4] + z[6] + z[14] + z[15] + z[21] + z[23] + z[28];
z[1] = -z[3] + z[7] + z[18];
z[2] = -z[1] * z[16];
z[4] = z[8] + -z[10] + -z[36];
z[4] = abb[3] * z[4];
z[6] = abb[23] + z[9];
z[6] = abb[5] * z[6];
z[7] = -abb[24] + z[39];
z[9] = z[7] + -z[52];
z[10] = z[9] * z[55];
z[14] = -z[19] + 2 * z[54];
z[15] = -z[14] * z[48];
z[2] = z[2] + z[4] + z[6] + z[10] + z[15] + z[24] + 2 * z[31];
z[2] = abb[14] * z[2];
z[3] = abb[24] + -z[3] + z[5] + z[59];
z[3] = z[3] * z[12];
z[4] = z[1] * z[55];
z[5] = abb[22] + z[20];
z[5] = abb[4] * z[5];
z[10] = -z[50] + z[56];
z[10] = abb[3] * z[10];
z[12] = -z[17] + z[33];
z[12] = abb[0] * z[12];
z[3] = z[3] + z[4] + -z[5] + -z[6] + z[10] + z[12];
z[3] = abb[12] * z[3];
z[1] = abb[1] * z[1];
z[1] = z[1] + z[35];
z[4] = -z[13] * z[55];
z[6] = abb[3] * z[9];
z[7] = z[7] + -z[40];
z[9] = abb[0] * z[7];
z[4] = z[1] + z[4] + z[6] + z[9] + z[22];
z[4] = abb[13] * z[4];
z[6] = abb[2] * z[7];
z[7] = -abb[3] * z[14];
z[1] = -z[1] + z[6] + z[7] + -z[31] + -z[34];
z[6] = z[8] + -z[11] + -z[47];
z[6] = abb[0] * z[6];
z[1] = 2 * z[1] + z[5] + z[6];
z[1] = abb[11] * z[1];
z[1] = z[1] + z[2] + z[3] + 2 * z[4];
z[1] = 2 * m1_set::bc<T>[0] * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_50_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-27.889403285087090801069980359840300133101474350624794681469490382"),stof<T>("-83.196400767844090906085274414735652250291181611233973436221401978")}, std::complex<T>{stof<T>("-72.82259149892357832230631600522619241414666033586994373781921442"),stof<T>("147.09821813557976877576959808236507505806231605827848685447006468")}, std::complex<T>{stof<T>("-51.485043156627675525378910142189396073097600436838178394929161717"),stof<T>("48.152360661795769158351087595667449725253277076219544555100510636")}, std::complex<T>{stof<T>("81.676400206767532035408011094789515680798742834561082087615104311"),stof<T>("-5.334667649647456319790451842255117641966227657540240624281705879")}, std::complex<T>{stof<T>("62.33087954390197187021557766297567441691197399731788961165602121"),stof<T>("4.011486417002503095075024048738249631372191753874525630165082433")}, std::complex<T>{stof<T>("-30.722961452271725654200051441313820896957881183938102022158180716"),stof<T>("-47.192666622593818842210453926689595238404321502839753834012979621")}, std::complex<T>{stof<T>("-6.2861617571935545640236204756924383267523504807230656289121200018"),stof<T>("-7.6929847562484529013792209737114797793863885202695222950595298166")}, std::complex<T>{stof<T>("-1.091630268192701708001828241307879024172744121136462991877465036"),stof<T>("-31.497863384434761426915167224305706600330124032957193082127117469")}, std::complex<T>{stof<T>("1.586469303213321861709221708068931424263939161469902928506673507"),stof<T>("-31.497863384434761426915167224305706600330124032957193082127117469")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[123].real()/kbase.W[123].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_50_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_50_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("49.694277564353185052973548822507349488143457106271160765044407911"),stof<T>("83.653732130240188447931571566428040223531033874664911795469827396")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,27> abb = {dl[0], dl[1], dl[2], dl[4], dlog_W12(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[123].real()/k.W[123].real())};

                    
            return f_4_50_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_50_DLogXconstant_part(base_point<T>, kend);
	value += f_4_50_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_50_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_50_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_50_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_50_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_50_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_50_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
