/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_770.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_770_abbreviated (const std::array<T,81>& abb) {
T z[164];
z[0] = -abb[57] + abb[59] + -abb[60];
z[1] = 2 * abb[61];
z[2] = abb[56] + abb[62];
z[3] = z[0] + z[1] + -z[2];
z[4] = abb[27] * z[3];
z[5] = 2 * abb[67];
z[6] = abb[65] + -abb[68];
z[7] = z[5] + -z[6];
z[8] = abb[16] * z[7];
z[9] = z[4] + z[8];
z[10] = 4 * abb[9];
z[11] = abb[67] + abb[68];
z[12] = -abb[65] + z[11];
z[13] = z[10] * z[12];
z[14] = -z[9] + z[13];
z[15] = abb[23] * z[3];
z[16] = 2 * abb[68];
z[17] = abb[67] + z[16];
z[18] = abb[0] * z[17];
z[19] = abb[58] + -abb[61] + -z[0];
z[20] = abb[28] * z[19];
z[15] = z[15] + -z[18] + 4 * z[20];
z[21] = 3 * abb[6];
z[22] = 4 * abb[17] + -z[21];
z[23] = 2 * abb[8];
z[24] = 2 * abb[4];
z[25] = z[22] + z[23] + -z[24];
z[25] = abb[67] * z[25];
z[26] = 3 * abb[18];
z[27] = 2 * abb[19];
z[28] = z[26] + z[27];
z[28] = abb[69] * z[28];
z[29] = abb[6] + z[24];
z[29] = abb[65] * z[29];
z[30] = abb[8] * z[16];
z[31] = -abb[58] + -abb[61] + z[2];
z[32] = abb[25] * z[31];
z[33] = 2 * z[32];
z[34] = abb[3] * abb[68];
z[35] = abb[6] * abb[68];
z[36] = abb[1] * z[11];
z[37] = abb[65] + 4 * abb[67] + 5 * abb[68];
z[37] = abb[5] * z[37];
z[25] = z[14] + z[15] + z[25] + z[28] + z[29] + z[30] + -z[33] + 4 * z[34] + -5 * z[35] + -8 * z[36] + z[37];
z[25] = abb[32] * z[25];
z[37] = abb[24] + abb[26];
z[38] = z[31] * z[37];
z[39] = abb[15] * z[11];
z[40] = abb[22] * z[31];
z[41] = z[39] + z[40];
z[42] = z[38] + z[41];
z[43] = abb[7] * z[11];
z[44] = z[42] + z[43];
z[45] = 3 * abb[3];
z[46] = abb[5] + -abb[17];
z[47] = -abb[16] + z[10] + z[45] + 3 * z[46];
z[48] = -abb[64] * z[47];
z[49] = abb[65] + abb[68];
z[50] = 2 * z[49];
z[51] = abb[13] * z[50];
z[52] = -z[20] + z[51];
z[53] = -abb[65] + z[5];
z[54] = 3 * abb[68];
z[55] = z[53] + z[54];
z[55] = abb[5] * z[55];
z[56] = 4 * abb[68];
z[57] = -abb[65] + abb[67];
z[58] = z[56] + z[57];
z[58] = abb[3] * z[58];
z[59] = abb[17] * abb[65];
z[60] = 4 * abb[6];
z[61] = abb[17] + -z[60];
z[61] = abb[67] * z[61];
z[14] = z[14] + -4 * z[35] + -6 * z[36] + z[44] + z[48] + -z[52] + z[55] + z[58] + 3 * z[59] + z[61];
z[48] = 2 * abb[38];
z[14] = z[14] * z[48];
z[55] = 2 * abb[17];
z[58] = -abb[6] + z[55];
z[61] = 2 * abb[9];
z[62] = -abb[4] + z[61];
z[63] = -abb[5] + abb[7] + z[58] + -z[62];
z[64] = 2 * abb[3];
z[65] = z[63] + -z[64];
z[66] = abb[64] + abb[66];
z[65] = -z[65] * z[66];
z[67] = z[19] * z[37];
z[68] = abb[65] + abb[67];
z[69] = abb[7] * z[68];
z[51] = z[51] + -z[69];
z[70] = abb[4] + abb[6];
z[71] = abb[67] * z[70];
z[72] = 2 * z[36];
z[71] = z[71] + z[72];
z[73] = z[12] * z[61];
z[74] = abb[3] * z[16];
z[75] = abb[6] * z[16];
z[76] = -abb[4] + -z[58];
z[76] = abb[65] * z[76];
z[17] = -abb[65] + z[17];
z[77] = -abb[5] * z[17];
z[65] = z[51] + z[65] + -z[67] + z[71] + -z[73] + -z[74] + z[75] + z[76] + z[77];
z[76] = 2 * abb[35];
z[65] = z[65] * z[76];
z[77] = abb[18] + abb[20];
z[78] = abb[19] + z[77];
z[78] = abb[69] * z[78];
z[79] = 2 * abb[65];
z[80] = abb[68] + z[79];
z[80] = abb[5] * z[80];
z[50] = abb[10] * z[50];
z[81] = -z[50] + z[78] + z[80];
z[82] = abb[23] * z[31];
z[83] = z[42] + z[82];
z[23] = z[23] + -z[70];
z[23] = abb[67] * z[23];
z[23] = z[23] + -z[33];
z[33] = z[24] + -z[61];
z[84] = abb[5] + abb[6];
z[85] = -abb[14] + z[84];
z[86] = z[33] + z[85];
z[87] = -abb[64] * z[86];
z[88] = abb[14] * abb[68];
z[89] = abb[65] * z[24];
z[90] = abb[4] * abb[68];
z[87] = -z[23] + -z[73] + -z[81] + z[83] + z[87] + -z[88] + -z[89] + z[90];
z[91] = 2 * abb[33];
z[87] = z[87] * z[91];
z[92] = 4 * z[36];
z[93] = z[9] + z[92];
z[22] = -z[22] * z[57];
z[94] = abb[5] * z[49];
z[95] = -z[35] + z[94];
z[96] = 2 * abb[7];
z[97] = z[6] * z[96];
z[98] = abb[10] * z[49];
z[99] = 4 * z[98];
z[100] = abb[18] + 2 * abb[20] + z[27];
z[100] = abb[69] * z[100];
z[15] = -z[15] + z[22] + z[93] + z[95] + z[97] + -z[99] + z[100];
z[15] = abb[36] * z[15];
z[22] = -z[18] + z[50];
z[97] = z[20] + z[22];
z[100] = abb[14] * abb[65];
z[101] = -z[97] + z[100];
z[102] = abb[3] * z[57];
z[103] = abb[23] * z[19];
z[102] = z[102] + z[103];
z[104] = abb[18] + -abb[20];
z[104] = abb[69] * z[104];
z[105] = abb[17] * abb[67];
z[106] = -abb[6] + abb[17];
z[107] = abb[65] * z[106];
z[104] = z[67] + z[101] + z[102] + -z[104] + -z[105] + z[107];
z[105] = 2 * abb[6];
z[108] = abb[3] + -abb[17];
z[109] = abb[0] + z[105] + z[108];
z[110] = abb[64] * z[109];
z[110] = z[104] + -z[110];
z[111] = 2 * abb[34];
z[110] = z[110] * z[111];
z[112] = abb[5] * z[11];
z[113] = z[42] + z[112];
z[114] = abb[8] * abb[67];
z[115] = -z[32] + -z[82] + z[114];
z[116] = -abb[4] + abb[17];
z[117] = abb[65] * z[116];
z[118] = z[35] + -z[117];
z[119] = abb[8] * abb[68];
z[118] = z[72] + -z[74] + -z[113] + z[115] + 2 * z[118] + -z[119];
z[120] = 2 * abb[37];
z[118] = z[118] * z[120];
z[1] = 4 * abb[58] + -3 * z[0] + -z[1] + -z[2];
z[1] = abb[36] * z[1];
z[121] = abb[32] * z[3];
z[1] = z[1] + z[121];
z[1] = z[1] * z[37];
z[121] = -abb[5] + abb[16];
z[96] = -abb[0] + 2 * z[58] + z[96] + -z[121];
z[96] = abb[36] * z[96];
z[122] = abb[4] + z[105];
z[123] = -z[10] + 2 * z[122];
z[124] = abb[5] + abb[16];
z[125] = abb[0] + z[123] + z[124];
z[125] = abb[32] * z[125];
z[126] = abb[32] + abb[36];
z[126] = abb[14] * z[126];
z[96] = -z[96] + -z[125] + z[126];
z[86] = z[86] * z[91];
z[125] = z[109] * z[111];
z[86] = z[86] + z[96] + z[125];
z[47] = -z[47] * z[48];
z[126] = -abb[5] + z[116];
z[127] = abb[37] * z[126];
z[128] = 4 * z[127];
z[47] = z[47] + -z[86] + -z[128];
z[47] = abb[66] * z[47];
z[96] = -z[96] + -z[128];
z[96] = abb[64] * z[96];
z[128] = abb[36] * z[49];
z[129] = -abb[32] * z[6];
z[129] = z[128] + z[129];
z[129] = abb[14] * z[129];
z[130] = abb[37] * z[49];
z[128] = -z[128] + z[130];
z[130] = 4 * abb[13];
z[131] = z[128] * z[130];
z[1] = z[1] + z[14] + z[15] + z[25] + z[47] + z[65] + z[87] + z[96] + z[110] + z[118] + z[129] + z[131];
z[14] = abb[0] + z[64];
z[15] = -abb[16] + z[14] + -z[58] + z[61];
z[25] = abb[63] + z[66];
z[15] = z[15] * z[25];
z[47] = z[73] + z[103];
z[65] = z[47] + z[67];
z[87] = -abb[65] + z[16];
z[96] = -z[5] + -z[87];
z[96] = abb[5] * z[96];
z[118] = -z[12] * z[64];
z[129] = abb[19] + abb[20];
z[131] = abb[69] * z[129];
z[132] = -abb[4] + z[21];
z[132] = abb[68] * z[132];
z[133] = -abb[65] * z[58];
z[134] = -abb[4] + z[105];
z[135] = abb[67] * z[134];
z[15] = z[15] + z[22] + -z[41] + -z[65] + z[93] + z[96] + -z[100] + z[118] + -z[131] + z[132] + z[133] + z[135];
z[15] = abb[31] * z[15];
z[1] = 2 * z[1] + 4 * z[15];
z[1] = m1_set::bc<T>[0] * z[1];
z[15] = abb[4] + -abb[6];
z[96] = abb[68] * z[15];
z[89] = z[89] + -z[96];
z[100] = z[43] + z[73];
z[118] = z[100] + -z[119];
z[132] = z[41] + z[82];
z[70] = -abb[8] + z[70];
z[70] = abb[67] * z[70];
z[32] = z[32] + z[70] + -z[89] + -z[118] + z[132];
z[70] = abb[49] * z[32];
z[133] = abb[6] + abb[8];
z[135] = z[116] + z[133];
z[135] = abb[67] * z[135];
z[136] = abb[4] + z[106];
z[136] = abb[65] * z[136];
z[73] = -z[73] + -z[102] + z[135] + -z[136];
z[87] = abb[8] * z[87];
z[135] = -z[73] + -z[87];
z[135] = abb[48] * z[135];
z[137] = abb[8] * abb[65];
z[138] = abb[8] + -abb[17];
z[139] = abb[67] * z[138];
z[137] = z[59] + -z[137] + z[139];
z[68] = abb[5] * z[68];
z[68] = z[68] + z[102];
z[139] = z[68] + -z[69] + -z[72] + -z[137];
z[139] = abb[50] * z[139];
z[140] = z[35] + z[72];
z[112] = z[112] + -z[140];
z[133] = abb[67] * z[133];
z[141] = abb[3] * z[12];
z[133] = -z[112] + z[133] + -z[136] + -z[141];
z[136] = abb[8] * z[6];
z[142] = z[47] + -z[133] + z[136];
z[142] = abb[51] * z[142];
z[143] = abb[25] * z[19];
z[137] = z[137] + -z[143];
z[144] = abb[5] * abb[65];
z[101] = z[101] + z[131] + z[137] + -z[144];
z[131] = abb[0] + z[138];
z[138] = z[66] * z[131];
z[138] = -z[101] + z[138];
z[138] = abb[46] * z[138];
z[114] = z[114] + z[119];
z[129] = -abb[18] + -z[129];
z[129] = abb[69] * z[129];
z[129] = z[35] + -z[114] + z[129];
z[129] = abb[47] * z[129];
z[145] = abb[50] * z[19];
z[146] = abb[48] * z[19];
z[147] = abb[65] * abb[78];
z[147] = z[145] + -z[146] + z[147];
z[147] = abb[28] * z[147];
z[148] = abb[47] * abb[68];
z[79] = -abb[47] * z[79];
z[79] = z[79] + -z[148];
z[79] = abb[5] * z[79];
z[149] = abb[47] * abb[65];
z[149] = z[148] + z[149];
z[149] = abb[10] * z[149];
z[150] = abb[47] * abb[67];
z[148] = z[148] + z[150];
z[148] = abb[7] * z[148];
z[150] = abb[68] * abb[78];
z[151] = abb[23] * z[150];
z[152] = abb[47] * z[88];
z[70] = z[70] + z[79] + z[129] + z[135] + z[138] + z[139] + z[142] + z[147] + z[148] + 2 * z[149] + z[151] + -z[152];
z[79] = -z[9] + z[96];
z[96] = z[24] + z[106];
z[96] = abb[65] * z[96];
z[129] = abb[8] + z[106];
z[135] = -abb[16] + z[33] + z[129];
z[138] = z[66] * z[135];
z[87] = z[20] + z[87];
z[139] = abb[4] + abb[8];
z[142] = abb[17] + z[139];
z[142] = abb[67] * z[142];
z[41] = -z[41] + z[65] + -z[79] + -z[87] + z[96] + z[138] + -z[142] + z[143];
z[41] = 4 * z[41];
z[96] = abb[45] * z[41];
z[129] = -abb[3] + z[129];
z[138] = abb[4] + -z[61] + z[129];
z[138] = abb[48] * z[138];
z[129] = -z[62] + z[129];
z[142] = abb[51] * z[129];
z[85] = abb[47] * z[85];
z[147] = abb[28] * abb[78];
z[148] = -abb[4] + abb[9];
z[149] = abb[49] * z[148];
z[85] = -z[85] + z[138] + z[142] + z[147] + 2 * z[149];
z[138] = abb[7] + -abb[8];
z[142] = 2 * abb[13];
z[147] = abb[3] + -z[46] + z[138] + -z[142];
z[149] = -abb[50] * z[147];
z[151] = abb[45] * z[135];
z[152] = abb[46] * z[131];
z[149] = z[85] + z[149] + z[151] + z[152];
z[151] = 3 * abb[17];
z[10] = -z[10] + z[151];
z[152] = -abb[3] + z[121];
z[153] = z[10] + -z[142] + z[152];
z[153] = z[48] * z[153];
z[154] = -abb[36] + abb[37];
z[130] = z[130] * z[154];
z[63] = -z[63] + z[142];
z[63] = z[63] * z[76];
z[155] = abb[37] * z[116];
z[63] = z[63] + -z[86] + z[130] + z[153] + -4 * z[155];
z[63] = m1_set::bc<T>[0] * z[63];
z[86] = abb[19] + -abb[20];
z[130] = 2 * abb[18] + abb[21] + z[86];
z[153] = -abb[52] * z[130];
z[63] = z[63] + 2 * z[149] + z[153];
z[149] = 2 * abb[63];
z[63] = z[63] * z[149];
z[153] = abb[5] + z[108];
z[138] = z[138] + -z[153];
z[155] = abb[50] * z[138];
z[85] = -z[85] + z[155];
z[155] = -4 * z[66];
z[85] = z[85] * z[155];
z[155] = abb[21] * z[7];
z[156] = abb[20] * abb[68];
z[157] = abb[19] * z[49];
z[158] = abb[65] * z[77];
z[159] = abb[18] * z[11];
z[155] = -z[155] + z[156] + z[157] + z[158] + -z[159];
z[156] = 2 * abb[5];
z[157] = abb[0] + abb[6];
z[158] = -8 * abb[30] + z[156] + z[157];
z[159] = 2 * abb[14];
z[160] = -z[158] + -z[159];
z[160] = abb[69] * z[160];
z[161] = -z[66] * z[130];
z[160] = -z[155] + z[160] + z[161];
z[161] = abb[29] * z[3];
z[161] = 2 * z[161];
z[160] = 2 * z[160] + z[161];
z[160] = abb[52] * z[160];
z[162] = -abb[47] * z[31];
z[163] = abb[51] * z[19];
z[146] = -z[146] + -z[150] + z[162] + -z[163];
z[150] = -4 * z[37];
z[150] = z[146] * z[150];
z[145] = z[145] + -z[146];
z[146] = 4 * abb[25];
z[145] = z[145] * z[146];
z[162] = abb[13] * z[49];
z[163] = abb[50] * z[162];
z[1] = abb[79] + abb[80] + z[1] + z[63] + 4 * z[70] + z[85] + z[96] + z[145] + z[150] + z[160] + 8 * z[163];
z[63] = 2 * abb[0] + abb[3] + -abb[14] + -abb[16] + -z[106] + z[139];
z[25] = z[25] * z[63];
z[63] = z[136] + z[143];
z[70] = abb[14] * z[6];
z[85] = -abb[8] + -z[15];
z[85] = abb[67] * z[85];
z[96] = -abb[5] * z[12];
z[139] = abb[18] + -abb[19];
z[139] = abb[69] * z[139];
z[25] = -2 * z[18] + z[25] + -z[43] + z[50] + z[63] + -z[70] + z[72] + -z[79] + z[82] + z[85] + z[96] + -z[117] + z[139] + -z[141];
z[25] = abb[31] * z[25];
z[70] = -2 * abb[58] + z[0] + z[2];
z[79] = abb[23] * z[70];
z[85] = -3 * z[18] + 2 * z[20] + z[79] + z[99];
z[96] = 2 * z[40];
z[13] = z[13] + z[96];
z[99] = -z[21] + z[55];
z[99] = abb[67] * z[99];
z[139] = -3 * abb[65] + abb[68] + z[5];
z[139] = abb[5] * z[139];
z[26] = abb[69] * z[26];
z[26] = z[13] + z[26] + z[29] + -z[30] + -3 * z[35] + z[74] + z[85] + -z[93] + z[99] + z[139];
z[26] = abb[32] * z[26];
z[7] = abb[5] * z[7];
z[29] = -abb[17] + z[105];
z[29] = abb[67] * z[29];
z[29] = -z[20] + z[29];
z[17] = abb[3] * z[17];
z[7] = -z[7] + -z[17] + z[29] + -z[59] + z[75] + z[93] + -z[100];
z[17] = -z[61] + -z[108] + z[121];
z[30] = abb[64] * z[17];
z[30] = -z[7] + z[30] + z[42];
z[30] = z[30] * z[48];
z[42] = -z[57] * z[58];
z[27] = -abb[18] + z[27];
z[27] = abb[69] * z[27];
z[27] = z[9] + z[27] + z[35] + z[42] + -z[85] + z[94];
z[27] = abb[36] * z[27];
z[42] = abb[5] * z[57];
z[42] = z[34] + z[42];
z[58] = abb[4] + abb[17];
z[58] = abb[65] * z[58];
z[58] = z[38] + z[42] + z[58] + -z[71] + -z[90] + z[118];
z[61] = -z[58] * z[120];
z[71] = 3 * abb[0] + z[121];
z[74] = z[71] + z[123];
z[74] = abb[32] * z[74];
z[71] = -z[71] + 2 * z[106];
z[71] = abb[36] * z[71];
z[46] = z[46] + z[62];
z[62] = z[46] * z[120];
z[85] = abb[32] + -abb[36];
z[93] = abb[14] * z[85];
z[62] = -z[62] + -z[71] + -z[74] + z[93];
z[71] = z[17] * z[48];
z[74] = -abb[14] + z[157];
z[94] = z[74] * z[91];
z[71] = z[62] + -z[71] + z[94] + z[125];
z[94] = -abb[66] * z[71];
z[99] = 2 * z[129];
z[105] = -abb[35] * z[99];
z[71] = -z[71] + z[105];
z[71] = abb[63] * z[71];
z[62] = -abb[64] * z[62];
z[105] = -z[63] + z[133];
z[108] = -z[66] * z[129];
z[65] = -z[65] + z[105] + z[108];
z[65] = z[65] * z[76];
z[88] = -z[18] + z[88];
z[108] = z[35] + z[43];
z[77] = abb[69] * z[77];
z[77] = z[77] + z[88] + -z[108];
z[118] = -abb[64] * z[74];
z[118] = -z[77] + z[118] + -z[132];
z[118] = z[91] * z[118];
z[123] = -z[39] * z[85];
z[125] = abb[36] * z[40];
z[123] = z[123] + z[125];
z[70] = z[37] * z[70];
z[85] = z[70] * z[85];
z[93] = -z[6] * z[93];
z[25] = z[25] + z[26] + z[27] + z[30] + z[61] + z[62] + z[65] + z[71] + z[85] + z[93] + z[94] + z[110] + z[118] + -2 * z[123];
z[25] = abb[31] * z[25];
z[26] = z[98] + z[136];
z[27] = 5 * abb[6];
z[24] = -abb[17] + -z[24] + z[27];
z[24] = abb[65] * z[24];
z[30] = z[5] + -z[49];
z[61] = -z[30] * z[156];
z[62] = -abb[4] + 13 * abb[8];
z[62] = abb[67] * z[62];
z[56] = abb[2] * z[56];
z[65] = -5 * abb[67] + 4 * z[6];
z[65] = abb[3] * z[65];
z[4] = -2 * z[4] + z[24] + -8 * z[26] + z[56] + z[61] + z[62] + z[65] + z[69] + -z[75];
z[24] = -abb[4] + abb[16];
z[26] = abb[7] + -abb[17];
z[56] = abb[9] * (T(4) / T(3));
z[61] = abb[14] * (T(2) / T(3));
z[21] = abb[2] * (T(4) / T(3)) + abb[8] * (T(8) / T(3)) + -z[21] + (T(-2) / T(3)) * z[24] + (T(-1) / T(3)) * z[26] + -z[56] + z[61];
z[24] = abb[3] * (T(2) / T(3)) + -z[21];
z[24] = z[24] * z[66];
z[26] = z[78] + -z[162];
z[62] = abb[61] * (T(-8) / T(3)) + abb[58] * (T(2) / T(3)) + (T(-5) / T(3)) * z[0] + z[2];
z[62] = -z[37] * z[62];
z[65] = -abb[58] + 3 * abb[61] + 2 * z[0] + -z[2];
z[65] = abb[23] * z[65];
z[56] = -z[12] * z[56];
z[0] = 13 * abb[61] + 8 * z[0] + -5 * z[2];
z[0] = -abb[58] + (T(1) / T(3)) * z[0];
z[0] = abb[25] * z[0];
z[2] = z[49] * z[61];
z[0] = z[0] + z[2] + (T(1) / T(3)) * z[4] + (T(-2) / T(3)) * z[8] + z[24] + (T(4) / T(3)) * z[26] + (T(8) / T(3)) * z[36] + z[56] + z[62] + z[65];
z[2] = prod_pow(m1_set::bc<T>[0], 2);
z[0] = z[0] * z[2];
z[4] = z[57] * z[64];
z[8] = abb[6] * abb[67];
z[24] = -abb[6] * abb[65];
z[4] = z[4] + -z[8] + -z[9] + z[18] + z[24] + -z[28] + 2 * z[43] + -z[79] + -z[95];
z[4] = abb[36] * z[4];
z[18] = z[34] + z[119];
z[24] = abb[19] * abb[69];
z[26] = z[24] + z[117];
z[28] = -abb[65] + -z[11];
z[28] = abb[5] * z[28];
z[28] = -z[18] + z[22] + -z[26] + z[28] + z[140];
z[28] = abb[32] * z[28];
z[4] = z[4] + z[28];
z[4] = abb[32] * z[4];
z[14] = z[14] + z[60] + -z[121];
z[14] = abb[36] * z[14];
z[28] = -abb[0] + z[116];
z[43] = abb[32] * z[28];
z[14] = z[14] + z[43];
z[14] = abb[32] * z[14];
z[56] = abb[28] * abb[53];
z[57] = abb[71] * z[131];
z[56] = z[56] + -z[57];
z[57] = abb[73] + abb[76];
z[57] = z[57] * z[99];
z[60] = 2 * abb[72];
z[61] = abb[32] * abb[36];
z[62] = z[60] + -z[61];
z[62] = abb[14] * z[62];
z[65] = abb[3] + z[134];
z[71] = 2 * abb[42];
z[65] = z[65] * z[71];
z[71] = 2 * abb[2] + -z[126];
z[75] = 2 * abb[43];
z[71] = z[71] * z[75];
z[60] = z[60] * z[84];
z[75] = 4 * abb[74];
z[78] = z[75] * z[148];
z[79] = abb[77] * z[130];
z[14] = z[14] + -2 * z[56] + z[57] + -z[60] + z[62] + z[65] + -z[71] + z[78] + -z[79];
z[56] = abb[36] * z[64];
z[57] = abb[32] * z[126];
z[56] = z[56] + z[57];
z[60] = abb[37] * z[46];
z[56] = 2 * z[56] + z[60];
z[56] = abb[37] * z[56];
z[62] = -abb[6] + abb[7];
z[45] = z[45] + z[62] + z[124];
z[65] = 2 * abb[40];
z[45] = z[45] * z[65];
z[45] = z[45] + z[56];
z[56] = -z[14] + z[45];
z[56] = abb[64] * z[56];
z[71] = z[120] * z[148];
z[74] = -abb[36] * z[74];
z[78] = abb[0] + -abb[5];
z[79] = abb[32] * z[78];
z[71] = z[71] + z[74] + z[79];
z[33] = -z[33] + z[78];
z[74] = abb[33] * z[33];
z[74] = 2 * z[71] + -z[74];
z[74] = abb[33] * z[74];
z[79] = abb[36] * z[109];
z[43] = z[43] + z[79];
z[79] = abb[34] * z[28];
z[79] = 2 * z[43] + -z[79];
z[79] = abb[34] * z[79];
z[84] = abb[44] * z[46];
z[14] = -z[14] + -z[74] + z[79] + 2 * z[84];
z[17] = abb[32] * z[17];
z[74] = abb[7] + z[106];
z[79] = z[64] + z[74];
z[79] = abb[36] * z[79];
z[84] = abb[9] + z[153];
z[85] = z[84] * z[120];
z[79] = z[17] + z[79] + z[85];
z[10] = z[10] + -z[62];
z[85] = 6 * abb[3] + 4 * abb[5] + -z[10];
z[93] = abb[38] * z[85];
z[93] = -2 * z[79] + z[93];
z[93] = abb[38] * z[93];
z[45] = z[14] + z[45] + z[93];
z[45] = abb[66] * z[45];
z[93] = abb[73] * z[19];
z[94] = abb[72] * z[31];
z[93] = z[93] + z[94];
z[94] = abb[42] * z[19];
z[98] = abb[76] * z[19];
z[94] = -z[93] + z[94] + -z[98];
z[98] = abb[53] * z[16];
z[94] = -2 * z[94] + -z[98];
z[98] = -z[3] * z[61];
z[98] = -z[94] + z[98];
z[98] = abb[26] * z[98];
z[99] = -z[61] + -z[65];
z[99] = z[3] * z[99];
z[94] = -z[94] + z[99];
z[94] = abb[24] * z[94];
z[0] = z[0] + z[4] + z[25] + z[45] + z[56] + z[94] + z[98];
z[3] = -abb[26] * z[3];
z[4] = abb[3] + abb[5];
z[4] = z[4] * z[30];
z[5] = z[5] + z[49];
z[5] = abb[7] * z[5];
z[25] = abb[6] * z[53];
z[3] = z[3] + z[4] + z[5] + -z[9] + z[25] + z[140];
z[3] = abb[40] * z[3];
z[4] = abb[64] * z[46];
z[4] = z[4] + -z[58];
z[4] = abb[44] * z[4];
z[5] = abb[4] * abb[67];
z[9] = z[5] + z[90];
z[25] = -abb[14] + z[78];
z[30] = z[25] * z[66];
z[45] = abb[5] * abb[68];
z[46] = -abb[69] * z[86];
z[30] = -z[9] + z[30] + -z[45] + z[46] + z[83] + z[88];
z[30] = abb[39] * z[30];
z[46] = z[66] * z[138];
z[46] = z[46] + -z[51];
z[51] = abb[63] * z[147];
z[53] = -z[20] + z[72];
z[51] = z[46] + z[51] + z[53] + -z[68] + z[137];
z[51] = abb[75] * z[51];
z[56] = z[95] + z[117];
z[16] = -abb[2] * z[16];
z[16] = z[16] + z[34] + z[50] + -z[56] + -z[119];
z[16] = abb[43] * z[16];
z[15] = abb[65] * z[15];
z[50] = -abb[18] + -z[86];
z[50] = abb[69] * z[50];
z[5] = -z[5] + z[15] + z[50] + z[102] + -z[144];
z[5] = abb[42] * z[5];
z[15] = abb[67] + -abb[68];
z[50] = abb[3] * z[15];
z[15] = -abb[5] * z[15];
z[9] = z[9] + z[15] + -z[44] + z[50] + z[72];
z[9] = abb[41] * z[9];
z[15] = z[81] + -z[108] + z[114];
z[15] = abb[72] * z[15];
z[44] = z[73] + z[87];
z[44] = abb[73] * z[44];
z[47] = -z[47] + z[105];
z[47] = abb[76] * z[47];
z[66] = abb[71] * z[101];
z[68] = abb[23] + abb[25];
z[68] = abb[68] * z[68];
z[72] = abb[28] * abb[65];
z[68] = z[68] + z[72];
z[68] = abb[53] * z[68];
z[3] = z[3] + z[4] + z[5] + z[9] + z[15] + z[16] + z[30] + z[44] + z[47] + z[51] + z[66] + z[68];
z[4] = -abb[67] + z[54];
z[5] = -z[4] * z[64];
z[9] = abb[64] * z[85];
z[15] = 2 * z[39];
z[16] = abb[4] + z[27];
z[16] = abb[68] * z[16];
z[27] = -abb[6] + -z[151];
z[27] = abb[65] * z[27];
z[30] = 6 * abb[6] + -z[116];
z[30] = abb[67] * z[30];
z[39] = -abb[67] + -z[54];
z[39] = abb[5] * z[39];
z[44] = abb[7] * z[6];
z[5] = z[5] + z[9] + -z[13] + -z[15] + z[16] + z[27] + z[30] + 10 * z[36] + z[39] + z[44] + z[52] + -z[70];
z[5] = abb[38] * z[5];
z[7] = z[7] + -z[40];
z[7] = abb[32] * z[7];
z[9] = -z[11] * z[122];
z[11] = abb[65] * z[55];
z[4] = abb[3] * z[4];
z[4] = z[4] + z[9] + z[11] + -z[92] + z[100] + z[113];
z[4] = abb[37] * z[4];
z[9] = z[69] + z[107];
z[11] = -z[9] + -z[29] + -z[35] + -z[50] + -z[92];
z[11] = abb[36] * z[11];
z[13] = -abb[64] * z[79];
z[16] = abb[36] * z[19];
z[19] = abb[32] * z[31];
z[16] = z[16] + z[19];
z[16] = -z[16] * z[37];
z[19] = z[128] * z[142];
z[4] = z[4] + z[7] + z[11] + z[13] + z[16] + -z[19] + z[123];
z[4] = 2 * z[4] + z[5];
z[4] = z[4] * z[48];
z[5] = -z[22] + z[24] + z[80];
z[7] = abb[64] * z[33];
z[7] = -z[5] + z[7] + z[15] + -z[23] + z[38] + 2 * z[82] + -z[89] + z[96] + -z[100];
z[7] = abb[33] * z[7];
z[11] = -abb[37] * z[32];
z[13] = -z[38] + -z[40] + z[115];
z[5] = z[5] + z[13] + z[119];
z[5] = abb[32] * z[5];
z[15] = z[77] + z[82];
z[15] = abb[36] * z[15];
z[16] = -abb[64] * z[71];
z[5] = z[5] + z[11] + z[15] + z[16] + z[123];
z[5] = 2 * z[5] + z[7];
z[5] = z[5] * z[91];
z[7] = -abb[3] + abb[13];
z[7] = abb[5] * (T(2) / T(3)) + (T(-4) / T(3)) * z[7] + -z[21];
z[2] = z[2] * z[7];
z[7] = z[142] * z[154];
z[11] = abb[36] * z[74];
z[7] = z[7] + z[11];
z[15] = abb[9] + -abb[17];
z[16] = -z[15] * z[120];
z[16] = -z[7] + z[16] + -z[17];
z[10] = -z[10] + z[142];
z[10] = abb[38] * z[10];
z[10] = z[10] + 2 * z[16];
z[10] = abb[38] * z[10];
z[15] = -abb[13] + -z[15];
z[15] = z[15] * z[48];
z[16] = -abb[5] + -z[116];
z[16] = abb[37] * z[16];
z[7] = z[7] + z[15] + z[16];
z[15] = -abb[35] * z[147];
z[7] = 2 * z[7] + z[15];
z[7] = abb[35] * z[7];
z[15] = abb[70] * z[135];
z[16] = abb[39] * z[25];
z[15] = -z[15] + z[16];
z[16] = z[62] + z[152];
z[16] = z[16] * z[65];
z[17] = 2 * z[57] + z[60];
z[17] = abb[37] * z[17];
z[2] = z[2] + z[7] + z[10] + z[14] + 2 * z[15] + z[16] + z[17];
z[2] = z[2] * z[149];
z[7] = -z[35] + z[59];
z[10] = abb[9] * z[12];
z[12] = -abb[64] * z[84];
z[10] = z[7] + -z[8] + z[10] + z[12] + z[34] + -z[36] + z[45] + -z[162];
z[10] = z[10] * z[48];
z[12] = -abb[67] * z[106];
z[9] = z[9] + z[12] + z[53] + z[67];
z[9] = abb[36] * z[9];
z[12] = abb[67] * z[116];
z[14] = z[12] + z[18] + z[20] + z[112];
z[14] = abb[32] * z[14];
z[15] = -z[18] + -z[56];
z[15] = abb[37] * z[15];
z[11] = -z[11] + z[127];
z[16] = -z[48] * z[84];
z[16] = -z[11] + z[16];
z[16] = abb[66] * z[16];
z[11] = -abb[64] * z[11];
z[9] = z[9] + z[10] + z[11] + z[14] + z[15] + z[16] + z[19];
z[10] = abb[4] + -abb[8];
z[10] = abb[67] * z[10];
z[11] = abb[5] * z[6];
z[14] = abb[67] + -z[49];
z[14] = abb[3] * z[14];
z[7] = -z[7] + z[10] + z[11] + z[14] + -z[46] + z[63] + z[103];
z[7] = abb[35] * z[7];
z[7] = z[7] + 2 * z[9];
z[7] = z[7] * z[76];
z[9] = -abb[70] * z[41];
z[10] = -abb[37] * z[58];
z[11] = -z[13] + z[42] + z[117] + -z[140];
z[11] = abb[32] * z[11];
z[8] = z[8] + z[50] + z[140];
z[8] = abb[36] * z[8];
z[8] = z[8] + z[11] + -z[123];
z[8] = 2 * z[8] + z[10];
z[8] = z[8] * z[120];
z[10] = -abb[36] * z[104];
z[11] = -z[12] + z[26] + -z[97] + z[144];
z[12] = abb[32] * z[11];
z[13] = abb[64] * z[43];
z[10] = z[10] + z[12] + z[13];
z[12] = -abb[64] * z[28];
z[11] = -z[11] + z[12];
z[11] = abb[34] * z[11];
z[10] = 2 * z[10] + z[11];
z[10] = z[10] * z[111];
z[11] = abb[69] * z[158];
z[11] = z[11] + z[155];
z[11] = 2 * z[11] + -z[161];
z[11] = abb[77] * z[11];
z[12] = -z[32] * z[75];
z[13] = abb[69] * abb[77];
z[14] = abb[42] * abb[65];
z[15] = abb[68] * abb[72];
z[13] = z[13] + z[14] + z[15];
z[6] = z[6] * z[61];
z[6] = z[6] + 2 * z[13];
z[6] = z[6] * z[159];
z[13] = -z[93] * z[146];
z[0] = abb[54] + abb[55] + 2 * z[0] + z[2] + 4 * z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_770_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("32.956844600689461635090114165883724737052826743171725653297932899"),stof<T>("-10.757002055713661982612541008703347206520567675858769933891847136")}, std::complex<T>{stof<T>("48.389223528459310168130450869953407056308691317944699117038443347"),stof<T>("-11.796735750326443730433504252492078714584450220287678159942546189")}, std::complex<T>{stof<T>("-32.956844600689461635090114165883724737052826743171725653297932899"),stof<T>("10.757002055713661982612541008703347206520567675858769933891847136")}, std::complex<T>{stof<T>("32.956844600689461635090114165883724737052826743171725653297932899"),stof<T>("-10.757002055713661982612541008703347206520567675858769933891847136")}, std::complex<T>{stof<T>("-17.524465672919613102049777461814042417796962168398752189557422451"),stof<T>("9.717268361100880234791577764914615698456685131429861707841148082")}, std::complex<T>{stof<T>("-15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("-14.032463215616331487562664731618874572918008853918306446529061408"),stof<T>("-9.695945678801458658118730142104924523412378992113788371423579329")}, std::complex<T>{stof<T>("-30.401708866891955532861160461496932199443108066054951460687586595"),stof<T>("-45.000097283519233781222127188524795380688324307298384443448994413")}, std::complex<T>{stof<T>("-1.132699960112929679730911814059604533150465662833572213217989601"),stof<T>("36.105115637866145651414687547245681925152780039840012880878284257")}, std::complex<T>{stof<T>("-30.401708866891955532861160461496932199443108066054951460687586595"),stof<T>("-45.000097283519233781222127188524795380688324307298384443448994413")}, std::complex<T>{stof<T>("-3.9318187404805197820521584578624244708205094635939562581493146446"),stof<T>("-1.0306356838971807472985840279556750782218872016936305185446502025")}, std::complex<T>{stof<T>("14.977800989703972452159484215422222710863785581005329288414524704"),stof<T>("67.777713173891206101367114103938205383416903529136989134497599514")}, std::complex<T>{stof<T>("0.051881463697184215754529699493716126989310907775594881303416497"),stof<T>("-25.098197810448281517736238067280671084815067568294361227844109094")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_770_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_770_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (-m1_set::bc<T>[0] * (-1 * v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (4 * m1_set::bc<T>[0] * (v[3] + -v[5])) / tend;


		return (abb[63] + abb[64] + abb[66]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[63] + -abb[64] + -abb[66];
z[1] = abb[32] + -abb[34];
return 8 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_770_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(4)) * (-v[3] + v[5]) * (4 * m1_set::bc<T>[1] * (4 + v[3] + v[5]) + 8 * (-2 * (1 + v[3]) + v[5]) + -m1_set::bc<T>[2] * (12 * v[0] + -4 * v[1] + 3 * (8 + v[3] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = (2 * (-2 + 2 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (-v[3] + v[5])) / tend;


		return (abb[63] + abb[64] + abb[66]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[32] + abb[34];
z[1] = abb[31] + -abb[36];
z[0] = z[0] * z[1];
z[0] = abb[39] + abb[42] + -abb[43] + z[0];
z[1] = abb[63] + abb[64] + abb[66];
return 8 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_770_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (4 * m1_set::bc<T>[0] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (abb[64] + abb[66] + abb[67]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[64] + -abb[66] + -abb[67];
z[1] = abb[36] + -abb[38];
return 8 * abb[12] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_770_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4]) * (24 + 4 * v[0] + v[1] + -3 * v[2] + -v[3] + -5 * v[4] + -4 * v[5])) / prod_pow(tend, 2);
c[1] = (12 * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[64] + abb[66] + abb[67]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[35] + -abb[37] + 2 * abb[38];
z[1] = -abb[36] + abb[38];
z[0] = z[0] * z[1];
z[0] = 3 * abb[40] + z[0];
z[1] = -abb[64] + -abb[66] + -abb[67];
return 8 * abb[12] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_770_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-37.733026168577779341887047151199953132821300782053757322306070688"),stof<T>("-11.447849602437662442185975274660668748242668699790036546348267891")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,81> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W20(k,dl), dlog_W25(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_14(k), f_2_15(k), f_2_16(k), f_2_17(k), f_2_18(k), f_2_2_im(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_9_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), f_2_27_im(k), T{0}, T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_2_re(k), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_9_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k), f_2_27_re(k), T{0}, T{0}};
abb[54] = SpDLog_f_4_770_W_16_Im(t, path, abb);
abb[55] = SpDLog_f_4_770_W_20_Im(t, path, abb);
abb[79] = SpDLog_f_4_770_W_16_Re(t, path, abb);
abb[80] = SpDLog_f_4_770_W_20_Re(t, path, abb);

                    
            return f_4_770_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_770_DLogXconstant_part(base_point<T>, kend);
	value += f_4_770_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_770_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_770_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_770_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_770_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_770_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_770_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
