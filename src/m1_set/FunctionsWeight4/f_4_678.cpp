/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_678.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_678_abbreviated (const std::array<T,38>& abb) {
T z[74];
z[0] = 2 * abb[24];
z[1] = -2 * abb[25] + 6 * abb[29];
z[2] = abb[28] + z[1];
z[3] = 3 * abb[27];
z[4] = 5 * abb[26];
z[5] = z[0] + -z[2] + z[3] + z[4];
z[6] = 2 * abb[4];
z[5] = z[5] * z[6];
z[7] = -abb[28] + z[1];
z[8] = 3 * abb[23];
z[9] = -abb[24] + z[8];
z[10] = 4 * abb[27];
z[11] = z[7] + -z[9] + -z[10];
z[11] = abb[3] * z[11];
z[12] = 5 * abb[28];
z[13] = -6 * abb[25] + 18 * abb[29];
z[14] = z[12] + -z[13];
z[15] = 2 * abb[27];
z[16] = abb[23] + z[15];
z[17] = 6 * abb[26];
z[18] = 5 * abb[24] + z[17];
z[19] = z[14] + z[16] + z[18];
z[19] = abb[6] * z[19];
z[20] = 4 * abb[26];
z[21] = z[10] + z[20];
z[22] = abb[23] + abb[24];
z[23] = z[14] + z[21] + 3 * z[22];
z[24] = 2 * abb[8];
z[23] = z[23] * z[24];
z[24] = 2 * abb[30] + abb[31] + -abb[32];
z[25] = 2 * z[24];
z[26] = abb[9] * z[25];
z[26] = z[23] + z[26];
z[27] = 3 * abb[28];
z[28] = -z[15] + z[27];
z[29] = -abb[23] + abb[24];
z[30] = -z[20] + z[28] + -z[29];
z[31] = abb[7] * z[30];
z[13] = 7 * abb[28] + -z[13];
z[32] = 5 * abb[27];
z[33] = 6 * abb[23] + -abb[26] + z[13] + z[32];
z[33] = abb[5] * z[33];
z[34] = z[1] + -z[22];
z[35] = abb[26] + abb[27];
z[36] = 2 * abb[28] + -z[34] + z[35];
z[37] = abb[0] * z[36];
z[38] = 2 * abb[26];
z[39] = -abb[28] + z[38];
z[40] = z[29] + z[39];
z[41] = abb[2] * z[40];
z[42] = abb[11] * z[25];
z[5] = z[5] + -z[11] + z[19] + -z[26] + 2 * z[31] + z[33] + z[37] + 5 * z[41] + z[42];
z[11] = -abb[34] * z[5];
z[33] = z[27] + -z[38];
z[37] = z[1] + -z[15];
z[9] = z[9] + z[33] + -z[37];
z[9] = abb[7] * z[9];
z[43] = 2 * z[41];
z[44] = abb[4] * z[40];
z[45] = z[43] + z[44];
z[46] = abb[28] + z[15] + -z[34] + z[38];
z[47] = abb[3] * z[46];
z[47] = -z[42] + z[47];
z[48] = -z[27] + z[34];
z[49] = abb[0] * z[48];
z[50] = 2 * abb[5];
z[51] = z[36] * z[50];
z[25] = abb[10] * z[25];
z[9] = z[9] + z[25] + 2 * z[45] + -z[47] + -z[49] + -z[51];
z[9] = abb[13] * z[9];
z[45] = z[1] + -z[38];
z[49] = 3 * abb[24];
z[52] = -abb[23] + z[49];
z[28] = z[28] + -z[45] + z[52];
z[28] = abb[7] * z[28];
z[15] = -abb[28] + z[15];
z[53] = z[15] + -z[29];
z[54] = abb[4] * z[53];
z[55] = abb[6] * z[36];
z[56] = z[54] + -z[55];
z[57] = abb[1] * z[53];
z[58] = 2 * z[57];
z[59] = z[56] + z[58];
z[60] = abb[0] * z[46];
z[61] = abb[3] * z[48];
z[25] = z[25] + -z[42];
z[28] = -z[25] + z[28] + 2 * z[59] + -z[60] + -z[61];
z[28] = abb[16] * z[28];
z[9] = z[9] + z[28];
z[28] = z[41] + z[57];
z[48] = abb[7] * z[48];
z[59] = -z[28] + z[48];
z[61] = abb[9] * z[24];
z[62] = z[59] + -z[61];
z[10] = -z[10] + z[29] + z[33];
z[33] = abb[4] * z[10];
z[63] = 3 * abb[26];
z[64] = -abb[25] + 3 * abb[29];
z[0] = -abb[23] + z[0] + z[63] + -z[64];
z[0] = abb[0] * z[0];
z[65] = -z[46] * z[50];
z[66] = abb[10] * z[24];
z[0] = 2 * z[0] + -z[33] + -z[47] + -z[62] + z[65] + z[66];
z[0] = abb[12] * z[0];
z[0] = z[0] + -z[9];
z[0] = abb[12] * z[0];
z[47] = 2 * abb[6];
z[65] = z[46] * z[47];
z[67] = abb[4] * z[30];
z[68] = 2 * abb[23];
z[69] = -abb[24] + z[3] + -z[64] + z[68];
z[69] = abb[3] * z[69];
z[60] = z[42] + -z[60] + -z[62] + -z[65] + -z[67] + 2 * z[69];
z[60] = abb[14] * z[60];
z[62] = -z[28] + z[55];
z[3] = z[3] + -z[34] + z[63];
z[3] = abb[7] * z[3];
z[69] = -abb[28] + z[35];
z[70] = abb[4] * z[69];
z[3] = z[3] + -z[61] + -z[62] + -z[70];
z[30] = abb[0] * z[30];
z[71] = abb[3] * z[10];
z[3] = 2 * z[3] + z[30] + z[42] + -z[51] + -z[66] + z[71];
z[3] = abb[12] * z[3];
z[30] = abb[11] * z[24];
z[70] = z[30] + z[70];
z[71] = -abb[27] + z[64];
z[38] = abb[24] + z[38];
z[72] = z[38] + -z[71];
z[72] = abb[0] * z[72];
z[73] = abb[23] + abb[28];
z[71] = z[71] + -z[73];
z[71] = abb[3] * z[71];
z[62] = z[62] + -z[70] + z[71] + z[72];
z[62] = z[48] + 2 * z[62] + z[66];
z[62] = abb[15] * z[62];
z[3] = z[3] + -z[9] + z[60] + z[62];
z[3] = abb[14] * z[3];
z[60] = abb[3] * z[53];
z[62] = z[60] + z[66];
z[31] = z[31] + z[62];
z[61] = z[30] + z[61];
z[43] = z[31] + z[43] + z[61] + -z[67];
z[43] = 2 * z[43];
z[67] = -abb[35] * z[43];
z[10] = abb[7] * z[10];
z[40] = abb[0] * z[40];
z[71] = z[10] + z[40];
z[33] = -z[33] + z[58] + z[61] + z[71];
z[33] = 2 * z[33];
z[58] = -abb[33] * z[33];
z[41] = z[41] + z[44];
z[61] = z[50] * z[69];
z[31] = z[30] + z[31] + -z[40] + 2 * z[41] + z[61];
z[31] = prod_pow(abb[13], 2) * z[31];
z[41] = abb[9] + -abb[11];
z[29] = abb[26] + -abb[27] + z[29];
z[41] = z[29] * z[41];
z[61] = -abb[0] + abb[3];
z[24] = z[24] * z[61];
z[53] = -abb[10] * z[53];
z[24] = z[24] + 2 * z[41] + z[53];
z[24] = abb[17] * z[24];
z[41] = abb[6] * z[69];
z[41] = z[41] + z[54] + z[57];
z[41] = z[30] + 2 * z[41] + -z[62] + z[71];
z[41] = prod_pow(abb[16], 2) * z[41];
z[0] = z[0] + z[3] + z[11] + z[24] + z[31] + z[41] + z[58] + z[67];
z[2] = -z[2] + z[32] + z[63] + z[68];
z[2] = z[2] * z[6];
z[3] = z[7] + -z[20] + -z[52];
z[3] = abb[0] * z[3];
z[7] = 6 * abb[27];
z[11] = 5 * abb[23] + z[7];
z[14] = z[11] + z[14] + z[38];
z[20] = abb[5] * z[14];
z[4] = 6 * abb[24] + -abb[27] + z[4] + z[13];
z[4] = abb[6] * z[4];
z[13] = abb[3] * z[36];
z[2] = z[2] + -z[3] + z[4] + 2 * z[10] + z[13] + z[20] + -z[25] + -z[26] + 5 * z[57];
z[2] = 2 * z[2];
z[3] = -abb[36] * z[2];
z[4] = z[7] + z[17] + -z[27] + -z[34];
z[10] = abb[4] * z[4];
z[10] = z[10] + z[19] + z[28];
z[13] = abb[24] + abb[28];
z[7] = -z[7] + -z[8] + z[13] + z[45];
z[7] = abb[3] * z[7];
z[17] = -z[17] + z[37] + -z[49] + z[73];
z[17] = abb[0] * z[17];
z[19] = 3 * abb[7];
z[4] = z[4] * z[19];
z[14] = z[14] * z[50];
z[4] = z[4] + z[7] + -2 * z[10] + -z[14] + z[17] + z[23];
z[7] = abb[37] * z[4];
z[10] = -abb[26] + z[64];
z[13] = z[10] + -z[13];
z[13] = abb[0] * z[13];
z[10] = -z[10] + z[16];
z[10] = abb[3] * z[10];
z[10] = z[10] + z[13] + -z[28] + -z[70];
z[10] = 2 * z[10] + z[48] + z[51] + -z[66];
z[10] = abb[12] * z[10];
z[13] = z[28] + -z[40] + -z[60];
z[13] = abb[15] * z[13];
z[10] = z[9] + z[10] + z[13];
z[13] = 2 * abb[15];
z[10] = z[10] * z[13];
z[0] = 2 * z[0] + z[3] + z[7] + z[10];
z[3] = -z[12] + -z[34] + 8 * z[35];
z[3] = abb[7] * z[3];
z[7] = abb[25] + abb[28];
z[7] = -36 * abb[29] + 12 * z[7];
z[10] = abb[23] + abb[27];
z[14] = -37 * abb[24] + z[10];
z[14] = abb[26] * (T(-37) / T(3)) + -z[7] + (T(1) / T(3)) * z[14];
z[14] = z[14] * z[47];
z[12] = -11 * abb[27] + z[12] + -2 * z[22];
z[12] = 4 * abb[29] + abb[26] * (T(-11) / T(3)) + abb[25] * (T(-4) / T(3)) + (T(1) / T(3)) * z[12];
z[12] = abb[4] * z[12];
z[10] = abb[24] + abb[26] + -37 * z[10];
z[7] = -z[7] + (T(1) / T(3)) * z[10];
z[7] = z[7] * z[50];
z[10] = abb[28] * (T(5) / T(3)) + -z[34] + (T(4) / T(3)) * z[35];
z[10] = abb[8] * z[10];
z[16] = abb[28] + 16 * abb[29] + abb[25] * (T(-16) / T(3));
z[17] = 7 * abb[23] + abb[26] * (T(-56) / T(3)) + abb[24] * (T(-37) / T(3)) + abb[27] * (T(2) / T(3)) + z[16];
z[17] = abb[0] * z[17];
z[16] = 7 * abb[24] + abb[27] * (T(-56) / T(3)) + abb[23] * (T(-37) / T(3)) + abb[26] * (T(2) / T(3)) + z[16];
z[16] = abb[3] * z[16];
z[3] = 4 * z[3] + z[7] + 16 * z[10] + 8 * z[12] + z[14] + z[16] + z[17] + (T(-17) / T(3)) * z[28] + (T(-8) / T(3)) * z[30];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[0] = 2 * z[0] + z[3];
z[0] = 2 * z[0];
z[3] = -abb[19] * z[5];
z[5] = -z[28] + -z[56];
z[7] = z[1] + z[8] + z[15] + -z[18];
z[7] = abb[0] * z[7];
z[8] = 4 * abb[5] + -z[19];
z[8] = z[8] * z[46];
z[10] = -abb[28] + z[21] + -z[34];
z[12] = abb[3] * z[10];
z[14] = 4 * z[30];
z[5] = 2 * z[5] + z[7] + z[8] + z[12] + -z[14];
z[5] = abb[12] * z[5];
z[7] = -z[29] * z[61];
z[6] = z[6] * z[69];
z[8] = -abb[5] * z[36];
z[6] = z[6] + z[7] + z[8] + z[42] + -z[55] + -z[59];
z[6] = z[6] * z[13];
z[7] = -z[28] + -z[44] + z[65];
z[1] = z[1] + -z[11] + z[39] + z[49];
z[1] = abb[3] * z[1];
z[8] = abb[0] * z[10];
z[10] = -z[19] * z[46];
z[1] = z[1] + 2 * z[7] + z[8] + z[10] + -z[14] + z[51];
z[1] = abb[14] * z[1];
z[1] = z[1] + z[5] + z[6] + z[9];
z[1] = m1_set::bc<T>[0] * z[1];
z[5] = -abb[18] * z[33];
z[6] = -abb[20] * z[43];
z[1] = z[1] + z[3] + z[5] + z[6];
z[2] = -abb[21] * z[2];
z[3] = abb[22] * z[4];
z[1] = 2 * z[1] + z[2] + z[3];
z[1] = 4 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_678_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-38.115233147755032586984254137082034469017334132814535378529593366"),stof<T>("-40.222184800925762186127797039018983504557876128910731391486163864")}, std::complex<T>{stof<T>("89.865808172085952796405037237519155538757208876198204554464900877"),stof<T>("-67.889155747473569401681280307657025301109039103172966892638208939")}, std::complex<T>{stof<T>("51.750575024330920209420783100437121069739874743383669175935307511"),stof<T>("-108.111340548399331587809077346676008805666915232083698284124372803")}, std::complex<T>{stof<T>("-75.13699536756355559938626022566222034904388483905344675893522374"),stof<T>("-128.59960818006314524778559999242608758326431583645245368766128764")}, std::complex<T>{stof<T>("-203.11803668740454098277555160026341035681842784806618669192971798"),stof<T>("-100.93263723351533803223211672378804578671315286219021818650924257")}, std::complex<T>{stof<T>("216.75337856398042860521208056361849695754096845863532048933543213"),stof<T>("-47.4008881158097557417047576619069465235116384988042114891012941")}, std::complex<T>{stof<T>("-155.25172507299276062826234930131136320921962423015100752780592253"),stof<T>("324.33402164519799476342723204002802641700074569625109485237311841")}, stof<T>("-27.798199965403973130008609371046810056707915075587306640714604869"), stof<T>("-13.899099982701986565004304685523405028353957537793653320357302435"), stof<T>("13.899099982701986565004304685523405028353957537793653320357302435")};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[68].real()/kbase.W[68].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_678_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_678_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-4.46224203696279888179315988170255594879326409725515145317757135"),stof<T>("373.42874154104376401327311195322402263306151580838982958959237833")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,38> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dl[4], dlog_W15(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), dlog_W69(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_11(k), f_2_23(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[68].real()/k.W[68].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_4_678_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_678_DLogXconstant_part(base_point<T>, kend);
	value += f_4_678_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_678_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_678_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_678_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_678_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_678_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_678_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
