/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_309.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_309_abbreviated (const std::array<T,30>& abb) {
T z[40];
z[0] = -abb[18] + abb[24];
z[1] = abb[20] * (T(5) / T(2));
z[2] = abb[23] * (T(11) / T(2));
z[3] = abb[19] + abb[22];
z[4] = abb[21] * (T(-11) / T(6)) + (T(7) / T(6)) * z[0] + z[1] + -z[2] + (T(11) / T(3)) * z[3];
z[4] = abb[6] * z[4];
z[5] = abb[20] * (T(1) / T(2));
z[6] = z[3] + z[5];
z[7] = 2 * abb[23];
z[6] = abb[21] * (T(-4) / T(3)) + (T(-5) / T(12)) * z[0] + (T(5) / T(2)) * z[6] + -z[7];
z[6] = abb[3] * z[6];
z[8] = -3 * z[0];
z[9] = 5 * z[3];
z[10] = 4 * abb[20] + abb[23] * (T(7) / T(2)) + abb[21] * (T(13) / T(2)) + -z[9];
z[10] = z[8] + (T(1) / T(3)) * z[10];
z[10] = abb[8] * z[10];
z[11] = abb[23] * (T(-53) / T(4)) + 11 * z[3];
z[11] = (T(3) / T(4)) * z[0] + (T(1) / T(3)) * z[11];
z[11] = abb[1] * z[11];
z[12] = abb[20] * (T(1) / T(4)) + z[3];
z[12] = abb[23] * (T(-1) / T(2)) + (T(-19) / T(12)) * z[0] + (T(5) / T(3)) * z[12];
z[12] = abb[0] * z[12];
z[1] = abb[23] * (T(17) / T(2)) + -z[1] + -9 * z[3];
z[13] = (T(-1) / T(2)) * z[0];
z[1] = abb[21] + (T(1) / T(2)) * z[1] + -z[13];
z[1] = abb[7] * z[1];
z[14] = -abb[20] + z[0];
z[15] = abb[2] * z[14];
z[16] = -abb[21] + z[0];
z[17] = abb[5] * z[16];
z[18] = -abb[20] + abb[23];
z[19] = abb[4] * z[18];
z[1] = z[1] + z[4] + z[6] + z[10] + z[11] + z[12] + (T(7) / T(4)) * z[15] + (T(13) / T(6)) * z[17] + (T(4) / T(3)) * z[19];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[4] = 3 * abb[23];
z[6] = abb[21] + abb[20] * (T(3) / T(2)) + z[3] + -z[4] + z[13];
z[6] = abb[8] * z[6];
z[10] = -abb[20] + 5 * abb[23];
z[11] = 4 * z[3];
z[12] = z[10] + -z[11] + -z[16];
z[12] = abb[7] * z[12];
z[20] = z[17] + z[19];
z[21] = -z[5] + z[13];
z[22] = -abb[23] + z[3];
z[23] = abb[21] + z[21] + -z[22];
z[23] = abb[3] * z[23];
z[24] = z[5] + z[22];
z[13] = z[13] + z[24];
z[13] = abb[0] * z[13];
z[25] = 2 * z[3];
z[26] = -abb[21] + z[25];
z[27] = -abb[20] + z[7];
z[28] = z[26] + -z[27];
z[29] = abb[6] * z[28];
z[22] = 2 * z[22];
z[30] = abb[1] * z[22];
z[29] = z[29] + z[30];
z[6] = z[6] + z[12] + -z[13] + z[20] + -z[23] + z[29];
z[13] = abb[29] * z[6];
z[23] = abb[23] * (T(3) / T(2)) + -z[3];
z[30] = abb[21] * (T(1) / T(2));
z[31] = z[0] + -z[23] + -z[30];
z[31] = abb[8] * z[31];
z[32] = -abb[23] + z[0];
z[33] = abb[1] * z[32];
z[34] = z[17] + z[33];
z[35] = z[19] + z[34];
z[36] = -z[0] + z[27];
z[36] = abb[6] * z[36];
z[24] = -z[24] + z[30];
z[24] = abb[3] * z[24];
z[37] = z[0] + -z[3];
z[37] = abb[0] * z[37];
z[24] = z[24] + z[31] + (T(-1) / T(2)) * z[35] + z[36] + z[37];
z[24] = prod_pow(abb[9], 2) * z[24];
z[31] = -2 * z[0];
z[2] = -2 * abb[21] + -z[2] + z[5] + z[9] + -z[31];
z[2] = abb[7] * z[2];
z[9] = -z[5] + -z[16] + z[23];
z[9] = abb[3] * z[9];
z[23] = z[7] + z[14] + -z[25];
z[35] = abb[0] * z[23];
z[37] = -abb[20] + 4 * abb[23] + -z[0] + -z[25];
z[37] = abb[8] * z[37];
z[38] = -z[19] + z[37];
z[39] = z[35] + -z[38];
z[2] = z[2] + z[9] + -z[29] + -z[39];
z[9] = -abb[28] * z[2];
z[0] = abb[23] + z[0];
z[29] = z[0] + -z[25];
z[29] = abb[0] * z[29];
z[28] = abb[3] * z[28];
z[0] = 2 * abb[20] + -z[0];
z[0] = abb[6] * z[0];
z[0] = z[0] + z[12] + z[28] + z[29] + -z[38];
z[12] = abb[26] * z[0];
z[4] = z[4] + -z[26] + z[31];
z[26] = abb[8] * z[4];
z[4] = abb[7] * z[4];
z[17] = -z[17] + z[33];
z[22] = abb[0] * z[22];
z[22] = -z[4] + -z[17] + z[22] + z[26];
z[26] = abb[9] * z[22];
z[18] = z[16] + z[18];
z[18] = abb[8] * z[18];
z[28] = abb[3] * z[32];
z[14] = abb[0] * z[14];
z[17] = -z[14] + z[17] + z[18] + -z[28];
z[5] = -abb[23] + z[5] + z[30];
z[5] = abb[7] * z[5];
z[17] = z[5] + (T(1) / T(2)) * z[17];
z[17] = abb[12] * z[17];
z[17] = z[17] + z[26];
z[17] = abb[12] * z[17];
z[16] = abb[3] * z[16];
z[26] = z[4] + z[16] + z[39];
z[26] = abb[9] * z[26];
z[29] = abb[21] + -abb[23];
z[29] = abb[3] * z[29];
z[19] = -z[19] + z[29];
z[5] = -z[5] + (T(1) / T(2)) * z[19];
z[5] = abb[11] * z[5];
z[5] = z[5] + z[26];
z[5] = abb[11] * z[5];
z[7] = -z[3] + z[7] + z[21];
z[19] = -abb[3] + abb[8];
z[7] = z[7] * z[19];
z[3] = z[3] + z[21];
z[3] = abb[0] * z[3];
z[3] = z[3] + z[7] + -z[15] + -z[36];
z[3] = prod_pow(abb[10], 2) * z[3];
z[7] = z[18] + -z[20];
z[14] = -z[7] + z[14] + z[16] + -z[33];
z[16] = abb[25] * z[14];
z[7] = z[7] + z[29];
z[18] = -abb[27] * z[7];
z[19] = abb[26] + abb[27] + abb[29];
z[19] = z[15] * z[19];
z[1] = z[1] + z[3] + z[5] + z[9] + z[12] + z[13] + z[16] + z[17] + z[18] + z[19] + z[24];
z[3] = z[6] + z[15];
z[3] = abb[17] * z[3];
z[5] = -abb[3] * z[23];
z[6] = z[8] + z[11] + -z[27];
z[6] = abb[0] * z[6];
z[8] = -abb[20] + abb[21] + 7 * abb[23] + z[8] + -z[11];
z[8] = abb[8] * z[8];
z[4] = -z[4] + z[5] + z[6] + z[8] + z[34] + -2 * z[36];
z[4] = abb[9] * z[4];
z[5] = -z[10] + z[25] + -z[31];
z[5] = abb[7] * z[5];
z[5] = z[5] + -z[28] + -z[35] + z[37];
z[5] = abb[11] * z[5];
z[6] = -abb[12] * z[22];
z[4] = z[4] + z[5] + z[6];
z[4] = m1_set::bc<T>[0] * z[4];
z[2] = -abb[16] * z[2];
z[0] = z[0] + z[15];
z[0] = abb[14] * z[0];
z[5] = abb[13] * z[14];
z[6] = -z[7] + z[15];
z[6] = abb[15] * z[6];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_309_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-37.784108152005527095540819707533887911514396121641668952150471157"),stof<T>("-5.067059899366094060370433717159257422971564027674087353880854906")}, std::complex<T>{stof<T>("1.470120519011008190139430587688042343218415864285356550679957287"),stof<T>("-62.24484391841205541057355820545907524692043911217652846679426971")}, std::complex<T>{stof<T>("-2.994343547218440672905655331369292223101835995077943641957142001"),stof<T>("-22.944567613307694580908978108534978209967468280405624544146127321")}, std::complex<T>{stof<T>("-24.446944110666708254697539131981555459811130000889522029812453553"),stof<T>("20.677207097799510631158874955406360044331932771262048100581947642")}, std::complex<T>{stof<T>("1.470120519011008190139430587688042343218415864285356550679957287"),stof<T>("-62.24484391841205541057355820545907524692043911217652846679426971")}, std::complex<T>{stof<T>("-11.812941013131386358077055831871082571819845989959559831060832891"),stof<T>("59.445144534554145299953227641428435989584410593646017556477594482")}, std::complex<T>{stof<T>("37.784108152005527095540819707533887911514396121641668952150471157"),stof<T>("5.067059899366094060370433717159257422971564027674087353880854906")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_309_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_309_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-60.925265239659516117110672740821913749196539194015009120308880879"),stof<T>("40.986139344337398179961264869754232440645777188676771631804558729")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,30> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), f_1_1(k), f_1_4(k), f_1_5(k), f_1_11(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_4_309_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_309_DLogXconstant_part(base_point<T>, kend);
	value += f_4_309_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_309_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_309_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_309_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_309_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_309_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_309_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
