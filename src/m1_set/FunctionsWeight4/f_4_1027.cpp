/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_1027.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_1027_abbreviated (const std::array<T,85>& abb) {
T z[79];
z[0] = 3 * abb[32];
z[1] = 3 * abb[37];
z[2] = z[0] + -z[1];
z[3] = 2 * abb[38];
z[4] = 4 * abb[31];
z[5] = z[3] + -z[4];
z[6] = abb[35] + -abb[36];
z[7] = z[2] + z[5] + z[6];
z[7] = abb[38] * z[7];
z[8] = 2 * abb[33];
z[9] = -z[4] + z[8];
z[2] = z[2] + -z[6] + -z[9];
z[2] = abb[33] * z[2];
z[10] = abb[35] + abb[36];
z[11] = 2 * abb[31];
z[12] = z[10] + -z[11];
z[13] = 2 * abb[32];
z[14] = 4 * abb[37] + z[12] + -z[13];
z[14] = abb[32] * z[14];
z[15] = 2 * abb[37];
z[12] = z[12] + -z[15];
z[12] = abb[37] * z[12];
z[16] = z[10] * z[11];
z[17] = abb[32] + -abb[37];
z[18] = z[6] + z[17];
z[19] = abb[39] * z[18];
z[20] = 2 * abb[42];
z[21] = -z[19] + z[20];
z[22] = -z[6] + z[17];
z[23] = abb[34] * z[22];
z[24] = 2 * abb[43];
z[25] = z[23] + z[24];
z[26] = 2 * abb[36];
z[27] = -abb[35] + z[26];
z[27] = abb[35] * z[27];
z[28] = prod_pow(m1_set::bc<T>[0], 2);
z[29] = (T(10) / T(3)) * z[28];
z[30] = 2 * abb[74];
z[31] = 2 * abb[44];
z[32] = prod_pow(abb[36], 2);
z[2] = -z[2] + z[7] + -z[12] + -z[14] + z[16] + z[21] + z[25] + -z[27] + z[29] + z[30] + z[31] + z[32];
z[7] = abb[63] + abb[64];
z[2] = -z[2] * z[7];
z[12] = abb[38] * z[22];
z[14] = z[12] + z[23];
z[16] = abb[33] * z[18];
z[27] = z[16] + z[19];
z[33] = -abb[32] + z[15];
z[34] = -z[6] + z[33];
z[34] = abb[32] * z[34];
z[35] = z[6] * z[11];
z[36] = abb[37] + z[6];
z[36] = abb[37] * z[36];
z[34] = z[14] + -z[27] + -z[34] + z[35] + z[36];
z[36] = abb[65] + abb[66];
z[34] = -z[34] * z[36];
z[2] = z[2] + z[34];
z[2] = abb[0] * z[2];
z[34] = (T(1) / T(2)) * z[22];
z[34] = abb[38] * z[34];
z[37] = 2 * abb[41];
z[38] = z[20] + z[34] + -z[37];
z[39] = -abb[36] + abb[35] * (T(1) / T(2));
z[39] = abb[35] * z[39];
z[40] = (T(1) / T(2)) * z[32];
z[39] = z[39] + z[40];
z[41] = (T(1) / T(2)) * z[19];
z[42] = abb[74] + z[41];
z[43] = (T(1) / T(2)) * z[23];
z[44] = (T(1) / T(2)) * z[10];
z[45] = z[11] + z[44];
z[46] = -abb[37] + -z[13] + z[45];
z[46] = abb[32] * z[46];
z[47] = -abb[37] + z[6];
z[48] = 7 * abb[32] + -z[47];
z[48] = -z[8] + (T(1) / T(2)) * z[48];
z[48] = abb[33] * z[48];
z[49] = (T(4) / T(3)) * z[28];
z[50] = prod_pow(abb[31], 2);
z[51] = abb[44] + z[50];
z[52] = -abb[37] * z[44];
z[46] = -z[38] + z[39] + -z[42] + -z[43] + z[46] + z[48] + z[49] + -z[51] + z[52];
z[46] = abb[64] * z[46];
z[48] = abb[37] * (T(5) / T(2));
z[52] = abb[32] * (T(5) / T(2));
z[53] = z[48] + -z[52];
z[54] = (T(3) / T(2)) * z[6];
z[55] = z[9] + z[53] + z[54];
z[55] = abb[33] * z[55];
z[56] = (T(3) / T(2)) * z[23];
z[57] = prod_pow(abb[35], 2);
z[58] = -z[32] + z[57];
z[58] = (T(1) / T(2)) * z[58];
z[59] = (T(1) / T(2)) * z[6];
z[60] = z[11] + z[59];
z[61] = -abb[37] * z[60];
z[62] = -z[4] + z[59];
z[33] = -z[33] + -z[62];
z[33] = abb[32] * z[33];
z[63] = -abb[31] + 2 * z[6];
z[63] = abb[31] * z[63];
z[63] = -abb[44] + z[63];
z[64] = (T(2) / T(3)) * z[28];
z[33] = z[33] + z[38] + -z[42] + z[55] + -z[56] + z[58] + z[61] + -z[63] + -z[64];
z[33] = abb[65] * z[33];
z[38] = -abb[31] + 2 * z[10];
z[38] = abb[31] * z[38];
z[42] = 4 * abb[72];
z[38] = abb[44] + abb[74] + (T(20) / T(3)) * z[28] + z[38] + 3 * z[39] + z[42];
z[55] = (T(3) / T(2)) * z[19];
z[10] = (T(3) / T(2)) * z[10] + -z[11];
z[61] = -abb[37] * z[10];
z[65] = -z[4] + z[44];
z[66] = 5 * abb[37];
z[67] = -z[65] + -z[66];
z[67] = abb[32] * z[67];
z[68] = -z[4] + z[54];
z[52] = abb[37] * (T(3) / T(2)) + z[52] + z[68];
z[52] = abb[38] * z[52];
z[69] = abb[32] * (T(1) / T(2)) + abb[37] * (T(7) / T(2)) + z[62];
z[69] = abb[33] * z[69];
z[52] = z[37] + z[38] + z[43] + z[52] + -z[55] + z[61] + z[67] + z[69];
z[52] = abb[63] * z[52];
z[61] = (T(1) / T(2)) * z[18];
z[61] = abb[33] * z[61];
z[67] = z[51] + -z[61];
z[69] = z[34] + z[67];
z[43] = -abb[74] + z[43];
z[70] = -z[41] + z[43];
z[58] = (T(-8) / T(3)) * z[28] + z[58] + z[69] + z[70];
z[71] = -z[11] + z[54];
z[72] = -abb[32] + -z[71];
z[72] = abb[32] * z[72];
z[73] = abb[37] * z[59];
z[72] = z[37] + -z[58] + z[72] + z[73];
z[72] = abb[66] * z[72];
z[33] = z[33] + z[46] + z[52] + z[72];
z[33] = abb[7] * z[33];
z[46] = z[5] + -z[53] + z[54];
z[46] = abb[38] * z[46];
z[52] = 2 * abb[71];
z[53] = z[43] + -z[52];
z[54] = z[53] + -z[64];
z[72] = abb[37] + -z[62];
z[72] = abb[37] * z[72];
z[60] = -z[15] + -z[60];
z[60] = abb[32] * z[60];
z[40] = z[24] + -z[40] + z[46] + z[54] + z[55] + (T(1) / T(2)) * z[57] + z[60] + -z[61] + -z[63] + z[72];
z[40] = abb[66] * z[40];
z[46] = -abb[37] * z[65];
z[10] = -z[10] + -z[66];
z[10] = abb[32] * z[10];
z[55] = abb[37] * (T(1) / T(2)) + abb[32] * (T(7) / T(2)) + z[62];
z[55] = abb[38] * z[55];
z[48] = abb[32] * (T(3) / T(2)) + z[48] + z[68];
z[48] = abb[33] * z[48];
z[10] = z[10] + z[38] + -z[41] + z[46] + z[48] + -z[52] + z[55] + z[56];
z[10] = abb[64] * z[10];
z[38] = z[39] + z[41];
z[39] = abb[37] + z[44];
z[39] = abb[32] * z[39];
z[39] = z[38] + -z[39] + -z[67];
z[41] = -z[15] + z[45];
z[41] = abb[37] * z[41];
z[46] = -abb[32] + z[6];
z[48] = 7 * abb[37] + -z[46];
z[48] = -z[3] + (T(1) / T(2)) * z[48];
z[48] = abb[38] * z[48];
z[41] = -z[24] + z[39] + z[41] + z[42] + z[48] + z[49] + z[53];
z[41] = abb[63] * z[41];
z[48] = -abb[37] + -z[71];
z[48] = abb[37] * z[48];
z[49] = abb[32] * z[59];
z[48] = z[48] + z[49] + z[52] + -z[58];
z[48] = abb[65] * z[48];
z[10] = z[10] + z[40] + z[41] + z[48];
z[10] = abb[8] * z[10];
z[40] = z[37] + -z[52];
z[41] = -abb[31] + abb[37];
z[48] = z[41] + z[59];
z[48] = abb[32] * z[48];
z[49] = -abb[31] + -z[59];
z[49] = abb[37] * z[49];
z[38] = z[34] + -z[38] + -z[40] + -z[43] + z[48] + z[49] + z[51] + z[61] + z[64];
z[38] = abb[28] * z[38];
z[43] = -abb[32] + -abb[37];
z[44] = -abb[31] + z[44];
z[43] = z[43] * z[44];
z[44] = abb[35] * (T(3) / T(2)) + -z[26];
z[44] = abb[35] * z[44];
z[43] = -z[28] + (T(3) / T(2)) * z[32] + z[40] + z[43] + z[44] + -z[69] + z[70];
z[44] = abb[25] + abb[27];
z[48] = -z[43] * z[44];
z[45] = -abb[37] * z[45];
z[34] = -z[34] + -z[39] + z[45] + -z[54];
z[34] = abb[26] * z[34];
z[39] = abb[31] + abb[35];
z[39] = z[17] * z[39];
z[39] = -z[19] + -z[23] + z[39] + z[52];
z[45] = -abb[29] * z[39];
z[49] = abb[61] + abb[62];
z[51] = abb[30] * abb[45];
z[34] = z[34] + z[38] + z[45] + z[48] + (T(-1) / T(2)) * z[49] + z[51];
z[34] = abb[67] * z[34];
z[38] = -z[1] + z[3] + z[46];
z[38] = abb[38] * z[38];
z[45] = abb[35] * z[6];
z[30] = z[30] + -z[45];
z[35] = (T(16) / T(3)) * z[28] + z[30] + z[35];
z[46] = -abb[35] + abb[37];
z[48] = z[11] + -z[46];
z[48] = abb[32] * z[48];
z[49] = abb[36] + abb[37] + z[11];
z[51] = abb[37] * z[49];
z[52] = 4 * abb[71];
z[38] = -2 * z[19] + -z[25] + z[35] + -z[38] + z[42] + z[48] + -z[51] + z[52];
z[42] = abb[63] + abb[66];
z[48] = abb[13] * z[42];
z[38] = z[38] * z[48];
z[51] = z[12] + -z[23];
z[53] = abb[35] * abb[36];
z[31] = z[31] + -z[32] + z[53];
z[53] = 4 * abb[41] + -z[31];
z[22] = -abb[33] + z[11] + z[22];
z[22] = z[8] * z[22];
z[54] = -abb[35] + z[11] + z[17];
z[54] = abb[32] * z[54];
z[55] = -abb[31] + z[6];
z[55] = z[11] * z[55];
z[56] = -abb[36] + z[11];
z[56] = abb[37] * z[56];
z[20] = -z[20] + z[22] + -z[51] + z[53] + -z[54] + z[55] + z[56];
z[20] = abb[11] * z[20];
z[22] = -abb[38] + z[15];
z[54] = abb[31] + -abb[32] + z[22];
z[54] = abb[38] * z[54];
z[56] = abb[31] + abb[37];
z[57] = abb[32] + -z[56];
z[57] = abb[37] * z[57];
z[54] = -abb[43] + abb[72] + z[54] + z[57];
z[57] = 4 * abb[15];
z[54] = z[54] * z[57];
z[58] = abb[80] + abb[82];
z[59] = abb[77] + z[58];
z[59] = z[54] + (T(1) / T(2)) * z[59];
z[60] = -z[20] + z[59];
z[60] = abb[64] * z[60];
z[61] = 5 * abb[32] + -z[66];
z[9] = z[6] + -z[9] + z[61];
z[9] = abb[33] * z[9];
z[62] = abb[36] + z[4];
z[63] = -z[1] + z[13] + z[62];
z[63] = abb[32] * z[63];
z[4] = z[4] + z[26];
z[26] = abb[35] + abb[37];
z[64] = z[4] + -z[26];
z[64] = abb[37] * z[64];
z[50] = 2 * z[50];
z[9] = z[9] + -z[21] + -z[31] + -z[50] + -z[63] + z[64];
z[9] = abb[1] * z[9];
z[58] = -abb[77] + z[58];
z[58] = (T(1) / T(2)) * z[58];
z[20] = -z[9] + z[20] + z[58];
z[20] = abb[65] * z[20];
z[63] = abb[64] + abb[66];
z[64] = abb[63] + abb[65];
z[65] = z[63] + z[64];
z[65] = abb[10] * z[65];
z[66] = z[43] * z[65];
z[19] = z[16] + -z[19];
z[18] = -abb[38] + z[11] + -z[18];
z[3] = z[3] * z[18];
z[18] = -abb[36] + abb[37];
z[67] = z[11] + z[18];
z[67] = abb[32] * z[67];
z[68] = z[11] + z[46];
z[68] = abb[37] * z[68];
z[3] = -z[3] + -z[19] + z[24] + z[31] + -z[55] + -z[67] + z[68];
z[24] = -abb[63] + abb[66];
z[24] = abb[12] * z[24];
z[3] = -z[3] * z[24];
z[31] = 2 * abb[73];
z[43] = -z[31] + z[43];
z[55] = -abb[65] + z[7];
z[67] = -abb[66] + z[55];
z[68] = abb[5] * z[67];
z[43] = z[43] * z[68];
z[69] = -abb[1] * z[64];
z[24] = z[24] + z[69];
z[69] = abb[64] + abb[65];
z[70] = abb[63] + z[69];
z[71] = -abb[66] + z[70];
z[72] = abb[9] * z[71];
z[73] = abb[66] + z[70];
z[74] = abb[10] * z[73];
z[72] = z[72] + -z[74];
z[74] = -z[63] + z[64];
z[75] = abb[17] * z[74];
z[76] = -abb[26] + -z[44];
z[76] = abb[67] * z[76];
z[77] = abb[8] * z[71];
z[24] = 2 * z[24] + z[68] + -z[72] + z[75] + z[76] + z[77];
z[24] = abb[40] * z[24];
z[22] = abb[38] * z[22];
z[75] = prod_pow(abb[37], 2);
z[22] = -abb[43] + 2 * abb[72] + z[22] + z[28] + -z[75];
z[75] = 2 * abb[63] + z[63];
z[75] = abb[19] * z[75];
z[22] = -z[22] * z[75];
z[13] = -abb[33] + z[13];
z[76] = abb[33] * z[13];
z[77] = prod_pow(abb[32], 2);
z[76] = -abb[42] + z[28] + z[76] + -z[77];
z[77] = 2 * abb[64];
z[78] = z[64] + z[77];
z[78] = abb[20] * z[78];
z[76] = -z[76] * z[78];
z[22] = z[22] + z[24] + z[76];
z[9] = -z[9] + z[59];
z[9] = abb[63] * z[9];
z[0] = z[0] + -z[8] + -z[47];
z[0] = abb[33] * z[0];
z[8] = abb[32] + z[49];
z[8] = abb[32] * z[8];
z[24] = abb[35] + z[11];
z[24] = abb[37] * z[24];
z[0] = z[0] + -z[8] + -z[21] + 2 * z[23] + z[24] + z[35];
z[8] = abb[14] * z[69];
z[0] = z[0] * z[8];
z[5] = z[5] + -z[6] + z[61];
z[5] = abb[38] * z[5];
z[4] = -abb[32] + -abb[35] + z[1] + z[4];
z[4] = abb[32] * z[4];
z[6] = z[15] + z[62];
z[6] = abb[37] * z[6];
z[4] = z[4] + -z[5] + -z[6] + -z[25] + -z[50] + z[53];
z[4] = -abb[2] * z[4] * z[63];
z[5] = abb[21] + abb[22];
z[5] = z[5] * z[7];
z[6] = abb[21] + -abb[22];
z[6] = z[6] * z[36];
z[21] = abb[23] * z[74];
z[23] = z[42] + -z[69];
z[24] = abb[24] * z[23];
z[5] = z[5] + z[6] + z[21] + z[24];
z[5] = abb[45] * z[5];
z[6] = z[29] + z[30];
z[21] = 2 * abb[35];
z[24] = -abb[36] + z[17] + z[21];
z[24] = abb[32] * z[24];
z[25] = abb[35] * abb[37];
z[24] = -z[6] + z[24] + -z[25] + -z[27];
z[27] = abb[3] * z[24];
z[29] = abb[75] * (T(1) / T(2));
z[30] = abb[81] * (T(1) / T(2));
z[27] = z[27] + -z[29] + z[30] + z[54] + z[58];
z[27] = abb[66] * z[27];
z[28] = (T(1) / T(3)) * z[28] + -z[32] + -z[45];
z[32] = abb[36] * abb[37];
z[35] = abb[32] * z[46];
z[32] = z[28] + z[32] + -z[35] + -z[40] + z[51];
z[35] = -abb[65] + abb[66];
z[36] = z[7] + z[35];
z[32] = abb[6] * z[32] * z[36];
z[36] = z[18] + z[21];
z[36] = abb[37] * z[36];
z[26] = abb[32] * z[26];
z[6] = z[6] + -z[14] + z[26] + -z[36] + z[52];
z[14] = -abb[63] + abb[65];
z[14] = abb[4] * z[14];
z[6] = -z[6] * z[14];
z[26] = abb[3] * abb[64];
z[24] = -z[24] * z[26];
z[18] = abb[32] * z[18];
z[18] = z[18] + z[19] + -z[25] + -z[28];
z[19] = -abb[66] + z[64];
z[25] = -abb[64] + -z[19];
z[18] = abb[9] * z[18] * z[25];
z[25] = abb[63] + z[35];
z[28] = abb[64] + -z[25];
z[28] = abb[18] * z[28] * z[39];
z[36] = abb[31] + abb[36];
z[36] = -z[17] * z[36];
z[12] = z[12] + z[16] + z[36] + -z[37];
z[16] = -abb[64] + z[19];
z[12] = abb[17] * z[12] * z[16];
z[16] = abb[3] * abb[66];
z[16] = z[8] + -z[16] + z[26];
z[23] = abb[18] * z[23];
z[26] = abb[7] * z[71];
z[16] = 2 * z[16] + z[23] + -z[26] + z[72];
z[26] = abb[29] + z[44];
z[26] = abb[67] * z[26];
z[26] = z[16] + z[26];
z[26] = z[26] * z[31];
z[31] = abb[64] + z[64];
z[36] = 4 * abb[16];
z[31] = z[31] * z[36];
z[13] = z[13] + -z[41];
z[13] = abb[33] * z[13];
z[17] = abb[31] + z[17];
z[17] = abb[32] * z[17];
z[13] = -abb[42] + z[13] + -z[17];
z[13] = z[13] * z[31];
z[17] = abb[16] * z[70];
z[17] = -z[8] + -z[17] + z[78];
z[36] = abb[8] * abb[64];
z[37] = abb[7] * z[7];
z[36] = -z[17] + z[36] + z[37];
z[36] = 4 * z[36];
z[37] = abb[70] * z[36];
z[39] = abb[63] + z[63];
z[39] = abb[15] * z[39];
z[40] = abb[8] * abb[63];
z[39] = z[39] + z[40] + -z[75];
z[40] = abb[0] * z[7];
z[42] = -z[39] + -z[40] + -z[48];
z[42] = abb[68] * z[42];
z[45] = abb[7] * abb[64];
z[17] = -z[17] + z[40] + z[45];
z[17] = 4 * z[17];
z[45] = -abb[69] * z[17];
z[29] = z[29] * z[55];
z[47] = 3 * abb[63];
z[49] = abb[64] + z[47];
z[50] = -abb[65] + z[49];
z[30] = z[30] * z[50];
z[50] = 3 * abb[64];
z[51] = z[25] + z[50];
z[51] = (T(1) / T(2)) * z[51];
z[52] = abb[78] * z[51];
z[47] = -abb[66] + z[47] + z[69];
z[47] = (T(1) / T(2)) * z[47];
z[53] = abb[76] * z[47];
z[19] = z[19] + z[50];
z[19] = (T(1) / T(2)) * z[19];
z[50] = abb[79] * z[19];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[9] + z[10] + z[12] + z[13] + z[18] + z[20] + 2 * z[22] + z[24] + z[26] + z[27] + z[28] + z[29] + z[30] + z[32] + z[33] + z[34] + z[37] + z[38] + 4 * z[42] + z[43] + z[45] + z[50] + z[52] + z[53] + z[60] + z[66];
z[2] = 2 * abb[46];
z[3] = 2 * m1_set::bc<T>[0];
z[4] = abb[38] * z[3];
z[5] = abb[32] * z[3];
z[6] = 2 * abb[49];
z[9] = abb[52] + z[6];
z[4] = 2 * abb[50] + -z[2] + z[4] + -z[5] + z[9];
z[4] = z[4] * z[48];
z[10] = abb[37] * m1_set::bc<T>[0];
z[12] = abb[32] * m1_set::bc<T>[0];
z[13] = -abb[49] + z[12];
z[18] = z[10] + -z[13];
z[20] = -z[18] * z[23];
z[22] = -z[7] + z[35];
z[22] = abb[9] * m1_set::bc<T>[0] * z[22] * z[46];
z[23] = abb[33] + -abb[37];
z[23] = z[3] * z[23];
z[23] = abb[52] + z[23];
z[8] = z[8] * z[23];
z[23] = abb[35] * m1_set::bc<T>[0];
z[13] = -z[13] + z[23];
z[23] = abb[64] + z[25];
z[13] = abb[6] * z[13] * z[23];
z[14] = -z[9] * z[14];
z[23] = abb[31] + -abb[35];
z[23] = z[3] * z[23];
z[2] = -abb[52] + -z[2] + -z[23];
z[2] = z[2] * z[40];
z[2] = z[2] + z[4] + z[8] + z[13] + z[14] + z[20] + z[22];
z[4] = abb[33] * m1_set::bc<T>[0];
z[8] = 4 * z[4];
z[13] = -z[8] + 6 * z[12];
z[14] = 3 * abb[31] + -z[21];
z[15] = z[14] + -z[15];
z[15] = z[3] * z[15];
z[20] = abb[38] * m1_set::bc<T>[0];
z[20] = abb[50] + z[20];
z[21] = 4 * z[20];
z[22] = abb[52] + z[21];
z[15] = -z[13] + z[15] + z[22];
z[15] = abb[63] * z[15];
z[11] = m1_set::bc<T>[0] * z[11];
z[13] = -abb[52] + z[11] + -z[13];
z[13] = abb[64] * z[13];
z[5] = -z[5] + z[11];
z[11] = -abb[52] + z[5];
z[11] = abb[65] * z[11];
z[5] = abb[52] + z[5];
z[5] = abb[66] * z[5];
z[5] = z[5] + z[11] + z[13] + z[15];
z[5] = abb[7] * z[5];
z[11] = -z[1] + z[14];
z[11] = z[3] * z[11];
z[6] = -z[6] + z[8] + z[11] + -4 * z[12] + z[22];
z[6] = abb[64] * z[6];
z[1] = abb[31] + -z[1];
z[1] = z[1] * z[3];
z[1] = z[1] + -z[9] + z[21];
z[1] = abb[63] * z[1];
z[3] = z[3] * z[41];
z[8] = -z[3] + z[9];
z[8] = abb[65] * z[8];
z[3] = z[3] + z[9];
z[11] = -abb[66] * z[3];
z[1] = z[1] + z[6] + z[8] + z[11];
z[1] = abb[8] * z[1];
z[4] = z[4] + -z[12];
z[6] = -z[4] * z[78];
z[8] = abb[46] * z[39];
z[10] = z[10] + -z[20];
z[10] = z[10] * z[75];
z[6] = z[6] + -z[8] + z[10];
z[8] = -z[9] + z[23];
z[9] = -z[8] * z[44];
z[10] = abb[26] + abb[28];
z[3] = z[3] * z[10];
z[10] = abb[51] + -z[18];
z[10] = abb[29] * z[10];
z[11] = abb[83] + abb[84];
z[3] = z[3] + z[9] + 2 * z[10] + (T(1) / T(2)) * z[11];
z[3] = abb[67] * z[3];
z[9] = abb[67] * z[44];
z[9] = z[9] + z[16];
z[10] = 2 * abb[51];
z[9] = z[9] * z[10];
z[11] = z[35] + z[49];
z[11] = abb[59] * z[11];
z[13] = abb[55] * z[67];
z[14] = abb[58] * z[73];
z[11] = z[11] + z[13] + z[14];
z[13] = m1_set::bc<T>[0] * z[56];
z[12] = -z[12] + z[13] + -z[20];
z[12] = z[12] * z[57];
z[13] = abb[53] + abb[60];
z[13] = -z[12] + (T(1) / T(2)) * z[13];
z[7] = z[7] * z[13];
z[13] = -abb[53] + abb[60];
z[13] = (T(1) / T(2)) * z[13];
z[14] = abb[3] * abb[52];
z[12] = -z[12] + z[13] + -2 * z[14];
z[12] = abb[66] * z[12];
z[15] = z[8] * z[65];
z[16] = m1_set::bc<T>[0] * z[41];
z[4] = z[4] + z[16];
z[4] = z[4] * z[31];
z[16] = abb[48] * z[36];
z[17] = -abb[47] * z[17];
z[14] = z[14] * z[77];
z[18] = abb[57] * z[19];
z[19] = abb[54] * z[47];
z[20] = abb[56] * z[51];
z[8] = z[8] + -z[10];
z[8] = z[8] * z[68];
z[10] = abb[65] * z[13];
z[1] = z[1] + 2 * z[2] + z[3] + z[4] + z[5] + 4 * z[6] + z[7] + z[8] + z[9] + z[10] + (T(1) / T(2)) * z[11] + z[12] + z[14] + z[15] + z[16] + z[17] + z[18] + z[19] + z[20];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_1027_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-20.742632909782878748124859918116438910629673859667542553551291571"),stof<T>("77.746052280784268507343458837796503842537483665982655291255190516")}, std::complex<T>{stof<T>("-20.742632909782878748124859918116438910629673859667542553551291571"),stof<T>("77.746052280784268507343458837796503842537483665982655291255190516")}, std::complex<T>{stof<T>("-1.5878653822997918967911179085265562738209800160562444877505438544"),stof<T>("-3.0775302532745012710375701816904739530658043246269021620678773304")}, std::complex<T>{stof<T>("-1.5878653822997918967911179085265562738209800160562444877505438544"),stof<T>("-3.0775302532745012710375701816904739530658043246269021620678773304")}, std::complex<T>{stof<T>("12.097305882114827542032612717488351764077172829486174779259610837"),stof<T>("-9.585923934639867303669713633499836524562927947608141683407252985")}};
	
	std::vector<C> intdlogs = {rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[125].real()/kbase.W[125].real()), rlog(k.W[126].real()/kbase.W[126].real()), C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_1027_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_1027_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-41.7243699904255161358645893334985969227387645901938714657232615"),stof<T>("46.263905952378121967455625142503109056607618922210736841519066161")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({199, 201});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,85> abb = {dl[0], dl[1], dlog_W3(k,dl), dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W14(k,dl), dlog_W15(k,dl), dlog_W23(k,dl), dlog_W27(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W65(k,dl), dlog_W67(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_5(k), f_2_8(k), f_2_17(k), f_2_20(k), f_2_21(k), f_2_23(k), f_2_2_im(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_10_im(k), f_2_12_im(k), f_2_22_im(k), T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[125].real()/k.W[125].real()), rlog(kend.W[126].real()/k.W[126].real()), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_2_re(k), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_10_re(k), f_2_12_re(k), f_2_22_re(k), T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}};
{
auto c = dlog_W163(k,dv) * f_2_31(k);
abb[75] = c.real();
abb[53] = c.imag();
SpDLog_Sigma5<T,199,162>(k, dv, abb[75], abb[53], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W166(k,dv) * f_2_31(k);
abb[76] = c.real();
abb[54] = c.imag();
SpDLog_Sigma5<T,199,165>(k, dv, abb[76], abb[54], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W168(k,dv) * f_2_33(k);
abb[77] = c.real();
abb[55] = c.imag();
SpDLog_Sigma5<T,201,167>(k, dv, abb[77], abb[55], f_2_33_series_coefficients<T>);
}
{
auto c = dlog_W170(k,dv) * f_2_33(k);
abb[78] = c.real();
abb[56] = c.imag();
SpDLog_Sigma5<T,201,169>(k, dv, abb[78], abb[56], f_2_33_series_coefficients<T>);
}
{
auto c = dlog_W177(k,dv) * f_2_31(k);
abb[79] = c.real();
abb[57] = c.imag();
SpDLog_Sigma5<T,199,176>(k, dv, abb[79], abb[57], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W179(k,dv) * f_2_31(k);
abb[80] = c.real();
abb[58] = c.imag();
SpDLog_Sigma5<T,199,178>(k, dv, abb[80], abb[58], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W181(k,dv) * f_2_33(k);
abb[81] = c.real();
abb[59] = c.imag();
SpDLog_Sigma5<T,201,180>(k, dv, abb[81], abb[59], f_2_33_series_coefficients<T>);
}
{
auto c = dlog_W184(k,dv) * f_2_33(k);
abb[82] = c.real();
abb[60] = c.imag();
SpDLog_Sigma5<T,201,183>(k, dv, abb[82], abb[60], f_2_33_series_coefficients<T>);
}
{
auto c = dlog_W190(k,dv) * f_2_31(k);
abb[83] = c.real();
abb[61] = c.imag();
SpDLog_Sigma5<T,199,189>(k, dv, abb[83], abb[61], f_2_31_series_coefficients<T>);
}
{
auto c = dlog_W192(k,dv) * f_2_33(k);
abb[84] = c.real();
abb[62] = c.imag();
SpDLog_Sigma5<T,201,191>(k, dv, abb[84], abb[62], f_2_33_series_coefficients<T>);
}

                    
            return f_4_1027_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_1027_DLogXconstant_part(base_point<T>, kend);
	value += f_4_1027_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_1027_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_1027_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_1027_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_1027_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_1027_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_1027_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
