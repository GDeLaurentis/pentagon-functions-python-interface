/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_244.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_244_abbreviated (const std::array<T,56>& abb) {
T z[98];
z[0] = abb[42] + abb[44] + -abb[45] + -abb[46];
z[1] = abb[52] + abb[43] * (T(1) / T(2)) + (T(1) / T(4)) * z[0];
z[2] = abb[18] * z[1];
z[3] = abb[51] * (T(1) / T(4));
z[4] = abb[23] * z[3];
z[2] = z[2] + -z[4];
z[5] = -abb[47] + abb[48];
z[6] = (T(1) / T(2)) * z[5];
z[7] = abb[13] * z[6];
z[7] = -z[2] + z[7];
z[8] = -abb[47] + abb[49];
z[9] = abb[1] * z[8];
z[10] = 2 * z[9];
z[11] = z[7] + -z[10];
z[12] = abb[16] * z[1];
z[13] = abb[47] + abb[48];
z[14] = -abb[49] + z[13];
z[14] = 2 * z[14];
z[15] = abb[11] * z[14];
z[16] = abb[20] * abb[51];
z[17] = z[11] + 3 * z[12] + -z[15] + (T(1) / T(4)) * z[16];
z[18] = abb[15] * z[5];
z[19] = (T(1) / T(2)) * z[18];
z[20] = abb[19] * z[1];
z[21] = z[19] + -z[20];
z[22] = abb[25] * abb[51];
z[23] = z[21] + (T(1) / T(4)) * z[22];
z[24] = abb[48] + abb[50];
z[25] = z[8] + z[24];
z[26] = abb[14] * z[25];
z[27] = (T(1) / T(2)) * z[26];
z[28] = abb[51] * (T(1) / T(2));
z[29] = abb[22] * z[28];
z[30] = abb[24] * abb[51];
z[31] = z[29] + z[30];
z[32] = 3 * abb[47];
z[33] = abb[49] + z[24] + z[32];
z[34] = abb[0] * (T(1) / T(2));
z[33] = z[33] * z[34];
z[35] = abb[50] * (T(1) / T(2));
z[36] = abb[49] * (T(3) / T(2)) + -z[35];
z[37] = z[13] + -z[36];
z[37] = abb[3] * z[37];
z[38] = abb[49] + abb[50];
z[39] = abb[48] + z[32];
z[40] = -z[38] + z[39];
z[41] = abb[4] * (T(1) / T(2));
z[42] = z[40] * z[41];
z[43] = abb[49] * (T(1) / T(2));
z[44] = z[35] + z[43];
z[45] = 2 * abb[47];
z[46] = -z[44] + z[45];
z[46] = abb[8] * z[46];
z[47] = abb[17] * z[1];
z[48] = abb[2] * z[24];
z[49] = 2 * z[48];
z[31] = z[17] + z[23] + z[27] + (T(1) / T(2)) * z[31] + z[33] + z[37] + z[42] + z[46] + z[47] + -z[49];
z[31] = abb[31] * z[31];
z[26] = z[26] + z[30];
z[33] = abb[25] * z[28];
z[37] = z[26] + z[33];
z[42] = abb[50] + z[8];
z[46] = 3 * abb[48];
z[50] = -z[42] + z[46];
z[50] = abb[0] * z[50];
z[51] = abb[20] * z[28];
z[50] = -z[29] + -z[37] + z[50] + -z[51];
z[52] = abb[47] + z[24];
z[53] = 2 * z[52];
z[54] = abb[12] * z[53];
z[55] = 3 * z[47] + z[49] + -z[54];
z[56] = 2 * abb[48];
z[57] = z[44] + z[56];
z[57] = abb[3] * z[57];
z[58] = abb[47] + z[46];
z[59] = z[38] + z[58];
z[60] = z[41] * z[59];
z[61] = abb[50] * (T(3) / T(2)) + -z[43];
z[62] = z[13] + z[61];
z[62] = abb[8] * z[62];
z[11] = -z[11] + z[12] + -z[21] + (T(1) / T(2)) * z[50] + z[55] + z[57] + z[60] + z[62];
z[11] = abb[30] * z[11];
z[50] = -z[9] + z[48];
z[7] = z[7] + z[50];
z[57] = -abb[49] + abb[50];
z[60] = -z[13] + z[57];
z[62] = abb[7] * z[60];
z[63] = abb[48] + abb[49];
z[64] = abb[9] * z[63];
z[65] = (T(1) / T(4)) * z[62] + z[64];
z[66] = z[8] + -z[24];
z[67] = abb[0] * z[66];
z[68] = abb[4] * (T(1) / T(4));
z[69] = z[60] * z[68];
z[70] = -abb[47] + abb[50];
z[71] = abb[10] * z[70];
z[72] = abb[3] * abb[47];
z[73] = abb[8] * abb[48];
z[67] = z[7] + -z[23] + -z[65] + z[67] + z[69] + z[71] + z[72] + z[73];
z[67] = abb[28] * z[67];
z[69] = abb[22] * abb[51];
z[30] = z[30] + z[69];
z[72] = 2 * abb[3];
z[73] = z[8] * z[72];
z[51] = -z[15] + z[51];
z[27] = z[27] + (T(1) / T(2)) * z[30] + z[51] + -z[73];
z[30] = abb[8] * z[13];
z[73] = abb[43] + 2 * abb[52] + (T(1) / T(2)) * z[0];
z[74] = abb[16] * z[73];
z[30] = z[30] + z[74];
z[75] = 3 * abb[49] + -abb[50];
z[76] = -z[58] + z[75];
z[76] = z[34] * z[76];
z[77] = abb[13] * z[5];
z[76] = z[76] + z[77];
z[77] = abb[18] * z[73];
z[78] = abb[23] * z[28];
z[77] = z[77] + -z[78];
z[79] = 4 * z[9] + z[77];
z[80] = z[25] * z[41];
z[81] = z[27] + z[30] + z[76] + -z[79] + -z[80];
z[81] = abb[33] * z[81];
z[82] = -abb[49] + 3 * abb[50];
z[83] = z[39] + z[82];
z[83] = abb[0] * z[83];
z[83] = z[22] + z[83];
z[84] = z[26] + z[83];
z[85] = abb[19] * z[73];
z[85] = -z[18] + z[85];
z[86] = -z[54] + z[85];
z[87] = 4 * z[48] + z[86];
z[88] = 2 * abb[8];
z[89] = z[24] * z[88];
z[90] = abb[3] * z[13];
z[91] = abb[17] * z[73];
z[90] = z[90] + z[91];
z[80] = z[80] + (T(-1) / T(2)) * z[84] + z[87] + z[89] + z[90];
z[80] = abb[29] * z[80];
z[16] = z[16] + z[69];
z[84] = z[16] + z[62];
z[92] = (T(1) / T(2)) * z[84];
z[60] = -z[41] * z[60];
z[60] = z[30] + z[60] + z[90] + z[92];
z[60] = abb[32] * z[60];
z[11] = z[11] + z[31] + z[60] + z[67] + -z[80] + -z[81];
z[11] = abb[28] * z[11];
z[31] = -z[13] + z[38];
z[60] = abb[5] * z[31];
z[67] = (T(1) / T(2)) * z[60];
z[93] = -z[31] * z[41];
z[14] = -abb[0] * z[14];
z[94] = -2 * abb[49] + z[39];
z[94] = abb[3] * z[94];
z[95] = abb[8] * z[58];
z[14] = z[14] + z[29] + z[51] + z[67] + -z[79] + z[91] + z[93] + z[94] + z[95];
z[14] = abb[35] * z[14];
z[29] = (T(1) / T(2)) * z[62];
z[51] = 5 * abb[48];
z[79] = -7 * abb[47] + -z[51] + -z[82];
z[79] = z[34] * z[79];
z[93] = 5 * abb[47];
z[94] = abb[48] + -z[38] + z[93];
z[95] = abb[3] * (T(1) / T(2));
z[94] = z[94] * z[95];
z[96] = abb[49] + z[52];
z[97] = abb[6] * z[96];
z[28] = -abb[21] * z[28];
z[28] = z[28] + z[29] + -z[37] + z[64] + z[79] + z[94] + (T(1) / T(2)) * z[97];
z[37] = z[8] * z[41];
z[21] = -z[4] + -z[21] + (T(1) / T(2)) * z[28] + z[37] + z[47] + (T(7) / T(2)) * z[48] + -z[54] + -z[71] + z[89];
z[21] = prod_pow(abb[29], 2) * z[21];
z[28] = 2 * z[13];
z[36] = -z[28] + z[36];
z[36] = abb[3] * z[36];
z[37] = -z[28] + -z[61];
z[37] = abb[8] * z[37];
z[54] = abb[0] * z[57];
z[61] = -abb[4] * z[13];
z[17] = -z[17] + z[23] + z[36] + z[37] + z[54] + -z[55] + z[61] + (T(-1) / T(4)) * z[69];
z[17] = abb[31] * z[17];
z[23] = -z[80] + z[81];
z[36] = abb[21] * abb[51];
z[36] = z[36] + -z[97];
z[37] = z[16] + z[36];
z[22] = -z[22] + z[37];
z[54] = (T(1) / T(2)) * z[22];
z[8] = abb[48] + -abb[50] + z[8];
z[8] = abb[0] * z[8];
z[55] = abb[3] * z[5];
z[18] = z[8] + -z[18] + z[54] + z[55];
z[12] = z[12] + -z[47];
z[20] = z[12] + -z[20];
z[32] = -abb[49] + z[32];
z[47] = -z[24] + z[32];
z[61] = -z[47] * z[68];
z[69] = -abb[47] + z[44];
z[69] = abb[8] * z[69];
z[4] = z[4] + (T(1) / T(2)) * z[18] + -z[20] + z[50] + z[61] + z[69];
z[4] = abb[30] * z[4];
z[4] = z[4] + z[17] + z[23];
z[4] = abb[30] * z[4];
z[17] = -4 * abb[47] + z[35] + -z[43] + -z[51];
z[17] = abb[0] * z[17];
z[18] = abb[12] * z[52];
z[35] = abb[48] * (T(-1) / T(2)) + z[93];
z[35] = abb[3] * z[35];
z[17] = z[9] + z[17] + -z[18] + z[19] + z[20] + (T(-1) / T(4)) * z[22] + z[35] + -z[71];
z[18] = abb[48] + z[38];
z[18] = abb[47] + (T(-1) / T(3)) * z[18];
z[18] = z[18] * z[68];
z[19] = abb[23] * abb[51];
z[20] = abb[47] * (T(1) / T(3)) + abb[48] * (T(3) / T(2)) + (T(1) / T(6)) * z[57];
z[20] = abb[8] * z[20];
z[17] = (T(1) / T(3)) * z[17] + z[18] + (T(-1) / T(12)) * z[19] + z[20];
z[17] = prod_pow(m1_set::bc<T>[0], 2) * z[17];
z[18] = -7 * abb[48] + z[75] + -z[93];
z[18] = abb[0] * z[18];
z[19] = abb[47] + z[38] + z[51];
z[19] = abb[8] * z[19];
z[18] = -z[18] + -z[19] + z[60];
z[19] = -z[24] * z[41];
z[2] = -z[2] + (T(-7) / T(2)) * z[9] + (T(-1) / T(4)) * z[18] + z[19] + z[27] + z[65] + (T(-1) / T(2)) * z[71];
z[18] = prod_pow(abb[33], 2);
z[2] = z[2] * z[18];
z[19] = z[54] + z[78];
z[20] = z[41] * z[96];
z[22] = 2 * abb[50] + z[58];
z[22] = abb[8] * z[22];
z[35] = abb[0] * z[53];
z[39] = abb[3] * z[39];
z[20] = z[19] + z[20] + z[22] + -z[35] + z[39] + z[87] + z[91];
z[22] = -abb[54] * z[20];
z[35] = abb[47] + abb[49] + -z[24];
z[35] = abb[0] * z[35];
z[16] = (T(1) / T(2)) * z[16] + z[35] + z[67];
z[35] = -abb[48] + -z[44];
z[35] = abb[3] * z[35];
z[39] = z[42] + z[46];
z[42] = -z[39] * z[68];
z[43] = -abb[8] * z[6];
z[7] = z[7] + z[12] + (T(1) / T(2)) * z[16] + z[35] + z[42] + z[43];
z[7] = abb[31] * z[7];
z[7] = z[7] + -z[23];
z[7] = abb[31] * z[7];
z[12] = -z[26] + z[62];
z[16] = z[55] + z[91];
z[23] = 2 * z[64];
z[35] = -abb[4] * z[63];
z[35] = -z[10] + (T(-1) / T(2)) * z[12] + -z[16] + -z[23] + z[35] + z[76] + -z[77];
z[35] = abb[34] * z[35];
z[12] = z[12] + -z[36];
z[36] = z[95] * z[96];
z[42] = z[48] + z[64];
z[36] = z[36] + -z[42];
z[43] = z[41] * z[66];
z[12] = (T(-1) / T(2)) * z[12] + z[36] + -z[43] + z[78];
z[46] = abb[29] * z[12];
z[40] = abb[0] * z[40];
z[40] = z[26] + z[40] + -z[60];
z[48] = -abb[4] + -z[88];
z[48] = abb[48] * z[48];
z[40] = (T(1) / T(2)) * z[40] + z[48] + -z[90];
z[40] = abb[31] * z[40];
z[37] = z[26] + z[37];
z[48] = abb[0] * z[59];
z[48] = -z[37] + z[48];
z[50] = -abb[4] + -z[72];
z[50] = abb[47] * z[50];
z[30] = -z[30] + (T(1) / T(2)) * z[48] + z[50] + -z[78];
z[30] = abb[30] * z[30];
z[43] = z[43] + -z[71];
z[48] = -z[9] + z[43];
z[50] = z[48] + z[92];
z[51] = abb[8] * (T(1) / T(2));
z[52] = z[31] * z[51];
z[36] = -z[36] + z[50] + z[52];
z[36] = abb[32] * z[36];
z[26] = z[26] + z[84];
z[53] = z[26] + -z[60];
z[48] = z[48] + (T(1) / T(2)) * z[53];
z[52] = z[48] + z[52];
z[53] = -abb[33] * z[52];
z[30] = z[30] + (T(1) / T(2)) * z[36] + z[40] + z[46] + z[53];
z[30] = abb[32] * z[30];
z[36] = -abb[18] + -abb[19];
z[36] = z[6] * z[36];
z[40] = -abb[3] + abb[15];
z[40] = z[1] * z[40];
z[46] = -z[13] + -z[44];
z[46] = abb[17] * z[46];
z[53] = -abb[4] * z[73];
z[3] = -abb[26] * z[3];
z[0] = 2 * abb[43] + 4 * abb[52] + z[0];
z[54] = -abb[27] * z[0];
z[3] = z[3] + z[36] + z[40] + z[46] + z[53] + z[54];
z[3] = abb[38] * z[3];
z[36] = z[26] + z[83];
z[40] = z[49] + 2 * z[71];
z[46] = abb[4] * z[70];
z[53] = abb[8] * z[5];
z[36] = (T(-1) / T(2)) * z[36] + z[40] + z[46] + z[53] + z[85];
z[46] = -abb[53] * z[36];
z[12] = -abb[36] * z[12];
z[53] = z[51] + -z[95];
z[25] = z[25] * z[53];
z[53] = -abb[0] * z[13];
z[25] = z[25] + z[42] + z[50] + z[53];
z[25] = abb[37] * z[25];
z[42] = abb[35] + abb[53] + -abb[54];
z[42] = z[42] * z[73];
z[44] = -z[13] + z[44];
z[44] = abb[38] * z[44];
z[50] = z[1] * z[18];
z[42] = z[42] + z[44] + z[50];
z[42] = abb[16] * z[42];
z[44] = -abb[55] * z[48];
z[6] = z[6] * z[18];
z[1] = abb[38] * z[1];
z[5] = abb[35] * z[5];
z[5] = z[1] + z[5] + z[6];
z[5] = abb[13] * z[5];
z[6] = abb[55] * z[31];
z[1] = -z[1] + (T(-1) / T(2)) * z[6];
z[1] = abb[8] * z[1];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[7] + z[11] + z[12] + z[14] + z[17] + z[21] + z[22] + z[25] + z[30] + z[35] + z[42] + z[44] + z[46];
z[0] = abb[17] * z[0];
z[0] = z[0] + -z[33] + z[86];
z[2] = -z[58] + z[82];
z[2] = z[2] * z[34];
z[3] = -abb[3] * z[59];
z[4] = -z[28] + -z[57];
z[4] = abb[8] * z[4];
z[5] = abb[50] + z[56];
z[6] = -abb[47] + -z[5];
z[6] = abb[4] * z[6];
z[2] = -z[0] + z[2] + z[3] + z[4] + z[6] + -z[10] + z[23] + (T(1) / T(2)) * z[26] + -z[40] + -z[74];
z[2] = abb[28] * z[2];
z[3] = z[10] + -z[49];
z[4] = -z[24] + -z[32];
z[4] = abb[0] * z[4];
z[6] = 3 * z[13] + z[57];
z[6] = abb[3] * z[6];
z[7] = z[39] * z[41];
z[5] = z[5] * z[88];
z[0] = z[0] + -z[3] + z[4] + z[5] + z[6] + z[7] + -z[15] + -z[67];
z[0] = abb[31] * z[0];
z[4] = z[41] * z[47];
z[5] = -z[38] + z[45];
z[5] = abb[8] * z[5];
z[3] = z[3] + z[4] + z[5] + -z[8] + -z[16] + -z[19] + z[74] + -z[85];
z[3] = abb[30] * z[3];
z[4] = z[39] * z[51];
z[5] = abb[0] * z[28];
z[4] = -z[4] + z[5];
z[5] = z[4] + 3 * z[9] + -z[23] + -z[27] + -z[29] + -z[43] + z[67];
z[5] = abb[33] * z[5];
z[6] = abb[47] * z[72];
z[4] = -z[4] + z[6] + -z[9] + (T(1) / T(2)) * z[37] + -z[71] + z[78];
z[4] = abb[32] * z[4];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[80];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = -z[20] + -z[74];
z[2] = abb[40] * z[2];
z[3] = -z[36] + z[74];
z[3] = abb[39] * z[3];
z[4] = -abb[41] * z[52];
z[0] = z[0] + z[2] + z[3] + z[4];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_244_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("3.0212403926155634861881016532672724583386885189432450770666360627"),stof<T>("-6.1454510857536799062108407455617833387997981486530960398138588694")}, std::complex<T>{stof<T>("6.0424807852311269723762033065345449166773770378864901541332721254"),stof<T>("-12.2909021715073598124216814911235666775995962973061920796277177388")}, std::complex<T>{stof<T>("3.0212403926155634861881016532672724583386885189432450770666360627"),stof<T>("-6.1454510857536799062108407455617833387997981486530960398138588694")}, std::complex<T>{stof<T>("-3.0212403926155634861881016532672724583386885189432450770666360627"),stof<T>("6.1454510857536799062108407455617833387997981486530960398138588694")}, std::complex<T>{stof<T>("-3.0212403926155634861881016532672724583386885189432450770666360627"),stof<T>("6.1454510857536799062108407455617833387997981486530960398138588694")}, std::complex<T>{stof<T>("-6.9677034655081426145254938301974874778519076555609150602244021842"),stof<T>("-2.4670173279991011741196022669295648127619777470011308985795000179")}, std::complex<T>{stof<T>("22.530656495243534782469072771938344985092784825473562749524909763"),stof<T>("2.078533442339936977156719992571484005950635767989453659081975135")}, std::complex<T>{stof<T>("-1.1267222078670382437443633273720852016511522812744344454673585005"),stof<T>("-2.0332855638317073435153816395082973838495214591324097321920347361")}, std::complex<T>{stof<T>("0.0620301157883849391947034340858736621477752766585777805754366075"),stof<T>("-1.6123378821367580291422256596178032175735468016471634226558303079")}, std::complex<T>{stof<T>("0.65014392619570208351699626144945763353564230515482995745745358731"),stof<T>("-0.79808681113405645396023651105679316073000096384394056216966926958")}, std::complex<T>{stof<T>("12.084961570462253944752406613069089833354754075772980308266544251"),stof<T>("-24.581804343014719624843362982247133355199192594612384159255435478")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[123].real()/kbase.W[123].real()), rlog(k.W[128].real()/kbase.W[128].real()), C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}, rlog(k.W[194].real()/kbase.W[194].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_244_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_244_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-42.890624163918968045148916689928925894404543873940612323993144384"),stof<T>("-9.990433145954740370336546595559296574844265280949463489817276881")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,56> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W15(k,dl), dlog_W18(k,dl), dlog_W22(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_19(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_11_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real()), rlog(kend.W[128].real()/k.W[128].real()), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), rlog(kend.W[194].real()/k.W[194].real()), f_2_4_re(k), f_2_7_re(k), f_2_11_re(k)};

                    
            return f_4_244_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_244_DLogXconstant_part(base_point<T>, kend);
	value += f_4_244_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_244_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_244_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_244_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_244_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_244_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_244_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
