/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_593.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_593_abbreviated (const std::array<T,69>& abb) {
T z[139];
z[0] = abb[52] * (T(1) / T(2));
z[1] = abb[51] * (T(1) / T(2));
z[2] = z[0] + z[1];
z[3] = -abb[57] + abb[58];
z[4] = 2 * abb[50];
z[5] = abb[56] * (T(1) / T(2));
z[6] = 2 * abb[48];
z[7] = -abb[47] + -z[2] + -z[3] + z[4] + z[5] + -z[6];
z[7] = abb[8] * z[7];
z[8] = abb[52] + -abb[55];
z[9] = 2 * abb[57];
z[10] = z[8] + -z[9];
z[11] = 4 * abb[58];
z[12] = abb[48] + -abb[56];
z[13] = 2 * abb[54];
z[14] = z[10] + z[11] + -z[12] + -z[13];
z[14] = abb[1] * z[14];
z[15] = abb[52] * (T(3) / T(2));
z[16] = -abb[57] + -z[1] + z[15];
z[17] = -abb[50] + abb[55];
z[18] = abb[49] + -abb[54];
z[19] = z[17] + z[18];
z[20] = z[16] + -z[19];
z[21] = -abb[48] + z[5];
z[22] = z[20] + z[21];
z[23] = abb[16] * z[22];
z[24] = 2 * abb[58];
z[25] = -z[13] + z[24];
z[26] = abb[51] + z[17];
z[27] = z[25] + z[26];
z[27] = abb[18] * z[27];
z[28] = z[23] + -z[27];
z[29] = abb[50] + z[13];
z[30] = 3 * abb[58];
z[31] = z[29] + -z[30];
z[21] = z[21] + -z[31];
z[32] = -abb[47] + abb[53];
z[33] = abb[49] + -z[2] + -z[21] + -z[32];
z[33] = abb[9] * z[33];
z[17] = z[17] + -z[18];
z[34] = 2 * abb[47];
z[35] = -z[17] + z[34];
z[36] = abb[6] * z[35];
z[37] = 3 * abb[55];
z[38] = abb[50] + abb[54];
z[39] = z[37] + -z[38];
z[40] = 3 * abb[53];
z[41] = -4 * abb[49] + 2 * abb[51] + z[39] + z[40];
z[42] = abb[4] * z[41];
z[43] = 2 * abb[49];
z[44] = 2 * abb[53];
z[45] = z[43] + -z[44];
z[46] = -abb[58] + z[13];
z[47] = -abb[47] + -abb[50] + -z[45] + z[46];
z[47] = abb[7] * z[47];
z[48] = abb[61] + abb[62] + abb[63] + abb[64];
z[49] = abb[24] * z[48];
z[50] = 2 * z[49];
z[51] = abb[49] + -abb[55];
z[52] = abb[47] + z[51];
z[53] = abb[13] * z[52];
z[54] = 4 * z[53];
z[55] = abb[26] * z[48];
z[56] = 2 * z[55];
z[57] = abb[23] * z[48];
z[58] = 2 * z[57];
z[59] = abb[25] * z[48];
z[7] = z[7] + z[14] + -z[28] + z[33] + -z[36] + z[42] + z[47] + z[50] + z[54] + z[56] + z[58] + z[59];
z[14] = prod_pow(abb[32], 2);
z[7] = z[7] * z[14];
z[33] = 3 * abb[47];
z[47] = -z[9] + z[33];
z[60] = 5 * abb[49] + -z[40];
z[61] = abb[51] * (T(3) / T(2));
z[62] = 2 * abb[55];
z[15] = z[15] + -z[21] + z[47] + -z[60] + z[61] + z[62];
z[15] = abb[9] * z[15];
z[21] = abb[47] + abb[55];
z[63] = 2 * z[21];
z[6] = -abb[56] + z[6];
z[64] = abb[51] + z[6];
z[65] = -abb[52] + z[64];
z[66] = -z[24] + z[63] + z[65];
z[67] = abb[1] * z[66];
z[68] = -z[13] + z[37];
z[69] = -abb[51] + -z[68];
z[70] = -abb[46] + abb[48];
z[71] = 2 * abb[52];
z[72] = -abb[47] + z[71];
z[73] = z[69] + -z[70] + z[72];
z[73] = abb[0] * z[73];
z[16] = -abb[46] + -abb[58] + z[5] + z[16] + -2 * z[18];
z[16] = abb[8] * z[16];
z[74] = -abb[46] + abb[58];
z[75] = -abb[55] + abb[57] + -z[34] + -z[64] + z[74];
z[75] = abb[3] * z[75];
z[76] = abb[58] + z[18];
z[77] = -abb[53] + z[76];
z[78] = abb[7] * z[77];
z[79] = abb[53] + -abb[55];
z[80] = abb[46] + z[79];
z[81] = abb[2] * z[80];
z[82] = z[78] + z[81];
z[83] = 2 * abb[46];
z[17] = -z[17] + z[83];
z[84] = abb[17] * z[17];
z[85] = abb[28] * z[48];
z[84] = z[84] + z[85];
z[85] = abb[27] * z[48];
z[86] = -z[84] + z[85];
z[87] = abb[46] + z[51];
z[87] = abb[10] * z[87];
z[88] = 4 * z[87];
z[15] = z[15] + z[16] + -z[28] + -z[42] + -z[67] + z[73] + z[75] + -z[82] + -z[86] + -z[88];
z[15] = abb[31] * z[15];
z[16] = -abb[55] + z[43];
z[28] = z[16] + -z[29] + -z[34];
z[29] = abb[52] + z[44];
z[44] = -z[9] + z[29] + z[64];
z[73] = 6 * abb[58];
z[28] = 2 * z[28] + -z[44] + z[73];
z[28] = abb[9] * z[28];
z[75] = 3 * abb[52];
z[19] = -z[9] + -2 * z[19] + -z[64] + z[75];
z[89] = abb[16] * z[19];
z[90] = -z[56] + z[89];
z[91] = -z[58] + z[90];
z[92] = abb[47] + -abb[50];
z[93] = z[18] + z[92];
z[93] = z[24] + z[65] + 2 * z[93];
z[93] = abb[8] * z[93];
z[94] = -z[30] + z[37];
z[64] = -abb[57] + -z[64] + z[72] + -z[94];
z[72] = 2 * abb[1];
z[95] = z[64] * z[72];
z[96] = z[27] + z[49];
z[97] = z[59] + z[96];
z[98] = 2 * z[78];
z[93] = z[28] + z[91] + z[93] + -z[95] + -2 * z[97] + z[98];
z[93] = abb[32] * z[93];
z[97] = abb[32] * z[66];
z[99] = 2 * abb[3];
z[100] = z[97] * z[99];
z[93] = z[93] + z[100];
z[15] = z[15] + z[93];
z[15] = abb[31] * z[15];
z[100] = abb[56] * (T(3) / T(4));
z[101] = abb[46] * (T(3) / T(4));
z[102] = abb[48] * (T(1) / T(4));
z[103] = z[0] + -z[1] + -z[21] + z[100] + z[101] + -z[102];
z[103] = abb[0] * z[103];
z[104] = z[49] + z[55];
z[23] = z[23] + -z[104];
z[105] = z[12] + z[34];
z[10] = z[10] + z[13] + z[105];
z[10] = abb[1] * z[10];
z[106] = abb[50] + abb[57];
z[107] = -abb[54] + z[106];
z[108] = abb[46] * (T(1) / T(2));
z[109] = z[107] + -z[108];
z[110] = abb[48] * (T(3) / T(2)) + -z[5];
z[52] = -z[52] + z[109] + -z[110];
z[52] = abb[8] * z[52];
z[111] = abb[46] * (T(1) / T(4));
z[112] = -z[20] + z[111];
z[113] = abb[56] * (T(1) / T(4));
z[102] = z[102] + z[113];
z[114] = abb[47] + -z[102] + -z[112];
z[114] = abb[3] * z[114];
z[115] = -abb[50] + -z[18] + z[32];
z[116] = abb[7] * z[115];
z[117] = abb[47] + z[12];
z[117] = abb[12] * z[117];
z[10] = z[10] + -z[23] + z[36] + z[52] + z[59] + -z[81] + z[103] + z[114] + z[116] + -2 * z[117];
z[10] = abb[34] * z[10];
z[52] = abb[48] + abb[58];
z[103] = -z[47] + -z[52] + z[62] + -z[71];
z[103] = z[72] * z[103];
z[114] = abb[49] + abb[54];
z[116] = z[92] + z[114];
z[9] = -z[9] + z[65] + 2 * z[116];
z[9] = abb[8] * z[9];
z[29] = -z[6] + z[29];
z[16] = abb[47] + z[16];
z[16] = -abb[51] + 2 * z[16] + -z[29];
z[116] = abb[9] * z[16];
z[118] = -z[59] + z[117];
z[119] = -z[49] + z[118];
z[120] = 2 * abb[7];
z[115] = -z[115] * z[120];
z[121] = abb[47] + z[8];
z[3] = z[3] + z[121];
z[122] = -z[3] * z[99];
z[9] = z[9] + z[91] + z[103] + z[115] + z[116] + 2 * z[119] + z[122];
z[9] = abb[32] * z[9];
z[91] = -abb[55] + z[24];
z[103] = abb[46] * (T(5) / T(4)) + z[2] + z[18] + z[91] + z[102] + -z[106];
z[103] = abb[3] * z[103];
z[106] = abb[47] + -abb[54];
z[106] = abb[48] * (T(7) / T(4)) + z[2] + z[101] + 2 * z[106] + -z[113];
z[106] = abb[0] * z[106];
z[113] = 2 * z[59];
z[115] = z[58] + z[113];
z[119] = 2 * z[81];
z[122] = z[86] + z[115] + z[119];
z[3] = z[3] * z[72];
z[3] = z[3] + -z[23];
z[123] = -abb[52] + z[26];
z[105] = abb[46] + abb[57] + -z[105] + -z[114] + -z[123];
z[105] = abb[8] * z[105];
z[103] = z[3] + z[103] + z[105] + z[106] + -z[116] + z[122];
z[103] = abb[31] * z[103];
z[101] = -z[101] + z[102];
z[2] = -z[2] + -z[25] + z[101];
z[2] = abb[0] * z[2];
z[25] = -abb[50] + abb[58];
z[102] = -abb[46] + z[25];
z[102] = abb[14] * z[102];
z[2] = z[2] + 2 * z[102];
z[105] = z[6] + z[34] + z[51] + -z[107];
z[105] = abb[8] * z[105];
z[100] = abb[48] * (T(5) / T(4)) + -z[100] + z[112];
z[100] = abb[3] * z[100];
z[106] = z[98] + z[119];
z[3] = -z[2] + -z[3] + z[86] + z[100] + z[105] + z[106];
z[3] = abb[33] * z[3];
z[100] = abb[53] + abb[58];
z[105] = z[26] + z[100] + -z[114];
z[107] = abb[32] * z[105];
z[112] = abb[31] * z[105];
z[107] = z[107] + -z[112];
z[116] = 2 * abb[15];
z[119] = z[107] * z[116];
z[3] = z[3] + z[9] + z[10] + z[103] + z[119];
z[3] = abb[34] * z[3];
z[9] = abb[51] + -abb[55];
z[10] = -3 * abb[50] + z[9] + z[30] + z[40] + -z[114];
z[10] = abb[5] * z[10];
z[31] = z[31] + -z[51];
z[31] = abb[9] * z[31];
z[31] = z[31] + z[42];
z[103] = z[10] + z[31];
z[114] = 5 * abb[53];
z[124] = 3 * abb[46];
z[43] = abb[50] + -abb[51] + z[43] + z[62] + -z[114] + -z[124];
z[43] = abb[8] * z[43];
z[125] = 3 * abb[51] + -abb[58];
z[68] = -z[68] + -z[124] + -z[125];
z[68] = abb[0] * z[68];
z[126] = z[105] * z[116];
z[127] = abb[3] * z[77];
z[128] = z[85] + z[127];
z[88] = z[88] + -z[102];
z[43] = z[43] + z[68] + -9 * z[81] + z[84] + z[88] + -z[98] + z[103] + -z[115] + z[126] + -2 * z[128];
z[43] = abb[36] * z[43];
z[68] = z[57] + z[59];
z[98] = z[68] + z[86];
z[115] = 3 * z[81];
z[126] = z[98] + z[115];
z[128] = -abb[51] + z[46];
z[129] = abb[46] + abb[55] + -z[128];
z[129] = abb[0] * z[129];
z[129] = -z[102] + z[129];
z[130] = z[78] + z[127];
z[80] = abb[8] * z[80];
z[80] = z[80] + z[126] + z[129] + z[130];
z[131] = abb[31] + abb[33];
z[131] = z[80] * z[131];
z[132] = abb[33] * z[105];
z[133] = -z[112] + -z[132];
z[133] = abb[15] * z[133];
z[134] = abb[15] * z[105];
z[80] = z[80] + -z[134];
z[135] = -abb[34] * z[80];
z[131] = z[131] + z[133] + z[135];
z[43] = z[43] + 2 * z[131];
z[43] = abb[36] * z[43];
z[131] = 4 * abb[47];
z[0] = abb[50] + -abb[57] + abb[56] * (T(-5) / T(4)) + abb[48] * (T(11) / T(4)) + -z[0] + -z[11] + -z[18] + z[37] + z[61] + -z[111] + z[131];
z[0] = abb[3] * z[0];
z[37] = z[66] * z[72];
z[61] = abb[49] + -3 * abb[54] + abb[57] + z[24] + z[123];
z[61] = abb[8] * z[61];
z[0] = z[0] + z[2] + z[23] + -2 * z[27] + z[28] + z[37] + z[61] + -z[122];
z[0] = abb[31] * z[0];
z[2] = z[27] + z[104];
z[23] = -z[2] + z[10];
z[27] = z[23] + -z[84];
z[28] = -z[13] + z[30];
z[37] = abb[46] * (T(-3) / T(2)) + -z[8] + -z[28] + z[34] + z[110];
z[37] = abb[0] * z[37];
z[61] = -z[67] + 3 * z[102];
z[66] = -abb[55] + z[76];
z[108] = z[66] + z[108];
z[110] = abb[48] * (T(1) / T(2));
z[111] = -z[5] + -z[108] + z[110];
z[111] = abb[8] * z[111];
z[122] = -z[18] + z[40];
z[4] = z[4] + z[62];
z[123] = abb[58] + -z[4] + z[122];
z[133] = -z[34] + z[83] + z[123];
z[135] = abb[3] * z[133];
z[37] = -z[27] + z[37] + z[61] + -z[82] + z[111] + z[135];
z[37] = abb[33] * z[37];
z[0] = z[0] + z[37] + -z[93];
z[0] = abb[33] * z[0];
z[31] = z[2] + z[31];
z[37] = -z[34] + -z[40] + z[62] + z[76];
z[37] = abb[7] * z[37];
z[40] = -z[21] + z[128];
z[40] = abb[1] * z[40];
z[57] = z[36] + -z[57];
z[40] = z[40] + z[57];
z[82] = abb[49] + z[38];
z[93] = abb[51] + abb[58];
z[111] = -abb[47] + z[62] + -z[82] + z[93];
z[111] = abb[8] * z[111];
z[37] = -z[31] + z[37] + z[40] + z[111];
z[37] = abb[32] * z[37];
z[111] = z[18] + z[30];
z[128] = abb[53] + -z[4] + z[111];
z[135] = abb[7] * z[128];
z[45] = -z[26] + z[45];
z[45] = abb[8] * z[45];
z[68] = z[68] + z[85];
z[45] = -z[45] + z[68] + -z[103] + z[135];
z[85] = 3 * abb[49];
z[103] = abb[51] + abb[53];
z[94] = -z[38] + z[85] + -z[94] + -z[103];
z[135] = -abb[15] * z[94];
z[136] = abb[3] * z[128];
z[135] = z[45] + z[135] + z[136];
z[135] = abb[36] * z[135];
z[137] = abb[8] * z[105];
z[130] = -z[31] + z[130] + z[137];
z[137] = -abb[31] * z[130];
z[138] = z[99] + z[120];
z[25] = z[25] + z[79];
z[25] = z[25] * z[138];
z[77] = abb[8] * z[77];
z[23] = -z[23] + z[25] + z[68] + -z[77];
z[25] = -abb[33] * z[23];
z[38] = 5 * abb[55] + -z[38] + -z[60] + z[125];
z[38] = abb[32] * z[38];
z[60] = abb[58] + 2 * z[51] + -z[103];
z[68] = abb[31] * z[60];
z[38] = z[38] + 2 * z[68] + z[132];
z[38] = abb[15] * z[38];
z[68] = abb[8] + z[120];
z[77] = abb[47] + z[79];
z[68] = z[68] * z[77];
z[40] = -z[40] + z[68] + z[127] + -z[134];
z[40] = abb[34] * z[40];
z[25] = z[25] + z[37] + z[38] + z[40] + z[135] + z[137];
z[37] = -4 * abb[55] + -z[76] + z[114] + z[131];
z[37] = abb[7] * z[37];
z[38] = abb[53] + z[34];
z[39] = abb[49] + z[38] + -z[39] + -z[93];
z[39] = abb[8] * z[39];
z[21] = abb[51] + z[21];
z[40] = -abb[58] + z[21];
z[40] = z[40] * z[72];
z[68] = abb[15] * z[60];
z[31] = z[31] + z[37] + z[39] + z[40] + -z[54] + 4 * z[68] + -z[136];
z[31] = abb[35] * z[31];
z[25] = 2 * z[25] + z[31];
z[25] = abb[35] * z[25];
z[19] = abb[3] * z[19];
z[31] = 3 * abb[48];
z[37] = abb[50] + -abb[54] + -z[51];
z[37] = abb[56] + -z[31] + 2 * z[37] + -z[47];
z[37] = abb[8] * z[37];
z[39] = -abb[54] + abb[55];
z[40] = abb[47] + -2 * z[39];
z[40] = -abb[51] + abb[56] + -4 * abb[57] + 2 * z[40] + z[75];
z[40] = abb[1] * z[40];
z[47] = -z[35] * z[120];
z[19] = z[19] + 2 * z[36] + z[37] + z[40] + z[47] + z[50] + -z[90] + z[113] + -z[117];
z[19] = abb[38] * z[19];
z[37] = -z[38] + -z[62] + -z[65] + z[111];
z[37] = z[37] * z[99];
z[38] = -z[94] * z[116];
z[40] = -abb[49] + z[34];
z[40] = 2 * z[40] + z[44];
z[40] = abb[9] * z[40];
z[44] = -z[42] + z[59];
z[16] = -abb[8] * z[16];
z[16] = z[16] + z[37] + z[38] + z[40] + 2 * z[44] + z[58] + -z[89] + z[95];
z[16] = abb[37] * z[16];
z[37] = z[84] + z[96];
z[38] = abb[47] * (T(1) / T(2)) + z[108];
z[38] = abb[8] * z[38];
z[38] = z[37] + -z[38] + z[55] + -11 * z[87] + 8 * z[102];
z[40] = abb[49] * (T(1) / T(3));
z[44] = -5 * abb[50] + -abb[54];
z[32] = (T(2) / T(3)) * z[32] + z[40] + (T(1) / T(3)) * z[44] + z[91];
z[32] = abb[7] * z[32];
z[44] = abb[47] * (T(13) / T(2));
z[1] = 7 * abb[55] + z[1] + -z[13] + -z[44];
z[47] = abb[48] + abb[52];
z[47] = (T(13) / T(6)) * z[47];
z[1] = abb[58] * (T(8) / T(3)) + (T(1) / T(3)) * z[1] + -z[47] + -z[83];
z[1] = abb[0] * z[1];
z[9] = -abb[54] + z[9];
z[9] = -abb[50] + (T(1) / T(3)) * z[9] + -z[40] + z[100];
z[9] = abb[5] * z[9];
z[4] = -abb[58] + abb[46] * (T(-13) / T(2)) + z[4] + z[18] + z[44];
z[4] = -abb[53] + (T(1) / T(3)) * z[4];
z[4] = abb[3] * z[4];
z[40] = abb[58] * (T(-7) / T(3)) + abb[51] * (T(1) / T(6)) + abb[47] * (T(9) / T(2)) + z[47] + -z[62];
z[40] = abb[1] * z[40];
z[1] = z[1] + z[4] + z[9] + z[32] + (T(-1) / T(3)) * z[38] + z[40] + (T(-11) / T(3)) * z[53] + (T(-2) / T(3)) * z[81];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[4] = -abb[54] + z[21];
z[4] = z[4] * z[72];
z[4] = z[4] + z[53] + -z[57];
z[9] = z[42] + z[104];
z[21] = -z[62] + z[92] + z[122];
z[32] = -abb[7] * z[21];
z[38] = abb[15] * z[41];
z[35] = abb[8] * z[35];
z[32] = -z[4] + -z[9] + z[32] + -z[35] + z[38];
z[32] = abb[66] * z[32];
z[39] = abb[46] + abb[51] + z[39];
z[40] = 2 * abb[0];
z[39] = z[39] * z[40];
z[40] = abb[8] * z[17];
z[9] = z[9] + z[39] + z[40] + z[87] + z[126];
z[38] = z[9] + -z[38];
z[38] = abb[65] * z[38];
z[39] = z[60] * z[116];
z[39] = z[39] + -z[130];
z[39] = abb[40] * z[39];
z[32] = z[32] + z[38] + z[39];
z[38] = abb[39] * z[94];
z[39] = -abb[33] * z[107];
z[40] = -z[14] * z[105];
z[44] = abb[32] * z[112];
z[38] = z[38] + z[39] + z[40] + z[44];
z[38] = z[38] * z[116];
z[5] = -z[5] + -z[51] + z[109] + -z[110];
z[5] = abb[19] * z[5];
z[22] = -abb[21] * z[22];
z[20] = z[20] + -z[101];
z[20] = abb[20] * z[20];
z[39] = abb[59] + -abb[60];
z[40] = abb[8] + -abb[30] + abb[0] * (T(1) / T(4)) + abb[3] * (T(3) / T(4));
z[40] = -z[39] * z[40];
z[17] = -abb[22] * z[17];
z[44] = -abb[29] * z[48];
z[5] = z[5] + z[17] + z[20] + z[22] + z[40] + z[44];
z[5] = abb[41] * z[5];
z[17] = 2 * abb[39];
z[20] = -z[17] * z[45];
z[22] = abb[3] + abb[7];
z[22] = z[22] * z[123];
z[40] = abb[46] + z[66];
z[40] = abb[8] * z[40];
z[22] = -z[22] + z[27] + z[40] + z[115] + z[129];
z[22] = 2 * z[22];
z[27] = abb[67] * z[22];
z[14] = z[14] * z[64];
z[17] = -z[17] * z[128];
z[14] = z[14] + z[17];
z[14] = abb[3] * z[14];
z[17] = -abb[31] + abb[34];
z[40] = -abb[33] + -3 * z[17];
z[40] = abb[34] * z[40];
z[44] = abb[31] * abb[33];
z[40] = z[40] + z[44];
z[40] = abb[20] * z[40];
z[44] = -abb[31] + abb[33];
z[44] = abb[19] * z[44];
z[45] = abb[33] + abb[34];
z[45] = z[44] * z[45];
z[47] = (T(-1) / T(4)) * z[40] + (T(1) / T(2)) * z[45];
z[47] = abb[60] * z[47];
z[40] = (T(1) / T(2)) * z[40] + -z[45];
z[40] = abb[59] * z[40];
z[0] = abb[68] + z[0] + z[1] + z[3] + z[5] + z[7] + z[14] + z[15] + z[16] + z[19] + z[20] + z[25] + z[27] + 2 * z[32] + z[38] + (T(1) / T(2)) * z[40] + z[43] + z[47];
z[1] = z[13] + z[34];
z[3] = -z[1] + z[26] + z[29] + z[30] + -z[85];
z[3] = abb[9] * z[3];
z[2] = -z[2] + z[3] + -z[42];
z[3] = z[6] + z[8] + -z[28] + z[33];
z[3] = abb[1] * z[3];
z[5] = abb[52] + z[52] + -z[82];
z[5] = abb[8] * z[5];
z[3] = z[2] + z[3] + z[5] + -z[54] + z[57] + z[78] + -z[117];
z[3] = abb[32] * z[3];
z[5] = abb[36] * z[80];
z[6] = z[23] + -z[134];
z[6] = abb[35] * z[6];
z[7] = abb[3] * z[97];
z[3] = z[3] + -z[5] + z[6] + z[7];
z[5] = -z[69] + -z[71] + z[74];
z[5] = abb[0] * z[5];
z[2] = -z[2] + z[5] + -z[67] + z[81] + z[88] + z[98];
z[5] = abb[56] + z[71];
z[6] = z[5] + z[24];
z[7] = -z[6] + -z[70] + 2 * z[82];
z[7] = abb[8] * z[7];
z[5] = -z[5] + z[31];
z[8] = -abb[51] + -z[63];
z[8] = abb[46] + -z[5] + 2 * z[8] + z[11];
z[8] = abb[3] * z[8];
z[2] = 2 * z[2] + z[7] + z[8];
z[2] = abb[31] * z[2];
z[7] = z[10] + -z[37] + -z[61];
z[1] = -abb[55] + -z[1];
z[1] = 2 * z[1] + -z[5] + z[73] + z[124];
z[1] = abb[0] * z[1];
z[5] = -abb[55] + z[18];
z[5] = abb[46] + 2 * z[5] + -z[12] + z[24];
z[5] = abb[8] * z[5];
z[8] = -z[99] * z[133];
z[1] = z[1] + z[5] + 2 * z[7] + z[8] + -z[56] + z[106];
z[1] = abb[33] * z[1];
z[5] = abb[56] + -z[46] + z[121];
z[5] = abb[1] * z[5];
z[5] = z[5] + -z[36] + -z[86] + z[102] + z[118];
z[7] = abb[55] + z[13];
z[6] = -abb[48] + -z[6] + 2 * z[7] + -z[124];
z[6] = abb[0] * z[6];
z[7] = -abb[46] + -z[12];
z[7] = abb[3] * z[7];
z[5] = 2 * z[5] + z[6] + z[7] + -z[106];
z[5] = abb[34] * z[5];
z[6] = abb[20] * z[17];
z[6] = z[6] + z[44];
z[6] = z[6] * z[39];
z[1] = z[1] + z[2] + 2 * z[3] + z[5] + z[6] + z[119];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[42] * z[9];
z[3] = abb[44] * z[22];
z[4] = z[4] + z[35] + z[42] + z[49];
z[5] = -z[21] * z[120];
z[4] = -2 * z[4] + z[5] + -z[56];
z[4] = abb[43] * z[4];
z[5] = -abb[42] + abb[43];
z[5] = z[5] * z[41] * z[116];
z[1] = abb[45] + z[1] + 2 * z[2] + z[3] + z[4] + z[5];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_593_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("33.706053055515298288839095570101245313636647942428658327598171961"),stof<T>("50.961377933537765307332419140777549464780828639273489602082343258")}, std::complex<T>{stof<T>("11.858026132483910657718379006669116221305018847510705623791277394"),stof<T>("-56.879646427640951417939862551058491074130978128676838193480500277")}, std::complex<T>{stof<T>("15.906370797905423851752454953211023491375675565932195527849166268"),stof<T>("11.057996405110180876423471423951151950785385011025780364855342733")}, std::complex<T>{stof<T>("-26.838331748098920589511380876061648957629331972514306536496090407"),stof<T>("-11.156613126976248734663096518086661797552330618153893688362247169")}, std::complex<T>{stof<T>("19.951921924348071445516545983985641398493729237037885813901714986"),stof<T>("-9.195139706723453282066128734824871446144474898744586122859910221")}, std::complex<T>{stof<T>("3.99214115123710515555086421751400831887046925024364274524650727"),stof<T>("-18.091150631019225221082721300622652058801548881111807164189096765")}, std::complex<T>{stof<T>("32.551977062607529083248990298121337283831124709239946400888151237"),stof<T>("21.466638565505560250781031719302091742362819897581158019157754403")}, std::complex<T>{stof<T>("6.7353799965619372045429879339872034198064024037499747569895055434"),stof<T>("-1.3422343308323370821987211391889269365807527714048069895205015157")}, std::complex<T>{stof<T>("-1.721504163271503338186745204545680007331323486481997119145553561"),stof<T>("-28.401176069548536737200656501838082003612038160539071494984603999")}, std::complex<T>{stof<T>("-0.2763918106676341814796327240858328421747738605893962837744060502"),stof<T>("1.9232461890621132253448052434278699448465971300673805737648824541")}, stof<T>("-8.1467639364098125700497265545092086142014652425291793387481963645"), std::complex<T>{stof<T>("-38.162658122975090905833493810102835172170037353845123221464291653"),stof<T>("15.647282074402721483579294507257428545894794410168039337949283315")}, std::complex<T>{stof<T>("1.7314772302804613735427175946745870918310611219429528881459093378"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("-1.7314772302804613735427175946745870918310611219429528881459093378"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("-3.1923472445362258158409460442271726429200038553757622486786770783")}, std::complex<T>{stof<T>("2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("-3.1923472445362258158409460442271726429200038553757622486786770783")}, std::complex<T>{stof<T>("2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("-3.1923472445362258158409460442271726429200038553757622486786770783")}, std::complex<T>{stof<T>("2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("-3.1923472445362258158409460442271726429200038553757622486786770783")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[33].real()/kbase.W[33].real()), rlog(k.W[70].real()/kbase.W[70].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[123].real()/kbase.W[123].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_593_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_593_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(56)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (2 * (112 * m1_set::bc<T>[1] + -84 * m1_set::bc<T>[2] + 56 * m1_set::bc<T>[4] + -49 * v[1] + -35 * v[2] + 21 * v[3] + 36 * v[4] + 7 * v[5]) + 7 * (6 * v[0] + 8 * m1_set::bc<T>[1] * v[0] + -21 * m1_set::bc<T>[2] * v[0] + 8 * m1_set::bc<T>[1] * v[1] + m1_set::bc<T>[2] * v[1] + 8 * m1_set::bc<T>[1] * v[2] + -9 * m1_set::bc<T>[2] * v[2] + -8 * m1_set::bc<T>[1] * v[3] + 3 * m1_set::bc<T>[2] * v[3] + -16 * m1_set::bc<T>[1] * v[4] + 12 * m1_set::bc<T>[2] * v[4] + 4 * m1_set::bc<T>[4] * (v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -8 * m1_set::bc<T>[1] * v[5] + 9 * m1_set::bc<T>[2] * v[5]))) / prod_pow(tend, 2);
c[1] = (-(4 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2] + 2 * m1_set::bc<T>[4]) * (v[0] + -1 * v[1] + v[2] + v[3] + -1 * v[5])) / tend;


		return (2 * abb[49] + abb[50] + -abb[51] + -2 * abb[53] + -abb[55]) * (t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[6];
z[0] = -abb[33] + abb[34];
z[1] = -2 * abb[49] + -abb[50] + abb[51] + 2 * abb[53] + abb[55];
z[1] = abb[11] * z[1];
z[0] = z[0] * z[1];
z[2] = 2 * abb[35];
z[2] = z[1] * z[2];
z[3] = abb[31] * z[1];
z[3] = -z[0] + z[2] + z[3];
z[4] = abb[36] * z[1];
z[3] = 2 * z[3] + -5 * z[4];
z[3] = abb[36] * z[3];
z[4] = abb[37] + -abb[40];
z[5] = 2 * z[1];
z[4] = z[4] * z[5];
z[5] = abb[35] * z[1];
z[0] = 2 * z[0] + z[5];
z[0] = abb[35] * z[0];
z[2] = -abb[31] * z[2];
z[1] = abb[39] * z[1];
return z[0] + -4 * z[1] + z[2] + z[3] + z[4];
}

}
template <typename T, typename TABB> T SpDLog_f_4_593_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(2)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (2 * abb[49] + abb[50] + -abb[51] + -2 * abb[53] + -abb[55]) * (t * c[0] + -2 * c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = -abb[49] + abb[53];
z[0] = -abb[50] + abb[51] + abb[55] + 2 * z[0];
z[1] = abb[35] + -abb[36];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_593_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-43.837623605525059103782776619003555742200251039657810455806317352"),stof<T>("-34.859701539291110108982035290351984755759745157931530015992864135")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,69> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W71(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_9_im(k), f_2_12_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), IntDLogL_W_17(t,k,kend,dl), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[33].real()/k.W[33].real()), rlog(kend.W[70].real()/k.W[70].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_2_re(k), f_2_9_re(k), f_2_12_re(k), T{0}};
abb[45] = SpDLog_f_4_593_W_17_Im(t, path, abb);
abb[68] = SpDLog_f_4_593_W_17_Re(t, path, abb);

                    
            return f_4_593_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_593_DLogXconstant_part(base_point<T>, kend);
	value += f_4_593_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_593_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_593_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_593_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_593_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_593_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_593_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
