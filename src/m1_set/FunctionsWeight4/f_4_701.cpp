/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_701.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_701_abbreviated (const std::array<T,66>& abb) {
T z[92];
z[0] = -abb[3] + abb[16];
z[1] = 2 * abb[0];
z[2] = abb[6] + z[1];
z[3] = 4 * abb[10];
z[4] = 2 * abb[7];
z[5] = z[0] + z[2] + -z[3] + -z[4];
z[5] = abb[34] * z[5];
z[6] = abb[0] + -abb[10];
z[7] = 2 * abb[31];
z[8] = z[6] * z[7];
z[9] = abb[3] + -abb[5];
z[10] = abb[6] + z[9];
z[11] = abb[4] + -z[10];
z[11] = abb[32] * z[11];
z[12] = abb[28] * z[6];
z[11] = z[8] + z[11] + -2 * z[12];
z[12] = z[0] + -z[1];
z[13] = abb[6] + z[12];
z[14] = -z[4] + z[13];
z[15] = abb[33] * z[14];
z[10] = abb[7] + z[10];
z[16] = 2 * abb[30];
z[17] = z[10] * z[16];
z[5] = z[5] + 2 * z[11] + z[15] + z[17];
z[5] = m1_set::bc<T>[0] * z[5];
z[11] = 2 * abb[10];
z[15] = abb[4] + abb[6];
z[17] = z[11] + -z[15];
z[18] = abb[7] + -z[0] + z[17];
z[19] = abb[49] * z[18];
z[20] = abb[46] * z[6];
z[10] = abb[48] * z[10];
z[10] = z[10] + z[19] + -2 * z[20];
z[19] = -abb[33] + -abb[34] + z[16];
z[19] = m1_set::bc<T>[0] * z[19];
z[19] = 2 * abb[48] + z[19];
z[19] = abb[14] * z[19];
z[21] = abb[17] + -abb[18] + abb[19];
z[22] = abb[20] + z[21];
z[23] = -abb[50] * z[22];
z[5] = z[5] + 2 * z[10] + -z[19] + z[23];
z[5] = abb[56] * z[5];
z[10] = -abb[5] + abb[13];
z[23] = z[10] + z[12];
z[24] = abb[31] * z[23];
z[25] = -abb[16] + z[15];
z[26] = -abb[13] + z[1];
z[27] = z[25] + z[26];
z[28] = -abb[28] * z[27];
z[29] = -abb[6] + abb[12];
z[29] = abb[32] * z[29];
z[28] = -z[24] + z[28] + 2 * z[29];
z[30] = abb[4] + -abb[13];
z[31] = -abb[3] + z[4];
z[32] = abb[6] + abb[12];
z[33] = z[30] + -z[31] + 2 * z[32];
z[33] = z[16] * z[33];
z[31] = -4 * abb[12] + -abb[16] + z[2] + z[31];
z[31] = abb[34] * z[31];
z[13] = 2 * abb[5] + -z[13];
z[34] = -abb[33] * z[13];
z[28] = 2 * z[28] + z[31] + z[33] + z[34];
z[28] = m1_set::bc<T>[0] * z[28];
z[31] = abb[5] + abb[7];
z[33] = 2 * abb[12];
z[30] = abb[6] + z[30] + -z[31] + z[33];
z[30] = abb[47] * z[30];
z[27] = -abb[46] * z[27];
z[34] = -abb[5] + abb[7];
z[35] = abb[3] + abb[6] + -z[34];
z[35] = abb[48] * z[35];
z[27] = z[27] + z[30] + z[35];
z[21] = -abb[20] + z[21];
z[30] = -abb[50] * z[21];
z[19] = -z[19] + 2 * z[27] + z[28] + z[30];
z[19] = abb[59] * z[19];
z[27] = abb[22] + abb[24] + abb[26];
z[28] = -abb[51] + abb[54];
z[30] = -abb[53] + z[28];
z[35] = z[27] * z[30];
z[36] = -abb[6] + abb[10];
z[37] = 2 * abb[60];
z[38] = z[36] * z[37];
z[39] = abb[56] * z[18];
z[35] = z[35] + z[38] + z[39];
z[35] = abb[35] * z[35];
z[38] = abb[57] + abb[58];
z[39] = abb[1] * z[38];
z[40] = abb[12] * z[38];
z[41] = z[39] + -z[40];
z[33] = -abb[13] + z[33];
z[42] = -abb[7] + z[33];
z[43] = abb[3] + z[42];
z[44] = -abb[59] * z[43];
z[45] = abb[52] + -abb[55];
z[46] = abb[53] + z[45];
z[47] = -abb[24] * z[46];
z[44] = 2 * z[41] + z[44] + z[47];
z[44] = abb[29] * z[44];
z[35] = z[35] + z[44];
z[35] = 2 * z[35];
z[35] = m1_set::bc<T>[0] * z[35];
z[44] = -abb[34] * z[41];
z[29] = z[29] * z[38];
z[44] = -z[29] + z[44];
z[32] = z[32] * z[38];
z[47] = 2 * z[39];
z[32] = z[32] + -z[47];
z[32] = abb[30] * z[32];
z[32] = z[32] + -z[44];
z[32] = m1_set::bc<T>[0] * z[32];
z[48] = -abb[47] * z[41];
z[49] = abb[6] * z[38];
z[50] = -z[39] + z[49];
z[50] = abb[48] * z[50];
z[32] = z[32] + z[48] + z[50];
z[48] = z[28] + z[45];
z[50] = abb[33] * z[48];
z[51] = abb[34] * z[48];
z[52] = z[16] * z[48];
z[52] = -z[50] + -z[51] + z[52];
z[52] = m1_set::bc<T>[0] * z[52];
z[53] = abb[48] * z[48];
z[52] = z[52] + 2 * z[53];
z[52] = abb[21] * z[52];
z[54] = abb[33] * z[30];
z[55] = z[51] + z[54];
z[56] = abb[28] * z[46];
z[57] = abb[31] * z[46];
z[56] = z[56] + -z[57];
z[58] = abb[30] * z[48];
z[58] = -z[55] + z[56] + z[58];
z[58] = m1_set::bc<T>[0] * z[58];
z[59] = abb[49] * z[30];
z[53] = z[53] + z[59];
z[60] = abb[46] * z[46];
z[58] = z[53] + z[58] + z[60];
z[61] = 2 * abb[22];
z[58] = z[58] * z[61];
z[62] = -2 * abb[53] + z[28] + -z[45];
z[63] = abb[33] * z[62];
z[63] = z[51] + z[63];
z[64] = abb[32] * z[30];
z[65] = -z[57] + z[64];
z[65] = 2 * z[65];
z[66] = 2 * z[46];
z[67] = abb[30] * z[66];
z[67] = -z[63] + z[65] + z[67];
z[67] = m1_set::bc<T>[0] * z[67];
z[68] = abb[47] * z[66];
z[67] = z[67] + z[68];
z[68] = abb[23] + abb[25];
z[67] = z[67] * z[68];
z[69] = abb[28] + -abb[31];
z[70] = -z[6] * z[69];
z[71] = -abb[0] + -abb[6] + z[11];
z[72] = -abb[34] * z[71];
z[73] = abb[0] + -abb[6];
z[74] = -abb[33] * z[73];
z[70] = 2 * z[70] + z[72] + z[74];
z[70] = m1_set::bc<T>[0] * z[70];
z[72] = abb[49] * z[36];
z[20] = -z[20] + z[72];
z[72] = -abb[17] * abb[50];
z[20] = 2 * z[20] + z[70] + z[72];
z[20] = z[20] * z[37];
z[70] = abb[53] + z[28] + 2 * z[45];
z[70] = abb[30] * z[70];
z[55] = -z[55] + z[70];
z[55] = m1_set::bc<T>[0] * z[55];
z[70] = abb[47] * z[46];
z[53] = z[53] + z[55] + z[70];
z[53] = abb[24] * z[53];
z[55] = 2 * z[56];
z[63] = z[55] + -z[63];
z[63] = m1_set::bc<T>[0] * z[63];
z[59] = z[59] + z[60];
z[59] = 2 * z[59] + z[63];
z[59] = abb[26] * z[59];
z[60] = abb[27] * z[48];
z[63] = abb[50] * z[60];
z[5] = z[5] + z[19] + z[20] + 4 * z[32] + z[35] + z[52] + 2 * z[53] + z[58] + z[59] + z[63] + z[67];
z[5] = 2 * z[5];
z[19] = abb[63] * z[34];
z[2] = abb[61] * z[2];
z[20] = abb[6] * abb[63];
z[32] = -abb[6] * abb[44];
z[34] = 2 * abb[9];
z[35] = -abb[6] + z[34];
z[52] = abb[5] + z[35];
z[52] = abb[40] * z[52];
z[53] = abb[37] + abb[61];
z[58] = abb[44] + -z[53];
z[58] = abb[13] * z[58];
z[59] = abb[37] + abb[40] + -abb[63];
z[59] = abb[3] * z[59];
z[63] = -abb[40] + -abb[61];
z[63] = abb[16] * z[63];
z[67] = -abb[37] + abb[61];
z[67] = abb[4] * z[67];
z[2] = z[2] + z[19] + -z[20] + z[32] + z[52] + z[58] + z[59] + z[63] + z[67];
z[19] = -abb[28] + abb[33];
z[32] = z[16] * z[19];
z[52] = abb[28] + abb[34];
z[52] = abb[33] * z[52];
z[58] = abb[37] + abb[63];
z[59] = abb[28] * abb[34];
z[63] = prod_pow(abb[28], 2);
z[67] = prod_pow(m1_set::bc<T>[0], 2);
z[70] = (T(2) / T(3)) * z[67];
z[32] = -z[32] + z[52] + -2 * z[58] + -z[59] + -z[63] + -z[70];
z[32] = abb[14] * z[32];
z[52] = abb[39] + -abb[44];
z[58] = abb[37] + z[52];
z[59] = 4 * abb[8];
z[72] = z[58] * z[59];
z[32] = z[32] + -z[72];
z[72] = -abb[6] + abb[13];
z[74] = abb[32] * z[72];
z[75] = abb[2] * abb[32];
z[76] = 2 * abb[8];
z[77] = -abb[32] * z[76];
z[24] = -z[24] + z[74] + z[75] + z[77];
z[77] = 4 * abb[0];
z[25] = -abb[2] + z[10] + -z[25] + z[34] + -z[77];
z[25] = abb[28] * z[25];
z[24] = 2 * z[24] + z[25];
z[24] = abb[28] * z[24];
z[25] = abb[2] + z[26] + -z[35];
z[26] = abb[31] * z[25];
z[26] = z[26] + -z[74] + z[75];
z[34] = abb[6] + z[59];
z[74] = 6 * abb[0] + -z[0];
z[78] = 4 * abb[9] + -z[74];
z[79] = -2 * abb[2] + z[34] + z[78];
z[79] = abb[28] * z[79];
z[13] = abb[34] * z[13];
z[80] = -abb[33] * z[25];
z[13] = z[13] + 2 * z[26] + z[79] + z[80];
z[13] = abb[33] * z[13];
z[26] = -abb[4] + z[76];
z[79] = abb[3] + z[26];
z[80] = 2 * abb[13];
z[81] = -abb[5] + -z[1] + z[35] + -z[79] + z[80];
z[82] = prod_pow(abb[31], 2);
z[81] = z[81] * z[82];
z[59] = -z[9] + z[59];
z[4] = -z[4] + z[15] + -z[59] + z[80];
z[4] = abb[30] * z[4];
z[83] = z[10] + -z[76];
z[84] = -abb[6] + z[83];
z[84] = z[19] * z[84];
z[43] = abb[34] * z[43];
z[85] = abb[8] + abb[12] + -abb[13];
z[85] = abb[32] * z[85];
z[84] = -z[43] + z[84] + 2 * z[85];
z[4] = z[4] + 2 * z[84];
z[4] = abb[30] * z[4];
z[23] = z[7] * z[23];
z[78] = abb[6] + -z[78] + -z[80];
z[78] = abb[28] * z[78];
z[23] = z[23] + z[78];
z[23] = abb[34] * z[23];
z[10] = -abb[2] + -z[10] + z[79];
z[78] = 2 * abb[42];
z[80] = -2 * abb[41] + -z[78];
z[80] = z[10] * z[80];
z[84] = prod_pow(abb[32], 2);
z[72] = z[72] * z[84];
z[85] = 2 * abb[37];
z[86] = -2 * abb[39] + -abb[40] + abb[44] + -z[85];
z[86] = -z[84] + 2 * z[86];
z[86] = abb[2] * z[86];
z[87] = 2 * abb[43];
z[25] = -z[25] * z[87];
z[42] = abb[5] + -z[15] + -z[42];
z[88] = 2 * abb[62];
z[42] = z[42] * z[88];
z[89] = abb[6] + -5 * abb[12];
z[89] = 5 * abb[7] + abb[13] + z[77] + 2 * z[89];
z[89] = -abb[3] + abb[16] * (T(-2) / T(3)) + (T(1) / T(3)) * z[89];
z[89] = z[67] * z[89];
z[90] = abb[0] + -abb[9];
z[91] = abb[36] * z[90];
z[21] = abb[65] * z[21];
z[61] = abb[45] * z[61];
z[2] = 2 * z[2] + z[4] + z[13] + z[21] + z[23] + z[24] + z[25] + -z[32] + z[42] + z[61] + z[72] + z[80] + z[81] + z[86] + z[89] + -4 * z[91];
z[2] = abb[59] * z[2];
z[4] = abb[2] + -abb[16];
z[13] = -abb[6] + abb[15];
z[21] = -z[1] + -z[4] + z[13];
z[21] = abb[36] * z[21];
z[23] = z[1] + -z[11];
z[23] = abb[61] * z[23];
z[24] = -z[52] + -z[85];
z[24] = abb[4] * z[24];
z[18] = abb[64] * z[18];
z[25] = -abb[4] + abb[8];
z[42] = -z[6] + -z[25];
z[42] = z[42] * z[82];
z[61] = abb[5] * abb[63];
z[72] = -abb[15] * z[58];
z[52] = -abb[63] + z[52];
z[52] = abb[7] * z[52];
z[80] = -abb[63] + z[58];
z[80] = abb[3] * z[80];
z[18] = -z[18] + -z[20] + z[21] + z[23] + z[24] + z[42] + z[52] + z[61] + z[72] + z[80];
z[20] = abb[7] + -abb[15];
z[21] = -z[20] + -z[79];
z[21] = abb[32] * z[21];
z[21] = z[8] + z[21] + z[75];
z[17] = abb[15] + z[17];
z[4] = -abb[7] + -z[4] + z[17] + -z[77];
z[4] = abb[28] * z[4];
z[4] = z[4] + 2 * z[21];
z[4] = abb[28] * z[4];
z[17] = -z[1] + z[17];
z[21] = abb[33] * z[17];
z[3] = z[3] + -z[74];
z[23] = -2 * abb[4] + z[3] + z[34];
z[23] = abb[28] * z[23];
z[14] = -abb[34] * z[14];
z[24] = abb[31] * z[6];
z[14] = z[14] + z[21] + z[23] + 4 * z[24];
z[14] = abb[33] * z[14];
z[23] = -abb[7] + z[13] + -z[76];
z[19] = z[19] * z[23];
z[23] = abb[8] + -abb[15];
z[24] = abb[32] * z[23];
z[34] = abb[11] * abb[32];
z[24] = z[24] + z[34];
z[9] = -abb[15] + z[9];
z[42] = 2 * abb[11];
z[52] = -z[9] + -z[42];
z[52] = abb[34] * z[52];
z[19] = z[19] + 2 * z[24] + z[52];
z[24] = 3 * abb[15] + -z[42] + -z[59];
z[24] = abb[30] * z[24];
z[19] = 2 * z[19] + z[24];
z[19] = abb[30] * z[19];
z[24] = 2 * z[34];
z[9] = abb[32] * z[9];
z[9] = -z[8] + z[9] + z[24];
z[3] = abb[6] + -2 * abb[15] + -z[3];
z[3] = abb[28] * z[3];
z[3] = z[3] + 2 * z[9];
z[3] = abb[34] * z[3];
z[9] = abb[4] + -abb[6] + -abb[15];
z[0] = -4 * abb[7] + -8 * abb[10] + 2 * z[0] + -z[9] + z[77];
z[34] = (T(1) / T(3)) * z[67];
z[0] = z[0] * z[34];
z[31] = z[13] + z[31];
z[52] = z[31] * z[84];
z[58] = -2 * z[58] + -z[84];
z[58] = abb[2] * z[58];
z[59] = z[17] * z[87];
z[31] = -abb[2] + z[31];
z[61] = 2 * abb[38];
z[31] = z[31] * z[61];
z[61] = z[61] + z[84];
z[42] = -z[42] * z[61];
z[13] = z[13] + -z[26];
z[26] = z[13] * z[78];
z[72] = abb[41] * z[25];
z[22] = abb[65] * z[22];
z[0] = z[0] + z[3] + z[4] + z[14] + 2 * z[18] + z[19] + z[22] + z[26] + z[31] + -z[32] + z[42] + z[52] + z[58] + z[59] + -4 * z[72];
z[0] = abb[56] * z[0];
z[3] = abb[63] * z[48];
z[4] = abb[44] * z[30];
z[14] = abb[39] * z[30];
z[4] = z[3] + z[4] + -z[14];
z[18] = -z[46] * z[53];
z[19] = abb[34] * z[30];
z[22] = z[19] + z[56];
z[22] = abb[33] * z[22];
z[18] = -z[4] + z[18] + z[22];
z[22] = abb[53] + 4 * z[28] + 5 * z[45];
z[22] = z[22] * z[34];
z[26] = z[30] * z[84];
z[22] = z[22] + -z[26];
z[28] = abb[28] * z[48];
z[31] = -z[28] + z[50];
z[31] = z[16] * z[31];
z[32] = abb[41] + z[82];
z[32] = z[32] * z[66];
z[34] = -abb[38] + abb[64];
z[42] = 2 * z[30];
z[45] = -z[34] * z[42];
z[50] = -z[57] + -z[64];
z[52] = abb[28] * z[30];
z[50] = 2 * z[50] + -z[52];
z[50] = abb[28] * z[50];
z[53] = -abb[34] * z[55];
z[18] = 2 * z[18] + -z[22] + -z[31] + z[32] + z[45] + z[50] + z[53];
z[18] = abb[22] * z[18];
z[32] = -abb[41] + -abb[42] + abb[62];
z[32] = z[32] * z[66];
z[45] = abb[31] * z[66];
z[50] = z[45] + z[52];
z[50] = abb[28] * z[50];
z[26] = -z[26] + z[32] + z[50];
z[32] = -z[28] + -z[65];
z[32] = abb[34] * z[32];
z[50] = abb[34] * z[62];
z[53] = z[28] + z[50];
z[53] = abb[33] * z[53];
z[55] = -z[52] + z[54];
z[58] = z[19] + -z[55];
z[59] = -abb[30] * z[62];
z[58] = 2 * z[58] + z[59];
z[58] = abb[30] * z[58];
z[59] = z[48] * z[70];
z[64] = abb[37] * z[48];
z[65] = abb[40] * z[46];
z[72] = -z[64] + -z[65];
z[74] = z[46] * z[82];
z[76] = abb[38] * z[42];
z[32] = -z[26] + z[32] + z[53] + z[58] + -z[59] + 2 * z[72] + z[74] + z[76];
z[32] = abb[23] * z[32];
z[53] = abb[28] * z[62];
z[45] = z[45] + z[53];
z[58] = abb[34] * z[45];
z[58] = z[58] + -z[59];
z[62] = -abb[37] * z[46];
z[14] = z[14] + z[62] + -z[65];
z[50] = z[50] + -z[53];
z[53] = abb[32] * z[42];
z[62] = z[50] + -z[53];
z[62] = abb[33] * z[62];
z[72] = prod_pow(abb[30], 2);
z[72] = z[72] + z[82];
z[72] = z[46] * z[72];
z[19] = z[19] + -z[52];
z[54] = -z[19] + z[54];
z[74] = -abb[35] * z[30];
z[54] = 2 * z[54] + z[74];
z[54] = abb[35] * z[54];
z[74] = -abb[36] * z[42];
z[76] = 2 * abb[45];
z[76] = abb[56] * z[76];
z[14] = 2 * z[14] + -z[26] + z[54] + z[58] + z[62] + z[72] + z[74] + z[76];
z[14] = abb[25] * z[14];
z[26] = -abb[6] + abb[9];
z[54] = -z[6] + z[26];
z[62] = abb[43] * z[54];
z[6] = abb[61] * z[6];
z[26] = abb[40] * z[26];
z[36] = -abb[64] * z[36];
z[6] = -z[6] + -z[26] + -z[36] + -z[62] + z[91];
z[11] = -3 * abb[0] + z[11] + z[35];
z[11] = abb[28] * z[11];
z[26] = abb[33] * z[54];
z[35] = -z[7] * z[54];
z[36] = abb[34] * z[73];
z[35] = z[11] + z[26] + z[35] + z[36];
z[35] = abb[33] * z[35];
z[11] = -z[8] + -z[11];
z[11] = abb[34] * z[11];
z[36] = z[54] * z[82];
z[1] = abb[9] + abb[10] + -z[1];
z[1] = abb[28] * z[1];
z[1] = z[1] + z[8];
z[1] = abb[28] * z[1];
z[8] = -z[70] * z[71];
z[1] = z[1] + -2 * z[6] + z[8] + z[11] + z[35] + z[36];
z[1] = abb[60] * z[1];
z[6] = abb[11] * z[38];
z[8] = -z[6] + -z[40] + z[47];
z[8] = abb[34] * z[8];
z[11] = -z[6] + z[39];
z[11] = abb[32] * z[11];
z[8] = z[8] + -z[11] + z[29];
z[29] = -z[6] + -z[39] + 2 * z[49];
z[29] = abb[30] * z[29];
z[8] = 2 * z[8] + z[29];
z[8] = abb[30] * z[8];
z[29] = (T(-5) / T(3)) * z[67] + -z[88];
z[29] = -z[29] * z[41];
z[35] = z[38] * z[61];
z[36] = -abb[11] * z[35];
z[38] = abb[63] * z[38];
z[35] = z[35] + 2 * z[38];
z[35] = abb[1] * z[35];
z[38] = 2 * abb[34];
z[38] = -z[11] * z[38];
z[39] = -abb[63] * z[49];
z[1] = z[1] + z[8] + z[29] + z[35] + z[36] + z[38] + 2 * z[39];
z[3] = -z[3] + -z[64];
z[8] = z[28] + z[51];
z[8] = abb[33] * z[8];
z[28] = -z[48] * z[63];
z[29] = -abb[28] * z[51];
z[3] = 2 * z[3] + z[8] + z[28] + z[29] + -z[31] + -z[59];
z[3] = abb[21] * z[3];
z[7] = z[7] * z[25];
z[8] = abb[28] * z[13];
z[23] = abb[11] + z[23];
z[23] = z[16] * z[23];
z[9] = -abb[32] * z[9];
z[25] = -abb[15] + z[15];
z[25] = abb[33] * z[25];
z[7] = z[7] + z[8] + z[9] + z[23] + -z[24] + z[25];
z[7] = abb[56] * z[7];
z[8] = -z[10] * z[69];
z[9] = abb[7] + -z[15] + -z[83];
z[9] = abb[30] * z[9];
z[15] = abb[6] + -z[33];
z[15] = abb[32] * z[15];
z[8] = z[8] + z[9] + z[15] + z[43] + -z[75];
z[8] = abb[59] * z[8];
z[6] = z[6] + -z[49];
z[6] = abb[30] * z[6];
z[6] = z[6] + z[11] + z[44];
z[9] = abb[30] * z[46];
z[11] = z[9] + -z[56];
z[11] = -z[11] * z[68];
z[15] = abb[33] * z[46];
z[15] = z[15] + -z[57];
z[15] = abb[22] * z[15];
z[23] = abb[34] * z[46];
z[9] = -z[9] + z[23];
z[23] = abb[24] * z[9];
z[6] = 2 * z[6] + z[7] + z[8] + z[11] + z[15] + z[23];
z[7] = z[46] * z[68];
z[8] = abb[56] * z[13];
z[10] = -abb[59] * z[10];
z[7] = z[7] + z[8] + z[10];
z[7] = abb[29] * z[7];
z[6] = 2 * z[6] + z[7];
z[6] = abb[29] * z[6];
z[7] = -z[9] + -z[55];
z[7] = z[7] * z[16];
z[8] = abb[33] * abb[34];
z[8] = z[8] + -z[34];
z[8] = z[8] * z[42];
z[9] = -z[52] + -z[53];
z[9] = abb[28] * z[9];
z[10] = -abb[62] * z[66];
z[4] = -2 * z[4] + z[7] + z[8] + z[9] + z[10] + -z[22];
z[4] = abb[24] * z[4];
z[7] = -abb[36] + -abb[64];
z[7] = z[7] * z[30];
z[8] = -abb[61] * z[46];
z[7] = z[7] + z[8] + -z[65];
z[8] = abb[33] * z[50];
z[9] = -abb[28] * z[45];
z[7] = 2 * z[7] + z[8] + z[9] + z[58];
z[7] = abb[26] * z[7];
z[8] = -abb[28] + abb[34];
z[9] = -z[8] * z[90];
z[9] = z[9] + -z[26];
z[9] = z[9] * z[37];
z[10] = -z[12] + z[20];
z[10] = -z[8] * z[10];
z[10] = z[10] + -z[21];
z[10] = abb[56] * z[10];
z[11] = -z[19] * z[27];
z[12] = 2 * abb[59];
z[12] = z[12] * z[90];
z[8] = abb[33] + -z[8];
z[8] = z[8] * z[12];
z[8] = z[8] + z[9] + z[10] + z[11];
z[9] = z[37] * z[54];
z[10] = abb[56] * z[17];
z[9] = z[9] + z[10] + -z[12];
z[9] = abb[35] * z[9];
z[8] = 2 * z[8] + z[9];
z[8] = abb[35] * z[8];
z[9] = abb[17] * z[37];
z[9] = z[9] + -z[60];
z[9] = abb[65] * z[9];
z[0] = z[0] + 2 * z[1] + z[2] + z[3] + z[4] + z[6] + z[7] + z[8] + z[9] + z[14] + z[18] + z[32];
z[0] = 2 * z[0];
return {z[5], z[0]};
}


template <typename T> std::complex<T> f_4_701_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("-1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("10.4023028191312333362719401831913221365702768824772793193192573969"),stof<T>("4.0099607112369939801253103352120106710102959542432765442127783144")}, std::complex<T>{stof<T>("5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("-5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("-15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("-43.629921461479222958882104424550926411301923592425513393845316336"),stof<T>("-35.051320719475604067750287361067351782071350735219280609897925655")}, std::complex<T>{stof<T>("-25.514205731730803297405729886997480010359956010634664886931192621"),stof<T>("-36.916552190357586975765762273827768645282794678010635435462489747")}, std::complex<T>{stof<T>("-25.514205731730803297405729886997480010359956010634664886931192621"),stof<T>("-36.916552190357586975765762273827768645282794678010635435462489747")}, std::complex<T>{stof<T>("-38.788330174393503524783156811706184825492083256084363457680538721"),stof<T>("-35.155922194044081085036539936540929621236005412970168900628053236")}, std::complex<T>{stof<T>("-27.266064834752539870826065121313655656450724718606683845191270348"),stof<T>("-13.595328248826547361489176099425089692637134646712362756476500542")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_701_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_701_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-15.027648461811750172159449090228423730552511801388229864895411365"),stof<T>("18.707462405770759069511983873352611993254053234583799179192624201")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,66> abb = {dl[0], dl[1], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W15(k,dl), dlog_W21(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_19(k), f_2_21(k), f_2_28_im(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_701_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_701_DLogXconstant_part(base_point<T>, kend);
	value += f_4_701_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_701_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_701_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_701_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_701_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_701_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_701_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
