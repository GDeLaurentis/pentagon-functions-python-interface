/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_709.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_709_abbreviated (const std::array<T,65>& abb) {
T z[81];
z[0] = 2 * abb[31];
z[1] = -abb[29] + z[0];
z[2] = abb[33] + z[1];
z[3] = 2 * abb[28];
z[4] = z[2] + -z[3];
z[5] = 2 * abb[0];
z[6] = z[4] * z[5];
z[7] = -abb[4] + abb[15];
z[8] = abb[13] + z[7];
z[9] = -abb[6] + z[8];
z[10] = -z[3] * z[9];
z[11] = 2 * abb[34];
z[12] = -abb[5] + abb[15];
z[13] = -z[11] * z[12];
z[14] = 2 * abb[7];
z[15] = abb[33] + abb[34];
z[16] = -z[14] * z[15];
z[17] = -abb[6] + abb[16] + z[12] + z[14];
z[18] = 2 * abb[35];
z[17] = z[17] * z[18];
z[19] = -abb[5] + abb[13];
z[20] = -abb[3] + abb[15] + z[19];
z[21] = z[0] * z[20];
z[22] = -abb[4] + abb[5];
z[23] = -abb[16] + z[22];
z[24] = abb[13] + z[23];
z[24] = abb[33] * z[24];
z[25] = abb[4] + z[19];
z[26] = abb[16] + z[25];
z[27] = 2 * abb[3] + -z[26];
z[28] = abb[29] * z[27];
z[10] = -z[6] + z[10] + z[13] + z[16] + z[17] + z[21] + z[24] + z[28];
z[10] = m1_set::bc<T>[0] * z[10];
z[13] = z[5] + -z[9];
z[13] = abb[46] * z[13];
z[16] = abb[3] + -abb[6];
z[17] = z[7] + z[16];
z[21] = abb[7] + z[17];
z[21] = abb[49] * z[21];
z[24] = abb[3] + z[23];
z[28] = abb[7] + -z[24];
z[28] = abb[48] * z[28];
z[13] = z[13] + z[21] + z[28];
z[21] = z[15] + -z[18];
z[21] = m1_set::bc<T>[0] * z[21];
z[21] = abb[48] + abb[49] + -z[21];
z[21] = abb[1] * z[21];
z[28] = 4 * z[21];
z[29] = -abb[17] + abb[18];
z[30] = -abb[19] + z[29];
z[31] = abb[50] * z[30];
z[10] = z[10] + 2 * z[13] + -z[28] + z[31];
z[10] = abb[58] * z[10];
z[13] = 2 * abb[33];
z[31] = abb[7] * z[13];
z[32] = abb[33] * z[26];
z[31] = z[31] + -z[32];
z[23] = -abb[3] + z[23];
z[32] = abb[7] + z[23];
z[33] = -z[18] * z[32];
z[34] = 2 * abb[32];
z[22] = -abb[3] + -abb[6] + z[22];
z[22] = z[22] * z[34];
z[35] = abb[6] + -abb[7] + z[25];
z[36] = 2 * abb[30];
z[37] = z[35] * z[36];
z[26] = z[14] + -z[26];
z[26] = abb[29] * z[26];
z[6] = -z[6] + z[22] + z[26] + z[31] + z[33] + z[37];
z[6] = m1_set::bc<T>[0] * z[6];
z[22] = abb[47] * z[35];
z[26] = -abb[48] * z[32];
z[32] = abb[46] * z[5];
z[22] = z[22] + z[26] + z[32];
z[26] = -abb[34] + abb[35];
z[32] = abb[32] + -abb[33] + z[26];
z[32] = m1_set::bc<T>[0] * z[32];
z[32] = abb[49] + z[32];
z[32] = abb[12] * z[32];
z[29] = abb[19] + z[29];
z[33] = abb[50] * z[29];
z[6] = z[6] + 2 * z[22] + -z[28] + 4 * z[32] + z[33];
z[6] = abb[59] * z[6];
z[22] = -abb[29] + abb[30];
z[28] = -abb[51] + abb[55];
z[33] = -abb[52] + z[28];
z[22] = z[22] * z[33];
z[37] = -abb[53] + abb[54];
z[38] = z[28] + -z[37];
z[39] = abb[33] * z[38];
z[40] = -abb[52] + -z[28] + 2 * z[37];
z[41] = -abb[35] * z[40];
z[42] = -abb[52] + z[37];
z[43] = abb[34] * z[42];
z[22] = z[22] + -z[39] + z[41] + z[43];
z[22] = m1_set::bc<T>[0] * z[22];
z[41] = abb[48] * z[38];
z[44] = abb[47] * z[33];
z[44] = z[41] + z[44];
z[45] = abb[49] * z[42];
z[22] = z[22] + z[44] + -z[45];
z[22] = abb[23] * z[22];
z[46] = -abb[25] * z[41];
z[47] = abb[24] * z[44];
z[22] = z[22] + z[46] + z[47];
z[46] = abb[29] * z[38];
z[46] = z[39] + z[46];
z[47] = z[0] * z[42];
z[47] = 2 * z[43] + -z[46] + -z[47];
z[48] = z[34] + -z[36];
z[49] = -z[33] * z[48];
z[50] = z[18] * z[38];
z[51] = z[47] + z[49] + z[50];
z[52] = abb[24] * z[51];
z[46] = z[46] + -z[50];
z[46] = abb[25] * z[46];
z[46] = z[46] + z[52];
z[46] = m1_set::bc<T>[0] * z[46];
z[52] = z[3] * z[42];
z[40] = -z[18] * z[40];
z[40] = z[40] + z[47] + z[52];
z[40] = m1_set::bc<T>[0] * z[40];
z[47] = abb[46] * z[42];
z[45] = -z[45] + z[47];
z[41] = z[41] + z[45];
z[40] = z[40] + 2 * z[41];
z[40] = abb[21] * z[40];
z[21] = -z[21] + z[32];
z[32] = abb[47] + abb[48];
z[32] = abb[4] * z[32];
z[41] = abb[0] * abb[46];
z[32] = z[21] + z[32] + z[41];
z[41] = -z[18] + z[34];
z[47] = -abb[29] + z[36];
z[52] = -abb[33] + z[47];
z[53] = -z[41] + z[52];
z[53] = abb[4] * z[53];
z[4] = -abb[0] * z[4];
z[4] = z[4] + z[53];
z[4] = m1_set::bc<T>[0] * z[4];
z[53] = abb[18] * abb[50];
z[4] = z[4] + 2 * z[32] + z[53];
z[32] = 2 * abb[56];
z[4] = z[4] * z[32];
z[51] = m1_set::bc<T>[0] * z[51];
z[44] = 2 * z[44] + z[51];
z[44] = abb[22] * z[44];
z[51] = abb[4] * abb[35];
z[53] = -abb[4] * abb[32];
z[53] = z[51] + z[53];
z[53] = m1_set::bc<T>[0] * z[53];
z[54] = abb[4] * abb[48];
z[21] = z[21] + z[53] + z[54];
z[21] = abb[57] * z[21];
z[53] = abb[56] + abb[59];
z[54] = abb[28] + -abb[33];
z[55] = abb[30] + -abb[31] + z[54];
z[55] = m1_set::bc<T>[0] * z[55];
z[55] = abb[46] + abb[47] + z[55];
z[55] = -abb[10] * z[53] * z[55];
z[21] = z[21] + z[55];
z[55] = abb[35] * z[42];
z[56] = -z[43] + z[55];
z[57] = abb[28] * z[42];
z[58] = z[56] + -z[57];
z[59] = abb[31] * z[42];
z[60] = z[58] + z[59];
z[61] = -m1_set::bc<T>[0] * z[60];
z[45] = z[45] + z[61];
z[61] = 2 * abb[26];
z[45] = z[45] * z[61];
z[62] = abb[27] * z[38];
z[63] = abb[58] + abb[59];
z[63] = abb[20] * z[63];
z[62] = -z[62] + z[63];
z[63] = abb[50] * z[62];
z[4] = z[4] + z[6] + z[10] + 4 * z[21] + 2 * z[22] + z[40] + z[44] + z[45] + z[46] + z[63];
z[4] = 2 * z[4];
z[6] = prod_pow(abb[31], 2);
z[10] = 2 * abb[41];
z[6] = z[6] + z[10];
z[21] = -abb[28] + abb[30] + abb[31];
z[21] = -abb[29] + 2 * z[21];
z[21] = abb[29] * z[21];
z[22] = -abb[28] + z[13];
z[22] = abb[28] * z[22];
z[21] = z[21] + z[22];
z[22] = 2 * z[54];
z[40] = -abb[30] + z[22];
z[40] = abb[30] * z[40];
z[44] = 2 * abb[42];
z[45] = -z[6] + z[21] + z[40] + -z[44];
z[45] = abb[8] * z[45];
z[46] = -abb[33] + abb[34];
z[46] = -abb[35] + 2 * z[46];
z[46] = abb[35] * z[46];
z[63] = abb[33] + z[26];
z[63] = -abb[32] + 2 * z[63];
z[63] = abb[32] * z[63];
z[46] = -2 * abb[39] + z[46] + z[63];
z[46] = abb[11] * z[46];
z[17] = -abb[63] * z[17];
z[64] = -abb[2] + z[7];
z[65] = -abb[42] * z[64];
z[66] = 2 * abb[38];
z[67] = abb[2] * z[66];
z[24] = abb[62] * z[24];
z[17] = z[17] + z[24] + z[45] + z[46] + z[65] + z[67];
z[24] = 2 * abb[29];
z[65] = z[24] + -z[34];
z[67] = -abb[28] + abb[35];
z[65] = z[65] * z[67];
z[67] = -abb[28] + z[26];
z[67] = z[18] * z[67];
z[68] = abb[34] + z[3];
z[68] = abb[34] * z[68];
z[69] = -abb[31] + z[11];
z[70] = abb[31] * z[69];
z[65] = -z[65] + z[66] + -z[67] + -z[68] + z[70];
z[66] = 2 * abb[9];
z[65] = z[65] * z[66];
z[22] = -abb[31] + z[22];
z[22] = abb[31] * z[22];
z[67] = 2 * abb[60];
z[22] = z[22] + -z[44] + z[67];
z[2] = 3 * abb[28] + -z[2] + -z[36];
z[2] = abb[29] * z[2];
z[44] = 3 * abb[33] + -z[3];
z[44] = abb[28] * z[44];
z[2] = -z[2] + z[22] + z[40] + z[44];
z[40] = z[2] * z[5];
z[44] = 4 * abb[33] + -abb[35];
z[44] = abb[35] * z[44];
z[70] = abb[33] * abb[34];
z[70] = -abb[63] + z[70];
z[71] = -abb[62] + z[70];
z[72] = -abb[39] + z[71];
z[73] = prod_pow(m1_set::bc<T>[0], 2);
z[74] = (T(5) / T(3)) * z[73];
z[44] = -z[44] + z[63] + 2 * z[72] + -z[74];
z[44] = abb[1] * z[44];
z[40] = z[40] + 2 * z[44] + z[65];
z[63] = abb[3] + -abb[16];
z[65] = -z[7] + z[63];
z[72] = -abb[28] * z[65];
z[75] = z[7] + -z[16];
z[75] = abb[34] * z[75];
z[76] = abb[33] * z[12];
z[15] = -abb[7] * z[15];
z[15] = z[15] + z[72] + z[75] + -z[76];
z[72] = 2 * abb[15];
z[16] = 3 * abb[4] + -abb[5] + z[14] + z[16] + -z[72];
z[16] = abb[35] * z[16];
z[15] = 2 * z[15] + z[16];
z[15] = abb[35] * z[15];
z[1] = z[1] + z[34];
z[1] = z[1] * z[64];
z[16] = -abb[16] + z[25];
z[34] = 2 * abb[2];
z[75] = -z[16] + z[34];
z[75] = abb[28] * z[75];
z[65] = z[18] * z[65];
z[27] = -abb[33] * z[27];
z[1] = z[1] + z[27] + z[65] + z[75];
z[1] = abb[29] * z[1];
z[27] = abb[2] + abb[6];
z[8] = -z[8] + z[27] + z[63];
z[8] = abb[28] * z[8];
z[63] = -abb[16] + z[19];
z[65] = -abb[4] + z[72];
z[72] = z[63] + z[65];
z[72] = abb[33] * z[72];
z[8] = z[8] + z[72];
z[8] = abb[28] * z[8];
z[12] = -abb[3] + z[12] + z[27];
z[72] = abb[34] * z[12];
z[75] = -abb[28] * z[12];
z[75] = z[75] + z[76];
z[75] = -z[72] + 2 * z[75];
z[75] = abb[34] * z[75];
z[20] = z[20] * z[54];
z[20] = z[20] + z[72];
z[72] = -abb[5] + abb[6];
z[65] = abb[3] + -z[65] + -z[72];
z[65] = abb[31] * z[65];
z[20] = 2 * z[20] + z[65];
z[20] = abb[31] * z[20];
z[65] = z[7] * z[18];
z[7] = abb[2] + z[7];
z[76] = -abb[28] * z[7];
z[77] = -abb[34] * z[64];
z[65] = z[65] + z[76] + z[77];
z[64] = -abb[32] * z[64];
z[64] = z[64] + 2 * z[65];
z[64] = abb[32] * z[64];
z[14] = z[14] * z[71];
z[9] = z[9] * z[67];
z[65] = -abb[4] + abb[13] + -abb[16];
z[65] = abb[5] + abb[7] * (T(-5) / T(3)) + abb[0] * (T(-4) / T(3)) + abb[15] * (T(-1) / T(3)) + (T(2) / T(3)) * z[65];
z[65] = z[65] * z[73];
z[25] = abb[2] + -abb[3] + z[25];
z[10] = z[10] * z[25];
z[1] = z[1] + z[8] + z[9] + z[10] + z[14] + z[15] + 2 * z[17] + z[20] + -z[40] + z[64] + z[65] + z[75];
z[1] = abb[58] * z[1];
z[8] = abb[33] * abb[35];
z[9] = abb[32] * z[26];
z[8] = z[8] + -z[9] + -z[70];
z[8] = 2 * z[8] + z[74];
z[8] = abb[12] * z[8];
z[9] = -abb[5] + abb[14];
z[10] = z[9] + z[27];
z[14] = abb[38] * z[10];
z[15] = -abb[4] + abb[14];
z[17] = -abb[6] + z[15];
z[20] = -abb[42] * z[17];
z[25] = -abb[2] + z[15];
z[26] = abb[3] + z[25];
z[64] = -abb[39] * z[26];
z[23] = abb[62] * z[23];
z[65] = abb[24] * abb[45];
z[14] = z[8] + -z[14] + -z[20] + -z[23] + -z[64] + z[65];
z[20] = z[11] + z[47];
z[20] = z[17] * z[20];
z[23] = 2 * abb[6];
z[16] = -z[16] + z[23];
z[16] = abb[28] * z[16];
z[47] = abb[16] + z[15];
z[64] = -abb[7] + z[47];
z[65] = -z[18] * z[64];
z[16] = z[16] + z[20] + -z[31] + z[65];
z[16] = abb[29] * z[16];
z[10] = -abb[7] + z[10];
z[20] = -abb[28] * z[10];
z[31] = z[15] * z[18];
z[65] = abb[3] + z[9];
z[65] = abb[33] * z[65];
z[67] = -abb[34] * z[17];
z[20] = z[20] + z[31] + z[65] + z[67];
z[26] = -abb[7] + -z[26];
z[26] = abb[32] * z[26];
z[20] = 2 * z[20] + z[26];
z[20] = abb[32] * z[20];
z[11] = z[11] * z[15];
z[26] = abb[28] * z[64];
z[11] = z[11] + z[26] + -z[65];
z[26] = abb[3] + 4 * abb[4] + -abb[5] + -3 * abb[14];
z[26] = abb[35] * z[26];
z[11] = 2 * z[11] + z[26];
z[11] = abb[35] * z[11];
z[26] = -abb[13] + z[27] + -z[47];
z[26] = abb[28] * z[26];
z[27] = -abb[4] + 2 * abb[14] + z[63];
z[27] = abb[33] * z[27];
z[26] = z[26] + z[27];
z[26] = abb[28] * z[26];
z[15] = abb[6] + z[15];
z[27] = -z[15] * z[68];
z[31] = prod_pow(abb[28], 2);
z[47] = -abb[38] + -abb[39] + abb[62];
z[47] = z[31] + 2 * z[47];
z[47] = abb[7] * z[47];
z[63] = abb[6] * z[0] * z[69];
z[64] = -abb[7] + abb[14] + z[19];
z[54] = z[54] * z[64];
z[17] = -abb[30] * z[17];
z[17] = z[17] + 2 * z[54];
z[17] = abb[30] * z[17];
z[54] = abb[0] + -abb[7];
z[19] = abb[16] + z[19];
z[19] = -abb[4] + abb[6] + -abb[14] + -2 * z[19] + -4 * z[54];
z[54] = (T(1) / T(3)) * z[73];
z[19] = z[19] * z[54];
z[11] = z[11] + -2 * z[14] + z[16] + z[17] + z[19] + z[20] + z[26] + z[27] + -z[40] + z[47] + z[63];
z[11] = abb[59] * z[11];
z[14] = abb[33] * z[42];
z[16] = -z[14] + z[57];
z[17] = 2 * z[16] + z[43];
z[17] = abb[34] * z[17];
z[19] = z[16] + z[43];
z[20] = 2 * z[19] + -z[59];
z[20] = abb[31] * z[20];
z[17] = z[17] + -z[20];
z[20] = abb[33] * z[33];
z[26] = abb[28] * z[33];
z[27] = z[20] + -z[26];
z[40] = z[27] * z[48];
z[47] = abb[35] * z[38];
z[47] = -2 * z[39] + z[47];
z[47] = abb[35] * z[47];
z[48] = abb[28] * z[38];
z[63] = z[39] + -z[48];
z[63] = abb[29] * z[63];
z[64] = abb[52] * (T(1) / T(3)) + (T(2) / T(3)) * z[28] + -z[37];
z[65] = z[64] * z[73];
z[67] = 2 * z[42];
z[68] = abb[41] * z[67];
z[65] = z[65] + z[68];
z[69] = abb[62] * z[38];
z[70] = abb[38] * z[33];
z[70] = -z[69] + z[70];
z[71] = abb[28] * z[39];
z[74] = 2 * abb[36];
z[75] = z[38] * z[74];
z[76] = 2 * abb[44];
z[77] = -z[33] * z[76];
z[78] = abb[40] * z[67];
z[40] = z[17] + z[40] + z[47] + z[63] + -z[65] + 2 * z[70] + z[71] + z[75] + z[77] + z[78];
z[40] = abb[22] * z[40];
z[47] = z[18] * z[33];
z[63] = 2 * abb[52] + -z[28] + -z[37];
z[71] = abb[28] * z[63];
z[47] = -z[39] + z[47] + z[71];
z[49] = -z[47] + -z[49];
z[49] = abb[29] * z[49];
z[75] = abb[39] * z[33];
z[77] = -z[69] + z[75];
z[71] = abb[33] * z[71];
z[26] = z[14] + z[26];
z[55] = 2 * z[26] + -z[55];
z[55] = abb[35] * z[55];
z[3] = z[3] * z[33];
z[78] = -abb[32] * z[3];
z[79] = prod_pow(abb[30], 2);
z[80] = z[33] * z[79];
z[17] = z[17] + z[49] + z[55] + -z[68] + z[71] + 2 * z[77] + z[78] + z[80];
z[17] = abb[24] * z[17];
z[49] = abb[28] + -z[0] + -z[52];
z[49] = abb[29] * z[49];
z[52] = abb[42] + -abb[62];
z[55] = -abb[28] * abb[33];
z[6] = z[6] + z[49] + 2 * z[52] + z[55] + z[79];
z[6] = abb[4] * z[6];
z[8] = -z[8] + -z[44] + z[46];
z[2] = -abb[0] * z[2];
z[44] = abb[4] * abb[34];
z[44] = z[44] + -z[51];
z[41] = z[41] * z[44];
z[46] = -abb[0] + -abb[4];
z[46] = z[46] * z[73];
z[2] = z[2] + z[6] + z[8] + z[41] + z[45] + (T(2) / T(3)) * z[46];
z[2] = z[2] * z[32];
z[6] = -abb[36] + abb[40] + -abb[60] + abb[63];
z[6] = z[6] * z[42];
z[41] = z[14] + -z[58];
z[41] = abb[35] * z[41];
z[19] = -z[19] + z[59];
z[19] = abb[31] * z[19];
z[45] = -abb[29] * z[60];
z[14] = -abb[28] * z[14];
z[46] = -abb[33] * z[43];
z[49] = -abb[45] * abb[58];
z[6] = z[6] + z[14] + z[19] + z[41] + z[45] + z[46] + z[49];
z[6] = z[6] * z[61];
z[14] = -z[26] + z[56];
z[14] = z[14] * z[18];
z[18] = abb[63] * z[42];
z[18] = z[18] + z[70] + z[75];
z[19] = abb[32] * z[33];
z[3] = -z[3] + z[19];
z[3] = abb[32] * z[3];
z[13] = z[13] * z[43];
z[3] = z[3] + -z[13] + -z[14] + 2 * z[18];
z[13] = -abb[29] * z[47];
z[14] = abb[33] * z[63];
z[14] = z[14] + z[57];
z[14] = abb[28] * z[14];
z[0] = -z[0] * z[16];
z[16] = -abb[60] * z[67];
z[0] = z[0] + z[3] + z[13] + z[14] + z[16] + -z[65];
z[0] = abb[21] * z[0];
z[13] = 2 * abb[61];
z[14] = -z[13] + -z[31] + -z[76];
z[14] = z[14] * z[33];
z[16] = -z[27] * z[36];
z[18] = -abb[35] * z[33];
z[18] = z[18] + z[20];
z[18] = z[18] * z[24];
z[19] = -abb[52] + -4 * z[28] + 5 * z[37];
z[19] = z[19] * z[54];
z[3] = z[3] + z[14] + z[16] + z[18] + z[19];
z[3] = abb[23] * z[3];
z[14] = -2 * abb[58] + -z[32];
z[16] = -abb[0] + abb[8];
z[14] = z[14] * z[16];
z[16] = abb[21] * z[33];
z[18] = abb[24] * z[33];
z[16] = z[16] + z[18];
z[5] = -abb[13] + z[5] + -z[25];
z[5] = abb[59] * z[5];
z[5] = z[5] + z[14] + z[16];
z[5] = abb[37] * z[5];
z[14] = abb[32] + -abb[35];
z[14] = z[14] * z[44];
z[19] = -abb[4] * abb[62];
z[14] = z[14] + z[19];
z[8] = z[8] + 2 * z[14];
z[8] = abb[57] * z[8];
z[12] = z[12] + -z[66];
z[12] = abb[58] * z[12];
z[14] = abb[24] * z[42];
z[12] = z[12] + -z[14];
z[15] = -z[15] + z[66];
z[15] = abb[59] * z[15];
z[19] = abb[22] * z[42];
z[15] = -z[12] + z[15] + z[19];
z[15] = abb[43] * z[15];
z[19] = -abb[6] + abb[9];
z[19] = abb[59] * z[19];
z[12] = -z[12] + 2 * z[19];
z[12] = abb[40] * z[12];
z[5] = z[5] + z[8] + z[12] + z[15];
z[8] = 2 * abb[10];
z[8] = z[8] * z[53];
z[12] = -abb[59] * z[35];
z[15] = -abb[4] * z[32];
z[19] = -abb[22] * z[33];
z[12] = z[8] + z[12] + z[15] + -z[18] + z[19];
z[12] = z[12] * z[13];
z[13] = z[39] + z[48] + -z[50];
z[15] = abb[28] + -abb[29];
z[13] = z[13] * z[15];
z[13] = z[13] + 2 * z[69];
z[13] = abb[25] * z[13];
z[15] = abb[16] + z[66];
z[9] = abb[2] + z[9] + -z[15] + z[23];
z[9] = abb[59] * z[9];
z[15] = abb[15] + -z[15] + z[34] + z[72];
z[15] = abb[58] * z[15];
z[18] = abb[25] * z[38];
z[9] = z[9] + -z[14] + z[15] + z[18];
z[9] = z[9] * z[74];
z[14] = z[21] + z[22] + (T(4) / T(3)) * z[73] + -z[79];
z[8] = z[8] * z[14];
z[14] = -abb[59] * z[29];
z[15] = -abb[58] * z[30];
z[19] = -abb[18] * z[32];
z[14] = z[14] + z[15] + z[19] + -z[62];
z[14] = abb[64] * z[14];
z[10] = -z[10] + z[66];
z[10] = abb[59] * z[10];
z[7] = -z[7] + z[66];
z[7] = abb[58] * z[7];
z[7] = z[7] + z[10] + -z[16];
z[7] = z[7] * z[76];
z[10] = -abb[24] * z[64];
z[10] = z[10] + (T(2) / T(3)) * z[18];
z[10] = z[10] * z[73];
z[0] = z[0] + z[1] + z[2] + z[3] + 2 * z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[17] + z[40];
z[0] = 2 * z[0];
return {z[4], z[0]};
}


template <typename T> std::complex<T> f_4_709_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("-5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("-10.4023028191312333362719401831913221365702768824772793193192573969"),stof<T>("-4.0099607112369939801253103352120106710102959542432765442127783144")}, std::complex<T>{stof<T>("-15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("-1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("-5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("1.751859103021736573420335234316175646090768707972018958260077726"),stof<T>("-23.321223941531039614276586174402678952645660031298272678985989205")}, std::complex<T>{stof<T>("-25.514205731730803297405729886997480010359956010634664886931192621"),stof<T>("-36.916552190357586975765762273827768645282794678010635435462489747")}, std::complex<T>{stof<T>("13.2741244426627002273774269247087048151321272454496985707493460998"),stof<T>("-1.7606299963135058907292223372868390240467892650404665348344365107")}, std::complex<T>{stof<T>("18.115715729748419661476374537553446400941967581790848506914123715"),stof<T>("-1.865231470881982908015474912760416863211443942791354825564564092")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_709_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_709_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("15.027648461811750172159449090228423730552511801388229864895411365"),stof<T>("-18.707462405770759069511983873352611993254053234583799179192624201")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,65> abb = {dl[0], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W15(k,dl), dlog_W18(k,dl), dlog_W26(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_6(k), f_1_7(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_19(k), f_2_21(k), f_2_28_im(k), f_2_4_im(k), f_2_6_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_4_re(k), f_2_6_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k)};

                    
            return f_4_709_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_709_DLogXconstant_part(base_point<T>, kend);
	value += f_4_709_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_709_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_709_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_709_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_709_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_709_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_709_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
