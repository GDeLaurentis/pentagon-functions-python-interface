/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_58.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_58_abbreviated (const std::array<T,68>& abb) {
T z[179];
z[0] = abb[57] * (T(1) / T(4));
z[1] = abb[49] * (T(4) / T(3)) + z[0];
z[2] = abb[58] * (T(1) / T(4));
z[3] = abb[44] * (T(1) / T(2));
z[4] = 2 * abb[59];
z[5] = 2 * abb[56];
z[6] = abb[46] * (T(-5) / T(4)) + z[5];
z[6] = abb[50] * (T(4) / T(3)) + abb[47] * (T(5) / T(4)) + z[1] + z[2] + -z[3] + -z[4] + (T(1) / T(3)) * z[6];
z[6] = abb[6] * z[6];
z[7] = abb[43] * (T(1) / T(4)) + -z[3];
z[8] = abb[50] * (T(1) / T(4));
z[9] = abb[57] * (T(1) / T(2));
z[10] = 4 * abb[46];
z[11] = abb[56] * (T(13) / T(2)) + -z[10];
z[2] = abb[52] * (T(-13) / T(2)) + abb[59] * (T(-8) / T(3)) + abb[54] * (T(4) / T(3)) + z[2] + z[7] + -z[8] + z[9] + (T(1) / T(3)) * z[11];
z[2] = abb[0] * z[2];
z[11] = 2 * abb[50];
z[12] = 2 * abb[49];
z[13] = z[11] + z[12];
z[14] = -abb[56] + abb[59];
z[15] = abb[58] * (T(1) / T(2));
z[16] = abb[46] * (T(1) / T(2));
z[14] = -z[3] + -z[13] + (T(7) / T(3)) * z[14] + -z[15] + z[16];
z[14] = abb[1] * z[14];
z[17] = abb[43] + -abb[56];
z[18] = abb[46] * (T(5) / T(6)) + abb[50] * (T(11) / T(3)) + z[15] + -z[17];
z[19] = abb[47] * (T(1) / T(4));
z[20] = abb[59] * (T(1) / T(3)) + -z[19];
z[1] = abb[54] * (T(-5) / T(6)) + z[1] + z[3] + (T(1) / T(2)) * z[18] + -z[20];
z[1] = abb[8] * z[1];
z[18] = abb[47] * (T(1) / T(2));
z[21] = -abb[54] + z[18];
z[22] = abb[43] * (T(3) / T(2));
z[23] = abb[49] + z[21] + z[22];
z[24] = z[9] + -z[16];
z[25] = abb[56] + z[24];
z[26] = abb[50] * (T(1) / T(2));
z[27] = z[23] + -z[25] + z[26];
z[28] = abb[16] * z[27];
z[29] = 13 * abb[1] + -abb[6];
z[29] = abb[55] * z[29];
z[29] = z[28] + z[29];
z[20] = abb[43] * (T(-13) / T(12)) + abb[56] * (T(-2) / T(3)) + abb[49] * (T(-1) / T(6)) + abb[50] * (T(5) / T(12)) + abb[46] * (T(9) / T(4)) + abb[54] * (T(13) / T(6)) + z[0] + z[3] + z[20];
z[20] = abb[3] * z[20];
z[30] = abb[49] + abb[50];
z[31] = abb[56] + z[30];
z[32] = abb[46] + z[31];
z[32] = (T(1) / T(2)) * z[32];
z[33] = z[4] + z[32];
z[33] = -z[9] + -z[18] + (T(1) / T(3)) * z[33];
z[33] = abb[17] * z[33];
z[34] = -abb[50] + -abb[56] + z[15];
z[35] = z[16] + z[34];
z[36] = abb[47] * (T(3) / T(4));
z[0] = abb[59] + -z[0] + (T(1) / T(2)) * z[35] + -z[36];
z[35] = -abb[4] * z[0];
z[37] = abb[43] + abb[50];
z[38] = -abb[46] + z[37];
z[39] = 4 * abb[54];
z[40] = 8 * abb[59] + abb[49] * (T(-7) / T(2)) + -4 * z[38] + z[39];
z[41] = abb[47] * (T(3) / T(2));
z[40] = (T(1) / T(3)) * z[40] + -z[41];
z[40] = abb[12] * z[40];
z[42] = abb[62] * (T(1) / T(4));
z[43] = abb[23] + abb[25];
z[44] = abb[22] + z[43];
z[45] = -abb[26] + z[44];
z[46] = abb[24] + z[45];
z[47] = -z[42] * z[46];
z[48] = abb[43] + -abb[46];
z[49] = abb[49] * (T(1) / T(2));
z[50] = -abb[54] + abb[56] * (T(-1) / T(2)) + -z[48] + z[49];
z[51] = abb[45] + -abb[52];
z[50] = (T(1) / T(3)) * z[50] + (T(-1) / T(2)) * z[51];
z[50] = abb[2] * z[50];
z[51] = 5 * abb[44];
z[52] = abb[48] + -abb[56];
z[53] = z[51] + -z[52];
z[53] = abb[13] * z[53];
z[54] = abb[10] * abb[48];
z[55] = 5 * abb[43];
z[56] = -abb[56] + -z[55];
z[56] = abb[10] * z[56];
z[57] = abb[60] + abb[61];
z[58] = abb[19] * z[57];
z[1] = z[1] + z[2] + z[6] + z[14] + z[20] + (T(1) / T(2)) * z[29] + z[33] + z[35] + z[40] + z[47] + z[50] + z[53] + z[54] + z[56] + (T(1) / T(4)) * z[58];
z[2] = prod_pow(m1_set::bc<T>[0], 2);
z[1] = z[1] * z[2];
z[6] = abb[50] * (T(5) / T(2));
z[14] = 3 * abb[58];
z[20] = z[6] + -z[14];
z[29] = abb[44] * (T(3) / T(2));
z[33] = abb[51] * (T(3) / T(2));
z[35] = z[29] + z[33];
z[40] = 6 * abb[55];
z[47] = 4 * abb[49];
z[50] = 4 * abb[59];
z[53] = abb[57] * (T(3) / T(2));
z[56] = -abb[46] + 9 * abb[56];
z[56] = z[20] + z[35] + -z[40] + z[47] + -z[50] + z[53] + (T(1) / T(2)) * z[56];
z[56] = abb[1] * z[56];
z[58] = abb[51] * (T(9) / T(2));
z[59] = -z[3] + z[58];
z[60] = abb[49] + abb[56];
z[61] = 3 * abb[59];
z[20] = -abb[46] + z[20] + z[59] + (T(5) / T(2)) * z[60] + -z[61];
z[20] = abb[3] * z[20];
z[60] = abb[58] * (T(3) / T(2));
z[62] = -abb[56] + z[60];
z[63] = -abb[46] + z[62];
z[64] = abb[49] * (T(3) / T(2));
z[65] = abb[44] + abb[59];
z[66] = -z[41] + -z[63] + z[64] + z[65];
z[66] = abb[8] * z[66];
z[67] = z[15] + z[18];
z[68] = abb[44] + abb[50] + -z[25] + z[67];
z[69] = abb[7] * z[68];
z[70] = abb[46] + z[30];
z[71] = -abb[58] + z[70];
z[72] = abb[44] + (T(1) / T(2)) * z[71];
z[73] = 3 * abb[11];
z[72] = z[72] * z[73];
z[72] = (T(-3) / T(2)) * z[69] + z[72];
z[74] = z[31] + -z[60];
z[75] = abb[46] * (T(3) / T(2));
z[76] = z[74] + z[75];
z[77] = -z[4] + z[41];
z[78] = z[53] + z[77];
z[79] = 2 * abb[44];
z[80] = -z[76] + z[78] + -z[79];
z[81] = abb[14] * z[80];
z[82] = abb[50] * (T(3) / T(2));
z[83] = abb[59] + z[82];
z[63] = abb[44] * (T(5) / T(2)) + -z[53] + -z[63] + z[83];
z[63] = abb[6] * z[63];
z[84] = -abb[48] + z[25];
z[85] = z[15] + -z[18];
z[86] = -abb[51] + abb[59] + -z[84] + z[85];
z[87] = 3 * abb[9];
z[86] = z[86] * z[87];
z[88] = z[18] + z[24];
z[89] = -abb[44] + z[88];
z[90] = -abb[51] + -z[74] + z[89];
z[91] = abb[15] * z[90];
z[25] = z[25] + -z[30];
z[92] = z[15] + z[25];
z[19] = -abb[48] + z[19] + (T(1) / T(2)) * z[92];
z[92] = 3 * abb[5];
z[19] = z[19] * z[92];
z[32] = z[32] + -z[78];
z[32] = abb[17] * z[32];
z[92] = abb[23] + abb[24];
z[93] = abb[25] * (T(1) / T(2)) + z[92];
z[94] = abb[62] * (T(3) / T(2));
z[95] = z[93] * z[94];
z[96] = abb[13] * abb[44];
z[97] = 6 * z[96];
z[20] = z[19] + z[20] + -z[32] + z[56] + z[63] + z[66] + -z[72] + -z[81] + z[86] + (T(3) / T(2)) * z[91] + z[95] + -z[97];
z[20] = abb[31] * z[20];
z[56] = 4 * abb[50];
z[63] = 6 * abb[58];
z[66] = abb[49] + z[4];
z[86] = 3 * abb[56];
z[91] = 3 * abb[57];
z[95] = 9 * abb[44] + z[10] + z[56] + -z[63] + z[66] + z[86] + -z[91];
z[95] = abb[1] * z[95];
z[98] = 5 * abb[49];
z[99] = 5 * abb[56];
z[100] = z[98] + z[99];
z[101] = 5 * abb[50] + z[100];
z[102] = z[4] + z[79];
z[103] = z[41] + z[53];
z[104] = z[60] + -z[75] + -z[101] + z[102] + z[103];
z[104] = abb[14] * z[104];
z[105] = 2 * abb[48];
z[106] = -z[5] + z[105];
z[107] = abb[47] + abb[57];
z[108] = -abb[46] + -z[106] + z[107];
z[109] = abb[58] + -z[102] + z[108];
z[110] = z[87] * z[109];
z[25] = z[25] + z[67] + -z[105];
z[105] = abb[5] * z[25];
z[111] = 3 * z[105];
z[110] = z[110] + -z[111];
z[112] = 4 * abb[44];
z[113] = 2 * abb[46];
z[114] = -z[14] + z[31] + z[112] + z[113];
z[115] = abb[3] * z[114];
z[116] = -abb[56] + z[16];
z[117] = z[60] + 5 * z[116];
z[118] = 3 * abb[50];
z[119] = 3 * abb[49];
z[120] = z[118] + z[119];
z[121] = z[117] + z[120];
z[122] = z[78] + z[112] + z[121];
z[123] = abb[6] + abb[8];
z[122] = z[122] * z[123];
z[123] = 3 * z[69];
z[71] = z[71] + z[79];
z[73] = z[71] * z[73];
z[124] = z[73] + z[123];
z[95] = z[95] + z[104] + z[110] + z[115] + z[122] + -z[124];
z[95] = abb[30] * z[95];
z[104] = 3 * abb[51];
z[115] = -z[4] + z[104];
z[122] = 4 * abb[56];
z[112] = -abb[46] + z[63] + -z[112] + -z[115] + -z[120] + -z[122];
z[112] = abb[1] * z[112];
z[125] = -abb[44] + -z[5] + z[14];
z[115] = -z[13] + -z[115] + z[125];
z[126] = abb[3] * z[115];
z[127] = -z[16] + z[62];
z[128] = -z[11] + z[127];
z[129] = -z[41] + z[53];
z[130] = 3 * abb[44];
z[131] = abb[49] + z[128] + z[129] + -z[130];
z[131] = abb[6] * z[131];
z[129] = -abb[50] + z[129];
z[132] = -z[12] + z[127] + -z[129];
z[132] = abb[8] * z[132];
z[112] = z[81] + z[112] + z[126] + z[131] + z[132];
z[112] = abb[34] * z[112];
z[126] = z[50] + z[79];
z[131] = -z[86] + z[113] + z[126];
z[132] = 6 * abb[51];
z[120] = -z[14] + z[120] + -z[131] + z[132];
z[133] = abb[3] * z[120];
z[134] = abb[8] * z[80];
z[135] = abb[6] * z[80];
z[136] = z[134] + z[135];
z[137] = 3 * abb[47];
z[138] = -abb[46] + z[137];
z[50] = -z[31] + -z[50] + z[91] + z[138];
z[50] = abb[17] * z[50];
z[133] = z[50] + -z[81] + z[133] + -z[136];
z[139] = 6 * abb[59];
z[113] = abb[44] + z[113] + z[139];
z[63] = 9 * abb[51] + -z[63] + z[101] + -z[113];
z[63] = abb[1] * z[63];
z[101] = abb[46] + z[4];
z[140] = abb[44] + z[101];
z[107] = -abb[51] + -z[107] + z[140];
z[107] = z[87] * z[107];
z[63] = z[63] + z[107] + z[133];
z[141] = -abb[29] + abb[33];
z[63] = z[63] * z[141];
z[142] = abb[30] * z[50];
z[143] = 3 * abb[15];
z[144] = abb[33] + -abb[34];
z[145] = -abb[29] + z[144];
z[146] = z[90] * z[143] * z[145];
z[145] = z[92] * z[145];
z[147] = abb[24] + z[43];
z[148] = abb[30] * z[147];
z[145] = z[145] + -z[148];
z[148] = z[94] * z[145];
z[20] = z[20] + z[63] + z[95] + z[112] + -z[142] + z[146] + z[148];
z[20] = abb[31] * z[20];
z[63] = 2 * abb[54];
z[95] = -z[26] + z[63];
z[112] = abb[46] * (T(5) / T(2)) + -z[86];
z[146] = abb[43] * (T(7) / T(2));
z[47] = z[47] + -z[78] + -z[95] + z[112] + z[146];
z[47] = abb[8] * z[47];
z[148] = -z[16] + z[77];
z[149] = z[53] + z[60];
z[56] = abb[49] + z[56] + z[122] + z[148] + -z[149];
z[56] = abb[6] * z[56];
z[122] = -z[53] + z[77] + z[122];
z[150] = abb[46] * (T(7) / T(2)) + -z[6] + z[12] + z[22] + -z[122];
z[150] = abb[3] * z[150];
z[151] = -z[4] + z[91];
z[152] = -abb[43] + abb[54];
z[153] = abb[56] + z[11] + -z[151] + -z[152];
z[153] = abb[0] * z[153];
z[154] = -abb[54] + z[37];
z[155] = z[12] + -z[101] + z[154];
z[155] = abb[12] * z[155];
z[156] = abb[49] + -abb[56];
z[157] = abb[43] + z[156];
z[10] = z[10] + -z[39] + 7 * z[157];
z[10] = abb[2] * z[10];
z[34] = -z[24] + z[34] + -z[77];
z[77] = 3 * abb[4];
z[157] = z[34] * z[77];
z[158] = 3 * z[28];
z[159] = (T(3) / T(2)) * z[57];
z[160] = abb[18] * z[159];
z[10] = z[10] + z[47] + z[50] + z[56] + -z[150] + z[153] + z[155] + z[157] + -z[158] + -z[160];
z[47] = -z[46] * z[94];
z[56] = abb[19] * z[159];
z[47] = -z[10] + z[47] + z[56];
z[47] = abb[67] * z[47];
z[56] = -abb[56] + -z[4] + z[53];
z[37] = (T(3) / T(4)) * z[37];
z[150] = abb[58] * (T(3) / T(4));
z[153] = -abb[46] + abb[54];
z[155] = -z[29] + z[33] + -z[37] + z[56] + z[150] + z[153];
z[155] = abb[0] * z[155];
z[157] = abb[43] * (T(1) / T(2));
z[161] = z[6] + z[157];
z[162] = -z[14] + z[119];
z[103] = abb[54] + -z[16] + -z[86] + z[103] + -z[104] + -z[161] + -z[162];
z[103] = abb[3] * z[103];
z[163] = (T(3) / T(2)) * z[28];
z[164] = z[135] + z[163];
z[115] = abb[1] * z[115];
z[129] = abb[44] + -z[60] + z[129];
z[165] = 3 * abb[43];
z[112] = abb[54] + -z[98] + -z[112] + -z[129] + -z[165];
z[166] = abb[8] * (T(1) / T(2));
z[112] = z[112] * z[166];
z[66] = -z[66] + z[138] + z[154];
z[66] = abb[12] * z[66];
z[103] = -z[66] + (T(1) / T(2)) * z[103] + z[112] + z[115] + z[155] + z[164];
z[103] = abb[34] * z[103];
z[112] = abb[1] * z[120];
z[112] = z[50] + z[112];
z[120] = -abb[56] + z[75];
z[138] = abb[50] * (T(-7) / T(2)) + 3 * z[120] + z[157] + -z[162];
z[154] = abb[57] * (T(3) / T(4));
z[167] = z[36] + z[154];
z[168] = abb[54] * (T(1) / T(2));
z[58] = -z[58] + z[126] + (T(1) / T(2)) * z[138] + -z[167] + -z[168];
z[58] = abb[3] * z[58];
z[138] = z[119] + z[165];
z[169] = -z[16] + z[60] + -z[99] + -z[118] + z[138];
z[169] = abb[57] * (T(9) / T(4)) + -z[4] + -z[29] + z[36] + -z[168] + (T(1) / T(2)) * z[169];
z[169] = abb[8] * z[169];
z[170] = z[66] + z[81];
z[58] = z[58] + -z[107] + -z[112] + -z[155] + -z[163] + z[169] + z[170];
z[58] = abb[29] * z[58];
z[107] = z[5] + -z[60];
z[155] = z[91] + -z[102] + -z[107] + z[153] + -z[161];
z[155] = abb[0] * z[155];
z[135] = z[135] + z[158];
z[157] = -z[26] + z[157];
z[161] = -abb[56] + z[53];
z[148] = z[148] + z[157] + z[161];
z[148] = abb[3] * z[148];
z[169] = 2 * abb[43];
z[171] = z[12] + z[169];
z[172] = z[5] + z[153];
z[173] = z[171] + -z[172];
z[174] = 2 * abb[8];
z[175] = z[173] * z[174];
z[148] = z[135] + z[148] + z[155] + -z[170] + -z[175];
z[155] = abb[32] * z[148];
z[114] = abb[1] * z[114];
z[131] = -z[118] + z[131] + -z[137];
z[131] = abb[3] * z[131];
z[176] = -z[31] + z[65];
z[176] = 4 * z[176];
z[177] = abb[6] * z[176];
z[131] = z[81] + z[114] + z[131] + z[134] + z[177];
z[131] = abb[30] * z[131];
z[131] = z[131] + -z[142] + z[155];
z[64] = abb[56] * (T(3) / T(2)) + -z[60] + z[64] + z[82] + z[104] + -z[140];
z[64] = abb[1] * z[64];
z[76] = z[65] + (T(1) / T(2)) * z[76] + -z[167];
z[76] = abb[6] * z[76];
z[64] = -z[32] + z[64] + z[76] + -z[163];
z[76] = abb[44] + z[150];
z[61] = z[61] + z[76];
z[134] = abb[54] * (T(3) / T(2)) + -z[75];
z[104] = -abb[56] + abb[43] * (T(7) / T(4)) + -z[8] + -z[53] + z[61] + -z[104] + -z[134];
z[104] = abb[0] * z[104];
z[36] = -z[36] + z[154];
z[37] = abb[46] * (T(-11) / T(4)) + z[5] + -z[36] + z[37] + -z[63] + -z[65];
z[37] = abb[3] * z[37];
z[48] = z[30] + z[48];
z[140] = abb[59] + z[168];
z[18] = z[18] + (T(1) / T(2)) * z[48] + -z[140];
z[48] = 3 * abb[12];
z[18] = z[18] * z[48];
z[154] = abb[43] + abb[49] + z[60] + 3 * z[116];
z[177] = -abb[59] + z[167];
z[178] = (T(1) / T(2)) * z[154] + -z[177];
z[178] = abb[8] * z[178];
z[18] = z[18] + z[37] + z[64] + z[104] + -z[160] + z[178];
z[18] = abb[33] * z[18];
z[18] = z[18] + z[58] + z[103] + z[131];
z[18] = abb[33] * z[18];
z[37] = z[53] + z[150];
z[58] = abb[50] * (T(13) / T(4)) + abb[43] * (T(17) / T(4)) + -z[16] + -z[37] + -z[65] + z[99] + z[168];
z[58] = abb[0] * z[58];
z[103] = abb[43] * (T(5) / T(4));
z[8] = -abb[46] + -z[8] + -z[56] + -z[103] + -z[137] + z[150];
z[8] = abb[3] * z[8];
z[56] = z[65] + -z[67] + -z[84];
z[56] = z[56] * z[87];
z[38] = -z[38] + -z[98];
z[38] = (T(1) / T(2)) * z[38] + z[41] + z[140];
z[38] = abb[12] * z[38];
z[41] = abb[10] * abb[43];
z[84] = -z[19] + 6 * z[41];
z[8] = z[8] + z[38] + z[56] + z[58] + z[81] + -z[84] + -z[164] + z[175];
z[8] = abb[32] * z[8];
z[38] = -abb[34] * z[148];
z[31] = z[15] + z[31] + z[88] + -z[102];
z[58] = abb[6] * z[31];
z[31] = abb[14] * z[31];
z[31] = z[31] + z[105];
z[98] = abb[9] * z[109];
z[58] = -z[31] + z[58] + z[98];
z[98] = -abb[3] * z[34];
z[98] = z[58] + z[98];
z[98] = abb[30] * z[98];
z[8] = z[8] + z[38] + 3 * z[98];
z[8] = abb[32] * z[8];
z[38] = 3 * abb[54];
z[55] = z[38] + -z[55] + -z[75] + z[99] + -z[119] + z[129];
z[55] = z[55] * z[166];
z[7] = abb[50] * (T(-7) / T(4)) + z[7] + -z[33] + z[150] + z[161];
z[7] = abb[0] * z[7];
z[82] = z[22] + z[82];
z[98] = z[82] + -z[116] + z[162];
z[99] = -z[33] + z[168];
z[98] = -z[4] + (T(1) / T(2)) * z[98] + -z[99] + z[167];
z[98] = abb[3] * z[98];
z[7] = z[7] + z[55] + -z[81] + z[98] + -z[115] + z[163];
z[7] = abb[34] * z[7];
z[55] = z[29] + z[83] + -z[134] + -z[149] + z[171];
z[55] = abb[8] * z[55];
z[83] = 6 * abb[52];
z[37] = -abb[56] + abb[50] * (T(11) / T(4)) + -z[37] + z[59] + z[83] + -z[103];
z[37] = abb[0] * z[37];
z[59] = -abb[46] + -z[62] + z[82] + z[119];
z[59] = (T(1) / T(2)) * z[59] + -z[65] + -z[99];
z[59] = abb[3] * z[59];
z[62] = abb[59] + -z[15] + -z[52] + -z[89];
z[62] = z[62] * z[87];
z[37] = z[37] + z[55] + z[59] + z[62] + z[64] + z[84] + z[160];
z[37] = abb[29] * z[37];
z[55] = -abb[46] + z[14];
z[59] = (T(1) / T(2)) * z[55] + -z[78] + -z[79];
z[59] = abb[3] * z[59];
z[62] = abb[14] * z[176];
z[59] = z[59] + -z[62] + -z[110] + -z[114] + -z[136];
z[64] = abb[30] * z[59];
z[7] = z[7] + z[37] + z[64] + z[142] + z[155];
z[7] = abb[29] * z[7];
z[16] = -z[16] + z[86];
z[37] = z[16] + -z[60] + -z[165];
z[64] = -abb[50] + z[49];
z[3] = abb[54] + z[3] + z[33] + (T(1) / T(2)) * z[37] + -z[64] + -z[167];
z[3] = abb[3] * z[3];
z[16] = abb[49] + -z[6] + (T(-1) / T(2)) * z[16] + z[36] + -z[76] + z[169];
z[16] = abb[8] * z[16];
z[29] = z[29] + -z[36] + -z[64] + (T(-1) / T(2)) * z[127];
z[29] = abb[6] * z[29];
z[33] = z[35] + z[49] + -z[53] + -z[128];
z[33] = abb[1] * z[33];
z[35] = abb[44] + z[107] + -z[157];
z[35] = abb[0] * z[35];
z[3] = z[3] + z[16] + z[29] + z[33] + z[35] + z[72];
z[16] = prod_pow(abb[34], 2);
z[3] = z[3] * z[16];
z[29] = abb[56] * (T(-9) / T(2)) + -z[26] + -z[49] + z[60] + z[101] + -z[130];
z[29] = abb[1] * z[29];
z[33] = abb[46] * (T(-1) / T(4)) + -z[5] + -z[13] + z[61] + -z[167];
z[33] = abb[3] * z[33];
z[35] = -27 * abb[49] + -z[117] + -z[118];
z[35] = (T(1) / T(2)) * z[35] + -z[51] + -z[177];
z[35] = abb[6] * z[35];
z[36] = -z[79] + (T(-1) / T(2)) * z[121] + -z[177];
z[36] = abb[8] * z[36];
z[19] = z[19] + z[29] + z[33] + z[35] + z[36] + z[56] + -z[62];
z[19] = abb[30] * z[19];
z[29] = -abb[46] + -abb[56];
z[14] = z[14] + 2 * z[29] + -z[51] + -z[118] + z[151];
z[14] = abb[1] * z[14];
z[29] = 2 * abb[6];
z[33] = -z[29] + -z[174];
z[35] = -abb[56] + z[70];
z[35] = 2 * z[35] + z[130];
z[33] = z[33] * z[35];
z[35] = abb[3] * z[80];
z[14] = z[14] + z[33] + z[35] + -z[81] + z[124];
z[14] = abb[34] * z[14];
z[14] = z[14] + z[19];
z[14] = abb[30] * z[14];
z[19] = z[11] + z[130];
z[33] = -z[9] + z[19] + z[75] + -z[85] + z[156];
z[33] = abb[6] * z[33];
z[35] = abb[50] + z[79];
z[36] = z[9] + z[12] + z[35] + -z[67] + z[120];
z[36] = abb[8] * z[36];
z[37] = -abb[3] + abb[15];
z[37] = z[37] * z[90];
z[5] = -abb[57] + z[5];
z[19] = abb[49] + abb[51] + z[5] + z[19] + -z[55];
z[19] = abb[1] * z[19];
z[49] = abb[62] * (T(1) / T(2));
z[51] = -abb[64] + z[49];
z[53] = abb[22] + z[92];
z[55] = z[51] * z[53];
z[56] = -abb[11] * z[71];
z[19] = z[19] + z[33] + z[36] + z[37] + z[55] + z[56] + -z[69];
z[33] = 3 * abb[36];
z[19] = z[19] * z[33];
z[36] = abb[24] + abb[26];
z[37] = abb[22] + abb[25];
z[55] = z[36] + -z[37];
z[56] = abb[29] * (T(1) / T(2));
z[61] = z[55] * z[56];
z[62] = abb[24] * abb[30];
z[64] = abb[32] * z[45];
z[62] = z[62] + z[64];
z[65] = abb[22] + abb[24] + abb[25] + -abb[26];
z[65] = abb[23] + (T(1) / T(2)) * z[65];
z[65] = abb[34] * z[65];
z[67] = abb[33] * z[46];
z[61] = z[61] + z[62] + z[65] + (T(-1) / T(2)) * z[67];
z[61] = abb[33] * z[61];
z[65] = abb[23] + -abb[26];
z[65] = z[56] * z[65];
z[55] = abb[34] * z[55];
z[70] = abb[24] + z[44];
z[71] = abb[30] * z[70];
z[55] = (T(1) / T(2)) * z[55] + -z[64] + z[65] + z[71];
z[55] = abb[29] * z[55];
z[64] = abb[26] * (T(1) / T(2)) + -z[44];
z[64] = abb[32] * z[64];
z[65] = abb[30] * z[44];
z[45] = abb[34] * z[45];
z[45] = -z[45] + z[64] + z[65];
z[45] = abb[32] * z[45];
z[64] = abb[22] * abb[34];
z[64] = -z[64] + (T(1) / T(2)) * z[71];
z[64] = abb[30] * z[64];
z[65] = (T(1) / T(2)) * z[16];
z[71] = z[53] * z[65];
z[37] = abb[35] * z[37];
z[70] = abb[38] * z[70];
z[37] = z[37] + z[45] + -z[55] + z[61] + -z[64] + -z[70] + -z[71];
z[45] = abb[31] * z[93];
z[45] = z[45] + z[145];
z[45] = abb[31] * z[45];
z[55] = abb[27] * abb[39];
z[61] = abb[37] * z[44];
z[64] = abb[67] * z[46];
z[70] = abb[65] * z[36];
z[45] = -z[37] + z[45] + (T(-1) / T(2)) * z[55] + z[61] + -z[64] + z[70];
z[55] = z[2] * z[46];
z[61] = 3 * abb[66];
z[64] = z[61] * z[147];
z[45] = 3 * z[45] + (T(-1) / T(2)) * z[55] + -z[64];
z[33] = -z[33] * z[53];
z[33] = z[33] + -z[45];
z[33] = abb[63] * z[33];
z[53] = (T(1) / T(2)) * z[57];
z[55] = abb[32] * z[53];
z[64] = abb[34] * z[57];
z[55] = z[55] + z[64];
z[55] = abb[32] * z[55];
z[70] = abb[34] * z[53];
z[71] = abb[32] * z[57];
z[70] = z[70] + z[71];
z[72] = abb[29] * z[53];
z[75] = z[70] + -z[72];
z[76] = abb[33] * z[53];
z[76] = -z[75] + z[76];
z[76] = abb[33] * z[76];
z[55] = z[55] + z[76];
z[76] = -abb[39] * z[27];
z[80] = abb[65] + abb[67];
z[80] = z[57] * z[80];
z[75] = abb[29] * z[75];
z[75] = -z[55] + z[75] + z[76] + z[80];
z[76] = z[2] * z[53];
z[75] = 3 * z[75] + z[76];
z[75] = abb[21] * z[75];
z[33] = z[33] + z[75];
z[37] = -z[37] * z[94];
z[25] = abb[14] * z[25];
z[25] = z[25] + -z[105];
z[15] = z[5] + -z[15] + z[82];
z[15] = abb[0] * z[15];
z[27] = abb[8] * z[27];
z[17] = abb[10] * z[17];
z[17] = z[17] + z[54];
z[54] = z[36] * z[49];
z[53] = abb[18] * z[53];
z[15] = -z[15] + -2 * z[17] + -z[25] + -z[27] + z[28] + -z[53] + z[54];
z[17] = 3 * abb[65];
z[27] = z[15] * z[17];
z[9] = z[9] + z[116];
z[28] = abb[49] + z[9] + z[85];
z[28] = abb[6] * z[28];
z[53] = abb[8] * z[68];
z[5] = -abb[58] + z[5] + z[35];
z[5] = abb[1] * z[5];
z[35] = abb[44] + z[52];
z[52] = 2 * abb[13];
z[35] = z[35] * z[52];
z[5] = z[5] + z[25] + z[28] + z[35] + z[53] + -z[69];
z[25] = -z[49] * z[147];
z[25] = z[5] + z[25];
z[25] = z[25] * z[61];
z[28] = -abb[64] * z[45];
z[4] = 2 * abb[51] + -z[4];
z[35] = z[4] + z[74] + z[88];
z[35] = abb[3] * z[35];
z[45] = -abb[44] + -abb[51] + abb[58] + -z[106];
z[45] = abb[9] * z[45];
z[31] = -z[31] + z[35] + z[45] + -z[115];
z[31] = abb[35] * z[31];
z[35] = abb[3] + -abb[4];
z[35] = z[34] * z[35];
z[44] = z[44] * z[49];
z[35] = z[35] + z[44] + -z[58];
z[35] = abb[37] * z[35];
z[44] = abb[20] * z[90];
z[21] = -abb[43] + -abb[44] + -z[21] + z[24] + -z[74];
z[21] = abb[18] * z[21];
z[21] = z[21] + z[44];
z[24] = abb[0] + abb[3];
z[24] = -abb[8] + 2 * abb[28] + abb[16] * (T(-1) / T(4)) + (T(-3) / T(4)) * z[24];
z[24] = z[24] * z[57];
z[42] = -abb[27] * z[42];
z[21] = (T(1) / T(2)) * z[21] + z[24] + z[42];
z[21] = abb[39] * z[21];
z[21] = z[21] + z[31] + z[35];
z[24] = z[50] + z[59];
z[24] = abb[38] * z[24];
z[31] = abb[34] * z[173];
z[35] = 14 * abb[43] + 6 * abb[45] + -z[83] + -z[153] + 8 * z[156];
z[35] = abb[32] * z[35];
z[31] = 4 * z[31] + z[35];
z[31] = abb[32] * z[31];
z[17] = z[16] + -z[17];
z[17] = z[17] * z[173];
z[35] = 2 * abb[32];
z[42] = abb[34] + z[35];
z[42] = abb[29] + -2 * z[42];
z[42] = abb[29] * z[42] * z[173];
z[35] = -abb[29] + z[35];
z[44] = -abb[34] + -z[35];
z[44] = abb[33] + 2 * z[44];
z[44] = abb[33] * z[44] * z[173];
z[17] = z[17] + z[31] + z[42] + z[44];
z[17] = abb[2] * z[17];
z[31] = abb[7] + -abb[11];
z[42] = (T(1) / T(2)) * z[31];
z[44] = abb[8] + z[42];
z[44] = z[16] * z[44];
z[45] = -abb[11] + z[174];
z[29] = -abb[7] + z[29] + z[45];
z[49] = abb[34] * z[29];
z[50] = abb[1] + -abb[8];
z[52] = -abb[30] * z[50];
z[49] = z[49] + z[52];
z[49] = abb[30] * z[49];
z[52] = -abb[30] * z[29];
z[42] = -abb[1] + -z[42];
z[42] = abb[31] * z[42];
z[42] = z[42] + z[52];
z[42] = abb[31] * z[42];
z[2] = z[2] * z[50];
z[29] = -abb[36] * z[29];
z[50] = -abb[6] + abb[7] + z[50];
z[52] = abb[66] * z[50];
z[2] = z[2] + z[29] + z[42] + z[44] + z[49] + z[52];
z[29] = 3 * abb[53];
z[2] = z[2] * z[29];
z[42] = abb[30] * z[34];
z[44] = -abb[32] * z[0];
z[44] = z[42] + z[44];
z[44] = abb[32] * z[44];
z[0] = abb[33] * z[0];
z[0] = z[0] + -z[42];
z[0] = abb[33] * z[0];
z[0] = z[0] + z[44];
z[0] = z[0] * z[77];
z[9] = abb[51] + -z[9] + -z[23] + z[26];
z[9] = abb[39] * z[9];
z[16] = -z[16] * z[57];
z[23] = z[70] + z[72];
z[23] = abb[29] * z[23];
z[9] = z[9] + z[16] + z[23] + -z[55];
z[16] = abb[19] * (T(3) / T(2));
z[9] = z[9] * z[16];
z[23] = abb[6] * z[40];
z[23] = z[23] + -z[32] + z[97];
z[23] = prod_pow(abb[30], 2) * z[23];
z[32] = abb[33] * (T(-1) / T(2)) + z[56];
z[40] = abb[29] + abb[34];
z[32] = z[32] * z[40];
z[32] = abb[35] + z[32] + z[65];
z[32] = z[32] * z[90] * z[143];
z[0] = z[0] + z[1] + z[2] + z[3] + z[7] + z[8] + z[9] + z[14] + z[17] + z[18] + z[19] + z[20] + 3 * z[21] + z[23] + z[24] + z[25] + z[27] + z[28] + z[32] + (T(1) / T(2)) * z[33] + z[37] + z[47];
z[1] = -z[60] + z[132];
z[2] = -abb[46] + abb[56];
z[3] = abb[43] * (T(5) / T(2));
z[2] = z[1] + 3 * z[2] + -z[3] + z[6] + z[63] + -z[126];
z[2] = abb[3] * z[2];
z[6] = abb[18] * z[57];
z[6] = 3 * z[6] + -z[112];
z[7] = z[60] + z[63] + -z[78] + -z[116] + -z[138];
z[7] = abb[8] * z[7];
z[4] = -abb[58] + z[4] + z[108];
z[4] = z[4] * z[87];
z[4] = z[4] + -z[111];
z[8] = z[151] + z[172];
z[1] = -12 * abb[52] + abb[50] * (T(-9) / T(2)) + -z[1] + z[8] + z[22];
z[1] = abb[0] * z[1];
z[1] = z[1] + z[2] + -z[4] + -z[6] + z[7] + -12 * z[41] + z[158] + -z[170];
z[1] = abb[29] * z[1];
z[2] = abb[46] + z[3] + -z[60] + -z[95] + z[119];
z[2] = abb[3] * z[2];
z[3] = -z[73] + z[123];
z[7] = -z[8] + z[60] + z[82];
z[7] = abb[0] * z[7];
z[8] = -abb[50] + z[12] + -z[125] + z[151];
z[8] = abb[1] * z[8];
z[9] = abb[44] + z[13] + -z[153];
z[9] = z[9] * z[174];
z[2] = z[2] + z[3] + z[7] + z[8] + z[9] + z[66] + -z[135];
z[2] = abb[34] * z[2];
z[7] = -3 * abb[46] + z[26] + z[38] + -z[79] + z[91] + z[107] + z[132] + -z[139] + -z[146];
z[7] = abb[0] * z[7];
z[8] = abb[46] * (T(11) / T(2)) + z[39] + z[79] + -z[82] + -z[122];
z[8] = abb[3] * z[8];
z[9] = -abb[47] + -z[30] + z[101] + z[152];
z[9] = z[9] * z[48];
z[12] = z[78] + -z[154];
z[12] = abb[8] * z[12];
z[6] = z[6] + z[7] + z[8] + z[9] + z[12] + z[135];
z[6] = abb[33] * z[6];
z[7] = 12 * abb[55] + -z[11] + -z[91] + -z[100] + z[113];
z[7] = abb[1] * z[7];
z[8] = -z[94] * z[147];
z[3] = -z[3] + z[4] + z[7] + z[8] + 12 * z[96] + -z[133];
z[3] = abb[31] * z[3];
z[4] = -abb[26] + z[43];
z[4] = abb[34] * z[4];
z[7] = abb[29] * z[36];
z[4] = z[4] + z[7] + z[62] + -z[67];
z[7] = z[4] * z[94];
z[8] = -abb[33] * z[34];
z[8] = z[8] + z[42];
z[8] = z[8] * z[77];
z[9] = abb[2] * z[173];
z[11] = z[35] + -z[144];
z[11] = z[9] * z[11];
z[12] = z[57] * z[141];
z[12] = z[12] + z[64] + -z[71];
z[12] = z[12] * z[16];
z[1] = z[1] + z[2] + z[3] + z[6] + z[7] + z[8] + 2 * z[11] + z[12] + -z[131];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = 3 * abb[64];
z[3] = abb[63] * (T(3) / T(2));
z[6] = z[2] + z[3] + -z[94];
z[6] = z[6] * z[46];
z[7] = abb[19] + abb[21];
z[7] = z[7] * z[159];
z[6] = z[6] + z[7] + -z[10];
z[6] = abb[42] * z[6];
z[7] = -z[9] + z[15];
z[7] = abb[40] * z[7];
z[8] = -z[51] * z[147];
z[5] = z[5] + z[8];
z[5] = abb[41] * z[5];
z[5] = z[5] + z[7];
z[7] = abb[31] * z[147];
z[4] = -z[4] + z[7];
z[4] = m1_set::bc<T>[0] * z[4];
z[7] = abb[40] * z[36];
z[4] = z[4] + -z[7];
z[2] = z[2] * z[4];
z[7] = abb[41] * z[147];
z[4] = z[4] + z[7];
z[3] = z[3] * z[4];
z[4] = abb[29] + abb[33];
z[4] = z[4] * z[57];
z[4] = z[4] + -z[64] + -z[71];
z[4] = m1_set::bc<T>[0] * z[4];
z[7] = abb[40] * z[57];
z[4] = z[4] + z[7];
z[4] = abb[21] * z[4];
z[7] = -abb[7] + -z[45];
z[7] = abb[34] * z[7];
z[8] = 2 * abb[1] + z[31];
z[8] = abb[31] * z[8];
z[7] = z[7] + z[8];
z[7] = m1_set::bc<T>[0] * z[7];
z[8] = abb[41] * z[50];
z[7] = z[7] + z[8];
z[7] = z[7] * z[29];
z[1] = z[1] + z[2] + z[3] + (T(3) / T(2)) * z[4] + 3 * z[5] + z[6] + z[7];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_58_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-29.8297873261948356717858391913509514610886283505734617708341465"),stof<T>("1.050037242795016369076280666230968880315617794176350129814800076")}, std::complex<T>{stof<T>("36.836233041906024713955290955048932483087550049266093550803652239"),stof<T>("20.349170209509904284667372000873445004062220068500555756908990083")}, stof<T>("16.478415522602809253409778172429612904902144124051238655796702647"), std::complex<T>{stof<T>("29.011153462054467373588942483155147808939971796526895510775313198"),stof<T>("-29.711833090859618217746234866222348928064147732803406611432187001")}, std::complex<T>{stof<T>("-9.658052796969135869663357833310842258185453742758277665178790003"),stof<T>("16.603258485636910763449039141803475630117466217398960659116609347")}, std::complex<T>{stof<T>("-7.4726942728635207479164982361511508952916169540190790684405443109"),stof<T>("7.238020470109964209694876473075531399099601493833223519274473181")}, std::complex<T>{stof<T>("-17.38459811085762477109336431237468773211553723447949018049910918"),stof<T>("3.070040846087211751091846688083264586948990898070299231333924912")}, std::complex<T>{stof<T>("8.241799527417118350464387391414566202804435174568191864919765951"),stof<T>("21.744498715314835058120721651508119181333855899593708927511296725")}, std::complex<T>{stof<T>("13.929035040845007189688763917368831116427619455163020651860680958"),stof<T>("12.728093956999899223519132577884604770096256634731995620806293824")}, std::complex<T>{stof<T>("2.754494927950699312986032411753586947337636553396743452879642238"),stof<T>("77.51569170074955043869078766775348800556322141471276923536134526")}, std::complex<T>{stof<T>("-15.639725824704900363543386068086837941856187743739759434226724311"),stof<T>("-26.238591946164474130431078670427989972227183207816396208172040982")}, std::complex<T>{stof<T>("1.107030853033549347562179671205675854551103932387966687007657496"),stof<T>("-29.580462627378923262853323453281354234575591471931257053965937591")}, std::complex<T>{stof<T>("65.072213567404070089223413044267425137216199664702822945631706382"),stof<T>("-54.765239769469693711824787285140178897375095162536470359248684512")}, std::complex<T>{stof<T>("-22.10104043583227111528832415324611875492821543257524957478689661"),stof<T>("16.863473797746558280556099302156542937119281019876266965924499258")}, std::complex<T>{stof<T>("-5.0515349974214529034072409504904025648548018028124810677421291576"),stof<T>("1.0066757481242528116490408543916952024355645785536052421403761367")}, std::complex<T>{stof<T>("-1.4853408623903941775700023282203034067366770014683110969421267106"),stof<T>("-2.5547511592843874835054786769005226002611227079059833853658668355")}, std::complex<T>{stof<T>("38.162658122975090905833493810102835172170037353845123221464291653"),stof<T>("-15.647282074402721483579294507257428545894794410168039337949283315")}, std::complex<T>{stof<T>("-4.508486263436370352178905277044479972582659063860789597164506018"),stof<T>("-16.998743050228066886647040890658569908971995091117554097951430544")}, std::complex<T>{stof<T>("-4.508486263436370352178905277044479972582659063860789597164506018"),stof<T>("-16.998743050228066886647040890658569908971995091117554097951430544")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("3.9008635571742125011019775686967458012138538309289797447447215239"),stof<T>("-4.7885208668043387237614190663407589643800057830636433730180156175")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[69].real()/kbase.W[69].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[123].real()/kbase.W[123].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_58_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_58_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("20.630445372809358650596400540353151430230760890169178571096455994"),stof<T>("-17.521097613372472698357518540994381750144579744291939969497759165")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,68> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W21(k,dl), dlog_W22(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W70(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[69].real()/k.W[69].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[123].real()/k.W[123].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k)};

                    
            return f_4_58_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_58_DLogXconstant_part(base_point<T>, kend);
	value += f_4_58_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_58_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_58_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_58_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_58_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_58_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_58_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
