/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_604.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_604_abbreviated (const std::array<T,64>& abb) {
T z[116];
z[0] = abb[32] * (T(1) / T(2));
z[1] = abb[22] + abb[24];
z[2] = abb[25] + z[1];
z[3] = z[0] * z[2];
z[4] = abb[26] * abb[33];
z[5] = abb[26] + z[2];
z[6] = abb[34] * z[5];
z[7] = abb[26] + abb[27];
z[7] = abb[30] * z[7];
z[3] = z[3] + z[4] + -z[6] + -z[7];
z[3] = abb[32] * z[3];
z[4] = abb[33] * z[5];
z[5] = abb[23] * abb[33];
z[4] = z[4] + z[5] + -z[7];
z[8] = -abb[27] + z[2];
z[0] = z[0] * z[8];
z[8] = abb[23] + z[8];
z[9] = abb[31] * z[8];
z[0] = -z[0] + -z[4] + z[6] + (T(1) / T(2)) * z[9];
z[0] = abb[31] * z[0];
z[10] = abb[25] * (T(1) / T(2));
z[11] = abb[26] + abb[27] * (T(1) / T(2)) + z[10];
z[12] = abb[23] * (T(1) / T(2)) + z[11];
z[12] = abb[30] * z[12];
z[13] = abb[25] + abb[26];
z[14] = abb[33] * z[13];
z[5] = -z[5] + z[12] + -z[14];
z[5] = abb[30] * z[5];
z[12] = abb[26] + z[1];
z[10] = z[10] + z[12];
z[10] = abb[34] * z[10];
z[14] = abb[33] * z[12];
z[10] = z[10] + -z[14];
z[10] = abb[34] * z[10];
z[14] = prod_pow(abb[33], 2);
z[15] = z[1] * z[14];
z[16] = abb[28] * abb[39];
z[15] = -z[15] + z[16];
z[16] = prod_pow(m1_set::bc<T>[0], 2);
z[17] = z[8] * z[16];
z[18] = abb[23] + z[13];
z[18] = abb[37] * z[18];
z[19] = abb[38] * z[1];
z[2] = abb[36] * z[2];
z[20] = abb[62] * z[12];
z[13] = abb[35] * z[13];
z[0] = z[0] + z[2] + z[3] + -z[5] + z[10] + z[13] + (T(-1) / T(2)) * z[15] + (T(-1) / T(6)) * z[17] + -z[18] + z[19] + -z[20];
z[2] = abb[58] + abb[59];
z[3] = -z[0] * z[2];
z[5] = 5 * abb[54];
z[10] = 2 * abb[46] + z[5];
z[13] = abb[48] * (T(1) / T(2));
z[15] = z[10] + -z[13];
z[17] = abb[49] * (T(1) / T(2));
z[18] = -abb[50] + z[17];
z[19] = abb[55] * (T(1) / T(2));
z[20] = abb[52] * (T(2) / T(3));
z[15] = abb[47] * (T(-17) / T(6)) + (T(1) / T(3)) * z[15] + z[18] + -z[19] + -z[20];
z[15] = abb[4] * z[15];
z[13] = 7 * abb[46] + -8 * abb[54] + z[13] + -z[18];
z[18] = abb[47] * (T(1) / T(2));
z[21] = 3 * abb[45];
z[13] = abb[52] + abb[53] * (T(-1) / T(6)) + (T(1) / T(3)) * z[13] + z[18] + -z[21];
z[13] = abb[3] * z[13];
z[22] = 2 * abb[54];
z[23] = -abb[46] + abb[48];
z[24] = z[22] + -z[23];
z[25] = 2 * abb[47];
z[26] = -abb[55] + z[25];
z[27] = z[24] + z[26];
z[28] = abb[14] * z[27];
z[29] = abb[45] + -abb[55];
z[30] = abb[54] + z[29];
z[31] = abb[10] * z[30];
z[32] = -z[28] + z[31];
z[33] = -abb[3] + abb[8];
z[34] = abb[0] + z[33];
z[35] = abb[44] * z[34];
z[36] = abb[0] * z[29];
z[35] = z[35] + z[36];
z[37] = abb[49] + abb[50];
z[38] = -z[23] + z[37];
z[39] = -abb[53] + (T(1) / T(2)) * z[38];
z[40] = abb[17] * z[39];
z[41] = -abb[50] + abb[53] + abb[55] + abb[47] * (T(-7) / T(3));
z[20] = abb[45] * (T(-5) / T(3)) + z[20] + (T(1) / T(2)) * z[41];
z[20] = abb[2] * z[20];
z[41] = abb[46] + abb[53];
z[41] = abb[47] * (T(-7) / T(6)) + abb[48] * (T(-5) / T(2)) + abb[45] * (T(-1) / T(2)) + abb[54] * (T(8) / T(3)) + abb[51] * (T(13) / T(6)) + (T(-1) / T(6)) * z[37] + (T(1) / T(3)) * z[41];
z[41] = abb[8] * z[41];
z[42] = -abb[49] + abb[50];
z[43] = 5 * abb[46] + abb[48];
z[44] = z[42] + -z[43];
z[45] = -z[25] + (T(-1) / T(2)) * z[44];
z[46] = -abb[52] + (T(1) / T(3)) * z[45];
z[46] = abb[6] * z[46];
z[47] = -abb[49] + abb[53];
z[48] = abb[47] + z[47];
z[49] = abb[45] + -abb[46];
z[48] = (T(4) / T(3)) * z[48] + (T(7) / T(2)) * z[49];
z[48] = abb[13] * z[48];
z[19] = -abb[54] + abb[46] * (T(1) / T(2)) + -z[19];
z[18] = abb[51] * (T(-1) / T(6)) + abb[44] * (T(2) / T(3)) + z[18] + (T(1) / T(3)) * z[19];
z[18] = abb[1] * z[18];
z[13] = z[13] + z[15] + 13 * z[18] + z[20] + (T(8) / T(3)) * z[32] + (T(-13) / T(6)) * z[35] + (T(1) / T(3)) * z[40] + z[41] + z[46] + z[48];
z[13] = z[13] * z[16];
z[11] = (T(1) / T(2)) * z[1] + z[11];
z[15] = abb[31] + -abb[32];
z[11] = -z[11] * z[15];
z[1] = abb[33] * z[1];
z[1] = z[1] + -z[6] + z[7] + z[11];
z[6] = abb[25] + -abb[27];
z[6] = (T(1) / T(2)) * z[6];
z[7] = abb[29] * z[6];
z[7] = z[1] + z[7];
z[7] = abb[29] * z[7];
z[0] = z[0] + z[7];
z[7] = -abb[56] * z[0];
z[11] = abb[27] + z[12];
z[16] = -abb[60] * z[11];
z[18] = abb[61] * z[8];
z[0] = -z[0] + z[16] + z[18];
z[0] = abb[57] * z[0];
z[16] = 2 * abb[55];
z[18] = z[16] + -z[24];
z[19] = 2 * abb[52];
z[20] = z[19] + z[42];
z[32] = 4 * abb[47];
z[41] = z[18] + -z[20] + -z[32];
z[46] = abb[4] * z[41];
z[48] = z[20] + -z[24];
z[50] = abb[16] * z[48];
z[51] = z[46] + z[50];
z[52] = (T(1) / T(2)) * z[23];
z[53] = z[17] + z[52];
z[54] = 3 * abb[54];
z[55] = abb[50] * (T(5) / T(2)) + -z[25] + z[53] + -z[54];
z[55] = abb[5] * z[55];
z[56] = abb[50] * (T(1) / T(2));
z[53] = z[53] + -z[56];
z[57] = 3 * abb[52];
z[58] = 3 * abb[55] + z[53] + -z[54] + -z[57];
z[58] = abb[7] * z[58];
z[59] = -abb[46] + z[16] + -z[22] + z[42];
z[60] = 2 * abb[51];
z[61] = 3 * abb[48] + z[59] + -z[60];
z[61] = abb[8] * z[61];
z[62] = abb[46] + abb[48];
z[63] = z[42] + z[62];
z[64] = abb[15] * z[63];
z[65] = -abb[3] + abb[15];
z[66] = abb[51] * z[65];
z[66] = (T(1) / T(2)) * z[64] + -z[66];
z[10] = -abb[48] + z[10] + -z[16];
z[10] = abb[3] * z[10];
z[67] = abb[52] + -abb[55];
z[68] = abb[9] * z[67];
z[69] = 2 * abb[8];
z[70] = abb[3] + z[69];
z[71] = abb[9] + -z[70];
z[71] = abb[44] * z[71];
z[10] = z[10] + 5 * z[28] + z[51] + z[55] + z[58] + z[61] + -z[66] + z[68] + z[71];
z[10] = abb[34] * z[10];
z[61] = z[16] + -z[42];
z[71] = 8 * abb[47];
z[72] = 6 * abb[52];
z[73] = -z[22] + 3 * z[23] + -z[61] + -z[71] + z[72];
z[73] = abb[4] * z[73];
z[74] = abb[49] + 5 * abb[50];
z[75] = -z[16] + -z[22] + -z[23] + z[74];
z[75] = abb[8] * z[75];
z[76] = 6 * abb[54];
z[77] = -z[23] + z[76];
z[74] = -z[32] + z[74] + -z[77];
z[78] = abb[5] * z[74];
z[79] = z[42] + z[72];
z[80] = -6 * abb[55] + z[77] + z[79];
z[80] = abb[7] * z[80];
z[81] = z[78] + -z[80];
z[82] = z[23] + z[42];
z[83] = 4 * abb[55] + z[32];
z[84] = -z[22] + -z[72] + -z[82] + z[83];
z[84] = abb[16] * z[84];
z[85] = 2 * abb[3];
z[27] = z[27] * z[85];
z[27] = -z[27] + -4 * z[28] + z[73] + z[75] + -z[81] + z[84];
z[73] = abb[33] * z[27];
z[10] = z[10] + z[73];
z[10] = abb[34] * z[10];
z[73] = 2 * abb[45];
z[75] = -z[16] + z[19] + z[73];
z[86] = 3 * abb[49];
z[87] = 2 * abb[53];
z[88] = -abb[50] + -z[23] + -z[75] + z[86] + -z[87];
z[88] = abb[3] * z[88];
z[89] = 3 * abb[46];
z[90] = -z[21] + z[89];
z[91] = 2 * abb[49] + -z[25] + -z[87] + z[90];
z[91] = abb[13] * z[91];
z[45] = z[45] + -z[57];
z[45] = abb[6] * z[45];
z[45] = z[45] + -z[91];
z[92] = 10 * abb[47] + -abb[55];
z[93] = -abb[50] + abb[53];
z[94] = -11 * abb[45] + 9 * abb[52] + -z[92] + z[93];
z[94] = abb[2] * z[94];
z[95] = 5 * abb[52];
z[96] = -abb[45] + -abb[49] + 2 * abb[50] + -abb[53] + -z[16] + z[95];
z[96] = abb[8] * z[96];
z[88] = z[31] + -z[40] + z[45] + z[46] + -z[50] + z[58] + z[88] + z[94] + z[96];
z[88] = abb[30] * z[88];
z[94] = 3 * abb[50];
z[71] = z[16] + z[19] + z[71] + -z[77] + -z[86] + z[94];
z[71] = abb[4] * z[71];
z[77] = -z[19] + -z[76] + z[82] + z[83];
z[77] = abb[16] * z[77];
z[77] = z[77] + z[80];
z[43] = -z[19] + z[43] + -z[61];
z[43] = abb[3] * z[43];
z[26] = z[20] + z[26];
z[26] = z[26] * z[69];
z[26] = z[26] + -z[43] + z[71] + -z[77];
z[43] = -abb[33] * z[26];
z[44] = -z[32] + -z[44] + -z[72];
z[61] = abb[6] * z[44];
z[71] = abb[33] * z[61];
z[43] = z[43] + -z[71] + z[88];
z[43] = abb[30] * z[43];
z[83] = abb[55] + z[25];
z[86] = -abb[46] + abb[51];
z[88] = -abb[44] + z[54] + -z[83] + -z[86];
z[96] = 2 * abb[1];
z[97] = z[88] * z[96];
z[98] = abb[9] + z[33];
z[99] = 2 * abb[44];
z[98] = z[98] * z[99];
z[98] = 2 * z[68] + z[98];
z[100] = -z[50] + z[98];
z[65] = z[60] * z[65];
z[64] = -z[64] + z[65];
z[65] = -abb[54] + abb[55];
z[101] = -abb[46] + z[65];
z[101] = abb[3] * z[101];
z[101] = -z[28] + z[101];
z[102] = z[18] + z[42];
z[102] = abb[8] * z[102];
z[97] = -z[46] + -z[64] + -z[97] + z[100] + 2 * z[101] + -z[102];
z[97] = abb[34] * z[97];
z[75] = z[75] + z[82];
z[75] = abb[3] * z[75];
z[83] = abb[45] + -z[57] + z[83] + z[93];
z[101] = 2 * abb[2];
z[83] = z[83] * z[101];
z[38] = z[38] + -z[87];
z[102] = abb[17] * z[38];
z[103] = 2 * z[31];
z[104] = z[102] + z[103];
z[105] = -z[46] + z[104];
z[29] = -abb[52] + -z[29] + z[93];
z[106] = z[29] * z[69];
z[75] = z[50] + z[75] + z[83] + z[105] + z[106];
z[75] = abb[30] * z[75];
z[106] = -z[75] + z[97];
z[101] = z[29] * z[101];
z[107] = z[40] + z[101];
z[108] = -abb[44] + z[65] + z[86];
z[96] = z[96] * z[108];
z[96] = z[66] + z[96];
z[109] = abb[48] + z[37] + -z[89];
z[110] = -abb[53] + z[73];
z[109] = (T(1) / T(2)) * z[109] + z[110];
z[109] = abb[3] * z[109];
z[111] = abb[46] + abb[48] + abb[49];
z[112] = -z[94] + z[111];
z[112] = -abb[51] + -z[110] + (T(1) / T(2)) * z[112];
z[112] = abb[8] * z[112];
z[34] = z[34] * z[99];
z[113] = 2 * z[36];
z[34] = z[34] + -z[46] + z[96] + z[107] + z[109] + z[112] + z[113];
z[34] = abb[32] * z[34];
z[19] = -z[19] + -z[65] + z[89];
z[19] = abb[3] * z[19];
z[19] = z[19] + z[28];
z[89] = 3 * abb[47];
z[109] = abb[52] + -abb[54];
z[112] = z[42] + z[89] + z[109];
z[114] = 4 * abb[4];
z[112] = z[112] * z[114];
z[115] = abb[8] * z[41];
z[19] = 2 * z[19] + z[50] + -z[112] + z[115];
z[19] = abb[33] * z[19];
z[19] = z[19] + -z[71];
z[65] = -abb[52] + z[65];
z[53] = -z[25] + z[53] + z[65];
z[53] = abb[4] * z[53];
z[71] = abb[1] * z[108];
z[29] = abb[2] * z[29];
z[53] = -z[29] + z[53] + -z[71];
z[57] = -z[57] + z[73] + (T(-1) / T(2)) * z[82];
z[57] = abb[3] * z[57];
z[71] = -abb[48] + abb[49];
z[82] = abb[50] + z[71];
z[108] = -abb[46] + z[82];
z[108] = abb[45] + -abb[53] + (T(1) / T(2)) * z[108];
z[112] = abb[51] + z[108];
z[112] = abb[8] * z[112];
z[35] = -z[35] + -z[40] + -z[45] + z[53] + z[57] + z[112];
z[35] = abb[31] * z[35];
z[34] = -z[19] + z[34] + z[35] + -z[106];
z[34] = abb[31] * z[34];
z[35] = z[53] + -z[66];
z[45] = z[31] + z[68];
z[6] = -z[2] * z[6];
z[49] = -abb[3] * z[49];
z[53] = -abb[45] + z[93];
z[57] = -abb[8] * z[53];
z[66] = 2 * abb[0] + -3 * abb[9] + -z[33];
z[66] = abb[44] * z[66];
z[6] = z[6] + z[35] + -z[40] + -3 * z[45] + z[49] + z[57] + -z[58] + z[66] + z[113];
z[6] = abb[29] * z[6];
z[40] = -z[16] + z[56] + z[110] + (T(1) / T(2)) * z[111];
z[45] = -abb[51] + z[40];
z[45] = abb[8] * z[45];
z[40] = abb[3] * z[40];
z[40] = -z[40] + z[45] + z[96] + z[100] + -z[103] + -z[107];
z[15] = z[15] * z[40];
z[33] = z[33] * z[48];
z[40] = 2 * z[28];
z[45] = z[40] + -z[80];
z[48] = abb[47] + z[65];
z[49] = 4 * abb[16];
z[48] = z[48] * z[49];
z[33] = z[33] + z[45] + z[46] + -z[48];
z[46] = abb[33] * z[33];
z[1] = -z[1] * z[2];
z[1] = z[1] + z[6] + z[15] + z[46] + z[75] + z[97];
z[1] = abb[29] * z[1];
z[6] = 6 * abb[47];
z[15] = 10 * abb[46] + z[6] + -z[21] + -z[72] + z[82] + -z[87];
z[15] = abb[3] * z[15];
z[21] = z[32] + 4 * z[47] + z[90];
z[21] = abb[13] * z[21];
z[44] = abb[4] * z[44];
z[46] = abb[45] + abb[47] + -abb[52];
z[47] = 6 * abb[2];
z[46] = z[46] * z[47];
z[38] = abb[8] * z[38];
z[47] = abb[56] + z[2];
z[49] = z[8] * z[47];
z[15] = -z[15] + z[21] + -z[38] + -z[44] + z[46] + z[49] + z[61] + z[102];
z[21] = abb[61] * z[15];
z[38] = -abb[3] * z[41];
z[41] = -z[23] + z[89] + -z[109];
z[41] = z[41] * z[114];
z[44] = z[22] + -z[67] + -z[94];
z[44] = z[44] * z[69];
z[38] = z[38] + z[40] + z[41] + z[44] + z[50] + z[78];
z[38] = abb[33] * z[38];
z[41] = -z[17] + z[56];
z[44] = abb[45] + z[41] + z[52] + -z[54];
z[44] = abb[8] * z[44];
z[46] = -abb[3] * z[108];
z[49] = -abb[0] + z[70];
z[49] = abb[44] * z[49];
z[35] = z[35] + -z[36] + z[44] + z[46] + z[49] + -z[55];
z[35] = abb[32] * z[35];
z[35] = z[35] + z[38] + z[106];
z[35] = abb[32] * z[35];
z[38] = z[6] + (T(3) / T(2)) * z[42] + -z[52] + z[109];
z[38] = abb[3] * z[38];
z[42] = 11 * abb[46] + abb[48];
z[5] = 16 * abb[47] + -abb[55] + abb[50] * (T(11) / T(2)) + -z[5] + z[17] + (T(1) / T(2)) * z[42] + -z[95];
z[5] = abb[4] * z[5];
z[17] = z[6] + (T(-3) / T(2)) * z[23] + z[41] + -z[109];
z[17] = abb[8] * z[17];
z[5] = z[5] + z[17] + -z[28] + z[38] + -z[48] + z[58];
z[5] = z[5] * z[14];
z[14] = abb[62] * z[27];
z[17] = 3 * abb[44];
z[6] = abb[46] + 10 * abb[50] + z[6] + -z[17] + -z[60] + -z[71] + -z[76];
z[6] = abb[8] * z[6];
z[38] = abb[4] * z[74];
z[41] = abb[3] * z[63];
z[6] = z[6] + z[38] + z[41] + z[64] + -z[78];
z[6] = abb[36] * z[6];
z[16] = -z[16] + z[20] + z[62];
z[16] = abb[3] * z[16];
z[20] = -z[67] * z[69];
z[16] = z[16] + z[20] + z[64] + z[77] + -z[98];
z[16] = abb[35] * z[16];
z[20] = z[26] + z[61];
z[20] = abb[37] * z[20];
z[26] = z[37] + z[73] + -z[87];
z[18] = -z[18] + z[26];
z[18] = abb[8] * z[18];
z[30] = 2 * z[30];
z[30] = abb[3] * z[30];
z[18] = z[18] + -z[30] + -z[45];
z[30] = z[11] * z[47];
z[30] = z[18] + z[30] + -z[83] + z[84] + -z[104];
z[37] = -abb[60] * z[30];
z[33] = abb[38] * z[33];
z[38] = -abb[18] + -abb[19];
z[41] = abb[50] + z[111];
z[41] = -abb[51] + -abb[53] + (T(1) / T(2)) * z[41];
z[38] = z[38] * z[41];
z[41] = -abb[51] + (T(1) / T(2)) * z[63];
z[41] = abb[20] * z[41];
z[39] = -abb[21] * z[39];
z[38] = z[38] + z[39] + z[41];
z[38] = abb[39] * z[38];
z[17] = z[17] + -z[94];
z[25] = 2 * abb[48] + -z[17] + -z[25] + -z[60];
z[39] = prod_pow(abb[34], 2);
z[41] = prod_pow(abb[32], 2);
z[41] = -z[39] + z[41];
z[41] = z[25] * z[41];
z[42] = 4 * abb[51];
z[17] = 4 * abb[48] + z[17] + -z[32] + -z[42];
z[17] = abb[36] * z[17];
z[17] = z[17] + z[41];
z[17] = abb[11] * z[17];
z[41] = abb[35] * z[88];
z[44] = -11 * abb[44] + 9 * abb[54] + z[86] + -z[92];
z[39] = z[39] * z[44];
z[44] = abb[44] + abb[47] + -abb[54];
z[45] = abb[36] * z[44];
z[39] = z[39] + 2 * z[41] + -6 * z[45];
z[39] = abb[1] * z[39];
z[0] = abb[63] + z[0] + z[1] + z[3] + z[5] + z[6] + z[7] + z[10] + z[13] + z[14] + z[16] + z[17] + z[20] + z[21] + z[33] + z[34] + z[35] + z[37] + z[38] + z[39] + z[43];
z[1] = -5 * abb[48] + z[42] + -z[59];
z[1] = abb[8] * z[1];
z[3] = 5 * abb[44] + -abb[55] + z[32] + -z[54] + -z[86];
z[5] = 4 * abb[1];
z[3] = z[3] * z[5];
z[6] = -4 * abb[54] + abb[55] + z[23];
z[6] = z[6] * z[85];
z[7] = -abb[3] + abb[9];
z[7] = abb[8] + -2 * z[7];
z[7] = z[7] * z[99];
z[10] = 4 * z[68];
z[1] = z[1] + z[3] + z[6] + z[7] + -z[10] + -8 * z[28] + -z[51] + -z[81];
z[1] = abb[34] * z[1];
z[3] = z[5] * z[44];
z[5] = z[3] + z[105];
z[6] = -4 * abb[45] + -z[24] + z[79];
z[6] = abb[3] * z[6];
z[7] = z[24] + -z[26];
z[7] = abb[8] * z[7];
z[6] = z[5] + z[6] + z[7] + -z[40] + z[61] + -2 * z[91] + z[101];
z[6] = abb[31] * z[6];
z[7] = -abb[45] + abb[54] + -z[23];
z[7] = abb[3] * z[7];
z[7] = z[7] + z[28] + -z[29];
z[13] = z[22] + -z[53];
z[13] = z[13] * z[69];
z[14] = abb[8] * abb[44];
z[5] = -z[5] + 2 * z[7] + z[13] + -6 * z[14] + z[78];
z[5] = abb[32] * z[5];
z[7] = -abb[32] + abb[34];
z[7] = abb[11] * z[7] * z[25];
z[1] = z[1] + z[5] + z[6] + 2 * z[7] + z[19] + -z[75];
z[1] = m1_set::bc<T>[0] * z[1];
z[5] = abb[41] * z[15];
z[6] = -abb[0] + abb[9];
z[6] = abb[44] * z[6];
z[6] = -z[6] + z[36];
z[3] = -z[3] + -4 * z[6] + z[10] + -z[18] + 6 * z[31] + z[50] + z[101] + z[102];
z[3] = m1_set::bc<T>[0] * z[3];
z[6] = m1_set::bc<T>[0] * z[11];
z[7] = -z[2] * z[6];
z[3] = z[3] + z[7];
z[3] = abb[29] * z[3];
z[7] = abb[42] * z[27];
z[10] = -abb[40] * z[30];
z[13] = abb[27] * abb[32];
z[14] = abb[34] * z[12];
z[4] = -z[4] + z[9] + z[13] + z[14];
z[4] = m1_set::bc<T>[0] * z[4];
z[9] = abb[42] * z[12];
z[4] = z[4] + z[9];
z[2] = z[2] * z[4];
z[6] = abb[29] * z[6];
z[4] = -z[4] + z[6];
z[6] = -abb[56] * z[4];
z[9] = -abb[40] * z[11];
z[8] = abb[41] * z[8];
z[4] = -z[4] + z[8] + z[9];
z[4] = abb[57] * z[4];
z[1] = abb[43] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[10];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_604_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("65.879166082837796935572721603555096999493718859701565104530342898"),stof<T>("-62.043725838942075668588093994512772473442811046282338124687551489")}, std::complex<T>{stof<T>("11.818739223413243445745795987723437790666512983892190367890315121"),stof<T>("44.754399886010530852318458314120175237537482781549564337076155391")}, std::complex<T>{stof<T>("-13.021270994015708623865005644429316643937731182069318685197577669"),stof<T>("-40.770881982060263797409294286963742228363931147405017681445075007")}, std::complex<T>{stof<T>("-18.688491362182091527113373370845541383594268123669497648083374903"),stof<T>("-42.699832963396815448111803220809056052745503021421371719876539208")}, std::complex<T>{stof<T>("-5.0868616221304144532389323202907086744574045354542976972389888906"),stof<T>("-6.3356073919658355856693634081766835552946287252264899872358874776")}, std::complex<T>{stof<T>("-11.595246956722915450066907220024011012509770083197367548360795171"),stof<T>("-8.550799722671620200528872298323175527394160533667792478950411291")}, std::complex<T>{stof<T>("3.5233541977180355023414680173768846980898456852559759885164864596"),stof<T>("-11.5317422799914715205284351009741972532504716991494846242902983441")}, std::complex<T>{stof<T>("14.235614369537093677332968124518190850967208527247491741360852382"),stof<T>("24.811106568733956045617349238159990093643421220443962861420478247")}, std::complex<T>{stof<T>("28.047142876499479648595544777175144846893072748278675484952211386"),stof<T>("13.006971778289777983726806804056996473558405155221492303361916016")}, std::complex<T>{stof<T>("-63.301908878788412641598797524714208892981284069846555517884539182"),stof<T>("63.539658901733851567275641798393917143682286505912596354835844172")}, std::complex<T>{stof<T>("-15.249364149174099020409427900445254981112084466904179619284153667"),stof<T>("-8.439689959568424838107098532890253955337796255698246480042585109")}, std::complex<T>{stof<T>("-1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("1.5961736222681129079204730221135863214600019276878811243393385392")}, std::complex<T>{stof<T>("-1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("1.5961736222681129079204730221135863214600019276878811243393385392")}, std::complex<T>{stof<T>("-1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("1.5961736222681129079204730221135863214600019276878811243393385392")}, std::complex<T>{stof<T>("-1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("1.5961736222681129079204730221135863214600019276878811243393385392")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[30].real()/kbase.W[30].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_604_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_604_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(56)) * (v[2] + v[3]) * (56 * (-4 * m1_set::bc<T>[1] + -2 * m1_set::bc<T>[4] + m1_set::bc<T>[2] * (3 + v[0])) + -42 * v[2] + -42 * v[3] + 40 * v[4] + 56 * (m1_set::bc<T>[1] * (v[2] + v[3]) + v[5] + -2 * m1_set::bc<T>[1] * v[5]) + 7 * (4 * (-2 + m1_set::bc<T>[2]) * v[1] + 4 * m1_set::bc<T>[4] * (v[2] + v[3] + -2 * v[5]) + m1_set::bc<T>[2] * (v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5])))) / prod_pow(tend, 2);
c[1] = (-(4 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2] + 2 * m1_set::bc<T>[4]) * (v[2] + v[3])) / tend;


		return (2 * abb[47] + -abb[49] + abb[50] + 2 * abb[52] + -abb[55]) * (t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[6];
z[0] = -abb[35] + abb[38];
z[1] = abb[47] + abb[52];
z[1] = -abb[49] + abb[50] + -abb[55] + 2 * z[1];
z[0] = z[0] * z[1];
z[2] = abb[30] * z[1];
z[3] = abb[33] * z[1];
z[4] = z[2] + -z[3];
z[4] = abb[32] * z[4];
z[5] = 2 * z[1];
z[5] = abb[37] * z[5];
z[0] = z[0] + z[4] + z[5];
z[4] = abb[29] + abb[31];
z[1] = z[1] * z[4];
z[4] = 2 * z[1] + -z[3];
z[4] = abb[33] * z[4];
z[1] = -z[1] + -2 * z[3];
z[1] = 2 * z[1] + 5 * z[2];
z[1] = abb[30] * z[1];
z[0] = 2 * z[0] + z[1] + z[4];
return abb[12] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_604_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(2)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (2 * abb[47] + -abb[49] + abb[50] + 2 * abb[52] + -abb[55]) * (t * c[0] + -2 * c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = abb[47] + abb[52];
z[0] = abb[49] + -abb[50] + abb[55] + -2 * z[0];
z[1] = -abb[30] + abb[33];
return 2 * abb[12] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_604_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-91.839574159149954502048497615416143478191699154082212461626411046"),stof<T>("13.437294422386908445724416362317914335742764758470218670248136872")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,64> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W15(k,dl), dlog_W18(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W26(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_19(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_11_im(k), T{0}, rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[17].real()/k.W[17].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[30].real()/k.W[30].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_4_re(k), f_2_7_re(k), f_2_11_re(k), T{0}};
abb[43] = SpDLog_f_4_604_W_19_Im(t, path, abb);
abb[63] = SpDLog_f_4_604_W_19_Re(t, path, abb);

                    
            return f_4_604_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_604_DLogXconstant_part(base_point<T>, kend);
	value += f_4_604_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_604_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_604_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_604_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_604_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_604_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_604_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
