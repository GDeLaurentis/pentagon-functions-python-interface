/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_7.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_7_abbreviated (const std::array<T,25>& abb) {
T z[31];
z[0] = -abb[11] + 2 * abb[13];
z[0] = abb[11] * z[0];
z[1] = abb[11] + -abb[13];
z[2] = abb[14] + z[1];
z[3] = 3 * abb[12] + -4 * z[2];
z[3] = abb[12] * z[3];
z[4] = abb[14] + 2 * z[1];
z[4] = abb[14] * z[4];
z[5] = 3 * abb[15];
z[6] = prod_pow(abb[13], 2);
z[0] = 4 * abb[16] + -z[0] + z[3] + z[4] + z[5] + z[6];
z[0] = abb[1] * z[0];
z[3] = abb[11] + abb[13];
z[7] = abb[11] * (T(1) / T(2));
z[3] = z[3] * z[7];
z[8] = (T(1) / T(2)) * z[1];
z[9] = abb[14] * z[8];
z[9] = z[3] + -z[6] + z[9];
z[10] = abb[3] * z[9];
z[11] = (T(1) / T(2)) * z[10];
z[12] = prod_pow(abb[11], 2);
z[13] = prod_pow(abb[12], 2);
z[12] = abb[15] + z[12] + -z[13];
z[12] = abb[4] * z[12];
z[13] = z[11] + z[12];
z[14] = abb[12] * (T(1) / T(2));
z[15] = z[2] + -z[14];
z[15] = abb[12] * z[15];
z[15] = -abb[15] + -abb[16] + z[15];
z[16] = abb[14] * z[2];
z[17] = abb[11] * z[1];
z[18] = z[16] + z[17];
z[19] = -z[15] + (T(1) / T(2)) * z[18];
z[19] = abb[6] * z[19];
z[20] = abb[12] + -abb[14];
z[21] = -z[1] + z[20];
z[22] = 2 * abb[12];
z[23] = z[21] * z[22];
z[24] = 2 * abb[16];
z[23] = z[23] + z[24];
z[25] = -abb[11] + 3 * abb[13];
z[25] = z[7] * z[25];
z[26] = (T(5) / T(2)) * z[1];
z[27] = -abb[14] + -z[26];
z[27] = abb[14] * z[27];
z[25] = z[25] + z[27];
z[25] = -abb[15] + -z[23] + (T(1) / T(2)) * z[25];
z[25] = abb[2] * z[25];
z[27] = abb[14] * z[1];
z[28] = z[6] + z[17] + z[27];
z[28] = -z[15] + (T(1) / T(2)) * z[28];
z[28] = abb[0] * z[28];
z[29] = abb[14] * (T(-1) / T(2)) + z[1];
z[29] = abb[14] * z[29];
z[30] = abb[12] * (T(3) / T(2)) + -z[2];
z[30] = abb[12] * z[30];
z[29] = abb[16] + z[29] + z[30];
z[29] = abb[5] * z[29];
z[25] = -z[0] + -z[13] + z[19] + z[25] + z[28] + z[29];
z[25] = abb[22] * z[25];
z[28] = -abb[12] + 2 * z[2];
z[28] = abb[12] * z[28];
z[24] = -z[24] + z[28];
z[28] = 2 * abb[15];
z[18] = -z[18] + z[24] + -z[28];
z[29] = abb[6] * z[18];
z[4] = -z[4] + z[24];
z[4] = abb[5] * z[4];
z[4] = z[4] + -z[29];
z[24] = 3 * z[17];
z[16] = -z[6] + z[16] + z[24];
z[5] = z[5] + z[23];
z[16] = z[5] + (T(1) / T(2)) * z[16];
z[16] = abb[0] * z[16];
z[23] = 3 * abb[11] + -5 * abb[13];
z[23] = z[7] * z[23];
z[26] = abb[14] + -z[26];
z[26] = abb[14] * z[26];
z[23] = z[23] + z[26];
z[23] = (T(1) / T(2)) * z[23] + z[28];
z[23] = abb[2] * z[23];
z[11] = -z[4] + -z[11] + z[12] + z[16] + z[23];
z[11] = abb[19] * z[11];
z[0] = z[0] + -3 * z[19];
z[12] = 5 * abb[11] + -7 * abb[13];
z[7] = z[7] * z[12];
z[12] = abb[14] + z[8];
z[12] = abb[14] * z[12];
z[7] = z[7] + z[12];
z[7] = z[5] + (T(1) / T(2)) * z[7];
z[7] = abb[2] * z[7];
z[6] = -z[6] + z[17];
z[6] = (T(1) / T(2)) * z[6] + z[12] + -z[15];
z[6] = abb[0] * z[6];
z[2] = z[2] + z[14];
z[2] = abb[12] * z[2];
z[12] = abb[14] * (T(3) / T(2)) + z[1];
z[12] = abb[14] * z[12];
z[2] = abb[16] + -z[2] + z[12];
z[12] = abb[5] * z[2];
z[14] = abb[7] + abb[8];
z[15] = -abb[9] + (T(-1) / T(2)) * z[14];
z[15] = abb[17] * z[15];
z[6] = z[0] + z[6] + z[7] + z[12] + z[13] + (T(3) / T(2)) * z[15];
z[6] = abb[18] * z[6];
z[7] = z[24] + z[27];
z[5] = z[5] + (T(1) / T(2)) * z[7];
z[5] = abb[2] * z[5];
z[7] = abb[0] + abb[5];
z[2] = z[2] * z[7];
z[0] = z[0] + z[2] + z[5] + z[10];
z[0] = abb[21] * z[0];
z[2] = -z[17] + z[27] + -z[28];
z[2] = abb[2] * z[2];
z[5] = abb[0] * z[18];
z[10] = abb[8] + abb[9];
z[10] = abb[17] * z[10];
z[2] = z[2] + z[4] + z[5] + z[10];
z[2] = abb[20] * z[2];
z[4] = abb[19] + abb[22];
z[5] = 3 * abb[7] + -abb[8];
z[4] = z[4] * z[5];
z[5] = abb[21] * (T(3) / T(2));
z[10] = -z[5] * z[14];
z[12] = abb[2] + abb[3];
z[12] = -abb[10] + abb[0] * (T(1) / T(2)) + (T(3) / T(4)) * z[12];
z[13] = -abb[23] * z[12];
z[5] = -abb[19] + abb[22] * (T(1) / T(2)) + -z[5];
z[5] = abb[9] * z[5];
z[4] = (T(1) / T(4)) * z[4] + z[5] + z[10] + z[13];
z[4] = abb[17] * z[4];
z[5] = abb[7] * z[9];
z[8] = -abb[14] + z[8];
z[8] = abb[14] * z[8];
z[3] = z[3] + z[8];
z[3] = abb[8] * z[3];
z[3] = z[3] + z[5];
z[3] = (T(1) / T(2)) * z[3];
z[5] = abb[23] * z[3];
z[8] = -abb[17] * z[12];
z[3] = z[3] + z[8];
z[3] = abb[24] * z[3];
z[0] = z[0] + 2 * z[2] + z[3] + z[4] + z[5] + z[6] + z[11] + z[25];
z[2] = abb[1] * z[21];
z[3] = abb[5] * z[20];
z[3] = -z[2] + z[3];
z[4] = -abb[14] + z[22];
z[5] = -abb[11] + z[4];
z[5] = abb[2] * z[5];
z[6] = -abb[13] + abb[14];
z[6] = abb[0] * z[6];
z[8] = -abb[11] + abb[12];
z[8] = abb[4] * z[8];
z[8] = 2 * z[8];
z[9] = abb[3] * z[1];
z[3] = 2 * z[3] + -z[5] + -z[6] + z[8] + -z[9];
z[5] = abb[18] + -abb[22];
z[3] = z[3] * z[5];
z[5] = -abb[11] + abb[14];
z[6] = -abb[2] * z[5];
z[4] = abb[13] + -z[4];
z[4] = abb[0] * z[4];
z[4] = z[4] + z[6] + z[8] + z[9];
z[4] = abb[19] * z[4];
z[6] = -abb[2] + z[7];
z[6] = z[6] * z[20];
z[2] = -z[2] + z[6] + -z[9];
z[2] = abb[21] * z[2];
z[1] = abb[7] * z[1];
z[5] = abb[8] * z[5];
z[1] = z[1] + -z[5];
z[5] = -abb[23] + -abb[24];
z[1] = z[1] * z[5];
z[1] = z[1] + 2 * z[2] + z[3] + z[4];
z[1] = m1_set::bc<T>[0] * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_7_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-4.8300758664509391426608997301875218643117616432449799328167556211"),stof<T>("1.9232461890621132253448052434278699448465971300673805737648824541")}, std::complex<T>{stof<T>("-0.1859740342343892659891806003510186755604945581291532499184772354"),stof<T>("4.7899567889837363577216385151411567990514034932799901182524591126")}, stof<T>("11.603560379632653516777243490797562669966968487137418839181020009"), std::complex<T>{stof<T>("-10.44588202203287663506034087523528452373475132868453610248878839"),stof<T>("-2.8667105999216231323768332717132868542048063632126095444875766584")}, std::complex<T>{stof<T>("-0.9717043233653876157277220152112594706717226003237294867737543831"),stof<T>("-1.9232461890621132253448052434278699448465971300673805737648824541")}, std::complex<T>{stof<T>("0.5121113943864244331929380064625054643259069593013927369576886889"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("0.5121113943864244331929380064625054643259069593013927369576886889"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[123].real()/kbase.W[123].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_7_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_7_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("3.418195301021092614159912660648828092986374776835646673858686758"),stof<T>("18.226830022069768290601181394297538735379604688337712301888224693")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,25> abb = {dl[0], dl[1], dl[2], dl[4], dlog_W12(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real())};

                    
            return f_4_7_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_7_DLogXconstant_part(base_point<T>, kend);
	value += f_4_7_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_7_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_7_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_7_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_7_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_7_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_7_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
