/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_566.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_566_abbreviated (const std::array<T,31>& abb) {
T z[44];
z[0] = abb[21] + -abb[26];
z[1] = abb[22] + abb[27];
z[2] = -abb[20] + z[1];
z[3] = abb[23] + -abb[24];
z[2] = z[0] + (T(1) / T(2)) * z[2] + -z[3];
z[2] = abb[5] * z[2];
z[4] = abb[20] * (T(1) / T(2));
z[5] = abb[26] + z[4];
z[6] = -abb[22] + 5 * abb[27];
z[7] = z[5] + -z[6];
z[8] = abb[23] * (T(9) / T(2)) + z[7];
z[8] = abb[4] * z[8];
z[2] = z[2] + -z[8];
z[9] = abb[24] * (T(1) / T(2));
z[4] = 6 * abb[22] + -abb[23] + (T(1) / T(2)) * z[0] + z[4] + z[9];
z[4] = abb[3] * z[4];
z[10] = 3 * abb[27];
z[11] = abb[22] + z[10];
z[12] = -abb[20] + abb[25];
z[13] = abb[24] + z[12];
z[14] = abb[23] * (T(1) / T(2));
z[15] = (T(1) / T(2)) * z[11] + -z[13] + -z[14];
z[15] = abb[0] * z[15];
z[16] = 2 * abb[26];
z[17] = abb[20] + z[16];
z[18] = 2 * abb[21];
z[19] = z[17] + -z[18];
z[10] = -abb[22] + z[10];
z[20] = 3 * z[10] + -z[19];
z[21] = 7 * abb[23] + abb[24] + -z[20];
z[21] = abb[9] * z[21];
z[22] = abb[23] + -abb[27];
z[23] = abb[22] + z[22];
z[24] = abb[8] * z[23];
z[25] = 3 * z[24];
z[26] = abb[21] + z[22];
z[27] = abb[6] * z[26];
z[28] = 3 * z[1];
z[29] = abb[20] + z[28];
z[30] = abb[21] + abb[26] + -z[29];
z[30] = abb[24] + abb[25] + (T(1) / T(2)) * z[30];
z[30] = abb[2] * z[30];
z[4] = z[2] + z[4] + z[15] + z[21] + -z[25] + z[27] + z[30];
z[4] = abb[11] * z[4];
z[15] = 6 * abb[23];
z[21] = 2 * abb[24];
z[30] = -z[15] + z[20] + -z[21];
z[30] = abb[9] * z[30];
z[31] = 2 * abb[27];
z[32] = 2 * abb[23];
z[33] = abb[21] + -abb[22] + -z[31] + z[32];
z[34] = 2 * abb[6];
z[33] = z[33] * z[34];
z[30] = -z[30] + z[33];
z[33] = 2 * abb[25];
z[34] = z[32] + z[33];
z[16] = -z[16] + z[18];
z[18] = z[16] + z[29] + -z[34];
z[18] = abb[3] * z[18];
z[11] = 2 * abb[20] + -abb[23] + z[11] + -z[21] + -z[33];
z[11] = abb[0] * z[11];
z[29] = 2 * z[6] + -z[17];
z[35] = 9 * abb[23];
z[36] = -z[29] + z[35];
z[36] = abb[4] * z[36];
z[11] = z[11] + z[18] + z[30] + -z[36];
z[11] = abb[10] * z[11];
z[4] = z[4] + z[11];
z[4] = abb[11] * z[4];
z[11] = 4 * abb[21];
z[18] = -abb[22] + abb[27];
z[37] = z[11] + -z[17] + z[18];
z[37] = abb[2] * z[37];
z[20] = 8 * abb[23] + -z[20];
z[20] = abb[9] * z[20];
z[38] = -abb[22] + z[22];
z[39] = abb[3] * z[38];
z[26] = abb[0] * z[26];
z[26] = z[20] + -z[24] + -z[26] + 3 * z[27] + -z[36] + z[37] + -2 * z[39];
z[37] = abb[28] * z[26];
z[39] = z[1] + z[21];
z[40] = z[19] + z[32];
z[41] = z[39] + -z[40];
z[42] = abb[5] * z[41];
z[19] = z[19] + -z[21];
z[31] = -abb[23] + z[19] + z[31];
z[31] = abb[0] * z[31];
z[31] = z[31] + -z[36] + z[42];
z[43] = -z[0] + -z[1];
z[34] = -abb[24] + z[34] + 3 * z[43];
z[34] = abb[3] * z[34];
z[11] = abb[20] + 4 * abb[26] + -z[11] + z[33] + -z[39];
z[11] = abb[2] * z[11];
z[11] = z[11] + -z[30] + -z[31] + z[34];
z[11] = abb[11] * z[11];
z[16] = -abb[24] + abb[25] + -z[14] + -z[16] + (T(1) / T(2)) * z[18];
z[16] = abb[0] * z[16];
z[30] = abb[9] * z[3];
z[33] = abb[21] + abb[22] + -6 * abb[27] + z[15];
z[33] = abb[6] * z[33];
z[34] = -abb[20] + abb[24] + -z[0];
z[34] = -abb[23] + abb[25] + (T(1) / T(2)) * z[34];
z[34] = abb[3] * z[34];
z[39] = -abb[20] + 13 * abb[21] + -abb[22] + -15 * abb[26] + 11 * abb[27];
z[39] = abb[24] + -6 * abb[25] + (T(1) / T(2)) * z[39];
z[39] = abb[2] * z[39];
z[2] = z[2] + z[16] + -z[30] + z[33] + z[34] + z[39];
z[2] = abb[13] * z[2];
z[16] = -abb[21] + -abb[27] + z[13];
z[16] = abb[0] * z[16];
z[33] = -abb[21] + z[22];
z[33] = abb[6] * z[33];
z[34] = abb[2] * z[12];
z[16] = -z[16] + -z[30] + z[33] + -z[34];
z[16] = 2 * z[16];
z[33] = -abb[10] * z[16];
z[2] = z[2] + z[11] + z[33];
z[2] = abb[13] * z[2];
z[11] = 3 * abb[21];
z[33] = 3 * abb[20] + 5 * abb[22] + -23 * abb[27];
z[33] = abb[24] + 3 * abb[26] + -z[11] + (T(1) / T(2)) * z[33] + z[35];
z[33] = abb[9] * z[33];
z[35] = 2 * z[27] + -z[36];
z[0] = abb[24] + z[0];
z[32] = z[0] + -z[32];
z[39] = -abb[20] + z[28];
z[39] = z[32] + (T(1) / T(2)) * z[39];
z[39] = abb[3] * z[39];
z[10] = abb[20] + z[10];
z[0] = -abb[23] + -z[0] + (T(1) / T(2)) * z[10];
z[0] = abb[0] * z[0];
z[10] = -z[1] + -z[19];
z[10] = abb[2] * z[10];
z[0] = z[0] + z[10] + -z[24] + z[33] + z[35] + z[39] + z[42];
z[0] = abb[16] * z[0];
z[10] = 17 * abb[20] + 7 * abb[26];
z[6] = 7 * abb[21] + z[6] + -z[10];
z[6] = abb[25] + (T(1) / T(12)) * z[6];
z[6] = abb[3] * z[6];
z[10] = -11 * abb[21] + z[10] + -5 * z[18];
z[10] = -abb[25] + (T(1) / T(12)) * z[10];
z[10] = abb[2] * z[10];
z[18] = -abb[24] + z[18];
z[18] = abb[1] * z[18];
z[19] = abb[22] + abb[21] * (T(11) / T(6)) + (T(17) / T(6)) * z[22];
z[19] = abb[6] * z[19];
z[22] = 13 * abb[27];
z[33] = abb[22] * (T(-17) / T(3)) + z[22];
z[33] = abb[24] * (T(-13) / T(2)) + abb[21] * (T(-11) / T(3)) + (T(1) / T(2)) * z[33];
z[33] = abb[0] * z[33];
z[6] = z[6] + z[10] + (T(-13) / T(4)) * z[18] + z[19] + (T(-17) / T(6)) * z[24] + (T(1) / T(2)) * z[33];
z[6] = prod_pow(m1_set::bc<T>[0], 2) * z[6];
z[10] = abb[22] + 10 * abb[23] + z[17] + z[21] + -z[22];
z[10] = abb[9] * z[10];
z[17] = -abb[3] * z[41];
z[19] = -z[5] + z[9] + z[23];
z[19] = abb[0] * z[19];
z[17] = -z[10] + z[17] + z[19] + z[36];
z[17] = abb[14] * z[17];
z[19] = abb[3] * z[3];
z[18] = 3 * z[18] + -z[19] + z[25] + z[27];
z[19] = abb[21] + -abb[27];
z[9] = -z[9] + -z[14] + -z[19];
z[9] = abb[0] * z[9];
z[22] = abb[24] * (T(3) / T(2));
z[24] = 3 * abb[23];
z[7] = z[7] + z[22] + z[24];
z[25] = abb[5] * z[7];
z[8] = z[8] + -z[30];
z[9] = -z[8] + z[9] + z[18] + z[25];
z[9] = abb[12] * z[9];
z[25] = z[28] + -z[40];
z[25] = abb[3] * z[25];
z[23] = abb[0] * z[23];
z[20] = z[20] + -z[23] + z[25] + z[35];
z[23] = -abb[11] * z[20];
z[9] = z[9] + z[23];
z[9] = abb[12] * z[9];
z[23] = z[1] + z[32];
z[23] = abb[3] * z[23];
z[21] = -abb[20] + abb[26] * (T(-3) / T(2)) + abb[21] * (T(5) / T(2)) + -z[1] + z[21];
z[21] = abb[2] * z[21];
z[10] = z[10] + z[21] + z[23] + z[31];
z[10] = abb[15] * z[10];
z[3] = -z[3] + z[12];
z[3] = abb[3] * z[3];
z[11] = abb[22] + -4 * abb[27] + z[11];
z[21] = 4 * abb[23] + z[11];
z[21] = abb[6] * z[21];
z[3] = z[3] + z[21];
z[14] = abb[24] * (T(7) / T(2)) + z[11] + z[14];
z[14] = abb[0] * z[14];
z[8] = -z[3] + z[8] + z[14] + z[34];
z[14] = prod_pow(abb[10], 2);
z[8] = z[8] * z[14];
z[5] = -2 * z[1] + z[5] + -z[22] + z[24];
z[5] = abb[14] * z[5];
z[7] = -z[7] * z[14];
z[5] = z[5] + z[7];
z[5] = abb[5] * z[5];
z[7] = -abb[29] * z[20];
z[0] = abb[30] + z[0] + z[2] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[17] + z[37];
z[2] = -z[3] + -z[30] + z[34];
z[3] = -3 * abb[24] + -z[15] + z[29];
z[3] = abb[5] * z[3];
z[3] = z[3] + z[36];
z[4] = -abb[23] + -7 * abb[24] + -2 * z[11];
z[4] = abb[0] * z[4];
z[2] = -2 * z[2] + -z[3] + z[4];
z[2] = abb[10] * z[2];
z[1] = -z[1] + z[13];
z[1] = abb[0] * z[1];
z[4] = -abb[6] * z[38];
z[5] = abb[3] * z[12];
z[1] = z[1] + z[4] + z[5] + z[30];
z[1] = abb[11] * z[1];
z[4] = -z[18] + -z[30];
z[5] = abb[23] + abb[24] + 2 * z[19];
z[5] = abb[0] * z[5];
z[3] = z[3] + 2 * z[4] + z[5];
z[3] = abb[12] * z[3];
z[4] = abb[13] * z[16];
z[1] = 2 * z[1] + z[2] + z[3] + z[4];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[17] * z[26];
z[3] = -abb[18] * z[20];
z[1] = abb[19] + z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_566_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("9.1118220582962293160178355381234170283232829781850109379106403981"),stof<T>("9.7812732036126736341472518627969598728977377827176859217746206723")}, std::complex<T>{stof<T>("12.67262861072841065817056040558548178790433310978863582883692616"),stof<T>("-8.661091960906169612841175700383432352010509808429150692424553483")}, std::complex<T>{stof<T>("-13.390254929167338137093774949720361579389186301901077864157032259"),stof<T>("-5.741372891659826482863380604066334731981357920761442437893184854")}, std::complex<T>{stof<T>("35.174705598191978111282170893429260395616802389874724630904598817"),stof<T>("6.861554134366330504169456766479862252868585895049977667243252043")}, std::complex<T>{stof<T>("-21.784450669024639974188395943708898816227616087973646766747566558"),stof<T>("-1.120181242706504021306076162413527520887227974288535229350067189")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[31].real()/kbase.W[31].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_566_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_566_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,8> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4]) * (2 * (-8 * v[0] + -5 * v[1] + -5 * v[2] + v[3] + 5 * v[4] + -4 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5])) + m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]))) / prod_pow(tend, 2);
c[1] = ((T(-1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4]) * (-56 + -16 * m1_set::bc<T>[1] + 48 * m1_set::bc<T>[2] + -60 * v[0] + -11 * v[1] + -39 * v[2] + -5 * v[3] + 39 * v[4] + 2 * m1_set::bc<T>[2] * (20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]) + 28 * v[5] + 4 * m1_set::bc<T>[1] * (-2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5]))) / prod_pow(tend, 2);
c[2] = ((T(1) / T(16)) * (v[1] + v[2] + -v[3] + -v[4]) * (8 + 4 * v[0] + -5 * v[1] + -v[2] + 5 * v[3] + v[4] + -4 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + -4 * v[5])) / prod_pow(tend, 2);
c[3] = ((T(-1) / T(16)) * (v[1] + v[2] + -v[3] + -v[4]) * (104 + -80 * m1_set::bc<T>[1] + -24 * m1_set::bc<T>[2] + 68 * v[0] + -7 * v[1] + 45 * v[2] + 15 * v[3] + -45 * v[4] + -m1_set::bc<T>[2] * (20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]) + -20 * m1_set::bc<T>[1] * (2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + -52 * v[5])) / prod_pow(tend, 2);
c[4] = ((T(1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4]) * (96 + 80 * v[0] + -30 * v[1] + 34 * v[2] + 22 * v[3] + -34 * v[4] + m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]) + -32 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + -64 * v[5])) / prod_pow(tend, 2);
c[5] = ((1 + 2 * m1_set::bc<T>[1]) * (T(-1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;
c[6] = ((-5 + 6 * m1_set::bc<T>[2]) * (T(-1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;
c[7] = ((16 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (T(-1) / T(4)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return abb[22] * c[5] + abb[24] * c[6] + abb[23] * c[7] + abb[27] * (17 * c[5] + 16 * c[6] + 22 * c[7]) * (T(-1) / T(21)) + t * (abb[20] * c[0] + abb[21] * c[1] + abb[22] * c[2] + abb[27] * c[3] + abb[23] * c[4] + -abb[24] * (c[0] + c[3] + c[4]) + abb[26] * (10 * c[0] + 3 * c[1] + c[2] + c[3]) * (T(1) / T(3))) + abb[21] * (-23 * c[5] + 8 * c[6] + 4 * c[7]) * (T(1) / T(14)) + abb[20] * (17 * c[5] + -5 * c[6] + c[7]) * (T(1) / T(21)) + abb[26] * (47 * c[5] + 4 * (-5 * c[6] + c[7])) * (T(1) / T(42));
	}
	{
T z[7];
z[0] = abb[23] + -abb[24];
z[1] = 2 * abb[10];
z[0] = z[0] * z[1];
z[1] = 2 * abb[20];
z[2] = 3 * abb[26] + z[1];
z[3] = 5 * abb[24];
z[4] = 3 * abb[21] + abb[22] + -4 * abb[23] + abb[27] + -z[2] + z[3];
z[4] = abb[11] * z[4];
z[5] = -abb[22] + z[3];
z[2] = -2 * abb[21] + 6 * abb[23] + abb[27] * (T(-11) / T(2)) + z[2] + (T(-1) / T(2)) * z[5];
z[2] = abb[13] * z[2];
z[2] = -z[0] + z[2] + z[4];
z[2] = abb[13] * z[2];
z[4] = abb[21] + 2 * abb[24] + -abb[26] + -abb[27];
z[4] = -abb[20] + -abb[23] + 2 * z[4];
z[6] = abb[14] + -abb[16];
z[4] = z[4] * z[6];
z[0] = abb[11] * z[0];
z[1] = 3 * abb[27] + abb[21] * (T(-9) / T(2)) + abb[26] * (T(7) / T(2)) + z[1] + -z[5];
z[1] = abb[15] * z[1];
z[3] = -3 * abb[22] + 9 * abb[27] + -z[3];
z[3] = -abb[21] + -2 * abb[23] + (T(1) / T(2)) * z[3];
z[3] = prod_pow(abb[11], 2) * z[3];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4];
return abb[7] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_566_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (abb[23] + -abb[24]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[23] + -abb[24];
z[1] = -abb[11] + abb[13];
return 2 * abb[7] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_566_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = T(0);
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,31> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dlog_W8(k,dl), dl[3], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W32(k,dl), f_1_1(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[13].real()/k.W[13].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[31].real()/k.W[31].real()), f_2_2_re(k), f_2_10_re(k), T{0}};
abb[19] = SpDLog_f_4_566_W_20_Im(t, path, abb);
abb[30] = SpDLog_f_4_566_W_20_Re(t, path, abb);

                    
            return f_4_566_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_566_DLogXconstant_part(base_point<T>, kend);
	value += f_4_566_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_566_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_566_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_566_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_566_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_566_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_566_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
