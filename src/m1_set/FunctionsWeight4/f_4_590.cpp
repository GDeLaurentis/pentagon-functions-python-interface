/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_590.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_590_abbreviated (const std::array<T,69>& abb) {
T z[134];
z[0] = abb[48] * (T(1) / T(4));
z[1] = abb[54] * (T(1) / T(4));
z[2] = z[0] + z[1];
z[3] = abb[52] * (T(1) / T(2));
z[4] = abb[51] * (T(1) / T(2));
z[5] = z[3] + z[4];
z[6] = abb[49] + -abb[55];
z[7] = -abb[50] + z[6];
z[8] = 2 * abb[58];
z[9] = -abb[56] + z[8];
z[10] = -abb[57] + abb[46] * (T(5) / T(4)) + z[2] + z[5] + z[7] + z[9];
z[10] = abb[3] * z[10];
z[11] = abb[48] + -abb[54];
z[12] = -abb[46] + z[11];
z[13] = 2 * abb[47];
z[14] = -abb[52] + z[13];
z[15] = abb[49] + abb[55];
z[16] = -abb[50] + abb[56];
z[17] = abb[51] + z[16];
z[18] = abb[57] + -z[12] + -z[14] + -z[15] + -z[17];
z[18] = abb[8] * z[18];
z[19] = 2 * abb[53];
z[20] = 2 * abb[48];
z[21] = -abb[54] + z[20];
z[22] = -z[14] + z[19] + -z[21];
z[23] = 2 * abb[49];
z[24] = -abb[56] + z[23];
z[25] = -abb[51] + -z[22] + 2 * z[24];
z[26] = abb[9] * z[25];
z[27] = -z[6] + z[16];
z[28] = 2 * abb[46];
z[29] = -z[27] + z[28];
z[30] = abb[17] * z[29];
z[31] = abb[61] + abb[62] + abb[63] + abb[64];
z[32] = abb[28] * z[31];
z[33] = -z[30] + z[32];
z[34] = abb[49] + -abb[58];
z[35] = -abb[53] + abb[55] + -z[17] + z[34];
z[36] = abb[16] * z[35];
z[37] = abb[27] * z[31];
z[38] = 2 * z[37];
z[39] = 2 * z[36] + z[38];
z[40] = z[33] + z[39];
z[41] = abb[46] * (T(3) / T(4));
z[1] = -z[1] + z[41];
z[42] = z[1] + z[5];
z[43] = 2 * abb[55];
z[44] = abb[48] * (T(7) / T(4)) + z[13] + z[42] + -z[43];
z[44] = abb[0] * z[44];
z[45] = abb[56] + -abb[58];
z[46] = -abb[52] + z[45];
z[47] = abb[47] + -abb[57];
z[48] = z[46] + -z[47];
z[49] = 2 * abb[1];
z[50] = z[48] * z[49];
z[51] = abb[59] + abb[60];
z[52] = abb[20] * z[51];
z[53] = (T(3) / T(4)) * z[52];
z[54] = abb[19] * z[51];
z[55] = (T(1) / T(2)) * z[54];
z[56] = abb[53] + -abb[56];
z[57] = abb[46] + z[56];
z[58] = abb[2] * z[57];
z[59] = 2 * z[58];
z[10] = z[10] + z[18] + -z[26] + z[40] + z[44] + -z[50] + z[53] + z[55] + z[59];
z[10] = abb[33] * z[10];
z[18] = 4 * abb[58];
z[44] = abb[51] * (T(3) / T(2));
z[60] = abb[46] * (T(1) / T(4));
z[61] = 4 * abb[47];
z[62] = 3 * abb[56];
z[7] = -abb[57] + abb[54] * (T(-5) / T(4)) + abb[48] * (T(11) / T(4)) + -z[3] + -z[7] + -z[18] + z[44] + -z[60] + z[61] + z[62];
z[7] = abb[3] * z[7];
z[63] = 2 * abb[57];
z[64] = -abb[52] + z[63];
z[65] = abb[51] + z[21];
z[66] = -z[19] + -z[61] + z[64] + -z[65];
z[67] = 3 * abb[58];
z[68] = -z[43] + z[67];
z[69] = -abb[50] + z[68];
z[24] = z[24] + z[69];
z[24] = 2 * z[24] + z[66];
z[24] = abb[9] * z[24];
z[70] = abb[55] + -abb[58];
z[42] = -z[0] + z[42] + -2 * z[70];
z[42] = abb[0] * z[42];
z[70] = z[14] + z[65];
z[71] = 2 * z[45] + z[70];
z[72] = z[49] * z[71];
z[73] = z[17] + -z[43];
z[74] = z[8] + z[73];
z[74] = abb[18] * z[74];
z[75] = 2 * z[74];
z[72] = z[72] + -z[75];
z[76] = abb[50] + -abb[58];
z[77] = abb[46] + z[76];
z[77] = abb[14] * z[77];
z[78] = z[59] + 2 * z[77];
z[79] = z[72] + -z[78];
z[80] = abb[49] + -abb[52] + -3 * abb[55] + abb[57] + z[8] + z[17];
z[80] = abb[8] * z[80];
z[7] = z[7] + z[24] + -z[40] + -z[42] + (T(1) / T(4)) * z[52] + z[55] + z[79] + z[80];
z[7] = abb[32] * z[7];
z[80] = abb[54] * (T(1) / T(2));
z[81] = -abb[48] + z[80];
z[82] = z[69] + z[81];
z[83] = 3 * abb[47];
z[84] = -z[63] + z[83];
z[85] = 3 * abb[53];
z[86] = 5 * abb[49] + -z[85];
z[87] = abb[52] * (T(3) / T(2));
z[88] = 2 * abb[56];
z[44] = z[44] + -z[82] + z[84] + -z[86] + z[87] + z[88];
z[44] = abb[9] * z[44];
z[89] = abb[57] + z[4];
z[90] = -abb[58] + z[43];
z[91] = -abb[46] + -z[23] + z[80] + z[87] + -z[89] + z[90];
z[91] = abb[8] * z[91];
z[92] = abb[1] * z[71];
z[93] = abb[58] + z[6];
z[94] = -abb[53] + z[93];
z[95] = abb[5] * z[94];
z[96] = -z[74] + z[95];
z[92] = z[33] + z[58] + z[92] + z[96];
z[97] = abb[50] + abb[55];
z[98] = z[62] + -z[97];
z[99] = 2 * abb[51];
z[100] = -4 * abb[49] + z[85] + z[98] + z[99];
z[101] = abb[6] * z[100];
z[102] = abb[24] * z[31];
z[101] = z[101] + -z[102];
z[102] = -abb[57] + z[13];
z[103] = -abb[46] + -z[45] + -z[65] + -z[102];
z[103] = abb[3] * z[103];
z[104] = -abb[46] + z[62];
z[105] = 2 * abb[52];
z[106] = -abb[47] + z[105];
z[107] = -abb[48] + -abb[51] + z[43] + -z[104] + z[106];
z[107] = abb[0] * z[107];
z[108] = -abb[49] + abb[56];
z[109] = -abb[46] + z[108];
z[109] = abb[10] * z[109];
z[110] = 4 * z[109];
z[44] = z[44] + z[91] + -z[92] + -z[101] + z[103] + z[107] + z[110];
z[44] = abb[31] * z[44];
z[71] = abb[3] * z[71];
z[91] = z[36] + z[37];
z[71] = z[71] + -z[91] + z[96];
z[103] = abb[57] + 3 * z[45] + z[65] + -z[106];
z[106] = abb[1] * z[103];
z[107] = z[71] + z[106];
z[111] = -abb[50] + z[93];
z[111] = z[70] + 2 * z[111];
z[111] = abb[8] * z[111];
z[24] = z[24] + 2 * z[107] + z[111];
z[107] = abb[35] * z[24];
z[7] = z[7] + z[10] + z[44] + z[107];
z[7] = abb[31] * z[7];
z[10] = 3 * abb[51] + -abb[58];
z[44] = 3 * abb[46];
z[107] = -z[10] + z[43] + -z[44] + -z[62];
z[107] = abb[0] * z[107];
z[111] = 5 * abb[53];
z[112] = abb[50] + -abb[51] + z[23] + -z[44] + z[88] + -z[111];
z[112] = abb[8] * z[112];
z[69] = z[69] + -z[108];
z[113] = abb[9] * z[69];
z[113] = -z[101] + z[113];
z[114] = abb[26] * z[31];
z[114] = z[113] + z[114];
z[115] = abb[51] + -abb[55];
z[116] = -abb[49] + -abb[56] + -3 * z[76] + z[85] + z[115];
z[117] = abb[7] * z[116];
z[118] = 2 * z[95];
z[119] = abb[3] * z[94];
z[40] = -z[40] + -9 * z[58] + z[77] + z[107] + -z[110] + z[112] + -z[114] + z[117] + -z[118] + -2 * z[119];
z[40] = abb[36] * z[40];
z[107] = 3 * z[58];
z[110] = z[33] + z[107];
z[112] = -abb[51] + z[90];
z[120] = -abb[56] + z[112];
z[121] = -abb[46] + z[120];
z[122] = abb[0] * z[121];
z[122] = -z[77] + -z[110] + z[122];
z[123] = z[91] + z[119];
z[57] = abb[8] * z[57];
z[57] = z[57] + z[95] + -z[122] + z[123];
z[95] = abb[32] + -abb[33];
z[124] = abb[31] + z[95];
z[124] = z[57] * z[124];
z[40] = z[40] + 2 * z[124];
z[40] = abb[36] * z[40];
z[16] = z[6] + z[16];
z[87] = -z[16] + z[87];
z[1] = z[1] + z[87];
z[1] = abb[20] * z[1];
z[124] = -abb[8] + abb[30] + abb[3] * (T(-3) / T(4)) + abb[0] * (T(-1) / T(4));
z[51] = z[51] * z[124];
z[124] = z[80] + z[87];
z[124] = abb[21] * z[124];
z[125] = abb[20] + abb[21];
z[126] = -z[4] * z[125];
z[127] = abb[50] + -abb[55] + z[108];
z[128] = abb[46] * (T(1) / T(2));
z[129] = z[127] + -z[128];
z[130] = -z[80] + z[129];
z[130] = abb[19] * z[130];
z[131] = -abb[22] * z[29];
z[132] = abb[29] * z[31];
z[125] = abb[19] + -z[125];
z[125] = abb[57] * z[125];
z[133] = -abb[21] + abb[19] * (T(-1) / T(2)) + abb[20] * (T(-1) / T(4));
z[133] = abb[48] * z[133];
z[1] = z[1] + z[51] + z[124] + z[125] + z[126] + z[130] + z[131] + z[132] + z[133];
z[1] = abb[41] * z[1];
z[51] = abb[54] * (T(3) / T(4));
z[124] = abb[47] + abb[56];
z[0] = -z[0] + z[3] + -z[4] + z[41] + z[51] + -z[124];
z[0] = abb[0] * z[0];
z[3] = abb[56] + z[64];
z[4] = -z[3] + z[11] + z[13] + z[43];
z[4] = abb[1] * z[4];
z[41] = -z[87] + z[89];
z[60] = z[41] + z[60];
z[2] = abb[47] + -z[2] + -z[60];
z[2] = abb[3] * z[2];
z[47] = z[47] + -z[80];
z[64] = abb[48] * (T(3) / T(2));
z[87] = -z[47] + -z[64] + z[129];
z[87] = abb[8] * z[87];
z[89] = abb[47] + -abb[53];
z[125] = abb[50] + z[6] + z[89];
z[125] = abb[5] * z[125];
z[27] = -z[13] + z[27];
z[126] = abb[4] * z[27];
z[129] = abb[47] + z[11];
z[129] = abb[11] * z[129];
z[130] = 2 * z[129];
z[0] = z[0] + z[2] + z[4] + -z[53] + -z[58] + z[87] + -z[125] + -z[126] + -z[130];
z[2] = prod_pow(abb[33], 2);
z[0] = z[0] * z[2];
z[4] = 2 * abb[50];
z[20] = -abb[58] + z[4] + -z[5] + -z[20] + -z[47];
z[20] = abb[8] * z[20];
z[47] = abb[13] * z[61];
z[53] = z[47] + z[101] + z[126];
z[58] = -abb[3] * z[103];
z[8] = -abb[55] + z[8];
z[3] = -z[3] + 2 * z[8] + -z[11];
z[3] = abb[1] * z[3];
z[5] = abb[49] + -z[5] + -z[82] + z[89];
z[5] = abb[9] * z[5];
z[8] = abb[13] * z[108];
z[36] = -2 * z[8] + z[36];
z[19] = z[19] + -z[23];
z[82] = -abb[47] + -abb[50] + z[19] + z[90];
z[82] = abb[5] * z[82];
z[3] = z[3] + z[5] + z[20] + 2 * z[36] + z[38] + z[53] + z[58] + z[74] + z[82];
z[3] = abb[35] * z[3];
z[5] = -abb[58] + z[88];
z[20] = -abb[48] + z[5] + -z[84] + -z[105];
z[20] = abb[1] * z[20];
z[36] = abb[3] * z[48];
z[20] = z[20] + z[36] + -z[91] + z[125] + z[129];
z[15] = -abb[50] + z[15];
z[15] = 2 * z[15] + -z[63] + z[70];
z[15] = abb[8] * z[15];
z[15] = z[15] + 2 * z[20] + z[26];
z[15] = abb[33] * z[15];
z[20] = -abb[32] * z[24];
z[3] = z[3] + z[15] + z[20];
z[3] = abb[35] * z[3];
z[15] = z[21] + z[102] + -z[127];
z[15] = abb[8] * z[15];
z[20] = (T(-1) / T(2)) * z[52] + -z[54];
z[24] = abb[48] * (T(5) / T(4)) + -z[51] + z[60];
z[24] = abb[3] * z[24];
z[15] = z[15] + (T(1) / T(2)) * z[20] + z[24] + z[33] + z[42] + z[50] + z[78] + z[118];
z[15] = abb[33] * z[15];
z[20] = -abb[56] + z[68];
z[14] = abb[46] * (T(-3) / T(2)) + z[14] + -z[20] + z[64] + -z[80];
z[14] = abb[0] * z[14];
z[4] = z[4] + z[88];
z[24] = -abb[58] + z[4];
z[26] = -z[6] + z[85];
z[36] = z[24] + -z[26];
z[42] = z[13] + -z[28] + z[36];
z[42] = abb[3] * z[42];
z[48] = -abb[56] + z[93];
z[50] = z[48] + z[128];
z[51] = abb[48] * (T(1) / T(2)) + -z[50] + -z[80];
z[51] = abb[8] * z[51];
z[14] = z[14] + -z[42] + z[51] + -z[55] + -3 * z[77] + -z[92];
z[14] = abb[32] * z[14];
z[14] = z[14] + z[15];
z[14] = abb[32] * z[14];
z[15] = -z[13] + -z[85] + z[88] + z[93];
z[15] = abb[5] * z[15];
z[10] = 5 * abb[56] + z[10] + -z[86] + -z[97];
z[10] = abb[16] * z[10];
z[51] = z[37] + z[126];
z[55] = z[34] + z[97];
z[58] = -abb[47] + z[88];
z[60] = abb[51] + -z[55] + z[58];
z[60] = abb[8] * z[60];
z[64] = -abb[47] + z[120];
z[64] = abb[1] * z[64];
z[10] = z[10] + z[15] + -z[51] + z[60] + z[64] + -z[74] + z[113];
z[10] = abb[35] * z[10];
z[15] = abb[51] + abb[53];
z[5] = z[5] + z[15] + -z[23];
z[5] = abb[16] * z[5];
z[35] = abb[8] * z[35];
z[35] = -2 * z[5] + z[35] + -z[96] + -z[113] + -z[119];
z[60] = abb[31] * z[35];
z[80] = 3 * abb[49];
z[15] = -z[15] + -z[62] + z[67] + z[80] + -z[97];
z[15] = abb[16] * z[15];
z[62] = z[6] + z[67];
z[4] = abb[53] + -z[4] + z[62];
z[82] = abb[5] * z[4];
z[82] = -z[15] + z[82];
z[4] = abb[3] * z[4];
z[85] = z[4] + z[114];
z[17] = z[17] + z[19];
z[19] = abb[8] * z[17];
z[19] = z[19] + z[37] + z[82] + z[85] + -z[117];
z[19] = abb[36] * z[19];
z[86] = z[56] + -z[76];
z[86] = 2 * z[86];
z[87] = abb[3] + abb[5];
z[86] = z[86] * z[87];
z[90] = abb[8] * z[94];
z[86] = -z[86] + z[90] + -z[91];
z[90] = -z[74] + z[86] + z[117];
z[90] = abb[32] * z[90];
z[91] = 2 * abb[5] + abb[8];
z[56] = abb[47] + z[56];
z[56] = z[56] * z[91];
z[56] = z[56] + -z[64] + z[123] + z[126];
z[56] = abb[33] * z[56];
z[64] = abb[31] * z[31];
z[91] = abb[35] * z[31];
z[64] = z[64] + -z[91];
z[92] = abb[32] * z[31];
z[94] = z[64] + -z[92];
z[96] = abb[25] * z[94];
z[97] = -abb[26] * z[64];
z[10] = z[10] + z[19] + z[56] + z[60] + z[90] + -z[96] + z[97];
z[5] = -z[5] + z[8];
z[19] = -4 * abb[56] + z[61] + -z[93] + z[111];
z[19] = abb[5] * z[19];
z[13] = -abb[51] + z[13];
z[34] = abb[53] + z[13] + z[34] + -z[98];
z[34] = abb[8] * z[34];
z[56] = abb[25] * z[31];
z[56] = z[56] + -z[74];
z[60] = abb[47] + abb[51] + z[45];
z[60] = z[49] * z[60];
z[5] = 4 * z[5] + z[19] + z[34] + -z[47] + -z[56] + z[60] + -z[85];
z[5] = abb[34] * z[5];
z[5] = z[5] + 2 * z[10];
z[5] = abb[34] * z[5];
z[10] = -abb[53] + z[62] + -z[70] + -z[88];
z[10] = abb[3] * z[10];
z[10] = z[10] + -z[15] + z[37] + -z[101] + -z[106];
z[10] = abb[37] * z[10];
z[15] = abb[40] * z[35];
z[19] = -z[30] + z[107];
z[34] = z[19] + z[32] + z[77];
z[34] = abb[67] * z[34];
z[35] = abb[67] * z[36];
z[47] = abb[38] * z[27];
z[47] = z[35] + z[47];
z[47] = abb[5] * z[47];
z[60] = -abb[38] * z[126];
z[62] = -abb[39] + abb[65];
z[37] = z[37] * z[62];
z[10] = z[10] + z[15] + z[34] + z[37] + z[47] + z[60];
z[15] = abb[16] * z[100];
z[15] = z[15] + -z[101];
z[26] = abb[50] + -z[26] + z[58];
z[26] = abb[5] * z[26];
z[34] = z[115] + z[124];
z[34] = z[34] * z[49];
z[27] = abb[8] * z[27];
z[37] = abb[13] * abb[47];
z[26] = -z[8] + -z[15] + -z[26] + -z[27] + z[34] + z[37] + z[51];
z[27] = 2 * abb[66];
z[34] = -z[26] * z[27];
z[37] = -z[8] + z[37] + z[109];
z[47] = abb[47] * (T(1) / T(2)) + z[50];
z[47] = abb[8] * z[47];
z[33] = z[33] + -11 * z[37] + z[47] + z[56] + -z[59] + 8 * z[77];
z[37] = abb[51] * (T(1) / T(6));
z[47] = abb[47] * (T(13) / T(6));
z[50] = abb[48] * (T(13) / T(6));
z[18] = -abb[55] + z[18];
z[18] = 7 * abb[56] + abb[52] * (T(-13) / T(2)) + 2 * z[18];
z[18] = (T(1) / T(3)) * z[18] + -z[28] + z[37] + -z[47] + -z[50];
z[18] = abb[0] * z[18];
z[28] = abb[55] * (T(1) / T(3));
z[51] = abb[49] * (T(1) / T(3));
z[9] = abb[50] * (T(-5) / T(3)) + z[9] + -z[28] + z[51] + (T(-2) / T(3)) * z[89];
z[9] = abb[5] * z[9];
z[58] = abb[51] + -abb[56];
z[28] = abb[53] + -z[28] + -z[51] + (T(1) / T(3)) * z[58] + -z[76];
z[28] = abb[7] * z[28];
z[37] = abb[58] * (T(-7) / T(3)) + abb[47] * (T(9) / T(2)) + abb[52] * (T(13) / T(6)) + z[37] + z[50] + -z[88];
z[37] = abb[1] * z[37];
z[6] = z[6] + z[24];
z[6] = -abb[53] + abb[46] * (T(-13) / T(6)) + (T(1) / T(3)) * z[6] + z[47];
z[6] = abb[3] * z[6];
z[6] = z[6] + z[9] + z[18] + z[28] + (T(1) / T(3)) * z[33] + z[37];
z[9] = prod_pow(m1_set::bc<T>[0], 2);
z[6] = z[6] * z[9];
z[18] = 3 * abb[52];
z[16] = -2 * z[16] + z[18] + -z[63] + -z[65];
z[24] = -abb[37] + -abb[38];
z[24] = z[16] * z[24];
z[28] = -z[16] * z[95];
z[33] = -z[41] + z[81];
z[37] = -abb[35] * z[33];
z[28] = z[28] + z[37];
z[28] = abb[35] * z[28];
z[37] = abb[35] * z[16];
z[41] = -abb[31] + z[95];
z[41] = z[33] * z[41];
z[37] = z[37] + z[41];
z[37] = abb[31] * z[37];
z[41] = abb[32] * abb[33];
z[41] = -z[2] + z[41];
z[33] = z[33] * z[41];
z[24] = z[24] + z[28] + z[33] + z[37];
z[24] = abb[15] * z[24];
z[4] = -z[4] + -z[82] + z[101];
z[28] = 2 * abb[39];
z[4] = z[4] * z[28];
z[33] = 3 * abb[48];
z[37] = abb[54] + -z[33] + -z[84] + 2 * z[127];
z[37] = abb[38] * z[37];
z[25] = -abb[37] * z[25];
z[17] = -z[17] * z[28];
z[41] = 2 * abb[65];
z[47] = z[29] * z[41];
z[50] = abb[46] + z[48];
z[51] = abb[67] * z[50];
z[17] = z[17] + z[25] + z[37] + z[47] + 2 * z[51];
z[17] = abb[8] * z[17];
z[16] = abb[38] * z[16];
z[16] = z[16] + 2 * z[35];
z[16] = abb[3] * z[16];
z[15] = z[15] + z[109];
z[25] = -z[15] + z[110];
z[25] = z[25] * z[41];
z[23] = -z[23] + -z[66];
z[23] = abb[37] * z[23];
z[28] = -z[28] * z[69];
z[23] = z[23] + z[28];
z[23] = abb[9] * z[23];
z[28] = abb[33] * z[31];
z[28] = z[28] + -z[92];
z[35] = z[28] + -z[91];
z[37] = 2 * abb[35];
z[35] = z[35] * z[37];
z[2] = z[2] + -z[27];
z[2] = z[2] * z[31];
z[27] = -z[28] + 2 * z[91];
z[27] = abb[31] * z[27];
z[2] = -z[2] + z[27] + z[35];
z[27] = abb[38] + abb[40] + abb[65];
z[35] = -abb[67] + z[27];
z[35] = z[31] * z[35];
z[28] = abb[32] * z[28];
z[28] = -z[2] + -z[28] + 2 * z[35];
z[35] = -abb[25] * z[28];
z[27] = -abb[39] + -z[27];
z[27] = z[27] * z[31];
z[47] = abb[33] * z[92];
z[2] = z[2] + 2 * z[27] + z[47];
z[2] = abb[26] * z[2];
z[27] = abb[34] * z[31];
z[47] = -z[27] + -2 * z[94];
z[47] = abb[34] * z[47];
z[9] = z[9] * z[31];
z[9] = (T(1) / T(3)) * z[9] + -z[28] + z[47];
z[9] = abb[23] * z[9];
z[28] = abb[55] + -abb[56] + -abb[57];
z[13] = abb[54] + z[13] + z[18] + 4 * z[28];
z[13] = abb[1] * z[13];
z[13] = z[13] + -z[129];
z[13] = abb[38] * z[13];
z[18] = abb[39] + abb[67];
z[28] = -prod_pow(abb[32], 2);
z[18] = 2 * z[18] + z[28];
z[18] = abb[7] * z[18] * z[116];
z[28] = abb[46] + abb[56] + z[115];
z[41] = z[28] * z[41];
z[47] = -abb[67] * z[121];
z[41] = z[41] + z[47];
z[47] = 2 * abb[0];
z[41] = z[41] * z[47];
z[51] = -abb[67] * z[75];
z[0] = abb[68] + z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[9] + 2 * z[10] + z[13] + z[14] + z[16] + z[17] + z[18] + z[23] + z[24] + z[25] + z[34] + z[35] + z[40] + z[41] + z[51];
z[1] = z[22] + z[67] + z[73] + -z[80];
z[1] = abb[9] * z[1];
z[2] = -z[1] + z[101];
z[3] = abb[54] + z[105];
z[4] = -z[3] + z[33] + z[61];
z[5] = abb[46] + -z[4] + -4 * z[45] + -z[99];
z[5] = abb[3] * z[5];
z[6] = z[104] + -z[105] + -z[112];
z[6] = z[6] * z[47];
z[7] = 2 * z[32];
z[9] = -z[7] + 2 * z[30];
z[3] = abb[48] + z[3];
z[10] = abb[46] + -z[3] + 2 * z[55];
z[10] = abb[8] * z[10];
z[2] = 2 * z[2] + z[5] + z[6] + -z[9] + z[10] + z[39] + -z[52] + -z[54] + -z[79] + -8 * z[109];
z[2] = abb[31] * z[2];
z[5] = z[42] + z[117];
z[4] = -z[4] + 2 * z[20] + z[44];
z[4] = abb[0] * z[4];
z[6] = z[9] + -z[118];
z[9] = -z[12] + 2 * z[48];
z[9] = abb[8] * z[9];
z[4] = z[4] + 2 * z[5] + -z[6] + z[9] + z[54] + z[59] + z[72] + 6 * z[77];
z[4] = abb[32] * z[4];
z[5] = z[43] + z[45];
z[3] = -z[3] + 2 * z[5] + -z[44];
z[3] = abb[0] * z[3];
z[5] = abb[47] + abb[54] + -z[43] + -z[46];
z[5] = z[5] * z[49];
z[9] = -abb[46] + -z[11];
z[9] = abb[3] * z[9];
z[3] = z[3] + z[5] + z[6] + z[9] + z[52] + -z[78] + 2 * z[126] + z[130];
z[3] = abb[33] * z[3];
z[5] = abb[52] + -abb[56] + z[21] + -z[68] + z[83];
z[5] = abb[1] * z[5];
z[6] = abb[48] + abb[52] + -z[55];
z[6] = abb[8] * z[6];
z[1] = z[1] + z[5] + z[6] + 4 * z[8] + -z[53] + z[71] + -z[129];
z[1] = z[1] * z[37];
z[5] = abb[36] * z[57];
z[6] = z[56] + z[117];
z[8] = -z[6] + -z[86];
z[8] = abb[34] * z[8];
z[5] = z[5] + -z[8] + z[96];
z[8] = 2 * abb[26];
z[9] = -z[8] * z[64];
z[1] = z[1] + z[2] + z[3] + z[4] + -2 * z[5] + z[9];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[43] * z[26];
z[3] = z[36] * z[87];
z[4] = abb[8] * z[50];
z[5] = abb[23] * z[31];
z[3] = z[3] + z[4] + z[5] + z[6] + -z[122];
z[3] = abb[44] * z[3];
z[4] = abb[42] + -abb[43];
z[4] = z[4] * z[31];
z[5] = -z[27] + -z[94];
z[5] = m1_set::bc<T>[0] * z[5];
z[5] = -z[4] + z[5];
z[5] = abb[23] * z[5];
z[2] = z[2] + -z[3] + -z[5];
z[3] = abb[8] * z[29];
z[3] = z[3] + -z[15] + z[19];
z[5] = abb[0] * z[28];
z[3] = 2 * z[3] + 4 * z[5] + z[7] + z[38];
z[3] = abb[42] * z[3];
z[5] = -2 * abb[25] + -z[8];
z[4] = z[4] * z[5];
z[1] = abb[45] + z[1] + -2 * z[2] + z[3] + z[4];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_590_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("33.706053055515298288839095570101245313636647942428658327598171961"),stof<T>("50.961377933537765307332419140777549464780828639273489602082343258")}, std::complex<T>{stof<T>("11.858026132483910657718379006669116221305018847510705623791277394"),stof<T>("-56.879646427640951417939862551058491074130978128676838193480500277")}, std::complex<T>{stof<T>("15.906370797905423851752454953211023491375675565932195527849166268"),stof<T>("11.057996405110180876423471423951151950785385011025780364855342733")}, std::complex<T>{stof<T>("-26.838331748098920589511380876061648957629331972514306536496090407"),stof<T>("-11.156613126976248734663096518086661797552330618153893688362247169")}, std::complex<T>{stof<T>("19.951921924348071445516545983985641398493729237037885813901714986"),stof<T>("-9.195139706723453282066128734824871446144474898744586122859910221")}, std::complex<T>{stof<T>("3.99214115123710515555086421751400831887046925024364274524650727"),stof<T>("-18.091150631019225221082721300622652058801548881111807164189096765")}, std::complex<T>{stof<T>("32.551977062607529083248990298121337283831124709239946400888151237"),stof<T>("21.466638565505560250781031719302091742362819897581158019157754403")}, std::complex<T>{stof<T>("-0.2763918106676341814796327240858328421747738605893962837744060502"),stof<T>("1.9232461890621132253448052434278699448465971300673805737648824541")}, std::complex<T>{stof<T>("6.7353799965619372045429879339872034198064024037499747569895055434"),stof<T>("-1.3422343308323370821987211391889269365807527714048069895205015157")}, std::complex<T>{stof<T>("-1.721504163271503338186745204545680007331323486481997119145553561"),stof<T>("-28.401176069548536737200656501838082003612038160539071494984603999")}, stof<T>("-8.1467639364098125700497265545092086142014652425291793387481963645"), std::complex<T>{stof<T>("-38.162658122975090905833493810102835172170037353845123221464291653"),stof<T>("15.647282074402721483579294507257428545894794410168039337949283315")}, std::complex<T>{stof<T>("1.7314772302804613735427175946745870918310611219429528881459093378"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("1.7314772302804613735427175946745870918310611219429528881459093378"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("-3.1923472445362258158409460442271726429200038553757622486786770783")}, std::complex<T>{stof<T>("2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("-3.1923472445362258158409460442271726429200038553757622486786770783")}, std::complex<T>{stof<T>("2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("-3.1923472445362258158409460442271726429200038553757622486786770783")}, std::complex<T>{stof<T>("2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("-3.1923472445362258158409460442271726429200038553757622486786770783")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[36].real()/kbase.W[36].real()), rlog(k.W[76].real()/kbase.W[76].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[123].real()/kbase.W[123].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_590_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_590_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(56)) * (v[1] + v[2] + -v[3] + -v[4]) * (2 * (-112 * m1_set::bc<T>[1] + 84 * m1_set::bc<T>[2] + -56 * m1_set::bc<T>[4] + -49 * v[1] + 7 * v[2] + 21 * v[3] + 13 * v[4] + -28 * v[5]) + 7 * (4 * (2 + -4 * m1_set::bc<T>[1] + 5 * m1_set::bc<T>[2] + -2 * m1_set::bc<T>[4]) * v[0] + 8 * m1_set::bc<T>[1] * v[1] + m1_set::bc<T>[2] * v[1] + -8 * m1_set::bc<T>[1] * v[2] + 13 * m1_set::bc<T>[2] * v[2] + -8 * m1_set::bc<T>[1] * v[3] + 3 * m1_set::bc<T>[2] * v[3] + 8 * m1_set::bc<T>[1] * v[4] + -13 * m1_set::bc<T>[2] * v[4] + 16 * m1_set::bc<T>[1] * v[5] + -12 * m1_set::bc<T>[2] * v[5] + 4 * m1_set::bc<T>[4] * (v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])))) / prod_pow(tend, 2);
c[1] = (-(4 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2] + 2 * m1_set::bc<T>[4]) * (v[1] + v[2] + -1 * v[3] + -1 * v[4])) / tend;


		return (2 * abb[49] + abb[50] + -abb[51] + -2 * abb[53] + -abb[56]) * (t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[6];
z[0] = -abb[32] + abb[33];
z[1] = -2 * abb[49] + -abb[50] + abb[51] + 2 * abb[53] + abb[56];
z[1] = abb[12] * z[1];
z[0] = z[0] * z[1];
z[2] = 2 * abb[34];
z[2] = z[1] * z[2];
z[3] = abb[31] * z[1];
z[3] = -z[0] + z[2] + z[3];
z[4] = abb[36] * z[1];
z[3] = 2 * z[3] + -5 * z[4];
z[3] = abb[36] * z[3];
z[4] = abb[37] + -abb[40];
z[5] = 2 * z[1];
z[4] = z[4] * z[5];
z[5] = abb[34] * z[1];
z[0] = 2 * z[0] + z[5];
z[0] = abb[34] * z[0];
z[2] = -abb[31] * z[2];
z[1] = abb[39] * z[1];
return z[0] + -4 * z[1] + z[2] + z[3] + z[4];
}

}
template <typename T, typename TABB> T SpDLog_f_4_590_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (2 * abb[49] + abb[50] + -abb[51] + -2 * abb[53] + -abb[56]) * (t * c[0] + 2 * c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = -abb[49] + abb[53];
z[0] = -abb[50] + abb[51] + abb[56] + 2 * z[0];
z[1] = abb[34] + -abb[36];
return 2 * abb[12] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_590_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-43.837623605525059103782776619003555742200251039657810455806317352"),stof<T>("-34.859701539291110108982035290351984755759745157931530015992864135")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,69> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W18(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W25(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W77(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_14(k), f_2_20(k), f_2_23(k), f_2_2_im(k), f_2_10_im(k), f_2_12_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[36].real()/k.W[36].real()), rlog(kend.W[76].real()/k.W[76].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real()), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_2_re(k), f_2_10_re(k), f_2_12_re(k), T{0}};
abb[45] = SpDLog_f_4_590_W_20_Im(t, path, abb);
abb[68] = SpDLog_f_4_590_W_20_Re(t, path, abb);

                    
            return f_4_590_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_590_DLogXconstant_part(base_point<T>, kend);
	value += f_4_590_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_590_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_590_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_590_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_590_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_590_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_590_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
