/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_472.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_472_abbreviated (const std::array<T,62>& abb) {
T z[137];
z[0] = 2 * abb[56];
z[1] = -abb[46] + abb[47];
z[2] = abb[55] + z[1];
z[3] = z[0] + -z[2];
z[4] = 3 * abb[45];
z[5] = 3 * abb[57];
z[6] = 3 * abb[48] + -z[5];
z[7] = 3 * abb[51];
z[8] = 3 * abb[53];
z[9] = 2 * abb[54];
z[10] = -z[3] + z[4] + z[6] + -z[7] + z[8] + z[9];
z[10] = abb[10] * z[10];
z[11] = -abb[51] + abb[56];
z[12] = (T(1) / T(2)) * z[2];
z[13] = z[11] + -z[12];
z[14] = abb[16] * z[13];
z[15] = abb[46] + -abb[55];
z[16] = abb[47] + z[15];
z[17] = (T(1) / T(2)) * z[16];
z[18] = abb[43] + -abb[50];
z[19] = z[17] + z[18];
z[20] = abb[14] * z[19];
z[21] = z[14] + z[20];
z[22] = -abb[46] + abb[54];
z[23] = abb[50] + z[22];
z[24] = 3 * abb[44];
z[25] = 5 * abb[43] + -z[24];
z[26] = 2 * z[23] + -z[25];
z[26] = abb[9] * z[26];
z[27] = 2 * abb[51];
z[28] = z[9] + -z[27];
z[29] = abb[48] + abb[49];
z[30] = z[28] + -z[29];
z[31] = 2 * abb[57];
z[32] = -abb[55] + z[31];
z[33] = z[30] + z[32];
z[34] = abb[17] * z[33];
z[35] = z[26] + z[34];
z[36] = abb[43] + -abb[44];
z[37] = abb[51] + -abb[54];
z[38] = -abb[48] + z[37];
z[38] = 2 * z[38];
z[39] = 2 * abb[45];
z[40] = abb[56] + abb[57] + -z[36] + z[38] + -z[39];
z[40] = abb[7] * z[40];
z[41] = -abb[57] + z[29];
z[42] = -abb[44] + abb[50] + -z[11] + -z[15] + z[41];
z[42] = abb[4] * z[42];
z[43] = 2 * abb[44];
z[44] = 2 * abb[43];
z[45] = z[43] + -z[44];
z[28] = -abb[55] + z[28];
z[46] = abb[45] + abb[48];
z[47] = -z[28] + -z[45] + -z[46];
z[47] = abb[0] * z[47];
z[48] = -abb[55] + z[1];
z[49] = (T(1) / T(2)) * z[48];
z[50] = z[8] + z[49];
z[51] = 4 * abb[51];
z[52] = z[9] + z[50] + -z[51];
z[53] = -abb[3] * z[52];
z[54] = -abb[47] + abb[50];
z[55] = z[29] + z[54];
z[56] = -abb[44] + -abb[53] + z[55];
z[57] = abb[1] * z[56];
z[58] = -abb[57] + z[46];
z[37] = -z[37] + z[58];
z[59] = 2 * abb[2];
z[60] = -z[37] * z[59];
z[61] = abb[23] + abb[25];
z[62] = abb[26] + abb[27];
z[63] = z[61] + z[62];
z[64] = abb[22] + abb[24];
z[65] = -z[63] + -z[64];
z[66] = abb[58] * (T(1) / T(2));
z[65] = z[65] * z[66];
z[67] = -abb[57] + z[12];
z[68] = abb[53] + z[67];
z[68] = abb[8] * z[68];
z[10] = z[10] + z[21] + z[35] + z[40] + z[42] + z[47] + z[53] + z[57] + z[60] + z[65] + z[68];
z[10] = abb[29] * z[10];
z[40] = 2 * abb[49];
z[42] = 2 * abb[48];
z[47] = z[40] + z[42];
z[53] = 4 * abb[54];
z[60] = z[47] + -z[53];
z[65] = 2 * abb[53];
z[68] = z[1] + z[65];
z[32] = z[32] + -z[51] + -z[60] + z[68];
z[69] = abb[15] * z[32];
z[70] = -abb[51] + z[9];
z[71] = -z[5] + z[70];
z[72] = z[39] + z[42];
z[73] = -abb[56] + z[72];
z[74] = abb[53] + z[73];
z[75] = z[2] + z[71] + z[74];
z[76] = 2 * abb[10];
z[75] = z[75] * z[76];
z[77] = 2 * abb[17];
z[33] = z[33] * z[77];
z[75] = z[33] + -z[69] + z[75];
z[77] = 2 * abb[1];
z[78] = z[56] * z[77];
z[20] = z[20] + z[78];
z[78] = -abb[43] + -abb[50] + z[43];
z[79] = abb[47] + abb[55];
z[80] = abb[51] + z[79];
z[81] = z[78] + z[80];
z[82] = -z[31] + z[81];
z[82] = abb[0] * z[82];
z[82] = z[20] + z[82];
z[83] = 4 * abb[45];
z[84] = 4 * abb[48] + z[53];
z[85] = z[83] + z[84];
z[86] = 4 * abb[57] + z[7];
z[87] = -abb[56] + z[12] + z[85] + -z[86];
z[87] = abb[7] * z[87];
z[88] = z[27] + z[78];
z[89] = 3 * abb[47];
z[90] = -abb[46] + z[89];
z[91] = -3 * abb[55] + -z[90];
z[91] = abb[56] + z[31] + -z[88] + (T(1) / T(2)) * z[91];
z[91] = abb[4] * z[91];
z[92] = z[62] + -z[64];
z[92] = z[66] * z[92];
z[93] = z[36] + z[41];
z[93] = abb[13] * z[93];
z[94] = 2 * z[93];
z[95] = abb[2] * z[37];
z[87] = -z[14] + -z[75] + z[82] + z[87] + z[91] + z[92] + -z[94] + 4 * z[95];
z[87] = abb[33] * z[87];
z[91] = abb[53] + z[1];
z[95] = -z[46] + z[70] + z[91];
z[96] = z[76] * z[95];
z[96] = -z[69] + z[96];
z[97] = -abb[45] + abb[49];
z[98] = -abb[56] + abb[57];
z[99] = -z[97] + z[98];
z[100] = z[59] * z[99];
z[14] = z[14] + z[100];
z[100] = -abb[51] + -abb[56] + -z[12] + z[31] + -z[45];
z[100] = abb[7] * z[100];
z[90] = -abb[55] + z[90];
z[73] = -z[73] + z[88] + (T(1) / T(2)) * z[90];
z[73] = abb[4] * z[73];
z[72] = z[72] + -z[81];
z[72] = abb[0] * z[72];
z[20] = z[14] + -z[20] + z[72] + z[73] + -z[92] + z[96] + z[100];
z[20] = abb[32] * z[20];
z[72] = -z[39] + z[40];
z[73] = z[2] + -z[31];
z[81] = z[27] + z[72] + z[73];
z[88] = abb[4] * z[81];
z[90] = abb[45] + abb[49];
z[38] = -abb[56] + z[5] + z[38] + -z[90];
z[38] = z[38] * z[59];
z[3] = z[3] + -z[27];
z[92] = abb[16] * z[3];
z[38] = z[38] + z[92];
z[100] = 4 * abb[7];
z[101] = z[37] * z[100];
z[63] = abb[58] * z[63];
z[73] = z[65] + z[73];
z[102] = abb[8] * z[73];
z[63] = z[38] + -z[63] + z[75] + z[88] + -z[101] + z[102];
z[63] = abb[34] * z[63];
z[32] = abb[4] * z[32];
z[75] = 2 * abb[47];
z[88] = abb[55] + z[75];
z[101] = -z[5] + z[53] + z[88];
z[103] = 2 * abb[46] + -z[101];
z[104] = z[76] * z[103];
z[105] = -z[33] + z[104];
z[30] = z[30] + z[68];
z[68] = abb[12] * z[30];
z[106] = 2 * z[68];
z[107] = abb[7] * z[73];
z[108] = -z[106] + z[107];
z[109] = 3 * abb[54];
z[110] = -z[27] + z[109];
z[91] = -abb[57] + z[91] + z[110];
z[111] = 4 * abb[15];
z[111] = z[91] * z[111];
z[112] = 6 * abb[53];
z[48] = z[48] + z[112];
z[113] = 8 * abb[51] + -z[48] + -z[53];
z[114] = abb[3] * z[113];
z[115] = abb[58] * z[61];
z[32] = -z[32] + z[102] + -z[105] + z[108] + -z[111] + -z[114] + -z[115];
z[115] = abb[31] * z[32];
z[108] = z[69] + z[108];
z[116] = z[8] + -z[9];
z[117] = z[44] + z[116];
z[118] = -abb[44] + -z[55] + z[117];
z[118] = z[77] * z[118];
z[119] = abb[57] + z[28];
z[120] = z[36] + z[119];
z[121] = 2 * abb[0];
z[120] = z[120] * z[121];
z[122] = 2 * abb[50];
z[123] = -z[16] + z[122];
z[124] = -z[44] + z[123];
z[125] = abb[14] * z[124];
z[118] = z[118] + z[120] + z[125];
z[120] = z[94] + -z[102];
z[126] = z[61] + z[64];
z[126] = abb[58] * z[126];
z[127] = 2 * abb[4];
z[56] = z[56] * z[127];
z[56] = -z[56] + -z[108] + z[118] + z[120] + z[126];
z[56] = abb[30] * z[56];
z[10] = z[10] + z[20] + z[56] + -z[63] + z[87] + z[115];
z[10] = abb[29] * z[10];
z[20] = abb[49] * (T(3) / T(2));
z[87] = 5 * abb[54];
z[115] = 2 * abb[52];
z[86] = -abb[48] + abb[45] * (T(-9) / T(2)) + z[20] + z[86] + -z[87] + -z[88] + z[115];
z[86] = abb[2] * z[86];
z[88] = -abb[53] + z[9];
z[49] = -z[31] + z[47] + -z[49] + -z[88];
z[49] = abb[15] * z[49];
z[12] = -z[12] + -z[42] + -z[71] + -z[90];
z[12] = abb[7] * z[12];
z[5] = abb[51] + z[2] + -z[5] + z[46] + z[88];
z[5] = abb[10] * z[5];
z[71] = 3 * abb[49];
z[90] = -abb[57] + -z[39] + z[71] + z[109];
z[75] = -abb[48] + -abb[53] + z[75] + z[90] + -z[115];
z[75] = abb[8] * z[75];
z[80] = -abb[52] + z[80];
z[126] = abb[48] + abb[57] + -z[72] + -z[80];
z[128] = -abb[4] * z[126];
z[17] = abb[45] + -abb[52] + z[17];
z[17] = abb[5] * z[17];
z[129] = -abb[26] * z[66];
z[130] = abb[11] * z[97];
z[5] = z[5] + z[12] + -z[17] + z[34] + z[49] + z[75] + z[86] + z[128] + z[129] + (T(-9) / T(2)) * z[130];
z[5] = abb[34] * z[5];
z[12] = abb[47] + z[110];
z[4] = -abb[49] + z[4] + 2 * z[12] + -z[31] + -z[115];
z[4] = abb[2] * z[4];
z[12] = z[1] + z[109];
z[29] = abb[57] + z[29];
z[12] = -2 * z[12] + z[27] + z[29];
z[49] = 2 * abb[15];
z[75] = -z[12] * z[49];
z[86] = abb[52] + z[22];
z[86] = 4 * z[86];
z[109] = -abb[45] + -z[71] + z[86];
z[109] = abb[11] * z[109];
z[119] = -z[97] + z[119];
z[128] = z[119] * z[127];
z[129] = -abb[47] + abb[52];
z[130] = -abb[49] + z[129];
z[131] = -abb[54] + abb[57] + z[130];
z[131] = abb[8] * z[131];
z[4] = z[4] + z[75] + z[105] + -z[109] + z[128] + 2 * z[131];
z[4] = abb[31] * z[4];
z[4] = z[4] + z[5];
z[4] = abb[34] * z[4];
z[5] = abb[46] + z[79];
z[75] = -abb[56] + (T(1) / T(2)) * z[5];
z[78] = -z[72] + z[75] + z[78];
z[78] = abb[4] * z[78];
z[105] = abb[7] * z[13];
z[128] = z[62] + z[64];
z[131] = z[61] + (T(1) / T(2)) * z[128];
z[131] = abb[58] * z[131];
z[14] = -z[14] + z[78] + -z[82] + z[105] + z[120] + z[131];
z[14] = abb[33] * z[14];
z[16] = -z[16] + z[115];
z[78] = z[16] + -z[39];
z[82] = abb[5] * z[78];
z[105] = abb[26] * abb[58];
z[105] = -z[82] + z[105];
z[120] = abb[53] + -abb[54];
z[132] = -z[97] + z[120];
z[133] = 4 * abb[8];
z[132] = z[132] * z[133];
z[119] = z[59] * z[119];
z[134] = abb[48] + z[129];
z[135] = -abb[53] + z[134];
z[136] = -z[127] * z[135];
z[108] = -z[105] + -z[108] + z[119] + z[132] + z[136];
z[108] = abb[31] * z[108];
z[81] = abb[7] * z[81];
z[119] = -z[59] * z[126];
z[126] = -abb[51] + z[46];
z[130] = z[126] + z[130];
z[132] = z[127] * z[130];
z[135] = abb[8] * z[135];
z[96] = z[81] + -z[96] + z[105] + z[119] + z[132] + 2 * z[135];
z[96] = abb[34] * z[96];
z[97] = z[36] + z[97];
z[105] = abb[7] * z[97];
z[119] = abb[2] * z[130];
z[97] = abb[4] * z[97];
z[126] = abb[44] + -z[54] + -z[126];
z[126] = abb[0] * z[126];
z[57] = z[57] + z[97] + -z[105] + z[119] + z[126] + -z[135];
z[57] = abb[32] * z[57];
z[56] = z[14] + -z[56] + z[57] + z[96] + z[108];
z[56] = abb[32] * z[56];
z[57] = z[15] + -z[89];
z[96] = abb[52] + z[31];
z[57] = -5 * abb[49] + z[8] + (T(1) / T(2)) * z[57] + z[83] + -z[87] + z[96];
z[57] = abb[8] * z[57];
z[6] = z[6] + z[27] + -z[50] + z[71];
z[6] = abb[6] * z[6];
z[6] = z[6] + 3 * z[68];
z[50] = abb[45] * (T(1) / T(2));
z[96] = abb[54] + abb[49] * (T(1) / T(2)) + z[50] + z[79] + -z[96];
z[96] = abb[2] * z[96];
z[12] = abb[15] * z[12];
z[40] = abb[45] + abb[52] + -abb[57] + -z[15] + -z[40] + z[116];
z[40] = abb[4] * z[40];
z[97] = abb[26] + z[61];
z[108] = abb[24] + z[97];
z[108] = z[66] * z[108];
z[20] = abb[45] * (T(-11) / T(2)) + z[20] + z[86];
z[20] = abb[11] * z[20];
z[12] = z[6] + z[12] + z[17] + z[20] + z[34] + z[40] + z[57] + z[96] + z[108];
z[17] = prod_pow(abb[31], 2);
z[12] = z[12] * z[17];
z[20] = 6 * abb[57];
z[34] = z[20] + z[51] + -z[65];
z[40] = 8 * abb[54];
z[57] = 3 * abb[46] + -abb[55] + z[34] + -z[40] + -z[47] + -z[89];
z[57] = abb[15] * z[57];
z[86] = 4 * abb[49];
z[84] = z[84] + z[86];
z[96] = z[2] + -z[20] + -z[65] + z[84];
z[108] = abb[7] + abb[8];
z[96] = -z[96] * z[108];
z[116] = 6 * abb[49];
z[20] = -6 * abb[48] + z[20] + z[48] + -z[51] + -z[116];
z[20] = abb[6] * z[20];
z[48] = 4 * z[68];
z[30] = z[30] * z[127];
z[68] = abb[24] + z[61];
z[68] = abb[58] * z[68];
z[30] = -z[20] + z[30] + z[48] + -z[57] + z[68] + z[96] + z[104];
z[30] = abb[37] * z[30];
z[20] = z[20] + -z[33];
z[68] = z[100] + z[133];
z[41] = -z[41] + z[120];
z[68] = z[41] * z[68];
z[73] = abb[4] * z[73];
z[96] = abb[24] * abb[58];
z[68] = z[20] + -z[68] + z[69] + -z[73] + -z[96] + -z[106];
z[69] = abb[31] * z[68];
z[73] = abb[0] + -abb[2];
z[37] = z[37] * z[73];
z[73] = z[36] + z[120];
z[96] = abb[1] * z[73];
z[41] = -abb[8] * z[41];
z[37] = z[37] + z[41] + -z[93] + z[96] + z[105];
z[37] = abb[33] * z[37];
z[37] = 2 * z[37] + z[63] + z[69];
z[37] = abb[33] * z[37];
z[41] = -abb[55] + 5 * z[1];
z[29] = -6 * abb[51] + z[8] + -z[29] + z[40] + (T(1) / T(2)) * z[41];
z[29] = abb[15] * z[29];
z[40] = 4 * abb[43];
z[8] = 5 * abb[44] + -z[8] + -z[40] + z[53] + -z[55];
z[8] = z[8] * z[77];
z[41] = z[43] + -z[67] + -z[117];
z[41] = abb[4] * z[41];
z[63] = z[54] + -z[110];
z[67] = 3 * abb[43];
z[63] = abb[44] + z[31] + 2 * z[63] + -z[67];
z[63] = abb[0] * z[63];
z[69] = -abb[24] * z[66];
z[6] = -z[6] + z[8] + z[29] + -z[35] + z[41] + z[63] + z[69] + z[94];
z[6] = abb[30] * z[6];
z[8] = -abb[33] * z[68];
z[6] = z[6] + z[8];
z[6] = abb[30] * z[6];
z[8] = -z[2] + z[34] + -z[85];
z[8] = abb[7] * z[8];
z[1] = -abb[51] + -z[1] + -z[9] + z[74];
z[1] = z[1] * z[76];
z[9] = z[95] * z[127];
z[29] = -abb[58] * z[62];
z[1] = z[1] + z[8] + z[9] + z[29] + z[38] + -z[57] + z[106] + z[114];
z[1] = abb[35] * z[1];
z[8] = abb[39] * z[32];
z[9] = abb[15] * z[113];
z[9] = z[9] + -z[114];
z[29] = abb[47] + -abb[55];
z[32] = z[29] + -z[51] + z[87];
z[34] = abb[50] + -z[32];
z[25] = -z[25] + 2 * z[34];
z[25] = abb[0] * z[25];
z[23] = -abb[43] + 4 * z[23] + -z[24];
z[23] = abb[9] * z[23];
z[34] = abb[4] * z[124];
z[38] = -abb[58] * z[64];
z[23] = -z[9] + z[23] + z[25] + z[34] + z[38] + -6 * z[96] + -z[125];
z[23] = abb[36] * z[23];
z[25] = 6 * abb[54] + -z[16] + -z[83] + -z[112] + z[116];
z[25] = abb[8] * z[25];
z[34] = 5 * abb[45] + -z[71] + -z[115];
z[32] = -2 * z[32] + -z[34];
z[32] = abb[2] * z[32];
z[38] = abb[4] * z[78];
z[41] = abb[58] * z[97];
z[9] = -z[9] + z[25] + z[32] + z[38] + z[41] + -z[82] + z[109];
z[9] = abb[38] * z[9];
z[5] = -z[0] + z[5];
z[25] = -abb[44] + abb[49];
z[32] = abb[45] * (T(17) / T(2));
z[38] = abb[43] * (T(13) / T(2)) + -z[5] + (T(17) / T(2)) * z[25] + -z[32] + z[122];
z[38] = abb[4] * z[38];
z[25] = abb[43] + -abb[45] + z[25];
z[25] = -z[3] + (T(5) / T(2)) * z[25];
z[25] = abb[7] * z[25];
z[41] = -2 * z[61] + -z[128];
z[41] = abb[58] * z[41];
z[25] = z[25] + z[38] + z[41] + z[92] + -z[125];
z[38] = abb[57] * (T(11) / T(3));
z[41] = abb[52] * (T(3) / T(2)) + z[38];
z[57] = abb[49] * (T(17) / T(6));
z[62] = abb[54] * (T(-31) / T(3)) + abb[48] * (T(-5) / T(3)) + -z[89];
z[50] = abb[56] * (T(-4) / T(3)) + abb[51] * (T(11) / T(3)) + z[41] + z[50] + -z[57] + (T(1) / T(2)) * z[62];
z[50] = abb[2] * z[50];
z[62] = abb[48] * (T(5) / T(2)) + abb[47] * (T(13) / T(2)) + abb[54] * (T(31) / T(2));
z[15] = abb[49] * (T(31) / T(2)) + -2 * z[15] + -z[32] + z[62];
z[32] = abb[53] * (T(7) / T(3));
z[15] = (T(1) / T(3)) * z[15] + -z[32] + -z[41];
z[15] = abb[8] * z[15];
z[41] = 2 * abb[55] + z[62];
z[38] = abb[50] * (T(-13) / T(6)) + abb[43] * (T(-2) / T(3)) + abb[45] * (T(5) / T(6)) + abb[44] * (T(17) / T(6)) + -z[7] + -z[38] + (T(1) / T(3)) * z[41];
z[38] = abb[0] * z[38];
z[41] = abb[48] + z[54];
z[41] = -31 * abb[54] + 17 * z[41];
z[32] = -8 * abb[44] + abb[43] * (T(31) / T(6)) + z[32] + (T(1) / T(6)) * z[41] + z[57];
z[32] = abb[1] * z[32];
z[15] = z[15] + (T(1) / T(3)) * z[25] + z[32] + z[38] + z[50] + (T(-11) / T(3)) * z[93];
z[15] = prod_pow(m1_set::bc<T>[0], 2) * z[15];
z[25] = abb[27] + z[61];
z[25] = abb[58] * z[25];
z[25] = z[25] + z[82] + -z[92];
z[32] = -z[39] + z[115];
z[5] = z[5] + -z[32];
z[5] = abb[4] * z[5];
z[0] = abb[45] + -z[0] + z[80];
z[0] = z[0] * z[59];
z[3] = abb[7] * z[3];
z[38] = abb[8] * z[78];
z[0] = -z[0] + z[3] + z[5] + z[25] + -z[38];
z[3] = -abb[60] * z[0];
z[2] = -z[2] + -z[31] + z[84] + -z[112];
z[2] = z[2] * z[108];
z[5] = abb[22] + z[61];
z[5] = abb[58] * z[5];
z[5] = z[5] + z[20];
z[38] = -z[31] + z[123];
z[41] = z[38] + -z[43] + z[47];
z[41] = abb[4] * z[41];
z[2] = z[2] + z[5] + -z[41] + z[94] + z[118];
z[41] = -abb[59] * z[2];
z[43] = -abb[18] + abb[21];
z[43] = z[13] * z[43];
z[19] = abb[20] * z[19];
z[47] = z[18] + z[75];
z[50] = abb[19] * z[47];
z[54] = abb[28] * z[66];
z[19] = z[19] + z[43] + -z[50] + -z[54];
z[43] = abb[61] * z[19];
z[17] = -z[17] * z[52];
z[50] = abb[37] * z[113];
z[17] = z[17] + z[50];
z[17] = abb[3] * z[17];
z[1] = z[1] + z[3] + z[4] + z[6] + z[8] + z[9] + z[10] + z[12] + z[15] + z[17] + z[23] + z[30] + z[37] + z[41] + z[43] + z[56];
z[3] = -z[31] + z[42] + -z[53];
z[4] = z[3] + z[16];
z[6] = 4 * abb[53];
z[8] = z[4] + z[6] + z[83] + -z[86];
z[9] = abb[8] * z[8];
z[10] = -abb[56] + -z[27] + z[46] + z[65] + -z[103];
z[12] = z[10] * z[76];
z[11] = -abb[48] + -abb[52] + -z[11] + -z[72] + z[79];
z[11] = z[11] * z[59];
z[4] = z[4] + z[51];
z[4] = abb[4] * z[4];
z[15] = 2 * z[22] + -z[34];
z[16] = 2 * abb[11];
z[15] = z[15] * z[16];
z[4] = z[4] + z[9] + z[11] + z[12] + z[15] + -z[25] + z[33] + z[81] + -z[111];
z[4] = abb[34] * z[4];
z[3] = -z[3] + -z[7] + z[18] + z[29] + -z[39];
z[3] = abb[0] * z[3];
z[7] = z[13] + z[45] + -z[72];
z[7] = abb[7] * z[7];
z[11] = -z[70] + -z[98] + z[134];
z[11] = z[11] * z[59];
z[12] = abb[4] * z[47];
z[3] = z[3] + z[7] + -z[9] + z[11] + z[12] + -z[21] + z[94] + 4 * z[96] + z[131];
z[3] = abb[32] * z[3];
z[7] = -abb[10] * z[10];
z[9] = z[24] + z[58] + -z[67];
z[9] = abb[0] * z[9];
z[10] = abb[2] * z[99];
z[11] = z[49] * z[91];
z[12] = z[36] + z[98];
z[12] = abb[7] * z[12];
z[13] = abb[56] + z[28] + -z[58];
z[13] = abb[4] * z[13];
z[16] = -z[73] * z[77];
z[7] = z[7] + z[9] + z[10] + z[11] + z[12] + z[13] + z[16] + -z[35] + -z[93];
z[7] = abb[29] * z[7];
z[9] = -z[24] + z[44] + z[55] + -z[88];
z[9] = abb[1] * z[9];
z[9] = -z[9] + z[93];
z[10] = z[48] + z[107] + -z[111];
z[6] = -6 * abb[44] + z[6] + z[38] + z[40] + z[60];
z[6] = abb[4] * z[6];
z[11] = -z[27] + z[101];
z[12] = z[11] + z[44] + -z[122];
z[12] = z[12] * z[121];
z[5] = -z[5] + z[6] + -6 * z[9] + z[10] + z[12] + 2 * z[26] + z[102] + -z[125];
z[5] = abb[30] * z[5];
z[6] = -abb[4] * z[8];
z[8] = -z[65] + z[90] + -z[129];
z[8] = z[8] * z[133];
z[9] = -z[11] + z[32];
z[9] = z[9] * z[59];
z[11] = abb[24] + abb[26];
z[11] = -abb[58] * z[11];
z[6] = z[6] + z[8] + z[9] + -z[10] + z[11] + -z[15] + z[20] + z[82];
z[6] = abb[31] * z[6];
z[3] = z[3] + z[4] + z[5] + z[6] + 2 * z[7] + z[14];
z[3] = m1_set::bc<T>[0] * z[3];
z[2] = -abb[40] * z[2];
z[0] = -abb[41] * z[0];
z[4] = abb[42] * z[19];
z[0] = z[0] + z[2] + z[3] + z[4];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_472_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("26.768985058639030131879448466204587843194209849485869459626654332"),stof<T>("-71.09976665845057414018308566493259719417126011945365390520978474")}, std::complex<T>{stof<T>("-42.916313497125379960109039975440756951706763485975498496114291434"),stof<T>("50.097203268269536149451995402961805638110798703170824860421667433")}, std::complex<T>{stof<T>("-68.998865259681302114571280956760981179530158456884751627751743829"),stof<T>("33.462032315675796419244727867436187632553147222387032094853331986")}, std::complex<T>{stof<T>("-7.7599537243468496712143941279548803188791498218471930075541938561"),stof<T>("2.8861775862569761974940276305852226522135036017093535981858642458")}, std::complex<T>{stof<T>("-5.0575627556733391880167629940750347462215625757424485869048046118"),stof<T>("-4.5664558505429241331392596983340719583697070838177303934981038314")}, std::complex<T>{stof<T>("-18.595137544313140632617906208723942820387168351627831868387623813"),stof<T>("-14.550432555754452788706755917838872146834908138660943021255930274")}, std::complex<T>{stof<T>("53.733539673834322450951809135243292402554831344156907201392758649"),stof<T>("-28.690179745535159152865625591053117529483797426873522866633384539")}, std::complex<T>{stof<T>("16.147328438486349828229591509236169108512553636489629036487637101"),stof<T>("21.002563390181037990731090261970791556060461416282829044788117307")}, std::complex<T>{stof<T>("34.457700471840500033963841532815684720944437378278081686748100531"),stof<T>("22.23167431373846896959060183318350230037359763187093672121801076")}, std::complex<T>{stof<T>("-3.329811958466160968998434387206254043411841238899987442028638633"),stof<T>("-19.322285125895090055085858194221942249904257934174452249475877721")}, std::complex<T>{stof<T>("-17.271625219662090912607795267992703281726162263884459014102458665"),stof<T>("-21.138988593369706307284751557739988174950239183665002331673644356")}, std::complex<T>{stof<T>("-14.483684283504898638158415130943135866560687868288878251996252622"),stof<T>("-8.545319157168662992939137604362808736006569133733018381228334482")}, std::complex<T>{stof<T>("-2.9218224286660744113546553976309690196603579241088665283397350529"),stof<T>("-6.6126383472427798164134010891832985216066551560037079983624411642")}, std::complex<T>{stof<T>("2.2707742825045578234848655102323880671024945024242466871796446223"),stof<T>("4.0356141500127251131740803438635949520553790971436215987294526578")}, std::complex<T>{stof<T>("24.303748446147996408504459090197558519166594349054900296452518855"),stof<T>("21.693912603527431625085336361492647674755891281338759814387022612")}, std::complex<T>{stof<T>("-1.2575190271596537991920991302195900456713969230739235361053132628"),stof<T>("1.262423601462443931986568394750185544768544624668046192565869342")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(abs(k.W[38])) - rlog(abs(kbase.W[38])), rlog(k.W[80].real()/kbase.W[80].real()), C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_472_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_472_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-7.350668572901619165007643210938794348163179690218144484593632625"),stof<T>("16.985472462834486492024365887645929422402519759398212833850673346")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({38});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,62> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W81(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(abs(kend.W[38])) - rlog(abs(k.W[38])), rlog(kend.W[80].real()/k.W[80].real()), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_472_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_472_DLogXconstant_part(base_point<T>, kend);
	value += f_4_472_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_472_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_472_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_472_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_472_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_472_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_472_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
