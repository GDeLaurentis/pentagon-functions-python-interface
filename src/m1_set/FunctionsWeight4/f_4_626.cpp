/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_626.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_626_abbreviated (const std::array<T,78>& abb) {
T z[154];
z[0] = abb[59] * (T(1) / T(4));
z[1] = abb[57] * (T(1) / T(2));
z[2] = abb[49] * (T(1) / T(4));
z[3] = -abb[51] + abb[52];
z[4] = abb[53] * (T(-1) / T(2)) + -z[0] + z[1] + -z[2] + -z[3];
z[4] = abb[14] * z[4];
z[5] = abb[49] * (T(1) / T(2));
z[6] = abb[56] + z[5];
z[7] = abb[50] + -abb[60];
z[8] = abb[47] + z[7];
z[9] = abb[53] + -z[8];
z[9] = -z[6] + (T(1) / T(2)) * z[9];
z[9] = abb[9] * z[9];
z[10] = abb[47] + abb[52];
z[11] = -abb[61] + z[10];
z[12] = abb[53] + z[11];
z[12] = -abb[57] + -z[5] + (T(1) / T(2)) * z[12];
z[12] = abb[10] * z[12];
z[4] = z[4] + z[9] + z[12];
z[9] = 2 * abb[51];
z[12] = 2 * abb[49];
z[13] = z[9] + -z[12];
z[14] = abb[56] + abb[57];
z[15] = -abb[47] + z[14];
z[16] = 2 * abb[53];
z[15] = abb[52] + abb[61] + z[7] + -z[13] + 4 * z[15] + -z[16];
z[15] = abb[8] * z[15];
z[17] = abb[15] * abb[52];
z[15] = -z[15] + z[17];
z[18] = 2 * abb[54];
z[19] = abb[51] * (T(1) / T(3)) + z[18];
z[20] = abb[47] * (T(5) / T(2));
z[21] = abb[60] + -abb[61];
z[22] = -4 * abb[53] + -z[21];
z[22] = abb[50] * (T(11) / T(2)) + 2 * z[22];
z[22] = abb[49] * (T(5) / T(2)) + abb[52] * (T(13) / T(6)) + (T(-5) / T(6)) * z[14] + z[19] + z[20] + (T(1) / T(3)) * z[22];
z[22] = abb[0] * z[22];
z[23] = 2 * abb[50];
z[24] = z[16] + z[21] + z[23];
z[24] = -abb[51] + 2 * z[24];
z[25] = abb[59] * (T(3) / T(2));
z[20] = abb[49] * (T(5) / T(3)) + abb[52] * (T(10) / T(3)) + (T(-5) / T(2)) * z[14] + z[20] + (T(1) / T(3)) * z[24] + -z[25];
z[20] = abb[3] * z[20];
z[24] = 2 * abb[58];
z[26] = abb[60] * (T(-7) / T(2)) + -z[24];
z[0] = abb[47] * (T(-7) / T(3)) + abb[48] * (T(-1) / T(2)) + abb[56] * (T(7) / T(2)) + abb[52] * (T(11) / T(6)) + z[0] + -z[2] + (T(1) / T(3)) * z[26];
z[0] = abb[1] * z[0];
z[2] = -abb[49] + z[24];
z[26] = abb[60] + abb[61];
z[27] = 11 * z[26];
z[28] = 23 * abb[50] + -z[27];
z[18] = abb[52] * (T(-11) / T(12)) + abb[51] * (T(5) / T(6)) + abb[47] * (T(17) / T(6)) + -z[2] + z[18] + (T(1) / T(12)) * z[28];
z[18] = abb[4] * z[18];
z[28] = abb[47] + abb[49];
z[29] = abb[50] + z[26];
z[30] = (T(1) / T(2)) * z[29];
z[31] = abb[51] + z[30];
z[32] = abb[52] * (T(7) / T(4)) + z[28] + (T(-1) / T(2)) * z[31];
z[33] = abb[5] * z[32];
z[34] = abb[15] * abb[49];
z[35] = z[33] + z[34];
z[36] = abb[20] + abb[21];
z[36] = (T(1) / T(2)) * z[36];
z[37] = -abb[63] + -abb[64] + abb[65];
z[38] = -abb[62] + (T(1) / T(3)) * z[37];
z[36] = z[36] * z[38];
z[37] = 3 * abb[62] + -z[37];
z[38] = (T(1) / T(2)) * z[37];
z[39] = abb[18] * z[38];
z[40] = abb[19] * z[38];
z[41] = z[39] + z[40];
z[42] = abb[22] + abb[27];
z[43] = abb[23] + abb[25];
z[42] = (T(1) / T(6)) * z[42] + (T(2) / T(3)) * z[43];
z[44] = abb[66] + abb[67] + abb[68] + -abb[69];
z[42] = -z[42] * z[44];
z[45] = abb[47] + z[24];
z[19] = abb[50] * (T(1) / T(3)) + -z[19] + z[45];
z[19] = abb[15] * z[19];
z[46] = abb[50] + -abb[61];
z[47] = 2 * abb[47];
z[48] = (T(-1) / T(2)) * z[46] + -z[47];
z[1] = z[1] + (T(1) / T(3)) * z[48];
z[1] = abb[2] * z[1];
z[48] = 4 * abb[58];
z[27] = abb[50] + z[27];
z[27] = (T(1) / T(4)) * z[27] + -z[48];
z[49] = 3 * abb[49];
z[27] = abb[51] * (T(-7) / T(2)) + abb[47] * (T(7) / T(6)) + abb[52] * (T(59) / T(12)) + (T(1) / T(3)) * z[27] + z[49];
z[27] = abb[7] * z[27];
z[50] = abb[52] * (T(1) / T(2));
z[51] = abb[51] + z[50];
z[52] = -abb[50] + z[26];
z[53] = (T(1) / T(2)) * z[52];
z[54] = -z[51] + z[53];
z[55] = abb[6] * z[54];
z[56] = (T(-1) / T(4)) * z[44];
z[57] = abb[26] * z[56];
z[58] = abb[0] + -abb[16];
z[59] = abb[7] + z[58];
z[60] = abb[55] * z[59];
z[61] = -abb[53] + abb[56];
z[62] = abb[57] + z[61];
z[63] = -abb[47] + z[62];
z[64] = abb[17] * z[63];
z[65] = abb[24] * z[56];
z[66] = abb[13] * z[61];
z[67] = -abb[50] + abb[51];
z[68] = -abb[49] + z[67];
z[68] = -abb[47] + abb[52] * (T(-4) / T(3)) + (T(1) / T(3)) * z[68];
z[68] = abb[16] * z[68];
z[0] = z[0] + 7 * z[1] + 5 * z[4] + (T(-2) / T(3)) * z[15] + z[18] + z[19] + z[20] + z[22] + z[27] + (T(1) / T(3)) * z[35] + z[36] + z[41] + z[42] + (T(1) / T(6)) * z[55] + z[57] + -2 * z[60] + (T(-8) / T(3)) * z[64] + z[65] + (T(5) / T(2)) * z[66] + z[68];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[1] = abb[60] * (T(1) / T(2));
z[4] = abb[50] * (T(1) / T(2));
z[15] = z[1] + -z[4];
z[18] = abb[61] * (T(1) / T(2));
z[19] = z[15] + -z[18];
z[20] = abb[51] + abb[53] + z[19];
z[20] = (T(1) / T(2)) * z[20];
z[22] = -abb[47] + abb[57];
z[27] = abb[52] * (T(1) / T(4));
z[35] = z[6] + -z[20] + z[22] + z[27];
z[35] = abb[8] * z[35];
z[36] = 3 * abb[47];
z[42] = z[36] + -z[67];
z[55] = 2 * abb[52];
z[68] = z[5] + (T(1) / T(2)) * z[42] + z[55];
z[69] = abb[16] * z[68];
z[70] = abb[27] * z[56];
z[71] = (T(1) / T(4)) * z[37];
z[72] = abb[21] * z[71];
z[69] = -z[69] + z[70] + -z[72];
z[70] = abb[47] * (T(3) / T(2));
z[72] = z[4] + z[70];
z[73] = 3 * abb[58];
z[74] = abb[51] * (T(1) / T(2));
z[75] = z[73] + -z[74];
z[76] = 3 * abb[54];
z[77] = z[75] + -z[76];
z[78] = z[5] + z[72] + z[77];
z[79] = abb[15] * z[78];
z[80] = abb[20] * z[71];
z[17] = -z[17] + z[79] + -z[80];
z[79] = abb[49] + -abb[53];
z[80] = 2 * abb[57];
z[11] = z[11] + -z[79] + -z[80];
z[81] = abb[10] * z[11];
z[82] = abb[52] * (T(3) / T(4));
z[83] = -z[5] + z[82];
z[20] = z[14] + -z[20] + -z[83];
z[20] = abb[3] * z[20];
z[43] = -z[43] * z[56];
z[84] = 2 * abb[56];
z[85] = z[79] + z[84];
z[8] = z[8] + z[85];
z[86] = abb[9] * z[8];
z[87] = -abb[60] + z[24];
z[88] = -abb[47] + abb[52];
z[89] = z[87] + -z[88];
z[90] = abb[1] * z[89];
z[91] = -abb[60] + z[76];
z[92] = -z[46] + z[91];
z[93] = abb[53] + -z[55] + -z[92];
z[93] = abb[0] * z[93];
z[94] = -abb[54] + abb[58];
z[94] = -z[88] + 3 * z[94];
z[94] = abb[4] * z[94];
z[95] = abb[49] + z[47];
z[96] = -abb[50] + abb[58] + -z[55] + -z[95];
z[96] = abb[7] * z[96];
z[97] = abb[22] * z[56];
z[98] = abb[47] + z[46];
z[99] = abb[2] * z[98];
z[20] = -z[17] + z[20] + -z[35] + z[43] + -z[65] + -z[69] + -z[81] + z[86] + z[90] + z[93] + z[94] + z[96] + -z[97] + -z[99];
z[20] = abb[31] * z[20];
z[43] = abb[49] + z[42];
z[93] = 4 * abb[52];
z[94] = z[43] + z[93];
z[96] = abb[16] * z[94];
z[100] = (T(-1) / T(2)) * z[44];
z[101] = abb[27] * z[100];
z[102] = abb[21] * z[38];
z[101] = z[101] + -z[102];
z[102] = -z[96] + z[101];
z[103] = abb[52] * (T(7) / T(2));
z[104] = z[47] + z[103];
z[105] = abb[49] + z[104];
z[106] = -abb[60] + 3 * abb[61];
z[107] = -abb[50] + z[106];
z[107] = z[105] + (T(1) / T(2)) * z[107];
z[107] = abb[0] * z[107];
z[108] = -abb[51] + z[50];
z[109] = -abb[47] + z[53] + z[108];
z[110] = abb[3] * z[109];
z[111] = z[40] + 4 * z[99];
z[112] = abb[17] * z[109];
z[113] = abb[28] * z[100];
z[112] = z[112] + z[113];
z[107] = z[102] + z[107] + -z[110] + z[111] + z[112];
z[107] = abb[32] * z[107];
z[110] = 6 * abb[54];
z[114] = 6 * abb[58] + -z[110];
z[42] = -z[42] + z[55] + -z[114];
z[42] = abb[15] * z[42];
z[115] = abb[22] * z[100];
z[116] = abb[20] * z[38];
z[34] = -z[34] + z[42] + -z[115] + z[116];
z[42] = abb[24] * z[100];
z[115] = z[39] + z[42];
z[116] = z[34] + 4 * z[90] + -z[115];
z[117] = 3 * abb[60] + -abb[61];
z[118] = 3 * abb[50];
z[119] = z[117] + z[118];
z[120] = -z[50] + z[95];
z[119] = z[24] + -z[110] + (T(1) / T(2)) * z[119] + z[120];
z[119] = abb[0] * z[119];
z[119] = z[112] + z[116] + z[119];
z[121] = abb[36] * z[119];
z[122] = abb[34] * z[97];
z[121] = -z[107] + z[121] + -z[122];
z[123] = 2 * z[90];
z[17] = z[17] + -z[123];
z[124] = 2 * z[99];
z[69] = z[69] + z[124];
z[125] = z[17] + -z[69];
z[126] = z[65] + z[125];
z[127] = abb[52] * (T(3) / T(2));
z[128] = -z[76] + z[127];
z[129] = -z[26] + -z[118];
z[129] = -z[12] + -z[36] + -z[128] + (T(1) / T(2)) * z[129];
z[129] = abb[0] * z[129];
z[130] = abb[18] * z[71];
z[131] = (T(3) / T(4)) * z[37];
z[132] = abb[19] * z[131];
z[129] = -z[112] + z[126] + z[129] + -z[130] + -z[132];
z[129] = abb[34] * z[129];
z[133] = z[18] + z[108];
z[134] = z[80] + z[133];
z[85] = -z[15] + -z[47] + z[85] + z[134];
z[85] = abb[8] * z[85];
z[135] = abb[25] * z[100];
z[85] = z[85] + z[135];
z[135] = abb[23] * z[100];
z[135] = z[85] + z[135];
z[136] = -abb[52] + z[62];
z[136] = abb[0] * z[136];
z[136] = -z[64] + z[136];
z[41] = -z[41] + z[135] + 2 * z[136];
z[41] = abb[33] * z[41];
z[19] = z[19] + -z[51] + -z[79];
z[19] = abb[33] * z[19];
z[79] = -z[26] + z[118];
z[136] = (T(1) / T(2)) * z[79];
z[120] = z[120] + z[136];
z[137] = -abb[34] * z[120];
z[138] = abb[36] * z[109];
z[19] = z[19] + z[137] + z[138];
z[19] = abb[3] * z[19];
z[89] = 2 * z[89];
z[137] = abb[36] * z[89];
z[28] = abb[50] + z[28];
z[139] = -abb[51] + z[28];
z[140] = abb[32] * z[139];
z[141] = abb[33] * z[139];
z[140] = z[140] + -z[141];
z[137] = z[137] + -z[140];
z[26] = (T(1) / T(2)) * z[26];
z[142] = z[23] + -z[26];
z[143] = abb[49] * (T(3) / T(2));
z[144] = z[77] + z[142] + z[143];
z[145] = abb[47] * (T(-7) / T(2)) + z[127] + -z[144];
z[145] = abb[34] * z[145];
z[145] = -z[137] + z[145];
z[145] = abb[4] * z[145];
z[146] = abb[36] * z[139];
z[141] = -z[141] + z[146];
z[147] = 2 * abb[32];
z[98] = z[98] * z[147];
z[148] = z[98] + z[141];
z[149] = abb[47] * (T(1) / T(2));
z[150] = z[5] + z[149];
z[151] = -z[74] + z[150];
z[152] = abb[58] + z[127];
z[106] = (T(1) / T(2)) * z[106] + z[152];
z[153] = -abb[50] + z[106] + z[151];
z[153] = abb[34] * z[153];
z[153] = z[148] + z[153];
z[153] = abb[7] * z[153];
z[19] = z[19] + z[20] + z[41] + -z[121] + z[129] + z[145] + z[153];
z[19] = abb[31] * z[19];
z[20] = abb[18] * z[131];
z[41] = z[20] + z[97] + z[126];
z[5] = -z[5] + z[74];
z[97] = z[5] + -z[26] + z[70] + z[152];
z[97] = abb[7] * z[97];
z[45] = z[45] + z[53] + z[128];
z[45] = abb[0] * z[45];
z[126] = abb[49] + -z[9] + z[30] + z[50];
z[128] = -abb[3] * z[126];
z[117] = -abb[58] + z[50] + -z[76] + (T(1) / T(2)) * z[117];
z[129] = abb[50] + z[150];
z[131] = -z[74] + -z[117] + -z[129];
z[131] = abb[4] * z[131];
z[145] = abb[19] * z[71];
z[45] = -z[41] + z[45] + z[97] + z[112] + z[128] + z[131] + -z[145];
z[45] = abb[31] * z[45];
z[76] = z[2] + -z[76];
z[97] = abb[47] + -z[21] + z[55] + -z[76];
z[97] = abb[0] * z[97];
z[17] = z[17] + z[20] + z[65] + z[69] + z[97] + z[132];
z[17] = abb[34] * z[17];
z[20] = z[21] + z[118];
z[2] = z[2] + -z[50];
z[20] = abb[47] + -z[2] + (T(1) / T(2)) * z[20];
z[21] = abb[4] * z[20];
z[69] = abb[47] + -abb[50] + z[93];
z[97] = -z[12] + z[24] + -z[69];
z[97] = abb[7] * z[97];
z[90] = z[90] + z[99];
z[2] = z[2] + -z[30];
z[2] = abb[0] * z[2];
z[13] = z[13] + -z[69];
z[30] = abb[3] * z[13];
z[2] = z[2] + z[21] + z[30] + z[40] + -z[90] + 2 * z[97];
z[2] = abb[35] * z[2];
z[21] = abb[0] * z[126];
z[12] = -z[12] + z[31] + -z[104];
z[30] = abb[5] * z[12];
z[31] = z[30] + -z[40];
z[21] = z[21] + z[31] + -z[112];
z[69] = -abb[33] + abb[36];
z[21] = z[21] * z[69];
z[97] = z[70] + z[143];
z[99] = z[23] + -z[74] + z[97] + z[117];
z[99] = abb[34] * z[99];
z[99] = z[99] + -z[140] + -z[146];
z[99] = abb[4] * z[99];
z[23] = z[23] + -z[106] + z[151];
z[23] = abb[34] * z[23];
z[104] = -2 * z[69];
z[104] = z[13] * z[104];
z[23] = z[23] + -z[98] + z[104];
z[23] = abb[7] * z[23];
z[13] = -z[13] * z[69];
z[98] = abb[34] * z[139];
z[13] = z[13] + z[98];
z[13] = abb[3] * z[13];
z[104] = z[58] * z[147];
z[126] = abb[7] + -z[58];
z[126] = abb[34] * z[126];
z[104] = z[104] + z[126];
z[126] = abb[31] * z[59];
z[128] = z[104] + -z[126];
z[131] = 3 * abb[55];
z[140] = z[128] * z[131];
z[146] = abb[31] + -abb[34];
z[151] = abb[26] * z[44] * z[146];
z[2] = z[2] + z[13] + z[17] + z[21] + z[23] + z[45] + z[99] + -z[107] + z[122] + z[140] + (T(1) / T(4)) * z[151];
z[2] = abb[35] * z[2];
z[13] = 2 * z[81] + z[102];
z[1] = z[1] + z[4];
z[17] = z[16] + -z[84];
z[21] = abb[61] * (T(-3) / T(2)) + z[1] + z[17] + -z[105];
z[21] = abb[0] * z[21];
z[23] = -abb[47] + z[80];
z[45] = z[15] + z[17] + -z[23] + z[133];
z[45] = abb[17] * z[45];
z[45] = z[45] + z[113];
z[99] = -abb[53] + abb[57];
z[3] = z[3] + -z[99];
z[102] = abb[14] * z[3];
z[102] = 2 * z[102] + -z[135];
z[11] = abb[3] * z[11];
z[105] = abb[57] + z[46];
z[107] = 2 * abb[2];
z[105] = z[105] * z[107];
z[94] = abb[7] * z[94];
z[11] = z[11] + z[13] + -z[21] + -z[39] + z[45] + -6 * z[60] + z[94] + -z[102] + z[105];
z[21] = abb[71] * z[11];
z[60] = 3 * abb[52];
z[94] = 3 * abb[59];
z[105] = 6 * abb[48] + -abb[60] + -z[24] + z[36] + z[49] + z[60] + -z[84] + -z[94];
z[105] = abb[1] * z[105];
z[6] = abb[51] + z[6] + -z[22] + -z[24] + (T(1) / T(4)) * z[79] + z[82];
z[6] = abb[0] * z[6];
z[22] = z[25] + z[143];
z[79] = z[22] + z[99];
z[82] = -z[9] + z[55] + z[79];
z[82] = abb[14] * z[82];
z[7] = -abb[61] + z[7];
z[7] = (T(-1) / T(4)) * z[7] + z[27] + -z[74];
z[27] = z[7] + z[62] + -z[70];
z[27] = abb[17] * z[27];
z[70] = abb[28] * z[56];
z[6] = z[6] + z[27] + z[33] + z[70] + -z[82] + -z[86] + z[105] + z[145];
z[6] = abb[36] * z[6];
z[27] = abb[34] * z[119];
z[6] = z[6] + z[27];
z[6] = abb[36] * z[6];
z[7] = -z[7] + z[62] + -z[149];
z[7] = abb[17] * z[7];
z[27] = z[65] + -z[66];
z[23] = -z[23] + -z[46];
z[23] = abb[2] * z[23];
z[29] = -abb[56] + abb[57] + (T(1) / T(4)) * z[29] + -z[83];
z[29] = abb[0] * z[29];
z[33] = -abb[57] + (T(1) / T(2)) * z[10];
z[33] = abb[3] * z[33];
z[7] = z[7] + z[23] + z[27] + z[29] + z[33] + -z[70] + z[81] + z[130];
z[7] = abb[32] * z[7];
z[23] = abb[0] * z[120];
z[29] = -z[23] + -z[112] + z[115];
z[33] = -abb[34] * z[29];
z[62] = abb[34] * z[88];
z[65] = -abb[3] * z[62];
z[7] = z[7] + z[33] + z[65];
z[7] = abb[32] * z[7];
z[33] = -abb[52] + -z[5] + z[72] + z[87];
z[33] = abb[36] * z[33];
z[65] = abb[34] * z[89];
z[33] = z[33] + z[65];
z[33] = abb[36] * z[33];
z[65] = -z[55] + z[114];
z[43] = z[43] + z[65];
z[70] = -abb[70] * z[43];
z[72] = abb[33] * z[88];
z[72] = z[62] + z[72];
z[72] = abb[34] * z[72];
z[67] = abb[47] + -abb[49] + z[67];
z[74] = -z[55] + z[67];
z[83] = -abb[37] * z[74];
z[48] = 2 * abb[60] + -z[48] + -z[74];
z[48] = abb[38] * z[48];
z[74] = -abb[72] * z[54];
z[62] = -z[62] * z[147];
z[87] = abb[75] * z[71];
z[33] = z[33] + z[48] + z[62] + z[70] + 2 * z[72] + z[74] + z[83] + z[87];
z[33] = abb[4] * z[33];
z[48] = -abb[52] + z[14];
z[28] = -z[16] + z[28] + z[48];
z[28] = abb[0] * z[28];
z[62] = -abb[60] + z[84] + z[88];
z[62] = abb[1] * z[62];
z[62] = z[62] + z[81];
z[70] = z[36] + z[46] + -z[80];
z[70] = abb[2] * z[70];
z[70] = z[70] + z[86];
z[1] = -z[1] + z[18];
z[74] = z[1] + -z[108];
z[63] = z[63] + z[74];
z[63] = abb[3] * z[63];
z[81] = -abb[4] + abb[7];
z[81] = z[81] * z[109];
z[83] = 2 * z[66];
z[28] = z[28] + z[62] + -z[63] + -3 * z[64] + -z[70] + z[81] + z[83] + -z[102];
z[63] = abb[74] * z[28];
z[50] = z[26] + -z[50] + z[77] + -z[129];
z[50] = abb[19] * z[50];
z[26] = abb[50] + abb[52] * (T(5) / T(2)) + z[26] + -z[75] + z[97];
z[26] = abb[18] * z[26];
z[75] = -abb[52] + z[78];
z[75] = abb[20] * z[75];
z[68] = abb[21] * z[68];
z[77] = abb[29] * (T(1) / T(4));
z[77] = z[44] * z[77];
z[78] = abb[15] + abb[16];
z[78] = z[71] * z[78];
z[37] = abb[30] * z[37];
z[26] = -z[26] + -z[37] + z[50] + z[68] + -z[75] + z[77] + z[78];
z[37] = -abb[75] * z[26];
z[50] = abb[50] + abb[60];
z[16] = z[16] + -z[18] + (T(3) / T(2)) * z[50] + -z[80] + z[95] + -z[110] + z[127];
z[16] = abb[0] * z[16];
z[15] = -z[15] + z[17] + z[36] + -z[134];
z[15] = abb[17] * z[15];
z[15] = z[15] + z[42] + -z[113];
z[17] = z[15] + z[85];
z[18] = z[34] + 2 * z[86];
z[34] = -abb[56] + -abb[60] + z[47] + -z[55] + z[73];
z[42] = 2 * abb[1];
z[34] = z[34] * z[42];
z[8] = abb[3] * z[8];
z[8] = z[8] + z[16] + -z[17] + z[18] + z[34] + z[40] + -z[83];
z[16] = -abb[70] * z[8];
z[34] = 4 * abb[51];
z[22] = z[22] + -z[34] + z[93] + -z[99];
z[22] = abb[14] * z[22];
z[42] = -abb[51] + z[1] + z[79] + z[127];
z[42] = abb[3] * z[42];
z[50] = 2 * abb[0];
z[3] = z[3] * z[50];
z[3] = z[3] + z[22] + z[30] + z[42] + -z[45] + -z[62];
z[22] = abb[7] * z[12];
z[22] = -z[3] + z[22] + z[135];
z[22] = abb[73] * z[22];
z[42] = -z[46] + z[55];
z[42] = z[42] * z[50];
z[42] = z[42] + -z[96] + z[101] + z[111] + z[115];
z[42] = abb[37] * z[42];
z[45] = abb[38] * z[12];
z[1] = -abb[53] + z[1] + -z[51] + z[94];
z[46] = prod_pow(abb[33], 2);
z[51] = (T(1) / T(2)) * z[46];
z[1] = z[1] * z[51];
z[25] = -abb[51] + -abb[56] + z[4] + z[25] + z[150];
z[25] = abb[36] * z[25];
z[55] = -abb[34] * z[109];
z[25] = z[25] + z[55];
z[25] = abb[36] * z[25];
z[55] = -abb[37] * z[54];
z[62] = abb[75] * z[38];
z[1] = z[1] + z[25] + z[45] + z[55] + z[62] + z[72];
z[1] = abb[3] * z[1];
z[25] = -abb[51] + abb[53];
z[25] = abb[0] * z[25];
z[55] = -abb[25] * z[56];
z[25] = z[25] + -z[27] + -z[35] + z[55] + z[82];
z[25] = z[25] * z[46];
z[27] = z[61] + z[74];
z[27] = abb[3] * z[27];
z[35] = z[50] * z[61];
z[27] = -z[27] + z[35] + z[66] + -z[70];
z[17] = z[17] + z[27];
z[17] = abb[72] * z[17];
z[20] = abb[34] * z[20];
z[20] = z[20] + -z[141];
z[20] = abb[34] * z[20];
z[4] = abb[61] + -z[4] + -z[5] + -z[149];
z[4] = abb[32] * z[4];
z[4] = z[4] + -z[98];
z[4] = abb[32] * z[4];
z[5] = abb[49] + z[36];
z[35] = -abb[51] + z[118];
z[36] = -2 * abb[61] + z[5] + z[35];
z[36] = abb[37] * z[36];
z[55] = abb[47] + z[49];
z[56] = abb[50] + abb[51];
z[56] = -8 * abb[52] + -z[55] + 3 * z[56];
z[56] = abb[38] * z[56];
z[4] = z[4] + z[20] + z[36] + z[56] + z[87];
z[4] = abb[7] * z[4];
z[20] = z[46] * z[100];
z[36] = abb[70] + abb[72];
z[56] = -abb[34] * z[69];
z[56] = abb[37] + abb[38] + z[36] + z[56];
z[56] = -z[44] * z[56];
z[61] = abb[34] * z[44];
z[62] = abb[32] * z[100];
z[61] = z[61] + z[62];
z[61] = abb[32] * z[61];
z[62] = -z[100] * z[146];
z[66] = -abb[36] * z[44];
z[62] = z[62] + z[66];
z[62] = abb[31] * z[62];
z[56] = -z[20] + z[56] + z[61] + z[62];
z[61] = abb[26] * (T(1) / T(2));
z[56] = z[56] * z[61];
z[29] = abb[33] * z[29];
z[23] = -z[23] + z[39] + z[90];
z[23] = abb[34] * z[23];
z[23] = z[23] + z[29];
z[23] = abb[34] * z[23];
z[29] = -abb[58] + z[91] + -z[139];
z[29] = z[29] * z[50];
z[29] = z[29] + z[40] + -z[116];
z[29] = abb[38] * z[29];
z[39] = abb[34] * z[54];
z[62] = abb[32] * z[54];
z[62] = -z[39] + (T(1) / T(2)) * z[62];
z[62] = abb[32] * z[62];
z[51] = abb[37] + abb[72] + -z[51];
z[51] = z[51] * z[54];
z[66] = abb[33] * z[39];
z[51] = z[51] + z[62] + z[66];
z[51] = abb[6] * z[51];
z[32] = -z[32] * z[46];
z[32] = z[32] + -z[45];
z[32] = abb[5] * z[32];
z[36] = -z[36] * z[44];
z[20] = -z[20] + z[36];
z[36] = abb[23] * (T(1) / T(2));
z[20] = z[20] * z[36];
z[45] = -abb[31] * z[128];
z[46] = -abb[37] * z[58];
z[58] = abb[18] + -abb[21];
z[62] = -abb[75] * z[58];
z[45] = z[45] + 2 * z[46] + z[62];
z[45] = z[45] * z[131];
z[0] = abb[76] + abb[77] + z[0] + z[1] + z[2] + z[4] + z[6] + z[7] + z[16] + z[17] + z[19] + z[20] + z[21] + z[22] + z[23] + z[25] + z[29] + z[32] + z[33] + z[37] + z[42] + z[45] + z[51] + z[56] + z[63];
z[1] = -abb[52] + z[9] + z[47] + -z[52];
z[1] = abb[17] * z[1];
z[2] = abb[50] + -z[9] + -z[10] + -z[76];
z[2] = abb[0] * z[2];
z[4] = abb[52] * (T(9) / T(2)) + -z[34] + z[53] + z[55];
z[4] = abb[3] * z[4];
z[6] = abb[51] * (T(3) / T(2));
z[7] = z[6] + z[117] + -z[150];
z[7] = abb[4] * z[7];
z[10] = abb[49] + -abb[51];
z[10] = -abb[58] + abb[52] * (T(13) / T(2)) + (T(9) / T(2)) * z[10] + -z[142] + z[149];
z[10] = abb[7] * z[10];
z[16] = z[59] * z[131];
z[17] = abb[28] * z[44];
z[2] = z[1] + z[2] + z[4] + z[7] + z[10] + z[16] + z[17] + z[30] + z[41] + z[57] + -z[145];
z[2] = abb[35] * z[2];
z[4] = z[49] + z[93];
z[7] = 5 * abb[47];
z[10] = z[4] + z[7] + -z[24] + z[35];
z[10] = abb[7] * z[10];
z[14] = -z[14] + z[60] + z[92];
z[14] = z[14] * z[50];
z[16] = 2 * z[64] + z[115];
z[17] = -z[65] + -z[67];
z[17] = abb[4] * z[17];
z[19] = -abb[51] + z[48];
z[20] = abb[3] * z[19];
z[10] = z[10] + z[13] + z[14] + z[16] + z[17] + -z[18] + -2 * z[20] + z[40] + -z[123] + z[124];
z[10] = abb[31] * z[10];
z[13] = abb[50] + -abb[54];
z[7] = abb[52] + z[7] + 3 * z[13] + z[49];
z[7] = abb[0] * z[7];
z[13] = abb[28] + abb[24] * (T(-3) / T(4));
z[13] = -z[13] * z[44];
z[1] = -z[1] + z[7] + z[13] + -z[125] + -z[130] + z[132];
z[1] = abb[34] * z[1];
z[7] = abb[53] + z[9];
z[4] = -z[4] + 2 * z[7] + -z[80] + -z[94];
z[4] = abb[14] * z[4];
z[7] = -z[19] * z[50];
z[4] = z[4] + z[7] + z[16] + -z[31] + -z[83];
z[4] = abb[33] * z[4];
z[5] = z[5] + -z[127] + z[136];
z[5] = abb[34] * z[5];
z[7] = abb[49] + abb[50] + abb[52] + z[9] + -z[94];
z[7] = abb[33] * z[7];
z[5] = z[5] + z[7] + -z[138];
z[5] = abb[3] * z[5];
z[7] = abb[47] * (T(11) / T(2)) + -z[103] + z[144];
z[7] = abb[34] * z[7];
z[7] = z[7] + z[137];
z[7] = abb[4] * z[7];
z[6] = z[6] + -z[97] + -z[106];
z[6] = abb[34] * z[6];
z[6] = z[6] + -z[148];
z[6] = abb[7] * z[6];
z[9] = abb[31] + abb[34] * (T(-3) / T(2)) + -z[69];
z[9] = -z[9] * z[44] * z[61];
z[13] = abb[33] * z[54];
z[13] = z[13] + -z[39];
z[13] = abb[6] * z[13];
z[14] = z[104] + -2 * z[126];
z[14] = z[14] * z[131];
z[1] = z[1] + z[2] + z[4] + z[5] + z[6] + z[7] + z[9] + z[10] + z[13] + z[14] + z[121];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[40] * z[11];
z[4] = abb[43] * z[28];
z[5] = abb[26] * z[100];
z[6] = z[5] + -z[8];
z[6] = abb[39] * z[6];
z[7] = -z[58] * z[131];
z[8] = abb[3] * z[38];
z[7] = z[7] + z[8] + -z[26];
z[7] = abb[44] * z[7];
z[3] = -z[3] + z[85];
z[3] = abb[42] * z[3];
z[8] = -abb[4] + abb[6];
z[8] = z[8] * z[54];
z[5] = z[5] + z[8] + z[15] + z[27] + z[135];
z[5] = abb[41] * z[5];
z[8] = -abb[39] * z[43];
z[9] = abb[44] * z[71];
z[8] = z[8] + z[9];
z[8] = abb[4] * z[8];
z[10] = abb[39] + abb[42];
z[10] = -z[10] * z[36] * z[44];
z[11] = abb[42] * z[12];
z[9] = z[9] + z[11];
z[9] = abb[7] * z[9];
z[1] = abb[45] + abb[46] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_626_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-7.723833217776720043679759610063842380867935539518803113133451409"),stof<T>("-59.450202751970214778664841189529314731265152873489133273653589746")}, stof<T>("16.478415522602809253409778172429612904902144124051238655796702647"), std::complex<T>{stof<T>("-3.300941606079431900910972032058264263198616122504288241094993289"),stof<T>("-41.579234307661505154449986143401620280066273769036359885879092491")}, std::complex<T>{stof<T>("11.050602910404208459617418054725200263357116652766466943166426799"),stof<T>("-24.332994533221299598074184222686601250764234297043889419977065705")}, std::complex<T>{stof<T>("22.207355031287185186537134050569300564990978186796578418667212246"),stof<T>("-4.878169585748482702555095972027750071828173143346888593386266365")}, std::complex<T>{stof<T>("-14.943447245129745108785983139804194614574299854376278576287339499"),stof<T>("-71.124316164188625728859258507432637978646334726228473873701477083")}, std::complex<T>{stof<T>("-14.525636174632261502010469729845101314140075858810342675857097839"),stof<T>("13.361837461724824073044971398953331644252523278903182767928716325")}, std::complex<T>{stof<T>("-19.100722667459244436383206109275828220706560705607123754758257386"),stof<T>("-27.082030987641587155581260548960753807352863386229030624878469983")}, std::complex<T>{stof<T>("19.100722667459244436383206109275828220706560705607123754758257386"),stof<T>("27.082030987641587155581260548960753807352863386229030624878469983")}, std::complex<T>{stof<T>("5.988687094436881345811310662738584446203671990155887283914284001"),stof<T>("45.889135226615389161817956217069956419934936560811249088292254641")}, std::complex<T>{stof<T>("5.988687094436881345811310662738584446203671990155887283914284001"),stof<T>("45.889135226615389161817956217069956419934936560811249088292254641")}, std::complex<T>{stof<T>("-27.821772621708670837621711155546408579273887813724922461276014238"),stof<T>("13.066340812511515257022509915181439268315224870546344330326080799")}, std::complex<T>{stof<T>("10.0373077107763664455840680704940522306111730998214863873040819287"),stof<T>("0.6110606900999232603504525665926984133723127941757514587983885366")}, std::complex<T>{stof<T>("-10.0373077107763664455840680704940522306111730998214863873040819287"),stof<T>("-0.6110606900999232603504525665926984133723127941757514587983885366")}, std::complex<T>{stof<T>("31.243007577102985441287065850207529578167952986563514966608129078"),stof<T>("-15.552619293020226531691557410794602189997768778945442903791245509")}, std::complex<T>{stof<T>("10.4143358590343284804290219500691765260559843288545049888693763595"),stof<T>("-5.184206431006742177230519136931534063332589592981814301263748503")}, std::complex<T>{stof<T>("10.4143358590343284804290219500691765260559843288545049888693763595"),stof<T>("-5.184206431006742177230519136931534063332589592981814301263748503")}, std::complex<T>{stof<T>("-10.4143358590343284804290219500691765260559843288545049888693763595"),stof<T>("5.184206431006742177230519136931534063332589592981814301263748503")}, std::complex<T>{stof<T>("4.1196055750861827043862642707354655921316033428964657066622416124"),stof<T>("-1.344625256964207747826567626087918400815070959482346241736480892")}, std::complex<T>{stof<T>("4.1196055750861827043862642707354655921316033428964657066622416124"),stof<T>("-1.344625256964207747826567626087918400815070959482346241736480892")}, std::complex<T>{stof<T>("4.1196055750861827043862642707354655921316033428964657066622416124"),stof<T>("-1.344625256964207747826567626087918400815070959482346241736480892")}, std::complex<T>{stof<T>("-4.1196055750861827043862642707354655921316033428964657066622416124"),stof<T>("1.344625256964207747826567626087918400815070959482346241736480892")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[124].real()/kbase.W[124].real()), rlog(k.W[127].real()/kbase.W[127].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_626_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_626_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,4> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (8 + -7 * v[0] + 3 * v[1] + 5 * v[2] + v[3] + -4 * v[4] + -2 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -5 * v[5]) * (-v[0] + v[1] + -v[2] + -v[3] + v[5])) / prod_pow(tend, 2);
c[1] = ((-1 + m1_set::bc<T>[1]) * (T(3) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[2] = ((1 + m1_set::bc<T>[1]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;
c[3] = (3 * m1_set::bc<T>[1] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return t * (abb[47] * c[0] + abb[49] * c[0] + -abb[51] * c[0] + abb[58] * c[0] + abb[50] * (c[0] + -c[1]) + abb[60] * (c[0] + -c[1]) + abb[52] * c[1] + abb[54] * (-3 * c[0] + 2 * c[1])) + abb[47] * c[2] + abb[49] * c[2] + -abb[51] * c[2] + abb[58] * c[2] + abb[50] * (c[2] + -c[3]) + abb[60] * (c[2] + -c[3]) + abb[52] * c[3] + abb[54] * (-3 * c[2] + 2 * c[3]);
	}
	{
T z[7];
z[0] = -abb[31] + abb[34];
z[1] = -abb[35] + abb[36];
z[2] = z[0] * z[1];
z[2] = -abb[38] + z[2];
z[3] = prod_pow(abb[35], 2);
z[4] = prod_pow(abb[36], 2);
z[3] = z[3] + -z[4];
z[4] = z[2] + z[3];
z[5] = 2 * z[4];
z[6] = abb[49] + abb[58];
z[5] = z[5] * z[6];
z[0] = -2 * z[0];
z[0] = z[0] * z[1];
z[0] = 2 * abb[38] + z[0] + z[3];
z[1] = -abb[50] + -abb[60];
z[0] = z[0] * z[1];
z[1] = abb[47] + -abb[51];
z[1] = z[1] * z[4];
z[2] = -abb[54] * z[2];
z[3] = abb[52] * z[3];
z[0] = z[0] + 2 * z[1] + 6 * z[2] + 3 * z[3] + z[5];
return abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_626_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[47] + abb[49] + abb[50] + -abb[51] + -3 * abb[54] + abb[58] + abb[60]) * (t * c[0] + -c[1]);
	}
	{
T z[2];
z[0] = -abb[47] + -abb[49] + -abb[50] + abb[51] + 3 * abb[54] + -abb[58] + -abb[60];
z[1] = abb[35] + -abb[36];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_626_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,4> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (v[2] + v[3]) * (16 + 8 * v[0] + 4 * v[1] + -3 * v[2] + -7 * v[3] + -4 * v[4] + 4 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 8 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(4)) * (v[2] + v[3]) * (-8 * v[0] + -4 * v[1] + -m1_set::bc<T>[1] * (-4 + v[2] + v[3]) + 4 * (-1 + v[3] + v[4]) + 2 * (-1 + m1_set::bc<T>[1]) * v[5])) / prod_pow(tend, 2);
c[2] = (v[2] + v[3] + -2 * m1_set::bc<T>[1] * (v[2] + v[3])) / tend;
c[3] = ((-2 + m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;


		return abb[50] * (t * c[0] + c[2]) + -abb[61] * (t * c[0] + c[2]) + abb[55] * (t * c[0] + -t * c[1] + c[2] + -c[3]) + abb[52] * (t * c[1] + c[3]);
	}
	{
T z[4];
z[0] = abb[32] + -abb[34];
z[1] = abb[31] + -abb[35];
z[1] = 2 * z[1];
z[0] = z[0] * z[1];
z[0] = 2 * abb[37] + z[0];
z[1] = -abb[50] + abb[61];
z[2] = 2 * abb[52] + -3 * abb[55] + z[1];
z[0] = z[0] * z[2];
z[2] = -prod_pow(abb[32], 2);
z[3] = prod_pow(abb[34], 2);
z[2] = z[2] + z[3];
z[1] = -abb[52] + z[1];
z[1] = z[1] * z[2];
z[0] = z[0] + z[1];
return abb[12] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_626_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[50] + -2 * abb[52] + 3 * abb[55] + -abb[61]) * (t * c[0] + -c[1]);
	}
	{
T z[2];
z[0] = abb[50] + -2 * abb[52] + 3 * abb[55] + -abb[61];
z[1] = abb[32] + -abb[34];
return 2 * abb[12] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_626_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-65.149604393954937216805904398696147287975448257796088752402797246"),stof<T>("45.574670308000932584279572775574979786579589927248862203873144988")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,78> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W17(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[14].real()/k.W[14].real()), IntDLogL_W_17(t,k,kend,dl), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[124].real()/k.W[124].real()), rlog(kend.W[127].real()/k.W[127].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), T{0}, T{0}};
abb[45] = SpDLog_f_4_626_W_17_Im(t, path, abb);
abb[46] = SpDLog_f_4_626_W_19_Im(t, path, abb);
abb[76] = SpDLog_f_4_626_W_17_Re(t, path, abb);
abb[77] = SpDLog_f_4_626_W_19_Re(t, path, abb);

                    
            return f_4_626_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_626_DLogXconstant_part(base_point<T>, kend);
	value += f_4_626_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_626_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_626_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_626_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_626_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_626_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_626_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
