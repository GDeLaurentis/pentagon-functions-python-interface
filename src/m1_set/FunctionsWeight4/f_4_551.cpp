/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_551.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_551_abbreviated (const std::array<T,29>& abb) {
T z[27];
z[0] = abb[20] * (T(3) / T(2));
z[1] = abb[19] + abb[23];
z[2] = 2 * abb[21];
z[3] = z[0] + (T(1) / T(2)) * z[1] + -z[2];
z[3] = abb[0] * z[3];
z[4] = abb[7] + abb[8];
z[5] = abb[24] + -abb[25];
z[6] = (T(1) / T(4)) * z[5];
z[4] = z[4] * z[6];
z[6] = abb[23] * (T(1) / T(2));
z[7] = abb[20] * (T(1) / T(2)) + z[6];
z[8] = abb[22] + abb[19] * (T(1) / T(2));
z[9] = z[7] + -z[8];
z[10] = abb[2] * (T(1) / T(2));
z[10] = z[9] * z[10];
z[4] = z[4] + -z[10];
z[10] = 3 * abb[22];
z[11] = abb[23] * (T(-3) / T(2)) + abb[19] * (T(7) / T(2)) + z[10];
z[12] = abb[20] * (T(5) / T(4)) + -z[2];
z[11] = (T(1) / T(2)) * z[11] + z[12];
z[11] = abb[3] * z[11];
z[13] = abb[19] + abb[22];
z[14] = -abb[23] + 3 * z[13];
z[15] = abb[20] + -z[2] + (T(1) / T(2)) * z[14];
z[16] = abb[6] * z[15];
z[17] = -abb[23] + z[13];
z[18] = abb[1] * z[17];
z[19] = z[16] + -2 * z[18];
z[11] = z[3] + -z[4] + z[11] + -z[19];
z[11] = abb[12] * z[11];
z[7] = -z[2] + z[7] + z[8];
z[7] = abb[0] * z[7];
z[20] = abb[23] * (T(5) / T(2)) + -z[8];
z[12] = z[12] + (T(1) / T(2)) * z[20];
z[12] = abb[3] * z[12];
z[7] = -z[4] + -z[7] + z[12] + z[19];
z[12] = abb[13] * z[7];
z[19] = 4 * abb[21];
z[20] = 2 * abb[20] + -z[19];
z[21] = abb[23] + z[13] + z[20];
z[21] = abb[0] * z[21];
z[14] = z[14] + z[20];
z[20] = abb[6] * z[14];
z[22] = 2 * z[17];
z[22] = abb[3] * z[22];
z[21] = 4 * z[18] + -z[20] + z[21] + z[22];
z[22] = abb[14] * z[21];
z[11] = z[11] + z[12] + z[22];
z[12] = -abb[19] + -abb[20] + abb[23];
z[23] = -abb[4] * z[12];
z[24] = z[18] + z[23];
z[10] = abb[19] * (T(5) / T(2)) + -z[6] + z[10];
z[25] = abb[20] * (T(3) / T(4)) + -z[2] + (T(1) / T(2)) * z[10];
z[25] = abb[3] * z[25];
z[3] = z[3] + z[4] + -z[16] + z[24] + z[25];
z[3] = abb[11] * z[3];
z[3] = z[3] + -z[11];
z[3] = abb[11] * z[3];
z[16] = -z[18] + (T(-5) / T(3)) * z[23];
z[1] = z[1] + -z[19];
z[25] = -abb[20] + (T(-1) / T(3)) * z[1];
z[25] = abb[0] * z[25];
z[26] = abb[21] * (T(4) / T(3));
z[13] = abb[23] * (T(-1) / T(3)) + abb[20] * (T(2) / T(3)) + z[13] + -z[26];
z[13] = abb[6] * z[13];
z[26] = -abb[22] + abb[20] * (T(-5) / T(12)) + abb[19] * (T(-3) / T(4)) + abb[23] * (T(1) / T(12)) + z[26];
z[26] = abb[3] * z[26];
z[4] = -z[4] + z[13] + (T(1) / T(2)) * z[16] + z[25] + z[26];
z[4] = prod_pow(m1_set::bc<T>[0], 2) * z[4];
z[7] = abb[12] * z[7];
z[13] = -abb[0] + abb[3];
z[13] = z[9] * z[13];
z[16] = (T(1) / T(2)) * z[5];
z[25] = abb[7] * z[16];
z[13] = z[13] + -z[18] + -z[25];
z[13] = abb[13] * z[13];
z[7] = z[7] + z[13];
z[7] = abb[13] * z[7];
z[13] = abb[20] + -abb[22];
z[13] = abb[0] * z[13];
z[17] = abb[3] * z[17];
z[13] = z[13] + z[17] + z[18] + -z[23];
z[13] = prod_pow(abb[14], 2) * z[13];
z[1] = 3 * abb[20] + z[1];
z[1] = abb[0] * z[1];
z[1] = z[1] + -z[20];
z[14] = abb[3] * z[14];
z[14] = z[1] + z[14] + 3 * z[18] + z[23];
z[17] = -abb[26] * z[14];
z[20] = -abb[15] * z[21];
z[21] = -abb[2] + -abb[3];
z[23] = (T(3) / T(2)) * z[5];
z[21] = z[21] * z[23];
z[23] = abb[8] * z[9];
z[26] = abb[0] * z[5];
z[21] = z[21] + 3 * z[23] + -z[26];
z[6] = z[6] + 3 * z[8];
z[2] = abb[20] * (T(1) / T(4)) + -z[2] + (T(1) / T(2)) * z[6];
z[2] = abb[7] * z[2];
z[6] = abb[9] * z[15];
z[5] = abb[10] * z[5];
z[2] = z[2] + -z[5] + -z[6] + (T(-1) / T(2)) * z[21];
z[5] = -abb[27] * z[2];
z[6] = abb[2] * z[9];
z[8] = abb[8] * z[16];
z[6] = z[6] + -z[8];
z[8] = abb[0] * z[12];
z[8] = z[6] + (T(1) / T(2)) * z[8] + z[18];
z[8] = abb[12] * z[8];
z[8] = z[8] + z[22];
z[8] = abb[12] * z[8];
z[3] = abb[28] + z[3] + z[4] + z[5] + z[7] + z[8] + z[13] + z[17] + z[20];
z[0] = -z[0] + -z[10] + z[19];
z[0] = abb[3] * z[0];
z[0] = z[0] + -z[1] + z[6] + -2 * z[24] + -z[25];
z[0] = abb[11] * z[0];
z[0] = z[0] + z[11];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = -abb[16] * z[14];
z[2] = -abb[17] * z[2];
z[0] = abb[18] + z[0] + z[1] + z[2];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_551_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("2.1074070396448583711487721334653896670080473931271924297839254653"),stof<T>("6.290617103617641190773064000823451480867423959630600136098352482")}, std::complex<T>{stof<T>("8.474314595464606516609840836557332407243567628329567014703344594"),stof<T>("15.317960766164836909300150850477036083318378421706943677724509143")}, std::complex<T>{stof<T>("-12.733815111639496290922137406183885480471040470404749169838838257"),stof<T>("-18.054687325094391437054173699307169204901908924152687083252313322")}, std::complex<T>{stof<T>("4.2595005161748897743122965696265530732274728420751821551354936634"),stof<T>("2.736726558929554527754022848830133121583530502445743405527804179")}, std::complex<T>{stof<T>("-1.8875214468817965659845470056534843067331750307489830546406995373"),stof<T>("3.073952256592314428553215974881268771197457590719930264362430233")}, std::complex<T>{stof<T>("1.8875214468817965659845470056534843067331750307489830546406995373"),stof<T>("-3.073952256592314428553215974881268771197457590719930264362430233")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[126].real()/kbase.W[126].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_551_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_551_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,4> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (v[1] + v[2] + -v[3] + -v[4]) * (-8 + 4 * v[0] + 3 * v[1] + -v[2] + v[3] + v[4] + 2 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 4 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(8)) * (v[1] + v[2] + -v[3] + -v[4]) * (8 + 12 * v[0] + -v[1] + 3 * v[2] + 5 * v[3] + -3 * v[4] + -2 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + -4 * v[5])) / prod_pow(tend, 2);
c[2] = ((1 + m1_set::bc<T>[1]) * (T(1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;
c[3] = (-(-1 + m1_set::bc<T>[1]) * (v[1] + v[2] + -1 * v[3] + -1 * v[4])) / tend;


		return t * abb[22] * c[0] + -2 * t * abb[21] * c[1] + abb[22] * c[2] + abb[19] * (t * c[0] + c[2]) + -2 * abb[21] * c[3] + abb[20] * (t * c[1] + c[3]) + abb[23] * (-t * c[0] + t * c[1] + -c[2] + c[3]);
	}
	{
T z[4];
z[0] = abb[11] + -abb[12];
z[1] = abb[19] + abb[22];
z[2] = 2 * abb[20] + -4 * abb[21] + abb[23] + z[1];
z[0] = z[0] * z[2];
z[1] = -abb[23] + z[1];
z[3] = abb[13] * z[1];
z[3] = z[0] + z[3];
z[3] = abb[13] * z[3];
z[1] = -abb[14] * z[1];
z[0] = -z[0] + z[1];
z[0] = abb[14] * z[0];
z[1] = -abb[15] * z[2];
z[0] = z[0] + z[1] + z[3];
return abb[5] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_551_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(8)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (abb[19] + 2 * abb[20] + -4 * abb[21] + abb[22] + abb[23]) * (2 * t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = -abb[19] + -2 * abb[20] + 4 * abb[21] + -abb[22] + -abb[23];
z[1] = abb[13] + -abb[14];
return abb[5] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_551_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.6540504437947780253475540265522262905274433574931339254392769478"),stof<T>("-10.6493270728263610057947163945733774452071741169954500111973046361")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dlog_W4(k,dl), dlog_W8(k,dl), dl[3], dlog_W14(k,dl), dlog_W20(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W127(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_2_im(k), f_2_24_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[12].real()/k.W[12].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[126].real()/k.W[126].real()), f_2_2_re(k), f_2_24_re(k), T{0}};
abb[18] = SpDLog_f_4_551_W_20_Im(t, path, abb);
abb[28] = SpDLog_f_4_551_W_20_Re(t, path, abb);

                    
            return f_4_551_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_551_DLogXconstant_part(base_point<T>, kend);
	value += f_4_551_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_551_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_551_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_551_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_551_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_551_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_551_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
