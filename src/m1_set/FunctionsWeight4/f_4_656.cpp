/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_656.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_656_abbreviated (const std::array<T,68>& abb) {
T z[155];
z[0] = 2 * abb[54];
z[1] = 2 * abb[51];
z[2] = z[0] + -z[1];
z[3] = 5 * abb[46];
z[4] = abb[50] + z[3];
z[5] = 3 * abb[56];
z[6] = -abb[47] + z[5];
z[7] = abb[45] + -abb[55];
z[8] = 2 * abb[48];
z[9] = z[2] + -z[4] + z[6] + z[7] + -z[8];
z[9] = abb[9] * z[9];
z[10] = 2 * abb[50];
z[11] = -z[0] + z[10];
z[12] = 8 * abb[46];
z[13] = 6 * abb[48];
z[14] = -abb[55] + -abb[56] + -z[1] + z[11] + z[12] + z[13];
z[14] = abb[5] * z[14];
z[15] = 3 * abb[50];
z[16] = -z[0] + z[15];
z[17] = z[7] + z[16];
z[18] = 8 * abb[48];
z[19] = 2 * abb[56];
z[20] = z[17] + z[18] + -z[19];
z[21] = abb[1] * z[20];
z[22] = abb[54] * (T(1) / T(2));
z[23] = abb[49] * (T(1) / T(2));
z[24] = z[22] + z[23];
z[25] = abb[47] + z[24];
z[26] = 2 * abb[46];
z[27] = z[8] + z[26];
z[28] = -abb[50] + abb[55];
z[29] = (T(1) / T(2)) * z[28];
z[30] = -abb[51] + abb[56] + -z[25] + z[27] + -z[29];
z[30] = abb[7] * z[30];
z[31] = -abb[49] + abb[54];
z[32] = z[28] + z[31];
z[33] = -abb[51] + (T(1) / T(2)) * z[32];
z[33] = abb[4] * z[33];
z[34] = 2 * abb[1];
z[35] = -abb[13] + z[34];
z[35] = z[1] * z[35];
z[36] = abb[45] * (T(1) / T(2));
z[37] = z[29] + -z[36];
z[38] = -z[8] + z[37];
z[39] = abb[13] * z[38];
z[40] = abb[57] + abb[58];
z[41] = abb[18] * z[40];
z[39] = z[39] + (T(1) / T(2)) * z[41];
z[42] = abb[59] + abb[60];
z[43] = abb[21] * z[42];
z[44] = (T(1) / T(2)) * z[43];
z[45] = z[39] + z[44];
z[46] = abb[25] * z[42];
z[47] = -abb[45] + z[28];
z[48] = abb[15] * z[47];
z[46] = z[46] + z[48];
z[48] = (T(1) / T(2)) * z[46];
z[49] = 5 * abb[1];
z[50] = -abb[13] + z[49];
z[50] = z[26] * z[50];
z[51] = abb[2] * z[47];
z[52] = abb[19] * z[40];
z[53] = abb[46] + -abb[49];
z[54] = abb[0] * z[53];
z[55] = abb[45] + abb[50];
z[56] = -abb[56] + z[55];
z[57] = -abb[6] * z[56];
z[9] = z[9] + z[14] + z[21] + z[30] + z[33] + -z[35] + z[45] + -z[48] + z[50] + z[51] + (T(-1) / T(2)) * z[52] + z[54] + z[57];
z[9] = prod_pow(abb[28], 2) * z[9];
z[14] = 3 * abb[46] + -abb[51];
z[21] = z[8] + z[14] + -z[24] + -z[28] + z[36];
z[21] = abb[0] * z[21];
z[30] = 7 * abb[50];
z[50] = -abb[55] + z[30];
z[54] = -z[5] + (T(1) / T(2)) * z[50];
z[57] = 4 * abb[51];
z[58] = 10 * abb[46] + -z[57];
z[59] = abb[45] * (T(3) / T(2));
z[60] = -z[0] + z[18] + z[54] + z[58] + z[59];
z[60] = abb[5] * z[60];
z[24] = abb[56] + z[24];
z[61] = -abb[47] + z[24];
z[62] = 4 * abb[46];
z[63] = 4 * abb[48];
z[64] = z[62] + z[63];
z[65] = 2 * abb[53];
z[66] = -abb[45] + z[65];
z[67] = 3 * abb[51];
z[68] = (T(-5) / T(2)) * z[28] + z[61] + z[64] + -z[66] + -z[67];
z[68] = abb[7] * z[68];
z[69] = 3 * abb[54];
z[70] = 10 * abb[48] + abb[49];
z[71] = 2 * abb[45];
z[72] = -abb[50] + abb[56];
z[73] = -z[69] + z[70] + z[71] + -4 * z[72];
z[73] = abb[1] * z[73];
z[74] = abb[20] * z[42];
z[43] = z[43] + z[74];
z[75] = z[27] + -z[31];
z[76] = abb[45] + z[75];
z[77] = 2 * z[72];
z[78] = -z[76] + z[77];
z[79] = abb[14] * z[78];
z[80] = z[43] + z[79];
z[81] = abb[49] + abb[54];
z[82] = -z[28] + z[81];
z[83] = abb[8] * z[82];
z[84] = abb[23] * z[42];
z[83] = -z[83] + z[84];
z[84] = z[80] + z[83];
z[17] = -z[1] + z[17];
z[85] = -9 * abb[46] + z[6] + -z[13] + -z[17];
z[85] = abb[9] * z[85];
z[86] = 3 * abb[55];
z[87] = -abb[50] + z[86];
z[87] = (T(1) / T(2)) * z[87];
z[24] = abb[53] + -z[24] + z[87];
z[24] = abb[6] * z[24];
z[88] = abb[22] + abb[24];
z[88] = z[42] * z[88];
z[89] = (T(1) / T(2)) * z[88];
z[90] = abb[17] * z[40];
z[91] = (T(1) / T(2)) * z[90];
z[92] = z[89] + -z[91];
z[93] = abb[49] + z[8];
z[94] = -z[0] + z[93];
z[95] = abb[12] * z[94];
z[96] = -abb[51] * z[49];
z[97] = 11 * abb[1];
z[98] = -abb[12] + z[97];
z[98] = abb[46] * z[98];
z[99] = 2 * abb[12];
z[100] = -abb[1] + -2 * abb[8] + z[99];
z[101] = -abb[53] * z[100];
z[21] = z[21] + -z[24] + -z[33] + z[60] + z[68] + z[73] + z[84] + z[85] + z[92] + -z[95] + z[96] + z[98] + z[101];
z[21] = abb[30] * z[21];
z[60] = -abb[55] + z[71];
z[68] = -4 * abb[50] + z[0] + z[5] + -z[60] + -z[70];
z[68] = abb[1] * z[68];
z[70] = abb[12] * abb[49];
z[68] = z[68] + z[70];
z[70] = -z[8] + z[81];
z[73] = z[28] + -z[62] + z[70];
z[73] = abb[0] * z[73];
z[85] = 6 * abb[46];
z[96] = z[13] + z[85];
z[98] = 2 * abb[47];
z[101] = z[96] + -z[98];
z[102] = -z[57] + z[101];
z[103] = -abb[45] + z[81];
z[104] = -abb[56] + z[28];
z[104] = -z[102] + z[103] + 2 * z[104];
z[105] = abb[7] * z[104];
z[58] = z[20] + z[58];
z[106] = 2 * abb[5];
z[107] = z[58] * z[106];
z[11] = 7 * abb[46] + -z[6] + z[11] + z[63];
z[108] = 2 * abb[9];
z[11] = z[11] * z[108];
z[109] = 2 * abb[13];
z[97] = abb[12] + z[97] + -z[109];
z[97] = z[26] * z[97];
z[110] = 3 * abb[1] + -abb[13];
z[110] = z[57] * z[110];
z[111] = -z[47] + z[63];
z[112] = abb[13] * z[111];
z[41] = -z[41] + z[112];
z[112] = abb[16] * z[40];
z[113] = z[41] + -z[112];
z[114] = -abb[55] + z[19];
z[115] = -z[55] + z[114];
z[116] = abb[6] * z[115];
z[68] = z[11] + 2 * z[68] + z[73] + -z[80] + -z[97] + z[105] + -z[107] + z[110] + z[113] + -z[116];
z[73] = abb[28] + -abb[31];
z[68] = z[68] * z[73];
z[73] = abb[52] + z[6] + -z[86];
z[73] = z[73] * z[108];
z[32] = -z[1] + z[32];
z[97] = abb[4] * z[32];
z[73] = z[73] + -z[97];
z[105] = 6 * abb[51];
z[107] = 5 * abb[55];
z[110] = 2 * abb[52];
z[117] = z[105] + -z[107] + z[110];
z[118] = -3 * abb[49] + -z[19] + z[64];
z[119] = abb[50] + -abb[54] + z[117] + -z[118];
z[119] = abb[14] * z[119];
z[120] = -abb[54] + z[86];
z[121] = -abb[49] + -abb[50] + z[65] + z[120];
z[122] = -z[98] + z[110];
z[123] = z[19] + z[122];
z[124] = z[121] + -z[123];
z[124] = abb[7] * z[124];
z[125] = -abb[55] + abb[56];
z[126] = abb[54] + z[125];
z[126] = z[34] * z[126];
z[127] = abb[1] + abb[8];
z[127] = z[65] * z[127];
z[128] = abb[1] * z[1];
z[127] = z[127] + z[128];
z[121] = -z[19] + z[121];
z[121] = abb[6] * z[121];
z[119] = -z[43] + -z[73] + -z[83] + z[119] + z[121] + z[124] + z[126] + -z[127];
z[119] = abb[32] * z[119];
z[121] = -abb[45] + -z[27] + 3 * z[28];
z[124] = 2 * abb[49];
z[129] = -z[65] + z[124];
z[130] = z[121] + -z[129];
z[130] = abb[7] * z[130];
z[84] = -z[84] + z[130];
z[130] = abb[5] * z[115];
z[131] = -z[88] + z[90];
z[130] = z[130] + z[131];
z[132] = abb[6] * z[47];
z[133] = z[130] + 2 * z[132];
z[134] = z[34] * z[125];
z[135] = -z[28] + z[75];
z[136] = abb[0] * z[135];
z[137] = abb[8] * z[65];
z[134] = -z[84] + -z[133] + -z[134] + z[136] + z[137];
z[138] = -abb[29] * z[134];
z[21] = z[21] + z[68] + z[119] + z[138];
z[21] = abb[30] * z[21];
z[68] = abb[46] + abb[48];
z[119] = abb[56] * (T(1) / T(3));
z[138] = abb[54] * (T(1) / T(3));
z[139] = abb[45] * (T(1) / T(3));
z[140] = abb[50] * (T(5) / T(3));
z[141] = abb[55] + -z[140];
z[141] = abb[53] * (T(5) / T(6)) + -z[23] + (T(-1) / T(6)) * z[68] + z[119] + -z[138] + -z[139] + (T(1) / T(2)) * z[141];
z[141] = abb[6] * z[141];
z[142] = z[30] + z[107];
z[142] = -abb[47] + -4 * abb[49] + 5 * abb[53] + abb[45] * (T(7) / T(4)) + -z[0] + -z[19] + z[27] + (T(1) / T(4)) * z[142];
z[142] = abb[7] * z[142];
z[46] = z[46] + z[52];
z[143] = 4 * abb[54];
z[144] = -z[63] + z[143];
z[145] = -z[23] + -z[144];
z[145] = abb[12] * z[145];
z[146] = 4 * abb[12];
z[147] = -5 * abb[8] + abb[1] * (T(-5) / T(2)) + z[146];
z[147] = abb[53] * z[147];
z[142] = -z[41] + z[46] + z[142] + z[145] + z[147];
z[140] = -z[86] + z[140];
z[145] = abb[49] * (T(1) / T(6));
z[147] = abb[45] * (T(1) / T(4));
z[119] = abb[48] + abb[54] * (T(2) / T(3)) + z[119] + (T(1) / T(4)) * z[140] + z[145] + z[147];
z[119] = abb[1] * z[119];
z[140] = z[30] + -z[107];
z[148] = 5 * abb[45];
z[140] = -abb[56] + abb[48] * (T(5) / T(2)) + -z[23] + -z[57] + (T(1) / T(2)) * z[140] + z[148];
z[140] = abb[46] + (T(1) / T(3)) * z[140];
z[140] = abb[5] * z[140];
z[149] = -abb[55] + abb[50] * (T(7) / T(3));
z[149] = -abb[51] + abb[49] * (T(1) / T(3)) + abb[48] * (T(7) / T(3)) + (T(1) / T(2)) * z[149];
z[138] = abb[46] + abb[47] * (T(-1) / T(3)) + -z[138] + z[147] + (T(1) / T(2)) * z[149];
z[138] = abb[0] * z[138];
z[50] = abb[54] + z[50];
z[50] = abb[56] + abb[51] * (T(-2) / T(3)) + (T(-1) / T(6)) * z[50] + -z[139] + z[145];
z[50] = abb[3] * z[50];
z[139] = z[83] + -z[112];
z[145] = abb[13] * (T(4) / T(3));
z[147] = abb[1] * (T(-3) / T(2)) + z[145];
z[147] = abb[51] * z[147];
z[145] = abb[1] + abb[12] * (T(3) / T(2)) + -z[145];
z[145] = abb[46] * z[145];
z[50] = -z[44] + z[50] + (T(-5) / T(12)) * z[51] + -z[92] + z[119] + z[138] + (T(-5) / T(6)) * z[139] + z[140] + z[141] + (T(1) / T(3)) * z[142] + z[145] + z[147];
z[50] = prod_pow(m1_set::bc<T>[0], 2) * z[50];
z[92] = -abb[45] + z[1];
z[63] = -z[63] + z[77] + z[81] + -z[85] + z[92];
z[63] = abb[0] * z[63];
z[77] = 4 * abb[56];
z[119] = abb[49] + z[77];
z[138] = 12 * abb[48];
z[139] = 11 * abb[50] + -z[86];
z[140] = (T(1) / T(2)) * z[139];
z[69] = -16 * abb[46] + abb[45] * (T(-5) / T(2)) + z[69] + z[105] + z[119] + -z[138] + -z[140];
z[69] = abb[5] * z[69];
z[105] = z[52] + z[88];
z[141] = 2 * z[51];
z[48] = z[48] + -z[141];
z[105] = z[48] + (T(1) / T(2)) * z[105];
z[142] = (T(3) / T(2)) * z[90];
z[145] = z[105] + -z[142];
z[20] = -z[20] * z[34];
z[147] = z[15] + -z[81];
z[102] = z[60] + z[102] + z[147];
z[149] = -abb[7] * z[102];
z[150] = z[19] + z[36];
z[151] = abb[50] + z[86];
z[151] = z[75] + -z[150] + (T(1) / T(2)) * z[151];
z[151] = abb[6] * z[151];
z[79] = z[74] + z[79];
z[152] = 4 * abb[1] + -abb[13];
z[152] = z[1] * z[152];
z[153] = -10 * abb[1] + abb[13];
z[153] = z[26] * z[153];
z[11] = z[11] + z[20] + -z[45] + z[63] + z[69] + -z[79] + (T(1) / T(2)) * z[112] + -z[145] + z[149] + z[151] + z[152] + z[153];
z[11] = abb[28] * z[11];
z[20] = z[12] + z[147];
z[13] = -z[13] + z[19] + -z[20] + z[57] + -z[60];
z[13] = abb[0] * z[13];
z[58] = abb[1] * z[58];
z[63] = 3 * abb[48];
z[69] = 3 * abb[45];
z[147] = -abb[49] + z[62] + z[63] + -z[69];
z[147] = z[106] * z[147];
z[149] = -abb[55] + z[15];
z[151] = -z[19] + z[69] + z[149];
z[152] = -abb[7] * z[151];
z[153] = -abb[45] + z[75];
z[154] = -abb[6] * z[153];
z[13] = z[13] + -z[51] + z[58] + -z[112] + z[147] + z[152] + z[154];
z[13] = abb[31] * z[13];
z[11] = z[11] + z[13];
z[11] = abb[31] * z[11];
z[13] = 6 * abb[56];
z[58] = z[13] + -z[98];
z[147] = 5 * abb[50];
z[7] = 18 * abb[46] + z[7] + -z[57] + -z[58] + z[138] + -z[143] + z[147];
z[7] = abb[5] * z[7];
z[138] = -z[13] + z[30];
z[143] = 18 * abb[48] + z[69] + -z[86] + z[110] + z[138] + -z[143];
z[143] = abb[1] * z[143];
z[13] = -z[13] + z[18];
z[18] = z[13] + z[20] + z[71] + -z[117];
z[18] = abb[14] * z[18];
z[18] = z[18] + -z[97];
z[10] = -13 * abb[46] + -5 * abb[52] + 7 * abb[55] + -z[10] + -z[57] + z[71] + z[144];
z[10] = abb[9] * z[10];
z[17] = z[17] + z[96] + -z[110];
z[17] = abb[7] * z[17];
z[20] = -z[49] + z[109];
z[20] = z[1] * z[20];
z[152] = 6 * abb[1] + -abb[13];
z[152] = z[62] * z[152];
z[4] = abb[52] + -2 * abb[55] + z[4] + -z[70];
z[4] = abb[0] * z[4];
z[4] = z[4] + z[7] + z[10] + z[17] + -z[18] + z[20] + -z[41] + z[43] + -z[131] + z[143] + z[152];
z[4] = abb[34] * z[4];
z[7] = -z[26] + z[57] + -z[111] + -z[124];
z[7] = abb[5] * z[7];
z[10] = 5 * abb[49];
z[17] = 2 * z[28];
z[20] = 4 * abb[45] + z[8] + z[10] + -z[17];
z[20] = abb[1] * z[20];
z[41] = -z[65] + z[82];
z[70] = abb[7] * z[41];
z[70] = z[70] + z[83];
z[82] = abb[1] + -abb[8] + z[99];
z[82] = z[65] * z[82];
z[10] = z[10] + z[144];
z[10] = abb[12] * z[10];
z[99] = -abb[1] + abb[13];
z[111] = z[57] * z[99];
z[143] = 2 * abb[0];
z[53] = z[53] * z[143];
z[49] = 9 * abb[12] + -4 * abb[13] + -z[49];
z[49] = abb[46] * z[49];
z[7] = -z[7] + -z[10] + z[20] + z[49] + z[53] + -z[70] + z[82] + z[111] + -z[113] + z[131];
z[10] = abb[62] * z[7];
z[20] = -abb[52] + abb[55] * (T(1) / T(2));
z[49] = abb[50] * (T(1) / T(2));
z[53] = abb[54] + -z[20] + -z[49] + -z[67] + z[68] + -z[98] + -z[124] + z[150];
z[53] = abb[7] * z[53];
z[82] = -abb[48] + abb[51];
z[111] = abb[55] * (T(7) / T(2));
z[144] = abb[50] * (T(3) / T(2));
z[3] = -abb[52] + z[3] + z[59] + -5 * z[82] + z[111] + -z[119] + z[144];
z[3] = abb[14] * z[3];
z[49] = z[49] + z[68];
z[20] = -abb[49] + -abb[51] + z[20] + z[36] + z[49];
z[20] = abb[0] * z[20];
z[82] = -abb[5] * z[104];
z[63] = -abb[54] + z[63];
z[104] = abb[50] + -abb[52] + z[63];
z[104] = z[34] * z[104];
z[85] = abb[1] * z[85];
z[3] = z[3] + z[20] + z[53] + z[82] + z[85] + -z[88] + -z[97] + z[104] + -z[128];
z[3] = abb[37] * z[3];
z[20] = abb[46] + z[65];
z[53] = -abb[1] + abb[12];
z[20] = z[20] * z[53];
z[53] = z[47] + -z[94];
z[53] = abb[1] * z[53];
z[82] = -abb[5] * z[153];
z[85] = -abb[7] * z[47];
z[94] = abb[45] + -abb[49] + abb[53] + -z[68];
z[94] = abb[6] * z[94];
z[20] = z[20] + z[51] + z[53] + z[82] + z[85] + -z[90] + 2 * z[94] + z[95] + z[136];
z[20] = abb[29] * z[20];
z[53] = -abb[12] + z[99];
z[53] = z[26] * z[53];
z[82] = abb[12] * z[124];
z[35] = z[35] + z[53] + z[82] + (T(-3) / T(2)) * z[112];
z[53] = z[56] + z[93];
z[53] = z[34] * z[53];
z[45] = z[35] + -z[45] + -z[53];
z[85] = -z[1] + -z[31] + z[64];
z[15] = abb[55] + z[15];
z[15] = (T(1) / T(2)) * z[15] + -z[19] + z[36] + z[85];
z[15] = abb[5] * z[15];
z[90] = (T(-3) / T(2)) * z[28] + z[36] + z[75];
z[90] = abb[6] * z[90];
z[94] = -z[31] + z[92];
z[94] = abb[0] * z[94];
z[95] = abb[7] * z[135];
z[15] = z[15] + -z[45] + z[79] + z[90] + z[91] + z[94] + z[95] + z[105];
z[15] = abb[28] * z[15];
z[59] = -z[29] + z[59];
z[90] = z[59] + -z[75];
z[90] = abb[6] * z[90];
z[29] = z[29] + z[36];
z[85] = -z[29] + z[85];
z[85] = abb[5] * z[85];
z[85] = -z[85] + z[90];
z[90] = -z[1] + z[27];
z[91] = z[90] + -z[115];
z[91] = abb[0] * z[91];
z[95] = abb[7] * z[115];
z[45] = z[45] + z[85] + z[91] + -z[95] + z[145];
z[45] = abb[31] * z[45];
z[91] = abb[32] * z[134];
z[15] = z[15] + z[20] + z[45] + z[91];
z[15] = abb[29] * z[15];
z[12] = z[12] + z[13] + z[67] + -z[103] + -z[110] + z[139];
z[12] = abb[7] * z[12];
z[13] = z[31] + z[57] + z[60] + z[138];
z[20] = abb[5] * z[13];
z[13] = abb[3] * z[13];
z[60] = -z[47] + z[90];
z[67] = z[60] + z[122];
z[67] = abb[0] * z[67];
z[12] = z[12] + -z[13] + -z[18] + z[20] + z[67] + z[74] + z[88];
z[16] = 4 * abb[52] + -z[16] + z[58] + z[92] + -z[96] + -z[107];
z[16] = abb[9] * z[16];
z[16] = z[16] + z[51];
z[18] = z[12] + -z[16];
z[18] = abb[32] * z[18];
z[20] = -z[36] + z[110];
z[58] = abb[55] * (T(5) / T(2));
z[6] = -z[6] + z[14] + -z[20] + z[58] + z[63] + z[144];
z[6] = abb[9] * z[6];
z[14] = z[22] + -z[23];
z[22] = abb[45] + z[1] + z[14] + z[54];
z[22] = abb[5] * z[22];
z[6] = z[6] + z[22] + z[33];
z[22] = abb[51] * (T(3) / T(2)) + z[64];
z[5] = z[5] + -z[22] + z[25] + -z[36] + -z[140];
z[5] = abb[7] * z[5];
z[23] = z[13] + -z[79];
z[14] = z[14] + -z[49] + z[58] + -z[123];
z[14] = abb[0] * z[14];
z[25] = -z[88] + -z[112];
z[33] = -abb[56] + -z[36] + z[87];
z[33] = abb[6] * z[33];
z[5] = z[5] + -z[6] + z[14] + z[23] + (T(1) / T(2)) * z[25] + z[33] + (T(-3) / T(2)) * z[51];
z[5] = abb[33] * z[5];
z[14] = z[77] + z[124];
z[25] = -z[14] + z[69] + z[101] + 3 * z[149];
z[25] = abb[7] * z[25];
z[33] = z[106] * z[151];
z[36] = abb[0] * z[102];
z[49] = z[112] + z[116];
z[23] = -z[23] + z[25] + z[33] + z[36] + z[49];
z[23] = abb[31] * z[23];
z[25] = -abb[50] + -z[75] + z[114];
z[25] = abb[0] * z[25];
z[33] = abb[7] * z[78];
z[36] = -z[46] + 4 * z[51];
z[25] = z[25] + z[33] + z[36] + -z[79] + z[133];
z[33] = -abb[28] + abb[29];
z[33] = z[25] * z[33];
z[5] = z[5] + z[18] + z[23] + z[33];
z[5] = abb[33] * z[5];
z[18] = -z[30] + -z[71] + z[98] + -z[118] + z[120];
z[18] = abb[7] * z[18];
z[19] = z[19] + z[86] + -z[147] + -z[148];
z[19] = abb[5] * z[19];
z[30] = abb[6] * z[151];
z[33] = abb[47] + abb[49];
z[54] = -abb[56] + z[33] + -z[60];
z[54] = z[54] * z[143];
z[13] = z[13] + z[18] + z[19] + z[30] + -z[36] + z[54] + -z[112] + -z[131];
z[13] = abb[35] * z[13];
z[14] = -z[14] + z[55] + z[96] + -z[117];
z[14] = abb[14] * z[14];
z[18] = abb[50] * (T(-11) / T(2)) + z[20] + -z[22] + z[61] + z[111];
z[18] = abb[7] * z[18];
z[19] = abb[51] + z[37] + -z[68];
z[19] = abb[0] * z[19];
z[20] = abb[51] + abb[53] + -abb[54] + 2 * z[125];
z[20] = abb[1] * z[20];
z[6] = -z[6] + z[14] + z[18] + z[19] + z[20] + -z[24] + (T(1) / T(2)) * z[51] + -z[89];
z[6] = abb[32] * z[6];
z[18] = abb[0] * z[60];
z[19] = z[18] + -z[130];
z[20] = abb[52] + z[72];
z[20] = 2 * z[20] + -z[76] + -z[98];
z[20] = abb[7] * z[20];
z[14] = z[14] + -z[19] + z[20] + z[49] + z[73];
z[14] = abb[28] * z[14];
z[6] = z[6] + z[14] + -z[23];
z[6] = abb[32] * z[6];
z[12] = -abb[36] * z[12];
z[14] = abb[13] + abb[15];
z[14] = z[14] * z[40];
z[20] = abb[19] * z[47];
z[22] = abb[26] * z[42];
z[14] = z[14] + z[20] + z[22];
z[20] = abb[7] + 2 * abb[27];
z[20] = z[20] * z[40];
z[22] = (T(1) / T(2)) * z[40];
z[23] = abb[5] * z[22];
z[14] = (T(1) / T(2)) * z[14] + -z[20] + -z[23];
z[20] = -z[1] + z[31] + z[59];
z[20] = abb[17] * z[20];
z[23] = -z[8] + z[29] + z[31];
z[24] = abb[16] * z[23];
z[29] = z[1] + -z[26] + z[38];
z[29] = abb[18] * z[29];
z[24] = z[14] + z[20] + z[24] + z[29];
z[24] = abb[64] * z[24];
z[29] = abb[36] + abb[37];
z[16] = z[16] * z[29];
z[29] = abb[14] * z[32];
z[29] = z[29] + -z[97];
z[0] = abb[1] * z[0];
z[0] = -z[0] + z[29] + z[43] + z[70] + z[127];
z[30] = -abb[63] * z[0];
z[27] = z[27] + z[28] + -z[69] + z[129];
z[28] = abb[62] * z[27];
z[31] = -abb[63] * z[41];
z[32] = abb[37] * z[78];
z[36] = -abb[64] * z[22];
z[28] = z[28] + z[31] + z[32] + z[36];
z[28] = abb[6] * z[28];
z[31] = z[46] + z[112] + -z[132] + -z[141];
z[29] = z[29] + z[31] + z[74] + z[94];
z[32] = abb[61] * z[29];
z[33] = z[33] + -z[114];
z[8] = -z[1] + z[8] + -z[33];
z[8] = abb[67] * z[8];
z[36] = -abb[16] * abb[64];
z[36] = abb[67] + z[36];
z[36] = z[26] * z[36];
z[3] = abb[65] + abb[66] + z[3] + z[4] + z[5] + z[6] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + z[15] + z[16] + z[21] + z[24] + z[28] + z[30] + z[32] + z[36] + z[50];
z[4] = z[17] + z[66] + -z[81];
z[5] = -abb[7] * z[4];
z[6] = -z[52] + z[88];
z[8] = z[47] + -z[93];
z[8] = z[8] * z[34];
z[5] = z[5] + (T(1) / T(2)) * z[6] + z[8] + z[18] + z[35] + -z[39] + z[44] + -z[48] + z[83] + z[85] + z[137] + -z[142];
z[5] = abb[29] * z[5];
z[4] = abb[6] * z[4];
z[4] = z[4] + -z[19] + z[126];
z[2] = 4 * abb[53] + -z[2] + z[121] + -z[124];
z[2] = abb[7] * z[2];
z[6] = abb[48] + -abb[54];
z[6] = z[6] * z[146];
z[8] = -abb[1] + z[109];
z[1] = z[1] * z[8];
z[8] = abb[12] + -abb[13];
z[8] = z[8] * z[62];
z[9] = z[65] * z[100];
z[10] = z[60] * z[108];
z[1] = z[1] + z[2] + z[4] + z[6] + z[8] + z[9] + z[10] + -z[80] + -2 * z[83] + z[97] + -z[113];
z[1] = abb[30] * z[1];
z[2] = abb[7] + -z[108];
z[2] = z[2] * z[60];
z[6] = abb[1] + abb[12];
z[6] = z[6] * z[26];
z[8] = z[56] + z[90];
z[8] = z[8] * z[106];
z[9] = -abb[1] * z[57];
z[2] = z[2] + z[6] + z[8] + z[9] + z[31] + z[53] + z[79] + -z[82] + -z[97] + z[136];
z[2] = abb[28] * z[2];
z[6] = abb[33] * z[25];
z[4] = -z[4] + -z[84] + -z[112] + z[127];
z[4] = abb[32] * z[4];
z[1] = z[1] + z[2] + z[4] + z[5] + z[6] + z[45];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[39] * z[7];
z[4] = z[23] + -z[26];
z[4] = abb[16] * z[4];
z[5] = z[37] + -z[90];
z[5] = abb[18] * z[5];
z[4] = z[4] + z[5] + z[14] + z[20];
z[4] = abb[41] * z[4];
z[5] = -abb[6] * z[41];
z[0] = -z[0] + z[5];
z[0] = abb[40] * z[0];
z[5] = abb[38] * z[29];
z[6] = abb[39] * z[27];
z[7] = -abb[41] * z[22];
z[6] = z[6] + z[7];
z[6] = abb[6] * z[6];
z[7] = -z[33] + z[90];
z[7] = abb[44] * z[7];
z[0] = abb[42] + abb[43] + z[0] + z[1] + z[2] + z[4] + z[5] + z[6] + z[7];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_656_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("1.6998821841373349780908535252282326406651977555360099955793770223"),stof<T>("-0.19425610876090151741340666725705101861638578849696195204159848")}, std::complex<T>{stof<T>("-2.754362249599921985189522502564216264468658200496839632231558848"),stof<T>("4.5758817812285534414847759957584789506505660580871473303792318232")}, stof<T>("7.2903750035089039573830922628708475851031178573555162241913142345"), std::complex<T>{stof<T>("-10.1714816702235455227595215759996634462678321861777665927611687704"),stof<T>("3.333204549817711744819274708829752251987161875862171249482444445")}, std::complex<T>{stof<T>("-5.8960233692386057588512942962128207380279398191183881792731974402"),stof<T>("-10.8585414287506369664111370156665127371457231792196756780107851216")}, std::complex<T>{stof<T>("-2.0156640832988733719606137261277754849443773192917153730660807468"),stof<T>("-9.8101203061006967871590423959948370570987047854916615491555962234")}, std::complex<T>{stof<T>("6.0666391264617025789854559254392443014633108622730192198673867702"),stof<T>("-5.0497755833144549446096779290700272941909024874627115469137068309")}, std::complex<T>{stof<T>("5.379982675798251584941789086443810122136937441715955438686577521"),stof<T>("17.515157361182847339700868237235297034760897382388858896796733101")}, std::complex<T>{stof<T>("0.3892962763256345937225983992044110191949462490770220042483242311"),stof<T>("-7.8992931638430520699552325084975109962785783853941592996827353574")}, std::complex<T>{stof<T>("-3.6698300063490346345036642997310546118443251055516215333383161093"),stof<T>("4.7102964200013096593830241595124050741098753502435860467147967252")}, std::complex<T>{stof<T>("-1.6048809138609959509188142370120174883144154325121793177869173784"),stof<T>("5.0998238860993871277760182364824319829888294352480755024407994981")}, std::complex<T>{stof<T>("1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("-7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("-7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("-0.2599334236531954369552408109471828770159706361072270565126747634")}, std::complex<T>{stof<T>("3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("-0.2599334236531954369552408109471828770159706361072270565126747634")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(abs(k.W[73])) - rlog(abs(kbase.W[73])), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[125].real()/kbase.W[125].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_656_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_656_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,6> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(16)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (-30 * v[0] + 14 * v[1] + -6 * v[2] + -6 * v[3] + 6 * v[5] + m1_set::bc<T>[2] * (21 * v[0] + -v[1] + -3 * (-8 + -3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = ((T(1) / T(32)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (2 * (13 * v[0] + -5 * v[1] + v[2] + v[3] + 4 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -v[5]) + m1_set::bc<T>[2] * (-21 * v[0] + v[1] + 3 * (-8 + -3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[2] = ((T(1) / T(16)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (-45 * v[0] + -7 * v[1] + 12 * m1_set::bc<T>[1] * (v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + 3 * (-24 + 16 * m1_set::bc<T>[1] + 8 * m1_set::bc<T>[2] + -7 * v[2] + 5 * v[3] + 12 * v[4] + 7 * v[5]) + m1_set::bc<T>[2] * (21 * v[0] + -v[1] + -3 * (-3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[3] = (m1_set::bc<T>[2] * (T(-3) / T(2)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;
c[4] = ((4 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (T(1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;
c[5] = ((-1 + 2 * m1_set::bc<T>[1] + m1_set::bc<T>[2]) * (T(3) / T(2)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return t * (abb[46] * c[0] + -abb[47] * c[0] + abb[48] * c[0] + abb[52] * c[0] + abb[45] * c[1] + abb[51] * c[2] + abb[50] * (c[0] + c[1] + c[2]) + -abb[55] * (c[0] + c[1] + c[2])) + abb[46] * c[3] + -abb[47] * c[3] + abb[48] * c[3] + abb[52] * c[3] + abb[45] * c[4] + abb[51] * c[5] + abb[50] * (c[3] + c[4] + c[5]) + -abb[55] * (c[3] + c[4] + c[5]);
	}
	{
T z[8];
z[0] = 2 * abb[45];
z[1] = -abb[50] + abb[55];
z[2] = z[0] + z[1];
z[3] = -3 * abb[51] + z[2];
z[3] = abb[32] * z[3];
z[0] = abb[51] * (T(-3) / T(2)) + -z[0] + (T(7) / T(2)) * z[1];
z[0] = abb[33] * z[0];
z[0] = z[0] + z[3];
z[0] = abb[33] * z[0];
z[3] = prod_pow(abb[32], 2);
z[3] = (T(9) / T(2)) * z[3];
z[4] = abb[37] + -z[3];
z[4] = z[1] * z[4];
z[5] = abb[48] + abb[52];
z[5] = 2 * z[5];
z[6] = abb[34] + -abb[37];
z[5] = z[5] * z[6];
z[2] = -abb[36] * z[2];
z[3] = 3 * abb[36] + z[3] + -2 * z[6];
z[3] = abb[51] * z[3];
z[7] = -abb[37] * abb[45];
z[1] = abb[45] + -z[1];
z[1] = abb[34] * z[1];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[7];
z[0] = abb[10] * z[0];
z[1] = abb[46] + -abb[47];
z[2] = 2 * abb[10];
z[1] = z[1] * z[2] * z[6];
return z[0] + z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_656_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_656_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (v[1] + v[2] + -v[3] + -v[4]) * (-8 + 4 * v[0] + 3 * v[1] + -v[2] + v[3] + v[4] + 2 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 4 * v[5])) / prod_pow(tend, 2);
c[1] = ((1 + m1_set::bc<T>[1]) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[45] + abb[50] + -abb[56]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[28] + -abb[29];
z[1] = abb[31] + z[0];
z[1] = abb[31] * z[1];
z[0] = abb[33] + z[0];
z[0] = abb[33] * z[0];
z[0] = -abb[35] + -z[0] + z[1];
z[1] = abb[45] + abb[50] + -abb[56];
return 2 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_656_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (abb[45] + abb[50] + -abb[56]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[31] + abb[33];
z[1] = abb[45] + abb[50] + -abb[56];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_656_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("8.2739611769218150643371769714977306890112298652685725958243571042"),stof<T>("-3.257769207872990923870296949427624775879145575046020273411930133")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 19, 73});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,68> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W17(k,dl), dlog_W20(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_24_im(k), T{0}, T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[11].real()/k.W[11].real()), IntDLogL_W_17(t,k,kend,dl), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(abs(kend.W[73])) - rlog(abs(k.W[73])), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[125].real()/k.W[125].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_24_re(k), T{0}, T{0}, T{0}};
abb[42] = SpDLog_f_4_656_W_17_Im(t, path, abb);
abb[43] = SpDLog_f_4_656_W_20_Im(t, path, abb);
abb[44] = SpDLogQ_W_74(k,dl,dlr).imag();
abb[65] = SpDLog_f_4_656_W_17_Re(t, path, abb);
abb[66] = SpDLog_f_4_656_W_20_Re(t, path, abb);
abb[67] = SpDLogQ_W_74(k,dl,dlr).real();

                    
            return f_4_656_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_656_DLogXconstant_part(base_point<T>, kend);
	value += f_4_656_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_656_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_656_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_656_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_656_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_656_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_656_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
