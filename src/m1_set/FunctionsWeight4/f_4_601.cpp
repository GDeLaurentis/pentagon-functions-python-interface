/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_601.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_601_abbreviated (const std::array<T,31>& abb) {
T z[45];
z[0] = abb[20] + -abb[25];
z[1] = abb[23] + abb[24];
z[2] = abb[19] * (T(1) / T(6));
z[3] = abb[22] * (T(1) / T(2));
z[0] = abb[21] * (T(1) / T(6)) + (T(-4) / T(3)) * z[0] + (T(-2) / T(3)) * z[1] + -z[2] + -z[3];
z[0] = abb[3] * z[0];
z[4] = abb[19] + abb[21];
z[5] = 4 * abb[20];
z[6] = z[4] + -z[5];
z[7] = -abb[22] + abb[25];
z[8] = 2 * z[7];
z[9] = -z[1] + z[6] + z[8];
z[10] = abb[6] * z[9];
z[11] = abb[26] + -abb[27];
z[12] = abb[9] * z[11];
z[13] = z[10] + z[12];
z[14] = abb[7] * z[11];
z[15] = z[13] + z[14];
z[3] = abb[25] * (T(1) / T(3)) + -z[3];
z[16] = abb[21] * (T(1) / T(2));
z[17] = abb[19] * (T(-7) / T(6)) + abb[23] * (T(1) / T(6)) + abb[20] * (T(13) / T(6)) + -z[3] + -z[16];
z[17] = abb[0] * z[17];
z[2] = abb[20] * (T(-2) / T(3)) + abb[24] * (T(-1) / T(6)) + z[2] + z[3];
z[2] = abb[2] * z[2];
z[3] = abb[8] * z[11];
z[18] = abb[23] * (T(-5) / T(2)) + 4 * z[7];
z[18] = abb[20] * (T(-10) / T(3)) + abb[24] * (T(-1) / T(2)) + abb[19] * (T(5) / T(6)) + abb[21] * (T(7) / T(6)) + (T(1) / T(3)) * z[18];
z[18] = abb[1] * z[18];
z[19] = -abb[23] + z[7];
z[20] = 4 * z[19];
z[21] = abb[19] * (T(13) / T(2)) + z[20];
z[21] = abb[20] * (T(-7) / T(2)) + (T(1) / T(3)) * z[21];
z[21] = abb[4] * z[21];
z[0] = z[0] + z[2] + (T(-1) / T(3)) * z[3] + (T(-2) / T(3)) * z[15] + z[17] + z[18] + z[21];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[2] = 2 * abb[19];
z[17] = -z[1] + z[2];
z[18] = 2 * abb[21];
z[21] = 4 * abb[22] + -z[18];
z[22] = 3 * abb[25];
z[23] = 8 * abb[20] + -z[17] + z[21] + -z[22];
z[24] = abb[3] * z[23];
z[24] = z[13] + z[24];
z[25] = 4 * abb[21];
z[26] = 4 * abb[19];
z[27] = -16 * abb[20] + -8 * abb[22] + 5 * abb[25] + -z[1] + z[25] + z[26];
z[27] = abb[1] * z[27];
z[28] = 2 * abb[22];
z[29] = -abb[25] + z[28];
z[30] = 3 * abb[20];
z[31] = z[29] + z[30];
z[32] = -abb[21] + z[31];
z[32] = abb[0] * z[32];
z[31] = -abb[19] + z[31];
z[33] = abb[2] * z[31];
z[34] = -abb[19] + abb[20];
z[35] = abb[4] * z[34];
z[27] = z[3] + -z[24] + z[27] + z[32] + z[33] + z[35];
z[32] = 2 * abb[11];
z[33] = z[27] * z[32];
z[36] = 3 * abb[19];
z[37] = 11 * abb[20] + -z[18] + 3 * z[29] + -z[36];
z[37] = abb[3] * z[37];
z[38] = 2 * abb[23];
z[39] = 5 * abb[20];
z[21] = -abb[19] + abb[25] + -z[21] + z[38] + -z[39];
z[21] = abb[0] * z[21];
z[38] = -5 * abb[19] + 20 * abb[20] + 11 * abb[22] + -abb[24] + -4 * abb[25] + -z[25] + -z[38];
z[38] = abb[1] * z[38];
z[2] = -z[2] + z[30];
z[19] = -z[2] + z[19];
z[40] = 2 * abb[4];
z[19] = z[19] * z[40];
z[41] = -abb[22] + abb[24];
z[42] = abb[19] + z[41];
z[43] = abb[20] * (T(-3) / T(2)) + -z[16] + z[42];
z[43] = abb[2] * z[43];
z[19] = z[14] + z[19] + z[21] + z[37] + z[38] + z[43];
z[19] = abb[12] * z[19];
z[21] = 3 * abb[22];
z[37] = abb[19] + -abb[24] + 2 * abb[25] + -z[5] + -z[21];
z[37] = abb[1] * z[37];
z[31] = -abb[3] * z[31];
z[38] = 2 * abb[20];
z[43] = z[29] + z[38];
z[43] = abb[0] * z[43];
z[31] = z[31] + z[35] + z[37] + z[43];
z[2] = -abb[21] + -z[2] + 2 * z[41];
z[2] = abb[2] * z[2];
z[14] = 2 * z[14];
z[2] = z[2] + -z[14] + 2 * z[31];
z[2] = abb[13] * z[2];
z[2] = z[2] + z[19] + z[33];
z[2] = abb[12] * z[2];
z[19] = 2 * abb[12];
z[19] = z[19] * z[27];
z[27] = abb[25] * (T(3) / T(2)) + -z[28];
z[28] = abb[19] * (T(1) / T(2));
z[31] = -abb[24] + z[27] + z[28];
z[33] = abb[21] * (T(3) / T(2));
z[37] = -z[5] + z[31] + z[33];
z[41] = abb[2] * z[37];
z[23] = abb[1] * z[23];
z[27] = -abb[23] + z[16] + z[27];
z[43] = abb[19] * (T(3) / T(2)) + -z[5] + z[27];
z[43] = abb[0] * z[43];
z[4] = abb[22] + -z[4] + z[38];
z[4] = abb[3] * z[4];
z[44] = (T(3) / T(2)) * z[3];
z[4] = 2 * z[4] + z[23] + z[41] + z[43] + -z[44];
z[4] = abb[14] * z[4];
z[24] = 2 * z[23] + z[24] + -z[44];
z[16] = -z[16] + z[31] + -z[38];
z[16] = abb[2] * z[16];
z[31] = -z[16] + -z[24] + -z[43];
z[31] = abb[13] * z[31];
z[27] = z[27] + -z[28] + -z[38];
z[27] = abb[0] * z[27];
z[24] = z[24] + z[27] + -2 * z[35];
z[27] = z[24] + z[41];
z[28] = -abb[11] * z[27];
z[4] = z[4] + -z[19] + z[28] + z[31];
z[4] = abb[14] * z[4];
z[28] = abb[19] + -abb[23];
z[7] = -z[7] + -z[28] + z[38];
z[7] = z[7] * z[40];
z[31] = abb[3] * z[9];
z[6] = -z[6] + z[29];
z[6] = abb[0] * z[6];
z[6] = -z[6] + z[7] + z[15] + z[23] + -z[31];
z[7] = abb[11] * z[6];
z[15] = z[16] + z[24];
z[15] = abb[13] * z[15];
z[7] = z[7] + z[15];
z[7] = abb[11] * z[7];
z[8] = -z[8] + z[39];
z[16] = -abb[21] + z[8] + -z[17];
z[16] = abb[3] * z[16];
z[17] = 12 * abb[20] + 5 * abb[22] + -z[22];
z[22] = 3 * abb[21] + -abb[24] + -z[17] + z[26];
z[22] = abb[1] * z[22];
z[23] = abb[2] * z[34];
z[10] = -z[10] + -z[12] + -z[16] + z[22] + z[23];
z[12] = -abb[25] + z[21];
z[16] = -abb[23] + z[12] + -z[18];
z[21] = 9 * abb[20];
z[16] = abb[19] + 2 * z[16] + z[21];
z[16] = abb[0] * z[16];
z[20] = -abb[19] + z[20] + -z[30];
z[20] = abb[4] * z[20];
z[10] = 2 * z[10] + -z[14] + z[16] + z[20];
z[16] = abb[28] * z[10];
z[17] = -abb[23] + -z[17] + z[25] + z[36];
z[17] = abb[1] * z[17];
z[1] = abb[19] + -z[1] + -z[8] + z[18];
z[1] = abb[3] * z[1];
z[8] = abb[20] + -abb[21];
z[20] = abb[0] * z[8];
z[1] = z[1] + z[3] + -z[13] + z[17] + z[20];
z[3] = -abb[24] + z[12];
z[3] = abb[21] + 2 * z[3] + z[21] + -z[26];
z[3] = abb[2] * z[3];
z[1] = 2 * z[1] + z[3] + z[14];
z[1] = abb[15] * z[1];
z[3] = -z[5] + z[18] + z[42];
z[3] = abb[1] * z[3];
z[5] = abb[0] + -abb[3];
z[5] = z[5] * z[8];
z[8] = abb[20] * (T(1) / T(2)) + z[33] + -z[42];
z[8] = abb[2] * z[8];
z[3] = z[3] + z[5] + z[8];
z[3] = prod_pow(abb[13], 2) * z[3];
z[5] = abb[9] * z[9];
z[8] = abb[8] * z[37];
z[9] = abb[0] + abb[2];
z[9] = 3 * abb[3] + abb[6] + -6 * abb[10] + (T(3) / T(2)) * z[9];
z[9] = z[9] * z[11];
z[11] = abb[21] + -abb[24] + -z[28];
z[11] = abb[7] * z[11];
z[5] = -z[5] + z[8] + -z[9] + z[11];
z[8] = abb[29] * z[5];
z[0] = abb[30] + z[0] + z[1] + z[2] + z[3] + z[4] + z[7] + z[8] + z[16];
z[1] = -z[6] * z[32];
z[2] = abb[14] * z[27];
z[1] = z[1] + z[2] + -z[15] + -z[19];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[16] * z[10];
z[3] = abb[17] * z[5];
z[1] = abb[18] + z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_601_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("11.0022124990841563100890025463692040674202285215086839570537751465"),stof<T>("2.306404323538963101237871284555742615571400235931426773322096829")}, std::complex<T>{stof<T>("7.5451495683499004586694994991586801063225233662002602758322845513"),stof<T>("-8.1717595144031961779983051052573179208911058695818241722400601793")}, std::complex<T>{stof<T>("-3.8570639192294694711176497773697730014971274149737665679938718713"),stof<T>("9.113505397743180713517481786218193288474086180257951934123659646")}, std::complex<T>{stof<T>("8.9933382388687259792955183902503283503713238438326875052644805606"),stof<T>("3.2481502068789476367570479655166179831543805466075545352056962956")}, std::complex<T>{stof<T>("-11.1350869877497077702822775913128952547002977612535560471986031353"),stof<T>("0.8043098821654289468078014932187410919523605298667199406899901851")}, std::complex<T>{stof<T>("-1.7776055802274397636792873384226079587726633557408108088467923456"),stof<T>("-2.0262300445221882917824247293676795375533705382371372379478432403")}, std::complex<T>{stof<T>("5.1308037300470366547383700732721051365847964024862086651021948251"),stof<T>("6.3404151149454952501397502564644908152841443413141669639254008115")}, std::complex<T>{stof<T>("-5.1308037300470366547383700732721051365847964024862086651021948251"),stof<T>("-6.3404151149454952501397502564644908152841443413141669639254008115")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[127].real()/kbase.W[127].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_601_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_601_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,4> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(4)) * (v[2] + v[3]) * (v[2] + v[3] + 2 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]))) / prod_pow(tend, 2);
c[1] = ((T(1) / T(16)) * (v[2] + v[3]) * (-56 + -16 * v[0] + -8 * v[1] + 7 * v[2] + 15 * v[3] + 8 * v[4] + -24 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -28 * v[5])) / prod_pow(tend, 2);
c[2] = (2 * (1 + m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;
c[3] = ((1 + 4 * m1_set::bc<T>[1]) * (T(3) / T(2)) * (v[2] + v[3])) / tend;


		return (abb[22] + abb[24] + -abb[25]) * (t * c[0] + c[2]) + abb[21] * (t * c[0] + -t * c[1] + c[2] + -c[3]) + abb[20] * (t * c[1] + c[3]);
	}
	{
T z[5];
z[0] = abb[20] + -abb[21];
z[1] = abb[11] + -abb[14];
z[1] = -z[0] * z[1];
z[1] = 2 * z[1];
z[2] = abb[22] + abb[24] + -abb[25];
z[2] = 4 * z[2];
z[3] = 5 * abb[20] + -abb[21] + z[2];
z[3] = abb[13] * z[3];
z[4] = abb[20] * (T(-15) / T(2)) + abb[21] * (T(7) / T(2)) + -z[2];
z[4] = abb[12] * z[4];
z[3] = -z[1] + z[3] + z[4];
z[3] = abb[12] * z[3];
z[0] = abb[13] * z[0];
z[0] = (T(5) / T(2)) * z[0] + z[1];
z[0] = abb[13] * z[0];
z[1] = -3 * abb[20] + -abb[21] + -z[2];
z[1] = abb[15] * z[1];
z[0] = z[0] + z[1] + z[3];
return abb[5] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_601_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[20] + -abb[21]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[20] + -abb[21];
z[1] = -abb[12] + abb[13];
return 2 * abb[5] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_601_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-9.903386635469010898447395683557304396589704562815811846858873651"),stof<T>("-25.912476163763681676751998312695220789062145727026312359538074704")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,31> abb = {dl[0], dl[5], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W15(k,dl), dlog_W19(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_13(k), f_2_4_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[14].real()/k.W[14].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[127].real()/k.W[127].real()), f_2_4_re(k), f_2_25_re(k), T{0}};
abb[18] = SpDLog_f_4_601_W_19_Im(t, path, abb);
abb[30] = SpDLog_f_4_601_W_19_Re(t, path, abb);

                    
            return f_4_601_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_601_DLogXconstant_part(base_point<T>, kend);
	value += f_4_601_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_601_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_601_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_601_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_601_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_601_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_601_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
