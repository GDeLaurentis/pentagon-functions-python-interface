/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_261.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_261_abbreviated (const std::array<T,28>& abb) {
T z[69];
z[0] = 2 * abb[25];
z[1] = 2 * abb[24];
z[2] = 5 * abb[27];
z[3] = 2 * abb[23];
z[4] = 2 * abb[26];
z[5] = abb[19] + abb[22];
z[6] = z[0] + z[1] + -z[2] + z[3] + z[4] + -z[5];
z[6] = abb[1] * z[6];
z[7] = 3 * abb[26];
z[8] = 3 * abb[23];
z[9] = z[7] + z[8];
z[10] = 4 * abb[27];
z[11] = z[5] + z[9] + -z[10];
z[12] = abb[3] * z[11];
z[13] = 2 * z[12];
z[14] = -abb[27] + z[5];
z[15] = abb[2] * z[14];
z[16] = z[13] + -z[15];
z[17] = 2 * abb[20];
z[18] = z[2] + z[17];
z[19] = -abb[22] + z[4];
z[20] = -abb[19] + -4 * abb[24] + z[18] + -z[19];
z[20] = abb[8] * z[20];
z[20] = -z[16] + z[20];
z[21] = 2 * abb[21];
z[22] = -z[17] + z[21];
z[23] = -abb[19] + z[3];
z[24] = z[22] + z[23];
z[25] = abb[22] + -abb[27];
z[26] = z[0] + -z[25];
z[27] = z[24] + -z[26];
z[28] = abb[7] * z[27];
z[29] = z[19] + z[22];
z[30] = abb[19] + -abb[27];
z[31] = -z[1] + z[30];
z[32] = z[29] + z[31];
z[33] = abb[6] * z[32];
z[34] = abb[23] + abb[26];
z[35] = 2 * abb[22];
z[36] = abb[25] + z[35];
z[37] = 2 * abb[19];
z[38] = -8 * abb[20] + 4 * abb[21] + -17 * abb[27] + 9 * z[34] + z[36] + z[37];
z[38] = abb[9] * z[38];
z[39] = z[21] + -z[37];
z[40] = z[8] + -z[17];
z[41] = 3 * abb[27];
z[42] = -abb[24] + -z[40] + z[41];
z[43] = abb[25] + -abb[26] + -z[39] + z[42];
z[43] = abb[0] * z[43];
z[18] = -abb[22] + -4 * abb[25] + z[18] + -z[23];
z[18] = abb[5] * z[18];
z[44] = -abb[25] + z[17];
z[45] = z[41] + z[44];
z[46] = -z[7] + z[45];
z[47] = z[35] + z[46];
z[48] = -abb[23] + abb[24] + -z[21] + z[47];
z[48] = abb[4] * z[48];
z[49] = abb[9] * abb[24];
z[6] = z[6] + z[18] + z[20] + -z[28] + -z[33] + z[38] + z[43] + z[48] + z[49];
z[6] = abb[18] * z[6];
z[38] = 4 * abb[23];
z[43] = -z[2] + z[38];
z[48] = 3 * abb[19];
z[29] = z[1] + z[29] + z[43] + -z[48];
z[29] = abb[0] * z[29];
z[50] = 4 * abb[20];
z[31] = abb[26] + z[3] + z[31] + z[35] + -z[50];
z[31] = abb[1] * z[31];
z[51] = 7 * abb[27];
z[52] = 5 * abb[26] + -z[51];
z[40] = abb[21] + z[37] + z[40] + z[52];
z[40] = abb[9] * z[40];
z[40] = z[40] + z[49];
z[40] = 2 * z[40];
z[53] = 4 * abb[26];
z[2] = -z[2] + z[53];
z[39] = -abb[20] + 5 * abb[24] + z[2] + -z[35] + -z[39];
z[39] = abb[8] * z[39];
z[54] = abb[19] + abb[24];
z[55] = -abb[20] + z[21];
z[56] = abb[26] + -z[35] + -z[54] + z[55];
z[56] = abb[4] * z[56];
z[29] = z[13] + z[29] + z[31] + z[33] + z[39] + -z[40] + z[56];
z[29] = abb[16] * z[29];
z[31] = -z[35] + z[44] + z[51];
z[33] = -z[9] + z[31] + -z[37];
z[33] = abb[9] * z[33];
z[33] = z[33] + -z[49];
z[39] = -abb[21] + z[37];
z[19] = -z[19] + z[39] + z[42];
z[19] = abb[0] * z[19];
z[39] = -z[3] + z[39];
z[44] = abb[24] + z[39];
z[44] = abb[8] * z[44];
z[56] = abb[21] + z[4];
z[36] = -z[36] + z[56];
z[57] = abb[5] * z[36];
z[58] = -abb[21] + z[47];
z[23] = -z[23] + z[58];
z[23] = abb[4] * z[23];
z[19] = -z[12] + z[19] + z[23] + -z[33] + -z[44] + z[57];
z[19] = abb[10] * z[19];
z[23] = abb[19] + z[8] + 4 * z[25];
z[23] = abb[7] * z[23];
z[57] = -abb[9] * z[36];
z[59] = 3 * abb[20];
z[60] = abb[22] * (T(-5) / T(2)) + abb[26] * (T(3) / T(2)) + -z[30] + -z[59];
z[60] = abb[8] * z[60];
z[61] = abb[1] * (T(3) / T(2));
z[62] = abb[22] + -abb[26];
z[63] = -z[61] * z[62];
z[64] = abb[25] + z[53];
z[65] = -z[59] + z[64];
z[66] = abb[21] + 5 * abb[22] + z[30] + -z[65];
z[66] = abb[4] * z[66];
z[67] = abb[21] + -abb[25] + z[62];
z[67] = abb[0] * z[67];
z[57] = z[12] + -z[23] + z[57] + z[60] + z[63] + z[66] + z[67];
z[57] = abb[12] * z[57];
z[17] = z[17] + z[41];
z[41] = -abb[21] + z[17];
z[41] = 2 * z[41];
z[60] = 3 * abb[22];
z[63] = -7 * abb[26] + -z[38] + z[41] + z[60];
z[63] = abb[8] * z[63];
z[66] = 3 * abb[1];
z[62] = z[62] * z[66];
z[67] = 2 * abb[9];
z[68] = -2 * abb[5] + z[67];
z[36] = z[36] * z[68];
z[46] = -abb[22] + z[3] + -z[46];
z[46] = abb[4] * z[46];
z[36] = z[36] + 2 * z[46] + -z[62] + z[63];
z[36] = abb[11] * z[36];
z[19] = 2 * z[19] + z[36] + z[57];
z[19] = abb[12] * z[19];
z[21] = -abb[20] + 5 * abb[25] + -z[21] + z[35] + -z[37] + z[43];
z[21] = abb[5] * z[21];
z[26] = abb[23] + z[4] + -z[26] + z[37] + -z[50];
z[26] = abb[1] * z[26];
z[0] = z[0] + z[2] + z[24] + -z[60];
z[0] = abb[4] * z[0];
z[2] = 5 * abb[23];
z[24] = abb[21] + z[2] + z[7] + -z[31];
z[24] = z[24] * z[67];
z[31] = abb[22] + z[37];
z[36] = abb[23] + -abb[25] + -z[31] + z[55];
z[36] = abb[0] * z[36];
z[0] = z[0] + z[13] + z[21] + -z[24] + z[26] + z[28] + z[36];
z[0] = abb[15] * z[0];
z[13] = abb[0] * z[27];
z[21] = -z[8] + z[37];
z[26] = -z[21] + z[25];
z[26] = abb[7] * z[26];
z[27] = abb[19] + abb[23] + -2 * z[58];
z[27] = abb[4] * z[27];
z[13] = z[13] + z[16] + -z[18] + -z[24] + z[26] + z[27];
z[13] = abb[17] * z[13];
z[16] = 4 * abb[19];
z[18] = abb[22] + z[7] + -z[10] + z[16];
z[18] = abb[6] * z[18];
z[24] = abb[9] * z[39];
z[24] = z[24] + z[49];
z[26] = abb[24] + z[38];
z[27] = z[26] + -z[59];
z[28] = 5 * abb[19] + abb[21] + z[25] + -z[27];
z[28] = abb[0] * z[28];
z[25] = abb[19] * (T(-5) / T(2)) + abb[23] * (T(3) / T(2)) + -z[25] + -z[59];
z[25] = abb[5] * z[25];
z[36] = -abb[19] + abb[23];
z[39] = z[36] * z[61];
z[43] = abb[21] + -abb[24] + -z[36];
z[43] = abb[4] * z[43];
z[12] = z[12] + -z[18] + z[24] + z[25] + z[28] + z[39] + z[43];
z[12] = abb[10] * z[12];
z[25] = -abb[19] + z[4] + -z[42];
z[25] = abb[0] * z[25];
z[24] = -z[24] + z[25] + z[44];
z[25] = z[36] * z[66];
z[17] = z[17] + -z[56];
z[17] = -7 * abb[23] + 2 * z[17] + z[48];
z[17] = abb[5] * z[17];
z[17] = z[17] + 2 * z[24] + z[25];
z[17] = abb[11] * z[17];
z[12] = z[12] + z[17];
z[12] = abb[10] * z[12];
z[1] = abb[22] + 6 * abb[23] + abb[26] + z[1] + -z[16] + -z[41];
z[1] = abb[0] * z[1];
z[16] = abb[4] * z[32];
z[7] = z[7] + z[30] + -z[35];
z[7] = abb[6] * z[7];
z[1] = z[1] + z[7] + z[16] + -z[20] + -z[40];
z[1] = abb[14] * z[1];
z[7] = abb[22] * (T(1) / T(2));
z[16] = abb[19] * (T(1) / T(2));
z[17] = z[7] + z[10] + z[16] + (T(-9) / T(2)) * z[34];
z[17] = abb[1] * z[17];
z[2] = -z[2] + -z[22] + -z[52];
z[2] = abb[9] * z[2];
z[20] = -z[51] + z[55];
z[7] = abb[19] + abb[26] * (T(11) / T(2)) + -z[7] + z[20] + z[38];
z[7] = abb[8] * z[7];
z[16] = abb[22] + abb[23] * (T(11) / T(2)) + -z[16] + z[20] + z[53];
z[16] = abb[5] * z[16];
z[20] = -abb[20] + abb[26];
z[5] = abb[23] + -z[5] + z[20];
z[22] = abb[0] + abb[4];
z[5] = z[5] * z[22];
z[2] = z[2] + z[5] + z[7] + z[16] + z[17];
z[2] = prod_pow(abb[11], 2) * z[2];
z[5] = z[18] + z[33];
z[7] = -abb[24] + z[45];
z[16] = -abb[22] + z[7] + z[21];
z[16] = abb[0] * z[16];
z[17] = -z[47] + z[54];
z[17] = abb[4] * z[17];
z[16] = z[5] + -4 * z[15] + -z[16] + z[17] + z[23];
z[17] = prod_pow(abb[13], 2) * z[16];
z[18] = abb[1] + -z[22];
z[14] = z[14] * z[18];
z[14] = z[14] + z[15];
z[14] = prod_pow(m1_set::bc<T>[0], 2) * z[14];
z[0] = z[0] + z[1] + z[2] + z[6] + z[12] + z[13] + (T(13) / T(3)) * z[14] + z[17] + z[19] + z[29];
z[1] = -abb[20] + z[10];
z[2] = abb[22] + abb[24] + -z[1] + z[4] + z[48];
z[2] = abb[8] * z[2];
z[2] = z[2] + z[33];
z[4] = abb[20] + abb[27];
z[4] = abb[19] + 3 * z[4] + -z[8] + z[35] + -z[64];
z[4] = abb[4] * z[4];
z[1] = abb[19] + abb[25] + -z[1] + z[3] + z[60];
z[1] = abb[5] * z[1];
z[3] = abb[27] + -z[20];
z[3] = 3 * z[3] + -z[26] + z[31];
z[3] = abb[0] * z[3];
z[6] = abb[1] * z[11];
z[3] = -z[1] + -z[2] + z[3] + z[4] + z[6];
z[3] = abb[11] * z[3];
z[4] = z[7] + -z[9] + z[35] + z[37];
z[6] = -abb[4] * z[4];
z[7] = -6 * abb[19] + abb[27] + z[27] + -z[35];
z[7] = abb[0] * z[7];
z[1] = z[1] + z[5] + z[6] + z[7] + -z[25];
z[1] = abb[10] * z[1];
z[4] = -abb[0] * z[4];
z[5] = -6 * abb[22] + abb[27] + -z[37] + z[65];
z[5] = abb[4] * z[5];
z[2] = z[2] + z[4] + z[5] + z[23] + z[62];
z[2] = abb[12] * z[2];
z[4] = -abb[13] * z[16];
z[1] = z[1] + z[2] + z[3] + z[4];
z[1] = 2 * m1_set::bc<T>[0] * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_261_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("12.5194637672641039763337340151644324145240588845330496113874561603"),stof<T>("-5.6101558082160618146969595527656114337817231597693076389397632891")}, std::complex<T>{stof<T>("0.3394340037310461680265009558717779677933145846794237123354591569"),stof<T>("-8.0879643405690955275226339819476705899842356371415888861166575836")}, stof<T>("-2.8647087364577724700765246000647064387245951537773892836555008887"), std::complex<T>{stof<T>("5.8326950007340043245074176038218806874466941270665849551479219328"),stof<T>("12.3276833164789670612724074325473904443012398231298037001286260817")}, std::complex<T>{stof<T>("-1.5878209939247496534748827435782481890933763103766138150468831951"),stof<T>("11.267978352966709366771281710368035531196772453393554466685740059")}, std::complex<T>{stof<T>("-1.7650343372912506061501720790697175063828574533885482237234188796"),stof<T>("5.9824799041403908134641517394561833940015394171526973706272410025")}, std::complex<T>{stof<T>("4.6925936352181035204994613969195293736786346234932926727690137825"),stof<T>("-7.3463101351597655110405205590701614752726809313370293032535760193")}, std::complex<T>{stof<T>("5.0989477726053499983514336677643035379839884470898508411926510324"),stof<T>("-6.6698607717283195091980852749449663468861905295055568723826493119")}, std::complex<T>{stof<T>("-10.9944933348084347671316159893712896540018645904837909617306669794"),stof<T>("-4.2939923137312728544979533379884460161439077794399148951196417531")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_261_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_261_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-12.1442885985573619139658584145260183277688322741217412217668694199"),stof<T>("-0.8689125103147682631993956548178457013970562198447016897579058107")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,28> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), f_1_1(k), f_1_3(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real())};

                    
            return f_4_261_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_261_DLogXconstant_part(base_point<T>, kend);
	value += f_4_261_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_261_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_261_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_261_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_261_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_261_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_261_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
