/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_698.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_698_abbreviated (const std::array<T,65>& abb) {
T z[92];
z[0] = -abb[29] + abb[30];
z[1] = -abb[23] + abb[25];
z[2] = z[0] + z[1];
z[2] = m1_set::bc<T>[0] * z[2];
z[2] = abb[42] + z[2];
z[3] = -abb[9] * z[2];
z[4] = 2 * abb[6];
z[5] = -abb[11] + z[4];
z[6] = abb[1] + abb[5];
z[7] = abb[9] + z[5] + -z[6];
z[7] = abb[38] * z[7];
z[8] = -abb[23] + abb[28];
z[9] = -abb[27] + z[8];
z[10] = abb[26] + z[9];
z[11] = m1_set::bc<T>[0] * z[10];
z[12] = z[4] * z[11];
z[3] = z[3] + z[7] + z[12];
z[7] = abb[25] + abb[27];
z[12] = abb[23] + z[7];
z[13] = 2 * abb[29];
z[14] = -abb[24] + z[12] + -z[13];
z[14] = m1_set::bc<T>[0] * z[14];
z[14] = abb[42] + z[14];
z[14] = abb[1] * z[14];
z[15] = 2 * abb[24];
z[16] = -abb[29] + z[12] + -z[15];
z[16] = m1_set::bc<T>[0] * z[16];
z[17] = -abb[39] + abb[40];
z[16] = z[16] + z[17];
z[16] = abb[2] * z[16];
z[18] = -abb[24] + z[8];
z[19] = abb[25] + z[18];
z[20] = abb[27] + 2 * z[19];
z[21] = abb[26] + -z[13] + z[20];
z[21] = m1_set::bc<T>[0] * z[21];
z[21] = abb[40] + abb[42] + z[21];
z[21] = abb[3] * z[21];
z[22] = abb[17] + abb[19];
z[23] = abb[18] + abb[20];
z[24] = 2 * z[22] + z[23];
z[24] = abb[64] * z[24];
z[25] = abb[26] + z[1];
z[26] = -abb[24] + z[25];
z[26] = m1_set::bc<T>[0] * z[26];
z[17] = z[17] + z[26];
z[17] = abb[8] * z[17];
z[26] = -abb[30] + z[13];
z[20] = z[20] + -z[26];
z[20] = m1_set::bc<T>[0] * z[20];
z[20] = abb[42] + z[20];
z[20] = abb[4] * z[20];
z[5] = -abb[0] + -abb[3] + z[5] + z[6];
z[6] = abb[41] * z[5];
z[27] = abb[26] + -abb[30];
z[28] = abb[0] * m1_set::bc<T>[0];
z[29] = z[27] * z[28];
z[30] = -abb[1] + z[4];
z[31] = -abb[2] + abb[3] + -abb[4] + -z[30];
z[31] = abb[44] * z[31];
z[27] = m1_set::bc<T>[0] * z[27];
z[27] = -abb[39] + z[27];
z[27] = abb[11] * z[27];
z[32] = -abb[4] + abb[5];
z[33] = abb[2] + z[32];
z[34] = -abb[11] + z[33];
z[35] = -abb[0] + z[34];
z[35] = abb[43] * z[35];
z[36] = -abb[24] + abb[29];
z[36] = m1_set::bc<T>[0] * z[36];
z[36] = -abb[39] + -abb[42] + z[36];
z[37] = abb[5] * z[36];
z[38] = abb[40] * z[32];
z[6] = z[3] + -z[6] + z[14] + -z[16] + z[17] + z[20] + -z[21] + z[24] + -z[27] + -z[29] + -z[31] + z[35] + -z[37] + -z[38];
z[14] = -abb[52] * z[6];
z[16] = abb[30] + z[9];
z[16] = m1_set::bc<T>[0] * z[16];
z[17] = abb[39] + -abb[43] + abb[44] + z[16];
z[20] = 2 * abb[7];
z[21] = z[17] * z[20];
z[6] = -z[6] + z[21];
z[6] = abb[54] * z[6];
z[21] = 2 * abb[27];
z[24] = -abb[23] + -abb[25] + z[13] + z[15] + -z[21];
z[24] = m1_set::bc<T>[0] * z[24];
z[24] = -abb[42] + z[24];
z[24] = abb[1] * z[24];
z[27] = 2 * abb[28];
z[29] = -abb[23] + -abb[24] + z[27];
z[31] = 2 * abb[26];
z[29] = -abb[27] + -z[26] + 2 * z[29] + z[31];
z[29] = z[28] * z[29];
z[35] = 3 * abb[25];
z[37] = abb[27] + -z[15] + z[35];
z[39] = 2 * abb[23];
z[40] = -abb[28] + z[26] + -z[37] + z[39];
z[40] = m1_set::bc<T>[0] * z[40];
z[40] = -abb[42] + z[40];
z[40] = abb[4] * z[40];
z[41] = abb[0] + -abb[6];
z[42] = abb[41] * z[41];
z[38] = -z[38] + z[42];
z[37] = -abb[23] + -abb[29] + z[37];
z[37] = m1_set::bc<T>[0] * z[37];
z[37] = abb[42] + z[37];
z[37] = abb[5] * z[37];
z[30] = -abb[0] + z[30] + -z[32];
z[30] = abb[44] * z[30];
z[43] = abb[25] + -abb[28];
z[44] = m1_set::bc<T>[0] * z[43];
z[44] = abb[42] + z[44];
z[44] = abb[3] * z[44];
z[34] = abb[0] + z[34];
z[34] = abb[43] * z[34];
z[45] = -abb[27] + abb[29];
z[45] = abb[2] * m1_set::bc<T>[0] * z[45];
z[46] = abb[11] * z[16];
z[47] = -abb[64] * z[22];
z[3] = -z[3] + z[24] + z[29] + -z[30] + -z[34] + z[37] + -2 * z[38] + z[40] + -z[44] + z[45] + -z[46] + z[47];
z[24] = abb[53] + abb[55];
z[3] = z[3] * z[24];
z[29] = 2 * z[9];
z[30] = abb[26] + z[29];
z[34] = abb[30] + z[30];
z[28] = -z[28] * z[34];
z[37] = abb[6] * z[11];
z[38] = -abb[5] * abb[39];
z[40] = -abb[5] + abb[6];
z[40] = abb[38] * z[40];
z[44] = -abb[5] + -z[41];
z[44] = abb[44] * z[44];
z[28] = z[28] + z[37] + z[38] + z[40] + z[42] + z[44];
z[37] = 2 * abb[56];
z[28] = z[28] * z[37];
z[38] = -abb[48] + abb[49];
z[40] = -abb[46] + abb[51];
z[42] = z[38] + -z[40];
z[44] = -abb[38] + abb[43];
z[44] = -z[42] * z[44];
z[45] = abb[27] + z[1];
z[46] = -abb[29] + z[45];
z[46] = m1_set::bc<T>[0] * z[46];
z[46] = abb[42] + z[46];
z[46] = -z[38] * z[46];
z[44] = z[44] + z[46];
z[46] = -abb[47] + abb[50];
z[47] = -abb[24] + z[45];
z[47] = m1_set::bc<T>[0] * z[47];
z[47] = -abb[39] + z[47];
z[47] = -z[46] * z[47];
z[48] = z[40] + -z[46];
z[49] = abb[40] + abb[41];
z[49] = z[48] * z[49];
z[36] = z[36] * z[40];
z[36] = z[36] + -z[44] + z[47] + z[49];
z[36] = abb[19] * z[36];
z[25] = -abb[29] + z[25];
z[25] = m1_set::bc<T>[0] * z[25];
z[25] = abb[42] + z[25];
z[25] = -z[25] * z[40];
z[47] = abb[26] + -abb[27];
z[47] = m1_set::bc<T>[0] * z[46] * z[47];
z[49] = abb[41] * z[48];
z[25] = z[25] + -z[44] + z[47] + z[49];
z[25] = abb[17] * z[25];
z[34] = m1_set::bc<T>[0] * z[34];
z[34] = -abb[39] + z[34];
z[34] = z[34] * z[40];
z[11] = -abb[39] + z[11];
z[11] = -z[11] * z[46];
z[16] = -z[16] * z[38];
z[11] = z[11] + z[16] + z[34];
z[11] = abb[21] * z[11];
z[12] = abb[26] + z[0] + -z[12] + z[27];
z[12] = z[12] * z[40];
z[16] = z[0] + z[8];
z[16] = z[16] * z[38];
z[27] = abb[27] + -abb[28];
z[34] = abb[25] + z[27];
z[44] = -abb[26] + z[34];
z[44] = z[44] * z[46];
z[12] = -z[12] + z[16] + -z[44];
z[12] = m1_set::bc<T>[0] * z[12];
z[16] = abb[18] * z[12];
z[44] = abb[40] * z[48];
z[12] = z[12] + z[44];
z[12] = abb[20] * z[12];
z[44] = -abb[16] * z[42];
z[2] = -z[2] * z[44];
z[47] = abb[20] * z[48];
z[49] = abb[18] * z[48];
z[47] = z[47] + z[49];
z[50] = abb[21] * z[48];
z[51] = -z[47] + z[50];
z[52] = -abb[41] * z[51];
z[49] = abb[40] * z[49];
z[53] = abb[16] + -abb[21] + z[23];
z[53] = -abb[38] * z[42] * z[53];
z[42] = abb[21] * z[42];
z[54] = abb[0] * z[37];
z[42] = z[42] + z[54];
z[42] = abb[43] * z[42];
z[54] = abb[52] + abb[56];
z[54] = z[20] * z[54];
z[17] = z[17] * z[54];
z[2] = z[2] + z[3] + z[6] + z[11] + z[12] + z[14] + z[16] + z[17] + z[25] + z[28] + z[36] + z[42] + z[49] + z[52] + z[53];
z[2] = 4 * z[2];
z[3] = abb[23] + abb[28];
z[6] = 2 * z[3];
z[11] = -z[6] + z[35];
z[11] = abb[25] * z[11];
z[12] = z[19] * z[21];
z[11] = 4 * abb[32] + z[11] + z[12];
z[14] = z[7] + z[18];
z[14] = -abb[29] + 2 * z[14];
z[14] = abb[29] * z[14];
z[16] = 2 * abb[61];
z[17] = prod_pow(m1_set::bc<T>[0], 2);
z[18] = z[16] + 6 * z[17];
z[10] = -abb[24] + z[10];
z[19] = z[10] * z[31];
z[25] = 2 * abb[25];
z[28] = abb[24] * z[25];
z[35] = prod_pow(abb[28], 2);
z[36] = 2 * abb[36];
z[42] = 2 * abb[63];
z[14] = -z[11] + z[14] + z[18] + z[19] + z[28] + -2 * z[35] + -z[36] + z[42];
z[14] = abb[3] * z[14];
z[49] = 2 * abb[33];
z[52] = 2 * abb[31];
z[53] = z[49] + -z[52];
z[26] = abb[30] * z[26];
z[55] = prod_pow(abb[25], 2);
z[56] = z[26] + z[42] + -z[53] + z[55];
z[57] = -abb[26] + z[15];
z[58] = abb[26] * z[57];
z[59] = (T(2) / T(3)) * z[17];
z[58] = z[56] + z[58] + z[59];
z[60] = -abb[23] + abb[24];
z[61] = z[21] * z[60];
z[62] = 2 * abb[62];
z[36] = z[36] + -z[62];
z[60] = 2 * z[60];
z[63] = -abb[29] + z[60];
z[63] = abb[29] * z[63];
z[64] = 2 * abb[58];
z[28] = z[28] + z[36] + -z[58] + z[61] + -z[63] + -z[64];
z[28] = abb[2] * z[28];
z[7] = -abb[24] + z[7];
z[7] = z[7] * z[13];
z[0] = z[0] + z[9];
z[0] = abb[30] * z[0];
z[61] = 2 * z[0];
z[63] = z[7] + z[61];
z[65] = z[42] + -z[62];
z[18] = z[18] + z[65];
z[66] = abb[25] + z[8];
z[66] = -abb[24] + 2 * z[66];
z[66] = abb[24] * z[66];
z[67] = -abb[33] + z[35];
z[11] = -z[11] + z[18] + z[63] + z[66] + -2 * z[67];
z[11] = abb[4] * z[11];
z[66] = 2 * z[8];
z[67] = -abb[24] + z[66];
z[67] = abb[24] * z[67];
z[67] = -z[19] + z[67];
z[68] = -abb[29] + z[66];
z[68] = abb[29] * z[68];
z[68] = -z[36] + -z[49] + -z[61] + -z[67] + z[68];
z[68] = abb[0] * z[68];
z[69] = -abb[28] + z[1];
z[70] = abb[25] * z[69];
z[71] = abb[23] * abb[28];
z[70] = z[70] + z[71];
z[72] = abb[24] * z[43];
z[73] = -abb[24] + z[1];
z[73] = abb[27] * z[73];
z[10] = abb[26] * z[10];
z[72] = abb[31] + -abb[34] + abb[58] + -z[10] + z[70] + -z[72] + z[73];
z[73] = 2 * abb[8];
z[72] = z[72] * z[73];
z[73] = 2 * abb[57];
z[74] = z[16] + -z[73];
z[75] = abb[24] + z[39];
z[75] = abb[24] * z[75];
z[7] = z[7] + z[74] + z[75];
z[76] = abb[23] * z[21];
z[58] = z[7] + -z[58] + -z[76];
z[58] = abb[1] * z[58];
z[76] = abb[27] * z[1];
z[34] = abb[29] * z[34];
z[0] = -abb[31] + abb[33] + -abb[57] + abb[61] + z[0] + z[34] + -z[70] + -z[76];
z[34] = -abb[9] * z[0];
z[30] = abb[26] * z[30];
z[76] = z[30] + z[73];
z[66] = -abb[27] + z[66];
z[66] = abb[27] * z[66];
z[77] = 3 * z[17] + -z[66];
z[78] = z[42] + z[76] + z[77];
z[4] = z[4] * z[78];
z[4] = z[4] + 2 * z[34];
z[29] = abb[30] + z[29];
z[29] = abb[30] * z[29];
z[34] = z[29] + -z[62];
z[76] = -z[34] + z[76];
z[76] = abb[11] * z[76];
z[45] = -abb[29] + 2 * z[45];
z[45] = abb[29] * z[45];
z[36] = -z[36] + z[45] + z[74];
z[45] = abb[27] * z[15];
z[74] = -abb[24] + 2 * z[1];
z[74] = abb[24] * z[74];
z[45] = -z[36] + z[45] + z[74];
z[79] = abb[5] * z[45];
z[80] = -abb[0] + abb[1];
z[81] = -abb[2] + -abb[3] + z[80];
z[82] = 2 * abb[34];
z[81] = z[81] * z[82];
z[33] = abb[3] + -abb[8] + z[33];
z[83] = 2 * abb[59];
z[33] = z[33] * z[83];
z[84] = abb[5] + abb[11];
z[84] = z[64] * z[84];
z[85] = 4 * abb[45];
z[85] = z[22] * z[85];
z[86] = 2 * abb[45];
z[23] = z[23] * z[86];
z[87] = 2 * abb[60];
z[5] = z[5] * z[87];
z[88] = z[32] + z[80];
z[89] = 2 * abb[35];
z[90] = z[88] * z[89];
z[91] = abb[15] * abb[37];
z[5] = -z[4] + z[5] + -z[11] + z[14] + z[23] + z[28] + z[33] + -z[58] + z[68] + z[72] + z[76] + z[79] + -z[81] + -z[84] + z[85] + -z[90] + 2 * z[91];
z[11] = -abb[52] * z[5];
z[14] = z[29] + z[65];
z[23] = z[14] + z[64] + z[77];
z[20] = z[20] * z[23];
z[5] = -z[5] + -z[20];
z[5] = abb[54] * z[5];
z[20] = 3 * abb[23] + -5 * abb[28] + z[15] + z[21];
z[20] = abb[27] * z[20];
z[28] = -abb[24] + abb[27] + abb[28];
z[28] = z[13] * z[28];
z[29] = abb[28] * z[3];
z[33] = 2 * abb[32];
z[29] = z[29] + z[33];
z[58] = prod_pow(abb[23], 2);
z[10] = 4 * z[10] + (T(16) / T(3)) * z[17] + z[20] + z[28] + -z[29] + z[53] + z[58] + z[61] + z[65] + z[75];
z[10] = abb[0] * z[10];
z[20] = -z[8] + z[15] + -z[25];
z[20] = abb[27] * z[20];
z[3] = -z[3] + z[25];
z[28] = z[3] * z[25];
z[65] = -abb[23] + z[25];
z[68] = -abb[24] + 2 * z[65];
z[68] = abb[24] * z[68];
z[18] = z[18] + -z[28] + z[68];
z[28] = abb[24] + -abb[25];
z[28] = z[13] * z[28];
z[68] = abb[28] * z[8];
z[68] = z[33] + z[68];
z[72] = z[58] + -z[73];
z[20] = -z[18] + -z[20] + z[28] + z[68] + z[72];
z[20] = abb[5] * z[20];
z[28] = prod_pow(abb[27], 2);
z[75] = abb[27] * z[13];
z[76] = (T(1) / T(3)) * z[17];
z[26] = -z[26] + -z[28] + z[49] + z[62] + z[75] + z[76];
z[26] = abb[2] * z[26];
z[28] = z[31] * z[57];
z[31] = -abb[27] + z[60];
z[31] = abb[27] * z[31];
z[7] = z[7] + -z[28] + z[31] + -z[56] + -z[76];
z[7] = abb[1] * z[7];
z[28] = z[33] + z[35];
z[12] = z[12] + -z[18] + z[28] + -z[49] + -z[63];
z[12] = abb[4] * z[12];
z[3] = abb[27] * z[3];
z[6] = -abb[25] + z[6];
z[6] = abb[25] * z[6];
z[3] = z[3] + -z[6] + z[29] + z[52] + z[58];
z[3] = abb[10] * z[3];
z[13] = z[13] * z[43];
z[13] = z[13] + z[16] + z[28] + -z[55];
z[13] = abb[3] * z[13];
z[9] = abb[27] * z[9];
z[16] = z[9] + z[76];
z[18] = z[16] + z[71];
z[31] = -z[18] + z[34] + z[72];
z[35] = abb[11] * z[31];
z[56] = abb[34] * z[80];
z[57] = abb[60] * z[41];
z[56] = z[56] + z[57];
z[57] = z[22] * z[86];
z[3] = z[3] + -z[4] + -z[7] + z[10] + z[12] + -z[13] + -z[20] + -z[26] + -z[35] + -4 * z[56] + z[57];
z[3] = -z[3] * z[24];
z[1] = z[1] * z[21];
z[4] = z[61] + z[75];
z[7] = abb[28] + z[39];
z[7] = abb[28] * z[7];
z[6] = -z[1] + z[4] + z[6] + -z[7] + -z[33] + z[53] + -z[73];
z[6] = -z[6] * z[38];
z[4] = z[4] + z[72];
z[7] = -z[19] + z[74];
z[10] = z[7] + -z[55];
z[12] = -z[4] + z[10] + -z[53] + z[66] + 2 * z[71] + z[76];
z[12] = z[12] * z[40];
z[13] = z[25] * z[69];
z[13] = z[13] + -z[76];
z[19] = abb[27] + 2 * z[43];
z[19] = abb[27] * z[19];
z[7] = -z[7] + z[13] + z[19] + z[28] + z[58];
z[7] = -z[7] * z[46];
z[6] = z[6] + z[7] + -z[12];
z[6] = abb[18] * z[6];
z[7] = abb[27] * z[8];
z[4] = z[4] + -z[7] + z[49] + -z[71];
z[4] = -z[4] * z[38];
z[10] = z[10] + z[18] + z[52];
z[10] = z[10] * z[46];
z[4] = z[4] + z[10] + -z[12];
z[4] = abb[20] * z[4];
z[7] = -z[7] + (T(10) / T(3)) * z[17] + z[42] + z[64] + z[71] + -z[72];
z[7] = abb[5] * z[7];
z[10] = z[30] + z[58];
z[8] = -3 * z[8] + z[21];
z[8] = abb[27] * z[8];
z[8] = z[8] + z[10] + z[14] + (T(8) / T(3)) * z[17] + -z[71];
z[8] = abb[0] * z[8];
z[12] = -abb[6] * z[78];
z[14] = -z[41] * z[87];
z[7] = z[7] + z[8] + z[12] + z[14];
z[7] = z[7] * z[37];
z[8] = z[13] + z[29];
z[12] = z[27] + z[65];
z[13] = abb[27] * z[12];
z[13] = z[8] + z[13] + -z[36];
z[13] = z[13] * z[38];
z[12] = z[12] + -z[15];
z[12] = abb[27] * z[12];
z[8] = z[8] + z[12] + -z[74];
z[8] = -z[8] * z[46];
z[12] = z[64] + -z[87];
z[12] = z[12] * z[48];
z[14] = -z[40] * z[45];
z[8] = z[8] + z[12] + z[13] + z[14];
z[8] = abb[19] * z[8];
z[12] = z[52] + z[67];
z[1] = z[1] + z[12] + -z[36] + 2 * z[70];
z[1] = -z[1] * z[40];
z[12] = z[12] + z[16] + -z[68];
z[12] = z[12] * z[46];
z[14] = z[82] + -z[87];
z[14] = z[14] * z[48];
z[1] = z[1] + z[12] + z[13] + z[14];
z[1] = abb[17] * z[1];
z[9] = z[9] + -z[58] + z[71];
z[9] = -2 * z[9] + z[30] + z[34] + -z[59] + -z[73];
z[9] = -z[9] * z[40];
z[12] = z[31] * z[38];
z[10] = z[10] + -z[18];
z[10] = z[10] * z[46];
z[9] = z[9] + z[10] + z[12];
z[9] = abb[21] * z[9];
z[0] = z[0] * z[44];
z[10] = abb[12] + abb[13];
z[12] = abb[14] + abb[15] + z[10];
z[12] = -z[12] * z[24];
z[13] = -z[38] + z[46];
z[13] = abb[22] * z[13];
z[10] = z[10] * z[37];
z[10] = z[10] + z[12] + z[13];
z[10] = abb[37] * z[10];
z[12] = z[22] * z[48];
z[13] = z[24] * z[88];
z[12] = z[12] + z[13] + z[47];
z[12] = z[12] * z[89];
z[13] = -2 * z[24];
z[13] = z[13] * z[32];
z[14] = -abb[19] * z[48];
z[13] = z[13] + z[14] + -z[47];
z[13] = z[13] * z[83];
z[14] = z[47] * z[82];
z[15] = z[51] * z[87];
z[16] = -z[23] * z[54];
z[17] = z[50] * z[64];
z[0] = 2 * z[0] + z[1] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + z[17];
z[0] = 2 * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_698_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-10.402302819131233336271940183191322136570276882477279319319257397"),stof<T>("12.769388978144903263363784176908690571680015421503048994714708313")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("10.402302819131233336271940183191322136570276882477279319319257397"),stof<T>("-12.769388978144903263363784176908690571680015421503048994714708313")}, std::complex<T>{stof<T>("-55.655065792243148404594928551035256275046546730325180159318277966"),stof<T>("7.742045297973095982344859865877476624342029237183667843740671537")}, std::complex<T>{stof<T>("-55.655065792243148404594928551035256275046546730325180159318277966"),stof<T>("7.742045297973095982344859865877476624342029237183667843740671537")}, std::complex<T>{stof<T>("-11.900894076796136801904386879572424097608812135765383308254157777"),stof<T>("-14.855427600915246846542877066135400887977168833731406221930959934")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_698_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_698_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("38.110611841821838256654171749691395615376097025881289769791065523"),stof<T>("-99.752436708742491903324638875885352772063604153708341344612993614")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,65> abb = {dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W22(k,dl), dlog_W25(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_16(k), f_2_17(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_9_im(k), f_2_12_im(k), f_2_22_im(k), f_2_27_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_9_re(k), f_2_12_re(k), f_2_22_re(k), f_2_27_re(k)};

                    
            return f_4_698_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_698_DLogXconstant_part(base_point<T>, kend);
	value += f_4_698_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_698_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_698_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_698_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_698_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_698_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_698_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
