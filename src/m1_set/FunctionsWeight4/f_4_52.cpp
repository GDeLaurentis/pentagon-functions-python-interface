/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_52.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_52_abbreviated (const std::array<T,26>& abb) {
T z[26];
z[0] = abb[18] + abb[20];
z[1] = -abb[19] + 2 * abb[25];
z[2] = abb[21] * (T(1) / T(4)) + (T(3) / T(4)) * z[0] + -z[1];
z[3] = abb[9] * z[2];
z[4] = abb[22] + abb[24];
z[5] = abb[6] * z[4];
z[3] = z[3] + (T(-3) / T(4)) * z[5];
z[6] = abb[7] * z[2];
z[7] = z[3] + z[6];
z[8] = abb[21] * (T(1) / T(2)) + (T(3) / T(2)) * z[0] + -2 * z[1];
z[9] = abb[8] * z[8];
z[10] = abb[1] * z[4];
z[11] = abb[2] * z[4];
z[12] = z[10] + (T(1) / T(2)) * z[11];
z[13] = (T(1) / T(4)) * z[4];
z[14] = -abb[23] + z[13];
z[14] = abb[3] * z[14];
z[15] = abb[0] * z[13];
z[16] = abb[23] + z[4];
z[16] = abb[5] * z[16];
z[17] = -z[7] + -z[9] + -z[12] + z[14] + -z[15] + -z[16];
z[17] = abb[14] * z[17];
z[18] = abb[0] + abb[3];
z[19] = (T(1) / T(2)) * z[4];
z[20] = z[18] * z[19];
z[21] = abb[7] * z[8];
z[20] = z[20] + -z[21];
z[21] = abb[9] * z[8];
z[5] = (T(-3) / T(2)) * z[5] + z[21];
z[21] = 2 * abb[1];
z[21] = z[4] * z[21];
z[21] = z[11] + z[21];
z[22] = z[5] + -z[20] + 2 * z[21];
z[22] = abb[12] * z[22];
z[13] = abb[3] * z[13];
z[7] = z[7] + -z[13] + -z[15] + z[21];
z[15] = -abb[11] + abb[13];
z[15] = z[7] * z[15];
z[15] = z[15] + z[17] + z[22];
z[15] = abb[14] * z[15];
z[17] = 5 * abb[22] + abb[24];
z[17] = abb[23] + (T(1) / T(4)) * z[17];
z[17] = abb[0] * z[17];
z[3] = -z[3] + z[17];
z[17] = abb[23] + -abb[24];
z[17] = abb[4] * z[17];
z[23] = z[16] + -z[17];
z[14] = z[3] + -3 * z[10] + -2 * z[11] + z[14] + z[23];
z[24] = prod_pow(abb[12], 2);
z[14] = z[14] * z[24];
z[3] = z[3] + z[6] + -z[12] + -z[13] + z[17];
z[3] = abb[11] * z[3];
z[3] = z[3] + z[22];
z[3] = abb[11] * z[3];
z[6] = abb[11] * z[7];
z[7] = abb[2] * abb[22];
z[13] = 2 * z[7] + z[10];
z[20] = -z[13] + z[20];
z[20] = abb[13] * z[20];
z[6] = z[6] + z[20] + -z[22];
z[6] = abb[13] * z[6];
z[20] = 13 * abb[23];
z[22] = -25 * abb[22] + abb[24];
z[22] = -z[20] + (T(1) / T(2)) * z[22];
z[22] = abb[0] * z[22];
z[20] = z[19] + z[20];
z[20] = abb[3] * z[20];
z[7] = 11 * z[7] + -z[10] + z[20] + z[22];
z[20] = prod_pow(m1_set::bc<T>[0], 2);
z[7] = z[7] * z[20];
z[22] = -z[2] * z[24];
z[24] = abb[21] * (T(-1) / T(12)) + (T(-1) / T(4)) * z[0] + (T(1) / T(3)) * z[1];
z[20] = z[20] * z[24];
z[8] = -abb[16] * z[8];
z[24] = (T(3) / T(4)) * z[4];
z[25] = abb[17] * z[24];
z[8] = z[8] + z[20] + z[22] + z[25];
z[8] = abb[7] * z[8];
z[0] = -abb[21] + -3 * z[0] + 4 * z[1];
z[1] = abb[2] + -2 * abb[10];
z[1] = z[0] * z[1];
z[18] = -abb[6] + -3 * z[18];
z[2] = z[2] * z[18];
z[18] = abb[9] * z[24];
z[1] = z[1] + z[2] + z[18];
z[1] = abb[17] * z[1];
z[2] = z[5] + 3 * z[12];
z[5] = -abb[23] + z[19];
z[5] = abb[3] * z[5];
z[5] = -z[2] + z[5] + -z[16];
z[5] = abb[16] * z[5];
z[12] = 3 * abb[22] + abb[24];
z[12] = abb[23] + (T(1) / T(2)) * z[12];
z[12] = abb[0] * z[12];
z[2] = -z[2] + z[12] + z[17];
z[2] = abb[15] * z[2];
z[12] = prod_pow(abb[11], 2);
z[12] = abb[15] + -abb[16] + z[12];
z[9] = z[9] * z[12];
z[1] = z[1] + z[2] + z[3] + z[5] + z[6] + (T(1) / T(6)) * z[7] + z[8] + z[9] + z[14] + z[15];
z[2] = abb[3] * abb[23];
z[3] = abb[22] + abb[23];
z[3] = abb[0] * z[3];
z[5] = z[2] + -z[3] + z[10] + z[11] + -z[23];
z[5] = abb[12] * m1_set::bc<T>[0] * z[5];
z[3] = z[3] + z[17];
z[6] = abb[3] * z[4];
z[3] = -2 * z[3] + z[6] + -z[21];
z[3] = m1_set::bc<T>[0] * z[3];
z[0] = m1_set::bc<T>[0] * z[0];
z[7] = abb[7] * z[0];
z[0] = abb[8] * z[0];
z[3] = z[0] + z[3] + z[7];
z[3] = abb[11] * z[3];
z[4] = abb[0] * z[4];
z[6] = -z[4] + -z[6] + 2 * z[13];
z[6] = m1_set::bc<T>[0] * z[6];
z[6] = z[6] + -z[7];
z[6] = abb[13] * z[6];
z[2] = z[2] + z[16];
z[2] = 2 * z[2] + z[4] + -z[21];
z[2] = m1_set::bc<T>[0] * z[2];
z[0] = -z[0] + z[2];
z[0] = abb[14] * z[0];
z[0] = z[0] + z[3] + 2 * z[5] + z[6];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_52_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-2.301289249236299788229142824963213884607522828012936456197368814"),stof<T>("-23.623397538326071070186375418229279950247593024717894811595338102")}, std::complex<T>{stof<T>("-3.068385665648399717638857099950951846143363770683915274929825085"),stof<T>("-31.497863384434761426915167224305706600330124032957193082127117469")}, std::complex<T>{stof<T>("-2.301289249236299788229142824963213884607522828012936456197368814"),stof<T>("-23.623397538326071070186375418229279950247593024717894811595338102")}, std::complex<T>{stof<T>("-0.7670964164120999294097142749877379615358409426709788187324562713"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("10.197404780206810193344980928934651809138084455046905100361927776"),stof<T>("20.458298351426015107106898150117174431191673240301772625993104768")}, std::complex<T>{stof<T>("31.159042455814367431017888404754588939611372077132970370600898193"),stof<T>("37.120487831621476733788850766478584132000485213499022896162861463")}, std::complex<T>{stof<T>("-0.8019285043356912841076702955806452428926438737558145946325952587"),stof<T>("-1.9232461890621132253448052434278699448465971300673805737648824541")}, std::complex<T>{stof<T>("6.136771331296799435277714199901903692286727541367830549859650171"),stof<T>("62.995726768869522853830334448611413200660248065914386164254234938")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[123].real()/kbase.W[123].real()), rlog(k.W[194].real()/kbase.W[194].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_52_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_52_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-36.118305944321496817704806091165451157091303517719321158621236885"),stof<T>("9.639617865581240152736631337069209401082804367992740311407016034")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,26> abb = {dl[0], dl[1], dl[2], dl[4], dlog_W12(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real()), rlog(kend.W[194].real()/k.W[194].real())};

                    
            return f_4_52_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_52_DLogXconstant_part(base_point<T>, kend);
	value += f_4_52_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_52_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_52_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_52_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_52_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_52_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_52_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
