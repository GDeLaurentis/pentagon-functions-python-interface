/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_774.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_774_abbreviated (const std::array<T,81>& abb) {
T z[131];
z[0] = 2 * abb[37];
z[1] = 2 * abb[31];
z[2] = z[0] + -z[1];
z[3] = 3 * abb[34];
z[4] = abb[32] + abb[36];
z[5] = z[2] + z[3] + -z[4];
z[5] = m1_set::bc<T>[0] * z[5];
z[6] = abb[35] * m1_set::bc<T>[0];
z[7] = -abb[51] + z[6];
z[5] = z[5] + -z[7];
z[8] = 2 * abb[2];
z[9] = z[5] * z[8];
z[10] = 2 * abb[33];
z[11] = -abb[37] + z[10];
z[12] = -abb[32] + z[11];
z[12] = m1_set::bc<T>[0] * z[12];
z[12] = abb[47] + z[12];
z[13] = abb[45] + abb[49];
z[14] = abb[46] + abb[51];
z[15] = z[12] + z[13] + z[14];
z[16] = abb[8] * z[15];
z[17] = abb[31] + -abb[34];
z[18] = -abb[32] + abb[33];
z[19] = z[17] + -z[18];
z[19] = m1_set::bc<T>[0] * z[19];
z[19] = abb[46] + -abb[47] + z[19];
z[20] = abb[14] * z[19];
z[21] = abb[36] + -abb[37];
z[22] = -abb[34] + 2 * z[21];
z[23] = m1_set::bc<T>[0] * z[22];
z[23] = abb[46] + z[23];
z[24] = abb[38] * m1_set::bc<T>[0];
z[25] = -abb[45] + z[24];
z[26] = z[23] + z[25];
z[27] = abb[17] * z[26];
z[28] = 2 * abb[34];
z[29] = -z[21] + z[28];
z[30] = -z[1] + z[29];
z[30] = m1_set::bc<T>[0] * z[30];
z[31] = 2 * abb[46];
z[30] = z[30] + -z[31];
z[32] = abb[16] * z[30];
z[9] = z[9] + z[16] + z[20] + z[27] + z[32];
z[16] = 3 * abb[37];
z[20] = -z[10] + z[16];
z[33] = -abb[36] + z[20];
z[34] = 4 * abb[34];
z[35] = z[1] + -z[33] + -z[34];
z[35] = m1_set::bc<T>[0] * z[35];
z[36] = -abb[49] + abb[51];
z[37] = abb[47] + z[6];
z[35] = z[24] + z[35] + -z[36] + z[37];
z[35] = abb[7] * z[35];
z[38] = 3 * abb[32];
z[39] = -abb[31] + z[34];
z[40] = -abb[33] + z[0];
z[41] = -z[38] + z[39] + z[40];
z[41] = m1_set::bc<T>[0] * z[41];
z[42] = abb[45] + -abb[49];
z[41] = -z[7] + z[41] + z[42];
z[41] = abb[4] * z[41];
z[43] = -abb[31] + z[28];
z[44] = abb[33] + -abb[37];
z[45] = z[43] + -z[44];
z[45] = m1_set::bc<T>[0] * z[45];
z[45] = z[14] + -z[37] + z[45];
z[46] = 2 * abb[10];
z[47] = z[45] * z[46];
z[48] = -abb[3] + abb[7];
z[49] = -abb[8] + z[46] + -z[48];
z[50] = abb[5] + -abb[17] + z[49];
z[50] = abb[50] * z[50];
z[51] = abb[31] + -abb[36];
z[52] = abb[33] + z[51];
z[53] = m1_set::bc<T>[0] * z[52];
z[53] = abb[49] + -z[25] + z[53];
z[54] = abb[15] * z[53];
z[55] = z[1] + -z[21];
z[55] = m1_set::bc<T>[0] * z[55];
z[56] = 2 * z[24];
z[57] = 2 * abb[45] + -z[56];
z[55] = z[55] + z[57];
z[58] = abb[0] * z[55];
z[59] = 2 * abb[36];
z[60] = z[3] + -z[59];
z[60] = m1_set::bc<T>[0] * z[60];
z[61] = -abb[49] + z[37];
z[60] = z[60] + -z[61];
z[60] = abb[6] * z[60];
z[62] = -abb[8] + abb[17];
z[63] = -abb[4] + z[8];
z[64] = -abb[6] + z[62] + -z[63];
z[65] = abb[48] * z[64];
z[66] = -abb[34] + z[1];
z[66] = m1_set::bc<T>[0] * z[66];
z[66] = -abb[48] + -abb[51] + -z[24] + z[66];
z[67] = -abb[3] * z[66];
z[68] = -abb[31] + z[44];
z[69] = m1_set::bc<T>[0] * z[68];
z[70] = -abb[46] + z[37] + z[69];
z[71] = abb[5] * z[70];
z[72] = abb[20] + abb[21];
z[73] = abb[18] + z[72];
z[74] = abb[52] * z[73];
z[35] = -z[9] + z[35] + z[41] + z[47] + z[50] + z[54] + -z[58] + z[60] + z[65] + z[67] + z[71] + z[74];
z[35] = abb[64] * z[35];
z[41] = abb[58] + abb[61];
z[47] = abb[45] + z[14];
z[47] = z[41] * z[47];
z[60] = abb[56] + -abb[61] + abb[62];
z[65] = abb[58] + -z[60];
z[67] = abb[48] * z[65];
z[71] = abb[45] * abb[56];
z[74] = z[67] + -z[71];
z[47] = z[47] + z[74];
z[75] = z[17] + z[44];
z[75] = m1_set::bc<T>[0] * z[75];
z[31] = abb[51] + z[13] + z[31] + z[75];
z[31] = abb[62] * z[31];
z[75] = abb[49] + z[75];
z[76] = abb[51] + -z[75];
z[76] = abb[56] * z[76];
z[75] = abb[46] + z[75];
z[77] = abb[59] * z[75];
z[31] = z[31] + -z[47] + z[76] + z[77];
z[31] = abb[23] * z[31];
z[15] = abb[62] * z[15];
z[76] = -abb[49] + z[14];
z[77] = -z[12] + z[76];
z[77] = abb[56] * z[77];
z[12] = abb[49] + z[12];
z[78] = abb[59] * z[12];
z[79] = abb[67] * abb[78];
z[15] = z[15] + -z[47] + z[77] + z[78] + z[79];
z[15] = abb[25] * z[15];
z[47] = abb[23] + abb[25];
z[77] = -abb[28] + -z[47];
z[77] = z[65] * z[77];
z[78] = z[48] + z[62];
z[79] = -abb[67] * z[78];
z[78] = abb[5] + -z[78];
z[78] = abb[65] * z[78];
z[80] = 2 * abb[66];
z[81] = -abb[7] * z[80];
z[82] = abb[67] + z[80];
z[82] = abb[5] * z[82];
z[77] = z[77] + z[78] + z[79] + z[81] + z[82];
z[77] = abb[50] * z[77];
z[78] = -abb[62] + z[41];
z[26] = -z[26] * z[78];
z[23] = z[23] + z[24];
z[23] = abb[56] * z[23];
z[23] = z[23] + z[26] + z[74];
z[23] = abb[28] * z[23];
z[26] = abb[37] * m1_set::bc<T>[0];
z[26] = -z[6] + z[26];
z[74] = -abb[67] * z[26];
z[79] = 2 * abb[32];
z[81] = -abb[33] + z[79];
z[82] = -abb[31] + z[81];
z[82] = m1_set::bc<T>[0] * z[82];
z[82] = -abb[47] + z[14] + z[82];
z[82] = abb[66] * z[82];
z[74] = z[74] + z[82];
z[74] = abb[5] * z[74];
z[54] = abb[63] * z[54];
z[15] = z[15] + z[23] + z[31] + z[35] + z[54] + z[74] + z[77];
z[23] = z[39] + -z[44] + -z[59];
z[23] = m1_set::bc<T>[0] * z[23];
z[31] = 2 * abb[47];
z[14] = abb[49] + -z[6] + z[14] + z[23] + -z[31];
z[23] = 2 * abb[7];
z[14] = z[14] * z[23];
z[35] = 2 * abb[3];
z[35] = z[35] * z[66];
z[54] = 2 * abb[48];
z[54] = z[54] * z[64];
z[35] = -z[35] + z[54] + -z[58];
z[54] = 4 * abb[32];
z[66] = -abb[36] + z[11];
z[74] = -z[28] + z[54] + z[66];
z[74] = m1_set::bc<T>[0] * z[74];
z[77] = 2 * z[6];
z[82] = 2 * abb[49];
z[74] = z[74] + -z[77] + z[82];
z[74] = abb[4] * z[74];
z[5] = abb[2] * z[5];
z[66] = m1_set::bc<T>[0] * z[66];
z[66] = z[66] + z[82];
z[83] = abb[15] * z[66];
z[32] = -z[32] + z[83];
z[83] = 2 * m1_set::bc<T>[0];
z[84] = z[18] * z[83];
z[84] = abb[45] + -abb[46] + z[31] + z[84];
z[85] = 2 * abb[8];
z[84] = z[84] * z[85];
z[86] = -abb[32] + abb[37];
z[87] = z[43] + z[86];
z[83] = z[83] * z[87];
z[83] = abb[51] + -z[77] + z[83];
z[88] = 2 * abb[14];
z[83] = z[83] * z[88];
z[89] = abb[36] * m1_set::bc<T>[0];
z[89] = -z[6] + z[89];
z[90] = 2 * abb[6];
z[89] = z[89] * z[90];
z[5] = 4 * z[5] + z[14] + 2 * z[27] + -z[32] + -z[35] + z[74] + -z[83] + z[84] + -z[89];
z[14] = 2 * abb[5];
z[26] = -z[14] * z[26];
z[27] = 2 * abb[25];
z[74] = abb[78] * z[27];
z[83] = -abb[19] + z[72];
z[84] = abb[52] * z[83];
z[26] = -z[5] + z[26] + z[74] + z[84];
z[26] = abb[65] * z[26];
z[74] = abb[18] * abb[63];
z[84] = 2 * abb[63];
z[89] = abb[20] * z[84];
z[74] = z[74] + z[89];
z[89] = 2 * abb[62];
z[91] = z[41] + -z[89];
z[92] = abb[57] + -abb[59];
z[93] = -z[91] + -z[92];
z[93] = abb[29] * z[93];
z[94] = abb[66] + -abb[67];
z[94] = abb[19] * z[94];
z[95] = abb[67] * z[72];
z[96] = abb[66] * z[73];
z[93] = z[74] + z[93] + z[94] + z[95] + z[96];
z[93] = abb[52] * z[93];
z[55] = -z[41] * z[55];
z[69] = -abb[49] + -z[24] + -z[69];
z[69] = abb[56] * z[69];
z[69] = z[69] + z[71];
z[95] = z[53] * z[89];
z[55] = z[55] + 2 * z[69] + z[95];
z[55] = abb[22] * z[55];
z[69] = z[12] * z[27];
z[75] = abb[23] * z[75];
z[66] = abb[22] * z[66];
z[69] = z[66] + z[69] + 2 * z[75];
z[75] = abb[57] * z[69];
z[95] = abb[31] + -abb[33];
z[95] = m1_set::bc<T>[0] * z[95];
z[42] = z[42] + z[95];
z[42] = abb[4] * z[42] * z[84];
z[66] = abb[59] * z[66];
z[26] = -z[26] + -z[42] + -z[55] + -z[66] + z[75] + -z[93];
z[42] = 2 * abb[67];
z[5] = -z[5] * z[42];
z[55] = -z[28] + z[79];
z[33] = z[33] + -z[55];
z[33] = m1_set::bc<T>[0] * z[33];
z[33] = 4 * abb[45] + z[33] + 2 * z[36];
z[33] = abb[4] * z[33];
z[66] = -abb[36] + z[1];
z[54] = 4 * abb[33] + -abb[37] + -z[54] + z[66];
z[54] = m1_set::bc<T>[0] * z[54];
z[54] = z[31] + z[54] + -2 * z[76];
z[54] = abb[7] * z[54];
z[19] = -z[19] * z[88];
z[12] = -z[12] * z[85];
z[75] = 2 * z[86];
z[76] = abb[34] + z[75];
z[66] = z[66] + -z[76];
z[66] = m1_set::bc<T>[0] * z[66];
z[36] = -abb[47] + -z[36] + z[66];
z[36] = z[36] * z[90];
z[66] = -abb[4] + abb[6];
z[85] = 4 * z[66];
z[90] = -abb[48] * z[85];
z[12] = z[12] + z[19] + z[32] + z[33] + z[36] + z[54] + z[90];
z[12] = z[12] * z[80];
z[19] = -abb[34] + z[59];
z[32] = z[19] + z[68];
z[32] = m1_set::bc<T>[0] * z[32];
z[6] = -z[6] + z[24] + z[32];
z[6] = abb[56] * z[6];
z[6] = z[6] + z[67] + -z[71];
z[24] = -abb[34] + z[52];
z[24] = m1_set::bc<T>[0] * z[24];
z[24] = z[24] + -z[25] + z[37];
z[24] = z[24] * z[89];
z[25] = 3 * abb[36];
z[32] = -z[1] + z[25];
z[33] = -abb[37] + z[32];
z[33] = m1_set::bc<T>[0] * z[33];
z[33] = z[33] + -z[57] + -z[77];
z[33] = z[33] * z[41];
z[36] = -z[10] + z[29];
z[36] = m1_set::bc<T>[0] * z[36];
z[36] = -z[31] + z[36];
z[37] = abb[60] + z[92];
z[36] = z[36] * z[37];
z[52] = abb[56] * z[31];
z[54] = abb[78] * z[80];
z[6] = 2 * z[6] + -z[24] + -z[33] + -z[36] + z[52] + -z[54];
z[24] = abb[24] + abb[26];
z[24] = 2 * z[24];
z[6] = -z[6] * z[24];
z[33] = -abb[32] + z[28];
z[2] = z[2] + z[33];
z[2] = m1_set::bc<T>[0] * z[2];
z[2] = z[2] + -z[7];
z[2] = abb[4] * z[2];
z[7] = abb[34] * m1_set::bc<T>[0];
z[7] = z[7] + -z[61];
z[7] = abb[6] * z[7];
z[2] = z[2] + z[7] + -z[9] + z[50];
z[7] = 4 * abb[31] + -8 * abb[34] + -z[20] + z[25];
z[7] = m1_set::bc<T>[0] * z[7];
z[7] = -2 * abb[51] + z[7] + z[31] + z[77];
z[7] = abb[7] * z[7];
z[9] = abb[10] * z[45];
z[14] = z[14] * z[70];
z[25] = abb[18] + 2 * abb[21];
z[31] = abb[52] * z[25];
z[2] = 2 * z[2] + z[7] + 4 * z[9] + z[14] + z[31] + z[35];
z[7] = 2 * abb[68];
z[2] = z[2] * z[7];
z[9] = -abb[29] * abb[52];
z[9] = z[9] + -z[69];
z[14] = 2 * abb[60];
z[9] = z[9] * z[14];
z[31] = abb[36] + z[20];
z[31] = m1_set::bc<T>[0] * z[31];
z[31] = z[31] + -z[56] + -z[82];
z[35] = abb[18] * z[31];
z[36] = 2 * abb[20];
z[45] = -z[36] * z[53];
z[50] = z[44] + z[51];
z[50] = m1_set::bc<T>[0] * z[50];
z[13] = z[13] + z[50];
z[50] = abb[19] * z[13];
z[52] = abb[4] + abb[15] + -4 * abb[30];
z[52] = abb[0] + abb[7] + 2 * z[52];
z[53] = -abb[52] * z[52];
z[35] = z[35] + z[45] + -2 * z[50] + z[53];
z[45] = 2 * abb[69];
z[35] = z[35] * z[45];
z[31] = -abb[7] * z[31];
z[31] = z[31] + -z[58];
z[31] = z[31] * z[84];
z[37] = z[37] + z[91];
z[50] = 2 * abb[27];
z[37] = z[37] * z[50];
z[30] = z[30] * z[37];
z[50] = abb[63] + abb[64] + abb[66];
z[53] = 8 * abb[13];
z[50] = z[50] * z[53];
z[13] = -z[13] * z[50];
z[2] = abb[79] + abb[80] + z[2] + z[5] + z[6] + z[9] + z[12] + z[13] + 4 * z[15] + -2 * z[26] + z[30] + z[31] + z[35];
z[5] = z[0] * z[86];
z[6] = 2 * abb[39];
z[9] = prod_pow(abb[32], 2);
z[12] = z[6] + -z[9];
z[13] = 2 * abb[74];
z[15] = abb[33] * z[40];
z[5] = z[5] + -z[12] + -z[13] + -z[15];
z[26] = -z[21] + z[55];
z[26] = abb[31] * z[26];
z[30] = -abb[37] + z[79];
z[31] = -abb[34] + z[30];
z[31] = z[28] * z[31];
z[35] = -abb[38] + z[0];
z[40] = 2 * abb[38];
z[53] = z[35] * z[40];
z[54] = -abb[35] + z[79];
z[54] = abb[35] * z[54];
z[56] = 4 * abb[42];
z[57] = 2 * abb[44];
z[58] = abb[36] * abb[37];
z[61] = 2 * abb[40];
z[26] = 4 * abb[41] + z[5] + z[26] + -z[31] + -z[53] + z[54] + z[56] + z[57] + z[58] + -z[61];
z[26] = abb[4] * z[26];
z[31] = abb[36] * z[44];
z[53] = abb[74] + z[31];
z[54] = abb[33] * z[81];
z[67] = abb[40] + z[9] + z[53] + -z[54];
z[68] = -abb[37] + -z[28] + z[81];
z[68] = abb[31] + 2 * z[68];
z[68] = abb[31] * z[68];
z[69] = -abb[36] + z[75];
z[70] = 5 * abb[34];
z[69] = 2 * z[69] + z[70];
z[69] = abb[34] * z[69];
z[71] = 2 * abb[71];
z[75] = prod_pow(m1_set::bc<T>[0], 2);
z[77] = z[71] + z[75];
z[82] = -abb[36] + z[43];
z[90] = 2 * abb[35];
z[82] = z[82] * z[90];
z[92] = 4 * abb[72];
z[93] = 2 * abb[75];
z[67] = 2 * z[67] + z[68] + z[69] + z[77] + -z[82] + -z[92] + z[93];
z[67] = abb[7] * z[67];
z[68] = -z[1] + z[90];
z[68] = z[68] * z[86];
z[69] = z[10] * z[81];
z[81] = abb[37] * z[30];
z[95] = -z[57] + z[81];
z[96] = 2 * abb[70];
z[97] = -z[71] + z[96];
z[98] = (T(5) / T(3)) * z[75];
z[68] = -z[9] + -z[68] + z[69] + z[92] + -z[95] + z[97] + z[98];
z[68] = abb[8] * z[68];
z[69] = -abb[35] + 2 * z[87];
z[69] = abb[35] * z[69];
z[87] = abb[34] + z[86];
z[34] = z[34] * z[87];
z[92] = z[28] + z[86];
z[92] = -abb[31] + 2 * z[92];
z[92] = abb[31] * z[92];
z[99] = (T(1) / T(3)) * z[75];
z[34] = z[9] + z[34] + -z[69] + -z[92] + -z[95] + z[99];
z[34] = abb[14] * z[34];
z[69] = 2 * abb[42];
z[100] = z[9] + z[69];
z[101] = (T(4) / T(3)) * z[75];
z[92] = -z[81] + -z[92] + z[100] + z[101];
z[102] = -abb[34] + z[21];
z[103] = abb[31] + z[102];
z[103] = z[90] * z[103];
z[104] = abb[32] + z[21];
z[105] = -z[70] + 4 * z[104];
z[105] = abb[34] * z[105];
z[106] = abb[36] * z[79];
z[103] = z[61] + z[92] + z[103] + -z[105] + z[106];
z[103] = z[8] * z[103];
z[105] = z[1] * z[102];
z[106] = z[21] * z[90];
z[22] = abb[34] * z[22];
z[107] = -abb[31] + z[21];
z[108] = abb[38] + 2 * z[107];
z[108] = abb[38] * z[108];
z[22] = z[22] + -z[97] + -z[105] + -z[106] + z[108];
z[97] = abb[17] * z[22];
z[64] = abb[3] + z[64];
z[105] = 2 * abb[73];
z[64] = z[64] * z[105];
z[64] = -z[64] + z[97] + z[103];
z[32] = -z[20] + z[32];
z[32] = abb[31] * z[32];
z[97] = prod_pow(abb[37], 2);
z[15] = -z[15] + z[97];
z[97] = z[6] + z[15];
z[103] = -z[96] + z[97];
z[106] = abb[36] * z[11];
z[32] = -z[32] + z[103] + z[106] + z[108];
z[32] = abb[0] * z[32];
z[108] = -z[32] + z[64];
z[109] = z[10] + z[107];
z[109] = abb[31] * z[109];
z[110] = abb[39] + abb[74];
z[111] = (T(2) / T(3)) * z[75];
z[106] = -z[106] + z[109] + -2 * z[110] + -z[111];
z[109] = abb[15] * z[106];
z[112] = -abb[31] + z[29];
z[112] = abb[31] * z[112];
z[113] = z[58] + z[61];
z[114] = z[111] + z[113];
z[115] = abb[37] * z[28];
z[112] = z[71] + z[112] + z[114] + -z[115];
z[115] = abb[16] * z[112];
z[109] = z[109] + -z[115];
z[40] = z[40] * z[51];
z[116] = 2 * abb[41];
z[117] = z[40] + z[116];
z[118] = abb[31] * z[43];
z[119] = -abb[35] + z[1];
z[119] = abb[35] * z[119];
z[118] = -z[93] + z[117] + z[118] + -z[119];
z[55] = abb[37] + z[55] + z[59];
z[55] = abb[34] * z[55];
z[120] = abb[37] + z[79];
z[120] = abb[36] * z[120];
z[55] = 3 * abb[40] + -z[55] + z[120];
z[120] = 2 * z[75];
z[55] = 2 * z[55] + z[56] + z[118] + -z[120];
z[55] = abb[3] * z[55];
z[19] = abb[34] * z[19];
z[56] = -abb[35] + z[59];
z[56] = abb[35] * z[56];
z[121] = z[19] + -z[56];
z[122] = -z[61] + -z[99] + z[121];
z[122] = abb[6] * z[122];
z[23] = abb[8] + abb[14] + -z[23] + -z[63];
z[123] = 2 * abb[43];
z[23] = z[23] * z[123];
z[62] = z[62] * z[93];
z[23] = -z[23] + -z[26] + -z[34] + z[55] + z[62] + z[67] + z[68] + z[108] + z[109] + -z[122];
z[26] = -abb[77] * z[72];
z[26] = z[23] + z[26];
z[26] = abb[67] * z[26];
z[34] = abb[38] * z[35];
z[34] = z[34] + -z[116];
z[21] = abb[33] + z[21];
z[21] = -abb[31] + 2 * z[21];
z[21] = abb[31] * z[21];
z[10] = abb[36] * z[10];
z[10] = -z[10] + z[21] + z[34] + -z[57] + -z[97];
z[21] = 2 * abb[9];
z[10] = z[10] * z[21];
z[21] = -abb[35] + z[0];
z[21] = abb[35] * z[21];
z[21] = -z[21] + z[34] + -z[93] + -z[99];
z[35] = abb[5] * z[21];
z[55] = -abb[77] * z[83];
z[27] = abb[53] * z[27];
z[23] = -z[10] + z[23] + z[27] + z[35] + z[55];
z[23] = abb[65] * z[23];
z[27] = z[1] + -z[28];
z[27] = z[27] * z[87];
z[17] = z[17] * z[90];
z[35] = 2 * abb[72];
z[55] = z[35] + z[54];
z[9] = -z[9] + z[55];
z[62] = z[9] + -z[93];
z[17] = -z[17] + z[27] + z[62] + -z[71] + -z[111] + -z[123];
z[17] = z[17] * z[46];
z[12] = z[12] + z[54];
z[27] = z[12] + z[35] + -z[69];
z[46] = -abb[31] + z[79];
z[46] = abb[31] * z[46];
z[54] = -abb[34] + z[79];
z[54] = abb[34] * z[54];
z[46] = -z[21] + z[27] + -z[46] + z[54] + -z[71];
z[46] = abb[5] * z[46];
z[67] = -abb[34] + z[4];
z[68] = abb[37] + z[67];
z[68] = abb[34] * z[68];
z[72] = abb[32] + abb[37];
z[72] = abb[36] * z[72];
z[68] = -z[61] + z[68] + -z[72];
z[72] = -z[69] + z[98];
z[68] = 2 * z[68] + z[72] + -z[118];
z[68] = abb[3] * z[68];
z[83] = abb[36] * z[18];
z[87] = abb[32] + z[102];
z[97] = abb[34] * z[87];
z[83] = z[83] + z[97];
z[97] = abb[33] + z[102];
z[102] = abb[31] * z[97];
z[102] = abb[71] + z[83] + -z[102];
z[118] = abb[32] * abb[37];
z[122] = -abb[32] + z[44];
z[124] = abb[33] * z[122];
z[118] = z[118] + z[124];
z[124] = abb[39] + -abb[42] + -abb[72] + z[102] + z[118];
z[125] = abb[14] * z[124];
z[115] = -z[115] + z[125];
z[118] = -abb[74] + z[118];
z[125] = abb[70] + abb[71];
z[126] = -z[118] + z[125];
z[127] = prod_pow(abb[31], 2);
z[127] = -z[119] + z[127];
z[128] = (T(13) / T(3)) * z[75];
z[126] = 2 * z[126] + -z[127] + z[128];
z[129] = z[35] + z[126];
z[129] = abb[8] * z[129];
z[130] = abb[5] + -abb[6] + abb[7] + z[63];
z[130] = z[123] * z[130];
z[49] = z[49] + -z[63];
z[63] = 2 * abb[76];
z[49] = z[49] * z[63];
z[63] = abb[8] + abb[17];
z[63] = z[63] * z[93];
z[17] = -z[17] + z[46] + z[49] + -z[63] + z[68] + -2 * z[115] + -z[129] + -z[130];
z[46] = -4 * abb[44] + z[34] + -z[96];
z[38] = -z[0] + z[38];
z[38] = 2 * z[38] + -z[70];
z[38] = abb[34] * z[38];
z[33] = z[1] * z[33];
z[43] = -z[30] + z[43];
z[43] = z[43] * z[90];
z[5] = -z[5] + z[33] + z[38] + z[43] + z[46] + -z[69] + -z[111];
z[5] = abb[4] * z[5];
z[33] = -z[69] + z[99];
z[38] = z[44] * z[59];
z[38] = z[13] + z[38];
z[43] = abb[32] + -z[29];
z[43] = z[28] * z[43];
z[18] = abb[34] + z[18];
z[18] = -abb[31] + 2 * z[18];
z[18] = abb[31] * z[18];
z[49] = 4 * abb[40];
z[63] = -abb[36] + z[28];
z[63] = -abb[35] + 2 * z[63];
z[63] = abb[35] * z[63];
z[9] = z[9] + z[18] + -z[33] + -z[38] + z[43] + -z[49] + z[63];
z[9] = abb[6] * z[9];
z[43] = -abb[33] * z[51];
z[63] = abb[38] * z[51];
z[68] = abb[70] + z[111];
z[43] = z[43] + z[63] + z[68] + z[110];
z[70] = abb[41] + z[43];
z[115] = abb[15] * z[70];
z[73] = abb[77] * z[73];
z[129] = z[32] + z[73] + z[115];
z[3] = -z[3] + 2 * z[104];
z[3] = z[3] * z[28];
z[3] = z[3] + -z[49] + z[62] + z[82];
z[62] = z[13] + z[101];
z[82] = z[62] + -z[117];
z[4] = abb[33] + z[4] + -z[16] + -z[28];
z[4] = z[1] * z[4];
z[101] = -z[59] * z[122];
z[4] = -z[3] + z[4] + -z[82] + z[101];
z[4] = abb[7] * z[4];
z[101] = -abb[44] + z[99];
z[104] = 4 * abb[1];
z[101] = z[101] * z[104];
z[4] = z[4] + z[5] + z[9] + -z[10] + -z[17] + z[64] + z[101] + -2 * z[129];
z[4] = abb[64] * z[4];
z[5] = -abb[31] + z[86];
z[5] = abb[35] + 2 * z[5];
z[5] = abb[35] * z[5];
z[9] = abb[34] * z[76];
z[5] = z[5] + -z[9] + -z[69];
z[38] = z[38] + -z[55];
z[64] = -abb[34] + z[44];
z[64] = z[1] * z[64];
z[64] = -z[5] + -z[38] + -z[57] + -z[61] + z[64] + -z[81] + z[98];
z[64] = abb[6] * z[64];
z[76] = abb[31] + z[86];
z[76] = -abb[35] + 2 * z[76];
z[76] = abb[35] * z[76];
z[55] = 4 * abb[75] + -z[55] + z[76] + z[95];
z[76] = z[20] + z[51];
z[76] = abb[31] * z[76];
z[11] = -z[11] + z[79];
z[11] = abb[36] * z[11];
z[11] = z[11] + -z[13] + -z[19] + z[55] + z[61] + z[71] + -z[76];
z[11] = abb[7] * z[11];
z[13] = -z[6] + -z[13] + z[15];
z[15] = -abb[37] + z[28] + z[51];
z[15] = abb[31] * z[15];
z[5] = -4 * abb[70] + z[5] + -z[13] + z[15] + -z[57] + z[113] + -z[120];
z[5] = abb[4] * z[5];
z[15] = z[88] * z[124];
z[19] = -z[35] + -z[98] + 2 * z[118];
z[51] = -abb[8] * z[19];
z[57] = abb[32] * abb[36];
z[61] = -abb[34] * z[67];
z[57] = abb[40] + z[57] + z[61];
z[33] = -z[33] + 2 * z[57];
z[33] = abb[3] * z[33];
z[57] = abb[73] * z[85];
z[5] = z[5] + z[11] + z[15] + z[33] + z[51] + z[57] + z[64] + -z[73] + z[109];
z[5] = abb[66] * z[5];
z[11] = z[83] + z[110];
z[15] = -abb[70] + z[11];
z[33] = 2 * z[97];
z[51] = abb[31] + -z[33];
z[51] = abb[31] * z[51];
z[15] = 2 * z[15] + z[51] + -z[75] + -z[119];
z[15] = abb[56] * z[15];
z[11] = -abb[70] + -z[11] + -z[71];
z[33] = abb[31] + z[33];
z[33] = abb[31] * z[33];
z[11] = 2 * z[11] + z[33] + -z[119] + -z[128];
z[11] = abb[62] * z[11];
z[33] = (T(8) / T(3)) * z[75] + 2 * z[125] + -z[127];
z[33] = z[33] * z[41];
z[41] = z[65] * z[93];
z[33] = z[33] + z[41];
z[51] = -abb[56] + abb[62];
z[57] = z[51] * z[69];
z[61] = z[65] * z[105];
z[57] = z[57] + z[61];
z[64] = z[102] + z[110];
z[64] = 2 * z[64] + z[72];
z[67] = -abb[59] * z[64];
z[11] = z[11] + z[15] + z[33] + z[57] + z[67];
z[11] = abb[23] * z[11];
z[15] = -z[118] + -z[125];
z[15] = 2 * z[15] + -z[75] + z[127];
z[15] = abb[56] * z[15];
z[67] = -abb[62] * z[126];
z[71] = abb[59] * z[19];
z[35] = z[35] * z[51];
z[42] = abb[53] * z[42];
z[15] = z[15] + z[33] + -z[35] + z[42] + z[61] + z[67] + z[71];
z[15] = abb[25] * z[15];
z[33] = -abb[33] + -z[107];
z[33] = abb[31] * z[33];
z[31] = -abb[70] + z[31] + z[33] + -z[63] + z[110];
z[31] = abb[56] * z[31];
z[33] = -z[43] * z[89];
z[42] = abb[31] * z[107];
z[43] = z[42] + z[58] + z[111];
z[40] = z[40] + z[96];
z[58] = z[40] + z[43];
z[58] = abb[61] * z[58];
z[63] = z[96] + z[117];
z[43] = z[43] + z[63];
z[43] = abb[58] * z[43];
z[60] = z[60] * z[116];
z[31] = 2 * z[31] + z[33] + z[43] + z[58] + -z[60];
z[31] = abb[22] * z[31];
z[1] = abb[37] * z[1];
z[1] = z[1] + -z[6] + -z[54] + -z[55] + z[69] + -z[77];
z[1] = abb[66] * z[1];
z[6] = abb[67] * z[21];
z[1] = z[1] + z[6];
z[1] = abb[5] * z[1];
z[6] = abb[36] * z[20];
z[6] = -z[6] + z[76] + z[82];
z[20] = -abb[7] * z[6];
z[21] = abb[31] + -z[0];
z[21] = abb[31] * z[21];
z[13] = -z[13] + z[21] + z[46];
z[13] = abb[4] * z[13];
z[13] = z[13] + z[20] + -z[32] + z[101];
z[13] = abb[63] * z[13];
z[20] = -abb[63] + -abb[67];
z[10] = z[10] * z[20];
z[20] = abb[22] * z[106];
z[19] = abb[25] * z[19];
z[21] = abb[23] * z[64];
z[32] = abb[29] * abb[77];
z[19] = -z[19] + -z[20] + z[21] + z[32];
z[21] = abb[57] * z[19];
z[32] = -abb[56] + z[78];
z[22] = z[22] * z[32];
z[22] = z[22] + z[41] + -z[61];
z[22] = abb[28] * z[22];
z[32] = -z[84] * z[115];
z[20] = abb[59] * z[20];
z[33] = -abb[59] + z[91];
z[33] = abb[29] * z[33];
z[33] = z[33] + -z[74] + -z[94];
z[33] = abb[77] * z[33];
z[1] = z[1] + z[4] + z[5] + z[10] + z[11] + z[13] + z[15] + z[20] + z[21] + z[22] + z[23] + z[26] + z[31] + z[32] + z[33];
z[4] = abb[36] + z[86];
z[4] = z[4] * z[28];
z[4] = z[4] + -z[49];
z[5] = z[40] + z[56];
z[10] = -abb[32] + abb[34];
z[10] = -abb[31] + 2 * z[10];
z[10] = abb[31] * z[10];
z[11] = abb[36] * z[0];
z[10] = -z[4] + z[5] + z[10] + z[11] + z[12] + z[98];
z[10] = abb[62] * z[10];
z[13] = z[28] * z[87];
z[15] = -abb[31] + 2 * z[87];
z[15] = abb[31] * z[15];
z[5] = z[5] + -z[12] + -z[13] + z[15] + z[99];
z[5] = abb[56] * z[5];
z[12] = -z[42] + -z[75] + -z[113] + z[121];
z[13] = -z[12] + z[63];
z[13] = abb[58] * z[13];
z[12] = z[12] + -z[40];
z[12] = abb[61] * z[12];
z[15] = z[29] + -z[79];
z[15] = abb[31] * z[15];
z[9] = -z[9] + z[15] + z[27] + z[114];
z[15] = z[9] + -z[123];
z[20] = abb[57] + abb[60];
z[15] = z[15] * z[20];
z[9] = abb[59] * z[9];
z[20] = abb[59] + z[51];
z[20] = z[20] * z[123];
z[21] = abb[53] * z[80];
z[5] = -z[5] + -z[9] + -z[10] + -z[12] + z[13] + z[15] + z[20] + z[21] + -z[35] + z[57] + -z[60];
z[5] = z[5] * z[24];
z[4] = -z[4] + z[18] + -z[38] + z[56] + z[99] + -z[100];
z[4] = abb[6] * z[4];
z[9] = -abb[31] + abb[37];
z[9] = z[9] * z[90];
z[0] = abb[32] + -z[0];
z[0] = -abb[34] + 2 * z[0];
z[0] = abb[34] * z[0];
z[0] = z[0] + z[9] + z[49] + -z[92];
z[0] = abb[4] * z[0];
z[9] = abb[36] + -z[16] + -z[39] + z[79];
z[9] = abb[31] * z[9];
z[10] = abb[36] * z[30];
z[3] = -z[3] + z[9] + z[10];
z[3] = abb[7] * z[3];
z[9] = -abb[77] * z[25];
z[0] = z[0] + z[3] + z[4] + z[9] + -z[17] + z[108];
z[0] = z[0] * z[7];
z[3] = -abb[31] + z[59];
z[3] = abb[31] * z[3];
z[3] = z[3] + -z[11] + z[34] + z[62] + -z[103];
z[3] = abb[19] * z[3];
z[4] = z[36] * z[70];
z[6] = abb[18] * z[6];
z[7] = abb[77] * z[52];
z[3] = z[3] + z[4] + z[6] + z[7];
z[3] = z[3] * z[45];
z[4] = z[14] * z[19];
z[6] = z[37] * z[112];
z[7] = -abb[14] + z[8] + z[48];
z[8] = abb[65] + abb[67];
z[7] = z[7] * z[8];
z[8] = z[47] * z[65];
z[9] = -abb[5] + abb[7] + z[66];
z[9] = abb[66] * z[9];
z[7] = z[7] + z[8] + z[9];
z[7] = abb[76] * z[7];
z[8] = abb[31] * z[44];
z[8] = -abb[44] + z[8] + -z[53] + -z[68];
z[8] = -z[8] * z[50];
z[0] = abb[54] + abb[55] + z[0] + 2 * z[1] + z[3] + z[4] + z[5] + z[6] + 4 * z[7] + z[8];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_774_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-48.389223528459310168130450869953407056308691317944699117038443347"),stof<T>("11.796735750326443730433504252492078714584450220287678159942546189")}, std::complex<T>{stof<T>("-15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("32.956844600689461635090114165883724737052826743171725653297932899"),stof<T>("-10.757002055713661982612541008703347206520567675858769933891847136")}, std::complex<T>{stof<T>("15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("-1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("-15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("32.956844600689461635090114165883724737052826743171725653297932899"),stof<T>("-10.757002055713661982612541008703347206520567675858769933891847136")}, std::complex<T>{stof<T>("-17.524465672919613102049777461814042417796962168398752189557422451"),stof<T>("9.717268361100880234791577764914615698456685131429861707841148082")}, std::complex<T>{stof<T>("-5.957391057342135285693590092575649429567304212121563924157947368"),stof<T>("-39.07861243167864131057641461106806128740498002050804085489030253")}, std::complex<T>{stof<T>("-38.738575473652917917339464426712873678747484691887493345321779341"),stof<T>("-81.450950355975582291843418728492263301927251976715148298961003537")}, std::complex<T>{stof<T>("-23.451557420512199603966606109048186542548396890595871055292053351"),stof<T>("-8.110417567593150725487277824633505338385982740703367510457517451")}, std::complex<T>{stof<T>("-54.435237139879038223951995513351918926052935243979122291517095303"),stof<T>("-84.603931422830465507602190558258616843356011949733847867758898786")}, std::complex<T>{stof<T>("-23.451557420512199603966606109048186542548396890595871055292053351"),stof<T>("-8.110417567593150725487277824633505338385982740703367510457517451")}, std::complex<T>{stof<T>("-3.9318187404805197820521584578624244708205094635939562581493146446"),stof<T>("-1.0306356838971807472985840279556750782218872016936305185446502025")}, std::complex<T>{stof<T>("0.051881463697184215754529699493716126989310907775594881303416497"),stof<T>("-25.098197810448281517736238067280671084815067568294361227844109094")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[30].real()/kbase.W[30].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_774_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_774_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (4 * m1_set::bc<T>[0] * (-v[3] + v[5])) / tend;


		return (abb[64] + abb[65] + abb[67]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[64] + abb[65] + abb[67];
z[1] = abb[32] + -abb[34];
return 8 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_774_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(2)) * (-v[3] + v[5]) * (8 + -3 * v[3] + 7 * v[5])) / prod_pow(tend, 2);
c[1] = (4 * (-v[3] + v[5])) / tend;


		return (abb[64] + abb[65] + abb[67]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[35] + -abb[36];
z[1] = -abb[32] + abb[34];
z[0] = z[0] * z[1];
z[0] = abb[42] + z[0];
z[1] = -abb[64] + -abb[65] + -abb[67];
return 8 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_774_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (-4 * m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[65] + abb[67] + abb[68]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[65] + abb[67] + abb[68];
z[1] = abb[34] + -abb[36];
return 8 * abb[12] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_774_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(2)) * (v[2] + v[3]) * (24 + -7 * v[2] + v[3] + 4 * v[5])) / prod_pow(tend, 2);
c[1] = (12 * (v[2] + v[3])) / tend;


		return (abb[65] + abb[67] + abb[68]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[32] + 2 * abb[34] + -abb[35];
z[1] = abb[34] + -abb[36];
z[0] = z[0] * z[1];
z[0] = 3 * abb[40] + z[0];
z[1] = -abb[65] + -abb[67] + -abb[68];
return 8 * abb[12] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_774_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("33.038769654238719899899751872880935656930988392883391916982684723"),stof<T>("-9.319281607123689980053158591708910530079780758013529230104628835")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,81> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W19(k,dl), dlog_W24(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_13(k), f_2_15(k), f_2_16(k), f_2_17(k), f_2_18(k), f_2_2_im(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_9_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), f_2_27_im(k), T{0}, T{0}, rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[30].real()/k.W[30].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), f_2_2_re(k), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_9_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), f_2_27_re(k), T{0}, T{0}};
abb[54] = SpDLog_f_4_774_W_16_Im(t, path, abb);
abb[55] = SpDLog_f_4_774_W_19_Im(t, path, abb);
abb[79] = SpDLog_f_4_774_W_16_Re(t, path, abb);
abb[80] = SpDLog_f_4_774_W_19_Re(t, path, abb);

                    
            return f_4_774_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_774_DLogXconstant_part(base_point<T>, kend);
	value += f_4_774_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_774_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_774_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_774_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_774_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_774_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_774_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
