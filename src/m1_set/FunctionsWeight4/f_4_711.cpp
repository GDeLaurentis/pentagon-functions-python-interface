/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_711.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_711_abbreviated (const std::array<T,68>& abb) {
T z[89];
z[0] = 2 * abb[36];
z[1] = -abb[34] + z[0];
z[2] = 2 * abb[29];
z[3] = abb[33] + z[1] + -z[2];
z[4] = 2 * abb[0];
z[5] = -abb[13] + z[4];
z[6] = z[3] * z[5];
z[7] = abb[33] + abb[34];
z[8] = 2 * abb[35];
z[9] = z[7] + -z[8];
z[10] = abb[15] * z[9];
z[11] = -4 * abb[31] + -z[2] + z[7];
z[11] = abb[3] * z[11];
z[12] = abb[29] + -abb[36];
z[13] = abb[32] + z[12];
z[14] = 2 * abb[16];
z[15] = z[13] * z[14];
z[16] = 2 * abb[3];
z[17] = -abb[16] + z[16];
z[17] = z[8] * z[17];
z[18] = 2 * abb[32];
z[19] = z[1] + -z[18];
z[20] = -abb[33] + z[8];
z[21] = z[19] + z[20];
z[21] = abb[6] * z[21];
z[22] = -abb[34] + abb[36];
z[23] = 2 * z[22];
z[24] = abb[4] * z[23];
z[6] = z[6] + z[10] + z[11] + z[15] + z[17] + z[21] + z[24];
z[6] = m1_set::bc<T>[0] * z[6];
z[5] = -abb[47] * z[5];
z[11] = abb[6] + -abb[15];
z[11] = abb[50] * z[11];
z[15] = abb[49] + abb[50];
z[17] = -abb[47] + z[15];
z[17] = abb[3] * z[17];
z[21] = -abb[47] + abb[49];
z[24] = -abb[16] * z[21];
z[25] = -abb[49] + abb[50];
z[25] = abb[4] * z[25];
z[5] = z[5] + z[11] + z[17] + z[24] + z[25];
z[11] = -abb[29] + abb[35];
z[11] = m1_set::bc<T>[0] * z[11];
z[11] = z[11] + z[21];
z[17] = 2 * abb[5];
z[11] = z[11] * z[17];
z[21] = abb[32] + abb[33];
z[24] = -abb[35] + z[21];
z[25] = -abb[31] + z[24];
z[25] = m1_set::bc<T>[0] * z[25];
z[25] = -abb[49] + z[25];
z[25] = abb[12] * z[25];
z[26] = 4 * z[25];
z[5] = 2 * z[5] + z[6] + z[11] + -z[26];
z[5] = abb[59] * z[5];
z[6] = abb[30] + -abb[31];
z[11] = 2 * z[6];
z[9] = z[9] + -z[11];
z[27] = -abb[3] + abb[6];
z[9] = -z[9] * z[27];
z[21] = -z[8] + z[21];
z[28] = abb[1] * z[21];
z[29] = z[3] * z[4];
z[30] = 2 * abb[30];
z[31] = -abb[34] + z[30];
z[32] = -abb[33] + z[31];
z[33] = -abb[13] * z[32];
z[34] = abb[31] + -abb[35];
z[34] = abb[4] * z[34];
z[9] = z[9] + z[10] + -4 * z[28] + z[29] + z[33] + 2 * z[34];
z[9] = m1_set::bc<T>[0] * z[9];
z[10] = abb[48] + abb[50];
z[27] = z[10] * z[27];
z[29] = -abb[4] + -abb[15];
z[29] = abb[50] * z[29];
z[33] = abb[1] * z[15];
z[34] = -abb[47] * z[4];
z[35] = -abb[13] * abb[48];
z[27] = z[27] + z[29] + 2 * z[33] + z[34] + z[35];
z[29] = -m1_set::bc<T>[0] * z[6];
z[29] = -abb[48] + z[29];
z[17] = z[17] * z[29];
z[29] = abb[30] + -abb[33] + z[12];
z[29] = m1_set::bc<T>[0] * z[29];
z[29] = abb[47] + abb[48] + z[29];
z[34] = abb[10] * z[29];
z[35] = -abb[17] + abb[18] + abb[20];
z[36] = abb[19] + z[35];
z[37] = -abb[51] * z[36];
z[9] = z[9] + z[17] + z[26] + 2 * z[27] + 4 * z[34] + z[37];
z[9] = abb[62] * z[9];
z[17] = -abb[52] + abb[56];
z[27] = abb[54] + -abb[55];
z[34] = -2 * abb[53] + z[17] + -z[27];
z[37] = abb[34] * z[34];
z[38] = abb[53] + z[27];
z[39] = z[0] * z[38];
z[37] = z[37] + z[39];
z[40] = -abb[53] + z[17];
z[41] = abb[31] * z[40];
z[42] = -z[37] + 2 * z[41];
z[43] = z[8] * z[38];
z[44] = z[17] + z[27];
z[45] = abb[33] * z[44];
z[43] = z[42] + z[43] + -z[45];
z[46] = abb[23] * z[43];
z[47] = z[30] * z[40];
z[47] = -z[37] + z[47];
z[48] = 2 * z[38];
z[49] = abb[29] * z[48];
z[49] = -z[45] + z[47] + z[49];
z[49] = abb[21] * z[49];
z[50] = abb[27] * z[44];
z[51] = z[20] * z[50];
z[52] = abb[34] * z[50];
z[46] = z[46] + z[49] + z[51] + -z[52];
z[46] = m1_set::bc<T>[0] * z[46];
z[21] = m1_set::bc<T>[0] * z[21];
z[15] = -z[15] + z[21];
z[15] = abb[59] * z[15];
z[7] = abb[30] + abb[35] + -z[7];
z[7] = m1_set::bc<T>[0] * z[7];
z[7] = z[7] + z[10];
z[7] = abb[62] * z[7];
z[7] = z[7] + z[15];
z[7] = abb[7] * z[7];
z[10] = abb[50] * z[50];
z[15] = -abb[47] * z[38];
z[21] = abb[48] * z[40];
z[49] = -z[15] + z[21];
z[49] = abb[21] * z[49];
z[51] = abb[49] * z[38];
z[53] = abb[23] * z[51];
z[7] = z[7] + z[10] + z[49] + z[53];
z[10] = abb[53] + z[17] + 2 * z[27];
z[10] = abb[35] * z[10];
z[49] = abb[34] * z[40];
z[53] = abb[32] * z[38];
z[49] = z[49] + z[53];
z[54] = abb[30] * z[40];
z[55] = z[49] + -z[54];
z[10] = z[10] + -z[45] + -z[55];
z[10] = m1_set::bc<T>[0] * z[10];
z[45] = abb[50] * z[44];
z[10] = z[10] + z[21] + z[45] + z[51];
z[21] = abb[22] + abb[24];
z[45] = 2 * z[21];
z[10] = z[10] * z[45];
z[43] = m1_set::bc<T>[0] * z[43];
z[45] = abb[49] * z[48];
z[43] = z[43] + z[45];
z[43] = abb[25] * z[43];
z[45] = -abb[3] * abb[48];
z[56] = -abb[0] * abb[47];
z[45] = z[45] + z[56];
z[3] = abb[0] * z[3];
z[32] = -abb[3] * z[32];
z[3] = z[3] + z[32];
z[3] = m1_set::bc<T>[0] * z[3];
z[32] = 2 * abb[10];
z[29] = z[29] * z[32];
z[56] = -abb[18] * abb[51];
z[3] = z[3] + z[29] + 2 * z[45] + z[56];
z[29] = 2 * abb[57];
z[3] = z[3] * z[29];
z[45] = abb[3] * abb[31];
z[56] = abb[3] * abb[35];
z[28] = -z[28] + z[45] + -z[56];
z[28] = m1_set::bc<T>[0] * z[28];
z[45] = abb[3] * abb[50];
z[28] = z[28] + z[33] + -z[45];
z[25] = z[25] + z[28];
z[33] = abb[60] + abb[61];
z[45] = -4 * z[33];
z[25] = z[25] * z[45];
z[26] = -z[26] + -4 * z[28];
z[26] = abb[58] * z[26];
z[28] = abb[36] * z[38];
z[45] = z[28] + -z[53];
z[57] = abb[29] * z[38];
z[57] = -z[45] + z[57];
z[58] = abb[35] * z[38];
z[59] = z[57] + -z[58];
z[59] = m1_set::bc<T>[0] * z[59];
z[15] = -z[15] + -z[51] + z[59];
z[51] = 2 * abb[26];
z[15] = z[15] * z[51];
z[59] = abb[28] * z[44];
z[35] = -abb[19] + z[35];
z[35] = abb[59] * z[35];
z[35] = z[35] + -z[59];
z[59] = -abb[51] * z[35];
z[3] = z[3] + z[5] + 2 * z[7] + z[9] + z[10] + z[15] + z[25] + z[26] + z[43] + z[46] + z[59];
z[3] = 2 * z[3];
z[5] = 2 * abb[31];
z[7] = -abb[34] + z[5];
z[7] = abb[34] * z[7];
z[9] = 2 * abb[43];
z[7] = z[7] + -z[9];
z[10] = 2 * abb[42];
z[15] = z[7] + -z[10];
z[25] = 2 * abb[65];
z[26] = 2 * abb[66];
z[43] = z[25] + z[26];
z[46] = abb[29] + -abb[34];
z[59] = z[5] + z[46];
z[60] = -abb[29] * z[59];
z[61] = abb[33] * z[46];
z[62] = 2 * abb[34];
z[63] = -abb[36] + z[62];
z[63] = abb[36] * z[63];
z[64] = 2 * abb[63];
z[65] = 2 * abb[45];
z[66] = -abb[31] + z[18];
z[66] = abb[31] * z[66];
z[60] = z[15] + -z[43] + z[60] + z[61] + z[63] + z[64] + -z[65] + z[66];
z[60] = abb[3] * z[60];
z[66] = -z[25] + z[64];
z[67] = 2 * abb[37];
z[68] = z[65] + -z[67];
z[69] = -abb[32] + z[22];
z[69] = z[0] * z[69];
z[70] = 2 * abb[33];
z[13] = -z[13] * z[70];
z[71] = prod_pow(abb[32], 2);
z[71] = 2 * abb[44] + z[71];
z[72] = 2 * abb[41] + z[71];
z[73] = abb[31] + z[18];
z[73] = abb[31] * z[73];
z[74] = abb[31] + abb[32];
z[75] = -abb[36] + z[74];
z[75] = abb[29] + 2 * z[75];
z[75] = abb[29] * z[75];
z[7] = -z[7] + z[13] + -z[66] + z[68] + z[69] + z[72] + z[73] + z[75];
z[7] = abb[16] * z[7];
z[13] = -abb[36] + z[18];
z[69] = abb[36] * z[13];
z[69] = z[69] + -z[72];
z[22] = -z[22] * z[70];
z[75] = -abb[32] + abb[36];
z[75] = -abb[29] + 2 * z[75];
z[75] = abb[29] * z[75];
z[76] = abb[32] + z[46];
z[76] = -abb[35] + 2 * z[76];
z[76] = abb[35] * z[76];
z[22] = z[10] + z[22] + z[25] + -z[26] + z[69] + z[75] + z[76];
z[22] = abb[4] * z[22];
z[25] = -abb[35] + z[46] + z[74];
z[25] = z[8] * z[25];
z[75] = -abb[34] + z[74];
z[76] = z[2] * z[75];
z[77] = 2 * abb[39];
z[78] = -z[65] + z[77];
z[69] = z[67] + z[69];
z[25] = z[25] + z[69] + -z[76] + z[78];
z[76] = 2 * abb[9];
z[25] = z[25] * z[76];
z[76] = z[8] * z[46];
z[76] = -z[26] + z[76];
z[79] = abb[29] * z[46];
z[79] = z[61] + z[67] + -z[76] + z[79];
z[79] = abb[15] * z[79];
z[2] = 3 * abb[34] + -z[0] + z[2] + -z[30];
z[2] = abb[29] * z[2];
z[30] = prod_pow(abb[30], 2);
z[9] = z[9] + z[30];
z[80] = abb[34] * z[31];
z[63] = -z[9] + z[63] + z[80];
z[80] = z[63] + z[64];
z[81] = 3 * abb[29] + -z[0] + -z[31];
z[81] = abb[33] * z[81];
z[82] = 2 * abb[38];
z[2] = -z[2] + z[80] + z[81] + -z[82];
z[4] = z[2] * z[4];
z[4] = z[4] + z[25] + z[79];
z[25] = -abb[32] + abb[34];
z[25] = z[0] * z[25];
z[75] = -abb[29] + 2 * z[75];
z[75] = abb[29] * z[75];
z[75] = -4 * abb[37] + z[75];
z[15] = -4 * abb[39] + z[15] + z[25] + z[65] + z[72] + -z[73] + z[75];
z[15] = abb[2] * z[15];
z[25] = -abb[35] + z[70];
z[25] = abb[35] * z[25];
z[65] = z[25] + z[26];
z[72] = abb[29] * z[19];
z[19] = abb[29] + -z[19];
z[19] = abb[33] * z[19];
z[19] = z[10] + z[19] + -z[65] + z[69] + z[72];
z[19] = abb[6] * z[19];
z[63] = -z[10] + z[63];
z[72] = abb[29] + -abb[30];
z[72] = z[70] * z[72];
z[79] = -abb[30] + abb[34];
z[81] = abb[29] + 2 * z[79];
z[81] = abb[29] * z[81];
z[72] = -z[63] + -z[72] + z[81] + z[82];
z[72] = abb[8] * z[72];
z[81] = -abb[29] + z[18];
z[81] = abb[29] * z[81];
z[83] = abb[35] + -z[18];
z[83] = abb[35] * z[83];
z[66] = z[66] + -z[69] + z[81] + z[83];
z[66] = abb[5] * z[66];
z[69] = -abb[31] + abb[33];
z[69] = abb[35] * z[69];
z[81] = abb[32] * abb[33];
z[81] = -abb[65] + z[81];
z[83] = abb[31] * abb[32];
z[69] = z[69] + -z[81] + z[83];
z[84] = prod_pow(m1_set::bc<T>[0], 2);
z[85] = (T(5) / T(3)) * z[84];
z[69] = 2 * z[69] + z[85];
z[69] = abb[12] * z[69];
z[86] = 2 * z[69];
z[87] = -abb[29] + abb[33];
z[1] = -abb[29] + z[1];
z[1] = z[1] * z[87];
z[1] = z[1] + -z[10] + -z[64];
z[1] = abb[13] * z[1];
z[10] = 4 * abb[0] + 2 * abb[15];
z[64] = 2 * abb[13];
z[16] = abb[16] + z[10] + z[16] + -z[64];
z[16] = -abb[6] + (T(1) / T(3)) * z[16];
z[16] = z[16] * z[84];
z[59] = -abb[32] + abb[33] + -z[59];
z[59] = abb[16] * z[59];
z[88] = -abb[32] + z[46];
z[88] = abb[3] * z[88];
z[59] = z[59] + z[88];
z[14] = abb[3] + z[14];
z[14] = abb[35] * z[14];
z[14] = z[14] + 2 * z[59];
z[14] = abb[35] * z[14];
z[1] = z[1] + z[4] + z[7] + z[14] + z[15] + z[16] + z[19] + z[22] + z[60] + z[66] + 2 * z[72] + -z[86];
z[1] = abb[59] * z[1];
z[7] = abb[30] + abb[32];
z[7] = -abb[34] + 2 * z[7];
z[7] = abb[34] * z[7];
z[7] = z[7] + -z[9];
z[9] = 2 * abb[64];
z[14] = -z[9] + z[78];
z[0] = -z[0] * z[13];
z[13] = (T(1) / T(3)) * z[84];
z[15] = -abb[31] * z[18];
z[0] = 4 * abb[41] + z[0] + z[7] + -z[13] + -z[14] + z[15] + z[71] + z[75];
z[0] = abb[5] * z[0];
z[15] = abb[39] + -abb[66];
z[16] = -abb[34] + z[11];
z[16] = abb[29] * z[16];
z[11] = -abb[29] + z[11];
z[19] = abb[34] + -z[11];
z[19] = abb[33] * z[19];
z[15] = -z[9] + 2 * z[15] + z[16] + z[19] + -z[25] + -z[68];
z[15] = abb[6] * z[15];
z[16] = 2 * abb[40];
z[19] = z[16] + z[73];
z[7] = -z[7] + z[19] + z[71] + z[82];
z[22] = z[9] + z[61];
z[18] = -z[18] + -z[46];
z[18] = abb[29] * z[18];
z[25] = z[46] + 2 * z[74];
z[59] = -z[8] + z[25];
z[59] = z[8] * z[59];
z[18] = -z[7] + z[18] + z[22] + z[26] + z[59];
z[18] = abb[3] * z[18];
z[59] = z[67] + z[78];
z[6] = -abb[29] + z[6];
z[6] = z[6] * z[70];
z[25] = abb[33] + -z[25];
z[25] = 3 * abb[35] + 2 * z[25];
z[25] = abb[35] * z[25];
z[60] = -abb[30] + z[74];
z[60] = abb[29] + 2 * z[60];
z[60] = abb[29] * z[60];
z[6] = z[6] + z[7] + z[25] + -z[59] + z[60];
z[6] = abb[14] * z[6];
z[7] = z[70] * z[74];
z[25] = -abb[31] + z[70];
z[25] = -abb[35] + 2 * z[25];
z[25] = abb[35] * z[25];
z[7] = -z[7] + z[19] + z[25] + z[43];
z[7] = abb[1] * z[7];
z[25] = abb[23] * abb[46];
z[25] = z[7] + z[25];
z[43] = prod_pow(abb[31], 2);
z[60] = z[16] + z[43];
z[5] = -abb[29] + z[5];
z[5] = abb[29] * z[5];
z[5] = z[5] + -z[59] + -z[60] + -z[82];
z[5] = abb[2] * z[5];
z[12] = z[12] * z[70];
z[59] = z[9] + (T(4) / T(3)) * z[84];
z[23] = -abb[29] + z[23];
z[23] = abb[29] * z[23];
z[12] = z[12] + z[23] + z[59] + z[80];
z[23] = -z[12] * z[32];
z[31] = -abb[29] + z[31];
z[31] = z[31] * z[87];
z[9] = z[9] + z[82];
z[31] = z[9] + z[31];
z[31] = abb[13] * z[31];
z[32] = abb[31] * z[70];
z[61] = -z[32] + z[60] + z[65];
z[61] = abb[4] * z[61];
z[10] = -10 * abb[1] + abb[3] + -2 * abb[6] + abb[14] + z[10] + z[64];
z[10] = z[10] * z[13];
z[36] = abb[67] * z[36];
z[0] = z[0] + z[4] + z[5] + z[6] + z[10] + z[15] + z[18] + z[23] + -2 * z[25] + z[31] + z[36] + z[61] + z[86];
z[0] = abb[62] * z[0];
z[4] = 2 * z[53];
z[5] = z[4] + -z[28];
z[5] = abb[36] * z[5];
z[6] = z[38] * z[71];
z[5] = z[5] + -z[6];
z[6] = -z[30] + z[77] + -z[82];
z[6] = z[6] * z[40];
z[10] = z[53] + z[54];
z[15] = abb[29] * z[40];
z[10] = 2 * z[10] + -z[15] + -z[37];
z[10] = abb[29] * z[10];
z[18] = abb[29] * z[34];
z[18] = z[18] + -z[47];
z[23] = abb[33] * z[18];
z[25] = z[40] * z[43];
z[30] = -abb[41] + abb[42];
z[31] = -abb[37] + -abb[65] + -z[30];
z[31] = z[31] * z[48];
z[36] = z[41] + -z[54];
z[37] = -z[36] * z[62];
z[41] = -z[4] + z[58];
z[41] = abb[35] * z[41];
z[6] = -z[5] + z[6] + z[10] + z[23] + z[25] + z[31] + z[37] + z[41];
z[6] = abb[23] * z[6];
z[10] = abb[34] * z[44];
z[4] = z[4] + z[10] + -z[15] + -z[39];
z[4] = abb[29] * z[4];
z[10] = (T(2) / T(3)) * z[84];
z[23] = -z[10] + -z[67];
z[23] = z[23] * z[44];
z[31] = -abb[29] * z[44];
z[31] = z[31] + -z[42];
z[31] = abb[33] * z[31];
z[37] = -z[15] + z[49];
z[39] = abb[33] * z[40];
z[39] = -z[37] + z[39];
z[34] = -abb[35] * z[34];
z[34] = z[34] + 2 * z[39];
z[34] = abb[35] * z[34];
z[30] = -z[30] * z[48];
z[39] = abb[65] * z[48];
z[25] = -z[25] + z[39];
z[39] = z[16] * z[40];
z[4] = z[4] + -z[5] + z[23] + -z[25] + z[30] + z[31] + z[34] + z[39];
z[4] = abb[25] * z[4];
z[5] = abb[33] * z[38];
z[5] = z[5] + z[37] + -z[58];
z[5] = z[5] * z[8];
z[8] = abb[53] + 4 * z[17] + 5 * z[27];
z[8] = z[8] * z[13];
z[13] = z[15] + 2 * z[36];
z[13] = abb[29] * z[13];
z[14] = z[14] + z[16];
z[14] = z[14] * z[40];
z[15] = z[26] * z[44];
z[16] = z[55] * z[70];
z[5] = z[5] + z[8] + z[13] + -z[14] + z[15] + -z[16] + z[25];
z[5] = -z[5] * z[21];
z[8] = abb[33] + z[46];
z[8] = z[8] * z[38];
z[8] = z[8] + z[53] + -z[58];
z[8] = abb[35] * z[8];
z[13] = -abb[37] + abb[41] + -abb[63] + abb[65];
z[13] = z[13] * z[38];
z[14] = abb[34] * z[38];
z[15] = -z[14] + z[45];
z[15] = abb[36] * z[15];
z[14] = z[14] + -z[28];
z[14] = abb[29] * z[14];
z[16] = -abb[33] * z[57];
z[17] = abb[34] * z[53];
z[21] = -abb[46] * abb[59];
z[8] = z[8] + z[13] + z[14] + z[15] + z[16] + z[17] + z[21];
z[8] = z[8] * z[51];
z[2] = abb[0] * z[2];
z[12] = -abb[10] * z[12];
z[13] = -abb[29] * abb[34];
z[13] = z[13] + z[22] + z[63];
z[13] = abb[3] * z[13];
z[14] = abb[0] + abb[3];
z[14] = z[10] * z[14];
z[15] = abb[18] * abb[67];
z[2] = z[2] + z[12] + z[13] + z[14] + z[15] + z[72];
z[2] = z[2] * z[29];
z[12] = z[70] * z[79];
z[11] = abb[29] * z[11];
z[11] = z[11] + z[12] + -z[59] + z[60] + z[76] + z[78];
z[11] = abb[62] * z[11];
z[12] = abb[35] * z[24];
z[12] = abb[66] + z[12] + -z[81];
z[12] = 2 * z[12] + z[85];
z[12] = abb[59] * z[12];
z[11] = z[11] + z[12];
z[11] = abb[7] * z[11];
z[12] = -abb[33] + z[74];
z[12] = -abb[35] + 2 * z[12];
z[12] = abb[35] * z[12];
z[12] = z[12] + -z[19] + z[32];
z[12] = abb[11] * z[12];
z[13] = abb[3] * z[74];
z[13] = z[13] + -z[56];
z[13] = abb[35] * z[13];
z[14] = -abb[66] + z[83];
z[14] = abb[3] * z[14];
z[13] = -z[13] + z[14];
z[14] = abb[1] * z[85];
z[7] = z[7] + z[12] + 2 * z[13] + z[14];
z[12] = z[7] + -z[69];
z[13] = 2 * z[33];
z[12] = z[12] * z[13];
z[7] = 2 * z[7] + -z[86];
z[7] = abb[58] * z[7];
z[13] = z[18] * z[87];
z[9] = -z[9] * z[40];
z[14] = -abb[42] + -abb[63];
z[14] = z[14] * z[48];
z[9] = z[9] + z[13] + z[14];
z[9] = abb[21] * z[9];
z[13] = -z[26] + -z[67];
z[13] = z[13] * z[50];
z[14] = abb[29] * z[50];
z[14] = z[14] + -z[52];
z[15] = -abb[29] + z[20];
z[14] = z[14] * z[15];
z[15] = -abb[21] + -abb[23];
z[15] = z[15] * z[44];
z[15] = z[15] + -z[50];
z[10] = z[10] * z[15];
z[15] = abb[67] * z[35];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15];
z[0] = 2 * z[0];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_711_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("10.4023028191312333362719401831913221365702768824772793193192573969"),stof<T>("4.0099607112369939801253103352120106710102959542432765442127783144")}, std::complex<T>{stof<T>("15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("-1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("-15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("-5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("-27.266064834752539870826065121313655656450724718606683845191270348"),stof<T>("-13.595328248826547361489176099425089692637134646712362756476500542")}, std::complex<T>{stof<T>("-25.514205731730803297405729886997480010359956010634664886931192621"),stof<T>("-36.916552190357586975765762273827768645282794678010635435462489747")}, std::complex<T>{stof<T>("-38.788330174393503524783156811706184825492083256084363457680538721"),stof<T>("-35.155922194044081085036539936540929621236005412970168900628053236")}, std::complex<T>{stof<T>("-25.514205731730803297405729886997480010359956010634664886931192621"),stof<T>("-36.916552190357586975765762273827768645282794678010635435462489747")}, std::complex<T>{stof<T>("-25.514205731730803297405729886997480010359956010634664886931192621"),stof<T>("-36.916552190357586975765762273827768645282794678010635435462489747")}, std::complex<T>{stof<T>("-18.115715729748419661476374537553446400941967581790848506914123715"),stof<T>("1.865231470881982908015474912760416863211443942791354825564564092")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_711_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_711_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-15.027648461811750172159449090228423730552511801388229864895411365"),stof<T>("18.707462405770759069511983873352611993254053234583799179192624201")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,68> abb = {dl[0], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W14(k,dl), dlog_W18(k,dl), dlog_W23(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_20(k), f_2_21(k), f_2_29_im(k), f_2_2_im(k), f_2_9_im(k), f_2_10_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_2_re(k), f_2_9_re(k), f_2_10_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_711_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_711_DLogXconstant_part(base_point<T>, kend);
	value += f_4_711_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_711_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_711_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_711_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_711_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_711_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_711_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
