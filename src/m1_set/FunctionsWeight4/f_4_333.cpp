/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_333.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_333_abbreviated (const std::array<T,58>& abb) {
T z[124];
z[0] = abb[19] + abb[21];
z[1] = abb[29] * z[0];
z[2] = abb[19] + abb[22];
z[3] = abb[21] + z[2];
z[4] = abb[20] + z[3];
z[5] = abb[26] * z[4];
z[6] = abb[22] * abb[27];
z[7] = abb[20] * abb[30];
z[1] = (T(1) / T(2)) * z[1] + -z[5] + z[6] + z[7];
z[1] = abb[29] * z[1];
z[5] = abb[27] + -abb[30];
z[8] = -abb[23] + z[2];
z[9] = -abb[20] + z[8];
z[9] = (T(1) / T(2)) * z[9];
z[5] = z[5] * z[9];
z[9] = -abb[21] + abb[23];
z[10] = abb[26] * (T(1) / T(2));
z[11] = z[9] * z[10];
z[12] = abb[20] + abb[21];
z[12] = abb[28] * z[12];
z[5] = z[5] + z[11] + z[12];
z[5] = abb[26] * z[5];
z[11] = abb[26] + -abb[27];
z[13] = -abb[23] + z[3];
z[14] = z[11] * z[13];
z[15] = abb[20] * (T(1) / T(2));
z[16] = abb[31] * z[15];
z[7] = -z[7] + z[14] + z[16];
z[7] = abb[31] * z[7];
z[14] = abb[23] * (T(1) / T(2));
z[16] = abb[22] * (T(1) / T(2));
z[17] = abb[21] + abb[19] * (T(1) / T(2)) + -z[14] + z[16];
z[18] = z[15] + z[17];
z[19] = abb[27] * z[18];
z[12] = z[12] + -z[19];
z[12] = abb[30] * z[12];
z[20] = prod_pow(abb[28], 2);
z[16] = z[16] * z[20];
z[21] = abb[20] + abb[23];
z[21] = abb[32] * z[21];
z[22] = abb[35] * z[3];
z[23] = abb[20] + z[13];
z[24] = abb[56] * z[23];
z[25] = abb[21] + abb[22];
z[26] = abb[20] + z[25];
z[27] = abb[55] * z[26];
z[6] = abb[28] * z[6];
z[28] = abb[24] * (T(1) / T(2));
z[29] = abb[57] * z[28];
z[4] = abb[36] * z[4];
z[1] = z[1] + -z[4] + z[5] + -z[6] + z[7] + -z[12] + z[16] + z[21] + z[22] + -z[24] + -z[27] + z[29];
z[4] = prod_pow(m1_set::bc<T>[0], 2);
z[5] = z[4] * z[18];
z[6] = abb[33] * (T(3) / T(2));
z[7] = z[2] * z[6];
z[1] = (T(3) / T(2)) * z[1] + -z[5] + z[7];
z[5] = abb[20] + z[0];
z[7] = abb[34] * z[5];
z[7] = z[1] + (T(3) / T(2)) * z[7];
z[12] = abb[51] + abb[53];
z[7] = z[7] * z[12];
z[1] = abb[52] * z[1];
z[16] = abb[44] * (T(1) / T(2));
z[21] = abb[42] * (T(1) / T(2));
z[22] = z[16] + z[21];
z[24] = abb[46] * (T(1) / T(2));
z[27] = abb[48] * (T(1) / T(2));
z[29] = abb[41] + abb[43] * (T(1) / T(2));
z[30] = z[22] + -z[24] + -z[27] + z[29];
z[30] = abb[11] * z[30];
z[31] = abb[40] * (T(1) / T(2));
z[32] = 2 * abb[44];
z[33] = -7 * abb[41] + abb[43] * (T(-7) / T(2)) + z[31] + -z[32];
z[34] = abb[48] * (T(3) / T(2));
z[35] = abb[47] * (T(1) / T(2));
z[24] = abb[42] * (T(-5) / T(6)) + z[24] + (T(1) / T(3)) * z[33] + z[34] + -z[35];
z[24] = abb[5] * z[24];
z[33] = 2 * abb[41] + abb[43];
z[36] = -z[21] + -z[33] + z[34];
z[37] = -z[16] + -z[35] + z[36];
z[38] = abb[14] * z[37];
z[39] = abb[48] * (T(3) / T(4));
z[40] = abb[40] * (T(1) / T(4));
z[41] = -8 * abb[41] + -4 * abb[43] + abb[44] * (T(-11) / T(2)) + z[40];
z[41] = abb[42] * (T(7) / T(12)) + z[35] + z[39] + (T(1) / T(3)) * z[41];
z[41] = abb[6] * z[41];
z[42] = 2 * z[33];
z[43] = abb[42] + abb[44];
z[44] = abb[47] + z[42] + z[43];
z[45] = abb[45] * (T(13) / T(2));
z[46] = abb[48] * (T(-5) / T(2)) + abb[46] * (T(13) / T(2)) + (T(-4) / T(3)) * z[44] + z[45];
z[46] = abb[2] * z[46];
z[47] = abb[54] * z[17];
z[48] = 7 * abb[47];
z[49] = -11 * abb[44] + abb[42] * (T(-7) / T(2)) + -4 * z[33] + z[48];
z[49] = -abb[48] + abb[46] * (T(7) / T(2)) + (T(1) / T(3)) * z[49];
z[49] = abb[8] * z[49];
z[50] = -abb[40] + abb[42];
z[48] = 14 * abb[41] + 7 * abb[43] + z[48] + (T(-25) / T(4)) * z[50];
z[45] = abb[48] * (T(-1) / T(4)) + -z[45] + (T(1) / T(3)) * z[48];
z[45] = abb[0] * z[45];
z[48] = -abb[44] + abb[47];
z[51] = -abb[40] + z[48];
z[52] = -abb[12] * z[51];
z[53] = (T(1) / T(2)) * z[52];
z[15] = abb[54] * z[15];
z[54] = abb[1] * z[51];
z[24] = z[15] + z[24] + 7 * z[30] + z[38] + z[41] + z[45] + z[46] + z[47] + z[49] + -z[53] + (T(-10) / T(3)) * z[54];
z[24] = z[4] * z[24];
z[30] = abb[2] * z[37];
z[41] = abb[42] * (T(1) / T(4));
z[45] = -z[39] + z[41];
z[46] = z[29] + z[45];
z[47] = abb[47] * (T(1) / T(4));
z[49] = abb[44] * (T(1) / T(4));
z[55] = z[46] + z[47] + z[49];
z[56] = abb[14] * z[55];
z[57] = (T(1) / T(4)) * z[52];
z[58] = z[56] + z[57];
z[58] = 3 * z[58];
z[59] = z[16] + z[40] + z[46];
z[60] = abb[6] * z[59];
z[61] = abb[8] * z[55];
z[62] = abb[44] + z[31] + -z[36];
z[63] = abb[5] * z[62];
z[64] = 3 * abb[45];
z[65] = abb[42] * (T(5) / T(4)) + -z[39] + z[64];
z[66] = z[29] + z[35];
z[67] = abb[40] * (T(5) / T(4));
z[68] = z[65] + -z[66] + -z[67];
z[69] = abb[0] * z[68];
z[70] = -abb[42] + abb[48];
z[71] = z[48] + z[70];
z[71] = -abb[45] + (T(1) / T(2)) * z[71];
z[72] = abb[3] * z[71];
z[73] = (T(3) / T(2)) * z[72];
z[74] = abb[54] * (T(3) / T(4));
z[9] = -z[9] * z[74];
z[9] = z[9] + -z[30] + -z[54] + -z[58] + z[60] + z[61] + z[63] + z[69] + -z[73];
z[9] = abb[26] * z[9];
z[60] = abb[44] * (T(3) / T(4));
z[47] = -z[36] + z[40] + z[47] + z[60];
z[47] = abb[6] * z[47];
z[61] = abb[47] * (T(5) / T(4));
z[60] = -abb[40] + z[46] + -z[60] + z[61];
z[60] = abb[5] * z[60];
z[56] = -z[56] + z[57];
z[8] = z[8] * z[74];
z[57] = 2 * z[54];
z[63] = abb[20] * z[74];
z[69] = z[57] + -z[63];
z[75] = 3 * abb[48];
z[76] = z[44] + -z[75];
z[77] = abb[2] * z[76];
z[8] = z[8] + z[47] + 3 * z[56] + z[60] + z[69] + z[77];
z[47] = -abb[27] * z[8];
z[56] = abb[13] * z[37];
z[60] = -z[35] + z[40] + -z[46];
z[78] = abb[0] * z[60];
z[8] = z[8] + -z[56] + z[78];
z[8] = abb[30] * z[8];
z[79] = abb[8] * z[37];
z[80] = abb[54] * (T(3) / T(2));
z[81] = abb[20] * z[80];
z[82] = z[79] + -z[81];
z[83] = 2 * abb[5];
z[84] = z[51] * z[83];
z[85] = abb[21] * z[80];
z[52] = (T(3) / T(2)) * z[52];
z[86] = (T(1) / T(2)) * z[51];
z[87] = abb[6] * z[86];
z[84] = z[52] + 4 * z[54] + z[82] + z[84] + -z[85] + -z[87];
z[85] = abb[28] * z[84];
z[88] = -abb[47] + z[31] + z[36];
z[89] = abb[28] * z[88];
z[60] = abb[27] * z[60];
z[90] = -z[60] + z[89];
z[90] = abb[0] * z[90];
z[91] = abb[27] + -abb[28];
z[92] = z[56] * z[91];
z[8] = z[8] + z[9] + z[47] + z[85] + z[90] + z[92];
z[8] = abb[26] * z[8];
z[9] = abb[45] * (T(3) / T(2));
z[47] = z[9] + -z[33];
z[85] = z[21] + z[35];
z[92] = abb[46] * (T(3) / T(2));
z[93] = z[16] + -z[47] + z[85] + -z[92];
z[93] = abb[2] * z[93];
z[94] = abb[47] * (T(7) / T(4));
z[45] = abb[44] * (T(11) / T(4)) + z[33] + -z[45] + -z[92] + -z[94];
z[45] = abb[8] * z[45];
z[92] = -z[29] + z[35] + -z[43] + z[92];
z[95] = abb[5] * z[92];
z[9] = -abb[47] + z[9] + z[22] + -z[29];
z[9] = abb[13] * z[9];
z[96] = -abb[45] + z[48];
z[96] = abb[10] * z[96];
z[48] = -abb[48] + z[48];
z[97] = abb[42] + z[48];
z[98] = abb[7] * z[97];
z[99] = -3 * z[96] + (T(3) / T(4)) * z[98];
z[100] = abb[44] + z[29] + -z[85];
z[101] = abb[6] * z[100];
z[0] = -z[0] * z[74];
z[48] = -abb[42] + z[48];
z[48] = abb[46] + (T(1) / T(2)) * z[48];
z[102] = abb[4] * z[48];
z[103] = (T(3) / T(2)) * z[102];
z[0] = z[0] + z[9] + z[45] + -z[73] + z[93] + z[95] + -z[99] + z[101] + z[103];
z[0] = abb[29] * z[0];
z[9] = abb[5] * z[37];
z[45] = (T(3) / T(2)) * z[98];
z[73] = abb[22] * z[80];
z[95] = z[45] + -z[73];
z[32] = -abb[47] + z[32] + z[33];
z[101] = -abb[42] + z[32];
z[104] = abb[8] * z[101];
z[101] = abb[6] * z[101];
z[101] = z[9] + z[56] + z[77] + z[95] + z[101] + 2 * z[104];
z[101] = abb[27] * z[101];
z[105] = -abb[48] + z[33];
z[106] = -abb[46] + z[43] + z[105];
z[106] = abb[11] * z[106];
z[107] = 3 * z[106];
z[108] = 3 * z[102];
z[109] = z[81] + -z[107] + z[108];
z[110] = abb[6] * z[37];
z[111] = -z[77] + z[110];
z[112] = 3 * abb[46];
z[113] = 2 * abb[42] + -z[112];
z[114] = z[32] + z[113];
z[115] = z[83] * z[114];
z[116] = abb[8] * z[114];
z[115] = -z[56] + -z[109] + z[111] + -z[115] + -2 * z[116];
z[115] = abb[30] * z[115];
z[117] = z[3] * z[80];
z[117] = -z[82] + z[111] + z[117];
z[118] = -z[33] + z[64];
z[119] = 2 * abb[47];
z[43] = z[43] + z[118] + -z[119];
z[120] = abb[13] * z[43];
z[121] = 2 * z[120];
z[122] = -z[9] + z[121];
z[123] = 3 * z[72] + z[117] + z[122];
z[123] = abb[26] * z[123];
z[0] = z[0] + z[101] + z[115] + z[123];
z[0] = abb[29] * z[0];
z[101] = z[13] * z[80];
z[9] = -z[9] + z[101];
z[76] = abb[6] * z[76];
z[101] = 2 * z[77];
z[123] = z[9] + 3 * z[38] + -z[56] + z[76] + -z[79] + z[101];
z[11] = -z[11] * z[123];
z[107] = -z[107] + z[116];
z[64] = z[64] + z[112];
z[116] = 2 * z[44] + -z[64] + -z[75];
z[116] = abb[2] * z[116];
z[114] = abb[5] * z[114];
z[76] = z[76] + z[107] + z[114] + z[116] + -z[120];
z[76] = abb[29] * z[76];
z[39] = -z[39] + -z[41] + -z[47] + -z[49] + z[61];
z[39] = abb[13] * z[39];
z[41] = abb[5] + abb[8];
z[47] = z[41] * z[92];
z[61] = abb[6] * z[55];
z[39] = z[39] + z[47] + z[61] + -z[63] + z[93] + -z[103];
z[39] = abb[31] * z[39];
z[11] = z[11] + z[39] + z[76] + -z[115];
z[11] = abb[31] * z[11];
z[39] = abb[47] * (T(3) / T(4));
z[36] = -z[36] + z[39] + -z[40] + z[49];
z[36] = abb[6] * z[36];
z[40] = abb[44] * (T(5) / T(4));
z[47] = abb[40] + -z[39] + z[40] + z[46];
z[47] = abb[5] * z[47];
z[17] = z[17] * z[80];
z[17] = z[17] + -z[58];
z[36] = -z[17] + -z[36] + -z[47] + z[69] + -z[77] + z[79];
z[47] = abb[27] * z[36];
z[56] = z[56] + -z[84];
z[56] = abb[28] * z[56];
z[58] = -abb[6] * z[62];
z[61] = -abb[0] * z[88];
z[62] = abb[40] + -abb[44] + -z[33] + -z[113];
z[62] = z[62] * z[83];
z[58] = -z[54] + z[58] + z[61] + z[62] + -z[77] + -z[107];
z[58] = abb[30] * z[58];
z[47] = z[47] + z[56] + z[58] + -z[90];
z[47] = abb[30] * z[47];
z[56] = -abb[54] * z[5];
z[58] = abb[6] * z[97];
z[5] = abb[52] * z[5];
z[5] = z[5] + z[56] + z[58];
z[33] = z[27] + -z[33];
z[56] = abb[44] * (T(3) / T(2)) + -z[33];
z[58] = -z[56] + z[85];
z[58] = abb[8] * z[58];
z[22] = 2 * abb[45] + abb[47] * (T(-3) / T(2)) + z[22] + z[33];
z[33] = -abb[13] * z[22];
z[61] = -abb[45] + abb[47] + z[105];
z[62] = abb[9] * z[61];
z[69] = (T(1) / T(2)) * z[98];
z[5] = (T(1) / T(2)) * z[5] + z[33] + z[58] + -z[62] + -z[69] + -z[72];
z[5] = abb[34] * z[5];
z[33] = abb[13] * z[71];
z[33] = z[33] + -z[72];
z[48] = -z[41] * z[48];
z[58] = abb[54] * (T(1) / T(2));
z[3] = -z[3] * z[58];
z[71] = abb[45] + abb[46];
z[76] = -abb[48] + z[71];
z[76] = abb[2] * z[76];
z[3] = z[3] + z[33] + z[48] + z[76] + z[102];
z[3] = abb[35] * z[3];
z[48] = abb[5] * z[86];
z[53] = z[48] + z[53] + z[54];
z[2] = -z[2] * z[58];
z[70] = -abb[40] + -z[70];
z[70] = abb[45] + (T(1) / T(2)) * z[70];
z[70] = abb[0] * z[70];
z[2] = z[2] + z[33] + -z[53] + z[70];
z[2] = abb[33] * z[2];
z[33] = abb[32] + -abb[36];
z[70] = abb[26] * abb[29];
z[70] = -z[33] + z[70];
z[70] = z[61] * z[70];
z[27] = abb[45] * (T(-1) / T(2)) + -z[27] + z[66];
z[66] = prod_pow(abb[26], 2);
z[76] = z[27] * z[66];
z[27] = -abb[31] * z[27];
z[61] = -abb[29] * z[61];
z[27] = z[27] + z[61];
z[27] = abb[31] * z[27];
z[27] = z[27] + z[70] + z[76];
z[27] = abb[9] * z[27];
z[33] = -z[33] * z[72];
z[2] = z[2] + z[3] + z[5] + z[27] + z[33];
z[3] = z[29] + -z[40] + -z[65] + z[94];
z[3] = z[3] * z[20];
z[5] = 2 * z[43];
z[5] = abb[36] * z[5];
z[27] = -abb[27] * abb[28] * z[37];
z[33] = 3 * abb[32];
z[22] = -z[22] * z[33];
z[3] = z[3] + z[5] + z[22] + z[27];
z[3] = abb[13] * z[3];
z[5] = z[25] * z[58];
z[22] = abb[8] * z[97];
z[5] = z[5] + z[15] + (T(1) / T(2)) * z[22] + -z[53] + -z[69] + z[87];
z[5] = 3 * z[5];
z[22] = abb[55] * z[5];
z[27] = -z[38] + z[111];
z[35] = -2 * abb[46] + abb[42] * (T(3) / T(2)) + -z[35] + z[56];
z[35] = z[35] * z[41];
z[13] = z[13] * z[58];
z[13] = -z[13] + -z[15] + z[27] + -z[35] + -z[102] + z[106];
z[13] = 3 * z[13];
z[35] = -abb[56] * z[13];
z[41] = abb[28] + abb[27] * (T(1) / T(2));
z[41] = abb[27] * z[41];
z[41] = -abb[55] + (T(1) / T(2)) * z[20] + -z[41] + z[66];
z[43] = abb[16] * (T(1) / T(2));
z[41] = z[41] * z[43];
z[43] = -abb[15] + abb[17];
z[43] = z[10] * z[43];
z[53] = abb[15] + abb[17];
z[56] = abb[28] * z[53];
z[58] = (T(1) / T(2)) * z[53];
z[61] = abb[27] * z[58];
z[56] = z[56] + -z[61];
z[61] = abb[30] * z[58];
z[43] = z[43] + -z[56] + -z[61];
z[10] = z[10] * z[43];
z[43] = abb[15] * abb[30];
z[43] = z[43] + z[56];
z[65] = abb[30] * (T(1) / T(2));
z[43] = z[43] * z[65];
z[58] = abb[55] * z[58];
z[65] = abb[0] + abb[6];
z[65] = -abb[5] + 2 * abb[25] + abb[12] * (T(-1) / T(4)) + (T(-3) / T(4)) * z[65];
z[66] = abb[57] * z[65];
z[10] = -z[10] + z[41] + -z[43] + -z[58] + z[66];
z[41] = abb[17] + abb[15] * (T(9) / T(2));
z[41] = 2 * abb[16] + (T(1) / T(2)) * z[41];
z[4] = z[4] * z[41];
z[41] = abb[16] + -abb[17];
z[6] = z[6] * z[41];
z[4] = -z[4] + z[6] + 3 * z[10];
z[6] = abb[49] + abb[50];
z[4] = z[4] * z[6];
z[10] = z[16] + -z[21] + z[29] + -z[31];
z[10] = abb[6] * z[10];
z[16] = -abb[5] * z[59];
z[29] = abb[8] * z[100];
z[10] = z[10] + z[16] + z[29] + -z[30] + (T(1) / T(2)) * z[54];
z[10] = abb[27] * z[10];
z[16] = abb[6] * z[51];
z[16] = z[16] + z[54];
z[30] = z[16] + z[48] + -z[95] + -z[104];
z[30] = abb[28] * z[30];
z[10] = z[10] + z[30];
z[10] = abb[27] * z[10];
z[16] = z[16] + -z[73];
z[16] = (T(1) / T(2)) * z[16] + -z[29] + z[99];
z[16] = z[16] * z[20];
z[29] = abb[36] * z[117];
z[14] = -abb[54] * z[14];
z[14] = z[14] + -z[15] + -z[27];
z[14] = z[14] * z[33];
z[15] = z[60] + z[89];
z[15] = abb[27] * z[15];
z[27] = z[20] * z[68];
z[15] = z[15] + z[27];
z[15] = abb[0] * z[15];
z[27] = abb[16] + abb[18];
z[30] = -3 * z[27];
z[30] = z[30] * z[55];
z[33] = z[51] * z[53];
z[41] = -abb[24] * z[74];
z[30] = z[30] + (T(3) / T(4)) * z[33] + z[41];
z[30] = abb[57] * z[30];
z[37] = -abb[36] * z[37];
z[20] = z[20] * z[51];
z[20] = (T(-5) / T(4)) * z[20] + z[37];
z[20] = abb[5] * z[20];
z[0] = z[0] + z[1] + 3 * z[2] + z[3] + z[4] + z[7] + z[8] + z[10] + z[11] + z[14] + z[15] + z[16] + z[20] + z[22] + z[24] + z[29] + z[30] + z[35] + z[47];
z[1] = abb[44] * (T(5) / T(2)) + -z[34] + z[42] + -z[85];
z[2] = abb[8] * z[1];
z[3] = z[25] * z[80];
z[4] = 6 * z[96];
z[3] = z[2] + z[3] + z[4] + -z[45] + z[48] + -z[52] + -5 * z[54] + z[81] + -z[87] + z[121];
z[3] = abb[28] * z[3];
z[7] = z[38] + z[106];
z[8] = -z[44] + 6 * z[71] + -z[75];
z[8] = abb[2] * z[8];
z[7] = 3 * z[7] + z[8] + z[9] + -z[82] + z[108] + z[110] + z[121];
z[7] = abb[31] * z[7];
z[8] = 4 * abb[41] + 2 * abb[43];
z[9] = -z[8] + z[21] + z[34] + -z[39] + -z[40] + z[67];
z[9] = abb[6] * z[9];
z[10] = -z[31] + z[39] + z[46] + -z[49];
z[10] = abb[5] * z[10];
z[2] = -z[2] + z[9] + z[10] + -z[17] + z[54] + -z[63] + -z[101];
z[2] = abb[27] * z[2];
z[9] = z[36] + z[78];
z[9] = abb[30] * z[9];
z[10] = -z[44] + z[64];
z[10] = abb[2] * z[10];
z[11] = -abb[42] + -2 * z[32] + z[112];
z[11] = abb[8] * z[11];
z[10] = z[10] + z[11];
z[1] = -abb[6] * z[1];
z[1] = z[1] + -z[4] + 2 * z[10] + z[95] + -z[109] + -z[122];
z[1] = abb[29] * z[1];
z[4] = z[8] + z[50] + -z[75] + z[119];
z[8] = -abb[5] + abb[6];
z[4] = z[4] * z[8];
z[8] = -abb[47] + z[50] + z[118];
z[8] = 2 * z[8];
z[10] = -abb[0] * z[8];
z[4] = z[4] + z[10] + -z[57] + z[101] + -z[121];
z[4] = abb[26] * z[4];
z[8] = -abb[28] * z[8];
z[8] = z[8] + -z[60];
z[8] = abb[0] * z[8];
z[10] = -abb[26] + abb[31];
z[10] = z[10] * z[62];
z[1] = z[1] + z[2] + z[3] + z[4] + z[7] + z[8] + z[9] + 6 * z[10];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[38] * z[13];
z[3] = abb[37] * z[5];
z[4] = abb[30] * z[18];
z[5] = abb[20] + abb[22];
z[5] = abb[29] * z[5];
z[7] = abb[31] * z[23];
z[8] = abb[28] * z[26];
z[4] = z[4] + z[5] + -z[7] + -z[8] + z[19];
z[4] = m1_set::bc<T>[0] * z[4];
z[5] = abb[38] * z[23];
z[7] = abb[37] * z[26];
z[8] = abb[39] * z[28];
z[4] = z[4] + -z[5] + -z[7] + z[8];
z[5] = abb[52] + z[12];
z[5] = (T(3) / T(2)) * z[5];
z[4] = z[4] * z[5];
z[5] = -z[56] + z[61];
z[7] = -abb[26] + (T(1) / T(2)) * z[91];
z[7] = abb[16] * z[7];
z[8] = abb[15] * abb[26];
z[5] = (T(1) / T(2)) * z[5] + z[7] + -z[8];
z[5] = m1_set::bc<T>[0] * z[5];
z[7] = abb[16] + z[53];
z[8] = abb[37] * (T(1) / T(2));
z[7] = z[7] * z[8];
z[8] = abb[39] * z[65];
z[5] = z[5] + -z[7] + z[8];
z[6] = 3 * z[6];
z[5] = z[5] * z[6];
z[6] = -z[27] * z[55];
z[7] = -abb[24] * abb[54];
z[7] = z[7] + z[33];
z[6] = z[6] + (T(1) / T(4)) * z[7];
z[6] = abb[39] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + 3 * z[6];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_333_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("10.269512493905512545637669731671240915263504226021180975992455577"),stof<T>("-58.554694814178373260477368471541400794702884064369990307851491817")}, std::complex<T>{stof<T>("-87.808318409424196712863513489722099758364160450913732976760279726"),stof<T>("12.043475572765425361043016237067971883919211911958527719860594222")}, std::complex<T>{stof<T>("-17.470784366395097099421394687436125969479083356039415516819461543"),stof<T>("42.993029214271078330685831479865254404261186486712346526888485666")}, std::complex<T>{stof<T>("-43.904159204712098356431756744861049879182080225456866488380139863"),stof<T>("6.021737786382712680521508118533985941959605955979263859930297111")}, std::complex<T>{stof<T>("-38.148814222331855297655024941017786507008320111885051484873249098"),stof<T>("8.117688644546420305100701584260566079796731294693622101679823984")}, std::complex<T>{stof<T>("65.477352306750047753539962526075242143578640739751693118275195217"),stof<T>("42.537230464296366813635157249429466831643957923240539923670222409")}, std::complex<T>{stof<T>("87.462304184670414339822295141339345656147468208240474607580221573"),stof<T>("-61.106444422316792402107236167703768505157237966556155152819281946")}, std::complex<T>{stof<T>("-5.7553449823802430587767318038432633721737601135718150035068907648"),stof<T>("-2.0959508581637076245791934657265801378371253387143582417495268727")}, std::complex<T>{stof<T>("-26.087360613563218883969143709042169807486304626744192602380620166"),stof<T>("12.091677421663001390899896569304528158936445523864544766000499169")}, std::complex<T>{stof<T>("-16.512200902804655404527292095446812870862691003762646777629453187"),stof<T>("24.063143698697434894401075584092701938234885903411332628943547409")}, std::complex<T>{stof<T>("-16.512200902804655404527292095446812870862691003762646777629453187"),stof<T>("24.063143698697434894401075584092701938234885903411332628943547409")}, std::complex<T>{stof<T>("1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("-1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("-1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("-1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("-1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("1.893635402193665897979852592125278317152816937002069288848804013")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[22].real()/kbase.W[22].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[126].real()/kbase.W[126].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_333_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_333_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("2.048661529930890250814984560956898326304568318487319572884966684"),stof<T>("78.33165179290404870354153341096227691479327643828139069085250629")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,58> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W21(k,dl), dlog_W23(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[22].real()/k.W[22].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[126].real()/k.W[126].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_333_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_333_DLogXconstant_part(base_point<T>, kend);
	value += f_4_333_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_333_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_333_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_333_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_333_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_333_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_333_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
