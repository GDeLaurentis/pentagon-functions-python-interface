/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_114.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_114_abbreviated (const std::array<T,57>& abb) {
T z[68];
z[0] = abb[33] + abb[34];
z[1] = (T(1) / T(2)) * z[0];
z[2] = -abb[35] + z[1];
z[3] = abb[4] + abb[7];
z[4] = abb[17] + z[3];
z[5] = z[2] * z[4];
z[3] = abb[6] + z[3];
z[6] = abb[32] * z[3];
z[6] = -z[5] + z[6];
z[6] = m1_set::bc<T>[0] * z[6];
z[7] = abb[31] * m1_set::bc<T>[0];
z[8] = abb[41] + z[7];
z[0] = -abb[35] + z[0];
z[9] = m1_set::bc<T>[0] * z[0];
z[9] = -abb[42] + -z[8] + z[9];
z[10] = abb[8] * z[9];
z[11] = z[3] * z[8];
z[12] = abb[42] * z[4];
z[6] = z[6] + z[10] + -z[11] + z[12];
z[13] = abb[5] * (T(1) / T(4));
z[14] = abb[32] + -abb[35];
z[15] = m1_set::bc<T>[0] * z[14];
z[15] = -abb[42] + z[15];
z[16] = z[13] * z[15];
z[1] = m1_set::bc<T>[0] * z[1];
z[1] = z[1] + -z[8];
z[17] = abb[15] * (T(1) / T(4));
z[18] = z[1] * z[17];
z[19] = abb[32] * m1_set::bc<T>[0];
z[20] = -z[8] + z[19];
z[21] = abb[13] * (T(1) / T(2));
z[22] = z[20] * z[21];
z[23] = abb[18] + abb[19] + abb[21];
z[24] = abb[20] + z[23];
z[24] = (T(1) / T(8)) * z[24];
z[25] = abb[43] * z[24];
z[2] = m1_set::bc<T>[0] * z[2];
z[2] = -abb[42] + z[2];
z[26] = abb[14] * z[2];
z[6] = (T(1) / T(4)) * z[6] + -z[16] + -z[18] + -z[22] + z[25] + z[26];
z[16] = -abb[48] + -abb[50];
z[6] = z[6] * z[16];
z[16] = abb[26] + abb[27];
z[18] = abb[22] + abb[24];
z[22] = z[16] + z[18];
z[25] = abb[34] * z[22];
z[27] = abb[27] + z[18];
z[27] = abb[33] * z[27];
z[28] = abb[26] * abb[33];
z[27] = z[27] + z[28];
z[25] = z[25] + z[27];
z[29] = abb[27] * abb[35];
z[30] = abb[24] + abb[26];
z[31] = abb[32] * z[30];
z[25] = (T(1) / T(2)) * z[25] + -z[29] + -z[31];
z[25] = m1_set::bc<T>[0] * z[25];
z[29] = abb[23] + abb[25];
z[9] = z[9] * z[29];
z[8] = -abb[22] * z[8];
z[31] = abb[27] * abb[42];
z[32] = abb[28] * abb[43];
z[8] = z[8] + z[9] + z[25] + -z[31] + (T(-1) / T(2)) * z[32];
z[9] = abb[44] + abb[45] + -abb[46];
z[25] = (T(-1) / T(4)) * z[9];
z[8] = z[8] * z[25];
z[19] = z[3] * z[19];
z[11] = -z[11] + z[19];
z[19] = abb[15] * (T(1) / T(2));
z[1] = z[1] * z[19];
z[25] = abb[5] * (T(1) / T(2));
z[15] = z[15] * z[25];
z[31] = abb[20] * (T(1) / T(4));
z[31] = abb[43] * z[31];
z[1] = -z[1] + (T(1) / T(2)) * z[11] + -z[15] + z[31];
z[7] = abb[41] + -abb[42] + z[7];
z[11] = -abb[32] + (T(1) / T(2)) * z[0];
z[11] = m1_set::bc<T>[0] * z[11];
z[7] = (T(1) / T(2)) * z[7] + z[11];
z[7] = abb[8] * z[7];
z[7] = z[1] + z[7];
z[11] = abb[49] * (T(1) / T(2));
z[15] = abb[51] * (T(-1) / T(2)) + -z[11];
z[7] = z[7] * z[15];
z[15] = abb[13] * z[20];
z[1] = -z[1] + (T(-1) / T(2)) * z[10] + z[15];
z[10] = abb[47] * (T(1) / T(2));
z[1] = z[1] * z[10];
z[5] = m1_set::bc<T>[0] * z[5];
z[5] = z[5] + -z[12];
z[5] = (T(1) / T(4)) * z[5] + -z[26];
z[5] = abb[52] * z[5];
z[2] = -z[2] * z[23];
z[12] = -abb[29] + (T(1) / T(4)) * z[4];
z[15] = abb[43] * z[12];
z[2] = (T(1) / T(2)) * z[2] + z[15];
z[15] = abb[53] * (T(1) / T(2));
z[2] = z[2] * z[15];
z[20] = abb[52] * z[23];
z[20] = (T(1) / T(8)) * z[20];
z[23] = -abb[43] * z[20];
z[1] = z[1] + z[2] + z[5] + z[6] + z[7] + z[8] + z[23];
z[1] = (T(1) / T(8)) * z[1];
z[2] = z[16] + -z[18];
z[2] = abb[34] * z[2];
z[5] = -abb[27] + z[18];
z[5] = abb[33] * z[5];
z[2] = z[2] + z[5] + -z[28];
z[5] = abb[30] * (T(1) / T(2));
z[6] = z[5] * z[22];
z[7] = abb[35] * z[16];
z[2] = (T(1) / T(2)) * z[2] + -z[6] + z[7];
z[2] = abb[30] * z[2];
z[6] = abb[32] * (T(1) / T(2));
z[7] = z[6] * z[30];
z[8] = abb[24] * abb[34];
z[7] = z[7] + -z[8] + -z[28];
z[7] = abb[32] * z[7];
z[23] = abb[35] * (T(1) / T(2));
z[26] = abb[26] * z[23];
z[16] = abb[34] * z[16];
z[16] = z[16] + z[26] + -z[28];
z[16] = abb[35] * z[16];
z[26] = abb[30] + -abb[33];
z[28] = -z[18] * z[26];
z[30] = abb[31] * (T(1) / T(2));
z[31] = abb[24] * z[30];
z[8] = -z[8] + z[28] + z[31];
z[8] = abb[31] * z[8];
z[28] = prod_pow(m1_set::bc<T>[0], 2);
z[31] = (T(1) / T(3)) * z[28];
z[22] = z[22] * z[31];
z[27] = abb[34] * z[27];
z[32] = -abb[36] + -abb[55];
z[32] = abb[27] * z[32];
z[33] = abb[24] * abb[38];
z[34] = abb[22] * abb[54];
z[18] = abb[37] * z[18];
z[35] = abb[36] + -abb[39];
z[36] = abb[26] * z[35];
z[2] = z[2] + z[7] + -z[8] + -z[16] + -z[18] + -z[22] + (T(1) / T(2)) * z[27] + z[32] + z[33] + -z[34] + -z[36];
z[7] = -abb[35] + z[5];
z[8] = abb[32] + z[7];
z[8] = z[5] * z[8];
z[16] = z[26] * z[30];
z[16] = z[8] + -z[16];
z[18] = abb[33] * abb[34];
z[22] = -abb[55] + z[18];
z[27] = abb[34] * abb[35];
z[32] = z[22] + -z[27];
z[33] = abb[38] + -abb[40];
z[34] = abb[39] + z[33];
z[36] = prod_pow(abb[32], 2);
z[36] = (T(1) / T(2)) * z[36];
z[37] = z[34] + z[36];
z[38] = -abb[54] + z[32] + z[37];
z[38] = -z[16] + -z[31] + (T(1) / T(2)) * z[38];
z[29] = z[29] * z[38];
z[39] = abb[56] * (T(1) / T(4));
z[40] = abb[28] * z[39];
z[2] = (T(1) / T(2)) * z[2] + z[29] + -z[40];
z[9] = (T(1) / T(2)) * z[9];
z[2] = z[2] * z[9];
z[9] = abb[37] + abb[39];
z[9] = abb[4] * z[9];
z[29] = abb[6] + -abb[7];
z[29] = abb[38] * z[29];
z[3] = abb[54] * z[3];
z[40] = abb[4] + -abb[7];
z[41] = abb[40] * z[40];
z[3] = z[3] + z[9] + z[29] + -z[41];
z[9] = abb[4] + abb[17];
z[29] = abb[34] * z[9];
z[41] = abb[7] * abb[33];
z[29] = z[29] + z[41];
z[42] = abb[35] * (T(3) / T(2));
z[43] = abb[7] * z[42];
z[43] = -z[29] + z[43];
z[43] = abb[35] * z[43];
z[18] = (T(1) / T(2)) * z[18];
z[44] = -abb[55] + z[18];
z[4] = -z[4] * z[44];
z[45] = abb[7] + -abb[17];
z[45] = abb[36] * z[45];
z[4] = -z[4] + z[45];
z[45] = abb[4] + abb[6];
z[46] = abb[34] * z[45];
z[41] = z[41] + z[46];
z[46] = abb[6] * z[6];
z[46] = -z[41] + z[46];
z[46] = abb[32] * z[46];
z[43] = z[3] + z[4] + z[43] + z[46];
z[47] = -abb[7] + abb[17] * (T(1) / T(2));
z[48] = z[5] * z[47];
z[49] = abb[4] * (T(1) / T(2));
z[47] = z[47] + z[49];
z[50] = abb[35] * z[47];
z[51] = -abb[33] + abb[34];
z[9] = -abb[7] + z[9];
z[9] = -z[9] * z[51];
z[9] = (T(1) / T(4)) * z[9];
z[52] = z[6] * z[40];
z[48] = z[9] + z[48] + -z[50] + z[52];
z[48] = abb[30] * z[48];
z[50] = abb[31] + -abb[32];
z[53] = abb[30] + z[51];
z[54] = z[50] + -z[53];
z[54] = abb[31] * z[54];
z[55] = abb[30] * abb[32];
z[56] = abb[32] * z[51];
z[57] = (T(1) / T(6)) * z[28];
z[54] = -z[33] + z[54] + z[55] + z[56] + z[57];
z[54] = abb[1] * z[54];
z[14] = abb[30] * z[14];
z[55] = prod_pow(abb[35], 2);
z[58] = -abb[39] + abb[40];
z[14] = -abb[37] + z[14] + -z[36] + (T(1) / T(2)) * z[55] + z[58];
z[14] = abb[11] * z[14];
z[38] = abb[8] * z[38];
z[59] = 3 * abb[4] + abb[6];
z[60] = abb[31] * (T(1) / T(4));
z[59] = z[59] * z[60];
z[60] = abb[7] * z[5];
z[60] = (T(1) / T(2)) * z[41] + -z[60];
z[61] = abb[4] * abb[32];
z[59] = z[59] + -z[60] + -z[61];
z[59] = abb[31] * z[59];
z[61] = abb[7] + abb[17];
z[62] = abb[4] + (T(1) / T(2)) * z[61];
z[62] = z[31] * z[62];
z[63] = -abb[32] + z[30];
z[63] = abb[31] * z[63];
z[36] = z[36] + z[63];
z[36] = abb[12] * z[36];
z[43] = -z[14] + z[36] + z[38] + (T(-1) / T(2)) * z[43] + z[48] + z[54] + z[59] + z[62];
z[42] = z[42] + z[51];
z[42] = abb[35] * z[42];
z[48] = abb[32] + z[51];
z[48] = abb[32] * z[48];
z[62] = -abb[30] + 3 * abb[35] + z[51];
z[62] = abb[30] * z[62];
z[35] = -abb[37] + abb[38] + z[35];
z[42] = z[35] + z[42] + -z[48] + -z[62];
z[48] = (T(1) / T(2)) * z[51];
z[62] = -abb[32] + abb[31] * (T(3) / T(4)) + -z[5] + -z[48];
z[62] = abb[31] * z[62];
z[42] = (T(1) / T(2)) * z[42] + -z[62];
z[62] = abb[16] * (T(1) / T(2));
z[42] = z[42] * z[62];
z[64] = z[5] * z[53];
z[18] = z[18] + -z[31];
z[26] = abb[31] * z[26];
z[26] = abb[37] + abb[54] + -z[18] + -z[26] + z[64];
z[17] = z[17] * z[26];
z[64] = abb[32] * abb[34];
z[50] = -abb[34] + z[50];
z[65] = abb[31] * z[50];
z[64] = -abb[54] + z[57] + z[64] + z[65];
z[21] = z[21] * z[64];
z[0] = z[0] * z[23];
z[65] = -abb[55] + z[18];
z[0] = z[0] + -z[65];
z[0] = abb[14] * z[0];
z[6] = -abb[33] + z[6];
z[6] = abb[32] * z[6];
z[66] = -abb[33] + z[23];
z[66] = abb[35] * z[66];
z[66] = -abb[55] + z[66];
z[6] = abb[39] + z[6] + -z[66];
z[13] = z[6] * z[13];
z[67] = -abb[32] + z[5];
z[67] = abb[30] * z[67];
z[67] = abb[37] + z[67];
z[37] = abb[36] + z[37] + z[67];
z[37] = abb[3] * z[37];
z[24] = abb[56] * z[24];
z[13] = -z[0] + z[13] + z[17] + -z[21] + z[24] + (T(-1) / T(4)) * z[37] + z[42] + (T(1) / T(2)) * z[43];
z[17] = abb[50] * z[13];
z[21] = abb[35] + z[51];
z[24] = abb[35] * z[21];
z[42] = abb[36] + z[57];
z[24] = z[24] + z[42];
z[43] = -z[7] + z[48];
z[43] = abb[30] * z[43];
z[24] = (T(1) / T(2)) * z[24] + -z[43];
z[24] = abb[2] * z[24];
z[13] = z[13] + -z[24];
z[13] = abb[48] * z[13];
z[48] = -z[30] + z[53];
z[48] = abb[31] * z[48];
z[23] = z[23] + z[51];
z[23] = abb[35] * z[23];
z[21] = abb[30] * z[21];
z[21] = -z[21] + z[23] + z[35] + z[48] + -z[56];
z[21] = z[21] * z[62];
z[19] = z[19] * z[26];
z[6] = z[6] * z[25];
z[25] = abb[20] * z[39];
z[6] = z[6] + z[19] + z[21] + z[25] + (T(-1) / T(2)) * z[37];
z[19] = abb[4] * z[57];
z[3] = -z[3] + z[19];
z[19] = abb[10] + z[40];
z[21] = z[19] * z[55];
z[25] = abb[6] * (T(1) / T(2));
z[26] = -abb[4] + -z[25];
z[26] = abb[32] * z[26];
z[26] = z[26] + z[41];
z[26] = abb[32] * z[26];
z[21] = z[3] + z[21] + z[26];
z[26] = -abb[34] + abb[35];
z[26] = abb[35] * z[26];
z[22] = abb[39] + abb[54] + z[22] + z[26] + -z[33];
z[26] = abb[32] * (T(1) / T(4));
z[33] = -abb[33] + z[26];
z[33] = abb[32] * z[33];
z[16] = z[16] + (T(1) / T(2)) * z[22] + -z[31] + z[33];
z[16] = abb[8] * z[16];
z[22] = -abb[7] + abb[10] + z[49];
z[22] = z[5] * z[22];
z[19] = -abb[35] * z[19];
z[19] = z[19] + z[22] + z[52];
z[19] = abb[30] * z[19];
z[22] = z[30] * z[45];
z[31] = abb[7] * abb[30];
z[22] = z[22] + z[31] + -z[41];
z[22] = z[22] * z[30];
z[14] = z[6] + -z[14] + z[16] + z[19] + (T(1) / T(2)) * z[21] + z[22];
z[14] = (T(1) / T(2)) * z[14] + -z[24];
z[14] = abb[51] * z[14];
z[16] = z[7] + -z[51];
z[16] = abb[30] * z[16];
z[16] = z[16] + z[23] + z[42];
z[16] = abb[0] * z[16];
z[19] = z[58] + z[63] + -z[67];
z[19] = abb[9] * z[19];
z[6] = -z[6] + (T(1) / T(2)) * z[16] + z[19] + -z[54];
z[19] = abb[4] * z[5];
z[21] = abb[32] * z[40];
z[19] = z[19] + -z[21];
z[19] = abb[30] * z[19];
z[3] = -z[3] + z[19];
z[19] = -z[3] + -z[46];
z[22] = -abb[13] * z[64];
z[19] = -z[6] + (T(1) / T(2)) * z[19] + z[22] + z[38] + z[59];
z[10] = z[10] * z[19];
z[19] = abb[54] + z[32] + z[34];
z[22] = abb[33] * (T(-1) / T(2)) + z[5] + -z[50];
z[22] = abb[31] * z[22];
z[23] = (T(1) / T(2)) * z[28];
z[26] = -abb[34] + z[26];
z[26] = abb[32] * z[26];
z[8] = -z[8] + (T(1) / T(2)) * z[19] + z[22] + -z[23] + z[26];
z[8] = abb[8] * z[8];
z[19] = -abb[7] + -z[25];
z[19] = abb[32] * z[19];
z[19] = z[19] + z[41];
z[19] = abb[32] * z[19];
z[3] = -z[3] + z[19];
z[19] = -abb[7] + abb[4] * (T(3) / T(2)) + z[25];
z[19] = z[19] * z[30];
z[19] = z[19] + -z[21] + -z[60];
z[19] = abb[31] * z[19];
z[3] = (T(1) / T(2)) * z[3] + -z[6] + z[8] + z[19] + z[36];
z[3] = z[3] * z[11];
z[6] = -z[27] + z[43];
z[8] = z[6] + -z[23] + z[44];
z[8] = abb[19] * z[8];
z[6] = -abb[36] + z[6] + z[65];
z[6] = abb[21] * z[6];
z[5] = -z[5] * z[51];
z[5] = abb[36] + z[5] + z[18] + z[66];
z[5] = abb[18] * z[5];
z[5] = z[5] + z[6] + z[8];
z[6] = -abb[56] * z[12];
z[5] = (T(1) / T(2)) * z[5] + z[6];
z[5] = z[5] * z[15];
z[6] = abb[10] + abb[7] * (T(-3) / T(2));
z[6] = abb[35] * z[6];
z[6] = z[6] + z[29];
z[6] = abb[35] * z[6];
z[8] = z[49] + (T(1) / T(3)) * z[61];
z[8] = z[8] * z[28];
z[4] = -z[4] + z[6] + z[8];
z[6] = abb[10] + z[47];
z[6] = z[6] * z[7];
z[6] = z[6] + z[9];
z[6] = abb[30] * z[6];
z[4] = (T(1) / T(2)) * z[4] + z[6];
z[0] = -z[0] + (T(1) / T(2)) * z[4];
z[0] = abb[52] * z[0];
z[4] = -abb[50] + -abb[52];
z[4] = z[4] * z[24];
z[6] = abb[51] + abb[52];
z[6] = z[6] * z[16];
z[7] = abb[56] * z[20];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + (T(1) / T(4)) * z[6] + z[7] + z[10] + z[13] + z[14] + z[17];
z[0] = (T(1) / T(8)) * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_114_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.046618135920903845379257436535277128814528324294392835157426320101"),stof<T>("0.065697141821923954643434940311408509850923023301427807930085523535")}, std::complex<T>{stof<T>("-0.10008051447798893754928177361011678232881992032254754429695053896"),stof<T>("0.15667203808942366193361105525774746104804654211009050740095937023")}, std::complex<T>{stof<T>("-0.082064970009860568655597121064660901620155644010025932392538094835"),stof<T>("0.054582425969257142069214373319759432577297424981320104728035288333")}, std::complex<T>{stof<T>("-0.10008051447798893754928177361011678232881992032254754429695053896"),stof<T>("0.15667203808942366193361105525774746104804654211009050740095937023")}, std::complex<T>{stof<T>("-0.099810523118215498410829068505066218644782890704261014287079393735"),stof<T>("0.054582425969257142069214373319759432577297424981320104728035288333")}, std::complex<T>{stof<T>("-0.073004002965430891648177869491412222559798291561667069582026037991"),stof<T>("0.090974896267499707290176114946338951197123518808662699470873846691")}, std::complex<T>{stof<T>("-0.05308425981936598198548838172921953686782368677692209810094997181"),stof<T>("0.076949906797383748750224446478305787075727995940786863171375303992")}};
	
	std::vector<C> intdlogs = {rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[28].real()/kbase.W[28].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_114_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_114_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.0338312628989066314987710296439447261305518220647766021407303074"),stof<T>("-0.038631280347612859422852000582678734739397174701595147393936191928")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,57> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[28].real()/k.W[28].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_114_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_114_DLogXconstant_part(base_point<T>, kend);
	value += f_4_114_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_114_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_114_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_114_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_114_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_114_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_114_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
