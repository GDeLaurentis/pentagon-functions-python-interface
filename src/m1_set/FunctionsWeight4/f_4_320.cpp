/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_320.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_320_abbreviated (const std::array<T,29>& abb) {
T z[75];
z[0] = abb[7] * abb[12];
z[1] = abb[8] + abb[9];
z[2] = abb[11] * z[1];
z[3] = abb[9] * abb[13];
z[4] = abb[8] * abb[13];
z[0] = z[0] + -z[2] + z[3] + z[4];
z[2] = abb[7] * abb[14];
z[0] = 2 * z[0] + -z[2];
z[0] = abb[14] * z[0];
z[2] = z[3] + (T(3) / T(2)) * z[4];
z[5] = -abb[7] + abb[9];
z[6] = abb[11] * z[5];
z[7] = abb[9] + abb[8] * (T(3) / T(2));
z[8] = abb[12] * z[7];
z[6] = -z[2] + z[6] + z[8];
z[6] = abb[11] * z[6];
z[8] = 5 * abb[7] + 2 * abb[9];
z[8] = 2 * abb[8] + (T(1) / T(3)) * z[8];
z[9] = prod_pow(m1_set::bc<T>[0], 2);
z[8] = z[8] * z[9];
z[10] = abb[7] + abb[9];
z[10] = abb[27] * z[10];
z[5] = abb[15] * z[5];
z[5] = z[5] + z[10];
z[2] = abb[12] * z[2];
z[10] = prod_pow(abb[13], 2);
z[11] = 2 * abb[27] + (T(3) / T(2)) * z[10];
z[11] = abb[8] * z[11];
z[12] = abb[0] + abb[2];
z[12] = 3 * abb[3] + abb[6] + -6 * abb[10] + (T(3) / T(2)) * z[12];
z[13] = abb[28] * z[12];
z[0] = z[0] + -z[2] + 2 * z[5] + z[6] + z[8] + z[11] + z[13];
z[2] = abb[25] + abb[26];
z[0] = -z[0] * z[2];
z[5] = -abb[21] + abb[24];
z[6] = 2 * abb[18];
z[8] = 3 * abb[19];
z[11] = -abb[22] + z[5] + z[6] + -z[8];
z[13] = 2 * abb[4];
z[11] = z[11] * z[13];
z[14] = 4 * abb[20];
z[15] = 4 * abb[24];
z[16] = 2 * abb[22];
z[17] = -5 * abb[18] + 20 * abb[19] + 11 * abb[21] + -abb[23] + -z[14] + -z[15] + -z[16];
z[17] = abb[1] * z[17];
z[18] = 5 * abb[19];
z[19] = 4 * abb[21];
z[20] = z[18] + z[19];
z[21] = 2 * abb[20];
z[22] = abb[24] + z[21];
z[23] = -abb[18] + z[16];
z[24] = -z[20] + z[22] + z[23];
z[24] = abb[0] * z[24];
z[15] = z[15] + -z[19];
z[19] = 4 * abb[23] + -z[15];
z[25] = abb[19] * (T(-15) / T(2)) + abb[20] * (T(7) / T(2)) + -z[19];
z[25] = abb[5] * z[25];
z[26] = 3 * abb[18];
z[27] = 3 * abb[24];
z[28] = z[26] + z[27];
z[29] = 11 * abb[19] + 6 * abb[21] + -z[21] + -z[28];
z[29] = abb[3] * z[29];
z[30] = abb[18] + -abb[21];
z[31] = abb[20] * (T(1) / T(2));
z[32] = abb[23] + abb[19] * (T(-3) / T(2)) + z[30] + -z[31];
z[32] = abb[2] * z[32];
z[11] = z[11] + z[17] + z[24] + z[25] + z[29] + z[32];
z[11] = abb[14] * z[11];
z[17] = 2 * abb[21];
z[24] = z[8] + z[17];
z[25] = 2 * abb[23];
z[29] = -abb[20] + z[25];
z[32] = z[6] + -z[24] + z[29];
z[32] = abb[2] * z[32];
z[33] = -abb[20] + z[18] + z[19];
z[33] = abb[5] * z[33];
z[34] = -abb[24] + z[24];
z[35] = -abb[18] + z[34];
z[36] = 2 * abb[3];
z[37] = -z[35] * z[36];
z[38] = 4 * abb[19];
z[39] = -abb[18] + 3 * abb[21] + z[38];
z[40] = 2 * abb[24];
z[41] = -abb[23] + -z[39] + z[40];
z[42] = 2 * abb[1];
z[41] = z[41] * z[42];
z[43] = abb[18] + -abb[19];
z[44] = abb[4] * z[43];
z[45] = 2 * z[44];
z[32] = z[32] + z[33] + z[37] + z[41] + -z[45];
z[32] = abb[12] * z[32];
z[33] = 2 * abb[19] + abb[21];
z[37] = 2 * z[33];
z[41] = abb[18] + abb[20];
z[46] = z[37] + -z[41];
z[47] = abb[22] + abb[23];
z[48] = -z[27] + 2 * z[46] + z[47];
z[49] = abb[3] * z[48];
z[50] = z[40] + -z[47];
z[51] = -z[46] + z[50];
z[52] = abb[6] * z[51];
z[49] = z[49] + z[52];
z[53] = -5 * abb[24] + 4 * z[46] + z[47];
z[53] = abb[1] * z[53];
z[54] = -abb[19] + abb[20];
z[55] = abb[5] * z[54];
z[44] = z[44] + z[55];
z[53] = z[44] + z[49] + z[53];
z[56] = abb[13] * z[53];
z[57] = abb[2] * abb[13];
z[58] = -z[35] * z[57];
z[56] = z[56] + z[58];
z[35] = abb[2] * z[35];
z[53] = z[35] + -z[53];
z[58] = 2 * abb[11];
z[53] = z[53] * z[58];
z[34] = -abb[20] + z[34];
z[59] = abb[11] + -abb[13];
z[34] = z[34] * z[59];
z[59] = abb[19] + abb[21];
z[59] = 2 * z[59];
z[60] = -abb[24] + z[59];
z[60] = abb[12] * z[60];
z[34] = z[34] + z[60];
z[60] = 2 * abb[0];
z[34] = z[34] * z[60];
z[11] = z[11] + z[32] + z[34] + z[53] + 2 * z[56];
z[11] = abb[14] * z[11];
z[32] = 12 * abb[19] + 5 * abb[21];
z[28] = -abb[22] + z[14] + z[28] + -z[32];
z[28] = z[28] * z[42];
z[34] = 3 * z[24] + -z[40];
z[53] = 4 * abb[18];
z[56] = -z[29] + z[34] + -z[53];
z[56] = abb[2] * z[56];
z[17] = z[17] + z[18];
z[18] = z[17] + -z[50];
z[21] = abb[18] + z[21];
z[50] = -z[18] + z[21];
z[50] = z[36] * z[50];
z[61] = 2 * z[52];
z[19] = abb[20] + z[8] + z[19];
z[19] = abb[5] * z[19];
z[28] = -z[19] + z[28] + z[50] + z[56] + -z[61];
z[50] = -abb[27] * z[28];
z[56] = abb[1] * z[48];
z[62] = -abb[3] * z[51];
z[63] = -abb[18] + abb[22];
z[64] = -abb[24] + z[33] + z[63];
z[64] = z[13] * z[64];
z[62] = z[52] + z[56] + z[62] + z[64];
z[62] = abb[11] * z[62];
z[48] = z[42] * z[48];
z[48] = z[48] + z[49];
z[45] = -z[45] + -z[48];
z[45] = abb[13] * z[45];
z[64] = abb[24] * (T(3) / T(2));
z[65] = z[59] + -z[64];
z[66] = abb[18] * (T(1) / T(2));
z[67] = abb[23] + z[31] + z[65] + -z[66];
z[68] = -abb[2] * z[67];
z[44] = 2 * z[44] + z[48] + z[68];
z[44] = abb[12] * z[44];
z[68] = z[37] + -z[64];
z[69] = abb[20] * (T(3) / T(2));
z[70] = -abb[23] + z[69];
z[71] = -z[66] + z[68] + -z[70];
z[72] = z[57] * z[71];
z[44] = z[44] + z[45] + z[62] + z[72];
z[44] = abb[11] * z[44];
z[14] = z[14] + z[23] + -z[34];
z[14] = abb[15] * z[14];
z[34] = abb[18] * (T(3) / T(2));
z[45] = -abb[22] + z[31];
z[62] = z[34] + z[45] + -z[68];
z[68] = z[10] * z[62];
z[62] = -abb[13] * z[62];
z[72] = -abb[12] * z[54];
z[62] = z[62] + z[72];
z[62] = abb[12] * z[62];
z[45] = z[45] + -z[66];
z[65] = z[45] + -z[65];
z[72] = abb[12] + -abb[13];
z[65] = z[65] * z[72];
z[46] = -abb[24] + z[46];
z[72] = -abb[11] * z[46];
z[65] = z[65] + z[72];
z[65] = abb[11] * z[65];
z[72] = 2 * z[54];
z[73] = abb[27] * z[72];
z[14] = z[14] + z[62] + z[65] + z[68] + z[73];
z[14] = abb[0] * z[14];
z[25] = -abb[24] + z[25];
z[62] = abb[21] * (T(1) / T(2));
z[65] = abb[19] + z[62];
z[68] = abb[20] * (T(-5) / T(2)) + z[16] + z[25] + z[65] + -z[66];
z[68] = abb[3] * z[68];
z[73] = abb[18] * (T(1) / T(6));
z[74] = abb[24] * (T(-8) / T(3)) + abb[23] * (T(13) / T(6)) + abb[22] * (T(17) / T(6)) + -z[31] + (T(1) / T(3)) * z[33] + z[73];
z[74] = abb[1] * z[74];
z[62] = abb[19] * (T(-2) / T(3)) + -z[62];
z[62] = abb[23] * (T(-3) / T(2)) + abb[20] * (T(2) / T(3)) + z[40] + 5 * z[62] + z[73];
z[62] = abb[2] * z[62];
z[31] = -z[31] + z[65];
z[31] = abb[22] * (T(-13) / T(6)) + abb[24] * (T(1) / T(3)) + 3 * z[31];
z[31] = abb[0] * z[31];
z[5] = -abb[23] + z[5];
z[65] = abb[19] + abb[20] * (T(-1) / T(3)) + (T(-2) / T(3)) * z[5];
z[65] = abb[5] * z[65];
z[31] = z[31] + (T(2) / T(3)) * z[52] + z[62] + 5 * z[65] + (T(1) / T(3)) * z[68] + z[74];
z[9] = z[9] * z[31];
z[6] = -abb[20] + -z[6] + z[18];
z[6] = z[6] * z[36];
z[18] = -3 * abb[20] + abb[23] + -z[27] + z[32] + -z[53];
z[18] = z[18] * z[42];
z[8] = abb[18] + 4 * abb[22] + z[8] + -z[15];
z[8] = abb[4] * z[8];
z[6] = z[6] + z[8] + z[18] + z[61];
z[6] = abb[15] * z[6];
z[8] = 2 * z[55];
z[15] = -z[8] + -z[48];
z[15] = abb[13] * z[15];
z[18] = abb[21] + z[38];
z[21] = abb[23] + -z[18] + z[21];
z[21] = abb[1] * z[21];
z[27] = abb[19] * (T(1) / T(2)) + -z[30] + z[70];
z[27] = abb[2] * z[27];
z[30] = abb[3] * z[54];
z[21] = z[21] + z[27] + z[30] + (T(-5) / T(2)) * z[55];
z[21] = abb[12] * z[21];
z[27] = z[57] * z[67];
z[15] = z[15] + z[21] + z[27];
z[15] = abb[12] * z[15];
z[21] = abb[9] * z[51];
z[27] = abb[8] * z[71];
z[30] = -abb[20] + abb[23] + -z[63];
z[30] = abb[7] * z[30];
z[21] = -z[21] + z[27] + z[30];
z[27] = -abb[28] * z[21];
z[30] = z[33] + -z[41];
z[30] = z[30] * z[36];
z[30] = z[30] + z[56];
z[30] = z[10] * z[30];
z[10] = -z[10] * z[71];
z[31] = abb[15] * z[43];
z[10] = z[10] + 2 * z[31];
z[10] = abb[2] * z[10];
z[0] = z[0] + z[6] + z[9] + z[10] + z[11] + z[14] + z[15] + z[27] + z[30] + z[44] + z[50];
z[6] = z[16] + z[17] + -z[26] + -z[40];
z[9] = z[6] * z[13];
z[10] = -z[24] + -z[29] + z[40];
z[10] = abb[2] * z[10];
z[11] = abb[18] + -z[24] + z[47];
z[11] = z[11] * z[36];
z[13] = 3 * abb[22] + z[25] + -z[39];
z[13] = z[13] * z[42];
z[14] = -abb[20] + -z[23] + z[59];
z[14] = z[14] * z[60];
z[5] = 13 * abb[19] + -5 * abb[20] + -8 * z[5];
z[5] = abb[5] * z[5];
z[5] = z[5] + z[9] + z[10] + z[11] + z[13] + z[14] + z[61];
z[5] = abb[14] * z[5];
z[9] = -z[18] + z[22] + -z[63];
z[9] = z[9] * z[42];
z[10] = abb[18] * (T(-5) / T(2)) + z[20] + -z[64] + z[70];
z[10] = abb[2] * z[10];
z[11] = abb[24] + -z[47] + z[72];
z[11] = abb[3] * z[11];
z[9] = z[9] + z[10] + z[11] + -z[19] + -z[52];
z[9] = abb[12] * z[9];
z[10] = abb[1] * z[46];
z[8] = -z[8] + -4 * z[10] + -z[49];
z[8] = abb[13] * z[8];
z[6] = -abb[4] * z[6];
z[10] = abb[3] + z[42];
z[10] = z[10] * z[46];
z[6] = z[6] + z[10] + -z[35] + z[55];
z[6] = z[6] * z[58];
z[10] = abb[24] * (T(1) / T(2));
z[11] = -abb[22] + -z[10] + z[37] + -z[66] + -z[69];
z[11] = abb[13] * z[11];
z[10] = z[10] + -z[59];
z[13] = z[10] + -z[45];
z[13] = abb[12] * z[13];
z[14] = -z[43] * z[58];
z[11] = z[11] + z[13] + z[14];
z[11] = abb[0] * z[11];
z[10] = -z[10] + -z[34] + z[70];
z[10] = z[10] * z[57];
z[5] = z[5] + z[6] + z[8] + z[9] + z[10] + z[11];
z[5] = m1_set::bc<T>[0] * z[5];
z[1] = abb[7] + z[1];
z[1] = 2 * z[1];
z[6] = -z[1] * z[2];
z[8] = abb[0] * z[72];
z[6] = z[6] + z[8] + -z[28];
z[6] = abb[16] * z[6];
z[8] = abb[7] + abb[8];
z[8] = z[8] * z[58];
z[7] = 2 * abb[7] + z[7];
z[7] = abb[12] * z[7];
z[1] = abb[14] * z[1];
z[1] = -z[1] + z[3] + (T(1) / T(2)) * z[4] + z[7] + -z[8];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = abb[17] * z[12];
z[1] = z[1] + -z[3];
z[1] = z[1] * z[2];
z[2] = -abb[17] * z[21];
z[1] = z[1] + z[2] + z[5] + z[6];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_320_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("16.774396578957533516169655943055954307778175816159455215988566903"),stof<T>("-26.110884686065693501306655796323847712003549036978211781147967509")}, std::complex<T>{stof<T>("-31.886856572184315835172538778320211070195049335755095618186338088"),stof<T>("-26.290912553205554295770364170619737260644214521801756044734270572")}, std::complex<T>{stof<T>("7.758075433140595933329982419555091062781637452711671316305447627"),stof<T>("24.109630109091705822758359150238232695991206228981906800238103449")}, std::complex<T>{stof<T>("-13.051344469422047704018234293616948521509536695786459245520030825"),stof<T>("-28.292167130179541974318660816705352276656557329798061025644134632")}, std::complex<T>{stof<T>("16.147328438486349828229591509236169108512553636489629036487637101"),stof<T>("21.002563390181037990731090261970791556060461416282829044788117307")}, std::complex<T>{stof<T>("26.367866057911321266540949745992967634308572349675502933865415749"),stof<T>("-20.785834666835619432162498403340823471551092686062229997136613617")}, std::complex<T>{stof<T>("-17.580404968155742354548820419759985521592944959640581442230364646"),stof<T>("14.037719203417061707875034479037692096073594299788730988996315471")}, std::complex<T>{stof<T>("-12.706830249422815026553822945632900427012151979369938324316701223"),stof<T>("18.504492816647903222941232676700919811913219805712734707446374667")}, std::complex<T>{stof<T>("-12.706830249422815026553822945632900427012151979369938324316701223"),stof<T>("18.504492816647903222941232676700919811913219805712734707446374667")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[127].real()/kbase.W[127].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_320_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_320_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-33.736319408592452351618705590900483679111583057196313202166435047"),stof<T>("58.670340331518170244610123791005614116393126657534399890448229205")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dlog_W3(k,dl), dlog_W7(k,dl), dlog_W10(k,dl), dlog_W13(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), dlr[2], f_1_1(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[127].real()/k.W[127].real()), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_320_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_320_DLogXconstant_part(base_point<T>, kend);
	value += f_4_320_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_320_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_320_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_320_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_320_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_320_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_320_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
