/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_116.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_116_abbreviated (const std::array<T,55>& abb) {
T z[58];
z[0] = abb[32] * (T(1) / T(2));
z[1] = -abb[30] + z[0];
z[2] = abb[31] * (T(1) / T(2));
z[3] = z[1] + z[2];
z[4] = -abb[22] + -abb[24];
z[3] = m1_set::bc<T>[0] * z[3] * z[4];
z[4] = abb[21] + abb[23];
z[5] = abb[31] + abb[32];
z[6] = -abb[29] + -abb[33] + z[5];
z[6] = m1_set::bc<T>[0] * z[6];
z[6] = -abb[39] + -abb[40] + z[6];
z[7] = -z[4] * z[6];
z[8] = -abb[29] + z[0];
z[9] = z[2] + z[8];
z[9] = m1_set::bc<T>[0] * z[9];
z[9] = -abb[39] + z[9];
z[10] = abb[20] * z[9];
z[11] = abb[41] * (T(1) / T(2));
z[12] = abb[26] * z[11];
z[5] = -abb[33] + (T(1) / T(2)) * z[5];
z[5] = m1_set::bc<T>[0] * z[5];
z[5] = -abb[40] + z[5];
z[13] = abb[25] * z[5];
z[3] = z[3] + z[7] + -z[10] + z[12] + -z[13];
z[7] = -abb[42] + -abb[43] + abb[44];
z[7] = (T(-1) / T(2)) * z[7];
z[3] = z[3] * z[7];
z[10] = abb[49] + abb[50];
z[12] = abb[32] * z[10];
z[13] = abb[31] * z[10];
z[14] = z[12] + z[13];
z[15] = abb[33] * z[10];
z[14] = (T(1) / T(2)) * z[14] + -z[15];
z[14] = m1_set::bc<T>[0] * z[14];
z[16] = abb[40] * z[10];
z[14] = z[14] + -z[16];
z[16] = abb[29] + -abb[30];
z[17] = m1_set::bc<T>[0] * z[16];
z[17] = abb[39] + z[17];
z[18] = abb[45] * z[17];
z[19] = abb[47] * z[5];
z[20] = abb[51] * z[11];
z[18] = z[14] + -z[18] + z[19] + -z[20];
z[19] = abb[7] * (T(1) / T(2));
z[21] = abb[4] * (T(1) / T(2));
z[22] = -z[19] + -z[21];
z[18] = z[18] * z[22];
z[22] = abb[13] * (T(1) / T(2));
z[9] = z[9] * z[22];
z[23] = abb[12] * z[17];
z[17] = abb[6] * z[17];
z[24] = abb[18] * abb[41];
z[9] = z[9] + (T(1) / T(2)) * z[17] + -z[23] + (T(-1) / T(4)) * z[24];
z[9] = abb[45] * z[9];
z[24] = abb[17] + -abb[19];
z[25] = abb[41] * z[24];
z[26] = abb[15] * z[5];
z[27] = abb[16] * z[11];
z[25] = (T(1) / T(2)) * z[25] + z[26] + z[27];
z[26] = abb[47] * (T(1) / T(2));
z[25] = z[25] * z[26];
z[11] = z[10] * z[11];
z[5] = -abb[51] * z[5];
z[27] = z[5] + z[11];
z[28] = abb[17] * (T(1) / T(2));
z[29] = abb[16] * (T(1) / T(2)) + z[28];
z[27] = z[27] * z[29];
z[17] = -z[17] + z[23];
z[23] = abb[46] + abb[48];
z[17] = -z[17] * z[23];
z[6] = -abb[8] * z[6];
z[29] = abb[30] + -abb[33];
z[29] = m1_set::bc<T>[0] * z[29];
z[29] = -abb[40] + z[29];
z[29] = abb[5] * z[29];
z[6] = z[6] + z[29];
z[29] = abb[45] * (T(1) / T(2));
z[6] = z[6] * z[29];
z[5] = z[5] + -z[11];
z[11] = abb[19] * (T(1) / T(2));
z[5] = z[5] * z[11];
z[14] = z[14] + z[20];
z[20] = abb[15] * (T(1) / T(2));
z[14] = z[14] * z[20];
z[30] = abb[27] * abb[51];
z[31] = -abb[41] * z[30];
z[3] = z[3] + z[5] + z[6] + z[9] + z[14] + z[17] + z[18] + z[25] + z[27] + z[31];
z[3] = (T(1) / T(8)) * z[3];
z[5] = -abb[29] + abb[32];
z[5] = abb[31] * z[5];
z[6] = prod_pow(abb[30], 2);
z[9] = (T(1) / T(2)) * z[6];
z[14] = abb[36] + z[9];
z[17] = abb[37] + z[14];
z[18] = abb[32] * abb[33];
z[5] = z[5] + z[17] + -z[18];
z[25] = abb[28] * (T(1) / T(2));
z[27] = -abb[33] + z[25];
z[31] = -z[16] + z[27];
z[31] = z[25] * z[31];
z[32] = abb[38] * (T(1) / T(2));
z[33] = abb[52] * (T(1) / T(2));
z[34] = abb[53] * (T(1) / T(2));
z[35] = prod_pow(m1_set::bc<T>[0], 2);
z[36] = (T(1) / T(3)) * z[35];
z[5] = (T(1) / T(2)) * z[5] + -z[31] + -z[32] + -z[33] + -z[34] + -z[36];
z[31] = abb[45] * z[5];
z[37] = -abb[30] + abb[29] * (T(1) / T(2));
z[37] = abb[29] * z[37];
z[14] = z[14] + z[37];
z[38] = (T(1) / T(6)) * z[35];
z[39] = z[14] + z[38];
z[40] = -abb[47] * z[39];
z[31] = z[31] + z[40];
z[31] = abb[8] * z[31];
z[40] = z[10] * z[25];
z[15] = -z[15] + z[40];
z[12] = z[12] + -z[13];
z[40] = (T(1) / T(2)) * z[12] + -z[15];
z[40] = abb[28] * z[40];
z[41] = z[0] * z[13];
z[40] = z[40] + z[41];
z[42] = abb[54] * (T(1) / T(2));
z[43] = abb[51] * z[42];
z[44] = abb[34] * z[10];
z[43] = z[43] + -z[44];
z[45] = abb[53] + z[36];
z[46] = z[18] + z[45];
z[46] = z[10] * z[46];
z[46] = -z[40] + -z[43] + z[46];
z[46] = z[20] * z[46];
z[47] = abb[31] * z[0];
z[47] = -abb[53] + z[47];
z[48] = -z[18] + z[47];
z[49] = abb[31] + -abb[32];
z[50] = z[27] + (T(1) / T(2)) * z[49];
z[51] = abb[28] * z[50];
z[52] = abb[34] + z[36] + z[51];
z[53] = z[48] + -z[52];
z[54] = abb[51] * z[53];
z[55] = z[10] * z[42];
z[54] = z[54] + z[55];
z[11] = z[11] * z[54];
z[48] = (T(-1) / T(2)) * z[35] + z[48] + -z[51];
z[48] = abb[51] * z[48];
z[48] = z[48] + -z[55];
z[28] = z[28] * z[48];
z[48] = z[10] * z[38];
z[51] = z[44] + -z[48];
z[51] = abb[2] * z[51];
z[54] = -abb[30] + z[25];
z[56] = abb[28] * z[54];
z[56] = abb[35] + -abb[38] + z[56];
z[17] = abb[34] + z[17] + z[56];
z[57] = -abb[47] + -z[29];
z[17] = abb[3] * z[17] * z[57];
z[30] = abb[54] * z[30];
z[11] = z[11] + z[17] + z[28] + z[30] + z[31] + z[46] + z[51];
z[17] = -z[0] * z[16];
z[28] = -z[25] * z[54];
z[30] = abb[35] + abb[37];
z[31] = (T(1) / T(12)) * z[35];
z[35] = -abb[30] + abb[29] * (T(3) / T(4));
z[35] = abb[29] * z[35];
z[17] = z[17] + z[28] + (T(-1) / T(2)) * z[30] + z[31] + z[32] + -z[33] + z[35];
z[17] = abb[45] * z[17];
z[28] = abb[31] * abb[32];
z[28] = (T(1) / T(4)) * z[28] + -z[34];
z[32] = -z[25] * z[50];
z[0] = -abb[33] * z[0];
z[0] = z[0] + z[14] + z[28] + -z[31] + z[32];
z[0] = abb[47] * z[0];
z[18] = -abb[53] + -z[18] + -z[38];
z[18] = z[10] * z[18];
z[18] = z[18] + z[40];
z[31] = z[23] * z[39];
z[32] = abb[54] * (T(1) / T(4));
z[33] = -abb[51] * z[32];
z[0] = z[0] + z[17] + (T(1) / T(2)) * z[18] + z[31] + z[33] + -z[44];
z[0] = z[0] * z[21];
z[4] = z[4] * z[5];
z[5] = abb[33] * (T(1) / T(2));
z[17] = z[5] + -z[49];
z[17] = abb[33] * z[17];
z[1] = abb[31] * z[1];
z[9] = abb[37] + z[9];
z[1] = z[1] + z[9] + -z[17] + -z[52];
z[1] = abb[24] * z[1];
z[18] = abb[25] * z[53];
z[2] = -z[2] + z[8] + z[25];
z[2] = abb[28] * z[2];
z[8] = abb[31] * z[8];
z[2] = abb[35] + z[2] + -z[8] + z[36];
z[8] = abb[52] + z[2];
z[21] = abb[20] * z[8];
z[31] = abb[26] * z[42];
z[1] = z[1] + z[18] + -z[21] + -z[31];
z[18] = prod_pow(abb[29], 2);
z[21] = -z[6] + z[18];
z[31] = abb[32] * z[16];
z[33] = abb[36] + z[31];
z[21] = (T(1) / T(2)) * z[21] + -z[33];
z[2] = z[2] + z[21];
z[34] = abb[22] * (T(1) / T(2));
z[2] = z[2] * z[34];
z[1] = (T(1) / T(2)) * z[1] + -z[2] + z[4];
z[1] = z[1] * z[7];
z[2] = abb[34] + z[17];
z[4] = abb[31] * z[16];
z[7] = abb[29] + -abb[33] + z[49];
z[7] = abb[28] * z[7];
z[7] = z[2] + -z[4] + z[7] + (T(-1) / T(2)) * z[18] + -z[30] + z[33];
z[7] = abb[14] * z[7];
z[17] = -abb[52] + z[21];
z[17] = abb[6] * z[17];
z[7] = z[7] + z[17];
z[18] = abb[29] * z[16];
z[18] = z[18] + z[38];
z[16] = abb[28] * z[16];
z[4] = -abb[38] + -z[4] + z[16];
z[16] = -z[4] + z[18] + -z[33];
z[16] = abb[1] * z[16];
z[8] = z[8] * z[22];
z[21] = abb[37] + -z[37] + z[56];
z[21] = abb[9] * z[21];
z[18] = -abb[52] + z[18] + -z[31];
z[18] = abb[12] * z[18];
z[22] = abb[18] * z[32];
z[7] = (T(1) / T(2)) * z[7] + z[8] + z[16] + -z[18] + z[21] + z[22];
z[7] = z[7] * z[29];
z[8] = -z[10] * z[45];
z[10] = z[5] * z[10];
z[13] = z[10] + -z[13];
z[13] = abb[33] * z[13];
z[22] = -z[12] * z[25];
z[8] = z[8] + z[13] + z[22] + z[41] + -z[43];
z[13] = -abb[31] + z[5];
z[5] = z[5] * z[13];
z[22] = abb[28] * z[49];
z[5] = abb[34] * (T(-1) / T(2)) + z[5] + (T(1) / T(4)) * z[22] + z[28] + -z[39];
z[5] = abb[47] * z[5];
z[22] = abb[36] + -abb[52] + z[4];
z[22] = z[22] * z[29];
z[5] = z[5] + (T(1) / T(2)) * z[8] + z[22];
z[5] = z[5] * z[19];
z[8] = z[25] * z[49];
z[13] = abb[33] * z[13];
z[8] = abb[34] + z[8] + z[13] + -z[36] + z[47];
z[8] = abb[51] * z[8];
z[8] = z[8] + -z[55];
z[8] = abb[16] * z[8];
z[10] = z[10] + z[12];
z[10] = abb[33] * z[10];
z[12] = -z[12] + z[15];
z[12] = abb[28] * z[12];
z[10] = z[10] + z[12];
z[12] = z[27] + z[49];
z[12] = abb[28] * z[12];
z[2] = z[2] + z[12] + z[38];
z[12] = -abb[45] + -abb[47];
z[2] = z[2] * z[12];
z[2] = z[2] + z[10] + z[44] + z[48];
z[2] = abb[0] * z[2];
z[12] = -abb[30] * abb[31];
z[9] = abb[53] + z[9] + z[12] + -z[13];
z[9] = abb[5] * abb[45] * z[9];
z[2] = z[2] + z[8] + z[9];
z[8] = abb[29] * abb[30];
z[4] = abb[34] + 3 * abb[36] + z[4] + z[6] + -z[8] + z[31];
z[4] = abb[11] * z[4];
z[4] = z[4] + z[16];
z[6] = abb[34] + z[14];
z[6] = abb[14] * z[6];
z[8] = -abb[34] + z[38];
z[8] = abb[2] * z[8];
z[6] = -z[4] + z[6] + z[8] + -z[17] + z[18];
z[8] = (T(-1) / T(2)) * z[23];
z[6] = z[6] * z[8];
z[8] = -z[20] * z[53];
z[9] = -abb[54] * z[24];
z[12] = -abb[16] * z[32];
z[4] = z[4] + z[8] + (T(1) / T(4)) * z[9] + z[12] + z[21];
z[4] = z[4] * z[26];
z[8] = (T(-1) / T(2)) * z[10] + -z[44];
z[8] = abb[10] * z[8];
z[0] = z[0] + z[1] + (T(1) / T(4)) * z[2] + z[4] + z[5] + z[6] + z[7] + z[8] + (T(1) / T(2)) * z[11];
z[0] = (T(1) / T(4)) * z[0];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_116_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("-0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("-0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("-0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("-0.0932362718418076907585148730705542576290566485887856703148526402"),stof<T>("0.13139428364384790928686988062281701970184604660285561586017104707")}, std::complex<T>{stof<T>("0.15857841537533631273724318157876290766383516510697648013934649957"),stof<T>("0.78962621475086254715007802603495023153910242888041274580055308638")}, std::complex<T>{stof<T>("0.05879978563572713186183200744216747796292413936160178333583300988"),stof<T>("0.32298204875312312682787828194897286668984675589463653848067227191")}, std::complex<T>{stof<T>("0.15857841537533631273724318157876290766383516510697648013934649957"),stof<T>("0.78962621475086254715007802603495023153910242888041274580055308638")}, std::complex<T>{stof<T>("0.04096978247778764887725391088736150103624356733802015380899415234"),stof<T>("0.32298204875312312682787828194897286668984675589463653848067227191")}, std::complex<T>{stof<T>("0.04096978247778764887725391088736150103624356733802015380899415234"),stof<T>("0.32298204875312312682787828194897286668984675589463653848067227191")}, std::complex<T>{stof<T>("-0.10616851963873196397097676345843907373564737355384419620189994362"),stof<T>("0.15389981359476749750044889295661157415145599188157372634275060798")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[28].real()/kbase.W[28].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_116_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_116_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.13305680338994742963069498214053208883208019363135258179383488028"),stof<T>("-0.19866858098008036264840377005922946980291070087594261951747322104")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,55> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[28].real()/k.W[28].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_116_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_116_DLogXconstant_part(base_point<T>, kend);
	value += f_4_116_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_116_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_116_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_116_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_116_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_116_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_116_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
