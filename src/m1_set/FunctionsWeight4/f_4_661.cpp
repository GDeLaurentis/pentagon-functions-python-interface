/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_661.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_661_abbreviated (const std::array<T,63>& abb) {
T z[112];
z[0] = abb[47] * (T(1) / T(2));
z[1] = abb[46] * (T(1) / T(2));
z[2] = z[0] + z[1];
z[3] = abb[48] * (T(1) / T(2));
z[4] = z[2] + z[3];
z[5] = abb[51] * (T(1) / T(2));
z[6] = abb[45] * (T(1) / T(2)) + z[5];
z[7] = -abb[49] + z[6];
z[8] = abb[52] * (T(1) / T(2));
z[9] = -z[4] + z[7] + z[8];
z[10] = abb[7] * z[9];
z[11] = abb[20] + abb[22];
z[12] = abb[54] + -abb[55];
z[13] = (T(1) / T(2)) * z[12];
z[14] = z[11] * z[13];
z[15] = -z[10] + z[14];
z[16] = -abb[53] + abb[49] * (T(1) / T(2)) + z[6];
z[17] = -z[8] + z[16];
z[18] = 3 * abb[9];
z[17] = z[17] * z[18];
z[19] = (T(3) / T(2)) * z[15] + z[17];
z[20] = 3 * abb[49];
z[21] = 2 * abb[53];
z[22] = -abb[47] + z[21];
z[23] = z[20] + -z[22];
z[24] = abb[46] + abb[48];
z[25] = 2 * abb[52];
z[26] = -z[23] + -z[24] + z[25];
z[27] = 2 * abb[13];
z[26] = z[26] * z[27];
z[27] = abb[52] * (T(5) / T(4));
z[28] = abb[47] * (T(1) / T(4));
z[29] = abb[48] * (T(3) / T(4)) + -z[28];
z[30] = z[27] + -z[29];
z[31] = abb[51] * (T(3) / T(4));
z[32] = abb[45] * (T(3) / T(4));
z[33] = z[31] + z[32];
z[34] = -abb[53] + z[33];
z[35] = abb[46] * (T(1) / T(4));
z[36] = z[34] + -z[35];
z[37] = abb[44] + -z[30] + z[36];
z[38] = -abb[5] * z[37];
z[39] = abb[51] * (T(1) / T(4));
z[40] = abb[52] * (T(1) / T(4));
z[29] = -z[29] + z[39] + z[40];
z[41] = abb[46] * (T(3) / T(4));
z[42] = -abb[53] + abb[45] * (T(5) / T(4)) + z[29] + -z[41];
z[43] = 3 * abb[6];
z[44] = -z[42] * z[43];
z[23] = -abb[52] + z[23];
z[45] = abb[44] + -abb[46];
z[46] = -z[23] + z[45];
z[46] = abb[1] * z[46];
z[36] = -z[28] + z[36];
z[47] = 2 * abb[44];
z[48] = abb[52] * (T(-7) / T(4)) + abb[48] * (T(9) / T(4)) + -z[36] + z[47];
z[48] = abb[4] * z[48];
z[38] = -z[19] + -z[26] + z[38] + z[44] + z[46] + z[48];
z[38] = abb[30] * z[38];
z[44] = abb[51] * (T(3) / T(2));
z[46] = -z[21] + z[44];
z[48] = abb[45] * (T(3) / T(2));
z[49] = z[46] + z[48];
z[50] = abb[48] * (T(3) / T(2));
z[51] = z[47] + z[50];
z[52] = abb[52] * (T(5) / T(2));
z[53] = -z[2] + z[49] + z[51] + -z[52];
z[54] = abb[5] * z[53];
z[55] = abb[19] * z[13];
z[55] = z[10] + z[55];
z[56] = -abb[48] + abb[52];
z[57] = abb[47] + -abb[51];
z[58] = abb[45] + z[57];
z[59] = -z[56] + z[58];
z[60] = -abb[46] + z[59];
z[61] = abb[44] + (T(1) / T(2)) * z[60];
z[62] = abb[8] * z[61];
z[63] = abb[21] * z[13];
z[63] = z[62] + z[63];
z[64] = -z[55] + -z[63];
z[65] = abb[46] * (T(5) / T(2));
z[48] = abb[48] * (T(5) / T(2)) + -z[44] + -z[48] + z[65];
z[66] = abb[47] * (T(5) / T(2));
z[67] = abb[53] + -z[20];
z[67] = abb[52] * (T(7) / T(2)) + -z[48] + -z[66] + 2 * z[67];
z[67] = abb[13] * z[67];
z[68] = 3 * abb[51];
z[69] = abb[52] + z[21];
z[70] = -2 * abb[47] + z[68] + -z[69];
z[71] = -z[45] + z[70];
z[71] = abb[1] * z[71];
z[72] = abb[45] + abb[49] + abb[51];
z[69] = -z[69] + z[72];
z[73] = z[18] * z[69];
z[74] = (T(3) / T(2)) * z[12];
z[75] = abb[24] * z[74];
z[54] = z[54] + 3 * z[64] + z[67] + -z[71] + z[73] + -z[75];
z[54] = abb[31] * z[54];
z[49] = -z[4] + -z[8] + z[49];
z[64] = abb[5] * z[49];
z[67] = abb[4] * z[49];
z[26] = 3 * z[15] + z[26] + z[64] + z[67] + z[73];
z[76] = -abb[26] * z[26];
z[77] = z[0] + z[8];
z[78] = abb[46] * (T(3) / T(2));
z[5] = abb[45] * (T(5) / T(2)) + z[5] + -z[21] + -z[50] + z[77] + -z[78];
z[50] = 3 * z[5];
z[79] = abb[3] * z[50];
z[80] = abb[49] + -abb[53];
z[6] = z[6] + 2 * z[80];
z[81] = abb[52] * (T(3) / T(2));
z[4] = z[4] + z[6] + -z[81];
z[4] = abb[13] * z[4];
z[4] = z[4] + -z[15];
z[15] = abb[24] * z[13];
z[82] = z[4] + -z[15];
z[69] = abb[9] * z[69];
z[69] = z[69] + -z[82];
z[69] = 3 * z[69] + -z[79];
z[69] = abb[28] * z[69];
z[50] = abb[28] * z[50];
z[53] = abb[31] * z[53];
z[53] = z[50] + z[53];
z[53] = abb[4] * z[53];
z[83] = -abb[26] * z[49];
z[50] = z[50] + z[83];
z[50] = abb[6] * z[50];
z[83] = abb[13] * z[49];
z[83] = -z[75] + z[83];
z[84] = z[79] + z[83];
z[85] = 2 * abb[48];
z[86] = -abb[52] + z[22] + z[85];
z[87] = 2 * abb[46];
z[88] = 3 * abb[45] + -z[86] + -z[87];
z[89] = 2 * z[88];
z[90] = -abb[4] + -abb[6];
z[90] = z[89] * z[90];
z[64] = z[64] + z[84] + z[90];
z[64] = abb[27] * z[64];
z[38] = z[38] + z[50] + z[53] + z[54] + z[64] + z[69] + z[76];
z[38] = abb[30] * z[38];
z[50] = abb[47] + abb[52];
z[53] = -abb[43] + abb[50];
z[54] = abb[45] * (T(1) / T(4));
z[64] = abb[53] * (T(1) / T(3));
z[31] = abb[49] + abb[48] * (T(-13) / T(12)) + abb[44] * (T(-1) / T(3)) + -z[31] + -z[41] + (T(11) / T(12)) * z[50] + (T(2) / T(3)) * z[53] + z[54] + -z[64];
z[31] = abb[6] * z[31];
z[50] = abb[46] + abb[47];
z[39] = abb[52] * (T(-3) / T(4)) + abb[44] * (T(4) / T(3)) + abb[48] * (T(11) / T(12)) + -z[32] + z[39] + (T(-5) / T(12)) * z[50] + z[53] + z[64];
z[39] = abb[5] * z[39];
z[11] = z[11] * z[12];
z[69] = abb[19] * z[12];
z[11] = z[11] + z[69];
z[76] = (T(3) / T(2)) * z[11];
z[90] = abb[21] * z[12];
z[12] = abb[23] * z[12];
z[62] = z[12] + -5 * z[62] + -z[76] + (T(-5) / T(2)) * z[90];
z[7] = z[3] + z[7];
z[2] = z[2] + z[8];
z[91] = abb[44] + -z[2] + z[7];
z[92] = abb[14] * z[91];
z[1] = -z[1] + z[53];
z[59] = -z[1] + (T(1) / T(2)) * z[59];
z[93] = abb[12] * z[59];
z[94] = abb[44] + abb[46];
z[64] = 2 * abb[49] + abb[51] * (T(-5) / T(2)) + abb[48] * (T(-1) / T(3)) + abb[47] * (T(7) / T(3)) + z[8] + z[64] + (T(-1) / T(6)) * z[94];
z[64] = abb[1] * z[64];
z[94] = abb[44] * (T(2) / T(3));
z[57] = abb[49] + z[57];
z[95] = (T(2) / T(3)) * z[56] + -z[57] + -z[94];
z[95] = abb[0] * z[95];
z[96] = -abb[3] * z[42];
z[97] = -abb[46] + z[56];
z[98] = z[53] + z[97];
z[99] = abb[2] * z[98];
z[22] = abb[52] + z[22];
z[22] = abb[45] + abb[46] * (T(-2) / T(3)) + (T(-1) / T(3)) * z[22] + z[94];
z[22] = abb[4] * z[22];
z[22] = z[22] + z[31] + z[39] + (T(1) / T(2)) * z[62] + z[64] + z[92] + z[93] + z[95] + z[96] + (T(1) / T(3)) * z[99];
z[22] = prod_pow(m1_set::bc<T>[0], 2) * z[22];
z[31] = abb[49] * (T(3) / T(2));
z[39] = abb[44] * (T(1) / T(2));
z[46] = -z[3] + z[31] + -z[39] + z[46] + z[53] + -z[77];
z[46] = abb[0] * z[46];
z[62] = (T(3) / T(2)) * z[93];
z[64] = 2 * z[99];
z[94] = z[62] + z[64];
z[46] = z[46] + z[94];
z[95] = (T(-1) / T(2)) * z[53];
z[27] = abb[47] * (T(5) / T(4)) + z[27] + -z[95];
z[96] = abb[48] * (T(1) / T(4));
z[32] = abb[51] * (T(9) / T(4)) + -z[21] + -z[27] + z[32] + -z[39] + z[41] + z[96];
z[32] = abb[5] * z[32];
z[100] = abb[44] + abb[48];
z[72] = z[21] + -z[72] + z[100];
z[72] = z[18] * z[72];
z[72] = z[72] + -z[83];
z[101] = (T(3) / T(2)) * z[92];
z[102] = z[11] + z[12];
z[102] = (T(3) / T(4)) * z[102];
z[86] = -z[20] + z[86];
z[103] = 3 * abb[44] + -abb[46] + z[86];
z[104] = abb[1] * z[103];
z[32] = z[32] + -z[46] + -z[72] + -z[101] + -z[102] + 2 * z[104];
z[32] = abb[26] * z[32];
z[86] = z[47] + -z[53] + z[86];
z[105] = abb[0] * z[86];
z[106] = z[45] + z[53];
z[107] = abb[6] * z[106];
z[106] = abb[5] * z[106];
z[108] = -abb[4] * z[88];
z[105] = z[99] + -z[104] + z[105] + z[106] + -2 * z[107] + z[108];
z[105] = abb[27] * z[105];
z[84] = -abb[28] * z[84];
z[74] = abb[23] * z[74];
z[72] = -z[72] + -z[74] + 3 * z[104];
z[72] = abb[31] * z[72];
z[74] = abb[31] * z[49];
z[106] = abb[28] * z[49];
z[108] = z[74] + z[106];
z[109] = -abb[5] * z[108];
z[103] = abb[31] * z[103];
z[88] = abb[28] * z[88];
z[88] = z[88] + -z[103];
z[33] = z[33] + z[41];
z[110] = 4 * abb[53];
z[27] = 4 * abb[44] + abb[49] * (T(-9) / T(2)) + abb[48] * (T(13) / T(4)) + -z[27] + -z[33] + z[110];
z[27] = abb[26] * z[27];
z[27] = z[27] + 2 * z[88];
z[27] = abb[6] * z[27];
z[88] = 3 * z[92];
z[111] = abb[31] * z[88];
z[89] = abb[28] * z[89];
z[89] = -z[74] + z[89];
z[89] = abb[4] * z[89];
z[27] = z[27] + z[32] + -z[72] + z[84] + z[89] + z[105] + z[109] + z[111];
z[27] = abb[27] * z[27];
z[3] = abb[53] + z[3] + -z[39] + -z[44] + z[50] + (T(-3) / T(2)) * z[53];
z[3] = abb[5] * z[3];
z[32] = z[101] + -z[104];
z[39] = abb[23] * z[13];
z[50] = z[39] + -z[55];
z[36] = -z[36] + z[40] + z[96];
z[36] = abb[4] * z[36];
z[84] = 3 * z[57];
z[89] = -abb[44] + z[56];
z[96] = z[84] + -z[89];
z[105] = abb[0] * (T(1) / T(2));
z[109] = z[96] * z[105];
z[3] = z[3] + -z[17] + z[32] + z[36] + (T(3) / T(2)) * z[50] + -z[62] + -z[99] + z[109];
z[3] = abb[26] * z[3];
z[17] = 3 * z[93];
z[36] = z[53] + z[70];
z[36] = abb[0] * z[36];
z[36] = z[17] + z[36] + z[76] + 4 * z[99];
z[50] = z[36] + -z[83];
z[50] = abb[28] * z[50];
z[62] = z[50] + -z[111];
z[70] = abb[4] * z[108];
z[76] = abb[28] * z[98];
z[74] = z[74] + 2 * z[76];
z[74] = abb[5] * z[74];
z[3] = z[3] + z[62] + z[70] + z[72] + z[74];
z[3] = abb[26] * z[3];
z[70] = z[56] + z[58];
z[65] = -z[53] + z[65];
z[72] = abb[44] + -z[65] + (T(3) / T(2)) * z[70];
z[74] = abb[5] * (T(1) / T(2));
z[72] = z[72] * z[74];
z[7] = z[7] + -z[77];
z[65] = 3 * z[7] + -z[65];
z[65] = z[47] + (T(1) / T(2)) * z[65];
z[65] = abb[6] * z[65];
z[12] = -z[11] + z[12];
z[46] = (T(3) / T(4)) * z[12] + z[32] + -z[46] + -z[65] + -z[67] + -z[72];
z[46] = abb[27] * z[46];
z[11] = (T(1) / T(2)) * z[11] + z[63];
z[67] = 3 * z[11] + z[71] + -z[83];
z[71] = abb[6] * z[49];
z[72] = 2 * z[89];
z[98] = abb[5] * z[72];
z[109] = abb[4] * z[72];
z[71] = z[67] + z[71] + z[98] + z[109];
z[71] = abb[30] * z[71];
z[72] = abb[31] * z[72];
z[72] = z[72] + z[106];
z[72] = abb[4] * z[72];
z[98] = abb[31] * z[89];
z[76] = z[76] + z[98];
z[98] = 2 * abb[5];
z[76] = z[76] * z[98];
z[46] = z[46] + -z[71] + z[72] + z[76];
z[71] = z[84] + z[89];
z[71] = z[71] * z[105];
z[71] = z[71] + -z[94];
z[72] = -3 * z[53] + z[78];
z[76] = 3 * z[58];
z[78] = 5 * z[56] + -z[76];
z[78] = -abb[44] + -z[72] + (T(1) / T(2)) * z[78];
z[74] = z[74] * z[78];
z[32] = z[32] + -z[71] + z[74] + -z[83] + z[102];
z[32] = abb[26] * z[32];
z[31] = z[31] + -z[47] + z[95];
z[28] = abb[48] * (T(-5) / T(4)) + -z[21] + z[28] + z[31] + z[33] + z[40];
z[28] = abb[26] * z[28];
z[28] = z[28] + -z[108];
z[28] = abb[6] * z[28];
z[33] = -abb[31] * z[67];
z[40] = abb[0] + -abb[1] + abb[4];
z[40] = z[40] * z[89];
z[40] = z[40] + -z[99] + z[107];
z[40] = abb[29] * z[40];
z[28] = z[28] + z[32] + z[33] + z[40] + -z[46] + -z[50];
z[28] = abb[29] * z[28];
z[0] = -z[0] + -z[21] + -z[48] + 2 * z[53] + z[81];
z[0] = abb[5] * z[0];
z[0] = z[0] + z[36] + z[79];
z[0] = abb[34] * z[0];
z[32] = abb[33] * z[5];
z[33] = prod_pow(abb[28], 2);
z[5] = z[5] * z[33];
z[5] = z[5] + z[32];
z[5] = abb[3] * z[5];
z[9] = abb[13] * z[9];
z[9] = z[9] + -z[55];
z[15] = z[9] + -z[15];
z[36] = abb[1] * z[57];
z[36] = z[36] + -z[63];
z[40] = z[15] + z[36];
z[40] = abb[58] * z[40];
z[48] = -abb[9] * z[89];
z[48] = z[39] + z[48] + -z[82] + -z[104];
z[48] = abb[32] * z[48];
z[50] = abb[32] + abb[57];
z[50] = z[50] * z[92];
z[5] = z[5] + z[40] + z[48] + z[50];
z[20] = abb[52] + z[20];
z[40] = -5 * abb[44] + -abb[47] + -3 * abb[48] + z[20] + z[68] + z[87] + -z[110];
z[40] = abb[1] * z[40];
z[10] = z[10] + z[14] + z[69];
z[14] = (T(1) / T(2)) * z[10] + z[63];
z[8] = -z[8] + -z[16] + z[100];
z[8] = z[8] * z[18];
z[8] = z[8] + 3 * z[14] + z[40] + -z[83];
z[14] = prod_pow(abb[31], 2);
z[8] = z[8] * z[14];
z[7] = z[1] + -z[7];
z[7] = abb[15] * z[7];
z[16] = abb[17] * z[59];
z[40] = abb[18] * z[91];
z[1] = -abb[44] + -z[1] + (T(1) / T(2)) * z[70];
z[48] = abb[16] * z[1];
z[13] = abb[25] * z[13];
z[7] = z[7] + z[13] + z[16] + z[40] + -z[48];
z[13] = abb[59] * z[7];
z[16] = abb[45] * (T(-9) / T(2)) + z[21] + (T(7) / T(2)) * z[24] + z[44] + -z[52] + -z[66];
z[16] = abb[34] * z[16];
z[24] = z[33] * z[42];
z[24] = z[24] + z[32];
z[32] = z[58] + z[97];
z[32] = (T(1) / T(2)) * z[32];
z[40] = -abb[58] * z[32];
z[42] = abb[57] * z[61];
z[40] = -z[24] + z[40] + z[42];
z[37] = z[14] * z[37];
z[40] = z[16] + -z[37] + 3 * z[40];
z[40] = abb[4] * z[40];
z[26] = -abb[35] * z[26];
z[29] = -abb[44] + z[29] + z[35] + z[54] + z[80];
z[14] = z[14] * z[29];
z[2] = z[2] + z[6] + -z[51];
z[2] = abb[32] * z[2];
z[6] = -abb[57] * z[91];
z[2] = z[2] + z[6] + z[14] + -z[24];
z[6] = abb[46] + -abb[48] + -abb[53] + z[31] + z[77];
z[6] = abb[26] * z[6];
z[6] = z[6] + 2 * z[103] + z[106];
z[6] = abb[26] * z[6];
z[14] = -abb[35] * z[49];
z[2] = 3 * z[2] + z[6] + z[14] + z[16];
z[2] = abb[6] * z[2];
z[6] = z[23] + z[53];
z[6] = abb[0] * z[6];
z[6] = z[6] + -z[19] + z[83] + z[99];
z[6] = z[6] * z[33];
z[14] = -z[30] + z[34] + z[41] + -z[53];
z[14] = z[14] * z[33];
z[16] = abb[58] * z[61];
z[19] = -abb[57] * z[32];
z[16] = z[16] + z[19];
z[14] = z[14] + 3 * z[16] + -z[37];
z[14] = abb[5] * z[14];
z[16] = abb[5] * z[59];
z[19] = abb[0] * z[57];
z[15] = z[15] + z[16] + z[19] + -z[93] + -z[99];
z[15] = 3 * z[15];
z[16] = -abb[56] * z[15];
z[4] = 3 * z[4] + -z[73] + -z[75];
z[4] = abb[33] * z[4];
z[19] = -abb[62] * z[49];
z[23] = z[57] + z[89];
z[23] = abb[1] * z[23];
z[11] = -z[11] + z[23] + z[39];
z[11] = 3 * z[11];
z[23] = abb[57] * z[11];
z[0] = abb[60] + abb[61] + z[0] + z[2] + z[3] + z[4] + 3 * z[5] + z[6] + z[8] + (T(3) / T(2)) * z[13] + z[14] + z[16] + z[19] + z[22] + z[23] + z[26] + z[27] + z[28] + z[38] + z[40];
z[2] = z[18] * z[89];
z[2] = z[2] + z[83];
z[3] = z[56] + -z[76];
z[3] = abb[44] + (T(1) / T(2)) * z[3] + -z[72];
z[3] = abb[5] * z[3];
z[4] = -abb[0] * z[96];
z[3] = -z[2] + z[3] + z[4] + z[17] + 3 * z[55] + z[64] + -z[104];
z[3] = abb[26] * z[3];
z[4] = -z[25] + z[47] + -z[84] + z[85];
z[4] = abb[1] * z[4];
z[5] = (T(-1) / T(4)) * z[12] + z[63];
z[1] = abb[5] * z[1];
z[1] = (T(3) / T(2)) * z[1] + z[4] + 3 * z[5] + z[65] + z[71] + -z[101] + z[109];
z[1] = abb[29] * z[1];
z[4] = -z[47] + -z[60];
z[4] = abb[8] * z[4];
z[4] = z[4] + -z[10] + z[39] + -z[90];
z[5] = 5 * abb[47] + -6 * abb[51] + z[20] + z[21] + z[45];
z[5] = abb[1] * z[5];
z[2] = z[2] + 3 * z[4] + z[5];
z[2] = abb[31] * z[2];
z[4] = -abb[26] * z[86];
z[4] = z[4] + -z[108];
z[4] = abb[6] * z[4];
z[1] = z[1] + z[2] + z[3] + z[4] + -z[46] + -z[62];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = (T(3) / T(2)) * z[7];
z[2] = abb[39] * z[2];
z[3] = -z[43] * z[91];
z[3] = z[3] + z[11] + z[88];
z[3] = abb[37] * z[3];
z[4] = -abb[36] * z[15];
z[5] = abb[38] * z[61];
z[6] = -abb[37] * z[32];
z[5] = z[5] + z[6];
z[5] = abb[5] * z[5];
z[6] = -abb[38] * z[32];
z[7] = abb[37] * z[61];
z[6] = z[6] + z[7];
z[6] = abb[4] * z[6];
z[5] = z[5] + z[6];
z[6] = z[9] + z[36];
z[6] = 3 * z[6] + -z[75];
z[6] = abb[38] * z[6];
z[7] = -abb[42] * z[49];
z[1] = abb[40] + abb[41] + z[1] + z[2] + z[3] + z[4] + 3 * z[5] + z[6] + z[7];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_661_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-1.9199712708610032646570607360206978943387699704876487992275079596"),stof<T>("-2.3683656031641206184443785616499261670486199736651258493215837874")}, std::complex<T>{stof<T>("-1.926546076142315960506474136442282937262294152631277069540486659"),stof<T>("-15.797566346568351308716995489891197084094153174661371118569168943")}, std::complex<T>{stof<T>("2.728986533072813935965881254948291681419501868887366728433945348"),stof<T>("13.2476544035186577448289863716499810925997384570373333673487691944")}, std::complex<T>{stof<T>("3.5756564364446030331447494388019930518598719780176358625104872301"),stof<T>("-3.2101481916672582729114674947237806255119606920142127017833134406")}, std::complex<T>{stof<T>("0.8090152622118106713088205189275937870807318983997179292064373884"),stof<T>("10.879288800354537126384607810000054925551118483372207518027185407")}, std::complex<T>{stof<T>("7.1070834264479149445700378122562934774365815631610922498378912673"),stof<T>("7.4875942688017059080295097586849844755933630473990829143450560048")}, std::complex<T>{stof<T>("-2.7732159795141050576853423202959843077026642617615462036170285409"),stof<T>("0.6602362486175647090234583764825646340175459743901749505629136915")}, std::complex<T>{stof<T>("-1.6114557191423086467682276374336025312379396146558075880998960776"),stof<T>("-8.329376857304843562496598691758838934056703765748169766806785658")}, std::complex<T>{stof<T>("-1.6048809138609959509188142370120174883144154325121793177869173784"),stof<T>("5.0998238860993871277760182364824319829888294352480755024407994981")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(abs(k.W[77])) - rlog(abs(kbase.W[77])), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_661_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_661_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (-v[3] + v[5]) * (-8 + 12 * v[0] + -4 * v[1] + v[3] + -5 * v[5] + 2 * m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = (-(1 + m1_set::bc<T>[1]) * (-1 * v[3] + v[5])) / tend;


		return (abb[43] + 3 * abb[45] + -abb[46] + abb[47] + -abb[48] + -abb[50] + -2 * abb[53]) * (2 * t * c[0] + -c[1]) * (T(1) / T(2));
	}
	{
T z[3];
z[0] = abb[27] + -abb[28];
z[1] = -abb[26] + abb[29];
z[0] = z[0] * z[1];
z[1] = -prod_pow(abb[27], 2);
z[2] = prod_pow(abb[28], 2);
z[0] = abb[34] + z[0] + z[1] + z[2];
z[1] = -abb[43] + -3 * abb[45] + abb[46] + -abb[47] + abb[48] + abb[50] + 2 * abb[53];
return abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_661_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(8)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (-v[3] + v[5])) / tend;


		return (abb[43] + 3 * abb[45] + -abb[46] + abb[47] + -abb[48] + -abb[50] + -2 * abb[53]) * (2 * t * c[0] + -c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = abb[43] + 3 * abb[45] + -abb[46] + abb[47] + -abb[48] + -abb[50] + -2 * abb[53];
z[1] = -abb[27] + abb[28];
return abb[10] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_661_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(-3) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (v[2] + v[3])) / tend;


		return (abb[45] + -abb[46] + -abb[48] + -abb[49] + abb[52]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -prod_pow(abb[30], 2);
z[1] = prod_pow(abb[28], 2);
z[0] = z[0] + z[1];
z[1] = -abb[45] + abb[46] + abb[48] + abb[49] + -abb[52];
return 3 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_661_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_661_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("2.7324371176930167648795330290797422262601471755726559175486423585"),stof<T>("-3.2220739863461210789907795984768614188744419325826183336426377993")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 18, 77});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,63> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W16(k,dl), dlog_W19(k,dl), dlog_W28(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_4(k), f_1_7(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_16(k), f_2_19(k), f_2_4_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), T{0}, T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[12].real()/k.W[12].real()), IntDLogL_W_16(t,k,kend,dl), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(abs(kend.W[77])) - rlog(abs(k.W[77])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_4_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k), T{0}, T{0}, T{0}};
abb[40] = SpDLog_f_4_661_W_16_Im(t, path, abb);
abb[41] = SpDLog_f_4_661_W_19_Im(t, path, abb);
abb[42] = SpDLogQ_W_78(k,dl,dlr).imag();
abb[60] = SpDLog_f_4_661_W_16_Re(t, path, abb);
abb[61] = SpDLog_f_4_661_W_19_Re(t, path, abb);
abb[62] = SpDLogQ_W_78(k,dl,dlr).real();

                    
            return f_4_661_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_661_DLogXconstant_part(base_point<T>, kend);
	value += f_4_661_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_661_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_661_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_661_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_661_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_661_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_661_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
