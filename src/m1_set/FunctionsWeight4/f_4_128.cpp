/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_128.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_128_abbreviated (const std::array<T,55>& abb) {
T z[53];
z[0] = abb[30] + -abb[31];
z[1] = abb[28] + z[0];
z[2] = m1_set::bc<T>[0] * z[1];
z[3] = abb[33] * m1_set::bc<T>[0];
z[4] = -abb[39] + z[3];
z[5] = z[2] + -z[4];
z[6] = abb[0] * z[5];
z[7] = -abb[28] + abb[29];
z[7] = m1_set::bc<T>[0] * z[7];
z[8] = abb[9] * z[7];
z[9] = abb[2] * abb[39];
z[6] = z[6] + z[8] + z[9];
z[8] = abb[29] + z[0];
z[8] = m1_set::bc<T>[0] * z[8];
z[8] = abb[40] + -z[3] + z[8];
z[10] = abb[41] * (T(1) / T(2));
z[8] = (T(1) / T(2)) * z[8] + z[10];
z[8] = abb[7] * z[8];
z[11] = abb[29] + -abb[31];
z[12] = m1_set::bc<T>[0] * z[11];
z[13] = abb[40] + z[12];
z[14] = abb[6] * (T(1) / T(2));
z[13] = z[13] * z[14];
z[15] = abb[41] + z[5];
z[16] = abb[15] * (T(1) / T(2));
z[17] = z[15] * z[16];
z[18] = abb[5] * (T(1) / T(2));
z[19] = -abb[30] + abb[32];
z[19] = m1_set::bc<T>[0] * z[19];
z[19] = -abb[41] + z[19];
z[19] = z[18] * z[19];
z[20] = -abb[39] + abb[41];
z[21] = abb[28] + abb[32];
z[22] = -abb[29] + z[21];
z[23] = -abb[30] + z[22];
z[23] = m1_set::bc<T>[0] * z[23];
z[24] = abb[40] + -z[20] + -z[23];
z[25] = abb[8] * (T(1) / T(2));
z[24] = z[24] * z[25];
z[26] = abb[39] + -abb[40];
z[7] = z[7] + -z[26];
z[27] = abb[4] * (T(1) / T(2));
z[28] = -z[7] * z[27];
z[12] = -abb[11] * z[12];
z[8] = z[6] + z[8] + -z[12] + -z[13] + z[17] + z[19] + z[24] + z[28];
z[19] = -z[0] + z[21];
z[19] = -abb[29] + (T(1) / T(2)) * z[19];
z[19] = m1_set::bc<T>[0] * z[19];
z[24] = abb[40] * (T(1) / T(2)) + -z[19];
z[24] = abb[1] * z[24];
z[28] = z[2] + -z[3];
z[29] = abb[39] + (T(1) / T(2)) * z[28];
z[30] = abb[10] * z[29];
z[24] = z[24] + z[30];
z[31] = m1_set::bc<T>[0] * z[22];
z[31] = -abb[40] + -z[4] + z[31];
z[32] = abb[13] * (T(1) / T(4));
z[33] = -z[31] * z[32];
z[8] = (T(1) / T(2)) * z[8] + -z[24] + z[33];
z[8] = abb[47] * z[8];
z[33] = -abb[28] + abb[31];
z[34] = -abb[32] + z[33];
z[34] = m1_set::bc<T>[0] * z[34];
z[4] = z[4] + z[34];
z[34] = -abb[22] + -abb[24];
z[4] = z[4] * z[34];
z[23] = -abb[41] + z[23] + z[26];
z[34] = abb[21] + abb[23];
z[23] = z[23] * z[34];
z[35] = abb[25] * z[15];
z[31] = abb[20] * z[31];
z[4] = z[4] + z[23] + z[31] + -z[35];
z[23] = abb[44] * z[4];
z[28] = -abb[41] + -z[28];
z[28] = abb[17] * z[28];
z[15] = abb[19] * z[15];
z[31] = -abb[16] * z[20];
z[15] = z[15] + z[28] + z[31];
z[15] = abb[51] * z[15];
z[15] = z[15] + z[23];
z[23] = abb[0] * (T(1) / T(2));
z[5] = z[5] * z[23];
z[20] = z[20] * z[25];
z[5] = z[5] + z[17] + -z[20];
z[12] = -z[5] + z[12];
z[17] = abb[40] * (T(-3) / T(2)) + abb[39] * (T(1) / T(2)) + z[19];
z[17] = abb[12] * z[17];
z[17] = z[17] + z[24];
z[19] = abb[13] * (T(1) / T(2));
z[20] = z[19] * z[26];
z[12] = (T(-1) / T(2)) * z[12] + -z[13] + -z[17] + z[20];
z[13] = abb[46] + abb[48];
z[12] = z[12] * z[13];
z[20] = abb[42] * (T(1) / T(4));
z[24] = abb[43] * (T(1) / T(4));
z[28] = -z[20] + -z[24];
z[4] = z[4] * z[28];
z[5] = z[5] + z[9];
z[5] = (T(1) / T(2)) * z[5] + -z[30];
z[9] = -abb[49] + abb[50];
z[5] = -z[5] * z[9];
z[7] = -abb[4] * z[7];
z[28] = -abb[7] * abb[40];
z[30] = abb[8] * z[26];
z[6] = z[6] + z[7] + z[28] + z[30];
z[6] = (T(1) / T(2)) * z[6] + -z[17];
z[6] = abb[45] * z[6];
z[3] = (T(1) / T(2)) * z[3] + -z[10];
z[2] = (T(1) / T(2)) * z[2] + -z[3] + z[26];
z[2] = -z[2] * z[13];
z[7] = abb[30] * (T(1) / T(2));
z[11] = -z[7] + -z[11];
z[11] = m1_set::bc<T>[0] * z[11];
z[3] = -abb[39] + z[3] + z[11];
z[3] = abb[47] * z[3];
z[10] = z[10] + z[29];
z[10] = z[9] * z[10];
z[11] = -abb[45] * z[26];
z[2] = z[2] + z[3] + z[10] + z[11];
z[3] = abb[3] * (T(1) / T(2));
z[2] = z[2] * z[3];
z[2] = z[2] + z[4] + z[5] + z[6] + z[8] + z[12] + (T(1) / T(4)) * z[15];
z[2] = (T(1) / T(4)) * z[2];
z[4] = -z[0] + z[22];
z[4] = abb[29] * z[4];
z[5] = prod_pow(m1_set::bc<T>[0], 2);
z[6] = (T(1) / T(6)) * z[5];
z[8] = -abb[35] + z[6];
z[10] = abb[53] + z[4] + z[8];
z[11] = -abb[33] + z[0] + z[21];
z[12] = abb[33] * z[11];
z[12] = -abb[36] + z[12];
z[15] = prod_pow(abb[32], 2);
z[15] = abb[37] + (T(1) / T(2)) * z[15];
z[17] = abb[28] * (T(1) / T(2));
z[22] = abb[32] + z[17];
z[26] = abb[28] * z[22];
z[28] = prod_pow(abb[31], 2);
z[29] = -abb[31] + z[7];
z[29] = abb[30] * z[29];
z[26] = -abb[54] + -z[10] + -z[12] + z[15] + z[26] + (T(1) / T(2)) * z[28] + z[29];
z[26] = abb[7] * z[26];
z[28] = abb[33] * (T(1) / T(2));
z[29] = -abb[32] + z[28];
z[29] = abb[33] * z[29];
z[29] = abb[36] + z[29];
z[17] = -abb[32] + z[17];
z[30] = abb[28] * z[17];
z[31] = abb[29] * (T(1) / T(2));
z[35] = -abb[32] + z[31];
z[35] = abb[29] * z[35];
z[30] = -abb[34] + z[15] + z[29] + -z[30] + z[35];
z[36] = -abb[9] * z[30];
z[37] = abb[28] * abb[30];
z[38] = prod_pow(abb[28], 2);
z[39] = abb[28] * abb[31];
z[37] = -z[6] + z[37] + (T(1) / T(2)) * z[38] + -z[39];
z[40] = z[1] + -z[28];
z[41] = abb[33] * z[40];
z[42] = abb[52] + z[41];
z[43] = z[37] + -z[42];
z[44] = abb[0] * z[43];
z[45] = abb[2] * abb[52];
z[36] = z[36] + -z[44] + z[45];
z[44] = abb[28] * abb[32];
z[15] = z[15] + z[44];
z[44] = abb[31] * (T(1) / T(2));
z[33] = z[33] * z[44];
z[46] = z[7] + -z[44];
z[47] = z[17] + -z[46];
z[47] = abb[30] * z[47];
z[48] = abb[52] + -abb[54];
z[10] = -z[10] + z[15] + z[33] + z[47] + -z[48];
z[10] = z[10] * z[25];
z[33] = abb[52] + -abb[53];
z[30] = z[30] + z[33];
z[27] = -z[27] * z[30];
z[47] = abb[11] * z[44];
z[49] = abb[11] * abb[32];
z[47] = z[47] + -z[49];
z[47] = abb[31] * z[47];
z[50] = abb[11] * z[31];
z[49] = -z[49] + z[50];
z[49] = abb[29] * z[49];
z[50] = abb[11] * abb[35];
z[47] = z[47] + -z[49] + z[50];
z[49] = z[38] + -z[39];
z[50] = abb[30] * z[1];
z[50] = (T(-1) / T(3)) * z[5] + z[49] + z[50];
z[51] = -abb[54] + (T(1) / T(2)) * z[50];
z[42] = z[42] + -z[51];
z[16] = z[16] * z[42];
z[52] = abb[32] + -z[7];
z[52] = abb[30] * z[52];
z[6] = abb[54] + z[6] + z[29] + z[52];
z[6] = z[6] * z[18];
z[18] = -abb[32] + z[44];
z[18] = abb[31] * z[18];
z[18] = -abb[35] + -abb[53] + -z[18] + z[35];
z[14] = z[14] * z[18];
z[6] = z[6] + z[10] + -z[14] + -z[16] + (T(1) / T(2)) * z[26] + z[27] + -z[36] + -z[47];
z[10] = z[37] + -z[41];
z[10] = -abb[52] + (T(1) / T(2)) * z[10];
z[10] = abb[10] * z[10];
z[18] = -abb[32] * z[0];
z[18] = z[15] + z[18];
z[26] = -z[22] + z[46];
z[27] = abb[29] * (T(3) / T(4)) + z[26];
z[27] = abb[29] * z[27];
z[5] = (T(1) / T(12)) * z[5];
z[18] = -z[5] + (T(1) / T(2)) * z[18] + z[27];
z[27] = abb[53] * (T(1) / T(2)) + -z[18];
z[27] = abb[1] * z[27];
z[27] = -z[10] + z[27];
z[29] = abb[30] * z[21];
z[29] = z[12] + -z[29];
z[4] = z[4] + -z[33];
z[21] = abb[31] * z[21];
z[21] = abb[34] + -z[4] + z[21] + z[29];
z[32] = z[21] * z[32];
z[37] = -abb[28] + z[0];
z[31] = z[31] + z[37];
z[31] = abb[29] * z[31];
z[46] = abb[34] + abb[35];
z[31] = z[31] + z[46];
z[52] = abb[28] + abb[31];
z[0] = z[0] * z[52];
z[0] = z[0] + -z[38];
z[0] = (T(1) / T(2)) * z[0] + -z[31];
z[0] = abb[14] * z[0];
z[0] = (T(1) / T(4)) * z[0] + (T(1) / T(2)) * z[6] + z[27] + z[32];
z[0] = abb[47] * z[0];
z[6] = z[44] * z[52];
z[26] = abb[30] * z[26];
z[4] = abb[54] + z[4] + -z[6] + z[8] + -z[15] + -z[26];
z[4] = z[4] * z[34];
z[6] = z[22] + -z[44];
z[6] = abb[30] * z[6];
z[6] = -abb[52] + z[6] + -z[12] + (T(1) / T(2)) * z[49];
z[6] = abb[24] * z[6];
z[8] = abb[20] * z[21];
z[12] = abb[28] + z[44];
z[12] = abb[31] * z[12];
z[12] = abb[52] + z[12] + z[29] + z[31];
z[12] = abb[22] * z[12];
z[21] = abb[25] * z[42];
z[4] = -z[4] + -z[6] + z[8] + z[12] + -z[21];
z[6] = abb[44] * (T(-1) / T(4)) + z[20];
z[6] = z[4] * z[6];
z[8] = abb[26] * abb[38];
z[4] = z[4] + (T(-1) / T(2)) * z[8];
z[4] = z[4] * z[24];
z[8] = z[23] * z[43];
z[12] = abb[30] * z[37];
z[12] = z[12] + z[39];
z[12] = (T(1) / T(2)) * z[12] + z[48];
z[20] = z[12] * z[25];
z[8] = z[8] + -z[16] + -z[20];
z[16] = -z[8] + z[47];
z[18] = abb[53] * (T(-3) / T(2)) + abb[52] * (T(1) / T(2)) + z[18];
z[18] = abb[12] * z[18];
z[18] = z[18] + z[27];
z[19] = z[19] * z[33];
z[14] = z[14] + (T(1) / T(2)) * z[16] + -z[18] + z[19];
z[14] = -z[13] * z[14];
z[8] = z[8] + -z[45];
z[8] = (T(1) / T(2)) * z[8] + -z[10];
z[8] = -z[8] * z[9];
z[10] = -abb[4] * z[30];
z[16] = abb[7] * abb[53];
z[19] = -abb[8] * z[33];
z[10] = z[10] + z[16] + z[19] + -z[36];
z[10] = (T(1) / T(2)) * z[10] + z[18];
z[10] = abb[45] * z[10];
z[16] = z[17] + z[44];
z[16] = abb[31] * z[16];
z[1] = -z[1] * z[7];
z[1] = -abb[36] + z[1] + -z[15] + z[16] + z[46];
z[7] = z[11] * z[28];
z[11] = abb[54] * (T(1) / T(2));
z[1] = abb[52] + (T(1) / T(2)) * z[1] + z[5] + z[7] + z[11] + -z[35];
z[1] = abb[47] * z[1];
z[5] = z[28] * z[40];
z[5] = -z[5] + -z[11] + -z[33] + (T(1) / T(4)) * z[50];
z[5] = -z[5] * z[13];
z[7] = -z[41] + z[51];
z[11] = -abb[52] + (T(1) / T(2)) * z[7];
z[11] = z[9] * z[11];
z[15] = abb[45] * z[33];
z[1] = z[1] + z[5] + z[11] + z[15];
z[1] = z[1] * z[3];
z[3] = abb[47] + -z[9] + z[13];
z[5] = -abb[16] + -abb[17];
z[5] = z[3] * z[5];
z[3] = abb[19] * z[3];
z[9] = -abb[42] + abb[44];
z[9] = abb[26] * z[9];
z[11] = -abb[18] * abb[47];
z[3] = -z[3] + z[5] + z[9] + z[11];
z[3] = (T(1) / T(8)) * z[3];
z[3] = abb[38] * z[3];
z[5] = -abb[17] * z[7];
z[7] = abb[19] * z[42];
z[9] = -abb[16] * z[12];
z[5] = z[5] + -z[7] + z[9];
z[7] = -abb[3] + -abb[8] + -abb[15];
z[7] = abb[27] + (T(1) / T(4)) * z[7];
z[7] = abb[38] * z[7];
z[5] = (T(1) / T(2)) * z[5] + z[7];
z[5] = abb[51] * z[5];
z[0] = z[0] + z[1] + z[3] + z[4] + (T(1) / T(2)) * z[5] + z[6] + z[8] + z[10] + z[14];
z[0] = (T(1) / T(4)) * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_128_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.09768604677786429431275133139085887213217589538973625879348237781"),stof<T>("-0.44884017236342461996103013408604024550381413551460746113004692871")}, std::complex<T>{stof<T>("-0.32783505779352584141901041773387745058899685637351807678096670347"),stof<T>("-0.02117969308723342926587963376439328458141610239887590962529290064")}, std::complex<T>{stof<T>("-0.01092763591743866215983175453483077731851538919491943323303390782"),stof<T>("-0.2192497057816049459905440144306084051266348444245836900417945748")}, std::complex<T>{stof<T>("-0.32783505779352584141901041773387745058899685637351807678096670347"),stof<T>("-0.02117969308723342926587963376439328458141610239887590962529290064")}, std::complex<T>{stof<T>("0.028506725943667559253406397001870086321410769674888617946429444721"),stof<T>("-0.085291897119023604635171637206313103698356982662243759378095386177")}, std::complex<T>{stof<T>("-0.028506725943667559253406397001870086321410769674888617946429444721"),stof<T>("0.085291897119023604635171637206313103698356982662243759378095386177")}, std::complex<T>{stof<T>("-0.19757415930072218380369023884017404767300818427486720038558645755"),stof<T>("0.27602727033741684098080560531544625171981658056668086306849614823")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_128_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_128_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.18015282672072209931603483675635724138602115369846005316782506576"),stof<T>("-0.09057116692072558972886564779102762741615190556831182642412473628")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,55> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_9_im(k), f_2_12_im(k), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_2_re(k), f_2_9_re(k), f_2_12_re(k)};

                    
            return f_4_128_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_128_DLogXconstant_part(base_point<T>, kend);
	value += f_4_128_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_128_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_128_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_128_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_128_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_128_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_128_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
