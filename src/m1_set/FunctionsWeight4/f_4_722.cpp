/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_722.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_722_abbreviated (const std::array<T,37>& abb) {
T z[97];
z[0] = abb[25] + abb[27];
z[1] = abb[30] + z[0];
z[2] = 2 * abb[31];
z[3] = z[1] + -z[2];
z[4] = abb[0] * z[3];
z[5] = 4 * abb[31];
z[6] = 2 * abb[25];
z[7] = z[5] + -z[6];
z[8] = abb[28] + -abb[30];
z[9] = abb[26] + z[8];
z[10] = z[7] + z[9];
z[11] = 4 * abb[29];
z[12] = z[10] + -z[11];
z[12] = abb[6] * z[12];
z[13] = z[4] + z[12];
z[14] = 3 * abb[28];
z[15] = 2 * abb[30];
z[16] = z[14] + -z[15];
z[17] = 2 * abb[26];
z[18] = z[16] + z[17];
z[19] = 2 * abb[29];
z[20] = abb[25] + z[19];
z[21] = 2 * abb[27];
z[22] = z[20] + -z[21];
z[23] = -z[18] + z[22];
z[23] = abb[4] * z[23];
z[24] = z[19] + z[21];
z[25] = -abb[26] + z[24];
z[26] = -abb[28] + z[6];
z[27] = abb[30] + z[2] + -z[25] + -z[26];
z[27] = abb[3] * z[27];
z[28] = 2 * abb[28];
z[29] = abb[26] + z[24] + z[28];
z[30] = 3 * abb[30];
z[31] = z[29] + -z[30];
z[32] = -abb[25] + z[2];
z[33] = z[31] + -z[32];
z[34] = abb[8] * z[33];
z[35] = -abb[30] + z[0];
z[36] = abb[5] * z[35];
z[37] = abb[32] + abb[33];
z[38] = abb[10] * z[37];
z[39] = z[36] + z[38];
z[40] = abb[25] + -abb[28];
z[41] = abb[2] * z[40];
z[23] = z[13] + z[23] + z[27] + z[34] + -z[39] + z[41];
z[23] = abb[16] * z[23];
z[27] = -z[11] + z[32];
z[41] = 3 * abb[26];
z[16] = abb[27] + z[16] + z[27] + z[41];
z[16] = abb[8] * z[16];
z[42] = abb[11] * z[37];
z[43] = abb[12] * z[37];
z[44] = z[42] + -z[43];
z[16] = z[16] + -z[44];
z[45] = abb[26] + abb[27];
z[27] = abb[28] + z[27] + z[45];
z[27] = abb[3] * z[27];
z[46] = 2 * abb[4];
z[47] = z[9] * z[46];
z[48] = abb[2] * z[9];
z[13] = z[13] + -z[16] + z[27] + z[47] + 4 * z[48];
z[27] = abb[14] + -abb[17];
z[27] = z[13] * z[27];
z[23] = z[23] + z[27];
z[27] = -z[19] + z[21];
z[9] = -z[6] + z[9];
z[47] = -z[9] + -z[27];
z[47] = abb[6] * z[47];
z[49] = -3 * abb[2] + abb[3];
z[49] = z[40] * z[49];
z[50] = abb[28] + -z[22];
z[50] = abb[4] * z[50];
z[51] = -abb[26] + abb[27];
z[52] = -z[6] + -z[51];
z[52] = abb[0] * z[52];
z[47] = z[39] + z[47] + z[49] + z[50] + z[52];
z[47] = abb[15] * z[47];
z[23] = 2 * z[23] + z[47];
z[23] = abb[15] * z[23];
z[47] = 4 * abb[28];
z[49] = abb[25] + -abb[26] + z[30] + -z[47];
z[49] = abb[4] * z[49];
z[50] = abb[25] + abb[30];
z[52] = -z[28] + z[50];
z[52] = z[51] + 2 * z[52];
z[52] = abb[0] * z[52];
z[35] = abb[3] * z[35];
z[49] = -z[35] + -z[38] + z[43] + z[49] + z[52];
z[52] = 2 * abb[19];
z[49] = z[49] * z[52];
z[53] = z[6] + -z[8];
z[54] = 6 * abb[31];
z[55] = abb[26] + z[54];
z[56] = 3 * abb[27];
z[57] = -z[19] + -2 * z[53] + z[55] + -z[56];
z[57] = abb[6] * z[57];
z[58] = -abb[26] + z[50];
z[59] = z[28] + -z[58];
z[59] = abb[2] * z[59];
z[59] = -z[57] + z[59];
z[59] = abb[21] * z[59];
z[60] = 2 * z[8] + z[19] + z[45];
z[60] = abb[21] * z[60];
z[61] = abb[21] * z[2];
z[60] = z[60] + -z[61];
z[60] = abb[4] * z[60];
z[59] = z[59] + z[60];
z[60] = -z[1] + z[17] + z[28];
z[60] = abb[0] * z[60];
z[62] = 2 * z[48];
z[35] = z[35] + -z[42] + z[60] + -z[62];
z[35] = prod_pow(abb[17], 2) * z[35];
z[60] = abb[30] + z[28];
z[63] = 3 * abb[25];
z[25] = z[25] + -z[60] + z[63];
z[25] = abb[21] * z[25];
z[25] = z[25] + -z[61];
z[25] = abb[3] * z[25];
z[64] = z[29] + z[50] + -z[54];
z[65] = 2 * abb[7];
z[66] = abb[21] * z[64] * z[65];
z[31] = -abb[25] + -z[31];
z[31] = abb[21] * z[31];
z[31] = z[31] + z[61];
z[67] = 4 * z[8] + z[45];
z[52] = z[52] * z[67];
z[31] = 3 * z[31] + z[52];
z[31] = abb[8] * z[31];
z[52] = z[19] + z[28];
z[68] = z[52] + -z[58];
z[68] = abb[21] * z[68];
z[61] = -z[61] + z[68];
z[61] = abb[0] * z[61];
z[23] = z[23] + z[25] + z[31] + z[35] + z[49] + 2 * z[59] + z[61] + z[66];
z[25] = -z[30] + z[41];
z[31] = -abb[25] + z[5];
z[24] = z[24] + z[25] + -z[31] + z[47];
z[24] = abb[4] * z[24];
z[35] = z[15] + z[26];
z[49] = 4 * abb[27];
z[35] = -abb[29] + 2 * z[35] + z[49] + -z[55];
z[35] = abb[6] * z[35];
z[55] = abb[7] * z[64];
z[59] = -z[19] + z[56];
z[53] = -z[2] + z[53] + z[59];
z[61] = abb[3] * z[53];
z[64] = 4 * abb[26];
z[66] = 5 * z[8] + z[59] + z[64];
z[66] = abb[8] * z[66];
z[68] = 6 * abb[28];
z[69] = 5 * abb[30];
z[70] = -abb[25] + z[68] + -z[69];
z[71] = 5 * abb[26] + z[70];
z[71] = abb[2] * z[71];
z[72] = -abb[29] + -z[0] + z[2];
z[73] = -abb[28] + z[72];
z[73] = abb[0] * z[73];
z[24] = -z[24] + -z[35] + z[38] + z[43] + -z[55] + -z[61] + z[66] + -z[71] + z[73];
z[24] = 4 * z[24];
z[35] = -abb[34] * z[24];
z[55] = z[11] + z[63];
z[61] = 5 * abb[27];
z[71] = z[17] + z[61];
z[73] = 7 * abb[30];
z[74] = 10 * abb[28] + -z[55] + z[71] + -z[73];
z[74] = abb[4] * z[74];
z[75] = 3 * abb[29];
z[76] = -abb[26] + z[75];
z[77] = -z[15] + z[40] + z[76];
z[78] = 4 * abb[3];
z[77] = z[77] * z[78];
z[79] = 8 * abb[29];
z[80] = z[17] + z[79];
z[81] = 5 * abb[25] + z[69];
z[82] = -abb[27] + z[28] + z[80] + -z[81];
z[82] = abb[0] * z[82];
z[83] = 8 * abb[31];
z[84] = z[30] + z[83];
z[85] = 4 * abb[25];
z[86] = -abb[26] + z[49];
z[87] = abb[28] + -10 * abb[29] + z[84] + -z[85] + -z[86];
z[88] = 2 * abb[8];
z[87] = z[87] * z[88];
z[12] = 4 * z[12];
z[72] = z[8] + z[72];
z[89] = abb[7] * z[72];
z[89] = 8 * z[89];
z[48] = 8 * z[48];
z[90] = 4 * z[43] + -z[48];
z[91] = 3 * z[38];
z[92] = 2 * z[42];
z[74] = -z[12] + z[74] + z[77] + z[82] + z[87] + -z[89] + z[90] + z[91] + z[92];
z[74] = abb[14] * z[74];
z[77] = 12 * abb[31];
z[79] = z[28] + z[50] + z[56] + -z[77] + z[79];
z[79] = abb[0] * z[79];
z[18] = -abb[25] + z[18];
z[18] = abb[2] * z[18];
z[18] = z[18] + -z[57];
z[71] = z[47] + z[55] + z[71] + -z[84];
z[71] = abb[4] * z[71];
z[82] = abb[28] + z[6];
z[84] = -abb[26] + z[21];
z[87] = 5 * abb[29];
z[93] = z[5] + -2 * z[82] + z[84] + -z[87];
z[93] = abb[3] * z[93];
z[94] = z[14] + z[15];
z[85] = z[85] + z[94];
z[85] = 10 * abb[27] + 11 * abb[29] + -28 * abb[31] + z[41] + 2 * z[85];
z[85] = abb[7] * z[85];
z[95] = 2 * z[36];
z[18] = 2 * z[18] + -4 * z[34] + z[71] + z[79] + z[85] + -z[91] + z[93] + z[95];
z[18] = abb[16] * z[18];
z[34] = 5 * abb[28];
z[71] = 6 * abb[29];
z[7] = z[7] + z[25] + z[34] + -z[71];
z[25] = z[7] * z[88];
z[25] = z[25] + z[90];
z[79] = z[25] + z[92];
z[85] = -z[11] + z[61];
z[90] = 6 * abb[26] + z[28];
z[91] = abb[25] + -z[30] + z[85] + z[90];
z[91] = abb[4] * z[91];
z[50] = -abb[29] + -z[2] + z[21] + z[50];
z[50] = z[50] * z[78];
z[93] = -z[1] + z[83];
z[80] = -z[28] + z[80];
z[96] = -z[80] + z[93];
z[96] = abb[0] * z[96];
z[50] = -z[38] + z[50] + -z[79] + z[89] + z[91] + z[96];
z[50] = abb[17] * z[50];
z[29] = -abb[30] + z[29] + -z[31];
z[29] = abb[4] * z[29];
z[29] = z[29] + -z[38] + -z[57];
z[31] = 2 * z[29];
z[57] = 4 * abb[30];
z[82] = -z[57] + -z[82];
z[91] = 20 * abb[31];
z[82] = -abb[26] + -6 * abb[27] + 2 * z[82] + -z[87] + z[91];
z[82] = abb[7] * z[82];
z[45] = z[5] + -z[45] + -z[52];
z[52] = 2 * abb[0];
z[45] = z[45] * z[52];
z[96] = abb[30] + -z[40];
z[96] = -abb[29] + -z[84] + 2 * z[96];
z[96] = abb[3] * z[96];
z[33] = z[33] * z[88];
z[33] = -z[31] + z[33] + z[45] + z[82] + z[96];
z[45] = 2 * abb[18];
z[33] = z[33] * z[45];
z[18] = z[18] + z[33] + z[50] + z[74];
z[18] = abb[16] * z[18];
z[33] = -z[47] + z[73];
z[0] = abb[26] * (T(-2) / T(3)) + -z[0] + z[5] + -z[19] + (T(-1) / T(3)) * z[33];
z[0] = z[0] * z[46];
z[46] = abb[25] * (T(23) / T(3));
z[33] = -13 * abb[29] + -z[33] + -z[46] + (T(-14) / T(3)) * z[51] + z[91];
z[33] = abb[3] * z[33];
z[50] = abb[27] + -abb[28];
z[50] = 11 * abb[25] + abb[26] + 8 * z[50] + -z[69];
z[11] = -z[2] + z[11] + (T(1) / T(3)) * z[50];
z[11] = z[11] * z[65];
z[46] = 9 * abb[26] + abb[27] * (T(4) / T(3)) + abb[29] * (T(22) / T(3)) + abb[28] * (T(32) / T(3)) + -z[30] + -z[46] + -z[77];
z[46] = abb[0] * z[46];
z[14] = -6 * abb[25] + -7 * abb[29] + 14 * abb[31] + abb[27] * (T(-16) / T(3)) + abb[30] * (T(-11) / T(3)) + z[14] + z[17];
z[14] = abb[6] * z[14];
z[14] = z[14] + z[38];
z[50] = -abb[27] + abb[29];
z[50] = -abb[26] + (T(-1) / T(3)) * z[8] + (T(2) / T(3)) * z[50];
z[50] = abb[8] * z[50];
z[51] = abb[25] + -abb[30];
z[69] = abb[29] + z[51];
z[69] = abb[1] * z[69];
z[60] = -abb[26] + z[60];
z[60] = abb[25] + (T(-1) / T(3)) * z[60];
z[60] = abb[2] * z[60];
z[0] = z[0] + z[11] + 2 * z[14] + z[33] + (T(10) / T(3)) * z[42] + z[46] + 4 * z[50] + z[60] + (T(-13) / T(3)) * z[69];
z[11] = prod_pow(m1_set::bc<T>[0], 2);
z[0] = z[0] * z[11];
z[14] = abb[26] + z[21];
z[33] = abb[29] + z[14] + -z[77] + 2 * z[94];
z[46] = abb[7] * z[33];
z[50] = -z[5] + 2 * z[26];
z[60] = abb[29] + z[50] + z[86];
z[60] = abb[3] * z[60];
z[2] = -z[2] + z[47];
z[47] = z[2] + -z[58];
z[47] = z[47] * z[52];
z[58] = z[67] * z[88];
z[1] = abb[29] + -3 * abb[31] + z[1];
z[73] = 8 * abb[9];
z[74] = z[1] * z[73];
z[82] = 2 * z[43];
z[86] = z[82] + z[92];
z[31] = z[31] + z[46] + z[47] + -z[58] + z[60] + -z[74] + -z[86];
z[31] = 2 * z[31];
z[46] = -abb[35] * z[31];
z[34] = -z[34] + z[57] + -z[64];
z[47] = -abb[25] + -z[34];
z[47] = abb[2] * z[47];
z[22] = -z[22] + -z[34];
z[22] = abb[4] * z[22];
z[34] = z[51] + z[59];
z[34] = abb[3] * z[34];
z[22] = z[22] + z[34] + z[39] + z[44] + z[47] + -z[66];
z[22] = abb[20] * z[22];
z[34] = prod_pow(abb[18], 2);
z[39] = abb[21] + -z[34];
z[39] = z[36] * z[39];
z[22] = z[22] + z[39];
z[9] = z[9] + -z[21] + -z[71] + z[83];
z[9] = abb[6] * z[9];
z[39] = abb[4] * z[67];
z[9] = z[9] + z[39] + z[62] + -z[95];
z[7] = abb[8] * z[7];
z[39] = -z[17] + z[71];
z[44] = -abb[27] + -z[5] + -z[8] + z[39];
z[44] = abb[3] * z[44];
z[2] = abb[30] + -z[2] + z[6] + -z[17];
z[2] = z[2] * z[52];
z[2] = z[2] + z[7] + -z[9] + z[44] + z[82];
z[2] = abb[14] * z[2];
z[6] = -z[17] + -z[70] + z[85];
z[6] = abb[4] * z[6];
z[6] = z[6] + -z[12] + -z[38];
z[7] = z[17] + z[68];
z[12] = z[7] + -z[93];
z[12] = abb[0] * z[12];
z[32] = abb[28] + z[32] + -z[76];
z[32] = z[32] * z[78];
z[12] = -z[6] + z[12] + -z[25] + z[32] + z[92];
z[12] = abb[17] * z[12];
z[25] = z[3] + z[19] + -z[28];
z[32] = -abb[0] + abb[8];
z[32] = z[25] * z[32];
z[3] = abb[3] * z[3];
z[44] = z[65] * z[72];
z[3] = z[3] + z[32] + -z[42] + -z[43] + z[44];
z[3] = 4 * z[3];
z[32] = abb[18] * z[3];
z[2] = 2 * z[2] + z[12] + z[32];
z[2] = abb[14] * z[2];
z[3] = -abb[17] * z[3];
z[12] = z[29] + 2 * z[69];
z[12] = 2 * z[12];
z[21] = z[21] + z[50] + z[76];
z[21] = abb[3] * z[21];
z[29] = -abb[28] + 6 * abb[30];
z[29] = -abb[29] + 2 * z[29] + -z[77] + z[84];
z[29] = abb[7] * z[29];
z[4] = 2 * z[4] + z[12] + z[21] + z[29];
z[4] = abb[18] * z[4];
z[3] = z[3] + z[4];
z[3] = abb[18] * z[3];
z[4] = abb[27] + abb[30];
z[21] = z[4] + -z[55] + z[90];
z[21] = abb[10] * z[21];
z[27] = z[8] + z[27] + z[41];
z[29] = abb[12] * z[27];
z[32] = abb[0] + 3 * abb[4] + -4 * abb[13] + z[78];
z[32] = z[32] * z[37];
z[4] = -z[4] + z[20];
z[20] = 2 * abb[11];
z[4] = z[4] * z[20];
z[4] = z[4] + -z[21] + -2 * z[29] + -z[32];
z[20] = abb[36] * z[4];
z[21] = -abb[16] + z[45];
z[21] = abb[16] * z[21];
z[11] = -abb[21] + -2 * abb[34] + z[11] + z[21] + -z[34];
z[11] = z[1] * z[11] * z[73];
z[0] = z[0] + z[2] + z[3] + z[11] + z[18] + z[20] + 4 * z[22] + 2 * z[23] + z[35] + z[46];
z[0] = 2 * z[0];
z[2] = 16 * abb[31];
z[3] = 7 * abb[25] + 12 * abb[29] + -z[2] + z[17] + -z[28] + z[30] + z[56];
z[3] = abb[4] * z[3];
z[11] = abb[6] * z[53];
z[17] = z[14] + z[15] + -z[87];
z[17] = abb[3] * z[17];
z[18] = z[33] * z[65];
z[2] = -z[2] + z[61] + z[81];
z[7] = z[2] + z[7];
z[7] = abb[0] * z[7];
z[3] = z[3] + z[7] + 4 * z[11] + 2 * z[17] + z[18] + -7 * z[38] + -z[79];
z[3] = abb[16] * z[3];
z[2] = -z[2] + -z[80];
z[2] = abb[0] * z[2];
z[7] = z[27] * z[88];
z[10] = abb[27] + -z[10] + z[75];
z[10] = z[10] * z[78];
z[2] = z[2] + z[6] + z[7] + z[10] + -6 * z[42] + -z[48] + z[89];
z[2] = abb[17] * z[2];
z[6] = abb[27] + -abb[30] + z[19] + -z[54] + -z[63] + z[64] + z[68];
z[6] = abb[0] * z[6];
z[7] = -z[15] + -z[39] + -z[40] + z[54];
z[7] = abb[3] * z[7];
z[6] = z[6] + z[7] + z[9] + -z[16] + -z[44];
z[6] = abb[14] * z[6];
z[7] = abb[15] * z[13];
z[6] = z[6] + -z[7];
z[7] = z[8] + -z[63];
z[7] = 2 * z[7] + -z[49] + -z[76] + z[83];
z[7] = abb[3] * z[7];
z[8] = z[26] + -z[57];
z[5] = z[5] + 2 * z[8] + z[14] + z[87];
z[5] = abb[7] * z[5];
z[8] = -z[25] * z[88];
z[9] = -abb[28] + abb[29];
z[9] = abb[0] * z[9];
z[5] = z[5] + z[7] + z[8] + 4 * z[9] + -z[12] + z[86];
z[5] = z[5] * z[45];
z[7] = abb[18] * z[36];
z[2] = z[2] + z[3] + z[5] + 4 * z[6] + 8 * z[7];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = -abb[22] * z[24];
z[5] = -abb[23] * z[31];
z[4] = abb[24] * z[4];
z[6] = -abb[16] + abb[18];
z[6] = m1_set::bc<T>[0] * z[6];
z[6] = -abb[22] + z[6];
z[1] = abb[9] * z[1] * z[6];
z[1] = 16 * z[1] + z[2] + z[3] + z[4] + z[5];
z[1] = 2 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_722_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("184.59701950267346798585441479195217662138146356808560951682975081"),stof<T>("239.80092275748920226503509253047515224208029854097392904410292705")}, std::complex<T>{stof<T>("-199.43320356595267664239436499057374235211848479921214834587935148"),stof<T>("-304.76764858584429587446681274696580428161022751803640172248330014")}, std::complex<T>{stof<T>("-13.865819917005231472757563453979174022395514930391002214177383546"),stof<T>("-109.082102917842024700752015197551285522777497527380748272875078372")}, std::complex<T>{stof<T>("-201.66805311390243499559782716099512314950196761897869261577988085"),stof<T>("-307.14077458210519124816076926482388884862463802423980983381115016")}, std::complex<T>{stof<T>("244.41240454619956613486166207743013329626627718929933562793396178"),stof<T>("-196.26587602022025266752361490560801495380408763426214514870649481")}, std::complex<T>{stof<T>("101.656573462022117649573516908596886062795261682948736558294251418"),stof<T>("-68.601974394941691538911104441023737397600124001811859957348940012")}, std::complex<T>{stof<T>("-100.60661801771934845868193049851908584354998543391942344277196114"),stof<T>("548.98522805482844115327074434794131287337655971971950939978923857")}, std::complex<T>{stof<T>("78.387943251704230387346286045015673616542737138028555266848003702"),stof<T>("-53.791869827793974246061946491202813403623492668084888602198432542")}, std::complex<T>{stof<T>("78.387943251704230387346286045015673616542737138028555266848003702"),stof<T>("-53.791869827793974246061946491202813403623492668084888602198432542")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[67].real()/kbase.W[67].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[127].real()/kbase.W[127].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_722_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_722_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("556.8465432141650467042261889229312716641808649374899147529456659"),stof<T>("-232.45183649129688186893432624088284846660782269275581469562423276")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,37> abb = {dl[0], dlog_W3(k,dl), dl[5], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W13(k,dl), dlog_W15(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W68(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_19(k), f_2_4_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[67].real()/k.W[67].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[127].real()/k.W[127].real()), f_2_4_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_722_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_722_DLogXconstant_part(base_point<T>, kend);
	value += f_4_722_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_722_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_722_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_722_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_722_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_722_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_722_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
