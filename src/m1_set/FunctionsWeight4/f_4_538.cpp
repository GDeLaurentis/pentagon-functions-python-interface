/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_538.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_538_abbreviated (const std::array<T,30>& abb) {
T z[40];
z[0] = abb[20] + 2 * abb[25];
z[1] = 2 * abb[22];
z[2] = z[0] + -z[1];
z[3] = 2 * abb[24];
z[4] = abb[23] + -abb[26];
z[5] = z[2] + z[3] + z[4];
z[6] = abb[5] * z[5];
z[7] = abb[20] * (T(1) / T(2));
z[8] = abb[25] + z[7];
z[9] = -abb[24] + z[1];
z[10] = abb[23] + abb[26];
z[11] = -abb[21] + (T(1) / T(2)) * z[10];
z[12] = z[8] + -z[9] + z[11];
z[12] = abb[4] * z[12];
z[13] = -abb[25] + z[4];
z[14] = -abb[20] + 2 * z[13];
z[15] = abb[22] + z[3] + z[14];
z[15] = abb[3] * z[15];
z[16] = abb[21] + -abb[26];
z[17] = abb[22] + z[16];
z[18] = abb[6] * z[17];
z[19] = 2 * z[18];
z[20] = z[15] + z[19];
z[21] = abb[24] + z[4];
z[21] = abb[1] * z[21];
z[22] = 2 * z[21];
z[23] = z[20] + z[22];
z[24] = 2 * abb[21];
z[25] = z[10] + -z[24];
z[26] = -z[0] + z[25];
z[26] = abb[2] * z[26];
z[27] = -abb[22] + z[8];
z[28] = -abb[23] + 3 * abb[26];
z[29] = abb[21] + abb[24] + -z[27] + (T(-1) / T(2)) * z[28];
z[29] = abb[9] * z[29];
z[30] = abb[24] + z[27];
z[31] = z[11] + -z[30];
z[31] = abb[0] * z[31];
z[32] = abb[22] + z[4];
z[33] = abb[8] * z[32];
z[12] = z[6] + z[12] + -z[23] + -z[26] + 3 * z[29] + z[31] + z[33];
z[12] = abb[16] * z[12];
z[29] = z[2] + z[25];
z[29] = abb[4] * z[29];
z[24] = z[3] + z[24];
z[2] = -z[2] + z[24] + -z[28];
z[2] = abb[9] * z[2];
z[28] = abb[0] * z[32];
z[31] = 2 * z[33];
z[23] = -z[2] + z[23] + -z[28] + -z[29] + -z[31];
z[28] = abb[12] * z[23];
z[7] = -z[7] + z[13];
z[13] = abb[24] * (T(1) / T(2));
z[32] = abb[22] + z[7] + z[13];
z[32] = abb[5] * z[32];
z[34] = abb[22] * (T(1) / T(2));
z[7] = abb[24] + z[7] + z[34];
z[7] = abb[3] * z[7];
z[35] = abb[22] + -abb[24];
z[35] = abb[4] * z[35];
z[32] = -z[7] + z[32] + -z[35];
z[36] = z[13] + z[16] + z[34];
z[36] = abb[0] * z[36];
z[36] = -z[18] + -z[22] + z[31] + z[32] + z[36];
z[36] = abb[11] * z[36];
z[28] = z[28] + z[36];
z[28] = abb[11] * z[28];
z[36] = abb[28] * z[23];
z[37] = abb[21] + -abb[23];
z[13] = -z[13] + -z[27] + (T(1) / T(2)) * z[37];
z[13] = abb[4] * z[13];
z[37] = (T(1) / T(2)) * z[4];
z[30] = z[30] + z[37];
z[30] = abb[5] * z[30];
z[7] = z[7] + z[13] + -z[30];
z[13] = -z[8] + z[11];
z[13] = abb[2] * z[13];
z[30] = z[13] + z[18];
z[11] = z[11] + z[34];
z[38] = -abb[24] + z[11];
z[38] = abb[0] * z[38];
z[38] = z[2] + -z[7] + -z[21] + -z[30] + z[38];
z[38] = prod_pow(abb[12], 2) * z[38];
z[2] = z[2] + -z[15];
z[26] = -z[2] + z[26];
z[39] = abb[25] + z[16];
z[3] = -abb[22] + z[3];
z[39] = abb[20] + z[3] + 2 * z[39];
z[39] = abb[0] * z[39];
z[9] = z[9] + z[16];
z[9] = abb[4] * z[9];
z[6] = -z[6] + z[9] + z[26] + z[39];
z[9] = -abb[15] * z[6];
z[5] = -abb[4] * z[5];
z[39] = abb[24] * (T(3) / T(2));
z[25] = -z[25] + z[27] + z[39];
z[25] = abb[0] * z[25];
z[4] = abb[24] * (T(-5) / T(2)) + -2 * z[4] + -z[27];
z[4] = abb[5] * z[4];
z[4] = -z[2] + z[4] + z[5] + z[22] + z[25];
z[4] = abb[14] * z[4];
z[5] = abb[22] + z[10] + -z[24];
z[5] = abb[0] * z[5];
z[2] = z[2] + z[5] + z[29];
z[2] = abb[12] * z[2];
z[5] = z[16] + -z[34] + z[39];
z[5] = abb[0] * z[5];
z[5] = z[5] + -z[18] + -z[32];
z[5] = abb[10] * z[5];
z[2] = z[2] + z[5];
z[2] = abb[10] * z[2];
z[5] = abb[12] * z[6];
z[0] = -z[0] + z[11];
z[0] = abb[0] * z[0];
z[0] = z[0] + -z[7] + z[30];
z[0] = abb[13] * z[0];
z[0] = z[0] + z[5];
z[0] = abb[13] * z[0];
z[3] = z[3] + z[16];
z[3] = abb[0] * z[3];
z[5] = -z[18] + z[33];
z[6] = abb[4] * z[17];
z[3] = z[3] + -z[5] + 2 * z[6] + z[26];
z[6] = -abb[27] * z[3];
z[7] = z[8] + z[37];
z[7] = abb[4] * z[7];
z[7] = z[7] + z[13] + -11 * z[33];
z[8] = 7 * abb[26] + abb[23] * (T(1) / T(3));
z[8] = abb[21] * (T(-11) / T(3)) + abb[24] * (T(-7) / T(2)) + (T(1) / T(2)) * z[8];
z[8] = abb[0] * z[8];
z[7] = (T(1) / T(3)) * z[7] + z[8] + (T(11) / T(3)) * z[18] + (T(7) / T(2)) * z[21];
z[7] = prod_pow(m1_set::bc<T>[0], 2) * z[7];
z[0] = abb[29] + z[0] + z[2] + z[4] + z[6] + (T(1) / T(2)) * z[7] + z[9] + z[12] + z[28] + z[36] + z[38];
z[1] = abb[24] + z[1] + z[14];
z[1] = abb[5] * z[1];
z[2] = 2 * z[16];
z[4] = abb[22] + -3 * abb[24] + -z[2];
z[4] = abb[0] * z[4];
z[4] = z[1] + z[4] + -z[15] + z[19] + -2 * z[35];
z[4] = abb[10] * z[4];
z[6] = -z[31] + z[35];
z[2] = -abb[22] + -abb[24] + -z[2];
z[2] = abb[0] * z[2];
z[1] = -z[1] + z[2] + 2 * z[6] + z[20] + 4 * z[21];
z[1] = abb[11] * z[1];
z[2] = abb[24] + z[16];
z[2] = abb[0] * z[2];
z[2] = z[2] + z[5] + -z[21];
z[2] = abb[12] * z[2];
z[1] = z[1] + 2 * z[2] + z[4];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[18] * z[23];
z[3] = -abb[17] * z[3];
z[1] = abb[19] + z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_538_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("12.167344128214953571732473671194683908099341997763856428278242457"),stof<T>("20.550552081361626739730676952202164030099696606619048054700665324")}, std::complex<T>{stof<T>("-9.3131899292339144285307869183397178166529937726162666622218311787"),stof<T>("-4.1126760216559621378873502335357239590050872368184367797598318983")}, std::complex<T>{stof<T>("-6.0742657438101381959682209659761817418268722392557805511274275392"),stof<T>("-11.966078532656791533961480412362773585634487366888877373170901703")}, std::complex<T>{stof<T>("15.406268313638729804295039623558219982925463531124342539372646097"),stof<T>("12.697149570360797343656546773375114403470296476548607461289595519")}, std::complex<T>{stof<T>("-6.0930783844048153757642527052185021662724697585080758771508149182"),stof<T>("-8.5844735487048352057691965398393904444652092397301706815297636205")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[28].real()/kbase.W[28].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_538_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_538_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,4> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(16)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * ((-35 + 4 * m1_set::bc<T>[1] + 21 * m1_set::bc<T>[2]) * v[0] + 7 * v[1] + -11 * v[2] + v[3] + 4 * (-6 + 4 * m1_set::bc<T>[1] + 6 * m1_set::bc<T>[2] + 3 * v[4]) + 4 * m1_set::bc<T>[1] * (v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + 11 * v[5] + -m1_set::bc<T>[2] * (v[1] + 3 * (-3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = ((T(-1) / T(32)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (2 * (13 * v[0] + -5 * v[1] + v[2] + v[3] + 4 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -v[5]) + m1_set::bc<T>[2] * (-21 * v[0] + v[1] + 3 * (-8 + -3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[2] = ((-1 + 2 * m1_set::bc<T>[1] + 3 * m1_set::bc<T>[2]) * (T(-1) / T(2)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;
c[3] = ((4 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (T(1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return t * abb[24] * c[0] + t * abb[22] * c[1] + -2 * t * abb[25] * c[1] + abb[24] * c[2] + abb[21] * (t * c[0] + c[2]) + -abb[26] * (t * c[0] + c[2]) + -abb[22] * c[3] + 2 * abb[25] * c[3] + abb[20] * (-t * c[1] + c[3]);
	}
	{
T z[5];
z[0] = -abb[21] + -abb[24] + abb[26];
z[1] = 2 * abb[22];
z[2] = -z[0] + -z[1];
z[2] = abb[12] * z[2];
z[1] = (T(-1) / T(2)) * z[0] + z[1];
z[1] = abb[13] * z[1];
z[1] = z[1] + z[2];
z[1] = abb[13] * z[1];
z[2] = -abb[14] + abb[16];
z[3] = prod_pow(abb[12], 2);
z[3] = abb[15] + 2 * z[2] + (T(3) / T(2)) * z[3];
z[0] = z[0] * z[3];
z[3] = -abb[12] + abb[13];
z[4] = 2 * abb[13];
z[3] = z[3] * z[4];
z[2] = 2 * abb[15] + z[2];
z[3] = z[2] + z[3];
z[4] = -abb[20] + -2 * abb[25];
z[3] = z[3] * z[4];
z[2] = abb[22] * z[2];
z[0] = z[0] + z[1] + z[2] + z[3];
return abb[7] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_538_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_538_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = T(0);
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,30> abb = {dl[0], dl[1], dlog_W4(k,dl), dlog_W7(k,dl), dlog_W10(k,dl), dlog_W12(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), f_1_1(k), f_1_3(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_9_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[11].real()/k.W[11].real()), IntDLogL_W_17(t,k,kend,dl), rlog(kend.W[28].real()/k.W[28].real()), f_2_2_re(k), f_2_9_re(k), T{0}};
abb[19] = SpDLog_f_4_538_W_17_Im(t, path, abb);
abb[29] = SpDLog_f_4_538_W_17_Re(t, path, abb);

                    
            return f_4_538_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_538_DLogXconstant_part(base_point<T>, kend);
	value += f_4_538_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_538_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_538_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_538_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_538_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_538_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_538_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
