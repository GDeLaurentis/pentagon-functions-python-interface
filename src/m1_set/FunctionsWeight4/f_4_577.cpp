/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_577.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_577_abbreviated (const std::array<T,68>& abb) {
T z[150];
z[0] = 2 * abb[53];
z[1] = 2 * abb[50];
z[2] = z[0] + -z[1];
z[3] = abb[46] + abb[47];
z[4] = abb[55] * (T(5) / T(2));
z[5] = abb[48] + abb[49];
z[5] = (T(1) / T(2)) * z[5];
z[6] = abb[54] * (T(1) / T(2));
z[7] = 2 * abb[56];
z[8] = -z[2] + -z[3] + z[4] + -z[5] + z[6] + -z[7];
z[8] = abb[0] * z[8];
z[9] = 3 * abb[47];
z[10] = -abb[54] + z[9];
z[11] = 3 * abb[46] + -abb[51];
z[12] = z[10] + z[11];
z[13] = 3 * abb[48];
z[14] = abb[45] + z[13];
z[15] = 3 * abb[56];
z[16] = -abb[50] + z[15];
z[17] = z[0] + -z[4] + -z[12] + (T(-1) / T(2)) * z[14] + z[16];
z[17] = abb[9] * z[17];
z[18] = abb[55] * (T(1) / T(2));
z[19] = 2 * abb[51];
z[20] = z[18] + -z[19];
z[21] = abb[49] * (T(1) / T(2));
z[22] = -abb[45] + z[21];
z[23] = abb[48] * (T(7) / T(2));
z[24] = -z[6] + z[20] + z[22] + -z[23];
z[25] = z[15] + z[24];
z[25] = abb[4] * z[25];
z[17] = z[17] + z[25];
z[25] = 4 * abb[46];
z[26] = 4 * abb[47];
z[27] = z[25] + z[26];
z[28] = 3 * abb[51];
z[29] = abb[45] + 11 * abb[48] + z[28];
z[30] = -abb[49] + z[29];
z[30] = z[27] + (T(1) / T(2)) * z[30];
z[31] = abb[55] * (T(3) / T(2));
z[32] = abb[50] + z[6] + z[15] + -z[30] + z[31];
z[32] = abb[5] * z[32];
z[33] = 4 * abb[51];
z[34] = -abb[49] + abb[54];
z[35] = z[33] + z[34];
z[36] = 6 * abb[56];
z[37] = 2 * abb[45];
z[38] = z[36] + -z[37];
z[39] = 7 * abb[48];
z[40] = abb[55] + -z[35] + z[38] + -z[39];
z[41] = abb[8] * z[40];
z[42] = abb[59] + abb[60] + abb[61] + abb[62];
z[43] = abb[24] * z[42];
z[44] = abb[22] * z[42];
z[41] = z[41] + -z[43] + -z[44];
z[45] = z[6] + z[18];
z[5] = -abb[51] + -z[5] + z[45];
z[5] = abb[6] * z[5];
z[46] = abb[25] * z[42];
z[47] = abb[23] * z[42];
z[48] = z[46] + z[47];
z[49] = (T(1) / T(2)) * z[48];
z[50] = z[5] + -z[49];
z[51] = -abb[45] + z[7];
z[52] = 2 * abb[48];
z[53] = z[51] + -z[52];
z[54] = 2 * abb[46];
z[55] = 2 * abb[47];
z[56] = z[54] + z[55];
z[57] = -z[34] + z[56];
z[58] = z[53] + -z[57];
z[59] = abb[15] * z[58];
z[60] = abb[26] * z[42];
z[59] = z[59] + z[60];
z[61] = abb[55] * (T(7) / T(2));
z[23] = abb[51] * (T(-3) / T(2)) + -z[23] + -z[37] + z[61];
z[23] = abb[11] * z[23];
z[62] = -z[19] + z[56];
z[63] = -abb[50] + z[62];
z[64] = -abb[49] + z[7];
z[65] = -abb[55] + z[63] + z[64];
z[66] = abb[16] * z[65];
z[67] = 3 * abb[55];
z[68] = abb[45] + abb[48];
z[69] = z[67] + -z[68];
z[69] = -abb[56] + (T(1) / T(2)) * z[69];
z[69] = abb[7] * z[69];
z[70] = -abb[57] + abb[58];
z[71] = abb[17] * z[70];
z[72] = (T(1) / T(2)) * z[71];
z[73] = -abb[56] + z[68];
z[74] = abb[12] * z[73];
z[75] = 2 * z[74];
z[76] = -abb[55] + z[68];
z[77] = abb[1] * z[76];
z[8] = z[8] + z[17] + z[23] + z[32] + -z[41] + -z[50] + -z[59] + z[66] + z[69] + z[72] + -z[75] + (T(3) / T(2)) * z[77];
z[8] = abb[34] * z[8];
z[23] = 6 * abb[46];
z[32] = 6 * abb[47];
z[69] = z[23] + z[32];
z[78] = abb[45] + z[69];
z[79] = 2 * abb[54];
z[80] = z[13] + -z[79];
z[81] = z[78] + z[80];
z[82] = -z[1] + z[36];
z[83] = 5 * abb[55];
z[84] = 4 * abb[53] + z[19] + -z[81] + z[82] + -z[83];
z[84] = abb[9] * z[84];
z[85] = -z[19] + z[34];
z[86] = -abb[48] + abb[55];
z[87] = z[85] + z[86];
z[88] = abb[6] * z[87];
z[89] = z[48] + -z[88];
z[84] = z[84] + z[89];
z[90] = abb[49] + abb[54];
z[91] = 8 * abb[46];
z[92] = 8 * abb[47];
z[93] = z[0] + z[90] + -z[91] + -z[92];
z[36] = z[36] + z[67];
z[29] = -z[29] + z[36] + z[93];
z[29] = abb[5] * z[29];
z[94] = 6 * abb[51];
z[83] = -z[83] + z[94];
z[38] = -z[13] + z[38] + z[83] + z[93];
z[38] = abb[15] * z[38];
z[38] = z[38] + z[60];
z[93] = z[41] + z[77];
z[95] = z[28] + -z[37] + -z[86];
z[95] = abb[11] * z[95];
z[40] = abb[4] * z[40];
z[96] = z[62] + z[76];
z[97] = z[2] + z[96];
z[98] = abb[0] * z[97];
z[29] = -z[29] + z[38] + -z[40] + -z[84] + z[93] + -z[95] + z[98];
z[40] = abb[31] * z[29];
z[98] = -abb[55] + z[7];
z[99] = -z[68] + z[98];
z[100] = abb[7] * z[99];
z[101] = 2 * abb[16];
z[65] = z[65] * z[101];
z[101] = z[65] + z[71];
z[100] = z[100] + -z[101];
z[102] = z[59] + z[100];
z[103] = 2 * abb[49];
z[104] = 4 * abb[56];
z[105] = z[103] + z[104];
z[106] = -z[1] + z[69];
z[107] = 9 * abb[48] + -z[67] + -z[105] + z[106];
z[108] = 3 * abb[45];
z[109] = z[107] + z[108];
z[109] = abb[5] * z[109];
z[110] = abb[54] + z[33];
z[13] = abb[49] + abb[55] + -z[13] + -z[37] + z[110];
z[106] = z[13] + -z[106];
z[111] = abb[0] * z[106];
z[112] = -abb[55] + -z[7] + 3 * z[68];
z[113] = 2 * abb[4];
z[114] = z[112] * z[113];
z[109] = z[41] + z[102] + z[109] + -z[111] + z[114];
z[109] = abb[32] * z[109];
z[111] = 2 * abb[7];
z[114] = z[76] * z[111];
z[115] = abb[4] * z[99];
z[116] = abb[18] * z[70];
z[115] = z[115] + -z[116];
z[114] = z[59] + z[114] + -z[115];
z[117] = abb[5] * z[58];
z[118] = abb[27] * z[42];
z[119] = abb[14] * z[76];
z[118] = z[118] + -z[119];
z[120] = -abb[20] * z[70];
z[121] = z[118] + -z[120];
z[122] = -z[75] + 4 * z[77] + z[121];
z[98] = -abb[48] + z[98];
z[123] = -z[57] + z[98];
z[123] = abb[0] * z[123];
z[117] = -z[114] + z[117] + -z[122] + z[123];
z[123] = abb[33] * z[117];
z[8] = z[8] + z[40] + z[109] + z[123];
z[8] = abb[34] * z[8];
z[40] = z[16] + z[79];
z[123] = abb[55] + z[19];
z[124] = -abb[45] + z[123];
z[125] = 5 * abb[46];
z[126] = abb[48] + z[125];
z[127] = z[40] + -z[55] + -z[124] + -z[126];
z[128] = abb[9] * z[127];
z[129] = -abb[50] + abb[56];
z[130] = abb[48] + -abb[49];
z[45] = -abb[51] + -z[45] + z[56] + z[129] + (T(1) / T(2)) * z[130];
z[45] = abb[5] * z[45];
z[130] = -z[18] + (T(1) / T(2)) * z[68];
z[131] = z[62] + z[130];
z[132] = abb[13] * z[131];
z[133] = (T(1) / T(2)) * z[70];
z[134] = abb[19] * z[133];
z[132] = z[132] + -z[134];
z[134] = abb[21] * z[42];
z[135] = z[120] + z[134];
z[135] = (T(1) / T(2)) * z[135];
z[136] = (T(1) / T(2)) * z[42];
z[136] = abb[27] * z[136];
z[119] = (T(1) / T(2)) * z[119] + -z[136];
z[10] = abb[48] + -abb[51] + z[10];
z[10] = -abb[55] + -abb[56] + 2 * z[10] + z[91];
z[10] = abb[4] * z[10];
z[136] = -abb[7] * z[73];
z[137] = abb[46] + -abb[49];
z[138] = abb[0] * z[137];
z[5] = z[5] + z[10] + z[45] + -z[66] + -z[77] + z[119] + z[128] + -z[132] + z[135] + z[136] + z[138];
z[5] = abb[30] * z[5];
z[10] = abb[45] * (T(1) / T(2));
z[45] = abb[48] * (T(1) / T(2));
z[128] = -z[7] + -z[10] + z[31] + z[45] + z[57];
z[128] = abb[7] * z[128];
z[23] = z[19] + -z[23] + -z[26] + z[53] + z[90];
z[23] = abb[0] * z[23];
z[49] = -z[49] + z[132];
z[53] = -z[120] + z[134];
z[53] = -z[49] + (T(1) / T(2)) * z[53];
z[40] = -7 * abb[46] + -z[26] + z[40] + -z[52];
z[132] = 2 * abb[9];
z[40] = z[40] * z[132];
z[136] = z[40] + z[53];
z[138] = z[59] + -z[65];
z[139] = 2 * z[77];
z[119] = z[119] + -z[139];
z[140] = -z[75] + (T(3) / T(2)) * z[116] + -z[119];
z[106] = abb[5] * z[106];
z[141] = 12 * abb[47];
z[142] = 16 * abb[46] + z[141];
z[143] = 3 * abb[54];
z[144] = abb[49] + z[94] + -z[142] + z[143];
z[145] = abb[48] * (T(-11) / T(2)) + abb[45] * (T(-5) / T(2)) + z[31] + z[104] + z[144];
z[145] = abb[4] * z[145];
z[23] = z[23] + -z[72] + z[106] + z[128] + -z[136] + -z[138] + -z[140] + z[145];
z[23] = abb[32] * z[23];
z[106] = abb[48] * (T(3) / T(2));
z[128] = z[10] + z[106];
z[145] = -z[7] + z[27];
z[20] = z[20] + -z[34] + z[128] + z[145];
z[20] = abb[4] * z[20];
z[31] = -z[31] + z[57] + z[128];
z[31] = abb[7] * z[31];
z[128] = abb[45] + z[85];
z[128] = abb[0] * z[128];
z[146] = z[57] + -z[86];
z[147] = abb[5] * z[146];
z[148] = (T(1) / T(2)) * z[116];
z[149] = (T(3) / T(2)) * z[71];
z[20] = z[20] + z[31] + z[53] + z[59] + -z[119] + -z[128] + z[147] + -z[148] + -z[149];
z[20] = abb[33] * z[20];
z[31] = z[0] + z[83];
z[53] = -z[31] + z[68] + z[69] + -z[105];
z[53] = abb[15] * z[53];
z[69] = abb[53] + z[16] + -z[67];
z[69] = z[69] * z[132];
z[69] = z[69] + z[89];
z[83] = z[2] + z[58];
z[83] = abb[5] * z[83];
z[96] = -abb[0] * z[96];
z[83] = z[53] + z[69] + z[83] + z[96] + z[100] + z[115];
z[83] = abb[31] * z[83];
z[96] = -abb[34] * z[117];
z[5] = z[5] + z[20] + z[23] + z[83] + z[96];
z[5] = abb[30] * z[5];
z[20] = abb[50] + -abb[53];
z[23] = 5 * abb[51];
z[83] = 11 * abb[45] + 23 * abb[48];
z[83] = abb[55] * (T(-23) / T(2)) + z[3] + -z[20] + z[23] + (T(1) / T(2)) * z[83];
z[83] = abb[11] * z[83];
z[96] = z[33] + -z[76];
z[100] = -z[27] + z[96];
z[100] = abb[13] * z[100];
z[105] = abb[19] * z[70];
z[100] = z[100] + z[105] + z[134];
z[105] = -7 * abb[45] + -25 * abb[48];
z[105] = 17 * abb[51] + abb[52] + (T(1) / T(2)) * z[105];
z[115] = 4 * abb[54];
z[105] = -17 * abb[47] + abb[53] + abb[46] * (T(-41) / T(2)) + abb[55] * (T(5) / T(4)) + z[104] + (T(1) / T(2)) * z[105] + z[115];
z[105] = abb[2] * z[105];
z[117] = -z[86] + z[90];
z[117] = -abb[52] + (T(1) / T(2)) * z[117];
z[117] = abb[3] * z[117];
z[43] = (T(-5) / T(2)) * z[43] + -z[46] + -z[47] + z[83] + -z[100] + z[105] + z[117] + -z[118] + z[120];
z[24] = abb[56] + (T(1) / T(3)) * z[24];
z[24] = abb[8] * z[24];
z[46] = -z[18] + z[45];
z[47] = z[21] + -z[37];
z[83] = -abb[52] + z[3];
z[47] = -z[46] + (T(1) / T(3)) * z[47] + (T(1) / T(6)) * z[83];
z[47] = abb[7] * z[47];
z[105] = -abb[45] + z[19];
z[117] = 5 * abb[48];
z[118] = 10 * abb[47];
z[119] = 5 * abb[50] + abb[53] * (T(-13) / T(2)) + z[105] + z[117] + z[118];
z[119] = -5 * abb[56] + abb[55] * (T(23) / T(6)) + abb[46] * (T(29) / T(6)) + -z[79] + (T(1) / T(3)) * z[119];
z[119] = abb[9] * z[119];
z[44] = z[44] + z[116];
z[120] = abb[49] + -abb[51] + z[10];
z[120] = -abb[47] + abb[53] * (T(11) / T(3)) + -z[45] + (T(1) / T(3)) * z[120];
z[120] = abb[55] * (T(-43) / T(12)) + abb[50] * (T(-5) / T(3)) + abb[46] * (T(-5) / T(6)) + abb[54] * (T(1) / T(3)) + abb[56] * (T(11) / T(3)) + (T(1) / T(2)) * z[120];
z[120] = abb[0] * z[120];
z[22] = abb[50] + 8 * abb[51] + -z[22] + z[45];
z[22] = abb[46] * (T(-17) / T(3)) + abb[47] * (T(-9) / T(2)) + abb[54] * (T(5) / T(3)) + -z[18] + (T(1) / T(3)) * z[22];
z[22] = abb[4] * z[22];
z[134] = -abb[49] + abb[55] * (T(-5) / T(12)) + abb[56] * (T(-4) / T(3)) + abb[45] * (T(-1) / T(4)) + abb[54] * (T(2) / T(3)) + abb[51] * (T(5) / T(3)) + abb[48] * (T(7) / T(4)) + (T(-1) / T(3)) * z[83];
z[134] = abb[5] * z[134];
z[22] = z[22] + 5 * z[24] + (T(1) / T(3)) * z[43] + (T(-5) / T(6)) * z[44] + z[47] + -z[72] + (T(11) / T(3)) * z[74] + (T(-17) / T(4)) * z[77] + z[119] + z[120] + z[134];
z[22] = prod_pow(m1_set::bc<T>[0], 2) * z[22];
z[24] = abb[64] * z[29];
z[23] = z[23] + z[104];
z[43] = abb[49] + abb[53];
z[44] = abb[45] * (T(3) / T(2));
z[47] = 5 * abb[47] + -z[23] + -z[43] + z[44] + z[61] + z[106] + z[125];
z[47] = abb[15] * z[47];
z[72] = z[1] + -z[7];
z[45] = z[18] + z[45];
z[106] = z[3] + z[10];
z[119] = abb[53] + abb[54] + -z[28] + -z[45] + -z[72] + -z[103] + z[106];
z[119] = abb[5] * z[119];
z[120] = 2 * abb[55];
z[52] = -z[52] + z[120];
z[64] = z[1] + z[52] + -z[64] + -z[78] + z[110];
z[78] = -abb[4] * z[64];
z[97] = abb[11] * z[97];
z[58] = abb[7] * z[58];
z[43] = -abb[51] + -z[43] + z[45] + z[106];
z[43] = abb[0] * z[43];
z[43] = z[43] + z[47] + z[58] + -z[65] + -z[77] + z[78] + z[84] + -z[97] + z[119];
z[43] = abb[39] * z[43];
z[45] = abb[48] + z[105];
z[47] = 7 * abb[55];
z[45] = 13 * abb[46] + 5 * abb[53] + z[26] + 2 * z[45] + -z[47] + -z[115];
z[45] = abb[9] * z[45];
z[58] = abb[55] + z[33];
z[78] = -z[58] + -z[115] + z[117];
z[82] = -abb[45] + -18 * abb[46] + -z[78] + z[82] + -z[141];
z[82] = abb[4] * z[82];
z[105] = z[55] + -z[90];
z[106] = -abb[53] + -z[105] + z[120] + -z[126];
z[106] = abb[0] * z[106];
z[81] = z[81] + -z[123];
z[110] = -z[0] + z[81];
z[117] = abb[5] * z[110];
z[88] = z[88] + z[100] + z[116];
z[38] = -z[38] + z[45] + z[82] + -z[88] + -z[97] + z[106] + -z[117];
z[45] = abb[63] * z[38];
z[82] = z[6] + z[129];
z[97] = 2 * abb[52];
z[4] = abb[45] + abb[48] * (T(5) / T(2)) + -z[4] + z[21] + z[27] + -z[28] + z[82] + -z[97];
z[4] = abb[5] * z[4];
z[21] = abb[45] + -abb[49];
z[6] = -z[6] + z[11] + (T(1) / T(2)) * z[21] + z[55] + -z[86];
z[6] = abb[0] * z[6];
z[11] = z[39] + z[108];
z[106] = z[15] + z[79];
z[92] = 10 * abb[46] + z[92];
z[18] = (T(1) / T(2)) * z[11] + -z[18] + -z[33] + z[92] + -z[106];
z[18] = abb[4] * z[18];
z[117] = -abb[48] + z[67];
z[119] = -z[90] + z[117];
z[119] = abb[52] + -abb[56] + (T(1) / T(2)) * z[119];
z[119] = abb[7] * z[119];
z[50] = z[50] + z[66] + z[119];
z[16] = -9 * abb[46] + z[16] + -z[32] + -z[80] + z[124];
z[16] = abb[9] * z[16];
z[66] = 11 * abb[46] + 4 * abb[48] + abb[49] + z[37] + z[118];
z[23] = abb[52] + -z[23] + z[66] + -z[143];
z[23] = abb[2] * z[23];
z[90] = -z[90] + z[97];
z[118] = z[86] + z[90];
z[119] = abb[3] * z[118];
z[4] = z[4] + z[6] + z[16] + z[18] + z[23] + -z[50] + z[59] + z[119] + z[148];
z[4] = abb[35] * z[4];
z[6] = abb[5] * z[64];
z[16] = -z[51] + -z[58] + z[80] + z[92];
z[18] = z[16] * z[113];
z[23] = z[48] + z[100];
z[48] = z[25] + z[105];
z[51] = z[48] + -z[86];
z[51] = abb[0] * z[51];
z[6] = -z[6] + z[18] + z[23] + z[40] + z[51] + z[102];
z[18] = abb[30] + -abb[32];
z[40] = -z[6] * z[18];
z[51] = -z[7] + z[90] + z[117];
z[58] = abb[7] * z[51];
z[2] = -z[2] + z[51];
z[2] = abb[5] * z[2];
z[51] = 3 * abb[49] + -abb[54] + -z[145];
z[31] = abb[48] + z[31] + z[51];
z[31] = abb[15] * z[31];
z[2] = z[2] + z[31] + z[58] + -z[60] + z[65] + -z[69] + -z[119];
z[2] = abb[31] * z[2];
z[31] = z[56] + -z[97] + z[103];
z[14] = z[14] + z[31] + -z[67];
z[14] = abb[5] * z[14];
z[56] = abb[0] * z[146];
z[14] = z[14] + z[56] + z[114] + z[119];
z[58] = -abb[33] * z[14];
z[59] = abb[55] + -z[66] + z[94] + z[106];
z[64] = z[18] * z[59];
z[65] = abb[51] + abb[52] + -abb[54];
z[66] = -abb[55] + abb[56];
z[69] = -z[65] + z[66];
z[69] = abb[31] * z[69];
z[80] = abb[33] * z[66];
z[64] = z[64] + z[69] + z[80];
z[69] = 2 * abb[2];
z[64] = z[64] * z[69];
z[2] = z[2] + z[4] + z[40] + z[58] + z[64];
z[2] = abb[35] * z[2];
z[4] = z[1] + -z[37] + -z[39] + z[51] + z[67];
z[4] = abb[5] * z[4];
z[37] = -z[7] + -z[67] + 5 * z[68];
z[37] = abb[4] * z[37];
z[40] = -abb[49] + abb[56] + z[63] + z[76];
z[51] = 2 * abb[0];
z[40] = z[40] * z[51];
z[58] = abb[7] * z[112];
z[4] = -z[4] + z[37] + z[40] + z[41] + -z[58] + -z[101] + -z[116] + -z[122];
z[37] = abb[65] * z[4];
z[26] = -z[26] + -z[54] + z[96] + -z[103];
z[26] = abb[4] * z[26];
z[31] = -z[31] + -z[86] + z[108];
z[31] = abb[7] * z[31];
z[40] = -z[51] * z[137];
z[54] = -abb[5] * z[118];
z[23] = -z[23] + z[26] + z[31] + z[40] + z[54] + z[71] + z[116] + z[119];
z[23] = abb[38] * z[23];
z[26] = -abb[32] * z[16];
z[31] = z[19] + -z[55];
z[40] = abb[46] + abb[49];
z[54] = -z[31] + z[40] + z[73];
z[58] = abb[33] * z[54];
z[26] = z[26] + z[58];
z[63] = abb[30] * z[16];
z[26] = 2 * z[26] + z[63];
z[26] = abb[30] * z[26];
z[55] = z[55] + z[97];
z[63] = 4 * abb[45];
z[33] = -5 * abb[49] + z[33] + z[52] + -z[55] + -z[63] + z[125];
z[33] = abb[38] * z[33];
z[52] = prod_pow(abb[32], 2);
z[64] = z[16] * z[52];
z[40] = -z[40] + -z[55] + -z[76] + z[79];
z[40] = abb[33] * z[40];
z[54] = -abb[32] * z[54];
z[55] = -abb[31] * z[66];
z[54] = z[54] + z[55];
z[40] = z[40] + 2 * z[54];
z[40] = abb[33] * z[40];
z[11] = -24 * abb[46] + -18 * abb[47] + 10 * abb[51] + -z[0] + -z[11] + z[36] + z[115];
z[36] = abb[63] * z[11];
z[54] = z[7] + z[65] + -z[120];
z[54] = prod_pow(abb[31], 2) * z[54];
z[12] = abb[48] + -abb[53] + z[12];
z[12] = abb[39] * z[12];
z[55] = abb[37] * z[65];
z[12] = z[12] + z[55];
z[12] = 2 * z[12] + z[26] + z[33] + z[36] + z[40] + z[54] + z[64];
z[12] = abb[2] * z[12];
z[7] = z[7] + z[13] + -z[32] + -z[91];
z[7] = abb[0] * z[7];
z[9] = -abb[49] + z[9] + z[25] + -z[108];
z[9] = z[9] * z[113];
z[13] = -abb[45] + z[57];
z[25] = -abb[7] * z[13];
z[26] = -abb[5] * z[112];
z[7] = z[7] + z[9] + z[25] + z[26] + z[71] + z[75] + z[77];
z[7] = z[7] * z[52];
z[9] = abb[20] * z[76];
z[25] = abb[28] * z[42];
z[26] = abb[13] * z[70];
z[9] = -z[9] + z[25] + z[26];
z[10] = z[10] + -z[46];
z[25] = z[10] + -z[57];
z[25] = abb[17] * z[25];
z[26] = z[44] + z[46];
z[32] = z[26] + z[85];
z[32] = abb[18] * z[32];
z[33] = abb[19] * z[131];
z[36] = abb[4] + abb[7] + -abb[14];
z[36] = z[36] * z[133];
z[40] = abb[5] + 2 * abb[29];
z[40] = z[40] * z[70];
z[9] = (T(-1) / T(2)) * z[9] + z[25] + z[32] + z[33] + z[36] + z[40];
z[25] = abb[66] * z[9];
z[0] = z[0] + -z[30] + z[61] + z[82];
z[0] = abb[5] * z[0];
z[3] = abb[51] + -z[3] + -z[130];
z[3] = abb[0] * z[3];
z[30] = abb[51] + -z[86];
z[30] = abb[11] * z[30];
z[0] = z[0] + z[3] + z[17] + (T(9) / T(2)) * z[30] + -z[50] + z[53] + (T(-1) / T(2)) * z[77];
z[0] = abb[31] * z[0];
z[0] = z[0] + -z[109];
z[0] = abb[31] * z[0];
z[3] = z[10] + z[19] + -z[27] + z[34];
z[3] = abb[4] * z[3];
z[10] = z[26] + -z[57];
z[10] = abb[7] * z[10];
z[10] = z[10] + z[140] + z[149];
z[17] = -z[62] + z[99];
z[17] = abb[0] * z[17];
z[19] = abb[5] * z[99];
z[3] = -z[3] + -z[10] + z[17] + z[19] + -z[49] + z[135];
z[17] = -abb[32] * z[3];
z[19] = z[21] + -z[83];
z[19] = z[19] * z[111];
z[13] = -abb[4] * z[13];
z[21] = abb[5] * z[76];
z[13] = z[13] + z[19] + z[21] + z[56] + -z[77] + z[116];
z[13] = abb[33] * z[13];
z[14] = abb[31] * z[14];
z[13] = z[13] + z[14] + z[17];
z[13] = abb[33] * z[13];
z[14] = abb[15] * z[87];
z[14] = z[14] + z[60] + z[89];
z[17] = abb[7] * z[76];
z[17] = -z[14] + -z[17] + z[71] + -z[121] + z[128] + -z[139];
z[17] = abb[36] * z[17];
z[19] = -abb[5] + -abb[7];
z[19] = z[19] * z[118];
z[14] = z[14] + z[19] + z[119];
z[14] = abb[37] * z[14];
z[0] = abb[67] + z[0] + z[2] + z[5] + z[7] + z[8] + z[12] + z[13] + z[14] + z[17] + z[22] + z[23] + z[24] + z[25] + z[37] + z[43] + z[45];
z[2] = -z[127] * z[132];
z[1] = z[1] + -z[81];
z[1] = abb[5] * z[1];
z[5] = -abb[45] + z[104];
z[7] = z[5] + -z[78] + -z[142];
z[7] = abb[4] * z[7];
z[8] = -z[66] * z[111];
z[12] = -z[48] + z[98];
z[12] = abb[0] * z[12];
z[1] = z[1] + z[2] + z[7] + z[8] + z[12] + z[75] + -z[88] + -z[138] + -z[139];
z[1] = abb[30] * z[1];
z[2] = z[31] + z[72] + -z[76];
z[2] = abb[0] * z[2];
z[5] = 6 * abb[48] + -z[5] + z[35] + -z[120];
z[7] = -abb[5] * z[5];
z[8] = abb[45] * (T(-7) / T(2)) + -z[46] + -z[144];
z[8] = abb[4] * z[8];
z[2] = z[2] + z[7] + z[8] + z[10] + -z[41] + z[136];
z[2] = abb[32] * z[2];
z[7] = z[28] + z[107];
z[7] = abb[5] * z[7];
z[5] = abb[4] * z[5];
z[5] = z[5] + z[7] + z[102] + -z[116];
z[7] = z[28] + z[39] + -z[47] + z[63];
z[7] = abb[11] * z[7];
z[8] = z[15] + -2 * z[20] + -z[67];
z[8] = z[8] * z[51];
z[7] = z[5] + z[7] + z[8] + 2 * z[41] + 6 * z[74] + -7 * z[77] + -z[84] + -z[121];
z[7] = abb[34] * z[7];
z[3] = -abb[33] * z[3];
z[8] = -z[59] * z[69];
z[6] = z[6] + z[8];
z[6] = abb[35] * z[6];
z[8] = -abb[9] * z[110];
z[10] = z[20] * z[51];
z[5] = -z[5] + z[8] + z[10] + -z[93] + z[95];
z[5] = abb[31] * z[5];
z[8] = -z[16] * z[18];
z[8] = z[8] + -z[58];
z[8] = z[8] * z[69];
z[1] = z[1] + z[2] + z[3] + z[5] + z[6] + z[7] + z[8];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[2] * z[11];
z[2] = z[2] + z[38];
z[2] = abb[40] * z[2];
z[3] = abb[41] * z[29];
z[4] = abb[42] * z[4];
z[5] = abb[43] * z[9];
z[1] = abb[44] + z[1] + z[2] + z[3] + z[4] + z[5];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_577_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("8.332498121433851188010996181799423035828358241979703065562506468"),stof<T>("17.241546354651752069069038392202319790094035965076567946230686824")}, std::complex<T>{stof<T>("35.867514236241937117296348974423050581565048884433055202811141568"),stof<T>("-22.913293390406915818405194664515597463858627704694437487583215512")}, std::complex<T>{stof<T>("26.346632171856277587834222287852908921375951357309876579016298523"),stof<T>("-28.511072328997372750406537606604159245572935863804537959215386546")}, std::complex<T>{stof<T>("-11.3560503402535897956907404439725713973466117930928033960245692895"),stof<T>("-2.7808208250222644861955015119160630039936777321233131912797027779")}, std::complex<T>{stof<T>("-13.0791276248259685854525114581324865046196569498088903222261749412"),stof<T>("2.940284387940533941694333355713752871325454524409409993910466771")}, std::complex<T>{stof<T>("0.317186848142956329952458332513727652790920508079934578261253139"),stof<T>("35.793852001516653633431014340104211747579453332720773754041011732")}, std::complex<T>{stof<T>("-49.78955460706360956085254946454257034343409502282257236269267668"),stof<T>("5.309587799836373706758252383626894151506719711333346569852285092")}, std::complex<T>{stof<T>("-0.121058435063374396626252553158405508338306300325456954603282636"),stof<T>("-24.076671325498680271351592723443014794561188193929953036645662074")}, std::complex<T>{stof<T>("3.7543739735198909893165905509176669888831736304401893220893023994"),stof<T>("3.1791173494869824883837453188588822999785024552713102518527118527")}, std::complex<T>{stof<T>("10.154671958433028074268771664757912581959741584848941289528241227"),stof<T>("21.577160551715980809161953953126380294302481694298566217435649366")}, std::complex<T>{stof<T>("1.00524996874097978809576299985933667093425600048938448283835756"),stof<T>("-30.513520402711689685045874057871514243327069100966073743551296246")}, std::complex<T>{stof<T>("1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("-7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("-1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("-3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("0.2599334236531954369552408109471828770159706361072270565126747634")}, std::complex<T>{stof<T>("-3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("0.2599334236531954369552408109471828770159706361072270565126747634")}, std::complex<T>{stof<T>("-3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("0.2599334236531954369552408109471828770159706361072270565126747634")}, std::complex<T>{stof<T>("-3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("0.2599334236531954369552408109471828770159706361072270565126747634")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[22].real()/kbase.W[22].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[82].real()/kbase.W[82].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[124].real()/kbase.W[124].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_577_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_577_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,4> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (56 + 39 * v[0] + 5 * v[1] + 15 * v[2] + -13 * v[3] + -28 * v[4] + -15 * v[5]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (v[0] + -v[1] + v[2] + v[3] + 2 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -v[5])) / prod_pow(tend, 2);
c[2] = ((T(9) / T(2)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;
c[3] = (2 * (1 + m1_set::bc<T>[1]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return t * (abb[46] * c[0] + -abb[49] * c[0] + (abb[47] + abb[49] + abb[52] + -abb[54]) * c[1]) + abb[46] * c[2] + abb[47] * c[3] + abb[52] * c[3] + -abb[54] * c[3] + abb[49] * (-c[2] + c[3]);
	}
	{
T z[4];
z[0] = abb[46] + -abb[49];
z[1] = abb[30] + -abb[32];
z[0] = z[0] * z[1];
z[0] = 2 * z[0];
z[1] = abb[47] + abb[52] + -abb[54];
z[2] = abb[46] + abb[49] + 2 * z[1];
z[3] = -abb[35] * z[2];
z[3] = -z[0] + z[3];
z[3] = abb[35] * z[3];
z[2] = abb[33] * z[2];
z[0] = z[0] + z[2];
z[0] = abb[33] * z[0];
z[1] = -9 * abb[46] + 5 * abb[49] + -4 * z[1];
z[1] = abb[38] * z[1];
z[0] = z[0] + z[1] + z[3];
return abb[10] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_577_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[46] + -abb[49]) * (t * c[0] + -c[1]);
	}
	{
T z[2];
z[0] = abb[46] + -abb[49];
z[1] = -abb[33] + abb[35];
return 2 * abb[10] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_577_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("21.885902269420221292189670162401813607724404598929906615988465686"),stof<T>("8.793898342292785441448452689613961441952168150055060183613968757")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,68> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W17(k,dl), dlog_W23(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W83(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_15(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), f_2_11_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), IntDLogL_W_17(t,k,kend,dl), rlog(kend.W[22].real()/k.W[22].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[82].real()/k.W[82].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[124].real()/k.W[124].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_2_re(k), f_2_10_re(k), f_2_11_re(k), f_2_25_re(k), T{0}};
abb[44] = SpDLog_f_4_577_W_17_Im(t, path, abb);
abb[67] = SpDLog_f_4_577_W_17_Re(t, path, abb);

                    
            return f_4_577_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_577_DLogXconstant_part(base_point<T>, kend);
	value += f_4_577_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_577_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_577_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_577_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_577_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_577_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_577_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
