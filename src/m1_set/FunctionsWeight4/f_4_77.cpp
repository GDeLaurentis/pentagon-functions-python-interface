/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_77.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_77_abbreviated (const std::array<T,29>& abb) {
T z[20];
z[0] = -abb[20] + abb[25];
z[1] = -abb[23] + z[0];
z[1] = abb[0] * z[1];
z[2] = -abb[24] + abb[25];
z[3] = abb[20] + z[2];
z[3] = abb[1] * z[3];
z[4] = z[1] + -z[3];
z[5] = 2 * abb[25];
z[6] = abb[21] + abb[24];
z[7] = z[5] + -z[6];
z[8] = abb[8] * z[7];
z[9] = abb[22] + abb[23];
z[10] = abb[2] * (T(1) / T(6));
z[11] = z[9] * z[10];
z[2] = abb[22] + z[2];
z[2] = abb[4] * z[2];
z[12] = abb[6] * z[6];
z[10] = -2 * abb[6] + -z[10];
z[10] = abb[25] * z[10];
z[4] = (T(1) / T(6)) * z[2] + (T(13) / T(6)) * z[4] + z[8] + z[10] + z[11] + z[12];
z[4] = prod_pow(m1_set::bc<T>[0], 2) * z[4];
z[6] = -abb[25] + (T(1) / T(2)) * z[6];
z[10] = abb[3] * z[6];
z[11] = (T(1) / T(2)) * z[2] + z[10];
z[13] = abb[2] + -abb[6];
z[13] = z[5] * z[13];
z[6] = abb[9] * z[6];
z[9] = abb[2] * z[9];
z[9] = -z[6] + -2 * z[9] + -z[11] + z[12] + z[13];
z[9] = prod_pow(abb[13], 2) * z[9];
z[5] = abb[6] * z[5];
z[5] = z[5] + -z[12];
z[12] = (T(1) / T(2)) * z[3] + z[6];
z[0] = -abb[21] + z[0];
z[0] = abb[5] * z[0];
z[13] = (T(3) / T(2)) * z[0] + -2 * z[1] + z[5] + z[10] + z[12];
z[13] = abb[10] * z[13];
z[14] = z[0] + -z[3];
z[15] = -abb[8] + abb[9];
z[15] = z[7] * z[15];
z[15] = -z[14] + z[15];
z[15] = abb[12] * z[15];
z[16] = abb[3] + abb[8];
z[17] = 2 * abb[9];
z[18] = z[16] + -z[17];
z[18] = z[7] * z[18];
z[19] = abb[11] * z[18];
z[13] = z[13] + z[15] + z[19];
z[13] = abb[10] * z[13];
z[15] = -abb[12] * z[18];
z[6] = 3 * z[6] + z[8] + -z[11];
z[6] = abb[11] * z[6];
z[8] = -abb[3] + abb[9];
z[8] = z[7] * z[8];
z[2] = -z[2] + z[8];
z[11] = abb[13] * z[2];
z[6] = z[6] + z[11] + z[15];
z[6] = abb[11] * z[6];
z[11] = -abb[26] * z[7];
z[15] = abb[27] * z[7];
z[15] = z[11] + z[15];
z[15] = z[15] * z[17];
z[17] = abb[16] * z[18];
z[10] = (T(-1) / T(2)) * z[0] + -z[10] + z[12];
z[10] = prod_pow(abb[12], 2) * z[10];
z[8] = z[8] + z[14];
z[8] = abb[14] * z[8];
z[11] = -z[11] * z[16];
z[12] = z[7] * z[16];
z[14] = -abb[27] * z[12];
z[2] = -abb[15] * z[2];
z[2] = abb[28] + z[2] + z[4] + z[6] + z[8] + z[9] + z[10] + z[11] + z[13] + z[14] + z[15] + z[17];
z[4] = -z[0] + -z[5];
z[3] = 2 * z[3];
z[1] = 4 * z[1] + -z[3] + 2 * z[4] + z[12];
z[1] = abb[10] * z[1];
z[4] = -abb[3] + abb[8];
z[4] = z[4] * z[7];
z[0] = 2 * z[0] + -z[3] + z[4];
z[0] = abb[12] * z[0];
z[0] = z[0] + z[1];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[17] + -abb[18];
z[1] = z[1] * z[18];
z[0] = abb[19] + z[0] + z[1];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_77_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-7.63044610169493535091196880111479307520521581628708430584049572"),stof<T>("38.858042149405861786122438198413189216559838772369452802595963388")}, std::complex<T>{stof<T>("-4.9697883383752481730624831771813899881226763488320975655959259892"),stof<T>("-3.3446858857911688637157672710702281795850663451075500060920289537")}, stof<T>("-10.985610348401872835606518781619741936601429416034159103864468432"), std::complex<T>{stof<T>("0.918164975983566437662010803917862315779212184465581150959880746"),stof<T>("25.838563900249850146230262555917829335187740471570923078453781753")}, std::complex<T>{stof<T>("14.564433087705126451118015209471007339463181067954726995068918909"),stof<T>("-16.364164134947180503607942913565588060957164645906079730234210588")}, std::complex<T>{stof<T>("-10.5128097253134447157175428362074796671197169035882105804328736658"),stof<T>("-6.1297138795115007789065523712820130946455094805572933421275422114")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[27].real()/kbase.W[27].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_77_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_77_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(16)) * (-v[3] + v[5]) * (-8 + -v[3] + -3 * v[5] + 4 * m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = ((1 + 2 * m1_set::bc<T>[1]) * (T(-1) / T(2)) * (-v[3] + v[5])) / tend;


		return (abb[21] + abb[22] + -abb[25]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[11], 2);
z[1] = -abb[11] + abb[13] * (T(3) / T(2));
z[1] = abb[13] * z[1];
z[0] = abb[15] + (T(-1) / T(2)) * z[0] + z[1];
z[1] = abb[21] + abb[22] + -abb[25];
return abb[7] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_77_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_77_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("25.062198349128753332405720419107074466511047088032165960801759704"),stof<T>("-10.532401933342117029128547768612955572278494556724271365979172144")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dl[1], dl[5], dlog_W8(k,dl), dl[3], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_2_3(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), T{0}, rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[27].real()/k.W[27].real()), f_2_4_re(k), f_2_6_re(k), T{0}};
abb[19] = SpDLog_f_4_77_W_16_Im(t, path, abb);
abb[28] = SpDLog_f_4_77_W_16_Re(t, path, abb);

                    
            return f_4_77_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_77_DLogXconstant_part(base_point<T>, kend);
	value += f_4_77_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_77_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_77_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_77_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_77_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_77_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_77_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
