/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_141.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_141_abbreviated (const std::array<T,29>& abb) {
T z[46];
z[0] = 2 * abb[23];
z[1] = 3 * abb[19] + z[0];
z[2] = abb[18] + -abb[20];
z[3] = 2 * z[2];
z[4] = 3 * abb[21];
z[5] = abb[22] + -z[1] + z[3] + z[4];
z[6] = abb[5] * z[5];
z[7] = abb[19] * (T(3) / T(2));
z[8] = abb[21] * (T(3) / T(2));
z[9] = z[7] + -z[8];
z[10] = -abb[23] + z[2];
z[11] = abb[22] * (T(1) / T(2));
z[12] = z[10] + z[11];
z[13] = z[9] + -z[12];
z[14] = abb[6] * z[13];
z[15] = 2 * abb[21];
z[16] = -abb[23] + z[15];
z[17] = abb[19] * (T(1) / T(2));
z[18] = 3 * z[2];
z[19] = -abb[22] + z[18];
z[19] = z[16] + -z[17] + (T(1) / T(4)) * z[19];
z[19] = abb[0] * z[19];
z[20] = -abb[23] + z[8];
z[21] = -abb[22] + z[2];
z[22] = z[20] + (T(1) / T(4)) * z[21];
z[23] = -abb[3] * z[22];
z[24] = abb[21] + z[2];
z[25] = -abb[19] + z[24];
z[25] = abb[2] * z[25];
z[26] = abb[24] + -abb[26];
z[27] = -abb[25] + (T(-1) / T(2)) * z[26];
z[28] = abb[7] * z[27];
z[29] = (T(3) / T(2)) * z[28];
z[30] = abb[19] + -abb[22];
z[31] = abb[21] + z[30];
z[32] = abb[1] * z[31];
z[19] = z[6] + z[14] + z[19] + z[23] + (T(-3) / T(2)) * z[25] + -z[29] + -z[32];
z[19] = abb[12] * z[19];
z[23] = 5 * abb[22];
z[33] = z[2] + z[23];
z[33] = -abb[23] + -z[7] + (T(1) / T(4)) * z[33];
z[33] = abb[0] * z[33];
z[3] = -z[0] + z[3];
z[34] = 3 * abb[22];
z[35] = 5 * abb[19] + -abb[21] + -z[3] + -z[34];
z[36] = abb[1] * z[35];
z[33] = z[33] + z[36];
z[37] = 2 * z[31];
z[38] = abb[5] * z[37];
z[38] = z[14] + z[38];
z[39] = abb[21] * (T(1) / T(2));
z[40] = 2 * abb[19];
z[41] = 7 * abb[22] + z[2];
z[41] = -abb[23] + -z[39] + -z[40] + (T(1) / T(4)) * z[41];
z[41] = abb[3] * z[41];
z[7] = z[2] + -z[7] + z[11];
z[42] = z[7] + z[39];
z[42] = abb[2] * z[42];
z[43] = abb[8] * z[27];
z[29] = z[29] + -z[33] + z[38] + z[41] + z[42] + z[43];
z[29] = abb[11] * z[29];
z[19] = z[19] + z[29];
z[19] = abb[12] * z[19];
z[29] = -z[2] + z[34];
z[16] = -z[16] + -z[17] + (T(1) / T(4)) * z[29];
z[16] = abb[0] * z[16];
z[29] = abb[2] * z[31];
z[28] = -z[28] + z[29];
z[28] = (T(-1) / T(2)) * z[28] + z[43];
z[29] = -abb[19] + 5 * abb[21] + -abb[22] + z[3];
z[29] = abb[5] * z[29];
z[41] = abb[1] * z[37];
z[42] = -abb[23] + abb[21] * (T(7) / T(2)) + (T(5) / T(4)) * z[21];
z[42] = abb[3] * z[42];
z[16] = -z[14] + z[16] + -z[28] + -z[29] + z[41] + z[42];
z[16] = abb[12] * z[16];
z[41] = 5 * z[2];
z[34] = -z[34] + -z[41];
z[34] = -z[20] + (T(1) / T(4)) * z[34] + z[40];
z[34] = abb[3] * z[34];
z[28] = -z[14] + z[28] + z[33] + z[34];
z[28] = abb[11] * z[28];
z[33] = abb[6] * z[5];
z[34] = abb[3] * z[37];
z[29] = z[29] + -z[33] + -z[34];
z[34] = 7 * abb[19];
z[3] = -abb[21] + z[3] + z[23] + -z[34];
z[3] = abb[1] * z[3];
z[23] = abb[0] * z[37];
z[3] = z[3] + z[23] + z[29];
z[23] = abb[14] * z[3];
z[37] = abb[22] + z[2];
z[42] = -abb[19] + (T(1) / T(2)) * z[37];
z[44] = -abb[0] * z[42];
z[45] = abb[21] + (T(1) / T(2)) * z[21];
z[45] = abb[2] * z[45];
z[32] = -z[32] + -z[43] + z[44] + z[45];
z[32] = abb[13] * z[32];
z[16] = z[16] + z[23] + z[28] + z[32];
z[16] = abb[13] * z[16];
z[8] = -z[0] + z[8];
z[23] = 4 * abb[22];
z[28] = abb[19] * (T(-15) / T(2)) + (T(7) / T(2)) * z[2] + z[8] + z[23];
z[28] = abb[1] * z[28];
z[32] = abb[19] * (T(7) / T(2));
z[43] = abb[22] * (T(5) / T(2)) + z[10] + -z[32] + -z[39];
z[43] = abb[3] * z[43];
z[44] = abb[23] + z[30];
z[44] = abb[0] * z[44];
z[28] = z[28] + z[38] + z[43] + z[44];
z[28] = abb[14] * z[28];
z[31] = abb[5] * z[31];
z[31] = z[31] + z[44];
z[31] = 2 * z[31] + -z[33];
z[4] = 4 * abb[23] + -z[4];
z[38] = -11 * abb[19] + 6 * abb[22] + -z[4] + z[41];
z[38] = abb[1] * z[38];
z[35] = abb[3] * z[35];
z[35] = z[31] + -z[35] + z[38];
z[38] = -abb[11] * z[35];
z[41] = -abb[12] * z[3];
z[28] = z[28] + z[38] + z[41];
z[28] = abb[14] * z[28];
z[7] = z[7] + z[20];
z[7] = abb[9] * z[7];
z[9] = -abb[22] + z[0] + (T(-1) / T(2)) * z[2] + z[9];
z[9] = abb[8] * z[9];
z[20] = abb[3] * (T(-3) / T(2)) + abb[0] * (T(-1) / T(2));
z[20] = z[20] * z[27];
z[27] = abb[2] + -abb[10];
z[26] = 2 * abb[25] + z[26];
z[27] = z[26] * z[27];
z[22] = -abb[7] * z[22];
z[7] = z[7] + z[9] + z[20] + z[22] + z[27];
z[7] = abb[15] * z[7];
z[9] = 2 * abb[22];
z[8] = (T(3) / T(2)) * z[2] + z[8] + z[9] + -z[32];
z[8] = abb[1] * z[8];
z[13] = -abb[3] * z[13];
z[8] = z[8] + z[13] + z[14] + z[44];
z[13] = prod_pow(abb[11], 2);
z[8] = z[8] * z[13];
z[14] = abb[21] * (T(5) / T(2)) + z[10] + -z[11] + -z[17];
z[14] = abb[5] * z[14];
z[20] = -z[37] + z[40];
z[22] = abb[4] * z[20];
z[0] = abb[21] + -z[0] + -z[30];
z[0] = abb[0] * z[0];
z[0] = z[0] + z[14] + z[22];
z[14] = abb[19] + -abb[21];
z[2] = abb[23] + (T(-2) / T(3)) * z[2] + -z[11] + (T(7) / T(6)) * z[14];
z[2] = abb[1] * z[2];
z[11] = (T(1) / T(3)) * z[12] + -z[17] + z[39];
z[11] = abb[6] * z[11];
z[10] = abb[19] + -z[10] + -z[15];
z[10] = abb[3] * z[10];
z[0] = (T(1) / T(3)) * z[0] + z[2] + (T(2) / T(3)) * z[10] + z[11];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[2] = -z[4] + z[18] + z[23] + -z[34];
z[2] = abb[1] * z[2];
z[4] = abb[3] * z[5];
z[4] = z[2] + z[4] + -z[22] + -z[33] + 2 * z[44];
z[5] = -abb[27] * z[4];
z[10] = -abb[28] * z[3];
z[11] = z[13] * z[42];
z[12] = -abb[11] * z[20];
z[13] = abb[14] * z[42];
z[12] = z[12] + -3 * z[13];
z[12] = abb[14] * z[12];
z[11] = z[11] + z[12];
z[11] = abb[4] * z[11];
z[0] = z[0] + z[5] + z[7] + z[8] + z[10] + z[11] + z[16] + z[19] + z[28];
z[5] = z[15] + z[21];
z[7] = -abb[0] * z[5];
z[8] = abb[3] * z[20];
z[10] = -abb[7] * z[26];
z[6] = -z[6] + z[7] + z[8] + z[10] + 2 * z[25] + z[33] + z[36];
z[6] = abb[12] * z[6];
z[1] = z[1] + -z[9] + -z[24];
z[1] = abb[3] * z[1];
z[7] = abb[7] + abb[8];
z[7] = z[7] * z[26];
z[8] = abb[2] * z[20];
z[1] = z[1] + -z[2] + z[7] + z[8] + -z[31];
z[1] = abb[11] * z[1];
z[2] = abb[14] * z[35];
z[7] = abb[0] + -abb[2];
z[5] = z[5] * z[7];
z[7] = -abb[8] * z[26];
z[5] = z[5] + z[7] + z[29] + -z[36];
z[5] = abb[13] * z[5];
z[7] = abb[11] + abb[14];
z[7] = z[7] * z[22];
z[1] = z[1] + z[2] + z[5] + z[6] + z[7];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[16] * z[4];
z[3] = -abb[17] * z[3];
z[1] = z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_141_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-4.3080130294798100297032187444824170648041567248677161988047028117"),stof<T>("-6.667834103490437016712743283272233831533282238691846164464730301")}, std::complex<T>{stof<T>("5.1685047525126802049137448023017476974723701710487284699223535932"),stof<T>("8.8697518978216205340721591248574844202357208555319408500955593207")}, std::complex<T>{stof<T>("4.3080130294798100297032187444824170648041567248677161988047028117"),stof<T>("6.667834103490437016712743283272233831533282238691846164464730301")}, std::complex<T>{stof<T>("-19.515523587473482564954655745381223709656874673017669308926928238"),stof<T>("-13.899594857300568574446238187513152637824582497307720674592411335")}, std::complex<T>{stof<T>("-0.8604917230328701752105260578193306326682134461810122711176507815"),stof<T>("-2.2019177943311835173594158415852505887024386168400946856308290197")}, std::complex<T>{stof<T>("16.068002281026542710461963058718137277520931394330965381239876208"),stof<T>("9.433678548141315075092910745826169394993738875455969195758510053")}, std::complex<T>{stof<T>("-0.94645826884586151334932252788511187146196813235744440321885439"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("-1.89291653769172302669864505577022374292393626471488880643770878"),stof<T>("15.748931692217380713457583612152853300165062016478596541063558734")}, std::complex<T>{stof<T>("0.94645826884586151334932252788511187146196813235744440321885439"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[46].real()/kbase.W[46].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_141_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_141_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("17.477038949176712724826996413913032050783717973894922013598557238"),stof<T>("-15.828075831849186499478378227964838234362390539565122199534003401")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dlog_W4(k,dl), dl[2], dl[4], dlog_W14(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_11(k), f_2_23(k), f_2_2_im(k), f_2_12_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[46].real()/k.W[46].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_2_re(k), f_2_12_re(k)};

                    
            return f_4_141_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_141_DLogXconstant_part(base_point<T>, kend);
	value += f_4_141_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_141_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_141_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_141_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_141_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_141_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_141_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
