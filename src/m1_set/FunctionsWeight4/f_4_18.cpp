/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_18.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_18_abbreviated (const std::array<T,24>& abb) {
T z[22];
z[0] = prod_pow(abb[13], 2);
z[1] = -abb[18] + abb[22] * (T(1) / T(2));
z[2] = z[0] * z[1];
z[3] = abb[17] * abb[23];
z[2] = -z[2] + (T(3) / T(2)) * z[3];
z[2] = (T(1) / T(2)) * z[2];
z[4] = abb[13] * (T(1) / T(2));
z[5] = z[1] * z[4];
z[6] = -abb[18] + abb[21];
z[7] = abb[14] * z[6];
z[5] = z[5] + 2 * z[7];
z[5] = abb[14] * z[5];
z[8] = -abb[13] + abb[14];
z[9] = z[1] * z[8];
z[9] = (T(1) / T(2)) * z[9];
z[10] = -abb[11] * z[1];
z[10] = -z[9] + z[10];
z[10] = abb[11] * z[10];
z[11] = prod_pow(m1_set::bc<T>[0], 2);
z[12] = (T(13) / T(6)) * z[11];
z[6] = -z[6] * z[12];
z[5] = -z[2] + z[5] + z[6] + z[10];
z[5] = abb[3] * z[5];
z[4] = -abb[14] + z[4];
z[4] = abb[14] * z[1] * z[4];
z[6] = -abb[18] + abb[20];
z[10] = abb[11] * z[6];
z[9] = -z[9] + 2 * z[10];
z[9] = abb[11] * z[9];
z[6] = -z[6] * z[12];
z[2] = -z[2] + z[4] + z[6] + z[9];
z[2] = abb[0] * z[2];
z[4] = 3 * abb[18] + abb[19] + -abb[22];
z[6] = -abb[9] * z[4];
z[9] = -abb[2] * abb[23];
z[12] = abb[7] * z[1];
z[6] = z[6] + z[9] + 3 * z[12];
z[6] = abb[17] * z[6];
z[9] = abb[1] * z[4];
z[12] = abb[18] + abb[19];
z[12] = abb[2] * z[12];
z[9] = z[9] + z[12];
z[13] = abb[7] * abb[23];
z[14] = z[9] + (T(-1) / T(2)) * z[13];
z[0] = z[0] * z[14];
z[14] = -prod_pow(abb[14], 2);
z[15] = prod_pow(abb[11], 2);
z[14] = z[14] + z[15];
z[15] = abb[8] * abb[23];
z[14] = z[14] * z[15];
z[0] = z[0] + z[6] + z[14];
z[6] = abb[6] * (T(1) / T(2));
z[14] = -abb[1] + z[6];
z[14] = z[4] * z[14];
z[1] = abb[2] * z[1];
z[1] = z[1] + (T(-1) / T(4)) * z[13] + z[14];
z[14] = abb[13] * z[1];
z[16] = -abb[1] + abb[6];
z[16] = z[4] * z[16];
z[16] = -z[12] + z[16];
z[17] = abb[5] * z[4];
z[17] = (T(-1) / T(2)) * z[16] + z[17];
z[17] = abb[14] * z[17];
z[14] = z[14] + z[17];
z[14] = abb[14] * z[14];
z[1] = -z[1] * z[8];
z[16] = z[13] + -z[16];
z[17] = abb[4] * z[4];
z[16] = (T(1) / T(2)) * z[16] + z[17];
z[16] = abb[11] * z[16];
z[1] = z[1] + z[16];
z[1] = abb[11] * z[1];
z[16] = abb[4] + abb[5];
z[6] = abb[2] * (T(3) / T(2)) + -z[6] + -z[16];
z[6] = z[4] * z[6];
z[17] = 2 * abb[21];
z[18] = 27 * abb[18] + 9 * abb[19] + -5 * abb[22];
z[18] = -z[17] + (T(1) / T(2)) * z[18];
z[18] = abb[1] * z[18];
z[19] = 2 * abb[1];
z[20] = abb[20] * z[19];
z[6] = z[6] + z[18] + -z[20];
z[6] = abb[12] * z[6];
z[18] = 3 * abb[1] + abb[2] + -abb[6];
z[18] = z[4] * z[18];
z[21] = -abb[11] + -z[8];
z[21] = z[18] * z[21];
z[6] = z[6] + z[21];
z[6] = abb[12] * z[6];
z[21] = abb[15] + abb[16];
z[18] = z[18] * z[21];
z[21] = -abb[19] + abb[20] + abb[21];
z[21] = -abb[18] + (T(1) / T(3)) * z[21];
z[21] = abb[1] * z[21];
z[12] = (T(-1) / T(3)) * z[12] + z[21];
z[11] = z[11] * z[12];
z[3] = abb[10] * z[3];
z[0] = (T(1) / T(2)) * z[0] + z[1] + z[2] + z[3] + z[5] + z[6] + (T(13) / T(2)) * z[11] + z[14] + z[18];
z[1] = -2 * abb[4] + z[19];
z[1] = z[1] * z[4];
z[2] = 2 * abb[18] + -abb[22];
z[3] = abb[2] * z[2];
z[1] = z[1] + z[3] + -z[13];
z[1] = abb[11] * z[1];
z[5] = -abb[2] + z[16];
z[5] = z[4] * z[5];
z[6] = -9 * abb[18] + -3 * abb[19] + abb[22] + z[17];
z[6] = abb[1] * z[6];
z[5] = z[5] + z[6] + z[20];
z[5] = abb[12] * z[5];
z[6] = -2 * z[9] + z[13];
z[6] = abb[13] * z[6];
z[9] = -2 * abb[5] + z[19];
z[4] = z[4] * z[9];
z[3] = z[3] + z[4];
z[3] = abb[14] * z[3];
z[4] = abb[11] + -abb[13];
z[4] = -z[2] * z[4];
z[4] = z[4] + -4 * z[7];
z[4] = abb[3] * z[4];
z[7] = -abb[11] + abb[14];
z[7] = z[7] * z[15];
z[2] = -z[2] * z[8];
z[2] = z[2] + -4 * z[10];
z[2] = abb[0] * z[2];
z[1] = z[1] + z[2] + z[3] + z[4] + 2 * z[5] + z[6] + z[7];
z[1] = m1_set::bc<T>[0] * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_18_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-36.682310515106630392833272475757325351289707818356945272902581058"),stof<T>("52.741337696243548012391414832183606836297750117548411587622738085")}, std::complex<T>{stof<T>("-29.785090647802799150146195723860226353371503743151522035464699399"),stof<T>("40.270115479984572303661626132451167089337041827188553468552702566")}, std::complex<T>{stof<T>("22.60890283178492313406981515200700402818461207269985546617044954"),stof<T>("7.583483977093285575622000127537769702729375417392099625370886916")}, std::complex<T>{stof<T>("-14.090505933109796230390926423375822274203138121265296011899736434"),stof<T>("-16.208213815228034833662178340414469559996889447617196596911157526")}, std::complex<T>{stof<T>("-0.8105885156856478304959059883670413780316349381145681084164157239"),stof<T>("-1.9232461890621132253448052434278699448465971300673805737648824541")}, std::complex<T>{stof<T>("0.0777645199269873530365534850398990571898457862453410706965229878"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[123].real()/kbase.W[123].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_18_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_18_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("66.78018101498659928530765354656407583002880692937230888787194221"),stof<T>("38.996471004800725582877602954239115397734733864433887304746513135")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,24> abb = {dl[0], dl[1], dl[2], dl[4], dlog_W12(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_23(k), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[123].real()/k.W[123].real())};

                    
            return f_4_18_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_18_DLogXconstant_part(base_point<T>, kend);
	value += f_4_18_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_18_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_18_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_18_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_18_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_18_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_18_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
