/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_564.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_564_abbreviated (const std::array<T,58>& abb) {
T z[104];
z[0] = abb[28] + -abb[29];
z[1] = -abb[30] + abb[32];
z[2] = -abb[31] + 2 * z[1];
z[3] = abb[27] * (T(1) / T(2));
z[4] = z[0] + -z[2] + z[3];
z[4] = abb[27] * z[4];
z[5] = -abb[30] + abb[31];
z[6] = abb[32] + z[5];
z[7] = -z[0] * z[6];
z[8] = 2 * abb[36];
z[9] = prod_pow(abb[30], 2);
z[10] = (T(3) / T(2)) * z[9];
z[11] = abb[32] * (T(1) / T(2));
z[12] = -abb[30] + z[11];
z[12] = abb[32] * z[12];
z[13] = prod_pow(abb[31], 2);
z[14] = (T(3) / T(2)) * z[13];
z[4] = z[4] + z[7] + z[8] + z[10] + 3 * z[12] + -z[14];
z[4] = abb[14] * z[4];
z[15] = 2 * abb[30];
z[16] = abb[32] + z[15];
z[17] = abb[28] * (T(1) / T(2));
z[18] = abb[29] + abb[31] + z[16] + -z[17];
z[18] = abb[28] * z[18];
z[19] = z[3] + -z[6];
z[19] = abb[27] * z[19];
z[20] = -abb[29] + z[6];
z[20] = abb[29] * z[20];
z[21] = 3 * abb[32];
z[22] = abb[30] * z[21];
z[23] = abb[36] + z[14];
z[18] = z[10] + z[18] + z[19] + -z[20] + -z[22] + z[23];
z[19] = prod_pow(m1_set::bc<T>[0], 2);
z[20] = -z[18] + (T(23) / T(6)) * z[19];
z[20] = abb[3] * z[20];
z[18] = abb[5] * z[18];
z[22] = 2 * abb[8];
z[24] = 3 * abb[12];
z[25] = -abb[1] + 2 * abb[2] + z[22] + -z[24];
z[25] = abb[29] * z[25];
z[26] = -abb[2] + abb[12];
z[27] = abb[32] * z[26];
z[28] = abb[1] * abb[31];
z[27] = z[27] + -z[28];
z[27] = 3 * z[27];
z[29] = 2 * abb[32] + -z[5];
z[29] = abb[8] * z[29];
z[30] = -abb[2] + abb[1] * (T(1) / T(2));
z[31] = -abb[8] + z[30];
z[31] = abb[28] * z[31];
z[25] = z[25] + -z[27] + z[29] + z[31];
z[25] = abb[28] * z[25];
z[29] = abb[8] * (T(1) / T(2));
z[31] = -z[29] + z[30];
z[31] = abb[27] * z[31];
z[32] = abb[1] + abb[2];
z[33] = z[0] * z[32];
z[34] = abb[8] * z[6];
z[31] = 3 * z[28] + -z[31] + z[33] + -z[34];
z[31] = abb[27] * z[31];
z[30] = z[29] + z[30];
z[30] = abb[29] * z[30];
z[2] = abb[8] * z[2];
z[2] = -z[2] + z[27] + z[30];
z[2] = abb[29] * z[2];
z[27] = 3 * abb[34];
z[30] = prod_pow(abb[32], 2);
z[23] = z[23] + z[27] + (T(3) / T(2)) * z[30];
z[23] = abb[8] * z[23];
z[33] = -abb[7] + 7 * abb[12] + abb[1] * (T(-13) / T(3)) + abb[2] * (T(5) / T(3));
z[33] = abb[5] * (T(-23) / T(6)) + abb[8] * (T(-5) / T(3)) + (T(1) / T(2)) * z[33];
z[33] = z[19] * z[33];
z[34] = -abb[30] + z[17];
z[34] = abb[28] * z[34];
z[34] = -z[12] + z[34];
z[35] = 3 * abb[7];
z[34] = z[34] * z[35];
z[36] = z[26] * z[30];
z[37] = abb[34] + z[14];
z[37] = abb[1] * z[37];
z[36] = z[36] + z[37];
z[38] = -abb[1] + abb[14];
z[39] = 3 * abb[33];
z[38] = z[38] * z[39];
z[40] = abb[3] + abb[5];
z[41] = -abb[7] + z[40];
z[26] = abb[8] + -z[26] + z[41];
z[42] = 3 * abb[56];
z[43] = z[26] * z[42];
z[2] = z[2] + z[4] + z[18] + -z[20] + -z[23] + z[25] + -z[31] + z[33] + z[34] + 3 * z[36] + -z[38] + z[43];
z[4] = abb[45] * z[2];
z[18] = abb[14] + z[41];
z[20] = 3 * abb[35];
z[23] = z[18] * z[20];
z[2] = z[2] + z[23];
z[2] = abb[44] * z[2];
z[23] = abb[36] + (T(1) / T(2)) * z[9];
z[25] = -abb[30] + z[3];
z[25] = abb[27] * z[25];
z[31] = abb[31] * (T(1) / T(2));
z[33] = -abb[30] + z[31];
z[33] = abb[31] * z[33];
z[12] = abb[54] + z[12];
z[25] = -abb[33] + -abb[55] + z[12] + z[23] + -z[25] + z[33];
z[33] = 3 * abb[21];
z[25] = z[25] * z[33];
z[33] = abb[29] * (T(1) / T(2));
z[34] = z[17] + -z[33];
z[36] = -abb[32] + z[34];
z[38] = z[3] + z[36];
z[38] = abb[27] * z[38];
z[36] = abb[28] * z[36];
z[43] = abb[29] * abb[32];
z[44] = (T(1) / T(2)) * z[30];
z[36] = z[36] + z[38] + z[43] + z[44];
z[38] = (T(1) / T(2)) * z[19];
z[43] = z[38] + z[42];
z[36] = 3 * z[36] + -z[43];
z[36] = abb[25] * z[36];
z[45] = abb[28] + -abb[32];
z[46] = -abb[29] + z[45];
z[47] = abb[31] + z[46];
z[47] = abb[27] * z[47];
z[48] = abb[32] * z[1];
z[49] = abb[31] * z[5];
z[49] = -abb[55] + z[49];
z[7] = -abb[33] + z[7] + z[47] + z[48] + -z[49];
z[47] = 3 * abb[24];
z[7] = z[7] * z[47];
z[47] = z[5] + z[34];
z[48] = abb[27] * z[47];
z[50] = -abb[31] + z[33];
z[51] = abb[29] * z[50];
z[48] = abb[34] + z[23] + -z[48] + z[49] + z[51];
z[49] = abb[28] * z[50];
z[12] = -z[12] + -z[48] + z[49];
z[49] = 3 * abb[23];
z[12] = z[12] * z[49];
z[47] = abb[28] * z[47];
z[47] = z[47] + z[48];
z[43] = -z[43] + 3 * z[47];
z[47] = abb[20] * z[43];
z[48] = abb[20] + -abb[24] + -abb[25];
z[49] = 3 * abb[54];
z[48] = z[48] * z[49];
z[7] = z[7] + -z[12] + z[25] + z[36] + z[47] + z[48];
z[12] = abb[21] + abb[23];
z[25] = abb[24] + z[12];
z[36] = z[20] * z[25];
z[36] = z[7] + z[36];
z[47] = abb[53] * z[36];
z[43] = z[43] + z[49];
z[43] = abb[22] * z[43];
z[36] = z[36] + z[43];
z[36] = abb[51] * z[36];
z[48] = -abb[52] + abb[53];
z[43] = -z[43] * z[48];
z[36] = -z[36] + -z[43] + z[47];
z[43] = abb[46] * (T(9) / T(2));
z[47] = abb[50] * (T(3) / T(2));
z[51] = 3 * abb[49];
z[52] = -abb[43] + z[51];
z[53] = abb[47] * (T(1) / T(2));
z[54] = abb[42] + z[43] + -z[47] + -z[52] + -z[53];
z[54] = z[17] * z[54];
z[55] = 3 * abb[42];
z[56] = abb[47] + abb[50];
z[57] = z[55] + -z[56];
z[58] = abb[31] * z[57];
z[59] = 3 * abb[46];
z[60] = -z[56] + z[59];
z[61] = (T(1) / T(2)) * z[60];
z[62] = abb[32] * z[61];
z[63] = abb[30] * z[60];
z[64] = abb[47] * (T(5) / T(2)) + z[47] + -z[52];
z[65] = abb[46] * (T(3) / T(4));
z[64] = -abb[42] + (T(1) / T(2)) * z[64] + -z[65];
z[64] = abb[29] * z[64];
z[54] = z[54] + z[58] + -z[62] + -z[63] + z[64];
z[54] = abb[28] * z[54];
z[64] = abb[42] * (T(1) / T(2));
z[66] = 2 * abb[43];
z[67] = z[64] + z[66];
z[68] = abb[47] * (T(5) / T(4));
z[69] = abb[50] * (T(3) / T(4));
z[70] = -z[51] + z[65] + z[67] + z[68] + -z[69];
z[70] = abb[29] * z[70];
z[71] = abb[32] * z[60];
z[71] = -z[63] + z[71];
z[58] = -z[58] + (T(1) / T(2)) * z[71];
z[70] = z[58] + z[70];
z[70] = abb[29] * z[70];
z[72] = abb[47] * (T(3) / T(2));
z[73] = abb[50] * (T(1) / T(2));
z[74] = -z[52] + z[72] + z[73];
z[75] = -abb[42] + z[65] + (T(1) / T(2)) * z[74];
z[75] = z[0] * z[75];
z[76] = z[53] + -z[73];
z[77] = -abb[43] + z[64] + -z[76];
z[77] = abb[27] * z[77];
z[58] = z[58] + z[75] + z[77];
z[58] = abb[27] * z[58];
z[75] = (T(1) / T(2)) * z[56];
z[77] = -abb[42] + z[75];
z[78] = abb[46] * (T(1) / T(2));
z[79] = z[77] + -z[78];
z[80] = -z[27] + -z[39];
z[80] = z[79] * z[80];
z[81] = -abb[36] * z[60];
z[82] = -z[10] * z[60];
z[82] = z[81] + z[82];
z[83] = abb[46] * (T(3) / T(2));
z[84] = -13 * abb[43] + abb[47] * (T(-23) / T(2));
z[84] = 13 * abb[49] + abb[42] * (T(-13) / T(3)) + z[73] + -z[83] + (T(1) / T(3)) * z[84];
z[38] = z[38] * z[84];
z[84] = abb[32] * z[63];
z[14] = z[14] * z[57];
z[14] = z[14] + z[38] + z[54] + z[58] + z[70] + z[80] + (T(1) / T(2)) * z[82] + (T(3) / T(2)) * z[84];
z[14] = abb[3] * z[14];
z[38] = 3 * abb[47];
z[54] = 5 * abb[43];
z[58] = -z[38] + -z[54];
z[58] = abb[2] * z[58];
z[70] = abb[2] * abb[49];
z[80] = abb[2] * abb[48];
z[82] = abb[2] * abb[50];
z[58] = z[58] + 5 * z[70] + z[80] + z[82];
z[84] = abb[46] + -abb[47];
z[85] = -abb[43] + abb[49];
z[86] = z[84] + z[85];
z[87] = abb[12] * z[86];
z[58] = (T(1) / T(2)) * z[58] + -z[87];
z[58] = z[30] * z[58];
z[88] = -abb[47] + abb[50];
z[89] = -abb[48] + z[78] + (T(1) / T(2)) * z[88];
z[90] = abb[30] * z[89];
z[91] = -z[11] * z[89];
z[91] = z[90] + z[91];
z[91] = abb[32] * z[91];
z[23] = -z[23] * z[89];
z[23] = z[23] + z[91];
z[23] = abb[6] * z[23];
z[91] = abb[10] * abb[42];
z[92] = abb[10] * abb[47];
z[91] = z[91] + -z[92];
z[93] = abb[6] * z[89];
z[94] = -z[91] + (T(-1) / T(2)) * z[93];
z[94] = abb[31] * z[94];
z[95] = abb[30] * z[93];
z[94] = z[94] + z[95];
z[94] = abb[31] * z[94];
z[76] = -z[76] + -z[78] + z[85];
z[85] = abb[15] * z[76];
z[44] = -z[44] * z[85];
z[96] = z[3] + -z[17] + z[50];
z[96] = abb[27] * z[96];
z[50] = -z[0] * z[50];
z[50] = abb[34] + (T(1) / T(2)) * z[13] + z[50] + z[96];
z[50] = abb[13] * z[50] * z[79];
z[37] = z[37] * z[57];
z[96] = prod_pow(abb[27], 2);
z[13] = -z[13] + z[96];
z[96] = abb[42] + -abb[50];
z[97] = abb[48] + z[96];
z[97] = abb[9] * z[97];
z[13] = z[13] * z[97];
z[13] = z[13] + z[23] + z[37] + z[44] + z[50] + z[58] + z[94];
z[23] = abb[52] * (T(1) / T(2));
z[7] = z[7] * z[23];
z[37] = -abb[47] + 5 * abb[50];
z[44] = 3 * abb[48];
z[37] = (T(1) / T(2)) * z[37] + -z[44] + -z[83];
z[50] = -abb[32] * z[37];
z[17] = z[17] * z[60];
z[58] = abb[31] * z[61];
z[94] = abb[47] + -2 * abb[50] + z[44];
z[15] = -z[15] * z[94];
z[98] = z[33] * z[60];
z[99] = -abb[50] + abb[48] * (T(3) / T(2)) + z[53];
z[100] = -abb[27] * z[99];
z[15] = z[15] + -z[17] + z[50] + -z[58] + z[98] + z[100];
z[15] = abb[27] * z[15];
z[50] = abb[46] + abb[48] + -abb[50];
z[98] = z[31] * z[50];
z[90] = -z[90] + z[98];
z[98] = 3 * abb[31];
z[90] = z[90] * z[98];
z[98] = 3 * abb[55];
z[100] = z[89] * z[98];
z[90] = z[90] + -z[100];
z[100] = abb[31] * z[60];
z[71] = z[71] + z[100];
z[34] = z[34] * z[71];
z[100] = -abb[47] + 3 * abb[50];
z[100] = -2 * abb[48] + -z[78] + (T(1) / T(2)) * z[100];
z[101] = -abb[30] * z[100];
z[11] = -z[11] * z[50];
z[11] = z[11] + z[101];
z[11] = z[11] * z[21];
z[8] = -z[8] * z[94];
z[9] = 3 * z[9];
z[50] = -z[9] * z[99];
z[8] = z[8] + z[11] + z[15] + z[34] + z[50] + z[90];
z[8] = abb[14] * z[8];
z[11] = -abb[48] + z[75] + -z[78];
z[9] = z[9] * z[11];
z[11] = -abb[46] + z[88];
z[15] = z[11] * z[27];
z[21] = z[21] * z[63];
z[9] = z[9] + z[15] + z[21] + z[81];
z[15] = z[58] + z[62];
z[21] = abb[28] * (T(1) / T(4)) + -z[33];
z[21] = z[21] * z[60];
z[21] = -z[15] + z[21] + -z[63];
z[21] = abb[28] * z[21];
z[34] = 2 * abb[47];
z[50] = -abb[50] + z[34];
z[50] = abb[30] * z[50];
z[75] = 5 * abb[47] + -abb[50] + -z[59];
z[88] = abb[29] * z[75];
z[15] = z[15] + z[50] + (T(1) / T(4)) * z[88];
z[15] = abb[29] * z[15];
z[88] = -abb[27] * z[61];
z[71] = z[71] + z[88];
z[3] = z[3] * z[71];
z[3] = z[3] + (T(1) / T(2)) * z[9] + z[15] + z[21] + -z[90];
z[3] = abb[5] * z[3];
z[9] = 5 * abb[48] + abb[46] * (T(13) / T(2)) + (T(-23) / T(6)) * z[56];
z[9] = abb[5] * z[9];
z[9] = z[9] + z[85];
z[15] = -abb[2] * z[53];
z[15] = z[15] + z[82];
z[15] = (T(1) / T(3)) * z[15] + (T(-1) / T(2)) * z[80];
z[21] = abb[47] * (T(-43) / T(2)) + -z[54];
z[21] = (T(1) / T(3)) * z[21] + -z[73];
z[21] = abb[46] * (T(-1) / T(4)) + abb[42] * (T(13) / T(6)) + (T(1) / T(2)) * z[21] + z[51];
z[21] = abb[8] * z[21];
z[54] = abb[7] * z[60];
z[71] = abb[43] + z[96];
z[88] = abb[0] * z[71];
z[90] = -abb[42] + (T(1) / T(3)) * z[56];
z[90] = abb[1] * z[90];
z[9] = (T(1) / T(2)) * z[9] + 5 * z[15] + z[21] + (T(1) / T(4)) * z[54] + (T(-7) / T(2)) * z[87] + (T(13) / T(6)) * z[88] + (T(13) / T(2)) * z[90];
z[9] = z[9] * z[19];
z[15] = 3 * abb[43];
z[19] = z[15] + z[34];
z[21] = abb[2] * z[19];
z[34] = abb[2] * z[51];
z[21] = -z[21] + z[34] + z[82];
z[54] = z[65] + z[69];
z[69] = 2 * abb[42];
z[90] = z[66] + z[69];
z[94] = abb[47] * (T(7) / T(4)) + z[54] + -z[90];
z[94] = abb[8] * z[94];
z[96] = abb[1] * z[57];
z[99] = (T(1) / T(2)) * z[96];
z[55] = abb[10] * z[55];
z[55] = z[21] + z[55] + z[88] + -3 * z[92] + z[94] + z[99];
z[55] = abb[29] * z[55];
z[87] = -z[21] + z[87];
z[92] = abb[32] * z[87];
z[94] = abb[32] * z[85];
z[101] = abb[31] * z[96];
z[92] = z[92] + z[94] + z[101];
z[92] = 3 * z[92];
z[19] = -abb[50] + z[19] + -z[51];
z[19] = abb[32] * z[19];
z[50] = -z[19] + z[50];
z[50] = 2 * z[50] + -z[58];
z[50] = abb[8] * z[50];
z[50] = z[50] + z[55] + -z[92];
z[50] = abb[29] * z[50];
z[55] = (T(3) / T(2)) * z[85];
z[86] = z[24] * z[86];
z[64] = 4 * abb[43] + abb[49] * (T(-9) / T(2)) + abb[47] * (T(13) / T(4)) + -z[54] + -z[64];
z[64] = abb[8] * z[64];
z[102] = (T(1) / T(2)) * z[88];
z[64] = -2 * z[21] + z[55] + z[64] + z[86] + -z[96] + -z[102];
z[64] = abb[29] * z[64];
z[102] = -z[21] + z[55] + z[102];
z[103] = abb[49] * (T(3) / T(2));
z[67] = z[67] + -z[103];
z[54] = z[54] + -z[67] + -z[68];
z[54] = abb[8] * z[54];
z[54] = z[54] + z[99] + -z[102];
z[54] = abb[28] * z[54];
z[68] = abb[30] * z[61];
z[58] = -z[58] + z[68];
z[68] = 2 * z[19] + -z[58];
z[68] = abb[8] * z[68];
z[54] = z[54] + z[64] + z[68] + z[92];
z[54] = abb[28] * z[54];
z[15] = abb[47] + z[15] + -z[44];
z[15] = abb[2] * z[15];
z[15] = z[15] + -z[34] + z[82];
z[34] = -abb[47] + z[103];
z[64] = abb[42] + abb[43] * (T(-1) / T(2)) + z[34] + -z[73];
z[64] = abb[8] * z[64];
z[68] = 2 * z[88];
z[55] = (T(-1) / T(2)) * z[15] + -z[55] + z[64] + -z[68] + (T(3) / T(2)) * z[93] + z[99];
z[55] = abb[27] * z[55];
z[64] = abb[50] * (T(-5) / T(4)) + abb[47] * (T(3) / T(4)) + z[65] + z[67];
z[64] = abb[8] * z[64];
z[64] = z[64] + z[96] + z[102];
z[0] = -z[0] * z[64];
z[64] = abb[43] + abb[47];
z[65] = abb[2] * z[64];
z[65] = z[65] + -z[70] + z[80] + -z[82];
z[67] = abb[32] * z[65];
z[67] = z[67] + z[94];
z[70] = z[67] + -z[95] + -z[101];
z[58] = z[19] + z[58];
z[58] = abb[8] * z[58];
z[0] = z[0] + z[55] + z[58] + 3 * z[70];
z[0] = abb[27] * z[0];
z[55] = 2 * abb[49] + -z[66] + -z[72] + z[73] + z[78];
z[55] = abb[8] * z[55];
z[41] = z[41] * z[61];
z[55] = z[41] + z[55] + -z[85] + -z[87];
z[42] = -z[42] * z[55];
z[58] = abb[8] * z[76];
z[58] = -z[58] + z[65] + z[85] + -z[93];
z[61] = abb[14] * z[89];
z[65] = z[58] + z[61];
z[49] = z[49] * z[65];
z[65] = -abb[17] + abb[19];
z[65] = z[65] * z[76];
z[66] = -abb[18] * z[79];
z[70] = -abb[49] + z[77] + z[78];
z[70] = abb[16] * z[70];
z[65] = z[65] + z[66] + z[70];
z[66] = -abb[51] + z[48];
z[66] = abb[26] * z[66];
z[65] = (T(3) / T(2)) * z[65] + (T(3) / T(4)) * z[66];
z[65] = abb[37] * z[65];
z[10] = -z[10] * z[11];
z[38] = abb[46] + abb[50] + -z[38];
z[27] = z[27] * z[38];
z[10] = z[10] + z[27] + -z[81];
z[27] = abb[43] * (T(-3) / T(2)) + z[34] + z[73];
z[27] = z[27] * z[30];
z[30] = abb[30] * z[11];
z[34] = abb[31] * z[84];
z[34] = z[30] + z[34];
z[34] = abb[31] * z[34];
z[10] = (T(1) / T(2)) * z[10] + 3 * z[27] + (T(3) / T(2)) * z[34];
z[10] = abb[8] * z[10];
z[27] = z[62] + -z[63];
z[27] = abb[32] * z[27];
z[34] = -z[17] + z[63];
z[34] = abb[28] * z[34];
z[27] = z[27] + z[34];
z[34] = abb[7] * (T(3) / T(2));
z[27] = z[27] * z[34];
z[38] = abb[14] * z[100];
z[38] = z[38] + -z[93];
z[66] = abb[13] * z[79];
z[66] = -z[38] + z[66] + z[96];
z[39] = z[39] * z[66];
z[23] = z[23] * z[25];
z[18] = abb[45] * z[18];
z[18] = z[18] + z[23] + z[38] + -z[41];
z[18] = z[18] * z[20];
z[20] = -abb[34] + -abb[55];
z[20] = z[11] * z[20];
z[23] = z[11] * z[31];
z[23] = z[23] + -z[30];
z[23] = abb[31] * z[23];
z[25] = -z[11] * z[33];
z[25] = z[25] + z[30];
z[25] = abb[29] * z[25];
z[20] = z[20] + z[23] + z[25];
z[23] = abb[4] * (T(3) / T(2));
z[20] = z[20] * z[23];
z[25] = z[11] * z[29];
z[25] = z[25] + z[93];
z[29] = z[25] * z[98];
z[0] = abb[57] + z[0] + z[2] + z[3] + z[4] + z[7] + z[8] + z[9] + z[10] + 3 * z[13] + z[14] + z[18] + z[20] + z[27] + z[29] + (T(-1) / T(2)) * z[36] + z[39] + z[42] + z[49] + z[50] + z[54] + z[65];
z[2] = -z[47] + z[53] + z[90];
z[3] = 6 * abb[49] + -z[2] + -z[43];
z[3] = abb[28] * z[3];
z[4] = 2 * abb[31];
z[7] = -z[4] * z[57];
z[8] = abb[42] + z[51] + -2 * z[64];
z[9] = 2 * abb[29];
z[8] = z[8] * z[9];
z[10] = 2 * abb[27];
z[13] = z[10] * z[71];
z[3] = z[3] + z[7] + z[8] + z[13] + -z[62] + 2 * z[63];
z[3] = abb[3] * z[3];
z[7] = 3 * z[85];
z[8] = -z[69] + z[74] + z[83];
z[8] = abb[8] * z[8];
z[13] = 2 * z[96];
z[8] = z[7] + z[8] + z[13] + z[15] + 4 * z[88] + -3 * z[93];
z[8] = abb[27] * z[8];
z[14] = z[44] + -2 * z[56] + z[59];
z[14] = abb[30] * z[14];
z[15] = -abb[31] * z[37];
z[18] = -z[33] * z[75];
z[14] = z[14] + z[15] + z[17] + z[18] + -z[62];
z[14] = abb[5] * z[14];
z[7] = z[7] + -z[13] + -z[21] + z[68];
z[13] = -4 * abb[47] + z[52] + z[69];
z[13] = z[13] * z[22];
z[13] = -z[7] + z[13] + -z[86] + -6 * z[91];
z[13] = abb[29] * z[13];
z[15] = 2 * z[91] + z[93];
z[15] = abb[31] * z[15];
z[17] = -z[4] * z[96];
z[15] = z[15] + z[17] + -z[67];
z[2] = z[2] + -z[83];
z[2] = abb[8] * z[2];
z[2] = z[2] + z[7] + -z[86];
z[2] = abb[28] * z[2];
z[7] = abb[30] * (T(-1) / T(2)) + z[31];
z[7] = z[7] * z[75];
z[7] = z[7] + -z[19];
z[7] = abb[8] * z[7];
z[17] = abb[28] * z[60];
z[17] = z[17] + -z[63];
z[17] = z[17] * z[34];
z[18] = abb[27] + -abb[31];
z[1] = z[1] + -z[18];
z[1] = abb[14] * z[1] * z[37];
z[19] = -z[18] * z[97];
z[1] = z[1] + z[2] + z[3] + z[7] + z[8] + z[13] + z[14] + 3 * z[15] + z[17] + 6 * z[19];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = 4 * abb[30] + z[4] + z[9] + z[45];
z[2] = z[2] * z[40];
z[3] = -z[4] + z[16];
z[3] = abb[8] * z[3];
z[4] = 2 * abb[1] + -abb[2];
z[7] = -abb[8] + z[4] + -z[24];
z[7] = abb[28] * z[7];
z[4] = -4 * abb[8] + z[4] + z[24];
z[4] = abb[29] * z[4];
z[8] = abb[28] + -abb[30];
z[8] = z[8] * z[35];
z[9] = z[10] * z[32];
z[6] = -abb[27] + z[6];
z[10] = abb[14] * z[6];
z[2] = z[2] + z[3] + -z[4] + z[7] + z[8] + -z[9] + -2 * z[10] + 6 * z[28];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = 3 * abb[40];
z[4] = z[3] * z[26];
z[2] = z[2] + -z[4];
z[4] = -abb[44] + -abb[45];
z[2] = z[2] * z[4];
z[4] = -abb[38] + abb[39];
z[6] = m1_set::bc<T>[0] * z[6];
z[6] = z[4] + z[6];
z[6] = abb[24] * z[6];
z[7] = m1_set::bc<T>[0] * z[18];
z[7] = -z[4] + z[7];
z[7] = z[7] * z[12];
z[5] = -abb[27] + abb[28] + z[5];
z[5] = m1_set::bc<T>[0] * z[5];
z[5] = z[4] + z[5];
z[8] = abb[20] * z[5];
z[9] = abb[27] + z[46];
z[9] = m1_set::bc<T>[0] * z[9];
z[9] = abb[38] + z[9];
z[9] = abb[25] * z[9];
z[10] = abb[20] + abb[25];
z[10] = abb[40] * z[10];
z[6] = -z[6] + -z[7] + z[8] + z[9] + z[10];
z[5] = abb[40] + z[5];
z[5] = abb[22] * z[5];
z[7] = -z[5] + -z[6];
z[7] = abb[51] * z[7];
z[5] = z[5] * z[48];
z[5] = z[5] + z[7];
z[7] = (T(3) / T(2)) * z[48];
z[6] = z[6] * z[7];
z[7] = abb[38] * z[58];
z[8] = abb[5] * z[89];
z[8] = z[8] + z[25];
z[8] = abb[39] * z[8];
z[4] = -z[4] * z[61];
z[4] = z[4] + z[7] + z[8];
z[3] = -z[3] * z[55];
z[7] = abb[29] + -abb[31];
z[7] = m1_set::bc<T>[0] * z[7];
z[7] = -abb[39] + z[7];
z[7] = z[7] * z[11] * z[23];
z[1] = abb[41] + z[1] + z[2] + z[3] + 3 * z[4] + (T(3) / T(2)) * z[5] + z[6] + z[7];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_564_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-36.682310515106630392833272475757325351289707818356945272902581058"),stof<T>("52.741337696243548012391414832183606836297750117548411587622738085")}, std::complex<T>{stof<T>("-26.198296627818665715852294092733382933427874830835336739337282745"),stof<T>("8.503853713337012963867488862109638201182081099726697992227678228")}, std::complex<T>{stof<T>("-49.524956236407311254082823283016140611008461205871124774759687584"),stof<T>("73.841783123950370409383066140526278607337617840009044329562912174")}, std::complex<T>{stof<T>("-49.524956236407311254082823283016140611008461205871124774759687584"),stof<T>("73.841783123950370409383066140526278607337617840009044329562912174")}, std::complex<T>{stof<T>("-25.29960182007458360275951304817094766106727341782048314949503923"),stof<T>("-0.231393115198248609905564510504544725433921351183129945062375423")}, std::complex<T>{stof<T>("-34.994242349480808660486572442622549822969684600108166592620316202"),stof<T>("26.641826692748775297875689373194511221230188847269298308405341226")}, std::complex<T>{stof<T>("78.871258056120432773457608788556736642252678241932957572828799883"),stof<T>("-76.572862457047181862460952427279532632895456463743946810263047842")}, std::complex<T>{stof<T>("22.151596628180127799237021635363734563250931212593987090763209676"),stof<T>("-5.541381265041952900884038064851839450190321124808665566465167137")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_564_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_564_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(32)) * (v[1] + v[2] + -v[3] + -v[4]) * (-m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]) + 4 * (12 + 4 * v[0] + v[1] + 5 * v[2] + v[3] + -5 * v[4] + -4 * v[5] + m1_set::bc<T>[1] * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])))) / prod_pow(tend, 2);
c[1] = ((-2 + 2 * m1_set::bc<T>[1] + 3 * m1_set::bc<T>[2]) * (T(-3) / T(4)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[44] + abb[45] + -abb[46] + -abb[48] + abb[50]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[30] + abb[32];
z[1] = abb[27] + abb[30];
z[0] = z[0] * z[1];
z[0] = abb[33] + -abb[35] + -abb[36] + z[0];
z[1] = abb[44] + abb[45] + -abb[46] + -abb[48] + abb[50];
return 3 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_564_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-3) / T(8)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(3) / T(2)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[44] + abb[45] + -abb[46] + -abb[48] + abb[50]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[30] + abb[32];
z[1] = -abb[44] + -abb[45] + abb[46] + abb[48] + -abb[50];
return 3 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_564_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("134.00016935824535130876180219353654991258256745266805246001734843"),stof<T>("57.62304878618258613068434794966887603493917368650055524885138918")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,58> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W18(k,dl), dlog_W20(k,dl), dlog_W25(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_14(k), f_2_20(k), f_2_23(k), f_2_2_im(k), f_2_10_im(k), f_2_12_im(k), T{0}, rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_2_re(k), f_2_10_re(k), f_2_12_re(k), T{0}};
abb[41] = SpDLog_f_4_564_W_20_Im(t, path, abb);
abb[57] = SpDLog_f_4_564_W_20_Re(t, path, abb);

                    
            return f_4_564_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_564_DLogXconstant_part(base_point<T>, kend);
	value += f_4_564_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_564_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_564_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_564_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_564_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_564_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_564_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
