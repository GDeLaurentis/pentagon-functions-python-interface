/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_3.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_3_abbreviated (const std::array<T,55>& abb) {
T z[99];
z[0] = abb[28] * (T(1) / T(4));
z[1] = abb[29] * (T(1) / T(2));
z[2] = 2 * abb[32];
z[3] = -z[0] + z[1] + -z[2];
z[3] = abb[28] * z[3];
z[4] = abb[34] + -abb[54];
z[5] = abb[36] * (T(1) / T(2));
z[4] = -3 * z[4] + -z[5];
z[6] = -abb[27] + z[2];
z[7] = abb[28] + -abb[29];
z[8] = z[6] + z[7];
z[9] = -abb[31] * z[8];
z[10] = abb[53] * (T(1) / T(2));
z[11] = abb[27] + -abb[32];
z[12] = abb[31] + z[11];
z[13] = -abb[30] + z[12];
z[14] = abb[30] * z[13];
z[14] = 2 * z[14];
z[15] = prod_pow(abb[29], 2);
z[16] = prod_pow(m1_set::bc<T>[0], 2);
z[17] = abb[32] * (T(1) / T(2));
z[18] = -abb[29] + -z[17];
z[18] = abb[32] * z[18];
z[19] = abb[27] + z[7];
z[20] = abb[27] * z[19];
z[3] = -abb[52] + z[3] + -z[4] + z[9] + z[10] + -z[14] + (T(5) / T(4)) * z[15] + (T(3) / T(2)) * z[16] + z[18] + z[20];
z[3] = abb[8] * z[3];
z[9] = abb[32] * (T(3) / T(2));
z[18] = abb[29] + -z[9];
z[18] = abb[32] * z[18];
z[18] = -abb[54] + -z[15] + z[18];
z[20] = -abb[28] + z[12];
z[21] = abb[30] * (T(1) / T(2));
z[20] = z[20] * z[21];
z[20] = (T(1) / T(12)) * z[16] + z[20];
z[22] = abb[35] * (T(1) / T(2));
z[23] = z[5] + z[22];
z[0] = abb[32] + z[0];
z[24] = -z[0] + z[1];
z[24] = abb[28] * z[24];
z[25] = abb[29] + abb[32];
z[26] = abb[27] * (T(1) / T(2));
z[27] = -abb[28] + z[26];
z[28] = z[25] + -z[27];
z[28] = z[26] * z[28];
z[29] = abb[27] * (T(3) / T(2));
z[30] = -abb[32] + z[29];
z[31] = abb[31] * (T(1) / T(4)) + -z[1] + z[30];
z[31] = abb[31] * z[31];
z[32] = abb[34] * (T(1) / T(2));
z[18] = -abb[33] + z[10] + (T(1) / T(2)) * z[18] + -z[20] + -z[23] + z[24] + z[28] + z[31] + z[32];
z[18] = abb[6] * z[18];
z[24] = 2 * z[11];
z[28] = abb[31] * (T(1) / T(2));
z[31] = z[24] + z[28];
z[31] = abb[31] * z[31];
z[33] = abb[28] * (T(1) / T(2));
z[34] = -z[12] + z[33];
z[35] = abb[30] * (T(1) / T(4)) + z[34];
z[35] = abb[30] * z[35];
z[0] = abb[28] * z[0];
z[36] = abb[33] + -abb[34];
z[37] = prod_pow(abb[32], 2);
z[38] = (T(1) / T(6)) * z[16];
z[39] = abb[54] + z[38];
z[40] = abb[28] + abb[32];
z[40] = abb[27] * z[40];
z[31] = -z[0] + -z[23] + z[31] + z[35] + -z[36] + -z[37] + -z[39] + z[40];
z[31] = abb[3] * z[31];
z[35] = 2 * abb[34];
z[40] = 2 * abb[54];
z[41] = z[35] + -z[40];
z[42] = abb[36] + z[41];
z[43] = 3 * abb[29];
z[44] = -abb[32] + z[43];
z[45] = abb[32] * z[44];
z[46] = abb[28] * abb[32];
z[45] = z[45] + z[46];
z[47] = (T(7) / T(2)) * z[15];
z[48] = z[45] + -z[47];
z[49] = abb[31] + 3 * z[19];
z[49] = z[28] * z[49];
z[50] = abb[30] * (T(-5) / T(2)) + z[12];
z[50] = z[21] * z[50];
z[51] = abb[33] * (T(-3) / T(2)) + abb[52] * (T(1) / T(2));
z[52] = (T(5) / T(3)) * z[16];
z[53] = abb[32] + z[43];
z[53] = -abb[27] + (T(1) / T(2)) * z[53];
z[53] = abb[27] * z[53];
z[48] = -z[42] + (T(1) / T(2)) * z[48] + z[49] + z[50] + z[51] + -z[52] + z[53];
z[48] = abb[0] * z[48];
z[49] = abb[27] + abb[31];
z[50] = 4 * abb[32];
z[53] = abb[30] * (T(7) / T(2));
z[49] = abb[28] + 4 * z[49] + -z[50] + -z[53];
z[49] = abb[30] * z[49];
z[54] = prod_pow(abb[28], 2);
z[55] = abb[35] + abb[36];
z[56] = (T(1) / T(2)) * z[54] + z[55];
z[57] = abb[31] + z[24];
z[57] = abb[31] * z[57];
z[58] = abb[27] * z[6];
z[59] = 2 * abb[52];
z[49] = 4 * abb[54] + -z[37] + z[49] + -z[56] + -z[57] + z[58] + z[59];
z[49] = abb[2] * z[49];
z[14] = z[14] + z[40];
z[24] = abb[31] * z[24];
z[57] = (T(1) / T(3)) * z[16];
z[24] = -z[14] + z[24] + z[57];
z[24] = abb[12] * z[24];
z[49] = z[24] + z[49];
z[58] = -abb[28] + z[21];
z[60] = abb[30] * z[58];
z[56] = z[56] + z[60];
z[61] = prod_pow(abb[27], 2);
z[62] = 2 * abb[33];
z[63] = -z[15] + -z[56] + z[61] + z[62];
z[63] = abb[9] * z[63];
z[64] = -abb[29] + z[33];
z[65] = abb[28] * z[64];
z[65] = abb[36] + -abb[53] + z[65];
z[66] = (T(1) / T(2)) * z[15];
z[67] = z[65] + z[66];
z[68] = abb[27] * z[27];
z[69] = abb[33] + -abb[35];
z[68] = -abb[52] + -z[60] + -z[67] + z[68] + z[69];
z[70] = abb[5] * (T(1) / T(2));
z[71] = z[68] * z[70];
z[72] = -abb[28] + z[28];
z[72] = abb[31] * z[72];
z[60] = abb[35] + z[39] + z[60] + -z[72];
z[72] = abb[4] * (T(1) / T(2));
z[73] = z[60] * z[72];
z[71] = z[71] + z[73];
z[73] = abb[29] * abb[32];
z[73] = -z[15] + z[73];
z[74] = -abb[29] + abb[32];
z[75] = abb[27] * z[74];
z[75] = -z[73] + z[75];
z[76] = abb[31] * z[74];
z[76] = -abb[34] + -z[75] + z[76];
z[77] = 2 * abb[11];
z[76] = z[76] * z[77];
z[78] = -z[15] + z[37];
z[79] = abb[28] * z[74];
z[79] = -abb[53] + z[79];
z[78] = abb[34] + (T(1) / T(2)) * z[78] + -z[79];
z[78] = abb[7] * z[78];
z[76] = -z[76] + (T(1) / T(2)) * z[78];
z[80] = -z[71] + z[76];
z[81] = z[11] * z[26];
z[82] = z[12] * z[28];
z[83] = z[12] + -z[21];
z[84] = abb[30] * z[83];
z[85] = z[39] + z[84];
z[81] = -abb[52] + z[81] + z[82] + -z[85];
z[86] = -abb[15] * z[81];
z[87] = -abb[29] + z[17];
z[88] = z[26] + z[87];
z[89] = -abb[27] + abb[31];
z[88] = -z[88] * z[89];
z[87] = abb[32] * z[87];
z[90] = abb[34] + z[87];
z[91] = z[66] + z[90];
z[92] = z[88] + z[91];
z[93] = abb[33] + z[92];
z[94] = abb[14] * z[93];
z[95] = abb[18] * (T(1) / T(2));
z[96] = -abb[16] + z[95];
z[96] = abb[37] * z[96];
z[97] = abb[37] * (T(1) / T(2));
z[98] = -abb[19] * z[97];
z[3] = z[3] + z[18] + z[31] + z[48] + -z[49] + -z[63] + -z[80] + z[86] + z[94] + z[96] + z[98];
z[3] = abb[47] * z[3];
z[18] = -z[33] * z[64];
z[31] = z[2] + z[7];
z[31] = abb[27] * z[31];
z[8] = -abb[31] + -z[8];
z[8] = abb[31] * z[8];
z[48] = abb[52] + -abb[54];
z[5] = -z[5] + z[8] + z[10] + (T(1) / T(4)) * z[15] + z[18] + z[31] + z[48] + z[57] + z[90];
z[5] = abb[8] * z[5];
z[8] = -abb[32] + -z[7];
z[8] = 2 * z[8] + -z[28];
z[8] = abb[31] * z[8];
z[10] = abb[30] * (T(5) / T(4)) + z[34];
z[10] = abb[30] * z[10];
z[18] = 2 * abb[29];
z[31] = -abb[32] + z[18];
z[34] = -abb[27] + z[31];
z[64] = abb[28] + -z[34];
z[64] = abb[27] * z[64];
z[0] = abb[33] + -z[0] + -z[4] + z[8] + z[10] + -z[22] + z[52] + z[64] + -2 * z[73];
z[0] = abb[3] * z[0];
z[4] = 4 * abb[29] + -abb[32];
z[4] = abb[32] * z[4];
z[8] = -abb[27] + 2 * z[31];
z[8] = abb[27] * z[8];
z[10] = abb[31] + 2 * z[34];
z[10] = abb[31] * z[10];
z[4] = -4 * abb[34] + z[4] + z[8] + -z[10] + -z[47] + z[57] + -z[62] + -z[65];
z[4] = abb[1] * z[4];
z[8] = 3 * abb[32];
z[10] = z[7] + z[8] + -z[26];
z[10] = abb[27] * z[10];
z[22] = abb[35] + z[65];
z[47] = abb[54] + z[22];
z[10] = z[10] + -z[47] + z[87];
z[52] = -abb[32] + abb[31] * (T(-3) / T(4)) + z[1] + z[27];
z[52] = abb[31] * z[52];
z[13] = abb[28] + z[13];
z[64] = z[13] * z[21];
z[32] = abb[52] + z[32];
z[10] = (T(1) / T(2)) * z[10] + (T(1) / T(4)) * z[16] + z[32] + z[52] + z[64];
z[10] = abb[6] * z[10];
z[52] = z[57] + z[59];
z[59] = prod_pow(abb[30], 2);
z[59] = -z[52] + -z[59] + z[61] + -z[67];
z[59] = abb[10] * z[59];
z[0] = z[0] + -z[4] + z[5] + z[10] + -z[24] + -z[59] + -z[80] + -z[94];
z[0] = abb[46] * z[0];
z[5] = z[17] + -z[18];
z[5] = abb[32] * z[5];
z[10] = z[9] + -z[18];
z[24] = abb[27] * z[10];
z[10] = -z[10] + -z[26];
z[10] = abb[31] * z[10];
z[58] = z[21] * z[58];
z[5] = z[5] + z[10] + 2 * z[15] + z[23] + z[24] + z[35] + (T(1) / T(4)) * z[54] + z[58];
z[5] = abb[3] * z[5];
z[10] = abb[30] * z[13];
z[13] = z[10] + z[39];
z[22] = z[13] + -z[22];
z[23] = z[27] + z[74];
z[23] = abb[27] * z[23];
z[24] = abb[27] + -abb[29];
z[27] = z[24] + z[28];
z[54] = -abb[31] * z[27];
z[23] = z[22] + z[23] + z[54] + z[90];
z[23] = abb[33] + (T(1) / T(2)) * z[23];
z[23] = abb[6] * z[23];
z[54] = abb[27] * abb[32];
z[58] = -abb[31] + z[11];
z[58] = abb[31] * z[58];
z[58] = z[54] + z[57] + z[58] + -z[67];
z[58] = abb[8] * z[58];
z[4] = -z[4] + z[5] + z[23] + (T(1) / T(2)) * z[58] + z[63] + -z[71] + -z[76];
z[4] = abb[48] * z[4];
z[5] = -abb[32] * z[74];
z[5] = z[5] + -z[46] + -z[66];
z[1] = z[1] + -z[33];
z[23] = -abb[32] + z[26];
z[58] = -z[1] + -z[23] + -z[28];
z[58] = abb[31] * z[58];
z[61] = abb[33] * (T(-1) / T(2)) + abb[52] * (T(3) / T(2));
z[63] = -abb[27] + (T(1) / T(2)) * z[25];
z[64] = abb[27] * z[63];
z[5] = (T(1) / T(2)) * z[5] + z[38] + z[40] + z[58] + z[61] + z[64] + (T(3) / T(2)) * z[84];
z[5] = abb[0] * z[5];
z[58] = -abb[5] * z[68];
z[64] = abb[31] * z[11];
z[54] = -z[37] + z[54] + -z[56] + z[64];
z[54] = abb[3] * z[54];
z[56] = abb[4] * z[60];
z[54] = z[54] + z[56] + z[58] + z[78];
z[56] = abb[31] + 3 * z[11];
z[58] = z[28] * z[56];
z[60] = -abb[32] * z[26];
z[14] = -z[14] + z[38] + z[58] + z[60] + (T(1) / T(2)) * z[67];
z[14] = abb[8] * z[14];
z[60] = -z[7] + z[23];
z[60] = abb[27] * z[60];
z[47] = -z[15] + -z[47] + z[60] + -z[87];
z[27] = z[27] * z[28];
z[20] = -z[20] + z[27] + -z[32] + (T(1) / T(2)) * z[47];
z[20] = abb[6] * z[20];
z[27] = abb[16] + -abb[17];
z[27] = z[27] * z[97];
z[5] = z[5] + z[14] + z[20] + z[27] + -z[49] + (T(1) / T(2)) * z[54] + z[59];
z[5] = abb[49] * z[5];
z[14] = abb[32] * z[25];
z[14] = z[14] + (T(-5) / T(2)) * z[15] + 3 * z[46];
z[20] = -abb[31] + z[19];
z[20] = z[20] * z[28];
z[25] = 3 * z[12] + -z[53];
z[25] = z[21] * z[25];
z[27] = abb[29] + z[8];
z[27] = (T(1) / T(2)) * z[27];
z[32] = -abb[27] + z[27];
z[32] = abb[27] * z[32];
z[14] = (T(1) / T(2)) * z[14] + (T(-4) / T(3)) * z[16] + z[20] + z[25] + z[32] + -z[42] + z[61];
z[14] = abb[46] * z[14];
z[16] = -z[26] + z[28];
z[20] = -abb[32] + z[33];
z[25] = abb[29] * (T(3) / T(2));
z[32] = -z[16] + -z[20] + -z[25];
z[32] = abb[31] * z[32];
z[33] = (T(-3) / T(2)) * z[15] + z[45];
z[42] = z[21] * z[83];
z[44] = -abb[27] + (T(1) / T(2)) * z[44];
z[44] = abb[27] * z[44];
z[32] = z[32] + (T(1) / T(2)) * z[33] + -z[35] + z[38] + z[42] + z[44] + z[51];
z[32] = abb[48] * z[32];
z[14] = z[14] + z[32];
z[14] = abb[0] * z[14];
z[32] = -abb[28] + z[17];
z[32] = abb[27] * z[32];
z[32] = z[32] + -z[82];
z[33] = (T(1) / T(2)) * z[37];
z[20] = abb[28] * z[20];
z[13] = -z[13] + z[20] + -z[32] + z[33] + -z[36] + z[55];
z[13] = abb[20] * z[13];
z[20] = z[15] + z[90];
z[42] = z[7] + z[26];
z[42] = abb[27] * z[42];
z[28] = z[28] + -z[74];
z[28] = abb[31] * z[28];
z[22] = z[20] + -z[22] + z[28] + z[42];
z[22] = abb[21] * z[22];
z[28] = z[7] + z[17];
z[42] = z[16] + -z[28];
z[42] = abb[31] * z[42];
z[28] = abb[27] * z[28];
z[20] = abb[52] + z[20] + z[28] + -z[39] + z[42] + z[65];
z[20] = abb[22] * z[20];
z[10] = z[10] + z[69];
z[28] = -z[10] + -z[32] + -z[39] + z[67];
z[28] = abb[23] * z[28];
z[32] = -abb[24] * z[81];
z[39] = -abb[25] * z[97];
z[13] = z[13] + z[20] + z[22] + z[28] + z[32] + z[39];
z[20] = abb[50] * (T(1) / T(2));
z[13] = z[13] * z[20];
z[22] = -abb[30] + 2 * z[12];
z[22] = abb[30] * z[22];
z[28] = 2 * abb[28];
z[32] = abb[32] + z[28];
z[32] = abb[27] * z[32];
z[39] = abb[31] * z[56];
z[32] = -z[22] + z[32] + -z[37] + z[39] + z[41] + -2 * z[46] + -z[57] + -z[62];
z[32] = abb[51] * z[32];
z[17] = abb[28] + z[17];
z[17] = abb[27] * z[17];
z[17] = -z[17] + z[33] + z[46] + -z[58] + z[85];
z[33] = z[17] + z[36];
z[36] = abb[41] + -abb[44];
z[37] = -z[33] * z[36];
z[39] = -abb[43] * z[17];
z[17] = -abb[34] + z[17];
z[17] = abb[45] * z[17];
z[42] = -abb[43] + abb[45];
z[44] = abb[33] * z[42];
z[45] = abb[34] * abb[43];
z[17] = z[17] + z[37] + z[39] + z[44] + z[45];
z[33] = -abb[42] * z[33];
z[37] = abb[48] * (T(1) / T(2));
z[39] = -abb[46] + -abb[47] + z[37];
z[39] = abb[37] * z[39];
z[17] = (T(1) / T(2)) * z[17] + z[32] + z[33] + z[39];
z[17] = abb[17] * z[17];
z[8] = z[8] + -z[18] + z[28];
z[18] = abb[27] * z[8];
z[32] = -z[8] + -z[89];
z[32] = abb[31] * z[32];
z[33] = abb[32] * z[31];
z[15] = -z[15] + z[33];
z[18] = -z[15] + z[18] + z[32] + z[41] + z[52];
z[18] = abb[51] * z[18];
z[9] = z[7] + z[9];
z[16] = z[9] + z[16];
z[16] = abb[31] * z[16];
z[9] = abb[27] * z[9];
z[9] = -z[9] + z[16] + -z[38] + -z[48];
z[16] = z[9] + -z[91];
z[32] = -abb[45] + z[36];
z[33] = -z[16] * z[32];
z[38] = z[66] + z[87];
z[9] = -z[9] + z[38];
z[9] = abb[43] * z[9];
z[9] = z[9] + z[33] + z[45];
z[16] = -abb[42] * z[16];
z[9] = (T(1) / T(2)) * z[9] + z[16] + z[18];
z[9] = abb[16] * z[9];
z[16] = z[36] * z[93];
z[18] = z[38] + z[88];
z[18] = abb[43] * z[18];
z[33] = -abb[45] * z[92];
z[16] = z[16] + z[18] + z[33] + -z[44] + z[45];
z[18] = z[34] * z[89];
z[15] = -z[15] + z[18] + z[35] + z[62];
z[15] = abb[51] * z[15];
z[18] = abb[42] * z[93];
z[15] = z[15] + (T(1) / T(2)) * z[16] + z[18];
z[15] = abb[18] * z[15];
z[16] = z[36] + -z[42];
z[18] = abb[51] + abb[42] * (T(1) / T(2)) + (T(1) / T(4)) * z[16];
z[33] = -abb[3] + -abb[8] + abb[14];
z[33] = z[18] * z[33];
z[35] = -2 * abb[42] + -4 * abb[51] + -z[16];
z[35] = abb[26] * z[35];
z[16] = (T(1) / T(2)) * z[16];
z[36] = abb[42] + 2 * abb[51];
z[38] = z[16] + z[36];
z[39] = -abb[6] * z[38];
z[37] = -abb[46] + -z[37];
z[37] = abb[16] * z[37];
z[41] = -abb[46] * z[95];
z[33] = z[33] + z[35] + z[37] + z[39] + z[41];
z[33] = abb[37] * z[33];
z[35] = abb[27] * z[11];
z[12] = abb[31] * z[12];
z[12] = z[12] + -z[22] + z[35] + -z[40] + -z[52];
z[12] = abb[51] * z[12];
z[16] = abb[42] + z[16];
z[16] = z[16] * z[81];
z[22] = abb[46] * z[97];
z[12] = z[12] + z[16] + z[22];
z[12] = abb[19] * z[12];
z[16] = abb[46] * z[81];
z[18] = abb[37] * z[18];
z[16] = z[16] + z[18];
z[16] = abb[15] * z[16];
z[18] = abb[31] * z[19];
z[10] = -abb[52] + -z[10] + z[18] + -z[75] + -z[79];
z[18] = abb[46] + -abb[47] + abb[48] + -abb[49];
z[22] = abb[13] * (T(1) / T(2));
z[18] = z[18] * z[22];
z[10] = z[10] * z[18];
z[0] = z[0] + z[3] + z[4] + z[5] + z[9] + z[10] + z[12] + z[13] + z[14] + z[15] + z[16] + z[17] + z[33];
z[3] = abb[40] * (T(1) / T(2));
z[4] = m1_set::bc<T>[0] * z[21];
z[5] = abb[39] * (T(1) / T(2));
z[9] = abb[31] * m1_set::bc<T>[0];
z[10] = -abb[27] + (T(-1) / T(2)) * z[74];
z[10] = m1_set::bc<T>[0] * z[10];
z[10] = abb[38] + -z[3] + -z[4] + z[5] + (T(3) / T(2)) * z[9] + z[10];
z[10] = abb[6] * z[10];
z[12] = (T(1) / T(2)) * z[7];
z[13] = 2 * abb[27];
z[14] = -z[12] + -z[13];
z[14] = m1_set::bc<T>[0] * z[14];
z[15] = 2 * z[9];
z[16] = -abb[38] + abb[40];
z[14] = z[5] + z[14] + z[15] + -z[16];
z[14] = abb[8] * z[14];
z[17] = -abb[27] + z[8];
z[17] = m1_set::bc<T>[0] * z[17];
z[21] = 3 * abb[40];
z[22] = abb[30] * m1_set::bc<T>[0];
z[17] = z[9] + z[17] + -z[21] + z[22];
z[17] = abb[3] * z[17];
z[2] = abb[28] + z[2] + z[13] + -z[43];
z[2] = m1_set::bc<T>[0] * z[2];
z[2] = -abb[39] + z[2] + -z[15];
z[2] = abb[1] * z[2];
z[15] = m1_set::bc<T>[0] * z[74];
z[33] = z[15] * z[77];
z[35] = abb[7] * z[15];
z[33] = -z[33] + (T(1) / T(2)) * z[35];
z[2] = z[2] + z[33];
z[37] = abb[28] * m1_set::bc<T>[0];
z[39] = abb[40] + z[9];
z[37] = z[37] + -z[39];
z[40] = z[37] * z[72];
z[24] = m1_set::bc<T>[0] * z[24];
z[41] = abb[38] + z[24];
z[42] = z[41] * z[70];
z[40] = z[40] + z[42];
z[7] = z[7] + z[13];
z[7] = m1_set::bc<T>[0] * z[7];
z[7] = 2 * abb[38] + -abb[39] + z[7];
z[7] = abb[10] * z[7];
z[42] = -z[22] + z[39];
z[11] = m1_set::bc<T>[0] * z[11];
z[43] = abb[38] + z[11];
z[44] = z[42] + z[43];
z[45] = abb[15] * z[44];
z[46] = abb[5] + -abb[7];
z[47] = z[5] * z[46];
z[48] = abb[40] + z[11] + -z[22];
z[49] = 2 * abb[12];
z[48] = z[48] * z[49];
z[10] = z[2] + z[7] + z[10] + z[14] + z[17] + -z[40] + -z[45] + z[47] + z[48];
z[10] = abb[46] * z[10];
z[14] = abb[28] * (T(3) / T(2)) + -z[13] + -z[25] + z[50];
z[14] = m1_set::bc<T>[0] * z[14];
z[17] = 2 * z[22];
z[14] = -abb[38] + z[5] + z[14] + z[17] + -z[21];
z[14] = abb[8] * z[14];
z[21] = -z[28] + -z[29] + z[31];
z[21] = m1_set::bc<T>[0] * z[21];
z[25] = abb[38] * (T(1) / T(2)) + -z[4];
z[28] = 2 * abb[40];
z[21] = -z[9] + z[21] + z[25] + z[28];
z[21] = abb[0] * z[21];
z[28] = z[9] + z[28];
z[17] = z[17] + -z[28];
z[29] = z[17] + -z[43];
z[31] = 2 * abb[2];
z[29] = z[29] * z[31];
z[29] = z[29] + z[48];
z[24] = abb[9] * z[24];
z[24] = 2 * z[24] + -z[40];
z[3] = z[3] + -z[4] + (T(1) / T(2)) * z[9];
z[4] = z[3] + -z[5];
z[13] = -z[13] + z[27];
z[13] = m1_set::bc<T>[0] * z[13];
z[13] = -z[4] + z[13];
z[13] = abb[6] * z[13];
z[27] = -3 * z[11] + -z[42];
z[27] = abb[3] * z[27];
z[13] = z[13] + z[14] + z[21] + z[24] + z[27] + z[29] + z[33] + z[45] + z[47];
z[13] = abb[47] * z[13];
z[14] = -abb[4] * z[37];
z[21] = abb[5] * z[41];
z[27] = -abb[39] * z[46];
z[14] = z[14] + z[21] + z[27] + -z[35];
z[21] = m1_set::bc<T>[0] * z[63];
z[4] = -abb[38] + -z[4] + z[21];
z[4] = abb[6] * z[4];
z[1] = -z[1] + z[6];
z[1] = m1_set::bc<T>[0] * z[1];
z[1] = z[1] + -z[5] + z[17];
z[1] = abb[8] * z[1];
z[6] = -abb[38] + z[22];
z[6] = (T(-3) / T(2)) * z[6] + z[28];
z[17] = m1_set::bc<T>[0] * z[30];
z[17] = z[6] + z[17];
z[17] = abb[0] * z[17];
z[21] = -abb[3] * z[11];
z[1] = z[1] + z[4] + -z[7] + (T(1) / T(2)) * z[14] + z[17] + z[21] + z[29];
z[1] = abb[49] * z[1];
z[4] = -abb[27] + -z[12];
z[4] = m1_set::bc<T>[0] * z[4];
z[4] = z[4] + z[5] + z[9];
z[4] = abb[8] * z[4];
z[7] = -abb[5] + -abb[7];
z[5] = z[5] * z[7];
z[7] = -abb[39] + z[15] + -z[42];
z[12] = abb[6] * z[7];
z[14] = -abb[3] * m1_set::bc<T>[0] * z[34];
z[2] = z[2] + z[4] + z[5] + (T(-1) / T(2)) * z[12] + z[14] + -z[24];
z[2] = abb[48] * z[2];
z[4] = abb[21] + abb[23];
z[4] = z[4] * z[7];
z[5] = -abb[20] * z[42];
z[7] = m1_set::bc<T>[0] * z[19];
z[7] = abb[38] + -abb[39] + z[7];
z[12] = z[7] + -z[39];
z[12] = abb[22] * z[12];
z[14] = abb[24] * z[44];
z[4] = z[4] + z[5] + z[12] + z[14];
z[4] = z[4] * z[20];
z[5] = -z[8] + -z[26];
z[5] = m1_set::bc<T>[0] * z[5];
z[5] = z[5] + z[6];
z[5] = abb[46] * z[5];
z[6] = m1_set::bc<T>[0] * z[23];
z[6] = z[6] + z[9] + z[25];
z[6] = abb[48] * z[6];
z[5] = z[5] + z[6];
z[5] = abb[0] * z[5];
z[6] = abb[27] * m1_set::bc<T>[0];
z[6] = z[6] + -z[9] + z[16];
z[6] = -abb[16] * z[6] * z[38];
z[8] = -abb[19] * z[38] * z[44];
z[3] = z[3] + z[11];
z[9] = -abb[43] + -z[32];
z[3] = z[3] * z[9];
z[9] = 2 * z[11] + z[42];
z[9] = -z[9] * z[36];
z[3] = z[3] + z[9];
z[3] = abb[17] * z[3];
z[7] = z[7] + -z[22];
z[7] = -z[7] * z[18];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[10] + z[13];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_3_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("3.0212403926155634861881016532672724583386885189432450770666360627"),stof<T>("-6.1454510857536799062108407455617833387997981486530960398138588694")}, std::complex<T>{stof<T>("6.0424807852311269723762033065345449166773770378864901541332721254"),stof<T>("-12.2909021715073598124216814911235666775995962973061920796277177388")}, std::complex<T>{stof<T>("3.0212403926155634861881016532672724583386885189432450770666360627"),stof<T>("-6.1454510857536799062108407455617833387997981486530960398138588694")}, std::complex<T>{stof<T>("-3.0212403926155634861881016532672724583386885189432450770666360627"),stof<T>("6.1454510857536799062108407455617833387997981486530960398138588694")}, std::complex<T>{stof<T>("-3.0212403926155634861881016532672724583386885189432450770666360627"),stof<T>("6.1454510857536799062108407455617833387997981486530960398138588694")}, std::complex<T>{stof<T>("-6.9677034655081426145254938301974874778519076555609150602244021842"),stof<T>("-2.4670173279991011741196022669295648127619777470011308985795000179")}, std::complex<T>{stof<T>("22.530656495243534782469072771938344985092784825473562749524909763"),stof<T>("2.078533442339936977156719992571484005950635767989453659081975135")}, std::complex<T>{stof<T>("1.1267222078670382437443633273720852016511522812744344454673585005"),stof<T>("2.0332855638317073435153816395082973838495214591324097321920347361")}, std::complex<T>{stof<T>("0.0620301157883849391947034340858736621477752766585777805754366075"),stof<T>("-1.6123378821367580291422256596178032175735468016471634226558303079")}, std::complex<T>{stof<T>("-0.65014392619570208351699626144945763353564230515482995745745358731"),stof<T>("0.79808681113405645396023651105679316073000096384394056216966926958")}, std::complex<T>{stof<T>("12.084961570462253944752406613069089833354754075772980308266544251"),stof<T>("-24.581804343014719624843362982247133355199192594612384159255435478")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[123].real()/kbase.W[123].real()), rlog(k.W[128].real()/kbase.W[128].real()), C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}, rlog(k.W[194].real()/kbase.W[194].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_3_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_3_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-42.890624163918968045148916689928925894404543873940612323993144384"),stof<T>("-9.990433145954740370336546595559296574844265280949463489817276881")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,55> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W21(k,dl), dlog_W22(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real()), rlog(kend.W[128].real()/k.W[128].real()), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), rlog(kend.W[194].real()/k.W[194].real()), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k)};

                    
            return f_4_3_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_3_DLogXconstant_part(base_point<T>, kend);
	value += f_4_3_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_3_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_3_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_3_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_3_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_3_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_3_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
