/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_879.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_879_abbreviated (const std::array<T,81>& abb) {
T z[146];
z[0] = abb[4] * abb[68];
z[1] = 2 * z[0];
z[2] = abb[67] + abb[70];
z[3] = abb[4] * z[2];
z[4] = abb[3] * z[2];
z[5] = z[1] + z[3] + z[4];
z[6] = abb[28] + abb[30];
z[7] = abb[27] + abb[29];
z[8] = 2 * z[7];
z[9] = -abb[31] + abb[32] + z[6] + z[8];
z[10] = abb[61] + abb[65];
z[9] = z[9] * z[10];
z[11] = 2 * abb[70];
z[12] = abb[67] + abb[69];
z[13] = z[11] + z[12];
z[14] = abb[6] * z[13];
z[15] = abb[10] * z[2];
z[16] = 2 * z[15];
z[17] = z[14] + -z[16];
z[18] = 4 * abb[1];
z[19] = abb[69] + z[2];
z[20] = abb[68] + z[19];
z[21] = z[18] * z[20];
z[22] = abb[68] + z[2];
z[23] = abb[12] * z[22];
z[24] = 2 * z[23];
z[25] = z[21] + -z[24];
z[26] = abb[62] + abb[64];
z[27] = abb[32] + z[7];
z[28] = -z[26] * z[27];
z[29] = -abb[31] + z[7];
z[30] = z[6] + z[29];
z[31] = abb[63] + abb[66];
z[32] = z[30] * z[31];
z[33] = abb[69] + abb[70];
z[34] = abb[5] * z[33];
z[35] = abb[16] * z[33];
z[36] = z[34] + -z[35];
z[37] = 2 * abb[69];
z[38] = abb[67] + -abb[70];
z[39] = -z[37] + -z[38];
z[39] = abb[7] * z[39];
z[40] = abb[67] + -abb[69];
z[41] = abb[15] * z[40];
z[9] = -z[5] + z[9] + z[17] + z[25] + z[28] + -z[32] + z[36] + z[39] + z[41];
z[9] = abb[42] * z[9];
z[28] = abb[17] * z[2];
z[39] = z[16] + -z[28];
z[42] = abb[4] * z[38];
z[42] = -z[39] + z[42];
z[43] = abb[2] * z[2];
z[44] = abb[7] * z[2];
z[45] = z[43] + -z[44];
z[46] = abb[6] * z[38];
z[47] = abb[33] + z[7];
z[48] = abb[66] * z[47];
z[46] = z[42] + -z[45] + -z[46] + -z[48];
z[48] = abb[63] * z[47];
z[49] = z[46] + -z[48];
z[50] = z[26] * z[47];
z[50] = z[49] + z[50];
z[50] = abb[37] * z[50];
z[9] = z[9] + z[50];
z[51] = abb[3] * z[12];
z[52] = -z[41] + z[51];
z[53] = -z[14] + z[52];
z[54] = abb[26] + z[6];
z[55] = abb[32] + z[54];
z[56] = z[8] + z[55];
z[57] = abb[65] * z[56];
z[58] = -abb[61] * z[56];
z[59] = 4 * z[23];
z[60] = abb[7] * z[12];
z[61] = abb[0] * z[19];
z[62] = 2 * z[61];
z[63] = abb[13] * z[12];
z[21] = -z[21] + z[53] + -z[57] + z[58] + z[59] + 2 * z[60] + z[62] + -z[63];
z[21] = abb[40] * z[21];
z[58] = abb[31] + z[54];
z[64] = z[10] * z[58];
z[65] = -z[28] + z[62];
z[66] = -z[63] + z[65];
z[67] = abb[31] + abb[33];
z[68] = abb[66] * z[67];
z[69] = abb[6] * z[12];
z[70] = abb[4] * z[33];
z[43] = z[35] + -z[43] + z[64] + -z[66] + -z[68] + -z[69] + -z[70];
z[64] = 2 * abb[43];
z[43] = z[43] * z[64];
z[67] = z[64] * z[67];
z[68] = abb[63] * z[67];
z[43] = z[43] + -z[68];
z[71] = -abb[33] + z[30];
z[72] = abb[66] * z[71];
z[66] = -z[36] + -z[44] + -z[51] + -z[66] + z[72];
z[72] = abb[63] * z[71];
z[73] = abb[26] + abb[31];
z[74] = z[10] * z[73];
z[72] = z[66] + z[72] + z[74];
z[74] = 2 * abb[36];
z[75] = z[72] * z[74];
z[76] = 2 * abb[1];
z[20] = z[20] * z[76];
z[77] = -z[20] + z[24];
z[78] = -z[10] + z[31];
z[78] = z[29] * z[78];
z[79] = abb[69] + -abb[70];
z[80] = abb[7] * z[79];
z[81] = -abb[6] * z[33];
z[78] = z[35] + z[77] + z[78] + z[80] + z[81];
z[81] = 2 * abb[39];
z[78] = z[78] * z[81];
z[82] = -abb[32] + z[54];
z[83] = z[10] * z[82];
z[84] = abb[4] * z[12];
z[16] = -z[16] + z[84];
z[84] = -z[16] + -z[61];
z[83] = z[53] + z[63] + z[83] + 2 * z[84];
z[83] = abb[41] * z[83];
z[84] = abb[33] * z[31];
z[5] = z[5] + -z[24] + z[39] + z[84];
z[5] = abb[38] * z[5];
z[5] = 2 * z[5] + 2 * z[9] + z[21] + -z[43] + z[75] + z[78] + z[83];
z[5] = m1_set::bc<T>[0] * z[5];
z[9] = 2 * abb[50];
z[21] = z[9] * z[66];
z[66] = z[10] * z[27];
z[16] = -z[16] + z[53] + z[60] + -z[66] + z[77];
z[53] = 2 * abb[52];
z[60] = -z[16] * z[53];
z[30] = z[10] * z[30];
z[66] = abb[3] * z[79];
z[30] = z[30] + -z[32] + z[66] + -z[80];
z[32] = 2 * abb[67];
z[75] = z[32] + z[33];
z[77] = abb[4] * z[75];
z[1] = z[1] + -z[20] + -z[30] + -z[36] + z[77];
z[1] = 2 * z[1];
z[36] = -abb[51] * z[1];
z[77] = abb[40] * z[56];
z[78] = -abb[33] + z[54];
z[83] = z[7] + z[78];
z[84] = z[74] * z[83];
z[78] = z[64] * z[78];
z[85] = 2 * abb[33];
z[86] = abb[38] * z[85];
z[87] = abb[41] * z[82];
z[77] = -z[77] + -z[78] + z[84] + z[86] + z[87];
z[77] = m1_set::bc<T>[0] * z[77];
z[84] = z[9] * z[83];
z[53] = z[27] * z[53];
z[86] = 2 * z[47];
z[88] = abb[53] * z[86];
z[89] = abb[35] * abb[54];
z[53] = z[53] + z[77] + z[84] + -z[88] + z[89];
z[77] = -abb[80] + -z[53];
z[77] = abb[64] * z[77];
z[53] = -abb[62] * z[53];
z[46] = abb[53] * z[46];
z[71] = abb[50] * z[71];
z[84] = -abb[53] * z[47];
z[71] = z[71] + z[84];
z[71] = abb[63] * z[71];
z[46] = z[46] + z[71];
z[71] = abb[20] + -abb[23];
z[71] = z[12] * z[71];
z[84] = abb[19] * z[13];
z[88] = abb[24] * z[40];
z[71] = z[71] + -z[84] + -z[88];
z[84] = -abb[54] * z[71];
z[9] = z[9] * z[73];
z[9] = z[9] + z[89];
z[9] = z[9] * z[10];
z[73] = -abb[57] + -abb[58];
z[73] = z[19] * z[73];
z[88] = -abb[69] + z[2];
z[89] = abb[55] * z[88];
z[90] = -abb[70] + z[12];
z[91] = abb[56] * z[90];
z[92] = 2 * abb[60] + -abb[65];
z[93] = abb[66] + -z[92];
z[93] = abb[80] * z[93];
z[5] = z[5] + z[9] + z[21] + z[36] + 2 * z[46] + z[53] + z[60] + z[73] + z[77] + z[84] + z[89] + z[91] + z[93];
z[5] = 2 * z[5];
z[9] = z[33] * z[76];
z[21] = abb[7] * z[19];
z[36] = z[9] + -z[21];
z[46] = 3 * abb[69] + z[2];
z[46] = abb[2] * z[46];
z[53] = abb[3] * z[19];
z[60] = z[46] + -z[53];
z[73] = 3 * abb[70];
z[77] = z[12] + z[73];
z[77] = abb[6] * z[77];
z[84] = z[41] + -z[63];
z[89] = -abb[28] + abb[33];
z[91] = z[7] + -z[89];
z[91] = abb[66] * z[91];
z[93] = abb[60] * z[8];
z[94] = abb[14] * z[38];
z[95] = abb[9] * z[33];
z[96] = abb[26] + -abb[32];
z[96] = abb[61] * z[96];
z[97] = -abb[32] + z[7];
z[98] = abb[26] + z[97];
z[98] = abb[65] * z[98];
z[89] = -abb[63] * z[89];
z[89] = z[28] + -z[34] + z[36] + z[60] + -z[70] + z[77] + -z[84] + z[89] + z[91] + -z[93] + -z[94] + -6 * z[95] + z[96] + z[98];
z[89] = abb[36] * z[89];
z[91] = abb[16] * abb[70];
z[96] = -z[61] + z[91];
z[98] = 2 * abb[31];
z[99] = abb[60] * z[98];
z[100] = z[96] + z[99];
z[101] = abb[2] * z[19];
z[102] = 2 * z[95];
z[103] = -abb[31] * abb[66];
z[103] = -z[100] + z[101] + -z[102] + -z[103];
z[14] = z[14] + -z[41];
z[104] = z[55] + z[98];
z[105] = abb[61] * z[104];
z[105] = z[63] + z[105];
z[106] = abb[3] * z[13];
z[107] = abb[70] * z[18];
z[108] = abb[65] * z[55];
z[103] = -z[14] + -2 * z[103] + z[105] + z[106] + -z[107] + z[108];
z[103] = abb[41] * z[103];
z[43] = -z[43] + z[89] + z[103];
z[43] = abb[36] * z[43];
z[89] = z[8] + -z[98];
z[103] = 2 * abb[30];
z[106] = abb[28] + z[89] + z[103];
z[106] = -z[31] * z[106];
z[89] = z[6] + z[89];
z[89] = z[10] * z[89];
z[108] = -z[34] + z[102];
z[109] = 2 * z[35];
z[110] = abb[30] * z[26];
z[111] = abb[68] + z[12];
z[112] = 2 * z[111];
z[113] = abb[11] * z[112];
z[0] = 4 * z[0];
z[114] = -3 * abb[67] + -abb[69] + -z[11];
z[114] = abb[4] * z[114];
z[17] = -z[0] + z[17] + z[20] + z[62] + z[66] + -2 * z[80] + z[89] + -z[94] + z[106] + -z[108] + -z[109] + z[110] + z[113] + z[114];
z[17] = abb[42] * z[17];
z[66] = abb[6] * z[11];
z[80] = abb[2] * z[37];
z[66] = z[66] + z[80];
z[80] = abb[31] + abb[32];
z[89] = z[10] * z[80];
z[95] = 4 * z[95];
z[106] = z[94] + z[95];
z[62] = -z[35] + z[62];
z[110] = abb[30] + z[29];
z[114] = abb[63] * z[110];
z[115] = abb[66] * z[110];
z[89] = -z[44] + -z[52] + -z[62] + -z[66] + z[70] + z[89] + z[106] + z[114] + z[115];
z[89] = z[74] * z[89];
z[115] = abb[16] * abb[69];
z[115] = -z[99] + z[115];
z[116] = abb[60] * z[7];
z[117] = z[115] + z[116];
z[118] = abb[8] * z[12];
z[119] = -z[15] + z[118];
z[120] = -z[61] + z[117] + -z[119];
z[121] = abb[70] + z[32];
z[121] = abb[4] * z[121];
z[121] = z[120] + z[121];
z[122] = 2 * abb[68];
z[123] = abb[69] + z[32];
z[73] = z[73] + z[122] + z[123];
z[73] = z[73] * z[76];
z[114] = z[94] + z[114];
z[124] = abb[7] * z[88];
z[114] = 2 * z[114] + -z[124];
z[59] = -z[0] + z[59];
z[124] = -z[7] + z[98];
z[125] = abb[65] * z[124];
z[126] = z[7] + z[103];
z[127] = abb[66] * z[126];
z[128] = abb[6] * z[90];
z[60] = -z[59] + z[60] + z[73] + z[114] + 2 * z[121] + z[125] + z[127] + -z[128];
z[60] = abb[38] * z[60];
z[73] = -z[99] + z[116];
z[121] = z[73] + -z[91];
z[127] = -z[61] + z[118];
z[129] = -z[15] + z[127];
z[130] = abb[4] * abb[69];
z[131] = -z[102] + z[121] + -z[129] + -z[130];
z[124] = abb[66] * z[124];
z[132] = z[76] * z[79];
z[124] = z[124] + -z[132];
z[133] = -z[21] + z[128];
z[134] = abb[3] * z[90];
z[134] = z[124] + -z[133] + z[134];
z[135] = 2 * abb[61];
z[136] = -z[80] * z[135];
z[137] = 2 * abb[32];
z[138] = z[7] + z[137];
z[139] = -abb[65] * z[138];
z[41] = 2 * z[41];
z[46] = -z[41] + z[46] + 2 * z[131] + z[134] + z[136] + z[139];
z[46] = abb[41] * z[46];
z[131] = abb[6] * z[19];
z[124] = z[124] + z[131];
z[15] = z[15] + z[127];
z[121] = -z[15] + z[121];
z[127] = abb[4] * abb[67];
z[136] = -z[121] + z[127];
z[139] = abb[2] * z[88];
z[140] = -z[53] + z[139];
z[141] = abb[65] * z[7];
z[142] = z[140] + z[141];
z[143] = -z[29] * z[135];
z[144] = abb[7] * z[90];
z[0] = z[0] + -z[124] + 2 * z[136] + -z[142] + z[143] + z[144];
z[0] = abb[40] * z[0];
z[136] = abb[41] * z[138];
z[138] = abb[30] + z[7];
z[143] = abb[32] + z[138];
z[74] = -z[74] * z[143];
z[144] = -abb[38] * z[126];
z[145] = abb[40] * z[7];
z[74] = z[74] + z[136] + z[144] + z[145];
z[74] = abb[64] * z[74];
z[136] = abb[40] * z[29];
z[80] = abb[41] * z[80];
z[143] = -abb[36] * z[143];
z[144] = -abb[38] * z[110];
z[80] = z[80] + z[136] + z[143] + z[144];
z[143] = 2 * abb[62];
z[80] = z[80] * z[143];
z[144] = -abb[38] + -abb[40];
z[144] = abb[11] * z[111] * z[144];
z[0] = z[0] + z[17] + z[46] + z[60] + z[74] + z[80] + z[89] + 4 * z[144];
z[0] = abb[42] * z[0];
z[17] = abb[26] + abb[28];
z[46] = 2 * z[17];
z[60] = -5 * z[7] + -z[46] + -z[103] + -z[137];
z[60] = abb[61] * z[60];
z[32] = z[32] + z[37];
z[74] = -abb[70] + z[32];
z[74] = abb[3] * z[74];
z[80] = abb[67] + abb[68];
z[80] = -4 * z[33] + -5 * z[80];
z[80] = z[76] * z[80];
z[63] = 2 * z[63];
z[8] = abb[33] + z[8];
z[89] = abb[66] * z[8];
z[103] = abb[67] + z[11];
z[103] = abb[2] * z[103];
z[41] = -z[41] + -z[42] + z[48] + z[60] + 4 * z[61] + -z[63] + z[74] + z[80] + z[89] + -z[93] + z[103];
z[42] = -abb[70] + z[37];
z[42] = abb[67] + (T(2) / T(3)) * z[42];
z[42] = abb[7] * z[42];
z[48] = -abb[70] + abb[69] * (T(-1) / T(3));
z[48] = abb[67] * (T(-1) / T(3)) + 2 * z[48];
z[48] = abb[6] * z[48];
z[23] = (T(10) / T(3)) * z[23] + (T(1) / T(3)) * z[41] + z[42] + z[48] + (T(-2) / T(3)) * z[57];
z[41] = prod_pow(m1_set::bc<T>[0], 2);
z[23] = z[23] * z[41];
z[42] = z[11] + z[123];
z[48] = abb[2] + -abb[7];
z[42] = z[42] * z[48];
z[48] = -z[116] + -z[118];
z[60] = abb[4] * z[11];
z[8] = abb[30] + z[8];
z[61] = abb[66] * z[8];
z[74] = abb[33] + z[138];
z[80] = abb[63] * z[74];
z[39] = z[9] + z[39] + z[42] + 2 * z[48] + -z[53] + z[60] + z[61] + z[80] + z[94] + z[128] + z[141];
z[39] = abb[38] * z[39];
z[42] = z[125] + -z[132];
z[48] = abb[4] * abb[70];
z[60] = -z[48] + -z[102] + -z[120];
z[61] = abb[64] + -abb[66];
z[80] = z[61] * z[126];
z[89] = z[110] * z[143];
z[60] = -z[42] + 2 * z[60] + z[77] + z[80] + z[89] + -z[114] + -z[140];
z[60] = abb[42] * z[60];
z[77] = -z[93] + -z[115] + z[131];
z[37] = z[2] + z[37];
z[80] = abb[2] * z[37];
z[89] = -z[80] + z[106];
z[102] = -abb[33] + z[138];
z[103] = -z[98] + z[102];
z[106] = abb[63] * z[103];
z[106] = -z[65] + z[106];
z[110] = abb[7] * z[37];
z[114] = 2 * abb[65];
z[120] = -z[29] * z[114];
z[18] = abb[69] * z[18];
z[47] = -abb[30] + z[47];
z[123] = -abb[66] * z[47];
z[77] = -z[18] + -2 * z[77] + z[89] + z[106] + z[110] + z[120] + z[123];
z[77] = abb[36] * z[77];
z[28] = z[28] + -z[48] + z[117] + z[129];
z[48] = z[7] + z[85];
z[85] = abb[66] * z[48];
z[28] = -2 * z[28] + -z[42] + z[53] + z[85] + z[101] + z[133];
z[28] = abb[43] * z[28];
z[28] = z[28] + z[68];
z[42] = abb[66] * z[7];
z[68] = z[42] + z[133] + z[142];
z[85] = z[116] + z[127];
z[101] = z[15] + -z[85];
z[101] = z[9] + z[68] + 2 * z[101];
z[101] = abb[41] * z[101];
z[48] = abb[43] * z[48];
z[110] = abb[41] * z[7];
z[47] = abb[36] * z[47];
z[8] = -abb[38] * z[8];
z[8] = z[8] + z[47] + -z[48] + -z[110];
z[8] = abb[64] * z[8];
z[47] = abb[63] + -abb[65];
z[117] = -abb[62] + z[47];
z[117] = z[29] * z[117];
z[120] = abb[7] * abb[69];
z[120] = z[93] + z[120];
z[123] = -abb[6] + -z[76];
z[123] = abb[69] * z[123];
z[117] = z[115] + z[117] + z[120] + z[123];
z[81] = z[81] * z[117];
z[103] = abb[36] * z[103];
z[67] = z[67] + z[103];
z[74] = -abb[38] * z[74];
z[74] = -z[67] + z[74];
z[74] = abb[62] * z[74];
z[8] = z[8] + z[28] + z[39] + z[50] + z[60] + z[74] + z[77] + z[81] + z[101];
z[8] = abb[37] * z[8];
z[39] = z[6] + z[98];
z[50] = z[10] * z[39];
z[13] = -abb[4] * z[13];
z[39] = abb[33] + z[39];
z[60] = -abb[66] * z[39];
z[74] = 2 * z[118];
z[11] = -abb[2] * z[11];
z[77] = -abb[6] * z[79];
z[11] = z[11] + z[13] + z[50] + -z[51] + z[60] + -z[65] + z[74] + z[77] + -z[108] + z[109];
z[11] = abb[43] * z[11];
z[13] = z[121] + z[127];
z[33] = -abb[67] + z[33];
z[33] = abb[2] * z[33];
z[50] = -abb[61] * z[98];
z[13] = 2 * z[13] + z[33] + z[50] + z[134] + -z[141];
z[13] = abb[41] * z[13];
z[11] = z[11] + z[13];
z[11] = abb[43] * z[11];
z[13] = z[6] + z[7];
z[13] = z[13] * z[47];
z[33] = abb[61] + -abb[66];
z[50] = -z[6] * z[33];
z[60] = z[70] + z[108];
z[65] = abb[2] * abb[69];
z[77] = abb[3] * abb[70];
z[81] = abb[6] * abb[70];
z[9] = -z[9] + z[13] + z[50] + z[60] + -z[65] + z[77] + -z[81] + z[120];
z[13] = abb[62] * z[7];
z[13] = -z[9] + z[13];
z[50] = abb[48] * z[13];
z[6] = abb[31] + z[6];
z[101] = abb[32] + z[6];
z[101] = -z[10] * z[101];
z[103] = abb[28] + abb[31];
z[108] = abb[66] * z[103];
z[14] = z[14] + -z[34] + z[62] + -z[89] + z[101] + z[108];
z[14] = abb[44] * z[14];
z[16] = abb[73] * z[16];
z[62] = z[26] * z[83];
z[62] = z[62] + -z[72];
z[62] = abb[71] * z[62];
z[49] = abb[74] * z[49];
z[72] = abb[46] * z[79];
z[83] = abb[47] * z[40];
z[83] = z[72] + z[83];
z[83] = abb[2] * z[83];
z[12] = abb[47] * z[12];
z[12] = z[12] + -z[72];
z[12] = abb[6] * z[12];
z[72] = abb[38] * abb[40];
z[72] = abb[45] + z[72];
z[72] = z[72] * z[112];
z[89] = prod_pow(abb[38], 2) * z[111];
z[72] = z[72] + z[89];
z[72] = abb[11] * z[72];
z[89] = abb[46] * z[6];
z[101] = abb[66] * z[89];
z[12] = z[12] + z[14] + z[16] + -z[49] + z[50] + z[62] + z[72] + z[83] + -z[101];
z[9] = -abb[36] * z[9];
z[14] = z[91] + z[99];
z[16] = z[29] * z[33];
z[22] = z[22] * z[76];
z[49] = abb[7] * abb[70];
z[16] = -z[14] + z[16] + z[22] + -z[24] + z[49] + z[81] + z[93];
z[16] = abb[40] * z[16];
z[22] = z[76] * z[111];
z[24] = abb[31] * z[47];
z[47] = abb[3] * abb[69];
z[22] = z[22] + z[24] + z[47] + z[65] + -z[113] + -z[115];
z[22] = abb[38] * z[22];
z[24] = -z[25] + -z[30] + z[35] + z[60] + -z[66] + z[113];
z[24] = abb[42] * z[24];
z[25] = abb[63] + abb[66] + -z[10];
z[6] = z[6] * z[25];
z[25] = -abb[2] + abb[6];
z[25] = z[25] * z[79];
z[6] = z[6] + z[25] + -z[35] + z[60];
z[6] = abb[43] * z[6];
z[25] = -abb[2] + -z[76];
z[25] = abb[70] * z[25];
z[30] = abb[31] * z[33];
z[14] = z[14] + z[25] + z[30] + z[77];
z[14] = abb[41] * z[14];
z[25] = -abb[38] + -abb[41];
z[25] = abb[31] * z[25];
z[30] = abb[36] * z[7];
z[25] = z[25] + z[30] + -z[136];
z[25] = abb[62] * z[25];
z[6] = z[6] + z[9] + z[14] + z[16] + z[22] + z[24] + z[25];
z[9] = abb[39] * z[13];
z[6] = 2 * z[6] + z[9];
z[6] = abb[39] * z[6];
z[9] = z[31] * z[138];
z[3] = z[3] + -z[4] + z[9] + z[20] + -z[44] + -z[74] + z[80] + z[94];
z[4] = -abb[38] * z[3];
z[9] = -z[53] + z[115];
z[13] = -abb[66] * z[102];
z[14] = -abb[65] * z[98];
z[9] = -2 * z[9] + z[13] + z[14] + -z[45] + -z[94] + -z[106] + -z[107];
z[9] = abb[36] * z[9];
z[4] = z[4] + z[9] + -z[28];
z[4] = abb[38] * z[4];
z[9] = abb[70] + z[32];
z[13] = -abb[3] + abb[6];
z[9] = z[9] * z[13];
z[13] = z[119] + z[130];
z[14] = z[13] + -z[116];
z[16] = z[27] + -z[54];
z[20] = abb[65] * z[16];
z[22] = -abb[61] * z[82];
z[9] = z[9] + 2 * z[14] + z[20] + z[22] + z[36] + z[42] + z[84] + z[139];
z[9] = abb[41] * z[9];
z[13] = -z[13] + -z[73] + z[96];
z[14] = z[58] * z[135];
z[20] = z[46] + z[126];
z[22] = abb[65] * z[20];
z[13] = 2 * z[13] + z[14] + -z[21] + z[22] + z[63] + -z[124] + -z[140];
z[13] = abb[43] * z[13];
z[14] = -abb[66] * z[29];
z[14] = z[14] + z[21] + z[93] + -z[100];
z[14] = 2 * z[14] + -z[18] + z[52] + -z[57] + -z[69] + -z[105];
z[14] = abb[36] * z[14];
z[15] = -z[15] + -z[85];
z[18] = -z[75] + -z[122];
z[18] = z[18] * z[76];
z[15] = 2 * z[15] + z[18] + z[59] + z[68];
z[15] = abb[38] * z[15];
z[9] = z[9] + z[13] + z[14] + z[15];
z[9] = abb[40] * z[9];
z[13] = 2 * abb[45];
z[3] = -z[3] * z[13];
z[14] = abb[30] + abb[32];
z[15] = abb[44] * z[14];
z[18] = abb[73] * z[27];
z[21] = abb[47] * z[54];
z[15] = z[15] + z[18] + z[21];
z[13] = z[13] * z[138];
z[18] = abb[35] * abb[75];
z[22] = abb[74] * z[86];
z[13] = z[13] + 2 * z[15] + z[18] + -z[22];
z[15] = -abb[41] * z[16];
z[16] = -abb[43] * z[20];
z[18] = abb[36] * z[56];
z[20] = -abb[38] * z[7];
z[15] = z[15] + z[16] + z[18] + z[20];
z[15] = abb[40] * z[15];
z[16] = -abb[33] + z[17];
z[18] = -z[16] + -z[97];
z[18] = abb[36] * z[18];
z[20] = -abb[41] * z[55];
z[18] = z[18] + z[20] + z[78];
z[18] = abb[36] * z[18];
z[20] = abb[38] * z[138];
z[22] = abb[36] * z[102];
z[22] = z[20] + z[22] + z[48];
z[22] = abb[38] * z[22];
z[24] = abb[33] * abb[43];
z[25] = z[24] + z[110];
z[25] = abb[43] * z[25];
z[14] = z[14] + z[17];
z[7] = abb[33] * (T(-1) / T(3)) + z[7] + (T(2) / T(3)) * z[14];
z[7] = z[7] * z[41];
z[7] = z[7] + z[13] + z[15] + z[18] + z[22] + z[25];
z[7] = abb[64] * z[7];
z[14] = -abb[41] * z[104];
z[15] = abb[32] + -z[16];
z[15] = abb[36] * z[15];
z[14] = z[14] + z[15] + z[78];
z[14] = abb[36] * z[14];
z[15] = z[20] + z[67];
z[15] = abb[38] * z[15];
z[16] = -z[58] * z[64];
z[17] = abb[36] * z[104];
z[16] = z[16] + z[17] + z[87];
z[16] = abb[40] * z[16];
z[17] = abb[41] * z[98];
z[17] = z[17] + z[24];
z[17] = abb[43] * z[17];
z[18] = -abb[33] + 2 * z[56];
z[18] = z[18] * z[41];
z[13] = z[13] + z[14] + z[15] + z[16] + z[17] + (T(1) / T(3)) * z[18];
z[13] = abb[62] * z[13];
z[1] = abb[72] * z[1];
z[14] = abb[18] + -abb[25];
z[2] = z[2] * z[14];
z[14] = -abb[21] * z[37];
z[15] = -z[26] + z[31];
z[15] = abb[34] * z[15];
z[16] = abb[22] * z[38];
z[2] = z[2] + z[14] + z[15] + z[16];
z[2] = abb[49] * z[2];
z[14] = -z[114] + -z[135];
z[15] = z[21] + -z[89];
z[14] = z[14] * z[15];
z[15] = -z[34] + -z[35] + z[70];
z[15] = -2 * z[15] + -z[95];
z[15] = abb[46] * z[15];
z[16] = -abb[4] * z[40];
z[16] = -z[16] + z[51] + -z[74];
z[16] = -2 * z[16] + -z[63];
z[16] = abb[47] * z[16];
z[10] = -abb[35] * z[10];
z[10] = z[10] + z[71];
z[10] = abb[75] * z[10];
z[17] = abb[44] * z[103];
z[17] = z[17] + -z[89];
z[18] = -prod_pow(abb[43], 2) * z[39];
z[17] = 2 * z[17] + z[18];
z[17] = abb[63] * z[17];
z[18] = abb[78] + abb[79];
z[18] = z[18] * z[19];
z[19] = -z[61] + -z[92];
z[19] = abb[59] * z[19];
z[20] = -abb[77] * z[90];
z[21] = -abb[76] * z[88];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + 2 * z[12] + z[13] + z[14] + z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[23] + z[43];
z[0] = 2 * z[0];
return {z[5], z[0]};
}


template <typename T> std::complex<T> f_4_879_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{T(0),stof<T>("43.326309677614922727734444793627968626357572945891214112613952377")}, std::complex<T>{stof<T>("15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("-1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("-10.2312275182042318649043666124740212509707261335343338040808817496"),stof<T>("-5.344960794459669883860928844665613777776125166322616271306655103")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-10.23122751820423186490436661247402125097072613353433380408088175"),stof<T>("16.318194044347791480006293552148370535402661306622990785000321086")}, std::complex<T>{stof<T>("15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("-22.702888533420243111688185640602715821242669017374515282357675242")}, std::complex<T>{stof<T>("-5.201151409565616668135970091595661068285138441238639659659628698"),stof<T>("-15.278460349735009732185330308359639027338778762194082558949622032")}, std::complex<T>{stof<T>("-2.169899870971765045929976370262525225312196900570067233089928583"),stof<T>("17.137090592408046811409712938905675281762523669257852194474888349")}, std::complex<T>{stof<T>("25.514205731730803297405729886997480010359956010634664886931192621"),stof<T>("36.916552190357586975765762273827768645282794678010635435462489747")}, std::complex<T>{stof<T>("0.2116482453615020981651573360484742754108311832618865884313999034"),stof<T>("1.7606299963135058907292223372868390240467892650404665348344365107")}, std::complex<T>{stof<T>("4.2957629656815018373855767348030281230352659938409329766615800432"),stof<T>("-4.3189018782410098948513983227365868076716924192490656589465367641")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_879_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_879_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("17.54788233364647729229789933765235065335508026836022713055770326"),stof<T>("-38.493820526471870964829953237337169380356630871743224037035876198")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({202});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,81> abb = {dl[0], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W15(k,dl), dlog_W18(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_14(k), f_2_15(k), f_2_20(k), f_2_23(k), f_2_2_im(k), f_2_10_im(k), f_2_11_im(k), f_2_12_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, T{0}, T{0}, rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_2_re(k), f_2_10_re(k), f_2_11_re(k), f_2_12_re(k), f_2_25_re(k), T{0}, T{0}, T{0}, T{0}, T{0}};
{
auto c = dlog_W169(k,dv) * f_2_34(k);
abb[76] = c.real();
abb[55] = c.imag();
SpDLog_Sigma5<T,202,168>(k, dv, abb[76], abb[55], f_2_34_series_coefficients<T>);
}
{
auto c = dlog_W172(k,dv) * f_2_34(k);
abb[77] = c.real();
abb[56] = c.imag();
SpDLog_Sigma5<T,202,171>(k, dv, abb[77], abb[56], f_2_34_series_coefficients<T>);
}
{
auto c = dlog_W175(k,dv) * f_2_34(k);
abb[78] = c.real();
abb[57] = c.imag();
SpDLog_Sigma5<T,202,174>(k, dv, abb[78], abb[57], f_2_34_series_coefficients<T>);
}
{
auto c = dlog_W178(k,dv) * f_2_34(k);
abb[79] = c.real();
abb[58] = c.imag();
SpDLog_Sigma5<T,202,177>(k, dv, abb[79], abb[58], f_2_34_series_coefficients<T>);
}
{
auto c = dlog_W193(k,dv) * f_2_34(k);
abb[80] = c.real();
abb[59] = c.imag();
SpDLog_Sigma5<T,202,192>(k, dv, abb[80], abb[59], f_2_34_series_coefficients<T>);
}

                    
            return f_4_879_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_879_DLogXconstant_part(base_point<T>, kend);
	value += f_4_879_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_879_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_879_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_879_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_879_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_879_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_879_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
