/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_246.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_246_abbreviated (const std::array<T,25>& abb) {
T z[32];
z[0] = abb[20] + -abb[22];
z[1] = abb[18] + z[0];
z[2] = 2 * abb[21];
z[3] = z[1] + z[2];
z[3] = abb[4] * z[3];
z[4] = 3 * abb[19];
z[5] = z[0] + z[4];
z[6] = -abb[18] + abb[21];
z[7] = (T(1) / T(2)) * z[5] + z[6];
z[8] = abb[6] * z[7];
z[9] = 2 * abb[5];
z[10] = abb[19] + z[0];
z[9] = z[9] * z[10];
z[11] = -abb[18] + 2 * abb[19] + z[0];
z[12] = z[2] + -z[11];
z[12] = abb[0] * z[12];
z[13] = 7 * abb[19] + 5 * z[0];
z[14] = z[6] + (T(1) / T(2)) * z[13];
z[14] = abb[2] * z[14];
z[15] = 5 * abb[19];
z[16] = 7 * z[0] + z[15];
z[16] = -z[6] + (T(1) / T(2)) * z[16];
z[16] = abb[1] * z[16];
z[12] = -z[3] + -z[8] + -z[9] + z[12] + z[14] + z[16];
z[12] = prod_pow(abb[14], 2) * z[12];
z[14] = 3 * z[0] + z[15];
z[15] = 2 * abb[18];
z[16] = -z[2] + z[15];
z[17] = z[14] + -z[16];
z[18] = abb[2] * z[17];
z[19] = z[5] + -z[16];
z[20] = abb[6] * z[19];
z[13] = z[13] + -z[16];
z[13] = abb[1] * z[13];
z[21] = 2 * z[10];
z[22] = abb[0] * z[21];
z[13] = z[9] + -z[13] + -z[18] + z[20] + z[22];
z[18] = abb[14] * z[13];
z[22] = abb[18] * (T(1) / T(2));
z[23] = (T(-5) / T(2)) * z[0] + -z[4] + z[22];
z[23] = -abb[21] + (T(1) / T(2)) * z[23];
z[23] = abb[0] * z[23];
z[17] = abb[1] * z[17];
z[17] = -z[8] + z[17] + z[23];
z[23] = -abb[19] + (T(-1) / T(2)) * z[0] + z[22];
z[24] = abb[3] * z[23];
z[25] = abb[23] + abb[24];
z[26] = abb[7] * z[25];
z[27] = (T(3) / T(2)) * z[24] + (T(3) / T(4)) * z[26];
z[5] = -abb[21] + -z[5] + z[15];
z[5] = abb[2] * z[5];
z[15] = abb[8] * z[25];
z[28] = (T(1) / T(2)) * z[15];
z[5] = z[5] + -z[17] + -z[27] + -z[28];
z[5] = abb[11] * z[5];
z[14] = z[6] + (T(1) / T(2)) * z[14];
z[14] = abb[1] * z[14];
z[23] = (T(3) / T(2)) * z[23];
z[29] = -abb[21] + z[23];
z[29] = abb[0] * z[29];
z[30] = abb[18] + -abb[19];
z[31] = abb[2] * z[30];
z[27] = z[14] + z[27] + z[29] + (T(-3) / T(2)) * z[31];
z[27] = abb[12] * z[27];
z[5] = z[5] + -z[18] + z[27];
z[5] = abb[12] * z[5];
z[4] = 2 * z[0] + z[4];
z[27] = z[4] + z[6];
z[27] = abb[2] * z[27];
z[17] = z[9] + -z[17] + (T(1) / T(2)) * z[24] + (T(1) / T(4)) * z[26] + -z[27] + z[28];
z[24] = -abb[11] + abb[12];
z[17] = z[17] * z[24];
z[8] = -z[8] + z[14];
z[14] = abb[21] + abb[19] * (T(3) / T(2)) + z[0] + -z[22];
z[14] = abb[2] * z[14];
z[22] = abb[0] * z[1];
z[14] = z[8] + z[14] + (T(-1) / T(2)) * z[22] + z[28];
z[14] = abb[13] * z[14];
z[14] = z[14] + z[17] + z[18];
z[14] = abb[13] * z[14];
z[17] = abb[2] * z[7];
z[8] = z[3] + z[8] + z[17] + -z[22];
z[8] = abb[11] * z[8];
z[8] = z[8] + z[18];
z[8] = abb[11] * z[8];
z[17] = abb[2] * z[19];
z[4] = -abb[18] + z[4];
z[18] = 4 * abb[21] + z[4];
z[18] = abb[4] * z[18];
z[4] = z[2] + -z[4];
z[4] = abb[0] * z[4];
z[19] = abb[1] * z[10];
z[4] = z[4] + z[17] + z[18] + 3 * z[19] + -z[20];
z[4] = abb[15] * z[4];
z[13] = -abb[16] * z[13];
z[17] = abb[2] + -abb[10] + abb[0] * (T(1) / T(4)) + abb[3] * (T(3) / T(4));
z[17] = z[17] * z[25];
z[18] = abb[7] * z[23];
z[7] = abb[9] * z[7];
z[1] = abb[21] + (T(1) / T(2)) * z[1];
z[1] = abb[8] * z[1];
z[1] = z[1] + z[7] + z[17] + z[18];
z[1] = abb[17] * z[1];
z[7] = -abb[0] * abb[21];
z[6] = abb[19] + z[6];
z[6] = abb[1] * z[6];
z[6] = z[6] + z[7];
z[6] = prod_pow(m1_set::bc<T>[0], 2) * z[6];
z[1] = z[1] + z[4] + z[5] + (T(13) / T(6)) * z[6] + z[8] + z[12] + z[13] + z[14];
z[4] = abb[3] * z[11];
z[4] = z[4] + -z[26];
z[5] = abb[2] * z[11];
z[6] = abb[1] * z[21];
z[7] = 2 * z[30];
z[7] = abb[0] * z[7];
z[7] = -2 * z[3] + -z[4] + z[5] + z[6] + z[7] + z[15];
z[7] = abb[11] * z[7];
z[8] = -abb[2] + abb[5];
z[8] = z[8] * z[10];
z[2] = -z[2] + -z[30];
z[2] = abb[0] * z[2];
z[0] = abb[19] + -z[0] + -z[16];
z[0] = abb[1] * z[0];
z[0] = z[0] + z[2] + z[3] + z[8];
z[0] = abb[14] * z[0];
z[2] = abb[0] * z[11];
z[2] = z[2] + -z[6];
z[3] = -z[2] + z[5] + -z[9] + -z[15];
z[3] = abb[13] * z[3];
z[2] = z[2] + z[4] + 2 * z[31];
z[2] = abb[12] * z[2];
z[0] = 2 * z[0] + z[2] + z[3] + z[7];
z[0] = m1_set::bc<T>[0] * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_246_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-19.44946153654748994118030943425611011408075493540683886558710197"),stof<T>("24.55442437106079720604615670428338045126587840729103378471051288")}, std::complex<T>{stof<T>("20.582281667592617342139847476311587677392565197939729730718195012"),stof<T>("-22.631178181998683980701351460855510506419281277223653210945630426")}, std::complex<T>{stof<T>("1.1328201310451274009595380420554775633118102625328908651310930424"),stof<T>("1.9232461890621132253448052434278699448465971300673805737648824541")}, std::complex<T>{stof<T>("26.020008535337350086719711905342405389990540756374283712513714011"),stof<T>("25.638171302187677012676173826844938907631284341544786708623200238")}, std::complex<T>{stof<T>("-1.1328201310451274009595380420554775633118102625328908651310930424"),stof<T>("-1.9232461890621132253448052434278699448465971300673805737648824541")}, std::complex<T>{stof<T>("-0.94645826884586151334932252788511187146196813235744440321885439"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("-0.94645826884586151334932252788511187146196813235744440321885439"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[123].real()/kbase.W[123].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_246_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_246_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("38.604936629238762210973694123844533674177461590638020540871979903"),stof<T>("-36.325765545198436456930733966422119138576637811736376511444368914")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,25> abb = {dl[0], dlog_W3(k,dl), dl[2], dl[4], dlog_W13(k,dl), dlog_W18(k,dl), dlog_W30(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real())};

                    
            return f_4_246_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_246_DLogXconstant_part(base_point<T>, kend);
	value += f_4_246_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_246_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_246_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_246_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_246_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_246_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_246_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
