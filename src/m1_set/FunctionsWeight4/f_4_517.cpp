/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_517.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_517_abbreviated (const std::array<T,29>& abb) {
T z[30];
z[0] = abb[19] * (T(1) / T(2));
z[1] = 3 * abb[20];
z[2] = abb[21] + -abb[23];
z[3] = z[0] + -z[1] + (T(-5) / T(2)) * z[2];
z[3] = -abb[22] + (T(1) / T(2)) * z[3];
z[3] = abb[0] * z[3];
z[4] = 2 * abb[19];
z[5] = 2 * abb[22];
z[6] = z[4] + -z[5];
z[7] = 5 * abb[20] + 3 * z[2];
z[8] = -z[6] + z[7];
z[9] = abb[1] * z[8];
z[10] = z[1] + z[2];
z[11] = -abb[19] + abb[22];
z[12] = (T(1) / T(2)) * z[10] + z[11];
z[13] = abb[6] * z[12];
z[3] = z[3] + z[9] + -z[13];
z[4] = abb[22] + -z[4] + z[10];
z[4] = abb[3] * z[4];
z[0] = -abb[20] + z[0] + (T(-1) / T(2)) * z[2];
z[14] = abb[2] * z[0];
z[4] = z[3] + z[4] + (T(3) / T(2)) * z[14];
z[4] = abb[14] * z[4];
z[1] = z[1] + 2 * z[2];
z[15] = z[1] + z[11];
z[15] = abb[3] * z[15];
z[3] = z[3] + (T(-1) / T(2)) * z[14] + z[15];
z[15] = abb[13] * z[3];
z[4] = z[4] + -z[15];
z[7] = (T(1) / T(2)) * z[7] + z[11];
z[7] = abb[1] * z[7];
z[15] = abb[19] + z[2];
z[16] = z[5] + z[15];
z[16] = abb[4] * z[16];
z[17] = abb[3] * z[12];
z[18] = -abb[0] * z[15];
z[13] = z[7] + -z[13] + z[16] + z[17] + z[18];
z[13] = abb[11] * z[13];
z[13] = -z[4] + z[13];
z[13] = abb[11] * z[13];
z[10] = -z[6] + z[10];
z[17] = abb[6] * z[10];
z[18] = 7 * abb[20] + 5 * z[2] + -z[6];
z[18] = abb[1] * z[18];
z[8] = abb[3] * z[8];
z[19] = 2 * abb[0];
z[20] = abb[20] + z[2];
z[21] = z[19] * z[20];
z[8] = z[8] + -z[17] + z[18] + -z[21];
z[18] = -abb[15] * z[8];
z[21] = -abb[11] + abb[14];
z[21] = z[8] * z[21];
z[6] = -abb[20] + z[2] + z[6];
z[6] = abb[1] * z[6];
z[22] = abb[19] + -abb[20];
z[23] = z[5] + z[22];
z[23] = abb[0] * z[23];
z[24] = abb[3] * z[20];
z[6] = z[6] + -z[16] + z[23] + z[24];
z[6] = abb[12] * z[6];
z[6] = z[6] + z[21];
z[6] = abb[12] * z[6];
z[21] = abb[7] * abb[13];
z[23] = abb[7] + abb[8] * (T(1) / T(2));
z[24] = abb[14] * z[23];
z[21] = z[21] + z[24];
z[24] = abb[13] * (T(1) / T(2));
z[21] = z[21] * z[24];
z[24] = abb[8] * (T(3) / T(2));
z[25] = abb[7] + z[24];
z[25] = abb[14] * z[25];
z[23] = abb[13] * z[23];
z[23] = z[23] + z[25];
z[23] = (T(1) / T(2)) * z[23];
z[25] = abb[11] * z[23];
z[26] = prod_pow(abb[14], 2);
z[27] = abb[8] * z[26];
z[28] = abb[3] + -abb[10] + abb[0] * (T(1) / T(4)) + abb[2] * (T(3) / T(4));
z[29] = abb[27] * z[28];
z[21] = z[21] + -z[25] + (T(3) / T(4)) * z[27] + z[29];
z[25] = abb[24] + abb[25];
z[21] = z[21] * z[25];
z[10] = abb[3] * z[10];
z[10] = z[10] + -z[17];
z[1] = -abb[19] + z[1];
z[17] = 4 * abb[22] + z[1];
z[17] = abb[4] * z[17];
z[1] = z[1] + -z[5];
z[1] = abb[0] * z[1];
z[5] = abb[1] * z[20];
z[1] = z[1] + -3 * z[5] + -z[10] + -z[17];
z[17] = abb[26] * z[1];
z[20] = -abb[3] * z[22];
z[14] = z[14] + z[20];
z[20] = -abb[22] + (T(3) / T(2)) * z[0];
z[20] = abb[0] * z[20];
z[7] = z[7] + (T(3) / T(2)) * z[14] + z[20];
z[7] = z[7] * z[26];
z[3] = -abb[14] * z[3];
z[14] = -abb[0] + abb[3];
z[14] = z[0] * z[14];
z[5] = -z[5] + z[14];
z[5] = abb[13] * z[5];
z[3] = z[3] + z[5];
z[3] = abb[13] * z[3];
z[5] = abb[20] + (T(1) / T(3)) * z[2] + (T(2) / T(3)) * z[11];
z[14] = -abb[3] + abb[6];
z[5] = z[5] * z[14];
z[11] = abb[20] * (T(-11) / T(6)) + -z[2] + (T(-5) / T(6)) * z[11];
z[11] = abb[1] * z[11];
z[2] = abb[19] * (T(-7) / T(6)) + abb[20] * (T(1) / T(2)) + (T(-2) / T(3)) * z[2];
z[14] = abb[22] * (T(-4) / T(3)) + z[2];
z[14] = abb[4] * z[14];
z[2] = abb[22] * (T(1) / T(6)) + -z[2];
z[2] = abb[0] * z[2];
z[2] = z[2] + z[5] + z[11] + z[14];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[0] = z[0] * z[24];
z[5] = abb[9] * z[12];
z[11] = abb[22] + (T(1) / T(2)) * z[15];
z[11] = abb[7] * z[11];
z[0] = z[0] + z[5] + z[11];
z[5] = abb[27] * z[0];
z[2] = abb[28] + z[2] + z[3] + z[5] + z[6] + z[7] + z[13] + z[17] + z[18] + z[21];
z[3] = z[15] * z[19];
z[3] = z[3] + -z[9] + -z[10] + -2 * z[16];
z[3] = abb[11] * z[3];
z[5] = abb[12] * z[8];
z[3] = z[3] + z[4] + z[5];
z[3] = m1_set::bc<T>[0] * z[3];
z[1] = abb[16] * z[1];
z[0] = abb[17] * z[0];
z[4] = m1_set::bc<T>[0] * z[23];
z[5] = abb[17] * z[28];
z[4] = z[4] + z[5];
z[4] = z[4] * z[25];
z[0] = abb[18] + z[0] + z[1] + z[3] + z[4];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_517_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-12.558186494602971382200965946342030939130564300585294968161611309"),stof<T>("-8.26351341246424653290877863834076519158569300260248379531480413")}, std::complex<T>{stof<T>("9.1233540961639644338354954050150142031825060194898608275712759429"),stof<T>("-0.4047561765982272097096049329857532079006470536313039722151089313")}, std::complex<T>{stof<T>("-3.4348323984390069483654705413270167359480582810954341405903353663"),stof<T>("-8.6682695890624737426183835713265183994863400562337877675299130609")}, std::complex<T>{stof<T>("-11.1350869877497077702822775913128952547002977612535560471986031353"),stof<T>("0.8043098821654289468078014932187410919523605298667199406899901851")}, std::complex<T>{stof<T>("3.4348323984390069483654705413270167359480582810954341405903353663"),stof<T>("8.6682695890624737426183835713265183994863400562337877675299130609")}, std::complex<T>{stof<T>("-0.87967591672810295777218924617445642026936795132549591364655243886"),stof<T>("0.64194487762304279435700210128213717713839885131132720351431713993")}, std::complex<T>{stof<T>("-0.87967591672810295777218924617445642026936795132549591364655243886"),stof<T>("0.64194487762304279435700210128213717713839885131132720351431713993")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[127].real()/kbase.W[127].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_517_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_517_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (v[2] + v[3]) * (-16 + -8 * v[0] + -4 * v[1] + 3 * v[2] + 7 * v[3] + 4 * v[4] + -4 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -8 * v[5])) / prod_pow(tend, 2);
c[1] = ((-1 + 2 * m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;


		return (abb[20] + abb[21] + -abb[23]) * (t * c[0] + c[1]);
	}
	{
T z[5];
z[0] = 2 * abb[5];
z[1] = abb[11] * z[0];
z[2] = abb[5] * abb[13];
z[3] = z[1] + -z[2];
z[3] = abb[13] * z[3];
z[4] = abb[5] * abb[12];
z[1] = -z[1] + z[4];
z[1] = abb[12] * z[1];
z[2] = -z[2] + z[4];
z[2] = abb[14] * z[2];
z[0] = -abb[15] * z[0];
z[0] = z[0] + z[1] + 2 * z[2] + z[3];
z[1] = -abb[20] + -abb[21] + abb[23];
return z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_517_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[20] + abb[21] + -abb[23]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[12] + abb[13];
z[1] = abb[20] + abb[21] + -abb[23];
return 2 * abb[5] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_517_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-6.859495604298487627245159647940849272023269126093753903584351445"),stof<T>("27.11799495240823726506638436624471203439663450196727558749723845")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dl[5], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W15(k,dl), dlog_W19(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_13(k), f_2_4_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[127].real()/k.W[127].real()), f_2_4_re(k), f_2_25_re(k), T{0}};
abb[18] = SpDLog_f_4_517_W_19_Im(t, path, abb);
abb[28] = SpDLog_f_4_517_W_19_Re(t, path, abb);

                    
            return f_4_517_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_517_DLogXconstant_part(base_point<T>, kend);
	value += f_4_517_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_517_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_517_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_517_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_517_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_517_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_517_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
