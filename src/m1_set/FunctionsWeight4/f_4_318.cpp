/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_318.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_318_abbreviated (const std::array<T,60>& abb) {
T z[126];
z[0] = abb[47] * (T(1) / T(2));
z[1] = -abb[45] + abb[46];
z[2] = z[0] + (T(1) / T(2)) * z[1];
z[3] = abb[50] + abb[51];
z[4] = abb[48] * (T(1) / T(2));
z[5] = 2 * abb[49];
z[6] = -abb[54] + z[5];
z[7] = z[2] + -z[3] + -z[4] + -z[6];
z[7] = abb[8] * z[7];
z[8] = 3 * abb[50];
z[9] = z[4] + z[8];
z[10] = 3 * abb[51];
z[2] = z[2] + -z[10];
z[11] = 3 * abb[54] + z[2] + -z[9];
z[11] = abb[3] * z[11];
z[12] = abb[50] + -abb[54];
z[13] = abb[43] + z[12];
z[14] = abb[9] * z[13];
z[15] = abb[51] + -abb[54];
z[16] = abb[44] + z[15];
z[17] = abb[10] * z[16];
z[18] = z[14] + z[17];
z[19] = abb[23] + abb[25];
z[20] = abb[26] + abb[27];
z[21] = z[19] + z[20];
z[22] = abb[22] + abb[24];
z[23] = z[21] + z[22];
z[24] = abb[56] * (T(1) / T(2));
z[25] = abb[55] * (T(1) / T(2)) + z[24];
z[23] = z[23] * z[25];
z[26] = -abb[48] + abb[53];
z[27] = -abb[44] + z[26];
z[28] = -z[12] + z[27];
z[29] = abb[2] * z[28];
z[4] = z[0] + z[4];
z[30] = -abb[52] + abb[45] * (T(1) / T(2));
z[31] = z[4] + z[30];
z[32] = abb[46] * (T(1) / T(2));
z[33] = z[31] + -z[32];
z[34] = abb[15] * z[33];
z[35] = -abb[45] + abb[47];
z[36] = -abb[46] + z[35];
z[37] = -abb[48] + z[36];
z[38] = (T(1) / T(2)) * z[37];
z[39] = abb[53] + z[38];
z[40] = abb[17] * z[39];
z[41] = z[34] + -z[40];
z[42] = abb[45] + -abb[52];
z[43] = abb[43] + z[42];
z[44] = z[15] + z[43];
z[45] = abb[1] * z[44];
z[46] = abb[43] + abb[44] + -abb[54];
z[46] = abb[0] * z[46];
z[47] = 2 * z[46];
z[27] = abb[43] + z[27];
z[48] = -abb[7] * z[27];
z[43] = -abb[44] + z[43];
z[49] = abb[4] * z[43];
z[7] = z[7] + -z[11] + -3 * z[18] + z[23] + -z[29] + -z[41] + z[45] + z[47] + z[48] + z[49];
z[7] = abb[29] * z[7];
z[23] = z[31] + z[32];
z[31] = -abb[53] + z[23];
z[48] = 2 * abb[44];
z[49] = 2 * abb[54];
z[50] = z[48] + -z[49];
z[51] = 2 * abb[43];
z[52] = z[31] + z[50] + z[51];
z[53] = abb[4] + -abb[7];
z[52] = z[52] * z[53];
z[54] = 2 * abb[51];
z[55] = -z[35] + z[54];
z[56] = 2 * abb[50];
z[57] = -abb[46] + abb[48];
z[58] = z[56] + z[57];
z[59] = z[55] + -z[58];
z[60] = abb[16] * z[59];
z[61] = 2 * z[17];
z[62] = -z[60] + z[61];
z[63] = -z[14] + z[45];
z[64] = z[20] + -z[22];
z[25] = z[25] * z[64];
z[64] = 2 * abb[2];
z[65] = z[28] * z[64];
z[25] = z[25] + z[34] + z[40] + -z[52] + -z[62] + -2 * z[63] + -z[65];
z[34] = abb[32] + -abb[33];
z[25] = z[25] * z[34];
z[34] = z[35] + z[50] + z[58];
z[40] = abb[4] * z[34];
z[50] = -z[5] + z[8];
z[52] = -abb[44] + z[50];
z[63] = abb[54] + z[26];
z[66] = z[52] + -z[63];
z[66] = z[64] * z[66];
z[67] = 2 * abb[53];
z[37] = z[37] + z[67];
z[68] = abb[17] * z[37];
z[66] = z[66] + z[68];
z[69] = z[49] + -z[55];
z[70] = 4 * abb[49];
z[71] = -z[58] + z[69] + -z[70];
z[72] = abb[8] * z[71];
z[73] = z[6] + z[58];
z[74] = abb[11] * z[73];
z[75] = 2 * z[74];
z[76] = z[72] + z[75];
z[77] = 2 * abb[7];
z[28] = z[28] * z[77];
z[78] = abb[55] + abb[56];
z[21] = z[21] * z[78];
z[21] = -z[21] + z[28] + z[40] + z[62] + -z[66] + -z[76];
z[28] = abb[34] * z[21];
z[40] = z[51] + -z[57] + -z[69];
z[79] = abb[7] * z[40];
z[80] = -z[5] + z[10];
z[81] = -abb[43] + z[80];
z[82] = -abb[54] + z[42];
z[83] = z[81] + z[82];
z[83] = abb[1] * z[83];
z[83] = -z[14] + z[83];
z[84] = 2 * abb[52];
z[85] = -abb[45] + z[84];
z[86] = -abb[47] + z[85];
z[87] = -z[57] + z[86];
z[88] = abb[15] * z[87];
z[83] = 2 * z[83] + z[88];
z[6] = z[6] + z[55];
z[55] = abb[12] * z[6];
z[89] = 2 * z[55];
z[60] = -z[60] + z[89];
z[90] = 2 * abb[4];
z[44] = z[44] * z[90];
z[44] = z[44] + z[60] + z[72] + -z[79] + z[83];
z[44] = abb[30] * z[44];
z[79] = z[19] + z[22];
z[79] = abb[30] * z[79];
z[91] = abb[56] * z[79];
z[44] = z[44] + z[91];
z[53] = z[53] * z[59];
z[59] = 6 * abb[51];
z[91] = -z[35] + z[59];
z[92] = 6 * abb[50];
z[93] = z[57] + z[92];
z[94] = 6 * abb[54] + -z[91] + -z[93];
z[94] = abb[3] * z[94];
z[89] = z[89] + z[94];
z[95] = abb[49] + -abb[50];
z[96] = -z[15] + z[95];
z[96] = abb[16] * z[96];
z[97] = 4 * z[96];
z[53] = z[53] + z[76] + z[89] + -z[97];
z[76] = abb[56] * z[19];
z[76] = z[53] + z[76];
z[76] = abb[31] * z[76];
z[98] = abb[31] * z[19];
z[98] = -z[79] + z[98];
z[98] = abb[55] * z[98];
z[7] = z[7] + z[25] + z[28] + -z[44] + z[76] + z[98];
z[7] = abb[29] * z[7];
z[25] = z[60] + -z[75];
z[28] = 5 * abb[48];
z[76] = abb[46] + z[28];
z[98] = -z[70] + z[76] + -z[91];
z[99] = abb[6] * z[98];
z[100] = -abb[50] + abb[51];
z[101] = 3 * abb[49];
z[102] = -z[35] + z[100] + z[101];
z[103] = 4 * abb[8];
z[102] = z[102] * z[103];
z[104] = 3 * abb[48];
z[12] = z[12] + -z[54] + z[104];
z[105] = -z[12] * z[77];
z[106] = -abb[4] * z[71];
z[107] = abb[24] * abb[56];
z[102] = z[25] + z[99] + z[102] + z[105] + z[106] + z[107];
z[102] = abb[31] * z[102];
z[105] = abb[22] + abb[27];
z[106] = abb[26] * (T(1) / T(2));
z[108] = abb[24] * (T(1) / T(2));
z[105] = z[19] + (T(1) / T(2)) * z[105] + z[106] + z[108];
z[105] = z[78] * z[105];
z[41] = -z[41] + z[105];
z[105] = abb[48] * (T(3) / T(2));
z[109] = -abb[53] + z[105];
z[110] = -z[0] + z[109];
z[111] = z[48] + -z[51];
z[30] = z[30] + z[32] + -z[110] + -z[111];
z[30] = abb[7] * z[30];
z[112] = abb[45] * (T(3) / T(2));
z[32] = abb[52] + z[32];
z[113] = -z[32] + z[112];
z[111] = -abb[53] + z[4] + z[111] + -z[113];
z[111] = abb[4] * z[111];
z[30] = z[30] + -z[41] + -2 * z[45] + z[47] + z[65] + -z[72] + z[111];
z[30] = abb[32] * z[30];
z[12] = abb[8] * z[12];
z[47] = abb[48] * (T(9) / T(2));
z[42] = abb[44] + abb[43] * (T(7) / T(2)) + -z[42] + -z[47] + -z[101];
z[42] = abb[7] * z[42];
z[65] = -z[54] + z[101];
z[72] = z[65] + z[82];
z[111] = 4 * abb[43];
z[114] = z[72] + z[111];
z[114] = abb[1] * z[114];
z[115] = abb[43] + -abb[48];
z[116] = abb[13] * z[115];
z[27] = abb[4] * z[27];
z[27] = -z[12] + z[27] + -z[29] + z[42] + -z[46] + z[114] + (T(-9) / T(2)) * z[116];
z[27] = abb[33] * z[27];
z[29] = -abb[49] + -z[51] + -z[82];
z[29] = abb[1] * z[29];
z[12] = z[12] + z[14] + z[29];
z[29] = 3 * abb[43];
z[42] = z[29] + -z[104];
z[114] = -abb[47] + abb[49] + abb[52];
z[116] = 4 * z[114];
z[117] = z[42] + -z[116];
z[117] = abb[13] * z[117];
z[118] = -z[99] + z[117];
z[40] = -abb[4] * z[40];
z[72] = -abb[43] + 9 * abb[48] + 2 * z[72];
z[72] = abb[7] * z[72];
z[12] = 2 * z[12] + z[40] + -z[60] + z[72] + -z[107] + z[118];
z[12] = abb[30] * z[12];
z[40] = -abb[30] + abb[31];
z[40] = abb[24] * abb[55] * z[40];
z[12] = z[12] + z[27] + z[30] + z[40] + z[102];
z[12] = abb[33] * z[12];
z[27] = 3 * abb[45];
z[15] = z[15] + z[27] + -z[56];
z[30] = abb[8] * z[15];
z[40] = 5 * abb[45];
z[60] = abb[47] + z[40];
z[72] = z[60] + -z[70] + -z[93];
z[102] = abb[5] * z[72];
z[34] = -abb[7] * z[34];
z[107] = z[49] + z[67];
z[119] = 2 * abb[48];
z[120] = 6 * abb[49];
z[121] = z[119] + z[120];
z[122] = 4 * abb[50];
z[123] = -abb[44] + 9 * abb[45] + -z[107] + z[121] + -z[122];
z[123] = abb[4] * z[123];
z[124] = -abb[49] + -z[48] + z[63];
z[124] = z[64] * z[124];
z[125] = -abb[26] * z[78];
z[34] = 2 * z[30] + z[34] + z[62] + -z[75] + -z[102] + z[123] + z[124] + z[125];
z[34] = abb[32] * z[34];
z[40] = abb[46] + z[40];
z[9] = -z[0] + z[5] + z[9] + (T(-1) / T(2)) * z[40];
z[9] = abb[5] * z[9];
z[9] = z[9] + -3 * z[74];
z[40] = abb[45] + abb[46];
z[4] = -z[4] + (T(1) / T(2)) * z[40];
z[40] = -abb[51] + z[4] + z[49] + -z[50];
z[40] = abb[16] * z[40];
z[50] = 2 * abb[46] + abb[54];
z[62] = abb[51] + -z[27] + z[50] + -z[121];
z[62] = abb[8] * z[62];
z[52] = -abb[54] + -z[4] + z[52];
z[52] = abb[7] * z[52];
z[50] = -abb[48] + -abb[53] + abb[44] * (T(-1) / T(2)) + z[50] + -z[101] + -z[112];
z[50] = abb[4] * z[50];
z[121] = 7 * abb[49];
z[123] = -8 * abb[44] + z[63] + z[92] + -z[121];
z[123] = abb[2] * z[123];
z[106] = z[78] * z[106];
z[40] = -z[9] + z[17] + z[40] + z[50] + z[52] + z[62] + z[106] + z[123];
z[40] = abb[34] * z[40];
z[21] = -abb[33] * z[21];
z[21] = z[21] + z[34] + z[40];
z[21] = abb[34] * z[21];
z[2] = abb[48] * (T(5) / T(2)) + z[2] + -z[5];
z[2] = abb[6] * z[2];
z[2] = z[2] + 3 * z[55];
z[5] = -8 * abb[43] + z[59] + -z[82] + -z[121];
z[5] = abb[1] * z[5];
z[34] = -abb[50] + -z[4] + z[49] + -z[80];
z[34] = abb[16] * z[34];
z[40] = 2 * abb[45];
z[50] = 2 * abb[47];
z[52] = z[40] + -z[50];
z[62] = abb[50] + abb[54] + -z[52] + -z[104] + -z[120];
z[62] = abb[8] * z[62];
z[50] = -abb[45] + -abb[52] + abb[54] + abb[43] * (T(-1) / T(2)) + z[50] + -z[101] + -z[105];
z[50] = abb[7] * z[50];
z[4] = -abb[54] + z[4] + z[81];
z[4] = abb[4] * z[4];
z[80] = abb[43] * (T(3) / T(2)) + -z[105] + z[116];
z[80] = abb[13] * z[80];
z[4] = z[2] + z[4] + z[5] + z[14] + z[34] + z[50] + z[62] + z[80];
z[5] = prod_pow(abb[30], 2);
z[4] = z[4] * z[5];
z[34] = abb[47] + -2 * z[1] + -z[10] + z[70] + z[119];
z[34] = abb[4] * z[34];
z[50] = abb[46] + z[27];
z[3] = -abb[54] + z[0] + -z[3] + (T(1) / T(2)) * z[50];
z[50] = 8 * abb[49];
z[3] = 3 * z[3] + z[47] + z[50];
z[3] = abb[8] * z[3];
z[8] = -z[8] + z[119];
z[52] = abb[46] + z[8] + z[52] + z[70];
z[52] = abb[7] * z[52];
z[62] = abb[26] + z[19];
z[80] = abb[24] + z[62];
z[24] = -z[24] * z[80];
z[2] = -z[2] + z[3] + z[9] + -z[11] + z[24] + z[34] + z[52];
z[3] = prod_pow(abb[31], 2);
z[2] = z[2] * z[3];
z[9] = z[57] + -z[100] + z[101];
z[9] = z[9] * z[103];
z[11] = -abb[7] * z[71];
z[15] = -z[15] * z[90];
z[24] = abb[26] * abb[56];
z[9] = z[9] + z[11] + z[15] + z[24] + -z[25] + z[102];
z[9] = abb[31] * z[9];
z[11] = 4 * abb[44];
z[15] = z[11] + -z[56] + -z[63] + z[101];
z[15] = abb[2] * z[15];
z[24] = -abb[49] + -z[112];
z[24] = abb[43] + abb[44] * (T(7) / T(2)) + 3 * z[24] + z[26];
z[24] = abb[4] * z[24];
z[25] = -abb[7] * z[43];
z[15] = z[15] + z[24] + z[25] + -z[30] + z[45] + -z[46];
z[15] = abb[32] * z[15];
z[24] = abb[26] * abb[31];
z[24] = z[24] + z[79];
z[24] = abb[55] * z[24];
z[9] = z[9] + z[15] + z[24] + z[44];
z[9] = abb[32] * z[9];
z[15] = abb[45] + abb[48];
z[24] = 7 * abb[50] + abb[54] * (T(13) / T(2));
z[25] = 8 * abb[46];
z[30] = 7 * abb[51];
z[15] = -8 * abb[47] + abb[49] * (T(-13) / T(2)) + (T(-11) / T(2)) * z[15] + z[24] + -z[25] + z[30];
z[15] = abb[8] * z[15];
z[15] = z[15] + z[68] + z[88];
z[34] = 2 * z[19] + z[20] + z[22];
z[43] = (T(1) / T(3)) * z[78];
z[34] = z[34] * z[43];
z[43] = abb[49] * (T(9) / T(2));
z[24] = abb[44] * (T(20) / T(3)) + (T(-1) / T(3)) * z[24] + (T(-13) / T(6)) * z[26] + z[43];
z[24] = abb[2] * z[24];
z[26] = -z[30] + (T(13) / T(2)) * z[82];
z[26] = abb[43] * (T(20) / T(3)) + (T(1) / T(3)) * z[26] + z[43];
z[26] = abb[1] * z[26];
z[30] = (T(-5) / T(3)) * z[114] + -z[115];
z[30] = abb[13] * z[30];
z[43] = abb[46] + abb[47];
z[44] = abb[49] * (T(7) / T(6)) + (T(1) / T(3)) * z[43];
z[45] = 3 * abb[52] + abb[45] * (T(-7) / T(3));
z[45] = -abb[48] + abb[53] * (T(-2) / T(3)) + abb[43] * (T(-1) / T(6)) + abb[44] * (T(13) / T(6)) + -z[44] + (T(1) / T(2)) * z[45];
z[45] = abb[7] * z[45];
z[44] = -abb[45] + abb[48] * (T(-7) / T(6)) + abb[52] * (T(-2) / T(3)) + abb[44] * (T(-1) / T(6)) + abb[53] * (T(3) / T(2)) + abb[43] * (T(13) / T(6)) + -z[44];
z[44] = abb[4] * z[44];
z[15] = (T(1) / T(3)) * z[15] + z[24] + z[26] + 2 * z[30] + z[34] + z[44] + z[45] + (T(-13) / T(6)) * z[46];
z[24] = prod_pow(m1_set::bc<T>[0], 2);
z[15] = z[15] * z[24];
z[26] = 4 * abb[54] + z[70];
z[30] = -z[26] + z[35] + z[54] + z[93];
z[30] = abb[16] * z[30];
z[34] = 3 * abb[47];
z[35] = -z[27] + z[34] + -z[50];
z[44] = z[49] + z[54];
z[45] = z[35] + -z[44] + z[93];
z[45] = abb[8] * z[45];
z[52] = 4 * z[55] + z[99];
z[28] = z[28] + -z[36] + -z[44];
z[28] = abb[7] * z[28];
z[28] = z[28] + -z[30] + z[45] + -z[52] + -z[94];
z[28] = abb[37] * z[28];
z[26] = -z[26] + z[56] + -z[57] + z[91];
z[26] = abb[16] * z[26];
z[26] = z[26] + z[94];
z[36] = 3 * abb[46];
z[44] = z[36] + -z[50] + -z[104];
z[45] = z[44] + -z[49] + -z[56] + z[91];
z[45] = abb[8] * z[45];
z[55] = 4 * z[74] + z[102];
z[60] = -z[56] + z[60];
z[57] = -z[49] + z[57] + z[60];
z[57] = abb[4] * z[57];
z[62] = -z[62] * z[78];
z[63] = -z[73] * z[77];
z[45] = -z[26] + z[45] + -z[55] + z[57] + z[62] + z[63];
z[45] = abb[38] * z[45];
z[57] = -abb[48] + z[67];
z[48] = abb[46] + z[48] + -z[57] + -z[69];
z[48] = abb[7] * z[48];
z[30] = -z[30] + z[48] + -z[61] + z[66] + -z[89];
z[30] = abb[35] * z[30];
z[48] = -z[49] + z[51] + z[58] + -z[86];
z[48] = abb[4] * z[48];
z[13] = -z[13] * z[77];
z[13] = z[13] + -z[26] + z[48] + -z[75] + z[83];
z[13] = abb[36] * z[13];
z[26] = z[29] + -z[120];
z[29] = -abb[46] + abb[47];
z[48] = -10 * abb[48] + z[26] + -z[29] + z[59] + z[85];
z[48] = abb[7] * z[48];
z[49] = abb[8] * z[98];
z[51] = abb[4] * z[87];
z[58] = abb[43] + abb[49] + -abb[51];
z[58] = abb[1] * z[58];
z[48] = z[48] + -z[49] + z[51] + 6 * z[58] + -z[88] + -z[118];
z[49] = abb[22] + z[19];
z[51] = abb[56] * z[49];
z[59] = -z[48] + z[51];
z[59] = abb[57] * z[59];
z[5] = z[5] * z[108];
z[61] = abb[24] + z[19];
z[61] = abb[37] * z[61];
z[22] = abb[36] * z[22];
z[20] = abb[35] * z[20];
z[62] = abb[28] * (T(1) / T(2));
z[63] = abb[59] * z[62];
z[5] = z[5] + z[20] + z[22] + -z[61] + z[63];
z[20] = abb[56] * z[5];
z[3] = z[3] * z[80];
z[22] = abb[57] * z[49];
z[3] = (T(-1) / T(2)) * z[3] + z[5] + z[22];
z[3] = abb[55] * z[3];
z[5] = 3 * abb[44];
z[22] = z[5] + -z[120];
z[29] = 10 * abb[45] + -z[22] + -z[29] + -z[57] + -z[92];
z[29] = abb[4] * z[29];
z[57] = abb[8] * z[72];
z[61] = abb[7] * z[37];
z[63] = abb[44] + z[95];
z[66] = abb[2] * z[63];
z[29] = z[29] + z[57] + -z[61] + -6 * z[66] + -z[102];
z[57] = abb[27] + z[19];
z[61] = z[57] * z[78];
z[61] = z[61] + z[68];
z[69] = z[29] + z[61];
z[69] = abb[58] * z[69];
z[19] = z[19] * z[78];
z[19] = z[19] + z[53];
z[19] = abb[39] * z[19];
z[53] = -abb[46] + abb[53];
z[71] = -4 * z[53] + -z[70];
z[5] = z[5] + -z[27];
z[27] = z[5] + z[71];
z[72] = abb[58] * z[27];
z[73] = abb[32] * z[27];
z[71] = abb[44] * (T(3) / T(2)) + -z[71] + -z[112];
z[71] = abb[34] * z[71];
z[71] = z[71] + z[73];
z[71] = abb[34] * z[71];
z[53] = abb[49] + z[53];
z[74] = abb[44] + -abb[45];
z[53] = (T(-5) / T(3)) * z[53] + -z[74];
z[24] = z[24] * z[53];
z[53] = prod_pow(abb[32], 2) * z[74];
z[24] = 2 * z[24] + (T(-9) / T(2)) * z[53] + z[71] + z[72];
z[24] = abb[14] * z[24];
z[53] = abb[18] + abb[19];
z[31] = -z[31] * z[53];
z[33] = abb[20] * z[33];
z[39] = abb[21] * z[39];
z[31] = z[31] + -z[33] + z[39];
z[31] = abb[59] * z[31];
z[6] = -abb[37] * z[6];
z[16] = -abb[35] * z[16];
z[6] = z[6] + z[16];
z[6] = z[6] * z[90];
z[2] = z[2] + z[3] + z[4] + z[6] + z[7] + z[9] + z[12] + z[13] + z[15] + z[19] + z[20] + z[21] + z[24] + z[28] + z[30] + z[31] + z[45] + z[59] + z[69];
z[1] = -abb[43] + 4 * abb[48] + -z[1] + -z[34] + -z[54] + z[84] + z[120];
z[1] = abb[7] * z[1];
z[3] = z[35] + z[54] + -z[76] + z[122];
z[4] = abb[8] * z[3];
z[6] = 4 * abb[51];
z[7] = -z[6] + z[70] + -z[87] + z[111];
z[7] = abb[4] * z[7];
z[9] = 5 * abb[49];
z[10] = 7 * abb[43] + z[9] + -z[10] + 2 * z[82];
z[10] = abb[1] * z[10];
z[10] = z[10] + -2 * z[14];
z[12] = -z[42] + -8 * z[114];
z[12] = abb[13] * z[12];
z[1] = z[1] + -z[4] + z[7] + 2 * z[10] + z[12] + z[51] + -z[52] + z[88] + -z[97];
z[1] = abb[30] * z[1];
z[7] = z[11] + -z[37] + z[70] + -z[122];
z[7] = abb[7] * z[7];
z[10] = -abb[44] + 4 * abb[45] + -abb[47] + abb[48] + -z[36] + -z[56] + z[67] + z[120];
z[10] = abb[4] * z[10];
z[6] = -z[6] + -z[44] + z[60];
z[11] = abb[8] * z[6];
z[8] = 7 * abb[44] + z[8] + z[9] + -z[107];
z[8] = z[8] * z[64];
z[7] = z[7] + z[8] + z[10] + z[11] + -4 * z[17] + -z[55] + z[61] + -z[97];
z[7] = abb[34] * z[7];
z[8] = abb[54] + -z[40] + -z[43] + z[56] + -z[65] + -z[119];
z[8] = abb[8] * z[8];
z[8] = z[8] + z[96];
z[3] = abb[7] * z[3];
z[6] = -abb[4] * z[6];
z[9] = abb[24] + abb[26];
z[10] = abb[56] * z[9];
z[3] = z[3] + z[6] + 4 * z[8] + z[10] + z[52] + z[55];
z[3] = abb[31] * z[3];
z[0] = -z[0] + z[113];
z[6] = abb[53] + -z[0] + z[26] + -z[47];
z[6] = abb[7] * z[6];
z[0] = z[0] + z[70] + z[109];
z[8] = -abb[4] * z[0];
z[4] = z[4] + z[6] + z[8] + -z[41] + 2 * z[58] + 4 * z[66] + -z[117];
z[4] = abb[33] * z[4];
z[0] = -abb[7] * z[0];
z[6] = abb[45] * (T(-9) / T(2)) + z[22] + z[32] + -z[110];
z[6] = abb[4] * z[6];
z[8] = z[63] * z[64];
z[0] = z[0] + z[6] + z[8] + -z[11] + -z[41] + 4 * z[58];
z[0] = abb[32] * z[0];
z[6] = z[18] + -z[46] + -z[58] + -z[66] + z[96];
z[6] = abb[29] * z[6];
z[8] = abb[30] * z[49];
z[9] = abb[31] * z[9];
z[8] = z[8] + z[9];
z[8] = abb[55] * z[8];
z[0] = z[0] + z[1] + z[3] + z[4] + 4 * z[6] + z[7] + z[8];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = -abb[40] * z[48];
z[3] = z[29] + z[68];
z[3] = abb[41] * z[3];
z[4] = -8 * abb[53] + -z[5] + z[25] + -z[50];
z[4] = abb[34] * z[4];
z[4] = z[4] + -z[73];
z[4] = m1_set::bc<T>[0] * z[4];
z[5] = abb[41] * z[27];
z[4] = z[4] + z[5];
z[4] = abb[14] * z[4];
z[5] = abb[40] * z[49];
z[6] = abb[41] * z[57];
z[7] = abb[42] * z[62];
z[5] = z[5] + z[6] + z[7];
z[5] = z[5] * z[78];
z[6] = abb[42] * z[23];
z[7] = abb[42] * abb[53];
z[6] = z[6] + -z[7];
z[6] = -z[6] * z[53];
z[8] = abb[42] * z[38];
z[7] = z[7] + z[8];
z[7] = abb[21] * z[7];
z[8] = -abb[42] * z[33];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_318_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("44.120529970733413815580724688696692232506243835713588493808950806"),stof<T>("-41.6650865301287942482805691217113402577275975395557653209603274")}, std::complex<T>{stof<T>("44.120529970733413815580724688696692232506243835713588493808950806"),stof<T>("-41.6650865301287942482805691217113402577275975395557653209603274")}, std::complex<T>{stof<T>("-24.32303288716065542586342764468403611225152784801632718931743735"),stof<T>("9.060804849737609669293905961117183543591208684270521862164340131")}, std::complex<T>{stof<T>("-4.6280220044621254406139148791233362814082138387190657127030861147"),stof<T>("-9.6950957140296500444414595673675666150603994208310027800133207896")}, std::complex<T>{stof<T>("-4.6280220044621254406139148791233362814082138387190657127030861147"),stof<T>("-9.6950957140296500444414595673675666150603994208310027800133207896")}, std::complex<T>{stof<T>("-24.32303288716065542586342764468403611225152784801632718931743735"),stof<T>("9.060804849737609669293905961117183543591208684270521862164340131")}, std::complex<T>{stof<T>("-58.85897600108854615955721712447096200480750398909620778431723833"),stof<T>("-24.655741997155050916528065279123290714843843751104640296418593219")}, std::complex<T>{stof<T>("17.271625219662090912607795267992703281726162263884459014102458665"),stof<T>("21.138988593369706307284751557739988174950239183665002331673644356")}, std::complex<T>{stof<T>("17.271625219662090912607795267992703281726162263884459014102458665"),stof<T>("21.138988593369706307284751557739988174950239183665002331673644356")}, std::complex<T>{stof<T>("-26.367866057911321266540949745992967634308572349675502933865415749"),stof<T>("20.785834666835619432162498403340823471551092686062229997136613617")}, std::complex<T>{stof<T>("-26.367866057911321266540949745992967634308572349675502933865415749"),stof<T>("20.785834666835619432162498403340823471551092686062229997136613617")}, std::complex<T>{stof<T>("-6.0732342408614025951702276868890554862640920631871516720254702564"),stof<T>("0.374554134215508883980872766880911682695456406389013909999050086")}, std::complex<T>{stof<T>("1.2575190271596537991920991302195900456713969230739235361053132628"),stof<T>("-1.262423601462443931986568394750185544768544624668046192565869342")}, std::complex<T>{stof<T>("1.2575190271596537991920991302195900456713969230739235361053132628"),stof<T>("-1.262423601462443931986568394750185544768544624668046192565869342")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[23].real()/kbase.W[23].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_318_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_318_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-49.766713082919627510255105322446026234933030980041081693788609915"),stof<T>("8.460136793594272622296150526007385742674952969853526058325434481")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,60> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[23].real()/k.W[23].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_318_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_318_DLogXconstant_part(base_point<T>, kend);
	value += f_4_318_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_318_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_318_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_318_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_318_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_318_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_318_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
