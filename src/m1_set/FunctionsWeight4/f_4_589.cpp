/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_589.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_589_abbreviated (const std::array<T,68>& abb) {
T z[139];
z[0] = 4 * abb[51];
z[1] = 2 * abb[53];
z[2] = z[0] + -z[1];
z[3] = 2 * abb[47];
z[4] = 2 * abb[48];
z[5] = z[3] + z[4];
z[6] = -abb[49] + abb[50];
z[7] = 4 * abb[56];
z[8] = 2 * abb[58];
z[9] = -abb[55] + z[8];
z[10] = -z[2] + -z[5] + z[6] + z[7] + z[9];
z[10] = abb[15] * z[10];
z[11] = abb[59] + abb[60] + abb[62] + abb[63];
z[12] = abb[61] + z[11];
z[13] = abb[26] * z[12];
z[10] = z[10] + -z[13];
z[14] = -abb[55] + z[6];
z[15] = (T(1) / T(2)) * z[14];
z[16] = 3 * abb[53];
z[17] = z[15] + z[16];
z[18] = 2 * abb[56];
z[19] = -z[0] + z[17] + z[18];
z[19] = abb[6] * z[19];
z[20] = 2 * abb[51];
z[21] = z[18] + -z[20];
z[22] = abb[47] + abb[48];
z[9] = z[9] + z[21] + -z[22];
z[9] = abb[17] * z[9];
z[23] = abb[47] + abb[56];
z[24] = -z[20] + 2 * z[23];
z[25] = abb[48] + abb[57];
z[26] = 3 * abb[58];
z[27] = z[24] + z[25] + -z[26];
z[28] = -abb[3] * z[27];
z[29] = -abb[49] + abb[56];
z[30] = abb[54] + z[29];
z[31] = 3 * abb[48];
z[32] = 2 * z[30] + z[31];
z[32] = abb[12] * z[32];
z[33] = abb[8] * abb[57];
z[34] = abb[55] + z[6];
z[35] = (T(1) / T(2)) * z[34];
z[36] = abb[51] + z[35];
z[37] = -abb[57] + z[36];
z[38] = abb[14] * z[37];
z[39] = z[33] + -z[38];
z[40] = abb[47] + abb[58];
z[41] = -abb[51] + abb[54];
z[42] = abb[50] + -z[40] + -z[41];
z[42] = abb[8] * z[42];
z[43] = z[1] + z[34];
z[40] = -z[40] + z[43];
z[40] = abb[5] * z[40];
z[44] = 4 * abb[58];
z[45] = 2 * abb[57];
z[46] = -abb[47] + -abb[55] + z[44] + -z[45];
z[46] = abb[1] * z[46];
z[47] = -z[18] + z[26];
z[48] = -z[34] + z[47];
z[49] = abb[47] + -abb[53];
z[50] = abb[51] + z[49];
z[51] = -z[48] + z[50];
z[51] = abb[9] * z[51];
z[52] = abb[49] + -abb[55];
z[53] = abb[50] + z[52];
z[54] = -abb[54] + (T(1) / T(2)) * z[53];
z[54] = abb[4] * z[54];
z[55] = abb[8] * z[4];
z[28] = z[9] + -z[10] + z[19] + z[28] + -z[32] + z[39] + z[40] + z[42] + z[46] + z[51] + -z[54] + z[55];
z[28] = abb[33] * z[28];
z[40] = abb[8] * z[20];
z[42] = -z[10] + z[40];
z[46] = 2 * z[9];
z[51] = abb[8] * z[8];
z[51] = -z[46] + z[51];
z[56] = z[51] + -z[55];
z[57] = -z[8] + z[43];
z[58] = abb[5] * z[57];
z[23] = -abb[51] + -abb[58] + z[23];
z[59] = 4 * z[23];
z[60] = abb[3] * z[59];
z[61] = z[56] + -z[58] + z[60];
z[62] = 2 * abb[1];
z[27] = z[27] * z[62];
z[63] = -z[34] + z[45];
z[64] = -z[20] + z[63];
z[65] = abb[14] * z[64];
z[27] = z[27] + -z[65];
z[66] = -abb[51] + z[3];
z[67] = abb[53] + -abb[57] + z[66];
z[48] = z[48] + -z[67];
z[68] = 2 * abb[9];
z[48] = z[48] * z[68];
z[69] = abb[8] * z[34];
z[69] = z[27] + -z[42] + z[48] + z[61] + -z[69];
z[70] = -abb[30] * z[69];
z[28] = z[28] + z[70];
z[28] = abb[33] * z[28];
z[70] = 3 * abb[47];
z[17] = -z[17] + z[20] + -z[26] + z[31] + z[70];
z[17] = abb[7] * z[17];
z[57] = abb[3] * z[57];
z[71] = z[57] + z[58];
z[72] = z[10] + z[71];
z[73] = 2 * abb[50];
z[26] = -abb[55] + z[26];
z[74] = -2 * abb[49] + z[7] + -z[26] + z[73];
z[75] = abb[9] * z[74];
z[75] = z[19] + z[40] + z[75];
z[76] = 5 * abb[53];
z[77] = 2 * abb[44];
z[78] = abb[49] + abb[52] + z[3] + -z[73] + -z[76] + -z[77];
z[78] = abb[8] * z[78];
z[79] = 4 * abb[44];
z[80] = 8 * abb[56];
z[81] = 2 * abb[52];
z[73] = 6 * abb[51] + abb[55] + abb[58] + -z[73] + -z[79] + -z[80] + z[81];
z[73] = abb[0] * z[73];
z[82] = 5 * abb[44];
z[29] = abb[52] + z[29];
z[83] = 2 * z[29] + -z[82];
z[83] = abb[10] * z[83];
z[84] = (T(1) / T(2)) * z[52];
z[85] = abb[50] * (T(1) / T(2));
z[86] = -abb[52] + z[85];
z[87] = z[84] + z[86];
z[88] = abb[44] + z[87];
z[89] = abb[16] * z[88];
z[90] = (T(1) / T(2)) * z[12];
z[91] = abb[27] * z[90];
z[91] = -z[89] + z[91];
z[92] = abb[50] + -abb[52];
z[93] = -z[22] + z[92];
z[94] = abb[44] + -abb[56];
z[94] = -9 * abb[53] + z[93] + -10 * z[94];
z[94] = abb[2] * z[94];
z[95] = abb[44] + abb[47];
z[95] = abb[13] * z[95];
z[96] = -abb[13] * abb[58];
z[97] = 2 * abb[8];
z[98] = abb[13] + z[97];
z[98] = abb[48] * z[98];
z[73] = -z[17] + z[72] + z[73] + z[75] + z[78] + -z[83] + -z[91] + z[94] + z[95] + z[96] + z[98];
z[73] = abb[34] * z[73];
z[78] = abb[55] + z[5];
z[94] = 3 * abb[50];
z[96] = 6 * abb[58];
z[2] = 3 * abb[49] + z[2] + -z[78] + -z[80] + -z[94] + z[96];
z[2] = abb[15] * z[2];
z[2] = z[2] + -z[13];
z[80] = 4 * abb[48];
z[98] = 4 * abb[47] + z[7];
z[99] = z[80] + z[98];
z[100] = -z[1] + z[34] + -z[96] + z[99];
z[101] = abb[3] + abb[5];
z[100] = z[100] * z[101];
z[74] = z[68] * z[74];
z[102] = 6 * abb[53];
z[103] = z[14] + z[102];
z[104] = 8 * abb[51];
z[105] = -z[7] + -z[103] + z[104];
z[106] = abb[6] * z[105];
z[74] = z[74] + -z[106];
z[107] = 6 * abb[48];
z[103] = -6 * abb[47] + -z[0] + z[96] + z[103] + -z[107];
z[103] = abb[7] * z[103];
z[108] = z[6] + z[18];
z[109] = -abb[47] + z[1] + z[108];
z[109] = abb[8] * z[109];
z[110] = abb[8] * abb[48];
z[109] = -z[40] + z[109] + -z[110];
z[100] = -z[2] + -z[74] + -z[100] + -z[103] + 2 * z[109];
z[109] = abb[32] * z[100];
z[111] = z[16] + -z[18] + z[77];
z[112] = z[93] + z[111];
z[113] = 2 * abb[2];
z[112] = z[112] * z[113];
z[114] = -abb[55] + abb[58];
z[115] = z[21] + z[114];
z[116] = abb[44] + z[115];
z[117] = 2 * abb[0];
z[116] = z[116] * z[117];
z[112] = z[112] + z[116];
z[116] = -z[53] + z[81];
z[118] = -z[77] + z[116];
z[119] = abb[16] * z[118];
z[120] = abb[27] * z[12];
z[119] = z[119] + z[120];
z[121] = 2 * z[95];
z[122] = z[119] + z[121];
z[123] = abb[8] + -abb[13];
z[124] = z[4] * z[123];
z[125] = z[122] + -z[124];
z[126] = abb[13] * z[8];
z[127] = -z[125] + z[126];
z[128] = z[49] + -z[92];
z[128] = z[97] * z[128];
z[72] = z[72] + -z[112] + z[127] + z[128];
z[128] = -abb[30] + abb[31];
z[129] = z[72] * z[128];
z[73] = z[73] + z[109] + z[129];
z[73] = abb[34] * z[73];
z[87] = -abb[47] + z[87];
z[109] = abb[44] * (T(-1) / T(2)) + z[87];
z[109] = abb[8] * z[109];
z[129] = abb[52] + abb[56];
z[85] = -abb[51] + -abb[55] + 8 * abb[58] + abb[44] * (T(-19) / T(2)) + abb[47] * (T(-13) / T(2)) + -z[85] + (T(1) / T(2)) * z[129];
z[85] = abb[0] * z[85];
z[129] = abb[8] + -8 * abb[13];
z[129] = abb[58] * z[129];
z[29] = 4 * z[29];
z[130] = abb[44] * (T(29) / T(2)) + -z[29];
z[130] = abb[10] * z[130];
z[85] = -z[9] + z[85] + -z[89] + 8 * z[95] + z[109] + z[129] + z[130];
z[84] = -abb[50] + abb[56] * (T(-7) / T(2)) + z[1] + z[84];
z[89] = abb[54] * (T(1) / T(6));
z[84] = abb[48] * (T(-7) / T(6)) + abb[47] * (T(-3) / T(2)) + z[8] + (T(1) / T(3)) * z[84] + z[89];
z[84] = abb[5] * z[84];
z[109] = abb[22] + abb[24];
z[129] = z[12] * z[109];
z[120] = z[120] + z[129];
z[130] = abb[56] * (T(-7) / T(3)) + z[22] + -z[92];
z[130] = abb[44] * (T(-7) / T(6)) + abb[53] * (T(-2) / T(3)) + (T(-1) / T(2)) * z[130];
z[130] = abb[2] * z[130];
z[18] = -abb[58] + z[18] + -z[35];
z[18] = -abb[53] + abb[44] * (T(-13) / T(6)) + abb[48] * (T(-3) / T(2)) + abb[47] * (T(2) / T(3)) + (T(1) / T(3)) * z[18];
z[18] = abb[3] * z[18];
z[131] = abb[56] + abb[50] * (T(1) / T(3)) + abb[47] * (T(13) / T(3));
z[89] = abb[48] * (T(-10) / T(3)) + abb[58] * (T(-7) / T(3)) + abb[51] * (T(-1) / T(3)) + -z[89] + (T(1) / T(2)) * z[131];
z[89] = abb[1] * z[89];
z[22] = -abb[53] + -abb[58] + z[22];
z[131] = abb[51] * (T(-2) / T(3)) + (T(1) / T(6)) * z[14] + -z[22];
z[131] = abb[7] * z[131];
z[132] = abb[8] * (T(-1) / T(2)) + abb[13] * (T(8) / T(3));
z[133] = abb[48] * z[132];
z[134] = abb[48] * (T(7) / T(2)) + (T(4) / T(3)) * z[30];
z[134] = abb[12] * z[134];
z[135] = 13 * abb[3] + abb[8] + -29 * abb[12];
z[135] = -abb[5] + (T(1) / T(3)) * z[135];
z[135] = abb[0] * (T(-13) / T(6)) + abb[1] * (T(17) / T(3)) + (T(1) / T(2)) * z[135];
z[135] = abb[45] * z[135];
z[132] = 3 * abb[0] + abb[10] * (T(-7) / T(2)) + abb[2] * (T(5) / T(3)) + abb[3] * (T(13) / T(6)) + -z[132];
z[132] = abb[46] * z[132];
z[18] = z[18] + z[84] + (T(1) / T(3)) * z[85] + z[89] + (T(1) / T(6)) * z[120] + z[130] + z[131] + z[132] + z[133] + z[134] + z[135];
z[18] = prod_pow(m1_set::bc<T>[0], 2) * z[18];
z[84] = abb[53] + z[93];
z[85] = abb[2] * z[84];
z[89] = z[35] + -z[49];
z[89] = abb[5] * z[89];
z[93] = abb[50] + abb[55];
z[120] = abb[49] + z[93];
z[130] = abb[44] + abb[54] + (T(-1) / T(2)) * z[120];
z[130] = abb[8] * z[130];
z[131] = -abb[44] + -abb[48] + z[37];
z[131] = abb[3] * z[131];
z[132] = -abb[47] + z[45];
z[133] = -abb[48] + abb[55] + -z[132];
z[133] = abb[1] * z[133];
z[92] = -abb[47] + abb[51] + z[92];
z[92] = abb[0] * z[92];
z[54] = z[39] + z[54] + -z[85] + z[89] + z[92] + z[110] + z[130] + z[131] + z[133];
z[54] = abb[31] * z[54];
z[84] = z[84] * z[113];
z[89] = z[84] + z[91];
z[92] = abb[44] + abb[52];
z[130] = z[92] + -z[93];
z[131] = -abb[51] + z[8] + z[130];
z[131] = abb[0] * z[131];
z[121] = z[89] + z[121] + z[131];
z[25] = -abb[58] + z[25];
z[131] = z[25] * z[62];
z[39] = -z[39] + z[131];
z[131] = abb[49] + abb[55];
z[131] = (T(1) / T(2)) * z[131];
z[86] = z[86] + z[131];
z[133] = -abb[44] + z[86];
z[133] = abb[8] * z[133];
z[134] = -abb[3] * z[37];
z[124] = z[39] + -z[58] + z[121] + -z[124] + -z[126] + z[133] + z[134];
z[124] = abb[30] * z[124];
z[50] = -z[50] + z[108];
z[126] = z[50] * z[68];
z[42] = z[42] + z[126];
z[126] = abb[57] * z[97];
z[65] = z[65] + z[126];
z[126] = z[3] + -z[14];
z[126] = abb[8] * z[126];
z[133] = 2 * abb[3];
z[25] = z[25] * z[133];
z[134] = z[3] + -z[43];
z[134] = abb[5] * z[134];
z[132] = -abb[58] + z[4] + z[132];
z[132] = z[62] * z[132];
z[25] = z[25] + -z[42] + -z[55] + -z[65] + z[126] + z[132] + z[134];
z[25] = abb[33] * z[25];
z[55] = -abb[48] + z[115];
z[55] = z[55] * z[62];
z[126] = 2 * abb[54];
z[132] = -z[53] + z[126];
z[134] = abb[4] * z[132];
z[135] = -abb[50] + abb[54];
z[49] = -z[49] + -z[135];
z[49] = abb[8] * z[49];
z[136] = 4 * abb[5];
z[137] = abb[53] + -abb[56];
z[138] = -abb[48] + z[137];
z[138] = z[136] * z[138];
z[49] = -z[10] + 2 * z[49] + z[55] + -z[57] + z[134] + z[138];
z[49] = abb[32] * z[49];
z[25] = z[25] + z[49] + z[54] + z[124];
z[25] = abb[31] * z[25];
z[23] = z[23] * z[62];
z[49] = -abb[53] + abb[58] + -z[35];
z[49] = abb[5] * z[49];
z[23] = -z[9] + z[23] + z[49] + z[85] + z[91];
z[49] = 3 * abb[51];
z[16] = z[16] + -z[47] + -z[49] + -z[63] + z[70];
z[16] = abb[9] * z[16];
z[33] = z[33] + z[38];
z[38] = -abb[47] + abb[58];
z[54] = abb[51] + abb[52] + -z[38] + -z[52];
z[54] = abb[8] * z[54];
z[57] = -abb[44] + abb[57];
z[24] = abb[58] + -z[24] + z[57];
z[24] = abb[3] * z[24];
z[21] = -abb[47] + abb[55] + -z[21] + z[77];
z[21] = abb[0] * z[21];
z[16] = z[16] + -z[19] + z[21] + -z[23] + z[24] + -z[33] + z[54] + z[83] + z[110];
z[16] = abb[29] * z[16];
z[19] = abb[8] + abb[13];
z[19] = z[8] * z[19];
z[21] = abb[13] * z[4];
z[24] = abb[1] * z[59];
z[19] = z[10] + z[19] + -z[21] + z[24] + -z[46];
z[21] = -abb[57] + z[35] + -z[44] + -z[49] + z[98];
z[21] = abb[3] * z[21];
z[44] = abb[50] * (T(3) / T(2)) + -z[92];
z[49] = abb[49] + -3 * abb[55];
z[49] = -z[44] + (T(1) / T(2)) * z[49];
z[49] = abb[8] * z[49];
z[21] = z[19] + z[21] + z[33] + -z[40] + z[48] + z[49] + -z[121];
z[21] = abb[30] * z[21];
z[33] = -z[3] + z[44] + -z[131];
z[33] = abb[8] * z[33];
z[36] = -abb[57] + z[8] + -z[36] + z[77];
z[36] = abb[3] * z[36];
z[40] = z[66] + z[130];
z[40] = abb[0] * z[40];
z[33] = z[33] + z[36] + -z[39] + z[40] + z[42] + z[89];
z[33] = abb[31] * z[33];
z[36] = abb[34] * z[72];
z[39] = abb[33] * z[69];
z[40] = abb[8] * z[0];
z[40] = z[40] + -z[56] + z[74];
z[1] = -z[1] + z[3];
z[14] = -z[1] + z[7] + z[14];
z[14] = abb[8] * z[14];
z[42] = 3 * abb[56] + -abb[58] + -z[20];
z[44] = abb[53] + z[6] + z[42];
z[48] = 4 * abb[15];
z[44] = z[44] * z[48];
z[14] = z[14] + -z[40] + z[44] + -z[71];
z[48] = -abb[32] * z[14];
z[16] = z[16] + z[21] + z[33] + -z[36] + z[39] + z[48];
z[16] = abb[29] * z[16];
z[21] = -z[52] + -z[94];
z[33] = -abb[58] + z[3];
z[39] = 6 * abb[56] + z[107];
z[21] = (T(1) / T(2)) * z[21] + z[33] + -z[39] + z[76] + z[126];
z[21] = abb[5] * z[21];
z[15] = abb[53] + -z[7] + -z[15] + -z[38];
z[15] = abb[8] * z[15];
z[5] = -abb[53] + z[5] + z[35] + -z[47];
z[5] = abb[3] * z[5];
z[38] = z[42] + -z[135];
z[38] = -abb[48] + 2 * z[38];
z[38] = abb[1] * z[38];
z[5] = z[5] + z[9] + z[15] + z[21] + z[32] + z[38] + -z[44] + z[75] + -z[110];
z[5] = abb[32] * z[5];
z[9] = z[34] + z[102];
z[15] = z[7] + z[8] + -z[9] + z[80];
z[15] = abb[5] * z[15];
z[6] = -12 * abb[51] + 16 * abb[56] + 5 * z[6] + -z[8] + -z[78] + z[102];
z[6] = abb[15] * z[6];
z[21] = abb[54] * z[97];
z[21] = z[21] + -z[134];
z[7] = z[7] + -z[120];
z[7] = abb[8] * z[7];
z[6] = z[6] + z[7] + -z[13] + z[15] + z[21] + -z[40] + -z[55];
z[6] = abb[33] * z[6];
z[7] = 4 * abb[3] + z[136];
z[15] = abb[56] + z[22];
z[7] = z[7] * z[15];
z[15] = abb[8] * z[43];
z[7] = z[7] + z[10] + -z[15] + z[51] + z[103];
z[15] = abb[30] * z[7];
z[5] = z[5] + z[6] + z[15];
z[5] = abb[32] * z[5];
z[6] = -z[41] + -z[45] + z[93];
z[6] = z[6] * z[62];
z[15] = abb[8] * z[120];
z[15] = z[15] + -z[21];
z[22] = abb[25] * z[12];
z[22] = z[22] + z[129];
z[34] = -abb[3] * z[64];
z[38] = abb[5] * z[132];
z[40] = 2 * abb[45];
z[41] = abb[1] + -abb[8];
z[42] = abb[4] + -abb[5] + z[41];
z[42] = z[40] * z[42];
z[6] = z[6] + -z[15] + -z[22] + z[34] + z[38] + z[42] + z[65];
z[6] = abb[36] * z[6];
z[33] = -z[33] + z[35] + z[111];
z[33] = abb[3] * z[33];
z[34] = -abb[48] + abb[58];
z[35] = -abb[8] + 3 * abb[13];
z[34] = z[34] * z[35];
z[38] = -abb[8] * z[87];
z[42] = -z[3] + z[26];
z[44] = abb[44] + z[42];
z[45] = -abb[0] * z[44];
z[17] = z[17] + -z[23] + z[33] + z[34] + z[38] + z[45] + -3 * z[95];
z[23] = prod_pow(abb[30], 2);
z[17] = z[17] * z[23];
z[33] = -abb[37] * z[100];
z[34] = z[0] + -z[43] + z[96] + -z[98];
z[34] = abb[3] * z[34];
z[38] = abb[8] * z[50];
z[43] = z[67] + -z[108];
z[43] = z[43] * z[68];
z[2] = -z[2] + -z[27] + z[34] + 2 * z[38] + z[43] + z[106];
z[2] = abb[35] * z[2];
z[27] = abb[1] + abb[3];
z[34] = -abb[0] + z[27];
z[38] = z[23] * z[34];
z[43] = -z[68] + z[133];
z[45] = abb[1] + abb[8] + z[43];
z[47] = abb[35] * z[45];
z[38] = z[38] + z[47];
z[47] = 5 * abb[12];
z[48] = -abb[4] + z[47];
z[49] = abb[5] + z[97];
z[50] = -abb[9] + z[27];
z[51] = z[48] + -z[49] + -z[50];
z[51] = abb[33] * z[51];
z[52] = 2 * abb[30];
z[54] = -z[45] * z[52];
z[51] = z[51] + z[54];
z[51] = abb[33] * z[51];
z[54] = 3 * abb[1];
z[47] = 6 * abb[5] + -z[47] + z[54] + z[97];
z[47] = abb[32] * z[47];
z[55] = abb[1] + -abb[4];
z[56] = 2 * abb[5] + z[55];
z[59] = abb[33] * z[56];
z[47] = z[47] + -2 * z[59];
z[47] = abb[32] * z[47];
z[49] = -abb[3] + z[49];
z[54] = abb[9] + z[49] + -z[54];
z[54] = abb[33] * z[54];
z[59] = -abb[30] * z[41];
z[56] = abb[32] * z[56];
z[54] = z[54] + z[56] + z[59];
z[49] = -abb[0] + abb[4] + -z[49] + z[62];
z[49] = abb[31] * z[49];
z[49] = z[49] + 2 * z[54];
z[49] = abb[31] * z[49];
z[54] = z[50] * z[52];
z[45] = abb[33] * z[45];
z[41] = abb[0] + -abb[9] + z[41];
z[41] = abb[31] * z[41];
z[41] = z[41] + z[45] + z[54];
z[27] = -abb[0] + 3 * abb[9] + -2 * z[27];
z[27] = abb[29] * z[27];
z[27] = z[27] + 2 * z[41];
z[27] = abb[29] * z[27];
z[27] = z[27] + -2 * z[38] + z[47] + z[49] + z[51];
z[27] = abb[45] * z[27];
z[38] = abb[15] * z[105];
z[13] = z[13] + z[38] + -z[106];
z[38] = abb[50] + -abb[55] + 5 * abb[56];
z[41] = -abb[52] + z[38];
z[41] = 2 * z[41] + z[82] + -z[104];
z[41] = abb[0] * z[41];
z[45] = abb[0] + -abb[10] + z[113];
z[47] = 3 * abb[46];
z[45] = z[45] * z[47];
z[47] = abb[44] + z[137];
z[49] = 6 * abb[2];
z[47] = z[47] * z[49];
z[49] = abb[8] * z[118];
z[29] = -abb[44] + z[29];
z[29] = abb[10] * z[29];
z[29] = -z[13] + z[29] + -z[41] + z[45] + -z[47] + z[49] + -z[119];
z[41] = abb[23] * z[12];
z[22] = z[22] + z[41];
z[41] = -z[22] + -z[29];
z[41] = abb[64] * z[41];
z[39] = z[39] + -z[102] + -z[132];
z[39] = abb[5] * z[39];
z[0] = -abb[54] + -z[0] + z[38];
z[0] = 2 * z[0] + -z[31];
z[0] = abb[1] * z[0];
z[38] = abb[4] + -abb[8];
z[38] = 5 * abb[1] + abb[12] + -2 * z[38] + z[136];
z[38] = abb[45] * z[38];
z[30] = 4 * z[30] + -z[31];
z[30] = abb[12] * z[30];
z[31] = abb[8] * z[53];
z[0] = z[0] + z[13] + -z[21] + -z[30] + z[31] + z[38] + -z[39];
z[13] = -z[0] + z[22];
z[13] = abb[65] * z[13];
z[3] = z[3] + z[116];
z[3] = abb[8] * z[3];
z[3] = z[3] + z[46] + -z[103];
z[9] = z[8] + z[9];
z[30] = z[9] + -z[99];
z[30] = z[30] * z[101];
z[31] = z[8] * z[123];
z[30] = z[3] + z[30] + -z[31] + -z[112] + -z[125];
z[31] = -abb[66] * z[30];
z[38] = abb[0] + -abb[2];
z[39] = z[35] + z[38] + -z[133];
z[45] = z[23] * z[39];
z[46] = abb[3] + -abb[8];
z[38] = z[38] + z[46];
z[38] = abb[31] * z[38];
z[47] = -abb[0] + z[123];
z[49] = abb[2] + z[47];
z[51] = z[49] * z[52];
z[51] = z[38] + z[51];
z[51] = abb[31] * z[51];
z[47] = -abb[2] + z[47];
z[53] = -z[47] * z[128];
z[54] = 3 * abb[10] + -z[117];
z[56] = z[54] + -z[123];
z[59] = 11 * abb[2] + -z[56];
z[59] = abb[34] * z[59];
z[53] = 2 * z[53] + z[59];
z[53] = abb[34] * z[53];
z[59] = abb[34] * z[47];
z[63] = -abb[30] * z[49];
z[38] = -z[38] + z[59] + z[63];
z[46] = -abb[2] + z[46] + z[54];
z[46] = abb[29] * z[46];
z[38] = 2 * z[38] + z[46];
z[38] = abb[29] * z[38];
z[46] = 2 * z[47];
z[47] = abb[66] * z[46];
z[38] = z[38] + z[45] + z[47] + z[51] + z[53];
z[38] = abb[46] * z[38];
z[45] = abb[19] + abb[20];
z[37] = z[37] * z[45];
z[45] = abb[28] * (T(1) / T(2));
z[11] = z[11] * z[45];
z[47] = -abb[21] * z[88];
z[51] = z[57] + -z[86];
z[51] = abb[18] * z[51];
z[45] = abb[61] * z[45];
z[11] = z[11] + z[37] + z[45] + z[47] + z[51];
z[11] = abb[39] * z[11];
z[14] = -z[14] + -z[22];
z[14] = abb[38] * z[14];
z[22] = -z[90] * z[128];
z[37] = abb[33] * z[12];
z[22] = z[22] + z[37];
z[45] = abb[32] * z[12];
z[47] = z[22] + -z[45];
z[47] = abb[29] * z[47];
z[51] = abb[30] * z[12];
z[51] = z[37] + z[51];
z[53] = abb[33] * z[51];
z[22] = abb[31] * z[22];
z[22] = z[22] + z[47] + -z[53];
z[47] = abb[32] * z[90];
z[53] = z[47] + -z[51];
z[53] = abb[32] * z[53];
z[23] = z[23] * z[90];
z[54] = abb[66] * z[12];
z[23] = -z[22] + z[23] + z[53] + -z[54];
z[23] = -z[23] * z[109];
z[53] = abb[34] * z[90];
z[53] = -z[45] + z[53];
z[53] = abb[34] * z[53];
z[47] = -z[37] + z[47];
z[47] = abb[32] * z[47];
z[47] = z[47] + z[53];
z[53] = abb[35] + -abb[37];
z[53] = z[12] * z[53];
z[54] = abb[29] * z[90];
z[54] = -z[45] + z[54];
z[54] = abb[29] * z[54];
z[57] = -prod_pow(abb[33], 2) * z[90];
z[53] = -z[47] + z[53] + z[54] + z[57];
z[53] = abb[23] * z[53];
z[54] = -abb[37] * z[12];
z[22] = z[22] + -z[47] + z[54];
z[22] = abb[25] * z[22];
z[2] = abb[67] + z[2] + z[5] + z[6] + z[11] + z[13] + z[14] + z[16] + z[17] + z[18] + z[22] + z[23] + z[25] + z[27] + z[28] + z[31] + z[33] + z[38] + z[41] + z[53] + z[73];
z[5] = -z[58] + z[84];
z[6] = -z[9] + -z[79] + z[98];
z[6] = abb[3] * z[6];
z[8] = z[4] + -z[8];
z[8] = z[8] * z[35];
z[9] = z[44] * z[117];
z[3] = -z[3] + z[5] + z[6] + z[8] + z[9] + z[24] + 6 * z[95] + z[119];
z[3] = abb[30] * z[3];
z[1] = -z[1] + -z[20] + z[26];
z[1] = z[1] * z[68];
z[1] = z[1] + z[106];
z[6] = -z[81] + z[120];
z[6] = abb[8] * z[6];
z[8] = -z[77] + z[115];
z[8] = z[8] * z[117];
z[6] = -z[1] + z[6] + z[8] + -z[19] + -z[60] + -2 * z[83] + z[84] + z[122];
z[6] = abb[29] * z[6];
z[4] = -z[4] + -z[42];
z[4] = z[4] * z[62];
z[1] = z[1] + z[4] + z[10] + -z[15] + 2 * z[32] + z[61];
z[1] = abb[33] * z[1];
z[4] = -abb[32] * z[7];
z[7] = -abb[48] + z[114];
z[7] = z[7] * z[62];
z[8] = -abb[44] + -z[114];
z[8] = z[8] * z[117];
z[9] = abb[8] * z[81];
z[5] = -z[5] + z[7] + z[8] + z[9] + -z[21] + z[127];
z[5] = abb[31] * z[5];
z[7] = z[34] * z[52];
z[8] = 4 * abb[1] + z[43] + -z[48];
z[8] = abb[33] * z[8];
z[9] = abb[31] * z[55];
z[10] = abb[29] * z[50];
z[7] = z[7] + z[8] + z[9] + -2 * z[10];
z[7] = z[7] * z[40];
z[8] = -abb[30] * z[39];
z[9] = abb[2] + -z[56];
z[9] = abb[29] * z[9];
z[10] = -abb[31] * z[49];
z[8] = z[8] + z[9] + z[10] + -z[59];
z[9] = 2 * abb[46];
z[8] = z[8] * z[9];
z[9] = abb[29] * z[12];
z[10] = z[9] + z[45] + -z[51];
z[10] = -z[10] * z[109];
z[9] = z[9] + -z[37];
z[11] = abb[23] + abb[25];
z[9] = -z[9] * z[11];
z[1] = z[1] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[36];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = -abb[40] * z[29];
z[0] = -abb[41] * z[0];
z[4] = abb[46] * z[46];
z[4] = z[4] + -z[30] + z[129];
z[4] = abb[42] * z[4];
z[5] = z[11] + z[109];
z[6] = -abb[40] + abb[41];
z[5] = z[5] * z[6] * z[12];
z[0] = abb[43] + z[0] + z[1] + z[3] + z[4] + z[5];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_589_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("44.23713195026187218143881542947823139797227645809737307457557201"),stof<T>("49.502518072288631026120604341123315498076331506729782953237808943")}, std::complex<T>{stof<T>("10.624334149109474899015143382588251795531220022564749627778395759"),stof<T>("-65.997105283720751541947529653951386511906066061300445624224606473")}, std::complex<T>{stof<T>("-17.3569211333895307305289182288822151281541099593310891383717436"),stof<T>("-44.846794470766763377782877834095880215250836180536111657441677762")}, std::complex<T>{stof<T>("37.850914001008348630011643986486117162459806765911487901008950891"),stof<T>("-9.085999239260718923919559126710554407860939987189425975425467942")}, std::complex<T>{stof<T>("-4.245631642495581397110755505521075020049942636079332116292261972"),stof<T>("37.167899974669224478950882713679889553150186654647579881421082561")}, std::complex<T>{stof<T>("-9.6828187376176999207974413199552490311688575092476137340277730479"),stof<T>("-9.197316798543106472178986495581096598325966055993558603818360175")}, std::complex<T>{stof<T>("5.0908180600955862436000824111323249140084946285878272807087843549"),stof<T>("-5.8901656697258340185603748109524106697434780372762098677635646154")}, std::complex<T>{stof<T>("-7.98428230247421031110172843502801663774093850048728549049301454"),stof<T>("36.182301262038450442165442601245304117603097762223614328378193531")}, std::complex<T>{stof<T>("-26.880210816872341450909897200596016269818166498766283936203828411"),stof<T>("-4.65572360152186764833772650702743528282549532619367129579613118")}, std::complex<T>{stof<T>("31.472211494394455128107256109418940386978529379426070389522817104"),stof<T>("19.743206069790808139077087813560942550894939419463439767378055971")}, std::complex<T>{stof<T>("3.3676899982809686022714939669936017099032012018749873784947527717"),stof<T>("-0.6711171654161685410993605695944634682903763857024034947602507578")}, std::complex<T>{stof<T>("22.757919100187496475499252166115590582918290638322726505229571943"),stof<T>("-32.875150133221177988546830916616618189020609743506265592323397971")}, stof<T>("-8.1467639364098125700497265545092086142014652425291793387481963645"), std::complex<T>{stof<T>("-38.162658122975090905833493810102835172170037353845123221464291653"),stof<T>("15.647282074402721483579294507257428545894794410168039337949283315")}, std::complex<T>{stof<T>("1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("-1.5961736222681129079204730221135863214600019276878811243393385392")}, std::complex<T>{stof<T>("1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("-1.5961736222681129079204730221135863214600019276878811243393385392")}, std::complex<T>{stof<T>("1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("-1.5961736222681129079204730221135863214600019276878811243393385392")}, std::complex<T>{stof<T>("1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("-1.5961736222681129079204730221135863214600019276878811243393385392")}, std::complex<T>{stof<T>("1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("-1.5961736222681129079204730221135863214600019276878811243393385392")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[22].real()/kbase.W[22].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[36].real()/kbase.W[36].real()), rlog(k.W[76].real()/kbase.W[76].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_589_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_589_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(112)) * (v[1] + v[2] + -v[3] + -v[4]) * (2 * (-112 * m1_set::bc<T>[1] + 84 * m1_set::bc<T>[2] + -56 * m1_set::bc<T>[4] + -49 * v[1] + 7 * v[2] + 21 * v[3] + 13 * v[4] + -28 * v[5]) + 7 * (4 * (2 + -4 * m1_set::bc<T>[1] + 5 * m1_set::bc<T>[2] + -2 * m1_set::bc<T>[4]) * v[0] + 8 * m1_set::bc<T>[1] * v[1] + m1_set::bc<T>[2] * v[1] + -8 * m1_set::bc<T>[1] * v[2] + 13 * m1_set::bc<T>[2] * v[2] + -8 * m1_set::bc<T>[1] * v[3] + 3 * m1_set::bc<T>[2] * v[3] + 8 * m1_set::bc<T>[1] * v[4] + -13 * m1_set::bc<T>[2] * v[4] + 16 * m1_set::bc<T>[1] * v[5] + -12 * m1_set::bc<T>[2] * v[5] + 4 * m1_set::bc<T>[4] * (v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])))) / prod_pow(tend, 2);
c[1] = (-(4 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2] + 2 * m1_set::bc<T>[4]) * (v[1] + v[2] + -1 * v[3] + -1 * v[4])) / tend;


		return (abb[47] + abb[48] + abb[49] + -abb[50] + 2 * abb[51] + -2 * abb[53] + -2 * abb[56]) * (2 * t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[10];
z[0] = abb[29] + abb[30] + -abb[31];
z[1] = 5 * abb[34];
z[2] = 2 * z[0] + -z[1];
z[2] = abb[34] * z[2];
z[3] = -2 * abb[34] + z[0];
z[3] = -abb[32] + 2 * z[3];
z[4] = abb[32] * z[3];
z[5] = -abb[35] + abb[38];
z[2] = -z[2] + z[4] + 2 * z[5];
z[4] = 4 * abb[37];
z[6] = -z[2] + -z[4];
z[7] = -abb[47] + -abb[49] + abb[50];
z[7] = z[6] * z[7];
z[6] = 2 * z[6];
z[8] = -abb[51] + abb[53];
z[6] = z[6] * z[8];
z[8] = 2 * abb[56];
z[2] = -z[2] * z[8];
z[9] = 2 * abb[48];
z[5] = z[5] * z[9];
z[0] = -z[0] * z[9];
z[1] = abb[48] * z[1];
z[0] = z[0] + z[1];
z[0] = abb[34] * z[0];
z[1] = abb[48] + -z[8];
z[1] = z[1] * z[4];
z[3] = abb[32] * abb[48] * z[3];
z[0] = z[0] + z[1] + z[2] + z[3] + z[5] + z[6] + z[7];
return abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_589_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (abb[47] + abb[48] + abb[49] + -abb[50] + 2 * abb[51] + -2 * abb[53] + -2 * abb[56]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[53] + -abb[56];
z[0] = abb[47] + abb[48] + abb[49] + -abb[50] + 2 * abb[51] + 2 * z[0];
z[1] = -abb[32] + abb[34];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_589_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-34.088696593189916341299168418629364323798529415304722665947423388"),stof<T>("-51.493598445318449833808303002488252352723527170284346934832118752")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,68> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W25(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W77(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_14(k), f_2_20(k), f_2_23(k), f_2_2_im(k), f_2_10_im(k), f_2_12_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[13].real()/k.W[13].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[22].real()/k.W[22].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[36].real()/k.W[36].real()), rlog(kend.W[76].real()/k.W[76].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_2_re(k), f_2_10_re(k), f_2_12_re(k), T{0}};
abb[43] = SpDLog_f_4_589_W_20_Im(t, path, abb);
abb[67] = SpDLog_f_4_589_W_20_Re(t, path, abb);

                    
            return f_4_589_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_589_DLogXconstant_part(base_point<T>, kend);
	value += f_4_589_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_589_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_589_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_589_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_589_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_589_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_589_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
