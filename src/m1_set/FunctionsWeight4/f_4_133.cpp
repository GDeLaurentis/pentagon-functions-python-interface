/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_133.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_133_abbreviated (const std::array<T,57>& abb) {
T z[68];
z[0] = abb[28] + abb[32];
z[1] = abb[30] + -abb[31];
z[2] = z[0] + -z[1];
z[2] = -abb[33] + (T(1) / T(2)) * z[2];
z[2] = abb[1] * z[2];
z[3] = abb[10] * abb[31];
z[4] = abb[10] * abb[33];
z[3] = z[3] + -z[4];
z[2] = z[2] + (T(-1) / T(2)) * z[3];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = abb[29] * (T(1) / T(2));
z[5] = abb[30] * (T(3) / T(2)) + -z[3];
z[6] = abb[28] + abb[31];
z[7] = (T(1) / T(2)) * z[6];
z[8] = abb[32] + z[7];
z[9] = abb[33] + z[5] + -z[8];
z[9] = m1_set::bc<T>[0] * z[9];
z[10] = abb[40] * (T(3) / T(2));
z[9] = z[9] + z[10];
z[11] = abb[3] * (T(1) / T(2));
z[9] = z[9] * z[11];
z[12] = -abb[40] + abb[41];
z[13] = abb[4] * (T(1) / T(2));
z[12] = z[12] * z[13];
z[14] = abb[39] + -abb[40];
z[15] = abb[8] * (T(1) / T(4));
z[16] = z[14] * z[15];
z[17] = abb[0] * (T(1) / T(4));
z[18] = abb[28] + z[1];
z[19] = -abb[29] + z[18];
z[19] = m1_set::bc<T>[0] * z[19];
z[20] = abb[39] + z[19];
z[21] = z[17] * z[20];
z[22] = abb[1] * (T(1) / T(2));
z[23] = abb[41] * z[22];
z[20] = abb[40] + z[20];
z[24] = abb[14] * z[20];
z[25] = -abb[33] + z[0];
z[26] = -abb[30] + z[25];
z[27] = m1_set::bc<T>[0] * z[26];
z[28] = -abb[40] + z[27];
z[29] = abb[15] * z[28];
z[2] = z[2] + -z[9] + z[12] + z[16] + z[21] + -z[23] + (T(-1) / T(4)) * z[24] + (T(-1) / T(2)) * z[29];
z[9] = abb[40] + (T(1) / T(2)) * z[19];
z[12] = abb[11] * z[9];
z[12] = z[2] + z[12];
z[16] = abb[46] + abb[48];
z[12] = z[12] * z[16];
z[21] = abb[20] + abb[22];
z[23] = z[21] * z[26];
z[26] = abb[23] * abb[32];
z[30] = abb[21] * abb[32];
z[26] = z[26] + z[30];
z[31] = abb[21] + abb[23];
z[32] = abb[30] * z[31];
z[33] = -abb[29] + z[25];
z[34] = abb[24] * z[33];
z[23] = z[23] + z[26] + -z[32] + z[34];
z[23] = m1_set::bc<T>[0] * z[23];
z[32] = abb[39] + -abb[41];
z[34] = abb[24] + z[21];
z[34] = -z[32] * z[34];
z[35] = abb[22] + abb[23];
z[36] = abb[20] + abb[21] + z[35];
z[36] = abb[40] * z[36];
z[37] = abb[25] * z[20];
z[23] = -z[23] + z[34] + z[36] + -z[37];
z[34] = abb[42] + -abb[43] + -abb[45];
z[36] = (T(-1) / T(2)) * z[34];
z[37] = z[23] * z[36];
z[38] = abb[0] + -abb[2] + abb[4];
z[39] = -abb[40] * z[38];
z[27] = abb[1] * z[27];
z[27] = z[27] + -z[29] + z[39];
z[27] = abb[51] * z[27];
z[19] = abb[40] + z[19];
z[39] = abb[52] * z[19];
z[40] = abb[39] * abb[52];
z[41] = z[39] + z[40];
z[41] = abb[14] * z[41];
z[42] = -abb[52] * z[9];
z[42] = (T(-1) / T(2)) * z[40] + z[42];
z[42] = abb[0] * z[42];
z[43] = abb[51] * z[28];
z[39] = (T(1) / T(2)) * z[39] + z[43];
z[39] = abb[3] * z[39];
z[43] = abb[40] * abb[52];
z[40] = -z[40] + z[43];
z[44] = abb[8] * (T(1) / T(2));
z[40] = z[40] * z[44];
z[14] = abb[16] * z[14];
z[45] = -abb[19] * z[20];
z[14] = z[14] + z[45];
z[45] = abb[53] * (T(1) / T(2));
z[14] = z[14] * z[45];
z[43] = abb[2] * z[43];
z[14] = z[14] + z[27] + z[37] + z[39] + z[40] + (T(1) / T(2)) * z[41] + z[42] + z[43];
z[2] = abb[49] * z[2];
z[27] = m1_set::bc<T>[0] * z[33];
z[27] = z[27] + z[32];
z[33] = abb[13] * (T(1) / T(2));
z[27] = z[27] * z[33];
z[37] = -abb[28] + abb[33];
z[37] = m1_set::bc<T>[0] * z[37];
z[39] = -z[32] + z[37];
z[40] = abb[7] * (T(1) / T(2));
z[39] = z[39] * z[40];
z[37] = abb[9] * z[37];
z[27] = z[27] + -z[29] + -z[37] + z[39];
z[29] = abb[30] + -abb[32];
z[37] = m1_set::bc<T>[0] * z[29];
z[37] = abb[40] + z[37];
z[39] = abb[6] * (T(1) / T(2));
z[37] = z[37] * z[39];
z[41] = -abb[1] + abb[5] * (T(1) / T(2));
z[42] = -abb[31] + abb[33];
z[42] = m1_set::bc<T>[0] * z[42];
z[42] = abb[41] + z[42];
z[43] = -z[41] * z[42];
z[46] = abb[29] + -abb[30];
z[46] = m1_set::bc<T>[0] * z[46];
z[46] = -abb[40] + z[46];
z[46] = z[11] * z[46];
z[47] = z[28] + -z[32];
z[47] = z[44] * z[47];
z[48] = -abb[29] + z[1];
z[49] = abb[33] + z[48];
z[49] = m1_set::bc<T>[0] * z[49];
z[50] = -abb[40] + -abb[41] + -z[49];
z[50] = z[13] * z[50];
z[20] = -abb[0] * z[20];
z[20] = z[20] + (T(1) / T(2)) * z[24] + z[27] + -z[37] + z[43] + z[46] + z[47] + z[50];
z[43] = abb[50] * (T(1) / T(2));
z[20] = z[20] * z[43];
z[42] = abb[5] * z[42];
z[24] = -z[24] + z[42];
z[5] = abb[32] + -z[5];
z[5] = m1_set::bc<T>[0] * z[5];
z[5] = z[5] + -z[10];
z[5] = abb[3] * z[5];
z[10] = z[28] + z[32];
z[10] = z[10] * z[44];
z[28] = -3 * abb[40] + abb[41] + -z[49];
z[28] = z[13] * z[28];
z[5] = z[5] + z[10] + (T(1) / T(2)) * z[24] + z[27] + z[28] + z[37];
z[10] = abb[47] * (T(1) / T(2));
z[5] = z[5] * z[10];
z[24] = abb[44] * (T(1) / T(4));
z[23] = -z[23] * z[24];
z[27] = abb[17] * abb[53];
z[27] = (T(1) / T(4)) * z[27];
z[19] = -z[19] * z[27];
z[28] = abb[49] + -abb[52];
z[32] = abb[47] + z[28];
z[32] = abb[11] * z[32];
z[9] = z[9] * z[32];
z[2] = z[2] + z[5] + z[9] + z[12] + (T(1) / T(2)) * z[14] + z[19] + z[20] + z[23];
z[2] = (T(1) / T(4)) * z[2];
z[5] = z[0] + z[48];
z[5] = abb[29] * z[5];
z[9] = abb[34] + z[5];
z[12] = -z[1] + z[25];
z[12] = abb[33] * z[12];
z[14] = abb[54] + -abb[56];
z[19] = z[12] + -z[14];
z[20] = abb[30] * z[0];
z[23] = abb[31] * abb[32];
z[25] = abb[28] * abb[31];
z[20] = -z[9] + z[19] + z[20] + -z[23] + -z[25];
z[37] = abb[24] * z[20];
z[42] = abb[33] * (T(1) / T(2));
z[46] = abb[30] + z[42];
z[47] = -z[6] + z[46];
z[47] = abb[33] * z[47];
z[48] = prod_pow(abb[28], 2);
z[49] = (T(1) / T(2)) * z[48];
z[50] = abb[34] + z[49];
z[7] = abb[31] * z[7];
z[51] = z[7] + z[50];
z[47] = -z[47] + -z[51];
z[47] = abb[23] * z[47];
z[52] = abb[30] * (T(1) / T(2));
z[53] = z[8] + -z[52];
z[53] = abb[30] * z[53];
z[19] = -z[7] + z[19] + z[53];
z[53] = abb[32] * (T(1) / T(2));
z[54] = abb[28] + z[53];
z[54] = abb[32] * z[54];
z[55] = -abb[37] + z[19] + -z[54];
z[55] = abb[22] * z[55];
z[54] = abb[37] + z[54];
z[56] = abb[35] + z[54];
z[19] = -z[19] + z[56];
z[57] = -abb[55] + z[19];
z[57] = abb[20] * z[57];
z[58] = z[31] * z[52];
z[59] = abb[23] * z[8];
z[30] = z[30] + -z[58] + z[59];
z[30] = abb[30] * z[30];
z[58] = z[3] * z[31];
z[26] = -z[26] + z[58];
z[26] = abb[29] * z[26];
z[25] = z[25] + -z[48];
z[58] = abb[30] * z[18];
z[58] = -z[25] + z[58];
z[58] = (T(1) / T(2)) * z[58];
z[59] = -z[3] + z[18];
z[60] = abb[29] * z[59];
z[61] = -abb[54] + z[58] + -z[60];
z[62] = prod_pow(m1_set::bc<T>[0], 2);
z[63] = (T(1) / T(6)) * z[62];
z[64] = abb[55] + z[63];
z[65] = -z[61] + z[64];
z[66] = abb[25] * z[65];
z[21] = z[21] + z[31];
z[21] = z[21] * z[63];
z[67] = abb[22] + z[31];
z[67] = abb[55] * z[67];
z[31] = abb[24] + z[31];
z[31] = abb[36] * z[31];
z[35] = abb[35] * z[35];
z[21] = z[21] + z[26] + z[30] + z[31] + -z[35] + z[37] + z[47] + z[55] + -z[57] + -z[66] + z[67];
z[26] = -z[21] * z[36];
z[30] = -z[0] + z[46];
z[30] = abb[33] * z[30];
z[30] = z[30] + z[49] + z[54];
z[0] = -z[0] + z[52];
z[0] = abb[30] * z[0];
z[31] = z[0] + z[30];
z[35] = -z[31] + z[63];
z[35] = abb[1] * z[35];
z[31] = z[31] + -z[64];
z[36] = abb[15] * z[31];
z[37] = abb[55] * z[38];
z[35] = z[35] + z[36] + z[37];
z[35] = abb[51] * z[35];
z[37] = abb[14] * z[65];
z[38] = (T(1) / T(2)) * z[37];
z[46] = z[3] * z[59];
z[47] = (T(1) / T(12)) * z[62];
z[49] = abb[55] + z[46] + z[47];
z[55] = abb[54] + -z[58];
z[55] = z[49] + (T(1) / T(2)) * z[55];
z[55] = abb[0] * z[55];
z[57] = -abb[28] + abb[31];
z[58] = abb[30] * z[57];
z[25] = -z[25] + z[58];
z[25] = -abb[54] + abb[55] + (T(1) / T(2)) * z[25];
z[58] = -z[25] * z[44];
z[59] = -abb[2] * abb[55];
z[55] = -z[38] + z[55] + z[58] + z[59];
z[55] = abb[52] * z[55];
z[58] = z[52] + -z[57];
z[59] = abb[30] * z[58];
z[59] = z[59] + -z[60] + -z[64];
z[60] = abb[52] * z[59];
z[31] = -abb[51] * z[31];
z[31] = z[31] + (T(1) / T(2)) * z[60];
z[31] = abb[3] * z[31];
z[60] = abb[19] * z[65];
z[66] = abb[16] * z[25];
z[60] = z[60] + z[66];
z[60] = z[45] * z[60];
z[26] = z[26] + z[31] + z[35] + z[55] + z[60];
z[21] = z[21] * z[24];
z[24] = -abb[32] + z[3];
z[24] = abb[29] * z[24];
z[24] = abb[36] + z[24];
z[31] = -abb[32] + z[42];
z[35] = abb[33] * z[31];
z[50] = -z[14] + -z[24] + -z[35] + z[50] + -z[54];
z[40] = z[40] * z[50];
z[20] = abb[36] + z[20];
z[20] = z[20] * z[33];
z[33] = abb[28] * abb[30];
z[33] = -z[24] + -z[30] + z[33];
z[33] = abb[9] * z[33];
z[20] = z[20] + z[33] + z[36] + -z[40];
z[29] = z[29] + z[57];
z[29] = abb[33] * z[29];
z[29] = -abb[35] + z[29] + z[54];
z[33] = -abb[32] + z[52];
z[40] = 3 * abb[28] + -abb[31];
z[40] = -z[33] + (T(1) / T(2)) * z[40];
z[40] = abb[30] * z[40];
z[7] = z[7] + z[14] + -z[29] + z[40] + -z[48] + z[64];
z[7] = z[7] * z[44];
z[14] = z[53] + -z[57];
z[14] = abb[32] * z[14];
z[40] = abb[31] * z[57];
z[14] = z[14] + (T(1) / T(2)) * z[40];
z[40] = -z[18] * z[52];
z[50] = abb[35] + abb[37];
z[9] = -abb[36] + z[9] + -z[14] + z[40] + -z[50] + z[64];
z[9] = z[9] * z[11];
z[40] = -abb[31] + z[52];
z[40] = abb[30] * z[40];
z[5] = abb[36] + -z[5] + z[40];
z[40] = prod_pow(abb[31], 2);
z[54] = -z[40] + -z[48];
z[12] = abb[56] + -z[5] + z[12] + (T(1) / T(2)) * z[54] + -z[56] + z[64];
z[12] = z[12] * z[13];
z[1] = -abb[28] + z[1] + z[42];
z[1] = abb[33] * z[1];
z[54] = z[6] * z[52];
z[1] = abb[35] + z[1] + z[51] + -z[54];
z[1] = abb[12] * z[1];
z[51] = abb[35] + (T(1) / T(2)) * z[40];
z[35] = abb[56] + -z[23] + -z[35] + z[51];
z[41] = z[35] * z[41];
z[33] = abb[30] * z[33];
z[24] = -z[24] + z[33] + -z[64];
z[24] = z[24] * z[39];
z[33] = abb[0] * z[65];
z[39] = abb[16] + abb[19];
z[54] = abb[17] + abb[18];
z[55] = z[39] + -z[54];
z[55] = abb[38] * z[55];
z[7] = (T(1) / T(2)) * z[1] + z[7] + z[9] + z[12] + z[20] + -z[24] + z[33] + -z[38] + z[41] + (T(1) / T(4)) * z[55];
z[7] = z[7] * z[43];
z[6] = z[6] + z[53];
z[6] = abb[32] * z[6];
z[9] = z[40] + -z[48];
z[9] = (T(1) / T(2)) * z[9];
z[0] = -abb[56] + z[0] + z[6] + -z[9];
z[6] = abb[28] * (T(1) / T(2)) + -z[31] + -z[52];
z[6] = abb[33] * z[6];
z[0] = (T(1) / T(2)) * z[0] + -z[6] + -z[47];
z[0] = abb[1] * z[0];
z[6] = -abb[31] + abb[32];
z[4] = z[4] * z[6];
z[6] = z[23] + -z[40];
z[6] = abb[10] * z[6];
z[4] = z[4] + -z[6] + -z[36];
z[6] = abb[30] * (T(3) / T(4));
z[8] = z[6] + -z[8];
z[8] = abb[30] * z[8];
z[12] = abb[55] * (T(3) / T(2)) + (T(1) / T(4)) * z[62];
z[8] = z[8] + -z[12] + z[30] + -z[46];
z[8] = z[8] * z[11];
z[11] = z[61] + -z[63];
z[11] = z[11] * z[17];
z[17] = -abb[31] + z[42];
z[17] = abb[33] * z[17];
z[17] = abb[55] + -abb[56] + z[17] + z[51];
z[17] = z[13] * z[17];
z[15] = z[15] * z[25];
z[23] = -abb[10] + z[22];
z[23] = abb[35] * z[23];
z[22] = abb[37] * z[22];
z[0] = z[0] + (T(1) / T(2)) * z[4] + z[8] + -z[11] + -z[15] + -z[17] + z[22] + -z[23] + (T(-1) / T(4)) * z[37];
z[4] = z[52] * z[58];
z[4] = z[4] + -z[49];
z[8] = abb[11] * z[4];
z[11] = abb[38] * (T(1) / T(8));
z[15] = z[11] * z[39];
z[8] = z[0] + -z[8] + z[15];
z[8] = -z[8] * z[16];
z[0] = -abb[49] * z[0];
z[15] = -abb[5] * z[35];
z[1] = z[1] + z[15] + z[37];
z[14] = -abb[34] + -abb[36] + z[14] + z[50];
z[6] = abb[32] + -z[6] + (T(1) / T(4)) * z[57];
z[6] = abb[30] * z[6];
z[15] = -abb[32] + z[18];
z[3] = z[3] * z[15];
z[3] = z[3] + z[6] + z[12] + (T(-1) / T(2)) * z[14];
z[3] = abb[3] * z[3];
z[5] = 3 * abb[55] + -abb[56] + -z[5] + z[9] + -z[29] + z[63];
z[5] = z[5] * z[13];
z[6] = -z[19] + z[64];
z[6] = z[6] * z[44];
z[1] = (T(1) / T(2)) * z[1] + z[3] + z[5] + z[6] + z[20] + z[24];
z[1] = z[1] * z[10];
z[3] = -z[28] * z[39];
z[5] = -z[39] + -z[54];
z[5] = abb[47] * z[5];
z[6] = abb[44] + z[34];
z[6] = abb[26] * z[6];
z[9] = -abb[0] + -abb[8] + -abb[14];
z[9] = abb[53] * z[9];
z[3] = z[3] + z[5] + z[6] + z[9];
z[3] = z[3] * z[11];
z[5] = -z[27] * z[59];
z[4] = z[4] * z[32];
z[6] = abb[27] * abb[38] * z[45];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[21] + (T(1) / T(2)) * z[26];
z[0] = (T(1) / T(4)) * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_133_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.15544607849632252103623350723968708096710118774860114818442850773"),stof<T>("-0.27679292663235168124861761364236671935592401363901717350041320928")}, std::complex<T>{stof<T>("-0.16267222866799748427671488366865148402518682361471992495473377346"),stof<T>("-0.14283511796977033989324523641807141792764615187667724283671402065")}, std::complex<T>{stof<T>("-0.15544607849632252103623350723968708096710118774860114818442850773"),stof<T>("-0.27679292663235168124861761364236671935592401363901717350041320928")}, std::complex<T>{stof<T>("-0.15544607849632252103623350723968708096710118774860114818442850773"),stof<T>("-0.27679292663235168124861761364236671935592401363901717350041320928")}, std::complex<T>{stof<T>("-0.21048062489256053764196837293710010686895007511718671115592204823"),stof<T>("0.04194482283851053381871003559965396676814852410640021842251567236")}, std::complex<T>{stof<T>("0.03821248390872231344252662188781923345220967043504184902307007321"),stof<T>("-0.51967346510026199823181284194628829558252737077167126374593754432")}, std::complex<T>{stof<T>("0.032870383003594372288837417966425626302093458254634235113250177528"),stof<T>("-0.060747135561956564218717333797977185097328502954697283075871499618")}, std::complex<T>{stof<T>("-0.25186751860815181882323830401799984856501583090687365866823217018"),stof<T>("0.27602727033741684098080560531544625171981658056668086306849614823")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_133_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_133_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.29125453957040087051919869849396333399652537439058339158447977451"),stof<T>("-0.03068996749074059118317976479273059248876755809690744889799906302")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,57> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W18(k,dl), dlog_W22(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W76(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_19(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_11_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_4_re(k), f_2_7_re(k), f_2_11_re(k)};

                    
            return f_4_133_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_133_DLogXconstant_part(base_point<T>, kend);
	value += f_4_133_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_133_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_133_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_133_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_133_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_133_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_133_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
