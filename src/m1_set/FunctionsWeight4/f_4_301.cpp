/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_301.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_301_abbreviated (const std::array<T,29>& abb) {
T z[89];
z[0] = abb[23] * (T(3) / T(2));
z[1] = abb[18] * (T(3) / T(2));
z[2] = z[0] + z[1];
z[3] = abb[19] * (T(-11) / T(2)) + z[2];
z[4] = abb[22] * (T(3) / T(2));
z[5] = 2 * abb[20];
z[6] = z[4] + -z[5];
z[7] = 3 * abb[21];
z[8] = abb[25] + abb[26];
z[9] = abb[24] + z[7] + -z[8];
z[10] = z[3] + z[6] + z[9];
z[11] = abb[6] * z[10];
z[12] = abb[19] * (T(7) / T(2));
z[13] = z[5] + z[12];
z[14] = abb[24] * (T(7) / T(2));
z[15] = abb[21] * (T(1) / T(2));
z[16] = abb[25] + z[13] + -z[14] + z[15];
z[17] = abb[23] * (T(1) / T(2));
z[18] = 2 * abb[22];
z[19] = -z[1] + z[16] + z[17] + z[18];
z[19] = abb[3] * z[19];
z[20] = abb[18] * (T(1) / T(2));
z[21] = z[17] + z[20];
z[22] = 10 * abb[19];
z[23] = 4 * abb[20];
z[24] = -z[21] + z[22] + z[23];
z[25] = abb[22] * (T(1) / T(2));
z[26] = z[8] + z[25];
z[27] = 6 * abb[24];
z[28] = abb[21] * (T(3) / T(2));
z[29] = -z[24] + -z[26] + z[27] + z[28];
z[29] = abb[2] * z[29];
z[30] = abb[24] * (T(1) / T(2));
z[31] = abb[18] + -abb[23];
z[13] = abb[26] + abb[22] * (T(5) / T(2)) + -z[13] + -z[30] + -z[31];
z[13] = abb[0] * z[13];
z[32] = 4 * abb[19];
z[33] = z[5] + z[32];
z[34] = 3 * abb[24];
z[35] = z[33] + -z[34];
z[36] = abb[21] + abb[22];
z[37] = z[35] + z[36];
z[37] = 2 * z[37];
z[38] = abb[4] * z[37];
z[39] = abb[18] + abb[23];
z[35] = z[35] + z[39];
z[40] = abb[5] * z[35];
z[41] = 2 * abb[23];
z[42] = 2 * abb[18];
z[43] = z[41] + z[42];
z[44] = 4 * abb[21];
z[45] = z[43] + z[44];
z[46] = 10 * abb[20];
z[47] = 23 * abb[19] + z[46];
z[48] = -abb[22] + -z[45] + z[47];
z[8] = 2 * z[8];
z[49] = 11 * abb[24] + -z[8] + -z[48];
z[49] = abb[1] * z[49];
z[50] = -abb[27] + abb[28];
z[51] = abb[8] * z[50];
z[52] = (T(3) / T(2)) * z[51];
z[53] = abb[7] * z[50];
z[54] = (T(1) / T(2)) * z[53];
z[13] = -z[11] + z[13] + z[19] + z[29] + z[38] + z[40] + z[49] + -z[52] + -z[54];
z[13] = abb[13] * z[13];
z[19] = abb[19] * (T(-9) / T(2)) + abb[24] * (T(5) / T(2)) + -z[5];
z[29] = abb[25] + -z[18] + z[19] + z[21] + -z[28];
z[29] = abb[3] * z[29];
z[49] = 2 * abb[24];
z[55] = z[15] + z[49];
z[56] = -abb[25] + abb[26];
z[32] = abb[22] * (T(7) / T(2)) + -z[2] + z[32] + -z[55] + z[56];
z[32] = abb[2] * z[32];
z[57] = 5 * abb[22];
z[58] = 2 * abb[21];
z[59] = 2 * abb[25];
z[60] = z[57] + z[58] + -z[59];
z[61] = abb[19] + abb[24] + z[5] + -z[60];
z[61] = abb[4] * z[61];
z[62] = -abb[26] + z[25];
z[63] = abb[19] * (T(1) / T(2));
z[30] = z[30] + z[62] + z[63];
z[30] = abb[0] * z[30];
z[64] = 7 * abb[19];
z[65] = abb[22] + -z[39] + z[64];
z[66] = -z[5] + z[34];
z[67] = -abb[26] + z[58];
z[68] = -z[65] + z[66] + z[67];
z[69] = abb[1] * z[68];
z[54] = (T(1) / T(2)) * z[51] + -z[54];
z[29] = z[11] + z[29] + z[30] + z[32] + z[54] + z[61] + -2 * z[69];
z[29] = abb[12] * z[29];
z[30] = abb[21] * (T(5) / T(2));
z[32] = abb[24] * (T(3) / T(2));
z[69] = 3 * abb[22];
z[70] = -abb[25] + z[17] + -z[20] + z[30] + -z[32] + -z[63] + z[69];
z[70] = abb[3] * z[70];
z[71] = abb[19] * (T(5) / T(2));
z[6] = -z[6] + -z[32] + -z[67] + z[71];
z[6] = abb[0] * z[6];
z[67] = -z[34] + z[59] + -z[69];
z[72] = 6 * abb[19];
z[73] = z[7] + -z[23] + -z[67] + -z[72];
z[73] = abb[4] * z[73];
z[64] = z[5] + z[64];
z[74] = abb[23] + z[42];
z[75] = abb[21] + z[49];
z[76] = -abb[26] + z[75];
z[76] = -z[18] + -z[64] + z[74] + 2 * z[76];
z[76] = abb[1] * z[76];
z[4] = z[4] + z[56];
z[2] = -z[2] + z[4];
z[56] = 2 * abb[19];
z[15] = -z[2] + z[15] + -z[56];
z[15] = abb[2] * z[15];
z[6] = z[6] + -z[11] + z[15] + z[52] + (T(3) / T(2)) * z[53] + z[70] + z[73] + z[76];
z[6] = abb[11] * z[6];
z[15] = 11 * abb[19] + z[23];
z[52] = 3 * abb[23];
z[70] = 3 * abb[18] + z[52];
z[9] = 2 * z[9] + -z[15] + z[69] + z[70];
z[9] = abb[6] * z[9];
z[37] = abb[3] * z[37];
z[37] = -z[9] + z[37];
z[73] = 2 * abb[26];
z[76] = -z[34] + z[73];
z[77] = -z[18] + z[31];
z[78] = z[56] + -z[76] + z[77];
z[78] = abb[0] * z[78];
z[79] = 5 * abb[19];
z[60] = z[34] + -z[60] + -z[79];
z[60] = abb[4] * z[60];
z[80] = 2 * abb[2];
z[68] = -z[68] * z[80];
z[81] = 37 * abb[19] + 14 * abb[20];
z[82] = -z[42] + z[81];
z[83] = 8 * abb[21] + 4 * abb[23];
z[84] = abb[25] + z[73];
z[85] = -abb[22] + -17 * abb[24] + z[82] + -z[83] + 2 * z[84];
z[85] = abb[1] * z[85];
z[40] = -z[37] + -z[40] + z[60] + z[68] + z[78] + z[85];
z[40] = abb[14] * z[40];
z[6] = z[6] + z[13] + z[29] + z[40];
z[6] = abb[11] * z[6];
z[13] = abb[20] + -abb[21];
z[29] = 3 * abb[19];
z[40] = -abb[25] + z[34];
z[60] = z[13] + z[29] + z[39] + -z[40];
z[60] = abb[3] * z[60];
z[68] = -z[20] + z[32];
z[78] = abb[19] + abb[20];
z[36] = -abb[26] + z[0] + z[36] + -z[68] + z[78];
z[36] = abb[0] * z[36];
z[85] = 3 * abb[20];
z[32] = abb[19] * (T(-13) / T(2)) + z[4] + z[32] + z[58] + -z[85];
z[32] = abb[2] * z[32];
z[58] = 7 * abb[24];
z[86] = z[58] + -z[59];
z[87] = abb[23] * (T(1) / T(4)) + abb[20] * (T(13) / T(2)) + abb[19] * (T(55) / T(4));
z[25] = abb[18] + z[25] + z[30] + z[86] + -z[87];
z[25] = abb[1] * z[25];
z[88] = -abb[19] + abb[23];
z[1] = abb[20] * (T(1) / T(2)) + z[1] + z[30] + -z[73] + (T(5) / T(4)) * z[88];
z[1] = abb[5] * z[1];
z[1] = z[1] + -z[11] + z[25] + z[32] + z[36] + z[60];
z[1] = abb[13] * z[1];
z[25] = -abb[24] + z[33] + z[39] + -z[73];
z[25] = abb[5] * z[25];
z[16] = -z[16] + -z[21];
z[16] = abb[3] * z[16];
z[19] = z[19] + -z[39] + -z[62];
z[19] = abb[0] * z[19];
z[4] = abb[21] * (T(-7) / T(2)) + -z[4] + z[24] + -z[49];
z[4] = abb[2] * z[4];
z[21] = -9 * abb[24] + z[48] + z[59];
z[24] = abb[1] * z[21];
z[4] = z[4] + z[11] + z[16] + z[19] + z[24] + -z[25] + -z[54];
z[4] = abb[12] * z[4];
z[16] = abb[0] * z[35];
z[9] = -z[9] + z[16];
z[19] = abb[20] + abb[22];
z[24] = -abb[23] + -z[19] + z[40] + -z[56];
z[24] = abb[3] * z[24];
z[30] = 4 * abb[18];
z[32] = 7 * abb[21] + z[30];
z[35] = -10 * abb[24] + abb[26] + z[59];
z[35] = 23 * abb[20] + abb[23] * (T(-13) / T(2)) + abb[19] * (T(101) / T(2)) + -z[32] + 2 * z[35] + -z[57];
z[35] = abb[1] * z[35];
z[36] = 7 * abb[20];
z[34] = abb[26] + z[34];
z[0] = -abb[18] + -abb[21] + abb[19] * (T(-25) / T(2)) + -z[0] + 2 * z[34] + -z[36];
z[0] = abb[5] * z[0];
z[21] = abb[2] * z[21];
z[0] = z[0] + -z[9] + z[21] + 2 * z[24] + z[35] + -z[38];
z[0] = abb[14] * z[0];
z[0] = z[0] + z[1] + z[4];
z[0] = abb[13] * z[0];
z[1] = abb[21] + -abb[22] + -z[40] + -z[41] + z[72] + z[85];
z[1] = abb[3] * z[1];
z[4] = -abb[20] + abb[22];
z[21] = abb[26] + -z[4] + -z[17] + z[56] + -z[68];
z[21] = abb[0] * z[21];
z[24] = -abb[21] + z[18];
z[15] = z[15] + z[24] + -z[27];
z[15] = abb[4] * z[15];
z[26] = abb[19] * (T(-37) / T(2)) + abb[24] * (T(15) / T(2)) + -z[26] + -z[36] + z[45];
z[26] = abb[2] * z[26];
z[20] = -z[20] + -3 * z[55] + z[87];
z[20] = abb[5] * z[20];
z[27] = 13 * abb[24] + abb[19] * (T(-175) / T(4)) + abb[20] * (T(-37) / T(2)) + abb[21] * (T(13) / T(2)) + abb[22] * (T(15) / T(2)) + abb[23] * (T(39) / T(4)) + -z[8] + z[42];
z[27] = abb[1] * z[27];
z[1] = z[1] + -z[11] + z[15] + z[20] + z[21] + z[26] + z[27];
z[1] = abb[14] * z[1];
z[8] = -abb[22] + 15 * abb[24] + -z[8] + z[30] + -z[81] + z[83];
z[8] = abb[1] * z[8];
z[11] = abb[21] + abb[24];
z[15] = 2 * z[11];
z[20] = -z[5] + z[15] + -z[65];
z[21] = z[20] * z[80];
z[8] = z[8] + z[16] + z[21] + z[25] + z[37] + -z[61];
z[8] = abb[12] * z[8];
z[1] = z[1] + z[8];
z[1] = abb[14] * z[1];
z[8] = 15 * abb[19] + 6 * abb[20];
z[16] = 6 * abb[21] + -z[8] + z[43] + -z[67];
z[16] = abb[2] * z[16];
z[21] = -abb[26] + z[86];
z[21] = -15 * abb[20] + abb[19] * (T(-69) / T(2)) + abb[23] * (T(9) / T(2)) + 2 * z[21] + z[32] + z[69];
z[21] = abb[1] * z[21];
z[25] = abb[21] + -abb[26];
z[25] = abb[18] + abb[20] + z[17] + 2 * z[25] + z[63];
z[25] = abb[5] * z[25];
z[23] = 8 * abb[19] + abb[21] + -abb[23] + z[23] + -2 * z[40];
z[23] = abb[3] * z[23];
z[9] = z[9] + z[16] + z[21] + z[23] + z[25];
z[9] = abb[16] * z[9];
z[16] = -z[18] + -z[22] + z[44] + z[66] + z[70] + -z[73];
z[16] = abb[2] * z[16];
z[18] = abb[19] + -abb[21];
z[21] = -abb[25] + -z[18];
z[21] = 2 * z[21] + z[57] + -z[85];
z[21] = abb[4] * z[21];
z[22] = z[44] + z[58] + -z[84];
z[22] = -27 * abb[19] + -abb[22] + 2 * z[22] + -z[46] + z[70];
z[22] = abb[1] * z[22];
z[23] = z[76] + z[78];
z[23] = abb[0] * z[23];
z[16] = z[16] + z[21] + z[22] + z[23] + z[37];
z[16] = abb[15] * z[16];
z[3] = -z[3] + z[5] + -z[14] + -z[28] + z[84];
z[3] = abb[7] * z[3];
z[10] = abb[9] * z[10];
z[2] = z[2] + z[28];
z[2] = abb[8] * z[2];
z[14] = abb[0] + -abb[10];
z[14] = abb[2] * (T(-3) / T(2)) + abb[3] * (T(-1) / T(2)) + -2 * z[14];
z[14] = z[14] * z[50];
z[2] = z[2] + z[3] + z[10] + z[14];
z[2] = abb[17] * z[2];
z[3] = abb[1] * z[20];
z[5] = z[5] + -z[11] + z[79];
z[10] = z[5] + -z[39];
z[14] = abb[3] * z[10];
z[21] = z[18] * z[80];
z[22] = -z[11] + z[56];
z[23] = abb[22] + z[22];
z[25] = abb[0] * z[23];
z[3] = z[3] + z[14] + -z[21] + z[25] + -z[53];
z[14] = prod_pow(abb[12], 2) * z[3];
z[21] = -abb[0] + abb[2];
z[21] = (T(1) / T(3)) * z[21];
z[21] = z[18] * z[21];
z[17] = abb[18] * (T(1) / T(3)) + z[17];
z[25] = -abb[20] + -z[71];
z[25] = z[17] + (T(1) / T(3)) * z[25];
z[25] = abb[3] * z[25];
z[12] = z[12] + z[13];
z[12] = (T(1) / T(3)) * z[12] + -z[17];
z[12] = abb[1] * z[12];
z[12] = z[12] + z[21] + z[25];
z[12] = prod_pow(m1_set::bc<T>[0], 2) * z[12];
z[0] = z[0] + z[1] + z[2] + z[6] + z[9] + (T(13) / T(2)) * z[12] + z[14] + z[16];
z[1] = z[5] + z[77];
z[2] = abb[3] * z[1];
z[5] = abb[22] + z[15] + -z[31] + -z[64];
z[5] = abb[1] * z[5];
z[6] = z[19] + 2 * z[22];
z[6] = abb[4] * z[6];
z[4] = z[4] + -2 * z[18];
z[4] = abb[0] * z[4];
z[9] = -abb[2] * z[23];
z[2] = z[2] + z[4] + z[5] + z[6] + z[9] + -z[51] + -z[53];
z[2] = abb[11] * z[2];
z[3] = abb[12] * z[3];
z[2] = -z[2] + z[3];
z[3] = z[24] + -z[33] + z[52];
z[3] = abb[3] * z[3];
z[4] = -abb[2] * z[20];
z[5] = -abb[20] + -z[29] + -z[77];
z[5] = abb[0] * z[5];
z[3] = z[3] + z[4] + z[5] + -z[6];
z[4] = -z[8] + 4 * z[11] + z[74];
z[4] = abb[5] * z[4];
z[5] = -z[7] + -z[49];
z[5] = -8 * abb[22] + -13 * abb[23] + 2 * z[5] + z[82];
z[5] = abb[1] * z[5];
z[3] = 2 * z[3] + z[4] + z[5];
z[3] = abb[14] * z[3];
z[1] = abb[0] * z[1];
z[5] = -abb[2] * z[10];
z[6] = -abb[3] * z[18];
z[1] = z[1] + z[5] + z[6] + z[51];
z[5] = 4 * abb[22] + 7 * abb[23] + z[42] + -z[47] + 2 * z[75];
z[5] = abb[1] * z[5];
z[1] = 2 * z[1] + -z[4] + z[5];
z[1] = abb[13] * z[1];
z[1] = z[1] + -2 * z[2] + z[3];
z[1] = m1_set::bc<T>[0] * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_301_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("9.6869727601234358283756987829976414816079881477263330133943208816"),stof<T>("-2.8667105999216231323768332717132868542048063632126095444875766584")}, std::complex<T>{stof<T>("-25.856555001590378619684004962422420603372102035310749093182550313"),stof<T>("-42.549703591125178582638666935247148258370724703464229454776803416")}, std::complex<T>{stof<T>("-21.471028890262488347189538394090783668697347352365124194195707959"),stof<T>("-8.434513020933967869489707693248722641929644805176173144438742485")}, std::complex<T>{stof<T>("-15.056065082936897732565438289509070458019581811366603970591961529"),stof<T>("21.338189276854817069854796561884803997734446425208732431485261227")}, std::complex<T>{stof<T>("7.2830823809441120322744119467777639214316416933010791788360691047"),stof<T>("7.4547312427313645511769304781062696064412569082540215413965542349")}, std::complex<T>{stof<T>("11.824318326513162870815936029571420537511066726345582260558540464"),stof<T>("17.957852577759362911131639042911466331464393675020906272949191457")}, std::complex<T>{stof<T>("5.927315205003752743291317557982873964803253067245023778432961209"),stof<T>("3.8464923781242264506896104868557398896931942601347611475297649083")}, stof<T>("-8.1467639364098125700497265545092086142014652425291793387481963645"), stof<T>("6.7204224806010003132978366608417129150559296866218675622805531285"), std::complex<T>{stof<T>("3.853240554803298895013242339923315581368802731963455130837505218"),stof<T>("15.748931692217380713457583612152853300165062016478596541063558734")}, std::complex<T>{stof<T>("-3.853240554803298895013242339923315581368802731963455130837505218"),stof<T>("-15.748931692217380713457583612152853300165062016478596541063558734")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[36].real()/kbase.W[36].real()), rlog(k.W[55].real()/kbase.W[55].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[123].real()/kbase.W[123].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_301_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_301_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("16.246222683398221337481899270641592734781357406993114707473297208"),stof<T>("42.21463319085329400259538759613081516528425332553039970541482098")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dlog_W3(k,dl), dl[2], dl[4], dlog_W13(k,dl), dlog_W18(k,dl), dlog_W30(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[36].real()/k.W[36].real()), rlog(kend.W[55].real()/k.W[55].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[123].real()/k.W[123].real())};

                    
            return f_4_301_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_301_DLogXconstant_part(base_point<T>, kend);
	value += f_4_301_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_301_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_301_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_301_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_301_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_301_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_301_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
