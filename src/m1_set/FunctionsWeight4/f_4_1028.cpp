/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_1028.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_1028_abbreviated (const std::array<T,85>& abb) {
T z[93];
z[0] = 3 * abb[36];
z[1] = 3 * abb[37];
z[2] = z[0] + -z[1];
z[3] = 2 * abb[38];
z[4] = 4 * abb[31];
z[5] = z[3] + -z[4];
z[6] = abb[34] + -abb[35];
z[7] = z[2] + -z[5] + -z[6];
z[7] = abb[38] * z[7];
z[8] = 2 * abb[32];
z[9] = -z[4] + z[8];
z[2] = z[2] + z[6] + z[9];
z[2] = abb[32] * z[2];
z[10] = 2 * abb[36];
z[11] = abb[34] + abb[35];
z[12] = -4 * abb[37] + z[10] + -z[11];
z[12] = abb[36] * z[12];
z[13] = 2 * abb[37];
z[14] = -z[11] + z[13];
z[14] = abb[37] * z[14];
z[15] = abb[35] + abb[36];
z[16] = abb[34] + abb[37];
z[17] = z[15] + z[16];
z[18] = 2 * abb[31];
z[17] = z[17] * z[18];
z[19] = abb[36] + -abb[37];
z[20] = -z[6] + z[19];
z[21] = abb[33] * z[20];
z[22] = 2 * abb[42];
z[23] = z[21] + z[22];
z[24] = z[6] + z[19];
z[25] = abb[39] * z[24];
z[26] = 2 * abb[35];
z[27] = -abb[34] + z[26];
z[28] = abb[34] * z[27];
z[29] = 2 * abb[43];
z[30] = 4 * abb[69] + z[29];
z[31] = prod_pow(m1_set::bc<T>[0], 2);
z[32] = (T(10) / T(3)) * z[31];
z[33] = 2 * abb[44];
z[34] = 2 * abb[74];
z[35] = prod_pow(abb[35], 2);
z[2] = z[2] + -z[7] + z[12] + z[14] + z[17] + z[23] + -z[25] + -z[28] + z[30] + z[32] + z[33] + z[34] + z[35];
z[2] = abb[0] * z[2];
z[7] = (T(1) / T(2)) * z[21];
z[12] = 2 * abb[70];
z[14] = z[7] + -z[12];
z[17] = (T(1) / T(2)) * z[25];
z[28] = z[14] + -z[17];
z[36] = (T(1) / T(2)) * z[24];
z[36] = abb[38] * z[36];
z[37] = (T(1) / T(2)) * z[20];
z[37] = abb[32] * z[37];
z[38] = z[36] + -z[37];
z[39] = (T(1) / T(2)) * z[11];
z[40] = abb[37] * z[39];
z[41] = abb[44] + abb[74];
z[42] = z[40] + z[41];
z[43] = 2 * abb[41];
z[44] = 2 * abb[73];
z[45] = z[43] + -z[44];
z[46] = abb[31] + -abb[36];
z[47] = -abb[37] + z[46];
z[47] = abb[31] * z[47];
z[48] = abb[34] * (T(3) / T(2)) + -z[26];
z[48] = abb[34] * z[48];
z[49] = abb[36] * z[39];
z[47] = z[28] + -z[31] + (T(3) / T(2)) * z[35] + z[38] + -z[42] + z[45] + -z[47] + z[48] + -z[49];
z[48] = abb[10] * z[47];
z[49] = abb[80] + abb[82];
z[48] = z[48] + (T(1) / T(2)) * z[49];
z[49] = abb[69] + -abb[72];
z[50] = -abb[38] + z[10];
z[51] = abb[31] + -abb[37] + z[50];
z[51] = abb[38] * z[51];
z[52] = abb[31] + z[19];
z[52] = abb[36] * z[52];
z[51] = abb[43] + z[49] + -z[51] + z[52];
z[52] = 4 * abb[16];
z[51] = z[51] * z[52];
z[51] = -z[48] + z[51];
z[53] = -abb[32] + z[13];
z[54] = z[46] + z[53];
z[54] = abb[32] * z[54];
z[46] = -abb[37] * z[46];
z[55] = prod_pow(abb[37], 2);
z[55] = abb[42] + z[55];
z[46] = abb[71] + z[46] + z[54] + -z[55];
z[54] = 4 * abb[15];
z[46] = z[46] * z[54];
z[2] = z[2] + -z[46] + z[51];
z[56] = z[17] + z[44];
z[57] = -abb[31] + z[10];
z[57] = abb[31] * z[57];
z[57] = -z[7] + z[57];
z[58] = -abb[35] + abb[34] * (T(1) / T(2));
z[58] = abb[34] * z[58];
z[58] = (T(1) / T(2)) * z[35] + z[58];
z[59] = (T(4) / T(3)) * z[31] + z[58];
z[60] = 4 * abb[72];
z[30] = z[30] + -z[60];
z[61] = -abb[37] + -z[10] + z[39];
z[61] = abb[36] * z[61];
z[62] = -abb[37] + z[6];
z[63] = 7 * abb[36];
z[64] = -z[62] + z[63];
z[64] = -z[3] + (T(1) / T(2)) * z[64];
z[64] = abb[38] * z[64];
z[42] = -z[30] + -z[37] + -z[42] + -z[56] + z[57] + z[59] + z[61] + z[64];
z[42] = abb[9] * z[42];
z[61] = z[0] + -z[3] + -z[62];
z[61] = abb[38] * z[61];
z[62] = z[18] * z[20];
z[64] = abb[34] * abb[37];
z[64] = z[25] + z[64];
z[65] = 4 * abb[73] + z[64];
z[66] = abb[34] * z[6];
z[34] = z[34] + -z[66];
z[67] = (T(16) / T(3)) * z[31] + z[34];
z[68] = abb[35] + abb[37];
z[69] = abb[36] + z[68];
z[69] = abb[36] * z[69];
z[30] = 2 * z[21] + -z[30] + z[61] + -z[62] + z[65] + z[67] + -z[69];
z[30] = abb[14] * z[30];
z[61] = (T(1) / T(3)) * z[31] + -z[35] + -z[66];
z[62] = abb[38] * z[24];
z[66] = -abb[35] + abb[37];
z[69] = abb[36] * z[66];
z[69] = z[62] + z[69];
z[45] = z[45] + -z[61] + -z[64] + z[69];
z[45] = abb[7] * z[45];
z[70] = z[12] + -z[21];
z[71] = abb[34] * abb[36];
z[72] = abb[31] * z[19];
z[64] = -z[44] + -z[64] + z[70] + z[71] + z[72];
z[71] = abb[18] * z[64];
z[73] = z[45] + -z[71];
z[30] = z[30] + -z[73];
z[74] = 5 * abb[36];
z[75] = 5 * abb[37];
z[76] = z[74] + -z[75];
z[77] = -z[6] + z[9] + z[76];
z[77] = abb[32] * z[77];
z[78] = abb[34] * abb[35];
z[33] = z[33] + -z[35] + z[78];
z[27] = -abb[36] + z[1] + z[27];
z[27] = abb[36] * z[27];
z[78] = 2 * z[19];
z[79] = -abb[31] + z[78];
z[79] = z[18] * z[79];
z[80] = abb[35] + z[13];
z[80] = abb[37] * z[80];
z[27] = -z[23] + z[27] + -z[33] + -z[77] + z[79] + -z[80];
z[27] = abb[1] * z[27];
z[72] = -z[43] + -z[62] + z[72];
z[77] = abb[32] * z[20];
z[79] = abb[35] * abb[37];
z[79] = z[77] + z[79];
z[80] = abb[35] * abb[36];
z[80] = z[72] + -z[79] + z[80];
z[80] = abb[17] * z[80];
z[27] = z[27] + -z[80];
z[81] = 2 * abb[34];
z[82] = -abb[35] + z[19] + z[81];
z[82] = abb[36] * z[82];
z[32] = z[32] + z[34];
z[34] = -z[32] + -z[62] + -z[65] + z[82];
z[34] = abb[3] * z[34];
z[65] = abb[76] + abb[79];
z[82] = abb[77] + abb[78];
z[83] = 3 * z[65] + z[82];
z[42] = -z[2] + -z[27] + z[30] + -z[34] + z[42] + (T(1) / T(2)) * z[83];
z[42] = abb[63] * z[42];
z[83] = -abb[36] + z[6];
z[84] = -z[1] + z[8] + z[83];
z[84] = abb[32] * z[84];
z[85] = z[18] * z[24];
z[68] = abb[37] * z[68];
z[86] = 4 * abb[70];
z[87] = 4 * abb[71];
z[88] = -abb[34] + abb[37];
z[89] = abb[36] * z[88];
z[23] = z[23] + 2 * z[25] + -z[67] + z[68] + z[84] + -z[85] + -z[86] + -z[87] + z[89];
z[23] = abb[13] * z[23];
z[58] = (T(20) / T(3)) * z[31] + z[41] + 3 * z[58] + z[60] + z[87];
z[60] = (T(3) / T(2)) * z[25];
z[67] = -z[39] + -z[75];
z[67] = abb[36] * z[67];
z[68] = abb[35] + z[10];
z[84] = z[16] + z[68];
z[84] = -abb[31] + 2 * z[84];
z[84] = abb[31] * z[84];
z[85] = 7 * abb[37];
z[90] = abb[36] + z[6] + z[85];
z[90] = -z[4] + (T(1) / T(2)) * z[90];
z[90] = abb[38] * z[90];
z[91] = abb[37] + z[6];
z[74] = z[74] + 3 * z[91];
z[74] = -z[4] + (T(1) / T(2)) * z[74];
z[74] = abb[32] * z[74];
z[11] = (T(3) / T(2)) * z[11];
z[92] = -abb[37] * z[11];
z[7] = z[7] + -z[44] + z[58] + -z[60] + z[67] + z[74] + z[84] + z[90] + z[92];
z[7] = abb[9] * z[7];
z[44] = 4 * abb[41] + -z[33];
z[67] = -abb[32] + z[18] + -z[24];
z[67] = z[8] * z[67];
z[24] = -abb[31] + z[24];
z[24] = z[18] * z[24];
z[74] = abb[37] * z[88];
z[24] = z[22] + -z[24] + z[25] + -z[44] + -z[67] + -z[69] + z[74];
z[24] = abb[11] * z[24];
z[67] = z[5] + -z[6] + -z[76];
z[67] = abb[38] * z[67];
z[1] = -z[1] + z[68];
z[1] = abb[36] * z[1];
z[68] = abb[31] + z[78];
z[68] = z[18] * z[68];
z[26] = -z[16] + z[26];
z[26] = abb[37] * z[26];
z[1] = z[1] + -z[25] + -z[26] + z[29] + -z[44] + z[67] + z[68];
z[1] = abb[2] * z[1];
z[26] = z[21] + z[77];
z[44] = z[66] + z[81];
z[44] = abb[37] * z[44];
z[16] = abb[36] * z[16];
z[16] = -z[16] + z[26] + -z[32] + z[44] + -z[86];
z[16] = abb[4] * z[16];
z[32] = z[61] + z[70];
z[44] = z[32] + z[79] + -z[89];
z[61] = abb[8] * z[44];
z[16] = z[16] + -z[61];
z[53] = abb[32] * z[53];
z[53] = 2 * abb[71] + z[31] + z[53] + -z[55];
z[55] = 4 * abb[19];
z[67] = -z[53] * z[55];
z[65] = abb[77] + 3 * abb[78] + z[65];
z[2] = z[1] + -z[2] + z[7] + -z[16] + -z[23] + z[24] + -z[45] + (T(1) / T(2)) * z[65] + z[67] + -z[71] + -z[80];
z[2] = abb[64] * z[2];
z[7] = 3 * z[6];
z[45] = z[7] + z[76];
z[9] = z[9] + (T(1) / T(2)) * z[45];
z[9] = abb[32] * z[9];
z[22] = z[22] + -z[36];
z[36] = prod_pow(abb[34], 2);
z[35] = -z[35] + z[36];
z[35] = abb[44] + -abb[74] + (T(1) / T(2)) * z[35];
z[36] = (T(1) / T(2)) * z[6];
z[45] = -z[13] + -z[36];
z[45] = abb[36] * z[45];
z[65] = -abb[36] + z[13];
z[67] = -z[6] + z[65];
z[68] = abb[31] + 2 * z[67];
z[68] = abb[31] * z[68];
z[69] = (T(2) / T(3)) * z[31];
z[70] = abb[37] + -z[36];
z[70] = abb[37] * z[70];
z[9] = z[9] + z[14] + z[22] + z[35] + -z[43] + z[45] + z[60] + z[68] + -z[69] + z[70];
z[9] = abb[65] * z[9];
z[0] = z[0] + z[7] + z[75];
z[0] = (T(1) / T(2)) * z[0] + -z[4];
z[0] = abb[38] * z[0];
z[45] = (T(3) / T(2)) * z[21];
z[11] = -z[11] + -z[75];
z[11] = abb[36] * z[11];
z[15] = abb[34] + z[13] + z[15];
z[15] = -abb[31] + 2 * z[15];
z[15] = abb[31] * z[15];
z[60] = z[63] + z[91];
z[4] = -z[4] + (T(1) / T(2)) * z[60];
z[4] = abb[32] * z[4];
z[0] = z[0] + z[4] + z[11] + -z[12] + z[15] + -z[17] + -z[40] + z[43] + z[45] + z[58];
z[0] = abb[63] * z[0];
z[4] = -abb[31] + z[13];
z[4] = abb[31] * z[4];
z[4] = z[4] + z[43];
z[11] = -z[13] + z[39];
z[11] = abb[37] * z[11];
z[12] = -z[83] + z[85];
z[8] = -z[8] + (T(1) / T(2)) * z[12];
z[8] = abb[32] * z[8];
z[12] = -abb[37] + -z[39];
z[12] = abb[36] * z[12];
z[8] = z[4] + z[8] + z[11] + z[12] + z[14] + z[17] + -z[22] + -z[41] + z[59] + z[87];
z[8] = abb[64] * z[8];
z[11] = (T(8) / T(3)) * z[31] + z[38];
z[12] = (T(3) / T(2)) * z[6];
z[13] = -abb[37] + -z[12];
z[13] = abb[37] * z[13];
z[14] = abb[36] * z[36];
z[4] = z[4] + z[11] + z[13] + z[14] + -z[28] + -z[35];
z[4] = abb[66] * z[4];
z[0] = z[0] + z[4] + z[8] + z[9];
z[0] = abb[6] * z[0];
z[4] = z[6] * z[18];
z[6] = abb[36] * z[67];
z[8] = abb[37] * z[91];
z[4] = -z[4] + z[6] + -z[8] + z[25] + -z[26] + z[62];
z[4] = abb[0] * z[4];
z[6] = abb[37] * z[36];
z[6] = z[6] + -z[35] + z[56];
z[8] = -abb[36] + -z[12];
z[8] = abb[36] * z[8];
z[8] = z[6] + z[8] + z[11] + z[57];
z[8] = abb[9] * z[8];
z[9] = -abb[76] + abb[79];
z[11] = -z[9] + -z[82];
z[8] = z[4] + z[8] + (T(1) / T(2)) * z[11] + -z[23] + -z[24] + -z[27] + z[34] + z[46] + z[48] + z[73];
z[8] = abb[65] * z[8];
z[7] = z[7] + -z[76];
z[5] = z[5] + (T(1) / T(2)) * z[7];
z[5] = abb[38] * z[5];
z[7] = -z[36] + -z[65];
z[7] = abb[36] * z[7];
z[10] = z[10] + -z[91];
z[10] = abb[31] + 2 * z[10];
z[10] = abb[31] * z[10];
z[5] = z[5] + -z[6] + z[7] + z[10] + z[29] + z[37] + -z[45] + -z[69];
z[5] = abb[9] * z[5];
z[6] = -abb[77] + abb[78] + z[9];
z[1] = z[1] + z[4] + z[5] + (T(1) / T(2)) * z[6] + z[16] + z[30] + -z[51] + -z[80];
z[1] = abb[66] * z[1];
z[4] = abb[67] * z[47];
z[5] = 2 * abb[40];
z[6] = abb[67] * z[5];
z[4] = z[4] + z[6];
z[7] = abb[25] + abb[27];
z[4] = z[4] * z[7];
z[9] = -z[5] + -z[47];
z[10] = -abb[63] + abb[66];
z[11] = abb[64] + -abb[65];
z[12] = z[10] + -z[11];
z[13] = abb[5] * z[12];
z[9] = z[9] * z[13];
z[14] = -abb[38] + z[18] + z[20];
z[3] = z[3] * z[14];
z[14] = abb[31] + z[20];
z[14] = z[14] * z[18];
z[15] = -abb[34] + z[19];
z[15] = abb[36] * z[15];
z[3] = 4 * abb[40] + z[3] + -z[14] + -z[15] + z[21] + -z[29] + -z[33] + -z[79];
z[3] = abb[12] * z[3] * z[10];
z[14] = abb[29] * z[64];
z[15] = -abb[34] + z[66];
z[15] = abb[36] * z[15];
z[15] = z[15] + -z[32] + -z[72];
z[15] = abb[28] * z[15];
z[16] = abb[61] + abb[62];
z[14] = z[14] + z[15] + (T(-1) / T(2)) * z[16];
z[14] = abb[67] * z[14];
z[15] = abb[64] + abb[65];
z[16] = abb[21] + abb[24];
z[15] = z[15] * z[16];
z[16] = abb[63] + abb[66];
z[17] = abb[21] + -abb[24];
z[17] = z[16] * z[17];
z[11] = -z[10] + -z[11];
z[11] = abb[23] * z[11];
z[18] = -abb[22] * z[12];
z[19] = -abb[30] * abb[67];
z[11] = z[11] + z[15] + z[17] + z[18] + z[19];
z[11] = abb[45] * z[11];
z[15] = 2 * abb[19];
z[17] = -z[15] * z[53];
z[17] = z[17] + z[61];
z[18] = abb[63] + abb[65];
z[17] = z[17] * z[18];
z[19] = abb[13] + abb[15];
z[15] = -abb[0] + -abb[6] + z[15] + -z[19];
z[15] = abb[64] * z[15];
z[20] = abb[0] + abb[15];
z[20] = abb[63] * z[20];
z[19] = abb[65] * z[19];
z[21] = abb[19] * z[18];
z[15] = z[15] + -z[19] + -z[20] + z[21];
z[15] = 4 * z[15];
z[19] = abb[68] * z[15];
z[20] = abb[38] * z[50];
z[21] = prod_pow(abb[36], 2);
z[20] = -abb[43] + z[20] + -z[21] + z[31] + -2 * z[49];
z[21] = 2 * abb[63] + abb[64] + abb[66];
z[21] = abb[20] * z[21];
z[20] = -z[20] * z[21];
z[22] = -abb[8] + abb[9] + abb[17];
z[23] = -2 * abb[1] + abb[10] + z[22];
z[23] = z[18] * z[23];
z[22] = abb[10] + -z[22];
z[22] = abb[66] * z[22];
z[24] = -abb[8] + abb[9] + abb[10] + -abb[17];
z[24] = abb[64] * z[24];
z[22] = z[22] + z[23] + z[24];
z[5] = z[5] * z[22];
z[22] = -abb[67] * z[44];
z[6] = z[6] + z[22];
z[6] = abb[26] * z[6];
z[22] = 3 * abb[64];
z[10] = abb[65] + -z[10] + z[22];
z[10] = abb[81] * z[10];
z[12] = (T(1) / T(2)) * z[12];
z[23] = -abb[75] * z[12];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[8] + z[9] + (T(1) / T(2)) * z[10] + z[11] + z[14] + z[17] + z[19] + 2 * z[20] + z[23] + z[42];
z[1] = 2 * m1_set::bc<T>[0];
z[2] = abb[32] * z[1];
z[3] = abb[36] * z[1];
z[4] = 2 * abb[48];
z[5] = abb[52] + z[4];
z[2] = -2 * abb[49] + -z[2] + z[3] + -z[5];
z[6] = 2 * abb[13];
z[2] = z[2] * z[6];
z[6] = abb[10] + abb[18];
z[6] = z[4] * z[6];
z[8] = abb[34] * z[1];
z[9] = abb[31] * z[1];
z[8] = z[8] + -z[9];
z[10] = 2 * abb[51];
z[11] = abb[52] + z[10];
z[14] = z[8] + z[11];
z[17] = abb[10] * z[14];
z[2] = z[2] + z[6] + z[17];
z[6] = abb[37] * m1_set::bc<T>[0];
z[19] = abb[36] * m1_set::bc<T>[0];
z[20] = z[6] + -z[19];
z[23] = abb[38] * m1_set::bc<T>[0];
z[23] = abb[50] + z[23];
z[24] = -abb[47] + z[23];
z[25] = abb[31] * m1_set::bc<T>[0];
z[26] = z[20] + z[24] + -z[25];
z[26] = z[26] * z[52];
z[27] = abb[32] * m1_set::bc<T>[0];
z[27] = abb[49] + z[27];
z[28] = -z[20] + -z[25] + z[27];
z[28] = z[28] * z[54];
z[29] = 2 * abb[47];
z[8] = abb[52] + -z[8] + z[29];
z[30] = 2 * abb[0];
z[8] = z[8] * z[30];
z[8] = z[8] + -z[26] + -z[28];
z[23] = 4 * z[23];
z[30] = 4 * z[27];
z[31] = abb[34] * m1_set::bc<T>[0];
z[25] = -abb[52] + -z[23] + -6 * z[25] + -z[30] + 4 * z[31];
z[32] = 6 * z[19];
z[10] = -4 * z[6] + -z[10] + -z[25] + -z[32];
z[10] = abb[9] * z[10];
z[33] = abb[48] + -z[19] + z[31];
z[34] = 2 * abb[8];
z[34] = z[33] * z[34];
z[35] = abb[4] * z[5];
z[35] = z[34] + 2 * z[35];
z[31] = abb[51] + -z[6] + z[31];
z[36] = 2 * abb[7];
z[31] = z[31] * z[36];
z[20] = -abb[51] + z[20];
z[20] = 2 * z[20];
z[36] = abb[18] * z[20];
z[37] = abb[54] + abb[57];
z[38] = abb[58] + abb[60];
z[39] = abb[53] + 3 * abb[59] + z[37] + z[38];
z[27] = -z[6] + z[27];
z[40] = abb[19] * z[27];
z[10] = -z[2] + -z[8] + z[10] + z[31] + z[35] + -z[36] + (T(1) / T(2)) * z[39] + -8 * z[40];
z[10] = abb[64] * z[10];
z[39] = abb[38] * z[1];
z[1] = abb[37] * z[1];
z[29] = 2 * abb[50] + -z[1] + z[11] + -z[29] + z[39];
z[39] = 2 * abb[14];
z[29] = z[29] * z[39];
z[31] = z[31] + z[36];
z[36] = abb[10] + -abb[18];
z[36] = z[4] * z[36];
z[29] = z[29] + z[31] + -z[36];
z[36] = abb[54] + -abb[57];
z[39] = -abb[53] + -abb[59] + -z[36] + z[38];
z[40] = -z[9] + z[11];
z[41] = -z[3] + -z[40];
z[41] = abb[9] * z[41];
z[26] = -z[17] + z[26] + z[29] + -z[35] + (T(1) / T(2)) * z[39] + z[41];
z[26] = abb[66] * z[26];
z[3] = -z[3] + z[9] + z[11];
z[3] = abb[9] * z[3];
z[35] = abb[59] + z[38];
z[36] = -abb[53] + z[35] + z[36];
z[11] = abb[3] * z[11];
z[11] = 2 * z[11];
z[2] = -z[2] + z[3] + -z[11] + z[28] + -z[31] + (T(1) / T(2)) * z[36];
z[2] = abb[65] * z[2];
z[3] = -4 * abb[47] + z[23] + -z[32] + -z[40];
z[3] = abb[9] * z[3];
z[23] = abb[53] + z[35] + 3 * z[37];
z[3] = z[3] + -z[8] + z[11] + -z[17] + (T(1) / T(2)) * z[23] + z[29];
z[3] = abb[63] * z[3];
z[6] = z[4] + 6 * z[6];
z[8] = -z[6] + -4 * z[19] + -z[25];
z[8] = abb[63] * z[8];
z[11] = -abb[52] + z[9];
z[17] = -z[1] + -z[4] + z[11];
z[17] = abb[65] * z[17];
z[1] = -z[1] + z[5] + z[9];
z[1] = abb[66] * z[1];
z[5] = -z[6] + z[11] + z[30];
z[5] = abb[64] * z[5];
z[1] = z[1] + z[5] + z[8] + z[17];
z[1] = abb[6] * z[1];
z[5] = abb[46] * z[15];
z[6] = -z[27] * z[55];
z[6] = z[6] + z[34];
z[6] = z[6] * z[18];
z[8] = -abb[26] + -abb[28];
z[9] = 2 * abb[67];
z[8] = z[8] * z[9] * z[33];
z[9] = z[4] + z[14];
z[7] = -z[7] * z[9];
z[4] = z[4] + z[20];
z[4] = abb[29] * z[4];
z[11] = abb[83] + abb[84];
z[4] = z[4] + z[7] + (T(1) / T(2)) * z[11];
z[4] = abb[67] * z[4];
z[7] = z[9] * z[13];
z[9] = -abb[65] + z[16] + z[22];
z[9] = abb[56] * z[9];
z[11] = -abb[55] * z[12];
z[12] = -z[19] + z[24];
z[12] = -z[12] * z[21];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + (T(1) / T(2)) * z[9] + z[10] + z[11] + 4 * z[12] + z[26];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_1028_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-20.742632909782878748124859918116438910629673859667542553551291571"),stof<T>("77.746052280784268507343458837796503842537483665982655291255190516")}, std::complex<T>{stof<T>("-20.742632909782878748124859918116438910629673859667542553551291571"),stof<T>("77.746052280784268507343458837796503842537483665982655291255190516")}, std::complex<T>{stof<T>("-1.5878653822997918967911179085265562738209800160562444877505438544"),stof<T>("-3.0775302532745012710375701816904739530658043246269021620678773304")}, std::complex<T>{stof<T>("-1.5878653822997918967911179085265562738209800160562444877505438544"),stof<T>("-3.0775302532745012710375701816904739530658043246269021620678773304")}, std::complex<T>{stof<T>("12.097305882114827542032612717488351764077172829486174779259610837"),stof<T>("-9.585923934639867303669713633499836524562927947608141683407252985")}};
	
	std::vector<C> intdlogs = {rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[124].real()/kbase.W[124].real()), rlog(k.W[127].real()/kbase.W[127].real()), C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_1028_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_1028_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-41.7243699904255161358645893334985969227387645901938714657232615"),stof<T>("46.263905952378121967455625142503109056607618922210736841519066161")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({198, 202});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,85> abb = {dl[0], dl[1], dlog_W3(k,dl), dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W14(k,dl), dlog_W15(k,dl), dlog_W24(k,dl), dlog_W26(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W64(k,dl), dlog_W68(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_5(k), f_2_8(k), f_2_18(k), f_2_19(k), f_2_21(k), f_2_23(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_9_im(k), f_2_11_im(k), f_2_12_im(k), f_2_22_im(k), T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[124].real()/k.W[124].real()), rlog(kend.W[127].real()/k.W[127].real()), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_9_re(k), f_2_11_re(k), f_2_12_re(k), f_2_22_re(k), T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}};
{
auto c = dlog_W162(k,dv) * f_2_30(k);
abb[75] = c.real();
abb[53] = c.imag();
SpDLog_Sigma5<T,198,161>(k, dv, abb[75], abb[53], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W164(k,dv) * f_2_30(k);
abb[76] = c.real();
abb[54] = c.imag();
SpDLog_Sigma5<T,198,163>(k, dv, abb[76], abb[54], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W169(k,dv) * f_2_34(k);
abb[77] = c.real();
abb[55] = c.imag();
SpDLog_Sigma5<T,202,168>(k, dv, abb[77], abb[55], f_2_34_series_coefficients<T>);
}
{
auto c = dlog_W172(k,dv) * f_2_34(k);
abb[78] = c.real();
abb[56] = c.imag();
SpDLog_Sigma5<T,202,171>(k, dv, abb[78], abb[56], f_2_34_series_coefficients<T>);
}
{
auto c = dlog_W175(k,dv) * f_2_34(k);
abb[79] = c.real();
abb[57] = c.imag();
SpDLog_Sigma5<T,202,174>(k, dv, abb[79], abb[57], f_2_34_series_coefficients<T>);
}
{
auto c = dlog_W178(k,dv) * f_2_34(k);
abb[80] = c.real();
abb[58] = c.imag();
SpDLog_Sigma5<T,202,177>(k, dv, abb[80], abb[58], f_2_34_series_coefficients<T>);
}
{
auto c = dlog_W183(k,dv) * f_2_30(k);
abb[81] = c.real();
abb[59] = c.imag();
SpDLog_Sigma5<T,198,182>(k, dv, abb[81], abb[59], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W185(k,dv) * f_2_30(k);
abb[82] = c.real();
abb[60] = c.imag();
SpDLog_Sigma5<T,198,184>(k, dv, abb[82], abb[60], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W189(k,dv) * f_2_30(k);
abb[83] = c.real();
abb[61] = c.imag();
SpDLog_Sigma5<T,198,188>(k, dv, abb[83], abb[61], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W193(k,dv) * f_2_34(k);
abb[84] = c.real();
abb[62] = c.imag();
SpDLog_Sigma5<T,202,192>(k, dv, abb[84], abb[62], f_2_34_series_coefficients<T>);
}

                    
            return f_4_1028_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_1028_DLogXconstant_part(base_point<T>, kend);
	value += f_4_1028_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_1028_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_1028_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_1028_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_1028_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_1028_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_1028_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
