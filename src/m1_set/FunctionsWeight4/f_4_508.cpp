/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_508.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_508_abbreviated (const std::array<T,29>& abb) {
T z[29];
z[0] = 3 * abb[20];
z[1] = -abb[21] + abb[23];
z[2] = z[0] + -z[1];
z[3] = 2 * abb[19];
z[4] = 2 * abb[22];
z[5] = z[3] + -z[4];
z[6] = z[2] + -z[5];
z[7] = abb[6] * z[6];
z[8] = 7 * abb[20] + -5 * z[1] + -z[5];
z[8] = abb[1] * z[8];
z[9] = -5 * abb[20] + 3 * z[1];
z[10] = z[5] + z[9];
z[11] = abb[2] * z[10];
z[12] = 2 * abb[0];
z[13] = -abb[20] + z[1];
z[14] = z[12] * z[13];
z[8] = -z[7] + z[8] + -z[11] + z[14];
z[11] = abb[14] * z[8];
z[14] = abb[19] * (T(1) / T(2));
z[15] = -z[0] + (T(5) / T(2)) * z[1] + z[14];
z[15] = -abb[22] + (T(1) / T(2)) * z[15];
z[15] = abb[0] * z[15];
z[10] = abb[1] * z[10];
z[16] = -abb[19] + abb[22];
z[17] = (T(1) / T(2)) * z[2] + z[16];
z[18] = abb[6] * z[17];
z[15] = -z[10] + z[15] + -z[18];
z[14] = -abb[20] + (T(1) / T(2)) * z[1] + z[14];
z[19] = abb[3] * z[14];
z[20] = abb[24] + abb[25];
z[21] = abb[7] * z[20];
z[22] = (T(3) / T(2)) * z[19] + (T(3) / T(4)) * z[21];
z[2] = abb[22] + z[2] + -z[3];
z[2] = abb[2] * z[2];
z[3] = abb[8] * z[20];
z[3] = (T(1) / T(2)) * z[3];
z[2] = z[2] + z[3] + z[15] + z[22];
z[23] = -abb[11] * z[2];
z[9] = (T(1) / T(2)) * z[9] + -z[16];
z[9] = abb[1] * z[9];
z[24] = (T(3) / T(2)) * z[14];
z[25] = -abb[22] + z[24];
z[25] = abb[0] * z[25];
z[26] = abb[19] + -abb[20];
z[27] = abb[2] * z[26];
z[22] = -z[9] + z[22] + z[25] + (T(-3) / T(2)) * z[27];
z[22] = abb[12] * z[22];
z[22] = z[11] + z[22] + z[23];
z[22] = abb[12] * z[22];
z[23] = -abb[19] + z[1];
z[25] = z[4] + -z[23];
z[25] = abb[4] * z[25];
z[27] = abb[2] * z[17];
z[28] = abb[0] * z[23];
z[9] = -z[9] + -z[18] + z[25] + z[27] + z[28];
z[9] = prod_pow(abb[11], 2) * z[9];
z[18] = -abb[11] * z[8];
z[5] = -abb[20] + -z[1] + z[5];
z[5] = abb[1] * z[5];
z[26] = z[4] + z[26];
z[26] = abb[0] * z[26];
z[27] = -abb[2] * z[13];
z[5] = z[5] + -z[25] + z[26] + z[27];
z[5] = abb[14] * z[5];
z[5] = z[5] + z[18];
z[5] = abb[14] * z[5];
z[8] = -abb[15] * z[8];
z[0] = -z[0] + 2 * z[1];
z[18] = z[0] + -z[16];
z[18] = abb[2] * z[18];
z[15] = z[3] + -z[15] + z[18] + (T(1) / T(2)) * z[19] + (T(1) / T(4)) * z[21];
z[18] = -abb[11] + abb[12];
z[18] = z[15] * z[18];
z[19] = -abb[0] + abb[2];
z[14] = z[14] * z[19];
z[13] = abb[1] * z[13];
z[3] = z[3] + z[13] + z[14];
z[3] = abb[13] * z[3];
z[3] = z[3] + z[18];
z[3] = abb[13] * z[3];
z[6] = abb[2] * z[6];
z[6] = z[6] + -z[7];
z[0] = abb[19] + z[0];
z[4] = z[0] + z[4];
z[4] = abb[0] * z[4];
z[0] = 4 * abb[22] + -z[0];
z[0] = abb[4] * z[0];
z[0] = z[0] + z[4] + z[6] + -3 * z[13];
z[4] = -abb[26] * z[0];
z[7] = abb[2] + -abb[10] + abb[0] * (T(1) / T(4)) + abb[3] * (T(3) / T(4));
z[7] = z[7] * z[20];
z[13] = abb[9] * z[17];
z[14] = abb[7] * z[24];
z[17] = -abb[22] + (T(1) / T(2)) * z[23];
z[17] = abb[8] * z[17];
z[7] = z[7] + z[13] + z[14] + -z[17];
z[13] = abb[27] * z[7];
z[14] = abb[20] + (T(-1) / T(3)) * z[1] + (T(2) / T(3)) * z[16];
z[17] = -abb[2] + abb[6];
z[14] = z[14] * z[17];
z[16] = abb[20] * (T(-11) / T(6)) + z[1] + (T(-5) / T(6)) * z[16];
z[16] = abb[1] * z[16];
z[1] = abb[19] * (T(-7) / T(6)) + abb[20] * (T(1) / T(2)) + (T(2) / T(3)) * z[1];
z[17] = abb[22] * (T(-4) / T(3)) + z[1];
z[17] = abb[4] * z[17];
z[1] = abb[22] * (T(1) / T(6)) + -z[1];
z[1] = abb[0] * z[1];
z[1] = z[1] + z[14] + z[16] + z[17];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[1] = abb[28] + z[1] + z[3] + z[4] + z[5] + z[8] + z[9] + z[13] + z[22];
z[3] = -z[12] * z[23];
z[3] = z[3] + -z[6] + z[10] + -2 * z[25];
z[3] = abb[11] * z[3];
z[4] = abb[13] * z[15];
z[2] = abb[12] * z[2];
z[2] = z[2] + z[3] + z[4] + z[11];
z[2] = m1_set::bc<T>[0] * z[2];
z[0] = -abb[16] * z[0];
z[3] = abb[17] * z[7];
z[0] = abb[18] + z[0] + z[2] + z[3];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_508_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-12.558186494602971382200965946342030939130564300585294968161611309"),stof<T>("-8.26351341246424653290877863834076519158569300260248379531480413")}, std::complex<T>{stof<T>("9.1233540961639644338354954050150142031825060194898608275712759429"),stof<T>("-0.4047561765982272097096049329857532079006470536313039722151089313")}, std::complex<T>{stof<T>("-3.4348323984390069483654705413270167359480582810954341405903353663"),stof<T>("-8.6682695890624737426183835713265183994863400562337877675299130609")}, std::complex<T>{stof<T>("-11.1350869877497077702822775913128952547002977612535560471986031353"),stof<T>("0.8043098821654289468078014932187410919523605298667199406899901851")}, std::complex<T>{stof<T>("3.4348323984390069483654705413270167359480582810954341405903353663"),stof<T>("8.6682695890624737426183835713265183994863400562337877675299130609")}, std::complex<T>{stof<T>("-0.87967591672810295777218924617445642026936795132549591364655243886"),stof<T>("0.64194487762304279435700210128213717713839885131132720351431713993")}, std::complex<T>{stof<T>("-0.87967591672810295777218924617445642026936795132549591364655243886"),stof<T>("0.64194487762304279435700210128213717713839885131132720351431713993")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[124].real()/kbase.W[124].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_508_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_508_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (16 + 13 * v[0] + 3 * v[1] + v[2] + -7 * v[3] + -8 * v[4] + -4 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -v[5]) * (-v[0] + v[1] + -v[2] + -v[3] + v[5])) / prod_pow(tend, 2);
c[1] = ((-1 + 2 * m1_set::bc<T>[1]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[20] + abb[21] + -abb[23]) * (t * c[0] + c[1]);
	}
	{
T z[4];
z[0] = abb[20] + abb[21] + -abb[23];
z[0] = abb[5] * z[0];
z[1] = -abb[11] + abb[12];
z[1] = z[0] * z[1];
z[1] = 2 * z[1];
z[2] = abb[13] * z[0];
z[2] = z[1] + z[2];
z[2] = abb[13] * z[2];
z[3] = -abb[14] * z[0];
z[1] = -z[1] + z[3];
z[1] = abb[14] * z[1];
z[0] = abb[15] * z[0];
return 2 * z[0] + z[1] + z[2];
}

}
template <typename T, typename TABB> T SpDLog_f_4_508_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[20] + abb[21] + -abb[23]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[13] + -abb[14];
z[1] = abb[20] + abb[21] + -abb[23];
return 2 * abb[5] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_508_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-6.859495604298487627245159647940849272023269126093753903584351445"),stof<T>("27.11799495240823726506638436624471203439663450196727558749723845")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dlog_W4(k,dl), dlog_W7(k,dl), dlog_W10(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W29(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlr[2], f_1_1(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_15(k), f_2_2_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[124].real()/k.W[124].real()), f_2_2_re(k), f_2_25_re(k), T{0}};
abb[18] = SpDLog_f_4_508_W_17_Im(t, path, abb);
abb[28] = SpDLog_f_4_508_W_17_Re(t, path, abb);

                    
            return f_4_508_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_508_DLogXconstant_part(base_point<T>, kend);
	value += f_4_508_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_508_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_508_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_508_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_508_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_508_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_508_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
