/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_115.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_115_abbreviated (const std::array<T,60>& abb) {
T z[103];
z[0] = abb[35] * (T(1) / T(2));
z[1] = abb[34] * (T(1) / T(2));
z[2] = z[0] + z[1];
z[3] = -abb[32] + z[2];
z[4] = abb[15] * z[3];
z[5] = abb[33] + -abb[36];
z[6] = abb[5] * (T(1) / T(2));
z[7] = z[5] * z[6];
z[8] = abb[32] + -abb[33];
z[9] = -abb[6] * z[8];
z[10] = (T(1) / T(2)) * z[9];
z[11] = abb[31] + -abb[34];
z[12] = abb[33] + z[11];
z[13] = abb[35] + -z[12];
z[13] = abb[13] * z[13];
z[7] = (T(1) / T(2)) * z[4] + z[7] + -z[10] + -z[13];
z[7] = m1_set::bc<T>[0] * z[7];
z[14] = -abb[32] + abb[34] * (T(1) / T(4)) + abb[35] * (T(3) / T(4));
z[14] = m1_set::bc<T>[0] * z[14];
z[14] = -abb[42] + z[14];
z[14] = abb[0] * z[14];
z[15] = -abb[32] + abb[35];
z[16] = m1_set::bc<T>[0] * z[15];
z[16] = -abb[42] + z[16];
z[16] = abb[1] * z[16];
z[14] = z[14] + -z[16];
z[17] = -abb[34] + abb[36];
z[15] = z[15] + -z[17];
z[18] = m1_set::bc<T>[0] * z[15];
z[18] = -abb[42] + z[18];
z[19] = abb[8] * (T(1) / T(2));
z[20] = z[18] * z[19];
z[21] = -abb[6] + abb[15];
z[21] = abb[42] * z[21];
z[20] = z[20] + (T(1) / T(2)) * z[21];
z[21] = abb[43] * (T(1) / T(2));
z[22] = -abb[5] + abb[8];
z[22] = z[21] * z[22];
z[23] = z[8] + -z[11];
z[24] = m1_set::bc<T>[0] * z[23];
z[24] = abb[42] + z[24];
z[25] = abb[7] * (T(1) / T(2));
z[24] = z[24] * z[25];
z[26] = abb[34] * (T(3) / T(2));
z[27] = abb[31] + abb[33];
z[28] = -abb[32] + abb[35] * (T(3) / T(2)) + z[26] + -z[27];
z[28] = m1_set::bc<T>[0] * z[28];
z[28] = -abb[42] + z[28];
z[29] = abb[4] * (T(1) / T(2));
z[28] = z[28] * z[29];
z[7] = z[7] + -z[14] + -z[20] + z[22] + z[24] + z[28];
z[7] = abb[49] * z[7];
z[22] = abb[34] + abb[36];
z[22] = (T(1) / T(2)) * z[22];
z[24] = abb[31] * (T(1) / T(2));
z[28] = abb[33] + z[24];
z[30] = z[0] + z[22] + -z[28];
z[30] = m1_set::bc<T>[0] * z[30];
z[30] = z[21] + z[30];
z[30] = abb[4] * z[30];
z[31] = -abb[36] + z[1];
z[32] = z[0] + z[31];
z[33] = m1_set::bc<T>[0] * z[32];
z[34] = -abb[43] + z[33];
z[35] = abb[17] * (T(1) / T(2));
z[36] = z[34] * z[35];
z[37] = abb[31] + -abb[36];
z[38] = abb[10] * m1_set::bc<T>[0] * z[37];
z[30] = z[30] + z[36] + -z[38];
z[36] = z[8] + -z[24];
z[39] = abb[35] * (T(1) / T(4));
z[40] = abb[36] + z[1];
z[40] = z[36] + -z[39] + (T(1) / T(2)) * z[40];
z[40] = m1_set::bc<T>[0] * z[40];
z[40] = abb[42] + z[21] + z[40];
z[40] = abb[7] * z[40];
z[41] = -abb[8] * z[18];
z[42] = abb[18] * abb[42];
z[43] = abb[31] + z[5];
z[44] = -abb[32] + z[43];
z[45] = abb[18] * z[44];
z[46] = m1_set::bc<T>[0] * z[45];
z[47] = abb[19] + -abb[22];
z[48] = abb[44] * z[47];
z[14] = -z[14] + z[30] + z[40] + z[41] + -z[42] + z[46] + (T(1) / T(4)) * z[48];
z[14] = abb[51] * z[14];
z[40] = abb[5] * z[5];
z[4] = z[4] + -z[9] + z[40];
z[4] = (T(1) / T(2)) * z[4] + z[45];
z[4] = m1_set::bc<T>[0] * z[4];
z[40] = abb[2] * m1_set::bc<T>[0];
z[41] = -z[5] * z[40];
z[46] = m1_set::bc<T>[0] * z[3];
z[46] = -abb[42] + z[46];
z[48] = -abb[0] * z[46];
z[4] = z[4] + -z[20] + z[41] + -z[42] + z[48];
z[4] = abb[54] * z[4];
z[13] = z[13] + z[45];
z[13] = m1_set::bc<T>[0] * z[13];
z[20] = abb[4] * z[18];
z[41] = -abb[35] + z[43];
z[41] = z[40] * z[41];
z[13] = -z[13] + z[16] + z[20] + z[41] + z[42];
z[16] = m1_set::bc<T>[0] * z[17];
z[20] = -abb[7] * z[16];
z[20] = z[13] + z[20];
z[20] = abb[50] * z[20];
z[42] = -abb[34] + abb[35];
z[45] = abb[0] * (T(1) / T(4));
z[42] = m1_set::bc<T>[0] * z[42] * z[45];
z[48] = -abb[31] + z[0];
z[49] = -z[31] + z[48];
z[49] = m1_set::bc<T>[0] * z[49];
z[49] = -abb[43] + z[49];
z[50] = -z[25] * z[49];
z[51] = abb[8] * z[16];
z[30] = z[30] + z[41] + z[42] + z[50] + z[51];
z[30] = abb[53] * z[30];
z[41] = -abb[7] + abb[14];
z[41] = z[16] * z[41];
z[13] = z[13] + z[41];
z[13] = abb[52] * z[13];
z[41] = abb[45] + abb[47];
z[50] = m1_set::bc<T>[0] * z[41];
z[15] = z[15] * z[50];
z[51] = abb[46] + abb[48];
z[18] = z[18] * z[51];
z[52] = z[41] + -z[51];
z[53] = abb[43] * z[52];
z[54] = abb[42] * z[41];
z[15] = z[15] + -z[18] + -z[53] + -z[54];
z[18] = abb[24] + abb[26];
z[18] = (T(1) / T(2)) * z[18];
z[15] = z[15] * z[18];
z[55] = -z[33] * z[51];
z[32] = z[32] * z[50];
z[32] = z[32] + -z[53] + z[55];
z[53] = abb[28] * (T(1) / T(2));
z[32] = z[32] * z[53];
z[55] = abb[54] + abb[55];
z[56] = -z[47] * z[55];
z[52] = abb[29] * z[52];
z[57] = abb[20] + abb[21];
z[58] = abb[54] * z[57];
z[52] = z[52] + z[56] + z[58];
z[56] = abb[53] * z[47];
z[58] = -abb[49] * z[57];
z[56] = -z[52] + z[56] + z[58];
z[58] = abb[17] * (T(1) / T(4));
z[59] = abb[7] * (T(1) / T(4));
z[60] = -abb[30] + z[45] + z[58] + z[59];
z[61] = abb[56] * z[60];
z[56] = (T(1) / T(4)) * z[56] + z[61];
z[56] = abb[44] * z[56];
z[2] = -abb[33] + z[2];
z[61] = z[2] * z[50];
z[2] = m1_set::bc<T>[0] * z[2] * z[51];
z[2] = -z[2] + z[61];
z[61] = abb[25] * (T(1) / T(2));
z[62] = abb[27] * (T(1) / T(2)) + z[61];
z[2] = z[2] * z[62];
z[62] = -abb[31] + abb[35];
z[40] = -z[40] * z[62];
z[63] = abb[2] * abb[43];
z[40] = z[40] + z[42] + z[63];
z[40] = abb[55] * z[40];
z[33] = abb[54] * z[33];
z[42] = abb[55] * z[34];
z[63] = abb[43] * abb[54];
z[33] = z[33] + z[42] + -z[63];
z[33] = z[33] * z[35];
z[42] = -z[3] + -z[5];
z[42] = m1_set::bc<T>[0] * z[42];
z[42] = abb[42] + z[42];
z[42] = abb[54] * z[42];
z[49] = -abb[55] * z[49];
z[42] = z[42] + z[49] + z[63];
z[42] = z[25] * z[42];
z[49] = z[1] + -z[5];
z[64] = abb[32] * (T(1) / T(2));
z[49] = -abb[31] + z[39] + (T(1) / T(2)) * z[49] + z[64];
z[49] = m1_set::bc<T>[0] * z[49];
z[21] = abb[42] * (T(1) / T(2)) + -z[21] + z[49];
z[21] = abb[54] * z[21];
z[49] = -abb[35] + abb[36] + z[11];
z[49] = m1_set::bc<T>[0] * z[49];
z[49] = abb[43] + z[49];
z[65] = abb[55] * (T(1) / T(2));
z[66] = -z[49] * z[65];
z[21] = z[21] + z[66];
z[21] = abb[4] * z[21];
z[46] = -z[46] * z[51];
z[3] = z[3] * z[50];
z[3] = z[3] + z[46] + -z[54];
z[3] = abb[23] * z[3];
z[34] = -abb[22] * z[34];
z[46] = -abb[36] + z[26];
z[48] = -z[46] + -z[48];
z[48] = m1_set::bc<T>[0] * z[48];
z[48] = abb[43] + z[48];
z[48] = abb[19] * z[48];
z[49] = abb[20] * z[49];
z[34] = z[34] + z[48] + z[49];
z[48] = abb[56] * (T(1) / T(2));
z[34] = z[34] * z[48];
z[49] = abb[2] + -z[6] + z[19];
z[49] = z[49] * z[63];
z[38] = -z[38] * z[55];
z[50] = abb[50] + abb[55];
z[16] = abb[14] * z[16] * z[50];
z[2] = z[2] + (T(1) / T(2)) * z[3] + z[4] + z[7] + z[13] + z[14] + z[15] + z[16] + z[20] + z[21] + z[30] + z[32] + z[33] + z[34] + z[38] + z[40] + z[42] + z[49] + z[56];
z[2] = (T(1) / T(8)) * z[2];
z[3] = z[0] * z[17];
z[4] = z[17] * z[24];
z[3] = z[3] + -z[4];
z[7] = abb[33] * (T(1) / T(2));
z[13] = -abb[34] + z[7];
z[14] = z[7] * z[13];
z[15] = abb[40] * (T(1) / T(2));
z[16] = prod_pow(m1_set::bc<T>[0], 2);
z[20] = (T(1) / T(4)) * z[16];
z[21] = -abb[34] + abb[36] * (T(1) / T(2));
z[21] = abb[36] * z[21];
z[30] = prod_pow(abb[34], 2);
z[14] = z[3] + -z[14] + -z[15] + z[20] + z[21] + (T(1) / T(4)) * z[30];
z[14] = abb[2] * z[14];
z[32] = abb[58] * (T(1) / T(2));
z[33] = (T(1) / T(2)) * z[16];
z[34] = z[32] + -z[33];
z[38] = -z[5] * z[17];
z[3] = z[3] + -z[34] + z[38];
z[3] = z[3] * z[29];
z[31] = z[24] + z[31];
z[31] = -z[31] * z[62];
z[31] = abb[37] + z[31];
z[30] = (T(1) / T(2)) * z[30];
z[40] = z[21] + z[30];
z[42] = (T(1) / T(6)) * z[16];
z[49] = z[31] + z[40] + z[42];
z[45] = z[45] * z[49];
z[54] = z[0] * z[11];
z[56] = -abb[37] + z[54];
z[62] = z[11] * z[24];
z[63] = abb[58] + z[56] + -z[62];
z[66] = z[40] + -z[42] + z[63];
z[59] = -z[59] * z[66];
z[67] = abb[59] * (T(1) / T(8));
z[47] = z[47] * z[67];
z[68] = -abb[36] + z[7];
z[68] = abb[33] * z[68];
z[69] = prod_pow(abb[36], 2);
z[70] = (T(1) / T(2)) * z[69];
z[71] = z[68] + z[70];
z[72] = abb[40] + z[71];
z[72] = abb[11] * z[72];
z[73] = z[33] + -z[40];
z[19] = -z[19] * z[73];
z[74] = (T(1) / T(3)) * z[16];
z[75] = z[31] + z[74];
z[76] = abb[58] + z[75];
z[58] = z[58] * z[76];
z[77] = z[0] + -z[24];
z[77] = z[37] * z[77];
z[77] = -abb[37] + z[77];
z[78] = abb[10] * z[77];
z[3] = z[3] + -z[14] + z[19] + z[45] + -z[47] + z[58] + z[59] + (T(-1) / T(2)) * z[72] + z[78];
z[3] = abb[53] * z[3];
z[19] = -z[24] + z[46];
z[19] = z[0] * z[19];
z[45] = abb[31] * (T(3) / T(2));
z[46] = -z[1] + z[45];
z[58] = -abb[36] + z[46];
z[58] = z[24] * z[58];
z[59] = abb[37] * (T(1) / T(2));
z[78] = abb[32] * z[11];
z[78] = -abb[57] + z[78];
z[19] = abb[38] + (T(5) / T(12)) * z[16] + -z[19] + (T(1) / T(2)) * z[40] + z[58] + z[59] + -z[78];
z[19] = abb[0] * z[19];
z[58] = abb[31] * z[17];
z[44] = abb[35] * z[44];
z[79] = z[44] + z[58];
z[80] = -z[11] + z[64];
z[81] = abb[32] * z[80];
z[82] = abb[33] * abb[34];
z[83] = abb[39] + z[82];
z[84] = -abb[40] + z[21];
z[85] = -abb[37] + abb[38] + z[79] + z[81] + -z[83] + -z[84];
z[86] = abb[16] * (T(1) / T(2));
z[85] = z[85] * z[86];
z[86] = -abb[39] + abb[41];
z[82] = z[82] + -z[86];
z[28] = -abb[34] + z[28];
z[28] = abb[31] * z[28];
z[28] = z[28] + z[30];
z[87] = -z[28] + z[33] + z[78] + z[82];
z[87] = z[25] * z[87];
z[62] = abb[38] + z[62];
z[54] = z[54] + z[62] + -z[78];
z[88] = abb[15] * z[54];
z[89] = prod_pow(abb[33], 2);
z[89] = abb[39] + (T(1) / T(2)) * z[89];
z[90] = prod_pow(abb[32], 2);
z[91] = (T(1) / T(2)) * z[90];
z[92] = abb[57] + z[89] + -z[91];
z[92] = abb[6] * z[92];
z[88] = -z[88] + z[92];
z[92] = z[5] + z[24];
z[92] = abb[31] * z[92];
z[17] = abb[35] * z[17];
z[93] = z[17] + z[92];
z[94] = abb[40] + -abb[41] + z[89];
z[78] = -abb[58] + z[78];
z[95] = z[78] + -z[93] + z[94];
z[96] = -z[74] + (T(1) / T(2)) * z[95];
z[97] = abb[8] * z[96];
z[98] = -abb[33] + z[24];
z[99] = abb[31] * z[98];
z[94] = abb[37] + abb[38] + z[94] + z[99];
z[94] = abb[3] * z[94];
z[97] = (T(1) / T(2)) * z[94] + -z[97];
z[99] = abb[13] * z[23];
z[100] = abb[32] * z[99];
z[13] = abb[33] * z[13];
z[84] = z[13] + -z[84];
z[101] = abb[58] + z[84];
z[6] = z[6] * z[101];
z[10] = z[10] + z[99];
z[10] = abb[35] * z[10];
z[102] = -abb[13] + abb[15] * (T(1) / T(6));
z[102] = z[16] * z[102];
z[6] = z[6] + z[10] + -z[19] + -z[85] + z[87] + (T(-1) / T(2)) * z[88] + -z[97] + -z[100] + z[102];
z[7] = -z[7] + z[80];
z[7] = abb[32] * z[7];
z[10] = abb[57] + z[28] + -z[83];
z[28] = z[0] * z[23];
z[83] = abb[41] * (T(1) / T(2));
z[7] = z[7] + (T(1) / T(2)) * z[10] + z[20] + -z[28] + z[83];
z[7] = abb[1] * z[7];
z[10] = -abb[33] + z[64];
z[20] = z[10] * z[64];
z[28] = z[24] * z[98];
z[64] = z[15] + -z[83];
z[20] = abb[38] + -z[20] + z[28] + z[64];
z[20] = abb[9] * z[20];
z[20] = z[7] + z[20];
z[8] = -z[8] + -z[26] + z[45];
z[8] = z[0] * z[8];
z[26] = -abb[33] + z[1];
z[26] = abb[31] * z[26];
z[28] = -abb[38] + abb[57] + -z[26];
z[12] = abb[32] * (T(3) / T(4)) + -z[12];
z[12] = abb[32] * z[12];
z[8] = z[8] + z[12] + (T(1) / T(2)) * z[28] + -z[64];
z[8] = (T(1) / T(2)) * z[8] + z[74];
z[8] = abb[4] * z[8];
z[12] = z[57] * z[67];
z[6] = (T(1) / T(2)) * z[6] + z[8] + z[12] + z[20];
z[6] = abb[49] * z[6];
z[8] = z[1] + -z[43];
z[8] = z[8] * z[24];
z[5] = -abb[32] + z[5] + z[46];
z[0] = z[0] * z[5];
z[5] = -abb[38] + -abb[57] + -z[69];
z[0] = z[0] + (T(1) / T(2)) * z[5] + z[8] + -z[15] + z[32] + z[42] + -z[68] + -z[83] + (T(1) / T(4)) * z[90];
z[0] = abb[54] * z[0];
z[5] = -abb[58] + -z[17] + z[58];
z[8] = z[5] + -z[16];
z[12] = -z[8] * z[65];
z[0] = z[0] + z[12];
z[0] = abb[4] * z[0];
z[12] = abb[41] + abb[57];
z[15] = z[12] + z[71];
z[17] = -z[15] + z[44] + z[91] + -z[92];
z[17] = abb[18] * z[17];
z[24] = abb[5] * z[101];
z[9] = abb[35] * z[9];
z[9] = z[9] + z[24] + -z[88];
z[24] = z[54] + z[74];
z[28] = -abb[0] * z[24];
z[43] = -abb[2] * z[101];
z[42] = abb[15] * z[42];
z[9] = (T(1) / T(2)) * z[9] + -z[17] + z[28] + z[42] + z[43] + z[85] + -z[97];
z[9] = abb[54] * z[9];
z[26] = z[21] + z[26] + -z[56] + -z[74] + z[78] + z[82];
z[26] = abb[54] * z[26];
z[28] = -abb[55] * z[66];
z[26] = z[26] + z[28];
z[26] = z[25] * z[26];
z[1] = z[1] + z[36];
z[1] = abb[35] * z[1];
z[1] = -z[1] + z[62] + z[81] + -z[89];
z[28] = z[1] + z[74];
z[28] = -z[28] * z[51];
z[1] = z[1] * z[41];
z[36] = z[41] * z[74];
z[1] = z[1] + z[28] + z[36];
z[1] = z[1] * z[61];
z[5] = z[5] + -z[33] + -z[40];
z[5] = abb[2] * z[5];
z[28] = abb[0] * z[49];
z[5] = z[5] + (T(1) / T(2)) * z[28];
z[5] = abb[55] * z[5];
z[28] = abb[58] + z[31];
z[28] = z[28] * z[41];
z[33] = -z[51] * z[76];
z[28] = z[28] + z[33] + z[36];
z[28] = z[28] * z[53];
z[33] = z[35] * z[76];
z[35] = z[33] * z[55];
z[0] = z[0] + z[1] + z[5] + z[9] + z[26] + z[28] + z[35];
z[1] = -abb[33] + z[80];
z[1] = abb[32] * z[1];
z[5] = z[21] + -z[30];
z[9] = -z[11] * z[39];
z[11] = -abb[33] + abb[31] * (T(-1) / T(4)) + abb[34] * (T(3) / T(4));
z[11] = abb[31] * z[11];
z[5] = -z[1] + (T(1) / T(2)) * z[5] + z[9] + z[11] + -z[12] + -z[13] + (T(1) / T(12)) * z[16] + -z[32] + z[59];
z[5] = abb[7] * z[5];
z[1] = z[1] + z[15] + z[16] + z[93];
z[9] = -abb[8] * z[1];
z[10] = abb[32] * z[10];
z[10] = z[10] + z[89];
z[10] = abb[12] * z[10];
z[5] = z[5] + z[9] + z[10] + -z[17] + -z[19] + z[33] + -z[94];
z[9] = abb[32] * z[23];
z[11] = -abb[32] + -z[22] + z[27];
z[11] = abb[35] * z[11];
z[4] = z[4] + z[9] + z[11] + -z[34];
z[4] = z[4] * z[29];
z[11] = abb[33] * abb[36];
z[12] = abb[35] * z[37];
z[11] = z[11] + z[12] + -z[70] + -z[86] + -z[92];
z[11] = (T(1) / T(2)) * z[11];
z[12] = abb[10] * z[11];
z[4] = z[4] + (T(1) / T(2)) * z[5] + z[12] + z[20] + -z[47];
z[4] = abb[51] * z[4];
z[5] = -z[9] + z[38] + -z[79];
z[5] = abb[16] * z[5];
z[9] = abb[35] * z[99];
z[12] = abb[13] * z[16];
z[5] = -z[5] + -z[9] + -z[10] + z[12] + -z[17] + z[72] + z[100];
z[1] = z[1] * z[29];
z[1] = -z[1] + (T(1) / T(2)) * z[5] + -z[7] + z[14];
z[5] = z[25] * z[73];
z[5] = -z[1] + z[5];
z[5] = abb[50] * z[5];
z[7] = abb[14] * (T(1) / T(2));
z[9] = -z[7] + z[25];
z[9] = z[9] * z[73];
z[1] = -z[1] + z[9];
z[1] = abb[52] * z[1];
z[9] = -z[75] + z[84];
z[9] = z[9] * z[51];
z[10] = -z[31] + z[84];
z[10] = -z[10] * z[41];
z[9] = z[9] + z[10] + z[36];
z[9] = abb[27] * z[9];
z[10] = z[41] * z[54];
z[12] = -z[24] * z[51];
z[10] = z[10] + z[12] + z[36];
z[10] = abb[23] * z[10];
z[9] = z[9] + z[10];
z[10] = abb[54] * z[11];
z[11] = abb[55] * z[77];
z[10] = z[10] + z[11];
z[10] = abb[10] * z[10];
z[11] = z[41] * z[95];
z[12] = z[51] * z[96];
z[11] = (T(1) / T(2)) * z[11] + -z[12] + -z[36];
z[11] = -z[11] * z[18];
z[12] = (T(-5) / T(6)) * z[16] + z[40] + -z[63];
z[12] = abb[19] * z[12];
z[8] = abb[20] * z[8];
z[13] = -abb[22] * z[76];
z[8] = z[8] + z[12] + z[13];
z[12] = -abb[59] * z[60];
z[8] = (T(1) / T(2)) * z[8] + z[12];
z[8] = z[8] * z[48];
z[12] = z[52] * z[67];
z[7] = -z[7] * z[50] * z[73];
z[0] = (T(1) / T(2)) * z[0] + z[1] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + (T(1) / T(4)) * z[9] + z[10] + z[11] + z[12];
z[0] = (T(1) / T(4)) * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_115_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("-0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("-0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("-0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("-0.60086751779409396355157479392146178660352093446466278897269777887"),stof<T>("0.71369786693229131647614382824053875589540444692855359891049920454")}, std::complex<T>{stof<T>("-1.3212496324146752793794140156688067857013151975707890667213928086"),stof<T>("0.8434002737337653198416459035053544840998950619754997108718017326")}, std::complex<T>{stof<T>("-0.76672795551796564544887259254159922462211988990160497426445996661"),stof<T>("0.65048126298211652630343498218673054426221331309112239872519042328")}, std::complex<T>{stof<T>("-1.3212496324146752793794140156688067857013151975707890667213928086"),stof<T>("0.8434002737337653198416459035053544840998950619754997108718017326")}, std::complex<T>{stof<T>("-0.81050460892544518899904319653046787745769393785823584700430229716"),stof<T>("0.48941725065523810735097743560424907116525855941236699874812864164")}, std::complex<T>{stof<T>("0.18261390179162965070966596235193556372877237025680415802123344081"),stof<T>("0.41328989670267373852583756807395615760041594725023174989765257276")}, std::complex<T>{stof<T>("-0.53776821282895166511817325939540943536902189284932211972746158888"),stof<T>("0.54299230350414774189133964333877188580490656229717786185895510081")}, std::complex<T>{stof<T>("-0.45206819008996940444056879493693166461096524631315406723780644064"),stof<T>("0.6183869553716871503642261524770373176934380471354809098273440296")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_115_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_115_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.02435556195130010557768050841578811653053069984159512999953262175"),stof<T>("-0.40509873509489876701374346996009466991183351566969552760882937672")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,60> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W81(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_115_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_115_DLogXconstant_part(base_point<T>, kend);
	value += f_4_115_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_115_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_115_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_115_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_115_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_115_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_115_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
