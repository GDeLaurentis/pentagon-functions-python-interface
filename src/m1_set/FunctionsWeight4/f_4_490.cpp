/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_490.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_490_abbreviated (const std::array<T,30>& abb) {
T z[39];
z[0] = prod_pow(m1_set::bc<T>[0], 2);
z[1] = (T(1) / T(6)) * z[0];
z[2] = 2 * abb[27];
z[3] = z[1] + z[2];
z[4] = abb[11] * (T(1) / T(2));
z[5] = abb[12] + -abb[13];
z[6] = (T(5) / T(2)) * z[5];
z[7] = -abb[11] + z[6];
z[7] = z[4] * z[7];
z[8] = abb[14] * (T(3) / T(2));
z[9] = -abb[13] + z[8];
z[10] = 2 * abb[11];
z[11] = 2 * abb[12] + -z[10];
z[12] = -z[9] + -z[11];
z[12] = abb[14] * z[12];
z[13] = prod_pow(abb[13], 2);
z[14] = (T(1) / T(4)) * z[13];
z[15] = abb[12] * abb[13];
z[7] = abb[15] + z[3] + z[7] + z[12] + z[14] + (T(3) / T(4)) * z[15];
z[7] = abb[3] * z[7];
z[12] = z[4] * z[5];
z[16] = abb[11] + -abb[12];
z[17] = abb[14] * z[16];
z[17] = abb[15] + abb[27] + z[17];
z[18] = (T(1) / T(2)) * z[5];
z[19] = abb[12] * z[18];
z[1] = z[1] + -z[12] + -z[17] + z[19];
z[1] = abb[2] * z[1];
z[19] = -abb[12] + abb[13] * (T(1) / T(2));
z[19] = abb[12] * z[19];
z[20] = (T(1) / T(2)) * z[13];
z[19] = z[19] + z[20];
z[19] = abb[0] * z[19];
z[21] = abb[0] * z[5];
z[22] = z[4] * z[21];
z[19] = z[19] + -z[22];
z[22] = (T(1) / T(2)) * z[19];
z[23] = -abb[11] + z[5];
z[24] = z[4] * z[23];
z[25] = (T(1) / T(2)) * z[15];
z[26] = z[17] + z[24] + z[25];
z[27] = (T(1) / T(3)) * z[0];
z[28] = -z[26] + -z[27];
z[28] = abb[6] * z[28];
z[1] = z[1] + z[7] + -z[22] + z[28];
z[1] = abb[23] * z[1];
z[6] = abb[11] + z[6];
z[6] = z[4] * z[6];
z[7] = 2 * abb[13];
z[28] = -abb[14] + z[7];
z[28] = abb[14] * z[28];
z[29] = 2 * abb[15];
z[6] = (T(-1) / T(2)) * z[0] + z[6] + -z[14] + (T(-5) / T(4)) * z[15] + z[28] + -z[29];
z[6] = abb[3] * z[6];
z[30] = 2 * abb[14];
z[31] = z[16] * z[30];
z[29] = z[15] + z[29];
z[32] = (T(2) / T(3)) * z[0];
z[33] = z[2] + z[32];
z[34] = abb[11] * z[23];
z[31] = z[29] + z[31] + z[33] + z[34];
z[34] = abb[6] * z[31];
z[35] = abb[14] * (T(1) / T(2));
z[11] = abb[13] + z[11] + z[35];
z[11] = abb[14] * z[11];
z[36] = 3 * abb[15];
z[3] = -z[3] + z[11] + -z[36];
z[37] = abb[12] * (T(1) / T(2));
z[38] = -abb[12] + -3 * abb[13];
z[38] = z[37] * z[38];
z[24] = z[3] + -z[24] + z[38];
z[24] = abb[2] * z[24];
z[6] = z[6] + -z[22] + z[24] + z[34];
z[6] = abb[20] * z[6];
z[9] = z[9] + -4 * z[16];
z[9] = abb[14] * z[9];
z[7] = -abb[12] + z[7];
z[7] = abb[12] * z[7];
z[16] = -abb[11] + 2 * z[5];
z[16] = abb[11] * z[16];
z[7] = 4 * abb[27] + z[7] + -z[9] + z[16] + z[20] + z[27] + z[36];
z[9] = abb[19] + abb[21];
z[20] = -abb[23] + z[9];
z[20] = abb[1] * z[20];
z[7] = -z[7] * z[20];
z[24] = -abb[15] + -z[25];
z[2] = -z[2] + z[11] + -z[12] + 3 * z[24] + z[27];
z[2] = abb[3] * z[2];
z[11] = -z[23] + z[35];
z[11] = abb[14] * z[11];
z[12] = abb[11] * (T(3) / T(2)) + -z[5];
z[12] = abb[11] * z[12];
z[11] = abb[27] + (T(4) / T(3)) * z[0] + z[11] + -z[12];
z[12] = -abb[2] * z[11];
z[2] = z[2] + z[12] + z[19];
z[2] = abb[21] * z[2];
z[12] = -abb[12] + -abb[13];
z[12] = z[12] * z[37];
z[19] = -abb[11] + z[18];
z[24] = -abb[11] * z[19];
z[12] = (T(-5) / T(6)) * z[0] + z[12] + -z[17] + z[24];
z[12] = abb[2] * z[12];
z[17] = -z[4] * z[19];
z[3] = z[3] + -z[14] + (T(-7) / T(4)) * z[15] + z[17];
z[3] = abb[3] * z[3];
z[3] = z[3] + z[12] + z[22];
z[3] = abb[19] * z[3];
z[12] = abb[2] * z[31];
z[14] = -abb[11] * z[5];
z[14] = z[14] + -z[28] + z[29];
z[14] = abb[3] * z[14];
z[17] = abb[7] + abb[8];
z[22] = abb[9] + z[17];
z[24] = -abb[28] * z[22];
z[12] = z[12] + z[14] + z[24] + -z[34];
z[14] = 2 * abb[22];
z[12] = z[12] * z[14];
z[24] = abb[14] + 2 * z[23];
z[24] = abb[14] * z[24];
z[16] = z[16] + -z[24] + z[33];
z[24] = -abb[20] + z[14];
z[16] = z[16] * z[24];
z[11] = -z[9] * z[11];
z[4] = -z[4] + -z[5];
z[4] = abb[11] * z[4];
z[8] = z[8] + z[23];
z[8] = abb[14] * z[8];
z[4] = -abb[27] + z[4] + z[8] + z[32];
z[4] = abb[23] * z[4];
z[4] = z[4] + z[11] + z[16];
z[4] = abb[4] * z[4];
z[8] = z[13] + z[15];
z[11] = abb[7] * (T(1) / T(2));
z[8] = z[8] * z[11];
z[13] = abb[7] * z[18];
z[15] = abb[11] * z[17];
z[13] = z[13] + z[15];
z[13] = abb[11] * z[13];
z[16] = z[0] * z[17];
z[17] = abb[8] * prod_pow(abb[12], 2);
z[8] = z[8] + -z[13] + z[16] + z[17];
z[13] = abb[0] + abb[3];
z[13] = abb[2] + (T(3) / T(2)) * z[13];
z[13] = -abb[10] + (T(1) / T(2)) * z[13];
z[16] = abb[28] * z[13];
z[8] = (T(1) / T(2)) * z[8] + z[16];
z[16] = abb[24] + abb[25] + abb[26];
z[8] = z[8] * z[16];
z[0] = z[0] + 3 * z[26];
z[17] = abb[6] * z[9];
z[0] = z[0] * z[17];
z[24] = abb[7] + abb[9];
z[24] = abb[21] * z[24];
z[11] = abb[9] + z[11];
z[11] = abb[19] * z[11];
z[11] = z[11] + z[24];
z[24] = abb[8] + abb[7] * (T(1) / T(4));
z[25] = abb[9] * (T(1) / T(2)) + -z[24];
z[25] = abb[23] * z[25];
z[24] = abb[9] + z[24];
z[24] = abb[20] * z[24];
z[11] = (T(3) / T(2)) * z[11] + z[24] + -z[25];
z[24] = abb[28] * z[11];
z[0] = abb[29] + z[0] + z[1] + z[2] + z[3] + z[4] + z[6] + z[7] + z[8] + z[12] + z[24];
z[1] = -abb[11] + z[30];
z[2] = (T(5) / T(4)) * z[5];
z[3] = -z[1] + -z[2];
z[3] = m1_set::bc<T>[0] * z[3];
z[4] = 2 * abb[16];
z[3] = z[3] + z[4];
z[3] = abb[3] * z[3];
z[6] = abb[14] + z[19];
z[6] = m1_set::bc<T>[0] * z[6];
z[6] = -abb[16] + z[6];
z[7] = abb[6] * z[6];
z[8] = m1_set::bc<T>[0] * z[21];
z[8] = (T(1) / T(4)) * z[8];
z[12] = abb[14] + z[18];
z[21] = m1_set::bc<T>[0] * z[12];
z[21] = -abb[16] + z[21];
z[21] = abb[2] * z[21];
z[3] = z[3] + z[7] + -z[8] + z[21];
z[3] = abb[23] * z[3];
z[7] = -z[5] + z[10] + -z[30];
z[7] = m1_set::bc<T>[0] * z[7];
z[7] = z[4] + z[7];
z[21] = abb[6] * z[7];
z[19] = z[19] + z[30];
z[19] = m1_set::bc<T>[0] * z[19];
z[19] = -z[4] + z[19];
z[19] = abb[2] * z[19];
z[24] = abb[3] * m1_set::bc<T>[0];
z[2] = -abb[11] + -z[2];
z[2] = z[2] * z[24];
z[2] = z[2] + -z[8] + z[19] + z[21];
z[2] = abb[20] * z[2];
z[19] = z[18] + z[30];
z[19] = m1_set::bc<T>[0] * z[19];
z[19] = -z[4] + z[19];
z[19] = abb[3] * z[19];
z[18] = abb[0] * m1_set::bc<T>[0] * z[18];
z[21] = abb[14] + z[5];
z[25] = 3 * abb[11] + -z[21];
z[25] = m1_set::bc<T>[0] * z[25];
z[25] = abb[16] + z[25];
z[26] = -abb[2] * z[25];
z[18] = z[18] + z[19] + z[26];
z[18] = abb[21] * z[18];
z[10] = -z[10] + z[12];
z[10] = m1_set::bc<T>[0] * z[10];
z[10] = -abb[16] + z[10];
z[10] = abb[2] * z[10];
z[12] = (T(1) / T(4)) * z[5];
z[1] = z[1] + z[12];
z[1] = m1_set::bc<T>[0] * z[1];
z[1] = z[1] + -z[4];
z[1] = abb[3] * z[1];
z[1] = z[1] + z[8] + z[10];
z[1] = abb[19] * z[1];
z[8] = abb[17] * z[11];
z[10] = abb[2] + -abb[6];
z[7] = z[7] * z[10];
z[5] = z[5] * z[24];
z[10] = -abb[17] * z[22];
z[5] = z[5] + z[7] + z[10];
z[5] = z[5] * z[14];
z[7] = 2 * abb[20] + -4 * abb[22];
z[10] = abb[14] + z[23];
z[10] = m1_set::bc<T>[0] * z[10];
z[10] = -abb[16] + z[10];
z[7] = z[7] * z[10];
z[9] = -z[9] * z[25];
z[10] = abb[11] + z[21];
z[10] = m1_set::bc<T>[0] * z[10];
z[10] = -abb[16] + z[10];
z[10] = abb[23] * z[10];
z[7] = z[7] + z[9] + z[10];
z[7] = abb[4] * z[7];
z[9] = abb[7] * z[12];
z[9] = z[9] + z[15];
z[9] = m1_set::bc<T>[0] * z[9];
z[10] = abb[17] * z[13];
z[9] = z[9] + z[10];
z[9] = z[9] * z[16];
z[6] = -z[6] * z[17];
z[10] = z[23] + z[30];
z[10] = m1_set::bc<T>[0] * z[10];
z[4] = -z[4] + z[10];
z[4] = z[4] * z[20];
z[1] = abb[18] + z[1] + z[2] + z[3] + 2 * z[4] + z[5] + 3 * z[6] + z[7] + z[8] + z[9] + z[18];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_490_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("15.94994955762769688659116053327821002967542676206799795382471441"),stof<T>("8.219431292131813448363959785914326629448032568407109190935755791")}, std::complex<T>{stof<T>("20.929768843778489303320301456397884454549260203377654859219803629"),stof<T>("4.389025920905036481071284721638033872172095059758790668766056188")}, std::complex<T>{stof<T>("17.2927064979043605357078419068528729370025332858244409150814485"),stof<T>("9.351472036322185656651151606485949644265114963281274350967124989")}, std::complex<T>{stof<T>("-44.545051568110305904873965659945094723752733454268195640953075438"),stof<T>("-11.042133330190817378716953084419313773978354909265911657594850771")}, std::complex<T>{stof<T>("6.3225762264274560658458222966943373322009399650660998666518233087"),stof<T>("-2.6983646270364047590054832437046697424588551137741533621383304054")}, std::complex<T>{stof<T>("4.6547188104916960897412832574814250337357180128234620229279515134"),stof<T>("-6.7898493908076716514634340510446747195333140327511877322391776059")}, std::complex<T>{stof<T>("4.6547188104916960897412832574814250337357180128234620229279515134"),stof<T>("-6.7898493908076716514634340510446747195333140327511877322391776059")}, std::complex<T>{stof<T>("4.6547188104916960897412832574814250337357180128234620229279515134"),stof<T>("-6.7898493908076716514634340510446747195333140327511877322391776059")}};
	
	std::vector<C> intdlogs = {rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[22].real()/kbase.W[22].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[126].real()/kbase.W[126].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_490_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_490_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (v[1] + v[2] + -v[3] + -v[4]) * (-8 + -4 * v[0] + v[1] + -3 * v[2] + -v[3] + 3 * v[4] + 4 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 4 * v[5])) / prod_pow(tend, 2);
c[1] = ((1 + 2 * m1_set::bc<T>[1]) * (T(1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[19] + abb[20] + -abb[23]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[13], 2);
z[1] = abb[13] + abb[14] * (T(-3) / T(2));
z[1] = abb[14] * z[1];
z[0] = -abb[15] + (T(1) / T(2)) * z[0] + z[1];
z[1] = abb[19] + abb[20] + -abb[23];
return abb[5] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_490_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_490_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-19.404183752038743628608815446550363284442971914323705253969811693"),stof<T>("-14.513060499893867756612911872780713857310045701521246164321599333")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,30> abb = {dl[0], dlog_W4(k,dl), dlog_W8(k,dl), dl[3], dlog_W14(k,dl), dlog_W20(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W127(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_2_im(k), f_2_24_im(k), T{0}, rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[22].real()/k.W[22].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[126].real()/k.W[126].real()), f_2_2_re(k), f_2_24_re(k), T{0}};
abb[18] = SpDLog_f_4_490_W_20_Im(t, path, abb);
abb[29] = SpDLog_f_4_490_W_20_Re(t, path, abb);

                    
            return f_4_490_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_490_DLogXconstant_part(base_point<T>, kend);
	value += f_4_490_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_490_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_490_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_490_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_490_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_490_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_490_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
