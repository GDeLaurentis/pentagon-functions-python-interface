/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_178.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_178_abbreviated (const std::array<T,29>& abb) {
T z[40];
z[0] = abb[22] * (T(1) / T(2));
z[1] = abb[19] * (T(3) / T(2));
z[2] = z[0] + -z[1];
z[3] = abb[21] * (T(1) / T(2));
z[4] = abb[18] + -abb[20];
z[5] = z[2] + z[3] + z[4];
z[5] = abb[2] * z[5];
z[6] = (T(1) / T(4)) * z[4];
z[7] = -abb[23] + z[6];
z[8] = 2 * abb[19];
z[9] = abb[22] * (T(-7) / T(4)) + z[3] + -z[7] + z[8];
z[9] = abb[3] * z[9];
z[10] = abb[22] * (T(5) / T(4));
z[11] = abb[23] + z[1];
z[12] = z[6] + z[10] + -z[11];
z[12] = abb[0] * z[12];
z[13] = 3 * abb[22];
z[14] = -abb[23] + z[4];
z[15] = 2 * z[14];
z[16] = 5 * abb[19] + -abb[21] + -z[13] + -z[15];
z[17] = abb[1] * z[16];
z[12] = z[12] + z[17];
z[17] = abb[19] + -abb[22];
z[18] = abb[21] + z[17];
z[19] = 2 * z[18];
z[20] = abb[5] * z[19];
z[21] = abb[24] + abb[26];
z[22] = abb[25] + (T(1) / T(2)) * z[21];
z[23] = abb[7] * z[22];
z[24] = abb[8] * z[22];
z[5] = z[5] + -z[9] + -z[12] + z[20] + (T(-3) / T(2)) * z[23] + -z[24];
z[5] = abb[12] * z[5];
z[9] = abb[21] * (T(3) / T(2));
z[2] = z[2] + z[9] + z[14];
z[25] = abb[12] * z[2];
z[26] = 3 * abb[19];
z[27] = 3 * abb[21];
z[28] = abb[22] + z[15] + -z[26] + z[27];
z[29] = abb[14] * z[28];
z[25] = z[25] + -z[29];
z[25] = abb[6] * z[25];
z[5] = z[5] + -z[25];
z[29] = abb[22] + z[4];
z[30] = -z[8] + z[29];
z[31] = abb[4] * z[30];
z[27] = 4 * abb[23] + -z[27];
z[32] = 11 * abb[19] + -6 * abb[22] + -5 * z[4] + z[27];
z[32] = abb[1] * z[32];
z[16] = abb[3] * z[16];
z[17] = abb[23] + z[17];
z[17] = abb[0] * z[17];
z[33] = 2 * z[17];
z[16] = z[16] + -z[20] + z[31] + z[32] + -z[33];
z[16] = abb[14] * z[16];
z[20] = (T(5) / T(4)) * z[4];
z[32] = abb[22] * (T(3) / T(4));
z[34] = -abb[23] + z[9];
z[8] = z[8] + -z[20] + -z[32] + -z[34];
z[8] = abb[3] * z[8];
z[35] = abb[6] * z[2];
z[36] = abb[2] * z[18];
z[36] = z[23] + z[36];
z[36] = z[24] + (T(1) / T(2)) * z[36];
z[8] = z[8] + z[12] + z[35] + -z[36];
z[8] = abb[13] * z[8];
z[12] = abb[3] * z[2];
z[37] = 2 * abb[23];
z[9] = -z[9] + z[37];
z[38] = 2 * abb[22] + abb[19] * (T(-7) / T(2)) + (T(3) / T(2)) * z[4] + -z[9];
z[38] = abb[1] * z[38];
z[29] = -abb[19] + (T(1) / T(2)) * z[29];
z[39] = abb[4] * z[29];
z[12] = z[12] + z[17] + -z[35] + z[38] + z[39];
z[12] = abb[11] * z[12];
z[8] = z[5] + z[8] + z[12] + z[16];
z[8] = abb[11] * z[8];
z[12] = 2 * abb[21];
z[16] = abb[19] * (T(1) / T(2)) + z[7] + z[12] + -z[32];
z[16] = abb[0] * z[16];
z[10] = abb[23] + abb[21] * (T(-7) / T(2)) + z[10] + -z[20];
z[10] = abb[3] * z[10];
z[17] = abb[1] * z[19];
z[20] = -abb[19] + 5 * abb[21] + -abb[22] + z[15];
z[20] = abb[5] * z[20];
z[10] = z[10] + z[16] + -z[17] + z[20] + -z[36];
z[16] = -abb[12] * z[10];
z[17] = 7 * abb[19];
z[15] = -abb[21] + 5 * abb[22] + z[15] + -z[17];
z[15] = abb[1] * z[15];
z[36] = abb[3] * z[19];
z[19] = abb[0] * z[19];
z[15] = z[15] + z[19] + z[20] + -z[36];
z[20] = abb[14] * z[15];
z[36] = (T(1) / T(2)) * z[4];
z[0] = abb[21] + -z[0] + z[36];
z[0] = abb[2] * z[0];
z[29] = -abb[0] * z[29];
z[38] = -abb[1] * z[18];
z[0] = z[0] + z[24] + z[29] + z[38];
z[0] = abb[13] * z[0];
z[0] = z[0] + z[16] + z[20] + z[25];
z[0] = abb[13] * z[0];
z[16] = abb[22] * (T(3) / T(2));
z[20] = abb[19] * (T(5) / T(2)) + -z[3] + -z[14] + -z[16];
z[24] = abb[5] * z[20];
z[11] = abb[21] + (T(3) / T(4)) * z[4] + -z[11] + z[32];
z[11] = abb[0] * z[11];
z[25] = abb[1] * z[20];
z[3] = abb[19] + -z[3] + -z[7] + -z[32];
z[3] = abb[3] * z[3];
z[7] = abb[19] + -abb[21];
z[29] = -z[4] + z[7];
z[29] = abb[2] * z[29];
z[23] = z[23] + z[29];
z[3] = z[3] + z[11] + (T(3) / T(2)) * z[23] + -z[24] + z[25];
z[3] = prod_pow(abb[12], 2) * z[3];
z[1] = abb[22] + -z[1] + -z[9] + z[36];
z[1] = abb[8] * z[1];
z[2] = abb[9] * z[2];
z[9] = -abb[2] + abb[10];
z[11] = 2 * abb[25] + z[21];
z[9] = z[9] * z[11];
z[6] = abb[22] * (T(1) / T(4)) + -z[6] + -z[34];
z[6] = abb[7] * z[6];
z[11] = abb[0] * (T(1) / T(2)) + abb[3] * (T(3) / T(2));
z[11] = z[11] * z[22];
z[1] = z[1] + z[2] + -z[6] + z[9] + -z[11];
z[2] = -abb[28] * z[1];
z[6] = abb[6] * z[28];
z[9] = 4 * abb[22] + 3 * z[4] + -z[17] + -z[27];
z[9] = abb[1] * z[9];
z[11] = abb[3] * z[28];
z[9] = -z[6] + z[9] + z[11] + z[31] + z[33];
z[9] = abb[15] * z[9];
z[4] = -4 * abb[19] + -abb[23] + (T(5) / T(2)) * z[4] + z[12] + z[16];
z[4] = abb[1] * z[4];
z[11] = -abb[3] * z[20];
z[12] = -abb[21] + abb[23];
z[12] = abb[0] * z[12];
z[4] = z[4] + z[11] + z[12] + z[24] + -3 * z[39];
z[4] = prod_pow(abb[14], 2) * z[4];
z[6] = -z[6] + z[15];
z[11] = abb[27] * z[6];
z[7] = abb[22] * (T(-1) / T(3)) + z[7] + (T(-2) / T(3)) * z[14];
z[7] = abb[6] * z[7];
z[7] = z[7] + (T(1) / T(3)) * z[15];
z[7] = prod_pow(m1_set::bc<T>[0], 2) * z[7];
z[0] = z[0] + z[2] + z[3] + z[4] + z[7] + z[8] + z[9] + z[11];
z[2] = -z[10] + z[35];
z[2] = abb[13] * z[2];
z[3] = -abb[21] + z[13] + -z[26] + -z[37];
z[3] = abb[1] * z[3];
z[4] = abb[5] * z[28];
z[3] = z[3] + z[4] + z[19] + 2 * z[31];
z[3] = abb[14] * z[3];
z[4] = -abb[3] + abb[5];
z[4] = z[4] * z[18];
z[7] = abb[1] * z[30];
z[4] = z[4] + z[7] + -z[31];
z[4] = abb[11] * z[4];
z[2] = z[2] + z[3] + 2 * z[4] + -z[5];
z[2] = m1_set::bc<T>[0] * z[2];
z[1] = -abb[17] * z[1];
z[3] = abb[16] * z[6];
z[1] = z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_178_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-3.4196200539686499290855411171457372292124491044235190751052025697"),stof<T>("0.5962110675485313984640198168892890990631591794626600213856577033")}, std::complex<T>{stof<T>("2.8439121391173544631584656661632247000508687230543569634638209414"),stof<T>("-7.4977925536325702398108340422123768399784456559844023132278085238")}, std::complex<T>{stof<T>("3.4196200539686499290855411171457372292124491044235190751052025697"),stof<T>("-0.5962110675485313984640198168892890990631591794626600213856577033")}, std::complex<T>{stof<T>("-24.304981669340233451367398615815031096773874893103117716340620236"),stof<T>("-14.34088325755864559999643683914407833832648205559921145810565621")}, std::complex<T>{stof<T>("0.5757079148512954659270754509825125291615803813691621116413816283"),stof<T>("6.9015814860840388413468142253230877409152864765217422918421508204")}, std::complex<T>{stof<T>("20.309653700520288056354782047686781338399845407310436529594036038"),stof<T>("8.035512839023138157113642430710279696474354758540129187649163093")}, std::complex<T>{stof<T>("-0.87967591672810295777218924617445642026936795132549591364655243886"),stof<T>("0.64194487762304279435700210128213717713839885131132720351431713993")}, std::complex<T>{stof<T>("-1.7593518334562059155443784923489128405387359026509918272931048777"),stof<T>("1.2838897552460855887140042025642743542767977026226544070286342799")}, std::complex<T>{stof<T>("-0.87967591672810295777218924617445642026936795132549591364655243886"),stof<T>("0.64194487762304279435700210128213717713839885131132720351431713993")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[48].real()/kbase.W[48].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[127].real()/kbase.W[127].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_178_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_178_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("16.698764443474069206160665201944608905560321237595181428785895705"),stof<T>("-25.391305441754378670488762034206475720902068452424866888124944241")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dlog_W3(k,dl), dlog_W7(k,dl), dlog_W10(k,dl), dlog_W13(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), dlr[2], f_1_1(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[48].real()/k.W[48].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[127].real()/k.W[127].real()), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_178_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_178_DLogXconstant_part(base_point<T>, kend);
	value += f_4_178_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_178_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_178_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_178_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_178_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_178_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_178_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
