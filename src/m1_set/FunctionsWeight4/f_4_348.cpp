/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_348.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_348_abbreviated (const std::array<T,63>& abb) {
T z[103];
z[0] = abb[20] + abb[22];
z[1] = abb[30] * (T(1) / T(2));
z[0] = z[0] * z[1];
z[1] = abb[20] + abb[21];
z[2] = abb[22] + z[1];
z[3] = abb[27] * z[2];
z[4] = abb[23] * abb[27];
z[0] = z[0] + -z[3] + -z[4];
z[0] = abb[30] * z[0];
z[3] = -abb[25] + z[1];
z[3] = abb[27] * z[3];
z[5] = abb[25] * abb[29];
z[3] = z[3] + z[4] + (T(1) / T(2)) * z[5];
z[3] = abb[29] * z[3];
z[6] = abb[22] + -abb[24];
z[7] = prod_pow(abb[27], 2);
z[8] = (T(1) / T(2)) * z[7];
z[6] = z[6] * z[8];
z[8] = abb[24] + abb[25];
z[9] = abb[27] * z[8];
z[10] = abb[21] + abb[23];
z[11] = -abb[25] + z[10];
z[12] = abb[32] * z[11];
z[9] = z[9] + (T(1) / T(2)) * z[12];
z[9] = abb[32] * z[9];
z[13] = abb[23] + z[1];
z[14] = abb[22] + z[13];
z[15] = z[8] + -z[14];
z[16] = abb[57] * z[15];
z[17] = abb[33] + -abb[59];
z[17] = abb[23] * z[17];
z[18] = abb[21] + abb[24];
z[18] = abb[33] * z[18];
z[2] = -abb[25] + z[2];
z[2] = abb[59] * z[2];
z[19] = abb[34] * z[13];
z[20] = abb[61] * z[14];
z[21] = abb[22] + abb[25];
z[22] = abb[20] + z[21];
z[23] = abb[60] * z[22];
z[0] = z[0] + -z[2] + z[3] + z[6] + z[9] + z[16] + z[17] + z[18] + z[19] + -z[20] + -z[23];
z[2] = 3 * abb[20] + -abb[24] + abb[21] * (T(5) / T(2));
z[2] = 2 * abb[22] + abb[23] * (T(5) / T(4)) + (T(1) / T(2)) * z[2];
z[3] = prod_pow(m1_set::bc<T>[0], 2);
z[2] = z[2] * z[3];
z[0] = (T(3) / T(2)) * z[0] + -z[2];
z[2] = -abb[24] + z[1];
z[6] = -abb[25] + (T(1) / T(2)) * z[2];
z[6] = abb[27] * z[6];
z[6] = (T(1) / T(2)) * z[4] + z[6];
z[9] = abb[30] * z[11];
z[11] = -abb[25] + z[13];
z[11] = abb[29] * z[11];
z[9] = z[6] + z[9] + -z[11] + -z[12];
z[12] = abb[24] + z[13];
z[13] = abb[28] * z[12];
z[13] = z[9] + (T(1) / T(2)) * z[13];
z[16] = abb[31] * (T(3) / T(2));
z[13] = z[13] * z[16];
z[16] = abb[25] * abb[30];
z[8] = abb[32] * z[8];
z[5] = -z[5] + -z[6] + -z[8] + z[16];
z[5] = (T(3) / T(2)) * z[5];
z[6] = abb[28] * z[5];
z[16] = abb[26] * abb[62];
z[6] = z[0] + z[6] + z[13] + (T(3) / T(4)) * z[16];
z[13] = abb[53] + abb[54] + abb[56];
z[6] = -z[6] * z[13];
z[0] = -abb[55] * z[0];
z[16] = abb[44] * (T(1) / T(2));
z[17] = abb[47] * (T(1) / T(2));
z[18] = z[16] + z[17];
z[19] = abb[45] * (T(1) / T(2));
z[20] = abb[46] * (T(1) / T(2));
z[23] = z[19] + -z[20];
z[24] = z[18] + -z[23];
z[25] = -abb[50] + z[24];
z[26] = -abb[3] + -abb[6];
z[26] = z[25] * z[26];
z[25] = abb[4] * z[25];
z[27] = z[16] + -z[17];
z[28] = -z[23] + z[27];
z[29] = -abb[43] + abb[49];
z[30] = z[28] + z[29];
z[31] = abb[13] * z[30];
z[30] = abb[5] * z[30];
z[32] = abb[44] + abb[46];
z[33] = -abb[50] + z[32];
z[34] = -abb[45] + z[29];
z[35] = z[33] + z[34];
z[35] = abb[2] * z[35];
z[26] = z[25] + z[26] + z[30] + -z[31] + z[35];
z[26] = abb[34] * z[26];
z[30] = abb[45] + z[32];
z[35] = abb[47] * (T(3) / T(2));
z[36] = abb[52] + (T(1) / T(2)) * z[30] + -z[35];
z[37] = abb[8] * z[36];
z[38] = abb[15] * z[36];
z[39] = z[37] + -z[38];
z[40] = abb[46] * (T(3) / T(2));
z[41] = -z[17] + -z[19] + z[40];
z[42] = abb[44] * (T(3) / T(2));
z[43] = abb[52] + z[42];
z[44] = 2 * abb[50] + -z[41] + -z[43];
z[45] = abb[3] * z[44];
z[45] = z[25] + z[39] + z[45];
z[46] = abb[59] * z[45];
z[18] = z[18] + z[23];
z[47] = -abb[51] + z[18];
z[48] = abb[3] + abb[5];
z[49] = -z[47] * z[48];
z[47] = abb[7] * z[47];
z[50] = -abb[42] + abb[48];
z[23] = z[23] + z[27] + z[50];
z[27] = abb[14] * z[23];
z[51] = abb[44] + abb[45];
z[52] = -abb[51] + z[51];
z[53] = -abb[46] + z[50];
z[54] = z[52] + z[53];
z[54] = abb[1] * z[54];
z[49] = -z[27] + z[47] + z[49] + z[54];
z[49] = abb[33] * z[49];
z[54] = abb[45] * (T(3) / T(2));
z[17] = -z[17] + -z[20] + z[54];
z[43] = 2 * abb[51] + -z[17] + -z[43];
z[43] = z[43] * z[48];
z[48] = abb[47] + -abb[52];
z[52] = -z[48] + z[52];
z[52] = abb[12] * z[52];
z[55] = z[47] + z[52];
z[43] = z[39] + z[43] + z[55];
z[56] = abb[60] * z[43];
z[53] = z[48] + z[53];
z[53] = abb[9] * z[53];
z[57] = 3 * abb[48];
z[58] = 2 * abb[46] + abb[52];
z[59] = z[57] + -z[58];
z[51] = 3 * abb[42] + -z[51] + -z[59];
z[60] = abb[1] * z[51];
z[61] = z[27] + z[53] + z[60];
z[62] = -abb[52] + z[16];
z[63] = 2 * abb[42];
z[64] = 2 * abb[48] + -z[63];
z[41] = -z[41] + z[62] + z[64];
z[41] = abb[6] * z[41];
z[41] = z[39] + z[41] + -z[61];
z[65] = abb[57] * z[41];
z[66] = 2 * abb[43];
z[67] = 2 * abb[49] + -z[66];
z[17] = -z[17] + z[62] + z[67];
z[17] = abb[5] * z[17];
z[62] = 3 * abb[49];
z[68] = 2 * abb[45] + abb[52];
z[69] = z[62] + -z[68];
z[32] = 3 * abb[43] + -z[32] + -z[69];
z[70] = abb[2] * z[32];
z[71] = z[31] + z[70];
z[34] = z[34] + z[48];
z[34] = abb[10] * z[34];
z[17] = z[17] + -z[34] + z[39] + -z[71];
z[39] = abb[55] + z[13];
z[21] = (T(1) / T(2)) * z[21];
z[39] = z[21] * z[39];
z[39] = z[17] + z[39];
z[39] = abb[58] * z[39];
z[26] = z[26] + z[39] + z[46] + z[49] + z[56] + z[65];
z[33] = z[33] + -z[48];
z[33] = abb[11] * z[33];
z[39] = abb[42] + abb[43];
z[46] = abb[45] + abb[46];
z[48] = z[39] + z[46];
z[49] = 2 * abb[52];
z[56] = z[48] + z[49];
z[65] = abb[41] * (T(2) / T(3));
z[72] = -abb[48] + -abb[49] + (T(1) / T(3)) * z[56] + z[65];
z[72] = abb[0] * z[72];
z[30] = 3 * abb[47] + -z[30] + -z[49];
z[73] = abb[8] * z[30];
z[72] = z[33] + z[52] + -z[72] + z[73];
z[73] = z[25] + z[34] + z[47] + z[53];
z[74] = z[27] + z[31];
z[75] = abb[52] * (T(-5) / T(6)) + abb[44] * (T(1) / T(3));
z[76] = abb[46] * (T(7) / T(6));
z[77] = abb[51] * (T(1) / T(2)) + abb[45] * (T(1) / T(3)) + z[64] + z[75] + -z[76];
z[77] = abb[1] * z[77];
z[78] = abb[45] * (T(7) / T(6));
z[75] = abb[50] * (T(1) / T(2)) + abb[46] * (T(1) / T(3)) + z[67] + z[75] + -z[78];
z[75] = abb[2] * z[75];
z[79] = abb[46] * (T(3) / T(4));
z[80] = abb[45] * (T(1) / T(4));
z[81] = abb[52] * (T(1) / T(2));
z[82] = z[79] + -z[80] + z[81];
z[65] = abb[47] * (T(5) / T(4)) + z[65];
z[83] = abb[43] * (T(-7) / T(6)) + abb[51] * (T(-3) / T(2)) + abb[42] * (T(-2) / T(3)) + abb[49] * (T(1) / T(2)) + abb[44] * (T(17) / T(12)) + z[65] + -z[82];
z[83] = abb[5] * z[83];
z[84] = abb[50] + abb[51];
z[76] = abb[47] + abb[52] * (T(7) / T(3)) + abb[44] * (T(17) / T(3)) + z[76] + z[78] + (T(-9) / T(2)) * z[84];
z[76] = abb[3] * z[76];
z[78] = abb[45] * (T(3) / T(4));
z[85] = abb[46] * (T(1) / T(4));
z[86] = z[78] + z[81] + -z[85];
z[87] = 3 * abb[50];
z[88] = -7 * abb[42] + abb[44] * (T(17) / T(2));
z[88] = abb[48] + -z[87] + (T(1) / T(3)) * z[88];
z[65] = abb[43] * (T(-2) / T(3)) + z[65] + -z[86] + (T(1) / T(2)) * z[88];
z[65] = abb[6] * z[65];
z[65] = -z[38] + z[65] + -2 * z[72] + (T(1) / T(2)) * z[73] + -z[74] + z[75] + z[76] + z[77] + z[83];
z[3] = z[3] * z[65];
z[65] = 2 * abb[5];
z[32] = z[32] * z[65];
z[72] = 3 * z[34];
z[73] = abb[3] * z[36];
z[32] = z[32] + z[38] + 3 * z[71] + z[72] + -z[73];
z[71] = abb[6] * z[36];
z[75] = z[32] + -z[71];
z[75] = abb[29] * z[75];
z[76] = abb[48] * (T(3) / T(2));
z[77] = -abb[52] + z[76];
z[83] = abb[49] * (T(3) / T(2));
z[48] = abb[41] + (T(1) / T(2)) * z[48] + -z[77] + -z[83];
z[48] = abb[0] * z[48];
z[27] = (T(3) / T(2)) * z[27];
z[88] = -z[27] + z[48] + -z[60];
z[31] = (T(3) / T(2)) * z[31];
z[72] = z[31] + 2 * z[70] + z[72] + z[88];
z[89] = abb[47] * (T(3) / T(4));
z[90] = abb[41] * (T(1) / T(2));
z[91] = z[89] + -z[90];
z[92] = 4 * abb[43];
z[93] = abb[49] * (T(9) / T(2)) + z[91] + -z[92];
z[94] = abb[45] * (T(13) / T(4));
z[95] = abb[46] * (T(5) / T(4));
z[96] = abb[42] + z[42];
z[96] = -z[49] + z[93] + -z[94] + z[95] + (T(1) / T(2)) * z[96];
z[96] = abb[5] * z[96];
z[96] = -z[38] + -z[72] + z[96];
z[96] = abb[27] * z[96];
z[97] = abb[43] * (T(1) / T(2)) + -z[90];
z[98] = z[63] + z[97];
z[99] = abb[44] * (T(3) / T(4));
z[77] = -z[77] + -z[80] + -z[89] + z[95] + z[98] + -z[99];
z[95] = abb[6] * abb[27];
z[77] = z[77] * z[95];
z[31] = z[31] + z[70];
z[88] = -z[31] + z[88];
z[83] = z[66] + -z[83];
z[78] = z[78] + -z[79];
z[79] = abb[44] * (T(5) / T(2));
z[100] = -abb[42] + z[79];
z[91] = -z[78] + -z[83] + -z[91] + (T(1) / T(2)) * z[100];
z[91] = abb[5] * z[91];
z[76] = -z[76] + z[98];
z[98] = abb[44] * (T(5) / T(4));
z[78] = -z[76] + z[78] + -z[89] + z[98];
z[78] = abb[6] * z[78];
z[12] = abb[55] * z[12];
z[12] = (T(-3) / T(4)) * z[12] + z[73] + z[78] + z[88] + z[91];
z[12] = abb[28] * z[12];
z[78] = 3 * abb[51];
z[91] = 2 * abb[44];
z[68] = abb[46] + -z[68] + z[78] + -z[91];
z[100] = 2 * abb[3];
z[101] = z[68] * z[100];
z[55] = 3 * z[55] + z[101];
z[101] = z[65] * z[68];
z[101] = -z[38] + z[55] + z[71] + z[101];
z[102] = abb[30] + -abb[32];
z[101] = z[101] * z[102];
z[102] = -abb[1] + abb[3];
z[68] = z[68] * z[102];
z[102] = abb[41] + -abb[42];
z[66] = abb[46] + -z[66] + z[69] + -z[102];
z[66] = abb[0] * z[66];
z[65] = -abb[6] + z[65];
z[39] = -abb[41] + -abb[44] + z[39];
z[65] = z[39] * z[65];
z[65] = z[65] + z[66] + z[68] + z[70];
z[65] = abb[31] * z[65];
z[66] = abb[55] * (T(3) / T(2));
z[9] = -z[9] * z[66];
z[9] = z[9] + z[12] + z[65] + z[75] + z[77] + z[96] + z[101];
z[9] = abb[31] * z[9];
z[12] = z[27] + -z[31] + z[48] + 3 * z[53] + 2 * z[60];
z[27] = z[83] + -z[90];
z[31] = abb[45] * (T(5) / T(4));
z[42] = abb[42] + -z[42];
z[42] = abb[52] + z[27] + z[31] + (T(1) / T(2)) * z[42] + -z[85] + -z[89];
z[42] = abb[5] * z[42];
z[42] = -z[12] + -z[38] + z[42];
z[42] = abb[27] * z[42];
z[48] = abb[45] + -z[58] + z[87] + -z[91];
z[58] = z[48] * z[100];
z[65] = 3 * z[33];
z[58] = 3 * z[25] + z[58] + z[65];
z[68] = 2 * abb[6];
z[69] = z[48] * z[68];
z[36] = abb[5] * z[36];
z[69] = z[36] + -z[38] + z[58] + z[69];
z[77] = -abb[29] + abb[30];
z[69] = z[69] * z[77];
z[77] = z[36] + z[73];
z[51] = z[51] * z[68];
z[51] = z[38] + z[51] + 3 * z[61] + -z[77];
z[61] = abb[32] * z[51];
z[83] = 4 * abb[42];
z[89] = abb[48] * (T(9) / T(2)) + -z[83] + z[89] + z[97];
z[90] = abb[46] * (T(13) / T(4));
z[31] = z[31] + -z[49] + z[89] + -z[90] + z[99];
z[31] = z[31] * z[95];
z[91] = -abb[2] + abb[3];
z[48] = z[48] * z[91];
z[91] = abb[41] + -abb[43];
z[59] = abb[45] + z[59] + -z[63] + -z[91];
z[59] = abb[0] * z[59];
z[63] = -abb[5] + z[68];
z[39] = z[39] * z[63];
z[39] = z[39] + z[48] + z[59] + z[60];
z[39] = abb[28] * z[39];
z[5] = -abb[55] * z[5];
z[5] = z[5] + z[31] + z[39] + z[42] + z[61] + z[69];
z[5] = abb[28] * z[5];
z[31] = -abb[27] * z[32];
z[32] = abb[44] * (T(1) / T(4));
z[39] = abb[47] * (T(1) / T(4));
z[42] = z[32] + z[39];
z[29] = -z[29] + -z[42] + z[86];
z[29] = abb[5] * z[29];
z[48] = (T(1) / T(2)) * z[38];
z[59] = (T(1) / T(2)) * z[33];
z[63] = abb[45] + -abb[50] + abb[52] + -z[67];
z[63] = abb[2] * z[63];
z[29] = (T(1) / T(2)) * z[25] + z[29] + z[34] + z[48] + -z[59] + z[63];
z[29] = abb[29] * z[29];
z[34] = abb[27] * z[71];
z[29] = 3 * z[29] + z[31] + z[34];
z[29] = abb[29] * z[29];
z[31] = -abb[44] + z[81];
z[63] = abb[42] * (T(1) / T(2));
z[20] = abb[45] + -z[20] + z[27] + z[31] + z[63];
z[20] = abb[5] * z[20];
z[20] = z[20] + (T(-3) / T(2)) * z[37] + z[38] + (T(-1) / T(2)) * z[73] + -z[88];
z[20] = z[7] * z[20];
z[38] = -abb[27] * z[51];
z[47] = -z[47] + z[52];
z[42] = -z[42] + -z[50] + z[82];
z[42] = abb[6] * z[42];
z[50] = abb[46] + -abb[51] + abb[52] + -z[64];
z[50] = abb[1] * z[50];
z[42] = z[42] + (T(-1) / T(2)) * z[47] + z[48] + z[50] + z[53];
z[42] = abb[32] * z[42];
z[38] = z[38] + 3 * z[42];
z[38] = abb[32] * z[38];
z[39] = -z[39] + -z[80] + -z[81] + z[84] + -z[85] + -z[98];
z[39] = abb[3] * z[39];
z[25] = -z[25] + z[47];
z[42] = z[25] + -z[37];
z[39] = z[39] + (T(1) / T(2)) * z[42] + z[59];
z[39] = abb[30] * z[39];
z[42] = abb[15] * z[30];
z[37] = 3 * z[37] + z[42] + -z[77];
z[47] = abb[27] * z[37];
z[34] = -z[34] + 3 * z[39] + z[47];
z[34] = abb[30] * z[34];
z[24] = -abb[43] + -abb[48] + z[24];
z[24] = abb[16] * z[24];
z[18] = -abb[42] + z[18];
z[18] = abb[17] * z[18];
z[28] = -abb[43] + z[28];
z[28] = abb[18] * z[28];
z[39] = abb[19] * z[23];
z[47] = abb[16] + abb[17];
z[47] = abb[41] * z[47];
z[48] = abb[17] + abb[18];
z[48] = abb[49] * z[48];
z[18] = z[18] + z[24] + -z[28] + -z[39] + z[47] + -z[48];
z[24] = abb[26] * (T(1) / T(2));
z[24] = -abb[55] * z[24];
z[24] = z[18] + z[24];
z[24] = abb[62] * z[24];
z[28] = abb[59] * z[44];
z[23] = abb[33] * z[23];
z[23] = z[23] + z[28];
z[19] = abb[46] + -z[19] + z[31] + z[76];
z[7] = z[7] * z[19];
z[7] = z[7] + 3 * z[23];
z[7] = abb[6] * z[7];
z[19] = z[37] + -z[71];
z[23] = abb[61] * z[19];
z[28] = abb[59] * z[65];
z[0] = z[0] + z[3] + z[5] + z[6] + z[7] + z[9] + z[20] + z[23] + (T(3) / T(2)) * z[24] + 3 * z[26] + z[28] + z[29] + z[34] + z[38];
z[3] = abb[40] * (T(1) / T(2));
z[5] = z[3] * z[18];
z[6] = abb[35] * z[41];
z[7] = abb[38] * z[43];
z[9] = abb[6] * z[44];
z[9] = z[9] + z[33] + z[45];
z[9] = abb[37] * z[9];
z[18] = abb[55] * z[21];
z[17] = z[17] + z[18];
z[17] = abb[36] * z[17];
z[5] = z[5] + z[6] + z[7] + z[9] + z[17];
z[6] = 4 * abb[44] + z[46] + z[49] + -z[78] + -z[87];
z[6] = z[6] * z[100];
z[6] = z[6] + -3 * z[25] + z[36] + -z[42] + -z[65] + z[71];
z[6] = abb[30] * z[6];
z[7] = -z[40] + z[54];
z[9] = -z[35] + z[79];
z[17] = -z[7] + z[9] + z[62] + -z[92] + z[102];
z[17] = abb[5] * z[17];
z[18] = -z[60] + -z[70];
z[20] = 2 * abb[41] + z[56] + -z[57] + -z[62];
z[20] = abb[0] * z[20];
z[23] = -abb[3] * z[30];
z[17] = z[17] + 2 * z[18] + z[20] + z[23] + -3 * z[74];
z[17] = abb[27] * z[17];
z[7] = z[7] + z[9] + z[57] + -z[83] + z[91];
z[7] = z[7] * z[95];
z[6] = z[6] + z[7] + z[17] + z[61] + z[75];
z[6] = m1_set::bc<T>[0] * z[6];
z[7] = 4 * abb[52] + abb[44] * (T(13) / T(4));
z[9] = -6 * abb[51] + abb[45] * (T(29) / T(4)) + z[7] + -z[63] + -z[90] + -z[93];
z[9] = abb[5] * z[9];
z[17] = abb[47] * (T(9) / T(4)) + -z[49];
z[18] = abb[46] * (T(-7) / T(4)) + z[17] + z[32] + -z[76] + -z[80];
z[18] = abb[6] * z[18];
z[9] = z[9] + z[18] + -z[42] + -z[55] + z[72];
z[9] = m1_set::bc<T>[0] * z[9];
z[18] = 3 * abb[55];
z[20] = abb[20] + 3 * abb[21] + -abb[24];
z[20] = -abb[25] + abb[23] * (T(3) / T(4)) + (T(1) / T(4)) * z[20];
z[20] = m1_set::bc<T>[0] * z[20];
z[23] = z[18] * z[20];
z[9] = z[9] + z[23];
z[9] = abb[31] * z[9];
z[1] = abb[24] + z[1];
z[1] = abb[27] * z[1];
z[10] = abb[30] * z[10];
z[1] = z[1] + z[4] + -z[8] + z[10] + -z[11];
z[1] = m1_set::bc<T>[0] * z[1];
z[4] = abb[35] * z[15];
z[8] = -abb[25] + z[14];
z[8] = abb[37] * z[8];
z[10] = abb[39] * z[14];
z[11] = abb[38] * z[22];
z[3] = abb[26] * z[3];
z[1] = z[1] + z[3] + z[4] + -z[8] + -z[10] + -z[11];
z[3] = abb[36] * z[21];
z[2] = abb[23] + z[2];
z[2] = -abb[25] + (T(1) / T(4)) * z[2];
z[2] = m1_set::bc<T>[0] * z[2];
z[4] = abb[28] * z[2];
z[8] = abb[31] * z[20];
z[3] = (T(1) / T(2)) * z[1] + -z[3] + z[4] + -z[8];
z[4] = -3 * z[13];
z[3] = z[3] * z[4];
z[1] = -z[1] * z[66];
z[4] = -abb[42] + z[16];
z[4] = abb[45] * (T(-7) / T(4)) + (T(1) / T(2)) * z[4] + z[17] + -z[27] + -z[85];
z[4] = abb[5] * z[4];
z[7] = -6 * abb[50] + abb[46] * (T(29) / T(4)) + z[7] + -z[89] + -z[94];
z[7] = abb[6] * z[7];
z[4] = z[4] + z[7] + z[12] + -z[42] + -z[58];
z[4] = m1_set::bc<T>[0] * z[4];
z[2] = -z[2] * z[18];
z[2] = z[2] + z[4];
z[2] = abb[28] * z[2];
z[4] = abb[39] * z[19];
z[1] = z[1] + z[2] + z[3] + z[4] + 3 * z[5] + z[6] + z[9];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_348_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("3.162273066096953499292856997926669629182883935648798084600840359"),stof<T>("-14.633300484517895006119187001685607389125752937927857609262389682")}, std::complex<T>{stof<T>("23.879381808358947358200499996971327984902276670437873584039445088"),stof<T>("-4.964525490228577077160159552622320638799579908595529274791218172")}, std::complex<T>{stof<T>("23.879381808358947358200499996971327984902276670437873584039445088"),stof<T>("-4.964525490228577077160159552622320638799579908595529274791218172")}, std::complex<T>{stof<T>("-7.561740476449930820768828760520971509047809643303704910743524227"),stof<T>("-39.662712088828239133034886373294235944962432921291617174748218184")}, std::complex<T>{stof<T>("-2.2302070825634306876494695339139159103281248486824463008880886316"),stof<T>("-8.1334732170627854074240357331830586106145669122790921519215021209")}, std::complex<T>{stof<T>("-2.2302070825634306876494695339139159103281248486824463008880886316"),stof<T>("-8.1334732170627854074240357331830586106145669122790921519215021209")}, std::complex<T>{stof<T>("52.280124536618640145522906367004017731252127027950625545488560833"),stof<T>("-31.691170685552146663778249547492250169229078027294164725907035726")}, std::complex<T>{stof<T>("-27.041654874455900857493356994897997614085160606086671668640285447"),stof<T>("19.597825974746472083279346554307928027925332846523386884053607854")}, std::complex<T>{stof<T>("-27.041654874455900857493356994897997614085160606086671668640285447"),stof<T>("19.597825974746472083279346554307928027925332846523386884053607854")}, std::complex<T>{stof<T>("-18.547848414472447225081140770364272386182591875816614974184009492"),stof<T>("36.493764361994030802771010192733497973147445917608054297617934235")}, std::complex<T>{stof<T>("-18.547848414472447225081140770364272386182591875816614974184009492"),stof<T>("36.493764361994030802771010192733497973147445917608054297617934235")}, std::complex<T>{stof<T>("-4.460414165126861375298939067827831820656249697364892601776177263"),stof<T>("-16.266946434125570814848071466366117221229133824558184303843004242")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_348_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_348_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("4.239758373616595838341639494337243101699632920623711066618063497"),stof<T>("100.330287989232514479659749259571264625722024222815105903458548292")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,63> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W28(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_16(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k)};

                    
            return f_4_348_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_348_DLogXconstant_part(base_point<T>, kend);
	value += f_4_348_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_348_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_348_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_348_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_348_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_348_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_348_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
