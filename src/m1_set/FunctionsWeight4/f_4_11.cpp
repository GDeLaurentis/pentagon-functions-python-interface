/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_11.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_11_abbreviated (const std::array<T,29>& abb) {
T z[39];
z[0] = abb[18] * (T(1) / T(2));
z[1] = -abb[22] + z[0];
z[2] = abb[20] * (T(1) / T(2));
z[3] = z[1] + -z[2];
z[4] = abb[24] * (T(3) / T(2));
z[5] = 2 * abb[19] + abb[21];
z[6] = 2 * z[5];
z[7] = z[3] + -z[4] + z[6];
z[7] = abb[0] * z[7];
z[8] = abb[25] + abb[26];
z[9] = abb[7] * z[8];
z[7] = z[7] + (T(-3) / T(2)) * z[9];
z[10] = 8 * z[5];
z[11] = 3 * abb[22];
z[12] = abb[24] * (T(-9) / T(2)) + abb[20] * (T(-7) / T(2)) + -z[0] + z[10] + -z[11];
z[12] = abb[3] * z[12];
z[13] = 4 * z[5];
z[14] = 3 * abb[24];
z[15] = z[13] + -z[14];
z[16] = abb[20] + abb[22];
z[17] = z[15] + -z[16];
z[18] = 4 * abb[1];
z[18] = z[17] * z[18];
z[19] = -abb[24] + z[6] + -z[16];
z[20] = 3 * abb[6];
z[20] = z[19] * z[20];
z[21] = abb[20] + z[6];
z[22] = -z[14] + z[21];
z[23] = -abb[18] + -z[22];
z[23] = abb[2] * z[23];
z[12] = -z[7] + z[12] + z[18] + -z[20] + z[23];
z[12] = abb[14] * z[12];
z[0] = abb[20] * (T(5) / T(2)) + -z[0] + z[4] + z[11] + -z[13];
z[0] = abb[3] * z[0];
z[4] = 2 * abb[24] + -z[6];
z[23] = -abb[20] + abb[23];
z[24] = z[4] + -z[23];
z[24] = abb[5] * z[24];
z[25] = 2 * abb[1];
z[25] = z[17] * z[25];
z[26] = 3 * z[24] + -z[25];
z[7] = -z[7] + z[20];
z[27] = 3 * abb[23];
z[28] = -z[14] + 2 * z[21] + -z[27];
z[29] = -abb[18] + z[28];
z[29] = abb[2] * z[29];
z[0] = z[0] + z[7] + z[26] + z[29];
z[0] = abb[13] * z[0];
z[29] = z[6] + -z[14];
z[30] = z[16] + z[29];
z[31] = abb[4] * z[30];
z[32] = z[20] + z[31];
z[33] = 2 * abb[3];
z[34] = z[17] * z[33];
z[30] = abb[2] * z[30];
z[18] = z[18] + -z[30] + -z[32] + z[34];
z[30] = 2 * abb[12];
z[30] = z[18] * z[30];
z[31] = z[25] + -z[31];
z[34] = abb[18] + -abb[20] + -2 * abb[22];
z[29] = -z[29] + z[34];
z[29] = abb[2] * z[29];
z[3] = abb[24] * (T(-15) / T(2)) + z[3] + z[10];
z[3] = abb[3] * z[3];
z[3] = z[3] + -z[7] + z[29] + 2 * z[31];
z[7] = -abb[11] * z[3];
z[0] = z[0] + z[7] + z[12] + z[30];
z[0] = abb[13] * z[0];
z[7] = -abb[20] + z[5];
z[10] = 2 * z[7];
z[11] = abb[18] + z[10] + -z[11];
z[11] = abb[2] * z[11];
z[12] = abb[4] * z[19];
z[29] = abb[18] + abb[20] + -z[15];
z[31] = abb[3] * z[29];
z[35] = z[5] + z[34];
z[35] = abb[0] * z[35];
z[36] = 3 * z[8];
z[36] = abb[8] * z[36];
z[37] = z[25] + z[36];
z[9] = 3 * z[9];
z[11] = -z[9] + -z[11] + -3 * z[12] + z[20] + z[31] + 4 * z[35] + -z[37];
z[11] = abb[11] * z[11];
z[3] = abb[14] * z[3];
z[3] = z[3] + z[11] + z[30];
z[3] = abb[11] * z[3];
z[12] = abb[18] * (T(13) / T(3));
z[21] = abb[24] + abb[22] * (T(-8) / T(3)) + z[12] + (T(-4) / T(3)) * z[21] + z[27];
z[21] = abb[2] * z[21];
z[31] = -8 * abb[20] + -z[5];
z[12] = abb[22] * (T(5) / T(3)) + -z[12] + z[14] + (T(1) / T(3)) * z[31];
z[12] = abb[3] * z[12];
z[14] = -abb[24] + z[5];
z[31] = abb[23] * (T(1) / T(2));
z[38] = -z[2] + z[14] + z[31];
z[38] = abb[5] * z[38];
z[2] = abb[22] + -z[2] + z[6];
z[2] = abb[24] + (T(-1) / T(3)) * z[2] + -z[31];
z[2] = abb[1] * z[2];
z[6] = abb[6] * z[19];
z[31] = -2 * abb[20] + 5 * z[5];
z[31] = -abb[24] + abb[22] * (T(-2) / T(3)) + (T(1) / T(3)) * z[31];
z[31] = abb[4] * z[31];
z[2] = z[2] + -z[6] + z[12] + z[21] + 4 * z[31] + (T(-13) / T(3)) * z[35] + 5 * z[38];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[4] = z[4] + z[23];
z[4] = abb[2] * z[4];
z[12] = z[19] * z[33];
z[4] = -z[4] + 2 * z[6] + -z[12] + -z[25];
z[6] = -abb[28] * z[4];
z[12] = abb[0] + abb[3];
z[12] = abb[2] + (T(3) / T(2)) * z[12];
z[12] = z[8] * z[12];
z[8] = -abb[10] * z[8];
z[14] = abb[8] * z[14];
z[8] = z[8] + z[14];
z[14] = abb[20] + -abb[24];
z[1] = -z[1] + (T(1) / T(2)) * z[14];
z[1] = abb[7] * z[1];
z[14] = abb[9] * z[19];
z[1] = z[1] + 2 * z[8] + z[12] + z[14];
z[1] = abb[15] * z[1];
z[8] = prod_pow(abb[12], 2);
z[12] = -abb[28] + -z[8];
z[12] = z[12] * z[24];
z[1] = z[1] + z[6] + z[12];
z[6] = 9 * abb[24];
z[12] = abb[20] + -z[13];
z[12] = 7 * abb[22] + z[6] + 4 * z[12] + z[27];
z[12] = abb[1] * z[12];
z[5] = 10 * z[5] + -z[6] + -z[16];
z[5] = abb[4] * z[5];
z[6] = -abb[22] + z[28];
z[6] = abb[2] * z[6];
z[13] = 4 * abb[3];
z[14] = -z[13] * z[17];
z[5] = z[5] + z[6] + 2 * z[12] + z[14] + z[20];
z[5] = z[5] * z[8];
z[6] = z[15] + z[34];
z[8] = abb[0] * z[6];
z[12] = -abb[2] * z[29];
z[7] = abb[18] + -z[7];
z[7] = z[7] * z[13];
z[7] = z[7] + z[8] + z[12] + -z[25] + z[36];
z[7] = abb[14] * z[7];
z[7] = z[7] + -z[30];
z[7] = abb[14] * z[7];
z[12] = abb[27] * z[18];
z[0] = z[0] + 3 * z[1] + z[2] + z[3] + z[5] + z[7] + 2 * z[12];
z[1] = -abb[12] * z[18];
z[2] = z[8] + -z[32];
z[3] = 2 * abb[18] + -abb[22];
z[5] = z[3] + -z[28];
z[5] = abb[2] * z[5];
z[6] = abb[3] * z[6];
z[5] = z[2] + z[5] + z[6] + -z[9] + -z[26];
z[5] = abb[13] * z[5];
z[3] = -z[3] + -z[22];
z[3] = z[3] * z[33];
z[6] = abb[18] + abb[22] + -z[10];
z[6] = abb[2] * z[6];
z[2] = -z[2] + z[3] + z[6] + -z[37];
z[2] = abb[14] * z[2];
z[1] = z[1] + z[2] + z[5] + -z[11];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[16] * z[18];
z[1] = z[1] + z[2];
z[2] = -z[4] + -z[24];
z[2] = abb[17] * z[2];
z[1] = 2 * z[1] + 3 * z[2];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_11_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-18.959443965275373902985288861958725493858885815901869383366351011"),stof<T>("15.58728128082497075093557636648036379123427287038040098584118758")}, std::complex<T>{stof<T>("7.29066661917076727356158130168775612123402945215260583749819591"),stof<T>("207.17350519872560019844394479365795599609582026535228560125707756")}, std::complex<T>{stof<T>("-60.131996987868912823259793315265656989122730483489743877265890411"),stof<T>("-61.050357396397359974557623481929038171749421762292890907765600958")}, std::complex<T>{stof<T>("3.645333309585383636780790650843878060617014726076302918749097957"),stof<T>("103.586752599362800099221972396828977998047910132676142800628538781")}, std::complex<T>{stof<T>("14.470992220823802754968917857785885663283930959861970428652489076"),stof<T>("-102.770320302245361239538219678772280499614144373765980554258950138")}, std::complex<T>{stof<T>("55.643545243417341675243422311092817158547775627449844922552028476"),stof<T>("-26.132681625023030514045019830362878536630449741092688660652161599")}, std::complex<T>{stof<T>("0.843118434866187511235580353328961769957940129963596035964763978"),stof<T>("-16.403713577942409610619329084537061289668038629290563232210776224")}, std::complex<T>{stof<T>("-0.466587119561924118219320910239394343139074717472046424179137927"),stof<T>("47.246795076652142140372750836458559900495186049435789623190676203")}, std::complex<T>{stof<T>("-0.466587119561924118219320910239394343139074717472046424179137927"),stof<T>("47.246795076652142140372750836458559900495186049435789623190676203")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_11_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_11_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("107.843456416863119401093077894541821529440754104644881943746560112"),stof<T>("-49.7680708994789113061447116959432323102822341359427172762299492")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dl[5], dl[2], dl[4], dlog_W15(k,dl), dlog_W22(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_4_re(k), f_2_7_re(k)};

                    
            return f_4_11_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_11_DLogXconstant_part(base_point<T>, kend);
	value += f_4_11_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_11_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_11_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_11_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_11_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_11_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_11_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
