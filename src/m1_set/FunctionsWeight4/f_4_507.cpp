/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_507.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_507_abbreviated (const std::array<T,60>& abb) {
T z[95];
z[0] = abb[30] * (T(1) / T(2));
z[1] = abb[29] + -abb[34] + z[0];
z[2] = abb[22] * z[1];
z[3] = abb[21] + abb[23];
z[4] = -abb[25] + z[3];
z[5] = abb[31] * z[4];
z[6] = abb[25] * abb[34];
z[7] = abb[25] * abb[32];
z[8] = abb[22] + z[4];
z[9] = -abb[33] * z[8];
z[2] = z[2] + z[5] + -z[6] + z[7] + z[9];
z[2] = abb[30] * z[2];
z[7] = abb[22] * (T(1) / T(2));
z[9] = abb[20] * (T(1) / T(2)) + z[7];
z[10] = abb[29] + -abb[31];
z[9] = z[9] * z[10];
z[11] = abb[31] * (T(1) / T(2));
z[12] = abb[26] * z[11];
z[13] = abb[26] * (T(1) / T(2));
z[14] = abb[25] + z[13];
z[15] = abb[29] * z[14];
z[9] = -z[6] + z[9] + z[12] + z[15];
z[9] = abb[32] * z[9];
z[11] = abb[34] + z[11];
z[12] = abb[29] * z[11];
z[15] = abb[31] * abb[34];
z[15] = -abb[37] + z[15];
z[16] = prod_pow(abb[34], 2);
z[12] = -abb[35] + abb[38] + -z[12] + z[15] + (T(1) / T(2)) * z[16];
z[17] = abb[32] * (T(1) / T(2));
z[10] = z[10] * z[17];
z[18] = prod_pow(abb[33], 2);
z[1] = -abb[33] + z[1];
z[1] = abb[30] * z[1];
z[1] = abb[36] + -abb[56] + z[1] + z[10] + z[12] + (T(1) / T(2)) * z[18];
z[1] = abb[24] * z[1];
z[10] = -abb[31] * z[14];
z[14] = -abb[29] * z[13];
z[10] = -z[6] + z[10] + z[14];
z[10] = abb[29] * z[10];
z[12] = abb[22] * z[12];
z[14] = -abb[29] + abb[32];
z[18] = abb[25] + abb[26];
z[19] = -z[14] * z[18];
z[20] = z[4] + z[7];
z[20] = abb[33] * z[20];
z[5] = -z[5] + z[19] + z[20];
z[5] = abb[33] * z[5];
z[19] = abb[29] * (T(1) / T(2)) + -z[11];
z[19] = abb[29] * z[19];
z[15] = z[15] + z[19];
z[15] = abb[20] * z[15];
z[19] = abb[25] * z[16];
z[20] = abb[31] * z[6];
z[18] = -abb[35] * z[18];
z[21] = abb[27] * (T(1) / T(2));
z[22] = -abb[58] * z[21];
z[23] = abb[26] + z[3];
z[24] = -abb[57] * z[23];
z[25] = abb[22] + abb[25];
z[25] = abb[36] * z[25];
z[26] = -abb[56] * z[8];
z[27] = abb[20] + abb[25];
z[28] = -abb[55] * z[27];
z[1] = z[1] + z[2] + z[5] + z[9] + z[10] + z[12] + z[15] + z[18] + z[19] + z[20] + z[22] + z[24] + z[25] + z[26] + z[28];
z[2] = abb[53] * (T(1) / T(2));
z[1] = z[1] * z[2];
z[5] = -abb[44] + -abb[46] + abb[47] + abb[48];
z[9] = -abb[45] + (T(1) / T(2)) * z[5];
z[10] = 2 * abb[54] + -z[9];
z[12] = abb[18] * z[10];
z[15] = -abb[49] + abb[50];
z[18] = abb[13] * z[15];
z[12] = z[12] + -z[18];
z[18] = abb[14] * z[15];
z[19] = abb[50] + abb[51];
z[20] = abb[10] * z[19];
z[22] = abb[19] * z[10];
z[22] = -z[12] + z[18] + -z[20] + -z[22];
z[24] = -abb[49] + abb[51];
z[25] = abb[2] * z[24];
z[26] = abb[49] + abb[50];
z[28] = abb[52] + z[26];
z[29] = abb[12] * z[28];
z[30] = z[25] + -z[29];
z[31] = abb[51] + abb[52];
z[32] = z[26] + z[31];
z[33] = abb[8] * z[32];
z[34] = -z[26] + z[31];
z[35] = abb[3] * z[34];
z[36] = abb[50] + abb[52];
z[37] = abb[49] * (T(1) / T(2));
z[38] = (T(2) / T(3)) * z[36] + z[37];
z[38] = abb[7] * z[38];
z[39] = abb[52] * (T(1) / T(2));
z[40] = abb[51] * (T(1) / T(2));
z[41] = z[39] + z[40];
z[42] = abb[50] * (T(1) / T(3)) + -z[41];
z[42] = abb[0] * z[42];
z[43] = abb[49] + abb[50] * (T(5) / T(3));
z[43] = (T(1) / T(3)) * z[31] + (T(1) / T(2)) * z[43];
z[43] = abb[5] * z[43];
z[37] = abb[50] + (T(1) / T(6)) * z[31] + z[37];
z[37] = abb[4] * z[37];
z[44] = abb[20] + abb[22];
z[45] = -abb[24] + -abb[26] + (T(-5) / T(2)) * z[3] + -z[44];
z[45] = abb[53] * z[45];
z[9] = -abb[54] + (T(1) / T(2)) * z[9];
z[46] = abb[16] * z[9];
z[47] = abb[1] * z[36];
z[48] = abb[17] * z[9];
z[22] = (T(-1) / T(3)) * z[22] + (T(2) / T(3)) * z[30] + (T(-5) / T(12)) * z[33] + (T(1) / T(12)) * z[35] + z[37] + z[38] + z[42] + z[43] + (T(1) / T(6)) * z[45] + -z[46] + z[47] + (T(-5) / T(3)) * z[48];
z[22] = prod_pow(m1_set::bc<T>[0], 2) * z[22];
z[30] = (T(1) / T(2)) * z[32];
z[35] = abb[8] * z[30];
z[37] = z[24] + z[36];
z[38] = abb[15] * (T(1) / T(2));
z[38] = z[37] * z[38];
z[42] = z[35] + -z[38];
z[43] = -abb[51] + abb[52];
z[45] = -z[26] + z[43];
z[48] = abb[6] * z[45];
z[49] = (T(1) / T(4)) * z[48];
z[50] = -abb[49] + abb[52];
z[51] = abb[9] * z[50];
z[52] = z[49] + -z[51];
z[53] = 3 * abb[49];
z[54] = abb[50] + z[53];
z[55] = -z[31] + z[54];
z[56] = abb[0] * (T(1) / T(4));
z[57] = -z[55] * z[56];
z[53] = z[36] + z[53];
z[58] = abb[51] + -z[53];
z[59] = abb[5] * (T(1) / T(4));
z[58] = z[58] * z[59];
z[60] = abb[4] * z[32];
z[61] = (T(1) / T(2)) * z[15];
z[62] = abb[52] + z[61];
z[62] = abb[7] * z[62];
z[57] = (T(1) / T(2)) * z[20] + -z[29] + z[42] + z[46] + (T(3) / T(2)) * z[47] + z[52] + z[57] + z[58] + (T(-1) / T(4)) * z[60] + z[62];
z[57] = abb[33] * z[57];
z[58] = -abb[51] + 3 * abb[52] + z[54];
z[60] = abb[0] * (T(1) / T(2));
z[58] = z[58] * z[60];
z[58] = z[38] + z[58];
z[36] = abb[7] * z[36];
z[36] = -z[29] + z[36];
z[62] = -z[18] + 4 * z[47];
z[63] = abb[5] * (T(1) / T(2));
z[64] = z[37] * z[63];
z[65] = abb[4] * z[26];
z[36] = 2 * z[36] + -z[58] + z[62] + z[64] + z[65];
z[36] = z[14] * z[36];
z[66] = abb[16] * z[10];
z[67] = z[35] + -z[66];
z[68] = abb[7] * z[26];
z[69] = z[38] + z[68];
z[70] = z[67] + -z[69];
z[71] = 3 * abb[50];
z[72] = abb[49] + z[71];
z[73] = z[31] + z[72];
z[74] = z[60] * z[73];
z[75] = abb[5] * abb[49];
z[76] = abb[4] * abb[49];
z[74] = -z[74] + z[75] + 2 * z[76];
z[75] = z[70] + -z[74];
z[75] = abb[31] * z[75];
z[77] = abb[29] * z[10];
z[78] = abb[32] * z[10];
z[79] = z[77] + -z[78];
z[80] = abb[17] * z[79];
z[75] = z[75] + z[80];
z[36] = z[36] + z[57] + -z[75];
z[36] = abb[33] * z[36];
z[57] = abb[13] * z[61];
z[80] = abb[18] * z[9];
z[57] = z[57] + z[80];
z[80] = z[26] + z[43];
z[81] = -abb[0] * z[80];
z[82] = z[45] * z[59];
z[83] = abb[7] * abb[50];
z[84] = abb[14] * z[61];
z[52] = -z[20] + z[47] + -z[52] + z[57] + z[76] + z[81] + z[82] + z[83] + -z[84];
z[52] = abb[29] * z[52];
z[76] = z[38] + z[57];
z[81] = 2 * z[47];
z[82] = -z[81] + z[84];
z[84] = 2 * z[29];
z[85] = z[82] + z[84];
z[40] = abb[52] * (T(3) / T(2)) + -z[40];
z[86] = z[26] + z[40];
z[86] = abb[7] * z[86];
z[87] = -z[31] + z[72];
z[87] = z[60] * z[87];
z[88] = z[63] * z[73];
z[89] = 2 * abb[50] + z[41];
z[89] = abb[4] * z[89];
z[46] = -z[46] + -z[76] + -z[85] + z[86] + z[87] + z[88] + z[89];
z[46] = abb[31] * z[46];
z[86] = 3 * abb[51] + -abb[52] + -z[72];
z[86] = z[60] * z[86];
z[86] = -z[12] + z[86];
z[87] = 2 * z[24];
z[88] = abb[4] * z[87];
z[64] = z[64] + -z[66] + -z[69] + -z[86] + z[88];
z[64] = abb[34] * z[64];
z[46] = z[46] + z[52] + z[64];
z[46] = abb[29] * z[46];
z[52] = (T(1) / T(2)) * z[34];
z[88] = abb[3] * z[52];
z[89] = -z[38] + z[88];
z[90] = z[63] * z[80];
z[48] = (T(1) / T(2)) * z[48];
z[90] = -z[48] + z[90];
z[52] = abb[7] * z[52];
z[52] = -z[51] + z[52];
z[25] = -z[25] + z[52];
z[91] = -z[25] + z[89] + z[90];
z[92] = abb[34] * z[91];
z[93] = z[48] + z[66];
z[45] = -z[45] * z[63];
z[45] = z[45] + z[65] + z[68] + z[93];
z[45] = abb[29] * z[45];
z[68] = z[55] * z[60];
z[94] = abb[5] * abb[50];
z[68] = -z[68] + 2 * z[83] + z[94];
z[65] = z[65] + z[68] + z[89];
z[83] = -abb[32] * z[65];
z[30] = abb[4] * z[30];
z[94] = z[20] + z[47];
z[30] = z[30] + z[90] + -z[94];
z[42] = z[30] + -z[42];
z[90] = abb[33] * z[42];
z[25] = z[25] + -z[30];
z[25] = z[0] * z[25];
z[25] = z[25] + z[45] + z[75] + z[83] + z[90] + z[92];
z[25] = abb[30] * z[25];
z[30] = -z[56] * z[73];
z[45] = -abb[52] + z[24] + -z[71];
z[45] = z[45] * z[59];
z[56] = -abb[51] + -z[61];
z[56] = abb[4] * z[56];
z[30] = z[20] + z[30] + z[45] + z[49] + (T(1) / T(2)) * z[52] + z[56] + -z[89];
z[30] = z[16] * z[30];
z[45] = 2 * abb[49] + -z[41];
z[45] = abb[7] * z[45];
z[49] = abb[51] + z[53];
z[49] = z[49] * z[60];
z[52] = z[55] * z[63];
z[39] = abb[51] * (T(3) / T(2)) + -z[39];
z[53] = z[26] + -z[39];
z[53] = abb[4] * z[53];
z[55] = 3 * z[9];
z[56] = abb[16] * z[55];
z[45] = z[45] + z[49] + z[52] + z[53] + -z[56] + z[76] + z[82];
z[45] = abb[29] * z[45];
z[49] = 2 * z[26];
z[40] = z[40] + z[49];
z[40] = abb[7] * z[40];
z[40] = z[40] + -z[56] + -z[85];
z[39] = z[39] + -z[49];
z[39] = abb[4] * z[39];
z[52] = abb[0] * z[43];
z[53] = abb[5] * z[26];
z[39] = -z[39] + z[40] + -z[52] + z[53] + z[57];
z[52] = -abb[31] * z[39];
z[56] = abb[34] * z[65];
z[59] = 3 * z[26];
z[43] = z[43] + z[59];
z[60] = abb[4] * (T(1) / T(2));
z[65] = -z[43] * z[60];
z[65] = z[47] + z[65] + -z[68];
z[65] = abb[32] * z[65];
z[45] = z[45] + z[52] + z[56] + z[65];
z[45] = abb[32] * z[45];
z[32] = z[32] * z[63];
z[52] = 2 * abb[0];
z[56] = z[28] * z[52];
z[65] = 2 * abb[52] + z[72];
z[65] = abb[7] * z[65];
z[68] = abb[4] * z[54];
z[32] = -z[32] + z[56] + -z[62] + -z[65] + z[67] + -z[68] + z[84];
z[56] = abb[17] * z[10];
z[62] = -z[32] + z[56];
z[62] = abb[57] * z[62];
z[18] = -z[18] + 2 * z[51];
z[65] = abb[7] * z[15];
z[50] = abb[5] * z[50];
z[50] = z[18] + z[50] + -z[58] + z[65] + z[81] + -z[93];
z[50] = abb[35] * z[50];
z[58] = z[20] + z[48];
z[37] = (T(1) / T(2)) * z[37];
z[65] = abb[7] * z[37];
z[67] = -abb[0] * z[26];
z[65] = z[47] + -z[51] + z[58] + z[65] + z[67];
z[65] = abb[38] * z[65];
z[67] = -abb[51] + z[26];
z[67] = z[52] * z[67];
z[68] = -abb[7] * z[72];
z[12] = z[12] + -z[66] + z[67] + z[68] + -z[88];
z[12] = abb[37] * z[12];
z[47] = z[47] + z[66];
z[43] = abb[7] * z[43];
z[43] = z[29] + (T(-1) / T(2)) * z[43] + -z[47] + -z[74];
z[43] = abb[31] * z[43];
z[43] = z[43] + -z[64];
z[43] = abb[31] * z[43];
z[66] = 4 * abb[37] + -abb[38] + (T(-3) / T(2)) * z[16];
z[66] = z[24] * z[66];
z[67] = abb[31] * z[24];
z[68] = abb[34] * z[24];
z[68] = -z[67] + -4 * z[68];
z[68] = abb[31] * z[68];
z[71] = abb[34] * z[87];
z[71] = z[67] + z[71];
z[72] = abb[29] * z[24];
z[73] = 2 * z[71] + -z[72];
z[73] = abb[29] * z[73];
z[67] = -z[67] + z[72];
z[24] = abb[32] * z[24];
z[67] = z[24] + -2 * z[67];
z[67] = abb[32] * z[67];
z[66] = z[66] + z[67] + z[68] + z[73];
z[66] = abb[2] * z[66];
z[67] = -abb[4] + -abb[7] + abb[13] + abb[14];
z[67] = z[9] * z[67];
z[68] = -z[26] + z[41];
z[68] = abb[16] * z[68];
z[5] = 2 * abb[45] + 4 * abb[54] + -z[5];
z[5] = abb[28] * z[5];
z[73] = abb[18] * z[61];
z[74] = abb[5] * z[10];
z[5] = z[5] + z[67] + -z[68] + z[73] + z[74];
z[26] = z[26] + z[41];
z[67] = -abb[17] * z[26];
z[67] = -z[5] + z[67];
z[67] = abb[58] * z[67];
z[19] = abb[5] * z[19];
z[15] = abb[4] * z[15];
z[15] = z[15] + z[19] + -z[38] + z[48] + -z[86];
z[19] = abb[2] * z[87];
z[19] = z[15] + z[19] + 2 * z[20] + z[56];
z[38] = abb[55] * z[19];
z[48] = abb[29] * z[9];
z[68] = abb[31] * z[55];
z[73] = abb[34] * z[10];
z[73] = -z[48] + z[68] + z[73] + -z[78];
z[73] = abb[32] * z[73];
z[16] = z[9] * z[16];
z[74] = -abb[37] * z[10];
z[75] = -abb[29] * z[68];
z[16] = z[16] + z[73] + z[74] + z[75];
z[16] = abb[17] * z[16];
z[73] = -abb[36] * z[91];
z[74] = abb[56] * z[42];
z[75] = abb[31] * z[9];
z[48] = z[48] + z[75];
z[14] = z[14] * z[48];
z[48] = abb[35] + abb[57];
z[48] = z[10] * z[48];
z[76] = -abb[33] * z[79];
z[78] = -abb[58] * z[61];
z[14] = z[14] + z[48] + z[76] + z[78];
z[14] = abb[19] * z[14];
z[48] = -abb[38] * z[80];
z[34] = abb[37] * z[34];
z[34] = z[34] + z[48];
z[34] = z[34] * z[63];
z[37] = -abb[38] * z[37];
z[48] = 2 * abb[51] + -z[54];
z[48] = abb[37] * z[48];
z[37] = z[37] + z[48];
z[37] = abb[4] * z[37];
z[1] = abb[59] + z[1] + z[12] + z[14] + z[16] + z[22] + z[25] + z[30] + z[34] + z[36] + z[37] + z[38] + z[43] + z[45] + z[46] + z[50] + z[62] + z[65] + z[66] + z[67] + z[73] + z[74];
z[12] = z[31] + z[59];
z[14] = z[12] * z[60];
z[14] = z[14] + z[56];
z[16] = -abb[52] * z[52];
z[22] = abb[5] * z[28];
z[16] = z[14] + z[16] + z[18] + z[22] + -z[33] + z[47] + -z[58] + z[69];
z[16] = abb[33] * z[16];
z[3] = z[3] + z[13];
z[13] = abb[31] * z[3];
z[18] = abb[26] + -z[44];
z[18] = z[17] * z[18];
z[17] = -abb[33] + z[11] + -z[17];
z[17] = abb[24] * z[17];
z[22] = -abb[25] * abb[29];
z[25] = abb[22] * z[11];
z[11] = -abb[29] + z[11];
z[11] = abb[20] * z[11];
z[6] = z[6] + z[11] + z[13] + z[17] + z[18] + z[22] + z[25];
z[3] = abb[25] * (T(1) / T(2)) + -z[3] + -z[7];
z[3] = abb[33] * z[3];
z[0] = z[0] * z[4];
z[0] = z[0] + z[3] + (T(1) / T(2)) * z[6];
z[0] = abb[53] * z[0];
z[3] = -z[12] * z[63];
z[4] = -z[41] + -z[49];
z[4] = abb[4] * z[4];
z[6] = abb[0] * z[31];
z[3] = z[3] + z[4] + z[6] + z[35] + -z[40] + z[57];
z[3] = abb[31] * z[3];
z[4] = abb[7] * abb[52];
z[4] = z[4] + z[20] + -z[29] + -z[51];
z[4] = 2 * z[4] + z[15] + z[81];
z[4] = abb[29] * z[4];
z[6] = -abb[32] * z[39];
z[7] = -z[14] + -z[53] + z[70] + z[94];
z[7] = abb[30] * z[7];
z[11] = abb[32] * z[55];
z[11] = z[11] + z[68] + z[77];
z[11] = abb[17] * z[11];
z[12] = z[24] + -z[71] + z[72];
z[12] = abb[2] * z[12];
z[9] = abb[32] * z[9];
z[13] = abb[33] * z[10];
z[9] = z[9] + z[13] + z[75];
z[9] = abb[19] * z[9];
z[0] = z[0] + z[3] + z[4] + z[6] + z[7] + z[9] + z[11] + 2 * z[12] + z[16] + -z[64];
z[0] = m1_set::bc<T>[0] * z[0];
z[3] = -abb[41] * z[32];
z[4] = -abb[42] * z[5];
z[5] = abb[39] * z[19];
z[6] = abb[40] * z[42];
z[7] = -abb[42] * z[21];
z[9] = -abb[41] * z[23];
z[11] = -abb[39] * z[27];
z[8] = -abb[24] + -z[8];
z[8] = abb[40] * z[8];
z[7] = z[7] + z[8] + z[9] + z[11];
z[2] = z[2] * z[7];
z[7] = -abb[42] * z[26];
z[8] = abb[41] * z[10];
z[7] = z[7] + z[8];
z[7] = abb[17] * z[7];
z[9] = -abb[42] * z[61];
z[8] = z[8] + z[9];
z[8] = abb[19] * z[8];
z[0] = abb[43] + z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_507_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.886161099690250984741505458611170936143031814797445273809479501"),stof<T>("-3.7792196039290779840740318596922210627402822973481223569948307766")}, std::complex<T>{stof<T>("1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("-7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("0.886161099690250984741505458611170936143031814797445273809479501"),stof<T>("-3.7792196039290779840740318596922210627402822973481223569948307766")}, std::complex<T>{stof<T>("-0.886161099690250984741505458611170936143031814797445273809479501"),stof<T>("3.7792196039290779840740318596922210627402822973481223569948307766")}, std::complex<T>{stof<T>("-0.886161099690250984741505458611170936143031814797445273809479501"),stof<T>("3.7792196039290779840740318596922210627402822973481223569948307766")}, std::complex<T>{stof<T>("-19.251487639891229114804122255455639533884839238370546186370837468"),stof<T>("2.702791952986176229640274038496251748496755566419030395721309865")}, std::complex<T>{stof<T>("-21.606546010172162322515379700341139867996638041966289214523326148"),stof<T>("-2.702000933968601905048161264218677370994974637217526244113750991")}, std::complex<T>{stof<T>("3.2904641725129597777725651834777845479008968023777904580793849356"),stof<T>("8.6310949606228472502995409892366960969985099394394082902086153971")}, std::complex<T>{stof<T>("-0.5527624557607640950123004076538785599513755392459028678507623014"),stof<T>("8.6180817887752370110104092174451592570304689492490970882826963077")}, std::complex<T>{stof<T>("-1.929047365971231066630042088008710289906983071846621682967563806"),stof<T>("0.1299667118265977184776204054735914385079853180536135282563373817")}, std::complex<T>{stof<T>("3.544644398761003938966021834444683744572127259189781095237918004"),stof<T>("-15.116878415716311936296127438768884250961129189392489427979323107")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[124].real()/kbase.W[124].real()), rlog(k.W[127].real()/kbase.W[127].real()), C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}, rlog(k.W[195].real()/kbase.W[195].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_507_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_507_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (16 + 13 * v[0] + 3 * v[1] + v[2] + -7 * v[3] + -8 * v[4] + -4 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -v[5]) * (-v[0] + v[1] + -v[2] + -v[3] + v[5])) / prod_pow(tend, 2);
c[1] = ((-1 + 2 * m1_set::bc<T>[1]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[49] + abb[50] + -abb[51]) * (t * c[0] + c[1]);
	}
	{
T z[4];
z[0] = abb[49] + abb[50] + -abb[51];
z[0] = abb[11] * z[0];
z[1] = -abb[29] + abb[31];
z[1] = z[0] * z[1];
z[1] = 2 * z[1];
z[2] = abb[32] * z[0];
z[2] = z[1] + z[2];
z[2] = abb[32] * z[2];
z[3] = -abb[34] * z[0];
z[1] = -z[1] + z[3];
z[1] = abb[34] * z[1];
z[0] = abb[37] * z[0];
return 2 * z[0] + z[1] + z[2];
}

}
template <typename T, typename TABB> T SpDLog_f_4_507_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[49] + abb[50] + -abb[51]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[32] + -abb[34];
z[1] = abb[49] + abb[50] + -abb[51];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_507_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-14.19964576974122959598944279035209578005745742925733362181650657"),stof<T>("38.621420869721328157496920574462338387013500108456424778024073909")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,60> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_15(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), f_2_11_im(k), f_2_25_im(k), T{0}, rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[124].real()/k.W[124].real()), rlog(kend.W[127].real()/k.W[127].real()), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), rlog(kend.W[195].real()/k.W[195].real()), f_2_2_re(k), f_2_10_re(k), f_2_11_re(k), f_2_25_re(k), T{0}};
abb[43] = SpDLog_f_4_507_W_17_Im(t, path, abb);
abb[59] = SpDLog_f_4_507_W_17_Re(t, path, abb);

                    
            return f_4_507_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_507_DLogXconstant_part(base_point<T>, kend);
	value += f_4_507_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_507_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_507_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_507_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_507_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_507_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_507_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
