/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_556.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_556_abbreviated (const std::array<T,60>& abb) {
T z[128];
z[0] = abb[46] * (T(1) / T(2));
z[1] = abb[47] * (T(1) / T(2));
z[2] = z[0] + z[1];
z[3] = abb[50] * (T(1) / T(2));
z[4] = z[2] + -z[3];
z[5] = abb[43] + -abb[49];
z[6] = z[4] + z[5];
z[7] = abb[15] * z[6];
z[8] = z[1] + z[3];
z[9] = abb[44] + abb[45];
z[10] = z[8] + z[9];
z[11] = abb[46] * (T(3) / T(2));
z[12] = -z[10] + z[11];
z[13] = abb[6] * z[12];
z[14] = -abb[46] + z[9];
z[15] = -abb[48] + abb[50] + z[14];
z[16] = abb[14] * z[15];
z[17] = -abb[47] + abb[50];
z[18] = abb[46] + z[17];
z[18] = -abb[48] + (T(1) / T(2)) * z[18];
z[19] = abb[7] * z[18];
z[20] = z[7] + z[13] + z[16] + -z[19];
z[21] = abb[51] * (T(1) / T(2));
z[22] = abb[53] * (T(1) / T(2));
z[23] = z[21] + z[22];
z[24] = abb[21] + abb[25];
z[25] = abb[23] * (T(1) / T(2));
z[26] = abb[24] + z[25];
z[24] = (T(1) / T(2)) * z[24] + z[26];
z[27] = -z[23] * z[24];
z[28] = 3 * abb[49];
z[29] = -z[9] + z[28];
z[30] = abb[43] * (T(3) / T(2));
z[31] = -abb[47] + z[3] + (T(1) / T(2)) * z[29] + -z[30];
z[31] = abb[8] * z[31];
z[32] = abb[47] * (T(3) / T(2));
z[33] = -z[3] + z[9] + z[32];
z[34] = abb[48] * (T(1) / T(2));
z[35] = (T(-5) / T(2)) * z[5] + -z[33] + z[34];
z[35] = abb[2] * z[35];
z[14] = abb[47] + z[14];
z[36] = z[5] + z[14];
z[37] = abb[12] * z[36];
z[20] = (T(1) / T(2)) * z[20] + z[27] + z[31] + z[35] + z[37];
z[27] = 3 * abb[28];
z[20] = z[20] * z[27];
z[31] = 2 * z[9];
z[35] = -z[11] + z[31];
z[38] = abb[50] * (T(5) / T(2));
z[39] = 3 * abb[48];
z[40] = z[1] + -z[35] + -z[38] + z[39];
z[41] = abb[14] * z[40];
z[42] = -abb[50] + z[9];
z[43] = 2 * abb[47];
z[44] = z[42] + z[43];
z[45] = z[28] + -z[44];
z[46] = 3 * abb[43];
z[47] = -z[45] + z[46];
z[48] = abb[8] * z[47];
z[48] = z[41] + z[48];
z[49] = abb[27] * z[48];
z[50] = abb[51] * (T(3) / T(2));
z[51] = abb[53] * (T(3) / T(2));
z[52] = z[50] + z[51];
z[53] = -abb[29] + abb[30];
z[54] = abb[27] + -z[53];
z[55] = abb[24] + abb[25];
z[54] = z[54] * z[55];
z[56] = abb[21] + abb[23];
z[57] = abb[24] + z[56];
z[58] = abb[31] * z[57];
z[54] = z[54] + z[58];
z[58] = z[52] * z[54];
z[59] = abb[30] * z[12];
z[60] = abb[29] * z[12];
z[61] = z[59] + -z[60];
z[62] = abb[14] * z[61];
z[63] = abb[29] * z[45];
z[64] = abb[30] * z[45];
z[65] = abb[43] * z[53];
z[66] = z[63] + -z[64] + 3 * z[65];
z[67] = abb[8] * z[66];
z[68] = abb[50] * (T(3) / T(2));
z[2] = 2 * abb[48] + z[2] + -z[9] + -z[68];
z[2] = abb[14] * z[2];
z[2] = z[2] + z[19];
z[69] = z[2] + -z[13];
z[70] = 3 * abb[31];
z[69] = z[69] * z[70];
z[5] = abb[48] + z[5] + -z[17];
z[71] = abb[27] * z[5];
z[71] = -z[66] + z[71];
z[71] = abb[2] * z[71];
z[72] = abb[27] + z[70];
z[72] = z[12] * z[72];
z[72] = z[61] + z[72];
z[73] = abb[3] * z[72];
z[4] = -abb[49] + z[4];
z[74] = abb[29] * z[4];
z[75] = abb[30] * z[4];
z[74] = -z[65] + z[74] + -z[75];
z[76] = abb[27] * z[6];
z[76] = z[74] + z[76];
z[77] = 3 * abb[15];
z[78] = -z[76] * z[77];
z[79] = -abb[49] + z[14];
z[80] = abb[29] * z[79];
z[79] = abb[30] * z[79];
z[81] = z[65] + z[79] + -z[80];
z[82] = 3 * abb[12];
z[81] = z[81] * z[82];
z[20] = z[20] + z[49] + z[58] + -z[62] + -2 * z[67] + z[69] + 3 * z[71] + z[73] + z[78] + z[81];
z[20] = abb[28] * z[20];
z[49] = abb[49] * (T(3) / T(2));
z[58] = abb[42] + z[49];
z[67] = abb[50] * (T(1) / T(4));
z[69] = abb[46] * (T(3) / T(4));
z[71] = abb[47] * (T(-3) / T(4)) + z[58] + -z[67] + -z[69];
z[71] = z[53] * z[71];
z[73] = abb[31] * z[12];
z[78] = abb[42] + z[9] + z[17];
z[78] = -abb[43] + (T(1) / T(2)) * z[78];
z[78] = abb[27] * z[78];
z[42] = -abb[47] + z[42];
z[81] = 3 * abb[42] + z[42];
z[83] = abb[32] * z[81];
z[71] = (T(-1) / T(2)) * z[65] + z[71] + -z[73] + z[78] + -z[83];
z[71] = abb[27] * z[71];
z[1] = z[1] + z[9];
z[78] = abb[46] * (T(9) / T(2));
z[84] = abb[42] + -z[1] + -z[28] + -z[68] + z[78];
z[85] = prod_pow(abb[29], 2);
z[86] = (T(1) / T(2)) * z[85];
z[84] = z[84] * z[86];
z[87] = abb[42] * (T(1) / T(2));
z[88] = abb[50] * (T(3) / T(4));
z[89] = z[87] + -z[88];
z[90] = abb[47] * (T(5) / T(4)) + z[9];
z[91] = -z[28] + z[69] + z[89] + z[90];
z[91] = abb[30] * z[91];
z[90] = -z[69] + z[90];
z[92] = -z[58] + z[88] + z[90];
z[92] = abb[29] * z[92];
z[91] = z[91] + z[92];
z[91] = abb[30] * z[91];
z[92] = 3 * abb[46];
z[93] = abb[47] + abb[50] + z[31];
z[94] = z[92] + -z[93];
z[95] = abb[29] * z[94];
z[59] = -z[59] + (T(-3) / T(2)) * z[73] + -z[95];
z[59] = abb[31] * z[59];
z[73] = -13 * abb[42] + -23 * z[9];
z[73] = 13 * abb[49] + abb[47] * (T(-23) / T(6)) + abb[43] * (T(-13) / T(3)) + z[3] + -z[11] + (T(1) / T(3)) * z[73];
z[96] = prod_pow(m1_set::bc<T>[0], 2);
z[97] = (T(1) / T(2)) * z[96];
z[73] = z[73] * z[97];
z[98] = abb[29] * (T(1) / T(2));
z[99] = 2 * abb[30];
z[100] = z[98] + z[99];
z[100] = abb[30] * z[100];
z[100] = z[86] + z[100];
z[100] = abb[43] * z[100];
z[101] = z[0] + -z[8];
z[102] = abb[42] + z[101];
z[103] = 3 * abb[33];
z[104] = z[102] * z[103];
z[105] = abb[29] * z[81];
z[106] = abb[30] * z[81];
z[107] = z[105] + -z[106];
z[83] = (T(3) / T(2)) * z[83] + z[107];
z[108] = abb[32] * z[83];
z[59] = z[59] + z[71] + z[73] + z[84] + z[91] + z[100] + z[104] + z[108];
z[59] = abb[3] * z[59];
z[71] = abb[20] + abb[22] + abb[25];
z[26] = z[26] + (T(1) / T(2)) * z[71];
z[26] = z[26] * z[53];
z[71] = abb[23] + -abb[25];
z[73] = abb[27] * (T(1) / T(2));
z[71] = z[71] * z[73];
z[84] = abb[20] + abb[22];
z[91] = abb[24] + z[84];
z[100] = abb[23] + z[91];
z[104] = abb[32] * z[100];
z[108] = abb[31] * z[84];
z[26] = z[26] + z[71] + -z[104] + z[108];
z[71] = 3 * abb[27];
z[26] = z[26] * z[71];
z[104] = -abb[25] + z[84];
z[109] = abb[23] + z[104];
z[109] = abb[29] * z[109];
z[110] = abb[23] + z[84];
z[111] = abb[30] * z[110];
z[109] = z[109] + -z[111];
z[109] = abb[30] * z[109];
z[104] = z[56] + z[104];
z[111] = z[85] * z[104];
z[109] = z[109] + -z[111];
z[111] = z[96] * z[104];
z[109] = 3 * z[109] + z[111];
z[100] = -z[53] * z[100];
z[25] = z[25] + z[91];
z[25] = abb[32] * z[25];
z[111] = abb[31] * z[91];
z[25] = z[25] + z[100] + -z[111];
z[100] = 3 * abb[32];
z[25] = z[25] * z[100];
z[56] = z[56] + z[91];
z[111] = abb[29] * z[56];
z[112] = abb[24] * abb[30];
z[108] = (T(1) / T(2)) * z[108] + -z[111] + z[112];
z[108] = z[70] * z[108];
z[111] = abb[23] + abb[24];
z[111] = z[103] * z[111];
z[112] = 3 * abb[58];
z[113] = z[91] * z[112];
z[114] = 3 * abb[56];
z[115] = abb[25] + z[91];
z[116] = z[114] * z[115];
z[25] = -z[25] + -z[26] + -z[108] + (T(1) / T(2)) * z[109] + -z[111] + z[113] + -z[116];
z[26] = 3 * abb[35];
z[108] = z[26] * z[57];
z[109] = 3 * abb[57];
z[111] = z[104] * z[109];
z[113] = 3 * abb[34];
z[116] = z[110] * z[113];
z[117] = abb[36] * z[84];
z[108] = z[25] + z[108] + z[111] + -z[116] + -3 * z[117];
z[24] = abb[28] * z[24];
z[24] = z[24] + -z[54];
z[24] = z[24] * z[27];
z[24] = z[24] + z[108];
z[27] = abb[37] * (T(3) / T(2));
z[54] = abb[26] * z[27];
z[54] = z[24] + z[54];
z[111] = abb[52] + abb[54];
z[116] = (T(-1) / T(2)) * z[111];
z[54] = z[54] * z[116];
z[25] = -z[22] * z[25];
z[21] = -z[21] * z[108];
z[108] = abb[55] * (T(1) / T(2));
z[24] = -z[24] * z[108];
z[116] = -abb[31] + abb[32] + -z[73];
z[116] = abb[27] * z[116];
z[116] = -abb[36] + -z[26] + z[86] + -z[109] + z[116];
z[116] = z[12] * z[116];
z[117] = abb[30] * z[44];
z[101] = -abb[48] + z[9] + -z[101];
z[118] = abb[31] * (T(3) / T(2));
z[101] = z[101] * z[118];
z[95] = -z[95] + z[101] + z[117];
z[95] = abb[31] * z[95];
z[101] = -z[67] + z[90];
z[101] = abb[30] * z[101];
z[101] = -z[60] + z[101];
z[101] = abb[30] * z[101];
z[117] = z[18] * z[70];
z[15] = abb[32] * z[15];
z[15] = (T(3) / T(2)) * z[15] + z[61] + z[117];
z[15] = abb[32] * z[15];
z[72] = abb[28] * z[72];
z[117] = 5 * abb[48] + abb[46] * (T(13) / T(2)) + (T(-23) / T(3)) * z[10];
z[117] = z[97] * z[117];
z[118] = z[18] * z[112];
z[17] = -abb[46] + z[17];
z[119] = abb[34] * z[17];
z[15] = z[15] + z[72] + z[95] + z[101] + z[116] + z[117] + z[118] + (T(3) / T(2)) * z[119];
z[15] = abb[4] * z[15];
z[72] = abb[13] * z[102];
z[95] = abb[8] * z[12];
z[101] = -abb[14] * z[12];
z[101] = 3 * z[72] + -z[95] + z[101];
z[101] = abb[32] * z[101];
z[116] = (T(1) / T(2)) * z[9];
z[58] = -abb[47] + abb[43] * (T(-1) / T(2)) + -z[3] + z[58] + -z[116];
z[58] = abb[8] * z[58];
z[117] = -abb[47] + z[9];
z[118] = abb[50] + (T(1) / T(2)) * z[117];
z[119] = abb[48] * (T(3) / T(2));
z[120] = z[118] + -z[119];
z[120] = abb[14] * z[120];
z[121] = z[19] + -z[72];
z[122] = -abb[42] + abb[50];
z[123] = -abb[48] + z[122];
z[123] = abb[9] * z[123];
z[121] = (T(1) / T(2)) * z[121] + -z[123];
z[58] = z[58] + z[120] + 3 * z[121];
z[58] = abb[27] * z[58];
z[32] = z[11] + -z[28] + z[32];
z[38] = abb[42] + z[32] + -z[38];
z[38] = z[38] * z[53];
z[121] = 2 * z[65];
z[38] = (T(1) / T(2)) * z[38] + z[121];
z[38] = abb[8] * z[38];
z[117] = 2 * abb[50] + -z[39] + z[117];
z[124] = 2 * abb[14];
z[117] = z[117] * z[124];
z[95] = -3 * z[19] + z[95] + z[117];
z[117] = abb[31] * z[95];
z[53] = -z[53] * z[72];
z[124] = (T(3) / T(2)) * z[53];
z[38] = z[38] + z[58] + z[62] + z[101] + z[117] + z[124];
z[38] = abb[27] * z[38];
z[14] = abb[8] * z[14];
z[14] = z[14] + z[16] + z[72];
z[16] = (T(1) / T(4)) * z[17];
z[58] = abb[5] * z[16];
z[101] = abb[42] + -abb[47];
z[101] = abb[10] * z[101];
z[58] = z[58] + -z[101];
z[117] = (T(1) / T(2)) * z[19];
z[14] = (T(-1) / T(2)) * z[14] + z[58] + -z[117] + z[123];
z[14] = z[14] * z[100];
z[61] = -abb[8] * z[61];
z[125] = abb[14] * z[18];
z[125] = -z[19] + z[125];
z[17] = (T(1) / T(2)) * z[17];
z[126] = abb[8] * z[17];
z[17] = abb[5] * z[17];
z[126] = -z[17] + -z[125] + z[126];
z[127] = z[70] * z[126];
z[14] = z[14] + -3 * z[53] + z[61] + -z[62] + z[127];
z[14] = abb[32] * z[14];
z[10] = -z[10] + -z[30] + z[49] + z[119];
z[10] = abb[27] * z[10];
z[10] = z[10] + z[66];
z[10] = abb[27] * z[10];
z[30] = -2 * z[63] + z[64];
z[30] = abb[30] * z[30];
z[45] = z[45] * z[85];
z[53] = 2 * abb[29] + -abb[30];
z[53] = abb[30] * z[53];
z[53] = z[53] + -z[85];
z[61] = z[46] * z[53];
z[34] = -z[34] + (T(1) / T(3)) * z[118];
z[34] = z[34] * z[96];
z[10] = z[10] + z[30] + 5 * z[34] + z[45] + z[61];
z[10] = abb[2] * z[10];
z[30] = abb[29] * z[122];
z[34] = abb[30] * z[122];
z[45] = z[30] + -z[34] + z[65];
z[61] = -abb[43] + z[122];
z[63] = 2 * z[61];
z[64] = abb[27] * z[63];
z[64] = (T(1) / T(2)) * z[45] + z[64];
z[64] = abb[27] * z[64];
z[65] = z[86] * z[122];
z[30] = (T(1) / T(2)) * z[30] + -z[34];
z[30] = abb[30] * z[30];
z[34] = abb[30] + -z[98];
z[34] = abb[30] * z[34];
z[34] = z[34] + -z[86];
z[34] = abb[43] * z[34];
z[61] = z[61] * z[96];
z[30] = z[30] + z[34] + (T(-13) / T(6)) * z[61] + z[64] + z[65];
z[30] = abb[0] * z[30];
z[33] = -z[0] + z[33];
z[34] = 2 * abb[43];
z[61] = -2 * abb[49] + z[33] + z[34];
z[61] = abb[8] * z[61];
z[64] = abb[3] * z[12];
z[65] = -z[13] + z[64];
z[47] = abb[2] * z[47];
z[37] = -z[7] + -z[37] + z[47] + z[61] + -z[65];
z[47] = -z[22] * z[104];
z[47] = z[37] + z[47];
z[47] = z[47] * z[109];
z[61] = z[73] + -z[100];
z[61] = z[61] * z[81];
z[61] = z[61] + -z[107];
z[61] = abb[27] * z[61];
z[73] = z[86] + z[103];
z[73] = z[73] * z[81];
z[86] = -z[105] + (T(1) / T(2)) * z[106];
z[86] = abb[30] * z[86];
z[42] = -abb[42] + (T(-1) / T(3)) * z[42];
z[42] = z[42] * z[96];
z[83] = z[83] * z[100];
z[42] = (T(13) / T(2)) * z[42] + z[61] + z[73] + z[83] + z[86];
z[42] = abb[1] * z[42];
z[33] = -abb[8] * z[33];
z[61] = abb[3] * z[102];
z[73] = z[22] * z[110];
z[83] = abb[1] * z[81];
z[33] = -z[17] + z[33] + z[61] + -z[72] + z[73] + z[83];
z[33] = z[33] * z[113];
z[0] = -abb[42] + -abb[49] + z[0] + z[8];
z[0] = abb[16] * z[0];
z[8] = abb[43] + z[4];
z[8] = abb[19] * z[8];
z[61] = -z[23] + -z[108];
z[61] = abb[26] * z[61];
z[73] = abb[18] * z[102];
z[83] = abb[17] * z[6];
z[0] = z[0] + z[8] + z[61] + z[73] + z[83];
z[0] = z[0] * z[27];
z[8] = z[69] + z[88];
z[27] = abb[49] * (T(-9) / T(2)) + abb[47] * (T(13) / T(4)) + -z[8] + z[31] + -z[87];
z[27] = abb[29] * z[27];
z[31] = 2 * abb[42];
z[8] = abb[47] * (T(7) / T(4)) + z[8] + -z[31] + z[116];
z[8] = abb[30] * z[8];
z[8] = z[8] + z[27];
z[8] = abb[30] * z[8];
z[27] = z[49] + -z[89] + -z[90];
z[27] = z[27] * z[85];
z[49] = abb[42] * (T(13) / T(2)) + -5 * z[9];
z[49] = abb[47] * (T(-43) / T(12)) + abb[43] * (T(-5) / T(6)) + abb[46] * (T(-1) / T(4)) + z[28] + (T(1) / T(3)) * z[49] + -z[67];
z[49] = z[49] * z[96];
z[53] = z[34] * z[53];
z[8] = z[8] + z[27] + z[49] + z[53];
z[8] = abb[8] * z[8];
z[27] = z[44] * z[99];
z[27] = z[27] + -z[60];
z[27] = abb[8] * z[27];
z[16] = -abb[8] * z[16];
z[16] = z[16] + -z[117] + z[120];
z[16] = z[16] * z[70];
z[44] = abb[6] * z[60];
z[49] = abb[30] * z[17];
z[49] = z[44] + z[49];
z[16] = z[16] + z[27] + 3 * z[49] + z[62];
z[16] = abb[31] * z[16];
z[4] = z[4] * z[85];
z[27] = -abb[29] * z[75];
z[49] = -abb[29] * abb[30];
z[49] = z[49] + z[85];
z[49] = abb[43] * z[49];
z[4] = z[4] + z[27] + z[49];
z[27] = z[71] * z[76];
z[49] = -z[6] * z[96];
z[4] = 3 * z[4] + z[27] + z[49];
z[4] = abb[15] * z[4];
z[5] = abb[2] * z[5];
z[5] = z[5] + -z[7];
z[7] = abb[8] * z[6];
z[7] = z[5] + z[7] + z[125];
z[27] = z[7] * z[114];
z[49] = z[51] * z[84];
z[49] = z[49] + -z[64] + z[95];
z[49] = abb[36] * z[49];
z[53] = -z[22] * z[57];
z[53] = -z[2] + z[53] + -z[65];
z[26] = z[26] * z[53];
z[53] = (T(-3) / T(2)) * z[85] + z[97];
z[53] = z[13] * z[53];
z[2] = z[2] + -z[72];
z[2] = z[2] * z[103];
z[57] = z[112] * z[126];
z[61] = -abb[30] * abb[43];
z[61] = z[61] + -z[79];
z[61] = abb[29] * z[61];
z[36] = z[36] * z[96];
z[36] = (T(7) / T(2)) * z[36] + 3 * z[61];
z[36] = abb[12] * z[36];
z[58] = prod_pow(abb[30], 2) * z[58];
z[61] = abb[30] * z[124];
z[0] = abb[59] + z[0] + z[2] + (T(1) / T(2)) * z[4] + z[8] + z[10] + z[14] + z[15] + z[16] + z[20] + z[21] + z[24] + z[25] + z[26] + z[27] + z[30] + z[33] + z[36] + z[38] + z[42] + z[47] + z[49] + z[53] + z[54] + z[57] + -3 * z[58] + z[59] + z[61];
z[2] = -z[23] * z[104];
z[2] = z[2] + z[37];
z[4] = 3 * abb[39];
z[2] = z[2] * z[4];
z[8] = abb[32] * m1_set::bc<T>[0];
z[10] = z[8] * z[40];
z[14] = abb[28] * m1_set::bc<T>[0];
z[4] = -z[4] + -z[14];
z[4] = z[4] * z[12];
z[12] = abb[47] * (T(5) / T(2)) + -z[3] + z[35];
z[15] = -abb[30] * z[12];
z[15] = z[15] + z[60];
z[15] = m1_set::bc<T>[0] * z[15];
z[16] = z[39] + z[92] + -2 * z[93];
z[20] = abb[31] * m1_set::bc<T>[0];
z[16] = z[16] * z[20];
z[21] = 3 * abb[40];
z[18] = z[18] * z[21];
z[4] = z[4] + z[10] + z[15] + z[16] + z[18];
z[4] = abb[4] * z[4];
z[10] = abb[40] + z[8];
z[10] = z[10] * z[91];
z[15] = abb[29] * z[104];
z[16] = abb[25] * abb[30];
z[15] = z[15] + z[16];
z[15] = m1_set::bc<T>[0] * z[15];
z[16] = abb[27] * m1_set::bc<T>[0];
z[18] = z[16] * z[115];
z[23] = z[20] * z[56];
z[10] = z[10] + z[15] + -z[18] + -z[23];
z[15] = abb[38] * z[115];
z[15] = -z[10] + z[15];
z[18] = abb[39] * z[104];
z[23] = z[14] * z[55];
z[18] = -z[15] + z[18] + z[23];
z[23] = abb[55] + z[111];
z[23] = (T(-3) / T(2)) * z[23];
z[18] = z[18] * z[23];
z[10] = -z[10] * z[51];
z[15] = z[15] * z[50];
z[23] = -z[52] * z[55];
z[5] = -3 * z[5] + z[23] + -z[48] + -z[64];
z[5] = z[5] * z[14];
z[14] = -m1_set::bc<T>[0] * z[45];
z[23] = z[16] * z[63];
z[14] = z[14] + -z[23];
z[14] = abb[0] * z[14];
z[24] = -m1_set::bc<T>[0] * z[100];
z[24] = z[16] + z[24];
z[24] = z[24] * z[81];
z[25] = -m1_set::bc<T>[0] * z[107];
z[24] = z[24] + z[25];
z[24] = abb[1] * z[24];
z[14] = z[14] + z[24];
z[17] = -z[17] + 2 * z[101];
z[24] = -abb[30] * z[17];
z[24] = z[24] + z[44];
z[25] = abb[29] + abb[30];
z[25] = abb[43] * z[25];
z[25] = z[25] + z[79] + z[80];
z[25] = z[25] * z[82];
z[24] = 3 * z[24] + z[25];
z[24] = m1_set::bc<T>[0] * z[24];
z[13] = -3 * z[13] + z[41];
z[13] = m1_set::bc<T>[0] * z[13];
z[25] = abb[8] * m1_set::bc<T>[0];
z[12] = z[12] * z[25];
z[13] = -z[12] + z[13];
z[13] = abb[31] * z[13];
z[19] = -z[19] + 2 * z[123];
z[17] = z[17] + -z[19];
z[17] = 3 * z[17] + -z[41];
z[17] = m1_set::bc<T>[0] * z[17];
z[12] = z[12] + z[17];
z[12] = abb[32] * z[12];
z[17] = 3 * z[19] + z[41];
z[17] = m1_set::bc<T>[0] * z[17];
z[3] = abb[43] + z[3] + -z[31] + z[32];
z[3] = z[3] * z[25];
z[3] = z[3] + z[17];
z[3] = abb[27] * z[3];
z[17] = -z[28] + -z[39] + z[46] + z[93];
z[17] = z[16] * z[17];
z[19] = -m1_set::bc<T>[0] * z[66];
z[17] = z[17] + z[19];
z[17] = abb[2] * z[17];
z[1] = z[1] + z[31] + -z[68];
z[19] = 6 * abb[49] + -z[1] + -z[78];
z[19] = abb[29] * z[19];
z[26] = abb[42] + z[29] + -z[43];
z[26] = z[26] * z[99];
z[27] = -abb[29] + -z[99];
z[27] = z[27] * z[34];
z[19] = z[19] + z[26] + z[27];
z[19] = m1_set::bc<T>[0] * z[19];
z[20] = z[20] * z[94];
z[8] = z[8] * z[81];
z[8] = -z[8] + z[20];
z[8] = 2 * z[8] + z[19] + -z[23];
z[8] = abb[3] * z[8];
z[19] = z[22] * z[115];
z[7] = z[7] + z[19];
z[7] = abb[38] * z[7];
z[1] = z[1] + -z[11];
z[1] = abb[29] * z[1];
z[9] = -abb[42] + z[9];
z[9] = -4 * abb[47] + -2 * z[9] + z[28];
z[9] = z[9] * z[99];
z[1] = z[1] + z[9] + -z[121];
z[1] = z[1] * z[25];
z[9] = z[21] * z[126];
z[11] = -m1_set::bc<T>[0] * z[74];
z[6] = -z[6] * z[16];
z[6] = z[6] + z[11];
z[6] = z[6] * z[77];
z[1] = abb[41] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + 3 * z[7] + z[8] + z[9] + z[10] + z[12] + z[13] + 2 * z[14] + z[15] + z[17] + z[18] + z[24];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_556_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-36.682310515106630392833272475757325351289707818356945272902581058"),stof<T>("52.741337696243548012391414832183606836297750117548411587622738085")}, std::complex<T>{stof<T>("-26.198296627818665715852294092733382933427874830835336739337282745"),stof<T>("8.503853713337012963867488862109638201182081099726697992227678228")}, std::complex<T>{stof<T>("-49.524956236407311254082823283016140611008461205871124774759687584"),stof<T>("73.841783123950370409383066140526278607337617840009044329562912174")}, std::complex<T>{stof<T>("-49.524956236407311254082823283016140611008461205871124774759687584"),stof<T>("73.841783123950370409383066140526278607337617840009044329562912174")}, std::complex<T>{stof<T>("-25.29960182007458360275951304817094766106727341782048314949503923"),stof<T>("-0.231393115198248609905564510504544725433921351183129945062375423")}, std::complex<T>{stof<T>("-34.994242349480808660486572442622549822969684600108166592620316202"),stof<T>("26.641826692748775297875689373194511221230188847269298308405341226")}, std::complex<T>{stof<T>("78.871258056120432773457608788556736642252678241932957572828799883"),stof<T>("-76.572862457047181862460952427279532632895456463743946810263047842")}, std::complex<T>{stof<T>("22.151596628180127799237021635363734563250931212593987090763209676"),stof<T>("-5.541381265041952900884038064851839450190321124808665566465167137")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[30].real()/kbase.W[30].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_556_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_556_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(32)) * (v[2] + v[3]) * (4 * (12 + 2 * v[1] + v[2] + -v[3] + -2 * v[4] + m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 4 * v[5]) + -m1_set::bc<T>[2] * (24 + 8 * v[0] + 4 * v[1] + v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5]))) / prod_pow(tend, 2);
c[1] = ((-2 + 2 * m1_set::bc<T>[1] + 3 * m1_set::bc<T>[2]) * (T(-3) / T(4)) * (v[2] + v[3])) / tend;


		return (abb[44] + abb[45] + -abb[46] + -abb[48] + abb[50]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[28] + -abb[31];
z[1] = abb[27] + abb[31];
z[0] = z[0] * z[1];
z[0] = abb[33] + -abb[35] + -abb[36] + z[0];
z[1] = abb[44] + abb[45] + -abb[46] + -abb[48] + abb[50];
return 3 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_556_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-3) / T(8)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(3) / T(2)) * (v[2] + v[3])) / tend;


		return (abb[44] + abb[45] + -abb[46] + -abb[48] + abb[50]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[28] + -abb[31];
z[1] = -abb[44] + -abb[45] + abb[46] + abb[48] + -abb[50];
return 3 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_556_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("134.00016935824535130876180219353654991258256745266805246001734843"),stof<T>("57.62304878618258613068434794966887603493917368650055524885138918")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,60> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W18(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_19(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_11_im(k), T{0}, rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[30].real()/k.W[30].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_4_re(k), f_2_7_re(k), f_2_11_re(k), T{0}};
abb[41] = SpDLog_f_4_556_W_19_Im(t, path, abb);
abb[59] = SpDLog_f_4_556_W_19_Re(t, path, abb);

                    
            return f_4_556_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_556_DLogXconstant_part(base_point<T>, kend);
	value += f_4_556_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_556_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_556_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_556_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_556_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_556_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_556_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
