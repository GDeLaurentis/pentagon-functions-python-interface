/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_650.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_650_abbreviated (const std::array<T,72>& abb) {
T z[165];
z[0] = abb[56] * (T(1) / T(2));
z[1] = abb[55] * (T(1) / T(2));
z[2] = z[0] + z[1];
z[3] = abb[51] * (T(1) / T(2));
z[4] = abb[48] + z[3];
z[5] = abb[50] * (T(1) / T(2));
z[6] = -z[2] + z[4] + z[5];
z[6] = abb[7] * z[6];
z[7] = abb[49] + abb[51];
z[8] = 2 * abb[48];
z[9] = z[7] + z[8];
z[10] = abb[50] + abb[55];
z[11] = abb[47] + z[10];
z[12] = 2 * z[9] + -z[11];
z[12] = abb[16] * z[12];
z[13] = abb[47] + abb[50];
z[14] = -abb[55] + z[13];
z[15] = abb[15] * z[14];
z[16] = abb[61] + abb[62];
z[17] = abb[26] * z[16];
z[18] = abb[25] * z[16];
z[19] = 2 * abb[6];
z[20] = abb[5] + z[19];
z[20] = abb[58] * z[20];
z[20] = z[6] + z[12] + z[15] + -z[17] + (T(5) / T(2)) * z[18] + z[20];
z[21] = abb[50] * (T(4) / T(3));
z[22] = abb[55] * (T(2) / T(3));
z[23] = z[21] + z[22];
z[24] = -abb[52] + abb[56] + abb[58];
z[25] = abb[53] * (T(5) / T(2));
z[26] = abb[47] * (T(35) / T(12));
z[27] = 7 * abb[51] + abb[49] * (T(29) / T(4));
z[24] = abb[57] * (T(-15) / T(4)) + abb[48] * (T(5) / T(2)) + abb[54] * (T(10) / T(3)) + -z[23] + (T(4) / T(3)) * z[24] + -z[25] + z[26] + (T(1) / T(3)) * z[27];
z[24] = abb[3] * z[24];
z[27] = abb[52] * (T(1) / T(2));
z[28] = 2 * abb[58];
z[29] = z[27] + -z[28];
z[30] = -abb[51] + abb[56];
z[31] = abb[57] * (T(3) / T(2));
z[32] = abb[49] * (T(5) / T(2));
z[23] = -5 * abb[53] + abb[48] * (T(-7) / T(2)) + abb[54] * (T(9) / T(2)) + -z[23] + -z[29] + (T(11) / T(3)) * z[30] + -z[31] + -z[32];
z[23] = abb[9] * z[23];
z[33] = abb[48] + -abb[53];
z[34] = abb[52] + abb[54];
z[35] = abb[58] + abb[49] * (T(1) / T(3));
z[36] = 2 * abb[57];
z[33] = abb[51] + abb[47] * (T(4) / T(3)) + (T(8) / T(3)) * z[33] + (T(5) / T(3)) * z[34] + -z[35] + -z[36];
z[33] = abb[17] * z[33];
z[37] = -abb[56] + z[10];
z[38] = -z[7] + z[37];
z[36] = -abb[52] + z[36];
z[39] = 2 * abb[53];
z[40] = -z[36] + z[38] + z[39];
z[40] = abb[8] * z[40];
z[41] = abb[24] * z[16];
z[40] = z[40] + z[41];
z[42] = 4 * abb[51];
z[43] = abb[49] * (T(-1) / T(4)) + -z[42];
z[21] = abb[54] * (T(-59) / T(12)) + abb[48] * (T(-11) / T(3)) + abb[52] * (T(-7) / T(6)) + abb[47] * (T(-5) / T(2)) + abb[57] * (T(29) / T(6)) + z[21] + z[25] + (T(1) / T(3)) * z[43];
z[21] = abb[13] * z[21];
z[22] = abb[57] * (T(-17) / T(12)) + abb[54] * (T(-5) / T(2)) + abb[53] * (T(-5) / T(6)) + abb[56] * (T(-2) / T(3)) + abb[50] * (T(1) / T(3)) + abb[48] * (T(11) / T(6)) + abb[49] * (T(35) / T(12)) + z[22] + z[26] + z[42];
z[22] = abb[0] * z[22];
z[25] = abb[55] + abb[56];
z[25] = abb[47] * (T(1) / T(3)) + abb[50] * (T(7) / T(6)) + -z[3] + (T(1) / T(6)) * z[25] + -z[35];
z[25] = abb[4] * z[25];
z[26] = 3 * abb[47];
z[35] = 3 * abb[49];
z[43] = abb[50] * (T(-17) / T(6)) + abb[56] * (T(-7) / T(6)) + abb[55] * (T(-5) / T(6)) + abb[48] * (T(4) / T(3)) + abb[51] * (T(25) / T(6)) + z[26] + z[35];
z[43] = abb[6] * z[43];
z[4] = abb[50] * (T(-5) / T(2)) + -11 * z[4];
z[4] = abb[47] + abb[56] * (T(11) / T(6)) + z[1] + (T(1) / T(3)) * z[4];
z[4] = abb[5] * z[4];
z[44] = -abb[48] + abb[57];
z[45] = -abb[52] + z[44];
z[46] = abb[14] * z[45];
z[47] = -abb[47] + abb[57];
z[48] = -abb[52] + z[47];
z[49] = abb[10] * z[48];
z[50] = abb[22] * z[16];
z[51] = abb[23] * z[16];
z[52] = abb[54] * (T(-11) / T(3)) + abb[56] * (T(-7) / T(3)) + abb[47] * (T(-7) / T(4)) + abb[58] * (T(-2) / T(3)) + abb[55] * (T(1) / T(3)) + abb[48] * (T(1) / T(6)) + abb[49] * (T(3) / T(4)) + abb[51] * (T(4) / T(3)) + abb[53] * (T(7) / T(2)) + abb[57] * (T(13) / T(4));
z[52] = abb[1] * z[52];
z[4] = z[4] + (T(1) / T(3)) * z[20] + z[21] + z[22] + z[23] + z[24] + z[25] + z[33] + (T(4) / T(3)) * z[40] + z[43] + (T(11) / T(6)) * z[46] + (T(5) / T(2)) * z[49] + z[50] + (T(5) / T(6)) * z[51] + z[52];
z[20] = prod_pow(m1_set::bc<T>[0], 2);
z[4] = z[4] * z[20];
z[21] = 3 * abb[18] + -abb[20];
z[22] = abb[19] + abb[21] + z[21];
z[23] = abb[31] * (T(1) / T(2));
z[22] = z[22] * z[23];
z[24] = abb[18] + abb[20];
z[25] = 3 * abb[19] + -abb[21];
z[33] = z[24] + z[25];
z[43] = abb[34] * (T(1) / T(2));
z[33] = z[33] * z[43];
z[52] = abb[20] + abb[21];
z[53] = abb[30] * (T(1) / T(2));
z[53] = z[52] * z[53];
z[54] = abb[18] + abb[19];
z[55] = abb[33] * z[54];
z[56] = -abb[18] + abb[20];
z[57] = abb[32] * z[56];
z[55] = z[55] + z[57];
z[22] = z[22] + z[33] + -z[53] + z[55];
z[22] = abb[30] * z[22];
z[33] = z[21] + z[25];
z[33] = z[23] * z[33];
z[53] = abb[18] * abb[33];
z[58] = abb[18] * abb[34];
z[33] = z[33] + z[53] + z[57] + z[58];
z[33] = abb[34] * z[33];
z[53] = abb[30] + -abb[31];
z[57] = -abb[19] + abb[21];
z[59] = z[53] * z[57];
z[60] = abb[18] * abb[35];
z[58] = -z[58] + -z[59] + (T(1) / T(2)) * z[60];
z[58] = abb[35] * z[58];
z[52] = (T(1) / T(3)) * z[52] + -z[54];
z[52] = z[20] * z[52];
z[59] = abb[19] * abb[32];
z[60] = abb[19] * abb[31];
z[59] = z[59] + -z[60];
z[59] = abb[31] * z[59];
z[61] = abb[5] + abb[6];
z[62] = -abb[15] + z[61];
z[62] = abb[3] + 2 * abb[29] + abb[16] * (T(-1) / T(2)) + (T(1) / T(2)) * z[62];
z[63] = -abb[68] * z[62];
z[54] = -abb[20] + z[54];
z[54] = abb[37] * z[54];
z[64] = -abb[18] + z[57];
z[64] = abb[36] * z[64];
z[65] = prod_pow(abb[32], 2);
z[66] = abb[19] * z[65];
z[60] = abb[33] * z[60];
z[67] = abb[18] + abb[21];
z[68] = abb[63] * z[67];
z[69] = abb[19] + abb[20];
z[70] = abb[64] * z[69];
z[22] = z[22] + -z[33] + z[52] + -z[54] + -z[58] + z[59] + -z[60] + z[63] + z[64] + (T(-1) / T(2)) * z[66] + z[68] + z[70];
z[33] = abb[59] + abb[60];
z[22] = -z[22] * z[33];
z[52] = abb[50] * (T(3) / T(2));
z[54] = (T(1) / T(2)) * z[7];
z[58] = abb[53] + abb[58] + -z[2] + z[27] + -z[52] + z[54];
z[58] = abb[3] * z[58];
z[59] = -abb[53] + z[0];
z[60] = z[54] + z[59];
z[63] = z[1] + z[5];
z[27] = -abb[57] + z[27] + -z[60] + z[63];
z[27] = abb[8] * z[27];
z[27] = z[27] + (T(1) / T(2)) * z[41];
z[41] = abb[47] * (T(1) / T(2));
z[64] = z[41] + z[63];
z[66] = -z[9] + z[64];
z[68] = abb[16] * z[66];
z[70] = 3 * abb[48];
z[71] = 2 * abb[51];
z[72] = abb[49] + z[71];
z[73] = z[70] + z[72];
z[74] = -abb[56] + z[73];
z[75] = -abb[58] + z[74];
z[76] = 2 * abb[1];
z[77] = z[75] * z[76];
z[78] = z[68] + z[77];
z[79] = 4 * abb[48];
z[80] = z[72] + z[79];
z[81] = -abb[47] + -abb[56] + z[80];
z[81] = abb[6] * z[81];
z[82] = 2 * abb[50];
z[83] = abb[55] + z[82];
z[84] = 3 * abb[58];
z[85] = z[83] + -z[84];
z[86] = -abb[57] + z[39];
z[87] = -abb[48] + abb[49] + -z[85] + z[86];
z[87] = abb[9] * z[87];
z[88] = abb[5] * abb[55];
z[89] = (T(1) / T(2)) * z[15];
z[90] = abb[5] + -abb[6];
z[91] = abb[58] * z[90];
z[92] = -abb[48] + z[30];
z[93] = -abb[55] + abb[57] + z[92];
z[93] = abb[0] * z[93];
z[58] = (T(1) / T(2)) * z[17] + -z[27] + -z[49] + z[58] + z[78] + z[81] + z[87] + -z[88] + -z[89] + z[91] + z[93];
z[58] = abb[30] * z[58];
z[81] = z[17] + -z[18];
z[87] = (T(1) / T(2)) * z[50];
z[89] = z[87] + -z[89];
z[91] = (T(1) / T(2)) * z[51];
z[93] = z[89] + z[91];
z[81] = z[68] + (T(1) / T(2)) * z[81] + -z[93];
z[94] = -z[30] + z[70];
z[95] = z[85] + z[94];
z[96] = 2 * abb[9];
z[95] = z[95] * z[96];
z[75] = abb[1] * z[75];
z[97] = 4 * z[75];
z[98] = z[81] + -z[95] + z[97];
z[99] = -z[8] + z[30];
z[100] = z[28] + z[99];
z[101] = -z[10] + z[100];
z[102] = abb[17] * z[101];
z[103] = abb[27] * z[16];
z[102] = z[102] + z[103];
z[19] = -abb[5] + z[19];
z[104] = z[19] * z[28];
z[104] = -z[102] + z[104];
z[105] = 2 * abb[56];
z[106] = 8 * abb[48] + -z[105];
z[107] = 5 * abb[51];
z[108] = z[35] + z[106] + z[107];
z[109] = -z[64] + -z[108];
z[109] = abb[6] * z[109];
z[110] = z[5] + -z[41] + -z[99];
z[111] = abb[55] * (T(3) / T(2));
z[112] = z[110] + z[111];
z[112] = abb[5] * z[112];
z[113] = 2 * abb[49];
z[114] = 3 * abb[51];
z[115] = -abb[56] + z[113] + z[114];
z[116] = abb[47] + z[8] + -z[82] + z[115];
z[117] = -abb[3] * z[116];
z[118] = -abb[55] + z[28];
z[9] = -z[9] + z[118];
z[9] = abb[0] * z[9];
z[9] = z[9] + -z[98] + z[104] + z[109] + z[112] + z[117];
z[9] = abb[31] * z[9];
z[109] = abb[47] * (T(3) / T(2));
z[112] = -z[1] + z[5] + z[99] + z[109];
z[112] = abb[5] * z[112];
z[117] = abb[47] + z[99];
z[119] = abb[3] * z[117];
z[119] = z[112] + z[119];
z[114] = abb[49] + z[114];
z[120] = z[79] + -z[105] + z[114];
z[121] = z[64] + z[120];
z[121] = abb[6] * z[121];
z[122] = abb[6] * z[28];
z[122] = -z[102] + z[122];
z[123] = 2 * abb[47];
z[124] = -abb[55] + z[123];
z[125] = -z[7] + -z[124];
z[125] = abb[0] * z[125];
z[81] = z[77] + z[81] + -z[119] + z[121] + -z[122] + z[125];
z[81] = abb[34] * z[81];
z[121] = 2 * abb[52];
z[125] = -z[35] + z[121];
z[126] = 5 * abb[57];
z[127] = 2 * abb[54];
z[128] = -abb[47] + z[39];
z[129] = z[125] + -z[126] + z[127] + z[128];
z[130] = 4 * abb[58];
z[131] = z[42] + -z[130];
z[132] = z[8] + -z[129] + z[131];
z[132] = abb[17] * z[132];
z[133] = 3 * abb[50];
z[134] = abb[55] + z[133];
z[135] = z[48] + z[100] + z[127] + -z[134];
z[135] = abb[3] * z[135];
z[136] = z[18] + z[51];
z[137] = z[50] + z[136];
z[138] = z[40] + z[137];
z[61] = z[28] * z[61];
z[61] = z[61] + z[138];
z[139] = z[39] + z[47];
z[140] = z[8] + z[72];
z[141] = z[139] + -z[140];
z[142] = abb[0] * z[141];
z[36] = -abb[54] + z[36];
z[143] = z[36] + z[85];
z[144] = z[96] * z[143];
z[145] = abb[6] * z[11];
z[144] = z[144] + z[145];
z[146] = abb[5] * z[11];
z[135] = z[61] + z[132] + z[135] + z[142] + -z[144] + -z[146];
z[135] = abb[33] * z[135];
z[142] = 2 * abb[55];
z[100] = -abb[47] + -z[100] + z[142];
z[100] = abb[0] * z[100];
z[101] = abb[3] * z[101];
z[137] = -z[15] + z[137];
z[122] = z[122] + -z[145];
z[100] = z[100] + -z[101] + -z[122] + -z[137];
z[100] = abb[32] * z[100];
z[9] = z[9] + z[58] + z[81] + z[100] + z[135];
z[9] = abb[30] * z[9];
z[58] = abb[57] * (T(1) / T(2));
z[81] = abb[53] + z[0] + -z[8] + z[28] + -z[58] + -z[64] + (T(-1) / T(2)) * z[114];
z[81] = abb[17] * z[81];
z[71] = -z[71] + z[82];
z[58] = -abb[48] + z[58];
z[101] = -abb[53] + abb[49] * (T(3) / T(2));
z[114] = abb[58] + z[41] + -z[58] + -z[71] + z[101];
z[114] = abb[3] * z[114];
z[135] = 5 * abb[48];
z[145] = z[72] + z[135];
z[85] = -z[85] + -z[86] + z[105] + -z[145];
z[85] = abb[9] * z[85];
z[106] = abb[55] + 3 * z[72] + z[106] + -z[128] + -z[130];
z[106] = abb[1] * z[106];
z[147] = 2 * z[73] + -z[105];
z[148] = z[64] + z[147];
z[148] = abb[6] * z[148];
z[54] = z[54] + -z[58] + -z[59];
z[54] = abb[0] * z[54];
z[149] = -z[18] + z[103];
z[150] = abb[5] + -3 * abb[6];
z[150] = abb[58] * z[150];
z[54] = z[6] + z[54] + z[81] + z[85] + -z[88] + -z[91] + z[106] + z[114] + z[148] + (T(1) / T(2)) * z[149] + z[150];
z[54] = abb[35] * z[54];
z[81] = -abb[55] + z[115];
z[85] = z[8] + z[81];
z[91] = -z[28] + z[133];
z[106] = z[85] + -z[91];
z[106] = abb[3] * z[106];
z[114] = 4 * abb[6];
z[148] = z[74] * z[114];
z[149] = z[12] + -z[17];
z[150] = -abb[47] + z[30];
z[151] = abb[0] * z[150];
z[148] = z[148] + -z[149] + z[151];
z[104] = z[104] + z[146];
z[75] = -6 * z[75] + z[95] + z[104] + -z[106] + -z[148];
z[53] = z[53] * z[75];
z[75] = -abb[58] + z[7] + z[36];
z[75] = abb[17] * z[75];
z[95] = abb[5] * abb[58];
z[75] = z[75] + -z[88] + z[95];
z[88] = abb[9] * z[143];
z[106] = abb[48] + z[82];
z[143] = -abb[54] + -abb[58] + z[106];
z[143] = abb[3] * z[143];
z[151] = -abb[57] + abb[58];
z[151] = abb[1] * z[151];
z[88] = z[46] + -z[75] + z[88] + z[143] + z[151];
z[88] = abb[33] * z[88];
z[143] = -abb[50] + abb[55];
z[151] = z[99] + z[143];
z[152] = abb[7] * z[151];
z[136] = z[136] + z[152];
z[153] = -abb[55] + abb[58];
z[153] = z[76] * z[153];
z[154] = abb[0] * z[117];
z[122] = z[122] + z[136] + z[153] + z[154];
z[153] = abb[34] * z[122];
z[53] = z[53] + z[54] + 2 * z[88] + z[153];
z[53] = abb[35] * z[53];
z[32] = 3 * abb[53] + abb[57] * (T(-7) / T(2)) + -z[32] + z[34] + -z[70] + -z[109] + -z[131];
z[32] = abb[17] * z[32];
z[54] = 4 * abb[50];
z[70] = 6 * abb[58];
z[88] = z[54] + -z[70] + z[142];
z[131] = -z[105] + z[140];
z[153] = 4 * abb[54];
z[155] = 4 * abb[57];
z[156] = abb[52] + -z[39] + -z[88] + -z[131] + z[153] + -z[155];
z[156] = abb[9] * z[156];
z[58] = z[41] + z[58];
z[157] = abb[55] + -abb[56];
z[158] = -abb[51] + z[157];
z[159] = -abb[52] + z[101];
z[160] = -abb[54] + -z[58] + z[91] + z[158] + -z[159];
z[160] = abb[3] * z[160];
z[161] = -z[127] + z[139];
z[162] = z[131] + z[161];
z[162] = abb[1] * z[162];
z[163] = z[82] + -z[140] + z[161];
z[164] = abb[13] * z[163];
z[58] = abb[53] + -abb[54] + abb[49] * (T(1) / T(2)) + z[58];
z[58] = abb[0] * z[58];
z[85] = z[85] + -z[133];
z[85] = abb[6] * z[85];
z[133] = -z[10] + z[99];
z[133] = abb[5] * z[133];
z[32] = -z[32] + z[49] + z[58] + z[61] + z[85] + z[133] + z[156] + -z[160] + z[162] + z[164];
z[58] = abb[67] * z[32];
z[61] = 3 * abb[57];
z[0] = -abb[48] + abb[54] * (T(-7) / T(2)) + -z[0] + z[1] + z[3] + z[29] + z[52] + z[61];
z[0] = abb[3] * z[0];
z[3] = z[71] + -z[159];
z[29] = abb[54] * (T(3) / T(2));
z[71] = -abb[47] + -z[3] + -z[29] + z[61];
z[71] = abb[13] * z[71];
z[2] = -abb[47] + abb[49] + abb[50] * (T(-7) / T(2)) + abb[51] * (T(3) / T(2)) + -z[2] + z[84];
z[2] = abb[4] * z[2];
z[2] = z[2] + -z[71];
z[71] = 2 * z[46];
z[84] = -abb[50] + abb[54];
z[84] = abb[0] * z[84];
z[85] = -abb[57] + z[118];
z[118] = abb[1] * z[85];
z[0] = z[0] + z[2] + -z[6] + -z[27] + -z[71] + z[75] + z[84] + -z[87] + z[118];
z[0] = abb[33] * z[0];
z[6] = abb[0] * z[116];
z[27] = -abb[47] + z[28];
z[75] = -z[27] + z[106];
z[84] = 2 * abb[3];
z[75] = z[75] * z[84];
z[87] = z[13] * z[114];
z[106] = 7 * abb[50];
z[81] = -z[81] + z[106];
z[116] = z[81] + z[123];
z[118] = -z[70] + z[116];
z[118] = abb[4] * z[118];
z[6] = -z[6] + -z[75] + -z[87] + z[104] + z[118];
z[75] = abb[31] * z[6];
z[0] = z[0] + z[75];
z[0] = abb[33] * z[0];
z[64] = z[64] + -z[120];
z[64] = abb[6] * z[64];
z[75] = -z[27] + z[140] + z[157];
z[75] = abb[0] * z[75];
z[87] = -z[17] + -z[18];
z[104] = z[11] + -z[28];
z[104] = abb[3] * z[104];
z[64] = z[64] + z[75] + -z[78] + (T(1) / T(2)) * z[87] + -z[93] + z[104] + z[112];
z[64] = abb[31] * z[64];
z[75] = -abb[33] * z[122];
z[78] = abb[6] * z[117];
z[87] = z[76] * z[92];
z[78] = z[78] + z[87] + -z[154];
z[78] = abb[34] * z[78];
z[64] = z[64] + z[75] + z[78] + -z[100];
z[64] = abb[34] * z[64];
z[75] = -z[70] + z[79];
z[78] = abb[54] * (T(1) / T(2)) + z[48] + -z[75] + z[101] + -z[106] + -z[158];
z[78] = abb[3] * z[78];
z[75] = -z[37] + -z[75] + -z[107] + z[129];
z[75] = abb[17] * z[75];
z[87] = abb[6] * z[70];
z[75] = z[75] + -z[87] + z[103] + -z[138];
z[3] = z[3] + -z[29] + -z[123];
z[3] = abb[13] * z[3];
z[29] = abb[0] * z[163];
z[87] = abb[6] * z[116];
z[3] = -z[3] + -z[29] + z[75] + -z[78] + z[87] + -z[118] + -z[156];
z[29] = -abb[65] * z[3];
z[45] = z[30] + z[45] + -z[143];
z[45] = abb[3] * z[45];
z[78] = -abb[57] + z[128];
z[38] = z[38] + z[78];
z[38] = abb[17] * z[38];
z[38] = -z[38] + z[40] + z[103];
z[40] = -z[105] + z[142];
z[78] = -abb[49] + z[40] + z[78];
z[78] = abb[1] * z[78];
z[87] = abb[5] * z[151];
z[45] = z[38] + z[45] + z[46] + z[49] + z[50] + z[78] + z[87] + -z[152];
z[50] = abb[66] * z[45];
z[78] = abb[49] + z[39];
z[87] = 5 * abb[54];
z[78] = 2 * z[78] + -z[87] + -z[121] + z[126] + -z[135];
z[78] = abb[9] * z[78];
z[93] = abb[47] + z[105] + -z[134] + -2 * z[145];
z[93] = abb[6] * z[93];
z[101] = abb[52] + z[39];
z[104] = z[101] + -z[127] + z[131];
z[104] = abb[3] * z[104];
z[44] = abb[54] + -z[44] + z[150];
z[44] = abb[0] * z[44];
z[44] = z[44] + z[75] + z[78] + -z[93] + z[104] + -z[164];
z[75] = -z[17] + -z[44];
z[75] = abb[63] * z[75];
z[30] = -z[30] + -z[91] + -z[124];
z[30] = abb[3] * z[30];
z[78] = -z[28] * z[90];
z[91] = -abb[47] + -abb[58] + -z[7] + z[10];
z[93] = 2 * abb[0];
z[91] = z[91] * z[93];
z[104] = abb[55] + -3 * z[13];
z[104] = abb[6] * z[104];
z[30] = z[30] + z[78] + z[91] + z[104] + z[118] + -z[137] + z[146];
z[30] = abb[37] * z[30];
z[6] = -abb[32] * z[6];
z[27] = z[27] + -z[79];
z[78] = z[27] + -z[115];
z[78] = abb[0] * z[78];
z[91] = -abb[47] + abb[48];
z[91] = z[91] * z[114];
z[104] = abb[5] * z[117];
z[106] = abb[58] + -z[13];
z[84] = z[84] * z[106];
z[77] = z[77] + z[78] + z[84] + z[91] + z[104];
z[77] = abb[31] * z[77];
z[6] = z[6] + z[77];
z[6] = abb[31] * z[6];
z[11] = z[11] + -z[147];
z[11] = abb[6] * z[11];
z[77] = abb[3] * z[151];
z[78] = -z[92] * z[93];
z[14] = abb[5] * z[14];
z[11] = z[11] + z[14] + -z[17] + z[77] + z[78] + -z[136];
z[11] = abb[36] * z[11];
z[5] = -z[5] + z[7] + z[109];
z[5] = abb[18] * z[5];
z[77] = -z[1] + z[110];
z[77] = abb[19] * z[77];
z[66] = abb[21] * z[66];
z[16] = abb[28] * z[16];
z[78] = abb[20] * z[13];
z[16] = z[16] + -z[78];
z[24] = z[1] * z[24];
z[5] = -z[5] + (T(1) / T(2)) * z[16] + z[24] + z[66] + z[77];
z[16] = -abb[68] * z[5];
z[24] = abb[58] + abb[57] * (T(-5) / T(2)) + (T(-3) / T(2)) * z[7] + z[34] + -z[41] + -z[59] + z[63];
z[24] = abb[17] * z[24];
z[31] = -abb[54] + z[13] + -z[28] + z[31] + z[60];
z[31] = abb[0] * z[31];
z[34] = -abb[54] + z[47];
z[47] = abb[3] * z[34];
z[47] = -z[47] + -z[103] + z[146];
z[2] = -z[2] + z[24] + z[31] + (T(1) / T(2)) * z[47] + z[49] + -z[95];
z[2] = z[2] * z[65];
z[24] = abb[56] + z[7] + -z[86] + z[123] + -z[142];
z[24] = abb[0] * z[24];
z[31] = abb[3] * z[48];
z[47] = 2 * z[49];
z[14] = z[14] + -z[15] + z[24] + -z[31] + -z[38] + -z[47];
z[15] = -abb[64] * z[14];
z[24] = -16 * abb[48] + 4 * abb[56] + z[70] + -5 * z[72] + z[161];
z[31] = abb[63] * z[24];
z[38] = abb[55] + -z[120];
z[38] = abb[36] * z[38];
z[31] = z[31] + 2 * z[38];
z[31] = abb[1] * z[31];
z[7] = z[7] + z[28] + -z[83];
z[38] = abb[71] * z[7];
z[48] = abb[36] + abb[63];
z[12] = z[12] * z[48];
z[20] = (T(1) / T(4)) * z[20] + -3 * z[65];
z[20] = abb[2] * z[20] * z[34];
z[0] = abb[69] + abb[70] + z[0] + z[2] + z[4] + z[6] + z[9] + z[11] + z[12] + z[15] + z[16] + z[20] + z[22] + z[29] + z[30] + z[31] + z[38] + z[50] + z[53] + z[58] + z[64] + z[75];
z[2] = -3 * abb[54] + 6 * abb[57] + -z[39] + z[42] + -z[54] + -z[123] + -z[125];
z[2] = abb[13] * z[2];
z[4] = -5 * abb[49] + -6 * abb[51] + 4 * abb[52] + -9 * abb[57] + -z[8] + z[70] + z[128] + z[153];
z[4] = abb[17] * z[4];
z[6] = abb[47] + -7 * abb[57] + z[28] + z[79] + z[87];
z[6] = abb[3] * z[6];
z[9] = z[82] + -z[127] + -z[141];
z[9] = abb[0] * z[9];
z[11] = -2 * abb[5] + -abb[6];
z[11] = z[11] * z[28];
z[12] = -z[76] * z[85];
z[15] = 3 * abb[55] + z[13];
z[15] = abb[5] * z[15];
z[2] = z[2] + z[4] + z[6] + z[9] + z[11] + z[12] + z[15] + 4 * z[46] + z[118] + -z[136] + z[144];
z[2] = abb[33] * z[2];
z[4] = -z[52] + -z[109] + -z[111] + -z[120] + z[130];
z[4] = abb[6] * z[4];
z[6] = -abb[49] + z[8];
z[9] = -z[6] + z[26] + -z[157];
z[9] = abb[0] * z[9];
z[11] = z[28] + -z[73] + -z[157];
z[11] = z[11] * z[76];
z[12] = 2 * z[102];
z[16] = -z[17] + 3 * z[18];
z[4] = z[4] + z[9] + z[11] + -z[12] + (T(1) / T(2)) * z[16] + (T(3) / T(2)) * z[51] + -z[68] + z[89] + z[119] + z[152];
z[4] = abb[34] * z[4];
z[9] = z[26] + z[79] + z[82] + z[115] + -z[130];
z[9] = abb[3] * z[9];
z[11] = -z[27] + z[35] + z[42] + -z[82] + z[157];
z[11] = abb[0] * z[11];
z[1] = z[1] + (T(9) / T(2)) * z[13] + z[108];
z[1] = abb[6] * z[1];
z[13] = abb[55] * (T(-5) / T(2)) + -z[41] + -z[52] + z[99];
z[13] = abb[5] * z[13];
z[16] = -z[19] * z[130];
z[1] = z[1] + z[9] + z[11] + z[12] + z[13] + z[16] + z[98] + -z[118];
z[1] = abb[31] * z[1];
z[1] = z[1] + z[2] + z[4] + -z[100];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[49] + z[61];
z[4] = -z[2] + z[40] + z[42] + z[79] + -z[128];
z[4] = abb[0] * z[4];
z[9] = z[10] + z[26] + -2 * z[80] + z[105];
z[9] = abb[6] * z[9];
z[10] = z[127] + z[130];
z[6] = 6 * abb[50] + z[6] + -z[10] + -z[139] + z[142];
z[6] = abb[3] * z[6];
z[2] = abb[48] + -abb[54] + z[2] + z[88] + -z[101];
z[2] = z[2] * z[96];
z[11] = -abb[5] * z[130];
z[2] = z[2] + z[4] + z[6] + z[9] + z[11] + z[15] + z[47] + -z[97] + -z[132] + -z[137] + z[149];
z[4] = abb[30] * m1_set::bc<T>[0];
z[2] = z[2] * z[4];
z[6] = abb[42] * z[32];
z[8] = abb[51] + -z[8] + -z[37] + z[113] + -z[121] + -z[127] + z[155];
z[8] = abb[17] * z[8];
z[9] = z[10] + -z[81];
z[9] = abb[3] * z[9];
z[10] = -z[36] + -z[88] + -z[94];
z[10] = z[10] * z[96];
z[11] = z[90] * z[130];
z[12] = abb[57] + 3 * z[74] + -z[130];
z[12] = z[12] * z[76];
z[8] = z[8] + z[9] + z[10] + z[11] + z[12] + -z[15] + -z[71] + z[103] + z[148];
z[9] = abb[35] * m1_set::bc<T>[0];
z[8] = z[8] * z[9];
z[3] = -abb[40] * z[3];
z[10] = abb[41] * z[45];
z[11] = abb[1] * z[24];
z[11] = z[11] + -z[44] + z[149];
z[11] = abb[38] * z[11];
z[12] = z[25] + z[56];
z[12] = z[12] * z[43];
z[13] = z[21] + z[57];
z[13] = z[13] * z[23];
z[12] = z[12] + z[13] + z[55];
z[12] = m1_set::bc<T>[0] * z[12];
z[13] = z[56] + z[57];
z[4] = z[4] * z[13];
z[9] = z[9] * z[57];
z[13] = abb[43] * z[62];
z[15] = abb[38] * z[67];
z[16] = abb[39] * z[69];
z[4] = -z[4] + z[9] + z[12] + z[13] + -z[15] + -z[16];
z[4] = z[4] * z[33];
z[5] = -abb[43] * z[5];
z[9] = -abb[39] * z[14];
z[7] = abb[46] * z[7];
z[1] = abb[44] + abb[45] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_650_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-6.29528515329787257381662736342755648630045211758223188305213529"),stof<T>("-64.523801920969199735358964251936598490033742049441984430025219812")}, std::complex<T>{stof<T>("-33.191100308207320131550318266562191383193779680128316816138219234"),stof<T>("-46.221600699162129032767430418644372766945054376522902069174229204")}, std::complex<T>{stof<T>("12.099491000985158508890548363100308502866350863069668498960914948"),stof<T>("-72.650642308036528454707782944359898074474816556365823300717959959")}, std::complex<T>{stof<T>("10.808913559091387235236103361320073994482499127648662398962360616"),stof<T>("6.4077927811544311565818295087385280728529920676325660171598369699")}, std::complex<T>{stof<T>("-9.556190782033000772068973201238137127998521344493552120569657174"),stof<T>("-89.775795015664713550209997856273018164177651713319197598178604883")}, std::complex<T>{stof<T>("-45.936503567357004834645510728662396876855909934067356986017349428"),stof<T>("33.735508803839325404217870049469907446528133334582252098590365979")}, std::complex<T>{stof<T>("5.988687094436881345811310662738584446203671990155887283914284001"),stof<T>("45.889135226615389161817956217069956419934936560811249088292254641")}, std::complex<T>{stof<T>("-6.9020956721917328427791134245095963619886505098679704905266234863"),stof<T>("11.5377331185761566759916865718435094622925464415053162839469128778")}, std::complex<T>{stof<T>("-0.6687458038860240805037139439018829383487291920268642821032152032"),stof<T>("-4.3436871840284605831045519526160562417641996288129021997010243338")}, std::complex<T>{stof<T>("18.661338235799718608053866232969153407763036212485276977573430121"),stof<T>("-5.819414905679509485406763196621858120264633123452250246685482398")}, std::complex<T>{stof<T>("37.665384064594267310429178539118502960545464236064112453467116866"),stof<T>("17.484370262917336362250816970612986793324841172198201351119013293")}, std::complex<T>{stof<T>("-7.0664435812360512966274921664650092807010245908202515927432043125"),stof<T>("7.8289245255777394387701317963110225412405031692603897496028551735")}, std::complex<T>{stof<T>("20.828671718068656960858043900138353052111968657709009977738752719"),stof<T>("-10.368412862013484354461038273863068126665179185963628602527497006")}, std::complex<T>{stof<T>("20.828671718068656960858043900138353052111968657709009977738752719"),stof<T>("-10.368412862013484354461038273863068126665179185963628602527497006")}, std::complex<T>{stof<T>("8.2392111501723654087725285414709311842632066857929314133244832248"),stof<T>("-2.689250513928415495653135252175836801630141918964692483472961784")}, std::complex<T>{stof<T>("8.2392111501723654087725285414709311842632066857929314133244832248"),stof<T>("-2.689250513928415495653135252175836801630141918964692483472961784")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(abs(k.W[83])) - rlog(abs(kbase.W[83])), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[126].real()/kbase.W[126].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_650_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_650_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (-v[3] + v[5]) * (-8 + 12 * v[0] + -4 * v[1] + v[3] + -5 * v[5] + 2 * m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = ((1 + m1_set::bc<T>[1]) * (-v[3] + v[5])) / tend;


		return (abb[47] + abb[50] + -abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[30] + -abb[34];
z[1] = abb[31] + z[0];
z[1] = abb[31] * z[1];
z[0] = abb[32] + z[0];
z[0] = abb[32] * z[0];
z[0] = -abb[37] + -z[0] + z[1];
z[1] = abb[47] + abb[50] + -abb[58];
return 2 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_650_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[3] + -v[5])) / tend;


		return (abb[47] + abb[50] + -abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[31] + abb[32];
z[1] = abb[47] + abb[50] + -abb[58];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_650_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(4)) * (v[1] + v[2] + -v[3] + -v[4]) * (3 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 2 * (-4 + -4 * v[0] + v[1] + -v[2] + -2 * v[3] + v[4] + 2 * v[5]))) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[48] + abb[51] + -abb[56]) * (t * c[0] + c[1]);
	}
	{
T z[3];
z[0] = abb[30] + -abb[31];
z[1] = abb[48] + abb[51] + -abb[56];
z[0] = z[0] * z[1];
z[2] = abb[34] * z[1];
z[2] = -z[0] + z[2];
z[2] = abb[34] * z[2];
z[1] = -abb[35] * z[1];
z[0] = z[0] + z[1];
z[0] = abb[35] * z[0];
z[0] = z[0] + z[2];
return 2 * abb[12] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_650_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[48] + abb[51] + -abb[56]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[34] + -abb[35];
z[1] = abb[48] + abb[51] + -abb[56];
return 2 * abb[12] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_650_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-126.507991365947897700912162513529227247739084552934068024746558466"),stof<T>("-5.948048920541609705065113771683172881204344917769751691760698988")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 19, 83});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,72> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W20(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W28(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_16(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), T{0}, T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(abs(kend.W[83])) - rlog(abs(k.W[83])), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[126].real()/k.W[126].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k), T{0}, T{0}, T{0}};
abb[44] = SpDLog_f_4_650_W_16_Im(t, path, abb);
abb[45] = SpDLog_f_4_650_W_20_Im(t, path, abb);
abb[46] = SpDLogQ_W_84(k,dl,dlr).imag();
abb[69] = SpDLog_f_4_650_W_16_Re(t, path, abb);
abb[70] = SpDLog_f_4_650_W_20_Re(t, path, abb);
abb[71] = SpDLogQ_W_84(k,dl,dlr).real();

                    
            return f_4_650_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_650_DLogXconstant_part(base_point<T>, kend);
	value += f_4_650_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_650_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_650_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_650_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_650_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_650_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_650_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
