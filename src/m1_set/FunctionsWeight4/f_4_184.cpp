/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_184.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_184_abbreviated (const std::array<T,29>& abb) {
T z[55];
z[0] = 2 * abb[22];
z[1] = 3 * abb[28];
z[2] = z[0] + -z[1];
z[3] = -abb[19] + abb[23];
z[4] = z[2] + z[3];
z[5] = 2 * abb[21];
z[6] = 2 * abb[26];
z[7] = z[5] + -z[6];
z[8] = 2 * abb[24];
z[9] = z[4] + z[7] + z[8];
z[10] = abb[7] * z[9];
z[11] = 2 * abb[20];
z[12] = 2 * abb[27];
z[13] = z[11] + -z[12];
z[14] = abb[19] + abb[23];
z[15] = -z[1] + z[14];
z[16] = -z[8] + z[13] + -z[15];
z[16] = abb[1] * z[16];
z[10] = z[10] + -z[16];
z[17] = -z[3] + z[6];
z[18] = z[2] + -z[8] + z[11] + z[17];
z[19] = abb[5] * z[18];
z[20] = -z[2] + z[3];
z[21] = 2 * abb[25];
z[22] = -z[5] + -z[12] + z[20] + z[21];
z[23] = abb[6] * z[22];
z[13] = z[4] + z[13] + z[21];
z[24] = abb[8] * z[13];
z[7] = z[7] + -z[15] + -z[21];
z[7] = abb[2] * z[7];
z[15] = 6 * abb[28];
z[0] = -z[0] + z[15];
z[25] = abb[26] + abb[27];
z[26] = abb[24] + abb[25];
z[27] = z[25] + z[26];
z[28] = z[14] + z[27];
z[29] = z[0] + -z[28];
z[29] = abb[3] * z[29];
z[30] = 2 * z[29];
z[31] = z[25] + -z[26];
z[32] = abb[20] + abb[21];
z[20] = -z[20] + z[31] + z[32];
z[20] = abb[4] * z[20];
z[33] = 4 * abb[22];
z[34] = abb[21] + z[33];
z[28] = abb[20] + -9 * abb[28] + z[28] + z[34];
z[28] = abb[9] * z[28];
z[20] = -z[7] + z[10] + z[19] + z[20] + -z[23] + z[24] + -3 * z[28] + -z[30];
z[20] = abb[18] * z[20];
z[35] = 2 * abb[19];
z[36] = -abb[23] + z[35];
z[37] = z[0] + z[36];
z[38] = 3 * abb[24];
z[39] = abb[20] + z[21];
z[40] = abb[26] + -z[5] + z[37] + -z[38] + -z[39];
z[40] = abb[15] * z[40];
z[41] = 3 * abb[25];
z[42] = abb[21] + z[8];
z[37] = abb[27] + -z[11] + z[37] + -z[41] + -z[42];
z[37] = abb[14] * z[37];
z[4] = z[4] + -z[31] + z[32];
z[4] = abb[18] * z[4];
z[31] = abb[19] + z[1];
z[43] = abb[24] + abb[26];
z[39] = -z[31] + z[39] + z[43];
z[44] = prod_pow(abb[13], 2);
z[45] = -z[39] * z[44];
z[46] = abb[25] + abb[27];
z[42] = -z[31] + z[42] + z[46];
z[47] = prod_pow(abb[11], 2);
z[48] = -z[42] * z[47];
z[4] = z[4] + z[37] + z[40] + z[45] + z[48];
z[4] = abb[0] * z[4];
z[37] = 2 * abb[23];
z[0] = -abb[19] + z[0] + z[37];
z[40] = 3 * abb[27];
z[6] = abb[20] + z[6];
z[5] = abb[25] + z[0] + -z[5] + -z[6] + -z[40];
z[5] = abb[4] * z[5];
z[30] = 2 * z[28] + z[30];
z[13] = -abb[0] * z[13];
z[45] = z[2] + z[35];
z[48] = abb[25] + -abb[27];
z[49] = abb[23] + -z[48];
z[50] = -abb[20] + z[45] + z[49];
z[50] = abb[8] * z[50];
z[5] = z[5] + z[13] + z[16] + z[23] + z[30] + z[50];
z[5] = abb[16] * z[5];
z[13] = abb[23] + z[1];
z[6] = z[6] + -z[13] + z[46];
z[6] = abb[4] * z[6];
z[23] = -z[15] + z[35];
z[35] = z[23] + z[34] + z[49];
z[35] = abb[6] * z[35];
z[46] = -z[28] + z[35];
z[49] = -abb[19] + z[15];
z[50] = -z[37] + z[49];
z[51] = abb[24] + -abb[26];
z[34] = -z[34] + z[50] + -z[51];
z[34] = abb[7] * z[34];
z[6] = z[6] + 2 * z[7] + -z[34] + z[46];
z[44] = -z[6] * z[44];
z[52] = abb[21] + z[12];
z[43] = -z[13] + z[43] + z[52];
z[43] = abb[4] * z[43];
z[33] = abb[20] + z[33];
z[50] = z[33] + z[48] + -z[50];
z[50] = abb[8] * z[50];
z[53] = -z[28] + z[50];
z[54] = -abb[23] + z[51];
z[23] = -z[23] + -z[33] + z[54];
z[23] = abb[5] * z[23];
z[16] = 2 * z[16] + -z[23] + z[43] + z[53];
z[33] = -z[16] * z[47];
z[43] = abb[22] + z[32];
z[15] = z[15] + z[36] + -z[38] + -z[41] + -z[43];
z[15] = abb[0] * z[15];
z[36] = -abb[19] + -abb[22] + z[26];
z[36] = abb[4] * z[36];
z[15] = z[15] + -z[23] + z[29] + z[35] + z[36];
z[15] = prod_pow(abb[10], 2) * z[15];
z[35] = 3 * abb[26];
z[36] = -z[35] + z[37] + -z[40] + -z[43] + z[49];
z[36] = abb[4] * z[36];
z[38] = -abb[22] + -abb[23] + z[25];
z[38] = abb[0] * z[38];
z[36] = z[29] + -z[34] + z[36] + z[38] + z[50];
z[36] = abb[12] * z[36];
z[13] = -z[13] + z[25] + z[43];
z[13] = abb[4] * z[13];
z[31] = z[26] + -z[31] + z[43];
z[31] = abb[0] * z[31];
z[13] = z[13] + -z[28] + -z[29] + z[31];
z[13] = abb[10] * z[13];
z[13] = 2 * z[13] + z[36];
z[13] = abb[12] * z[13];
z[7] = z[7] + z[30];
z[0] = abb[24] + z[0] + -z[11] + -z[35] + -z[52];
z[0] = abb[4] * z[0];
z[9] = -abb[0] * z[9];
z[11] = -abb[21] + z[45] + -z[54];
z[11] = abb[7] * z[11];
z[0] = z[0] + z[7] + z[9] + z[11] + -z[19];
z[0] = abb[17] * z[0];
z[9] = -abb[4] * z[18];
z[2] = abb[19] + z[2] + z[37];
z[11] = -abb[20] + z[2] + z[51];
z[11] = abb[5] * z[11];
z[9] = z[9] + -z[10] + z[11] + z[30];
z[9] = abb[15] * z[9];
z[10] = abb[4] * z[22];
z[2] = -abb[21] + z[2] + z[48];
z[2] = abb[6] * z[2];
z[2] = z[2] + z[7] + z[10] + -z[24];
z[2] = abb[14] * z[2];
z[7] = abb[19] * (T(1) / T(2));
z[10] = z[7] + z[25];
z[11] = abb[28] * (T(1) / T(2));
z[18] = abb[23] * (T(1) / T(6));
z[10] = (T(1) / T(3)) * z[10] + -z[11] + -z[18];
z[10] = abb[4] * z[10];
z[11] = z[11] + -z[18];
z[18] = -z[7] + z[26];
z[18] = -z[11] + (T(1) / T(3)) * z[18];
z[18] = abb[0] * z[18];
z[19] = abb[20] + -abb[24] + -abb[27] + -z[7];
z[19] = z[11] + (T(1) / T(3)) * z[19];
z[19] = abb[1] * z[19];
z[7] = abb[21] + -abb[25] + -abb[26] + -z[7];
z[7] = (T(1) / T(3)) * z[7] + z[11];
z[7] = abb[2] * z[7];
z[7] = z[7] + z[10] + z[18] + z[19];
z[7] = prod_pow(m1_set::bc<T>[0], 2) * z[7];
z[0] = z[0] + z[2] + z[4] + z[5] + 13 * z[7] + z[9] + z[13] + z[15] + z[20] + z[33] + z[44];
z[2] = abb[13] * z[6];
z[4] = abb[11] * z[16];
z[5] = z[1] + z[14] + -z[27] + -z[32];
z[6] = abb[4] * z[5];
z[3] = -z[1] + z[3] + z[8] + z[21];
z[3] = abb[0] * z[3];
z[3] = z[3] + z[6] + z[23] + -z[46];
z[3] = abb[10] * z[3];
z[5] = abb[0] * z[5];
z[1] = -z[1] + z[12] + z[17];
z[1] = abb[4] * z[1];
z[1] = z[1] + z[5] + z[34] + -z[53];
z[1] = abb[12] * z[1];
z[5] = abb[13] * z[39];
z[6] = abb[11] * z[42];
z[5] = z[5] + z[6];
z[5] = abb[0] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
z[1] = 2 * m1_set::bc<T>[0] * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_184_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("18.300227979244174754177440303650905905041095651075879335953870112"),stof<T>("62.335608215608560595883135067045514013895742653096940569788514956")}, std::complex<T>{stof<T>("17.71272534747205589667930264597781207359036815194968801768315853"),stof<T>("-33.465296087906324236153589746562718056467315063434000735166874016")}, std::complex<T>{stof<T>("17.71272534747205589667930264597781207359036815194968801768315853"),stof<T>("-33.465296087906324236153589746562718056467315063434000735166874016")}, std::complex<T>{stof<T>("11.012247252425002881412267218261563610813936656951635871957270791"),stof<T>("-16.500351407838480748118610891991576349144115247672166123082441048")}, std::complex<T>{stof<T>("-42.71343142176328366612377837734496644140789529802361949936291638"),stof<T>("-11.905367447634392871694566465911654250105227773901105222537207971")}, std::complex<T>{stof<T>("-42.113846325409059872239555463189698702316194608236513216449707756"),stof<T>("-30.176791520874157683235238844185307401911244714013240162995171495")}, std::complex<T>{stof<T>("-42.113846325409059872239555463189698702316194608236513216449707756"),stof<T>("-30.176791520874157683235238844185307401911244714013240162995171495")}, std::complex<T>{stof<T>("18.899813075598398548061663217806173644132796340862985618867078735"),stof<T>("44.064184142368795784342462688771860862089725712984805629330551432")}, std::complex<T>{stof<T>("18.899813075598398548061663217806173644132796340862985618867078735"),stof<T>("44.064184142368795784342462688771860862089725712984805629330551432")}, std::complex<T>{stof<T>("18.302678996078487664148437540682942171054192415632837588438037345"),stof<T>("3.919438179515763970517080571107534333448451373050900985120591667")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_184_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_184_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.356269263303538086958742183201720301919498265806740014705378478"),stof<T>("-17.553508724650553338357156634061763956652816578298129487911335813")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), f_1_1(k), f_1_3(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real())};

                    
            return f_4_184_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_184_DLogXconstant_part(base_point<T>, kend);
	value += f_4_184_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_184_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_184_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_184_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_184_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_184_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_184_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
