/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_979.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_979_abbreviated (const std::array<T,96>& abb) {
T z[154];
z[0] = 2 * abb[0];
z[1] = abb[4] + abb[5];
z[2] = z[0] + z[1];
z[3] = abb[6] + -abb[7];
z[4] = 2 * abb[11];
z[5] = z[2] + z[3] + -z[4];
z[5] = abb[74] * z[5];
z[6] = abb[6] + abb[7];
z[7] = z[0] + z[6];
z[8] = abb[4] + -abb[5];
z[9] = z[7] + z[8];
z[9] = abb[73] * z[9];
z[10] = abb[26] + abb[28];
z[11] = -2 * abb[65] + abb[71] + abb[72];
z[12] = z[10] * z[11];
z[13] = 2 * z[12];
z[14] = 2 * abb[25];
z[15] = z[11] * z[14];
z[5] = z[5] + -z[9] + z[13] + z[15];
z[5] = abb[36] * z[5];
z[9] = 2 * abb[12];
z[2] = z[2] + -z[3] + -z[9];
z[2] = abb[73] * z[2];
z[7] = z[7] + -z[8];
z[7] = abb[74] * z[7];
z[16] = abb[14] * abb[74];
z[17] = 2 * z[16];
z[18] = 2 * z[11];
z[19] = abb[24] * z[18];
z[20] = z[17] + z[19];
z[2] = z[2] + -z[7] + z[13] + z[20];
z[2] = abb[41] * z[2];
z[7] = abb[35] + abb[40];
z[13] = abb[36] + abb[41];
z[21] = z[7] + -z[13];
z[22] = abb[46] * z[21];
z[23] = abb[41] * abb[47];
z[24] = abb[36] * abb[47];
z[25] = z[23] + z[24];
z[26] = abb[38] * abb[47];
z[26] = -z[25] + z[26];
z[27] = -abb[39] + z[13];
z[28] = -abb[38] + z[27];
z[29] = abb[49] * z[28];
z[30] = abb[39] * abb[47];
z[22] = z[22] + z[26] + -z[29] + z[30];
z[29] = abb[48] * z[21];
z[29] = z[22] + z[29];
z[29] = abb[89] * z[29];
z[31] = abb[78] * z[27];
z[28] = abb[80] * z[28];
z[32] = abb[38] * abb[78];
z[28] = z[28] + z[31] + -z[32];
z[31] = abb[58] * z[28];
z[33] = -abb[6] + abb[14];
z[34] = -abb[4] + abb[12];
z[35] = z[33] + -z[34];
z[36] = abb[41] * z[35];
z[37] = -abb[5] + abb[11];
z[38] = abb[7] + z[37];
z[39] = abb[36] * z[38];
z[36] = z[36] + -z[39];
z[39] = 2 * abb[75];
z[36] = z[36] * z[39];
z[40] = abb[30] * abb[36];
z[41] = z[18] * z[40];
z[2] = z[2] + z[5] + z[29] + -z[31] + -z[36] + z[41];
z[5] = -abb[11] + -abb[12] + abb[14];
z[29] = z[5] * z[39];
z[31] = abb[30] * z[18];
z[36] = -abb[73] + abb[75];
z[41] = abb[13] * z[36];
z[20] = z[15] + z[20] + -z[29] + z[31] + -2 * z[41];
z[29] = z[3] + z[8];
z[42] = abb[3] + -abb[15];
z[43] = -abb[0] + z[42];
z[43] = 2 * z[43];
z[44] = -z[9] + z[29] + -z[43];
z[44] = abb[73] * z[44];
z[29] = -z[4] + -z[29] + -z[43];
z[29] = abb[74] * z[29];
z[43] = 2 * abb[24];
z[45] = 2 * abb[30];
z[46] = z[43] + z[45];
z[47] = z[10] + -z[46];
z[48] = abb[68] + abb[69];
z[47] = z[47] * z[48];
z[49] = -z[10] + z[14];
z[50] = abb[67] + abb[70];
z[51] = -z[49] * z[50];
z[52] = -abb[66] + 2 * abb[76];
z[53] = 2 * abb[72];
z[54] = abb[65] + z[52] + -z[53];
z[55] = z[10] * z[54];
z[56] = 2 * z[55];
z[29] = z[20] + z[29] + z[44] + z[47] + z[51] + -z[56];
z[29] = abb[34] * z[29];
z[44] = 2 * abb[2];
z[47] = z[37] + -z[44];
z[51] = abb[0] + -abb[15];
z[57] = 2 * z[51];
z[58] = 3 * abb[6];
z[59] = 2 * abb[4];
z[60] = abb[14] + z[47] + z[57] + -z[58] + z[59];
z[60] = abb[75] * z[60];
z[61] = abb[31] * z[54];
z[62] = 2 * z[61];
z[63] = abb[73] + abb[74];
z[64] = -z[39] + z[63];
z[65] = abb[1] * z[64];
z[66] = z[62] + z[65];
z[67] = z[12] + z[66];
z[68] = abb[25] * z[11];
z[69] = -z[16] + z[68];
z[70] = -abb[15] + z[0];
z[71] = -abb[2] + z[70];
z[72] = -z[59] + -z[71];
z[72] = abb[73] * z[72];
z[73] = -abb[5] + abb[6];
z[74] = abb[2] + abb[15];
z[75] = -abb[11] + z[73] + z[74];
z[75] = abb[74] * z[75];
z[60] = z[60] + z[67] + z[69] + z[72] + z[75];
z[60] = abb[35] * z[60];
z[72] = 3 * abb[7];
z[51] = abb[5] + z[51];
z[51] = z[34] + -z[44] + 2 * z[51] + -z[72];
z[51] = abb[75] * z[51];
z[75] = abb[24] * z[11];
z[76] = abb[30] * z[11];
z[77] = z[75] + -z[76];
z[78] = -abb[7] + abb[12];
z[79] = -abb[4] + z[74] + -z[78];
z[79] = abb[73] * z[79];
z[80] = 2 * abb[5];
z[81] = -z[71] + -z[80];
z[81] = abb[74] * z[81];
z[51] = z[51] + z[67] + z[77] + z[79] + z[81];
z[51] = abb[40] * z[51];
z[67] = -abb[71] + 3 * abb[72] + -2 * z[52];
z[67] = z[10] * z[67];
z[66] = z[66] + z[67];
z[67] = 2 * abb[6];
z[79] = z[59] + -z[67];
z[81] = 2 * abb[7];
z[82] = z[71] + -z[79] + -z[81];
z[82] = abb[74] * z[82];
z[57] = -z[44] + z[57];
z[83] = 3 * abb[4];
z[78] = z[78] + -z[83];
z[84] = -z[57] + -z[67] + -z[78];
z[84] = abb[75] * z[84];
z[78] = -z[74] + z[78];
z[78] = abb[73] * z[78];
z[77] = -z[66] + -z[77] + z[78] + z[82] + z[84];
z[77] = abb[39] * z[77];
z[78] = 3 * abb[5];
z[82] = -abb[11] + z[78];
z[57] = -z[33] + -z[57] + -z[81] + z[82];
z[57] = abb[75] * z[57];
z[84] = z[80] + -z[81];
z[71] = -z[67] + z[71] + -z[84];
z[71] = abb[73] * z[71];
z[74] = -abb[6] + -z[74] + -z[82];
z[74] = abb[74] * z[74];
z[57] = z[57] + -z[66] + -z[69] + z[71] + z[74];
z[57] = abb[38] * z[57];
z[66] = abb[31] * abb[36];
z[69] = -abb[31] + z[10];
z[71] = abb[24] + z[69];
z[74] = abb[41] * z[71];
z[66] = z[40] + z[66] + z[74];
z[74] = 3 * abb[35];
z[82] = abb[31] * z[74];
z[85] = 3 * z[69];
z[86] = abb[38] * z[85];
z[82] = z[82] + z[86];
z[86] = -abb[24] + abb[30] + abb[31];
z[86] = abb[39] * z[86];
z[71] = -abb[30] + z[71];
z[71] = abb[40] * z[71];
z[87] = abb[37] * z[10];
z[71] = -2 * z[66] + z[71] + z[82] + z[86] + -z[87];
z[71] = -z[48] * z[71];
z[86] = abb[31] * abb[41];
z[88] = abb[25] + z[69];
z[89] = abb[36] * z[88];
z[86] = z[86] + z[89];
z[89] = 3 * abb[40];
z[90] = abb[31] * z[89];
z[85] = abb[39] * z[85];
z[85] = z[85] + z[90];
z[90] = abb[25] + -abb[31];
z[90] = abb[38] * z[90];
z[91] = abb[35] * z[88];
z[87] = -z[85] + 2 * z[86] + z[87] + z[90] + -z[91];
z[87] = z[50] * z[87];
z[90] = -z[1] + z[6];
z[91] = -abb[0] + abb[3];
z[92] = abb[15] + z[91];
z[93] = -z[90] + 2 * z[92];
z[93] = z[63] * z[93];
z[94] = z[39] * z[90];
z[93] = -z[56] + z[93] + z[94];
z[93] = abb[37] * z[93];
z[95] = -abb[38] + 2 * abb[41];
z[96] = abb[35] + z[95];
z[96] = z[50] * z[96];
z[97] = abb[38] * z[11];
z[98] = abb[41] * z[18];
z[97] = z[97] + -z[98];
z[98] = abb[35] * z[11];
z[96] = z[96] + z[97] + -z[98];
z[98] = -abb[67] + z[11];
z[99] = -abb[70] + z[98];
z[100] = abb[34] * z[99];
z[101] = z[96] + 2 * z[100];
z[101] = abb[29] * z[101];
z[102] = abb[58] * z[21];
z[103] = abb[77] + abb[79];
z[104] = -z[102] * z[103];
z[105] = -2 * abb[36] + abb[39];
z[105] = z[36] * z[105];
z[106] = abb[40] * z[36];
z[106] = z[105] + -z[106];
z[107] = -abb[13] * z[106];
z[108] = abb[78] + abb[80];
z[109] = z[103] + z[108];
z[110] = -abb[89] * z[109];
z[111] = abb[47] + abb[49];
z[112] = abb[46] + z[111];
z[113] = abb[48] + z[112];
z[114] = abb[58] * z[113];
z[110] = z[110] + z[114];
z[110] = m1_set::bc<T>[0] * z[110];
z[29] = -z[2] + z[29] + z[51] + z[57] + z[60] + z[71] + z[77] + z[87] + z[93] + z[101] + z[104] + z[107] + (T(1) / T(3)) * z[110];
z[29] = m1_set::bc<T>[0] * z[29];
z[51] = abb[19] + abb[20] + -abb[23];
z[57] = abb[16] + z[51];
z[57] = abb[73] * z[57];
z[60] = abb[19] * abb[74];
z[57] = z[57] + 2 * z[60];
z[60] = abb[56] * z[57];
z[29] = -z[29] + z[60];
z[60] = 3 * abb[39];
z[71] = abb[38] + z[60];
z[71] = z[21] * z[71];
z[77] = 2 * abb[45];
z[87] = prod_pow(abb[36], 2);
z[93] = z[77] + z[87];
z[93] = 2 * z[93];
z[101] = 3 * abb[36] + -abb[41];
z[104] = -abb[35] + z[101];
z[104] = abb[35] * z[104];
z[107] = -abb[40] + z[13];
z[110] = abb[40] * z[107];
z[114] = 4 * abb[44];
z[71] = z[71] + z[93] + -z[104] + -z[110] + z[114];
z[104] = abb[46] * z[71];
z[110] = prod_pow(abb[41], 2);
z[115] = z[77] + z[110];
z[116] = 3 * abb[38];
z[117] = abb[39] + z[116];
z[117] = z[21] * z[117];
z[118] = 4 * abb[43];
z[119] = z[114] + z[118];
z[120] = -abb[35] + z[13];
z[120] = abb[35] * z[120];
z[121] = -abb[36] + 3 * abb[41];
z[122] = -abb[40] + z[121];
z[122] = abb[40] * z[122];
z[115] = -2 * z[115] + -z[117] + -z[119] + z[120] + z[122];
z[117] = abb[48] * z[115];
z[120] = abb[35] + z[89];
z[121] = -abb[38] + -z[120] + z[121];
z[121] = abb[38] * z[121];
z[27] = -z[27] + z[120];
z[27] = abb[39] * z[27];
z[120] = z[13] * z[120];
z[122] = 2 * z[110];
z[27] = -z[27] + z[120] + z[121] + -z[122];
z[120] = -z[27] + z[114];
z[120] = abb[49] * z[120];
z[121] = abb[40] + z[74];
z[123] = abb[47] * z[121];
z[23] = z[23] + -3 * z[24] + z[30] + z[123];
z[23] = abb[39] * z[23];
z[24] = z[1] + z[6];
z[30] = 2 * abb[3];
z[124] = abb[0] + abb[9] + z[30];
z[125] = 2 * abb[10];
z[124] = z[24] + 2 * z[124] + z[125];
z[126] = abb[57] * z[124];
z[26] = z[26] + z[123];
z[26] = abb[38] * z[26];
z[123] = 4 * abb[45];
z[127] = 2 * z[87];
z[128] = z[123] + z[127];
z[128] = abb[47] * z[128];
z[111] = z[111] * z[118];
z[129] = 4 * abb[42];
z[112] = z[112] * z[129];
z[25] = z[25] * z[121];
z[23] = 8 * abb[59] + z[23] + -z[25] + z[26] + z[104] + z[111] + z[112] + -z[117] + z[120] + -z[126] + z[128];
z[25] = -abb[58] * z[23];
z[26] = z[71] + z[129];
z[26] = abb[77] * z[26];
z[71] = abb[79] * z[115];
z[101] = abb[39] + -z[101] + z[121];
z[101] = abb[39] * z[101];
z[104] = z[13] * z[121];
z[93] = z[93] + z[101] + -z[104] + z[118];
z[93] = abb[78] * z[93];
z[27] = z[27] + -z[119];
z[27] = abb[80] * z[27];
z[101] = abb[88] * z[124];
z[104] = -z[74] + z[107];
z[104] = abb[78] * z[104];
z[32] = -z[32] + z[104];
z[32] = abb[38] * z[32];
z[26] = 8 * abb[90] + z[26] + -z[27] + -z[32] + -z[71] + z[93] + -z[101];
z[27] = z[108] * z[129];
z[27] = z[26] + z[27];
z[27] = abb[89] * z[27];
z[32] = z[41] + -z[68] + -z[76];
z[41] = abb[8] + -abb[15];
z[71] = z[0] + z[41];
z[93] = -z[37] + z[71];
z[93] = abb[74] * z[93];
z[101] = -abb[7] + z[41];
z[101] = abb[73] * z[101];
z[104] = abb[27] + abb[31];
z[107] = abb[67] * z[104];
z[38] = abb[75] * z[38];
z[38] = -z[32] + z[38] + z[93] + z[101] + z[107];
z[93] = abb[25] + abb[30] + z[104];
z[101] = -abb[68] * z[93];
z[101] = z[38] + z[101];
z[107] = 4 * abb[51];
z[101] = z[101] * z[107];
z[111] = abb[0] + abb[3];
z[112] = 2 * abb[8];
z[115] = -3 * abb[15] + z[111] + z[112];
z[117] = z[90] + -2 * z[115];
z[117] = -z[63] * z[117];
z[117] = z[56] + z[94] + z[117];
z[117] = abb[54] * z[117];
z[119] = z[16] + z[75];
z[71] = -z[34] + z[71];
z[71] = abb[73] * z[71];
z[35] = abb[75] * z[35];
z[120] = -abb[6] + z[41];
z[120] = abb[74] * z[120];
z[35] = -z[35] + z[71] + z[119] + z[120];
z[71] = 4 * abb[50];
z[120] = z[35] * z[71];
z[121] = abb[52] + abb[53] + abb[54];
z[124] = 4 * abb[27];
z[121] = z[121] * z[124];
z[126] = abb[25] + abb[31];
z[126] = abb[53] * z[126];
z[128] = abb[52] * z[88];
z[126] = z[126] + z[128];
z[121] = z[121] + 4 * z[126];
z[126] = 4 * abb[25];
z[128] = 3 * z[10] + z[126];
z[130] = abb[54] * z[128];
z[88] = abb[27] + z[88];
z[131] = abb[24] + z[88];
z[132] = z[71] * z[131];
z[133] = abb[32] * abb[56];
z[130] = z[121] + z[130] + z[132] + 2 * z[133];
z[132] = -abb[68] * z[130];
z[71] = z[71] * z[88];
z[134] = z[10] + z[126];
z[135] = abb[54] * z[134];
z[136] = abb[95] * (T(3) / T(2));
z[71] = z[71] + z[121] + z[135] + z[136];
z[121] = abb[67] * z[71];
z[135] = -abb[18] + -abb[21] + abb[22];
z[137] = abb[17] + z[135];
z[137] = abb[75] * z[137];
z[135] = -abb[17] + z[135];
z[135] = abb[74] * z[135];
z[138] = abb[18] * abb[73];
z[139] = abb[33] * z[98];
z[135] = z[135] + -z[137] + -2 * z[138] + z[139];
z[137] = 2 * abb[55];
z[138] = z[135] * z[137];
z[139] = -z[93] * z[107];
z[130] = -z[130] + z[136] + z[139];
z[130] = abb[69] * z[130];
z[107] = z[104] * z[107];
z[136] = -abb[33] * z[137];
z[71] = z[71] + z[107] + z[136];
z[71] = abb[70] * z[71];
z[22] = abb[58] * z[22];
z[21] = z[21] * z[103];
z[21] = z[21] + -z[28];
z[21] = abb[89] * z[21];
z[28] = abb[48] * z[102];
z[21] = -z[21] + z[22] + z[28];
z[21] = 2 * z[21];
z[22] = abb[34] * z[21];
z[28] = -abb[4] + abb[6];
z[107] = abb[8] + z[42];
z[136] = z[28] + -z[107];
z[137] = 4 * abb[52];
z[136] = -z[63] * z[136] * z[137];
z[137] = z[28] * z[137];
z[139] = -abb[5] + abb[7];
z[140] = abb[53] * z[139];
z[51] = -abb[16] + z[51];
z[141] = abb[56] * z[51];
z[137] = z[137] + 4 * z[140] + z[141];
z[137] = z[39] * z[137];
z[140] = -abb[3] + z[139];
z[141] = 4 * abb[53];
z[140] = z[140] * z[141];
z[141] = abb[53] * z[41];
z[140] = z[140] + -4 * z[141];
z[141] = abb[62] + abb[63];
z[142] = abb[61] * (T(3) / T(2)) + -z[140] + (T(-1) / T(2)) * z[141];
z[142] = abb[73] * z[142];
z[140] = abb[61] * (T(-1) / T(2)) + -z[140] + (T(3) / T(2)) * z[141];
z[140] = abb[74] * z[140];
z[133] = z[18] * z[133];
z[141] = 2 * abb[71];
z[143] = -3 * abb[65] + z[52] + z[141];
z[144] = abb[68] * (T(3) / T(2)) + -z[143];
z[144] = abb[95] * z[144];
z[145] = 3 * abb[73] + -abb[74];
z[145] = (T(1) / T(2)) * z[145];
z[146] = abb[60] * z[145];
z[99] = abb[29] * z[99];
z[147] = abb[50] * z[99];
z[22] = z[22] + z[25] + z[27] + -2 * z[29] + z[71] + z[101] + z[117] + z[120] + z[121] + z[130] + z[132] + z[133] + z[136] + z[137] + z[138] + z[140] + z[142] + z[144] + z[146] + 4 * z[147];
z[25] = abb[5] * (T(3) / T(2));
z[27] = abb[7] * (T(3) / T(2));
z[29] = abb[4] * (T(1) / T(2));
z[71] = z[25] + z[27] + -z[29];
z[101] = abb[6] * (T(1) / T(2));
z[117] = 3 * abb[0];
z[120] = z[101] + z[117];
z[121] = -abb[3] + z[112];
z[130] = -z[9] + -z[71] + z[120] + z[121];
z[130] = abb[73] * z[130];
z[132] = abb[4] * (T(3) / T(2));
z[133] = abb[6] * (T(3) / T(2));
z[136] = z[132] + z[133];
z[137] = abb[7] * (T(1) / T(2));
z[138] = z[117] + z[137];
z[140] = abb[5] * (T(1) / T(2));
z[142] = z[138] + z[140];
z[121] = -z[4] + z[121] + -z[136] + z[142];
z[121] = abb[74] * z[121];
z[144] = 2 * abb[27];
z[146] = z[14] + z[144];
z[147] = (T(1) / T(2)) * z[10];
z[148] = z[146] + z[147];
z[149] = z[46] + z[148];
z[149] = -z[48] * z[149];
z[150] = (T(3) / T(2)) * z[10];
z[144] = z[144] + z[150];
z[144] = z[50] * z[144];
z[20] = z[20] + -z[55] + z[121] + z[130] + z[144] + z[149];
z[20] = abb[34] * z[20];
z[49] = -abb[31] + z[49];
z[49] = abb[38] * z[49];
z[121] = z[124] + z[134];
z[121] = abb[37] * z[121];
z[130] = 2 * z[10];
z[134] = -abb[31] + z[130];
z[144] = z[14] + z[134];
z[144] = abb[35] * z[144];
z[49] = -z[49] + z[85] + -4 * z[86] + z[121] + z[144];
z[49] = -z[49] * z[50];
z[43] = z[43] + -z[45];
z[45] = abb[31] + z[10] + -z[43];
z[45] = abb[39] * z[45];
z[43] = z[43] + z[134];
z[43] = abb[40] * z[43];
z[86] = z[124] + z[128];
z[86] = abb[37] * z[86];
z[43] = z[43] + z[45] + -4 * z[66] + z[82] + -z[86];
z[43] = -z[43] * z[48];
z[45] = abb[15] + z[44];
z[66] = -z[9] + z[45];
z[58] = -z[30] + z[58];
z[86] = -abb[7] + -z[0] + -z[58] + -z[59] + -z[66];
z[86] = abb[73] * z[86];
z[121] = abb[0] + z[42];
z[128] = -z[44] + z[121];
z[134] = -abb[7] + z[34] + z[128];
z[134] = z[39] * z[134];
z[144] = 2 * z[65];
z[19] = z[19] + z[144];
z[134] = z[19] + -z[31] + z[134];
z[149] = -abb[65] + abb[71] + -abb[72] + z[52];
z[149] = z[10] * z[149];
z[149] = -z[61] + z[149];
z[149] = 2 * z[149];
z[151] = -z[44] + z[70];
z[72] = -z[30] + z[72];
z[152] = -abb[6] + z[72];
z[153] = z[151] + -z[152];
z[153] = abb[74] * z[153];
z[86] = z[86] + -z[134] + z[149] + z[153];
z[86] = abb[39] * z[86];
z[37] = z[33] + z[37] + z[128];
z[37] = z[37] * z[39];
z[17] = -z[17] + z[144];
z[37] = z[15] + z[17] + z[37];
z[128] = -z[4] + z[45];
z[0] = -abb[6] + -z[0] + -z[72] + -z[80] + -z[128];
z[0] = abb[74] * z[0];
z[58] = -abb[7] + z[58];
z[72] = -z[58] + z[151];
z[72] = abb[73] * z[72];
z[0] = z[0] + -z[37] + z[72] + z[149];
z[0] = abb[38] * z[0];
z[72] = z[12] + z[61];
z[72] = 2 * z[72];
z[128] = z[8] + z[67] + z[128];
z[128] = abb[74] * z[128];
z[45] = 4 * abb[0] + -z[45];
z[83] = abb[5] + -z[45] + -z[83];
z[83] = abb[73] * z[83];
z[37] = z[37] + z[72] + z[83] + z[128];
z[37] = abb[35] * z[37];
z[66] = -z[8] + z[66] + z[81];
z[66] = abb[73] * z[66];
z[45] = abb[4] + -z[45] + -z[78];
z[45] = abb[74] * z[45];
z[45] = z[45] + z[66] + z[72] + z[134];
z[45] = abb[40] * z[45];
z[66] = 2 * z[41] + z[111];
z[24] = -z[24] + 2 * z[66];
z[24] = -z[24] * z[63];
z[24] = z[24] + -z[56];
z[24] = abb[37] * z[24];
z[56] = -2 * z[103];
z[56] = z[56] * z[102];
z[66] = 2 * abb[13];
z[72] = -z[66] * z[106];
z[0] = z[0] + -2 * z[2] + z[20] + z[24] + z[37] + z[43] + z[45] + z[49] + z[56] + z[72] + z[86];
z[0] = abb[34] * z[0];
z[2] = (T(7) / T(2)) * z[10];
z[20] = z[2] + z[46];
z[20] = abb[40] * z[20];
z[24] = (T(5) / T(2)) * z[10];
z[37] = -abb[31] + z[24];
z[43] = 4 * abb[24] + z[37];
z[43] = abb[41] * z[43];
z[45] = abb[35] * z[150];
z[46] = abb[36] * z[37];
z[20] = -z[20] + z[43] + -z[45] + z[46];
z[20] = abb[39] * z[20];
z[43] = abb[37] * z[148];
z[45] = abb[31] * abb[40];
z[46] = abb[39] * z[69];
z[43] = z[43] + -z[45] + -z[46] + z[82];
z[43] = abb[37] * z[43];
z[7] = z[7] * z[147];
z[45] = -abb[31] + z[147];
z[45] = z[13] * z[45];
z[7] = z[7] + z[45];
z[46] = z[7] * z[116];
z[49] = abb[31] + z[150];
z[13] = z[13] * z[49];
z[40] = z[13] + 4 * z[40];
z[40] = abb[40] * z[40];
z[49] = abb[24] + z[10];
z[49] = z[49] * z[114];
z[56] = z[45] * z[74];
z[72] = z[69] * z[127];
z[74] = 4 * abb[83];
z[78] = 3 * abb[85] + z[74];
z[78] = z[10] * z[78];
z[82] = z[124] + z[126];
z[83] = abb[83] + abb[84] + abb[85];
z[82] = z[82] * z[83];
z[83] = -abb[83] + abb[84];
z[83] = abb[31] * z[83];
z[82] = z[82] + 4 * z[83];
z[83] = abb[31] * z[122];
z[86] = abb[64] * (T(3) / T(2));
z[20] = z[20] + z[40] + z[43] + -z[46] + -z[49] + z[56] + -z[72] + -z[78] + -z[82] + -z[83] + -z[86];
z[40] = -abb[68] * z[20];
z[2] = z[2] + z[14];
z[2] = abb[35] * z[2];
z[14] = z[37] + z[126];
z[14] = abb[36] * z[14];
z[43] = abb[40] * z[150];
z[37] = abb[41] * z[37];
z[2] = z[2] + -z[14] + -z[37] + z[43];
z[2] = abb[38] * z[2];
z[14] = z[146] + z[150];
z[14] = abb[37] * z[14];
z[37] = abb[31] * abb[35];
z[43] = abb[38] * z[69];
z[14] = z[14] + z[37] + z[43] + -z[85];
z[14] = abb[37] * z[14];
z[7] = z[7] * z[60];
z[37] = abb[85] + z[74];
z[43] = z[37] + -z[118];
z[43] = z[10] * z[43];
z[46] = z[69] * z[122];
z[45] = z[45] * z[89];
z[49] = abb[25] + z[10];
z[49] = z[49] * z[123];
z[13] = abb[35] * z[13];
z[56] = abb[31] * z[127];
z[2] = z[2] + z[7] + -z[13] + z[14] + -z[43] + -z[45] + z[46] + z[49] + z[56] + -z[82] + z[86];
z[7] = abb[67] * z[2];
z[13] = abb[30] + z[10];
z[14] = z[13] * z[129];
z[43] = 2 * abb[87];
z[45] = abb[32] * z[43];
z[14] = z[14] + -z[20] + z[45];
z[14] = abb[69] * z[14];
z[20] = abb[89] * z[23];
z[23] = 4 * abb[81];
z[45] = -z[23] * z[88];
z[46] = 4 * abb[82];
z[49] = -z[46] * z[104];
z[56] = 2 * abb[86];
z[60] = abb[33] * z[56];
z[2] = z[2] + z[45] + z[49] + z[60];
z[2] = abb[70] * z[2];
z[45] = abb[4] * (T(5) / T(2));
z[49] = -z[25] + z[45];
z[60] = 2 * abb[9];
z[69] = z[60] + -z[125];
z[72] = abb[7] * (T(7) / T(2));
z[78] = z[44] + -z[49] + z[69] + z[72] + -z[91] + -z[101];
z[78] = abb[74] * z[78];
z[82] = -z[44] + z[69];
z[83] = z[91] + z[137];
z[85] = z[9] + -z[82] + -z[83] + z[136] + -z[140];
z[85] = abb[73] * z[85];
z[53] = -7 * abb[65] + 4 * abb[71] + z[52] + z[53];
z[53] = z[10] * z[53];
z[34] = -abb[7] + -z[34] + -z[44];
z[34] = z[34] * z[39];
z[19] = -z[19] + -z[31] + z[34] + -z[53] + z[78] + z[85];
z[19] = abb[40] * z[19];
z[31] = -5 * abb[65] + 4 * abb[72] + -z[52] + z[141];
z[31] = z[10] * z[31];
z[34] = z[31] + z[62];
z[52] = z[27] + z[133];
z[78] = abb[0] + z[52];
z[85] = z[29] + z[140];
z[86] = -z[42] + z[78] + z[85];
z[89] = -4 * abb[12] + z[69] + z[86];
z[89] = abb[73] * z[89];
z[49] = z[49] + -z[138];
z[101] = z[69] + z[101];
z[102] = z[42] + z[101];
z[103] = z[49] + -z[102];
z[103] = abb[74] * z[103];
z[89] = z[34] + 4 * z[75] + z[89] + z[103];
z[89] = abb[41] * z[89];
z[60] = z[60] + z[125];
z[103] = abb[0] + 7 * abb[3] + abb[15] + -z[60] + z[85];
z[27] = abb[6] * (T(-13) / T(2)) + z[27] + z[103];
z[27] = abb[73] * z[27];
z[49] = abb[15] + z[49];
z[104] = 3 * abb[3];
z[106] = z[60] + z[104];
z[116] = abb[6] * (T(-9) / T(2)) + z[49] + z[106];
z[116] = abb[74] * z[116];
z[27] = z[27] + z[34] + z[116];
z[27] = abb[36] * z[27];
z[25] = z[25] + z[132];
z[52] = -z[25] + -z[52] + z[60] + z[91];
z[116] = -abb[74] * z[52];
z[124] = 5 * abb[3] + -z[60];
z[125] = abb[6] * (T(7) / T(2));
z[128] = z[124] + -z[125];
z[134] = abb[4] * (T(7) / T(2)) + -z[128] + -z[142];
z[134] = abb[73] * z[134];
z[136] = z[10] * z[143];
z[116] = z[116] + z[134] + -z[136];
z[116] = abb[35] * z[116];
z[9] = z[9] + -z[59] + z[121];
z[9] = abb[41] * z[9];
z[59] = z[79] + z[92];
z[134] = -abb[36] * z[59];
z[9] = z[9] + z[134];
z[9] = z[9] * z[39];
z[134] = -abb[2] + z[111];
z[134] = z[39] * z[134];
z[134] = z[65] + z[134];
z[137] = abb[2] + z[6];
z[111] = 2 * z[111] + -z[137];
z[111] = z[63] * z[111];
z[111] = z[111] + -z[134];
z[141] = -abb[39] * z[111];
z[142] = 4 * abb[1];
z[64] = abb[41] * z[64] * z[142];
z[9] = z[9] + z[19] + z[27] + z[64] + z[89] + z[116] + z[141];
z[9] = abb[39] * z[9];
z[19] = z[4] + -z[44] + z[71] + -z[91] + -z[101];
z[19] = abb[74] * z[19];
z[27] = abb[5] * (T(5) / T(2));
z[71] = z[27] + -z[132];
z[82] = -z[71] + z[82] + -z[83] + z[125];
z[82] = abb[73] * z[82];
z[33] = z[33] + -z[47];
z[33] = z[33] * z[39];
z[15] = -z[15] + z[17] + z[19] + z[33] + -z[53] + z[82];
z[15] = abb[35] * z[15];
z[17] = abb[15] + z[71];
z[19] = abb[7] * (T(-9) / T(2)) + z[17] + z[106] + -z[120];
z[19] = abb[73] * z[19];
z[33] = abb[7] * (T(-13) / T(2)) + z[103] + z[133];
z[33] = abb[74] * z[33];
z[19] = z[19] + z[33] + z[34];
z[19] = abb[41] * z[19];
z[33] = 4 * abb[2];
z[47] = z[33] + z[71] + -z[102] + -z[138];
z[47] = abb[73] * z[47];
z[53] = z[33] + z[86];
z[71] = -4 * abb[11] + z[53] + z[69];
z[71] = abb[74] * z[71];
z[82] = z[11] * z[126];
z[34] = z[34] + z[47] + z[71] + z[82];
z[34] = abb[36] * z[34];
z[47] = -abb[73] * z[52];
z[52] = z[72] + -z[120];
z[71] = z[52] + -z[124];
z[72] = abb[5] * (T(7) / T(2)) + -z[29] + z[71];
z[72] = abb[74] * z[72];
z[47] = z[47] + z[72] + -z[136];
z[47] = abb[40] * z[47];
z[72] = z[33] + -z[121];
z[4] = z[4] + -z[72] + -z[80];
z[4] = abb[36] * z[4];
z[80] = z[84] + z[92];
z[82] = -abb[41] * z[80];
z[4] = z[4] + z[82];
z[4] = z[4] * z[39];
z[82] = -abb[38] * z[111];
z[4] = z[4] + z[15] + z[19] + z[34] + z[47] + z[82];
z[4] = abb[38] * z[4];
z[5] = z[5] + -2 * z[90];
z[5] = abb[75] * z[5];
z[5] = -z[5] + -z[32] + z[99] + z[119];
z[15] = abb[24] + abb[30];
z[19] = abb[27] * (T(16) / T(3));
z[15] = abb[25] * (T(16) / T(3)) + (T(4) / T(3)) * z[15] + z[19] + z[24];
z[15] = z[15] * z[48];
z[10] = (T(17) / T(6)) * z[10] + z[19] + z[126];
z[10] = -z[10] * z[50];
z[19] = abb[3] + (T(16) / T(3)) * z[41] + z[117];
z[24] = abb[6] * (T(1) / T(6)) + abb[12] * (T(4) / T(3)) + abb[7] * (T(13) / T(6)) + -z[19] + -z[45] + -z[140];
z[24] = abb[73] * z[24];
z[19] = abb[7] * (T(1) / T(6)) + abb[11] * (T(4) / T(3)) + abb[6] * (T(13) / T(6)) + -z[19] + -z[27] + -z[29];
z[19] = abb[74] * z[19];
z[27] = (T(-2) / T(3)) * z[109];
z[27] = abb[58] * z[27];
z[29] = abb[89] * z[113];
z[5] = (T(-4) / T(3)) * z[5] + z[10] + z[15] + z[19] + z[24] + z[27] + (T(-2) / T(3)) * z[29] + (T(1) / T(3)) * z[31];
z[5] = m1_set::bc<T>[0] * z[5];
z[5] = z[5] + -z[21];
z[5] = m1_set::bc<T>[0] * z[5];
z[10] = abb[58] * z[26];
z[15] = z[55] + -z[61];
z[15] = 2 * z[15];
z[19] = -4 * abb[4] + z[58] + z[70];
z[19] = z[19] * z[63];
z[21] = z[39] * z[59];
z[19] = z[15] + z[19] + z[21];
z[19] = abb[39] * z[19];
z[21] = -4 * abb[5] + z[70] + z[152];
z[21] = z[21] * z[63];
z[24] = z[39] * z[80];
z[15] = z[15] + z[21] + z[24];
z[15] = abb[38] * z[15];
z[21] = -z[78] + z[85] + z[104] + z[112];
z[21] = z[21] * z[63];
z[21] = z[21] + -z[55] + z[94];
z[21] = abb[37] * z[21];
z[24] = -abb[15] + z[1];
z[24] = z[24] * z[63];
z[24] = z[24] + -z[62];
z[26] = z[79] + -z[92];
z[27] = z[26] * z[39];
z[27] = -z[24] + z[27];
z[27] = abb[35] * z[27];
z[29] = z[84] + -z[92];
z[31] = z[29] * z[39];
z[24] = -z[24] + z[31];
z[24] = abb[40] * z[24];
z[15] = z[15] + z[19] + z[21] + z[24] + z[27];
z[15] = abb[37] * z[15];
z[19] = abb[2] + abb[10] + -z[30] + z[67];
z[19] = abb[73] * z[19];
z[6] = -abb[3] + z[6];
z[21] = abb[4] + -z[6] + -z[44];
z[21] = abb[75] * z[21];
z[24] = -abb[3] + -abb[4] + -abb[10] + z[137];
z[24] = abb[74] * z[24];
z[13] = abb[68] * z[13];
z[27] = abb[58] * z[108];
z[13] = -z[12] + z[13] + z[19] + z[21] + z[24] + z[27] + -z[76];
z[13] = z[13] * z[129];
z[19] = -z[62] + z[136];
z[21] = z[25] + z[42] + -z[69];
z[24] = z[21] + -z[125] + z[138];
z[24] = abb[73] * z[24];
z[25] = -z[69] + z[86];
z[25] = -abb[74] * z[25];
z[24] = 4 * z[16] + z[19] + z[24] + z[25];
z[24] = abb[41] * z[24];
z[25] = -z[49] + z[128];
z[25] = abb[73] * z[25];
z[27] = -z[60] + z[86];
z[31] = -abb[74] * z[27];
z[25] = z[19] + z[25] + z[31];
z[25] = abb[36] * z[25];
z[31] = -2 * abb[14] + z[67] + -z[121];
z[31] = abb[41] * z[31];
z[26] = -abb[36] * z[26];
z[26] = z[26] + z[31];
z[26] = z[26] * z[39];
z[31] = -abb[2] + z[1];
z[31] = z[31] * z[63];
z[31] = z[31] + -z[134];
z[32] = abb[35] * z[31];
z[24] = z[24] + z[25] + z[26] + z[32] + -z[64];
z[24] = abb[35] * z[24];
z[25] = -z[53] + z[69];
z[25] = abb[73] * z[25];
z[21] = z[21] + -z[33] + -z[52];
z[21] = abb[74] * z[21];
z[21] = z[19] + z[21] + z[25] + 4 * z[76];
z[21] = abb[36] * z[21];
z[25] = -abb[73] * z[27];
z[17] = -z[17] + -z[71];
z[17] = abb[74] * z[17];
z[17] = z[17] + z[19] + z[25];
z[17] = abb[41] * z[17];
z[19] = -abb[41] * z[29];
z[25] = z[72] + z[81];
z[25] = abb[36] * z[25];
z[19] = z[19] + z[25];
z[19] = z[19] * z[39];
z[25] = abb[40] * z[31];
z[17] = z[17] + z[19] + z[21] + z[25];
z[17] = abb[40] * z[17];
z[19] = -abb[3] + abb[7];
z[21] = abb[9] + -z[8] + z[19];
z[21] = abb[74] * z[21];
z[25] = -abb[9] + abb[12];
z[25] = abb[73] * z[25];
z[1] = -abb[3] + z[1];
z[26] = -abb[7] + -abb[12] + z[1];
z[26] = abb[75] * z[26];
z[21] = -z[12] + z[21] + z[25] + z[26] + -z[65] + -z[75];
z[21] = z[21] * z[114];
z[6] = abb[5] + -z[6];
z[25] = -abb[10] + -z[6];
z[25] = abb[73] * z[25];
z[19] = abb[10] + 2 * z[19];
z[19] = abb[74] * z[19];
z[6] = abb[14] + z[6];
z[6] = abb[75] * z[6];
z[6] = z[6] + -z[12] + -z[16] + z[19] + z[25] + z[65];
z[6] = z[6] * z[118];
z[16] = z[96] + z[100];
z[16] = abb[34] * z[16];
z[19] = -abb[35] * z[97];
z[25] = 2 * abb[81];
z[26] = -z[25] * z[98];
z[27] = -abb[35] * z[95];
z[27] = 2 * abb[43] + z[27];
z[25] = z[25] + z[27];
z[25] = abb[70] * z[25];
z[18] = -abb[43] * z[18];
z[27] = abb[67] * z[27];
z[16] = z[16] + z[18] + z[19] + z[25] + z[26] + z[27];
z[16] = abb[29] * z[16];
z[18] = -abb[85] * z[115];
z[19] = -z[107] + z[139];
z[25] = 4 * abb[84];
z[19] = z[19] * z[25];
z[26] = z[74] * z[107];
z[27] = -z[28] * z[37];
z[28] = -abb[85] * z[139];
z[27] = z[27] + z[28];
z[18] = 2 * z[18] + z[19] + -z[26] + -z[27];
z[18] = z[18] * z[63];
z[19] = -z[87] + z[110];
z[3] = z[3] + -z[8];
z[3] = z[3] * z[19];
z[19] = -z[25] * z[139];
z[1] = -abb[6] + -abb[11] + z[1] + z[44];
z[1] = z[1] * z[77];
z[1] = z[1] + z[3] + z[19] + z[27];
z[1] = z[1] * z[39];
z[3] = z[48] * z[93];
z[3] = z[3] + -z[38];
z[3] = z[3] * z[46];
z[8] = -abb[2] + -abb[3] + abb[6] + abb[9] + z[8];
z[8] = abb[73] * z[8];
z[19] = -abb[2] + -abb[9] + abb[11];
z[19] = abb[74] * z[19];
z[8] = z[8] + -z[12] + z[19] + -z[68];
z[8] = z[8] * z[123];
z[19] = abb[9] + abb[10];
z[25] = -abb[15] + z[19] + -z[30];
z[26] = z[25] + -z[73] + z[81];
z[26] = abb[74] * z[26];
z[19] = z[19] + -z[70];
z[27] = abb[4] + abb[7] + -z[19];
z[27] = abb[73] * z[27];
z[26] = -z[12] + z[26] + z[27];
z[26] = z[26] * z[122];
z[25] = abb[4] + -abb[7] + z[25] + z[67];
z[25] = abb[73] * z[25];
z[19] = abb[5] + abb[6] + -z[19];
z[19] = abb[74] * z[19];
z[12] = -z[12] + z[19] + z[25];
z[12] = z[12] * z[127];
z[19] = z[48] * z[131];
z[25] = -abb[67] * z[88];
z[19] = z[19] + z[25] + -z[35];
z[19] = z[19] * z[23];
z[23] = -z[56] * z[135];
z[11] = abb[68] + -z[11];
z[11] = abb[32] * z[11];
z[25] = -abb[75] * z[51];
z[11] = z[11] + z[25] + z[57];
z[11] = z[11] * z[43];
z[25] = abb[93] + abb[94];
z[25] = (T(-1) / T(2)) * z[25];
z[27] = -abb[73] + 3 * abb[74];
z[25] = z[25] * z[27];
z[27] = -abb[85] * z[54] * z[130];
z[28] = -abb[91] + -abb[92];
z[28] = z[28] * z[145];
z[29] = -abb[64] * z[143];
z[30] = abb[40] * z[105];
z[31] = abb[42] * z[36];
z[30] = z[30] + 2 * z[31];
z[30] = z[30] * z[66];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + 2 * z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[23] + z[24] + z[25] + z[26] + z[27] + z[28] + z[29] + z[30] + z[40];
return {z[22], z[0]};
}


template <typename T> std::complex<T> f_4_979_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-13.340292182620083964030895946325322220714766981147194737923598445"),stof<T>("7.797857923950168976713207500327193035824669911177429562397356209")}, std::complex<T>{stof<T>("-48.389223528459310168130450869953407056308691317944699117038443347"),stof<T>("16.941955382268272302387790511713527287302313538204169765988248251")}, std::complex<T>{stof<T>("-39.626990691999503617105562139046385847410210233745323022259732122"),stof<T>("14.65593101768874647096914475886694372443290263144748471509052524")}, std::complex<T>{stof<T>("-39.626990691999503617105562139046385847410210233745323022259732122"),stof<T>("14.65593101768874647096914475886694372443290263144748471509052524")}, std::complex<T>{stof<T>("-39.626990691999503617105562139046385847410210233745323022259732122"),stof<T>("14.65593101768874647096914475886694372443290263144748471509052524")}, std::complex<T>{stof<T>("-39.626990691999503617105562139046385847410210233745323022259732122"),stof<T>("14.65593101768874647096914475886694372443290263144748471509052524")}, std::complex<T>{stof<T>("30.864757855539697066080673408139364638511729149545946927481020896"),stof<T>("-12.36990665310922063955049900602036016156349172469079966419280223")}, std::complex<T>{stof<T>("-65.913689201378923270180228331767449474105653486343451306595865798"),stof<T>("21.514004111427323965225082017406694413041135351717539867783694272")}, std::complex<T>{stof<T>("-15.685557527520906863936252674414916259975080651338963155225771137"),stof<T>("13.246574504988806398640121742384222593461597518646871128381379325")}, std::complex<T>{stof<T>("-15.685557527520906863936252674414916259975080651338963155225771137"),stof<T>("13.246574504988806398640121742384222593461597518646871128381379325")}, std::complex<T>{stof<T>("-25.508264756153529868290032786510842334196315778375716206267637055"),stof<T>("2.481030835992361594792520686235529751710377647595982018757668177")}, std::complex<T>{stof<T>("96.778447056918620336260901739906814112617382635889398234076886695"),stof<T>("-33.883910764536544604775581023427054574604627076408339531976496502")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, (log3(k.W[193]) - log3(kbase.W[193])), rlog(k.W[197].imag()/kbase.W[197].imag())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_979_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_979_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("191.05259063641252151764536631522631911747445734989615065604411008"),stof<T>("-142.57918967865601169228194877697246820571337909138435409975874916")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({203});

#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
        if constexpr (std::is_same_v<T,double>){
            if(always_switch_to_HP_for_sqrt_divergences and path.can_letters_vanish_Q({203})) {
                return {std::nan(""), std::numeric_limits<double>::max()};
            }
        }
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,96> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_14(k), f_2_15(k), f_2_16(k), dlog_W174(k,dv).imag(), dlog_W176(k,dv).imag(), dlog_W180(k,dv).imag(), dlog_W182(k,dv).imag(), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), f_2_25_im(k), f_2_35_im(k), (log3(kend.W[193]) - log3(k.W[193])).imag(), T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), rlog(kend.W[197].imag()/k.W[197].imag()), dlog_W174(k,dv).real(), dlog_W176(k,dv).real(), dlog_W180(k,dv).real(), dlog_W182(k,dv).real(), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k), f_2_25_re(k), f_2_35_re(k), (log3(kend.W[193]) - log3(k.W[193])).real(), T{0}, T{0}, T{0}, T{0}, T{0}, T{0}};
SpDLogSigma5_W_204(k, dv, dlr, abb[90], abb[59]);
{
auto c = dlog_W174(k,dv) * f_2_35(k);
abb[91] = c.real();
abb[60] = c.imag();
SpDLog_Sigma5<T,203,173>(k, dv, abb[91], abb[60], f_2_35_series_coefficients<T>);
}
{
auto c = dlog_W176(k,dv) * f_2_35(k);
abb[92] = c.real();
abb[61] = c.imag();
SpDLog_Sigma5<T,203,175>(k, dv, abb[92], abb[61], f_2_35_series_coefficients<T>);
}
{
auto c = dlog_W180(k,dv) * f_2_35(k);
abb[93] = c.real();
abb[62] = c.imag();
SpDLog_Sigma5<T,203,179>(k, dv, abb[93], abb[62], f_2_35_series_coefficients<T>);
}
{
auto c = dlog_W182(k,dv) * f_2_35(k);
abb[94] = c.real();
abb[63] = c.imag();
SpDLog_Sigma5<T,203,181>(k, dv, abb[94], abb[63], f_2_35_series_coefficients<T>);
}
{
auto c = dlog_W194(k,dv) * f_2_35(k);
abb[95] = c.real();
abb[64] = c.imag();
SpDLog_Sigma5<T,203,193>(k, dv, abb[95], abb[64], f_2_35_series_coefficients<T>);
}

                    
            return f_4_979_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_979_DLogXconstant_part(base_point<T>, kend);
	value += f_4_979_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_979_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_979_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_979_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_979_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_979_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_979_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
