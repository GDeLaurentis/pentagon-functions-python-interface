/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_489.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_489_abbreviated (const std::array<T,59>& abb) {
T z[86];
z[0] = -abb[44] + abb[46] + abb[47];
z[1] = -abb[45] + abb[43] * (T(-1) / T(2)) + (T(1) / T(2)) * z[0];
z[2] = 2 * abb[53] + -z[1];
z[3] = abb[19] * z[2];
z[4] = -abb[48] + abb[49];
z[5] = abb[15] * z[4];
z[3] = z[3] + z[5];
z[1] = -abb[53] + (T(1) / T(2)) * z[1];
z[6] = abb[16] * z[1];
z[7] = abb[18] * z[2];
z[8] = -abb[13] * z[4];
z[9] = abb[48] + abb[51];
z[10] = abb[10] * z[9];
z[11] = z[3] + -5 * z[6] + z[7] + -z[8] + z[10];
z[12] = abb[48] + abb[50];
z[13] = abb[49] + z[12];
z[14] = abb[12] * z[13];
z[15] = abb[49] + -abb[51];
z[16] = abb[2] * z[15];
z[17] = z[14] + z[16];
z[18] = abb[23] * abb[52];
z[19] = abb[51] + z[13];
z[20] = abb[8] * z[19];
z[21] = z[18] + -z[20];
z[22] = abb[22] + abb[24];
z[23] = abb[52] * (T(1) / T(4));
z[24] = z[22] * z[23];
z[25] = abb[50] + abb[51];
z[26] = abb[49] * (T(1) / T(2));
z[27] = abb[48] + (T(1) / T(6)) * z[25] + z[26];
z[27] = abb[6] * z[27];
z[28] = abb[48] * (T(5) / T(2)) + z[25];
z[28] = z[26] + (T(1) / T(3)) * z[28];
z[28] = abb[7] * z[28];
z[29] = abb[50] * (T(1) / T(2));
z[30] = abb[51] * (T(1) / T(2));
z[31] = z[29] + z[30];
z[32] = abb[48] * (T(1) / T(3)) + -z[31];
z[32] = abb[0] * z[32];
z[33] = (T(2) / T(3)) * z[12] + z[26];
z[33] = abb[5] * z[33];
z[34] = abb[21] * z[23];
z[35] = abb[17] * z[1];
z[36] = abb[1] * z[12];
z[37] = abb[25] * abb[52];
z[11] = (T(1) / T(3)) * z[11] + (T(-2) / T(3)) * z[17] + (T(5) / T(12)) * z[21] + z[24] + z[27] + z[28] + z[32] + z[33] + z[34] + -z[35] + z[36] + (T(-1) / T(6)) * z[37];
z[17] = prod_pow(m1_set::bc<T>[0], 2);
z[11] = z[11] * z[17];
z[27] = abb[25] * z[23];
z[28] = abb[19] * z[1];
z[5] = (T(1) / T(2)) * z[5] + -z[27] + -z[28];
z[27] = z[10] + -z[36];
z[28] = abb[50] + -abb[51];
z[32] = abb[48] + abb[49];
z[33] = z[28] + -z[32];
z[37] = abb[4] * z[33];
z[38] = (T(1) / T(4)) * z[37];
z[39] = abb[18] * z[1];
z[39] = (T(1) / T(2)) * z[8] + z[39];
z[40] = abb[7] * (T(1) / T(4));
z[41] = z[33] * z[40];
z[42] = z[12] + z[15];
z[43] = -abb[0] * z[42];
z[44] = abb[49] + -abb[50];
z[45] = abb[9] * z[44];
z[46] = abb[6] * abb[49];
z[47] = abb[5] * abb[48];
z[41] = -z[5] + z[16] + -z[27] + -z[34] + -z[38] + -z[39] + z[41] + z[43] + -z[45] + z[46] + z[47];
z[41] = abb[28] * z[41];
z[43] = -z[12] + z[15];
z[46] = abb[14] * z[43];
z[47] = (T(1) / T(2)) * z[46];
z[48] = abb[52] * (T(1) / T(2));
z[49] = abb[20] * z[48];
z[50] = abb[16] * z[2];
z[49] = z[47] + -z[49] + z[50];
z[51] = abb[6] * z[32];
z[52] = z[49] + z[51];
z[7] = z[7] + -z[8];
z[8] = 2 * z[14];
z[53] = z[7] + -z[8] + 4 * z[36];
z[54] = 3 * abb[49];
z[55] = abb[48] + z[54];
z[56] = 3 * abb[50] + -abb[51] + z[55];
z[57] = abb[0] * (T(1) / T(2));
z[56] = z[56] * z[57];
z[58] = (T(1) / T(2)) * z[43];
z[59] = abb[7] * z[58];
z[60] = abb[21] * z[48];
z[61] = z[52] + z[53] + -z[56] + -z[59] + -z[60];
z[61] = abb[30] * z[61];
z[62] = z[22] * z[48];
z[63] = abb[17] * z[2];
z[62] = z[50] + z[62] + z[63];
z[37] = (T(1) / T(2)) * z[37];
z[64] = abb[7] * (T(1) / T(2));
z[65] = -z[33] * z[64];
z[51] = z[37] + z[51] + z[62] + z[65];
z[51] = abb[32] * z[51];
z[65] = abb[30] * z[12];
z[66] = abb[32] * z[32];
z[67] = -2 * z[65] + z[66];
z[67] = abb[5] * z[67];
z[41] = z[41] + z[51] + -z[61] + z[67];
z[41] = abb[28] * z[41];
z[51] = abb[20] + (T(1) / T(2)) * z[22];
z[67] = z[48] * z[51];
z[67] = -z[47] + z[67];
z[68] = z[25] + -z[32];
z[69] = abb[5] * z[68];
z[70] = 3 * abb[48];
z[71] = -abb[50] + -z[15] + -z[70];
z[40] = z[40] * z[71];
z[71] = abb[49] + z[70];
z[72] = z[25] + z[71];
z[73] = abb[0] * (T(1) / T(4));
z[74] = -z[72] * z[73];
z[26] = abb[48] * (T(1) / T(2)) + -z[26];
z[75] = -abb[51] + -z[26];
z[75] = abb[6] * z[75];
z[38] = z[6] + z[10] + (T(3) / T(2)) * z[16] + z[38] + z[40] + (T(1) / T(2)) * z[45] + z[67] + (T(1) / T(4)) * z[69] + z[74] + z[75];
z[38] = abb[33] * z[38];
z[40] = abb[48] + -abb[51];
z[40] = abb[49] + abb[50] + 3 * z[40];
z[40] = z[40] * z[57];
z[74] = abb[25] * z[48];
z[3] = z[3] + -z[74];
z[40] = z[3] + z[40];
z[74] = abb[20] + z[22];
z[75] = z[48] * z[74];
z[47] = -z[47] + z[63] + z[75];
z[75] = abb[5] * z[32];
z[15] = abb[6] * z[15];
z[76] = 4 * z[16];
z[15] = 2 * z[15] + -z[40] + z[47] + z[59] + z[75] + z[76];
z[59] = -abb[28] * z[15];
z[75] = abb[7] * z[42];
z[74] = abb[52] * z[74];
z[74] = -z[46] + z[74] + -z[75];
z[75] = z[16] + z[37];
z[69] = z[45] + (T(1) / T(2)) * z[69] + (T(1) / T(2)) * z[74] + z[75];
z[74] = -abb[32] * z[69];
z[38] = z[38] + z[59] + z[74];
z[38] = abb[33] * z[38];
z[59] = 2 * z[16];
z[5] = z[5] + -z[59];
z[67] = -z[5] + z[34] + z[67];
z[74] = 2 * z[36];
z[39] = z[39] + -z[74];
z[29] = abb[51] * (T(3) / T(2)) + -z[29];
z[77] = -z[29] + z[32];
z[77] = abb[6] * z[77];
z[78] = -z[25] + z[55];
z[79] = z[64] * z[78];
z[12] = z[12] + z[54];
z[80] = abb[51] + z[12];
z[80] = z[57] * z[80];
z[81] = 2 * abb[49];
z[82] = (T(-1) / T(2)) * z[25] + z[81];
z[82] = abb[5] * z[82];
z[83] = 3 * z[1];
z[84] = abb[17] * z[83];
z[6] = -z[6] + z[39] + z[67] + z[77] + z[79] + z[80] + z[82] + -z[84];
z[6] = abb[28] * z[6];
z[16] = z[16] + -z[36];
z[77] = z[57] * z[78];
z[79] = abb[7] * abb[48];
z[77] = -z[77] + z[79];
z[79] = 2 * abb[48];
z[80] = abb[5] * z[79];
z[80] = z[77] + z[80];
z[54] = z[54] + z[70];
z[70] = z[28] + z[54];
z[82] = abb[6] * (T(1) / T(2));
z[85] = -z[70] * z[82];
z[85] = -z[16] + -z[50] + -z[80] + z[85];
z[85] = abb[31] * z[85];
z[77] = z[52] + z[77];
z[77] = -abb[32] * z[77];
z[52] = z[52] + z[80];
z[52] = abb[33] * z[52];
z[80] = -abb[32] * abb[48];
z[65] = z[65] + z[80];
z[65] = abb[5] * z[65];
z[6] = z[6] + z[52] + z[61] + 2 * z[65] + z[77] + z[85];
z[6] = abb[31] * z[6];
z[52] = abb[16] * z[83];
z[39] = z[8] + z[39] + z[52];
z[52] = z[31] + z[79];
z[52] = abb[6] * z[52];
z[61] = z[64] * z[72];
z[65] = -z[25] + z[71];
z[65] = z[57] * z[65];
z[30] = abb[50] * (T(3) / T(2)) + -z[30];
z[77] = z[30] + z[32];
z[77] = abb[5] * z[77];
z[52] = -z[35] + -z[39] + z[52] + z[61] + z[65] + -z[67] + z[77];
z[52] = abb[28] * z[52];
z[15] = abb[33] * z[15];
z[61] = z[79] + z[81];
z[30] = z[30] + z[61];
z[30] = abb[5] * z[30];
z[24] = -z[24] + -z[30] + z[39] + z[84];
z[5] = z[5] + z[34];
z[29] = z[29] + -z[61];
z[29] = abb[6] * z[29];
z[28] = abb[0] * z[28];
z[30] = abb[7] * z[32];
z[28] = z[5] + z[24] + z[28] + z[29] + -z[30];
z[28] = abb[31] * z[28];
z[29] = abb[30] * z[32];
z[29] = z[29] + -z[66];
z[29] = abb[5] * z[29];
z[15] = z[15] + z[28] + z[29];
z[28] = z[57] * z[72];
z[29] = abb[7] * abb[49];
z[34] = abb[6] * z[81];
z[28] = -z[28] + z[29] + z[34];
z[29] = abb[5] * z[70];
z[16] = z[14] + z[16] + -z[28] + (T(-1) / T(2)) * z[29] + -z[63];
z[16] = abb[29] * z[16];
z[29] = z[47] + z[60];
z[34] = abb[23] * z[48];
z[20] = (T(1) / T(2)) * z[20];
z[34] = -z[20] + z[34];
z[39] = z[29] + z[34];
z[28] = z[28] + z[39];
z[47] = abb[30] + -abb[32];
z[28] = z[28] * z[47];
z[16] = z[15] + z[16] + z[28] + z[52];
z[16] = abb[29] * z[16];
z[28] = abb[21] * abb[52];
z[18] = z[18] + z[28] + -z[46];
z[12] = abb[51] + -z[12];
z[12] = z[12] * z[64];
z[28] = abb[6] * z[19];
z[46] = -abb[52] * z[51];
z[12] = z[10] + z[12] + -z[18] + (T(-1) / T(2)) * z[28] + 3 * z[36] + z[46];
z[46] = -z[73] * z[78];
z[12] = (T(1) / T(2)) * z[12] + -z[14] + z[20] + z[35] + z[46];
z[14] = prod_pow(abb[30], 2);
z[12] = z[12] * z[14];
z[19] = z[19] * z[64];
z[35] = 2 * abb[0];
z[46] = z[13] * z[35];
z[47] = abb[6] * z[55];
z[19] = z[19] + z[34] + -z[46] + z[47] + z[53] + z[62];
z[46] = abb[55] * z[19];
z[47] = z[31] + z[32];
z[47] = abb[16] * z[47];
z[48] = z[31] + -z[32];
z[48] = abb[17] * z[48];
z[51] = abb[18] + abb[19];
z[52] = (T(1) / T(2)) * z[4];
z[51] = z[51] * z[52];
z[0] = -abb[43] + -2 * abb[45] + -4 * abb[53] + z[0];
z[0] = abb[27] * z[0];
z[23] = abb[26] * z[23];
z[2] = abb[7] * z[2];
z[0] = z[0] + -z[2] + -z[23] + -z[47] + z[48] + z[51];
z[2] = -abb[6] + abb[13] + abb[15];
z[23] = -z[1] * z[2];
z[23] = z[0] + z[23];
z[23] = abb[57] * z[23];
z[47] = z[64] * z[68];
z[48] = -abb[51] + z[32];
z[48] = z[35] * z[48];
z[51] = 2 * abb[51] + -z[55];
z[51] = abb[6] * z[51];
z[3] = z[3] + z[47] + z[48] + z[51] + -z[62] + -z[76];
z[3] = abb[35] * z[3];
z[47] = z[37] + 2 * z[45];
z[7] = -z[7] + z[47];
z[44] = -abb[7] * z[44];
z[48] = -abb[5] * z[4];
z[44] = -z[7] + -z[29] + z[44] + z[48] + -z[56] + z[74];
z[44] = abb[34] * z[44];
z[9] = abb[7] * z[9];
z[4] = abb[6] * z[4];
z[4] = -z[4] + z[9] + 2 * z[10] + z[40] + z[49] + -z[59];
z[9] = z[4] + z[37];
z[40] = abb[54] * z[9];
z[26] = abb[50] + z[26];
z[26] = z[14] * z[26];
z[48] = prod_pow(abb[32], 2);
z[49] = z[48] * z[68];
z[51] = 2 * abb[50];
z[52] = z[51] + z[71];
z[53] = abb[55] * z[52];
z[55] = abb[57] * z[1];
z[56] = -abb[35] * z[71];
z[26] = z[26] + (T(1) / T(4)) * z[49] + z[53] + z[55] + z[56];
z[26] = abb[5] * z[26];
z[42] = z[42] * z[64];
z[10] = z[10] + z[36];
z[36] = -z[10] + z[42];
z[42] = -z[36] + z[75];
z[43] = abb[6] * z[43];
z[22] = abb[52] * z[22];
z[43] = z[22] + z[43];
z[49] = -abb[5] * z[58];
z[32] = -abb[0] * z[32];
z[32] = z[32] + z[42] + (T(1) / T(2)) * z[43] + z[45] + z[49];
z[32] = abb[37] * z[32];
z[43] = abb[20] * abb[52];
z[18] = z[18] + z[28] + z[43];
z[18] = (T(1) / T(2)) * z[18] + -z[20] + z[36];
z[20] = abb[56] * z[18];
z[18] = z[18] + -z[37];
z[36] = abb[30] * z[18];
z[22] = z[22] + -z[28];
z[22] = (T(1) / T(2)) * z[22] + z[42];
z[22] = abb[32] * z[22];
z[22] = (T(1) / T(2)) * z[22] + z[36];
z[22] = abb[32] * z[22];
z[28] = abb[36] * z[69];
z[36] = -abb[56] * z[33];
z[33] = (T(1) / T(2)) * z[33];
z[33] = z[14] * z[33];
z[33] = z[33] + z[36];
z[36] = abb[4] * (T(1) / T(2));
z[33] = z[33] * z[36];
z[36] = -abb[32] + abb[33];
z[37] = abb[31] + -abb[33];
z[36] = z[36] * z[37];
z[17] = -abb[35] + -abb[36] + (T(1) / T(6)) * z[17] + z[36];
z[17] = abb[3] * z[17] * z[68];
z[14] = z[14] + (T(1) / T(2)) * z[48];
z[14] = z[14] * z[45];
z[3] = abb[58] + z[3] + z[6] + z[11] + z[12] + z[14] + z[16] + (T(1) / T(2)) * z[17] + z[20] + z[22] + z[23] + z[26] + z[28] + z[32] + z[33] + z[38] + z[40] + z[41] + z[44] + z[46];
z[6] = z[25] + z[54];
z[11] = z[6] * z[82];
z[11] = z[11] + z[50];
z[12] = abb[7] * z[13];
z[13] = -abb[50] * z[35];
z[7] = -z[7] + z[11] + z[12] + z[13] + z[21] + -z[27] + z[29];
z[7] = abb[30] * z[7];
z[12] = abb[5] * z[51];
z[4] = z[4] + -z[8] + z[12] + z[47] + z[74];
z[4] = abb[28] * z[4];
z[8] = -z[31] + -z[61];
z[8] = abb[6] * z[8];
z[6] = -z[6] * z[64];
z[12] = abb[0] * z[25];
z[5] = -z[5] + z[6] + z[8] + z[12] + z[24] + -z[34];
z[5] = abb[29] * z[5];
z[6] = z[10] + -z[11] + -z[30] + -z[39];
z[6] = abb[32] * z[6];
z[4] = z[4] + z[5] + z[6] + z[7] + z[15];
z[4] = m1_set::bc<T>[0] * z[4];
z[5] = abb[38] * z[9];
z[6] = abb[5] * z[52];
z[6] = z[6] + z[19];
z[6] = abb[39] * z[6];
z[2] = abb[5] + -z[2];
z[1] = z[1] * z[2];
z[0] = z[0] + z[1];
z[0] = abb[41] * z[0];
z[1] = abb[40] * z[18];
z[0] = abb[42] + z[0] + z[1] + z[4] + z[5] + z[6];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_489_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.886161099690250984741505458611170936143031814797445273809479501"),stof<T>("-3.7792196039290779840740318596922210627402822973481223569948307766")}, std::complex<T>{stof<T>("0.886161099690250984741505458611170936143031814797445273809479501"),stof<T>("-3.7792196039290779840740318596922210627402822973481223569948307766")}, std::complex<T>{stof<T>("1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("-7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("-0.886161099690250984741505458611170936143031814797445273809479501"),stof<T>("3.7792196039290779840740318596922210627402822973481223569948307766")}, std::complex<T>{stof<T>("-0.886161099690250984741505458611170936143031814797445273809479501"),stof<T>("3.7792196039290779840740318596922210627402822973481223569948307766")}, std::complex<T>{stof<T>("-21.606546010172162322515379700341139867996638041966289214523326148"),stof<T>("-2.702000933968601905048161264218677370994974637217526244113750991")}, std::complex<T>{stof<T>("-19.251487639891229114804122255455639533884839238370546186370837468"),stof<T>("2.702791952986176229640274038496251748496755566419030395721309865")}, std::complex<T>{stof<T>("-0.5527624557607640950123004076538785599513755392459028678507623014"),stof<T>("8.6180817887752370110104092174451592570304689492490970882826963077")}, std::complex<T>{stof<T>("3.2904641725129597777725651834777845479008968023777904580793849356"),stof<T>("8.6310949606228472502995409892366960969985099394394082902086153971")}, std::complex<T>{stof<T>("-1.929047365971231066630042088008710289906983071846621682967563806"),stof<T>("0.1299667118265977184776204054735914385079853180536135282563373817")}, std::complex<T>{stof<T>("3.544644398761003938966021834444683744572127259189781095237918004"),stof<T>("-15.116878415716311936296127438768884250961129189392489427979323107")}};
	
	std::vector<C> intdlogs = {rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[125].real()/kbase.W[125].real()), rlog(k.W[126].real()/kbase.W[126].real()), C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}, rlog(k.W[196].real()/kbase.W[196].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_489_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_489_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (v[1] + v[2] + -v[3] + -v[4]) * (-16 + -16 * v[0] + 3 * v[1] + -5 * v[2] + -7 * v[3] + 5 * v[4] + 4 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 8 * v[5])) / prod_pow(tend, 2);
c[1] = ((-1 + 2 * m1_set::bc<T>[1]) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[48] + abb[49] + -abb[51]) * (t * c[0] + c[1]);
	}
	{
T z[4];
z[0] = abb[48] + abb[49] + -abb[51];
z[0] = abb[11] * z[0];
z[1] = -abb[28] + abb[29];
z[1] = z[0] * z[1];
z[1] = 2 * z[1];
z[2] = abb[31] * z[0];
z[2] = z[1] + z[2];
z[2] = abb[31] * z[2];
z[3] = -abb[33] * z[0];
z[1] = -z[1] + z[3];
z[1] = abb[33] * z[1];
z[0] = abb[35] * z[0];
return 2 * z[0] + z[1] + z[2];
}

}
template <typename T, typename TABB> T SpDLog_f_4_489_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[48] + abb[49] + -abb[51]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[31] + -abb[33];
z[1] = abb[48] + abb[49] + -abb[51];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_489_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-14.19964576974122959598944279035209578005745742925733362181650657"),stof<T>("38.621420869721328157496920574462338387013500108456424778024073909")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,59> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W20(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_24_im(k), T{0}, rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[125].real()/k.W[125].real()), rlog(kend.W[126].real()/k.W[126].real()), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), rlog(kend.W[196].real()/k.W[196].real()), f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_24_re(k), T{0}};
abb[42] = SpDLog_f_4_489_W_20_Im(t, path, abb);
abb[58] = SpDLog_f_4_489_W_20_Re(t, path, abb);

                    
            return f_4_489_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_489_DLogXconstant_part(base_point<T>, kend);
	value += f_4_489_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_489_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_489_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_489_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_489_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_489_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_489_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
