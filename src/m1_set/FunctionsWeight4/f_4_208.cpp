/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_208.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_208_abbreviated (const std::array<T,62>& abb) {
T z[126];
z[0] = 5 * abb[46];
z[1] = 3 * abb[50];
z[2] = abb[45] * (T(-23) / T(3)) + z[0] + -z[1];
z[3] = abb[43] + abb[49];
z[4] = -abb[41] + abb[48];
z[5] = abb[44] * (T(3) / T(4));
z[2] = abb[47] * (T(-1) / T(2)) + abb[42] * (T(7) / T(6)) + (T(1) / T(4)) * z[2] + (T(3) / T(4)) * z[3] + (T(2) / T(3)) * z[4] + -z[5];
z[2] = abb[6] * z[2];
z[6] = abb[49] * (T(1) / T(2));
z[7] = abb[51] * (T(4) / T(3)) + abb[44] * (T(7) / T(6)) + -z[6];
z[8] = abb[45] * (T(2) / T(3));
z[9] = abb[50] * (T(1) / T(2));
z[10] = abb[47] * (T(3) / T(2));
z[11] = abb[43] * (T(-1) / T(6)) + abb[42] * (T(11) / T(6)) + z[7] + -z[8] + -z[9] + -z[10];
z[11] = abb[2] * z[11];
z[12] = abb[44] + -abb[45];
z[13] = abb[43] + -abb[49];
z[14] = -abb[46] + abb[50];
z[15] = z[12] + z[13] + -z[14];
z[15] = abb[42] + (T(1) / T(2)) * z[15];
z[16] = abb[3] * z[15];
z[17] = abb[44] * (T(1) / T(2));
z[18] = z[6] + -z[17];
z[19] = -abb[42] + z[18];
z[20] = abb[46] + abb[50];
z[21] = -abb[45] + z[20];
z[22] = abb[43] * (T(1) / T(2));
z[21] = -abb[47] + (T(1) / T(2)) * z[21] + -z[22];
z[23] = -z[19] + z[21];
z[24] = abb[14] * z[23];
z[25] = abb[54] + abb[56];
z[26] = abb[57] * (T(-1) / T(2)) + abb[52] * (T(1) / T(2)) + abb[53] * (T(1) / T(2)) + abb[55] * (T(1) / T(2)) + z[25];
z[27] = abb[25] * z[26];
z[27] = -z[24] + z[27];
z[28] = -z[4] + z[22];
z[29] = -abb[45] + z[14];
z[29] = (T(1) / T(2)) * z[29];
z[30] = -z[28] + z[29];
z[31] = -z[18] + -z[30];
z[32] = abb[12] * z[31];
z[33] = abb[42] * (T(1) / T(2));
z[34] = abb[47] + z[20];
z[35] = -3 * abb[49] + -5 * abb[51] + z[17] + z[33] + (T(5) / T(2)) * z[34];
z[35] = abb[9] * z[35];
z[36] = 3 * abb[44];
z[0] = -3 * abb[45] + abb[50] + z[0] + z[3] + -z[36];
z[37] = -abb[51] + (T(1) / T(4)) * z[0];
z[38] = abb[8] * z[37];
z[39] = abb[42] + abb[44];
z[40] = abb[49] * (T(5) / T(2));
z[41] = -abb[50] + abb[47] * (T(-9) / T(2)) + abb[43] * (T(-5) / T(6)) + abb[51] * (T(11) / T(3)) + (T(-11) / T(6)) * z[4] + (T(-2) / T(3)) * z[39] + z[40];
z[41] = abb[0] * z[41];
z[42] = abb[43] * (T(3) / T(2));
z[43] = abb[44] * (T(3) / T(2));
z[44] = abb[49] * (T(3) / T(2));
z[45] = z[42] + -z[43] + z[44];
z[46] = 3 * abb[46];
z[47] = z[1] + -z[46];
z[48] = abb[45] * (T(1) / T(3));
z[49] = z[47] + -z[48];
z[50] = abb[42] + z[4];
z[49] = -z[45] + (T(1) / T(2)) * z[49] + (T(5) / T(3)) * z[50];
z[51] = abb[5] * (T(1) / T(2));
z[49] = z[49] * z[51];
z[52] = abb[22] + abb[24];
z[53] = (T(3) / T(2)) * z[26];
z[52] = z[52] * z[53];
z[54] = abb[44] + abb[45];
z[55] = -abb[49] + z[54];
z[56] = -z[4] + z[55];
z[57] = abb[1] * z[56];
z[58] = -abb[43] + abb[44] + z[4];
z[48] = abb[46] + -z[48] + (T(-1) / T(3)) * z[58];
z[48] = abb[51] * (T(-1) / T(3)) + (T(1) / T(2)) * z[48];
z[48] = abb[11] * z[48];
z[8] = abb[46] + -z[8];
z[7] = abb[42] * (T(1) / T(6)) + abb[43] * (T(2) / T(3)) + -z[7] + 2 * z[8];
z[7] = abb[7] * z[7];
z[8] = -abb[46] + abb[47] + z[55];
z[8] = abb[10] * z[8];
z[55] = 3 * z[8];
z[58] = abb[23] * z[26];
z[59] = abb[21] * z[53];
z[2] = z[2] + z[7] + z[11] + (T(-1) / T(2)) * z[16] + z[27] + -z[32] + z[35] + -5 * z[38] + z[41] + 11 * z[48] + z[49] + -z[52] + -z[55] + (T(7) / T(2)) * z[57] + (T(-5) / T(2)) * z[58] + -z[59];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[7] = abb[50] * (T(3) / T(2));
z[11] = abb[43] + abb[45];
z[35] = abb[51] + (T(-3) / T(2)) * z[4] + -z[7] + z[11] + z[17] + -z[33];
z[35] = abb[5] * z[35];
z[18] = z[18] + z[21];
z[21] = abb[4] * z[18];
z[38] = z[46] + -z[54];
z[41] = z[1] + -z[3] + z[38];
z[48] = 2 * abb[51];
z[49] = (T(1) / T(2)) * z[41] + -z[48];
z[60] = abb[15] * z[49];
z[60] = (T(3) / T(2)) * z[21] + -z[57] + -z[60];
z[61] = z[6] + z[22];
z[62] = 2 * abb[42];
z[63] = z[61] + -z[62];
z[12] = -abb[51] + (T(-1) / T(2)) * z[4] + z[10] + -z[12] + z[63];
z[12] = abb[6] * z[12];
z[64] = (T(3) / T(2)) * z[32];
z[65] = -z[59] + z[64];
z[53] = abb[25] * z[53];
z[53] = (T(3) / T(2)) * z[24] + -z[53];
z[66] = abb[51] + (T(-1) / T(4)) * z[41];
z[66] = abb[7] * z[66];
z[67] = -abb[49] + z[34];
z[67] = -abb[51] + (T(1) / T(2)) * z[67];
z[68] = 3 * abb[9];
z[69] = z[67] * z[68];
z[70] = -2 * abb[44] + z[3] + -z[48];
z[71] = 3 * abb[47];
z[72] = z[70] + z[71];
z[73] = -3 * abb[42] + abb[45] + z[72];
z[74] = abb[2] * z[73];
z[75] = abb[47] + -abb[50];
z[76] = abb[43] + z[75];
z[77] = 3 * z[76];
z[78] = -abb[49] + z[39];
z[79] = z[77] + z[78];
z[80] = abb[0] * (T(1) / T(2));
z[79] = z[79] * z[80];
z[12] = z[12] + z[35] + z[53] + -z[60] + -z[65] + z[66] + -z[69] + z[74] + z[79];
z[12] = abb[27] * z[12];
z[20] = abb[45] + z[20];
z[35] = (T(3) / T(2)) * z[20];
z[66] = abb[44] * (T(5) / T(2));
z[28] = z[6] + z[28] + z[35] + -z[66] + z[71];
z[79] = (T(1) / T(2)) * z[28] + -z[48] + -z[62];
z[79] = abb[6] * z[79];
z[29] = -abb[41] + abb[48] + z[29];
z[29] = -abb[42] + 3 * z[29] + z[40] + -z[42] + -z[66];
z[29] = z[29] * z[51];
z[42] = -z[52] + z[53];
z[81] = z[42] + z[74];
z[82] = 2 * z[57];
z[77] = -z[77] + z[78];
z[77] = z[77] * z[80];
z[80] = abb[20] * z[26];
z[83] = 3 * z[80];
z[29] = z[29] + z[65] + z[77] + z[79] + z[81] + -z[82] + -z[83];
z[29] = abb[30] * z[29];
z[77] = z[34] + -z[39] + -z[48];
z[79] = abb[9] * z[77];
z[84] = abb[24] * z[26];
z[85] = z[80] + z[84];
z[79] = z[27] + z[79] + z[85];
z[79] = abb[32] * z[79];
z[86] = abb[22] * z[26];
z[87] = abb[32] * z[86];
z[79] = z[79] + z[87];
z[88] = 4 * abb[51];
z[41] = -z[41] + z[88];
z[89] = abb[31] * z[41];
z[90] = abb[32] * z[41];
z[89] = z[89] + -z[90];
z[89] = abb[15] * z[89];
z[91] = 2 * abb[6];
z[73] = z[73] * z[91];
z[73] = -z[73] + -3 * z[74];
z[73] = abb[32] * z[73];
z[92] = abb[32] * z[49];
z[93] = abb[7] * z[92];
z[73] = z[73] + 3 * z[79] + -z[89] + z[93];
z[79] = -abb[43] + z[48];
z[38] = z[4] + -z[38] + z[79];
z[38] = abb[11] * z[38];
z[89] = 2 * abb[43] + -z[1];
z[93] = abb[49] + z[48];
z[94] = z[89] + z[93];
z[95] = -z[4] + z[94];
z[95] = abb[0] * z[95];
z[95] = -3 * z[32] + -z[38] + 4 * z[57] + z[95];
z[96] = abb[6] * z[49];
z[97] = abb[7] * z[49];
z[98] = z[96] + z[97];
z[99] = 3 * z[26];
z[100] = abb[21] * z[99];
z[101] = z[83] + z[100];
z[102] = z[95] + -z[98] + z[101];
z[102] = abb[29] * z[102];
z[103] = -z[34] + z[93];
z[103] = abb[9] * z[103];
z[104] = z[21] + z[103];
z[105] = z[84] + z[86];
z[106] = -z[104] + z[105];
z[98] = z[98] + 3 * z[106];
z[106] = -abb[31] * z[98];
z[107] = abb[31] * z[49];
z[108] = -z[92] + z[107];
z[56] = abb[29] * z[56];
z[109] = -2 * z[56] + -z[108];
z[109] = abb[5] * z[109];
z[12] = z[12] + z[29] + z[73] + -z[102] + z[106] + z[109];
z[12] = abb[27] * z[12];
z[29] = z[16] + -z[80];
z[106] = abb[7] * z[78];
z[29] = 3 * z[29] + -2 * z[106];
z[96] = z[29] + z[96];
z[96] = abb[31] * z[96];
z[29] = -abb[32] * z[29];
z[106] = -abb[6] * z[92];
z[109] = abb[42] + -abb[45];
z[94] = z[94] + z[109];
z[110] = abb[32] * z[94];
z[94] = -abb[31] * z[94];
z[94] = z[94] + z[110];
z[94] = abb[2] * z[94];
z[111] = -abb[31] + abb[32];
z[111] = z[78] * z[111];
z[56] = z[56] + z[111];
z[56] = abb[5] * z[56];
z[111] = -abb[0] + abb[2] + -abb[7];
z[111] = z[78] * z[111];
z[112] = -abb[45] + z[50];
z[113] = abb[6] * z[112];
z[111] = z[57] + z[111] + z[113];
z[111] = abb[30] * z[111];
z[29] = z[29] + 2 * z[56] + z[94] + z[96] + z[102] + z[106] + z[111];
z[29] = abb[30] * z[29];
z[56] = abb[29] * z[49];
z[94] = -abb[30] * z[49];
z[79] = -z[71] + z[79];
z[96] = 2 * abb[49];
z[102] = z[79] + z[96];
z[106] = -z[54] + z[102];
z[106] = 2 * z[106];
z[111] = abb[31] * z[106];
z[92] = -z[56] + z[92] + z[94] + -z[111];
z[92] = abb[27] * z[92];
z[94] = 2 * abb[47];
z[113] = (T(1) / T(2)) * z[20] + -z[48] + z[94];
z[114] = z[17] + z[22];
z[115] = -z[44] + z[113] + z[114];
z[116] = 3 * z[115];
z[117] = -abb[31] * z[116];
z[117] = z[56] + z[117];
z[117] = abb[29] * z[117];
z[56] = -z[56] + z[108];
z[108] = -abb[30] * z[56];
z[118] = prod_pow(abb[32], 2);
z[119] = z[49] * z[118];
z[120] = abb[33] + -abb[35];
z[18] = z[18] * z[120];
z[121] = z[1] + z[46];
z[122] = 5 * abb[45];
z[123] = -z[121] + z[122];
z[123] = (T(1) / T(2)) * z[123];
z[66] = z[66] + z[123];
z[124] = abb[43] * (T(5) / T(2));
z[125] = -6 * abb[47] + abb[49] * (T(7) / T(2)) + z[48] + -z[66] + -z[124];
z[125] = abb[32] * z[125];
z[111] = -z[111] + z[125];
z[111] = abb[31] * z[111];
z[116] = abb[58] * z[116];
z[18] = 3 * z[18] + z[92] + z[108] + z[111] + z[116] + z[117] + -z[119];
z[18] = abb[13] * z[18];
z[92] = z[4] + -z[48];
z[6] = -z[6] + z[7] + z[10] + -z[33] + z[92] + -z[114];
z[6] = abb[0] * z[6];
z[7] = -z[38] + z[82];
z[6] = z[6] + -z[7];
z[10] = z[6] + z[65];
z[33] = -z[4] + z[45];
z[65] = z[33] + z[71] + z[123];
z[65] = -z[62] + (T(1) / T(2)) * z[65];
z[65] = abb[6] * z[65];
z[47] = z[47] + z[122];
z[45] = -z[45] + (T(1) / T(2)) * z[47] + -z[50];
z[45] = z[45] * z[51];
z[47] = -z[10] + z[45] + z[65] + z[81] + -z[97];
z[47] = abb[30] * z[47];
z[65] = abb[27] * z[49];
z[65] = z[56] + z[65];
z[65] = abb[13] * z[65];
z[56] = abb[5] * z[56];
z[81] = 2 * abb[45];
z[46] = -z[46] + -z[70] + z[81];
z[70] = 2 * z[46];
z[82] = abb[6] * z[70];
z[70] = abb[7] * z[70];
z[82] = z[70] + z[82] + z[100];
z[97] = (T(1) / T(2)) * z[0] + -z[48];
z[108] = abb[8] * z[97];
z[58] = z[58] + z[108];
z[111] = z[85] + z[86];
z[114] = z[58] + z[111];
z[114] = z[82] + 3 * z[114];
z[116] = abb[31] * z[114];
z[47] = z[47] + -z[56] + -z[65] + z[73] + -z[116];
z[40] = z[40] + z[124];
z[56] = 9 * abb[47];
z[35] = abb[44] * (T(13) / T(2)) + -z[4] + -z[35] + -z[40] + -z[56];
z[65] = 4 * abb[42];
z[73] = z[65] + z[88];
z[35] = (T(1) / T(2)) * z[35] + z[73];
z[35] = abb[6] * z[35];
z[116] = abb[45] + abb[46];
z[117] = z[1] + z[116];
z[40] = z[17] + -z[40] + -z[50] + (T(3) / T(2)) * z[117];
z[40] = (T(1) / T(2)) * z[40] + -z[48];
z[40] = abb[5] * z[40];
z[50] = abb[15] * z[41];
z[83] = z[50] + z[83];
z[68] = z[68] * z[77];
z[77] = 2 * z[74];
z[68] = z[68] + -z[77];
z[10] = -z[10] + z[35] + z[40] + -z[42] + z[68] + z[83];
z[10] = abb[27] * z[10];
z[35] = -z[4] + z[62] + -z[72];
z[35] = abb[0] * z[35];
z[40] = abb[7] * z[46];
z[42] = abb[5] + -z[91];
z[42] = z[42] * z[112];
z[35] = z[35] + -z[38] + z[40] + z[42] + -z[57] + z[74];
z[35] = abb[28] * z[35];
z[40] = -z[50] + -z[114];
z[40] = abb[29] * z[40];
z[10] = z[10] + z[35] + z[40] + -z[47];
z[10] = abb[28] * z[10];
z[35] = abb[6] * z[23];
z[40] = -abb[7] * z[15];
z[42] = -z[3] + z[39] + -z[75];
z[42] = abb[2] * z[42];
z[3] = -z[3] + z[14] + z[54];
z[14] = -z[3] * z[51];
z[14] = z[14] + z[16] + z[27] + z[35] + z[40] + z[42] + z[105];
z[14] = abb[34] * z[14];
z[34] = -abb[49] + -z[34];
z[34] = abb[51] + (T(1) / T(2)) * z[34] + z[39];
z[34] = abb[9] * z[34];
z[35] = abb[49] + z[20];
z[5] = abb[43] * (T(1) / T(4)) + -z[5];
z[35] = -abb[42] + abb[47] + -abb[51] + z[5] + (T(1) / T(4)) * z[35];
z[35] = abb[6] * z[35];
z[40] = (T(1) / T(2)) * z[84];
z[34] = z[34] + z[35] + -z[40] + -z[80];
z[34] = z[34] * z[118];
z[35] = abb[0] * z[76];
z[35] = -z[32] + z[35] + z[57] + z[85];
z[35] = abb[33] * z[35];
z[42] = (T(1) / T(2)) * z[118];
z[46] = z[42] + -z[120];
z[46] = z[21] * z[46];
z[51] = abb[35] + z[118];
z[51] = z[16] * z[51];
z[57] = -abb[20] + -abb[24];
z[72] = abb[35] * z[26];
z[57] = z[57] * z[72];
z[42] = abb[33] + -z[42];
z[42] = z[26] * z[42];
z[42] = z[42] + -z[72];
z[42] = abb[22] * z[42];
z[14] = z[14] + z[34] + z[35] + z[42] + z[46] + z[51] + z[57];
z[34] = abb[23] * z[99];
z[34] = z[34] + 3 * z[108];
z[35] = z[34] + z[52];
z[42] = abb[6] + abb[7];
z[46] = -3 * z[42];
z[46] = z[37] * z[46];
z[51] = abb[49] + z[79];
z[52] = z[4] + -z[51];
z[52] = abb[0] * z[52];
z[38] = z[35] + z[38] + z[46] + z[52] + z[55] + z[60] + -z[69] + z[101];
z[38] = abb[29] * z[38];
z[46] = z[42] * z[97];
z[52] = abb[21] * z[26];
z[46] = z[46] + -z[52] + -z[58] + -z[80] + -z[104];
z[52] = abb[31] * z[46];
z[38] = z[38] + 3 * z[52];
z[38] = abb[29] * z[38];
z[22] = -2 * z[4] + z[22] + -z[44] + z[48] + z[66];
z[44] = abb[59] * z[22];
z[15] = -abb[35] * z[15];
z[52] = abb[33] * z[31];
z[15] = z[15] + z[52];
z[11] = z[11] + -z[121];
z[52] = 5 * abb[49];
z[55] = z[11] + -z[36] + z[52];
z[57] = -z[48] + (T(-1) / T(2)) * z[55] + z[62];
z[57] = abb[32] * z[57];
z[55] = -abb[42] + abb[51] + (T(1) / T(4)) * z[55];
z[58] = abb[31] * z[55];
z[58] = z[57] + z[58];
z[58] = abb[31] * z[58];
z[55] = z[55] * z[118];
z[5] = -abb[51] + abb[49] * (T(-5) / T(4)) + -z[4] + -z[5] + (T(3) / T(4)) * z[20];
z[5] = prod_pow(abb[29], 2) * z[5];
z[5] = z[5] + 3 * z[15] + z[44] + z[55] + z[58];
z[5] = abb[5] * z[5];
z[9] = -abb[47] + -z[4] + z[9] + z[17] + -z[61] + (T(1) / T(2)) * z[116];
z[9] = abb[16] * z[9];
z[15] = abb[19] * z[23];
z[17] = -z[19] + z[30];
z[17] = abb[17] * z[17];
z[19] = abb[18] * z[31];
z[20] = abb[26] * z[26];
z[9] = z[9] + z[15] + -z[17] + z[19] + -z[20];
z[15] = (T(-3) / T(2)) * z[9];
z[15] = abb[61] * z[15];
z[13] = -5 * abb[42] + z[1] + -z[13] + -z[36] + z[71] + z[81] + -z[88];
z[13] = z[13] * z[118];
z[17] = z[51] + z[109];
z[17] = abb[31] * z[17];
z[17] = z[17] + z[110];
z[17] = abb[31] * z[17];
z[19] = abb[35] * z[76];
z[13] = z[13] + z[17] + -3 * z[19];
z[13] = abb[2] * z[13];
z[17] = -z[43] + z[63] + z[113];
z[17] = abb[6] * z[17];
z[19] = abb[9] * z[78];
z[17] = z[17] + z[19] + -z[21] + -z[27] + z[74] + -z[80];
z[19] = 3 * abb[58];
z[19] = -z[17] * z[19];
z[20] = z[21] + -z[86];
z[23] = -abb[6] * z[37];
z[26] = -abb[9] * z[67];
z[20] = -z[8] + (T(1) / T(2)) * z[20] + z[23] + z[26] + -z[40];
z[11] = 9 * abb[44] + -7 * abb[49] + z[11];
z[11] = abb[51] + (T(1) / T(4)) * z[11] + z[62];
z[11] = abb[7] * z[11];
z[11] = z[11] + 3 * z[20];
z[11] = abb[31] * z[11];
z[16] = -z[16] + z[85] + -z[104];
z[16] = abb[32] * z[16];
z[16] = z[16] + z[87];
z[20] = abb[7] * z[57];
z[11] = z[11] + 3 * z[16] + z[20];
z[11] = abb[31] * z[11];
z[16] = 5 * abb[43] + 9 * abb[46] + -z[1] + z[52] + -7 * z[54];
z[16] = (T(1) / T(2)) * z[16] + -z[48];
z[16] = z[16] * z[42];
z[20] = abb[22] * z[99];
z[16] = -z[16] + z[20] + z[34] + 3 * z[84] + -z[95];
z[20] = -abb[59] * z[16];
z[23] = z[90] + z[107];
z[23] = abb[31] * z[23];
z[26] = -abb[59] * z[41];
z[23] = z[23] + z[26] + z[119];
z[23] = abb[15] * z[23];
z[26] = -abb[5] * z[49];
z[27] = -abb[13] * z[106];
z[26] = z[26] + z[27] + -z[50] + -z[98];
z[26] = abb[36] * z[26];
z[27] = abb[13] * z[115];
z[30] = -z[27] + z[46];
z[30] = 3 * z[30];
z[31] = abb[60] * z[30];
z[3] = abb[35] * z[3];
z[3] = (T(-3) / T(2)) * z[3] + z[55];
z[3] = abb[7] * z[3];
z[34] = abb[33] * z[100];
z[2] = z[2] + z[3] + z[5] + z[10] + z[11] + z[12] + z[13] + 3 * z[14] + z[15] + z[18] + z[19] + z[20] + z[23] + z[26] + z[29] + z[31] + z[34] + z[38];
z[1] = 19 * abb[45] + -21 * abb[46] + z[1];
z[1] = (T(1) / T(2)) * z[1] + -z[33] + z[56];
z[1] = (T(1) / T(2)) * z[1] + -z[65];
z[1] = abb[6] * z[1];
z[1] = z[1] + z[6] + z[35] + -z[45] + z[53] + z[59] + z[64] + -z[68] + z[70];
z[1] = abb[28] * z[1];
z[0] = -z[0] + z[88];
z[0] = abb[8] * z[0];
z[3] = -abb[50] + -z[4] + z[93] + -z[94];
z[3] = abb[0] * z[3];
z[5] = -abb[52] + -abb[53] + -abb[55] + abb[57] + -2 * z[25];
z[5] = abb[23] * z[5];
z[0] = z[0] + z[3] + z[5] + z[7] + -2 * z[8] + -z[32] + -z[104] + -z[111];
z[0] = 3 * z[0] + -z[50] + -z[82];
z[0] = abb[29] * z[0];
z[3] = -z[28] + z[73];
z[3] = abb[6] * z[3];
z[5] = z[21] + -z[24] + -z[103];
z[6] = z[39] + -z[89] + z[92] + -z[96];
z[6] = abb[5] * z[6];
z[4] = -z[4] + -z[39] + z[102];
z[4] = abb[0] * z[4];
z[8] = abb[25] * z[99];
z[3] = z[3] + z[4] + 3 * z[5] + z[6] + z[7] + z[8] + -z[77] + z[83];
z[3] = abb[27] * z[3];
z[0] = z[0] + z[1] + z[3] + -z[47];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = (T(-1) / T(2)) * z[9];
z[1] = abb[40] * z[1];
z[3] = -abb[37] * z[17];
z[4] = abb[37] * z[27];
z[1] = z[1] + z[3] + z[4];
z[3] = abb[5] * z[22];
z[3] = z[3] + -z[16] + -z[50];
z[3] = abb[38] * z[3];
z[4] = abb[39] * z[30];
z[0] = z[0] + 3 * z[1] + z[3] + z[4];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_208_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("18.637792491674668814750023818104117471071713161681467043320545904"),stof<T>("-60.341860181793144151011350264853477041651685238276703111950883009")}, std::complex<T>{stof<T>("-15.254538031344221882234898815395995301210979956066736334501994495"),stof<T>("4.964525490228577077160159552622320638799579908595529274791218172")}, std::complex<T>{stof<T>("1.9230263948588290453996676827664011057810529065392366644334176496"),stof<T>("9.307535070333749016582503752162411395015383528204727111045930336")}, std::complex<T>{stof<T>("8.4124856135380201588587852463544668943566313660778329186119688761"),stof<T>("-6.2054549665462334704876011592708855319908336813078397410419910577")}, std::complex<T>{stof<T>("23.667023644882242041093684061750462195567611322144569253113963371"),stof<T>("-11.16998045677481054764776071189320617079041358990336901583320923")}, std::complex<T>{stof<T>("-5.531856137578063120391541743576013059963026160707794451212596247"),stof<T>("-33.915119523662488760840652524024513749197737097890297224342025657")}, std::complex<T>{stof<T>("6.449632563695912377695446426483077494809823066757646632807656336"),stof<T>("73.736174996707927462469030334058439387540189727061098078939252239")}, std::complex<T>{stof<T>("-18.637792491674668814750023818104117471071713161681467043320545904"),stof<T>("60.341860181793144151011350264853477041651685238276703111950883009")}, std::complex<T>{stof<T>("-7.9098606291675302648109037464247985588895033658331406771927900959"),stof<T>("-9.0513052348096113720353358696648715896727008691751971307336570652")}, std::complex<T>{stof<T>("-2.4256513792293189394475491826960694412481809067839289058525964298"),stof<T>("5.949225131022095825940433276773345726648151022278309760729717787")}, std::complex<T>{stof<T>("1.00524996874097978809576299985933667093425600048938448283835756"),stof<T>("-30.513520402711689685045874057871514243327069100966073743551296246")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("11.574284195827386399780252528052261739441898431079730097805382836"),stof<T>("-0.7798002709595863108657224328415486310479119083216811695380242902")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("11.574284195827386399780252528052261739441898431079730097805382836"),stof<T>("-0.7798002709595863108657224328415486310479119083216811695380242902")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[84].real()/kbase.W[84].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_208_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_208_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-47.823770164394209598422715464646930589120511918748693474957142454"),stof<T>("32.905444792205210045668180505886577453864605846940315980172814194")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,62> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W32(k,dl), dlog_W85(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[84].real()/k.W[84].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_24_re(k)};

                    
            return f_4_208_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_208_DLogXconstant_part(base_point<T>, kend);
	value += f_4_208_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_208_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_208_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_208_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_208_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_208_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_208_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
