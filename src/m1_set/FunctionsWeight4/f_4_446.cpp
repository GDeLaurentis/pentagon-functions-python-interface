/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_446.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_446_abbreviated (const std::array<T,62>& abb) {
T z[96];
z[0] = abb[33] * (T(3) / T(2));
z[1] = -abb[29] + z[0];
z[2] = abb[34] * (T(1) / T(2));
z[3] = -abb[32] + z[2];
z[4] = abb[30] * (T(1) / T(2));
z[5] = abb[31] * (T(1) / T(2));
z[6] = -z[1] + -z[3] + -z[4] + z[5];
z[6] = abb[5] * z[6];
z[7] = abb[15] * (T(1) / T(2));
z[8] = -abb[32] + abb[33];
z[9] = -abb[29] + z[8];
z[10] = abb[34] + z[9];
z[11] = z[7] * z[10];
z[12] = -abb[31] + abb[33];
z[13] = abb[4] * z[12];
z[14] = abb[30] + z[9];
z[15] = abb[8] * z[14];
z[16] = -z[13] + -z[15];
z[17] = z[4] + z[9];
z[18] = z[2] + z[17];
z[18] = abb[3] * z[18];
z[6] = z[6] + z[11] + (T(1) / T(2)) * z[16] + z[18];
z[6] = m1_set::bc<T>[0] * z[6];
z[16] = abb[8] * abb[40];
z[18] = -abb[40] + abb[41];
z[19] = abb[15] * z[18];
z[20] = abb[5] + -abb[8];
z[21] = -abb[4] + -z[20];
z[21] = abb[41] * z[21];
z[21] = -z[16] + z[19] + z[21];
z[22] = abb[30] + -abb[32];
z[22] = m1_set::bc<T>[0] * z[22];
z[22] = abb[42] + z[22];
z[23] = abb[7] * (T(1) / T(2));
z[22] = z[22] * z[23];
z[20] = abb[3] + z[20];
z[24] = abb[42] * (T(1) / T(2));
z[25] = z[20] * z[24];
z[26] = abb[29] + -abb[33];
z[27] = m1_set::bc<T>[0] * z[26];
z[28] = abb[9] * z[27];
z[29] = abb[12] * z[18];
z[30] = -abb[31] + z[8];
z[31] = -abb[30] + -z[30];
z[31] = m1_set::bc<T>[0] * z[31];
z[31] = -abb[41] + z[31];
z[31] = abb[0] * z[31];
z[6] = z[6] + (T(1) / T(2)) * z[21] + -z[22] + z[25] + z[28] + -z[29] + z[31];
z[21] = abb[31] + abb[32];
z[25] = (T(1) / T(2)) * z[21];
z[31] = abb[29] * (T(1) / T(2));
z[32] = -abb[33] + z[31];
z[33] = z[25] + z[32];
z[34] = -z[4] + z[33];
z[34] = m1_set::bc<T>[0] * z[34];
z[34] = abb[41] * (T(-1) / T(2)) + z[34];
z[34] = abb[1] * z[34];
z[35] = abb[16] * (T(1) / T(4));
z[36] = abb[29] + -abb[31];
z[37] = abb[30] + z[36];
z[38] = -abb[34] + z[37];
z[39] = m1_set::bc<T>[0] * z[38];
z[40] = abb[40] + z[39];
z[41] = abb[42] + z[40];
z[42] = z[35] * z[41];
z[43] = z[34] + -z[42];
z[6] = (T(1) / T(2)) * z[6] + -z[43];
z[44] = abb[2] * (T(1) / T(16));
z[45] = -abb[42] * z[44];
z[6] = (T(1) / T(8)) * z[6] + z[45];
z[6] = abb[55] * z[6];
z[45] = z[5] + -z[8];
z[46] = z[31] + z[45];
z[47] = abb[30] * (T(3) / T(2)) + -z[2] + -z[46];
z[48] = abb[3] * z[47];
z[49] = abb[5] * z[14];
z[48] = -z[48] + z[49];
z[48] = m1_set::bc<T>[0] * z[48];
z[47] = m1_set::bc<T>[0] * z[47];
z[49] = abb[40] * (T(1) / T(2));
z[50] = -abb[41] + z[49];
z[47] = z[47] + -z[50];
z[47] = abb[0] * z[47];
z[51] = z[19] + -z[29];
z[52] = 3 * abb[3] + abb[8];
z[53] = -abb[0] + (T(1) / T(2)) * z[52];
z[53] = abb[42] * z[53];
z[54] = abb[5] * abb[41];
z[49] = abb[8] * z[49];
z[47] = z[47] + z[48] + z[49] + -z[51] + -z[53] + z[54];
z[48] = m1_set::bc<T>[0] * z[12];
z[49] = abb[10] * (T(1) / T(2));
z[53] = z[48] * z[49];
z[54] = abb[42] + (T(1) / T(2)) * z[39];
z[54] = abb[13] * z[54];
z[43] = z[43] + (T(1) / T(2)) * z[47] + z[53] + z[54];
z[47] = abb[51] + abb[53];
z[47] = (T(1) / T(8)) * z[47];
z[43] = z[43] * z[47];
z[53] = abb[30] + -abb[34] + z[12];
z[53] = abb[5] * z[53];
z[13] = -z[13] + z[15] + z[53];
z[15] = z[5] + z[32];
z[32] = abb[3] * z[15];
z[11] = z[11] + (T(1) / T(2)) * z[13] + z[32];
z[11] = m1_set::bc<T>[0] * z[11];
z[13] = -z[2] + z[4] + -z[15];
z[13] = m1_set::bc<T>[0] * z[13];
z[13] = z[13] + -z[50];
z[13] = abb[0] * z[13];
z[32] = abb[5] + abb[8];
z[50] = -abb[4] + z[32];
z[50] = abb[41] * z[50];
z[19] = -z[19] + z[50];
z[50] = abb[5] * z[24];
z[11] = z[11] + z[13] + (T(1) / T(2)) * z[19] + -z[22] + z[29] + z[50];
z[11] = (T(1) / T(2)) * z[11] + z[34];
z[13] = abb[10] * (T(1) / T(16));
z[19] = z[13] * z[48];
z[11] = (T(1) / T(8)) * z[11] + z[19];
z[11] = abb[54] * z[11];
z[19] = m1_set::bc<T>[0] * z[14];
z[19] = abb[42] + z[18] + z[19];
z[22] = abb[21] + abb[23];
z[19] = -z[19] * z[22];
z[29] = m1_set::bc<T>[0] * z[10];
z[29] = z[18] + z[29];
z[29] = abb[25] * z[29];
z[18] = -z[18] + z[27];
z[27] = abb[22] * z[18];
z[34] = abb[26] * z[41];
z[19] = z[19] + z[27] + z[29] + -z[34];
z[27] = abb[24] * z[18];
z[27] = z[19] + z[27];
z[29] = abb[46] + abb[47] + abb[48];
z[29] = (T(1) / T(32)) * z[29];
z[27] = -z[27] * z[29];
z[34] = abb[44] * (T(1) / T(16));
z[19] = -z[19] * z[34];
z[48] = -abb[3] * z[39];
z[40] = abb[0] * z[40];
z[16] = z[16] + z[40] + z[48];
z[40] = abb[3] + abb[8];
z[48] = abb[0] + (T(-1) / T(2)) * z[40];
z[48] = abb[42] * z[48];
z[16] = (T(1) / T(2)) * z[16] + z[48];
z[24] = -abb[2] * z[24];
z[16] = (T(1) / T(2)) * z[16] + z[24] + -z[42] + z[54];
z[24] = abb[56] * (T(1) / T(8));
z[16] = z[16] * z[24];
z[42] = abb[45] + abb[50];
z[14] = z[14] * z[42];
z[48] = abb[29] * abb[49];
z[50] = abb[33] * abb[49];
z[53] = z[48] + -z[50];
z[54] = abb[32] * abb[49];
z[55] = z[53] + z[54];
z[56] = abb[30] * abb[49];
z[14] = z[14] + -z[55] + z[56];
z[14] = m1_set::bc<T>[0] * z[14];
z[57] = abb[49] + z[42];
z[58] = abb[41] * z[57];
z[59] = abb[40] * z[57];
z[58] = z[58] + -z[59];
z[60] = abb[42] * z[57];
z[14] = z[14] + z[58] + z[60];
z[61] = (T(1) / T(32)) * z[22];
z[14] = -z[14] * z[61];
z[26] = z[26] * z[42];
z[26] = z[26] + z[53];
z[26] = m1_set::bc<T>[0] * z[26];
z[26] = z[26] + -z[58];
z[53] = abb[22] * (T(1) / T(32));
z[62] = z[26] * z[53];
z[63] = -z[38] * z[42];
z[64] = abb[31] * abb[49];
z[65] = -z[48] + z[64];
z[66] = -z[56] + z[65];
z[67] = abb[34] * abb[49];
z[63] = z[63] + z[66] + z[67];
z[63] = m1_set::bc<T>[0] * z[63];
z[59] = -z[59] + -z[60] + z[63];
z[60] = abb[26] * (T(1) / T(32));
z[59] = z[59] * z[60];
z[63] = -abb[0] * z[18];
z[28] = z[28] + -z[51] + z[63];
z[28] = abb[52] * z[28];
z[10] = z[10] * z[42];
z[10] = z[10] + -z[55] + z[67];
z[10] = m1_set::bc<T>[0] * z[10];
z[10] = z[10] + z[58];
z[51] = abb[25] * (T(1) / T(32));
z[10] = z[10] * z[51];
z[58] = -abb[44] * z[18];
z[26] = (T(1) / T(2)) * z[26] + z[58];
z[58] = abb[24] * (T(1) / T(16));
z[26] = z[26] * z[58];
z[63] = -abb[54] + abb[55];
z[68] = abb[6] * (T(1) / T(32));
z[63] = z[63] * z[68];
z[18] = -z[18] * z[63];
z[39] = -abb[42] + -z[39];
z[39] = abb[18] * z[39];
z[68] = abb[40] + -abb[42];
z[68] = abb[17] * z[68];
z[41] = abb[20] * z[41];
z[39] = z[39] + z[41] + z[68];
z[39] = abb[57] * z[39];
z[6] = abb[61] + z[6] + z[10] + z[11] + z[14] + z[16] + z[18] + z[19] + z[26] + z[27] + (T(1) / T(16)) * z[28] + (T(1) / T(32)) * z[39] + z[43] + z[59] + z[62];
z[10] = abb[30] * (T(1) / T(4));
z[11] = abb[32] + z[5];
z[1] = -z[1] + -z[10] + z[11];
z[1] = abb[30] * z[1];
z[14] = abb[32] + z[38];
z[16] = z[2] * z[14];
z[18] = abb[29] * (T(1) / T(4));
z[0] = -abb[31] + abb[32] * (T(-1) / T(2)) + z[0] + -z[18];
z[0] = abb[29] * z[0];
z[19] = abb[59] * (T(1) / T(2));
z[26] = abb[33] * z[30];
z[27] = prod_pow(abb[32], 2);
z[28] = abb[31] * (T(1) / T(4));
z[38] = -abb[32] + -z[28];
z[38] = abb[31] * z[38];
z[0] = abb[38] * (T(-1) / T(2)) + z[0] + z[1] + -z[16] + z[19] + (T(-3) / T(2)) * z[26] + (T(-1) / T(4)) * z[27] + z[38];
z[0] = abb[5] * z[0];
z[1] = abb[29] * (T(-3) / T(4)) + z[8] + z[10] + -z[28];
z[1] = abb[30] * z[1];
z[38] = abb[35] * (T(1) / T(2));
z[16] = z[16] + z[38];
z[39] = abb[31] * z[11];
z[41] = (T(1) / T(2)) * z[27];
z[39] = z[39] + z[41];
z[43] = -abb[38] + z[39];
z[59] = abb[32] + abb[31] * (T(3) / T(2));
z[59] = -abb[33] + (T(1) / T(2)) * z[59];
z[59] = abb[29] * z[59];
z[1] = z[1] + z[16] + z[26] + (T(1) / T(2)) * z[43] + z[59];
z[1] = abb[3] * z[1];
z[43] = abb[29] * z[15];
z[43] = abb[35] + z[43];
z[62] = abb[31] * abb[32];
z[68] = z[26] + z[62];
z[46] = z[4] + -z[46];
z[46] = abb[30] * z[46];
z[69] = abb[34] * z[3];
z[70] = prod_pow(m1_set::bc<T>[0], 2);
z[71] = (T(1) / T(6)) * z[70];
z[72] = abb[59] + -z[43] + -z[46] + -z[68] + z[69] + z[71];
z[72] = abb[0] * z[72];
z[15] = abb[30] * z[15];
z[73] = abb[33] * (T(1) / T(2));
z[74] = -abb[31] + z[73];
z[74] = abb[33] * z[74];
z[75] = prod_pow(abb[31], 2);
z[43] = -z[15] + z[43] + z[74] + (T(1) / T(2)) * z[75];
z[43] = abb[14] * z[43];
z[74] = -abb[32] + z[5];
z[76] = abb[31] * z[74];
z[73] = -abb[32] + z[73];
z[73] = abb[33] * z[73];
z[76] = abb[59] + -z[73] + z[76];
z[76] = abb[4] * z[76];
z[77] = abb[39] * (T(1) / T(2));
z[78] = abb[19] * z[77];
z[43] = z[43] + -z[76] + -z[78];
z[27] = z[27] + z[75];
z[27] = (T(1) / T(2)) * z[27];
z[75] = z[26] + z[27];
z[45] = abb[29] * z[45];
z[45] = z[45] + z[75];
z[46] = z[45] + z[46];
z[76] = -abb[38] + abb[59];
z[78] = -z[46] + -z[76];
z[78] = abb[8] * z[78];
z[79] = abb[8] * abb[58];
z[78] = -z[43] + z[78] + z[79];
z[80] = -abb[32] + z[4];
z[80] = abb[30] * z[80];
z[81] = abb[60] + z[71];
z[80] = abb[37] + z[69] + -z[80] + z[81];
z[23] = z[23] * z[80];
z[80] = abb[3] + -abb[5];
z[82] = abb[15] + z[80];
z[82] = -abb[0] + (T(1) / T(2)) * z[82];
z[82] = abb[37] * z[82];
z[23] = z[23] + -z[82];
z[82] = abb[29] * z[12];
z[68] = -z[68] + z[82];
z[82] = abb[30] * z[9];
z[14] = abb[34] * z[14];
z[82] = abb[58] + -abb[59] + z[14] + -z[68] + z[82];
z[83] = abb[35] + z[82];
z[7] = z[7] * z[83];
z[84] = abb[39] * (T(1) / T(4));
z[85] = abb[18] * z[84];
z[86] = -abb[58] + z[76];
z[87] = abb[12] * z[86];
z[85] = z[85] + z[87];
z[88] = -abb[8] + z[80];
z[89] = (T(1) / T(12)) * z[70];
z[90] = -z[88] * z[89];
z[91] = abb[60] * (T(1) / T(2));
z[20] = -z[20] * z[91];
z[92] = -abb[4] + abb[14];
z[93] = z[88] + -z[92];
z[94] = abb[36] * (T(1) / T(2));
z[93] = z[93] * z[94];
z[84] = abb[17] * z[84];
z[0] = z[0] + z[1] + z[7] + z[20] + z[23] + z[72] + (T(1) / T(2)) * z[78] + -z[84] + z[85] + z[90] + z[93];
z[1] = z[10] + -z[33];
z[1] = abb[30] * z[1];
z[7] = -abb[33] + z[18];
z[20] = z[7] + z[25];
z[20] = abb[29] * z[20];
z[25] = abb[38] + z[39];
z[25] = (T(1) / T(2)) * z[25] + z[26];
z[1] = z[1] + -z[19] + z[20] + z[25] + -z[89] + z[94];
z[1] = abb[1] * z[1];
z[20] = abb[30] * z[37];
z[33] = abb[29] * z[36];
z[20] = z[20] + z[33];
z[37] = -z[2] + z[37];
z[39] = abb[34] * z[37];
z[20] = -abb[58] + (T(1) / T(2)) * z[20] + -z[39];
z[72] = z[20] + -z[81];
z[35] = z[35] * z[72];
z[78] = z[1] + z[35];
z[90] = -abb[32] + z[31];
z[90] = abb[29] * z[90];
z[73] = -z[41] + -z[73] + z[90];
z[90] = -abb[37] + -abb[38] + -z[69] + z[73];
z[90] = abb[35] + (T(1) / T(2)) * z[90];
z[90] = abb[9] * z[90];
z[0] = (T(1) / T(2)) * z[0] + z[78] + z[90];
z[93] = abb[20] * abb[39];
z[44] = abb[60] * z[44];
z[0] = (T(1) / T(8)) * z[0] + z[44] + (T(-1) / T(64)) * z[93];
z[0] = abb[55] * z[0];
z[18] = -z[8] + z[18];
z[44] = abb[31] * (T(3) / T(4));
z[94] = abb[30] * (T(3) / T(4));
z[95] = z[18] + z[44] + -z[94];
z[95] = abb[30] * z[95];
z[18] = z[18] + z[28];
z[18] = abb[29] * z[18];
z[28] = z[75] + -z[76];
z[75] = abb[58] * (T(1) / T(2));
z[18] = z[18] + z[28] + z[75];
z[37] = z[2] * z[37];
z[76] = -abb[60] + z[18] + -z[37] + (T(-1) / T(4)) * z[70] + -z[95];
z[76] = abb[0] * z[76];
z[30] = z[30] + z[94];
z[30] = abb[30] * z[30];
z[30] = z[30] + -z[37] + z[45];
z[30] = abb[3] * z[30];
z[8] = -z[8] + z[31];
z[8] = abb[29] * z[8];
z[8] = z[8] + z[28];
z[17] = abb[30] * z[17];
z[17] = z[8] + z[17];
z[28] = abb[5] * z[17];
z[37] = abb[3] * (T(-1) / T(2)) + abb[5] * (T(1) / T(3));
z[70] = (T(1) / T(2)) * z[70];
z[37] = z[37] * z[70];
z[52] = z[52] * z[91];
z[70] = abb[30] * z[36];
z[33] = -z[33] + z[70];
z[70] = abb[8] * (T(1) / T(2));
z[70] = z[33] * z[70];
z[70] = z[70] + z[79];
z[79] = abb[15] * z[86];
z[79] = z[79] + -z[87];
z[80] = -abb[0] + z[80];
z[80] = abb[36] * z[80];
z[87] = abb[18] * z[77];
z[28] = z[28] + -z[30] + -z[37] + z[52] + (T(-1) / T(2)) * z[70] + z[76] + z[79] + -z[80] + -z[84] + -z[87];
z[30] = z[4] + z[36];
z[30] = abb[30] * z[30];
z[30] = z[30] + -z[39];
z[36] = z[30] + -z[71];
z[37] = -abb[60] + (T(1) / T(2)) * z[36];
z[37] = abb[13] * z[37];
z[37] = z[37] + (T(1) / T(8)) * z[93];
z[39] = abb[30] * z[12];
z[39] = z[39] + -z[68];
z[49] = z[39] * z[49];
z[28] = (T(1) / T(2)) * z[28] + z[37] + z[49] + -z[78];
z[28] = z[28] * z[47];
z[47] = -z[12] * z[31];
z[9] = z[4] * z[9];
z[49] = abb[32] * z[5];
z[9] = -abb[38] + z[9] + z[16] + z[19] + (T(1) / T(2)) * z[26] + z[47] + z[49] + -z[75];
z[9] = abb[15] * z[9];
z[16] = abb[8] * z[17];
z[12] = z[4] + z[12];
z[12] = abb[30] * z[12];
z[8] = z[8] + z[12] + -z[14];
z[8] = abb[5] * z[8];
z[8] = z[8] + z[16] + -z[43];
z[7] = z[7] + z[44];
z[12] = abb[30] * z[7];
z[3] = -z[2] * z[3];
z[3] = z[3] + z[12] + -z[25] + z[38] + -z[59];
z[3] = abb[3] * z[3];
z[12] = abb[34] * (T(3) / T(4)) + -z[4] + -z[31] + z[74];
z[12] = abb[34] * z[12];
z[7] = -z[7] + z[10];
z[7] = abb[30] * z[7];
z[7] = z[7] + z[12] + z[18] + -z[89];
z[7] = abb[0] * z[7];
z[10] = -z[32] * z[89];
z[12] = -z[88] + -z[92];
z[12] = abb[0] + (T(1) / T(2)) * z[12];
z[12] = abb[36] * z[12];
z[14] = -abb[5] * z[91];
z[3] = z[3] + z[7] + (T(1) / T(2)) * z[8] + z[9] + z[10] + z[12] + z[14] + z[23] + -z[85];
z[1] = -z[1] + (T(1) / T(2)) * z[3];
z[3] = z[13] * z[39];
z[1] = (T(1) / T(8)) * z[1] + z[3];
z[1] = abb[54] * z[1];
z[3] = -abb[31] + abb[32];
z[3] = abb[49] * z[3];
z[3] = z[3] + z[48] + z[56] + -z[67];
z[3] = abb[34] * z[3];
z[7] = z[42] * z[82];
z[8] = abb[35] * z[57];
z[9] = abb[37] * z[57];
z[8] = z[8] + -z[9];
z[10] = abb[49] * z[21];
z[10] = z[10] + -z[50];
z[10] = abb[33] * z[10];
z[12] = abb[49] * abb[59];
z[13] = abb[49] * abb[58];
z[12] = z[12] + -z[13];
z[14] = -z[50] + z[64];
z[14] = abb[29] * z[14];
z[16] = -abb[30] * z[55];
z[17] = abb[49] * z[62];
z[3] = z[3] + z[7] + z[8] + -z[10] + -z[12] + z[14] + z[16] + z[17];
z[3] = z[3] * z[51];
z[7] = z[69] + -z[86];
z[14] = -z[7] + z[73];
z[14] = z[14] * z[42];
z[2] = abb[49] * z[2];
z[16] = z[2] + -z[54];
z[16] = abb[34] * z[16];
z[17] = abb[38] * abb[49];
z[12] = -z[12] + z[17];
z[17] = (T(-1) / T(2)) * z[50] + z[54];
z[17] = abb[33] * z[17];
z[18] = abb[49] * z[31];
z[19] = z[18] + -z[54];
z[19] = abb[29] * z[19];
z[21] = -abb[49] * z[41];
z[8] = z[8] + -z[12] + z[14] + -z[16] + z[17] + z[19] + z[21];
z[8] = z[8] * z[53];
z[14] = z[46] + -z[86];
z[17] = -abb[36] + z[81];
z[19] = z[14] + -z[17];
z[19] = -z[19] * z[22];
z[21] = -abb[37] + z[83];
z[21] = abb[25] * z[21];
z[22] = abb[26] * z[72];
z[23] = abb[37] + z[7];
z[25] = -abb[35] + z[23] + -z[73];
z[26] = -abb[22] * z[25];
z[31] = abb[27] * z[77];
z[19] = z[19] + z[21] + -z[22] + z[26] + z[31];
z[15] = z[15] + -z[45];
z[21] = -abb[36] + z[15] + -z[23];
z[22] = abb[24] * z[21];
z[22] = z[19] + z[22];
z[22] = -z[22] * z[29];
z[19] = -z[19] * z[34];
z[23] = abb[49] * z[27];
z[11] = abb[49] * z[11];
z[11] = z[11] + -z[50];
z[26] = abb[29] * z[11];
z[10] = -z[10] + z[12] + z[23] + z[26];
z[7] = -z[7] + z[15];
z[7] = z[7] * z[42];
z[5] = abb[49] * z[5];
z[5] = z[5] + z[18] + -z[50];
z[5] = abb[30] * z[5];
z[12] = -abb[36] * z[57];
z[5] = z[5] + z[7] + -z[9] + -z[10] + z[12] + -z[16];
z[7] = -abb[44] * z[21];
z[5] = (T(1) / T(2)) * z[5] + z[7];
z[5] = z[5] * z[58];
z[4] = abb[49] * z[4];
z[4] = z[4] + -z[11] + -z[18];
z[4] = abb[30] * z[4];
z[7] = z[14] * z[42];
z[9] = z[17] * z[57];
z[4] = -z[4] + -z[7] + z[9] + -z[10];
z[4] = z[4] * z[61];
z[7] = -z[20] * z[42];
z[9] = abb[29] * z[65];
z[10] = abb[30] * z[66];
z[9] = z[9] + z[10];
z[10] = z[57] * z[81];
z[2] = -z[2] + -z[66];
z[2] = abb[34] * z[2];
z[2] = z[2] + z[7] + (T(1) / T(2)) * z[9] + z[10] + z[13];
z[2] = z[2] * z[60];
z[7] = -abb[3] * z[36];
z[9] = abb[60] * z[40];
z[7] = z[7] + z[9] + -z[70];
z[9] = z[20] + -z[71];
z[9] = -abb[60] + (T(1) / T(2)) * z[9];
z[9] = abb[0] * z[9];
z[7] = (T(1) / T(2)) * z[7] + z[9] + -z[84];
z[9] = abb[2] * z[91];
z[7] = (T(1) / T(2)) * z[7] + z[9] + -z[35] + z[37];
z[7] = z[7] * z[24];
z[9] = -z[30] + z[81];
z[9] = abb[18] * z[9];
z[10] = abb[20] * z[72];
z[11] = -abb[58] + abb[60] + (T(-1) / T(2)) * z[33];
z[11] = abb[17] * z[11];
z[12] = -abb[0] + -abb[8];
z[12] = abb[39] * z[12];
z[13] = -abb[16] * z[77];
z[9] = z[9] + z[10] + z[11] + (T(1) / T(2)) * z[12] + z[13];
z[10] = abb[28] * abb[39];
z[9] = (T(1) / T(2)) * z[9] + z[10];
z[9] = abb[57] * z[9];
z[10] = abb[0] * z[25];
z[10] = z[10] + z[79];
z[10] = (T(1) / T(2)) * z[10] + z[90];
z[10] = abb[52] * z[10];
z[11] = z[25] * z[63];
z[12] = abb[27] * abb[39] * z[57];
z[0] = abb[43] + z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[7] + z[8] + (T(1) / T(16)) * z[9] + (T(1) / T(8)) * z[10] + z[11] + (T(1) / T(64)) * z[12] + z[19] + z[22] + z[28];
return {z[6], z[0]};
}


template <typename T> std::complex<T> f_4_446_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.0427802290817057885374239026824539953270740463452897187726221166"),stof<T>("0.26794842810340937909538814468734169242255837066297445670922101667")}, std::complex<T>{stof<T>("-0.00650108883169777177674601924065865208061292450316619317647578402"),stof<T>("0.14862968680540993627472910161979133818577915019246332137552000078")}, std::complex<T>{stof<T>("0.0427802290817057885374239026824539953270740463452897187726221166"),stof<T>("0.26794842810340937909538814468734169242255837066297445670922101667")}, std::complex<T>{stof<T>("-0.036288007742234619060353596686772714690581482826142804913541126494"),stof<T>("0.081650782474119265597042913007643687471640219311293356043670406472")}, std::complex<T>{stof<T>("-0.11233733165981908115865460940883609131886566373610456584169226952"),stof<T>("0.13217945716397632975053233715317979298955879724657241970870660756")}, std::complex<T>{stof<T>("-0.016435191501797186144418708983212813151046729127317117556625088764"),stof<T>("0.030373567780978282109358666898988592548664251477348641537935749809")}, std::complex<T>{stof<T>("-0.12593375930407590941161915200899992428250791545343682933411608509"),stof<T>("0.13801363516870842049040280265772312585990829028334043153424807411")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_446_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_446_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_446_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(512)) * (v[1] + v[2] + -v[3] + -v[4]) * (-2 * (8 * v[0] + 7 * v[1] + 7 * v[2] + -3 * v[3] + -7 * v[4]) + m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]))) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[2] * (T(3) / T(64)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[52] + abb[54] + abb[55]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[52] + abb[54] + abb[55];
z[1] = -abb[35] + abb[38];
return abb[11] * (T(1) / T(16)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_446_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.00897161861057647797021460969078765113333079215822420734776380894"),stof<T>("0.21201960730796774152001785807014792714958978032196034346572313048")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,62> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W18(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W25(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_14(k), f_2_20(k), f_2_23(k), f_2_2_im(k), f_2_10_im(k), f_2_12_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_2_re(k), f_2_10_re(k), f_2_12_re(k), T{0}};
abb[43] = SpDLog_f_4_446_W_20_Im(t, path, abb);
abb[61] = SpDLog_f_4_446_W_20_Re(t, path, abb);

                    
            return f_4_446_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_446_DLogXconstant_part(base_point<T>, kend);
	value += f_4_446_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_446_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_446_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_446_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_446_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_446_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_446_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
