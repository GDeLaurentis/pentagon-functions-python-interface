/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_1024.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_1024_abbreviated (const std::array<T,85>& abb) {
T z[116];
z[0] = 2 * abb[65];
z[1] = 2 * abb[64];
z[2] = -abb[66] + abb[67];
z[3] = z[0] + z[1] + z[2];
z[3] = abb[0] * z[3];
z[4] = abb[65] + abb[66];
z[5] = abb[12] * z[4];
z[6] = abb[64] + abb[67];
z[7] = abb[13] * z[6];
z[8] = z[5] + z[7];
z[9] = abb[64] + abb[66];
z[10] = abb[1] * z[9];
z[11] = z[8] + -z[10];
z[12] = abb[64] + abb[65];
z[13] = abb[67] + z[12];
z[13] = abb[16] * z[13];
z[14] = -z[11] + 4 * z[13];
z[15] = -abb[65] + abb[67];
z[16] = -abb[3] * z[15];
z[17] = -abb[64] + abb[67];
z[18] = abb[14] * z[17];
z[19] = z[16] + z[18];
z[20] = abb[66] + abb[67];
z[21] = -z[12] + z[20];
z[22] = abb[7] * z[21];
z[23] = abb[65] + -abb[66];
z[24] = abb[11] * z[23];
z[25] = z[22] + -z[24];
z[26] = z[12] + z[20];
z[27] = abb[8] * z[26];
z[28] = 2 * abb[67];
z[29] = 5 * abb[65];
z[30] = -abb[64] + -z[28] + -z[29];
z[30] = abb[10] * z[30];
z[31] = abb[26] + abb[28];
z[32] = abb[27] + abb[29];
z[33] = z[31] + z[32];
z[33] = abb[68] * z[33];
z[34] = abb[65] + abb[67];
z[35] = abb[4] * z[34];
z[36] = 3 * z[35];
z[37] = -abb[64] + abb[66];
z[38] = abb[2] * z[37];
z[39] = 3 * z[38];
z[40] = -abb[66] + z[12];
z[41] = abb[15] * z[40];
z[42] = 4 * z[41];
z[43] = 5 * abb[64];
z[44] = -abb[65] + 2 * abb[66] + -z[43];
z[44] = abb[5] * z[44];
z[30] = 2 * z[3] + z[14] + z[19] + z[25] + -z[27] + z[30] + z[33] + -z[36] + z[39] + z[42] + z[44];
z[30] = abb[35] * z[30];
z[33] = z[16] + -z[24];
z[44] = z[1] + -z[20];
z[44] = abb[5] * z[44];
z[45] = 2 * z[41];
z[46] = 2 * z[38];
z[47] = abb[10] * z[0];
z[44] = -z[3] + z[5] + -z[33] + z[35] + z[44] + -z[45] + -z[46] + z[47];
z[44] = abb[36] * z[44];
z[47] = z[34] + z[37];
z[48] = abb[18] * z[47];
z[49] = z[27] + -z[48];
z[50] = z[2] + z[12];
z[51] = abb[6] * z[50];
z[52] = z[2] + -z[12];
z[53] = abb[9] * z[52];
z[51] = z[51] + -z[53];
z[53] = (T(-1) / T(2)) * z[51];
z[54] = (T(1) / T(2)) * z[31];
z[55] = abb[30] + z[32];
z[56] = z[54] + z[55];
z[56] = abb[68] * z[56];
z[57] = -z[49] + -z[53] + z[56];
z[58] = 3 * abb[64];
z[59] = 3 * abb[65];
z[60] = z[2] + z[58] + z[59];
z[60] = abb[0] * z[60];
z[61] = 7 * abb[65] + 5 * abb[67] + z[9];
z[62] = abb[10] * (T(1) / T(2));
z[61] = z[61] * z[62];
z[63] = abb[66] + z[34];
z[43] = -z[43] + z[63];
z[64] = abb[5] * (T(1) / T(2));
z[43] = z[43] * z[64];
z[65] = 2 * z[18];
z[14] = z[14] + -5 * z[35] + z[43] + z[57] + z[60] + -z[61] + z[65];
z[43] = -abb[34] * z[14];
z[61] = z[34] + -2 * z[37];
z[61] = abb[5] * z[61];
z[66] = abb[0] * z[12];
z[61] = z[8] + z[18] + z[61] + -z[66];
z[67] = abb[68] * z[55];
z[67] = z[51] + z[67];
z[68] = 4 * z[35];
z[69] = z[67] + -z[68];
z[70] = -z[6] + z[23];
z[71] = abb[17] * z[70];
z[72] = abb[25] * abb[68];
z[71] = z[71] + z[72];
z[72] = 2 * z[24];
z[73] = -z[71] + z[72];
z[74] = 2 * abb[10];
z[75] = -z[15] * z[74];
z[76] = 4 * z[38];
z[61] = -z[42] + z[48] + 2 * z[61] + z[69] + -z[73] + z[75] + -z[76];
z[61] = abb[32] * z[61];
z[75] = z[22] + -z[48];
z[77] = abb[67] + z[37];
z[78] = -z[59] + z[77];
z[79] = z[62] * z[78];
z[79] = -z[75] + z[79];
z[80] = abb[0] * z[50];
z[81] = z[53] + z[80];
z[82] = z[18] + z[38];
z[83] = 3 * abb[67];
z[84] = -abb[64] + z[83];
z[85] = z[23] + z[84];
z[86] = z[64] * z[85];
z[87] = abb[30] + z[54];
z[87] = abb[68] * z[87];
z[88] = 2 * z[35];
z[86] = z[33] + z[79] + z[81] + -z[82] + z[86] + z[87] + -z[88];
z[86] = abb[37] * z[86];
z[89] = z[27] + z[71];
z[90] = abb[66] + z[6] + z[59];
z[91] = z[62] * z[90];
z[92] = z[7] + z[10];
z[93] = abb[68] * z[31];
z[94] = (T(1) / T(2)) * z[93];
z[95] = z[92] + z[94];
z[91] = -z[89] + z[91] + -z[95];
z[96] = abb[0] * z[52];
z[97] = -z[53] + z[96];
z[98] = z[5] + -z[35];
z[99] = -abb[65] + z[83];
z[100] = -z[37] + z[99];
z[101] = -z[64] * z[100];
z[102] = 2 * z[16];
z[101] = -z[91] + -z[97] + -z[98] + z[101] + -z[102];
z[101] = abb[38] * z[101];
z[103] = z[32] + z[54];
z[103] = abb[68] * z[103];
z[23] = z[23] + z[58];
z[104] = -abb[67] + z[23];
z[105] = z[64] * z[104];
z[25] = -z[16] + z[25];
z[103] = z[25] + z[71] + z[103] + z[105];
z[100] = z[62] * z[100];
z[105] = -z[18] + z[38];
z[106] = 2 * z[7];
z[100] = z[100] + z[105] + -z[106];
z[107] = -z[81] + -z[100] + z[103];
z[108] = -abb[40] * z[107];
z[30] = z[30] + z[43] + z[44] + z[61] + z[86] + z[101] + z[108];
z[30] = abb[36] * z[30];
z[43] = -z[18] + z[35];
z[44] = z[5] + z[27];
z[61] = z[31] + -z[32];
z[61] = abb[68] * z[61];
z[86] = 2 * z[66];
z[101] = -abb[65] + -z[58];
z[101] = abb[5] * z[101];
z[108] = -abb[64] + -z[59];
z[108] = abb[10] * z[108];
z[25] = -z[25] + -z[38] + z[43] + z[44] + -2 * z[51] + z[61] + z[86] + z[92] + z[101] + z[108];
z[25] = abb[37] * z[25];
z[61] = 3 * abb[66];
z[101] = abb[64] + z[61];
z[108] = -z[34] + z[101];
z[109] = z[62] * z[108];
z[110] = 2 * z[10];
z[97] = -z[97] + -z[103] + -z[105] + z[109] + -z[110];
z[97] = abb[35] * z[97];
z[85] = z[62] * z[85];
z[85] = -z[65] + z[85] + z[96];
z[58] = z[58] + z[63];
z[103] = z[58] * z[64];
z[103] = -z[35] + z[103];
z[11] = -z[11] + z[57] + z[85] + z[103];
z[11] = abb[34] * z[11];
z[57] = -z[62] + z[64];
z[105] = -z[57] * z[70];
z[109] = z[24] + z[53];
z[111] = z[16] + -z[66];
z[95] = z[95] + z[105] + z[109] + z[111];
z[95] = abb[38] * z[95];
z[105] = z[5] + -z[18];
z[112] = z[7] + z[24] + -z[105];
z[113] = abb[5] * z[9];
z[114] = -abb[10] * z[15];
z[114] = -z[80] + z[112] + z[113] + z[114];
z[115] = 2 * abb[32];
z[114] = z[114] * z[115];
z[11] = z[11] + z[25] + z[95] + z[97] + z[114];
z[11] = abb[38] * z[11];
z[25] = -z[7] + z[10];
z[6] = abb[65] + -z[6] + z[61];
z[95] = -z[6] * z[62];
z[44] = z[25] + z[44] + z[46] + -z[48] + -z[56] + z[81] + z[95] + -z[103];
z[44] = abb[35] * z[44];
z[56] = (T(-3) / T(2)) * z[51];
z[95] = z[59] + z[83] + z[101];
z[95] = z[64] * z[95];
z[97] = -z[54] + z[55];
z[97] = abb[68] * z[97];
z[36] = -3 * z[5] + -z[25] + -z[36] + -z[49] + -z[56] + -z[85] + z[95] + z[97];
z[36] = abb[34] * z[36];
z[57] = -z[47] * z[57];
z[85] = -z[5] + z[38];
z[95] = z[18] + z[66];
z[97] = -z[35] + z[95];
z[53] = z[53] + z[57] + -z[85] + z[94] + -z[97];
z[53] = abb[37] * z[53];
z[57] = abb[5] * z[37];
z[94] = abb[10] * z[34];
z[101] = -z[57] + z[94] + z[96] + -z[112];
z[101] = z[101] * z[115];
z[36] = z[36] + z[44] + z[53] + z[101];
z[36] = abb[37] * z[36];
z[6] = z[6] * z[64];
z[6] = z[6] + -z[96];
z[44] = 2 * z[5];
z[53] = z[44] + -z[75];
z[96] = -3 * z[37] + -z[99];
z[96] = z[62] * z[96];
z[99] = -abb[30] + (T(-3) / T(2)) * z[31];
z[99] = abb[68] * z[99];
z[33] = z[6] + 3 * z[18] + -z[33] + z[39] + -z[53] + -z[56] + z[96] + z[99];
z[33] = abb[37] * z[33];
z[19] = z[19] + -z[87] + -z[109];
z[15] = -7 * abb[64] + 5 * abb[66] + z[15];
z[15] = z[15] * z[64];
z[29] = z[29] + z[77];
z[29] = z[29] * z[62];
z[15] = -z[15] + -z[19] + z[29] + -5 * z[38] + -z[42] + z[53] + -z[60];
z[29] = -abb[35] + abb[36];
z[15] = -z[15] * z[29];
z[39] = z[51] + z[65];
z[42] = abb[30] + z[31];
z[42] = abb[68] * z[42];
z[42] = -z[39] + z[42] + -z[75];
z[53] = abb[10] * z[47];
z[45] = z[42] + z[44] + -z[45] + z[53] + -2 * z[57] + -z[76] + -z[86];
z[45] = abb[39] * z[45];
z[6] = -z[6] + z[19] + z[38] + z[44] + -z[79];
z[6] = abb[38] * z[6];
z[19] = z[66] + z[85];
z[53] = abb[10] * abb[65];
z[57] = z[19] + z[41] + -z[53] + z[57];
z[57] = abb[32] * z[57];
z[6] = z[6] + z[15] + z[33] + z[45] + 4 * z[57];
z[6] = abb[39] * z[6];
z[15] = 2 * z[31] + z[55];
z[15] = abb[68] * z[15];
z[15] = z[15] + z[48] + -z[51];
z[31] = abb[5] * abb[64];
z[18] = z[15] + z[18] + z[31] + z[71];
z[10] = z[10] + -z[38];
z[33] = z[1] + z[34];
z[33] = abb[20] * z[33];
z[38] = abb[65] + -z[1];
z[38] = abb[67] + (T(1) / T(3)) * z[38];
z[38] = abb[10] * z[38];
z[10] = (T(-2) / T(3)) * z[10] + (T(-7) / T(3)) * z[13] + (T(-1) / T(3)) * z[18] + (T(4) / T(3)) * z[33] + z[35] + z[38] + -z[66] + -z[106];
z[10] = prod_pow(m1_set::bc<T>[0], 2) * z[10];
z[18] = abb[5] * z[26];
z[35] = -z[18] + z[44] + z[49] + -z[67] + z[88];
z[35] = abb[43] * z[35];
z[38] = abb[5] * z[21];
z[44] = abb[68] * z[32];
z[44] = z[22] + z[44];
z[38] = z[38] + -z[44] + -z[51] + z[73] + z[102];
z[45] = abb[44] * z[38];
z[37] = z[0] + -z[37];
z[48] = abb[19] * z[37];
z[55] = -z[24] + z[48];
z[4] = -abb[5] * z[4];
z[4] = z[4] + -z[19] + z[55];
z[4] = abb[47] * z[4];
z[4] = z[4] + z[10] + z[35] + z[45];
z[10] = z[56] + -z[80];
z[19] = z[59] + -z[61] + -z[84];
z[19] = z[19] * z[64];
z[16] = z[16] + z[24];
z[32] = z[32] + -z[54];
z[32] = abb[68] * z[32];
z[16] = -z[10] + -3 * z[16] + z[19] + z[22] + z[32] + z[71] + z[100];
z[16] = abb[38] * z[16];
z[19] = abb[35] + abb[37];
z[19] = z[19] * z[107];
z[22] = abb[40] * z[38];
z[16] = z[16] + z[19] + z[22];
z[16] = abb[40] * z[16];
z[14] = abb[35] * z[14];
z[5] = -z[5] + z[95];
z[5] = 2 * z[5];
z[19] = -abb[5] * z[47];
z[22] = z[34] * z[74];
z[32] = 2 * z[13];
z[19] = -z[5] + z[19] + z[22] + -z[32] + z[49] + -z[69];
z[19] = abb[34] * z[19];
z[14] = z[14] + z[19];
z[14] = abb[34] * z[14];
z[19] = z[64] * z[108];
z[19] = z[19] + z[72] + -z[98];
z[9] = abb[65] + 3 * z[9] + z[83];
z[9] = z[9] * z[62];
z[9] = z[9] + -z[10] + -z[19] + -z[89] + -3 * z[92] + (T(-3) / T(2)) * z[93];
z[9] = abb[38] * z[9];
z[10] = z[19] + -z[81] + z[91];
z[19] = abb[37] + z[29];
z[10] = z[10] * z[19];
z[19] = -z[51] + z[93];
z[22] = abb[10] * z[26];
z[22] = z[19] + -z[22] + z[89] + 2 * z[92];
z[26] = abb[33] * z[22];
z[9] = z[9] + z[10] + z[26];
z[9] = abb[33] * z[9];
z[10] = -z[32] + z[88];
z[0] = z[0] + z[20];
z[0] = abb[10] * z[0];
z[1] = abb[5] * z[1];
z[0] = z[0] + z[1] + -z[3] + z[10] + -z[25] + -z[82];
z[0] = prod_pow(abb[35], 2) * z[0];
z[1] = z[15] + -z[76];
z[3] = -z[8] + -z[32] + -z[95] + z[113];
z[8] = z[28] + z[37];
z[8] = z[8] * z[74];
z[3] = -z[1] + 2 * z[3] + z[8] + z[68] + z[73];
z[3] = abb[35] * z[3];
z[8] = z[19] + z[88];
z[20] = abb[10] * z[21];
z[20] = z[20] + -z[46];
z[18] = z[8] + -z[18] + z[20] + 2 * z[105];
z[18] = abb[32] * z[18];
z[21] = -z[13] + z[31] + z[94] + -z[97];
z[25] = abb[34] * z[21];
z[3] = z[3] + z[18] + -4 * z[25];
z[3] = abb[32] * z[3];
z[18] = -abb[5] + abb[10];
z[26] = z[18] * z[70];
z[19] = z[19] + z[26] + z[72] + z[106] + z[110] + 2 * z[111];
z[19] = abb[45] * z[19];
z[22] = 2 * z[22];
z[26] = -abb[71] * z[22];
z[28] = abb[5] * z[12];
z[28] = z[28] + z[41] + z[53] + -z[55];
z[28] = abb[42] * z[28];
z[29] = abb[41] + -abb[47];
z[29] = z[29] * z[41];
z[28] = z[28] + z[29];
z[29] = abb[5] * abb[65];
z[24] = z[24] + z[29] + -z[48] + z[66];
z[24] = 4 * z[24];
z[24] = abb[41] * z[24];
z[18] = z[18] * z[47];
z[5] = -z[5] + z[8] + z[18] + -z[46];
z[5] = abb[46] * z[5];
z[8] = z[20] + z[42];
z[8] = 2 * z[8];
z[18] = -abb[72] * z[8];
z[20] = abb[22] * z[50];
z[29] = abb[21] * z[52];
z[34] = abb[23] * z[70];
z[35] = -abb[64] + z[63];
z[35] = abb[24] * z[35];
z[20] = -z[20] + z[29] + z[34] + z[35];
z[29] = -abb[31] * abb[68];
z[29] = -z[20] + z[29];
z[29] = abb[74] * z[29];
z[7] = -z[7] + z[33];
z[17] = abb[10] * z[17];
z[17] = -z[7] + -z[17] + z[32] + z[97];
z[17] = 2 * z[17];
z[32] = -abb[73] * z[17];
z[33] = abb[80] * z[78];
z[34] = abb[77] * z[104];
z[35] = abb[62] + abb[63];
z[35] = abb[68] * z[35];
z[33] = z[33] + -z[34] + -z[35];
z[7] = z[7] + -z[13];
z[34] = abb[10] * abb[64];
z[34] = z[7] + -z[34] + -z[66];
z[34] = 4 * z[34];
z[35] = abb[69] * z[34];
z[37] = abb[10] * z[12];
z[7] = z[7] + -z[31] + -z[37];
z[7] = 4 * z[7];
z[31] = abb[70] * z[7];
z[37] = abb[76] + abb[79];
z[37] = (T(1) / T(2)) * z[37];
z[37] = z[37] * z[50];
z[38] = abb[78] + abb[81];
z[38] = (T(-1) / T(2)) * z[38];
z[38] = z[38] * z[52];
z[41] = (T(1) / T(2)) * z[90];
z[42] = abb[82] * z[41];
z[45] = (T(1) / T(2)) * z[58];
z[46] = abb[75] * z[45];
z[0] = z[0] + z[3] + 2 * z[4] + z[5] + z[6] + z[9] + z[11] + z[14] + z[16] + z[18] + z[19] + z[24] + z[26] + 4 * z[28] + z[29] + z[30] + z[31] + z[32] + (T(-1) / T(2)) * z[33] + z[35] + z[36] + z[37] + z[38] + z[42] + z[46];
z[3] = -abb[35] * z[21];
z[4] = -abb[10] * abb[67];
z[4] = z[4] + z[13] + -z[43];
z[4] = abb[32] * z[4];
z[3] = z[3] + z[4] + z[25];
z[2] = abb[0] * z[2];
z[4] = -z[2] + z[106];
z[5] = -z[50] * z[74];
z[1] = z[1] + 2 * z[4] + z[5] + z[71];
z[1] = abb[37] * z[1];
z[2] = z[2] + -z[65] + z[110];
z[4] = z[52] * z[74];
z[2] = 2 * z[2] + z[4] + z[15] + z[71];
z[2] = abb[38] * z[2];
z[4] = abb[67] * z[74];
z[4] = z[4] + z[10] + z[27] + -z[39] + -z[44];
z[4] = abb[36] * z[4];
z[5] = -abb[33] * z[22];
z[6] = -abb[39] * z[8];
z[1] = z[1] + z[2] + 4 * z[3] + 2 * z[4] + z[5] + z[6];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[55] + abb[58];
z[3] = z[2] * z[40];
z[4] = abb[56] * z[23];
z[5] = -abb[57] * z[52];
z[6] = -abb[59] * z[78];
z[9] = abb[66] + z[12];
z[9] = abb[60] * z[9];
z[2] = -abb[56] + -abb[60] + z[2];
z[2] = abb[67] * z[2];
z[2] = z[2] + z[3] + z[4] + z[5] + z[6] + z[9];
z[3] = -abb[50] * z[22];
z[4] = -abb[53] * z[20];
z[5] = -abb[51] * z[8];
z[6] = -abb[52] * z[17];
z[8] = abb[48] * z[34];
z[7] = abb[49] * z[7];
z[9] = abb[83] + abb[84];
z[10] = -abb[31] * abb[53];
z[9] = (T(-1) / T(2)) * z[9] + z[10];
z[9] = abb[68] * z[9];
z[10] = abb[54] * z[45];
z[11] = abb[61] * z[41];
z[1] = z[1] + (T(1) / T(2)) * z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_1024_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("35.754665871700723108794116484294208571246700813899137004967334256"),stof<T>("16.491633548675635874539296260905778385781835158327393429015731683")}, std::complex<T>{stof<T>("15.299172975284955523706137583204596817602776227268947722199118695"),stof<T>("7.253446518536916494954193164126049482150122995964867019835692216")}, stof<T>("-1.4913792718499973244846009065923632007163434473081581903679203258"), std::complex<T>{stof<T>("1.07220200943060609760639042808394648759216621138800026996545093607"),stof<T>("-0.66074942683782383613524747436354166550955942458559860959311808426")}, std::complex<T>{stof<T>("-2.5150380543193075983841982604391800913427938461478470722106265256"),stof<T>("4.9667497776376699241622007318470126247813267979357331438214834646")}};
	
	std::vector<C> intdlogs = {rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[123].real()/kbase.W[123].real()), rlog(k.W[128].real()/kbase.W[128].real()), C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_1024_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_1024_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("13.600584507896593966863618621101654072166295179926694175228125532"),stof<T>("10.990644009996832900333512828444708263012833906081216199880230083")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({198, 202});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,85> abb = {dl[0], dl[1], dlog_W3(k,dl), dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W14(k,dl), dlog_W15(k,dl), dlog_W18(k,dl), dlog_W22(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W58(k,dl), dlog_W66(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_3(k), f_2_8(k), f_2_13(k), f_2_15(k), f_2_18(k), f_2_19(k), f_2_21(k), f_2_2_im(k), f_2_7_im(k), f_2_9_im(k), f_2_11_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real()), rlog(kend.W[128].real()/k.W[128].real()), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_2_re(k), f_2_7_re(k), f_2_9_re(k), f_2_11_re(k), f_2_22_re(k), f_2_25_re(k), T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}, T{0}};
{
auto c = dlog_W162(k,dv) * f_2_30(k);
abb[75] = c.real();
abb[54] = c.imag();
SpDLog_Sigma5<T,198,161>(k, dv, abb[75], abb[54], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W164(k,dv) * f_2_30(k);
abb[76] = c.real();
abb[55] = c.imag();
SpDLog_Sigma5<T,198,163>(k, dv, abb[76], abb[55], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W169(k,dv) * f_2_34(k);
abb[77] = c.real();
abb[56] = c.imag();
SpDLog_Sigma5<T,202,168>(k, dv, abb[77], abb[56], f_2_34_series_coefficients<T>);
}
{
auto c = dlog_W172(k,dv) * f_2_34(k);
abb[78] = c.real();
abb[57] = c.imag();
SpDLog_Sigma5<T,202,171>(k, dv, abb[78], abb[57], f_2_34_series_coefficients<T>);
}
{
auto c = dlog_W175(k,dv) * f_2_34(k);
abb[79] = c.real();
abb[58] = c.imag();
SpDLog_Sigma5<T,202,174>(k, dv, abb[79], abb[58], f_2_34_series_coefficients<T>);
}
{
auto c = dlog_W178(k,dv) * f_2_34(k);
abb[80] = c.real();
abb[59] = c.imag();
SpDLog_Sigma5<T,202,177>(k, dv, abb[80], abb[59], f_2_34_series_coefficients<T>);
}
{
auto c = dlog_W183(k,dv) * f_2_30(k);
abb[81] = c.real();
abb[60] = c.imag();
SpDLog_Sigma5<T,198,182>(k, dv, abb[81], abb[60], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W185(k,dv) * f_2_30(k);
abb[82] = c.real();
abb[61] = c.imag();
SpDLog_Sigma5<T,198,184>(k, dv, abb[82], abb[61], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W189(k,dv) * f_2_30(k);
abb[83] = c.real();
abb[62] = c.imag();
SpDLog_Sigma5<T,198,188>(k, dv, abb[83], abb[62], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W193(k,dv) * f_2_34(k);
abb[84] = c.real();
abb[63] = c.imag();
SpDLog_Sigma5<T,202,192>(k, dv, abb[84], abb[63], f_2_34_series_coefficients<T>);
}

                    
            return f_4_1024_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_1024_DLogXconstant_part(base_point<T>, kend);
	value += f_4_1024_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_1024_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_1024_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_1024_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_1024_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_1024_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_1024_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
