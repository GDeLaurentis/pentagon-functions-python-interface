/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_113.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_113_abbreviated (const std::array<T,60>& abb) {
T z[114];
z[0] = abb[8] * (T(1) / T(4));
z[1] = abb[15] * (T(1) / T(4));
z[2] = abb[3] * (T(1) / T(2));
z[3] = z[0] + z[1] + z[2];
z[4] = abb[5] * (T(3) / T(4));
z[5] = -abb[14] + z[4];
z[6] = -abb[6] + abb[7];
z[6] = (T(1) / T(2)) * z[6];
z[7] = abb[12] + z[6];
z[8] = -z[3] + z[5] + (T(1) / T(2)) * z[7];
z[8] = abb[31] * z[8];
z[9] = abb[30] + abb[33] + -abb[34];
z[10] = abb[1] * (T(1) / T(2));
z[9] = z[9] * z[10];
z[8] = z[8] + -z[9];
z[9] = abb[3] + -abb[6];
z[1] = -abb[8] + z[1] + (T(1) / T(4)) * z[9];
z[11] = abb[32] * z[1];
z[12] = abb[8] + abb[15];
z[13] = abb[30] * (T(1) / T(4));
z[14] = z[12] * z[13];
z[15] = abb[30] + -abb[33];
z[16] = abb[4] * (T(1) / T(4));
z[17] = -z[15] * z[16];
z[18] = abb[3] + -z[7];
z[18] = abb[8] + (T(1) / T(2)) * z[18];
z[18] = abb[34] * z[18];
z[19] = -abb[8] + z[9];
z[19] = (T(1) / T(4)) * z[19];
z[20] = -abb[13] + z[19];
z[21] = abb[33] * z[20];
z[14] = z[8] + -z[11] + z[14] + z[17] + z[18] + z[21];
z[14] = abb[50] * z[14];
z[7] = z[2] + -z[7];
z[7] = abb[34] * z[7];
z[17] = -abb[6] + abb[15];
z[18] = -abb[0] + z[17];
z[21] = abb[32] * (T(1) / T(2));
z[22] = -z[18] * z[21];
z[23] = abb[34] * (T(1) / T(2));
z[24] = abb[0] * z[23];
z[7] = z[7] + z[22] + -z[24];
z[3] = abb[6] + z[3];
z[22] = abb[30] * z[3];
z[25] = abb[6] * (T(1) / T(2));
z[26] = abb[3] + z[25];
z[27] = abb[8] * (T(1) / T(2));
z[28] = z[26] + -z[27];
z[29] = abb[0] * (T(1) / T(4));
z[30] = -abb[13] + z[29];
z[28] = (T(1) / T(2)) * z[28] + -z[30];
z[28] = abb[33] * z[28];
z[31] = abb[4] * (T(3) / T(4));
z[15] = -z[15] * z[31];
z[7] = (T(1) / T(2)) * z[7] + z[8] + z[15] + z[22] + -z[28];
z[7] = abb[51] * z[7];
z[8] = abb[18] * abb[54];
z[15] = abb[49] + abb[52];
z[22] = -abb[53] + z[15];
z[32] = (T(1) / T(4)) * z[22];
z[33] = abb[0] * z[32];
z[8] = (T(1) / T(2)) * z[8] + -z[33];
z[34] = z[22] * z[27];
z[35] = abb[54] * (T(1) / T(4));
z[36] = abb[19] * z[35];
z[37] = abb[3] * z[32];
z[36] = z[36] + z[37];
z[37] = abb[50] + z[22];
z[38] = abb[2] * z[37];
z[39] = abb[10] * abb[53];
z[40] = (T(1) / T(2)) * z[15];
z[41] = abb[9] * z[40];
z[42] = -z[8] + -z[34] + -z[36] + -z[38] + z[39] + z[41];
z[43] = abb[17] * (T(3) / T(4));
z[44] = z[37] * z[43];
z[45] = abb[45] + abb[46];
z[46] = -abb[44] + abb[48] * (T(1) / T(2));
z[46] = (T(1) / T(2)) * z[46];
z[47] = abb[55] + abb[47] * (T(-3) / T(4)) + (T(-3) / T(4)) * z[45] + z[46];
z[48] = -abb[26] * z[47];
z[49] = abb[21] * z[35];
z[48] = z[48] + z[49];
z[49] = abb[5] * z[40];
z[50] = z[44] + z[48] + -z[49];
z[51] = -abb[24] * z[47];
z[52] = z[50] + z[51];
z[53] = 3 * abb[5];
z[54] = -abb[15] + z[53];
z[55] = -abb[3] + abb[8];
z[56] = -z[54] + -z[55];
z[29] = -abb[10] + z[29];
z[56] = z[10] + -z[29] + (T(1) / T(4)) * z[56];
z[56] = abb[51] * z[56];
z[57] = 3 * abb[8];
z[54] = -z[54] + -z[57];
z[54] = abb[1] + (T(1) / T(2)) * z[54];
z[58] = abb[50] * (T(1) / T(2));
z[54] = z[54] * z[58];
z[54] = z[42] + z[52] + z[54] + z[56];
z[54] = abb[29] * z[54];
z[56] = abb[7] * abb[49];
z[59] = (T(1) / T(2)) * z[56];
z[60] = abb[12] * abb[49];
z[61] = (T(1) / T(2)) * z[60];
z[62] = abb[52] + -abb[53];
z[63] = abb[8] * z[62];
z[63] = z[36] + -z[48] + z[59] + -z[61] + z[63];
z[63] = abb[34] * z[63];
z[60] = -z[56] + z[60];
z[64] = abb[14] * abb[52];
z[65] = z[41] + -z[49] + (T(-1) / T(2)) * z[60] + z[64];
z[66] = -abb[31] * z[65];
z[36] = z[36] + z[48];
z[48] = -abb[22] * z[47];
z[67] = z[36] + -z[48];
z[68] = abb[13] * z[22];
z[8] = -z[8] + z[34] + -z[67] + z[68];
z[34] = -abb[33] * z[8];
z[69] = abb[34] * z[22];
z[70] = abb[32] * z[22];
z[71] = abb[33] * z[22];
z[72] = -abb[32] + abb[33];
z[73] = -abb[34] + z[72];
z[73] = abb[50] * z[73];
z[71] = -z[69] + -z[70] + z[71] + z[73];
z[71] = z[43] * z[71];
z[73] = -abb[33] * z[47];
z[74] = -abb[31] * z[47];
z[73] = z[73] + z[74];
z[75] = -abb[34] * z[47];
z[76] = -abb[32] * z[47];
z[77] = z[75] + z[76];
z[78] = z[73] + -z[77];
z[79] = -abb[23] * z[78];
z[80] = abb[8] * z[22];
z[81] = z[67] + -z[80];
z[82] = z[33] + z[81];
z[83] = abb[32] * z[82];
z[84] = -abb[30] * z[47];
z[85] = -z[73] + z[84];
z[85] = abb[24] * z[85];
z[86] = abb[34] * z[33];
z[7] = z[7] + z[14] + z[34] + z[54] + z[63] + z[66] + z[71] + z[79] + -z[83] + z[85] + z[86];
z[14] = abb[33] * (T(1) / T(2));
z[34] = z[14] * z[22];
z[54] = z[22] * z[23];
z[54] = z[54] + z[70];
z[34] = z[34] + -z[54];
z[63] = z[14] + -z[23];
z[66] = -abb[32] + z[63];
z[71] = abb[50] * z[66];
z[71] = z[34] + z[71];
z[79] = abb[2] * z[71];
z[7] = (T(1) / T(2)) * z[7] + -z[79];
z[7] = m1_set::bc<T>[0] * z[7];
z[85] = abb[0] * (T(1) / T(2));
z[87] = abb[5] + -abb[15];
z[88] = -abb[10] + z[85] + (T(3) / T(2)) * z[87];
z[89] = abb[51] * (T(1) / T(2));
z[88] = z[88] * z[89];
z[90] = abb[8] + z[87];
z[91] = abb[50] * (T(3) / T(4));
z[90] = z[90] * z[91];
z[40] = abb[15] * z[40];
z[91] = (T(1) / T(2)) * z[39];
z[92] = (T(3) / T(4)) * z[80];
z[35] = abb[18] * z[35];
z[40] = z[33] + -z[35] + z[40] + z[52] + -z[88] + -z[90] + z[91] + -z[92];
z[52] = (T(3) / T(2)) * z[38];
z[88] = z[40] + -z[52];
z[88] = abb[40] * z[88];
z[90] = abb[6] * abb[49];
z[93] = abb[8] * abb[49];
z[90] = z[90] + z[93];
z[94] = abb[5] * z[15];
z[95] = abb[15] * z[15];
z[56] = z[56] + z[64] + -z[90] + -z[94] + z[95];
z[96] = abb[41] * z[56];
z[97] = abb[5] * (T(3) / T(2));
z[6] = z[6] + -z[27] + z[97];
z[98] = abb[14] + abb[15] * (T(3) / T(2)) + -z[6];
z[99] = abb[1] + -z[98];
z[99] = abb[41] * z[99];
z[57] = -z[9] + z[57];
z[100] = abb[4] * (T(1) / T(2));
z[57] = abb[13] + (T(1) / T(2)) * z[57] + -z[100];
z[101] = -abb[42] * z[57];
z[101] = z[99] + z[101];
z[101] = abb[50] * z[101];
z[102] = abb[4] + -abb[6];
z[102] = -abb[3] + -abb[13] + (T(3) / T(2)) * z[102];
z[103] = abb[42] * z[102];
z[99] = z[99] + z[103];
z[99] = abb[51] * z[99];
z[96] = z[96] + -z[99] + -z[101];
z[78] = -m1_set::bc<T>[0] * z[78];
z[99] = abb[41] + abb[42];
z[99] = -z[47] * z[99];
z[78] = z[78] + -z[99];
z[101] = abb[25] * (T(1) / T(8));
z[78] = z[78] * z[101];
z[35] = -z[35] + -z[67] + z[92];
z[67] = abb[2] * (T(-3) / T(16)) + abb[17] * (T(3) / T(32));
z[67] = z[37] * z[67];
z[67] = (T(-1) / T(8)) * z[35] + z[67] + (T(-1) / T(16)) * z[68];
z[67] = abb[42] * z[67];
z[92] = abb[23] + abb[24];
z[103] = (T(-1) / T(8)) * z[92];
z[99] = z[99] * z[103];
z[7] = abb[59] + (T(1) / T(4)) * z[7] + z[67] + z[78] + (T(1) / T(8)) * z[88] + (T(-1) / T(16)) * z[96] + z[99];
z[67] = abb[15] * (T(1) / T(2));
z[78] = abb[3] + z[27];
z[88] = z[67] + z[78];
z[96] = z[25] + z[88];
z[99] = abb[31] * (T(1) / T(2));
z[103] = z[96] * z[99];
z[104] = abb[34] * (T(1) / T(4));
z[105] = abb[8] * (T(5) / T(2)) + z[2];
z[106] = abb[6] + z[105];
z[106] = z[104] * z[106];
z[20] = z[14] * z[20];
z[20] = -z[11] + z[20] + -z[103] + z[106];
z[20] = abb[33] * z[20];
z[63] = abb[31] + z[63];
z[13] = z[13] + -z[63];
z[13] = abb[30] * z[13];
z[106] = abb[33] * (T(1) / T(4));
z[107] = -abb[31] + z[23];
z[108] = z[106] + -z[107];
z[108] = abb[33] * z[108];
z[109] = prod_pow(abb[34], 2);
z[110] = (T(1) / T(2)) * z[109];
z[111] = 3 * abb[36] + abb[57] + z[110];
z[112] = -abb[34] + z[99];
z[112] = abb[31] * z[112];
z[13] = z[13] + -z[108] + (T(-1) / T(2)) * z[111] + -z[112];
z[13] = abb[1] * z[13];
z[6] = abb[3] + -abb[12] + abb[15] * (T(-5) / T(2)) + z[6];
z[6] = abb[31] * z[6];
z[108] = -abb[12] + z[2];
z[111] = abb[7] * (T(1) / T(2));
z[67] = -z[67] + z[108] + z[111];
z[67] = abb[34] * z[67];
z[6] = z[6] + -z[67];
z[9] = -3 * abb[15] + z[9] + z[53];
z[53] = -z[9] * z[21];
z[12] = z[12] * z[14];
z[12] = -z[6] + z[12] + z[53];
z[53] = abb[5] * (T(3) / T(8));
z[67] = -abb[15] + z[53];
z[0] = -abb[14] + abb[6] * (T(1) / T(4)) + z[0];
z[113] = abb[3] * (T(1) / T(4)) + -z[0];
z[113] = z[67] + (T(1) / T(2)) * z[113];
z[113] = abb[30] * z[113];
z[12] = (T(1) / T(2)) * z[12] + z[113];
z[12] = abb[30] * z[12];
z[98] = abb[57] * z[98];
z[113] = abb[6] + abb[7];
z[27] = z[27] + (T(1) / T(2)) * z[113];
z[113] = -abb[12] + abb[3] * (T(3) / T(2)) + z[27];
z[113] = abb[36] * z[113];
z[98] = -z[98] + z[113];
z[108] = -z[27] + z[108];
z[108] = z[108] * z[110];
z[108] = -z[98] + z[108];
z[113] = abb[34] * z[1];
z[1] = abb[13] * (T(1) / T(2)) + z[1] + z[53];
z[1] = abb[32] * z[1];
z[1] = z[1] + z[113];
z[1] = abb[32] * z[1];
z[53] = -abb[6] + abb[12] + z[111];
z[5] = z[5] + (T(1) / T(2)) * z[53] + -z[88];
z[5] = abb[31] * z[5];
z[53] = abb[34] * z[96];
z[5] = z[5] + z[53];
z[53] = z[5] * z[99];
z[88] = prod_pow(abb[32], 2);
z[99] = prod_pow(abb[33], 2);
z[88] = z[88] + -z[99];
z[72] = abb[30] * z[72];
z[72] = z[72] + (T(1) / T(2)) * z[88];
z[16] = -z[16] * z[72];
z[1] = z[1] + z[12] + z[13] + z[16] + z[20] + z[53] + (T(1) / T(2)) * z[108];
z[1] = abb[50] * z[1];
z[12] = -abb[56] * z[40];
z[16] = -abb[23] * z[47];
z[20] = z[57] * z[58];
z[40] = -z[89] * z[102];
z[53] = (T(1) / T(2)) * z[68];
z[20] = z[16] + z[20] + z[35] + z[40] + -z[44] + z[51] + z[52] + z[53];
z[20] = abb[58] * z[20];
z[15] = (T(1) / T(4)) * z[15];
z[15] = abb[9] * z[15];
z[15] = z[15] + (T(1) / T(4)) * z[94];
z[35] = abb[0] * z[22];
z[36] = z[15] + (T(1) / T(8)) * z[35] + (T(1) / T(2)) * z[36] + -z[48] + z[53] + -z[80] + -z[91];
z[36] = abb[32] * z[36];
z[40] = abb[34] * z[81];
z[40] = z[40] + z[86];
z[36] = z[36] + z[40];
z[36] = abb[32] * z[36];
z[44] = z[59] + z[61] + -z[90];
z[41] = -z[41] + -z[49] + z[95];
z[49] = z[41] + z[44];
z[49] = abb[31] * z[49];
z[44] = z[44] + -z[48];
z[52] = -abb[34] * z[44];
z[41] = z[41] + z[48];
z[53] = abb[32] * z[41];
z[57] = -z[48] + z[64] + z[93];
z[15] = z[15] + (T(1) / T(2)) * z[57] + -z[95];
z[15] = abb[30] * z[15];
z[15] = z[15] + z[49] + z[52] + z[53];
z[15] = abb[30] * z[15];
z[49] = abb[31] * z[96];
z[52] = abb[32] * z[18];
z[24] = -z[24] + z[52];
z[52] = abb[6] + z[78];
z[52] = abb[34] * z[52];
z[52] = -z[24] + z[52];
z[28] = -z[28] + -z[49] + (T(1) / T(2)) * z[52];
z[28] = abb[33] * z[28];
z[17] = -z[2] + z[17] + z[97];
z[17] = -abb[10] + (T(1) / T(2)) * z[17] + -z[30];
z[17] = abb[32] * z[17];
z[18] = abb[34] * z[18];
z[17] = z[17] + (T(1) / T(2)) * z[18];
z[17] = abb[32] * z[17];
z[5] = abb[31] * z[5];
z[2] = -abb[0] + -abb[12] + -z[2] + -z[27];
z[2] = z[2] * z[110];
z[2] = z[2] + z[5] + z[17] + z[28] + -z[98];
z[5] = abb[3] + z[87];
z[17] = abb[6] + z[5];
z[18] = abb[32] * z[17];
z[6] = -z[6] + (T(-3) / T(2)) * z[18];
z[3] = abb[33] * z[3];
z[0] = abb[3] * (T(5) / T(4)) + -z[0];
z[0] = (T(1) / T(2)) * z[0] + z[67];
z[0] = abb[30] * z[0];
z[0] = z[0] + z[3] + (T(1) / T(2)) * z[6];
z[0] = abb[30] * z[0];
z[3] = abb[33] * z[107];
z[6] = abb[36] + z[110];
z[3] = z[3] + -z[6] + -z[112];
z[18] = abb[16] * (T(3) / T(4));
z[27] = -z[3] * z[18];
z[28] = -z[31] * z[72];
z[5] = abb[1] + (T(1) / T(2)) * z[5];
z[30] = abb[16] * (T(1) / T(2)) + -z[5];
z[30] = abb[35] * z[30];
z[0] = z[0] + (T(1) / T(2)) * z[2] + z[13] + z[27] + z[28] + (T(3) / T(2)) * z[30];
z[0] = abb[51] * z[0];
z[2] = abb[57] * z[56];
z[13] = abb[3] * z[22];
z[27] = abb[19] * abb[54];
z[27] = z[13] + z[27] + -z[60];
z[27] = (T(1) / T(2)) * z[27] + -z[48] + -z[93];
z[27] = z[27] * z[109];
z[2] = z[2] + z[27];
z[27] = (T(1) / T(2)) * z[40] + z[83];
z[8] = -z[8] * z[14];
z[8] = z[8] + -z[27];
z[8] = abb[33] * z[8];
z[28] = -z[74] + z[75];
z[28] = abb[31] * z[28];
z[6] = -abb[57] + z[6];
z[6] = -z[6] * z[47];
z[6] = z[6] + -z[28];
z[28] = -z[23] * z[47];
z[30] = -z[14] * z[47];
z[28] = z[28] + -z[30];
z[31] = z[28] + -z[74];
z[40] = abb[33] * z[31];
z[48] = (T(1) / T(2)) * z[84];
z[52] = -z[48] + z[73];
z[52] = abb[30] * z[52];
z[40] = -z[6] + z[40] + z[52];
z[40] = abb[24] * z[40];
z[52] = z[74] + z[76];
z[48] = -z[48] + z[52];
z[48] = abb[30] * z[48];
z[53] = abb[32] * z[77];
z[48] = z[48] + -z[53];
z[30] = -z[30] + -z[74] + z[77];
z[30] = abb[33] * z[30];
z[6] = -z[6] + z[30] + z[48];
z[6] = abb[23] * z[6];
z[30] = prod_pow(abb[31], 2);
z[30] = (T(1) / T(2)) * z[30];
z[53] = -z[30] * z[65];
z[34] = abb[33] * z[34];
z[56] = z[69] + (T(1) / T(2)) * z[70];
z[56] = abb[32] * z[56];
z[57] = abb[33] * z[66];
z[21] = abb[34] + z[21];
z[21] = abb[32] * z[21];
z[21] = z[21] + z[57];
z[21] = abb[50] * z[21];
z[21] = z[21] + z[34] + z[56];
z[21] = z[21] * z[43];
z[34] = z[16] + z[41];
z[19] = -abb[15] + z[4] + z[19];
z[56] = z[10] + z[19];
z[57] = abb[50] + abb[51];
z[56] = z[56] * z[57];
z[51] = -z[34] + -z[51] + z[56];
z[51] = abb[38] * z[51];
z[44] = abb[36] * z[44];
z[56] = abb[50] * z[18];
z[3] = -z[3] * z[56];
z[5] = abb[50] * z[5];
z[5] = (T(-3) / T(2)) * z[5] + z[41] + z[56];
z[5] = abb[35] * z[5];
z[57] = abb[19] + abb[21];
z[22] = z[22] * z[57];
z[58] = abb[3] * abb[54];
z[22] = z[22] + z[58];
z[58] = -abb[27] * z[47];
z[59] = abb[8] + abb[0] * (T(3) / T(4));
z[59] = abb[54] * z[59];
z[22] = (T(3) / T(4)) * z[22] + z[58] + z[59];
z[58] = abb[18] + abb[20];
z[59] = z[57] + z[58];
z[59] = abb[50] * z[59];
z[58] = abb[51] * z[58];
z[58] = z[58] + z[59];
z[59] = -abb[28] + abb[17] * (T(1) / T(8));
z[59] = abb[54] * z[59];
z[22] = (T(1) / T(2)) * z[22] + (T(3) / T(8)) * z[58] + z[59];
z[22] = abb[39] * z[22];
z[9] = -abb[4] + z[9];
z[9] = abb[50] * z[9];
z[17] = -abb[4] + z[17];
z[17] = abb[51] * z[17];
z[9] = (T(1) / T(4)) * z[9] + (T(3) / T(4)) * z[17] + -z[34];
z[9] = abb[37] * z[9];
z[17] = z[33] * z[109];
z[0] = z[0] + z[1] + (T(1) / T(2)) * z[2] + z[3] + z[5] + z[6] + z[8] + z[9] + z[12] + z[15] + z[17] + z[20] + z[21] + z[22] + z[36] + z[40] + z[44] + z[51] + z[53];
z[1] = abb[30] * (T(1) / T(2)) + z[63];
z[1] = abb[1] * z[1];
z[2] = abb[30] * z[19];
z[1] = z[1] + z[2];
z[2] = z[104] + -z[106];
z[3] = -abb[15] + (T(3) / T(2)) * z[55];
z[2] = z[2] * z[3];
z[2] = z[1] + z[2] + -z[11] + z[103];
z[2] = abb[50] * z[2];
z[3] = abb[15] + z[78];
z[5] = -abb[34] * z[3];
z[5] = z[5] + -z[24];
z[3] = z[3] + -z[85];
z[3] = z[3] * z[14];
z[3] = z[3] + (T(1) / T(2)) * z[5] + z[49];
z[5] = -z[18] * z[63];
z[1] = z[1] + (T(1) / T(2)) * z[3] + z[5];
z[1] = abb[51] * z[1];
z[3] = z[14] * z[82];
z[5] = -abb[30] * z[41];
z[6] = z[43] * z[71];
z[8] = -z[31] + -z[84];
z[8] = abb[24] * z[8];
z[9] = z[52] + -z[84];
z[9] = abb[23] * z[9];
z[11] = -z[56] * z[63];
z[1] = z[1] + z[2] + z[3] + z[5] + z[6] + z[8] + z[9] + z[11] + -z[27];
z[2] = -abb[3] + -abb[6];
z[2] = -abb[8] + (T(1) / T(2)) * z[2] + -z[97];
z[2] = (T(1) / T(2)) * z[2] + -z[10] + z[18] + -z[29];
z[2] = abb[51] * z[2];
z[3] = -abb[8] + -z[4] + -z[10] + (T(-1) / T(2)) * z[26];
z[3] = abb[50] * z[3];
z[2] = z[2] + z[3] + -z[16] + z[42] + z[50] + z[56];
z[2] = abb[29] * z[2];
z[1] = (T(1) / T(2)) * z[1] + (T(1) / T(4)) * z[2] + -z[79];
z[1] = abb[29] * z[1];
z[2] = abb[32] + z[23] + -z[106];
z[2] = abb[33] * z[2];
z[3] = -abb[34] + abb[32] * (T(-3) / T(4));
z[3] = abb[32] * z[3];
z[2] = z[2] + z[3] + (T(-1) / T(4)) * z[109];
z[2] = abb[50] * z[2];
z[3] = -z[32] * z[109];
z[4] = -z[69] + (T(-3) / T(4)) * z[70];
z[4] = abb[32] * z[4];
z[5] = -abb[33] * z[32];
z[5] = z[5] + z[54];
z[5] = abb[33] * z[5];
z[6] = abb[56] * z[37];
z[2] = z[2] + z[3] + z[4] + z[5] + (T(3) / T(4)) * z[6];
z[2] = abb[2] * z[2];
z[0] = (T(1) / T(2)) * z[0] + z[1] + z[2];
z[1] = -abb[1] + 5 * abb[13];
z[2] = 5 * abb[14] + z[1] + z[25] + -z[100] + -z[105];
z[2] = abb[50] * z[2];
z[2] = -z[2] + z[38];
z[3] = -abb[10] + abb[14] + -z[25];
z[1] = z[1] + 5 * z[3] + z[78] + z[85];
z[1] = (T(1) / T(3)) * z[1] + -z[100];
z[1] = z[1] * z[89];
z[3] = -z[39] + z[64] + z[68];
z[4] = z[13] + z[35];
z[5] = abb[47] + z[45];
z[6] = abb[55] + z[46];
z[5] = (T(-1) / T(4)) * z[5] + (T(1) / T(3)) * z[6];
z[6] = -abb[22] + abb[26] + -z[92];
z[6] = z[5] * z[6];
z[8] = (T(-1) / T(12)) * z[57];
z[8] = abb[54] * z[8];
z[9] = abb[49] * (T(1) / T(3)) + (T(-1) / T(2)) * z[62];
z[9] = abb[8] * z[9];
z[10] = abb[17] * z[37];
z[1] = z[1] + (T(-1) / T(6)) * z[2] + (T(5) / T(6)) * z[3] + (T(-1) / T(12)) * z[4] + z[6] + z[8] + z[9] + (T(-1) / T(4)) * z[10];
z[2] = prod_pow(m1_set::bc<T>[0], 2);
z[1] = z[1] * z[2];
z[3] = z[28] + z[76];
z[4] = abb[33] * z[3];
z[3] = z[3] + -z[84];
z[3] = abb[29] * z[3];
z[6] = abb[35] + -abb[37] + -abb[38] + abb[57] + abb[58] + -z[30];
z[6] = -z[6] * z[47];
z[2] = z[2] * z[5];
z[2] = (T(-1) / T(2)) * z[2] + z[3] + z[4] + z[6] + z[48];
z[2] = z[2] * z[101];
z[0] = abb[43] + (T(1) / T(4)) * z[0] + (T(1) / T(16)) * z[1] + z[2];
return {z[7], z[0]};
}


template <typename T> std::complex<T> f_4_113_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.12190198616169414065943679902177330628793293221653061702327254762"),stof<T>("0.14964127708763558511754434582314871763687518072073885540681298805")}, std::complex<T>{stof<T>("-0.12190198616169414065943679902177330628793293221653061702327254762"),stof<T>("0.14964127708763558511754434582314871763687518072073885540681298805")}, std::complex<T>{stof<T>("-0.12190198616169414065943679902177330628793293221653061702327254762"),stof<T>("0.14964127708763558511754434582314871763687518072073885540681298805")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.37434432480862864873768578167426222937607726813349699639736723493"),stof<T>("0.54482405379463579202395693497358039631213540231051412693596171119")}, std::complex<T>{stof<T>("0.21241897287943911695266854329801187100246724674210575460269701779"),stof<T>("0.27122449406901858781933572249740958056328602962356001978759838012")}, std::complex<T>{stof<T>("0.53314796662106844680042461929854951232708005836517708355487879001"),stof<T>("0.21734922352139627000552935144790677205372457288437133824064960108")}, std::complex<T>{stof<T>("0.24336357417923899565204359180260152530948135093936223578899132426"),stof<T>("0.32444847655410999372569986159950380140205941937160134031605393919")}, std::complex<T>{stof<T>("-0.005470879275310993051332569820425024723643508483656850265248430919"),stof<T>("-0.128226067877388297793032192694104503251530244390654820646481225909")}, std::complex<T>{stof<T>("0.09392679715492438233706052660509333276213873049709978327426054204"),stof<T>("0.35414048021308472680514668522205353977024989773161571037398813634")}, std::complex<T>{stof<T>("0.16253598154892552087924906536236440838391057628870748936436339683"),stof<T>("-0.19952170278351411349005912776419829018250024096098514054241731739")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}, rlog(k.W[197].imag()/kbase.W[197].imag())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_113_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_113_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_113_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(1) / T(64)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[1] * (T(1) / T(16)) * (-v[3] + v[5])) / tend;


		return (abb[49] + abb[50] + abb[52]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[30], 2);
z[1] = prod_pow(abb[32], 2);
z[0] = z[0] + -z[1];
z[1] = abb[49] + abb[50] + abb[52];
return abb[11] * (T(1) / T(16)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_113_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.37971246311892227386010128278800750770033273917748718598768427657"),stof<T>("-0.05852278689582315266790133833554075900840957594547693199597408389")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,60> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W21(k,dl), dlog_W22(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), rlog(kend.W[197].imag()/k.W[197].imag()), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), T{0}};
abb[43] = SpDLog_f_4_113_W_16_Im(t, path, abb);
abb[59] = SpDLog_f_4_113_W_16_Re(t, path, abb);

                    
            return f_4_113_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_113_DLogXconstant_part(base_point<T>, kend);
	value += f_4_113_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_113_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_113_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_113_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_113_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_113_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_113_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
