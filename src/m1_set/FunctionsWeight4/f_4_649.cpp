/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_649.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_649_abbreviated (const std::array<T,72>& abb) {
T z[165];
z[0] = abb[50] * (T(4) / T(3));
z[1] = abb[48] * (T(11) / T(3));
z[2] = abb[53] * (T(5) / T(2));
z[3] = abb[51] * (T(4) / T(3));
z[4] = -7 * abb[52] + 29 * abb[57];
z[4] = -5 * abb[47] + abb[49] * (T(-1) / T(6)) + (T(1) / T(3)) * z[4];
z[4] = abb[54] * (T(-59) / T(12)) + -z[0] + -z[1] + z[2] + z[3] + (T(1) / T(2)) * z[4];
z[4] = abb[14] * z[4];
z[5] = -abb[50] + abb[55];
z[3] = abb[56] * (T(2) / T(3)) + z[3];
z[6] = abb[49] * (T(5) / T(2));
z[7] = 2 * abb[58];
z[8] = 3 * abb[57];
z[9] = -abb[52] + -z[8];
z[9] = -5 * abb[53] + abb[48] * (T(-7) / T(2)) + abb[54] * (T(9) / T(2)) + -z[3] + (T(11) / T(3)) * z[5] + -z[6] + z[7] + (T(1) / T(2)) * z[9];
z[9] = abb[10] * z[9];
z[10] = abb[55] * (T(1) / T(2));
z[11] = -abb[48] + z[10];
z[12] = abb[51] * (T(1) / T(2));
z[13] = abb[50] + -abb[56];
z[14] = -z[11] + z[12] + (T(1) / T(2)) * z[13];
z[15] = abb[4] * z[14];
z[16] = abb[47] + abb[51];
z[17] = 2 * abb[49];
z[18] = -abb[56] + z[17];
z[19] = -z[16] + z[18];
z[20] = 4 * abb[48];
z[21] = 2 * abb[50];
z[22] = z[19] + z[20] + z[21];
z[22] = abb[15] * z[22];
z[23] = abb[61] + abb[62];
z[24] = abb[26] * z[23];
z[25] = -abb[56] + z[16];
z[26] = abb[16] * z[25];
z[27] = z[24] + -z[26];
z[28] = -4 * abb[8] + abb[7] * (T(1) / T(2));
z[28] = abb[55] * z[28];
z[28] = z[15] + z[22] + -z[27] + z[28];
z[29] = -abb[48] + abb[53];
z[30] = abb[52] + abb[54];
z[31] = abb[58] + abb[49] * (T(1) / T(3));
z[32] = 2 * abb[57];
z[29] = abb[50] + abb[47] * (T(4) / T(3)) + (T(-8) / T(3)) * z[29] + (T(5) / T(3)) * z[30] + -z[31] + -z[32];
z[29] = abb[17] * z[29];
z[32] = -abb[52] + z[32];
z[33] = 2 * abb[53];
z[34] = -abb[49] + z[33];
z[35] = z[13] + z[32] + -z[34];
z[35] = abb[8] * z[35];
z[36] = abb[8] * abb[51];
z[37] = abb[24] * z[23];
z[36] = z[36] + z[37];
z[38] = -z[35] + z[36];
z[39] = abb[56] * (T(1) / T(2));
z[1] = abb[47] + abb[51] * (T(-5) / T(6)) + abb[58] * (T(1) / T(3)) + -z[1] + (T(11) / T(6)) * z[5] + z[39];
z[1] = abb[6] * z[1];
z[40] = abb[58] * (T(2) / T(3));
z[41] = 3 * abb[47];
z[42] = 3 * abb[49];
z[43] = abb[51] * (T(-17) / T(6)) + abb[55] * (T(-7) / T(6)) + abb[56] * (T(-5) / T(6)) + abb[48] * (T(4) / T(3)) + abb[50] * (T(25) / T(6)) + z[40] + z[41] + z[42];
z[43] = abb[5] * z[43];
z[44] = -abb[52] + abb[55] + abb[58];
z[2] = abb[57] * (T(-15) / T(4)) + abb[48] * (T(5) / T(2)) + abb[50] * (T(7) / T(3)) + abb[54] * (T(10) / T(3)) + abb[49] * (T(29) / T(12)) + abb[47] * (T(35) / T(12)) + -z[2] + -z[3] + (T(4) / T(3)) * z[44];
z[2] = abb[3] * z[2];
z[3] = abb[23] + abb[25];
z[3] = z[3] * z[23];
z[44] = abb[50] * (T(1) / T(2));
z[31] = abb[47] * (T(1) / T(3)) + abb[56] * (T(1) / T(6)) + abb[51] * (T(7) / T(6)) + -z[31] + -z[44];
z[31] = abb[7] * z[31];
z[0] = abb[54] * (T(-11) / T(3)) + abb[55] * (T(-7) / T(3)) + abb[47] * (T(-7) / T(4)) + abb[56] * (T(1) / T(3)) + abb[48] * (T(1) / T(6)) + abb[49] * (T(3) / T(4)) + abb[53] * (T(7) / T(2)) + abb[57] * (T(13) / T(4)) + z[0] + -z[40];
z[0] = abb[2] * z[0];
z[40] = 2 * abb[56];
z[45] = 35 * abb[47] + -17 * abb[57];
z[45] = abb[51] + abb[49] * (T(35) / T(4)) + z[40] + (T(1) / T(4)) * z[45];
z[46] = 4 * abb[50];
z[45] = abb[54] * (T(-5) / T(2)) + abb[53] * (T(-5) / T(6)) + abb[55] * (T(-2) / T(3)) + abb[48] * (T(11) / T(6)) + (T(1) / T(3)) * z[45] + z[46];
z[45] = abb[0] * z[45];
z[47] = abb[20] + abb[21];
z[48] = abb[18] + abb[19];
z[47] = (T(1) / T(3)) * z[47] + -z[48];
z[49] = -abb[60] * z[47];
z[50] = abb[47] + -abb[57];
z[51] = abb[54] + z[50];
z[52] = abb[1] * z[51];
z[53] = -abb[52] + abb[57];
z[54] = -abb[48] + z[53];
z[55] = abb[13] * z[54];
z[56] = abb[52] + z[50];
z[57] = abb[9] * z[56];
z[58] = abb[22] * z[23];
z[0] = z[0] + z[1] + z[2] + (T(5) / T(6)) * z[3] + z[4] + z[9] + (T(1) / T(3)) * z[28] + z[29] + z[31] + (T(4) / T(3)) * z[38] + z[43] + z[45] + z[49] + (T(-1) / T(4)) * z[52] + (T(11) / T(6)) * z[55] + (T(-5) / T(2)) * z[57] + z[58];
z[1] = prod_pow(m1_set::bc<T>[0], 2);
z[0] = z[0] * z[1];
z[2] = -abb[21] + 3 * z[48];
z[4] = abb[34] * (T(1) / T(2));
z[2] = z[2] * z[4];
z[9] = 3 * abb[18] + abb[21];
z[28] = abb[19] + z[9];
z[29] = abb[30] * (T(1) / T(2));
z[28] = z[28] * z[29];
z[31] = abb[31] + abb[33];
z[31] = abb[19] * z[31];
z[38] = abb[19] * abb[32];
z[43] = abb[30] + -abb[34];
z[45] = abb[20] * z[43];
z[49] = abb[19] + -abb[21];
z[49] = abb[35] * z[49];
z[2] = -z[2] + z[28] + -z[31] + z[38] + (T(-1) / T(2)) * z[45] + z[49];
z[2] = abb[31] * z[2];
z[28] = abb[18] * abb[30];
z[31] = abb[18] * abb[34];
z[28] = -z[28] + z[31] + (T(-1) / T(2)) * z[38] + z[45];
z[28] = abb[32] * z[28];
z[38] = 3 * abb[19];
z[45] = abb[18] + -abb[21] + z[38];
z[45] = z[4] * z[45];
z[59] = abb[33] * z[48];
z[49] = -z[49] + z[59];
z[59] = abb[21] * z[29];
z[45] = z[45] + z[49] + -z[59];
z[45] = abb[30] * z[45];
z[59] = abb[3] + 2 * abb[29];
z[60] = abb[5] + abb[6] + -abb[15] + -abb[16];
z[61] = z[59] + (T(1) / T(2)) * z[60];
z[61] = abb[68] * z[61];
z[62] = abb[18] * abb[35];
z[62] = -z[31] + z[62];
z[62] = abb[34] * z[62];
z[29] = z[29] * z[43];
z[29] = -abb[37] + z[29];
z[29] = abb[20] * z[29];
z[63] = abb[37] * z[48];
z[31] = abb[33] * z[31];
z[64] = abb[19] + abb[20];
z[65] = abb[64] * z[64];
z[66] = abb[18] + abb[21];
z[67] = abb[63] * z[66];
z[68] = prod_pow(abb[35], 2);
z[69] = (T(1) / T(2)) * z[68];
z[70] = abb[18] * z[69];
z[71] = -abb[21] + z[48];
z[72] = abb[36] * z[71];
z[2] = z[2] + z[28] + -z[29] + -z[31] + z[45] + -z[61] + z[62] + -z[63] + z[65] + z[67] + -z[70] + -z[72];
z[28] = -abb[60] * z[2];
z[1] = -z[1] * z[47];
z[1] = z[1] + -z[2];
z[1] = abb[59] * z[1];
z[2] = abb[47] + abb[56];
z[29] = (T(1) / T(2)) * z[2];
z[31] = z[12] + z[29];
z[45] = abb[49] + abb[50];
z[47] = 2 * abb[48];
z[61] = z[31] + -z[45] + -z[47];
z[62] = abb[15] * z[61];
z[63] = abb[49] + z[21];
z[65] = -abb[55] + z[63];
z[67] = -abb[58] + z[65];
z[70] = 3 * abb[48];
z[72] = z[67] + z[70];
z[73] = 2 * abb[2];
z[74] = z[72] * z[73];
z[26] = (T(1) / T(2)) * z[26];
z[75] = -z[26] + z[62] + z[74];
z[76] = z[5] + -z[47];
z[77] = -abb[56] + z[41];
z[78] = (T(1) / T(2)) * z[77];
z[79] = z[12] + z[76] + z[78];
z[79] = abb[6] * z[79];
z[80] = 3 * abb[50];
z[81] = 2 * abb[55];
z[82] = abb[49] + z[20] + z[80] + -z[81];
z[31] = z[31] + -z[82];
z[31] = abb[5] * z[31];
z[83] = z[3] + z[58];
z[84] = -z[24] + -z[83];
z[31] = z[31] + -z[75] + z[79] + (T(1) / T(2)) * z[84];
z[31] = abb[34] * z[31];
z[84] = z[7] + -z[47];
z[65] = z[2] + z[65] + -z[84];
z[65] = abb[34] * z[65];
z[85] = -abb[50] + z[84];
z[86] = abb[47] + -z[40] + z[85];
z[86] = abb[35] * z[86];
z[87] = abb[35] * abb[55];
z[86] = z[86] + z[87];
z[88] = abb[47] + z[76];
z[89] = -abb[33] * z[88];
z[90] = abb[49] + z[13];
z[91] = 2 * abb[47];
z[92] = -z[90] + -z[91];
z[92] = abb[30] * z[92];
z[65] = z[65] + z[86] + z[89] + z[92];
z[65] = abb[0] * z[65];
z[89] = abb[56] + -abb[58];
z[92] = z[73] * z[89];
z[93] = abb[51] + -abb[56];
z[94] = -z[76] + z[93];
z[95] = abb[4] * z[94];
z[96] = -z[7] + z[16];
z[97] = abb[56] + z[96];
z[98] = abb[5] * z[97];
z[92] = z[92] + z[95] + z[98];
z[99] = abb[33] * z[92];
z[26] = z[26] + z[62];
z[62] = -z[24] + z[83];
z[62] = z[26] + (T(1) / T(2)) * z[62];
z[79] = z[62] + -z[79];
z[100] = z[12] + z[39];
z[101] = abb[47] * (T(1) / T(2));
z[102] = z[100] + z[101];
z[103] = -z[7] + z[82] + z[102];
z[103] = abb[5] * z[103];
z[103] = z[74] + z[79] + z[103];
z[103] = abb[30] * z[103];
z[104] = abb[51] + abb[56];
z[85] = z[85] + -z[104];
z[85] = abb[35] * z[85];
z[85] = z[85] + z[87];
z[105] = abb[34] * z[97];
z[106] = -abb[30] * z[88];
z[105] = z[85] + z[105] + z[106];
z[105] = abb[3] * z[105];
z[106] = z[7] + z[76];
z[107] = -z[104] + z[106];
z[108] = abb[17] * z[107];
z[109] = abb[27] * z[23];
z[108] = z[108] + -z[109];
z[110] = abb[0] * z[88];
z[92] = -z[92] + -z[108] + z[110];
z[92] = abb[32] * z[92];
z[110] = -z[27] + z[98];
z[110] = abb[35] * z[110];
z[111] = abb[30] + abb[33];
z[112] = z[107] * z[111];
z[112] = -z[85] + z[112];
z[112] = abb[17] * z[112];
z[113] = -abb[0] + abb[5];
z[113] = z[88] * z[113];
z[114] = -abb[48] + z[5];
z[115] = z[73] * z[114];
z[113] = z[113] + z[115];
z[113] = abb[31] * z[113];
z[111] = -z[23] * z[111];
z[115] = abb[35] * z[23];
z[111] = z[111] + z[115];
z[111] = abb[27] * z[111];
z[31] = z[31] + z[65] + z[92] + z[99] + z[103] + z[105] + -z[110] + z[111] + z[112] + z[113];
z[31] = abb[31] * z[31];
z[65] = 2 * abb[52] + -5 * abb[57];
z[92] = 2 * abb[54];
z[99] = -z[42] + z[65] + z[92];
z[103] = 6 * abb[58];
z[105] = -z[20] + z[103];
z[111] = 5 * abb[50];
z[112] = abb[55] + -abb[56];
z[113] = z[16] + -z[33] + -z[99] + -z[105] + z[111] + -z[112];
z[113] = abb[17] * z[113];
z[113] = -z[35] + z[109] + z[113];
z[116] = 5 * abb[48];
z[117] = 5 * abb[54];
z[65] = -4 * abb[53] + -z[17] + z[65] + z[116] + z[117];
z[65] = abb[10] * z[65];
z[118] = z[46] + -z[81];
z[119] = z[17] + z[118];
z[120] = 3 * abb[51];
z[121] = -abb[47] + 10 * abb[48] + abb[56] + -z[103] + z[119] + z[120];
z[121] = abb[5] * z[121];
z[122] = abb[48] + abb[54];
z[123] = abb[47] + abb[57];
z[124] = z[5] + z[122] + -z[123];
z[124] = abb[0] * z[124];
z[50] = -z[33] + z[50];
z[125] = z[50] + z[92];
z[126] = z[47] + z[63];
z[127] = 2 * abb[51];
z[128] = z[125] + z[126] + -z[127];
z[129] = abb[14] * z[128];
z[130] = 5 * abb[49] + -z[103];
z[131] = 16 * abb[48] + 10 * abb[50] + -4 * abb[55] + z[125] + z[130];
z[131] = abb[2] * z[131];
z[132] = -z[81] + z[126];
z[133] = z[33] + z[132];
z[134] = abb[52] + -z[92] + z[133];
z[134] = abb[3] * z[134];
z[135] = abb[8] * abb[55];
z[135] = -z[36] + z[135];
z[65] = -z[22] + -z[65] + -z[113] + z[121] + z[124] + z[129] + z[131] + z[134] + z[135];
z[121] = -abb[64] * z[65];
z[124] = abb[57] * (T(1) / T(2));
z[131] = abb[50] * (T(3) / T(2));
z[134] = abb[49] * (T(1) / T(2));
z[136] = abb[53] + z[10] + z[84] + -z[102] + -z[124] + -z[131] + -z[134];
z[136] = abb[17] * z[136];
z[137] = 3 * abb[58];
z[138] = -abb[56] + -z[127] + z[137];
z[116] = abb[57] + -z[33] + -z[63] + z[81] + -z[116] + z[138];
z[116] = abb[10] * z[116];
z[139] = 6 * abb[48];
z[140] = z[102] + z[119] + -z[137] + z[139];
z[140] = abb[5] * z[140];
z[141] = 6 * abb[50];
z[142] = 4 * abb[58];
z[143] = z[141] + -z[142];
z[144] = 8 * abb[48];
z[145] = -z[81] + z[144];
z[146] = -z[33] + z[42];
z[147] = z[2] + z[143] + z[145] + z[146];
z[147] = abb[2] * z[147];
z[148] = -abb[53] + abb[49] * (T(3) / T(2));
z[21] = z[21] + -z[127] + z[148];
z[149] = abb[48] + abb[58] + z[21] + z[101] + -z[124];
z[149] = abb[3] * z[149];
z[150] = -abb[57] + z[45];
z[11] = abb[53] + -z[11] + (T(1) / T(2)) * z[150];
z[11] = abb[0] * z[11];
z[89] = abb[6] * z[89];
z[11] = z[11] + z[15] + -z[89] + (T(-1) / T(2)) * z[109] + z[116] + z[136] + z[140] + z[147] + z[149];
z[11] = abb[32] * z[11];
z[15] = z[22] + -z[83];
z[22] = 4 * z[72];
z[116] = abb[5] * z[22];
z[116] = -z[15] + z[116];
z[72] = abb[2] * z[72];
z[97] = abb[6] * z[97];
z[72] = 6 * z[72] + -z[97] + z[116];
z[136] = abb[34] * z[72];
z[140] = -abb[54] + z[32];
z[147] = -z[138] + z[140];
z[149] = abb[10] * z[147];
z[150] = -abb[57] + abb[58];
z[150] = abb[2] * z[150];
z[149] = z[55] + z[89] + z[149] + z[150];
z[150] = 2 * abb[33];
z[149] = z[149] * z[150];
z[151] = -z[5] + z[70];
z[152] = -z[138] + z[151];
z[153] = 2 * abb[10];
z[152] = z[152] * z[153];
z[72] = -z[72] + z[152];
z[72] = abb[30] * z[72];
z[154] = abb[34] * z[107];
z[155] = -abb[58] + z[45] + z[140];
z[156] = -z[150] * z[155];
z[157] = -abb[30] * z[107];
z[156] = z[154] + z[156] + z[157];
z[156] = abb[17] * z[156];
z[152] = abb[34] * z[152];
z[157] = -z[7] + z[120];
z[18] = z[18] + z[80];
z[158] = -abb[55] + z[47];
z[159] = z[18] + -z[157] + z[158];
z[160] = -z[43] * z[159];
z[161] = -abb[48] + abb[54];
z[162] = -abb[58] + z[127] + -z[161];
z[150] = z[150] * z[162];
z[150] = z[150] + z[160];
z[150] = abb[3] * z[150];
z[160] = -abb[47] + z[5];
z[160] = abb[0] * z[160];
z[43] = -z[43] * z[160];
z[162] = abb[34] * z[23];
z[163] = abb[30] * z[23];
z[163] = -z[162] + z[163];
z[163] = abb[27] * z[163];
z[11] = z[11] + z[43] + z[72] + z[136] + z[149] + z[150] + -z[152] + z[156] + z[163];
z[11] = abb[32] * z[11];
z[43] = abb[55] + z[148];
z[13] = -abb[52] + z[13] + z[43] + z[101] + z[124] + -z[157] + z[161];
z[13] = abb[3] * z[13];
z[72] = z[46] + -z[142];
z[124] = abb[47] * (T(3) / T(2));
z[6] = -3 * abb[53] + abb[57] * (T(7) / T(2)) + z[6] + -z[30] + z[70] + z[72] + z[124];
z[6] = abb[17] * z[6];
z[35] = z[35] + -z[83] + z[135];
z[83] = -z[125] + z[132];
z[83] = abb[2] * z[83];
z[125] = abb[5] * z[159];
z[132] = 4 * abb[51];
z[103] = z[40] + -z[103] + z[132];
z[135] = -abb[52] + z[103];
z[136] = 4 * abb[54];
z[133] = 4 * abb[57] + z[133] + z[135] + -z[136];
z[133] = abb[10] * z[133];
z[149] = abb[49] + z[123];
z[122] = abb[53] + -z[122] + (T(1) / T(2)) * z[149];
z[122] = abb[0] * z[122];
z[107] = abb[6] * z[107];
z[6] = z[6] + z[13] + -z[35] + -z[57] + z[83] + z[107] + z[122] + z[125] + -z[129] + -z[133];
z[13] = abb[67] * z[6];
z[53] = -z[2] + z[53];
z[83] = 7 * abb[51];
z[43] = abb[50] + abb[54] * (T(1) / T(2)) + z[43] + z[53] + -z[83] + z[105];
z[43] = abb[3] * z[43];
z[21] = -abb[52] + z[21];
z[105] = abb[54] * (T(3) / T(2));
z[107] = z[21] + z[91] + z[105];
z[107] = abb[14] * z[107];
z[122] = abb[0] * z[128];
z[18] = -z[18] + z[83];
z[83] = -abb[47] + z[137];
z[125] = -z[18] + 2 * z[83];
z[128] = -abb[55] + z[125];
z[128] = abb[5] * z[128];
z[125] = abb[7] * z[125];
z[129] = abb[7] + -abb[8];
z[137] = abb[55] * z[129];
z[36] = z[36] + z[43] + z[58] + -z[107] + z[113] + -z[122] + -z[125] + z[128] + -z[133] + z[137];
z[43] = abb[66] * z[36];
z[107] = -abb[47] + z[8];
z[21] = z[21] + -z[105] + z[107];
z[21] = abb[14] * z[21];
z[105] = z[44] + z[134];
z[113] = -z[39] + z[105];
z[122] = abb[52] * (T(1) / T(2));
z[128] = abb[53] + z[122];
z[133] = -abb[57] + z[12] + -z[113] + z[128];
z[134] = -abb[8] * z[133];
z[83] = -abb[49] + abb[51] * (T(7) / T(2)) + z[39] + -z[83] + -z[131];
z[137] = -abb[7] * z[83];
z[149] = z[37] + z[58];
z[129] = -z[10] * z[129];
z[150] = 2 * z[55];
z[156] = -abb[56] + -abb[57] + z[7];
z[159] = abb[2] * z[156];
z[129] = -z[21] + -z[89] + z[129] + z[134] + z[137] + (T(-1) / T(2)) * z[149] + -z[150] + z[159];
z[129] = abb[33] * z[129];
z[134] = -abb[58] + z[16];
z[137] = 4 * abb[5];
z[149] = z[134] * z[137];
z[149] = -z[97] + z[149];
z[159] = abb[7] * abb[55];
z[159] = -z[125] + z[159];
z[163] = -z[3] + z[159];
z[164] = -z[149] + z[163];
z[164] = abb[34] * z[164];
z[129] = z[129] + z[164];
z[129] = abb[33] * z[129];
z[133] = z[10] + -z[133];
z[133] = abb[8] * z[133];
z[37] = z[24] + -z[37];
z[67] = -abb[47] + z[20] + z[67];
z[67] = abb[5] * z[67];
z[164] = abb[49] + z[33];
z[138] = -abb[48] + -abb[57] + z[138] + z[164];
z[138] = abb[10] * z[138];
z[37] = (T(1) / T(2)) * z[37] + z[57] + z[67] + z[75] + -z[89] + z[133] + z[138];
z[37] = abb[30] * z[37];
z[67] = abb[56] * (T(3) / T(2));
z[75] = z[12] + z[67] + -z[101] + -z[106];
z[75] = abb[6] * z[75];
z[89] = z[42] + z[111] + z[145];
z[111] = -z[89] + -z[102] + z[142];
z[111] = abb[5] * z[111];
z[22] = abb[2] * z[22];
z[62] = -z[22] + -z[62] + z[75] + z[111];
z[62] = abb[34] * z[62];
z[75] = z[110] + z[152];
z[110] = z[147] * z[153];
z[98] = z[98] + z[110];
z[110] = -z[35] + -z[97] + -z[98];
z[110] = abb[33] * z[110];
z[37] = z[37] + z[62] + z[75] + z[110];
z[37] = abb[30] * z[37];
z[62] = abb[51] * (T(3) / T(2));
z[44] = -abb[48] + abb[54] * (T(-7) / T(2)) + -z[7] + z[8] + -z[10] + z[39] + z[44] + z[62] + z[122];
z[44] = abb[33] * z[44];
z[110] = -z[7] + z[127];
z[111] = abb[47] + abb[48] + z[110];
z[122] = 2 * abb[34];
z[133] = -z[111] * z[122];
z[44] = z[44] + z[133];
z[44] = abb[33] * z[44];
z[53] = z[53] + z[92] + z[106] + -z[120];
z[53] = abb[33] * z[53];
z[106] = -abb[58] + z[10];
z[113] = -z[62] + -z[106] + z[113] + z[128];
z[113] = abb[30] * z[113];
z[80] = z[17] + z[80];
z[128] = abb[47] + z[80];
z[133] = -z[127] + z[128];
z[138] = z[133] + z[158];
z[138] = abb[34] * z[138];
z[53] = z[53] + -z[85] + z[113] + -z[138];
z[53] = abb[30] * z[53];
z[113] = abb[37] * z[94];
z[51] = z[51] * z[69];
z[111] = abb[35] * z[111];
z[134] = -abb[34] * z[134];
z[111] = z[111] + z[134];
z[111] = z[111] * z[122];
z[44] = z[44] + z[51] + z[53] + z[111] + -z[113];
z[44] = abb[3] * z[44];
z[51] = -abb[47] + abb[58];
z[51] = abb[56] + -z[5] + 2 * z[51] + -z[120];
z[51] = abb[3] * z[51];
z[53] = -z[77] + -z[157];
z[53] = abb[5] * z[53];
z[77] = -abb[47] + -abb[58] + -z[45] + z[104];
z[77] = abb[0] * z[77];
z[51] = -z[27] + z[51] + z[53] + 2 * z[77] + z[97] + z[163];
z[51] = abb[36] * z[51];
z[53] = abb[55] + abb[57] + z[45];
z[77] = -abb[47] + z[33];
z[97] = z[53] + -z[77] + -z[104];
z[97] = abb[17] * z[97];
z[35] = -z[35] + z[97] + -z[109];
z[81] = z[34] + z[40] + -z[81] + -z[123];
z[81] = abb[2] * z[81];
z[5] = z[5] + z[54] + z[93];
z[5] = abb[3] * z[5];
z[54] = abb[6] * z[94];
z[5] = z[5] + z[35] + -z[54] + z[55] + -z[57] + z[81] + z[95];
z[54] = abb[65] * z[5];
z[81] = z[10] + z[83];
z[81] = abb[7] * z[81];
z[83] = -abb[58] + z[102];
z[83] = abb[6] * z[83];
z[21] = (T(-1) / T(2)) * z[3] + z[21] + 3 * z[52] + -z[57] + z[81] + z[83];
z[21] = z[21] * z[68];
z[30] = abb[57] * (T(-5) / T(2)) + z[30] + z[100] + -z[101] + -z[106] + -z[131] + -z[148];
z[30] = z[30] * z[68];
z[52] = -z[47] + z[77];
z[72] = z[52] + -z[72] + z[99];
z[77] = -abb[33] * z[72];
z[77] = z[77] + z[85] + -z[154];
z[77] = abb[30] * z[77];
z[81] = abb[33] * z[155];
z[81] = z[81] + -z[154];
z[81] = abb[33] * z[81];
z[83] = abb[34] * z[85];
z[30] = z[30] + z[77] + z[81] + z[83];
z[30] = abb[17] * z[30];
z[77] = z[3] + z[125] + z[149];
z[77] = abb[35] * z[77];
z[81] = abb[6] * z[88];
z[83] = -abb[47] + abb[48];
z[83] = z[83] * z[137];
z[74] = z[74] + z[81] + z[83];
z[74] = abb[34] * z[74];
z[81] = -abb[7] * z[87];
z[74] = z[74] + z[77] + z[81];
z[74] = abb[34] * z[74];
z[19] = -z[19] + -z[118] + -z[139];
z[19] = abb[5] * z[19];
z[77] = abb[56] + -z[82];
z[77] = z[73] * z[77];
z[81] = abb[6] * z[25];
z[19] = z[15] + z[19] + z[77] + z[81];
z[19] = abb[37] * z[19];
z[77] = z[50] + z[126];
z[83] = -abb[33] * z[77];
z[84] = -abb[56] + -z[45] + z[84];
z[84] = abb[34] * z[84];
z[93] = -abb[56] + abb[57] + z[114];
z[93] = abb[30] * z[93];
z[83] = z[83] + z[84] + -z[86] + z[93];
z[83] = abb[30] * z[83];
z[10] = -abb[53] + -abb[54] + abb[57] * (T(3) / T(2)) + z[10] + z[96] + z[105];
z[10] = z[10] * z[68];
z[68] = -abb[55] + z[20];
z[84] = -z[7] + z[68];
z[93] = -z[84] + -z[128];
z[93] = abb[34] * z[93];
z[94] = z[47] + z[133];
z[94] = abb[35] * z[94];
z[87] = -z[87] + z[93] + z[94];
z[87] = abb[34] * z[87];
z[93] = -abb[51] + abb[54];
z[93] = abb[33] * z[93];
z[93] = z[93] + -z[138];
z[93] = abb[33] * z[93];
z[94] = abb[37] * z[114];
z[10] = z[10] + z[83] + z[87] + z[93] + -2 * z[94];
z[10] = abb[0] * z[10];
z[33] = z[33] + z[40] + -z[53] + -z[91];
z[33] = abb[0] * z[33];
z[27] = z[27] + 2 * z[57];
z[53] = abb[3] * z[56];
z[33] = -z[27] + z[33] + z[35] + -z[53] + -z[81];
z[35] = abb[63] * z[33];
z[53] = abb[20] * z[61];
z[29] = -abb[50] + z[29] + -z[158];
z[29] = abb[18] * z[29];
z[45] = z[45] + z[78];
z[45] = abb[19] * z[45];
z[12] = z[12] * z[48];
z[12] = -z[12] + z[29] + z[45] + -z[53];
z[29] = abb[28] * z[23];
z[25] = abb[21] * z[25];
z[25] = -z[25] + z[29];
z[29] = z[12] + (T(-1) / T(2)) * z[25];
z[29] = abb[68] * z[29];
z[45] = abb[33] + -abb[35];
z[45] = z[45] * z[162];
z[23] = z[23] * z[69];
z[48] = -z[115] + z[162];
z[48] = abb[30] * z[48];
z[23] = z[23] + z[45] + z[48];
z[23] = abb[27] * z[23];
z[14] = -prod_pow(abb[33], 2) * z[14];
z[14] = z[14] + z[113];
z[14] = abb[4] * z[14];
z[45] = z[90] + -z[110];
z[48] = abb[71] * z[45];
z[0] = abb[69] + abb[70] + z[0] + z[1] + z[10] + z[11] + z[13] + z[14] + z[19] + z[21] + z[23] + z[28] + z[29] + z[30] + z[31] + z[35] + z[37] + z[43] + z[44] + z[48] + z[51] + z[54] + z[74] + z[121] + z[129];
z[1] = abb[50] + z[17] + 2 * z[32] + -z[92] + -z[104] + -z[158];
z[1] = abb[17] * z[1];
z[10] = -z[103] + -z[140] + -z[151];
z[10] = z[10] * z[153];
z[11] = z[92] + z[142];
z[13] = -abb[55] + z[11] + -z[18];
z[13] = abb[3] * z[13];
z[14] = 3 * abb[56] + z[16] + -z[142];
z[14] = abb[6] * z[14];
z[17] = 9 * abb[48] + -3 * abb[55] + abb[57] + z[42] + z[143];
z[17] = z[17] * z[73];
z[1] = z[1] + z[10] + z[13] + -z[14] + z[17] + -z[109] + z[116] + -z[150] + z[160];
z[1] = abb[32] * z[1];
z[10] = z[62] + -z[142];
z[13] = -z[10] + -z[67] + -z[82] + -z[124];
z[13] = abb[5] * z[13];
z[17] = z[7] + -z[63] + -z[70] + z[112];
z[17] = z[17] * z[73];
z[18] = -abb[49] + z[47];
z[19] = -z[18] + z[41] + z[112];
z[19] = abb[0] * z[19];
z[21] = abb[3] * z[88];
z[13] = z[13] + z[17] + z[19] + z[21] + -z[79] + -z[95] + -2 * z[108];
z[13] = abb[31] * z[13];
z[17] = -abb[52] + z[107];
z[17] = -3 * abb[54] + 2 * z[17] + z[46] + -z[132] + z[146];
z[17] = abb[14] * z[17];
z[19] = -z[73] * z[156];
z[17] = z[14] + z[17] + z[19] + 4 * z[55] + z[95] + z[98] + z[163];
z[17] = abb[33] * z[17];
z[3] = 3 * z[3] + -z[24] + z[58];
z[10] = abb[56] * (T(-5) / T(2)) + -z[10] + z[76] + -z[101];
z[10] = abb[6] * z[10];
z[16] = -8 * abb[58] + (T(9) / T(2)) * z[16] + z[39] + z[89];
z[16] = abb[5] * z[16];
z[3] = (T(1) / T(2)) * z[3] + z[10] + z[16] + z[22] + z[26] + -z[159];
z[3] = abb[34] * z[3];
z[10] = z[41] + z[104] + -z[119] + -z[144];
z[10] = abb[5] * z[10];
z[8] = z[8] + z[135] + -z[161] + -z[164];
z[8] = z[8] * z[153];
z[8] = z[8] + z[10] + z[14] + z[15] + -z[22] + -z[27];
z[8] = abb[30] * z[8];
z[2] = z[2] + z[42] + z[46] + z[84] + -z[127];
z[2] = abb[34] * z[2];
z[10] = z[20] + -z[34] + z[40] + -z[107] + z[118];
z[10] = abb[30] * z[10];
z[14] = z[77] + -z[92] + z[127];
z[14] = abb[33] * z[14];
z[2] = z[2] + z[10] + z[14] + z[86];
z[2] = abb[0] * z[2];
z[9] = z[9] + -z[64];
z[10] = abb[31] * (T(1) / T(2));
z[9] = z[9] * z[10];
z[10] = z[38] + -z[66];
z[10] = z[4] * z[10];
z[4] = -abb[30] + z[4];
z[4] = abb[20] * z[4];
z[14] = -abb[18] + abb[20];
z[14] = abb[32] * z[14];
z[15] = abb[30] * z[71];
z[4] = z[4] + z[9] + z[10] + z[14] + z[15] + z[49];
z[9] = abb[60] * z[4];
z[10] = z[41] + z[68] + z[80] + z[127] + -z[142];
z[10] = abb[34] * z[10];
z[11] = 6 * abb[51] + -z[11] + z[18] + z[40] + z[50];
z[11] = abb[30] * z[11];
z[7] = abb[47] + -7 * abb[57] + z[7] + z[20] + z[117];
z[7] = abb[33] * z[7];
z[7] = z[7] + z[10] + z[11] + z[85];
z[7] = abb[3] * z[7];
z[10] = 4 * abb[52] + -9 * abb[57] + z[52] + -z[130] + z[136] + -z[141];
z[10] = abb[33] * z[10];
z[11] = abb[30] * z[72];
z[10] = z[10] + z[11] + -z[85] + 2 * z[154];
z[10] = abb[17] * z[10];
z[11] = z[115] + -2 * z[162];
z[11] = abb[27] * z[11];
z[1] = z[1] + z[2] + z[3] + z[7] + z[8] + z[9] + z[10] + z[11] + z[13] + z[17] + -z[75];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[42] * z[6];
z[3] = -abb[39] * z[65];
z[6] = abb[41] * z[36];
z[5] = abb[40] * z[5];
z[4] = m1_set::bc<T>[0] * z[4];
z[7] = abb[43] * (T(1) / T(2));
z[8] = z[7] * z[60];
z[9] = abb[43] * z[59];
z[10] = abb[39] * z[64];
z[11] = abb[38] * z[66];
z[8] = -z[8] + -z[9] + z[10] + z[11];
z[4] = z[4] + -z[8];
z[4] = abb[59] * z[4];
z[9] = abb[43] * z[12];
z[10] = abb[38] * z[33];
z[8] = -abb[60] * z[8];
z[7] = -z[7] * z[25];
z[11] = abb[46] * z[45];
z[1] = abb[44] + abb[45] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_649_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-6.29528515329787257381662736342755648630045211758223188305213529"),stof<T>("-64.523801920969199735358964251936598490033742049441984430025219812")}, std::complex<T>{stof<T>("-33.191100308207320131550318266562191383193779680128316816138219234"),stof<T>("-46.221600699162129032767430418644372766945054376522902069174229204")}, std::complex<T>{stof<T>("12.099491000985158508890548363100308502866350863069668498960914948"),stof<T>("-72.650642308036528454707782944359898074474816556365823300717959959")}, std::complex<T>{stof<T>("-9.556190782033000772068973201238137127998521344493552120569657174"),stof<T>("-89.775795015664713550209997856273018164177651713319197598178604883")}, std::complex<T>{stof<T>("10.808913559091387235236103361320073994482499127648662398962360616"),stof<T>("6.4077927811544311565818295087385280728529920676325660171598369699")}, std::complex<T>{stof<T>("-45.936503567357004834645510728662396876855909934067356986017349428"),stof<T>("33.735508803839325404217870049469907446528133334582252098590365979")}, std::complex<T>{stof<T>("5.988687094436881345811310662738584446203671990155887283914284001"),stof<T>("45.889135226615389161817956217069956419934936560811249088292254641")}, std::complex<T>{stof<T>("-6.9020956721917328427791134245095963619886505098679704905266234863"),stof<T>("11.5377331185761566759916865718435094622925464415053162839469128778")}, std::complex<T>{stof<T>("18.661338235799718608053866232969153407763036212485276977573430121"),stof<T>("-5.819414905679509485406763196621858120264633123452250246685482398")}, std::complex<T>{stof<T>("-0.6687458038860240805037139439018829383487291920268642821032152032"),stof<T>("-4.3436871840284605831045519526160562417641996288129021997010243338")}, std::complex<T>{stof<T>("37.665384064594267310429178539118502960545464236064112453467116866"),stof<T>("17.484370262917336362250816970612986793324841172198201351119013293")}, std::complex<T>{stof<T>("-7.0664435812360512966274921664650092807010245908202515927432043125"),stof<T>("7.8289245255777394387701317963110225412405031692603897496028551735")}, std::complex<T>{stof<T>("20.828671718068656960858043900138353052111968657709009977738752719"),stof<T>("-10.368412862013484354461038273863068126665179185963628602527497006")}, std::complex<T>{stof<T>("20.828671718068656960858043900138353052111968657709009977738752719"),stof<T>("-10.368412862013484354461038273863068126665179185963628602527497006")}, std::complex<T>{stof<T>("8.2392111501723654087725285414709311842632066857929314133244832248"),stof<T>("-2.689250513928415495653135252175836801630141918964692483472961784")}, std::complex<T>{stof<T>("8.2392111501723654087725285414709311842632066857929314133244832248"),stof<T>("-2.689250513928415495653135252175836801630141918964692483472961784")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(abs(k.W[87])) - rlog(abs(kbase.W[87])), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[125].real()/kbase.W[125].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_649_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_649_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(4)) * (-v[3] + v[5]) * (-6 * v[0] + 2 * v[1] + -4 * (2 + v[3]) + 3 * m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (-v[3] + v[5])) / tend;


		return (abb[48] + abb[50] + -abb[55]) * (t * c[0] + c[1]);
	}
	{
T z[3];
z[0] = abb[30] + -abb[34];
z[1] = abb[48] + abb[50] + -abb[55];
z[0] = z[0] * z[1];
z[2] = abb[31] * z[1];
z[2] = -z[0] + z[2];
z[2] = abb[31] * z[2];
z[1] = -abb[32] * z[1];
z[0] = z[0] + z[1];
z[0] = abb[32] * z[0];
z[0] = z[0] + z[2];
return 2 * abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_649_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (-v[3] + v[5])) / tend;


		return (abb[48] + abb[50] + -abb[55]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[31] + -abb[32];
z[1] = abb[48] + abb[50] + -abb[55];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_649_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (v[1] + v[2] + -v[3] + -v[4]) * (-8 + 4 * v[0] + 3 * v[1] + -v[2] + v[3] + v[4] + 2 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 4 * v[5])) / prod_pow(tend, 2);
c[1] = ((1 + m1_set::bc<T>[1]) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[47] + abb[51] + -abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[30] + -abb[31];
z[1] = abb[34] + z[0];
z[1] = abb[34] * z[1];
z[0] = abb[35] + z[0];
z[0] = abb[35] * z[0];
z[0] = -abb[36] + -z[0] + z[1];
z[1] = abb[47] + abb[51] + -abb[58];
return 2 * abb[12] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_649_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (abb[47] + abb[51] + -abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[34] + abb[35];
z[1] = abb[47] + abb[51] + -abb[58];
return 2 * abb[12] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_649_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-126.507991365947897700912162513529227247739084552934068024746558466"),stof<T>("-5.948048920541609705065113771683172881204344917769751691760698988")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 19, 87});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,72> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W20(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W28(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_16(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), T{0}, T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(abs(kend.W[87])) - rlog(abs(k.W[87])), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[125].real()/k.W[125].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k), T{0}, T{0}, T{0}};
abb[44] = SpDLog_f_4_649_W_16_Im(t, path, abb);
abb[45] = SpDLog_f_4_649_W_20_Im(t, path, abb);
abb[46] = SpDLogQ_W_88(k,dl,dlr).imag();
abb[69] = SpDLog_f_4_649_W_16_Re(t, path, abb);
abb[70] = SpDLog_f_4_649_W_20_Re(t, path, abb);
abb[71] = SpDLogQ_W_88(k,dl,dlr).real();

                    
            return f_4_649_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_649_DLogXconstant_part(base_point<T>, kend);
	value += f_4_649_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_649_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_649_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_649_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_649_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_649_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_649_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
