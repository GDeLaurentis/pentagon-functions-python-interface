/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_845.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_845_abbreviated (const std::array<T,95>& abb) {
T z[152];
z[0] = abb[67] + abb[71];
z[1] = abb[68] + abb[70];
z[2] = z[0] + -z[1];
z[3] = abb[30] * z[2];
z[4] = abb[76] + abb[78];
z[5] = abb[13] * z[4];
z[6] = z[3] + z[5];
z[7] = abb[69] + abb[72];
z[1] = z[1] + -z[7];
z[8] = abb[27] * z[1];
z[9] = -abb[76] + abb[78];
z[10] = abb[14] * z[9];
z[11] = z[8] + -z[10];
z[12] = abb[28] * z[1];
z[13] = z[6] + -z[11] + -z[12];
z[14] = z[0] + -z[7];
z[15] = abb[31] * z[14];
z[16] = abb[29] * z[14];
z[17] = 2 * abb[9];
z[18] = z[9] * z[17];
z[19] = 2 * abb[7];
z[20] = abb[78] * z[19];
z[20] = -z[18] + z[20];
z[21] = abb[6] * abb[78];
z[22] = -abb[10] * z[4];
z[22] = -z[21] + z[22];
z[23] = abb[5] * abb[78];
z[24] = 2 * z[23];
z[25] = 2 * abb[76];
z[26] = abb[11] * z[25];
z[27] = abb[12] * z[25];
z[28] = 3 * abb[76];
z[29] = abb[78] + -z[28];
z[29] = abb[4] * z[29];
z[28] = abb[78] + z[28];
z[28] = abb[3] * z[28];
z[22] = z[13] + z[15] + z[16] + z[20] + 2 * z[22] + -z[24] + -z[26] + z[27] + z[28] + z[29];
z[28] = 2 * abb[39];
z[22] = z[22] * z[28];
z[29] = abb[5] * z[9];
z[16] = -z[16] + z[29];
z[30] = -z[15] + z[16];
z[31] = abb[28] * z[2];
z[3] = z[3] + z[31];
z[31] = abb[33] * z[1];
z[32] = abb[16] * z[4];
z[32] = z[31] + z[32];
z[33] = 2 * z[4];
z[34] = abb[10] * z[33];
z[35] = z[32] + -z[34];
z[36] = abb[32] * z[14];
z[37] = abb[17] * z[9];
z[38] = z[36] + z[37];
z[39] = 2 * abb[3];
z[40] = z[4] * z[39];
z[41] = abb[6] * z[4];
z[42] = abb[4] * z[9];
z[40] = z[3] + -z[20] + -z[30] + -z[35] + -z[38] + z[40] + -z[41] + 2 * z[42];
z[43] = 2 * abb[44];
z[40] = z[40] * z[43];
z[44] = 2 * abb[71];
z[45] = -abb[68] + abb[69];
z[46] = 4 * abb[66];
z[47] = -abb[70] + abb[72];
z[48] = -z[44] + -z[45] + z[46] + z[47];
z[49] = abb[31] * z[48];
z[50] = 2 * z[1];
z[50] = abb[30] * z[50];
z[50] = 2 * z[12] + z[50];
z[51] = 3 * abb[78];
z[52] = abb[76] + z[51];
z[52] = abb[6] * z[52];
z[53] = -abb[78] + z[25];
z[54] = abb[4] * z[53];
z[52] = z[49] + z[50] + z[52] + 2 * z[54];
z[55] = 4 * abb[9];
z[56] = z[9] * z[55];
z[57] = 3 * abb[3];
z[58] = -z[19] + -z[57];
z[58] = z[4] * z[58];
z[59] = 4 * abb[76];
z[60] = abb[11] * z[59];
z[59] = abb[12] * z[59];
z[61] = -z[59] + z[60];
z[62] = 2 * abb[0];
z[63] = abb[78] * z[62];
z[64] = z[32] + -z[63];
z[58] = z[11] + z[24] + z[52] + z[56] + z[58] + z[61] + z[64];
z[58] = abb[43] * z[58];
z[65] = z[9] * z[19];
z[66] = -4 * abb[10] + -z[57];
z[66] = z[4] * z[66];
z[67] = z[11] + z[63];
z[52] = z[32] + z[52] + z[65] + z[66] + z[67];
z[52] = abb[42] * z[52];
z[66] = 2 * abb[70];
z[68] = -z[46] + z[66];
z[69] = abb[71] + -abb[72];
z[70] = -abb[67] + abb[69];
z[71] = -z[68] + -3 * z[69] + z[70];
z[72] = abb[29] + abb[31];
z[71] = z[71] * z[72];
z[73] = abb[28] * z[14];
z[74] = -z[38] + z[73];
z[75] = 2 * abb[30];
z[76] = z[14] * z[75];
z[77] = z[74] + z[76];
z[78] = abb[78] + z[25];
z[79] = z[39] * z[78];
z[51] = -abb[76] + z[51];
z[51] = abb[5] * z[51];
z[51] = 3 * z[42] + -z[51] + -z[71] + z[77] + z[79];
z[71] = z[21] + z[34];
z[65] = -z[5] + -z[51] + z[61] + -z[63] + -z[65] + 2 * z[71];
z[65] = abb[38] * z[65];
z[79] = z[5] + -z[63];
z[80] = z[4] * z[19];
z[81] = 2 * z[21];
z[51] = -z[51] + -z[56] + -z[79] + z[80] + z[81];
z[51] = abb[41] * z[51];
z[56] = 2 * abb[69];
z[46] = abb[67] + abb[68] + -abb[70] + -abb[71] + z[46] + -z[56];
z[46] = -z[46] * z[72];
z[80] = 2 * abb[12];
z[82] = abb[4] + -abb[6];
z[83] = -abb[5] + z[82];
z[84] = z[80] + -z[83];
z[85] = 4 * abb[1];
z[86] = abb[3] + z[84] + -z[85];
z[87] = abb[75] + abb[77];
z[86] = z[86] * z[87];
z[88] = 2 * abb[11];
z[89] = z[83] + z[88];
z[90] = -abb[3] + z[89];
z[91] = abb[73] + abb[74];
z[90] = -z[90] * z[91];
z[92] = abb[4] * abb[76];
z[93] = abb[3] * abb[76];
z[94] = z[92] + -z[93];
z[95] = abb[11] * abb[76];
z[96] = z[94] + z[95];
z[46] = z[27] + z[46] + z[86] + z[90] + -2 * z[96];
z[46] = abb[40] * z[46];
z[86] = abb[42] + abb[43];
z[86] = abb[29] * z[48] * z[86];
z[90] = 2 * abb[66];
z[97] = -abb[71] + z[90];
z[98] = z[47] + z[97];
z[99] = abb[29] * z[98];
z[100] = abb[4] * abb[78];
z[101] = -z[23] + -z[99] + z[100];
z[102] = abb[31] * z[98];
z[103] = z[21] + z[102];
z[104] = abb[3] * abb[78];
z[105] = z[101] + -z[103] + z[104];
z[106] = 4 * abb[37];
z[105] = z[105] * z[106];
z[107] = abb[58] + abb[59];
z[108] = abb[56] + -abb[57];
z[109] = z[107] + z[108];
z[110] = abb[27] + abb[28] + z[72];
z[109] = -z[109] * z[110];
z[108] = -z[75] * z[108];
z[107] = -abb[56] + -abb[57] + z[107];
z[107] = abb[34] * z[107];
z[107] = z[107] + z[108] + z[109];
z[107] = m1_set::bc<T>[0] * z[107];
z[108] = 2 * abb[78];
z[109] = abb[5] * abb[42];
z[111] = z[108] * z[109];
z[22] = z[22] + z[40] + 2 * z[46] + z[51] + z[52] + z[58] + z[65] + z[86] + z[105] + 4 * z[107] + z[111];
z[22] = m1_set::bc<T>[0] * z[22];
z[40] = abb[4] * z[4];
z[15] = z[15] + z[40];
z[40] = abb[6] * z[9];
z[46] = -z[15] + z[40];
z[51] = abb[7] * z[9];
z[51] = -z[34] + z[51];
z[52] = abb[30] * z[14];
z[58] = -z[5] + z[16] + z[27] + z[46] + -z[51] + -z[52];
z[65] = 2 * abb[50];
z[86] = -z[58] * z[65];
z[105] = -abb[17] + z[62];
z[107] = abb[13] + z[39];
z[111] = z[105] + -z[107];
z[112] = 2 * abb[6];
z[113] = -z[85] + z[112];
z[114] = -abb[4] + abb[5];
z[115] = 3 * z[114];
z[116] = z[113] + z[115];
z[117] = 6 * abb[7];
z[118] = 4 * abb[12];
z[119] = -z[111] + z[116] + -z[117] + z[118];
z[119] = abb[38] * z[119];
z[120] = z[62] + z[107];
z[121] = -abb[17] + z[19];
z[55] = -z[55] + z[116] + z[120] + -z[121];
z[55] = abb[41] * z[55];
z[116] = abb[7] + -abb[17];
z[122] = -abb[3] + z[17];
z[123] = -z[114] + z[116] + z[122];
z[124] = z[43] * z[123];
z[125] = 2 * abb[1];
z[126] = z[82] + z[125];
z[127] = -abb[5] + z[126];
z[128] = abb[3] + z[127];
z[106] = z[106] * z[128];
z[128] = abb[37] + -abb[39];
z[129] = abb[8] * z[128];
z[55] = z[55] + z[106] + z[124] + -8 * z[129];
z[84] = -5 * abb[7] + z[17] + z[84] + z[107];
z[84] = z[28] * z[84];
z[106] = z[19] + z[127];
z[124] = -abb[0] + z[17] + z[80] + -z[106];
z[124] = abb[43] * z[124];
z[129] = -abb[0] + z[126];
z[129] = abb[42] * z[129];
z[129] = -z[109] + z[129];
z[124] = z[124] + -z[129];
z[84] = z[55] + -z[84] + z[119] + 2 * z[124];
z[84] = m1_set::bc<T>[0] * z[84];
z[119] = abb[7] + -abb[13];
z[80] = z[80] + z[83] + z[119];
z[124] = z[65] * z[80];
z[130] = -abb[3] + abb[9];
z[131] = 4 * z[130];
z[132] = abb[51] * z[131];
z[84] = z[84] + z[124] + -z[132];
z[124] = 2 * abb[22];
z[132] = abb[55] * z[124];
z[132] = -abb[61] + abb[62] + -abb[63] + z[132];
z[133] = -z[84] + -z[132];
z[133] = abb[75] * z[133];
z[134] = abb[7] * z[4];
z[134] = -z[18] + z[134];
z[135] = abb[3] * z[4];
z[136] = z[42] + z[135];
z[30] = z[30] + -z[52] + -z[74] + z[134] + -z[136];
z[52] = -abb[75] * z[123];
z[52] = -z[30] + z[52];
z[52] = abb[52] * z[52];
z[123] = 2 * z[123];
z[137] = abb[52] * z[123];
z[138] = abb[20] + abb[25];
z[139] = -abb[21] + abb[24] + z[138];
z[140] = abb[54] * z[139];
z[132] = abb[64] + -z[132] + -z[137] + z[140];
z[84] = -z[84] + z[132];
z[84] = abb[77] * z[84];
z[137] = 4 * abb[11];
z[111] = -z[19] + -z[111] + z[112] + z[115] + -z[137];
z[111] = abb[38] * z[111];
z[89] = -z[17] + z[89];
z[112] = abb[0] + z[89];
z[112] = abb[43] * z[112];
z[112] = z[112] + z[129];
z[89] = abb[7] + z[89] + -z[107];
z[89] = z[28] * z[89];
z[55] = -z[55] + -z[89] + -z[111] + 2 * z[112];
z[55] = m1_set::bc<T>[0] * z[55];
z[89] = abb[1] + -abb[11];
z[111] = z[89] + -z[130];
z[111] = 4 * z[111];
z[112] = abb[51] * z[111];
z[115] = z[119] + z[127];
z[65] = z[65] * z[115];
z[55] = z[55] + -z[65] + -z[112] + z[132];
z[55] = z[55] * z[91];
z[65] = abb[30] * z[1];
z[12] = z[12] + z[41];
z[65] = z[12] + z[65];
z[51] = -z[32] + -z[51] + -z[65] + z[136];
z[51] = 2 * z[51];
z[112] = abb[53] * z[51];
z[129] = abb[5] * z[4];
z[130] = abb[3] * z[9];
z[132] = z[129] + -z[130];
z[65] = -z[11] + z[26] + -z[65] + -z[132] + z[134];
z[65] = 2 * z[65];
z[134] = abb[51] * z[65];
z[140] = abb[19] + abb[26];
z[140] = z[4] * z[140];
z[141] = abb[22] + abb[23];
z[141] = z[9] * z[141];
z[142] = abb[35] * z[1];
z[140] = z[140] + -z[141] + z[142];
z[141] = -abb[55] * z[140];
z[9] = z[9] * z[138];
z[138] = abb[21] + abb[24];
z[4] = z[4] * z[138];
z[14] = abb[36] * z[14];
z[138] = abb[75] * z[139];
z[4] = -z[4] + z[9] + z[14] + -z[138];
z[9] = -abb[54] * z[4];
z[14] = -abb[94] * z[98];
z[138] = -abb[13] + z[125];
z[142] = -abb[14] + abb[18] + z[138];
z[142] = 4 * z[142];
z[143] = prod_pow(m1_set::bc<T>[0], 2);
z[144] = abb[89] * z[142] * z[143];
z[145] = abb[61] * z[78];
z[146] = -abb[63] * abb[78];
z[147] = -abb[62] * z[53];
z[148] = abb[75] + -abb[78];
z[148] = abb[64] * z[148];
z[9] = z[9] + z[14] + z[22] + 2 * z[52] + z[55] + z[84] + z[86] + z[112] + z[133] + z[134] + z[141] + z[144] + z[145] + z[146] + z[147] + z[148];
z[9] = 2 * z[9];
z[14] = abb[0] + abb[3];
z[22] = z[14] + z[126];
z[22] = abb[42] * z[22];
z[52] = abb[0] + abb[5];
z[52] = abb[43] * z[52];
z[55] = -abb[42] + abb[43];
z[84] = abb[15] * z[55];
z[22] = -z[22] + z[52] + -z[84];
z[52] = -abb[2] + abb[13] + z[57] + -z[116] + z[127];
z[52] = abb[37] * z[52];
z[86] = z[105] + z[107] + -z[114];
z[86] = abb[41] * z[86];
z[107] = abb[13] + z[105] + z[113] + z[114];
z[107] = abb[38] * z[107];
z[112] = -abb[5] + abb[15];
z[14] = 2 * z[14] + -z[112] + -z[119];
z[14] = z[14] * z[28];
z[113] = -abb[4] + abb[15];
z[116] = -abb[7] + -z[105] + z[113];
z[116] = z[43] * z[116];
z[14] = -z[14] + 2 * z[22] + z[52] + -z[86] + z[107] + -z[116];
z[14] = abb[37] * z[14];
z[22] = z[17] + z[62];
z[52] = 2 * abb[17];
z[86] = abb[3] + z[125];
z[107] = -abb[6] + z[86];
z[116] = -z[19] + -z[22] + z[52] + z[107] + z[114];
z[116] = abb[38] * z[116];
z[119] = -z[62] + z[122];
z[127] = z[119] + z[127];
z[127] = abb[41] * z[127];
z[133] = z[119] + -z[126];
z[134] = abb[42] * z[133];
z[133] = -abb[5] + z[133];
z[141] = abb[43] * z[133];
z[144] = 2 * z[84] + z[109];
z[141] = z[141] + z[144];
z[145] = z[112] + z[119];
z[145] = abb[44] * z[145];
z[116] = z[116] + -z[127] + -z[134] + z[141] + z[145];
z[116] = abb[44] * z[116];
z[119] = z[119] + z[126];
z[119] = abb[42] * z[119];
z[119] = -z[109] + z[119];
z[119] = abb[41] * z[119];
z[127] = -abb[2] + abb[7];
z[134] = 2 * abb[8];
z[145] = -abb[3] + -z[113] + z[127] + z[134];
z[146] = 2 * abb[48];
z[145] = z[145] * z[146];
z[123] = abb[81] * z[123];
z[105] = abb[2] + z[105] + -z[112];
z[147] = 2 * abb[45];
z[105] = z[105] * z[147];
z[148] = abb[41] + abb[42];
z[149] = z[28] + z[148];
z[149] = -3 * abb[37] + 2 * z[149];
z[149] = abb[37] * z[149];
z[148] = abb[39] + 2 * z[148];
z[148] = abb[39] * z[148];
z[148] = -z[148] + z[149];
z[148] = z[134] * z[148];
z[113] = abb[2] + 4 * abb[8] + z[113] + -z[120];
z[120] = 2 * abb[46];
z[113] = z[113] * z[120];
z[149] = z[122] + -z[126];
z[149] = abb[42] * z[149];
z[149] = z[109] + z[149];
z[149] = abb[43] * z[149];
z[150] = abb[92] + abb[93];
z[151] = abb[90] + -abb[91];
z[14] = z[14] + -z[105] + -z[113] + z[116] + z[119] + -z[123] + z[145] + z[148] + -z[149] + z[150] + z[151];
z[105] = abb[84] * z[124];
z[113] = abb[83] * z[139];
z[105] = z[14] + -z[105] + z[113];
z[17] = z[17] + -z[62];
z[62] = 2 * abb[13];
z[57] = z[17] + -z[57] + -z[62] + z[106];
z[57] = abb[41] * z[57];
z[22] = abb[3] + z[22] + z[126];
z[22] = abb[42] * z[22];
z[22] = z[22] + -z[57];
z[57] = -abb[15] + -z[17] + z[39];
z[106] = z[57] + z[126];
z[106] = abb[39] * z[106];
z[83] = z[17] + -z[83] + z[86];
z[113] = z[83] + -z[137];
z[116] = abb[38] * z[113];
z[106] = z[22] + z[106] + z[116] + z[141];
z[106] = abb[39] * z[106];
z[116] = -abb[6] + -2 * z[114] + z[121] + z[122] + z[138];
z[116] = abb[41] * z[116];
z[113] = abb[43] * z[113];
z[113] = z[113] + -z[116];
z[113] = abb[38] * z[113];
z[112] = abb[6] + z[112];
z[119] = -z[112] + z[125] + -z[127];
z[121] = 2 * abb[47];
z[119] = z[119] * z[121];
z[52] = 7 * abb[3] + -13 * abb[4] + 11 * abb[6] + z[52] + z[62];
z[62] = 4 * abb[7];
z[122] = -8 * abb[1] + 13 * abb[5] + -14 * abb[11] + z[52] + -z[62];
z[123] = (T(1) / T(3)) * z[143];
z[122] = z[122] * z[123];
z[111] = abb[80] * z[111];
z[106] = -z[105] + -z[106] + z[111] + z[113] + z[119] + -z[122];
z[106] = z[91] * z[106];
z[111] = abb[66] + z[47];
z[111] = -abb[71] + z[45] + 2 * z[111];
z[111] = -z[72] * z[111];
z[113] = abb[4] * z[78];
z[119] = -abb[3] * z[53];
z[13] = -z[13] + -z[21] + -z[23] + z[32] + z[38] + z[111] + z[113] + z[119];
z[13] = abb[37] * z[13];
z[111] = abb[29] * z[2];
z[113] = abb[3] + -abb[4];
z[113] = z[25] * z[113];
z[6] = z[6] + -z[12] + -z[29] + -z[67] + z[111] + z[113];
z[6] = z[6] * z[28];
z[12] = abb[76] * z[85];
z[29] = z[12] + z[32];
z[85] = z[67] + z[130];
z[113] = -abb[4] * z[108];
z[40] = z[29] + z[40] + z[49] + -z[85] + z[113];
z[40] = abb[42] * z[40];
z[49] = abb[31] * z[1];
z[41] = -z[41] + z[49];
z[24] = z[24] + -z[29] + -z[41] + z[67] + -z[135];
z[24] = abb[43] * z[24];
z[29] = abb[15] * z[25];
z[29] = -z[12] + z[29];
z[49] = z[29] + -z[74] + z[79];
z[67] = z[68] + z[69] + z[70];
z[68] = -abb[29] * z[67];
z[39] = -abb[78] * z[39];
z[39] = -z[15] + z[39] + z[49] + z[68] + z[129];
z[39] = abb[41] * z[39];
z[67] = -abb[31] * z[67];
z[42] = z[16] + -z[42] + -z[49] + z[67] + z[81];
z[42] = abb[38] * z[42];
z[48] = abb[43] * z[48];
z[49] = -abb[42] * z[1];
z[48] = z[48] + z[49];
z[48] = abb[29] * z[48];
z[38] = z[38] + z[64];
z[49] = abb[31] * z[2];
z[49] = z[3] + z[49];
z[64] = -z[38] + z[49] + z[136];
z[43] = z[43] * z[64];
z[64] = -abb[76] * z[55];
z[67] = 2 * abb[15];
z[67] = z[64] * z[67];
z[6] = z[6] + z[13] + z[24] + z[39] + z[40] + z[42] + z[43] + z[48] + -z[67];
z[6] = abb[37] * z[6];
z[13] = z[19] + z[57] + z[82];
z[13] = abb[39] * z[13];
z[19] = -z[62] + z[118];
z[24] = z[19] + z[133];
z[24] = abb[43] * z[24];
z[39] = -z[62] + z[83];
z[39] = abb[38] * z[39];
z[13] = z[13] + z[22] + z[24] + z[39] + z[144];
z[13] = abb[39] * z[13];
z[22] = abb[3] + abb[5];
z[17] = z[17] + z[19] + z[22] + -z[126];
z[17] = abb[43] * z[17];
z[17] = z[17] + -z[116];
z[17] = abb[38] * z[17];
z[19] = 22 * abb[1] + -z[52];
z[24] = abb[12] * (T(14) / T(3));
z[19] = abb[5] * (T(-13) / T(3)) + (T(1) / T(3)) * z[19] + -z[24] + z[117];
z[19] = z[19] * z[143];
z[39] = abb[80] * z[131];
z[13] = -z[13] + z[17] + z[19] + -z[39];
z[14] = z[13] + -z[14];
z[14] = abb[75] * z[14];
z[17] = abb[2] + abb[7];
z[19] = z[17] + -z[112];
z[39] = z[19] * z[121];
z[13] = z[13] + z[39] + -z[105];
z[13] = abb[77] * z[13];
z[39] = abb[76] * z[125];
z[40] = z[39] + z[92];
z[42] = -abb[70] + z[90];
z[43] = z[42] + -z[70];
z[48] = abb[31] * z[43];
z[52] = abb[15] * abb[76];
z[57] = abb[6] * abb[76];
z[27] = -z[27] + z[40] + z[48] + -z[52] + z[57];
z[27] = abb[38] * z[27];
z[62] = abb[42] * z[126];
z[68] = z[107] + -z[127];
z[74] = abb[37] * z[68];
z[79] = z[128] * z[134];
z[62] = z[62] + -z[74] + z[79] + z[84];
z[74] = abb[3] + -abb[7] + abb[12];
z[74] = z[28] * z[74];
z[79] = -abb[1] + abb[12];
z[81] = 2 * abb[38];
z[79] = z[79] * z[81];
z[81] = abb[43] * z[22];
z[74] = z[62] + z[74] + -z[79] + -z[81];
z[74] = z[74] * z[87];
z[79] = -abb[5] + -z[86] + z[88];
z[79] = abb[43] * z[79];
z[81] = abb[3] + z[89];
z[81] = z[28] * z[81];
z[62] = z[62] + z[79] + z[81];
z[62] = z[62] * z[91];
z[79] = -z[3] + z[57] + z[94];
z[43] = abb[29] * z[43];
z[81] = abb[5] * abb[76];
z[43] = z[43] + z[81];
z[48] = -z[43] + -z[48] + z[79];
z[82] = abb[37] * z[48];
z[45] = z[45] + -z[97];
z[83] = abb[31] * z[45];
z[40] = z[40] + -z[57] + -z[83];
z[40] = abb[42] * z[40];
z[39] = z[39] + z[93];
z[26] = z[26] + -z[39] + -z[81];
z[26] = abb[43] * z[26];
z[39] = -z[39] + z[43] + z[52];
z[39] = abb[41] * z[39];
z[43] = abb[29] * z[45];
z[45] = abb[43] * z[43];
z[52] = abb[12] * abb[76];
z[52] = z[52] + -z[96];
z[28] = z[28] * z[52];
z[52] = -abb[15] * z[64];
z[26] = z[26] + z[27] + z[28] + z[39] + z[40] + -z[45] + z[52] + z[62] + z[74] + z[82];
z[17] = z[17] + z[114] + -z[125];
z[17] = z[17] * z[87];
z[27] = -z[114] + z[127];
z[27] = -z[27] * z[91];
z[17] = z[17] + z[27] + z[43] + -z[79] + z[81] + z[83];
z[17] = abb[40] * z[17];
z[17] = z[17] + 2 * z[26];
z[17] = abb[40] * z[17];
z[26] = 13 * abb[78];
z[27] = z[25] + z[26];
z[27] = abb[6] * z[27];
z[28] = 20 * abb[76];
z[39] = -z[26] + z[28];
z[39] = abb[4] * z[39];
z[28] = -z[26] + -z[28];
z[28] = abb[3] * z[28];
z[26] = -z[25] + z[26];
z[26] = abb[5] * z[26];
z[37] = 2 * z[37];
z[10] = 2 * z[10];
z[40] = abb[13] * z[33];
z[26] = -z[10] + z[26] + z[27] + z[28] + z[37] + z[39] + -z[40] + 14 * z[95];
z[27] = 2 * abb[68];
z[0] = -z[0] + -z[7] + z[27] + z[66];
z[7] = 2 * abb[86];
z[0] = (T(1) / T(3)) * z[0] + -z[7];
z[0] = abb[28] * z[0];
z[28] = (T(1) / T(3)) * z[1] + -z[7];
z[28] = abb[27] * z[28];
z[0] = z[0] + z[28];
z[2] = (T(-1) / T(3)) * z[2] + -z[7];
z[2] = abb[30] * z[2];
z[7] = abb[34] + z[75] + z[110];
z[7] = abb[85] * z[7];
z[2] = z[2] + z[7];
z[7] = z[32] + z[36];
z[28] = -abb[67] + abb[68];
z[32] = 4 * abb[86];
z[28] = 3 * abb[70] + abb[66] * (T(-22) / T(3)) + abb[72] * (T(-11) / T(3)) + abb[71] * (T(13) / T(3)) + (T(-2) / T(3)) * z[28] + z[32];
z[28] = -z[28] * z[72];
z[39] = abb[87] + abb[88];
z[39] = 4 * z[39];
z[43] = -abb[34] + z[110];
z[39] = z[39] * z[43];
z[32] = abb[34] * z[32];
z[24] = -abb[76] * z[24];
z[43] = abb[60] * z[142];
z[0] = 2 * z[0] + 4 * z[2] + (T(2) / T(3)) * z[7] + z[24] + (T(1) / T(3)) * z[26] + z[28] + z[32] + z[39] + z[43];
z[0] = z[0] * z[143];
z[2] = abb[7] * z[25];
z[2] = z[2] + -z[18];
z[7] = z[2] + -z[50];
z[18] = abb[4] + abb[6];
z[18] = z[18] * z[78];
z[24] = z[34] + -z[63];
z[26] = abb[3] * z[78];
z[8] = -z[7] + 2 * z[8] + -z[10] + -z[12] + z[18] + -z[24] + -z[26] + -z[102];
z[8] = abb[42] * z[8];
z[10] = -z[20] + z[24];
z[18] = z[54] + z[103];
z[20] = abb[5] * z[78];
z[20] = z[10] + z[12] + z[18] + z[20] + -z[59] + -z[104];
z[20] = abb[43] * z[20];
z[28] = 2 * abb[67] + z[42] + -z[56] + z[69];
z[32] = abb[29] * z[28];
z[32] = -z[18] + z[29] + z[32];
z[34] = z[2] + z[34] + z[63];
z[22] = -z[22] * z[53];
z[22] = z[22] + -z[32] + z[34] + -z[40] + -z[76];
z[22] = abb[41] * z[22];
z[1] = abb[29] * z[1];
z[39] = z[1] + -z[10] + -z[46] + -z[132];
z[39] = abb[39] * z[39];
z[27] = z[27] + -z[47] + -z[56] + z[97];
z[40] = abb[29] * z[55];
z[43] = -z[27] * z[40];
z[28] = abb[31] * z[28];
z[28] = z[28] + -z[29] + z[101];
z[29] = -z[10] + z[26];
z[46] = -abb[6] * z[53];
z[46] = -z[28] + -z[29] + z[46] + z[60];
z[46] = abb[38] * z[46];
z[47] = abb[78] * z[109];
z[52] = z[47] + z[67];
z[8] = z[8] + z[20] + z[22] + z[39] + z[43] + z[46] + z[52];
z[8] = abb[39] * z[8];
z[20] = abb[31] * z[27];
z[12] = z[12] + z[20];
z[20] = abb[16] * z[33];
z[20] = z[12] + z[20] + z[21] + -z[23] + 2 * z[31] + -z[34] + z[50] + z[54] + -z[104];
z[20] = abb[43] * z[20];
z[22] = -z[36] + z[73];
z[2] = z[2] + -z[26];
z[21] = z[2] + -z[21] + -2 * z[22] + z[24] + -z[28] + z[37] + -z[76];
z[21] = abb[38] * z[21];
z[22] = z[10] + z[26];
z[24] = abb[6] * z[78];
z[12] = -z[12] + -z[22] + z[24] + -z[100];
z[12] = abb[42] * z[12];
z[24] = -abb[5] * z[53];
z[24] = -z[10] + z[24] + -z[32] + -z[104];
z[24] = abb[41] * z[24];
z[16] = -z[16] + z[41];
z[10] = z[10] + z[16] + z[136];
z[10] = abb[44] * z[10];
z[26] = -z[40] * z[98];
z[10] = z[10] + z[12] + z[20] + z[21] + z[24] + z[26] + z[52];
z[10] = abb[44] * z[10];
z[12] = abb[81] * z[30];
z[20] = z[80] * z[87];
z[21] = z[91] * z[115];
z[20] = z[20] + z[21] + z[58];
z[20] = abb[79] * z[20];
z[21] = -z[87] + -z[91];
z[24] = z[68] + -z[134];
z[21] = z[21] * z[24];
z[21] = z[21] + z[48];
z[21] = abb[49] * z[21];
z[12] = z[12] + z[20] + z[21];
z[20] = abb[3] + -abb[6];
z[21] = abb[76] + z[108];
z[20] = z[20] * z[21];
z[7] = z[7] + -z[11] + z[20] + -z[35] + -z[54] + z[83];
z[7] = abb[42] * z[7];
z[7] = z[7] + -z[47];
z[7] = abb[43] * z[7];
z[11] = -2 * abb[72] + -z[42] + z[44] + -z[70];
z[11] = z[11] * z[72];
z[20] = -abb[76] + z[108];
z[20] = -z[20] * z[114];
z[2] = -z[2] + z[5] + z[11] + z[20] + -z[71] + z[77];
z[2] = abb[41] * z[2];
z[11] = z[18] + z[99];
z[18] = -z[11] + -z[23] + z[29] + -z[61];
z[18] = abb[43] * z[18];
z[2] = z[2] + z[18];
z[2] = abb[38] * z[2];
z[18] = -abb[82] * z[51];
z[20] = -abb[80] * z[65];
z[1] = z[1] + -z[5] + z[15] + z[73] + z[85];
z[1] = z[1] * z[120];
z[5] = abb[5] + -abb[6];
z[5] = z[5] * z[25];
z[15] = abb[75] * z[19];
z[3] = z[3] + z[5] + z[15] + z[111];
z[3] = z[3] * z[121];
z[4] = abb[83] * z[4];
z[5] = -z[11] + z[22];
z[5] = abb[42] * z[5];
z[5] = z[5] + -z[47];
z[5] = abb[41] * z[5];
z[11] = z[16] + z[38];
z[11] = z[11] * z[147];
z[15] = abb[75] * z[124];
z[15] = z[15] + z[140];
z[15] = abb[84] * z[15];
z[16] = z[49] + -2 * z[94];
z[16] = z[16] * z[146];
z[19] = -z[25] * z[151];
z[21] = abb[42] * z[45];
z[22] = -abb[65] * z[98];
z[23] = -abb[90] + -abb[91] + z[150];
z[23] = abb[78] * z[23];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[10] + z[11] + 2 * z[12] + z[13] + z[14] + z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + z[23] + z[106];
z[0] = 2 * z[0];
return {z[9], z[0]};
}


template <typename T> std::complex<T> f_4_845_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("65.16124175127668153126058446024402888911289348178807025267998752"),stof<T>("187.82926484472532126748614561461626579056996112332777451354691725")}, std::complex<T>{stof<T>("-5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("-5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("10.060152217277230393536793041756720365371175384591388288842506102"),stof<T>("-10.099388811699551455892547158001484358148356997344369540526954736")}, std::complex<T>{stof<T>("-37.610696984276955962398688751000374627242034433189729270761246812"),stof<T>("-88.864938016512884905796799228307390716210802062991702486509981258")}, std::complex<T>{stof<T>("-37.610696984276955962398688751000374627242034433189729270761246812"),stof<T>("-88.864938016512884905796799228307390716210802062991702486509981258")}, std::complex<T>{stof<T>("42.640773092915571159167085271878734809927622125485423415182499863"),stof<T>("83.815243610663109177850525649306648537136623564319517716246503889")}, std::complex<T>{stof<T>("99.710178044973750252386082667054357175919656034901473263628816938"),stof<T>("-15.052445305866626618174960633655949945311434730448914446779643992")}, std::complex<T>{stof<T>("99.710178044973750252386082667054357175919656034901473263628816938"),stof<T>("-15.052445305866626618174960633655949945311434730448914446779643992")}, std::complex<T>{stof<T>("88.438761380254411799178704222807150816421330713990782794771155063"),stof<T>("-29.3588069102377698164638159932981645214751773211859412438357027")}, std::complex<T>{stof<T>("88.438761380254411799178704222807150816421330713990782794771155063"),stof<T>("-29.3588069102377698164638159932981645214751773211859412438357027")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_845_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_845_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("49.499357542415443179582979236732505355273196721728966272143772872"),stof<T>("-50.088749805493674438732238674734619143005036603016267680525524149")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({200});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,95> abb = {dl[0], dl[1], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W15(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_9_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), f_2_25_im(k), (log2_im(k.W[164]) - log2_im(base_point<T>.W[164])), (log2_im(k.W[166]) - log2_im(base_point<T>.W[166])), (log2_im(k.W[170]) - log2_im(base_point<T>.W[170])), (log2_im(k.W[172]) - log2_im(base_point<T>.W[172])), (log3_im(k.W[190]) - log3_im(base_point<T>.W[190])), T{0}, T{0}, T{0}, T{0}, T{0}, rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_6_re(k), f_2_9_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k), f_2_25_re(k), (log2_re(k.W[164]) - log2_re(base_point<T>.W[164])), (log2_re(k.W[166]) - log2_re(base_point<T>.W[166])), (log2_re(k.W[170]) - log2_re(base_point<T>.W[170])), (log2_re(k.W[172]) - log2_re(base_point<T>.W[172])), (log3_re(k.W[190]) - log3_re(base_point<T>.W[190])), T{0}, T{0}, T{0}, T{0}, T{0}};
{
auto c = dlog_W165(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[90] = c.real();
abb[61] = c.imag();
SpDLog_Sigma5<T,200,164>(k, dv, abb[90], abb[61], f_2_32_series_coefficients<T>);
}
{
auto c = dlog_W167(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[91] = c.real();
abb[62] = c.imag();
SpDLog_Sigma5<T,200,166>(k, dv, abb[91], abb[62], f_2_32_series_coefficients<T>);
}
{
auto c = dlog_W171(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[92] = c.real();
abb[63] = c.imag();
SpDLog_Sigma5<T,200,170>(k, dv, abb[92], abb[63], f_2_32_series_coefficients<T>);
}
{
auto c = dlog_W173(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[93] = c.real();
abb[64] = c.imag();
SpDLog_Sigma5<T,200,172>(k, dv, abb[93], abb[64], f_2_32_series_coefficients<T>);
}
{
auto c = dlog_W191(k,dv) * (f_2_32(k) + 4 * prod_pow(m1_set::bc<T>[0], 2));
abb[94] = c.real();
abb[65] = c.imag();
SpDLog_Sigma5<T,200,190>(k, dv, abb[94], abb[65], f_2_32_series_coefficients<T>);
}

                    
            return f_4_845_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_845_DLogXconstant_part(base_point<T>, kend);
	value += f_4_845_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_845_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_845_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_845_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_845_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_845_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_845_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
