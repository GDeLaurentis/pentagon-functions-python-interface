/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_546.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_546_abbreviated (const std::array<T,31>& abb) {
T z[38];
z[0] = abb[21] + abb[23];
z[1] = 2 * abb[20] + abb[22];
z[2] = 2 * z[1];
z[3] = abb[25] + z[0] + -z[2];
z[4] = -abb[6] * z[3];
z[5] = 2 * z[4];
z[6] = 2 * abb[23];
z[7] = abb[21] + z[6];
z[8] = -abb[19] + z[7];
z[9] = -z[1] + z[8];
z[9] = abb[0] * z[9];
z[10] = 2 * abb[21] + -5 * z[1];
z[10] = abb[25] + (T(1) / T(3)) * z[10];
z[11] = abb[23] * (T(2) / T(3));
z[12] = abb[19] * (T(-4) / T(3)) + -z[10] + z[11];
z[12] = abb[3] * z[12];
z[10] = -z[10] + -z[11];
z[10] = abb[4] * z[10];
z[11] = abb[21] * (T(1) / T(2));
z[13] = z[2] + -z[11];
z[14] = 2 * abb[25];
z[13] = abb[23] * (T(-4) / T(3)) + abb[24] * (T(-1) / T(2)) + (T(5) / T(3)) * z[13] + -z[14];
z[13] = abb[1] * z[13];
z[15] = -abb[26] + abb[27];
z[16] = abb[7] * z[15];
z[17] = 3 * z[16];
z[18] = abb[2] * abb[24];
z[19] = abb[2] * abb[19];
z[20] = abb[21] * (T(-11) / T(2)) + 7 * z[1];
z[20] = -abb[25] + abb[23] * (T(-8) / T(3)) + (T(1) / T(3)) * z[20];
z[20] = abb[2] * z[20];
z[10] = -z[5] + (T(13) / T(3)) * z[9] + 4 * z[10] + z[12] + z[13] + -z[17] + (T(1) / T(2)) * z[18] + (T(4) / T(3)) * z[19] + z[20];
z[12] = prod_pow(m1_set::bc<T>[0], 2);
z[10] = z[10] * z[12];
z[13] = 4 * z[1];
z[20] = 3 * abb[25];
z[21] = z[13] + -z[20];
z[22] = -abb[21] + z[21];
z[23] = -abb[19] + z[22];
z[23] = abb[3] * z[23];
z[24] = 3 * abb[23];
z[25] = -abb[21] + z[1];
z[26] = -z[24] + 2 * z[25];
z[26] = abb[2] * z[26];
z[27] = -abb[4] * z[3];
z[28] = -z[0] + z[21];
z[29] = abb[1] * z[28];
z[30] = 2 * z[29];
z[31] = z[19] + z[30];
z[4] = 3 * z[4];
z[9] = z[4] + -4 * z[9] + z[17] + -z[23] + -z[26] + -3 * z[27] + -z[31];
z[17] = prod_pow(abb[11], 2);
z[23] = z[9] * z[17];
z[26] = 4 * abb[1];
z[26] = z[26] * z[28];
z[4] = z[4] + -z[26];
z[26] = abb[19] * (T(1) / T(2));
z[27] = -abb[23] + z[26];
z[32] = -z[11] + z[27];
z[33] = abb[25] * (T(3) / T(2)) + -z[2];
z[34] = -z[32] + z[33];
z[34] = abb[0] * z[34];
z[16] = (T(-3) / T(2)) * z[16] + -z[19] + z[34];
z[19] = 8 * z[1];
z[32] = abb[25] * (T(15) / T(2)) + -z[19] + -z[32];
z[32] = abb[3] * z[32];
z[20] = z[2] + -z[20];
z[7] = z[7] + z[20];
z[7] = abb[2] * z[7];
z[34] = z[0] + z[20];
z[35] = 2 * abb[4];
z[35] = z[34] * z[35];
z[7] = z[4] + z[7] + z[16] + z[32] + z[35];
z[32] = abb[11] * z[7];
z[2] = abb[21] + z[2];
z[6] = z[2] + -z[6];
z[6] = abb[2] * z[6];
z[36] = 2 * abb[3];
z[37] = z[34] * z[36];
z[6] = z[6] + -3 * z[18] + z[30] + -z[35] + z[37];
z[6] = abb[12] * z[6];
z[30] = -z[11] + -z[26] + -z[33];
z[30] = abb[3] * z[30];
z[11] = z[1] + z[11];
z[33] = abb[2] * z[11];
z[35] = (T(3) / T(2)) * z[18];
z[30] = z[16] + z[29] + z[30] + z[33] + -z[35];
z[30] = abb[13] * z[30];
z[6] = z[6] + z[30] + z[32];
z[6] = abb[13] * z[6];
z[19] = abb[25] * (T(-9) / T(2)) + abb[21] * (T(-7) / T(2)) + z[19] + -z[24] + -z[26];
z[19] = abb[3] * z[19];
z[20] = -abb[21] + -z[20];
z[20] = abb[2] * z[20];
z[16] = -z[4] + z[16] + z[19] + z[20];
z[16] = abb[13] * z[16];
z[19] = abb[2] + abb[4];
z[19] = z[19] * z[34];
z[20] = z[28] * z[36];
z[4] = z[4] + z[19] + -z[20];
z[19] = 2 * z[4];
z[20] = abb[12] * z[19];
z[22] = abb[2] * z[22];
z[8] = -z[8] + z[21];
z[8] = abb[0] * z[8];
z[21] = abb[19] + -z[25];
z[21] = abb[3] * z[21];
z[8] = z[8] + 4 * z[21] + z[22] + -z[31];
z[8] = abb[14] * z[8];
z[8] = z[8] + z[16] + z[20] + -z[32];
z[8] = abb[14] * z[8];
z[16] = -abb[11] * z[19];
z[20] = 9 * abb[25];
z[0] = -z[0] + 10 * z[1] + -z[20];
z[21] = -abb[3] + abb[4];
z[0] = z[0] * z[21];
z[11] = -abb[23] + z[11];
z[11] = abb[2] * z[11];
z[13] = abb[21] + -z[13];
z[13] = 11 * abb[23] + 6 * abb[24] + 5 * z[13] + z[20];
z[13] = abb[1] * z[13];
z[0] = z[0] + z[11] + z[13] + -z[35];
z[0] = abb[12] * z[0];
z[0] = z[0] + z[16];
z[0] = abb[12] * z[0];
z[11] = -abb[21] + abb[25];
z[11] = (T(1) / T(2)) * z[11] + z[27];
z[11] = abb[7] * z[11];
z[13] = -abb[2] + 2 * abb[10] + abb[3] * (T(-3) / T(2));
z[13] = z[13] * z[15];
z[16] = -abb[9] * z[3];
z[20] = (T(3) / T(2)) * z[15];
z[20] = abb[0] * z[20];
z[11] = z[11] + -z[13] + z[16] + z[20];
z[1] = -abb[25] + z[1];
z[13] = abb[8] * z[1];
z[13] = -z[11] + 2 * z[13];
z[13] = abb[29] * z[13];
z[16] = -prod_pow(abb[14], 2);
z[12] = -z[12] + z[16] + z[17];
z[12] = abb[8] * z[12] * z[15];
z[12] = z[12] + z[13];
z[2] = -z[2] + z[14];
z[2] = abb[2] * z[2];
z[3] = -z[3] * z[36];
z[2] = z[2] + z[3] + -z[5] + z[18];
z[2] = 3 * z[2] + 6 * z[29];
z[2] = abb[15] * z[2];
z[3] = -abb[28] * z[19];
z[0] = abb[30] + z[0] + z[2] + z[3] + z[6] + z[8] + z[10] + 3 * z[12] + z[23];
z[2] = -abb[11] * z[9];
z[3] = abb[12] * z[4];
z[2] = z[2] + z[3];
z[3] = -abb[13] + abb[14];
z[3] = z[3] * z[7];
z[2] = 2 * z[2] + z[3];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = abb[17] * z[11];
z[4] = -abb[16] * z[19];
z[1] = abb[17] * z[1];
z[5] = -abb[11] * m1_set::bc<T>[0] * z[15];
z[1] = z[1] + z[5];
z[1] = abb[8] * z[1];
z[1] = abb[18] + 6 * z[1] + z[2] + -3 * z[3] + z[4];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_546_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("10.136426419815678412697177642306140753505640737452435483270761744"),stof<T>("49.413046312118955875886141470692412640741316153393515836844825111")}, std::complex<T>{stof<T>("-6.227444306250794676779768920348826742376437561684787019366391816"),stof<T>("38.096154908035289683399412155557973913452570247365415204412138984")}, std::complex<T>{stof<T>("41.04917951169013373346481824034043736439385857123899270959413576"),stof<T>("-35.238265216236073395732605540007005411479415806327627775036051924")}, std::complex<T>{stof<T>("-3.113722153125397338389884460174413371188218780842393509683195908"),stof<T>("19.048077454017644841699706077778986956726285123682707602206069492")}, std::complex<T>{stof<T>("30.912753091874455320767640598034296610888217833786557226323374016"),stof<T>("-84.651311528355029271618747010699418052220731959721143611880877036")}, std::complex<T>{stof<T>("-37.935457358564736395074933780166023993205639790396599199910939852"),stof<T>("16.190187762218428554032899462228018454753130682644920172829982432")}, std::complex<T>{stof<T>("27.92831286295017653844769954488855020241430807694077213756770908"),stof<T>("-40.739096344846029908780604306268048317199884196507126393435065635")}, std::complex<T>{stof<T>("-27.92831286295017653844769954488855020241430807694077213756770908"),stof<T>("40.739096344846029908780604306268048317199884196507126393435065635")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[127].real()/kbase.W[127].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_546_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_546_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(4)) * (v[2] + v[3]) * (8 + -v[2] + -v[3] + 4 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 4 * v[5])) / prod_pow(tend, 2);
c[1] = (-6 * (1 + 2 * m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;


		return (4 * abb[20] + -abb[21] + 2 * abb[22] + abb[24] + -2 * abb[25]) * (t * c[0] + c[1]) * (T(1) / T(4));
	}
	{
T z[5];
z[0] = -2 * abb[20] + -abb[22] + abb[25];
z[1] = abb[21] * (T(1) / T(2)) + z[0];
z[2] = prod_pow(abb[13], 2);
z[3] = z[1] * z[2];
z[0] = abb[21] + 2 * z[0];
z[4] = abb[13] * z[0];
z[1] = abb[12] * z[1];
z[1] = -3 * z[1] + z[4];
z[1] = abb[12] * z[1];
z[4] = -abb[13] + abb[12] * (T(3) / T(2));
z[4] = abb[12] * z[4];
z[2] = (T(-1) / T(2)) * z[2] + z[4];
z[2] = abb[24] * z[2];
z[0] = abb[24] + -z[0];
z[0] = abb[15] * z[0];
z[0] = z[0] + z[1] + z[2] + z[3];
return 3 * abb[5] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_546_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_546_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("21.459621077509670590233937235353460125442031547687652524297715566"),stof<T>("-33.422100577583928921598840244875571023198752041568562645819411427")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,31> abb = {dl[0], dl[5], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W15(k,dl), dlog_W19(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_13(k), f_2_4_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[14].real()/k.W[14].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[127].real()/k.W[127].real()), f_2_4_re(k), f_2_25_re(k), T{0}};
abb[18] = SpDLog_f_4_546_W_19_Im(t, path, abb);
abb[30] = SpDLog_f_4_546_W_19_Re(t, path, abb);

                    
            return f_4_546_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_546_DLogXconstant_part(base_point<T>, kend);
	value += f_4_546_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_546_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_546_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_546_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_546_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_546_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_546_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
