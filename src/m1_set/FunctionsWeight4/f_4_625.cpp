/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_625.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_625_abbreviated (const std::array<T,76>& abb) {
T z[169];
z[0] = abb[46] + abb[49];
z[1] = -abb[48] + z[0];
z[2] = abb[53] * (T(1) / T(2));
z[3] = abb[51] + -abb[55];
z[1] = abb[50] + (T(1) / T(2)) * z[1] + -z[2] + -z[3];
z[1] = abb[13] * z[1];
z[4] = abb[49] + abb[50];
z[5] = abb[47] + -abb[55] + z[4];
z[6] = abb[56] * (T(1) / T(2));
z[5] = abb[53] + (T(1) / T(2)) * z[5] + -z[6];
z[5] = abb[9] * z[5];
z[7] = -abb[46] + abb[52];
z[8] = (T(1) / T(2)) * z[7];
z[9] = abb[57] * (T(1) / T(2));
z[10] = -abb[48] + -z[8] + z[9];
z[10] = abb[10] * z[10];
z[1] = z[1] + z[5] + z[10];
z[5] = abb[47] + abb[49] + -abb[51];
z[10] = abb[56] + abb[57];
z[11] = -abb[52] + z[10];
z[11] = -abb[58] + (T(11) / T(12)) * z[11];
z[12] = 2 * abb[54];
z[13] = -abb[46] + z[12];
z[14] = abb[50] * (T(7) / T(4));
z[5] = abb[48] * (T(-7) / T(6)) + (T(5) / T(6)) * z[5] + -z[11] + z[13] + z[14];
z[5] = abb[7] * z[5];
z[10] = (T(1) / T(12)) * z[10];
z[15] = abb[52] * (T(1) / T(4));
z[16] = abb[51] * (T(1) / T(6)) + z[15];
z[17] = abb[50] * (T(3) / T(4));
z[18] = -abb[58] + z[0];
z[19] = -z[10] + z[16] + -z[17] + (T(-1) / T(3)) * z[18];
z[19] = abb[6] * z[19];
z[20] = abb[52] * (T(1) / T(3));
z[21] = -abb[49] + abb[58];
z[22] = abb[46] + (T(-1) / T(3)) * z[21];
z[22] = abb[50] * (T(1) / T(3)) + (T(1) / T(2)) * z[22];
z[2] = abb[56] * (T(1) / T(6)) + -z[2] + -z[20] + (T(1) / T(2)) * z[22];
z[2] = abb[1] * z[2];
z[22] = abb[50] * (T(1) / T(4));
z[23] = abb[58] + z[22];
z[10] = z[10] + z[16] + (T(-1) / T(3)) * z[23];
z[10] = abb[5] * z[10];
z[16] = abb[48] + abb[50];
z[24] = abb[52] + abb[57];
z[25] = 17 * abb[49] + 25 * abb[58];
z[25] = abb[46] + (T(1) / T(3)) * z[25];
z[16] = abb[55] * (T(-7) / T(2)) + abb[54] * (T(2) / T(3)) + (T(5) / T(3)) * z[16] + (T(-7) / T(6)) * z[24] + (T(1) / T(4)) * z[25];
z[16] = abb[2] * z[16];
z[24] = -abb[56] + abb[57];
z[25] = abb[51] * (T(1) / T(3)) + (T(2) / T(3)) * z[24];
z[26] = 5 * abb[49];
z[27] = 5 * abb[46] + -abb[58] + z[26];
z[28] = 4 * abb[50];
z[29] = abb[47] * (T(-5) / T(3)) + abb[48] * (T(3) / T(2)) + abb[55] * (T(5) / T(2)) + abb[53] * (T(5) / T(6)) + -z[25] + (T(-7) / T(12)) * z[27] + -z[28];
z[29] = abb[0] * z[29];
z[30] = -abb[63] + abb[66];
z[31] = abb[64] + abb[65];
z[32] = -abb[67] + (T(-1) / T(4)) * z[30] + (T(3) / T(4)) * z[31];
z[33] = abb[23] + abb[25];
z[34] = z[32] * z[33];
z[35] = 2 * abb[67] + (T(1) / T(2)) * z[30];
z[36] = (T(1) / T(2)) * z[31] + (T(-1) / T(3)) * z[35];
z[37] = abb[21] + abb[26];
z[36] = z[36] * z[37];
z[30] = 4 * abb[67] + z[30];
z[37] = (T(1) / T(3)) * z[30] + -z[31];
z[38] = abb[22] + abb[24];
z[39] = -2 * z[38];
z[37] = z[37] * z[39];
z[39] = abb[19] + abb[20];
z[39] = (T(1) / T(2)) * z[39];
z[40] = abb[59] + abb[61] + -abb[62];
z[41] = abb[60] + (T(1) / T(3)) * z[40];
z[39] = z[39] * z[41];
z[41] = 3 * abb[46];
z[11] = abb[50] * (T(-31) / T(4)) + abb[49] * (T(-29) / T(6)) + abb[48] * (T(-5) / T(6)) + abb[51] * (T(7) / T(2)) + abb[47] * (T(7) / T(6)) + z[11] + -z[41];
z[11] = abb[4] * z[11];
z[42] = 2 * abb[58];
z[43] = -z[4] + z[42];
z[44] = -abb[46] + abb[51];
z[45] = z[43] + -z[44];
z[46] = 4 * abb[52];
z[47] = -z[45] + z[46];
z[48] = 2 * abb[48];
z[49] = (T(1) / T(3)) * z[47] + z[48];
z[49] = abb[15] * z[49];
z[50] = -abb[50] + -4 * abb[58] + z[24];
z[51] = abb[53] + abb[55];
z[50] = -abb[52] + abb[51] * (T(2) / T(3)) + (T(-1) / T(3)) * z[50] + (T(-4) / T(3)) * z[51];
z[50] = abb[8] * z[50];
z[20] = -9 * abb[50] + 5 * abb[53] + abb[55] * (T(5) / T(3)) + -z[20] + (T(-7) / T(6)) * z[27];
z[20] = abb[48] * (T(-1) / T(6)) + (T(1) / T(2)) * z[20] + z[25];
z[20] = abb[3] * z[20];
z[25] = 2 * abb[52];
z[27] = z[25] + z[45];
z[45] = 2 * abb[47];
z[52] = (T(1) / T(3)) * z[27] + -z[45];
z[52] = abb[14] * z[52];
z[53] = -abb[15] + abb[4] * (T(2) / T(3));
z[53] = z[12] * z[53];
z[54] = -abb[50] + abb[52];
z[51] = abb[48] + z[51] + z[54];
z[51] = -abb[58] + (T(-1) / T(3)) * z[0] + (T(2) / T(3)) * z[51];
z[51] = abb[16] * z[51];
z[40] = 3 * abb[60] + z[40];
z[55] = (T(1) / T(2)) * z[40];
z[56] = abb[17] * z[55];
z[57] = abb[18] * z[55];
z[58] = abb[1] * abb[47];
z[1] = 5 * z[1] + 7 * z[2] + z[5] + z[10] + z[11] + z[16] + z[19] + z[20] + z[29] + z[34] + z[36] + z[37] + z[39] + z[49] + 2 * z[50] + 4 * z[51] + z[52] + z[53] + -z[56] + -z[57] + (T(7) / T(3)) * z[58];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[2] = abb[56] * (T(1) / T(4));
z[5] = abb[57] * (T(1) / T(4));
z[10] = z[2] + -z[5];
z[11] = -abb[53] + z[10];
z[16] = abb[52] * (T(3) / T(4));
z[19] = -z[16] + z[22];
z[20] = abb[51] * (T(1) / T(2));
z[29] = abb[58] + z[20];
z[36] = -abb[55] + z[11] + z[19] + z[29];
z[36] = abb[8] * z[36];
z[37] = z[32] * z[38];
z[36] = z[36] + z[37];
z[37] = -abb[55] + z[43];
z[38] = 2 * abb[53];
z[39] = -abb[56] + z[38];
z[49] = 3 * abb[47];
z[50] = z[37] + -z[39] + -z[49];
z[50] = abb[9] * z[50];
z[51] = abb[49] * (T(1) / T(2));
z[52] = abb[46] * (T(1) / T(2));
z[53] = abb[50] * (T(1) / T(2));
z[59] = z[51] + -z[52] + z[53];
z[60] = -abb[58] + z[25];
z[61] = z[20] + z[59] + z[60];
z[62] = 3 * abb[48];
z[63] = z[61] + z[62];
z[64] = abb[15] * z[63];
z[65] = abb[26] * z[32];
z[66] = (T(1) / T(4)) * z[40];
z[67] = abb[20] * z[66];
z[64] = z[64] + z[65] + z[67];
z[65] = -abb[58] + z[20];
z[59] = -abb[52] + z[49] + z[59] + z[65];
z[67] = abb[14] * z[59];
z[68] = abb[21] * z[32];
z[69] = abb[19] * z[66];
z[67] = -z[67] + z[68] + z[69];
z[68] = 4 * abb[48];
z[69] = -abb[57] + -z[12] + z[68];
z[60] = z[4] + z[60];
z[70] = z[60] + z[69];
z[71] = abb[2] * z[70];
z[72] = abb[50] + abb[52];
z[11] = z[11] + z[20] + (T(1) / T(4)) * z[72];
z[11] = abb[3] * z[11];
z[72] = -abb[52] + abb[57];
z[73] = -abb[46] + z[48];
z[74] = z[72] + -z[73];
z[75] = abb[10] * z[74];
z[76] = 2 * abb[50];
z[77] = -abb[56] + z[76];
z[78] = -abb[52] + -z[21] + z[77];
z[79] = abb[1] * z[78];
z[80] = 4 * z[58] + z[79];
z[81] = -abb[48] + z[45];
z[82] = abb[46] + abb[52];
z[83] = -z[81] + z[82];
z[83] = abb[4] * z[83];
z[84] = z[4] + z[25];
z[85] = z[48] + z[84];
z[86] = 3 * abb[54];
z[87] = -abb[47] + z[85] + -z[86];
z[87] = abb[7] * z[87];
z[24] = abb[50] + -abb[58] + z[24] + z[81];
z[24] = abb[0] * z[24];
z[81] = 3 * abb[15];
z[88] = -abb[4] + z[81];
z[89] = abb[54] * z[88];
z[11] = z[11] + z[24] + -z[34] + -z[36] + z[50] + -z[64] + -z[67] + z[71] + -z[75] + -z[80] + z[83] + z[87] + z[89];
z[11] = abb[30] * z[11];
z[24] = 4 * abb[47];
z[83] = z[24] + z[52];
z[87] = z[6] + z[9];
z[90] = 3 * abb[50];
z[91] = abb[49] * (T(5) / T(2)) + z[90];
z[92] = abb[52] * (T(1) / T(2));
z[93] = abb[58] + z[92];
z[94] = abb[48] + -z[20] + z[83] + -z[87] + z[91] + -z[93];
z[94] = abb[4] * z[94];
z[95] = 2 * z[71];
z[64] = z[64] + -z[95];
z[96] = 8 * z[58];
z[97] = 2 * z[79] + z[96];
z[67] = z[67] + z[97];
z[98] = z[64] + -z[67];
z[99] = -z[34] + z[98];
z[100] = abb[47] + abb[48];
z[101] = -z[87] + z[100];
z[102] = z[12] + -z[92];
z[103] = abb[49] + abb[58];
z[104] = z[53] + -z[101] + -z[102] + z[103];
z[104] = abb[0] * z[104];
z[105] = abb[50] * (T(3) / T(2));
z[106] = z[103] + z[105];
z[107] = abb[51] + z[87];
z[108] = z[92] + z[106] + -z[107];
z[109] = abb[16] * z[108];
z[35] = (T(-3) / T(2)) * z[31] + z[35];
z[110] = abb[27] * z[35];
z[109] = z[109] + -z[110];
z[111] = abb[50] * (T(5) / T(2));
z[112] = 2 * abb[49];
z[113] = abb[46] + z[111] + z[112];
z[114] = -z[87] + z[93];
z[115] = 2 * abb[51];
z[116] = z[113] + z[114] + -z[115];
z[117] = abb[3] * z[116];
z[118] = abb[57] * (T(3) / T(2)) + -z[6];
z[119] = abb[52] * (T(3) / T(2));
z[120] = abb[54] + -z[68] + z[118] + -z[119];
z[121] = abb[47] + z[120];
z[122] = z[51] + z[52];
z[123] = z[65] + -z[121] + z[122];
z[123] = abb[7] * z[123];
z[124] = (T(3) / T(4)) * z[40];
z[125] = abb[18] * z[124];
z[89] = z[89] + z[125];
z[125] = abb[17] * z[66];
z[94] = z[89] + z[94] + -z[99] + z[104] + -z[109] + z[117] + z[123] + z[125];
z[94] = abb[33] * z[94];
z[104] = abb[3] * z[108];
z[104] = z[104] + -z[109];
z[117] = 2 * z[70];
z[117] = abb[7] * z[117];
z[47] = 6 * abb[48] + z[47];
z[123] = abb[15] * z[47];
z[126] = 6 * abb[54];
z[127] = abb[15] * z[126];
z[127] = -z[123] + z[127];
z[128] = abb[20] * z[55];
z[129] = z[127] + -z[128];
z[130] = -z[4] + z[44];
z[131] = abb[4] * z[130];
z[131] = -z[57] + z[131];
z[132] = abb[26] * z[35];
z[117] = 4 * z[71] + z[104] + z[117] + z[129] + -z[131] + z[132];
z[117] = abb[31] * z[117];
z[133] = -abb[46] + z[53];
z[134] = -abb[58] + z[48];
z[135] = z[102] + -z[118] + -z[133] + z[134];
z[136] = abb[0] * abb[31];
z[135] = z[135] * z[136];
z[117] = z[117] + -z[135];
z[135] = abb[56] * (T(3) / T(2)) + -z[9];
z[137] = -abb[46] + z[135];
z[93] = -z[45] + z[93] + -z[105] + z[137];
z[93] = abb[0] * z[93];
z[138] = abb[21] * z[35];
z[27] = 6 * abb[47] + -z[27];
z[139] = abb[14] * z[27];
z[140] = abb[19] * z[55];
z[138] = z[138] + z[139] + -z[140];
z[79] = 16 * z[58] + 4 * z[79] + -z[138];
z[139] = abb[7] * z[130];
z[33] = z[33] * z[35];
z[140] = z[33] + z[56];
z[141] = z[139] + -z[140];
z[78] = z[24] + z[78];
z[142] = 2 * abb[4];
z[78] = z[78] * z[142];
z[78] = z[78] + z[79] + z[93] + z[104] + z[141];
z[78] = abb[35] * z[78];
z[93] = z[78] + -z[117];
z[104] = abb[46] * (T(3) / T(2));
z[143] = -z[68] + z[86] + z[104];
z[144] = abb[52] * (T(5) / T(2));
z[145] = -z[87] + z[144];
z[146] = abb[47] + abb[50];
z[147] = -z[51] + -z[65] + z[143] + -z[145] + -z[146];
z[147] = abb[7] * z[147];
z[81] = abb[4] + z[81];
z[81] = abb[54] * z[81];
z[124] = abb[17] * z[124];
z[81] = z[81] + -z[99] + -z[124];
z[99] = abb[48] + z[119];
z[135] = -z[24] + z[99] + z[135];
z[148] = z[29] + -z[76] + -z[122] + z[135];
z[148] = abb[4] * z[148];
z[149] = -z[92] + z[105];
z[150] = 2 * abb[46];
z[101] = -z[21] + z[101] + z[149] + z[150];
z[101] = abb[0] * z[101];
z[114] = z[114] + z[133];
z[133] = -abb[3] * z[114];
z[151] = abb[18] * z[66];
z[101] = -z[81] + z[101] + z[109] + z[133] + z[147] + z[148] + z[151];
z[101] = abb[34] * z[101];
z[11] = z[11] + z[93] + z[94] + z[101];
z[11] = abb[30] * z[11];
z[16] = z[16] + z[20];
z[94] = abb[48] + abb[53];
z[101] = abb[55] + z[94];
z[52] = -z[52] + z[101];
z[2] = z[2] + z[5];
z[5] = abb[49] + z[42];
z[14] = -z[2] + z[5] + z[14] + -z[16] + -z[52];
z[14] = abb[16] * z[14];
z[133] = 3 * abb[52];
z[147] = -abb[46] + abb[56] + 2 * z[21] + -z[28] + z[38] + z[133];
z[147] = abb[1] * z[147];
z[148] = abb[49] * (T(3) / T(2));
z[83] = abb[52] + abb[56] + z[29] + -z[83] + -z[111] + -z[148];
z[83] = abb[4] * z[83];
z[15] = -z[2] + z[15];
z[23] = -abb[53] + -z[15] + -z[23] + -z[51] + z[100];
z[23] = abb[0] * z[23];
z[19] = z[19] + -z[65];
z[152] = -z[2] + z[19];
z[152] = abb[5] * z[152];
z[106] = z[52] + -z[106];
z[106] = abb[3] * z[106];
z[153] = abb[27] * z[32];
z[14] = z[14] + z[23] + -z[50] + z[83] + -z[96] + z[106] + z[147] + -z[151] + -z[152] + z[153];
z[14] = prod_pow(abb[35], 2) * z[14];
z[23] = -abb[55] + z[49];
z[83] = 2 * abb[56];
z[96] = 4 * abb[53] + -abb[58] + z[23] + z[76] + -z[83] + z[112];
z[96] = abb[9] * z[96];
z[106] = z[48] + z[149];
z[23] = -z[23] + z[42] + -z[106] + z[137];
z[23] = abb[0] * z[23];
z[137] = z[38] + z[83];
z[147] = 6 * abb[50];
z[21] = abb[46] + -3 * z[21] + -z[46] + -z[137] + z[147];
z[21] = abb[1] * z[21];
z[154] = z[87] + z[119];
z[155] = 2 * abb[55];
z[156] = z[38] + z[155];
z[157] = z[48] + z[156];
z[5] = abb[50] * (T(-7) / T(2)) + -2 * z[5] + z[44] + z[154] + z[157];
z[5] = abb[16] * z[5];
z[5] = z[5] + -z[33] + z[110];
z[6] = z[6] + -z[9];
z[9] = abb[51] + z[6];
z[33] = z[42] + z[53];
z[158] = z[33] + -z[119];
z[159] = z[9] + -z[156] + z[158];
z[159] = abb[8] * z[159];
z[160] = abb[24] * z[35];
z[159] = z[159] + -z[160];
z[160] = z[0] + z[76];
z[161] = z[134] + -z[160];
z[162] = z[38] + z[161];
z[163] = z[115] + -z[155];
z[164] = z[162] + z[163];
z[165] = abb[13] * z[164];
z[165] = -z[159] + z[165];
z[166] = -abb[58] + z[155];
z[167] = -z[4] + -z[39] + z[166];
z[168] = abb[3] * z[167];
z[27] = abb[4] * z[27];
z[21] = z[5] + z[21] + z[23] + z[27] + z[57] + 12 * z[58] + z[96] + -z[138] + -z[165] + -z[168];
z[23] = abb[22] * z[35];
z[27] = z[21] + -z[23];
z[27] = abb[68] * z[27];
z[35] = z[104] + z[148];
z[58] = -z[87] + z[119];
z[62] = z[35] + z[58] + z[62] + z[65] + z[76];
z[62] = abb[18] * z[62];
z[49] = z[29] + -z[49] + z[122] + z[154];
z[49] = abb[17] * z[49];
z[63] = z[63] + -z[86];
z[63] = abb[20] * z[63];
z[59] = abb[19] * z[59];
z[76] = -abb[4] + -abb[7] + abb[14] + abb[15];
z[66] = z[66] * z[76];
z[76] = abb[17] + abb[18];
z[76] = z[76] * z[86];
z[32] = abb[28] * z[32];
z[55] = abb[3] * z[55];
z[40] = abb[29] * z[40];
z[32] = -z[32] + z[40] + z[49] + z[55] + z[59] + z[62] + -z[63] + -z[66] + -z[76];
z[40] = -abb[73] * z[32];
z[49] = abb[50] + z[65] + -z[122] + -z[135];
z[49] = abb[4] * z[49];
z[55] = -abb[50] + z[29] + -z[35] + z[121];
z[55] = abb[7] * z[55];
z[59] = abb[3] * z[130];
z[62] = z[0] + -z[12];
z[63] = -abb[47] + abb[48] + -abb[57] + -z[62] + -z[77];
z[63] = abb[0] * z[63];
z[49] = z[34] + z[49] + z[55] + z[59] + z[63] + z[64] + z[67] + -z[89] + -z[124];
z[49] = abb[33] * z[49];
z[55] = -z[107] + z[158];
z[59] = abb[5] * z[55];
z[63] = -z[59] + z[131];
z[64] = 2 * abb[7];
z[65] = z[60] * z[64];
z[66] = abb[0] * z[114];
z[60] = abb[3] * z[60];
z[65] = -z[60] + z[63] + -z[65] + -z[66] + z[109];
z[66] = -abb[35] * z[65];
z[62] = z[6] + -z[45] + z[48] + z[53] + z[62] + z[119];
z[67] = -abb[4] * z[62];
z[76] = z[45] + -z[87];
z[53] = abb[58] + -z[53] + -z[73] + -z[76] + -z[92];
z[53] = abb[0] * z[53];
z[71] = z[71] + z[80];
z[77] = -z[84] + z[100];
z[64] = z[64] * z[77];
z[53] = z[53] + -z[57] + -z[60] + z[64] + z[67] + z[71];
z[53] = abb[34] * z[53];
z[49] = z[49] + z[53] + z[66] + z[117];
z[49] = abb[34] * z[49];
z[53] = 3 * abb[58];
z[60] = z[25] + -z[53] + z[157] + -z[160];
z[60] = abb[16] * z[60];
z[64] = -abb[51] + z[6] + z[155];
z[66] = z[0] + -z[64] + -z[134] + z[149];
z[66] = abb[3] * z[66];
z[67] = -abb[0] * z[162];
z[66] = -z[23] + z[56] + z[60] + z[66] + z[67] + -z[131] + -z[139] + z[159];
z[66] = abb[30] * z[66];
z[67] = 3 * abb[49];
z[77] = -abb[52] + z[67] + z[147];
z[80] = -abb[58] + z[150];
z[84] = z[77] + z[80] + -z[115];
z[96] = z[84] * z[142];
z[58] = abb[51] + abb[50] * (T(-9) / T(2)) + -2 * z[18] + z[58];
z[104] = abb[6] * z[58];
z[114] = z[104] + z[141];
z[84] = abb[3] * z[84];
z[96] = z[84] + z[96] + -z[109] + z[114];
z[109] = abb[0] * z[116];
z[109] = z[96] + z[109];
z[109] = abb[33] * z[109];
z[65] = abb[34] * z[65];
z[117] = z[42] + -z[94] + -z[160] + z[163];
z[117] = abb[13] * z[117];
z[16] = abb[50] * (T(-9) / T(4)) + -z[2] + z[16] + -z[18];
z[16] = abb[6] * z[16];
z[16] = z[16] + z[34] + -z[117];
z[10] = abb[48] + z[10] + -z[19];
z[10] = abb[3] * z[10];
z[19] = abb[0] * z[3];
z[10] = z[10] + -z[16] + z[19] + -z[36] + z[152];
z[10] = abb[32] * z[10];
z[10] = z[10] + z[65] + z[66] + z[109];
z[10] = abb[32] * z[10];
z[19] = z[25] + z[42];
z[25] = z[26] + z[41];
z[26] = 11 * abb[50] + -3 * abb[51] + -z[19] + z[25];
z[26] = abb[4] * z[26];
z[36] = z[67] + z[90];
z[65] = z[36] + -z[44] + z[46];
z[66] = -z[42] + z[65];
z[67] = 2 * abb[57];
z[109] = 4 * abb[54];
z[119] = -8 * abb[48] + -z[66] + z[67] + z[109];
z[119] = abb[7] * z[119];
z[57] = z[57] + z[140];
z[121] = z[57] + -z[128];
z[124] = -abb[3] * z[58];
z[26] = z[26] + z[104] + z[119] + -z[121] + z[124] + -z[127] + -z[132];
z[26] = abb[36] * z[26];
z[62] = -abb[7] * z[62];
z[76] = z[76] + -z[102] + z[113] + z[134];
z[76] = abb[0] * z[76];
z[77] = z[77] + -z[100] + z[150];
z[77] = z[77] * z[142];
z[102] = -abb[4] * z[109];
z[62] = -z[56] + z[62] + -z[71] + z[76] + z[77] + z[84] + z[102];
z[62] = abb[33] * z[62];
z[71] = -abb[31] * z[96];
z[76] = -z[116] * z[136];
z[62] = z[62] + z[71] + z[76] + -z[78];
z[62] = abb[33] * z[62];
z[71] = 4 * abb[51];
z[76] = 4 * abb[55] + z[28] + -z[71] + z[80] + -z[94] + z[112];
z[76] = abb[13] * z[76];
z[77] = -abb[57] + z[4];
z[78] = -z[48] + -z[77] + z[166];
z[78] = abb[2] * z[78];
z[80] = abb[9] * z[167];
z[78] = z[78] + z[80];
z[9] = -abb[53] + -z[0] + z[9] + z[99] + -z[111];
z[9] = abb[3] * z[9];
z[58] = abb[4] * z[58];
z[5] = z[5] + -z[9] + -z[58] + z[76] + -z[78] + z[104] + z[159];
z[9] = -abb[0] * z[164];
z[9] = z[5] + z[9] + -z[23];
z[9] = abb[70] * z[9];
z[19] = 8 * abb[47] + abb[49] + -z[19] + z[44] + -z[83] + z[90];
z[19] = abb[4] * z[19];
z[58] = abb[3] * z[55];
z[66] = -abb[7] * z[66];
z[19] = z[19] + -z[57] + z[58] + -z[59] + z[66] + z[79];
z[19] = abb[37] * z[19];
z[33] = -z[33] + z[73] + -z[107] + z[144] + z[156];
z[33] = abb[16] * z[33];
z[33] = z[33] + -z[110] + z[159];
z[38] = abb[49] + -z[38] + z[106] + -z[118] + z[150];
z[38] = abb[0] * z[38];
z[37] = -5 * abb[48] + abb[57] + z[37] + z[86] + -z[133];
z[57] = 2 * abb[2];
z[37] = z[37] * z[57];
z[47] = z[47] + -z[126];
z[47] = abb[7] * z[47];
z[57] = abb[3] * z[74];
z[37] = z[33] + z[37] + z[38] + -z[47] + z[56] + z[57] + -z[129];
z[38] = z[37] + -z[132];
z[38] = abb[69] * z[38];
z[4] = -abb[58] + z[4] + z[82];
z[47] = abb[48] + -abb[53];
z[6] = z[3] + (T(-1) / T(2)) * z[4] + -z[6] + -z[47];
z[6] = abb[3] * z[6];
z[0] = z[0] + z[53];
z[0] = (T(-1) / T(2)) * z[0] + z[54] + z[101];
z[54] = 3 * abb[16];
z[0] = z[0] * z[54];
z[54] = -abb[4] + abb[7];
z[54] = z[54] * z[108];
z[7] = z[7] + z[39];
z[7] = abb[1] * z[7];
z[0] = -z[0] + -z[6] + z[7] + -z[54] + z[78] + z[165];
z[6] = -abb[55] + (T(1) / T(2)) * z[18] + z[94];
z[39] = -abb[0] * z[6];
z[39] = -z[0] + z[39] + z[75];
z[39] = abb[72] * z[39];
z[48] = z[48] + z[64] + -z[158];
z[48] = abb[3] * z[48];
z[54] = abb[7] * z[55];
z[7] = -z[7] + z[33] + -z[48] + z[54] + -z[59];
z[33] = abb[71] * z[7];
z[2] = abb[52] * (T(-5) / T(4)) + z[2] + z[22] + z[29] + -z[52];
z[2] = abb[16] * z[2];
z[22] = z[61] + z[69];
z[22] = abb[7] * z[22];
z[8] = z[3] + z[8];
z[8] = abb[3] * z[8];
z[2] = z[2] + z[8] + z[16] + z[22] + -z[125] + -z[153];
z[8] = prod_pow(abb[31], 2);
z[2] = z[2] * z[8];
z[3] = -z[3] + z[13] + z[15] + -z[17] + -z[47] + -z[51];
z[3] = z[3] * z[8];
z[13] = abb[48] + abb[54] + -z[44] + z[77];
z[13] = abb[36] * z[13];
z[15] = abb[56] + -z[146];
z[15] = abb[37] * z[15];
z[13] = z[13] + z[15];
z[3] = z[3] + 2 * z[13];
z[3] = abb[0] * z[3];
z[13] = -5 * abb[50] + -abb[57] + z[12] + -z[41] + z[46] + -z[112] + z[155];
z[13] = z[8] * z[13];
z[15] = abb[36] * z[70];
z[13] = z[13] + -4 * z[15];
z[13] = abb[2] * z[13];
z[8] = 2 * abb[69] + abb[71] + z[8];
z[8] = z[8] * z[75];
z[15] = -abb[69] + -abb[71] + -abb[72];
z[15] = z[15] * z[23];
z[1] = abb[74] + abb[75] + z[1] + z[2] + z[3] + z[8] + z[9] + z[10] + z[11] + z[13] + z[14] + z[15] + z[19] + z[26] + z[27] + z[33] + z[38] + z[39] + z[40] + z[49] + z[62];
z[2] = -z[18] + -z[24] + -z[28] + -z[67] + z[68] + z[137];
z[2] = abb[0] * z[2];
z[3] = z[45] + -z[65] + -z[68] + z[126];
z[3] = abb[7] * z[3];
z[8] = 2 * z[75] + -z[132];
z[9] = abb[51] + z[24] + -z[41] + -z[85];
z[9] = abb[4] * z[9];
z[10] = -z[115] + z[156] + z[161];
z[11] = abb[3] * z[10];
z[13] = -z[12] * z[88];
z[2] = z[2] + z[3] + z[8] + z[9] + z[11] + z[13] + -2 * z[50] + -z[60] + -z[95] + z[97] + -z[121] + z[123] + -z[138];
z[2] = abb[30] * z[2];
z[3] = z[12] + -z[36] + -z[42] + -z[82] + z[100] + z[115];
z[3] = abb[0] * z[3];
z[9] = abb[56] + z[72] + -z[90] + -2 * z[103] + z[115];
z[9] = abb[16] * z[9];
z[11] = -z[30] + 3 * z[31];
z[11] = abb[27] * z[11];
z[9] = -z[9] + z[11];
z[11] = z[53] + z[87];
z[12] = -abb[48] + -15 * abb[50] + abb[49] * (T(-17) / T(2)) + z[11] + -z[24] + (T(9) / T(2)) * z[44] + z[144];
z[12] = abb[4] * z[12];
z[13] = abb[50] * (T(-17) / T(2)) + -z[25] + z[71] + z[87] + z[92];
z[13] = abb[3] * z[13];
z[14] = abb[51] * (T(3) / T(2));
z[15] = abb[58] + -z[14] + z[120] + z[122] + z[146];
z[15] = abb[7] * z[15];
z[3] = z[3] + z[9] + z[12] + z[13] + z[15] + -3 * z[34] + -z[89] + z[98] + -z[104] + z[125];
z[3] = abb[33] * z[3];
z[12] = -abb[58] + -z[14] + z[35] + z[90] + -z[135];
z[12] = abb[4] * z[12];
z[11] = abb[47] + abb[52] * (T(13) / T(2)) + -z[11] + z[20] + z[91] + -z[143];
z[11] = abb[7] * z[11];
z[13] = abb[52] + -z[41] + z[43] + -z[100];
z[13] = abb[0] * z[13];
z[14] = -abb[46] + abb[49] + z[105] + z[145];
z[14] = abb[3] * z[14];
z[9] = -z[9] + z[11] + z[12] + z[13] + z[14] + z[59] + z[81] + z[151];
z[9] = abb[34] * z[9];
z[4] = -z[4] + -z[163];
z[4] = abb[3] * z[4];
z[10] = abb[0] * z[10];
z[4] = z[4] + z[10] + -z[60] + z[63] + z[114] + -2 * z[117];
z[4] = abb[32] * z[4];
z[2] = z[2] + z[3] + z[4] + z[9] + -z[93];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = abb[38] * z[21];
z[4] = -abb[43] * z[32];
z[5] = abb[40] * z[5];
z[0] = -abb[42] * z[0];
z[8] = z[8] + z[37];
z[8] = abb[39] * z[8];
z[7] = abb[41] * z[7];
z[9] = -abb[40] * z[164];
z[6] = -abb[42] * z[6];
z[6] = z[6] + z[9];
z[6] = abb[0] * z[6];
z[9] = abb[41] + abb[42];
z[10] = z[9] * z[75];
z[9] = -abb[38] + -abb[39] + -abb[40] + -z[9];
z[9] = z[9] * z[23];
z[0] = abb[44] + abb[45] + z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_625_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("6.29528515329787257381662736342755648630045211758223188305213529"),stof<T>("64.523801920969199735358964251936598490033742049441984430025219812")}, std::complex<T>{stof<T>("-5.281050507126616680119316962513288499811167565650091232690641837"),stof<T>("15.526829919047611086321662047333203059313467976301159021214607378")}, std::complex<T>{stof<T>("-43.515610792685911945174155007868578052843947807885947734169056977"),stof<T>("-21.251346999373495047638505934479996266071324351285224298607338805")}, std::complex<T>{stof<T>("6.29528515329787257381662736342755648630045211758223188305213529"),stof<T>("64.523801920969199735358964251936598490033742049441984430025219812")}, std::complex<T>{stof<T>("23.641173216999953607250672781580313235958020887930930709270697379"),stof<T>("104.714609308717099872643744281186595729303249801840079440073373918")}, std::complex<T>{stof<T>("-22.207355031287185186537134050569300564990978186796578418667212246"),stof<T>("4.878169585748482702555095972027750071828173143346888593386266365")}, std::complex<T>{stof<T>("-19.748328882274416958082107696501318884740837686321119076149784679"),stof<T>("-9.257298611307174545710301551067356499892680778567716146394831131")}, std::complex<T>{stof<T>("-5.988687094436881345811310662738584446203671990155887283914284001"),stof<T>("-45.889135226615389161817956217069956419934936560811249088292254641")}, std::complex<T>{stof<T>("5.606271407433924824979699509433678127991673006035949954031657791"),stof<T>("-50.023663798870932915599180247409258570524483628886365763422025806")}, std::complex<T>{stof<T>("-10.0373077107763664455840680704940522306111730998214863873040819287"),stof<T>("-0.6110606900999232603504525665926984133723127941757514587983885366")}, std::complex<T>{stof<T>("10.0373077107763664455840680704940522306111730998214863873040819287"),stof<T>("0.6110606900999232603504525665926984133723127941757514587983885366")}, std::complex<T>{stof<T>("33.746793841688566841746826044588539542320454671421371541144312285"),stof<T>("0.650917911326899004622720824739509447989267199031214121021101361")}, std::complex<T>{stof<T>("-10.4143358590343284804290219500691765260559843288545049888693763595"),stof<T>("5.184206431006742177230519136931534063332589592981814301263748503")}, std::complex<T>{stof<T>("-31.243007577102985441287065850207529578167952986563514966608129078"),stof<T>("15.552619293020226531691557410794602189997768778945442903791245509")}, std::complex<T>{stof<T>("-10.4143358590343284804290219500691765260559843288545049888693763595"),stof<T>("5.184206431006742177230519136931534063332589592981814301263748503")}, std::complex<T>{stof<T>("10.4143358590343284804290219500691765260559843288545049888693763595"),stof<T>("-5.184206431006742177230519136931534063332589592981814301263748503")}, std::complex<T>{stof<T>("-4.1196055750861827043862642707354655921316033428964657066622416124"),stof<T>("1.344625256964207747826567626087918400815070959482346241736480892")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("4.1196055750861827043862642707354655921316033428964657066622416124"),stof<T>("-1.344625256964207747826567626087918400815070959482346241736480892")}, std::complex<T>{stof<T>("16.47842230034473081754505708294186236852641337158586282664896645"),stof<T>("-5.378501027856830991306270504351673603260283837929384966945923568")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[124].real()/kbase.W[124].real()), rlog(k.W[127].real()/kbase.W[127].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_625_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_625_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,4> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(3) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(8)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (-16 + -13 * v[0] + -3 * v[1] + -v[2] + 7 * v[3] + 8 * v[4] + 4 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + v[5])) / prod_pow(tend, 2);
c[2] = (3 * m1_set::bc<T>[1] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;
c[3] = (-(-1 + 2 * m1_set::bc<T>[1]) * (v[0] + -1 * v[1] + v[2] + v[3] + -1 * v[5])) / tend;


		return abb[52] * (t * c[0] + c[2]) + abb[50] * (t * c[1] + c[3]) + -abb[56] * (t * c[1] + c[3]) + abb[47] * (t * (-c[0] + c[1]) + -c[2] + c[3]);
	}
	{
T z[4];
z[0] = -abb[30] + abb[33];
z[1] = -abb[50] + abb[56];
z[2] = -abb[47] + z[1];
z[0] = z[0] * z[2];
z[0] = 2 * z[0];
z[1] = -4 * abb[47] + 3 * abb[52] + z[1];
z[3] = abb[34] * z[1];
z[3] = z[0] + z[3];
z[3] = abb[34] * z[3];
z[1] = -abb[35] * z[1];
z[0] = -z[0] + z[1];
z[0] = abb[35] * z[0];
z[1] = abb[37] * z[2];
z[0] = z[0] + 2 * z[1] + z[3];
return abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_625_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[47] + abb[50] + -abb[56]) * (t * c[0] + -c[1]);
	}
	{
T z[2];
z[0] = -abb[34] + abb[35];
z[1] = abb[47] + abb[50] + -abb[56];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_625_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,4> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (v[2] + v[3]) * (32 + -8 * v[0] + -4 * v[1] + -9 * v[2] + -5 * v[3] + 4 * v[4] + 8 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 16 * v[5])) / prod_pow(tend, 2);
c[1] = ((-1 + m1_set::bc<T>[1]) * (T(-3) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[2] = ((1 + m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;
c[3] = (3 * m1_set::bc<T>[1] * (v[2] + v[3])) / tend;


		return t * (abb[50] * c[0] + abb[52] * c[1] + abb[46] * (c[0] + c[1]) + abb[49] * (c[0] + c[1]) + -abb[51] * (c[0] + c[1]) + abb[54] * (c[0] + c[1]) + -abb[57] * (c[0] + 2 * c[1]) + abb[48] * (c[0] + 3 * c[1])) + -abb[46] * c[2] + -abb[49] * c[2] + abb[51] * c[2] + -abb[54] * c[2] + -abb[48] * (c[2] + -2 * c[3]) + abb[57] * (c[2] + -c[3]) + abb[52] * c[3] + -abb[50] * (c[2] + c[3]);
	}
	{
T z[5];
z[0] = -abb[30] + abb[34];
z[1] = abb[46] + abb[49] + -abb[51] + abb[54];
z[2] = abb[48] + abb[50] + -abb[57] + z[1];
z[0] = z[0] * z[2];
z[1] = 2 * abb[48] + -z[1];
z[1] = -5 * abb[50] + 3 * abb[52] + -abb[57] + 2 * z[1];
z[3] = abb[33] * z[1];
z[3] = 2 * z[0] + z[3];
z[3] = abb[12] * abb[33] * z[3];
z[4] = 2 * abb[12];
z[2] = abb[36] * z[2] * z[4];
z[0] = -z[0] * z[4];
z[1] = -abb[12] * abb[31] * z[1];
z[0] = z[0] + z[1];
z[0] = abb[31] * z[0];
return z[0] + z[2] + z[3];
}

}
template <typename T, typename TABB> T SpDLog_f_4_625_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[46] + abb[48] + abb[49] + abb[50] + -abb[51] + abb[54] + -abb[57]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[31] + abb[33];
z[1] = abb[46] + abb[48] + abb[49] + abb[50] + -abb[51] + abb[54] + -abb[57];
return 2 * abb[12] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_625_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("80.480511210946986791498343142663484093588816339260970859286402711"),stof<T>("-1.728081412250482296539937309790276901088173537083154778856054757")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,76> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W17(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[13].real()/k.W[13].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[124].real()/k.W[124].real()), rlog(kend.W[127].real()/k.W[127].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), T{0}, T{0}};
abb[44] = SpDLog_f_4_625_W_17_Im(t, path, abb);
abb[45] = SpDLog_f_4_625_W_19_Im(t, path, abb);
abb[74] = SpDLog_f_4_625_W_17_Re(t, path, abb);
abb[75] = SpDLog_f_4_625_W_19_Re(t, path, abb);

                    
            return f_4_625_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_625_DLogXconstant_part(base_point<T>, kend);
	value += f_4_625_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_625_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_625_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_625_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_625_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_625_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_625_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
