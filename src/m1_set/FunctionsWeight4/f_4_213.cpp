/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_213.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_213_abbreviated (const std::array<T,26>& abb) {
T z[29];
z[0] = abb[18] + -abb[20];
z[0] = abb[0] * z[0];
z[1] = abb[22] + abb[18] * (T(-5) / T(3)) + abb[21] * (T(-1) / T(3));
z[1] = abb[3] * z[1];
z[2] = -7 * abb[18] + -5 * abb[19] + abb[22];
z[2] = abb[2] * z[2];
z[1] = (T(13) / T(3)) * z[0] + z[1] + (T(1) / T(3)) * z[2];
z[2] = abb[23] * (T(1) / T(2));
z[3] = abb[7] + abb[8];
z[2] = -z[2] * z[3];
z[4] = abb[19] + -abb[22];
z[5] = 3 * abb[18] + z[4];
z[6] = abb[5] * z[5];
z[4] = -abb[18] + (T(-1) / T(3)) * z[4];
z[4] = abb[6] * z[4];
z[7] = abb[20] + abb[21];
z[8] = -abb[22] + abb[18] * (T(-7) / T(2)) + abb[19] * (T(-7) / T(6)) + (T(13) / T(6)) * z[7];
z[8] = abb[1] * z[8];
z[1] = (T(1) / T(2)) * z[1] + z[2] + z[4] + z[6] + z[8];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[2] = abb[0] + abb[3];
z[4] = abb[22] * (T(1) / T(2));
z[8] = -abb[18] + z[4];
z[9] = z[2] * z[8];
z[10] = abb[1] * z[5];
z[11] = abb[18] + abb[19];
z[11] = abb[2] * z[11];
z[12] = abb[7] * abb[23];
z[13] = z[9] + z[10] + z[11] + (T(-1) / T(2)) * z[12];
z[13] = abb[14] * z[13];
z[9] = (T(1) / T(2)) * z[9];
z[14] = abb[2] * z[8];
z[14] = z[9] + z[14];
z[15] = abb[6] * (T(1) / T(2));
z[16] = -abb[1] + z[15];
z[16] = z[5] * z[16];
z[16] = z[14] + z[16];
z[17] = abb[11] + abb[12];
z[16] = z[16] * z[17];
z[17] = abb[7] * abb[11];
z[18] = abb[7] * abb[12];
z[19] = -z[17] + -z[18];
z[20] = abb[23] * (T(1) / T(4));
z[19] = z[19] * z[20];
z[13] = (T(1) / T(2)) * z[13] + z[16] + z[19];
z[13] = abb[14] * z[13];
z[16] = -abb[3] * z[8];
z[11] = -2 * z[0] + (T(1) / T(2)) * z[11] + z[16];
z[16] = prod_pow(abb[11], 2);
z[11] = z[11] * z[16];
z[19] = -abb[11] * z[14];
z[20] = -abb[0] + abb[2];
z[20] = z[8] * z[20];
z[21] = -abb[18] + abb[21];
z[21] = abb[3] * z[21];
z[20] = z[20] + 2 * z[21];
z[20] = abb[12] * z[20];
z[19] = z[19] + z[20];
z[19] = abb[12] * z[19];
z[20] = z[3] * z[16];
z[21] = -abb[8] * abb[12];
z[17] = (T(1) / T(2)) * z[17] + z[21];
z[17] = abb[12] * z[17];
z[2] = abb[2] + (T(3) / T(2)) * z[2];
z[21] = -abb[25] * z[2];
z[17] = z[17] + z[20] + z[21];
z[20] = abb[10] * abb[25];
z[17] = (T(1) / T(2)) * z[17] + z[20];
z[17] = abb[23] * z[17];
z[20] = z[5] * z[15];
z[21] = -abb[11] * abb[12];
z[21] = -z[16] + z[21];
z[21] = z[20] * z[21];
z[22] = abb[2] + -abb[5];
z[22] = z[5] * z[22];
z[23] = 9 * abb[18] + 3 * abb[19];
z[24] = -abb[22] + -2 * z[7] + z[23];
z[24] = abb[1] * z[24];
z[22] = z[22] + z[24];
z[22] = abb[13] * z[22];
z[24] = 3 * abb[1] + abb[2] + -abb[6];
z[24] = z[5] * z[24];
z[25] = -abb[11] + abb[14];
z[25] = z[24] * z[25];
z[22] = z[22] + z[25];
z[22] = abb[13] * z[22];
z[25] = abb[15] + abb[24];
z[25] = z[24] * z[25];
z[26] = abb[9] * z[5];
z[8] = abb[7] * z[8];
z[8] = 3 * z[8] + -z[26];
z[8] = (T(1) / T(2)) * z[8];
z[26] = abb[25] * z[8];
z[27] = abb[11] + -abb[12];
z[27] = abb[12] * z[27];
z[27] = (T(1) / T(2)) * z[16] + z[27];
z[27] = abb[1] * z[5] * z[27];
z[28] = -prod_pow(abb[13], 2);
z[16] = z[16] + z[28];
z[28] = abb[4] * z[5];
z[16] = z[16] * z[28];
z[6] = prod_pow(abb[12], 2) * z[6];
z[1] = z[1] + z[6] + z[11] + z[13] + z[16] + z[17] + z[19] + z[21] + z[22] + z[25] + z[26] + z[27];
z[6] = -2 * abb[1] + z[15];
z[6] = z[5] * z[6];
z[11] = 2 * abb[18];
z[4] = -abb[19] + z[4] + -z[11];
z[4] = abb[2] * z[4];
z[4] = z[4] + z[6] + -z[9] + (T(1) / T(4)) * z[12];
z[4] = abb[14] * z[4];
z[6] = z[14] + z[20];
z[6] = abb[12] * z[6];
z[9] = -abb[2] + 2 * abb[5] + -abb[6];
z[5] = z[5] * z[9];
z[7] = -abb[22] + 4 * z[7] + -z[23];
z[7] = abb[1] * z[7];
z[5] = z[5] + z[7];
z[5] = abb[13] * z[5];
z[7] = abb[2] + -abb[3];
z[9] = -abb[22] + z[11];
z[7] = z[7] * z[9];
z[0] = 4 * z[0] + z[7];
z[0] = abb[11] * z[0];
z[3] = -abb[11] * z[3];
z[3] = z[3] + (T(-1) / T(4)) * z[18];
z[3] = abb[23] * z[3];
z[7] = 2 * abb[11] + -abb[12];
z[7] = z[7] * z[10];
z[9] = -abb[11] + abb[13];
z[9] = z[9] * z[28];
z[0] = z[0] + z[3] + z[4] + z[5] + z[6] + z[7] + 2 * z[9];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[10] + (T(-1) / T(2)) * z[2];
z[2] = abb[23] * z[2];
z[2] = z[2] + z[8];
z[2] = abb[17] * z[2];
z[3] = abb[16] * z[24];
z[0] = z[0] + z[2] + z[3];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_213_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-29.812321165349265736397436937642014984677930848764465516646227806"),stof<T>("21.120501999199564205866697115437052890912296680443702455469877917")}, std::complex<T>{stof<T>("-1.412473870669542109763008607778447660347215594905893411814647339"),stof<T>("16.407715795943386256223781104166042751911400647254318617995466022")}, std::complex<T>{stof<T>("22.60890283178492313406981515200700402818461207269985546617044954"),stof<T>("7.583483977093285575622000127537769702729375417392099625370886916")}, std::complex<T>{stof<T>("21.690737855801356696407804348089141712405399888234274315210568794"),stof<T>("-18.255079923156564570608262428380059632458365054178823453082894837")}, std::complex<T>{stof<T>("-7.9498966964532781019215955851162892081296483535377788382747189337"),stof<T>("2.9794048714035505226716731447856398953640468017986699951187980132")}, std::complex<T>{stof<T>("-4.6547188104916960897412832574814250337357180128234620229279515134"),stof<T>("6.7898493908076716514634340510446747195333140327511877322391776059")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[125].real()/kbase.W[125].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_213_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_213_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("29.181057708589331283685112484610216956792812653741931903372352006"),stof<T>("-0.814085034213363193326907487164898452125888678469106165187611483")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,26> abb = {dl[0], dl[1], dlog_W8(k,dl), dl[3], dlog_W12(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_2_3(k), f_2_6_im(k), f_2_24_im(k), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[125].real()/k.W[125].real()), f_2_6_re(k), f_2_24_re(k)};

                    
            return f_4_213_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_213_DLogXconstant_part(base_point<T>, kend);
	value += f_4_213_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_213_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_213_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_213_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_213_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_213_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_213_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
