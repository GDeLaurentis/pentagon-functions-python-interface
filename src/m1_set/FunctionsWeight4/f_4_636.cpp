/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_636.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_636_abbreviated (const std::array<T,69>& abb) {
T z[163];
z[0] = abb[50] + -abb[52] + -abb[57];
z[1] = abb[47] + -abb[51];
z[2] = abb[58] + abb[46] * (T(5) / T(6));
z[3] = abb[54] + abb[59];
z[4] = 4 * abb[55];
z[5] = abb[56] * (T(1) / T(2));
z[6] = 2 * abb[53];
z[0] = abb[49] * (T(-5) / T(6)) + abb[45] * (T(-1) / T(6)) + abb[48] * (T(17) / T(6)) + (T(2) / T(3)) * z[0] + (T(-11) / T(6)) * z[1] + z[2] + (T(-1) / T(3)) * z[3] + -z[4] + -z[5] + z[6];
z[0] = abb[4] * z[0];
z[1] = abb[47] + -abb[48];
z[3] = abb[50] + abb[54];
z[7] = (T(3) / T(2)) * z[3];
z[8] = abb[56] * (T(1) / T(3));
z[9] = abb[57] * (T(5) / T(2));
z[10] = abb[49] * (T(3) / T(2));
z[11] = abb[53] + abb[58] * (T(-1) / T(3)) + abb[59] * (T(2) / T(3)) + (T(1) / T(6)) * z[1] + z[7] + -z[8] + -z[9] + -z[10];
z[11] = abb[2] * z[11];
z[12] = 2 * abb[56];
z[13] = 4 * abb[59];
z[14] = abb[49] * (T(13) / T(2)) + z[12] + -z[13];
z[15] = -abb[57] + z[1];
z[16] = abb[51] * (T(2) / T(3));
z[17] = 3 * abb[55];
z[7] = -z[7] + (T(1) / T(3)) * z[14] + (T(-35) / T(6)) * z[15] + -z[16] + -z[17];
z[7] = abb[3] * z[7];
z[14] = 2 * abb[52];
z[15] = 2 * abb[45];
z[18] = z[14] + -z[15];
z[19] = abb[49] + -abb[56];
z[20] = -abb[51] + z[18] + -z[19];
z[20] = abb[14] * z[20];
z[21] = -abb[49] + abb[51];
z[22] = -abb[56] + z[21];
z[23] = -z[6] + z[22];
z[24] = 2 * abb[58];
z[25] = z[23] + z[24];
z[26] = abb[15] * z[25];
z[27] = z[20] + -z[26];
z[28] = abb[47] + -abb[54];
z[29] = (T(1) / T(2)) * z[19];
z[30] = abb[51] * (T(1) / T(2));
z[31] = z[28] + z[29] + z[30];
z[31] = abb[6] * z[31];
z[32] = z[27] + -z[31];
z[33] = 2 * abb[57];
z[8] = abb[49] + -abb[59] + z[8] + z[33];
z[34] = abb[47] + abb[50];
z[35] = -abb[53] + abb[55];
z[36] = 2 * abb[51];
z[8] = abb[58] * (T(5) / T(3)) + 2 * z[8] + (T(-7) / T(3)) * z[34] + (T(-8) / T(3)) * z[35] + -z[36];
z[8] = abb[10] * z[8];
z[37] = abb[49] + abb[56];
z[38] = (T(5) / T(2)) * z[37];
z[39] = abb[51] * (T(5) / T(2));
z[40] = 4 * abb[53];
z[41] = 2 * abb[59];
z[42] = abb[45] * (T(5) / T(2)) + z[38] + -z[39] + -z[40] + -z[41];
z[43] = abb[48] * (T(3) / T(2));
z[2] = abb[47] * (T(3) / T(2)) + -z[2] + z[17] + (T(1) / T(3)) * z[42] + -z[43];
z[2] = abb[7] * z[2];
z[42] = z[6] + -z[33];
z[44] = abb[48] + abb[50];
z[45] = z[42] + z[44];
z[46] = 2 * abb[55];
z[47] = -z[21] + z[46];
z[48] = z[45] + -z[47];
z[49] = abb[13] * z[48];
z[50] = 4 * z[49];
z[51] = abb[21] + abb[26];
z[52] = abb[22] + abb[24];
z[53] = abb[23] + abb[25];
z[51] = (T(-1) / T(3)) * z[51] + (T(-4) / T(3)) * z[52] + (T(-1) / T(2)) * z[53];
z[51] = abb[60] * z[51];
z[54] = abb[49] * (T(1) / T(2));
z[9] = abb[45] * (T(-17) / T(2)) + abb[52] * (T(1) / T(2)) + abb[50] * (T(5) / T(2)) + z[9] + -z[12] + -z[54];
z[9] = -abb[53] + abb[47] * (T(5) / T(6)) + abb[46] * (T(8) / T(3)) + (T(1) / T(3)) * z[9];
z[9] = abb[0] * z[9];
z[55] = abb[59] + z[42];
z[16] = abb[55] * (T(-4) / T(3)) + abb[49] * (T(-2) / T(3)) + z[16] + (T(-1) / T(3)) * z[44] + z[55];
z[16] = abb[16] * z[16];
z[56] = -abb[51] + z[19];
z[57] = -abb[55] + z[44];
z[56] = abb[59] + abb[53] * (T(-2) / T(3)) + (T(1) / T(6)) * z[56] + -z[57];
z[56] = abb[5] * z[56];
z[58] = abb[49] + -abb[52];
z[59] = z[44] + -z[58];
z[60] = -abb[45] + abb[57] + (T(-5) / T(3)) * z[59];
z[60] = abb[55] * (T(1) / T(3)) + abb[46] * (T(4) / T(3)) + (T(1) / T(2)) * z[60];
z[60] = abb[1] * z[60];
z[61] = 8 * abb[53];
z[62] = 4 * abb[57];
z[63] = z[61] + -z[62];
z[64] = abb[56] + z[21];
z[65] = -z[63] + -z[64];
z[65] = z[46] + (T(1) / T(3)) * z[65];
z[65] = abb[8] * z[65];
z[66] = 4 * abb[51];
z[67] = 4 * abb[52] + z[62] + -z[66];
z[68] = abb[45] * (T(29) / T(2)) + -z[67];
z[68] = abb[46] * (T(-7) / T(2)) + (T(1) / T(3)) * z[68];
z[68] = abb[9] * z[68];
z[69] = abb[51] + -abb[54] + -abb[57];
z[70] = -abb[48] + abb[47] * (T(8) / T(3)) + (T(5) / T(3)) * z[69];
z[70] = abb[12] * z[70];
z[0] = z[0] + z[2] + z[7] + z[8] + z[9] + z[11] + z[16] + (T(1) / T(3)) * z[32] + z[50] + z[51] + z[56] + z[60] + 4 * z[65] + z[68] + 2 * z[70];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[2] = 3 * abb[47];
z[7] = 3 * abb[50];
z[8] = 3 * abb[53];
z[9] = -z[2] + -z[7] + z[8] + -z[17] + z[24];
z[11] = 3 * abb[59];
z[16] = z[11] + -z[33];
z[32] = z[16] + z[22];
z[51] = -z[9] + -z[32];
z[51] = abb[10] * z[51];
z[56] = 3 * abb[46];
z[60] = 5 * abb[45] + -z[56];
z[65] = -z[14] + -z[33] + z[36] + z[60];
z[65] = abb[9] * z[65];
z[68] = abb[51] + -abb[56];
z[70] = abb[59] + z[68];
z[71] = abb[46] + -abb[52];
z[72] = -abb[53] + abb[58];
z[73] = z[44] + -z[70] + -z[71] + -z[72];
z[73] = abb[4] * z[73];
z[29] = z[29] + -z[30];
z[74] = z[17] + z[29];
z[75] = z[33] + -z[40] + z[74];
z[75] = abb[8] * z[75];
z[54] = -abb[52] + z[54];
z[76] = abb[45] + z[54];
z[77] = -z[5] + z[30] + z[76];
z[78] = abb[14] * z[77];
z[79] = 2 * abb[46];
z[80] = -z[15] + z[79];
z[81] = abb[56] + z[42];
z[82] = z[34] + z[80] + -z[81];
z[83] = -abb[0] * z[82];
z[84] = 2 * abb[47];
z[85] = 2 * abb[50];
z[86] = z[84] + z[85];
z[87] = -abb[58] + z[86];
z[88] = -abb[45] + abb[46];
z[55] = z[55] + -z[87] + z[88];
z[55] = abb[7] * z[55];
z[89] = abb[46] + -z[57] + z[58];
z[90] = abb[1] * z[89];
z[72] = (T(1) / T(2)) * z[22] + z[72];
z[91] = abb[15] * z[72];
z[92] = abb[57] + -abb[59];
z[93] = -abb[53] + z[34] + z[92];
z[94] = 2 * abb[2];
z[95] = -z[93] * z[94];
z[96] = abb[21] + z[53];
z[97] = abb[26] + z[96];
z[98] = z[52] + z[97];
z[99] = abb[60] * (T(1) / T(2));
z[98] = z[98] * z[99];
z[100] = -z[30] + (T(1) / T(2)) * z[37];
z[101] = -abb[59] + z[100];
z[102] = abb[55] + z[101];
z[102] = abb[3] * z[102];
z[51] = z[51] + z[55] + -z[65] + z[73] + -z[75] + z[78] + z[83] + -z[90] + z[91] + z[95] + z[98] + z[102];
z[51] = abb[29] * z[51];
z[55] = -abb[45] + z[79];
z[73] = -abb[52] + z[55];
z[83] = abb[53] + z[37];
z[95] = z[73] + z[83];
z[98] = -z[86] + z[95];
z[98] = abb[0] * z[98];
z[34] = -z[34] + z[35];
z[102] = -z[21] + z[33];
z[103] = z[34] + z[102];
z[104] = 2 * abb[10];
z[105] = z[103] * z[104];
z[98] = z[98] + -z[105];
z[105] = -abb[52] + -z[5] + z[55];
z[106] = -z[6] + z[30];
z[10] = -z[10] + z[106];
z[107] = -z[10] + -z[87] + z[105];
z[107] = abb[4] * z[107];
z[108] = 2 * abb[1];
z[109] = z[89] * z[108];
z[78] = z[78] + -z[109];
z[110] = z[78] + -z[91];
z[111] = -abb[26] + z[96];
z[111] = (T(1) / T(2)) * z[111];
z[112] = -abb[27] + z[111];
z[112] = abb[60] * z[112];
z[112] = z[110] + z[112];
z[113] = z[40] + -z[47] + -z[62];
z[114] = 2 * abb[48];
z[115] = z[85] + z[114];
z[116] = -abb[56] + z[41];
z[117] = z[113] + z[115] + -z[116];
z[118] = abb[16] * z[117];
z[119] = abb[53] + abb[58] + z[80];
z[120] = z[41] + -z[100] + -z[119];
z[120] = abb[7] * z[120];
z[121] = -abb[58] + abb[59] + z[1];
z[121] = z[94] * z[121];
z[107] = -z[98] + z[107] + -z[112] + z[118] + z[120] + z[121];
z[107] = abb[32] * z[107];
z[35] = z[35] + z[87];
z[87] = z[32] + -z[35];
z[87] = z[87] * z[104];
z[120] = z[87] + -z[118];
z[95] = -z[41] + z[95];
z[95] = abb[0] * z[95];
z[122] = 4 * z[93];
z[123] = abb[2] * z[122];
z[124] = z[95] + z[123];
z[73] = -abb[58] + z[73];
z[10] = abb[56] * (T(-3) / T(2)) + z[10] + z[41] + -z[73];
z[10] = abb[4] * z[10];
z[125] = 4 * abb[47];
z[126] = 4 * abb[50];
z[127] = z[125] + z[126];
z[128] = z[62] + z[127];
z[129] = abb[58] + z[8] + -z[100];
z[130] = -z[13] + z[128] + -z[129];
z[130] = abb[7] * z[130];
z[10] = z[10] + z[112] + z[120] + z[124] + z[130];
z[10] = abb[33] * z[10];
z[112] = -z[84] + z[114];
z[23] = -z[23] + -z[41] + z[112];
z[130] = abb[4] * z[23];
z[131] = abb[47] + abb[48];
z[132] = z[85] + z[131];
z[133] = abb[58] + -z[11] + -z[42] + z[132];
z[133] = z[94] * z[133];
z[133] = -z[26] + z[133];
z[122] = abb[7] * z[122];
z[134] = abb[26] + abb[27];
z[135] = abb[60] * z[134];
z[122] = z[122] + -z[135];
z[135] = -z[47] + z[116];
z[136] = abb[3] * z[135];
z[120] = z[120] + z[122] + -z[130] + z[133] + z[136];
z[120] = abb[30] * z[120];
z[130] = z[17] + -z[33];
z[15] = -abb[46] + z[15] + -z[59] + z[130];
z[15] = z[15] * z[108];
z[81] = -abb[59] + z[81];
z[137] = z[81] + z[88];
z[138] = 2 * abb[0];
z[137] = z[137] * z[138];
z[15] = z[15] + z[20] + -z[137];
z[137] = 2 * z[49];
z[139] = abb[7] * z[135];
z[140] = -z[137] + z[139];
z[141] = z[136] + z[140];
z[142] = 2 * abb[4];
z[89] = z[89] * z[142];
z[143] = -abb[27] + z[96];
z[144] = abb[60] * z[143];
z[89] = z[15] + z[89] + z[118] + z[141] + -z[144];
z[89] = abb[34] * z[89];
z[144] = 2 * abb[49];
z[36] = -z[36] + z[144];
z[145] = abb[56] + -z[11] + z[36] + z[62];
z[145] = z[104] * z[145];
z[146] = 6 * abb[55];
z[64] = -z[64] + z[146];
z[63] = z[63] + -z[64];
z[147] = abb[8] * z[63];
z[148] = z[145] + -z[147];
z[117] = abb[4] * z[117];
z[149] = 3 * abb[57] + -abb[59];
z[150] = -z[6] + z[149];
z[151] = abb[55] + -z[21] + z[150];
z[152] = 4 * abb[16];
z[151] = z[151] * z[152];
z[152] = abb[60] * z[52];
z[117] = z[117] + z[148] + -z[151] + -z[152];
z[141] = z[117] + -z[141];
z[152] = abb[31] * z[141];
z[10] = z[10] + z[51] + z[89] + z[107] + z[120] + z[152];
z[10] = abb[29] * z[10];
z[51] = 3 * abb[48];
z[7] = z[6] + z[7] + -z[11] + z[51] + -z[74];
z[7] = abb[5] * z[7];
z[7] = z[7] + -3 * z[49];
z[74] = 5 * abb[57];
z[107] = -abb[54] + z[74];
z[152] = 5 * abb[48] + z[107] + -z[125];
z[153] = 3 * abb[49];
z[154] = -abb[56] + -z[153];
z[154] = z[17] + z[30] + z[41] + -z[152] + (T(1) / T(2)) * z[154];
z[154] = abb[3] * z[154];
z[70] = abb[47] + abb[54] + -z[70] + -z[114] + z[130];
z[70] = abb[4] * z[70];
z[155] = z[6] + z[44];
z[156] = 6 * abb[57];
z[36] = abb[59] + -z[36] + z[155] + -z[156];
z[157] = abb[16] * z[36];
z[158] = 4 * z[69];
z[159] = abb[47] * (T(-11) / T(2)) + z[43] + -z[158];
z[159] = abb[12] * z[159];
z[160] = -abb[54] + abb[57] + z[37] + -z[41];
z[161] = (T(1) / T(2)) * z[131] + z[160];
z[161] = abb[2] * z[161];
z[162] = z[52] + z[53];
z[162] = z[99] * z[162];
z[70] = z[7] + z[31] + z[70] + -z[75] + z[154] + z[157] + z[159] + z[161] + z[162];
z[70] = abb[31] * z[70];
z[75] = 2 * abb[16];
z[36] = -z[36] * z[75];
z[154] = abb[47] + z[51] + z[158];
z[154] = abb[12] * z[154];
z[81] = -z[1] + z[81];
z[142] = -z[81] * z[142];
z[157] = -abb[49] + abb[54];
z[150] = z[150] + -z[157];
z[2] = -abb[48] + z[2] + 2 * z[150];
z[2] = abb[2] * z[2];
z[150] = -abb[48] + -z[92] + z[157];
z[157] = 2 * abb[3];
z[150] = z[150] * z[157];
z[2] = z[2] + z[36] + z[142] + -z[145] + z[150] + z[154];
z[2] = abb[30] * z[2];
z[2] = z[2] + z[70];
z[2] = abb[31] * z[2];
z[36] = 2 * abb[54];
z[70] = -z[36] + z[144];
z[8] = -abb[50] + -abb[56] + abb[47] * (T(-9) / T(2)) + z[8] + z[13] + z[43] + -z[70] + -z[74];
z[8] = abb[2] * z[8];
z[16] = abb[53] + z[16] + -z[100] + -z[132];
z[16] = abb[7] * z[16];
z[43] = -abb[50] + -abb[55] + z[51] + z[70] + -z[84] + z[149];
z[43] = abb[3] * z[43];
z[3] = abb[59] + z[3] + -z[83] + -z[112];
z[70] = -abb[4] * z[3];
z[32] = -z[32] + -z[34];
z[32] = abb[10] * z[32];
z[34] = -z[41] + z[115];
z[29] = abb[55] + -z[29] + -z[33] + z[34];
z[29] = abb[16] * z[29];
z[74] = -abb[27] + z[53];
z[100] = -z[74] * z[99];
z[112] = abb[12] * z[1];
z[8] = z[8] + z[16] + z[29] + -z[31] + z[32] + z[43] + z[70] + z[100] + (T(9) / T(2)) * z[112];
z[8] = prod_pow(abb[30], 2) * z[8];
z[16] = -abb[58] + z[84];
z[29] = z[16] + -z[114];
z[5] = z[5] + z[30];
z[30] = z[5] + z[29] + z[54] + z[55];
z[30] = abb[4] * z[30];
z[31] = -z[97] * z[99];
z[32] = abb[7] * z[72];
z[30] = z[30] + z[31] + z[32] + -z[78] + -z[91] + -z[95] + -z[121] + z[136];
z[30] = abb[32] * z[30];
z[31] = 6 * abb[48];
z[32] = 6 * abb[59];
z[43] = -6 * abb[50] + -z[31] + z[32] + -z[40] + z[64];
z[43] = abb[5] * z[43];
z[54] = z[57] + z[92];
z[55] = 4 * z[54];
z[57] = abb[3] * z[55];
z[57] = z[43] + z[57] + z[137];
z[64] = abb[4] * z[135];
z[55] = abb[7] * z[55];
z[70] = abb[27] * abb[60];
z[55] = z[55] + z[57] + z[64] + -z[70] + -z[118];
z[64] = abb[31] + -abb[34];
z[55] = z[55] * z[64];
z[64] = abb[0] + -abb[2];
z[64] = z[64] * z[93];
z[70] = z[1] + z[88];
z[78] = abb[7] * z[70];
z[91] = abb[55] + -abb[57];
z[88] = -z[88] + z[91];
z[88] = abb[1] * z[88];
z[54] = abb[3] * z[54];
z[54] = z[54] + z[64] + -z[78] + z[88];
z[54] = abb[33] * z[54];
z[30] = z[30] + 2 * z[54] + z[55] + -z[120];
z[30] = abb[33] * z[30];
z[54] = 6 * abb[53];
z[44] = z[44] + z[54];
z[55] = 5 * abb[49] + -abb[56];
z[64] = 8 * abb[57];
z[39] = -abb[59] + z[17] + -z[39] + -z[44] + (T(1) / T(2)) * z[55] + z[64];
z[39] = abb[16] * z[39];
z[92] = z[80] + -z[101] + -z[130];
z[92] = abb[4] * z[92];
z[17] = -4 * abb[45] + 5 * abb[46] + -z[17] + -z[59] + z[62];
z[17] = z[17] * z[108];
z[58] = abb[59] + -z[58];
z[58] = -3 * abb[45] + abb[46] + z[40] + 2 * z[58] + -z[156];
z[58] = abb[0] * z[58];
z[59] = -abb[27] * z[99];
z[7] = -z[7] + z[17] + z[39] + z[58] + z[59] + z[65] + z[92];
z[7] = prod_pow(abb[34], 2) * z[7];
z[17] = -abb[4] * z[70];
z[39] = -abb[49] + abb[50];
z[58] = abb[54] + z[39];
z[59] = -abb[53] + z[1] + z[58];
z[70] = abb[2] * z[59];
z[39] = -abb[47] + abb[53] + -z[39] + z[71];
z[39] = abb[0] * z[39];
z[58] = -abb[55] + z[58];
z[71] = -abb[3] * z[58];
z[17] = z[17] + z[39] + z[70] + z[71] + z[78] + -z[90];
z[17] = abb[32] * z[17];
z[3] = -abb[2] * z[3];
z[39] = -abb[10] * z[103];
z[59] = abb[4] * z[59];
z[3] = z[3] + z[39] + z[59];
z[39] = abb[49] + z[68];
z[59] = -z[36] + z[39];
z[70] = z[59] + z[84];
z[71] = abb[6] * z[70];
z[78] = abb[60] * z[74];
z[78] = z[71] + z[78] + -z[118];
z[23] = abb[7] * z[23];
z[90] = z[58] * z[157];
z[3] = 2 * z[3] + z[23] + z[78] + z[90];
z[3] = abb[30] * z[3];
z[23] = -abb[2] * z[81];
z[58] = -abb[4] * z[58];
z[23] = z[23] + -z[49] + z[58];
z[1] = z[1] + z[91];
z[58] = 4 * abb[3];
z[1] = z[1] * z[58];
z[23] = z[1] + 2 * z[23] + -z[78] + z[139];
z[23] = abb[31] * z[23];
z[3] = z[3] + z[17] + z[23] + -z[89];
z[3] = abb[32] * z[3];
z[17] = -abb[52] + z[19];
z[17] = 10 * abb[57] + 2 * z[17] + z[60] + -z[61];
z[17] = abb[0] * z[17];
z[23] = -abb[45] + -z[56] + z[67];
z[23] = abb[9] * z[23];
z[56] = z[52] + z[143];
z[56] = abb[60] * z[56];
z[18] = z[18] + -z[39];
z[18] = abb[4] * z[18];
z[17] = -z[17] + z[18] + -z[20] + z[23] + z[56] + -6 * z[88];
z[18] = -z[17] + -z[147];
z[18] = abb[61] * z[18];
z[20] = abb[4] * z[103];
z[20] = z[20] + -z[49];
z[23] = -z[40] + z[64];
z[49] = -3 * abb[51] + -z[32] + z[153];
z[56] = abb[56] + z[23] + z[46] + z[49] + z[115];
z[56] = abb[16] * z[56];
z[58] = z[35] + -z[102];
z[58] = z[58] * z[104];
z[60] = z[52] + z[134];
z[60] = abb[60] * z[60];
z[20] = 2 * z[20] + z[56] + z[58] + z[60] + -z[133] + z[147];
z[58] = -abb[62] * z[20];
z[50] = z[43] + z[50];
z[48] = 2 * z[48];
z[48] = abb[4] * z[48];
z[60] = abb[27] + z[52];
z[60] = abb[60] * z[60];
z[48] = -z[48] + -z[50] + z[56] + z[60] + -z[148];
z[56] = 4 * abb[48];
z[60] = z[56] + z[126];
z[67] = z[60] + z[62];
z[78] = -z[22] + -z[32] + -z[46] + z[67];
z[81] = abb[3] + abb[7];
z[81] = z[78] * z[81];
z[81] = -z[48] + z[81];
z[81] = abb[64] * z[81];
z[88] = -z[40] + z[107];
z[19] = z[19] + z[88];
z[90] = 5 * abb[47];
z[19] = 2 * z[19] + -z[51] + z[90];
z[19] = abb[2] * z[19];
z[52] = z[52] + z[74];
z[52] = abb[60] * z[52];
z[74] = abb[4] * z[70];
z[19] = z[19] + -z[52] + z[74] + z[154];
z[52] = z[19] + -z[147];
z[52] = abb[63] * z[52];
z[14] = z[14] + -z[79];
z[34] = -z[14] + -z[34] + z[39];
z[34] = abb[4] * z[34];
z[39] = -abb[60] * z[96];
z[15] = z[15] + z[34] + z[39] + z[43];
z[15] = abb[36] * z[15];
z[31] = -z[31] + -z[59] + z[125] + z[146] + -z[156];
z[34] = abb[63] * z[31];
z[22] = z[22] + -z[41] + z[67] + -z[146];
z[22] = abb[36] * z[22];
z[39] = abb[35] * z[70];
z[34] = z[22] + z[34] + z[39];
z[34] = abb[3] * z[34];
z[39] = abb[51] + -z[24] + -z[36] + z[37] + z[84];
z[39] = abb[4] * z[39];
z[28] = z[24] + -z[28] + -z[83];
z[28] = z[28] * z[94];
z[41] = -abb[26] + -z[53];
z[41] = abb[60] * z[41];
z[28] = -z[26] + z[28] + z[39] + z[41];
z[28] = abb[35] * z[28];
z[32] = -abb[56] + z[32] + z[113] + -z[127];
z[39] = -abb[62] * z[32];
z[25] = abb[35] * z[25];
z[22] = z[22] + z[25] + z[39];
z[22] = abb[7] * z[22];
z[25] = abb[65] * z[141];
z[39] = abb[19] * z[77];
z[5] = -abb[58] + z[5] + z[76];
z[5] = abb[18] * z[5];
z[41] = abb[17] + abb[20];
z[41] = z[41] * z[72];
z[43] = abb[28] * z[99];
z[5] = z[5] + z[39] + z[41] + z[43];
z[39] = -abb[66] * z[5];
z[41] = -z[33] + -z[116] + z[155];
z[41] = abb[68] * z[41];
z[43] = abb[16] * z[63];
z[59] = abb[61] + abb[63];
z[59] = z[43] * z[59];
z[63] = -abb[35] + -abb[63];
z[63] = z[63] * z[71];
z[0] = abb[67] + z[0] + z[2] + z[3] + z[7] + z[8] + z[10] + z[15] + z[18] + z[22] + z[25] + z[28] + z[30] + z[34] + z[39] + z[41] + z[52] + z[58] + z[59] + z[63] + z[81];
z[2] = z[12] + z[49] + z[156];
z[3] = -z[2] + z[9];
z[3] = z[3] * z[104];
z[7] = -abb[56] + -z[24] + -z[40] + -z[47] + -z[80] + z[128];
z[7] = abb[7] * z[7];
z[8] = z[65] + z[136];
z[9] = 3 * abb[56] + -z[13];
z[10] = z[9] + z[54] + -z[62];
z[12] = -z[46] + z[60];
z[14] = abb[49] + abb[51] + -z[10] + -z[12] + -z[14] + z[24];
z[14] = abb[4] * z[14];
z[15] = z[82] * z[138];
z[18] = -abb[60] * z[97];
z[3] = z[3] + z[7] + 2 * z[8] + z[14] + z[15] + z[18] + z[27] + z[109] + z[123] + -z[137] + z[151];
z[3] = abb[29] * z[3];
z[7] = z[36] + -z[114];
z[8] = -abb[49] + z[4];
z[9] = abb[51] + -z[7] + -z[8] + -z[9] + z[23] + -z[86];
z[9] = abb[4] * z[9];
z[11] = 4 * abb[49] + 12 * abb[57] + -z[11] + -z[44] + z[46] + -z[66];
z[11] = z[11] * z[75];
z[14] = 11 * abb[47] + -z[51] + 8 * z[69];
z[14] = abb[12] * z[14];
z[15] = -z[131] + -2 * z[160];
z[15] = abb[2] * z[15];
z[8] = -abb[59] + -z[8] + z[152];
z[8] = z[8] * z[157];
z[18] = -abb[60] * z[53];
z[8] = z[8] + z[9] + z[11] + z[14] + z[15] + z[18] + z[50] + -z[71] + z[139] + -z[145];
z[8] = abb[31] * z[8];
z[9] = abb[49] * (T(-7) / T(2)) + z[16] + z[36] + -z[46] + -z[105] + z[106] + z[126];
z[9] = abb[4] * z[9];
z[11] = -z[110] + 2 * z[118];
z[14] = -z[29] + z[42] + -z[116];
z[14] = z[14] * z[94];
z[15] = z[13] + -z[46];
z[16] = abb[51] * (T(3) / T(2)) + z[15];
z[18] = -z[16] + (T(3) / T(2)) * z[37] + z[119];
z[18] = abb[7] * z[18];
z[22] = 2 * abb[27];
z[23] = abb[21] + -abb[26] + 3 * z[53];
z[23] = -z[22] + (T(1) / T(2)) * z[23];
z[23] = abb[60] * z[23];
z[1] = -z[1] + z[9] + -z[11] + z[14] + z[18] + z[23] + z[71] + z[98] + z[137];
z[1] = abb[32] * z[1];
z[7] = -z[7] + -z[15] + z[33] + -z[68] + z[153];
z[7] = abb[3] * z[7];
z[9] = z[10] + -z[21] + z[56] + -z[125];
z[9] = abb[4] * z[9];
z[10] = -abb[49] + z[13] + -z[85] + -z[88];
z[10] = -abb[48] + 2 * z[10] + -z[24] + -z[90];
z[10] = abb[2] * z[10];
z[2] = z[2] + z[35];
z[2] = z[2] * z[104];
z[12] = 5 * abb[51] + -16 * abb[57] + z[12] + -z[55] + z[61];
z[12] = abb[16] * z[12];
z[2] = z[2] + z[7] + z[9] + z[10] + z[12] + z[26] + -z[122] + -z[154];
z[2] = abb[30] * z[2];
z[7] = abb[50] + -abb[59];
z[4] = z[4] + -8 * z[7] + -z[56] + -z[64] + -z[125] + z[129];
z[4] = abb[7] * z[4];
z[6] = z[6] + -z[16] + z[38] + z[73];
z[6] = abb[4] * z[6];
z[7] = z[22] + -z[111];
z[7] = abb[60] * z[7];
z[4] = z[4] + z[6] + z[7] + z[11] + -z[57] + -z[87] + -z[124];
z[4] = abb[33] * z[4];
z[1] = z[1] + z[2] + z[3] + z[4] + z[8] + -z[89];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = z[43] + -z[147];
z[3] = z[2] + -z[17];
z[3] = abb[37] * z[3];
z[4] = -abb[7] * z[32];
z[4] = z[4] + -z[20];
z[4] = abb[38] * z[4];
z[6] = abb[7] * z[78];
z[6] = z[6] + -z[48];
z[6] = abb[40] * z[6];
z[2] = z[2] + z[19] + -z[71];
z[2] = abb[39] * z[2];
z[7] = abb[39] * z[31];
z[8] = abb[40] * z[78];
z[9] = -abb[41] * z[135];
z[7] = z[7] + z[8] + z[9];
z[7] = abb[3] * z[7];
z[5] = -abb[42] * z[5];
z[8] = z[117] + -z[140];
z[8] = abb[41] * z[8];
z[9] = z[45] + -z[116];
z[9] = abb[44] * z[9];
z[1] = abb[43] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_636_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("24.100744672951019957833951438829635161254142234583186469650513184"),stof<T>("13.91300768368572772009465068814295555155981333917183113609439933")}, std::complex<T>{stof<T>("-12.965657685201312187551673847516739906553844473329630422451910049"),stof<T>("-14.717317565851156666902452181361696643512173869038551076784389515")}, std::complex<T>{stof<T>("-7.453038353474326796558556191968674926058058888595805807487136502"),stof<T>("64.04209729914023530490019461280411042555154066205072648126593664")}, std::complex<T>{stof<T>("17.468543557532954303971307873365427474443096261051764987349787513"),stof<T>("-67.941610198319301954129935275599261908653789574060472776748485994")}, std::complex<T>{stof<T>("1.8827408479976437532709400880418589086126969562613697022593861952"),stof<T>("-9.6285066854902476379015642293687694902048801435636308776649461313")}, std::complex<T>{stof<T>("-9.995029773791171767176705415877449041026376800712791480717387862"),stof<T>("20.199375289806753333762580829392497428111708869531784307351473659")}, std::complex<T>{stof<T>("29.262881117601863291600794600545237935499014978160937005519255813"),stof<T>("-15.274691385661001291898558756037620512961438167844619665859067067")}, std::complex<T>{stof<T>("-11.1350869877497077702822775913128952547002977612535560471986031353"),stof<T>("0.8043098821654289468078014932187410919523605298667199406899901851")}, std::complex<T>{stof<T>("-19.491378573675435230138371031232186001404288077511675779743200558"),stof<T>("-53.761772318579331095931319256470211697622266360644677308971367916")}, std::complex<T>{stof<T>("-20.010534977849799274589457097274201589411414173168750660580038873"),stof<T>("24.098888188985819982992321492187648911213957781541530602834023013")}, std::complex<T>{stof<T>("-8.524530339827257308590758979917258376302264784503566825563001984"),stof<T>("104.917860599706457074633101611784395342211933030509247010433432214")}, std::complex<T>{stof<T>("0.6687458038860240805037139439018829383487291920268642821032152032"),stof<T>("4.3436871840284605831045519526160562417641996288129021997010243338")}, std::complex<T>{stof<T>("0.635768643898473000399275498646065350820234840115675302046332924"),stof<T>("-45.509903580956372324704787828645332621833108645583580913267943362")}, std::complex<T>{stof<T>("0.3770995406714526367745592174686979133639260616043059036115821508"),stof<T>("-7.0856312627672268401954367563287616384305251855206658797846966884")}, std::complex<T>{stof<T>("7.0664435812360512966274921664650092807010245908202515927432043125"),stof<T>("-7.8289245255777394387701317963110225412405031692603897496028551735")}, std::complex<T>{stof<T>("8.2392111501723654087725285414709311842632066857929314133244832248"),stof<T>("-2.689250513928415495653135252175836801630141918964692483472961784")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(abs(k.W[43])) - rlog(abs(kbase.W[43])), rlog(abs(k.W[89])) - rlog(abs(kbase.W[89])), C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_636_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_636_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (-8 + 7 * v[0] + -3 * v[1] + -5 * v[2] + -v[3] + 4 * v[4] + 2 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + 5 * v[5])) / prod_pow(tend, 2);
c[1] = ((1 + m1_set::bc<T>[1]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[45] + -abb[46] + abb[48] + abb[50] + -abb[59]) * (t * c[0] + -c[1]);
	}
	{
T z[2];
z[0] = abb[29] + -abb[32];
z[1] = abb[34] + z[0];
z[1] = abb[34] * z[1];
z[0] = abb[33] + z[0];
z[0] = abb[33] * z[0];
z[0] = abb[36] + -z[0] + z[1];
z[1] = abb[45] + -abb[46] + abb[48] + abb[50] + -abb[59];
return 2 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_636_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[45] + -abb[46] + abb[48] + abb[50] + -abb[59]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[45] + abb[46] + -abb[48] + -abb[50] + abb[59];
z[1] = -abb[33] + abb[34];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_636_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-14.8606844294719202977083930117954607963308343868167817279897138"),stof<T>("59.184131980752512909628491886741642060475018320814916870299090332")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 43, 89});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,69> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W17(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(abs(kend.W[43])) - rlog(abs(k.W[43])), rlog(abs(kend.W[89])) - rlog(abs(k.W[89])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), T{0}, T{0}};
abb[43] = SpDLog_f_4_636_W_17_Im(t, path, abb);
abb[44] = SpDLogQ_W_90(k,dl,dlr).imag();
abb[67] = SpDLog_f_4_636_W_17_Re(t, path, abb);
abb[68] = SpDLogQ_W_90(k,dl,dlr).real();

                    
            return f_4_636_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_636_DLogXconstant_part(base_point<T>, kend);
	value += f_4_636_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_636_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_636_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_636_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_636_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_636_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_636_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
