/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_485.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_485_abbreviated (const std::array<T,59>& abb) {
T z[86];
z[0] = abb[48] + -abb[49];
z[1] = abb[14] * z[0];
z[2] = -abb[43] + -abb[45] + abb[46] + abb[47];
z[3] = abb[44] + (T(-1) / T(2)) * z[2];
z[4] = 2 * abb[53] + z[3];
z[5] = abb[18] * z[4];
z[1] = z[1] + -z[5];
z[5] = abb[48] + abb[51];
z[6] = abb[10] * z[5];
z[7] = abb[15] * z[0];
z[8] = abb[19] * z[4];
z[9] = z[1] + -z[6] + z[7] + -z[8];
z[10] = abb[48] + abb[50];
z[11] = abb[49] + z[10];
z[12] = abb[12] * z[11];
z[13] = -abb[49] + abb[51];
z[14] = abb[2] * z[13];
z[15] = z[12] + -z[14];
z[16] = abb[20] * abb[52];
z[17] = abb[22] + abb[24];
z[17] = abb[52] * z[17];
z[18] = z[16] + z[17];
z[19] = abb[25] * abb[52];
z[19] = z[18] + z[19];
z[20] = abb[23] * abb[52];
z[21] = abb[51] + z[11];
z[22] = abb[8] * z[21];
z[23] = z[20] + z[22];
z[24] = abb[21] * abb[52];
z[25] = z[23] + z[24];
z[26] = abb[50] + abb[51];
z[27] = (T(1) / T(3)) * z[26];
z[28] = abb[49] * (T(1) / T(2));
z[29] = abb[48] * (T(5) / T(6)) + z[27] + z[28];
z[29] = abb[6] * z[29];
z[30] = (T(2) / T(3)) * z[10] + z[28];
z[30] = abb[4] * z[30];
z[31] = abb[48] + abb[49];
z[32] = z[26] + -z[31];
z[33] = abb[3] * z[32];
z[3] = abb[53] + (T(1) / T(2)) * z[3];
z[34] = abb[16] * z[3];
z[35] = abb[17] * z[3];
z[36] = abb[1] * z[10];
z[27] = abb[49] + z[27];
z[27] = abb[48] + (T(1) / T(2)) * z[27];
z[27] = abb[7] * z[27];
z[37] = (T(1) / T(2)) * z[26];
z[38] = abb[48] * (T(1) / T(3)) + -z[37];
z[38] = abb[0] * z[38];
z[9] = (T(-1) / T(3)) * z[9] + (T(-2) / T(3)) * z[15] + (T(-1) / T(6)) * z[19] + (T(-5) / T(12)) * z[25] + z[27] + z[29] + z[30] + (T(1) / T(12)) * z[33] + (T(5) / T(3)) * z[34] + z[35] + z[36] + z[38];
z[9] = prod_pow(m1_set::bc<T>[0], 2) * z[9];
z[15] = (T(1) / T(2)) * z[0];
z[19] = abb[15] * z[15];
z[25] = abb[19] * z[3];
z[27] = abb[52] * (T(1) / T(4));
z[29] = abb[25] * z[27];
z[19] = z[19] + -z[25] + z[29];
z[25] = -abb[50] + abb[51];
z[29] = z[25] + z[31];
z[30] = abb[5] * z[29];
z[33] = (T(1) / T(4)) * z[30];
z[38] = -abb[49] + abb[50];
z[39] = abb[9] * z[38];
z[40] = z[33] + z[39];
z[41] = abb[14] * z[15];
z[42] = abb[18] * z[3];
z[41] = z[41] + -z[42];
z[42] = abb[6] * z[29];
z[43] = -z[16] + -z[42];
z[44] = -z[10] + z[13];
z[45] = abb[0] * z[44];
z[46] = abb[4] * abb[48];
z[47] = abb[7] * abb[49];
z[43] = -z[6] + -z[14] + z[19] + z[36] + z[40] + -z[41] + (T(1) / T(4)) * z[43] + z[45] + z[46] + z[47];
z[43] = abb[28] * z[43];
z[45] = z[10] + z[13];
z[46] = abb[6] * z[45];
z[47] = -z[16] + z[46];
z[48] = abb[49] + abb[50];
z[48] = abb[48] + -abb[51] + 3 * z[48];
z[49] = abb[0] * (T(1) / T(2));
z[48] = z[48] * z[49];
z[50] = abb[13] * z[45];
z[51] = (T(1) / T(2)) * z[50];
z[52] = abb[52] * (T(1) / T(2));
z[53] = abb[21] * z[52];
z[54] = z[51] + z[53];
z[55] = abb[16] * z[4];
z[56] = z[54] + -z[55];
z[57] = abb[7] * z[31];
z[58] = -z[56] + z[57];
z[59] = 2 * z[12];
z[60] = 4 * z[36] + -z[59];
z[61] = abb[4] * z[10];
z[47] = -z[1] + (T(1) / T(2)) * z[47] + -z[48] + z[58] + z[60] + 2 * z[61];
z[47] = abb[30] * z[47];
z[61] = abb[25] * z[52];
z[7] = z[7] + -z[8] + z[61];
z[8] = 3 * abb[48];
z[61] = abb[49] + z[8];
z[62] = -abb[50] + 3 * abb[51] + -z[61];
z[62] = z[49] * z[62];
z[62] = z[7] + z[62];
z[46] = -z[17] + z[46];
z[63] = abb[4] * z[31];
z[64] = abb[17] * z[4];
z[63] = z[63] + z[64];
z[65] = abb[7] * z[13];
z[66] = 4 * z[14];
z[46] = (T(1) / T(2)) * z[46] + -z[54] + -z[62] + -z[63] + 2 * z[65] + z[66];
z[46] = abb[31] * z[46];
z[43] = z[43] + z[46] + -z[47];
z[43] = abb[28] * z[43];
z[65] = 3 * abb[49];
z[10] = z[10] + z[65];
z[67] = abb[51] + -z[10];
z[67] = abb[6] * z[67];
z[68] = abb[7] * z[21];
z[67] = -z[17] + -z[67] + z[68];
z[52] = abb[23] * z[52];
z[69] = (T(1) / T(2)) * z[22];
z[52] = z[52] + z[69];
z[70] = -z[51] + z[52];
z[71] = abb[48] + z[65];
z[72] = -z[26] + z[71];
z[73] = abb[0] * (T(1) / T(4));
z[74] = -z[72] * z[73];
z[28] = abb[48] * (T(1) / T(2)) + -z[28];
z[75] = abb[50] + z[28];
z[75] = abb[4] * z[75];
z[40] = -z[12] + -z[35] + (T(3) / T(2)) * z[36] + -z[40] + (T(-1) / T(4)) * z[67] + z[70] + z[74] + z[75];
z[67] = prod_pow(abb[30], 2);
z[40] = z[40] * z[67];
z[74] = abb[4] * z[32];
z[13] = -abb[50] + -z[8] + z[13];
z[13] = abb[6] * z[13];
z[13] = z[13] + z[17] + z[74];
z[75] = z[26] + z[61];
z[73] = -z[73] * z[75];
z[28] = -abb[51] + -z[28];
z[28] = abb[7] * z[28];
z[13] = (T(1) / T(4)) * z[13] + (T(-3) / T(2)) * z[14] + z[28] + -z[33] + -z[34] + (T(-1) / T(2)) * z[39] + z[54] + z[73];
z[28] = prod_pow(abb[31], 2);
z[13] = z[13] * z[28];
z[33] = 2 * z[14];
z[19] = z[19] + -z[33];
z[73] = z[19] + z[54];
z[18] = (T(1) / T(2)) * z[18];
z[76] = abb[6] * z[72];
z[76] = z[18] + z[76];
z[77] = 2 * z[36];
z[41] = z[41] + -z[77];
z[78] = abb[50] * (T(1) / T(2));
z[79] = abb[51] * (T(1) / T(2));
z[80] = z[78] + z[79];
z[81] = 2 * abb[49];
z[82] = -z[80] + z[81];
z[82] = abb[4] * z[82];
z[78] = abb[51] * (T(3) / T(2)) + -z[78];
z[83] = z[31] + -z[78];
z[83] = abb[7] * z[83];
z[10] = abb[51] + z[10];
z[10] = z[10] * z[49];
z[84] = 3 * z[3];
z[85] = abb[17] * z[84];
z[10] = z[10] + z[34] + z[41] + z[73] + (T(1) / T(2)) * z[76] + z[82] + z[83] + z[85];
z[10] = abb[28] * z[10];
z[34] = z[49] * z[72];
z[72] = abb[6] * abb[48];
z[76] = 2 * abb[48];
z[82] = abb[4] * z[76];
z[34] = -z[34] + z[72] + z[82];
z[72] = z[14] + z[36];
z[8] = z[8] + z[65];
z[65] = z[8] + -z[25];
z[82] = abb[7] * (T(1) / T(2));
z[83] = -z[65] * z[82];
z[83] = -z[34] + -z[55] + z[72] + z[83];
z[83] = abb[32] * z[83];
z[34] = z[34] + z[58];
z[58] = abb[31] * z[34];
z[10] = z[10] + z[47] + z[58] + z[83];
z[10] = abb[32] * z[10];
z[47] = abb[16] * z[84];
z[41] = -z[41] + z[47] + -z[59];
z[47] = z[76] + z[81];
z[58] = abb[50] * (T(3) / T(2)) + -z[79];
z[79] = z[47] + z[58];
z[79] = abb[4] * z[79];
z[79] = z[41] + z[79] + z[85];
z[78] = -z[47] + z[78];
z[78] = abb[7] * z[78];
z[83] = z[16] + -z[17];
z[25] = abb[0] * z[25];
z[84] = abb[6] * z[31];
z[25] = z[19] + z[25] + -z[78] + z[79] + (T(-1) / T(4)) * z[83] + z[84];
z[25] = abb[32] * z[25];
z[25] = z[25] + z[46];
z[46] = abb[6] * z[75];
z[46] = -z[18] + z[46];
z[58] = z[31] + z[58];
z[58] = abb[4] * z[58];
z[37] = z[37] + z[76];
z[37] = abb[7] * z[37];
z[76] = -z[26] + z[61];
z[76] = z[49] * z[76];
z[35] = z[35] + z[37] + z[41] + (T(1) / T(2)) * z[46] + z[58] + -z[73] + z[76];
z[35] = abb[28] * z[35];
z[37] = z[49] * z[75];
z[41] = abb[6] * abb[49];
z[46] = abb[7] * z[81];
z[37] = -z[37] + z[41] + z[46];
z[41] = abb[4] * z[65];
z[12] = z[12] + -z[37] + (T(-1) / T(2)) * z[41] + -z[64] + -z[72];
z[12] = abb[33] * z[12];
z[41] = -z[63] + z[70];
z[37] = -z[37] + z[41];
z[46] = -abb[30] * z[37];
z[12] = z[12] + -z[25] + z[35] + z[46];
z[12] = abb[33] * z[12];
z[35] = 2 * abb[50];
z[46] = z[35] + z[61];
z[46] = abb[4] * z[46];
z[21] = abb[6] * z[21];
z[16] = -z[16] + z[21];
z[21] = 2 * abb[0];
z[49] = z[11] * z[21];
z[58] = z[55] + z[64];
z[52] = z[52] + z[53];
z[65] = abb[7] * z[71];
z[16] = -z[1] + (T(1) / T(2)) * z[16] + z[46] + -z[49] + -z[52] + z[58] + z[60] + z[65];
z[46] = abb[56] * z[16];
z[44] = abb[6] * z[44];
z[44] = z[17] + z[44];
z[20] = z[20] + z[44] + -z[50] + -z[68];
z[22] = z[20] + z[22];
z[30] = (T(1) / T(2)) * z[30];
z[49] = z[6] + -z[30] + z[36];
z[22] = (T(1) / T(2)) * z[22] + z[49];
z[60] = -abb[30] * z[22];
z[63] = z[55] + z[63];
z[42] = z[17] + z[42];
z[42] = -z[30] + (T(1) / T(2)) * z[42] + z[57] + z[63];
z[42] = abb[28] * z[42];
z[37] = abb[33] * z[37];
z[14] = z[14] + z[39];
z[49] = z[14] + -z[49];
z[57] = z[44] + z[74];
z[65] = z[57] + -z[68];
z[65] = -z[49] + (T(1) / T(2)) * z[65];
z[65] = abb[29] * z[65];
z[24] = z[24] + z[50] + z[57];
z[14] = -z[14] + (T(1) / T(2)) * z[24];
z[24] = -z[14] + z[30];
z[24] = abb[31] * z[24];
z[34] = -abb[32] * z[34];
z[50] = abb[3] * (T(1) / T(2));
z[50] = z[32] * z[50];
z[57] = abb[31] + -abb[32];
z[57] = z[50] * z[57];
z[24] = z[24] + z[34] + z[37] + z[42] + z[57] + z[60] + (T(1) / T(2)) * z[65];
z[24] = abb[29] * z[24];
z[32] = abb[6] * z[32];
z[17] = -z[17] + z[32];
z[32] = -abb[51] + z[31];
z[32] = z[21] * z[32];
z[34] = -abb[4] * z[61];
z[37] = 2 * abb[51] + -z[71];
z[37] = abb[7] * z[37];
z[7] = -z[7] + (T(1) / T(2)) * z[17] + z[32] + z[34] + z[37] + -z[58] + z[66];
z[7] = abb[35] * z[7];
z[17] = abb[18] + abb[19];
z[15] = z[15] * z[17];
z[17] = abb[4] + abb[7] + -abb[14] + -abb[15];
z[3] = z[3] * z[17];
z[17] = z[31] + z[80];
z[17] = abb[16] * z[17];
z[32] = -z[31] + z[80];
z[32] = abb[17] * z[32];
z[2] = -2 * abb[44] + -4 * abb[53] + z[2];
z[2] = abb[27] * z[2];
z[4] = abb[6] * z[4];
z[27] = abb[26] * z[27];
z[2] = -z[2] + z[3] + z[4] + z[15] + z[17] + z[27] + -z[32];
z[3] = -abb[57] * z[2];
z[4] = z[30] + 2 * z[39];
z[1] = z[1] + -z[4] + z[18];
z[15] = abb[6] * z[38];
z[17] = abb[4] * z[0];
z[15] = -z[1] + z[15] + z[17] + -z[48] + -z[54] + -z[64] + z[77];
z[15] = abb[34] * z[15];
z[5] = abb[6] * z[5];
z[0] = abb[7] * z[0];
z[0] = z[0] + z[5] + 2 * z[6] + z[33] + -z[56] + -z[62];
z[5] = z[0] + -z[30];
z[17] = abb[54] * z[5];
z[20] = (T(-1) / T(2)) * z[20] + -z[36] + -z[69];
z[20] = abb[55] * z[20];
z[27] = abb[4] + -abb[7];
z[27] = z[27] * z[45];
z[27] = z[27] + z[44];
z[30] = -abb[0] * z[31];
z[27] = (T(1) / T(2)) * z[27] + z[30] + -z[49];
z[27] = abb[37] * z[27];
z[14] = abb[36] * z[14];
z[30] = -abb[36] + abb[55];
z[31] = abb[5] * (T(1) / T(2));
z[29] = z[29] * z[30] * z[31];
z[30] = abb[31] * abb[32];
z[30] = -abb[35] + -abb[36] + -z[28] + z[30];
z[30] = z[30] * z[50];
z[28] = -abb[55] + z[28] + (T(1) / T(2)) * z[67];
z[28] = z[6] * z[28];
z[3] = abb[58] + z[3] + z[7] + z[9] + z[10] + z[12] + z[13] + z[14] + z[15] + z[17] + z[20] + z[24] + z[27] + z[28] + z[29] + z[30] + z[40] + z[43] + z[46];
z[7] = z[8] + z[26];
z[8] = z[7] * z[82];
z[6] = z[6] + -z[8];
z[8] = abb[6] * z[11];
z[9] = -abb[50] * z[21];
z[1] = -z[1] + -z[6] + z[8] + z[9] + -z[23] + z[36] + z[51] + -z[53] + z[63];
z[1] = abb[30] * z[1];
z[8] = abb[4] * z[35];
z[0] = z[0] + -z[4] + z[8] + -z[59] + z[77];
z[0] = abb[28] * z[0];
z[4] = -abb[6] * z[7];
z[4] = z[4] + z[18];
z[7] = -z[47] + -z[80];
z[7] = abb[7] * z[7];
z[8] = abb[0] * z[26];
z[4] = (T(1) / T(2)) * z[4] + z[7] + z[8] + z[19] + z[52] + -z[79];
z[4] = abb[33] * z[4];
z[6] = z[6] + z[36] + z[41] + -z[55] + -z[84];
z[6] = abb[29] * z[6];
z[0] = z[0] + z[1] + z[4] + z[6] + -z[25];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[40] * z[16];
z[2] = -abb[41] * z[2];
z[4] = abb[38] * z[5];
z[5] = -abb[39] * z[22];
z[0] = abb[42] + z[0] + z[1] + z[2] + z[4] + z[5];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_485_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.886161099690250984741505458611170936143031814797445273809479501"),stof<T>("-3.7792196039290779840740318596922210627402822973481223569948307766")}, std::complex<T>{stof<T>("1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("-7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("0.886161099690250984741505458611170936143031814797445273809479501"),stof<T>("-3.7792196039290779840740318596922210627402822973481223569948307766")}, std::complex<T>{stof<T>("-0.886161099690250984741505458611170936143031814797445273809479501"),stof<T>("3.7792196039290779840740318596922210627402822973481223569948307766")}, std::complex<T>{stof<T>("-0.886161099690250984741505458611170936143031814797445273809479501"),stof<T>("3.7792196039290779840740318596922210627402822973481223569948307766")}, std::complex<T>{stof<T>("-21.606546010172162322515379700341139867996638041966289214523326148"),stof<T>("-2.702000933968601905048161264218677370994974637217526244113750991")}, std::complex<T>{stof<T>("-19.251487639891229114804122255455639533884839238370546186370837468"),stof<T>("2.702791952986176229640274038496251748496755566419030395721309865")}, std::complex<T>{stof<T>("-0.5527624557607640950123004076538785599513755392459028678507623014"),stof<T>("8.6180817887752370110104092174451592570304689492490970882826963077")}, std::complex<T>{stof<T>("3.2904641725129597777725651834777845479008968023777904580793849356"),stof<T>("8.6310949606228472502995409892366960969985099394394082902086153971")}, std::complex<T>{stof<T>("-1.929047365971231066630042088008710289906983071846621682967563806"),stof<T>("0.1299667118265977184776204054735914385079853180536135282563373817")}, std::complex<T>{stof<T>("3.544644398761003938966021834444683744572127259189781095237918004"),stof<T>("-15.116878415716311936296127438768884250961129189392489427979323107")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[124].real()/kbase.W[124].real()), rlog(k.W[127].real()/kbase.W[127].real()), C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}, rlog(k.W[195].real()/kbase.W[195].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_485_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_485_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (v[2] + v[3]) * (-16 + -8 * v[0] + -4 * v[1] + 3 * v[2] + 7 * v[3] + 4 * v[4] + -4 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -8 * v[5])) / prod_pow(tend, 2);
c[1] = ((-1 + 2 * m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;


		return (abb[48] + abb[49] + -abb[51]) * (t * c[0] + c[1]);
	}
	{
T z[5];
z[0] = 2 * abb[11];
z[1] = abb[28] * z[0];
z[2] = abb[11] * abb[32];
z[3] = z[1] + -z[2];
z[3] = abb[32] * z[3];
z[4] = abb[11] * abb[31];
z[1] = -z[1] + z[4];
z[1] = abb[31] * z[1];
z[2] = -z[2] + z[4];
z[2] = abb[33] * z[2];
z[0] = -abb[35] * z[0];
z[0] = z[0] + z[1] + 2 * z[2] + z[3];
z[1] = -abb[48] + -abb[49] + abb[51];
return z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_485_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[48] + abb[49] + -abb[51]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[31] + abb[32];
z[1] = abb[48] + abb[49] + -abb[51];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_485_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-14.19964576974122959598944279035209578005745742925733362181650657"),stof<T>("38.621420869721328157496920574462338387013500108456424778024073909")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,59> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W19(k,dl), dlog_W24(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_3(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_25_im(k), T{0}, rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[124].real()/k.W[124].real()), rlog(kend.W[127].real()/k.W[127].real()), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), rlog(kend.W[195].real()/k.W[195].real()), f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_25_re(k), T{0}};
abb[42] = SpDLog_f_4_485_W_19_Im(t, path, abb);
abb[58] = SpDLog_f_4_485_W_19_Re(t, path, abb);

                    
            return f_4_485_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_485_DLogXconstant_part(base_point<T>, kend);
	value += f_4_485_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_485_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_485_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_485_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_485_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_485_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_485_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
