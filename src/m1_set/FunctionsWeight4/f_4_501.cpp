/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_501.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_501_abbreviated (const std::array<T,19>& abb) {
T z[31];
z[0] = abb[14] + -abb[15];
z[1] = 2 * z[0];
z[2] = abb[12] + -abb[13];
z[3] = -z[1] + z[2];
z[4] = 2 * abb[16];
z[5] = z[3] + z[4];
z[5] = abb[0] * z[5];
z[6] = -abb[16] + z[2];
z[7] = z[0] + z[6];
z[8] = 2 * abb[1];
z[7] = z[7] * z[8];
z[9] = 4 * abb[16];
z[3] = z[3] + -z[9];
z[10] = abb[5] * z[3];
z[11] = z[0] + z[4];
z[12] = 2 * z[2];
z[13] = z[11] + -z[12];
z[14] = abb[2] * z[13];
z[15] = abb[3] * z[13];
z[5] = z[5] + z[7] + -z[10] + z[14] + -z[15];
z[7] = abb[17] * z[5];
z[10] = z[2] + z[4];
z[14] = abb[6] * z[10];
z[16] = (T(3) / T(2)) * z[2];
z[11] = -z[11] + -z[16];
z[11] = abb[8] * z[11];
z[17] = abb[16] + z[0];
z[18] = 2 * z[17];
z[19] = abb[7] * z[18];
z[11] = z[11] + z[14] + z[19];
z[11] = abb[8] * z[11];
z[14] = (T(1) / T(2)) * z[2];
z[17] = z[14] + z[17];
z[19] = prod_pow(abb[6], 2);
z[17] = z[17] * z[19];
z[20] = abb[6] * z[1];
z[21] = -abb[7] * abb[16];
z[21] = -z[20] + z[21];
z[21] = abb[7] * z[21];
z[22] = -z[0] + -z[12];
z[23] = abb[16] * (T(3) / T(2));
z[22] = (T(1) / T(3)) * z[22] + -z[23];
z[24] = prod_pow(m1_set::bc<T>[0], 2);
z[22] = z[22] * z[24];
z[11] = z[11] + z[17] + z[21] + z[22];
z[11] = abb[0] * z[11];
z[17] = -z[4] + z[12];
z[21] = -3 * z[0];
z[22] = z[17] + z[21];
z[25] = abb[7] * z[22];
z[26] = (T(-1) / T(2)) * z[0];
z[27] = -z[17] + -z[26];
z[27] = abb[8] * z[27];
z[28] = abb[6] * z[6];
z[27] = z[25] + z[27] + 2 * z[28];
z[27] = abb[8] * z[27];
z[23] = (T(1) / T(3)) * z[0] + -z[16] + z[23];
z[23] = z[23] * z[24];
z[28] = -z[0] + z[6];
z[29] = z[19] * z[28];
z[30] = (T(3) / T(2)) * z[0] + -z[6];
z[30] = abb[7] * z[30];
z[30] = z[20] + z[30];
z[30] = abb[7] * z[30];
z[23] = z[23] + z[27] + z[29] + z[30];
z[23] = abb[1] * z[23];
z[27] = -z[0] + z[2];
z[27] = -z[4] + 3 * z[27];
z[27] = abb[7] * z[27];
z[21] = z[2] + z[21];
z[29] = abb[16] + (T(-1) / T(2)) * z[21];
z[29] = abb[8] * z[29];
z[20] = -z[20] + z[27] + z[29];
z[20] = abb[8] * z[20];
z[27] = z[6] + -z[26];
z[27] = z[19] * z[27];
z[22] = -abb[6] * z[22];
z[29] = -abb[7] * z[16];
z[22] = z[22] + z[29];
z[22] = abb[7] * z[22];
z[20] = z[20] + z[22] + z[27];
z[20] = abb[2] * z[20];
z[9] = -z[1] + -z[9] + -z[14];
z[9] = z[9] * z[19];
z[14] = -abb[6] * z[3];
z[16] = abb[8] * z[16];
z[14] = z[14] + z[16];
z[14] = abb[8] * z[14];
z[12] = 10 * abb[16] + 5 * z[0] + z[12];
z[16] = (T(1) / T(3)) * z[24];
z[12] = z[12] * z[16];
z[9] = z[9] + z[12] + z[14];
z[9] = abb[5] * z[9];
z[4] = z[4] + -z[21];
z[4] = abb[2] * z[4];
z[12] = abb[1] * z[13];
z[13] = -abb[0] * z[18];
z[4] = z[4] + z[12] + z[13] + -z[15];
z[4] = abb[9] * z[4];
z[12] = abb[7] * z[15];
z[13] = z[6] + z[26];
z[13] = abb[3] * z[13];
z[14] = abb[8] * z[13];
z[14] = z[12] + z[14];
z[14] = abb[8] * z[14];
z[18] = (T(-7) / T(2)) * z[0] + -z[6];
z[18] = abb[2] * z[18];
z[18] = z[13] + z[18];
z[16] = z[16] * z[18];
z[13] = -z[13] * z[19];
z[12] = -abb[6] * z[12];
z[4] = abb[18] + z[4] + z[7] + z[9] + z[11] + z[12] + z[13] + z[14] + z[16] + z[20] + z[23];
z[7] = -z[0] + -z[17];
z[7] = abb[6] * z[7];
z[9] = abb[8] * z[1];
z[7] = z[7] + z[9] + z[25];
z[7] = abb[2] * z[7];
z[2] = 8 * abb[16] + 4 * z[0] + z[2];
z[2] = abb[6] * z[2];
z[3] = abb[8] * z[3];
z[2] = z[2] + z[3];
z[2] = abb[5] * z[2];
z[3] = -z[1] + -z[10];
z[3] = abb[6] * z[3];
z[9] = -abb[8] * z[10];
z[1] = abb[7] * z[1];
z[1] = z[1] + z[3] + z[9];
z[1] = abb[0] * z[1];
z[3] = -abb[6] * z[28];
z[0] = -abb[7] * z[0];
z[6] = -abb[8] * z[6];
z[0] = z[0] + z[3] + z[6];
z[0] = z[0] * z[8];
z[3] = -abb[6] + abb[7];
z[3] = z[3] * z[15];
z[0] = z[0] + z[1] + z[2] + z[3] + z[7];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[10] * z[5];
z[0] = abb[11] + z[0] + z[1];
return {z[4], z[0]};
}


template <typename T> std::complex<T> f_4_501_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-2.492896318695937418656728108245394813240962022432540428817509615"),stof<T>("19.916157290739631978126082164353181458272137447340628356075445937")}, std::complex<T>{stof<T>("2.492896318695937418656728108245394813240962022432540428817509615"),stof<T>("-19.916157290739631978126082164353181458272137447340628356075445937")}, std::complex<T>{stof<T>("11.1012998806386290033958640495456309544750518405613978050921518587"),stof<T>("1.9443065867365038185257410266947091257495555323395683322800986151")}, std::complex<T>{stof<T>("-11.1012998806386290033958640495456309544750518405613978050921518587"),stof<T>("-1.9443065867365038185257410266947091257495555323395683322800986151")}, std::complex<T>{stof<T>("20.010534977849799274589457097274201589411414173168750660580038873"),stof<T>("-24.098888188985819982992321492187648911213957781541530602834023013")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[24].real()/kbase.W[24].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_501_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_501_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (v[1] + v[2] + -v[3] + -v[4]) * (-8 + -4 * v[0] + 5 * v[1] + v[2] + -5 * v[3] + -v[4] + 4 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 4 * v[5])) / prod_pow(tend, 2);
c[1] = ((1 + 2 * m1_set::bc<T>[1]) * (T(1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[12] + -abb[13] + -abb[14] + abb[15]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[7], 2);
z[1] = -abb[7] + abb[8] * (T(-1) / T(2));
z[1] = abb[8] * z[1];
z[0] = -abb[9] + (T(3) / T(2)) * z[0] + z[1];
z[1] = abb[12] + -abb[13] + -abb[14] + abb[15];
return abb[4] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_501_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_501_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-40.081833819263252331412118968798477927242020301442431854868328364"),stof<T>("-25.843399207136838196112164879314357608347039975925836535972669997")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,19> abb = {dlog_W4(k,dl), dl[2], dlog_W8(k,dl), dlog_W10(k,dl), dlog_W20(k,dl), dlog_W25(k,dl), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_12_im(k), T{0}, rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[24].real()/k.W[24].real()), f_2_12_re(k), T{0}};
abb[11] = SpDLog_f_4_501_W_20_Im(t, path, abb);
abb[18] = SpDLog_f_4_501_W_20_Re(t, path, abb);

                    
            return f_4_501_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_501_DLogXconstant_part(base_point<T>, kend);
	value += f_4_501_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_501_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_501_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_501_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_501_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_501_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_501_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
