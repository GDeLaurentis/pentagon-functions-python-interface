/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_771.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_771_abbreviated (const std::array<T,69>& abb) {
T z[88];
z[0] = abb[30] * m1_set::bc<T>[0];
z[1] = abb[49] + z[0];
z[2] = abb[32] * m1_set::bc<T>[0];
z[3] = z[1] + -z[2];
z[4] = abb[35] * m1_set::bc<T>[0];
z[4] = abb[50] + z[4];
z[5] = abb[29] * m1_set::bc<T>[0];
z[5] = abb[48] + z[5];
z[6] = z[4] + -z[5];
z[7] = z[3] + z[6];
z[8] = 2 * abb[10];
z[7] = z[7] * z[8];
z[9] = abb[31] + abb[36];
z[10] = m1_set::bc<T>[0] * z[9];
z[11] = abb[33] * m1_set::bc<T>[0];
z[12] = -abb[51] + z[11];
z[13] = abb[34] * m1_set::bc<T>[0];
z[10] = z[10] + -z[12] + -z[13];
z[14] = 2 * abb[1];
z[15] = z[10] * z[14];
z[16] = abb[31] + -abb[36];
z[17] = m1_set::bc<T>[0] * z[16];
z[18] = abb[51] + z[17];
z[19] = -z[2] + z[18];
z[20] = z[5] + z[19];
z[21] = abb[16] * z[20];
z[18] = abb[2] * z[18];
z[7] = z[7] + z[15] + -z[18] + -z[21];
z[15] = 2 * z[5];
z[21] = -2 * z[1] + z[2] + -z[4] + z[15];
z[21] = abb[3] * z[21];
z[22] = abb[36] * m1_set::bc<T>[0];
z[23] = z[6] + -z[11] + z[22];
z[24] = -abb[15] * z[23];
z[25] = abb[31] * m1_set::bc<T>[0];
z[12] = z[12] + -z[25];
z[26] = -z[6] + z[12];
z[27] = -abb[7] * z[26];
z[28] = z[4] + z[19];
z[28] = abb[4] * z[28];
z[12] = abb[6] * z[12];
z[29] = -abb[5] * z[6];
z[12] = z[7] + z[12] + z[21] + z[24] + z[27] + z[28] + z[29];
z[12] = abb[59] * z[12];
z[3] = -z[3] + -2 * z[4] + z[15];
z[3] = abb[5] * z[3];
z[15] = z[5] + z[13];
z[21] = abb[51] + z[25];
z[24] = z[1] + -z[15] + z[21];
z[24] = abb[7] * z[24];
z[21] = z[13] + -z[21];
z[21] = abb[4] * z[21];
z[27] = -z[1] + z[5];
z[27] = abb[3] * z[27];
z[19] = z[1] + z[19];
z[19] = abb[6] * z[19];
z[28] = z[15] + -z[22];
z[29] = -z[1] + z[28];
z[30] = abb[13] * z[29];
z[3] = z[3] + z[7] + z[19] + z[21] + z[24] + z[27] + z[30];
z[3] = abb[62] * z[3];
z[5] = -z[2] + z[5] + z[17];
z[7] = -abb[55] + abb[58];
z[5] = z[5] * z[7];
z[17] = -abb[56] + abb[57];
z[19] = -z[17] * z[20];
z[20] = abb[51] * z[7];
z[5] = z[5] + z[19] + z[20];
z[5] = abb[27] * z[5];
z[0] = -z[0] + z[15];
z[15] = z[0] + -z[22];
z[15] = -z[7] * z[15];
z[19] = abb[54] * z[29];
z[21] = abb[49] * z[7];
z[15] = z[15] + z[19] + z[21];
z[15] = abb[21] * z[15];
z[19] = abb[54] + abb[56];
z[24] = -abb[57] + z[19];
z[23] = -abb[26] * z[23] * z[24];
z[3] = z[3] + z[5] + z[12] + z[15] + z[23];
z[4] = z[4] + z[13];
z[1] = -z[1] + z[4] + -z[11];
z[1] = abb[54] * z[1];
z[5] = z[17] * z[26];
z[0] = z[0] + -z[25];
z[0] = z[0] * z[7];
z[0] = -z[0] + z[1] + z[5] + z[20] + z[21];
z[1] = abb[22] + abb[24];
z[5] = 4 * z[1];
z[0] = z[0] * z[5];
z[4] = -z[2] + z[4] + -z[22];
z[4] = abb[54] * z[4];
z[5] = z[6] * z[17];
z[2] = -z[2] + z[28];
z[2] = z[2] * z[7];
z[2] = -z[2] + z[4] + -z[5];
z[4] = abb[23] + abb[25];
z[5] = 4 * z[4];
z[2] = z[2] * z[5];
z[5] = abb[1] * z[10];
z[5] = z[5] + -z[18];
z[6] = abb[60] + abb[61];
z[6] = 8 * z[6];
z[5] = z[5] * z[6];
z[0] = abb[67] + abb[68] + z[0] + z[2] + 4 * z[3] + z[5];
z[2] = prod_pow(abb[36], 2);
z[3] = 2 * z[2];
z[5] = 2 * abb[36];
z[6] = -abb[31] + z[5];
z[10] = abb[31] * z[6];
z[11] = 2 * abb[66];
z[12] = prod_pow(m1_set::bc<T>[0], 2);
z[13] = z[3] + z[10] + z[11] + (T(2) / T(3)) * z[12];
z[15] = 2 * abb[38];
z[18] = 2 * abb[45];
z[20] = z[15] + z[18];
z[21] = -abb[29] + z[5];
z[22] = -abb[34] + z[21];
z[22] = -abb[33] + 2 * z[22];
z[22] = abb[33] * z[22];
z[23] = 2 * abb[42];
z[22] = -z[20] + z[22] + z[23];
z[25] = abb[29] + -abb[31];
z[26] = z[5] + -z[25];
z[27] = -abb[29] * z[26];
z[28] = 2 * abb[33];
z[29] = z[26] + -z[28];
z[29] = abb[32] * z[29];
z[30] = abb[34] * z[5];
z[31] = 2 * abb[39];
z[32] = 2 * abb[44];
z[27] = z[13] + -z[22] + z[27] + z[29] + -z[30] + -z[31] + z[32];
z[27] = abb[2] * z[27];
z[29] = -abb[35] + z[28];
z[33] = abb[35] * z[29];
z[34] = abb[29] + -abb[36];
z[35] = 2 * z[34];
z[36] = abb[33] + z[35];
z[36] = abb[33] * z[36];
z[37] = 2 * abb[65] + -z[32];
z[38] = z[33] + -z[36] + z[37];
z[39] = 2 * abb[29];
z[40] = abb[34] + -abb[36];
z[41] = z[39] * z[40];
z[42] = 2 * abb[34];
z[43] = -abb[30] + z[42];
z[44] = abb[30] * z[43];
z[45] = 2 * abb[64];
z[44] = z[44] + z[45];
z[46] = -abb[34] + z[5];
z[46] = abb[34] * z[46];
z[47] = z[44] + z[46];
z[48] = abb[32] * z[35];
z[12] = (T(1) / T(3)) * z[12];
z[41] = -z[3] + -z[12] + z[38] + -z[41] + z[47] + z[48];
z[41] = z[8] * z[41];
z[48] = abb[32] + z[25];
z[49] = 2 * z[48];
z[50] = -abb[35] + z[49];
z[50] = abb[35] * z[50];
z[51] = 2 * abb[37];
z[52] = z[50] + -z[51];
z[49] = -abb[30] + z[49];
z[49] = abb[30] * z[49];
z[20] = z[20] + -z[49];
z[53] = 3 * abb[31] + -z[39];
z[53] = abb[29] * z[53];
z[54] = -abb[31] + abb[32];
z[55] = 3 * abb[29] + z[54];
z[55] = abb[32] * z[55];
z[53] = z[20] + -z[52] + -z[53] + z[55];
z[53] = abb[0] * z[53];
z[55] = 2 * abb[41];
z[56] = z[2] + z[55];
z[57] = z[11] + z[12];
z[58] = z[56] + z[57];
z[59] = abb[31] + z[5] + -z[42];
z[59] = abb[31] * z[59];
z[60] = -abb[31] + abb[34];
z[60] = z[28] * z[60];
z[59] = -z[23] + -z[58] + z[59] + z[60];
z[14] = z[14] * z[59];
z[60] = -abb[29] + abb[32];
z[6] = -abb[29] + z[6];
z[6] = -z[6] * z[60];
z[61] = -z[2] + z[6];
z[62] = z[10] + z[57];
z[63] = z[61] + z[62];
z[64] = abb[16] * z[63];
z[14] = -z[14] + z[41] + -2 * z[53] + -z[64];
z[41] = abb[31] * abb[36];
z[64] = z[2] + z[41];
z[65] = -abb[31] + z[40];
z[65] = abb[29] + 2 * z[65];
z[65] = abb[29] * z[65];
z[64] = 2 * z[64] + z[65];
z[32] = z[12] + z[32];
z[65] = 2 * abb[30];
z[66] = -abb[30] + abb[34] + z[48];
z[67] = z[65] * z[66];
z[45] = z[45] + z[67];
z[9] = -abb[34] + 2 * z[9];
z[9] = abb[34] * z[9];
z[67] = abb[33] + -abb[36];
z[67] = -abb[32] + 2 * z[67];
z[67] = abb[32] * z[67];
z[9] = z[9] + z[22] + -z[32] + z[45] + -z[64] + z[67];
z[9] = abb[13] * z[9];
z[22] = 2 * z[16];
z[67] = -abb[32] + z[22];
z[67] = abb[32] * z[67];
z[3] = -z[3] + z[62] + z[67];
z[68] = 2 * z[40];
z[69] = abb[29] + z[68];
z[69] = abb[29] * z[69];
z[46] = -z[3] + z[23] + -z[45] + -z[46] + z[69];
z[46] = abb[6] * z[46];
z[69] = abb[29] + -abb[34];
z[69] = 2 * z[69];
z[70] = abb[33] + z[69];
z[70] = abb[33] * z[70];
z[71] = prod_pow(abb[34], 2);
z[72] = -abb[29] + z[42];
z[72] = abb[29] * z[72];
z[70] = -z[18] + z[70] + z[71] + z[72];
z[71] = 2 * abb[35];
z[29] = z[29] * z[71];
z[73] = abb[29] + -abb[33];
z[73] = abb[32] * z[73];
z[29] = 4 * abb[65] + z[29] + -z[32] + z[44] + -z[70] + 2 * z[73];
z[29] = abb[5] * z[29];
z[32] = 2 * abb[31];
z[44] = -abb[29] + z[32];
z[44] = abb[29] * z[44];
z[39] = abb[32] * z[39];
z[39] = -z[39] + z[44];
z[44] = z[18] + z[51];
z[50] = -z[31] + -z[39] + z[44] + -z[50];
z[50] = abb[9] * z[50];
z[47] = -z[2] + z[15] + z[47] + -z[72];
z[47] = abb[3] * z[47];
z[72] = z[16] * z[42];
z[73] = prod_pow(abb[31], 2);
z[74] = z[2] + z[57] + z[72] + -z[73];
z[74] = abb[4] * z[74];
z[75] = abb[32] * z[48];
z[76] = z[73] + z[75];
z[57] = -z[57] + z[76];
z[32] = -abb[34] + z[32];
z[32] = abb[34] * z[32];
z[32] = z[32] + z[45];
z[45] = -abb[31] + z[42];
z[45] = abb[29] * z[45];
z[45] = -z[32] + z[45];
z[77] = 2 * abb[43];
z[78] = z[31] + z[45] + z[57] + z[77];
z[78] = abb[7] * z[78];
z[79] = abb[29] * z[25];
z[79] = z[75] + z[79];
z[80] = -z[15] + -z[31] + z[49] + -z[79];
z[80] = abb[14] * z[80];
z[81] = -abb[3] + abb[4];
z[81] = z[23] * z[81];
z[82] = abb[5] + -abb[6];
z[83] = -z[31] * z[82];
z[8] = -abb[3] + z[8];
z[84] = -abb[13] + z[8];
z[82] = -z[82] + z[84];
z[82] = z[77] * z[82];
z[85] = abb[17] + -abb[18] + -abb[19] + abb[20];
z[85] = abb[46] * z[85];
z[86] = 2 * abb[21];
z[87] = abb[47] * z[86];
z[9] = z[9] + -z[14] + z[27] + z[29] + z[46] + z[47] + -2 * z[50] + z[74] + z[78] + z[80] + z[81] + z[82] + z[83] + z[85] + z[87];
z[9] = abb[62] * z[9];
z[5] = abb[31] * z[5];
z[5] = 3 * z[2] + -z[5] + z[55] + z[72];
z[27] = abb[32] + z[35];
z[27] = abb[32] * z[27];
z[27] = z[27] + -z[49];
z[22] = -abb[29] + z[22];
z[22] = abb[29] * z[22];
z[22] = z[5] + z[22] + -z[27] + -z[38];
z[22] = abb[54] * z[22];
z[29] = -z[2] + z[41];
z[35] = abb[29] * z[16];
z[35] = -z[29] + z[35];
z[41] = abb[34] * z[16];
z[46] = z[35] + z[41];
z[27] = -z[27] + 2 * z[46];
z[27] = -z[7] * z[27];
z[47] = prod_pow(abb[29], 2);
z[38] = z[38] + z[47] + -z[56];
z[47] = -abb[56] * z[38];
z[38] = z[38] + z[51];
z[49] = abb[57] * z[38];
z[15] = z[15] + -z[23];
z[72] = -abb[54] + z[7];
z[15] = z[15] * z[72];
z[74] = z[31] * z[72];
z[19] = -z[19] * z[51];
z[15] = z[15] + z[19] + z[22] + z[27] + z[47] + z[49] + z[74];
z[15] = abb[23] * z[15];
z[19] = 2 * abb[40];
z[22] = -z[19] + z[37];
z[27] = abb[33] + -abb[35] + z[48];
z[47] = z[27] * z[71];
z[48] = z[22] + z[47];
z[49] = abb[29] * z[42];
z[51] = abb[33] + 2 * z[25];
z[51] = abb[33] * z[51];
z[32] = z[32] + -z[48] + -z[49] + z[51];
z[32] = abb[54] * z[32];
z[49] = abb[29] * abb[31];
z[71] = z[48] + z[49];
z[51] = z[51] + z[57] + -z[71];
z[57] = z[17] * z[51];
z[45] = z[12] + -z[45] + -z[76];
z[45] = z[7] * z[45];
z[76] = z[72] * z[77];
z[11] = z[7] * z[11];
z[32] = -z[11] + z[32] + -z[45] + -z[57] + z[74] + z[76];
z[45] = abb[22] * z[32];
z[32] = abb[24] * z[32];
z[56] = z[36] + z[56] + -z[71] + z[75];
z[56] = -z[17] * z[56];
z[21] = abb[29] * z[21];
z[21] = z[21] + -z[36] + z[48];
z[5] = z[5] + -z[21] + -z[67];
z[5] = abb[54] * z[5];
z[29] = -z[29] + z[41];
z[6] = -z[6] + 2 * z[29];
z[6] = -z[6] * z[7];
z[23] = -z[23] * z[72];
z[5] = z[5] + z[6] + z[23] + z[56];
z[5] = abb[25] * z[5];
z[6] = z[10] + z[12] + z[61];
z[6] = -z[6] * z[7];
z[10] = z[17] * z[63];
z[6] = z[6] + z[10] + -z[11];
z[6] = abb[27] * z[6];
z[10] = abb[33] + z[34];
z[10] = abb[32] * z[10];
z[11] = abb[33] * z[16];
z[23] = abb[35] * z[27];
z[10] = abb[37] + -abb[41] + -abb[65] + z[10] + -z[11] + -z[23] + -z[35];
z[11] = 2 * abb[26];
z[10] = -z[10] * z[11] * z[24];
z[23] = abb[34] + z[34];
z[23] = abb[32] * z[23];
z[24] = abb[30] * z[66];
z[23] = abb[38] + -abb[42] + -abb[64] + z[23] + -z[24] + -z[46];
z[23] = z[23] * z[72] * z[86];
z[7] = -z[7] + z[17];
z[24] = abb[28] * abb[46] * z[7];
z[27] = -abb[59] + -2 * abb[60];
z[27] = abb[18] * abb[46] * z[27];
z[5] = z[5] + z[6] + z[9] + z[10] + z[15] + z[23] + z[24] + z[27] + z[32] + z[45];
z[6] = -abb[34] + 4 * abb[36];
z[6] = abb[34] * z[6];
z[6] = z[6] + -z[44] + z[55];
z[9] = -z[25] + -z[40];
z[9] = -abb[33] + 2 * z[9];
z[9] = abb[33] * z[9];
z[10] = -abb[32] + z[68];
z[10] = abb[32] * z[10];
z[9] = z[6] + z[9] + z[10] + -z[12] + z[37] + z[47] + -z[64];
z[9] = abb[15] * z[9];
z[10] = z[43] * z[65];
z[15] = abb[32] * z[69];
z[10] = 4 * abb[64] + z[10] + -z[12] + z[15] + z[22] + z[33] + -z[70];
z[10] = abb[3] * z[10];
z[12] = z[26] + -z[42];
z[12] = z[12] * z[60];
z[15] = z[28] * z[40];
z[6] = -z[6] + z[12] + z[13] + z[15] + -z[19] + z[77];
z[6] = abb[2] * z[6];
z[3] = -z[3] + -z[21] + z[55];
z[3] = abb[4] * z[3];
z[12] = abb[7] * z[51];
z[13] = z[16] * z[28];
z[13] = z[13] + z[58] + -z[73];
z[13] = abb[6] * z[13];
z[16] = abb[5] * z[38];
z[21] = -z[19] + z[52] + -z[79];
z[21] = abb[14] * z[21];
z[8] = -abb[15] + z[8];
z[8] = z[8] * z[77];
z[22] = abb[19] + abb[20];
z[22] = abb[46] * z[22];
z[11] = abb[47] * z[11];
z[23] = abb[17] * abb[46];
z[3] = z[3] + z[6] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + -z[14] + z[16] + z[21] + z[22] + z[23];
z[3] = 2 * z[3];
z[3] = abb[59] * z[3];
z[2] = z[2] + z[15] + -z[30] + z[62];
z[6] = abb[29] + -z[54];
z[6] = abb[32] * z[6];
z[6] = z[2] + z[6] + z[18] + -z[19] + -z[31] + -z[49];
z[6] = abb[2] * z[6];
z[8] = abb[1] * z[59];
z[6] = -z[6] + -z[8] + z[50] + -z[53];
z[6] = -4 * z[6];
z[6] = abb[60] * z[6];
z[1] = abb[27] + -z[1] + -z[4];
z[1] = z[1] * z[7];
z[4] = abb[7] + abb[16];
z[7] = -abb[3] + abb[10];
z[7] = -abb[5] + -abb[15] + z[4] + 2 * z[7];
z[7] = abb[59] * z[7];
z[4] = -2 * abb[5] + z[4] + z[84];
z[4] = abb[62] * z[4];
z[9] = abb[21] * z[72];
z[10] = -abb[54] + z[17];
z[10] = abb[26] * z[10];
z[1] = z[1] + z[4] + z[7] + z[9] + z[10];
z[1] = abb[63] * z[1];
z[2] = abb[2] * z[2];
z[2] = z[2] + z[8];
z[2] = abb[61] * z[2];
z[4] = z[19] + -z[20] + z[39];
z[7] = abb[59] + abb[60];
z[4] = abb[8] * z[4] * z[7];
z[1] = z[1] + z[2] + z[4];
z[1] = abb[52] + abb[53] + 4 * z[1] + z[3] + 2 * z[5] + z[6];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_771_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("10.402302819131233336271940183191322136570276882477279319319257397"),stof<T>("-12.769388978144903263363784176908690571680015421503048994714708313")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("7.3061296264474758664919516205978361371242677354393911034758419912"),stof<T>("-6.300373455586862118120648214657704964338918906046829282594375544")}, std::complex<T>{stof<T>("-7.859117179210422963589665546873658674845781852900161900017576952"),stof<T>("-20.670851120199880116984210044734263468150192377256738462763025403")}, std::complex<T>{stof<T>("-14.424682837915131424796857938137399889179835508085986581507258664"),stof<T>("-20.670851120199880116984210044734263468150192377256738462763025403")}, std::complex<T>{stof<T>("7.3061296264474758664919516205978361371242677354393911034758419912"),stof<T>("-6.300373455586862118120648214657704964338918906046829282594375544")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_771_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_771_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (4 * m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[59] + abb[60] + abb[61]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[59] + abb[60] + abb[61];
z[1] = abb[34] + -abb[36];
return 8 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_771_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(2)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (-8 + v[0] + -5 * v[1] + v[2] + 5 * v[3] + 4 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (-4 * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[59] + abb[60] + abb[61]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[31] + abb[33];
z[1] = -abb[34] + abb[36];
z[0] = z[0] * z[1];
z[0] = abb[42] + z[0];
z[1] = abb[59] + abb[60] + abb[61];
return 8 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_771_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (-m1_set::bc<T>[0] * (v[1] + v[2] + -1 * v[3] + -1 * v[4]) * (-4 + -2 * v[0] + v[1] + -1 * v[2] + -1 * v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (4 * m1_set::bc<T>[0] * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[60] + abb[61] + abb[62]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[60] + abb[61] + abb[62];
z[1] = abb[33] + -abb[36];
return 8 * abb[12] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_771_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4]) * (8 + 4 * v[0] + -5 * v[1] + -v[2] + 5 * v[3] + v[4] + -4 * v[5])) / prod_pow(tend, 2);
c[1] = (4 * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (abb[60] + abb[61] + abb[62]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[31] + abb[34];
z[1] = -abb[33] + abb[36];
z[0] = z[0] * z[1];
z[0] = abb[41] + z[0];
z[1] = abb[60] + abb[61] + abb[62];
return 8 * abb[12] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_771_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("9.164641211541796764127685634255617945918232437963978350659362605"),stof<T>("-21.575584374546039739091214887501912652151697835318205572939116127")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,69> abb = {dl[0], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W20(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_20(k), f_2_21(k), f_2_23(k), f_2_29_im(k), f_2_2_im(k), f_2_9_im(k), f_2_10_im(k), f_2_12_im(k), T{0}, T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_2_re(k), f_2_9_re(k), f_2_10_re(k), f_2_12_re(k), T{0}, T{0}};
abb[52] = SpDLog_f_4_771_W_17_Im(t, path, abb);
abb[53] = SpDLog_f_4_771_W_20_Im(t, path, abb);
abb[67] = SpDLog_f_4_771_W_17_Re(t, path, abb);
abb[68] = SpDLog_f_4_771_W_20_Re(t, path, abb);

                    
            return f_4_771_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_771_DLogXconstant_part(base_point<T>, kend);
	value += f_4_771_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_771_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_771_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_771_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_771_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_771_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_771_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
