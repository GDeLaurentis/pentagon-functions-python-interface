/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_235.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_235_abbreviated (const std::array<T,29>& abb) {
T z[52];
z[0] = 2 * abb[19] + abb[21];
z[1] = 2 * z[0];
z[2] = -2 * abb[24] + z[1];
z[3] = -abb[20] + abb[23];
z[4] = z[2] + -z[3];
z[4] = abb[2] * z[4];
z[5] = abb[20] + abb[22];
z[6] = -abb[24] + z[1] + -z[5];
z[7] = 2 * abb[3];
z[8] = z[6] * z[7];
z[9] = abb[6] * z[6];
z[10] = 2 * z[9];
z[2] = z[2] + z[3];
z[2] = abb[5] * z[2];
z[3] = 4 * z[0];
z[11] = 3 * abb[24];
z[12] = z[3] + -z[11];
z[13] = -z[5] + z[12];
z[14] = 2 * abb[1];
z[15] = z[13] * z[14];
z[4] = z[2] + -z[4] + z[8] + -z[10] + z[15];
z[8] = abb[27] * z[4];
z[16] = abb[18] * (T(1) / T(2));
z[17] = abb[20] * (T(1) / T(2));
z[18] = -abb[22] + z[16] + -z[17];
z[19] = abb[7] * z[18];
z[20] = abb[9] * z[6];
z[21] = abb[7] * (T(1) / T(2));
z[22] = 2 * abb[8] + z[21];
z[22] = abb[24] * z[22];
z[23] = abb[8] * z[1];
z[19] = -z[19] + z[20] + -z[22] + z[23];
z[20] = abb[0] + abb[3];
z[20] = abb[2] + -2 * abb[10] + (T(3) / T(2)) * z[20];
z[22] = abb[25] * z[20];
z[22] = z[19] + z[22];
z[22] = abb[28] * z[22];
z[8] = -z[8] + z[22];
z[22] = 4 * abb[1];
z[22] = z[13] * z[22];
z[23] = 3 * abb[6];
z[23] = z[6] * z[23];
z[24] = z[22] + -z[23];
z[25] = abb[11] * z[24];
z[26] = abb[12] * z[24];
z[25] = z[25] + -z[26];
z[27] = abb[20] * (T(7) / T(2));
z[28] = 3 * abb[22];
z[29] = 8 * z[0];
z[30] = abb[24] * (T(-9) / T(2)) + -z[16] + -z[27] + -z[28] + z[29];
z[30] = abb[12] * z[30];
z[29] = abb[24] * (T(-15) / T(2)) + z[18] + z[29];
z[31] = -abb[11] * z[29];
z[31] = z[30] + z[31];
z[31] = abb[3] * z[31];
z[32] = abb[24] * (T(3) / T(2));
z[33] = z[1] + -z[32];
z[34] = -z[16] + -z[17] + z[33];
z[34] = abb[3] * z[34];
z[35] = -abb[24] + abb[23] * (T(1) / T(2)) + z[0] + -z[17];
z[35] = abb[5] * z[35];
z[36] = abb[1] * z[13];
z[34] = z[34] + -3 * z[35] + z[36];
z[34] = abb[13] * z[34];
z[31] = -z[25] + z[31] + z[34];
z[31] = abb[13] * z[31];
z[34] = -2 * z[2] + z[9];
z[36] = abb[18] * (T(4) / T(3));
z[37] = 2 * abb[22];
z[38] = 2 * abb[20] + z[0];
z[38] = -abb[24] + -z[36] + z[37] + (T(1) / T(3)) * z[38];
z[38] = abb[3] * z[38];
z[39] = z[1] + -z[17];
z[40] = 6 * abb[24];
z[39] = abb[22] * (T(-20) / T(3)) + abb[23] * (T(-13) / T(2)) + (T(1) / T(3)) * z[39] + z[40];
z[39] = abb[1] * z[39];
z[27] = 11 * z[0] + -z[27];
z[27] = -5 * abb[24] + abb[23] * (T(5) / T(2)) + (T(1) / T(3)) * z[27] + z[36];
z[27] = abb[2] * z[27];
z[36] = -abb[18] + abb[20] + z[37];
z[41] = -z[0] + z[36];
z[42] = abb[0] * z[41];
z[27] = z[27] + 2 * z[34] + z[38] + z[39] + (T(13) / T(3)) * z[42];
z[34] = prod_pow(m1_set::bc<T>[0], 2);
z[27] = z[27] * z[34];
z[38] = -abb[20] + z[3];
z[39] = 6 * abb[23];
z[42] = 9 * abb[24];
z[43] = 11 * abb[22] + -5 * z[38] + z[39] + z[42];
z[43] = abb[1] * z[43];
z[42] = -10 * z[0] + z[5] + z[42];
z[44] = abb[3] * z[42];
z[17] = abb[23] * (T(-3) / T(2)) + z[0] + z[17];
z[45] = -abb[22] + z[17];
z[45] = abb[2] * z[45];
z[35] = 9 * z[35] + z[43] + z[44] + z[45];
z[35] = abb[14] * z[35];
z[43] = z[7] * z[13];
z[44] = abb[11] + -abb[12];
z[45] = z[43] * z[44];
z[25] = z[25] + z[45];
z[45] = 3 * z[2];
z[46] = z[1] + -z[11];
z[5] = z[5] + z[46];
z[7] = z[5] * z[7];
z[47] = z[7] + z[15] + -z[45];
z[47] = abb[13] * z[47];
z[48] = 3 * abb[23];
z[1] = abb[20] + z[1];
z[37] = z[1] + -z[37] + -z[48];
z[37] = abb[13] * z[37];
z[44] = -z[5] * z[44];
z[37] = z[37] + 2 * z[44];
z[37] = abb[2] * z[37];
z[25] = 2 * z[25] + z[35] + z[37] + z[47];
z[25] = abb[14] * z[25];
z[35] = abb[18] + z[1] + -z[11];
z[35] = abb[12] * z[35];
z[37] = z[36] + z[46];
z[44] = abb[11] * z[37];
z[17] = -abb[18] + z[17];
z[17] = abb[13] * z[17];
z[17] = z[17] + -z[35] + z[44];
z[17] = abb[13] * z[17];
z[37] = -abb[12] * z[37];
z[44] = abb[20] + -z[0];
z[44] = -abb[18] + z[28] + 2 * z[44];
z[44] = abb[11] * z[44];
z[37] = z[37] + z[44];
z[37] = abb[11] * z[37];
z[44] = 2 * z[5];
z[46] = abb[15] * z[44];
z[47] = abb[18] + abb[20];
z[49] = -z[12] + z[47];
z[50] = prod_pow(abb[12], 2);
z[51] = -z[49] * z[50];
z[17] = z[17] + z[37] + z[46] + z[51];
z[17] = abb[2] * z[17];
z[37] = -abb[12] * z[44];
z[6] = abb[11] * z[6];
z[6] = -3 * z[6] + z[37];
z[6] = abb[11] * z[6];
z[37] = -abb[14] * z[42];
z[42] = abb[11] + abb[13];
z[44] = abb[12] + -z[42];
z[44] = z[5] * z[44];
z[37] = z[37] + 2 * z[44];
z[37] = abb[14] * z[37];
z[44] = 2 * abb[11];
z[5] = abb[13] * z[5] * z[44];
z[5] = z[5] + z[6] + z[37] + z[46];
z[5] = abb[4] * z[5];
z[6] = -z[15] + z[23];
z[6] = abb[11] * z[6];
z[6] = z[6] + z[26];
z[6] = abb[11] * z[6];
z[18] = z[18] + z[33];
z[23] = -z[18] * z[42];
z[33] = abb[12] * z[18];
z[23] = z[23] + -z[33];
z[23] = abb[13] * z[23];
z[37] = abb[11] * z[41];
z[41] = z[33] + -4 * z[37];
z[41] = abb[11] * z[41];
z[12] = z[12] + -z[36];
z[36] = z[12] * z[50];
z[23] = z[23] + z[36] + z[41];
z[23] = abb[0] * z[23];
z[36] = abb[12] + z[42];
z[41] = abb[13] * (T(1) / T(2));
z[36] = abb[7] * z[36] * z[41];
z[41] = abb[7] + abb[8];
z[42] = abb[11] * z[41];
z[46] = abb[12] * z[21];
z[42] = z[42] + z[46];
z[42] = abb[11] * z[42];
z[34] = z[34] * z[41];
z[51] = abb[8] * z[50];
z[34] = z[34] + z[36] + -z[42] + z[51];
z[36] = 3 * abb[25];
z[42] = z[34] * z[36];
z[51] = abb[28] * z[20];
z[34] = z[34] + z[51];
z[51] = 3 * abb[26];
z[34] = z[34] * z[51];
z[29] = abb[12] * z[29];
z[49] = abb[11] * z[49];
z[29] = z[29] + z[49];
z[29] = abb[11] * z[29];
z[0] = -z[0] + z[47];
z[0] = z[0] * z[50];
z[0] = 4 * z[0] + z[29];
z[0] = abb[3] * z[0];
z[24] = -z[24] + -z[43];
z[29] = 2 * abb[15];
z[24] = z[24] * z[29];
z[29] = -z[15] * z[50];
z[0] = z[0] + z[5] + z[6] + 3 * z[8] + z[17] + z[23] + z[24] + z[25] + z[27] + z[29] + z[31] + z[34] + z[42];
z[5] = z[10] + -z[45];
z[6] = -7 * abb[22] + z[11] + z[38] + -z[39];
z[6] = z[6] * z[14];
z[8] = 4 * abb[22] + z[1] + -z[40] + z[48];
z[8] = abb[2] * z[8];
z[5] = 3 * z[5] + z[6] + z[7] + z[8];
z[5] = abb[14] * z[5];
z[6] = z[12] * z[44];
z[7] = -z[6] + z[30];
z[7] = abb[3] * z[7];
z[3] = abb[20] * (T(-5) / T(2)) + z[3] + z[16] + -z[28] + -z[32];
z[3] = abb[3] * z[3];
z[2] = z[2] + -z[9];
z[2] = 3 * z[2] + z[3] + z[15];
z[2] = abb[13] * z[2];
z[1] = abb[18] + -2 * z[1] + z[11] + z[48];
z[1] = abb[13] * z[1];
z[1] = z[1] + z[6] + -z[35];
z[1] = abb[2] * z[1];
z[3] = abb[13] * z[18];
z[3] = z[3] + -z[33] + 8 * z[37];
z[3] = abb[0] * z[3];
z[6] = -abb[11] * z[22];
z[8] = abb[11] + -abb[14];
z[8] = abb[4] * z[8] * z[13];
z[1] = z[1] + z[2] + z[3] + z[5] + z[6] + z[7] + 4 * z[8] + z[26];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[16] * z[4];
z[3] = abb[17] * z[19];
z[2] = z[2] + z[3];
z[3] = z[41] * z[44];
z[4] = abb[13] * z[21];
z[3] = z[3] + -z[4] + z[46];
z[3] = m1_set::bc<T>[0] * z[3];
z[4] = abb[17] * z[20];
z[3] = z[3] + z[4];
z[4] = z[36] + z[51];
z[3] = z[3] * z[4];
z[1] = z[1] + 3 * z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_235_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("10.136426419815678412697177642306140753505640737452435483270761744"),stof<T>("49.413046312118955875886141470692412640741316153393515836844825111")}, std::complex<T>{stof<T>("62.016449439860470628856596746349350947398471447261370157934019224"),stof<T>("-2.303669873081336653811300436651062096387983285326558359917713737")}, std::complex<T>{stof<T>("16.342666487740412492110687010578136513715979500066190857770281915"),stof<T>("-37.818024478736524932408239111138834861375009645644491087870687376")}, std::complex<T>{stof<T>("31.008224719930235314428298373174675473699235723630685078967009612"),stof<T>("-1.151834936540668326905650218325531048193991642663279179958856869")}, std::complex<T>{stof<T>("-93.331498240273168710821681123063014183722930193382296623239194438"),stof<T>("-33.769556296391953506891077633004121363457436938593684035107370048")}, std::complex<T>{stof<T>("-99.537738308197902790235190491335009943933268955996051997738714609"),stof<T>("53.461514494463527301403302948827126138658888860444322889608142439")}, std::complex<T>{stof<T>("52.186847100527254983696205107582197956518053732299176061001423082"),stof<T>("-14.491655079186334042089413619362760229089887572136552621778598195")}, std::complex<T>{stof<T>("27.92831286295017653844769954488855020241430807694077213756770908"),stof<T>("-40.739096344846029908780604306268048317199884196507126393435065635")}, std::complex<T>{stof<T>("27.92831286295017653844769954488855020241430807694077213756770908"),stof<T>("-40.739096344846029908780604306268048317199884196507126393435065635")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[22].real()/kbase.W[22].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[126].real()/kbase.W[126].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_235_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_235_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("2.013076267711772938941469786052511330108362327882158921489187334"),stof<T>("-32.89944744645114588876204048199081865736833323081932889713094923")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dlog_W3(k,dl), dlog_W8(k,dl), dl[3], dlog_W13(k,dl), dlog_W23(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W127(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[22].real()/k.W[22].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[126].real()/k.W[126].real()), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_235_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_235_DLogXconstant_part(base_point<T>, kend);
	value += f_4_235_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_235_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_235_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_235_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_235_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_235_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_235_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
