/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_225.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_225_abbreviated (const std::array<T,66>& abb) {
T z[152];
z[0] = abb[48] * (T(1) / T(2));
z[1] = abb[46] * (T(1) / T(2));
z[2] = z[0] + z[1];
z[3] = abb[42] * (T(1) / T(2));
z[4] = abb[47] * (T(1) / T(2));
z[5] = z[3] + -z[4];
z[6] = z[2] + -z[5];
z[7] = 3 * abb[44];
z[8] = abb[45] + abb[53];
z[9] = z[6] + z[7] + -z[8];
z[10] = abb[16] * z[9];
z[11] = -abb[56] + -abb[57] + abb[58];
z[12] = 3 * abb[55] + -z[11];
z[13] = (T(1) / T(4)) * z[12];
z[14] = abb[20] * z[13];
z[10] = z[10] + z[14];
z[14] = 2 * abb[53];
z[15] = abb[46] + abb[48];
z[16] = z[14] + -z[15];
z[17] = -abb[50] + z[16];
z[18] = 2 * abb[49];
z[19] = -abb[54] + z[18];
z[20] = -z[7] + z[17] + -z[19];
z[20] = abb[10] * z[20];
z[21] = 2 * abb[45];
z[22] = -abb[53] + z[21];
z[6] = z[6] + z[22];
z[23] = 3 * abb[51];
z[24] = 3 * abb[43] + -z[23];
z[25] = z[6] + z[24];
z[26] = abb[14] * z[25];
z[27] = abb[19] * z[13];
z[26] = -z[26] + z[27];
z[27] = 4 * abb[43];
z[28] = 2 * abb[51];
z[29] = -abb[52] + z[27] + -z[28];
z[22] = z[15] + z[22];
z[30] = z[22] + z[29];
z[31] = abb[1] * z[30];
z[32] = 2 * abb[46];
z[33] = abb[48] + z[32];
z[34] = -abb[54] + z[33];
z[35] = 4 * abb[44];
z[36] = -z[8] + z[34] + z[35];
z[37] = abb[2] * z[36];
z[38] = 2 * abb[44];
z[39] = -abb[46] + abb[54];
z[40] = -abb[43] + abb[52];
z[41] = -abb[53] + z[38] + -z[39] + z[40];
z[41] = abb[0] * z[41];
z[42] = z[15] + z[21];
z[43] = 2 * abb[43];
z[44] = -abb[44] + -z[23] + z[42] + z[43];
z[44] = abb[5] * z[44];
z[45] = -abb[59] + abb[60] + abb[62];
z[46] = -3 * abb[61] + z[45];
z[47] = abb[25] * z[46];
z[48] = abb[23] * z[46];
z[49] = z[47] + -z[48];
z[50] = -abb[42] + abb[45];
z[51] = -abb[52] + z[43] + z[50];
z[51] = abb[9] * z[51];
z[52] = abb[45] * (T(1) / T(2));
z[53] = z[1] + z[52];
z[54] = abb[54] * (T(1) / T(2));
z[55] = abb[47] + z[53] + z[54];
z[56] = abb[52] * (T(1) / T(4));
z[55] = -abb[49] + (T(1) / T(2)) * z[55] + -z[56];
z[55] = abb[8] * z[55];
z[57] = -abb[42] + z[38];
z[58] = abb[43] + -abb[51];
z[59] = abb[45] + z[58];
z[60] = -z[57] + z[59];
z[60] = abb[6] * z[60];
z[41] = z[10] + z[20] + z[26] + z[31] + -z[37] + z[41] + z[44] + (T(-1) / T(4)) * z[49] + z[51] + z[55] + z[60];
z[41] = abb[28] * z[41];
z[44] = abb[53] + z[52];
z[49] = -abb[42] + z[44];
z[55] = abb[52] * (T(1) / T(2));
z[60] = abb[54] * (T(3) / T(2)) + -z[55];
z[61] = abb[46] * (T(3) / T(2));
z[62] = z[38] + -z[49] + -z[60] + z[61];
z[62] = abb[0] * z[62];
z[63] = -abb[42] + abb[47];
z[16] = z[16] + -z[63];
z[64] = 6 * abb[44] + -z[16] + -z[21];
z[65] = abb[16] * z[64];
z[66] = (T(1) / T(2)) * z[12];
z[67] = abb[20] * z[66];
z[65] = z[65] + z[67];
z[67] = 2 * abb[12];
z[39] = -abb[44] + z[39];
z[67] = z[39] * z[67];
z[68] = 4 * z[37] + -z[65] + z[67];
z[69] = z[54] + z[55];
z[70] = abb[47] + z[69];
z[71] = abb[48] + z[61];
z[72] = z[44] + -z[70] + z[71];
z[73] = abb[8] * z[72];
z[74] = abb[15] * z[72];
z[73] = z[73] + -z[74];
z[75] = abb[21] * z[46];
z[76] = abb[24] * z[46];
z[77] = z[75] + z[76];
z[78] = -z[47] + z[77];
z[79] = z[48] + z[78];
z[80] = -z[15] + z[63];
z[81] = abb[5] * z[80];
z[82] = abb[18] * z[66];
z[81] = z[81] + -z[82];
z[62] = -z[62] + z[68] + z[73] + (T(-1) / T(2)) * z[79] + z[81];
z[62] = abb[33] * z[62];
z[83] = 6 * abb[51];
z[84] = 4 * abb[45];
z[16] = -6 * abb[43] + z[16] + z[83] + -z[84];
z[85] = abb[14] * z[16];
z[86] = abb[19] * z[66];
z[85] = z[85] + z[86];
z[86] = (T(1) / T(2)) * z[46];
z[87] = abb[22] * z[86];
z[88] = z[85] + z[87];
z[89] = -abb[51] + z[40] + z[80];
z[90] = 2 * abb[13];
z[90] = z[89] * z[90];
z[91] = 4 * z[31] + z[88] + z[90];
z[92] = abb[52] * (T(3) / T(2)) + -z[54];
z[93] = -z[43] + z[92];
z[49] = z[1] + z[49];
z[94] = -z[28] + z[49] + z[93];
z[94] = abb[0] * z[94];
z[30] = 2 * z[30];
z[30] = abb[5] * z[30];
z[95] = abb[23] * z[86];
z[30] = z[30] + z[73] + z[91] + z[94] + z[95];
z[30] = abb[30] * z[30];
z[73] = -abb[42] + z[18];
z[94] = -z[33] + z[73];
z[96] = 2 * abb[50];
z[97] = z[43] + z[96];
z[98] = z[94] + z[97];
z[99] = 3 * abb[53];
z[100] = z[21] + z[98] + -z[99];
z[100] = abb[15] * z[100];
z[101] = -z[81] + z[100];
z[102] = abb[22] * z[46];
z[77] = z[48] + z[77] + z[102];
z[54] = -z[54] + z[55];
z[55] = z[54] + -z[97];
z[61] = -z[52] + z[61];
z[103] = abb[42] + abb[48];
z[104] = abb[53] + z[61] + z[103];
z[105] = abb[47] + z[55] + z[104];
z[105] = abb[8] * z[105];
z[106] = -abb[53] + z[43];
z[107] = z[73] + z[106];
z[108] = z[33] + -z[107];
z[108] = abb[0] * z[108];
z[77] = (T(1) / T(2)) * z[77] + z[101] + z[105] + z[108];
z[77] = abb[31] * z[77];
z[36] = abb[33] * z[36];
z[105] = -abb[30] + abb[31];
z[108] = -z[80] * z[105];
z[109] = 2 * z[36] + z[108];
z[109] = abb[6] * z[109];
z[41] = -z[30] + z[41] + z[62] + z[77] + z[109];
z[41] = abb[28] * z[41];
z[77] = -abb[42] + abb[53] * (T(13) / T(3));
z[109] = abb[48] * (T(1) / T(4));
z[77] = (T(1) / T(4)) * z[77] + -z[109];
z[110] = abb[43] + -abb[44];
z[111] = abb[52] * (T(2) / T(3));
z[112] = abb[51] * (T(11) / T(3)) + -z[111];
z[113] = abb[46] * (T(2) / T(3));
z[114] = abb[49] * (T(13) / T(6));
z[115] = abb[54] * (T(2) / T(3));
z[110] = z[77] + (T(3) / T(2)) * z[110] + -z[112] + -z[113] + z[114] + -z[115];
z[110] = abb[0] * z[110];
z[116] = z[28] + -z[43];
z[117] = z[14] + -z[63];
z[118] = -abb[46] + -z[84] + z[117];
z[119] = abb[48] * (T(1) / T(3));
z[118] = z[116] + (T(1) / T(3)) * z[118] + -z[119];
z[118] = abb[14] * z[118];
z[120] = 3 * abb[47];
z[77] = abb[45] * (T(-5) / T(6)) + abb[50] * (T(13) / T(6)) + -z[1] + z[77] + -z[120];
z[77] = abb[8] * z[77];
z[121] = abb[53] + abb[42] * (T(13) / T(3));
z[121] = -abb[46] + abb[45] * (T(-5) / T(3)) + (T(1) / T(2)) * z[121];
z[109] = -abb[44] + abb[54] * (T(4) / T(3)) + -z[109] + -z[114] + (T(1) / T(2)) * z[121];
z[109] = abb[2] * z[109];
z[114] = 3 * abb[45];
z[121] = -20 * abb[46] + 11 * z[63];
z[112] = abb[48] * (T(-11) / T(3)) + abb[43] * (T(7) / T(3)) + -z[112] + z[114] + (T(1) / T(3)) * z[121];
z[112] = abb[13] * z[112];
z[113] = abb[44] * (T(-7) / T(3)) + z[113] + z[114] + -z[115];
z[113] = abb[12] * z[113];
z[121] = z[14] + z[63];
z[122] = -abb[45] + 5 * abb[51] + abb[44] * (T(5) / T(2)) + z[2] + -z[121];
z[111] = abb[43] * (T(1) / T(2)) + -z[111] + (T(1) / T(3)) * z[122];
z[111] = abb[5] * z[111];
z[117] = z[21] + z[117];
z[122] = abb[46] + -z[117];
z[119] = z[38] + z[119] + (T(1) / T(3)) * z[122];
z[119] = abb[16] * z[119];
z[122] = abb[43] + abb[48];
z[115] = -abb[42] + abb[44] * (T(-1) / T(2)) + abb[47] * (T(-1) / T(3)) + abb[45] * (T(-1) / T(6)) + abb[53] * (T(2) / T(3)) + abb[51] * (T(4) / T(3)) + -z[32] + z[115] + (T(-5) / T(6)) * z[122];
z[115] = abb[6] * z[115];
z[122] = abb[21] + abb[24] + -abb[25];
z[122] = abb[23] + abb[22] * (T(1) / T(2)) + (T(1) / T(2)) * z[122];
z[45] = -abb[61] + (T(1) / T(3)) * z[45];
z[45] = z[45] * z[122];
z[122] = abb[18] + abb[19] + abb[20];
z[123] = abb[17] * (T(1) / T(2));
z[122] = (T(-1) / T(2)) * z[122] + -z[123];
z[11] = -abb[55] + (T(1) / T(3)) * z[11];
z[11] = z[11] * z[122];
z[122] = 13 * abb[42] + abb[53] * (T(-29) / T(3));
z[122] = abb[45] + abb[50] * (T(-13) / T(3)) + abb[46] * (T(47) / T(3)) + (T(1) / T(2)) * z[122];
z[122] = -7 * abb[51] + abb[52] * (T(-4) / T(3)) + abb[43] * (T(29) / T(3)) + abb[48] * (T(55) / T(12)) + (T(1) / T(2)) * z[122];
z[122] = abb[1] * z[122];
z[11] = z[11] + z[45] + z[77] + z[109] + z[110] + z[111] + z[112] + z[113] + z[115] + z[118] + z[119] + z[122];
z[11] = prod_pow(m1_set::bc<T>[0], 2) * z[11];
z[45] = 2 * abb[48];
z[77] = 5 * abb[46] + z[45];
z[109] = 3 * abb[42];
z[110] = -abb[52] + z[28] + -z[77] + z[84] + z[96] + -z[109];
z[110] = abb[1] * z[110];
z[111] = abb[54] * (T(1) / T(4));
z[112] = z[56] + z[111];
z[113] = abb[53] + z[4];
z[115] = -abb[50] + z[113];
z[118] = abb[46] * (T(1) / T(4));
z[119] = abb[43] + abb[49];
z[122] = abb[45] * (T(-5) / T(4)) + z[3] + z[112] + z[115] + z[118] + -z[119];
z[122] = abb[15] * z[122];
z[77] = -z[29] + -2 * z[63] + z[77] + -z[114];
z[77] = abb[13] * z[77];
z[124] = abb[45] * (T(3) / T(4));
z[125] = abb[46] * (T(-9) / T(4)) + -z[103] + -z[112] + z[113] + z[124];
z[125] = abb[7] * z[125];
z[126] = -abb[49] + abb[45] * (T(-1) / T(4)) + z[112];
z[127] = -abb[47] + abb[50];
z[128] = -abb[42] + z[28];
z[129] = -abb[43] + abb[46] * (T(-3) / T(4)) + -z[0] + -z[126] + z[127] + z[128];
z[129] = abb[0] * z[129];
z[130] = -z[3] + z[52] + -z[127];
z[130] = abb[8] * z[130];
z[6] = z[6] + z[29];
z[6] = abb[5] * z[6];
z[29] = abb[18] * z[12];
z[131] = (T(1) / T(4)) * z[29];
z[6] = z[6] + -z[51] + (T(-1) / T(4)) * z[76] + z[77] + z[110] + z[122] + z[125] + z[129] + z[130] + -z[131];
z[110] = prod_pow(abb[30], 2);
z[6] = z[6] * z[110];
z[122] = z[44] + z[69];
z[129] = 3 * abb[46];
z[130] = abb[48] * (T(5) / T(2)) + z[5] + z[35] + z[58] + -z[122] + z[129];
z[130] = abb[6] * z[130];
z[132] = 2 * z[31];
z[133] = (T(1) / T(4)) * z[102];
z[26] = z[26] + z[132] + z[133];
z[134] = abb[17] * z[12];
z[134] = z[26] + z[90] + (T(3) / T(4)) * z[134];
z[135] = 2 * z[37];
z[10] = -z[10] + z[135];
z[136] = z[74] + (T(1) / T(4)) * z[78];
z[137] = abb[43] + abb[44];
z[138] = z[28] + z[137];
z[139] = abb[48] + z[1] + z[122] + -z[138];
z[139] = abb[0] * z[139];
z[140] = abb[46] * (T(5) / T(2));
z[141] = z[45] + -z[69] + z[140];
z[142] = 2 * abb[47];
z[44] = abb[42] + z[44] + z[141] + -z[142];
z[143] = abb[8] * z[44];
z[144] = abb[45] * (T(3) / T(2));
z[145] = -abb[51] + z[144];
z[92] = abb[44] + -z[27] + z[92] + -z[145];
z[146] = z[0] + z[4];
z[147] = -abb[53] + z[3] + -z[92] + z[146];
z[147] = abb[5] * z[147];
z[130] = z[10] + z[130] + z[131] + z[134] + -z[136] + z[139] + z[143] + z[147];
z[130] = abb[28] * z[130];
z[10] = -z[10] + (T(3) / T(4)) * z[29] + -z[67];
z[29] = abb[43] + -z[35] + z[60] + z[145];
z[131] = z[0] + z[5];
z[139] = -abb[46] + abb[53] + z[29] + z[131];
z[139] = abb[6] * z[139];
z[143] = abb[42] * (T(3) / T(2));
z[145] = abb[48] * (T(3) / T(2));
z[147] = z[143] + z[145];
z[92] = -abb[46] + z[92] + z[113] + -z[147];
z[92] = abb[5] * z[92];
z[40] = -abb[44] + -z[34] + -z[40] + z[128];
z[40] = abb[0] * z[40];
z[78] = z[48] + (T(1) / T(2)) * z[78];
z[113] = abb[8] * z[80];
z[40] = -z[10] + z[40] + (T(-1) / T(2)) * z[78] + z[92] + z[113] + -z[134] + -z[139];
z[78] = abb[32] * z[40];
z[92] = z[54] + -z[144];
z[103] = -z[1] + z[38] + z[92] + -z[103] + z[116];
z[113] = abb[5] * z[103];
z[31] = z[31] + z[37];
z[37] = 3 * abb[48];
z[128] = 6 * abb[46] + z[37];
z[134] = 2 * abb[42];
z[139] = -z[8] + z[128] + z[134] + -z[142];
z[148] = abb[8] * z[139];
z[149] = abb[42] + -abb[53];
z[38] = z[38] + z[149];
z[116] = z[38] + z[52] + -z[116] + z[141];
z[116] = abb[0] * z[116];
z[141] = abb[42] + z[129];
z[138] = -abb[45] + z[37] + -z[138] + 2 * z[141];
z[141] = 2 * abb[6];
z[138] = z[138] * z[141];
z[77] = -z[31] + -z[77] + -z[82] + z[113] + z[116] + z[138] + z[148];
z[77] = abb[29] * z[77];
z[44] = abb[0] * z[44];
z[113] = z[45] + -z[144];
z[116] = -abb[47] + abb[46] * (T(9) / T(2)) + z[69] + z[113] + 2 * z[149];
z[138] = abb[7] * z[116];
z[150] = abb[24] * z[86];
z[151] = z[138] + z[150];
z[44] = -z[44] + z[74] + -z[81] + -z[148] + z[151];
z[44] = -z[44] * z[105];
z[81] = z[105] * z[139];
z[36] = -z[36] + z[81];
z[36] = z[36] * z[141];
z[36] = z[36] + z[44] + -z[62] + z[77] + z[78] + z[130];
z[36] = abb[29] * z[36];
z[44] = -z[70] + z[97];
z[1] = z[1] + z[14];
z[62] = abb[45] * (T(5) / T(2));
z[77] = z[1] + -z[44] + -z[62] + -z[73];
z[77] = abb[15] * z[77];
z[81] = z[71] + z[134];
z[93] = z[18] + z[52] + -z[81] + z[93];
z[93] = abb[0] * z[93];
z[17] = 5 * abb[43] + -abb[52] + -z[17] + -z[23] + z[114];
z[130] = 2 * abb[1];
z[17] = z[17] * z[130];
z[16] = -abb[5] * z[16];
z[139] = 2 * z[51];
z[86] = abb[21] * z[86];
z[86] = z[86] + z[150];
z[148] = -abb[52] + z[43];
z[50] = z[50] + z[148];
z[50] = abb[8] * z[50];
z[16] = z[16] + z[17] + z[50] + z[77] + -z[82] + z[85] + -z[86] + z[93] + z[139];
z[16] = abb[35] * z[16];
z[17] = z[23] + -z[146];
z[23] = abb[53] + z[69];
z[27] = -abb[44] + -abb[46] + z[17] + z[23] + -z[27] + -z[62] + z[143];
z[27] = abb[5] * z[27];
z[50] = z[81] + -z[122] + z[137];
z[50] = abb[0] * z[50];
z[49] = z[49] + -z[69];
z[62] = -abb[8] * z[49];
z[32] = -abb[53] + z[32];
z[29] = z[29] + -z[32] + -z[131];
z[29] = abb[6] * z[29];
z[10] = z[10] + -z[26] + z[27] + z[29] + z[50] + z[62] + z[136];
z[10] = abb[28] * z[10];
z[26] = abb[6] * z[103];
z[27] = -z[53] + -z[57] + z[69] + -z[106];
z[27] = abb[0] * z[27];
z[29] = -abb[54] + z[35];
z[35] = -abb[46] + -z[29] + z[114];
z[35] = abb[12] * z[35];
z[50] = abb[8] * z[22];
z[42] = -z[42] + z[137];
z[53] = 2 * abb[5];
z[42] = z[42] * z[53];
z[57] = abb[17] * z[66];
z[26] = z[26] + z[27] + z[31] + z[35] + z[42] + -z[50] + -z[57];
z[26] = abb[32] * z[26];
z[27] = abb[0] * z[49];
z[22] = z[22] * z[53];
z[22] = z[22] + z[27] + z[50] + -z[74] + z[87];
z[27] = -abb[31] + abb[33];
z[22] = z[22] * z[27];
z[1] = z[1] + -z[144];
z[31] = z[1] + -z[70];
z[42] = abb[4] * z[31];
z[27] = z[27] * z[42];
z[49] = abb[33] * z[80];
z[49] = z[49] + z[108];
z[49] = abb[6] * z[49];
z[27] = z[27] + -z[49];
z[49] = -z[12] * z[105];
z[50] = abb[33] * z[12];
z[50] = z[49] + z[50];
z[62] = abb[28] * z[66];
z[62] = z[50] + z[62];
z[62] = z[62] * z[123];
z[10] = z[10] + z[22] + z[26] + z[27] + z[30] + z[62];
z[10] = abb[32] * z[10];
z[22] = 4 * abb[53] + abb[46] * (T(7) / T(2)) + -z[18] + -z[63] + -z[69] + -z[97] + z[113];
z[22] = abb[15] * z[22];
z[26] = -abb[50] + z[7];
z[30] = abb[42] + -z[14] + z[26] + z[43] + -z[60] + z[61];
z[30] = abb[0] * z[30];
z[60] = 2 * abb[54];
z[61] = -12 * abb[44] + z[60] + z[73] + z[84] + z[99] + -z[128];
z[61] = abb[2] * z[61];
z[26] = -4 * abb[49] + -z[26] + -z[32] + -z[45] + z[60];
z[26] = abb[10] * z[26];
z[47] = -z[47] + -z[102];
z[62] = -abb[53] + -z[15] + z[96];
z[19] = -z[19] + z[62];
z[70] = abb[8] * z[19];
z[64] = -abb[6] * z[64];
z[26] = z[22] + z[26] + z[30] + (T(1) / T(2)) * z[47] + -z[57] + z[61] + z[64] + z[65] + z[70];
z[26] = abb[34] * z[26];
z[30] = 8 * abb[44];
z[47] = -abb[48] + -z[30] + z[60] + z[117] + -z[129];
z[47] = abb[6] * z[47];
z[37] = -z[37] + -z[84] + z[121] + -z[129];
z[61] = abb[5] * z[37];
z[64] = abb[8] * z[31];
z[70] = 2 * abb[0];
z[39] = z[39] * z[70];
z[57] = z[57] + z[82];
z[74] = z[79] + z[102];
z[74] = (T(1) / T(2)) * z[74];
z[39] = -z[39] + z[42] + z[47] + z[57] + -z[61] + -z[64] + -z[68] + z[74];
z[47] = abb[64] * z[39];
z[61] = abb[43] + -abb[49];
z[64] = (T(1) / T(2)) * z[149];
z[2] = -z[2] + -z[52] + z[54] + -z[61] + -z[64] + -z[127];
z[2] = abb[8] * z[2];
z[52] = abb[45] + -abb[54] + z[73];
z[52] = abb[2] * z[52];
z[51] = z[51] + z[52] + -z[86] + -z[95];
z[62] = -z[62] + z[148];
z[62] = abb[1] * z[62];
z[19] = abb[10] * z[19];
z[19] = -z[19] + z[62] + z[87];
z[62] = abb[50] + -z[0] + -z[64] + -z[119];
z[62] = abb[0] * z[62];
z[64] = abb[50] + z[119];
z[68] = -abb[42] + -z[99];
z[68] = abb[45] + -abb[46] + -z[0] + z[64] + (T(1) / T(2)) * z[68];
z[68] = abb[15] * z[68];
z[79] = abb[5] * z[72];
z[2] = z[2] + z[19] + -z[51] + z[62] + 3 * z[68] + z[79];
z[2] = abb[38] * z[2];
z[61] = -abb[48] + z[61] + z[63] + -z[92] + -z[140];
z[61] = abb[8] * z[61];
z[62] = -z[96] + z[142];
z[68] = -z[33] + z[62];
z[79] = z[68] + z[107];
z[80] = abb[0] * z[79];
z[81] = -abb[6] * z[116];
z[75] = z[48] + z[75];
z[19] = -z[19] + z[22] + z[61] + (T(-1) / T(2)) * z[75] + z[80] + z[81] + z[138];
z[19] = abb[36] * z[19];
z[22] = 8 * abb[43];
z[61] = 2 * abb[52];
z[37] = -4 * abb[51] + z[22] + -z[37] + -z[61];
z[37] = abb[5] * z[37];
z[80] = -z[14] + z[109];
z[21] = 11 * abb[46] + 5 * abb[48] + -z[21] + z[80] + -z[120];
z[21] = abb[6] * z[21];
z[81] = abb[8] * z[116];
z[48] = z[48] + z[76];
z[70] = z[70] * z[89];
z[21] = z[21] + -z[37] + (T(-1) / T(2)) * z[48] + -z[70] + z[81] + -z[91] + -z[138];
z[37] = -z[21] + z[57];
z[37] = abb[63] * z[37];
z[45] = 4 * abb[46] + z[45];
z[30] = abb[54] + z[14] + -z[30] + -z[45] + z[73] + z[114];
z[30] = abb[2] * z[30];
z[0] = -abb[53] + -z[0] + -z[118] + z[126] + z[137];
z[0] = abb[0] * z[0];
z[14] = -z[14] + z[119];
z[48] = abb[48] + -abb[50] + abb[46] * (T(7) / T(4)) + z[5] + -z[14] + -z[112] + -z[124];
z[48] = abb[15] * z[48];
z[57] = -abb[53] + -z[3] + z[64] + -z[71];
z[57] = abb[8] * z[57];
z[0] = z[0] + -z[20] + z[30] + -z[35] + z[48] + z[57] + -z[133];
z[30] = prod_pow(abb[33], 2);
z[0] = z[0] * z[30];
z[1] = abb[47] + z[1];
z[35] = -z[1] + -z[55];
z[35] = abb[8] * z[35];
z[31] = -abb[5] * z[31];
z[31] = z[31] + z[35] + z[42] + z[51] + z[77];
z[31] = abb[37] * z[31];
z[3] = -z[3] + z[7] + z[17] + -z[23] + -z[144];
z[3] = abb[18] * z[3];
z[7] = abb[19] * z[25];
z[9] = abb[20] * z[9];
z[17] = abb[5] + abb[6] + -abb[16];
z[13] = z[13] * z[17];
z[17] = abb[26] * z[46];
z[23] = abb[14] * z[12];
z[17] = z[17] + -z[23];
z[23] = abb[8] * z[66];
z[12] = abb[27] * z[12];
z[3] = -z[3] + z[7] + -z[9] + z[12] + z[13] + (T(1) / T(4)) * z[17] + z[23];
z[7] = z[4] + z[24] + z[32] + -z[69] + z[144] + z[147];
z[9] = -abb[17] * z[7];
z[9] = -z[3] + z[9];
z[9] = abb[65] * z[9];
z[5] = -z[5] + z[8] + -z[29] + -z[140] + -z[145];
z[5] = z[5] * z[30];
z[8] = -abb[38] * z[72];
z[5] = z[5] + z[8];
z[5] = abb[6] * z[5];
z[8] = abb[53] + -z[45] + z[119] + -4 * z[127] + -z[134];
z[8] = abb[36] * z[8];
z[12] = abb[42] + z[14] + -z[68];
z[13] = prod_pow(abb[31], 2);
z[14] = -z[13] + z[110];
z[14] = z[12] * z[14];
z[17] = abb[34] + -abb[38];
z[23] = z[17] * z[79];
z[8] = z[8] + z[14] + z[23];
z[8] = abb[11] * z[8];
z[1] = -z[1] + z[18] + z[54] + z[96];
z[14] = abb[35] + abb[36] + abb[37] + z[17];
z[14] = z[1] * z[14];
z[1] = -abb[31] * z[1];
z[17] = -z[56] + z[111];
z[23] = z[118] + -z[124];
z[24] = -abb[49] + z[17] + z[23] + z[115];
z[25] = -abb[28] * z[24];
z[1] = z[1] + z[25];
z[1] = abb[28] * z[1];
z[24] = -z[13] * z[24];
z[1] = z[1] + z[14] + z[24];
z[1] = abb[3] * z[1];
z[4] = abb[53] + -z[4] + z[23];
z[14] = abb[43] + -z[4] + z[17];
z[14] = abb[8] * z[14];
z[17] = -abb[0] * z[127];
z[14] = z[14] + z[17] + (T(-1) / T(4)) * z[75] + -z[125];
z[14] = z[13] * z[14];
z[17] = -z[30] * z[66];
z[23] = -abb[28] * z[49];
z[17] = z[17] + z[23];
z[17] = z[17] * z[123];
z[4] = z[4] + -z[112];
z[13] = z[13] + -z[30];
z[4] = abb[4] * z[4] * z[13];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + z[6] + z[8] + z[9] + z[10] + z[11] + z[14] + z[16] + z[17] + z[19] + z[26] + z[31] + z[36] + z[37] + z[41] + z[47];
z[1] = z[33] + -z[43];
z[2] = -z[1] + z[18] + -z[28] + -z[38];
z[2] = abb[0] * z[2];
z[4] = 2 * z[20];
z[5] = -abb[53] + z[98] + -z[142];
z[6] = abb[8] * z[5];
z[8] = -z[34] + -z[38] + -z[58];
z[8] = z[8] * z[141];
z[9] = abb[44] + abb[51] + -abb[52] + z[106];
z[9] = z[9] * z[53];
z[2] = z[2] + -z[4] + z[6] + z[8] + z[9] + -z[67] + z[90] + -z[100] + z[132] + -z[135] + -z[139];
z[2] = abb[28] * z[2];
z[6] = z[101] + z[151];
z[8] = abb[53] + z[61] + z[62] + -z[83] + -z[94];
z[8] = abb[0] * z[8];
z[9] = 7 * abb[46] + 4 * abb[48] + -abb[52] + z[22] + z[80] + -z[83] + -z[96];
z[9] = z[9] * z[130];
z[10] = -z[69] + z[96] + z[104] + -z[120];
z[10] = abb[8] * z[10];
z[11] = -z[33] + z[59] + z[63];
z[11] = abb[13] * z[11];
z[8] = z[6] + z[8] + z[9] + z[10] + 6 * z[11] + z[88] + z[95] + z[139];
z[8] = abb[30] * z[8];
z[1] = abb[42] + abb[53] + z[1] + z[18] + -z[60];
z[1] = abb[0] * z[1];
z[9] = -z[18] + -z[44] + z[104];
z[9] = abb[8] * z[9];
z[10] = -abb[44] + abb[45];
z[10] = abb[12] * z[10];
z[1] = z[1] + z[4] + z[9] + 6 * z[10] + -2 * z[52] + z[65] + z[74] + z[101];
z[1] = abb[33] * z[1];
z[4] = abb[29] * z[40];
z[5] = abb[0] * z[5];
z[9] = -abb[45] + -z[15] + -z[62] + -z[149];
z[9] = abb[8] * z[9];
z[5] = z[5] + -z[6] + z[9] + -z[87];
z[5] = abb[31] * z[5];
z[6] = abb[11] * z[12] * z[105];
z[9] = z[50] * z[123];
z[1] = z[1] + z[2] + z[4] + z[5] + 2 * z[6] + z[8] + z[9] + z[27] + z[78];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[40] * z[39];
z[4] = -z[21] + z[82];
z[4] = abb[39] * z[4];
z[3] = -abb[41] * z[3];
z[5] = -abb[41] * z[7];
z[6] = abb[39] * z[66];
z[5] = z[5] + z[6];
z[5] = abb[17] * z[5];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_225_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-18.532805985296568558258419582201622705850006466615638653357974654"),stof<T>("-28.062415966599068695583676699101002397539170265371126850982119055")}, std::complex<T>{stof<T>("62.766541908557314262262133197854539373388644717800048024808684762"),stof<T>("-83.921382720903470925214585995805339594569080603922359932812406607")}, std::complex<T>{stof<T>("16.708328833573151512685833899843273017649098211553432537178164178"),stof<T>("7.856199269870407737718601172087699856286205342283419949481911598")}, std::complex<T>{stof<T>("-15.174039060243312554155316783541819856332686735974540164195840943"),stof<T>("10.806274255576621535427432944306640759825980594681943129452466575")}, std::complex<T>{stof<T>("-9.000018562564364982881431596517489730825137845320556756949342491"),stof<T>("-45.990552720223322530997729206468540926450965637604533879004499053")}, std::complex<T>{stof<T>("-8.0982949699999671219704031004996070860402146519571067835376150271"),stof<T>("-5.9368840902615071308155325118259758386536884123564193828166509693")}, std::complex<T>{stof<T>("-18.532805985296568558258419582201622705850006466615638653357974654"),stof<T>("-28.062415966599068695583676699101002397539170265371126850982119055")}, std::complex<T>{stof<T>("-18.144754066858149068905201000318787262198335924913404592168093821"),stof<T>("-19.655691398047286199212022087388755513629744924392089337995959235")}, std::complex<T>{stof<T>("14.318713899577494803847245532949107210186789357386007684317814266"),stof<T>("17.464796433574190534576828679089828618370740289106933558166902228")}, std::complex<T>{stof<T>("-37.275584983349337629500047636208234942143426323362934086641091808"),stof<T>("120.683720363586288302022700529706954083303370476553406223901766019")}, std::complex<T>{stof<T>("-5.0640616850712605728445510775568457438497106153224516270917389906"),stof<T>("-9.7872971614052357116164436201066045743353814609176417256353631691")}, std::complex<T>{stof<T>("-3.0504464993464146106914062567804903618423654026054729314063395575"),stof<T>("-0.6532396965859606617528467618512557222414085621751501641867652778")}, std::complex<T>{stof<T>("5.0640616850712605728445510775568457438497106153224516270917389906"),stof<T>("9.7872971614052357116164436201066045743353814609176417256353631691")}, std::complex<T>{stof<T>("-11.571534677498103221595805901807969973238692540600462040647536049"),stof<T>("-10.877494903462274676778243940810832655876167744544808341485831129")}, std::complex<T>{stof<T>("-3.8571782258327010738652686339359899910795641802001540135491786831"),stof<T>("-3.6258316344874248922594146469369442186253892481816027804952770431")}, std::complex<T>{stof<T>("-3.8571782258327010738652686339359899910795641802001540135491786831"),stof<T>("-3.6258316344874248922594146469369442186253892481816027804952770431")}, std::complex<T>{stof<T>("3.8571782258327010738652686339359899910795641802001540135491786831"),stof<T>("3.6258316344874248922594146469369442186253892481816027804952770431")}, std::complex<T>{stof<T>("0.62875951357982689959604956510979502283569846153696176805265663139"),stof<T>("-0.631211800731221965993284197375092772384272312334023096282934671")}, std::complex<T>{stof<T>("-0.62875951357982689959604956510979502283569846153696176805265663139"),stof<T>("0.631211800731221965993284197375092772384272312334023096282934671")}, std::complex<T>{stof<T>("1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("-1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("-0.62875951357982689959604956510979502283569846153696176805265663139"),stof<T>("0.631211800731221965993284197375092772384272312334023096282934671")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[125].real()/kbase.W[125].real()), rlog(k.W[126].real()/kbase.W[126].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_225_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_225_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-88.661646922899298078975762263890597093647098064767249817798404155"),stof<T>("58.818836263230353806898064079998574235411765623187667739894189849")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,66> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W21(k,dl), dlog_W23(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[125].real()/k.W[125].real()), rlog(kend.W[126].real()/k.W[126].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_225_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_225_DLogXconstant_part(base_point<T>, kend);
	value += f_4_225_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_225_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_225_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_225_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_225_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_225_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_225_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
