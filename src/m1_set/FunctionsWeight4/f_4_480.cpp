/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_480.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_480_abbreviated (const std::array<T,25>& abb) {
T z[14];
z[0] = -abb[20] + abb[21];
z[1] = abb[5] * z[0];
z[2] = abb[19] + abb[21];
z[3] = abb[1] * z[2];
z[4] = -abb[0] + abb[3];
z[4] = abb[19] * z[4];
z[5] = abb[0] * abb[20];
z[0] = abb[4] * z[0];
z[0] = z[0] + z[1] + -z[3] + -z[4] + z[5];
z[0] = abb[13] * z[0];
z[6] = abb[4] * abb[20];
z[5] = -z[4] + z[5] + -z[6];
z[7] = abb[9] * z[5];
z[2] = abb[20] + z[2];
z[2] = abb[8] * z[2];
z[8] = -abb[0] + abb[6];
z[9] = abb[20] * z[8];
z[4] = -z[2] + z[4] + z[9];
z[6] = z[3] + z[6];
z[8] = abb[21] * z[8];
z[9] = z[4] + z[6] + z[8];
z[10] = abb[10] * z[9];
z[7] = z[7] + z[10];
z[10] = abb[0] * abb[21];
z[11] = -abb[19] + abb[21];
z[11] = abb[2] * z[11];
z[10] = z[1] + z[5] + -z[10] + z[11];
z[12] = abb[12] * z[10];
z[13] = abb[6] * abb[21];
z[4] = z[4] + -z[11] + z[13];
z[1] = -z[1] + z[4] + z[6];
z[6] = abb[11] * z[1];
z[6] = (T(1) / T(2)) * z[6] + -z[7] + z[12];
z[6] = abb[11] * z[6];
z[12] = prod_pow(abb[12], 2);
z[12] = -abb[14] + (T(-1) / T(2)) * z[12];
z[10] = z[10] * z[12];
z[12] = abb[6] * abb[20];
z[2] = -z[2] + z[12];
z[12] = abb[6] * (T(-1) / T(2)) + abb[4] * (T(-1) / T(6)) + abb[0] * (T(1) / T(3));
z[12] = abb[21] * z[12];
z[11] = (T(-1) / T(2)) * z[2] + (T(-1) / T(3)) * z[3] + (T(1) / T(6)) * z[11] + z[12];
z[11] = prod_pow(m1_set::bc<T>[0], 2) * z[11];
z[12] = prod_pow(abb[10], 2);
z[12] = -abb[23] + (T(1) / T(2)) * z[12];
z[12] = z[9] * z[12];
z[5] = prod_pow(abb[9], 2) * z[5];
z[13] = abb[20] + abb[21];
z[13] = abb[4] * z[13];
z[4] = z[4] + z[13];
z[13] = abb[22] * z[4];
z[1] = abb[15] * z[1];
z[0] = abb[24] + z[0] + z[1] + (T(1) / T(2)) * z[5] + z[6] + z[10] + z[11] + z[12] + z[13];
z[1] = z[2] + z[3] + z[8];
z[1] = abb[11] * z[1];
z[1] = z[1] + -z[7];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[17] * z[9];
z[3] = abb[16] * z[4];
z[1] = abb[18] + z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_480_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("4.5265515847366941230171050516542370507560712010050776614483466955"),stof<T>("-3.2523906960787655526035081656748791026823377006020980897012255124")}, std::complex<T>{stof<T>("2.8054621160391153676736734372377161202846975426878197923327787147"),stof<T>("-0.7024787530290719887154990474336631111879229829780603384808257633")}, std::complex<T>{stof<T>("0.1238779919086845439548833307509799591981766548881416733646057978"),stof<T>("-2.8983466282993503370065006898233817366279887513852532773763626286")}};
	
	std::vector<C> intdlogs = {rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[124].real()/kbase.W[124].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_480_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_480_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(32)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (8 * (-2 + -4 * v[0] + v[1] + -v[2] + v[4] + v[5]) + -m1_set::bc<T>[2] * (-21 * v[0] + v[1] + 3 * (-8 + -3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = ((-2 + 3 * m1_set::bc<T>[2]) * (T(-1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[19] + abb[20] + -abb[21]) * (t * c[0] + c[1]);
	}
	{
T z[4];
z[0] = abb[13] + -abb[14] + -abb[15];
z[1] = abb[19] + abb[20] + -abb[21];
z[0] = z[0] * z[1];
z[2] = abb[11] * z[1];
z[1] = (T(1) / T(2)) * z[1];
z[3] = -abb[12] * z[1];
z[2] = z[2] + z[3];
z[2] = abb[12] * z[2];
z[1] = -prod_pow(abb[11], 2) * z[1];
z[0] = z[0] + z[1] + z[2];
return abb[7] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_480_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_480_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.689608685008015972691047967457854495286884943243126460251964977"),stof<T>("-18.06932071092858534184112342034801989125644399599113081748854218")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,25> abb = {dl[0], dl[1], dlog_W4(k,dl), dlog_W7(k,dl), dlog_W10(k,dl), dlog_W12(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W24(k,dl), f_1_1(k), f_1_3(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_9_im(k), T{0}, rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[124].real()/k.W[124].real()), f_2_2_re(k), f_2_9_re(k), T{0}};
abb[18] = SpDLog_f_4_480_W_17_Im(t, path, abb);
abb[24] = SpDLog_f_4_480_W_17_Re(t, path, abb);

                    
            return f_4_480_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_480_DLogXconstant_part(base_point<T>, kend);
	value += f_4_480_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_480_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_480_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_480_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_480_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_480_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_480_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
