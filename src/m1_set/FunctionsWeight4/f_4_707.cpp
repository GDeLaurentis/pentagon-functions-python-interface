/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_707.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_707_abbreviated (const std::array<T,72>& abb) {
T z[118];
z[0] = abb[57] + abb[59];
z[1] = -2 * abb[61] + z[0];
z[2] = abb[58] + abb[60];
z[3] = abb[62] + z[2];
z[4] = -z[1] + z[3];
z[5] = abb[5] * z[4];
z[6] = abb[61] + 2 * abb[62] + z[0];
z[7] = 2 * z[2];
z[8] = z[6] + z[7];
z[9] = abb[6] * z[8];
z[10] = z[5] + z[9];
z[11] = -abb[61] + z[0];
z[12] = z[2] + z[11];
z[13] = -abb[63] + z[12];
z[13] = abb[9] * z[13];
z[14] = 2 * z[13];
z[15] = abb[62] + z[0];
z[16] = abb[63] + z[15];
z[16] = abb[10] * z[16];
z[17] = 2 * z[16];
z[18] = abb[61] + abb[62];
z[19] = abb[63] + z[18];
z[19] = abb[0] * z[19];
z[20] = z[14] + -z[17] + 4 * z[19];
z[21] = abb[52] + -abb[55];
z[22] = abb[53] + -abb[56];
z[23] = 2 * abb[54] + z[21] + z[22];
z[23] = abb[27] * z[23];
z[24] = abb[61] + z[3];
z[25] = abb[16] * z[24];
z[26] = z[23] + z[25];
z[27] = z[20] + -z[26];
z[28] = z[0] + z[3];
z[29] = abb[15] * z[28];
z[30] = abb[14] * z[11];
z[31] = z[29] + -z[30];
z[32] = abb[54] + z[21];
z[33] = abb[24] * z[32];
z[34] = abb[22] * z[32];
z[33] = z[33] + z[34];
z[35] = abb[8] * z[24];
z[36] = 4 * z[35];
z[37] = abb[7] * z[24];
z[38] = abb[25] * z[32];
z[39] = z[10] + z[27] + z[31] + z[33] + -z[36] + -z[37] + -z[38];
z[39] = abb[29] * z[39];
z[40] = 2 * z[29];
z[41] = 2 * z[30];
z[42] = z[40] + -z[41];
z[43] = abb[2] * z[24];
z[26] = z[26] + -z[43];
z[44] = z[26] + z[42];
z[45] = abb[54] + z[22];
z[46] = abb[24] * z[45];
z[47] = abb[22] * z[45];
z[48] = z[46] + z[47];
z[49] = abb[6] * z[28];
z[50] = abb[5] * z[11];
z[51] = -z[49] + z[50];
z[52] = abb[4] * z[28];
z[53] = abb[3] * z[11];
z[54] = z[52] + -z[53];
z[37] = z[37] + z[54];
z[20] = z[20] + z[37] + -z[44] + z[48] + -z[51];
z[20] = abb[31] * z[20];
z[55] = abb[5] * z[28];
z[56] = z[33] + z[55];
z[57] = abb[12] * z[15];
z[58] = 2 * z[57];
z[59] = -z[38] + z[56] + z[58];
z[12] = abb[11] * z[12];
z[60] = 2 * z[12];
z[61] = abb[6] * z[11];
z[62] = z[60] + z[61];
z[63] = abb[3] + abb[4];
z[63] = z[24] * z[63];
z[64] = -z[2] + 3 * z[18];
z[64] = abb[7] * z[64];
z[64] = -3 * z[31] + z[36] + -z[59] + z[62] + z[63] + z[64];
z[64] = abb[35] * z[64];
z[65] = abb[1] * z[18];
z[63] = -z[27] + -z[58] + z[60] + -z[63] + 4 * z[65];
z[63] = abb[32] * z[63];
z[66] = -z[29] + z[49];
z[1] = abb[62] + -z[1] + -z[2];
z[1] = abb[7] * z[1];
z[1] = z[1] + z[41];
z[67] = 2 * z[65];
z[60] = z[60] + z[67];
z[68] = -z[58] + z[60];
z[69] = 2 * z[50];
z[70] = -z[1] + -z[38] + -z[66] + -z[68] + z[69];
z[70] = abb[33] * z[70];
z[12] = z[12] + z[65];
z[65] = -z[12] + -z[66];
z[71] = z[48] + -z[50];
z[58] = -z[30] + z[58];
z[6] = abb[7] * z[6];
z[65] = -z[6] + z[58] + 2 * z[65] + -z[71];
z[65] = abb[34] * z[65];
z[72] = abb[16] * z[28];
z[73] = 2 * z[19];
z[74] = -z[14] + z[72] + -z[73];
z[75] = abb[27] * z[32];
z[76] = z[74] + z[75];
z[77] = z[29] + z[38];
z[78] = z[76] + z[77];
z[79] = abb[2] * z[28];
z[80] = z[52] + z[79];
z[81] = z[78] + -z[80];
z[82] = abb[36] * z[81];
z[83] = z[21] + -z[22];
z[84] = abb[29] + -abb[35];
z[84] = -z[83] * z[84];
z[85] = abb[33] * z[32];
z[86] = abb[34] * z[45];
z[84] = z[84] + -z[85] + z[86];
z[87] = -abb[31] * z[83];
z[88] = z[84] + -z[87];
z[89] = abb[21] + abb[23];
z[88] = -z[88] * z[89];
z[90] = abb[2] * z[11];
z[91] = z[53] + z[90];
z[92] = -z[30] + z[91];
z[93] = z[17] + -z[73];
z[94] = abb[16] * z[11];
z[95] = z[93] + -z[94];
z[96] = abb[27] * z[45];
z[97] = z[95] + z[96];
z[98] = z[92] + z[97];
z[98] = abb[30] * z[98];
z[99] = abb[35] * z[45];
z[100] = abb[29] * z[45];
z[99] = z[86] + -z[99] + z[100];
z[101] = abb[30] * z[45];
z[102] = -z[99] + z[101];
z[102] = abb[26] * z[102];
z[20] = z[20] + z[39] + z[63] + z[64] + z[65] + z[70] + z[82] + z[88] + z[98] + z[102];
z[20] = m1_set::bc<T>[0] * z[20];
z[39] = z[14] + -z[50];
z[63] = -z[2] + z[18];
z[64] = 2 * abb[63];
z[65] = z[63] + z[64];
z[70] = abb[7] * z[65];
z[82] = z[31] + -z[39] + -z[49] + -z[70] + z[93];
z[88] = abb[50] * z[82];
z[93] = abb[3] * z[28];
z[60] = -z[60] + z[93];
z[102] = z[29] + z[52] + z[60];
z[102] = abb[47] * z[102];
z[15] = -z[2] + z[15] + z[64];
z[15] = abb[7] * z[15];
z[80] = -z[49] + z[80];
z[15] = -z[15] + z[17] + -z[72] + z[80];
z[17] = abb[49] * z[15];
z[72] = -abb[22] + -abb[24] + -abb[26];
z[72] = z[11] * z[72];
z[103] = abb[25] * z[28];
z[0] = -abb[61] + 2 * z[0] + z[3];
z[3] = -abb[21] * z[0];
z[3] = z[3] + z[72] + z[103];
z[3] = abb[71] * z[3];
z[72] = -z[74] + -z[77];
z[72] = abb[44] * z[72];
z[74] = z[47] + -z[91] + z[94];
z[14] = -z[14] + z[74];
z[14] = abb[46] * z[14];
z[94] = z[7] + z[11] + -z[64];
z[103] = abb[46] * z[94];
z[104] = -abb[44] + abb[47];
z[104] = z[28] * z[104];
z[103] = z[103] + -z[104];
z[103] = abb[7] * z[103];
z[105] = abb[7] * z[11];
z[106] = z[61] + z[105];
z[107] = -z[30] + z[106];
z[95] = -z[95] + -z[107];
z[95] = abb[45] * z[95];
z[108] = abb[49] * z[32];
z[109] = abb[45] * z[45];
z[108] = z[108] + z[109];
z[110] = abb[46] * z[45];
z[111] = abb[44] * z[32];
z[110] = z[110] + z[111];
z[112] = -z[108] + -z[110];
z[112] = abb[27] * z[112];
z[113] = abb[4] * z[11];
z[58] = z[58] + -z[67] + -z[113];
z[106] = -z[53] + z[58] + z[106];
z[106] = abb[48] * z[106];
z[114] = abb[47] * z[32];
z[110] = z[110] + -z[114];
z[115] = abb[48] * z[45];
z[108] = -z[108] + z[110] + z[115];
z[116] = abb[21] * z[108];
z[117] = -abb[71] * z[0];
z[108] = z[108] + z[117];
z[108] = abb[23] * z[108];
z[111] = -z[111] + z[114];
z[111] = -abb[22] * z[111];
z[117] = abb[46] * z[11];
z[104] = -z[104] + z[117];
z[104] = abb[5] * z[104];
z[110] = abb[24] * z[110];
z[109] = -z[109] + z[115];
z[109] = abb[26] * z[109];
z[114] = abb[25] * z[114];
z[3] = z[3] + z[14] + z[17] + z[20] + z[72] + z[88] + z[95] + z[102] + z[103] + z[104] + z[106] + z[108] + z[109] + z[110] + z[111] + z[112] + z[114] + z[116];
z[3] = 4 * z[3];
z[14] = abb[70] * z[82];
z[17] = abb[13] * z[24];
z[20] = z[17] + -z[34];
z[35] = 2 * z[35];
z[72] = z[31] + -z[35];
z[10] = z[10] + -z[20] + z[38] + -z[43] + z[72];
z[10] = abb[37] * z[10];
z[82] = abb[32] * z[45];
z[88] = abb[31] * z[45];
z[82] = z[82] + -z[88];
z[95] = -z[82] + -z[99];
z[95] = abb[35] * z[95];
z[82] = z[82] + z[101];
z[99] = abb[29] + abb[34];
z[82] = z[82] * z[99];
z[99] = -abb[31] + abb[32];
z[101] = -abb[30] * z[99];
z[102] = prod_pow(abb[30], 2);
z[101] = abb[37] + -abb[39] + abb[65] + z[101] + -z[102];
z[101] = z[45] * z[101];
z[103] = abb[68] * z[45];
z[104] = abb[51] * z[11];
z[82] = z[82] + z[95] + z[101] + -z[103] + -z[104];
z[82] = abb[26] * z[82];
z[95] = -z[29] + z[35];
z[101] = -z[49] + z[79] + z[95];
z[101] = abb[40] * z[101];
z[94] = -abb[7] * z[94];
z[39] = z[39] + -z[74] + z[94] + z[96];
z[39] = abb[66] * z[39];
z[74] = abb[7] * z[28];
z[56] = z[56] + z[74];
z[78] = -z[56] + z[78];
z[78] = abb[64] * z[78];
z[80] = -z[33] + z[80];
z[94] = abb[5] * z[24];
z[96] = -z[72] + z[80] + -z[94] + -z[105];
z[106] = abb[42] * z[96];
z[9] = z[9] + -z[35];
z[71] = -z[9] + -z[71] + -z[92];
z[71] = abb[39] * z[71];
z[60] = z[60] + z[77];
z[108] = abb[21] * z[32];
z[56] = -z[52] + z[56] + -z[60] + z[108];
z[56] = abb[67] * z[56];
z[15] = -z[15] + z[75] + z[108];
z[15] = abb[69] * z[15];
z[75] = abb[6] * z[24];
z[91] = z[72] + z[75] + z[91];
z[108] = abb[41] * z[91];
z[109] = abb[40] * z[28];
z[110] = abb[68] * z[11];
z[109] = z[109] + z[110];
z[109] = abb[4] * z[109];
z[111] = abb[51] * z[28];
z[112] = abb[40] * z[32];
z[111] = z[111] + -z[112];
z[111] = abb[25] * z[111];
z[104] = z[104] + z[112];
z[112] = abb[22] * z[104];
z[114] = -abb[40] * z[4];
z[11] = abb[41] * z[11];
z[11] = z[11] + z[114];
z[11] = abb[5] * z[11];
z[10] = z[10] + z[11] + -z[14] + z[15] + z[39] + z[56] + z[71] + z[78] + z[82] + z[101] + z[106] + -z[108] + z[109] + z[111] + -z[112];
z[11] = 2 * z[38];
z[14] = z[11] + z[36];
z[15] = z[61] + -z[67];
z[4] = abb[3] * z[4];
z[8] = abb[4] * z[8];
z[36] = 2 * abb[7];
z[39] = z[18] * z[36];
z[21] = -abb[54] + -2 * z[21] + z[22];
z[21] = abb[24] * z[21];
z[4] = z[4] + z[8] + z[14] + z[15] + -z[17] + z[21] + -z[34] + z[39] + -z[42] + -z[55];
z[4] = abb[35] * z[4];
z[8] = -z[35] + z[40];
z[6] = -z[6] + z[8] + -z[30] + z[53] + -z[62] + z[113];
z[6] = abb[34] * z[6];
z[1] = -z[1] + -z[52] + z[59] + -z[93] + -z[95];
z[1] = abb[33] * z[1];
z[21] = -abb[24] * z[83];
z[17] = -z[17] + z[21];
z[21] = -z[17] + -z[37] + -z[38] + z[72];
z[22] = abb[29] + -abb[31];
z[21] = z[21] * z[22];
z[34] = -z[31] + z[68];
z[39] = abb[7] * z[63];
z[40] = -z[17] + z[39];
z[42] = z[34] + -z[38] + z[40] + -z[54];
z[42] = abb[32] * z[42];
z[1] = z[1] + z[6] + z[21] + z[42];
z[1] = 2 * z[1] + z[4];
z[1] = abb[35] * z[1];
z[4] = 2 * z[49];
z[6] = -z[4] + 14 * z[12] + -z[23] + -z[25] + z[29] + -z[30] + z[43] + z[48] + z[52] + -z[53];
z[7] = 10 * abb[63] + -z[7] + z[18];
z[7] = abb[7] * z[7];
z[7] = -z[7] + z[50];
z[12] = z[13] + -z[16];
z[13] = (T(-1) / T(3)) * z[83];
z[13] = -z[13] * z[89];
z[6] = (T(-1) / T(3)) * z[6] + (T(-2) / T(3)) * z[7] + 6 * z[12] + z[13] + (T(16) / T(3)) * z[19] + (T(14) / T(3)) * z[57];
z[6] = prod_pow(m1_set::bc<T>[0], 2) * z[6];
z[7] = -3 * z[2] + -z[18] + z[64];
z[7] = abb[7] * z[7];
z[12] = 4 * z[12] + 6 * z[19];
z[4] = z[4] + z[7] + z[12] + -z[14] + -z[17] + -z[26] + -z[69];
z[4] = abb[31] * z[4];
z[7] = -z[20] + z[27] + -z[31] + z[37] + z[46] + z[75] + z[94];
z[7] = abb[29] * z[7];
z[13] = -z[17] + z[70];
z[12] = z[11] + -z[12] + -z[13] + z[44];
z[12] = abb[32] * z[12];
z[14] = 2 * z[98];
z[4] = z[4] + z[7] + z[12] + z[14];
z[4] = abb[29] * z[4];
z[7] = abb[51] * z[0];
z[12] = abb[41] * z[45];
z[16] = abb[66] * z[45];
z[7] = z[7] + -z[12] + z[16] + z[103];
z[12] = -abb[32] * z[83];
z[12] = z[12] + -z[87];
z[17] = z[12] + z[84];
z[19] = 2 * abb[35];
z[17] = z[17] * z[19];
z[19] = prod_pow(abb[31], 2);
z[20] = 2 * abb[38];
z[19] = z[19] + z[20];
z[19] = -z[19] * z[83];
z[21] = abb[29] + abb[32];
z[12] = z[12] * z[21];
z[21] = -z[22] * z[32];
z[21] = 2 * z[21] + -z[85];
z[21] = abb[33] * z[21];
z[22] = -z[88] + z[100];
z[22] = 2 * z[22] + z[86];
z[22] = abb[34] * z[22];
z[23] = 2 * abb[65];
z[25] = z[23] * z[45];
z[7] = -2 * z[7] + z[12] + -z[17] + z[19] + z[21] + z[22] + z[25];
z[12] = abb[42] + abb[64];
z[17] = 2 * z[32];
z[19] = -z[12] * z[17];
z[19] = z[7] + z[19];
z[19] = abb[21] * z[19];
z[21] = 2 * z[52];
z[8] = -z[5] + -z[8] + -z[11] + z[21] + -z[33] + -z[49] + -z[76] + 2 * z[79];
z[8] = abb[36] * z[8];
z[11] = abb[29] + -z[99];
z[11] = z[11] * z[81];
z[5] = z[5] + -z[35] + z[77] + -z[80];
z[5] = abb[33] * z[5];
z[5] = z[5] + z[11];
z[5] = 2 * z[5] + z[8];
z[5] = abb[36] * z[5];
z[8] = abb[67] + abb[69] + -z[12];
z[8] = z[8] * z[17];
z[7] = z[7] + z[8];
z[7] = abb[23] * z[7];
z[8] = z[26] + -z[73];
z[11] = -abb[63] + z[2];
z[11] = z[11] * z[36];
z[11] = -z[8] + z[11] + -z[31] + z[48] + z[54];
z[11] = abb[31] * z[11];
z[11] = z[11] + z[14];
z[11] = abb[31] * z[11];
z[12] = -z[48] + -z[66] + -z[74];
z[12] = abb[31] * z[12];
z[9] = z[9] + z[47] + -z[50];
z[17] = z[9] + z[46] + z[92];
z[17] = abb[30] * z[17];
z[22] = z[58] + z[90];
z[22] = abb[32] * z[22];
z[25] = -z[50] + -z[74] + z[91];
z[26] = -abb[29] * z[25];
z[12] = z[12] + z[17] + z[22] + z[26];
z[17] = -abb[34] * z[25];
z[12] = 2 * z[12] + z[17];
z[12] = abb[34] * z[12];
z[17] = -z[30] + -z[38] + z[50] + z[105];
z[17] = abb[31] * z[17];
z[22] = abb[29] * z[96];
z[25] = z[34] + z[39] + -z[51];
z[25] = abb[34] * z[25];
z[26] = z[60] + -z[79];
z[26] = abb[32] * z[26];
z[17] = z[17] + z[22] + z[25] + z[26];
z[22] = abb[33] * z[96];
z[17] = 2 * z[17] + z[22];
z[17] = abb[33] * z[17];
z[22] = abb[28] * z[83];
z[2] = z[2] + z[18];
z[2] = abb[19] * z[2];
z[18] = abb[20] * z[0];
z[25] = -abb[17] * z[65];
z[24] = abb[18] * z[24];
z[2] = z[2] + z[18] + z[22] + z[24] + z[25];
z[2] = abb[43] * z[2];
z[18] = 2 * z[53];
z[9] = -z[9] + -z[18] + z[41] + -2 * z[90] + -z[97];
z[9] = z[9] * z[102];
z[8] = z[8] + z[13] + z[18] + -z[21];
z[8] = abb[31] * z[8];
z[13] = -abb[3] + abb[4];
z[0] = z[0] * z[13];
z[0] = z[0] + -z[40] + z[43] + z[67];
z[13] = abb[32] * z[0];
z[8] = z[8] + z[13] + -z[14];
z[8] = abb[32] * z[8];
z[0] = z[0] * z[20];
z[13] = -z[15] + z[53];
z[13] = 2 * z[13] + z[41] + -4 * z[57];
z[13] = abb[68] * z[13];
z[14] = abb[37] * z[45];
z[14] = z[14] + -z[16] + -z[104];
z[15] = -z[45] * z[102];
z[14] = 2 * z[14] + z[15];
z[14] = abb[24] * z[14];
z[15] = z[97] + z[107];
z[15] = z[15] * z[23];
z[16] = abb[41] * z[28];
z[16] = z[16] + -z[110];
z[16] = z[16] * z[36];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + 2 * z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + z[17] + z[19];
z[0] = 2 * z[0];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_707_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-10.402302819131233336271940183191322136570276882477279319319257397"),stof<T>("12.769388978144903263363784176908690571680015421503048994714708313")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-39.127742708599188415843921814168177327217606882285435505760270408"),stof<T>("-24.110144033389001136235819648456888144819222584841004372468916284")}, std::complex<T>{stof<T>("-39.127742708599188415843921814168177327217606882285435505760270408"),stof<T>("-24.110144033389001136235819648456888144819222584841004372468916284")}, std::complex<T>{stof<T>("-53.578578824072759289728574844223385287980097839800100636397660361"),stof<T>("-33.047500621757702881943217170995313409495833620192392272446330992")}, std::complex<T>{stof<T>("-53.578578824072759289728574844223385287980097839800100636397660361"),stof<T>("-33.047500621757702881943217170995313409495833620192392272446330992")}, std::complex<T>{stof<T>("-25.722252780192909327092031474302414320260816278425355599495051827"),stof<T>("-23.243718192739844943996252882180639840840353626088414697033473416")}};
	
	std::vector<C> intdlogs = {rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_707_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_707_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-39.791654278296587363448479654626997047413156050698524340387821261"),stof<T>("34.600555759611050309919618566339220328265813945433386627803780576")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,72> abb = {dl[0], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W15(k,dl), dlog_W23(k,dl), dlog_W26(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_8(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_14(k), f_2_19(k), f_2_20(k), f_2_23(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_10_im(k), f_2_11_im(k), f_2_12_im(k), f_2_22_im(k), f_2_26_im(k), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_10_re(k), f_2_11_re(k), f_2_12_re(k), f_2_22_re(k), f_2_26_re(k)};

                    
            return f_4_707_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_707_DLogXconstant_part(base_point<T>, kend);
	value += f_4_707_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_707_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_707_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_707_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_707_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_707_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_707_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
