/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_347.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_347_abbreviated (const std::array<T,62>& abb) {
T z[95];
z[0] = abb[44] + -abb[50];
z[1] = abb[48] + -abb[53];
z[2] = abb[47] + z[0] + -z[1];
z[2] = abb[10] * z[2];
z[3] = -abb[46] + abb[48];
z[4] = -abb[53] + z[3];
z[5] = abb[43] + -abb[49];
z[6] = z[4] + -z[5];
z[6] = abb[9] * z[6];
z[7] = abb[45] + abb[47];
z[8] = z[3] + z[7];
z[8] = -abb[52] + (T(1) / T(2)) * z[8];
z[9] = abb[5] * z[8];
z[10] = abb[45] + abb[46];
z[11] = -abb[47] + z[10];
z[12] = abb[48] + z[11];
z[12] = -abb[51] + (T(1) / T(2)) * z[12];
z[13] = abb[6] * z[12];
z[9] = -z[2] + z[6] + z[9] + z[13];
z[14] = abb[43] + abb[44];
z[15] = 2 * abb[53];
z[16] = abb[46] + z[15];
z[17] = z[14] + z[16];
z[18] = abb[47] * (T(1) / T(3));
z[19] = abb[42] * (T(2) / T(3));
z[17] = -abb[49] + -abb[50] + (T(1) / T(3)) * z[17] + z[18] + z[19];
z[17] = abb[0] * z[17];
z[20] = -abb[52] + z[7];
z[1] = -z[1] + z[20];
z[1] = abb[12] * z[1];
z[21] = -3 * abb[48] + z[7] + z[16];
z[22] = abb[8] * z[21];
z[4] = -abb[45] + abb[51] + z[4];
z[4] = abb[11] * z[4];
z[17] = -z[1] + z[4] + z[17] + z[22];
z[22] = abb[47] * (T(7) / T(6));
z[23] = abb[45] * (T(1) / T(3));
z[24] = 2 * abb[44];
z[25] = 2 * abb[50] + -z[24];
z[26] = abb[46] + abb[53] * (T(-5) / T(2));
z[26] = abb[51] * (T(1) / T(2)) + -z[22] + z[23] + z[25] + (T(1) / T(3)) * z[26];
z[26] = abb[2] * z[26];
z[27] = 2 * abb[43];
z[28] = 2 * abb[49] + -z[27];
z[29] = -7 * abb[46] + -5 * abb[53];
z[18] = abb[52] * (T(1) / T(2)) + z[18] + z[23] + z[28] + (T(1) / T(6)) * z[29];
z[18] = abb[1] * z[18];
z[23] = abb[45] * (T(1) / T(2));
z[29] = abb[47] * (T(1) / T(2));
z[30] = z[23] + -z[29];
z[3] = (T(1) / T(2)) * z[3];
z[31] = z[0] + z[3] + -z[30];
z[32] = -abb[14] * z[31];
z[33] = abb[54] + abb[55];
z[34] = (T(1) / T(2)) * z[33];
z[35] = abb[25] * z[34];
z[35] = -z[32] + z[35];
z[36] = abb[51] + abb[52];
z[37] = abb[46] * (T(1) / T(2));
z[38] = abb[53] + z[37];
z[22] = abb[48] + abb[45] * (T(17) / T(3)) + z[22] + (T(-9) / T(2)) * z[36] + (T(7) / T(3)) * z[38];
z[22] = abb[3] * z[22];
z[39] = z[23] + z[29];
z[40] = abb[46] + abb[48];
z[40] = (T(1) / T(2)) * z[40];
z[41] = z[5] + -z[39] + z[40];
z[42] = abb[13] * z[41];
z[43] = abb[21] + abb[23];
z[44] = 2 * z[43];
z[44] = z[33] * z[44];
z[45] = (T(3) / T(4)) * z[33];
z[46] = abb[22] * z[45];
z[47] = abb[24] * z[45];
z[46] = z[46] + z[47];
z[48] = abb[48] * (T(-3) / T(2)) + z[38] + z[39];
z[49] = abb[15] * z[48];
z[50] = abb[47] * (T(3) / T(2)) + -z[37];
z[51] = -abb[53] + abb[48] * (T(5) / T(2));
z[52] = abb[49] + -z[50] + z[51];
z[19] = abb[45] * (T(17) / T(12)) + z[19];
z[52] = abb[43] * (T(-7) / T(6)) + abb[51] * (T(-3) / T(2)) + abb[44] * (T(-2) / T(3)) + z[19] + (T(1) / T(2)) * z[52];
z[52] = abb[4] * z[52];
z[53] = abb[46] * (T(3) / T(2));
z[54] = -z[29] + z[53];
z[51] = abb[50] + z[51] + -z[54];
z[19] = abb[44] * (T(-7) / T(6)) + abb[52] * (T(-3) / T(2)) + abb[43] * (T(-2) / T(3)) + z[19] + (T(1) / T(2)) * z[51];
z[19] = abb[7] * z[19];
z[51] = abb[20] * z[34];
z[9] = (T(1) / T(2)) * z[9] + 2 * z[17] + z[18] + z[19] + z[22] + z[26] + z[35] + z[42] + z[44] + z[46] + -z[49] + z[51] + z[52];
z[9] = prod_pow(m1_set::bc<T>[0], 2) * z[9];
z[17] = -abb[7] * z[31];
z[18] = abb[3] + abb[4];
z[12] = -z[12] * z[18];
z[19] = abb[25] * z[33];
z[22] = abb[22] * z[33];
z[26] = z[19] + z[22];
z[11] = -abb[51] + -z[0] + z[11];
z[11] = abb[2] * z[11];
z[11] = z[11] + z[12] + z[13] + z[17] + (T(1) / T(2)) * z[26] + -z[32];
z[11] = abb[34] * z[11];
z[12] = -abb[19] * z[31];
z[3] = -abb[50] + z[3];
z[17] = -abb[42] + abb[43];
z[26] = z[3] + -z[17] + z[39];
z[26] = abb[16] * z[26];
z[31] = -abb[42] + abb[44];
z[40] = -abb[49] + z[40];
z[30] = -z[30] + z[31] + -z[40];
z[30] = abb[17] * z[30];
z[42] = abb[27] * z[34];
z[12] = z[12] + -z[26] + z[30] + -z[42];
z[26] = abb[61] * z[12];
z[30] = prod_pow(abb[29], 2);
z[42] = prod_pow(abb[30], 2);
z[44] = z[30] + -z[42];
z[52] = z[4] * z[44];
z[44] = z[34] * z[44];
z[55] = abb[34] * z[33];
z[44] = z[44] + z[55];
z[44] = abb[24] * z[44];
z[26] = z[26] + -z[44] + -z[52];
z[44] = abb[22] * z[34];
z[52] = z[13] + z[44];
z[55] = z[49] + z[52];
z[56] = abb[48] * (T(1) / T(2));
z[57] = -abb[53] + z[56];
z[37] = z[37] + z[57];
z[58] = abb[45] * (T(1) / T(4));
z[0] = abb[47] * (T(3) / T(4)) + z[0] + (T(-1) / T(2)) * z[37] + -z[58];
z[0] = abb[7] * z[0];
z[37] = abb[26] * z[33];
z[59] = (T(1) / T(4)) * z[37];
z[60] = abb[47] + -abb[51] + abb[53] + -z[25];
z[60] = abb[2] * z[60];
z[0] = z[0] + -z[2] + (T(1) / T(2)) * z[55] + -z[59] + z[60];
z[0] = z[0] * z[30];
z[30] = -abb[4] * z[41];
z[55] = abb[3] + abb[7];
z[60] = -z[8] * z[55];
z[61] = abb[20] * z[33];
z[61] = z[22] + z[61];
z[20] = -abb[46] + -z[5] + z[20];
z[20] = abb[1] * z[20];
z[62] = abb[24] * z[34];
z[20] = z[20] + z[30] + z[60] + (T(1) / T(2)) * z[61] + z[62];
z[20] = abb[35] * z[20];
z[30] = abb[8] * z[48];
z[60] = z[30] + -z[49];
z[63] = abb[45] * (T(3) / T(2)) + -z[57];
z[64] = 2 * abb[52] + -z[50] + -z[63];
z[55] = z[55] * z[64];
z[64] = abb[26] * z[34];
z[55] = z[1] + z[55] + z[60] + z[64];
z[34] = z[34] * z[43];
z[65] = z[34] + z[55];
z[65] = abb[59] * z[65];
z[52] = z[1] + -z[30] + -z[52];
z[38] = z[29] + z[38];
z[66] = abb[45] * (T(5) / T(2));
z[56] = -z[38] + -z[56] + -z[66];
z[36] = z[36] + (T(1) / T(2)) * z[56];
z[36] = abb[3] * z[36];
z[56] = (T(-1) / T(4)) * z[43];
z[56] = z[33] * z[56];
z[36] = z[36] + (T(1) / T(2)) * z[52] + z[56];
z[36] = z[36] * z[42];
z[52] = z[53] + -z[57];
z[39] = z[28] + z[39] + -z[52];
z[39] = abb[4] * z[39];
z[53] = 3 * abb[49];
z[56] = 2 * abb[46] + abb[53];
z[67] = z[53] + -z[56];
z[7] = 3 * abb[43] + -z[7] + -z[67];
z[68] = abb[1] * z[7];
z[39] = z[39] + z[51] + -z[68];
z[34] = z[34] + z[60];
z[44] = z[44] + -z[64];
z[51] = z[34] + z[44] + z[62];
z[62] = -z[6] + z[39] + z[51];
z[62] = abb[56] * z[62];
z[0] = z[0] + z[11] + z[20] + (T(-1) / T(2)) * z[26] + z[36] + z[62] + z[65];
z[11] = abb[43] * (T(1) / T(2));
z[20] = abb[50] * (T(3) / T(2)) + -z[11];
z[26] = abb[44] * (T(1) / T(2));
z[36] = abb[49] * (T(3) / T(2)) + -z[26];
z[38] = -abb[42] + z[20] + z[36] + -z[38];
z[38] = abb[0] * z[38];
z[62] = 3 * abb[50];
z[65] = 2 * abb[47] + abb[53];
z[69] = z[62] + -z[65];
z[10] = 3 * abb[44] + -z[10] + -z[69];
z[70] = abb[2] * z[10];
z[71] = z[38] + z[70];
z[72] = abb[25] * z[45];
z[72] = (T(3) / T(2)) * z[32] + -z[72];
z[73] = z[71] + z[72];
z[74] = abb[20] * z[45];
z[46] = z[46] + z[74];
z[75] = z[46] + -z[68];
z[76] = abb[42] * (T(1) / T(2));
z[36] = -z[27] + z[36] + z[76];
z[77] = -abb[45] + abb[46] + abb[53] * (T(1) / T(2)) + -z[29] + -z[36];
z[77] = abb[4] * z[77];
z[78] = z[24] + -z[76];
z[79] = -abb[46] + abb[53] + -z[62];
z[79] = -abb[45] + abb[47] + z[11] + z[78] + (T(1) / T(2)) * z[79];
z[79] = abb[7] * z[79];
z[45] = -z[43] * z[45];
z[80] = abb[3] * z[48];
z[45] = (T(-3) / T(2)) * z[30] + z[45] + z[49] + z[73] + -z[75] + z[77] + z[79] + (T(-1) / T(2)) * z[80];
z[45] = abb[28] * z[45];
z[77] = z[32] + z[70];
z[79] = 3 * z[2];
z[81] = (T(3) / T(2)) * z[33];
z[82] = abb[26] * z[81];
z[83] = -z[49] + z[82];
z[84] = z[79] + z[83];
z[85] = abb[4] * z[48];
z[86] = z[80] + z[85];
z[87] = 2 * abb[7];
z[10] = z[10] * z[87];
z[88] = abb[25] * z[81];
z[10] = z[10] + 3 * z[77] + -z[84] + -z[86] + -z[88];
z[10] = abb[29] * z[10];
z[77] = abb[15] * z[21];
z[48] = abb[7] * z[48];
z[30] = 3 * z[30] + -z[48] + -z[77] + -z[86];
z[86] = z[43] * z[81];
z[86] = z[30] + z[86];
z[88] = abb[30] * z[86];
z[45] = -z[10] + z[45] + z[88];
z[45] = abb[28] * z[45];
z[19] = z[19] + z[61];
z[19] = (T(1) / T(2)) * z[19] + -z[32];
z[32] = -z[29] + z[40];
z[32] = 3 * z[32] + -z[66];
z[40] = abb[42] + -z[32];
z[40] = -z[26] + -z[27] + (T(1) / T(2)) * z[40];
z[40] = abb[4] * z[40];
z[3] = z[3] + z[29];
z[3] = 3 * z[3] + z[17] + -z[66];
z[29] = (T(-1) / T(2)) * z[3] + -z[24];
z[29] = abb[7] * z[29];
z[29] = (T(3) / T(2)) * z[19] + z[29] + z[40] + z[47] + -z[68] + -z[71] + z[80];
z[29] = abb[31] * z[29];
z[40] = 2 * z[70];
z[38] = -z[38] + z[40] + z[72] + z[75];
z[47] = abb[48] * (T(3) / T(4));
z[61] = abb[45] * (T(3) / T(4)) + z[47];
z[66] = -abb[53] + z[61];
z[71] = abb[46] * (T(5) / T(4));
z[72] = abb[47] * (T(1) / T(4));
z[75] = -z[36] + -z[66] + z[71] + -z[72];
z[75] = abb[4] * z[75];
z[88] = 4 * abb[44];
z[11] = abb[50] * (T(-9) / T(2)) + -z[11] + z[76] + z[88];
z[61] = -z[15] + z[61];
z[89] = abb[47] * (T(13) / T(4));
z[71] = -z[11] + z[61] + z[71] + -z[89];
z[71] = abb[7] * z[71];
z[71] = -z[38] + z[71] + z[75] + z[84];
z[71] = abb[28] * z[71];
z[75] = 3 * abb[52];
z[84] = 2 * abb[45];
z[65] = abb[46] + -z[65] + z[75] + -z[84];
z[90] = z[65] * z[87];
z[91] = 2 * abb[3];
z[92] = z[65] * z[91];
z[93] = 3 * z[1];
z[92] = z[92] + z[93];
z[83] = z[83] + z[85] + z[90] + z[92];
z[90] = -abb[30] + abb[33];
z[83] = -z[83] * z[90];
z[94] = -abb[1] + abb[3];
z[65] = z[65] * z[94];
z[24] = abb[46] + z[17] + -z[24] + z[69];
z[24] = abb[0] * z[24];
z[69] = -abb[4] + z[87];
z[17] = abb[44] + -abb[45] + z[17];
z[69] = z[17] * z[69];
z[24] = z[24] + z[65] + z[69] + z[70];
z[24] = abb[32] * z[24];
z[24] = z[10] + z[24] + z[29] + z[71] + z[83];
z[24] = abb[32] * z[24];
z[29] = z[49] + z[82];
z[65] = abb[22] * z[81];
z[48] = z[48] + z[65];
z[65] = z[29] + -z[48];
z[69] = 3 * z[6];
z[71] = abb[24] * z[81];
z[82] = z[69] + -z[71];
z[83] = 2 * abb[4];
z[7] = z[7] * z[83];
z[81] = abb[20] * z[81];
z[7] = z[7] + z[65] + 3 * z[68] + -z[80] + -z[81] + z[82];
z[80] = abb[33] * z[7];
z[81] = 2 * z[68];
z[69] = -z[69] + z[73] + -z[81];
z[73] = 4 * abb[43];
z[26] = abb[49] * (T(-9) / T(2)) + -z[26] + z[73] + z[76];
z[76] = abb[46] * (T(13) / T(4));
z[87] = abb[47] * (T(5) / T(4));
z[61] = -z[26] + z[61] + -z[76] + z[87];
z[61] = abb[4] * z[61];
z[20] = -z[20] + z[78];
z[78] = abb[46] * (T(1) / T(4));
z[66] = z[20] + -z[66] + -z[78] + z[87];
z[66] = abb[7] * z[66];
z[29] = -z[29] + z[46] + z[61] + z[66] + z[69];
z[29] = abb[28] * z[29];
z[46] = 3 * abb[51];
z[56] = -abb[47] + -z[46] + z[56] + z[84];
z[61] = z[56] * z[83];
z[66] = z[56] * z[91];
z[84] = 3 * z[13];
z[66] = z[66] + -z[84];
z[87] = 3 * z[4];
z[94] = abb[24] * z[33];
z[61] = z[61] + z[65] + z[66] + z[87] + (T(-3) / T(2)) * z[94];
z[65] = abb[29] + -abb[30];
z[61] = z[61] * z[65];
z[65] = abb[2] + -abb[3];
z[56] = z[56] * z[65];
z[27] = abb[47] + -z[27] + z[31] + z[67];
z[27] = abb[0] * z[27];
z[65] = -abb[7] + z[83];
z[17] = z[17] * z[65];
z[17] = z[17] + z[27] + z[56] + z[68];
z[17] = abb[31] * z[17];
z[17] = z[17] + z[29] + z[61] + z[80];
z[17] = abb[31] * z[17];
z[7] = -abb[28] * z[7];
z[5] = z[5] + (T(1) / T(2)) * z[52] + -z[58] + -z[72];
z[5] = abb[4] * z[5];
z[1] = z[1] + -z[49];
z[27] = abb[46] + -abb[52] + abb[53] + -z[28];
z[27] = abb[1] * z[27];
z[1] = (T(-1) / T(2)) * z[1] + z[5] + z[6] + z[27] + z[59];
z[1] = abb[33] * z[1];
z[1] = 3 * z[1] + z[7];
z[1] = abb[33] * z[1];
z[5] = z[23] + z[25] + -z[50] + z[57];
z[5] = abb[7] * z[5];
z[2] = z[2] + z[5] + z[34] + z[35] + z[64] + -z[70];
z[2] = 3 * z[2];
z[5] = abb[57] * z[2];
z[6] = abb[60] * z[86];
z[7] = abb[32] * (T(1) / T(2));
z[23] = -abb[28] + abb[31];
z[23] = z[7] * z[23];
z[25] = prod_pow(abb[28], 2);
z[27] = abb[28] * abb[33];
z[28] = -abb[33] + abb[28] * (T(1) / T(2));
z[28] = abb[31] * z[28];
z[23] = abb[35] + abb[56] + z[23] + (T(-1) / T(2)) * z[25] + z[27] + z[28];
z[25] = 3 * abb[13];
z[23] = z[23] * z[25] * z[41];
z[27] = 2 * abb[51] + -z[54] + -z[63];
z[18] = z[18] * z[27];
z[4] = -z[4] + z[13] + z[18] + z[51];
z[4] = 3 * z[4];
z[13] = abb[58] * z[4];
z[18] = prod_pow(abb[33], 2);
z[18] = -z[18] + z[42];
z[27] = -abb[32] * z[90];
z[18] = abb[35] + abb[59] + (T(-1) / T(2)) * z[18] + z[27];
z[27] = 3 * abb[5];
z[18] = z[8] * z[18] * z[27];
z[28] = abb[18] * z[41];
z[29] = abb[61] * z[28];
z[0] = 3 * z[0] + z[1] + z[5] + z[6] + z[9] + z[13] + z[17] + z[18] + z[23] + z[24] + (T(3) / T(2)) * z[29] + z[45];
z[1] = abb[47] + z[16];
z[5] = 2 * abb[42] + z[1] + z[14] + -z[53] + -z[62];
z[5] = abb[0] * z[5];
z[6] = -z[31] + -z[32] + -z[73];
z[6] = abb[4] * z[6];
z[3] = -z[3] + -z[88];
z[3] = abb[7] * z[3];
z[9] = abb[3] * z[21];
z[3] = z[3] + z[5] + z[6] + z[9] + 3 * z[19] + -z[40] + z[71] + -z[81];
z[3] = abb[28] * z[3];
z[5] = 4 * abb[53] + abb[45] * (T(13) / T(4)) + -z[47];
z[6] = -6 * abb[51] + abb[46] * (T(29) / T(4)) + z[5] + z[26] + -z[89];
z[6] = abb[4] * z[6];
z[9] = abb[48] * (T(9) / T(4)) + -z[15] + z[58];
z[13] = abb[47] * (T(-7) / T(4)) + z[9] + -z[20] + -z[78];
z[13] = abb[7] * z[13];
z[14] = z[22] + z[94];
z[15] = z[77] + z[87];
z[16] = 3 * z[37];
z[6] = z[6] + z[13] + (T(-9) / T(4)) * z[14] + z[15] + z[16] + z[66] + -z[69] + -z[74];
z[6] = abb[31] * z[6];
z[1] = 4 * abb[45] + z[1] + -z[46] + -z[75];
z[1] = z[1] * z[91];
z[1] = z[1] + z[15] + z[48] + z[71] + z[84] + z[85] + -z[93];
z[1] = abb[30] * z[1];
z[9] = abb[46] * (T(-7) / T(4)) + z[9] + z[36] + -z[72];
z[9] = abb[4] * z[9];
z[5] = -6 * abb[52] + abb[47] * (T(29) / T(4)) + z[5] + z[11] + -z[76];
z[5] = abb[7] * z[5];
z[5] = z[5] + z[9] + -z[16] + z[38] + z[77] + -z[79] + -z[92];
z[5] = abb[32] * z[5];
z[9] = z[25] * z[41];
z[7] = abb[28] + -abb[33] + abb[31] * (T(-1) / T(2)) + z[7];
z[7] = z[7] * z[9];
z[8] = z[8] * z[27];
z[11] = abb[30] + -abb[32];
z[11] = z[8] * z[11];
z[1] = z[1] + z[3] + z[5] + z[6] + z[7] + z[10] + z[11] + z[80];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[37] * z[2];
z[3] = abb[39] * z[55];
z[5] = z[39] + z[44] + z[60];
z[5] = abb[36] * z[5];
z[3] = z[3] + z[5];
z[5] = abb[40] * z[30];
z[6] = -z[12] + z[28];
z[6] = abb[41] * z[6];
z[4] = abb[38] * z[4];
z[7] = (T(3) / T(2)) * z[43];
z[10] = abb[36] + abb[39] + abb[40];
z[7] = z[7] * z[10] * z[33];
z[9] = z[9] + -z[82];
z[9] = abb[36] * z[9];
z[8] = abb[39] * z[8];
z[1] = z[1] + z[2] + 3 * z[3] + z[4] + z[5] + (T(3) / T(2)) * z[6] + z[7] + z[8] + z[9];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_347_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("3.162273066096953499292856997926669629182883935648798084600840359"),stof<T>("-14.633300484517895006119187001685607389125752937927857609262389682")}, std::complex<T>{stof<T>("23.879381808358947358200499996971327984902276670437873584039445088"),stof<T>("-4.964525490228577077160159552622320638799579908595529274791218172")}, std::complex<T>{stof<T>("23.879381808358947358200499996971327984902276670437873584039445088"),stof<T>("-4.964525490228577077160159552622320638799579908595529274791218172")}, std::complex<T>{stof<T>("-7.561740476449930820768828760520971509047809643303704910743524227"),stof<T>("-39.662712088828239133034886373294235944962432921291617174748218184")}, std::complex<T>{stof<T>("-2.2302070825634306876494695339139159103281248486824463008880886316"),stof<T>("-8.1334732170627854074240357331830586106145669122790921519215021209")}, std::complex<T>{stof<T>("-2.2302070825634306876494695339139159103281248486824463008880886316"),stof<T>("-8.1334732170627854074240357331830586106145669122790921519215021209")}, std::complex<T>{stof<T>("52.280124536618640145522906367004017731252127027950625545488560833"),stof<T>("-31.691170685552146663778249547492250169229078027294164725907035726")}, std::complex<T>{stof<T>("-27.041654874455900857493356994897997614085160606086671668640285447"),stof<T>("19.597825974746472083279346554307928027925332846523386884053607854")}, std::complex<T>{stof<T>("-27.041654874455900857493356994897997614085160606086671668640285447"),stof<T>("19.597825974746472083279346554307928027925332846523386884053607854")}, std::complex<T>{stof<T>("-18.547848414472447225081140770364272386182591875816614974184009492"),stof<T>("36.493764361994030802771010192733497973147445917608054297617934235")}, std::complex<T>{stof<T>("-18.547848414472447225081140770364272386182591875816614974184009492"),stof<T>("36.493764361994030802771010192733497973147445917608054297617934235")}, std::complex<T>{stof<T>("-4.460414165126861375298939067827831820656249697364892601776177263"),stof<T>("-16.266946434125570814848071466366117221229133824558184303843004242")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_347_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_347_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("4.239758373616595838341639494337243101699632920623711066618063497"),stof<T>("100.330287989232514479659749259571264625722024222815105903458548292")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,62> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k)};

                    
            return f_4_347_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_347_DLogXconstant_part(base_point<T>, kend);
	value += f_4_347_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_347_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_347_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_347_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_347_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_347_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_347_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
