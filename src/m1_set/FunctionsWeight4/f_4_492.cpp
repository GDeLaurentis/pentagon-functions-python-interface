/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_492.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_492_abbreviated (const std::array<T,66>& abb) {
T z[145];
z[0] = 2 * abb[55];
z[1] = 2 * abb[53];
z[2] = z[0] + -z[1];
z[3] = abb[46] * (T(3) / T(2));
z[4] = abb[51] * (T(1) / T(2));
z[5] = abb[56] * (T(1) / T(2));
z[6] = -abb[49] + abb[54];
z[7] = -abb[47] + abb[48] + -abb[57] + z[2] + z[3] + z[4] + z[5] + -z[6];
z[7] = abb[0] * z[7];
z[8] = 3 * abb[57];
z[9] = 2 * abb[49];
z[10] = abb[54] + z[9];
z[11] = z[8] + -z[10];
z[12] = -z[0] + z[11];
z[13] = abb[48] * (T(1) / T(2));
z[14] = abb[50] * (T(1) / T(2));
z[15] = z[13] + -z[14];
z[16] = -abb[51] + abb[56];
z[17] = -abb[47] + z[16];
z[18] = -abb[52] + z[1] + z[12] + -z[15] + z[17];
z[18] = abb[10] * z[18];
z[19] = abb[49] * (T(1) / T(2));
z[20] = abb[54] * (T(1) / T(2));
z[21] = z[19] + z[20];
z[22] = -abb[52] + z[5];
z[15] = abb[55] + z[4] + z[15] + -z[21] + z[22];
z[15] = abb[7] * z[15];
z[23] = -abb[46] + abb[55];
z[24] = -abb[50] + z[23];
z[25] = abb[9] * z[24];
z[26] = z[15] + -z[25];
z[27] = 2 * abb[57];
z[28] = 2 * abb[47];
z[29] = z[27] + -z[28];
z[30] = abb[54] + -abb[56];
z[31] = z[29] + -z[30];
z[32] = -abb[52] + abb[48] * (T(3) / T(2));
z[33] = -z[14] + (T(1) / T(2)) * z[32];
z[34] = abb[53] * (T(3) / T(4));
z[35] = -abb[46] + z[9];
z[36] = z[31] + z[33] + z[34] + -z[35];
z[36] = abb[4] * z[36];
z[37] = abb[46] + z[20];
z[38] = abb[48] + abb[51] * (T(3) / T(2)) + -z[5];
z[39] = abb[49] * (T(7) / T(2)) + -z[8] + z[37] + -z[38];
z[40] = abb[5] * z[39];
z[41] = abb[49] + abb[54];
z[42] = -z[27] + z[41];
z[43] = z[16] + -z[28];
z[44] = z[42] + -z[43];
z[45] = abb[16] * z[44];
z[46] = abb[60] + abb[61];
z[47] = abb[27] * z[46];
z[45] = z[45] + -z[47];
z[48] = abb[25] * z[46];
z[49] = abb[23] * z[46];
z[50] = z[48] + z[49];
z[51] = z[45] + -z[50];
z[10] = z[10] + -z[27];
z[52] = abb[46] * (T(1) / T(2));
z[38] = -z[10] + z[38] + z[52];
z[38] = abb[8] * z[38];
z[53] = -abb[58] + abb[59];
z[54] = abb[21] * z[53];
z[55] = abb[26] * z[46];
z[54] = z[54] + z[55];
z[56] = -abb[19] * z[53];
z[57] = -z[54] + -z[56];
z[58] = abb[49] + -abb[51];
z[33] = -z[33] + z[58];
z[59] = 2 * abb[46];
z[60] = 3 * abb[55] + abb[53] * (T(-9) / T(4)) + -z[33] + -z[59];
z[60] = abb[14] * z[60];
z[61] = -abb[53] + z[23];
z[61] = abb[1] * z[61];
z[62] = abb[46] + abb[49];
z[63] = -abb[54] + z[62];
z[64] = abb[15] * z[63];
z[65] = (T(1) / T(2)) * z[64];
z[66] = -abb[57] + z[62];
z[67] = abb[11] * z[66];
z[7] = z[7] + z[18] + -z[26] + z[36] + z[38] + z[40] + z[51] + (T(1) / T(2)) * z[57] + z[60] + -3 * z[61] + -z[65] + -z[67];
z[7] = prod_pow(abb[36], 2) * z[7];
z[36] = abb[48] + abb[51];
z[10] = z[10] + -z[36];
z[10] = abb[18] * z[10];
z[15] = -z[10] + z[15];
z[38] = 3 * abb[49];
z[57] = -abb[46] + -7 * abb[47] + abb[51] + abb[56] + z[32] + -z[38];
z[60] = -abb[57] + z[14];
z[68] = 4 * abb[55];
z[57] = abb[53] * (T(-13) / T(4)) + -z[20] + (T(1) / T(2)) * z[57] + -z[60] + z[68];
z[57] = abb[4] * z[57];
z[69] = 2 * abb[50] + -5 * abb[55];
z[70] = 3 * abb[48];
z[71] = -z[1] + -z[69] + z[70];
z[72] = 2 * abb[52];
z[73] = z[71] + -z[72];
z[74] = 4 * abb[57];
z[75] = 4 * abb[51];
z[76] = z[74] + -z[75];
z[77] = abb[46] + z[28] + z[73] + -z[76];
z[77] = abb[16] * z[77];
z[39] = -abb[8] * z[39];
z[78] = z[25] + -z[50];
z[79] = -abb[52] + z[52];
z[80] = z[13] + z[79];
z[81] = abb[55] * (T(1) / T(2));
z[82] = -abb[47] + z[81];
z[83] = -abb[51] + -z[80] + z[82];
z[83] = abb[0] * z[83];
z[84] = -abb[47] + (T(1) / T(2)) * z[16];
z[85] = abb[57] + z[84];
z[86] = -z[21] + z[85];
z[86] = abb[3] * z[86];
z[33] = -abb[55] + abb[53] * (T(1) / T(4)) + z[33];
z[33] = abb[14] * z[33];
z[80] = -abb[56] + abb[55] * (T(-3) / T(2)) + z[27] + -z[80];
z[80] = abb[2] * z[80];
z[18] = -z[15] + z[18] + 3 * z[33] + z[39] + z[57] + z[77] + (T(1) / T(2)) * z[78] + z[80] + z[83] + z[86];
z[18] = abb[35] * z[18];
z[33] = 2 * abb[54];
z[39] = z[33] + z[72];
z[57] = 2 * abb[51];
z[78] = abb[48] + z[57];
z[80] = z[28] + z[78];
z[83] = 2 * abb[56];
z[86] = z[80] + -z[83];
z[87] = -abb[49] + abb[53];
z[88] = 6 * abb[57];
z[68] = abb[50] + -z[39] + -z[68] + -z[86] + 4 * z[87] + z[88];
z[68] = abb[10] * z[68];
z[87] = 7 * abb[49] + -z[88];
z[89] = 3 * abb[51];
z[90] = 2 * abb[48] + -abb[56] + z[89];
z[91] = abb[54] + z[59] + z[87] + -z[90];
z[92] = abb[5] * z[91];
z[93] = z[68] + z[92];
z[94] = 5 * abb[51];
z[95] = 4 * abb[47];
z[96] = abb[46] + z[41];
z[73] = -abb[56] + z[73] + -z[88] + z[94] + z[95] + z[96];
z[73] = abb[16] * z[73];
z[97] = z[36] + -z[41];
z[98] = abb[50] + z[72];
z[99] = abb[56] + z[0] + z[97] + -z[98];
z[99] = abb[7] * z[99];
z[73] = -z[47] + -z[50] + z[73] + -z[99];
z[30] = -abb[51] + z[30] + -z[32];
z[87] = abb[53] * (T(1) / T(2)) + z[24] + -z[30] + -z[87] + -z[95];
z[87] = abb[4] * z[87];
z[91] = abb[8] * z[91];
z[23] = z[23] + z[72];
z[100] = -z[1] + z[23];
z[101] = z[9] + -z[80] + z[100];
z[102] = abb[0] * z[101];
z[32] = z[32] + z[57];
z[57] = -abb[50] + -z[9] + z[32];
z[103] = abb[53] * (T(3) / T(2));
z[104] = z[59] + z[103];
z[105] = z[57] + z[104];
z[105] = abb[14] * z[105];
z[87] = z[73] + z[87] + -z[91] + z[93] + z[102] + -z[105];
z[91] = -abb[36] * z[87];
z[102] = abb[46] + z[42];
z[106] = abb[3] * z[102];
z[107] = 2 * z[10];
z[106] = z[106] + -z[107];
z[108] = abb[20] * z[53];
z[109] = z[106] + -z[108];
z[110] = abb[8] * z[102];
z[111] = z[50] + z[56];
z[110] = z[110] + z[111];
z[112] = -z[27] + z[38];
z[113] = abb[54] + z[112];
z[114] = -z[1] + z[113];
z[115] = z[24] + z[43] + -z[114];
z[115] = abb[4] * z[115];
z[80] = z[23] + -z[80];
z[80] = abb[0] * z[80];
z[116] = abb[50] + abb[53];
z[12] = z[12] + z[116];
z[117] = 2 * abb[10];
z[12] = z[12] * z[117];
z[77] = z[12] + z[77] + z[80] + -z[99] + -z[109] + -z[110] + z[115];
z[77] = abb[31] * z[77];
z[80] = z[92] + z[109];
z[115] = abb[46] + abb[47];
z[118] = -z[9] + z[27] + -z[115];
z[118] = abb[4] * z[118];
z[119] = 4 * abb[8];
z[66] = -z[66] * z[119];
z[120] = z[28] + z[90];
z[35] = z[35] + -z[120];
z[121] = abb[0] * z[35];
z[66] = z[45] + z[66] + z[80] + 2 * z[118] + z[121];
z[66] = abb[34] * z[66];
z[110] = -z[45] + z[110];
z[118] = z[6] + z[43];
z[121] = abb[6] * z[118];
z[122] = abb[24] * z[46];
z[55] = z[55] + z[122];
z[121] = -z[55] + z[121];
z[122] = abb[54] + -abb[57];
z[123] = 2 * abb[2];
z[124] = z[122] * z[123];
z[124] = -z[121] + z[124];
z[125] = abb[46] + z[43];
z[126] = -abb[0] * z[125];
z[126] = z[110] + z[124] + z[126];
z[126] = abb[33] * z[126];
z[18] = z[18] + z[66] + z[77] + z[91] + z[126];
z[18] = abb[35] * z[18];
z[66] = abb[28] * z[46];
z[77] = -z[48] + -z[55] + z[56] + z[66];
z[91] = z[19] + -z[20];
z[84] = z[84] + -z[91];
z[126] = abb[6] * z[84];
z[49] = (T(1) / T(2)) * z[49];
z[77] = -z[49] + (T(1) / T(2)) * z[77] + z[126];
z[127] = z[21] + z[52];
z[128] = z[28] + z[36];
z[129] = z[127] + -z[128];
z[130] = abb[17] * z[129];
z[131] = z[10] + -z[77] + -z[130];
z[132] = abb[48] * (T(7) / T(2));
z[22] = abb[57] + abb[49] * (T(-5) / T(2)) + abb[51] * (T(7) / T(2)) + abb[46] * (T(17) / T(4)) + z[22] + -z[28] + z[132];
z[133] = abb[54] * (T(1) / T(6));
z[134] = abb[55] * (T(1) / T(12));
z[22] = (T(1) / T(3)) * z[22] + -z[34] + z[133] + -z[134];
z[22] = abb[4] * z[22];
z[34] = 4 * abb[56];
z[135] = z[34] + -z[75];
z[13] = abb[52] + z[13];
z[136] = abb[53] + (T(-1) / T(2)) * z[13] + -z[135];
z[137] = abb[47] * (T(3) / T(2));
z[138] = abb[54] * (T(1) / T(3));
z[134] = abb[57] * (T(-2) / T(3)) + abb[46] * (T(1) / T(12)) + z[134] + (T(1) / T(3)) * z[136] + z[137] + z[138];
z[134] = abb[2] * z[134];
z[136] = abb[48] + z[72];
z[9] = abb[47] * (T(-31) / T(2)) + -z[9] + z[135] + -z[136];
z[135] = abb[55] * (T(-19) / T(6)) + abb[50] * (T(2) / T(3));
z[9] = abb[57] + abb[53] * (T(-13) / T(6)) + (T(1) / T(3)) * z[9] + -z[135] + -z[138];
z[9] = abb[10] * z[9];
z[138] = abb[48] * (T(5) / T(2));
z[139] = abb[47] + abb[52] + -z[138];
z[58] = abb[53] * (T(7) / T(3)) + abb[46] * (T(11) / T(6)) + z[58] + z[135] + (T(1) / T(3)) * z[139];
z[58] = abb[14] * z[58];
z[135] = abb[22] * z[53];
z[139] = z[108] + z[135];
z[140] = abb[47] + -abb[56];
z[133] = abb[48] + abb[49] * (T(-5) / T(6)) + abb[57] * (T(-1) / T(3)) + abb[51] * (T(7) / T(3)) + abb[46] * (T(7) / T(6)) + z[133] + (T(4) / T(3)) * z[140];
z[133] = abb[8] * z[133];
z[140] = abb[52] + abb[48] * (T(17) / T(2));
z[75] = abb[47] * (T(17) / T(2)) + z[75] + (T(1) / T(2)) * z[140];
z[75] = abb[55] * (T(-13) / T(4)) + abb[46] * (T(7) / T(4)) + abb[53] * (T(11) / T(6)) + (T(1) / T(3)) * z[75];
z[75] = abb[0] * z[75];
z[140] = (T(1) / T(2)) * z[62];
z[141] = abb[57] + z[140];
z[141] = -z[20] + (T(1) / T(3)) * z[141];
z[141] = abb[3] * z[141];
z[142] = 2 * z[17];
z[143] = abb[13] * z[142];
z[9] = z[9] + z[22] + z[58] + (T(13) / T(4)) * z[61] + z[75] + (T(1) / T(3)) * z[131] + z[133] + z[134] + (T(-1) / T(6)) * z[139] + z[141] + z[143];
z[9] = prod_pow(m1_set::bc<T>[0], 2) * z[9];
z[22] = z[32] + -z[41] + z[52] + -z[60] + z[137];
z[22] = abb[4] * z[22];
z[58] = 5 * abb[47];
z[60] = abb[55] + z[11] + -z[58] + -z[72] + -z[78] + z[83];
z[60] = abb[10] * z[60];
z[75] = z[130] + (T(1) / T(2)) * z[135];
z[130] = -z[45] + (T(1) / T(2)) * z[66] + z[75];
z[79] = 12 * abb[47] + 9 * abb[51] + abb[54] + -5 * abb[56] + -z[74] + z[79] + -z[81] + z[132];
z[79] = abb[2] * z[79];
z[55] = z[25] + z[55] + z[108];
z[4] = z[4] + -z[5];
z[5] = -z[4] + z[13] + -z[81];
z[5] = abb[0] * z[5];
z[81] = -abb[56] + z[78];
z[131] = 3 * abb[47];
z[132] = z[81] + z[131];
z[133] = -abb[57] + z[132];
z[134] = abb[8] * z[133];
z[85] = -abb[49] + -z[52] + z[85];
z[85] = abb[3] * z[85];
z[5] = z[5] + -z[15] + z[22] + (T(1) / T(2)) * z[55] + z[60] + z[79] + z[85] + -z[126] + z[130] + 3 * z[134] + z[143];
z[5] = abb[32] * z[5];
z[15] = -abb[46] + z[41];
z[22] = -abb[56] + z[72];
z[55] = z[15] + z[22] + z[27] + -z[71] + -z[89];
z[55] = abb[16] * z[55];
z[60] = abb[3] * z[44];
z[71] = -z[25] + z[99];
z[60] = z[60] + z[71] + -z[107];
z[72] = -z[16] + z[131];
z[79] = abb[50] + -abb[55] + z[72] + z[114];
z[79] = abb[4] * z[79];
z[85] = -z[27] + z[33];
z[114] = abb[48] + z[83];
z[23] = -z[23] + -z[85] + z[114];
z[23] = abb[2] * z[23];
z[12] = -z[12] + z[23] + -z[47] + z[55] + z[60] + z[79] + z[121];
z[12] = abb[35] * z[12];
z[23] = -z[11] + z[72];
z[23] = z[23] * z[117];
z[55] = -z[96] + 2 * z[128];
z[55] = abb[17] * z[55];
z[55] = z[55] + -z[66] + -z[135];
z[72] = z[23] + z[55];
z[79] = -abb[46] + z[16];
z[126] = abb[0] * z[79];
z[45] = -z[45] + z[126];
z[90] = -z[90] + z[113];
z[113] = -z[28] + z[90];
z[126] = abb[4] * z[113];
z[135] = abb[2] * z[133];
z[126] = z[45] + -z[72] + -z[126] + 4 * z[134] + 6 * z[135] + z[143];
z[106] = -z[106] + z[126];
z[106] = abb[34] * z[106];
z[109] = z[109] + -z[126];
z[109] = abb[31] * z[109];
z[89] = abb[48] + -z[83] + z[89] + z[95];
z[126] = -abb[57] + z[89];
z[126] = z[123] * z[126];
z[137] = abb[4] * z[118];
z[139] = abb[3] * z[63];
z[137] = z[55] + z[137] + z[139];
z[45] = z[45] + z[126] + 2 * z[134] + -z[137];
z[126] = -abb[33] * z[45];
z[134] = abb[34] * z[53];
z[141] = abb[33] * z[53];
z[144] = z[134] + -z[141];
z[144] = abb[20] * z[144];
z[5] = z[5] + z[12] + z[106] + z[109] + z[126] + z[144];
z[5] = abb[32] * z[5];
z[12] = -3 * abb[52] + abb[55] * (T(7) / T(2)) + z[3] + -z[76] + -z[116] + z[131] + z[138];
z[12] = abb[16] * z[12];
z[30] = abb[50] + z[30];
z[76] = z[52] + z[82];
z[82] = abb[53] + -z[30] + z[76] + -z[112];
z[82] = abb[4] * z[82];
z[101] = abb[14] * z[101];
z[106] = z[86] + z[100];
z[106] = abb[2] * z[106];
z[109] = -abb[8] * z[113];
z[76] = -abb[53] + z[13] + z[76];
z[76] = abb[0] * z[76];
z[12] = z[12] + -z[50] + -z[60] + z[68] + z[76] + z[82] + z[101] + z[106] + z[109];
z[12] = abb[40] * z[12];
z[60] = -8 * abb[47] + -z[70] + z[74] + z[83] + -z[94] + -z[127];
z[60] = abb[8] * z[60];
z[49] = z[49] + z[65];
z[68] = z[49] + z[130];
z[70] = z[43] + z[52];
z[74] = abb[54] * (T(3) / T(2)) + z[19] + -z[27] + -z[70];
z[74] = abb[3] * z[74];
z[48] = z[48] + z[54];
z[76] = z[48] + 3 * z[56];
z[35] = abb[4] * z[35];
z[82] = -abb[54] + z[29] + -z[36];
z[82] = abb[0] * z[82];
z[83] = 2 * z[67];
z[23] = z[23] + z[35] + z[60] + -z[68] + z[74] + (T(-1) / T(2)) * z[76] + z[82] + z[83] + -z[107] + -4 * z[135];
z[23] = abb[34] * z[23];
z[35] = -abb[57] + abb[49] * (T(3) / T(2));
z[4] = z[4] + z[13] + z[14] + -z[20] + -z[35];
z[4] = abb[4] * z[4];
z[13] = z[123] * z[133];
z[10] = -z[10] + -z[13] + z[65];
z[14] = -z[54] + z[66];
z[60] = z[81] + z[95];
z[65] = -abb[46] + -abb[57] + z[60];
z[65] = abb[8] * z[65];
z[74] = -abb[55] + z[136];
z[11] = -abb[47] + z[11] + z[74];
z[11] = abb[10] * z[11];
z[82] = abb[55] + z[17];
z[94] = -abb[54] + z[82];
z[94] = abb[0] * z[94];
z[106] = -abb[3] * z[122];
z[4] = z[4] + -z[10] + z[11] + (T(1) / T(2)) * z[14] + z[26] + z[65] + z[75] + z[94] + z[106];
z[4] = abb[31] * z[4];
z[11] = z[3] + z[43] + z[91];
z[11] = abb[3] * z[11];
z[13] = z[13] + z[143];
z[11] = z[11] + -z[13];
z[14] = -z[27] + z[89] + z[127];
z[14] = abb[8] * z[14];
z[26] = z[48] + -z[56];
z[48] = -abb[4] * z[125];
z[65] = z[36] + z[59];
z[94] = abb[54] + -z[65];
z[94] = abb[0] * z[94];
z[14] = -z[11] + z[14] + (T(1) / T(2)) * z[26] + z[48] + z[68] + z[94];
z[14] = abb[33] * z[14];
z[26] = z[85] + -z[125];
z[26] = abb[0] * z[26];
z[44] = abb[4] * z[44];
z[48] = z[54] + z[64];
z[64] = z[48] + -z[83];
z[26] = z[26] + z[44] + z[64] + z[110];
z[26] = abb[36] * z[26];
z[44] = z[134] + 3 * z[141];
z[44] = abb[20] * z[44];
z[4] = z[4] + z[14] + z[23] + z[26] + (T(1) / T(2)) * z[44];
z[4] = abb[31] * z[4];
z[14] = -z[89] + z[127];
z[14] = abb[8] * z[14];
z[23] = -z[66] + z[76];
z[44] = abb[4] * z[102];
z[31] = abb[46] + -z[31] + z[78];
z[31] = abb[0] * z[31];
z[11] = z[11] + z[14] + (T(1) / T(2)) * z[23] + z[31] + z[44] + z[49] + -z[75] + -z[83];
z[11] = abb[34] * z[11];
z[14] = abb[47] + z[36];
z[23] = z[14] + -z[21] + z[52];
z[23] = abb[8] * z[23];
z[31] = -abb[4] * z[84];
z[44] = -abb[54] + z[128];
z[49] = abb[2] * z[44];
z[52] = abb[46] + -abb[47];
z[66] = -abb[0] * z[52];
z[23] = z[23] + z[31] + z[49] + z[66] + z[75] + z[77] + (T(-1) / T(2)) * z[139] + -z[143];
z[23] = abb[33] * z[23];
z[11] = z[11] + z[23] + -z[26];
z[11] = abb[33] * z[11];
z[23] = 16 * abb[47] + 5 * abb[48] + 10 * abb[51] + -z[34] + -z[88] + -z[100];
z[23] = abb[2] * z[23];
z[31] = -5 * abb[53] + -z[58] + -z[69] + 2 * z[136];
z[31] = abb[10] * z[31];
z[34] = z[58] + z[81];
z[34] = abb[46] + -abb[54] + -2 * z[34] + -z[38] + z[88];
z[34] = abb[8] * z[34];
z[1] = -z[1] + z[86] + z[98];
z[49] = abb[4] * z[1];
z[58] = abb[47] + abb[53] + -abb[55] + z[79];
z[58] = abb[0] * z[58];
z[23] = -z[23] + -z[31] + z[34] + -z[49] + z[55] + -z[56] + -z[58] + z[73] + z[101];
z[31] = abb[62] * z[23];
z[34] = -z[54] + -z[111];
z[35] = abb[46] * (T(-5) / T(2)) + -z[20] + z[35] + z[95];
z[35] = abb[8] * z[35];
z[8] = z[8] + -z[41] + -z[60];
z[8] = abb[0] * z[8];
z[21] = abb[57] + -z[21] + z[70];
z[21] = abb[3] * z[21];
z[41] = -abb[49] + z[16];
z[37] = abb[57] + -z[37] + (T(1) / T(2)) * z[41];
z[37] = abb[4] * z[37];
z[8] = z[8] + -z[10] + z[21] + (T(1) / T(2)) * z[34] + z[35] + z[37] + -z[40] + 3 * z[67];
z[8] = abb[34] * z[8];
z[8] = z[8] + z[26];
z[8] = abb[34] * z[8];
z[10] = -abb[64] * z[87];
z[16] = -abb[54] + z[16] + z[59] + z[112];
z[16] = abb[4] * z[16];
z[21] = -abb[54] + -z[27] + 3 * z[62];
z[21] = abb[8] * z[21];
z[26] = abb[46] + z[97];
z[34] = abb[57] + z[26];
z[35] = 2 * abb[0];
z[34] = z[34] * z[35];
z[16] = z[16] + z[21] + z[34] + -z[64] + -z[80] + -z[111];
z[16] = abb[38] * z[16];
z[21] = -abb[55] + z[22];
z[22] = z[21] + -z[26];
z[22] = abb[16] * z[22];
z[22] = z[22] + -z[47];
z[6] = -abb[50] + -z[6] + z[82];
z[6] = abb[4] * z[6];
z[26] = abb[3] * z[118];
z[34] = -abb[46] + -abb[55] + z[39] + -z[114];
z[34] = abb[2] * z[34];
z[6] = z[6] + -z[22] + z[26] + z[34] + -z[71] + -z[121];
z[6] = abb[39] * z[6];
z[3] = z[3] + -z[19] + z[36];
z[3] = abb[19] * z[3];
z[19] = abb[22] * z[129];
z[26] = -abb[19] + -abb[21];
z[20] = z[20] * z[26];
z[26] = abb[29] * z[46];
z[34] = abb[21] * z[140];
z[37] = (T(1) / T(2)) * z[53];
z[39] = abb[17] * z[37];
z[3] = z[3] + z[19] + z[20] + (T(1) / T(2)) * z[26] + z[34] + z[39];
z[3] = abb[41] * z[3];
z[19] = -z[121] + z[137];
z[20] = -abb[54] + z[89];
z[20] = z[20] * z[123];
z[26] = -z[96] + 2 * z[132];
z[26] = abb[8] * z[26];
z[34] = abb[0] * z[142];
z[20] = -z[19] + z[20] + z[26] + z[34] + -z[111];
z[26] = -abb[63] * z[20];
z[25] = 2 * z[25] + -z[99];
z[21] = -z[21] + -z[33] + z[65];
z[21] = abb[0] * z[21];
z[21] = z[21] + z[22] + -z[25] + -z[48];
z[21] = abb[37] * z[21];
z[22] = z[70] + -z[91];
z[22] = abb[41] * z[22];
z[33] = -abb[37] + -abb[63];
z[33] = z[33] * z[53];
z[34] = -3 * z[134] + z[141];
z[34] = abb[33] * z[34];
z[37] = -prod_pow(abb[34], 2) * z[37];
z[22] = z[22] + z[33] + (T(1) / T(2)) * z[34] + z[37];
z[22] = abb[20] * z[22];
z[33] = abb[8] + -abb[15];
z[33] = -2 * abb[30] + (T(-1) / T(2)) * z[33];
z[34] = abb[41] * z[53];
z[33] = z[33] * z[34];
z[24] = -abb[37] * z[24];
z[24] = z[24] + -z[34];
z[24] = abb[4] * z[24];
z[37] = abb[37] * z[63];
z[34] = (T(-1) / T(2)) * z[34] + z[37];
z[34] = abb[3] * z[34];
z[3] = abb[65] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[16] + z[18] + z[21] + z[22] + z[24] + z[26] + z[31] + z[33] + z[34];
z[4] = -z[28] + z[59] + -z[98] + z[114];
z[4] = abb[4] * z[4];
z[5] = abb[3] * z[125];
z[5] = 2 * z[5] + -z[13];
z[6] = abb[46] + -z[42] + 2 * z[43];
z[6] = abb[8] * z[6];
z[7] = z[43] + -z[74];
z[7] = z[7] * z[117];
z[0] = 3 * abb[46] + -z[0] + z[120];
z[0] = abb[0] * z[0];
z[0] = z[0] + z[4] + z[5] + z[6] + z[7] + z[25] + z[51] + z[55] + z[56] + -2 * z[108];
z[0] = abb[31] * z[0];
z[4] = -z[29] + z[30] + z[38] + -z[104];
z[4] = abb[4] * z[4];
z[6] = -z[59] + z[90];
z[7] = abb[8] * z[6];
z[7] = z[7] + -z[51];
z[8] = -z[27] + z[36];
z[2] = -z[2] + -z[8] + -z[62];
z[2] = z[2] * z[35];
z[9] = 4 * abb[46] + -6 * abb[55] + abb[53] * (T(9) / T(2)) + -z[57];
z[9] = abb[14] * z[9];
z[10] = 4 * z[67];
z[2] = z[2] + z[4] + z[7] + z[9] + z[10] + -z[25] + 6 * z[61] + -z[93];
z[2] = abb[36] * z[2];
z[4] = -abb[49] + z[81] + z[115];
z[4] = z[4] * z[119];
z[6] = -abb[4] * z[6];
z[8] = abb[49] + z[8] + z[131];
z[8] = z[8] * z[35];
z[4] = z[4] + -z[5] + z[6] + z[8] + -z[10] + -z[72] + z[92];
z[4] = abb[34] * z[4];
z[5] = -2 * z[14] + z[15];
z[5] = abb[8] * z[5];
z[6] = z[35] * z[52];
z[8] = -z[44] * z[123];
z[9] = abb[13] * z[17];
z[5] = z[5] + z[6] + z[8] + 4 * z[9] + z[19] + z[50] + -z[56];
z[5] = abb[33] * z[5];
z[6] = z[32] + z[59] + -z[103];
z[6] = abb[4] * z[6];
z[1] = -abb[10] * z[1];
z[8] = abb[46] + -abb[53] + z[36];
z[8] = z[8] * z[35];
z[1] = z[1] + z[6] + -z[7] + z[8] + -z[105] + -z[124];
z[1] = abb[35] * z[1];
z[6] = z[45] + z[108];
z[6] = abb[32] * z[6];
z[7] = 2 * z[134] + -z[141];
z[7] = abb[20] * z[7];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + z[6] + z[7];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[42] * z[23];
z[2] = -abb[44] * z[87];
z[4] = -z[20] + -z[108];
z[4] = abb[43] * z[4];
z[0] = abb[45] + z[0] + z[1] + z[2] + z[4];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_492_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("16.372266051540969066830333934281105410733375500009770161324713235"),stof<T>("3.57017708996602100135888901356756269003146229522877873595348349")}, std::complex<T>{stof<T>("12.982220982250092558234125945450721727545295183176181940096242155"),stof<T>("-57.418824720771659684066586759480894575218007089727891377806418545")}, std::complex<T>{stof<T>("14.757972400465065999878037491542061958025935605256638484439830907"),stof<T>("-30.16615432059516207508232931845780171040920975653935859366936456")}, std::complex<T>{stof<T>("1.6765441412692805253217594784683437189017549806613713446496062926"),stof<T>("0.5433285895673986287932689419248425510551738554662535346872674323")}, std::complex<T>{stof<T>("4.0112116105539524936223891773353189346808160419222997293234304071"),stof<T>("2.6939930366061245724129316045958625281073645984417128132573124383")}, std::complex<T>{stof<T>("24.333161729871255696974344084144428200258035542375896444373719497"),stof<T>("-30.877626133123249598603112061259836287424336030252076273927101585")}, std::complex<T>{stof<T>("-13.436557943712315213858885826101965082466130259222992954387179377"),stof<T>("-5.81184027331700236285387486193389311149684600980470557177901155")}, std::complex<T>{stof<T>("-15.424402848368493090732070351758325673587871487030253901365631714"),stof<T>("3.957811196739249789771543087283601033484462538384355877798951219")}, std::complex<T>{stof<T>("7.8096660889735223330593301290231286639647705593830221073502749734"),stof<T>("5.2177975875694346952655363586154087649347047766373697958117282213")}, std::complex<T>{stof<T>("12.793687045791785054796812958743887296242639410114092783440208796"),stof<T>("15.41557936459251065851041702305958244255067838550080234422110859")}, std::complex<T>{stof<T>("-2.8569103575500320901668636795513837009990348075077614827402989009"),stof<T>("3.6173919491865887049477201737689811327635492786150704661472428007")}, std::complex<T>{stof<T>("-25.624678438685113315946258883354414898228045110307170547837719663"),stof<T>("2.337644545520443730328632302978504390276825602108864227892996658")}, std::complex<T>{stof<T>("-6.0424807852311269723762033065345449166773770378864901541332721254"),stof<T>("12.2909021715073598124216814911235666775995962973061920796277177388")}, std::complex<T>{stof<T>("6.0424807852311269723762033065345449166773770378864901541332721254"),stof<T>("-12.2909021715073598124216814911235666775995962973061920796277177388")}, std::complex<T>{stof<T>("-1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("1.5961736222681129079204730221135863214600019276878811243393385392")}, std::complex<T>{stof<T>("-1.3002878523914041670339925228989152670712846103096599149149071746"),stof<T>("1.5961736222681129079204730221135863214600019276878811243393385392")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[91].real()/kbase.W[91].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[128].real()/kbase.W[128].real()), C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_492_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_492_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (v[2] + v[3]) * (-8 + 5 * v[2] + 5 * v[3] + -4 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -4 * v[5])) / prod_pow(tend, 2);
c[1] = ((1 + 2 * m1_set::bc<T>[1]) * (T(1) / T(2)) * (v[2] + v[3])) / tend;


		return (abb[47] + abb[50] + -abb[55]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[35], 2);
z[1] = -abb[35] + abb[32] * (T(-1) / T(2));
z[1] = abb[32] * z[1];
z[0] = -abb[39] + (T(3) / T(2)) * z[0] + z[1];
z[1] = abb[47] + abb[50] + -abb[55];
return abb[12] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_492_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_492_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-57.867259964157005452358621825374303846590132630008104455557484842"),stof<T>("-23.853540206946938801175876561387870643707051167402509422590882057")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,66> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W15(k,dl), dlog_W18(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W26(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W92(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_19(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_11_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[91].real()/k.W[91].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[128].real()/k.W[128].real()), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_4_re(k), f_2_7_re(k), f_2_11_re(k), T{0}};
abb[45] = SpDLog_f_4_492_W_19_Im(t, path, abb);
abb[65] = SpDLog_f_4_492_W_19_Re(t, path, abb);

                    
            return f_4_492_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_492_DLogXconstant_part(base_point<T>, kend);
	value += f_4_492_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_492_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_492_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_492_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_492_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_492_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_492_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
