/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_569.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_569_abbreviated (const std::array<T,19>& abb) {
T z[21];
z[0] = 2 * abb[16];
z[1] = -abb[13] + abb[15];
z[2] = -3 * z[1];
z[3] = -abb[12] + abb[14];
z[4] = z[0] + z[2] + z[3];
z[4] = abb[1] * z[4];
z[5] = -abb[16] + z[1];
z[6] = 2 * abb[0];
z[5] = z[5] * z[6];
z[6] = abb[16] + z[3];
z[7] = 2 * z[6];
z[8] = -z[1] + z[7];
z[9] = abb[2] + -abb[3];
z[9] = z[8] * z[9];
z[10] = z[1] + -z[3];
z[10] = abb[5] * z[10];
z[4] = z[4] + z[5] + z[9] + -z[10];
z[5] = abb[17] * z[4];
z[9] = (T(-1) / T(2)) * z[1];
z[11] = z[6] + z[9];
z[12] = abb[3] * z[11];
z[13] = -abb[16] + z[3];
z[14] = (T(3) / T(2)) * z[1] + z[13];
z[14] = abb[1] * z[14];
z[15] = abb[0] * z[1];
z[16] = abb[2] * z[1];
z[14] = 2 * z[10] + z[12] + z[14] + -z[15] + -z[16];
z[14] = abb[6] * z[14];
z[15] = abb[3] * z[8];
z[17] = 2 * z[1];
z[18] = abb[0] * z[17];
z[15] = z[15] + -z[18];
z[7] = z[2] + z[7];
z[7] = abb[1] * z[7];
z[18] = 2 * z[16];
z[7] = z[7] + -z[15] + -z[18];
z[7] = abb[7] * z[7];
z[19] = abb[1] * z[3];
z[19] = z[10] + -z[16] + z[19];
z[20] = 2 * abb[8];
z[19] = z[19] * z[20];
z[19] = z[7] + -z[19];
z[14] = z[14] + z[19];
z[14] = abb[6] * z[14];
z[20] = abb[16] * (T(1) / T(2)) + z[1];
z[20] = abb[0] * z[20];
z[12] = -z[12] + z[20];
z[9] = abb[16] * (T(1) / T(3)) + (T(-3) / T(2)) * z[3] + z[9];
z[9] = abb[1] * z[9];
z[20] = -z[2] + (T(-1) / T(3)) * z[6];
z[20] = abb[2] * z[20];
z[9] = z[9] + (T(-11) / T(6)) * z[10] + (T(1) / T(3)) * z[12] + (T(1) / T(2)) * z[20];
z[9] = prod_pow(m1_set::bc<T>[0], 2) * z[9];
z[12] = abb[1] + -abb[3];
z[11] = z[11] * z[12];
z[20] = z[1] + z[3];
z[20] = abb[0] * z[20];
z[11] = z[11] + -z[16] + z[20];
z[11] = abb[8] * z[11];
z[7] = -z[7] + z[11];
z[7] = abb[8] * z[7];
z[8] = z[8] * z[12];
z[0] = z[0] + -z[3];
z[3] = z[0] + z[17];
z[3] = abb[0] * z[3];
z[11] = -z[1] + -z[6];
z[12] = 2 * abb[2];
z[11] = z[11] * z[12];
z[3] = z[3] + z[8] + z[11];
z[3] = abb[9] * z[3];
z[0] = abb[0] * z[0];
z[6] = -z[6] * z[12];
z[1] = -abb[1] * z[1];
z[0] = z[0] + z[1] + z[6];
z[0] = prod_pow(abb[7], 2) * z[0];
z[0] = abb[18] + z[0] + z[3] + z[5] + z[7] + z[9] + z[14];
z[1] = z[2] + -2 * z[13];
z[1] = abb[1] * z[1];
z[1] = z[1] + -4 * z[10] + -z[15] + z[18];
z[1] = abb[6] * z[1];
z[1] = z[1] + -z[19];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[10] * z[4];
z[1] = abb[11] + z[1] + z[2];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_569_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-8.017040131153785690530009467867480332729577558122370229616050761"),stof<T>("-14.108894707401180752782744424272541783465370838208919764819410027")}, std::complex<T>{stof<T>("13.8633717868675531393325047293566737142193596084063127054914224317"),stof<T>("1.18961275727625567966761314631362711587150060242345822559251915")}, std::complex<T>{stof<T>("8.017040131153785690530009467867480332729577558122370229616050761"),stof<T>("14.108894707401180752782744424272541783465370838208919764819410027")}, std::complex<T>{stof<T>("-13.8633717868675531393325047293566737142193596084063127054914224317"),stof<T>("-1.18961275727625567966761314631362711587150060242345822559251915")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_569_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_569_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,4> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (v[1] + v[2] + -v[3] + -v[4]) * (24 + 12 * v[0] + -5 * v[1] + 7 * v[2] + 5 * v[3] + -7 * v[4] + -4 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + -12 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(8)) * (v[1] + v[2] + -v[3] + -v[4]) * (v[1] + v[2] + -v[3] + -v[4] + 2 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]))) / prod_pow(tend, 2);
c[2] = ((-1 + 2 * m1_set::bc<T>[1]) * (T(-1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;
c[3] = ((1 + m1_set::bc<T>[1]) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return -t * abb[14] * c[0] + t * abb[15] * c[1] + -abb[14] * c[2] + abb[12] * (t * c[0] + c[2]) + abb[15] * c[3] + -abb[13] * (t * c[1] + c[3]) + -2 * abb[16] * (t * c[1] + c[3]);
	}
	{
T z[3];
z[0] = prod_pow(abb[8], 2);
z[1] = prod_pow(abb[7], 2);
z[0] = z[0] + -z[1];
z[1] = -abb[9] + z[0];
z[2] = abb[12] + -abb[14];
z[1] = z[1] * z[2];
z[0] = 2 * abb[9] + z[0];
z[2] = abb[13] + -abb[15] + 2 * abb[16];
z[0] = z[0] * z[2];
z[0] = z[0] + z[1];
return abb[4] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_569_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_569_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = T(0);
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,19> abb = {dlog_W4(k,dl), dl[2], dlog_W8(k,dl), dlog_W10(k,dl), dlog_W20(k,dl), dlog_W25(k,dl), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_12_im(k), T{0}, rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), IntDLogL_W_20(t,k,kend,dl), f_2_12_re(k), T{0}};
abb[11] = SpDLog_f_4_569_W_20_Im(t, path, abb);
abb[18] = SpDLog_f_4_569_W_20_Re(t, path, abb);

                    
            return f_4_569_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_569_DLogXconstant_part(base_point<T>, kend);
	value += f_4_569_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_569_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_569_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_569_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_569_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_569_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_569_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
