/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_83.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_83_abbreviated (const std::array<T,30>& abb) {
T z[45];
z[0] = 2 * abb[24];
z[1] = abb[23] + z[0];
z[2] = -abb[20] + z[1];
z[3] = 2 * abb[25];
z[4] = 2 * abb[22];
z[5] = 2 * abb[21];
z[6] = -3 * abb[26] + -z[2] + z[3] + z[4] + z[5];
z[6] = abb[9] * z[6];
z[7] = abb[26] * (T(1) / T(2));
z[8] = abb[20] * (T(1) / T(2));
z[9] = z[7] + z[8];
z[10] = abb[24] + abb[23] * (T(1) / T(2));
z[11] = z[9] + z[10];
z[12] = abb[21] + abb[22];
z[13] = z[11] + -z[12];
z[13] = abb[0] * z[13];
z[14] = abb[22] * (T(1) / T(2)) + -z[10];
z[15] = abb[25] + -abb[26];
z[16] = abb[20] + z[15];
z[17] = z[14] + z[16];
z[17] = abb[3] * z[17];
z[18] = abb[22] + -abb[26];
z[19] = abb[20] + z[18];
z[20] = abb[6] * z[19];
z[13] = z[13] + -z[17] + (T(1) / T(2)) * z[20];
z[21] = abb[25] * (T(1) / T(2));
z[22] = abb[21] + z[10];
z[23] = abb[20] + abb[22];
z[24] = -z[21] + -z[22] + z[23];
z[25] = abb[4] * (T(1) / T(2));
z[24] = z[24] * z[25];
z[26] = abb[21] + z[15];
z[27] = -z[14] + z[26];
z[27] = abb[5] * z[27];
z[9] = -z[9] + z[22];
z[22] = abb[1] * z[9];
z[18] = abb[21] + z[18];
z[28] = abb[8] * z[18];
z[24] = z[6] + z[13] + z[22] + z[24] + -z[27] + (T(-3) / T(2)) * z[28];
z[24] = abb[11] * z[24];
z[29] = abb[20] + -abb[24] + z[15];
z[30] = -abb[22] + abb[23];
z[29] = 2 * z[29] + -z[30];
z[29] = abb[3] * z[29];
z[31] = z[6] + -z[29];
z[5] = -abb[26] + z[5];
z[23] = z[5] + -z[23];
z[32] = abb[25] * (T(3) / T(2)) + z[10] + z[23];
z[32] = abb[4] * z[32];
z[33] = abb[24] + z[26];
z[33] = z[30] + 2 * z[33];
z[34] = abb[5] * z[33];
z[32] = -z[31] + z[32] + z[34];
z[35] = -z[1] + z[4];
z[36] = abb[20] + -abb[26];
z[37] = -z[3] + z[35] + -z[36];
z[38] = abb[0] * z[37];
z[38] = z[32] + z[38];
z[38] = abb[13] * z[38];
z[35] = -abb[20] + z[5] + z[35];
z[35] = abb[0] * z[35];
z[31] = -z[31] + z[35];
z[23] = -z[3] + -z[23];
z[23] = abb[4] * z[23];
z[23] = z[23] + -z[31];
z[23] = abb[10] * z[23];
z[2] = z[2] + z[5];
z[2] = abb[1] * z[2];
z[5] = z[2] + -z[29];
z[6] = z[5] + z[6];
z[39] = -z[20] + z[28];
z[40] = abb[21] + -abb[26];
z[3] = -abb[22] + z[3] + z[40];
z[41] = abb[4] * z[3];
z[42] = abb[0] * z[18];
z[43] = 2 * z[42];
z[41] = z[6] + -z[39] + -z[41] + -z[43];
z[44] = -abb[12] * z[41];
z[23] = z[23] + z[24] + z[38] + z[44];
z[23] = abb[11] * z[23];
z[12] = abb[25] + abb[26] * (T(-3) / T(2)) + z[8] + -z[10] + z[12];
z[12] = abb[9] * z[12];
z[9] = abb[22] + -abb[25] + -z[9];
z[9] = abb[4] * z[9];
z[24] = 2 * z[28];
z[5] = z[5] + z[9] + 3 * z[12] + z[20] + -z[24] + -z[34];
z[5] = abb[16] * z[5];
z[9] = 2 * abb[23] + 4 * abb[24] + -z[4] + z[26];
z[9] = abb[5] * z[9];
z[12] = abb[4] * z[33];
z[4] = -abb[25] + z[4];
z[26] = z[4] + z[40];
z[26] = abb[0] * z[26];
z[6] = -z[6] + z[9] + z[12] + z[26];
z[6] = abb[14] * z[6];
z[9] = abb[25] + -z[1] + z[18];
z[9] = abb[5] * z[9];
z[12] = abb[21] * (T(-1) / T(2)) + z[8] + z[14];
z[12] = abb[4] * z[12];
z[12] = z[9] + z[12] + z[13] + -z[22] + (T(1) / T(2)) * z[28];
z[12] = prod_pow(abb[12], 2) * z[12];
z[13] = z[17] + -z[20];
z[17] = abb[22] + -z[10] + -z[21];
z[17] = z[17] * z[25];
z[17] = -z[13] + z[17] + -z[27];
z[18] = prod_pow(abb[13], 2);
z[17] = z[17] * z[18];
z[21] = -abb[15] * z[32];
z[25] = -abb[15] * z[37];
z[3] = z[3] * z[18];
z[4] = -abb[21] + -z[4] + z[11];
z[4] = abb[16] * z[4];
z[3] = z[3] + z[4] + z[25];
z[3] = abb[0] * z[3];
z[4] = -abb[28] * z[41];
z[11] = -abb[4] * z[14];
z[11] = -z[9] + z[11] + z[13] + z[42];
z[11] = prod_pow(abb[10], 2) * z[11];
z[13] = abb[4] * z[19];
z[14] = 2 * z[20];
z[13] = -z[13] + -z[14] + z[24] + z[31];
z[19] = -abb[27] * z[13];
z[7] = z[7] + -z[8] + -z[10];
z[7] = abb[0] * z[7];
z[7] = z[7] + z[22];
z[8] = abb[2] * z[16];
z[10] = abb[21] + (T(-5) / T(4)) * z[15];
z[10] = abb[20] * (T(-3) / T(4)) + (T(1) / T(3)) * z[10];
z[10] = abb[4] * z[10];
z[7] = (T(3) / T(2)) * z[7] + (T(5) / T(12)) * z[8] + z[10] + (T(-1) / T(3)) * z[39];
z[7] = prod_pow(m1_set::bc<T>[0], 2) * z[7];
z[10] = -abb[15] + -abb[16] + -abb[27] + -z[18];
z[10] = z[10] * z[16];
z[15] = 2 * z[16];
z[15] = abb[13] * z[15];
z[16] = -abb[11] * z[16];
z[15] = z[15] + z[16];
z[15] = abb[11] * z[15];
z[10] = 2 * z[10] + z[15];
z[10] = abb[2] * z[10];
z[3] = abb[29] + z[3] + z[4] + z[5] + z[6] + z[7] + z[10] + z[11] + z[12] + z[17] + z[19] + z[21] + z[23];
z[4] = 2 * z[9] + -z[29];
z[2] = z[2] + -z[28];
z[0] = z[0] + z[30];
z[5] = abb[20] + -abb[21];
z[6] = z[0] + -z[5];
z[6] = abb[4] * z[6];
z[6] = z[2] + -z[4] + z[6] + -z[20] + z[35];
z[6] = abb[12] * z[6];
z[0] = -abb[4] * z[0];
z[0] = z[0] + z[4] + z[14] + -z[43];
z[0] = abb[10] * z[0];
z[1] = -z[1] + -z[36];
z[1] = abb[0] * z[1];
z[4] = -abb[4] * z[5];
z[1] = z[1] + z[2] + z[4] + z[20];
z[1] = abb[11] * z[1];
z[0] = z[0] + z[1] + z[6];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = -2 * z[8] + -z[13];
z[1] = abb[17] * z[1];
z[2] = -abb[18] * z[41];
z[0] = abb[19] + z[0] + z[1] + z[2];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_83_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-7.297815244011661950005926547427107866177471428623772838228059582"),stof<T>("25.083870070789602007372134675536747325309685541654812971766202288")}, std::complex<T>{stof<T>("13.762527604142530957634829244898549910044471151796273735472691894"),stof<T>("-25.194436406982817812622440205167502090728442556080402122365526553")}, std::complex<T>{stof<T>("-0.8968035811165683255097666066265694557989011445528072852234950345"),stof<T>("-6.2201122807431781040338473758367132925688561857526177952023091884")}, std::complex<T>{stof<T>("7.361515941247437333138669304098011499665900867725308182468127346"),stof<T>("6.1095459445499622987835418462059585271500991713270286446029849231")}, std::complex<T>{stof<T>("14.723031882494874666277338608196022999331801735450616364936254692"),stof<T>("12.219091889099924597567083692411917054300198342654057289205969846")}, std::complex<T>{stof<T>("-6.4647123601308690076289026974714420438669997231725008972446323115"),stof<T>("0.1105663361932158052503055296307547654187570144255891505993242653")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[27].real()/kbase.W[27].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_83_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_83_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,4> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(32)) * (-v[3] + v[5]) * (-8 * (3 + 2 * m1_set::bc<T>[1] + -6 * m1_set::bc<T>[2]) + -48 * v[0] + 16 * v[1] + v[3] + -13 * v[5] + -4 * m1_set::bc<T>[1] * (v[3] + v[5]) + 2 * m1_set::bc<T>[2] * (12 * v[0] + -4 * v[1] + 3 * v[3] + 9 * v[5]))) / prod_pow(tend, 2);
c[1] = ((T(1) / T(32)) * (-v[3] + v[5]) * (2 * (-12 * v[0] + 4 * v[1] + v[3] + -v[5] + -4 * m1_set::bc<T>[1] * (4 + v[3] + v[5])) + m1_set::bc<T>[2] * (12 * v[0] + -4 * v[1] + 3 * (8 + v[3] + 3 * v[5])))) / prod_pow(tend, 2);
c[2] = ((5 + 2 * m1_set::bc<T>[1] + -6 * m1_set::bc<T>[2]) * (T(-1) / T(4)) * (-v[3] + v[5])) / tend;
c[3] = ((-1 + 2 * m1_set::bc<T>[1] + 3 * m1_set::bc<T>[2]) * (T(1) / T(4)) * (-v[3] + v[5])) / tend;


		return t * (-2 * abb[22] + abb[23] + 2 * abb[24] + abb[25]) * c[0] + t * (abb[20] + 2 * abb[22] + -abb[23] + -2 * abb[24] + -abb[26]) * c[1] + abb[25] * c[2] + abb[20] * (c[2] + -c[3]) + -2 * abb[22] * c[3] + abb[23] * c[3] + 2 * abb[24] * c[3] + abb[26] * (-c[2] + c[3]);
	}
	{
T z[7];
z[0] = -abb[22] + abb[24];
z[1] = abb[25] + z[0];
z[1] = -abb[23] + abb[26] + -2 * z[1];
z[2] = abb[14] + -abb[16];
z[1] = z[1] * z[2];
z[3] = 2 * abb[26];
z[4] = abb[23] * (T(-1) / T(4)) + abb[25] * (T(7) / T(4)) + (T(-1) / T(2)) * z[0] + -z[3];
z[5] = prod_pow(abb[13], 2);
z[4] = z[4] * z[5];
z[0] = abb[23] * (T(1) / T(2)) + z[0];
z[3] = abb[25] * (T(5) / T(2)) + z[0] + -z[3];
z[6] = -abb[13] * z[3];
z[0] = abb[25] * (T(1) / T(2)) + z[0];
z[0] = abb[11] * z[0];
z[0] = (T(3) / T(2)) * z[0] + z[6];
z[0] = abb[11] * z[0];
z[6] = abb[11] * abb[13];
z[5] = z[5] + -z[6];
z[2] = -z[2] + 2 * z[5];
z[2] = abb[20] * z[2];
z[3] = 2 * abb[20] + z[3];
z[3] = abb[15] * z[3];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4];
return abb[7] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_83_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_83_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("16.80651846491207406460628272937722124522433337293285295159945418"),stof<T>("-19.766188406324520395543805006067096914476276195380056671206653296")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,30> abb = {dl[0], dl[1], dl[5], dlog_W8(k,dl), dl[3], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_2_3(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[11].real()/k.W[11].real()), IntDLogL_W_16(t,k,kend,dl), rlog(kend.W[27].real()/k.W[27].real()), f_2_4_re(k), f_2_6_re(k), T{0}};
abb[19] = SpDLog_f_4_83_W_16_Im(t, path, abb);
abb[29] = SpDLog_f_4_83_W_16_Re(t, path, abb);

                    
            return f_4_83_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_83_DLogXconstant_part(base_point<T>, kend);
	value += f_4_83_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_83_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_83_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_83_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_83_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_83_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_83_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
