/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_514.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_514_abbreviated (const std::array<T,30>& abb) {
T z[46];
z[0] = -abb[16] + abb[28];
z[1] = prod_pow(abb[11], 2);
z[2] = (T(1) / T(2)) * z[1];
z[3] = z[0] + -z[2];
z[4] = abb[12] * (T(1) / T(2));
z[5] = -abb[11] + z[4];
z[6] = abb[12] * z[5];
z[7] = -z[3] + z[6];
z[8] = prod_pow(m1_set::bc<T>[0], 2);
z[9] = (T(1) / T(3)) * z[8];
z[10] = 2 * abb[27];
z[11] = z[9] + z[10];
z[12] = prod_pow(abb[10], 2);
z[13] = prod_pow(abb[13], 2);
z[14] = z[12] + -z[13];
z[15] = z[7] + z[11] + -z[14];
z[16] = abb[6] * z[15];
z[17] = 2 * abb[15];
z[18] = z[10] + z[17];
z[19] = 2 * abb[14];
z[20] = 2 * abb[28];
z[21] = z[19] + z[20];
z[22] = z[18] + -z[21];
z[23] = 2 * abb[16];
z[24] = z[1] + z[23];
z[25] = abb[10] + -abb[11];
z[26] = -abb[13] + z[25];
z[27] = abb[12] + 2 * z[26];
z[27] = abb[12] * z[27];
z[27] = -z[14] + z[22] + z[24] + z[27];
z[28] = abb[3] * z[27];
z[29] = abb[15] + abb[27];
z[30] = abb[16] + z[13] + z[29];
z[31] = (T(5) / T(12)) * z[8];
z[32] = 2 * abb[13];
z[33] = -abb[12] + z[32];
z[33] = abb[12] * z[33];
z[30] = 2 * z[30] + -z[31] + -z[33];
z[30] = abb[2] * z[30];
z[16] = -z[16] + z[28] + z[30];
z[34] = z[12] + z[13];
z[35] = 2 * abb[11];
z[36] = -abb[10] + z[35];
z[37] = abb[13] + z[4] + -z[36];
z[37] = abb[12] * z[37];
z[38] = (T(3) / T(4)) * z[8];
z[39] = -abb[14] + z[38];
z[40] = abb[16] + z[1];
z[41] = (T(1) / T(2)) * z[40];
z[42] = -abb[27] + z[20];
z[37] = -abb[15] + -z[34] + z[37] + z[39] + z[41] + -z[42];
z[37] = abb[0] * z[37];
z[43] = -z[1] + z[23];
z[33] = -abb[14] + z[17] + -z[33] + z[34] + z[43];
z[33] = abb[5] * z[33];
z[0] = -z[0] + -z[2] + z[6] + z[39];
z[0] = abb[1] * z[0];
z[6] = -abb[28] + abb[16] * (T(1) / T(2));
z[39] = abb[12] * z[26];
z[31] = abb[15] + -abb[27] + z[6] + -z[19] + z[31] + z[39];
z[31] = abb[4] * z[31];
z[39] = abb[12] + z[26];
z[39] = abb[12] * z[39];
z[44] = -abb[14] + z[29];
z[45] = -abb[28] + abb[16] * (T(3) / T(2)) + z[39] + z[44];
z[45] = abb[9] * z[45];
z[31] = -z[0] + z[16] + z[31] + z[33] + z[37] + -3 * z[45];
z[31] = abb[26] * z[31];
z[37] = 2 * abb[10];
z[5] = -z[5] + z[32] + -z[37];
z[5] = abb[12] * z[5];
z[5] = z[3] + z[5] + z[9] + -z[17] + z[19];
z[5] = abb[4] * z[5];
z[9] = abb[12] + 2 * z[25];
z[9] = abb[12] * z[9];
z[10] = abb[14] + -z[9] + -z[10] + z[20] + z[34] + -z[40];
z[10] = abb[0] * z[10];
z[17] = (T(3) / T(2)) * z[8];
z[19] = -abb[12] + z[35];
z[32] = abb[12] * z[19];
z[34] = z[17] + -z[21] + -z[32] + z[43];
z[34] = abb[1] * z[34];
z[22] = 3 * abb[16] + z[22] + 2 * z[39];
z[22] = abb[9] * z[22];
z[33] = -z[22] + z[33];
z[5] = z[5] + z[10] + -z[33] + z[34];
z[5] = abb[21] * z[5];
z[0] = -z[0] + z[45];
z[10] = -abb[10] + abb[13];
z[39] = z[4] + -z[10];
z[39] = abb[12] * z[39];
z[29] = z[29] + -z[38] + z[39] + z[41];
z[38] = abb[4] * z[29];
z[39] = abb[27] * (T(1) / T(2)) + (T(-11) / T(12)) * z[8];
z[41] = abb[11] * abb[12];
z[6] = -z[1] + -z[6] + -z[39] + z[41];
z[6] = abb[0] * z[6];
z[4] = z[4] + z[26];
z[4] = abb[12] * z[4];
z[13] = (T(1) / T(2)) * z[13];
z[26] = (T(1) / T(2)) * z[12] + -z[13];
z[3] = -z[3] + z[4] + -z[26] + z[44];
z[4] = abb[3] * z[3];
z[7] = z[7] + z[26] + z[39];
z[7] = abb[6] * z[7];
z[4] = -z[0] + z[4] + z[6] + z[7] + z[38];
z[4] = abb[20] * z[4];
z[6] = -abb[27] + z[12];
z[6] = -abb[16] + (T(3) / T(2)) * z[6] + (T(-5) / T(3)) * z[8] + -z[9] + z[13] + z[21];
z[6] = abb[0] * z[6];
z[7] = z[22] + -z[28];
z[9] = abb[4] * z[15];
z[12] = z[20] + -z[24] + z[32];
z[13] = abb[27] * (T(5) / T(2)) + (T(-7) / T(12)) * z[8] + -z[12] + -z[26];
z[13] = abb[6] * z[13];
z[6] = z[6] + z[7] + z[9] + z[13] + -z[30];
z[6] = abb[24] * z[6];
z[9] = abb[12] + -2 * z[10];
z[9] = abb[12] * z[9];
z[9] = z[9] + -z[17] + z[18] + z[40];
z[9] = abb[4] * z[9];
z[10] = -abb[27] + (T(11) / T(6)) * z[8];
z[12] = z[10] + z[12] + -z[14];
z[12] = abb[6] * z[12];
z[13] = abb[12] * z[35];
z[1] = abb[16] + 2 * z[1] + -z[13];
z[10] = z[1] + -z[10] + -z[20];
z[13] = -abb[0] * z[10];
z[7] = -z[7] + z[9] + -z[12] + z[13] + z[34];
z[7] = abb[25] * z[7];
z[9] = abb[0] + abb[4];
z[9] = z[9] * z[29];
z[0] = z[0] + z[9] + -z[16];
z[0] = abb[23] * z[0];
z[9] = abb[0] + -abb[3];
z[3] = z[3] * z[9];
z[9] = -abb[4] * z[27];
z[3] = z[3] + z[9] + z[12] + -z[33];
z[3] = abb[22] * z[3];
z[9] = -abb[11] + abb[12] * (T(3) / T(2));
z[9] = abb[12] * z[9];
z[2] = -abb[28] + -z[2] + z[9] + z[11] + z[23];
z[9] = -abb[21] + -abb[24] + abb[26];
z[2] = z[2] * z[9];
z[11] = -abb[20] + abb[22];
z[10] = z[10] * z[11];
z[1] = -z[1] + z[42];
z[1] = 2 * z[1] + (T(11) / T(3)) * z[8];
z[1] = abb[25] * z[1];
z[1] = z[1] + z[2] + z[10];
z[1] = abb[8] * z[1];
z[0] = abb[29] + z[0] + z[1] + z[3] + z[4] + z[5] + z[6] + z[7] + z[31];
z[1] = -abb[11] + abb[12];
z[2] = z[1] + z[37];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = 2 * abb[17];
z[4] = -abb[18] + z[2] + z[3];
z[5] = abb[6] * z[4];
z[6] = m1_set::bc<T>[0] * z[25];
z[7] = -abb[17] + abb[18];
z[8] = z[6] + -z[7];
z[10] = abb[3] * z[8];
z[12] = 2 * z[10];
z[13] = abb[2] * z[3];
z[13] = z[12] + z[13];
z[5] = z[5] + -z[13];
z[14] = 2 * abb[18];
z[15] = -abb[17] + z[14];
z[2] = z[2] + -z[15];
z[2] = abb[0] * z[2];
z[6] = abb[5] * z[6];
z[16] = 2 * z[6];
z[17] = abb[11] + abb[12];
z[17] = m1_set::bc<T>[0] * z[17];
z[18] = -abb[18] + z[17];
z[20] = abb[1] * z[18];
z[7] = abb[9] * z[7];
z[21] = -abb[17] + -abb[18];
z[21] = abb[4] * z[21];
z[2] = z[2] + -z[5] + 3 * z[7] + -z[16] + -z[20] + z[21];
z[2] = abb[26] * z[2];
z[21] = abb[0] * z[8];
z[22] = 2 * z[20];
z[23] = 2 * z[7];
z[24] = abb[18] + z[17];
z[24] = abb[4] * z[24];
z[16] = z[16] + -2 * z[21] + z[22] + -z[23] + z[24];
z[16] = abb[21] * z[16];
z[7] = z[7] + z[20];
z[20] = -abb[18] + abb[17] * (T(1) / T(2));
z[24] = m1_set::bc<T>[0] * z[19];
z[24] = -z[20] + z[24];
z[24] = abb[0] * z[24];
z[1] = -abb[10] + z[1];
z[25] = m1_set::bc<T>[0] * z[1];
z[20] = z[20] + z[25];
z[20] = abb[6] * z[20];
z[17] = -abb[17] + z[17];
z[25] = abb[4] * z[17];
z[20] = z[7] + z[10] + z[20] + z[24] + -z[25];
z[20] = abb[20] * z[20];
z[4] = abb[4] * z[4];
z[24] = abb[10] * m1_set::bc<T>[0];
z[24] = abb[17] * (T(-3) / T(2)) + z[14] + -3 * z[24];
z[24] = abb[0] * z[24];
z[26] = 2 * abb[12] + -z[36];
z[26] = m1_set::bc<T>[0] * z[26];
z[14] = abb[17] * (T(5) / T(2)) + -z[14] + z[26];
z[14] = abb[6] * z[14];
z[4] = z[4] + -z[13] + z[14] + -z[23] + z[24];
z[4] = abb[24] * z[4];
z[13] = 2 * m1_set::bc<T>[0];
z[1] = z[1] * z[13];
z[1] = z[1] + -z[15];
z[1] = abb[6] * z[1];
z[1] = z[1] + z[23];
z[8] = -abb[4] * z[8];
z[6] = z[6] + z[8];
z[6] = -z[1] + 2 * z[6] + -z[10] + z[21];
z[6] = abb[22] * z[6];
z[8] = z[13] * z[19];
z[8] = z[8] + z[15];
z[10] = abb[0] * z[8];
z[1] = z[1] + z[10] + z[12] + z[22] + -2 * z[25];
z[1] = abb[25] * z[1];
z[10] = -abb[0] * z[17];
z[5] = z[5] + -z[7] + z[10] + -z[25];
z[5] = abb[23] * z[5];
z[3] = z[3] + z[18];
z[3] = z[3] * z[9];
z[7] = 2 * abb[25] + -z[11];
z[7] = z[7] * z[8];
z[3] = z[3] + z[7];
z[3] = abb[8] * z[3];
z[1] = abb[19] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[16] + z[20];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_514_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("18.030354108360705043457066510385725404331099222662823179650728832"),stof<T>("-22.220631606669139070965816410528306681055966430303651353712824902")}, std::complex<T>{stof<T>("13.762527604142530957634829244898549910044471151796273735472691894"),stof<T>("-25.194436406982817812622440205167502090728442556080402122365526553")}, std::complex<T>{stof<T>("-9.3131899292339144285307869183397178166529937726162666622218311787"),stof<T>("-4.1126760216559621378873502335357239590050872368184367797598318983")}, std::complex<T>{stof<T>("-7.297815244011661950005926547427107866177471428623772838228059582"),stof<T>("25.083870070789602007372134675536747325309685541654812971766202288")}, std::complex<T>{stof<T>("-2.252451818995921607297376894574565543811105726874055620184265341"),stof<T>("26.222741292131885403602861114433275874642296652696498982873332535")}, std::complex<T>{stof<T>("36.060708216721410086914133020771450808662198445325646359301457663"),stof<T>("-44.441263213338278141931632821056613362111932860607302707425649804")}, std::complex<T>{stof<T>("-6.4647123601308690076289026974714420438669997231725008972446323115"),stof<T>("0.1105663361932158052503055296307547654187570144255891505993242653")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[23].real()/kbase.W[23].real()), rlog(k.W[28].real()/kbase.W[28].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_514_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_514_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(32)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (2 * (13 * v[0] + -5 * v[1] + v[2] + v[3] + 4 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -v[5]) + m1_set::bc<T>[2] * (-21 * v[0] + v[1] + 3 * (-8 + -3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = ((4 + 4 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (T(-1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[22] + abb[23] + -abb[26]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[12] + -abb[13];
z[0] = abb[13] * z[0];
z[0] = -abb[15] + z[0];
z[0] = abb[14] + -abb[16] + 2 * z[0];
z[1] = -abb[22] + -abb[23] + abb[26];
return abb[7] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_514_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_514_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-52.815521927635466931572528319349679797842324160465163412861707653"),stof<T>("-65.052885001240185770880453308439429757799188670869256697628671009")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,30> abb = {dl[0], dl[1], dlog_W4(k,dl), dlog_W7(k,dl), dlog_W10(k,dl), dlog_W12(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), f_1_1(k), f_1_3(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_9_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[23].real()/k.W[23].real()), rlog(kend.W[28].real()/k.W[28].real()), f_2_2_re(k), f_2_9_re(k), T{0}};
abb[19] = SpDLog_f_4_514_W_17_Im(t, path, abb);
abb[29] = SpDLog_f_4_514_W_17_Re(t, path, abb);

                    
            return f_4_514_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_514_DLogXconstant_part(base_point<T>, kend);
	value += f_4_514_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_514_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_514_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_514_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_514_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_514_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_514_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
