/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_403.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_403_abbreviated (const std::array<T,36>& abb) {
T z[50];
z[0] = abb[27] * (T(1) / T(2));
z[1] = abb[29] * (T(1) / T(2));
z[2] = abb[25] * (T(1) / T(2));
z[3] = z[0] + z[1] + z[2];
z[4] = -abb[28] + z[3];
z[5] = 3 * abb[6];
z[4] = z[4] * z[5];
z[6] = abb[30] + abb[31] + abb[32];
z[7] = abb[10] * z[6];
z[8] = (T(1) / T(2)) * z[7];
z[9] = abb[9] * z[6];
z[10] = z[8] + (T(1) / T(2)) * z[9];
z[11] = 2 * abb[28];
z[12] = abb[26] * (T(1) / T(2)) + -z[11];
z[13] = abb[27] * (T(3) / T(2));
z[14] = abb[25] + z[12] + z[13];
z[14] = abb[3] * z[14];
z[15] = abb[26] + abb[29];
z[16] = 3 * abb[27];
z[17] = 3 * abb[25] + z[16];
z[18] = 5 * abb[28] + -z[15] + -z[17];
z[18] = abb[8] * z[18];
z[19] = -abb[28] + abb[29];
z[20] = abb[24] + z[19];
z[20] = abb[1] * z[20];
z[21] = (T(1) / T(2)) * z[20];
z[22] = abb[24] + -abb[29];
z[23] = abb[27] + z[22];
z[23] = abb[0] * z[23];
z[22] = abb[26] + z[22];
z[22] = abb[5] * z[22];
z[24] = abb[25] + abb[27];
z[25] = -abb[29] + z[24];
z[26] = -abb[2] * z[25];
z[27] = abb[26] + abb[29] * (T(-3) / T(2)) + abb[28] * (T(1) / T(2)) + z[2];
z[27] = abb[4] * z[27];
z[4] = z[4] + -z[10] + z[14] + (T(1) / T(2)) * z[18] + z[21] + (T(-3) / T(2)) * z[22] + 2 * z[23] + -z[26] + z[27];
z[4] = abb[13] * z[4];
z[14] = abb[27] + z[2];
z[12] = z[1] + z[12] + z[14];
z[12] = abb[3] * z[12];
z[14] = -z[14] + (T(1) / T(2)) * z[15];
z[18] = abb[0] * (T(1) / T(2));
z[18] = z[14] * z[18];
z[18] = (T(-1) / T(4)) * z[9] + z[18];
z[27] = -abb[26] + z[11];
z[28] = 2 * z[27];
z[29] = abb[29] + z[24];
z[30] = z[28] + -z[29];
z[31] = abb[6] * z[30];
z[12] = z[12] + -z[18] + -z[31];
z[32] = abb[29] * (T(3) / T(4));
z[0] = abb[25] * (T(1) / T(4)) + z[0];
z[33] = abb[28] + abb[26] * (T(-9) / T(4)) + z[0] + z[32];
z[33] = abb[4] * z[33];
z[34] = 4 * abb[28];
z[35] = abb[25] * (T(3) / T(2)) + z[13];
z[36] = abb[26] + abb[29] * (T(-7) / T(2)) + z[34] + -z[35];
z[36] = abb[8] * z[36];
z[37] = 2 * z[26];
z[38] = abb[26] + abb[28];
z[39] = 2 * abb[29] + -z[38];
z[40] = abb[7] * z[39];
z[33] = z[12] + z[33] + z[36] + -z[37] + z[40];
z[33] = abb[15] * z[33];
z[35] = -z[1] + -z[27] + z[35];
z[36] = abb[8] * z[35];
z[36] = z[36] + z[37];
z[41] = abb[26] * (T(5) / T(4)) + -z[11];
z[0] = abb[29] * (T(-5) / T(4)) + z[0] + -z[41];
z[0] = abb[4] * z[0];
z[0] = z[0] + z[12] + -z[36];
z[12] = -abb[16] * z[0];
z[30] = abb[3] * z[30];
z[30] = 4 * z[26] + z[30] + z[31];
z[28] = abb[29] + z[28];
z[42] = -z[17] + z[28];
z[42] = abb[8] * z[42];
z[43] = abb[4] * z[25];
z[43] = -z[30] + z[42] + 2 * z[43];
z[44] = abb[14] * z[43];
z[45] = z[20] + z[22];
z[46] = abb[8] * z[39];
z[47] = -z[40] + z[45] + z[46];
z[47] = abb[17] * z[47];
z[4] = z[4] + z[12] + z[33] + -z[44] + z[47];
z[4] = abb[13] * z[4];
z[12] = 4 * abb[27];
z[33] = 2 * abb[25];
z[47] = 7 * abb[28];
z[48] = -abb[26] + z[47];
z[48] = abb[29] + -z[12] + -z[33] + (T(1) / T(2)) * z[48];
z[48] = abb[3] * z[48];
z[49] = 4 * abb[29];
z[12] = -4 * abb[25] + abb[26] + -z[12] + z[47] + -z[49];
z[12] = abb[6] * z[12];
z[12] = z[12] + (T(1) / T(2)) * z[26] + z[48];
z[47] = z[20] + z[23];
z[28] = z[24] + (T(-1) / T(3)) * z[28];
z[28] = abb[8] * z[28];
z[48] = -abb[26] + abb[29] * (T(1) / T(3));
z[48] = abb[25] * (T(-1) / T(6)) + abb[27] * (T(1) / T(3)) + (T(1) / T(2)) * z[48];
z[48] = abb[4] * z[48];
z[10] = z[10] + (T(1) / T(3)) * z[12] + z[28] + z[40] + (T(-13) / T(6)) * z[47] + z[48];
z[10] = prod_pow(m1_set::bc<T>[0], 2) * z[10];
z[12] = abb[25] * (T(3) / T(4)) + -z[11] + z[13] + (T(1) / T(4)) * z[15];
z[12] = abb[9] * z[12];
z[15] = abb[11] * z[35];
z[28] = abb[3] * (T(1) / T(2));
z[35] = -abb[12] + z[28];
z[35] = z[6] * z[35];
z[47] = abb[0] + abb[4];
z[6] = (T(3) / T(4)) * z[6];
z[6] = z[6] * z[47];
z[27] = -abb[29] + z[27];
z[27] = abb[10] * z[27];
z[6] = z[6] + z[12] + z[15] + -z[27] + z[35];
z[12] = abb[35] * z[6];
z[2] = abb[26] * (T(3) / T(2)) + z[2];
z[1] = -z[1] + -z[2] + z[11];
z[1] = abb[3] * z[1];
z[13] = abb[25] * (T(-7) / T(4)) + -z[13] + z[32] + -z[41];
z[13] = abb[4] * z[13];
z[1] = z[1] + z[13] + -z[18] + z[36];
z[1] = abb[16] * z[1];
z[13] = -abb[25] + -z[19];
z[13] = abb[3] * z[13];
z[2] = -abb[28] + abb[29] * (T(5) / T(2)) + -z[2];
z[2] = abb[4] * z[2];
z[2] = z[2] + z[13] + z[26];
z[13] = -abb[29] + (T(1) / T(2)) * z[38];
z[15] = abb[8] * z[13];
z[2] = (T(1) / T(2)) * z[2] + 3 * z[15] + -z[18] + z[40];
z[2] = abb[15] * z[2];
z[18] = abb[4] * z[39];
z[19] = -z[18] + -z[40] + 2 * z[46];
z[27] = abb[17] * z[19];
z[1] = z[1] + z[2] + z[27];
z[1] = abb[15] * z[1];
z[2] = z[29] + -z[38];
z[2] = abb[3] * z[2];
z[2] = z[2] + -z[26];
z[27] = -abb[26] + abb[28];
z[32] = 3 * z[27];
z[35] = z[25] + -z[32];
z[35] = abb[4] * z[35];
z[31] = -z[2] + z[31] + z[35] + z[46];
z[31] = abb[15] * z[31];
z[25] = z[25] + z[32];
z[25] = abb[4] * z[25];
z[2] = -z[2] + z[25];
z[25] = 2 * abb[26];
z[32] = -abb[28] + z[25];
z[3] = -z[3] + z[32];
z[3] = abb[6] * z[3];
z[2] = (T(1) / T(2)) * z[2] + z[3] + -z[15];
z[2] = abb[14] * z[2];
z[3] = abb[16] * z[43];
z[2] = z[2] + z[3] + z[31];
z[2] = abb[14] * z[2];
z[3] = -abb[26] + abb[29];
z[31] = -abb[25] + z[3];
z[28] = z[28] * z[31];
z[14] = abb[0] * z[14];
z[8] = z[8] + z[14] + -z[26] + z[28];
z[8] = prod_pow(abb[16], 2) * z[8];
z[13] = -abb[4] * z[13];
z[13] = z[13] + z[15] + z[21] + (T(1) / T(2)) * z[22];
z[13] = prod_pow(abb[17], 2) * z[13];
z[14] = 2 * abb[27] + z[33] + z[38] + -z[49];
z[14] = abb[4] * z[14];
z[15] = 5 * abb[29] + -z[17] + -2 * z[32];
z[15] = abb[8] * z[15];
z[14] = z[14] + z[15] + -z[30] + -z[40];
z[15] = -abb[33] * z[14];
z[17] = -abb[20] + abb[34];
z[17] = z[17] * z[19];
z[3] = abb[28] + z[3] + -z[24];
z[21] = abb[4] + -abb[8];
z[3] = z[3] * z[21];
z[21] = abb[3] * z[27];
z[3] = z[3] + z[21] + z[26];
z[3] = abb[19] * z[3];
z[21] = -z[18] + -z[45] + z[46];
z[21] = abb[18] * z[21];
z[1] = z[1] + z[2] + 3 * z[3] + z[4] + z[8] + z[10] + z[12] + z[13] + z[15] + z[17] + z[21];
z[2] = -abb[26] + -z[16] + -z[33] + z[34];
z[2] = abb[3] * z[2];
z[3] = 2 * z[20] + -z[40];
z[4] = z[11] + -z[29];
z[4] = z[4] * z[5];
z[5] = 2 * z[22];
z[8] = -abb[25] + -abb[28] + 3 * abb[29] + -z[25];
z[8] = abb[4] * z[8];
z[2] = z[2] + -z[3] + z[4] + z[5] + z[7] + z[8] + z[9] + -4 * z[23] + z[37] + -z[42];
z[2] = abb[13] * z[2];
z[4] = -abb[15] + abb[16];
z[0] = z[0] * z[4];
z[3] = -z[3] + -z[5] + -z[18];
z[3] = abb[17] * z[3];
z[0] = z[0] + z[2] + z[3] + z[44];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[23] * z[6];
z[3] = -abb[21] * z[14];
z[4] = abb[22] * z[19];
z[0] = z[0] + z[2] + z[3] + z[4];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_403_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-7.63044610169493535091196880111479307520521581628708430584049572"),stof<T>("38.858042149405861786122438198413189216559838772369452802595963388")}, std::complex<T>{stof<T>("4.9643392092258240509846417516584680930739973460338388499602459789"),stof<T>("8.2194312921318134483639597859143266294480325684071091909357557909")}, std::complex<T>{stof<T>("15.95998050540324113025781827921649446642658385454555729362387764"),stof<T>("1.044340035113867617355517450567805692587028714651240662674027234")}, std::complex<T>{stof<T>("18.210871473887926973369852710770735252781745470290022066041329246"),stof<T>("35.190035936572035802881414162403778979452855434852197429420906742")}, std::complex<T>{stof<T>("-29.980618480405179453755950450474087384289552386313468645884156529"),stof<T>("-27.406297465137997882324895997984901834935519555171991387829061359")}, std::complex<T>{stof<T>("-4.1902334988859886498717205395131423349187769385221107137810503571"),stof<T>("-8.8280785065479055379120356149866828371043645943314467042658726168")}, std::complex<T>{stof<T>("4.6547188104916960897412832574814250337357180128234620229279515134"),stof<T>("-6.7898493908076716514634340510446747195333140327511877322391776059")}, std::complex<T>{stof<T>("4.6547188104916960897412832574814250337357180128234620229279515134"),stof<T>("-6.7898493908076716514634340510446747195333140327511877322391776059")}, std::complex<T>{stof<T>("4.6547188104916960897412832574814250337357180128234620229279515134"),stof<T>("-6.7898493908076716514634340510446747195333140327511877322391776059")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[127].real()/kbase.W[127].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_403_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_403_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("5.658014597090009703796904972556711182068075173708460706831948011"),stof<T>("-25.045462433235984785741459641393669429588540258245517530300771478")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,36> abb = {dl[0], dlog_W3(k,dl), dl[5], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W13(k,dl), dlog_W15(k,dl), dlog_W26(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_19(k), f_2_4_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[127].real()/k.W[127].real()), f_2_4_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_403_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_403_DLogXconstant_part(base_point<T>, kend);
	value += f_4_403_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_403_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_403_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_403_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_403_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_403_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_403_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
