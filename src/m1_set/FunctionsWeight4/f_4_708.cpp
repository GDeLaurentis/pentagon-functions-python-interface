/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_708.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_708_abbreviated (const std::array<T,71>& abb) {
T z[134];
z[0] = 3 * abb[64];
z[1] = 3 * abb[61];
z[2] = 2 * abb[62];
z[3] = -abb[63] + z[0] + -z[1] + -z[2];
z[3] = abb[8] * z[3];
z[4] = abb[57] + abb[58];
z[5] = 3 * abb[59];
z[6] = -abb[53] + abb[55] + -abb[56];
z[7] = -abb[54] + 2 * z[4] + -z[5] + -z[6];
z[8] = -abb[22] * z[7];
z[9] = abb[62] + abb[63];
z[10] = -abb[64] + z[9];
z[11] = abb[60] + z[10];
z[12] = abb[16] * z[11];
z[13] = -abb[54] + -abb[59] + z[4];
z[14] = abb[27] * z[13];
z[12] = z[12] + -z[14];
z[14] = -2 * abb[59] + z[4] + -z[6];
z[15] = -abb[26] * z[14];
z[16] = -abb[62] + 2 * abb[64];
z[17] = abb[61] + z[16];
z[18] = abb[17] * z[17];
z[15] = z[15] + z[18];
z[18] = abb[6] * z[17];
z[19] = abb[2] * z[11];
z[20] = 4 * z[19];
z[21] = z[18] + -z[20];
z[22] = abb[61] + z[2];
z[23] = -abb[63] + abb[64];
z[24] = z[22] + -z[23];
z[25] = abb[4] * z[24];
z[26] = -abb[61] + z[23];
z[27] = abb[5] * z[26];
z[27] = -z[25] + z[27];
z[28] = 2 * abb[8];
z[29] = -abb[17] + z[28];
z[30] = 2 * abb[4];
z[31] = -abb[6] + -z[29] + -z[30];
z[31] = abb[60] * z[31];
z[32] = abb[61] + z[11];
z[32] = abb[12] * z[32];
z[33] = 6 * z[32];
z[11] = abb[7] * z[11];
z[3] = z[3] + z[8] + -z[11] + z[12] + -z[15] + z[21] + z[27] + z[31] + z[33];
z[3] = abb[37] * z[3];
z[8] = 2 * abb[6];
z[31] = abb[61] + z[9];
z[34] = z[8] * z[31];
z[35] = abb[13] * z[31];
z[36] = 2 * z[35];
z[37] = abb[20] * abb[65];
z[38] = z[36] + -z[37];
z[39] = abb[61] + abb[63];
z[40] = abb[64] + z[39];
z[41] = abb[8] * z[40];
z[42] = abb[18] * abb[65];
z[34] = -z[34] + z[38] + -z[41] + -z[42];
z[43] = abb[5] + -abb[14];
z[44] = abb[6] + z[43];
z[44] = abb[60] * z[44];
z[45] = abb[7] * z[40];
z[46] = abb[19] * abb[65];
z[44] = -z[34] + z[44] + -z[45] + z[46];
z[47] = abb[32] * z[44];
z[48] = abb[6] * z[40];
z[49] = abb[60] * z[30];
z[48] = z[48] + -z[49];
z[49] = 2 * z[32];
z[27] = -z[27] + z[45] + -z[48] + -z[49];
z[27] = abb[34] * z[27];
z[3] = z[3] + z[27] + z[47];
z[27] = -abb[22] * z[14];
z[27] = -z[15] + z[27];
z[47] = abb[61] + abb[62];
z[50] = 2 * abb[63];
z[51] = z[47] + z[50];
z[52] = abb[5] * z[51];
z[53] = 2 * z[46] + z[52];
z[54] = z[27] + z[53];
z[55] = 2 * abb[61];
z[56] = abb[64] + z[55];
z[57] = z[9] + z[56];
z[58] = abb[6] * z[57];
z[58] = z[42] + z[58];
z[59] = abb[14] * abb[61];
z[60] = abb[14] * abb[62];
z[59] = z[59] + z[60];
z[61] = abb[61] + z[23];
z[62] = -abb[60] + z[61];
z[62] = abb[0] * z[62];
z[63] = z[59] + -z[62];
z[64] = -abb[61] + z[16];
z[65] = -z[28] * z[64];
z[29] = z[29] + -z[43];
z[29] = abb[60] * z[29];
z[66] = 4 * z[32];
z[67] = 4 * z[35];
z[29] = z[20] + z[29] + -2 * z[37] + -z[54] + -z[58] + -z[63] + z[65] + -z[66] + z[67];
z[29] = abb[35] * z[29];
z[65] = abb[17] + z[8];
z[68] = -z[30] + z[43] + z[65];
z[68] = abb[60] * z[68];
z[69] = 3 * abb[63] + -abb[64];
z[70] = 5 * abb[62];
z[71] = 4 * abb[61];
z[72] = z[69] + z[70] + z[71];
z[72] = abb[6] * z[72];
z[73] = abb[4] * z[10];
z[74] = z[41] + -z[73];
z[67] = 3 * z[42] + -z[67];
z[54] = z[54] + -z[63] + z[67] + z[68] + z[72] + 2 * z[74];
z[68] = -abb[54] + abb[59] + z[6];
z[72] = 2 * z[68];
z[74] = -abb[24] * z[72];
z[74] = -z[54] + z[74];
z[74] = abb[31] * z[74];
z[75] = z[42] + -z[73];
z[76] = abb[6] * z[47];
z[77] = -z[37] + z[75] + z[76];
z[78] = abb[23] * z[13];
z[79] = abb[25] * z[13];
z[78] = z[78] + z[79];
z[80] = abb[22] * z[13];
z[81] = -z[12] + z[80];
z[82] = abb[4] * abb[60];
z[63] = z[63] + -z[77] + z[78] + z[81] + z[82];
z[82] = abb[33] * z[63];
z[82] = 2 * z[82];
z[83] = z[10] + z[55];
z[83] = abb[8] * z[83];
z[83] = z[81] + z[83];
z[84] = 2 * z[19];
z[85] = z[49] + -z[84];
z[86] = abb[6] + abb[8];
z[87] = abb[60] * z[86];
z[88] = abb[6] * z[10];
z[87] = -z[78] + -z[83] + z[85] + -z[87] + -z[88];
z[89] = abb[24] * z[13];
z[90] = -z[87] + z[89];
z[91] = 2 * abb[36];
z[90] = z[90] * z[91];
z[92] = -z[46] + z[62];
z[93] = -z[52] + z[92];
z[81] = z[11] + z[81];
z[38] = z[38] + -z[59] + -z[81] + z[93];
z[38] = 2 * z[38];
z[94] = -abb[30] * z[38];
z[95] = -abb[37] * z[14];
z[96] = abb[34] * z[68];
z[97] = abb[32] * z[68];
z[98] = z[96] + -z[97];
z[95] = z[95] + -z[98];
z[99] = -abb[31] * z[14];
z[100] = -abb[35] * z[14];
z[101] = -2 * z[95] + z[99] + z[100];
z[102] = abb[23] + abb[25];
z[101] = -z[101] * z[102];
z[7] = -abb[37] * z[7];
z[7] = z[7] + z[97] + -z[100];
z[103] = 2 * abb[24];
z[7] = z[7] * z[103];
z[3] = 2 * z[3] + z[7] + z[29] + z[74] + -z[82] + z[90] + z[94] + z[101];
z[3] = m1_set::bc<T>[0] * z[3];
z[7] = 2 * abb[14];
z[29] = 2 * abb[5];
z[74] = abb[0] + abb[6] + -8 * abb[29] + z[7] + z[29];
z[74] = abb[65] * z[74];
z[90] = abb[28] * z[14];
z[94] = abb[19] * z[51];
z[57] = abb[18] * z[57];
z[101] = abb[20] * abb[62];
z[16] = abb[21] * z[16];
z[104] = -abb[20] + abb[21];
z[105] = abb[61] * z[104];
z[16] = -z[16] + z[57] + z[74] + z[90] + z[94] + z[101] + -z[105];
z[57] = abb[19] + z[104];
z[74] = abb[60] * z[57];
z[74] = z[16] + z[74];
z[74] = abb[52] * z[74];
z[23] = 2 * z[23];
z[90] = -abb[62] + -z[1] + z[23];
z[90] = abb[8] * z[90];
z[94] = 3 * abb[4] + abb[5] + -abb[17] + z[86];
z[94] = abb[60] * z[94];
z[23] = -abb[61] + -3 * abb[62] + z[23];
z[23] = abb[4] * z[23];
z[64] = abb[5] * z[64];
z[18] = z[18] + z[23] + z[27] + z[64] + z[90] + -z[94];
z[23] = z[18] + z[33] + -z[84];
z[23] = abb[50] * z[23];
z[27] = abb[5] + -abb[8];
z[33] = abb[62] + -abb[64];
z[64] = -abb[63] + z[33];
z[27] = z[27] * z[64];
z[90] = abb[4] + abb[5];
z[94] = -abb[8] + z[90];
z[94] = abb[60] * z[94];
z[27] = -z[27] + -z[73] + z[81] + z[84] + -z[94];
z[94] = abb[51] * z[27];
z[101] = abb[49] * z[44];
z[23] = z[23] + -z[94] + z[101];
z[94] = -abb[48] * z[38];
z[101] = -abb[50] * z[14];
z[104] = abb[49] * z[68];
z[101] = z[101] + z[104];
z[104] = 2 * z[102];
z[104] = z[101] * z[104];
z[105] = -abb[51] * z[13];
z[101] = z[101] + z[105];
z[101] = z[101] * z[103];
z[3] = z[3] + 2 * z[23] + z[74] + z[94] + z[101] + z[104];
z[3] = 2 * z[3];
z[23] = abb[3] * z[61];
z[74] = abb[15] * z[40];
z[94] = abb[61] + abb[64];
z[101] = abb[10] * z[94];
z[104] = 2 * z[101];
z[23] = z[23] + z[74] + -z[104];
z[105] = abb[60] + z[9];
z[56] = z[56] + z[105];
z[56] = abb[7] * z[56];
z[106] = -abb[23] * z[14];
z[107] = -abb[6] + abb[17];
z[107] = -abb[60] * z[107];
z[76] = z[12] + z[15] + -z[23] + -z[56] + z[76] + -z[79] + z[106] + z[107];
z[76] = abb[38] * z[76];
z[79] = abb[3] * z[40];
z[106] = abb[22] * z[68];
z[79] = z[79] + -z[106];
z[107] = abb[24] + abb[25];
z[107] = z[68] * z[107];
z[108] = abb[11] * z[105];
z[109] = 2 * z[108];
z[25] = z[25] + -z[109];
z[110] = z[25] + z[74];
z[111] = -abb[5] * z[24];
z[112] = abb[4] + -abb[5];
z[113] = 2 * abb[60];
z[114] = z[112] * z[113];
z[26] = abb[8] * z[26];
z[26] = z[26] + -z[79] + z[84] + z[107] + z[110] + z[111] + z[114];
z[26] = abb[41] * z[26];
z[107] = prod_pow(abb[32], 2);
z[111] = -2 * abb[43] + -z[107];
z[114] = abb[60] + abb[63];
z[111] = z[111] * z[114];
z[115] = -abb[32] * abb[35];
z[115] = -abb[39] + -abb[44] + z[115];
z[116] = 2 * z[114];
z[115] = z[115] * z[116];
z[117] = abb[32] * z[116];
z[118] = -abb[31] * z[114];
z[117] = z[117] + z[118];
z[117] = abb[31] * z[117];
z[116] = abb[31] * z[116];
z[118] = -abb[33] * z[114];
z[116] = z[116] + z[118];
z[116] = abb[33] * z[116];
z[118] = abb[31] + -abb[35];
z[119] = abb[32] + -z[118];
z[119] = -abb[30] + 2 * z[119];
z[114] = abb[30] * z[114] * z[119];
z[111] = z[111] + z[114] + z[115] + z[116] + z[117];
z[111] = abb[9] * z[111];
z[114] = abb[3] * z[10];
z[52] = z[46] + z[52] + -z[59] + z[77] + z[114];
z[52] = abb[43] * z[52];
z[31] = abb[15] * z[31];
z[77] = z[31] + -z[84];
z[115] = abb[8] * abb[63];
z[116] = z[77] + z[115];
z[40] = abb[5] * z[40];
z[40] = z[40] + -z[116];
z[117] = abb[6] * z[33];
z[119] = abb[4] + -abb[6];
z[120] = abb[15] + z[119];
z[121] = abb[60] * z[120];
z[122] = abb[7] * z[94];
z[123] = abb[3] * abb[63];
z[117] = z[40] + z[117] + -z[121] + -z[122] + z[123];
z[73] = -z[73] + z[104];
z[121] = -z[73] + -z[117];
z[121] = abb[45] * z[121];
z[124] = abb[6] * abb[63];
z[105] = abb[61] + z[105];
z[105] = abb[7] * z[105];
z[125] = z[105] + z[124];
z[126] = -z[123] + z[125];
z[127] = abb[5] + abb[15];
z[127] = abb[60] * z[127];
z[128] = -z[31] + z[126] + -z[127];
z[129] = abb[5] * z[47];
z[130] = -abb[1] * z[47];
z[36] = z[36] + -z[115] + -z[128] + -z[129] + 2 * z[130];
z[36] = abb[44] * z[36];
z[27] = z[27] + z[89];
z[27] = abb[69] * z[27];
z[22] = -abb[64] + z[22] + z[50];
z[22] = abb[6] * z[22];
z[89] = abb[8] * z[94];
z[130] = abb[5] * z[10];
z[22] = z[22] + -z[77] + z[89] + -z[130];
z[77] = abb[3] * z[94];
z[89] = z[77] + -z[106];
z[94] = -abb[5] + z[8];
z[131] = -abb[15] + z[94];
z[131] = abb[60] * z[131];
z[104] = -z[22] + z[89] + -z[104] + z[105] + -z[131];
z[131] = -abb[24] + -z[102];
z[131] = z[68] * z[131];
z[131] = z[104] + z[131];
z[131] = abb[46] * z[131];
z[44] = abb[67] * z[44];
z[132] = -z[74] + z[92];
z[43] = -abb[60] * z[43];
z[43] = -z[37] + z[43] + -z[79] + -z[132];
z[43] = abb[39] * z[43];
z[79] = abb[6] * z[64];
z[56] = -z[56] + z[79];
z[79] = abb[3] + z[119];
z[79] = abb[60] * z[79];
z[133] = abb[3] * z[64];
z[79] = -z[12] + -z[56] + z[79] + z[133];
z[133] = -z[73] + z[79];
z[133] = abb[42] * z[133];
z[26] = z[26] + z[27] + z[36] + z[43] + -z[44] + z[52] + z[76] + z[111] + z[121] + z[131] + z[133];
z[27] = -2 * z[12];
z[36] = z[0] + -z[50];
z[36] = abb[61] + 2 * z[36] + -z[70];
z[36] = abb[6] * z[36];
z[43] = -z[1] + -z[33];
z[43] = z[28] * z[43];
z[44] = z[64] + -z[71];
z[44] = abb[5] * z[44];
z[1] = 4 * abb[62] + z[1] + z[69];
z[1] = abb[15] * z[1];
z[28] = -5 * abb[6] + 4 * abb[15] + -z[28] + -z[112];
z[28] = abb[60] * z[28];
z[52] = 4 * z[101];
z[47] = -abb[4] * z[47];
z[1] = z[1] + -z[11] + -10 * z[19] + -z[27] + z[28] + 8 * z[32] + z[36] + z[43] + z[44] + z[47] + -z[52] + -2 * z[80] + -z[109];
z[1] = abb[37] * z[1];
z[11] = abb[5] + abb[8];
z[11] = abb[61] * z[11];
z[28] = -abb[6] + abb[15];
z[36] = -abb[60] * z[28];
z[11] = z[11] + z[19] + -z[31] + -z[32] + z[36] + z[88] + z[101] + z[108];
z[11] = abb[34] * z[11];
z[1] = z[1] + 4 * z[11];
z[1] = abb[37] * z[1];
z[11] = abb[14] + -abb[17] + z[120];
z[11] = abb[60] * z[11];
z[36] = -abb[24] * z[68];
z[11] = z[11] + z[15] + -z[22] + z[36] + z[46] + z[59] + -2 * z[62] + -z[75] + -z[77] + z[80] + -z[122];
z[11] = abb[30] * z[11];
z[22] = 2 * abb[54] + -z[4] + -z[6];
z[36] = abb[22] * z[22];
z[36] = -z[27] + z[36] + -z[59] + 3 * z[62];
z[17] = abb[5] * z[17];
z[17] = -z[15] + z[17] + z[20];
z[9] = -z[0] + -z[9] + -z[71];
z[9] = abb[6] * z[9];
z[43] = abb[3] + -abb[8];
z[43] = z[43] * z[50];
z[44] = 2 * abb[15];
z[47] = -z[33] * z[44];
z[59] = -z[44] + z[65];
z[65] = abb[5] + abb[14];
z[69] = z[59] + z[65];
z[69] = abb[60] * z[69];
z[9] = z[9] + z[17] + -z[36] + z[43] + z[47] + z[52] + -z[67] + z[69] + -2 * z[105];
z[9] = abb[31] * z[9];
z[10] = z[8] * z[10];
z[10] = z[10] + z[73];
z[43] = z[59] + -z[90];
z[43] = abb[60] * z[43];
z[47] = abb[15] * z[24];
z[52] = abb[8] * z[61];
z[17] = z[10] + -z[12] + z[17] + z[43] + -z[47] + z[52] + z[106];
z[17] = abb[37] * z[17];
z[43] = abb[34] * z[104];
z[47] = abb[6] + -abb[14];
z[47] = abb[60] * z[47];
z[34] = z[34] + -z[47] + -z[62] + z[74];
z[34] = abb[32] * z[34];
z[43] = z[17] + -z[34] + z[43];
z[47] = -abb[17] + -z[65];
z[47] = abb[60] * z[47];
z[36] = z[15] + z[36] + z[47] + -z[53] + z[58] + -2 * z[74];
z[36] = abb[35] * z[36];
z[47] = -z[73] + z[78];
z[52] = -z[47] + z[117];
z[53] = -z[52] * z[91];
z[58] = -2 * z[98] + -z[99] + z[100];
z[58] = abb[23] * z[58];
z[22] = -z[22] * z[118];
z[59] = abb[37] * z[68];
z[61] = z[59] + -z[96];
z[22] = z[22] + 2 * z[61];
z[22] = abb[25] * z[22];
z[61] = z[59] + -z[98];
z[62] = z[61] * z[103];
z[9] = z[9] + z[11] + z[22] + z[36] + 2 * z[43] + z[53] + z[58] + z[62] + -z[82];
z[9] = abb[30] * z[9];
z[11] = abb[3] + z[29];
z[7] = 5 * abb[8] + -2 * abb[17] + z[7] + -z[11] + z[28];
z[7] = abb[60] * z[7];
z[22] = abb[3] * z[33];
z[7] = -z[7] + z[22] + -z[31] + z[105];
z[19] = -z[19] + z[37] + z[42] + z[46];
z[28] = abb[1] * z[2];
z[36] = 2 * abb[1] + -abb[14];
z[36] = abb[61] * z[36];
z[15] = z[15] + z[28] + z[36] + -z[60];
z[28] = abb[54] * (T(1) / T(3));
z[36] = abb[59] * (T(5) / T(3)) + -z[4] + (T(2) / T(3)) * z[6] + z[28];
z[37] = -abb[22] + -z[102];
z[36] = z[36] * z[37];
z[4] = 5 * z[4] + -4 * z[6];
z[4] = (T(1) / T(3)) * z[4] + -z[5] + -z[28];
z[4] = abb[24] * z[4];
z[5] = -4 * abb[63] + -abb[64] + -z[70];
z[5] = (T(1) / T(3)) * z[5] + -z[55];
z[5] = abb[6] * z[5];
z[0] = abb[62] * (T(5) / T(3)) + -z[0] + (T(2) / T(3)) * z[39];
z[0] = abb[8] * z[0];
z[2] = -5 * abb[63] + -abb[64] + -z[2];
z[2] = -abb[61] + (T(1) / T(3)) * z[2];
z[2] = abb[5] * z[2];
z[0] = z[0] + z[2] + z[4] + z[5] + (T(-1) / T(3)) * z[7] + (T(2) / T(3)) * z[15] + (T(-4) / T(3)) * z[19] + (T(8) / T(3)) * z[35] + z[36] + -z[49];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[2] = z[55] + z[64];
z[2] = abb[8] * z[2];
z[4] = z[55] + -z[64];
z[4] = abb[5] * z[4];
z[5] = -abb[4] + abb[8] + -z[44] + z[94];
z[5] = abb[60] * z[5];
z[2] = z[2] + z[4] + z[5] + z[10] + z[20] + -2 * z[31] + -z[49] + z[81] + z[109];
z[2] = abb[37] * z[2];
z[4] = abb[35] * z[87];
z[5] = abb[3] + -abb[15];
z[5] = abb[60] * z[5];
z[5] = z[5] + -z[12] + z[22] + z[40] + z[125];
z[5] = abb[31] * z[5];
z[6] = abb[61] + -abb[62];
z[6] = -abb[5] * z[6];
z[6] = z[6] + -z[109] + z[115] + -z[128];
z[6] = abb[34] * z[6];
z[7] = abb[37] * z[13];
z[10] = -abb[35] * z[13];
z[10] = z[7] + z[10];
z[10] = abb[24] * z[10];
z[2] = z[2] + z[4] + z[5] + z[6] + z[10];
z[4] = -abb[36] * z[52];
z[2] = 2 * z[2] + z[4];
z[2] = abb[36] * z[2];
z[4] = abb[60] + z[33];
z[4] = abb[7] * z[4];
z[4] = z[4] + z[124];
z[5] = -abb[15] * z[33];
z[5] = z[4] + z[5] + z[84] + -z[115] + -z[123] + -z[127] + -z[130];
z[5] = abb[34] * z[5];
z[6] = abb[5] * abb[60];
z[6] = -z[6] + z[45] + z[132];
z[10] = abb[32] * z[6];
z[5] = z[5] + z[10] + -z[17];
z[10] = abb[35] * z[54];
z[15] = abb[15] + z[29];
z[15] = abb[60] * z[15];
z[15] = z[15] + -z[92] + z[116] + -z[126] + z[130];
z[15] = abb[31] * z[15];
z[17] = abb[35] * z[68];
z[17] = z[17] + -z[59];
z[17] = z[17] * z[103];
z[5] = 2 * z[5] + z[10] + z[15] + z[17];
z[5] = abb[31] * z[5];
z[10] = -abb[70] * z[16];
z[15] = -abb[61] + abb[64];
z[15] = abb[8] * z[15];
z[16] = -abb[5] + abb[15] + z[30];
z[16] = abb[60] * z[16];
z[4] = -z[4] + z[15] + z[16] + z[25] + z[31] + -z[89] + -z[129];
z[15] = prod_pow(abb[34], 2);
z[4] = z[4] * z[15];
z[16] = -abb[68] * z[14];
z[17] = abb[43] * z[13];
z[19] = z[16] + z[17];
z[25] = abb[60] + z[51];
z[25] = abb[47] * z[25];
z[25] = z[19] + -z[25];
z[28] = -2 * z[61] + z[100];
z[28] = abb[31] * z[28];
z[31] = abb[42] + abb[45];
z[33] = abb[35] * abb[37];
z[33] = z[31] + z[33];
z[35] = 2 * z[13];
z[33] = z[33] * z[35];
z[36] = z[68] * z[107];
z[37] = prod_pow(abb[37], 2);
z[13] = -z[13] * z[37];
z[39] = abb[67] * z[72];
z[40] = abb[39] * z[72];
z[13] = z[13] + -2 * z[25] + z[28] + z[33] + z[36] + -z[39] + z[40];
z[13] = abb[25] * z[13];
z[25] = abb[35] * z[63];
z[28] = -abb[60] * z[11];
z[12] = z[12] + z[28] + z[93] + -z[114];
z[12] = abb[31] * z[12];
z[28] = -z[47] + -z[79];
z[28] = abb[36] * z[28];
z[12] = z[12] + z[25] + z[28];
z[25] = 2 * abb[3] + z[29] + z[119];
z[25] = abb[60] * z[25];
z[22] = 2 * z[22] + z[25] + z[27] + z[47] + -z[56] + -z[93];
z[22] = abb[33] * z[22];
z[12] = 2 * z[12] + z[22];
z[12] = abb[33] * z[12];
z[22] = abb[6] * z[24];
z[24] = -z[24] + -z[113];
z[24] = abb[7] * z[24];
z[8] = abb[60] * z[8];
z[8] = z[8] + z[22] + -z[23] + z[24] + z[41] + z[106];
z[8] = 2 * z[8];
z[8] = abb[40] * z[8];
z[18] = -2 * z[18] + z[20] + -12 * z[32];
z[18] = abb[68] * z[18];
z[20] = z[30] + z[86];
z[20] = abb[60] * z[20];
z[20] = z[20] + -z[21] + -z[66] + z[83] + z[110];
z[20] = abb[37] * z[20];
z[21] = z[48] + z[85] + -z[110];
z[21] = abb[34] * z[21];
z[20] = z[20] + z[21] + z[34];
z[21] = 2 * abb[35];
z[20] = z[20] * z[21];
z[22] = abb[40] * z[68];
z[16] = -z[16] + z[22];
z[15] = z[15] * z[68];
z[23] = -z[35] * z[37];
z[7] = z[7] + -z[97];
z[7] = z[7] * z[21];
z[24] = abb[47] * z[50];
z[7] = z[7] + z[15] + 2 * z[16] + z[23] + z[24] + -z[39];
z[7] = abb[24] * z[7];
z[15] = abb[47] * abb[63];
z[15] = z[15] + -z[19] + z[22];
z[16] = -z[21] * z[95];
z[19] = z[31] * z[35];
z[14] = -z[14] * z[37];
z[21] = abb[31] * z[100];
z[14] = z[14] + 2 * z[15] + z[16] + z[19] + z[21] + -z[39];
z[14] = abb[23] * z[14];
z[15] = abb[66] * z[38];
z[6] = -z[6] * z[107];
z[16] = -2 * z[17] + z[24];
z[16] = abb[22] * z[16];
z[11] = -abb[4] + z[11];
z[11] = abb[43] * z[11];
z[17] = -abb[70] * z[57];
z[11] = 2 * z[11] + z[17];
z[11] = abb[60] * z[11];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + z[18] + z[20] + 2 * z[26];
z[0] = 2 * z[0];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_708_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("-5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("-10.4023028191312333362719401831913221365702768824772793193192573969"),stof<T>("-4.0099607112369939801253103352120106710102959542432765442127783144")}, std::complex<T>{stof<T>("-5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("-5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("-1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("-1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("-20.462455036408463729808733224948042501941452267068667608161763499"),stof<T>("6.089428100462557475767236822789473687138061043101092996314176422")}, std::complex<T>{stof<T>("-16.696773872606450352518356648377384318328992133872749094203137748"),stof<T>("-36.46547452212490451670783451114917658350120343027364657297047016")}, std::complex<T>{stof<T>("-16.567913731632433249499466392282724728612136552544704134525437244"),stof<T>("-35.843059862644944971793544257796964795091316595581974737669397166")}, std::complex<T>{stof<T>("-22.654164929948585638211946740953033747896296345994313018361085116"),stof<T>("-75.54408695380354582728424912221723787090618345078168742786077269")}, std::complex<T>{stof<T>("-14.897827988788396353183658562684984226756992194539273163699551436"),stof<T>("-37.603689858958450862522766595083803819138105860622441272503833676")}, std::complex<T>{stof<T>("0.332917245879767264462317345140113563477793260053919545549091759"),stof<T>("15.009482051475847810446723249506914494066987341766728719549045047")}, std::complex<T>{stof<T>("-0.051881463697184215754529699493716126989310907775594881303416497"),stof<T>("25.098197810448281517736238067280671084815067568294361227844109094")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_708_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_708_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("4.8390887666948276009127427655288467457089182538746770689892713919"),stof<T>("-9.9247761861834175663192947100875963928828506255281830520561068422")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,71> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W23(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_6(k), f_1_7(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_19(k), f_2_21(k), f_2_28_im(k), f_2_4_im(k), f_2_6_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_4_re(k), f_2_6_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k)};

                    
            return f_4_708_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_708_DLogXconstant_part(base_point<T>, kend);
	value += f_4_708_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_708_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_708_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_708_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_708_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_708_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_708_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
