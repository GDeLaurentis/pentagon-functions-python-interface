/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_217.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_217_abbreviated (const std::array<T,17>& abb) {
T z[13];
z[0] = abb[13] + abb[14];
z[0] = abb[0] * z[0];
z[1] = abb[13] + abb[14] * (T(-1) / T(3));
z[1] = abb[2] * z[1];
z[1] = (T(13) / T(3)) * z[0] + z[1];
z[2] = abb[10] + abb[12];
z[3] = -abb[11] + 2 * abb[15] + (T(-3) / T(2)) * z[2];
z[4] = abb[4] * z[3];
z[5] = abb[1] * abb[13];
z[6] = -abb[3] * z[3];
z[1] = (T(1) / T(2)) * z[1] + -z[4] + (T(-2) / T(3)) * z[5] + z[6];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[6] = abb[0] + abb[2];
z[7] = -abb[15] + abb[11] * (T(1) / T(2)) + (T(3) / T(4)) * z[2];
z[6] = z[6] * z[7];
z[8] = abb[1] * z[3];
z[2] = -2 * abb[11] + 4 * abb[15] + -3 * z[2];
z[9] = abb[5] * z[2];
z[10] = abb[3] * abb[13];
z[6] = 3 * z[6] + -z[8] + z[9] + (T(3) / T(4)) * z[10];
z[8] = abb[16] * z[6];
z[9] = (T(1) / T(2)) * z[5];
z[10] = abb[13] * (T(1) / T(2));
z[11] = -abb[0] * z[10];
z[12] = abb[2] * abb[14];
z[4] = -z[4] + z[9] + z[11] + 2 * z[12];
z[4] = abb[7] * z[4];
z[11] = abb[0] * abb[13];
z[12] = abb[2] * abb[13];
z[11] = z[11] + z[12];
z[7] = abb[3] * z[7];
z[7] = z[7] + z[9] + (T(1) / T(4)) * z[11];
z[11] = abb[8] * z[7];
z[4] = z[4] + z[11];
z[4] = abb[7] * z[4];
z[11] = abb[3] + abb[4];
z[3] = z[3] * z[11];
z[10] = -abb[2] * z[10];
z[3] = -2 * z[0] + z[3] + z[9] + z[10];
z[3] = abb[6] * z[3];
z[9] = abb[7] + -abb[8];
z[9] = z[7] * z[9];
z[3] = z[3] + -z[9];
z[3] = abb[6] * z[3];
z[7] = prod_pow(abb[8], 2) * z[7];
z[1] = z[1] + z[3] + z[4] + z[7] + z[8];
z[2] = -z[2] * z[11];
z[0] = 4 * z[0] + z[2] + -z[5] + z[12];
z[0] = abb[6] * z[0];
z[0] = z[0] + z[9];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[9] * z[6];
z[0] = z[0] + z[2];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_217_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("13.96415643147508826922384977244427510120715403847038606878385454"),stof<T>("-20.369548172423014954390302153134024158599942098253563196717532818")}, std::complex<T>{stof<T>("9.309437620983392179482566514962850067471436025646924045855903027"),stof<T>("-13.579698781615343302926868102089349439066628065502375464478355212")}, std::complex<T>{stof<T>("13.96415643147508826922384977244427510120715403847038606878385454"),stof<T>("-20.369548172423014954390302153134024158599942098253563196717532818")}, std::complex<T>{stof<T>("0.424591964293097211454670569656800591547894339290861810296853344"),stof<T>("-24.706523156059477937943070735346206320370658076696757918422412556")}, std::complex<T>{stof<T>("-0.918164975983566437662010803917862315779212184465581150959880746"),stof<T>("-25.838563900249850146230262555917829335187740471570923078453781753")}, std::complex<T>{stof<T>("-18.618875241966784358965133029925700134942872051293848091711806054"),stof<T>("27.159397563230686605853736204178698878133256131004750928956710424")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[196].real()/kbase.W[196].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_217_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_217_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-26.248324141008097494001323915310423913458637594008523829881447127"),stof<T>("5.734987892863597046223653678781769770907595712109748624107224358")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,17> abb = {dl[0], dlog_W8(k,dl), dl[3], dlog_W120(k,dv), dlog_W121(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_7(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[196].real()/k.W[196].real()), f_2_24_re(k)};

                    
            return f_4_217_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_217_DLogXconstant_part(base_point<T>, kend);
	value += f_4_217_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_217_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_217_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_217_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_217_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_217_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_217_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
