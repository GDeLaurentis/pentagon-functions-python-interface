/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_714.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_714_abbreviated (const std::array<T,39>& abb) {
T z[100];
z[0] = 3 * abb[8];
z[1] = 2 * abb[1];
z[2] = z[0] + z[1];
z[3] = 2 * abb[4];
z[4] = abb[3] + z[3];
z[5] = 3 * abb[0];
z[6] = z[2] + -z[4] + -z[5];
z[6] = abb[31] * z[6];
z[7] = abb[0] + abb[3];
z[8] = -abb[8] + z[7];
z[9] = 4 * abb[29];
z[10] = z[8] * z[9];
z[11] = -abb[3] + z[3];
z[12] = abb[0] + abb[8];
z[13] = z[11] + -z[12];
z[14] = abb[27] * z[13];
z[15] = 2 * abb[30];
z[16] = -abb[3] + abb[8];
z[17] = z[15] * z[16];
z[18] = 2 * abb[27];
z[19] = 2 * abb[31];
z[20] = -abb[25] + 3 * abb[26] + z[18] + -z[19];
z[20] = abb[5] * z[20];
z[21] = abb[34] + abb[35];
z[22] = abb[33] + z[21];
z[23] = abb[11] * z[22];
z[24] = abb[12] * z[22];
z[8] = abb[28] * z[8];
z[25] = -abb[0] * abb[25];
z[26] = -abb[30] * z[1];
z[27] = -abb[4] + abb[8];
z[28] = 2 * z[27];
z[29] = abb[0] + -z[28];
z[29] = abb[26] * z[29];
z[6] = z[6] + z[8] + z[10] + z[14] + z[17] + z[20] + z[23] + -z[24] + z[25] + z[26] + z[29];
z[6] = abb[19] * z[6];
z[8] = 4 * abb[7];
z[14] = abb[3] + z[8];
z[17] = 2 * abb[2];
z[20] = 4 * abb[6];
z[25] = -z[12] + z[14] + -z[17] + z[20];
z[25] = abb[31] * z[25];
z[26] = 2 * abb[3];
z[29] = abb[0] + z[26];
z[30] = abb[4] + z[29];
z[31] = 3 * abb[6];
z[32] = 3 * abb[7];
z[33] = z[30] + z[31] + z[32];
z[34] = 2 * abb[32];
z[33] = z[33] * z[34];
z[35] = 3 * abb[3];
z[36] = z[8] + z[35];
z[37] = -abb[0] + z[3];
z[38] = abb[8] + -z[36] + z[37];
z[38] = abb[28] * z[38];
z[4] = z[4] + z[12];
z[4] = abb[27] * z[4];
z[39] = -abb[4] + abb[7];
z[40] = -z[16] + z[39];
z[40] = z[15] * z[40];
z[41] = abb[7] + -abb[8];
z[42] = abb[0] + z[41];
z[43] = 2 * abb[26];
z[42] = z[42] * z[43];
z[44] = 2 * abb[7];
z[45] = -abb[8] + z[44];
z[46] = abb[0] + -abb[4];
z[47] = z[45] + z[46];
z[48] = z[26] + z[47];
z[49] = 2 * abb[29];
z[48] = z[48] * z[49];
z[50] = abb[28] + abb[29];
z[51] = -abb[27] + -abb[30] + z[50];
z[52] = 2 * abb[6];
z[53] = z[51] * z[52];
z[54] = 4 * abb[9];
z[55] = abb[27] + z[50];
z[56] = abb[31] + -3 * abb[32] + z[55];
z[54] = z[54] * z[56];
z[57] = z[23] + z[54];
z[58] = z[17] * z[50];
z[4] = z[4] + -z[24] + z[25] + -z[33] + -z[38] + -z[40] + z[42] + z[48] + -z[53] + -z[57] + z[58];
z[25] = abb[36] * z[4];
z[38] = 6 * abb[7];
z[40] = 2 * abb[0];
z[42] = abb[4] + z[40];
z[48] = z[16] + -z[38] + z[42] + -z[52];
z[48] = abb[31] * z[48];
z[59] = 4 * abb[8];
z[60] = z[26] + z[39] + -z[59];
z[60] = abb[30] * z[60];
z[61] = z[7] + z[41];
z[61] = z[9] * z[61];
z[62] = -z[1] + z[42];
z[63] = 2 * abb[8];
z[64] = -abb[7] + z[63];
z[65] = -z[62] + z[64];
z[65] = abb[26] * z[65];
z[60] = -z[60] + z[61] + -z[65];
z[61] = 3 * abb[4];
z[45] = -abb[3] + z[45] + -z[61];
z[45] = abb[27] * z[45];
z[22] = abb[10] * z[22];
z[65] = z[22] + z[24];
z[45] = z[45] + z[65];
z[66] = -z[27] + z[36] + z[40];
z[66] = abb[28] * z[66];
z[67] = abb[27] * z[20];
z[57] = z[57] + -z[67];
z[33] = z[33] + z[57];
z[48] = z[33] + z[45] + z[48] + -z[60] + -z[66];
z[66] = abb[37] * z[48];
z[6] = -z[6] + z[25] + z[66];
z[25] = abb[4] + z[63];
z[66] = 7 * abb[7] + z[5] + -z[25] + z[26] + z[31];
z[66] = z[34] * z[66];
z[68] = -abb[27] + abb[31];
z[69] = -abb[26] + z[68];
z[70] = 2 * abb[5];
z[69] = z[69] * z[70];
z[71] = -z[58] + z[69];
z[67] = -z[67] + z[71];
z[72] = z[44] + -z[63];
z[73] = z[3] + z[7];
z[74] = -z[72] + -z[73];
z[74] = abb[27] * z[74];
z[75] = 5 * abb[7];
z[76] = z[26] + z[75];
z[77] = 6 * abb[8];
z[78] = z[61] + z[76] + -z[77];
z[78] = abb[30] * z[78];
z[79] = 8 * abb[7];
z[80] = -z[11] + z[79];
z[81] = z[17] + -z[52];
z[82] = abb[0] + z[81];
z[83] = -z[80] + z[82];
z[83] = abb[31] * z[83];
z[62] = -z[32] + z[59] + -z[62];
z[62] = abb[26] * z[62];
z[84] = z[7] + -z[63];
z[85] = -z[32] + -z[84];
z[85] = abb[28] * z[85];
z[86] = abb[0] + -abb[3];
z[87] = 4 * abb[4];
z[88] = z[86] + z[87];
z[88] = abb[25] * z[88];
z[89] = -abb[3] + -z[72];
z[89] = -z[5] + 2 * z[89];
z[89] = z[49] * z[89];
z[90] = abb[11] * z[21];
z[54] = z[54] + z[62] + z[66] + z[67] + z[74] + z[78] + z[83] + 2 * z[85] + z[88] + z[89] + z[90];
z[54] = abb[15] * z[54];
z[62] = abb[26] * z[28];
z[62] = -z[23] + z[62] + z[71];
z[66] = abb[8] + z[46];
z[71] = -z[52] + z[66];
z[74] = z[34] * z[71];
z[55] = -abb[30] + z[55];
z[78] = z[52] * z[55];
z[83] = abb[30] * z[28];
z[74] = z[74] + z[78] + z[83];
z[78] = -z[81] + -z[84];
z[78] = abb[31] * z[78];
z[84] = -z[3] + z[7];
z[85] = z[63] + z[84];
z[85] = abb[27] * z[85];
z[86] = -z[63] + -z[86];
z[86] = abb[28] * z[86];
z[88] = abb[3] + abb[4];
z[89] = -z[63] + z[88];
z[89] = z[49] * z[89];
z[78] = -z[62] + z[74] + z[78] + -z[85] + z[86] + z[89];
z[78] = abb[17] * z[78];
z[54] = z[54] + 2 * z[78];
z[54] = abb[15] * z[54];
z[78] = abb[3] + z[37] + -z[59] + -z[81];
z[78] = abb[31] * z[78];
z[81] = -abb[28] * z[84];
z[84] = abb[3] + -abb[4];
z[86] = -z[49] * z[84];
z[89] = abb[26] * z[40];
z[23] = -z[23] + z[53] + z[58] + z[69] + z[78] + z[81] + z[83] + z[85] + z[86] + z[89];
z[23] = prod_pow(abb[17], 2) * z[23];
z[53] = -z[59] + z[79];
z[7] = z[1] + -z[7] + -z[52] + -z[53];
z[7] = abb[31] * z[7];
z[58] = z[72] + -z[73];
z[58] = abb[27] * z[58];
z[72] = abb[4] + z[26];
z[78] = -z[32] + -z[63] + z[72];
z[78] = abb[30] * z[78];
z[81] = abb[25] * z[40];
z[83] = abb[3] * z[9];
z[81] = z[81] + z[83];
z[83] = z[35] + z[38];
z[37] = z[37] + -z[83];
z[37] = abb[28] * z[37];
z[85] = abb[25] + -abb[28];
z[86] = 3 * abb[30];
z[89] = -2 * z[85] + -z[86];
z[89] = z[1] * z[89];
z[90] = -abb[7] + z[42];
z[91] = 4 * abb[1];
z[92] = -z[90] + -z[91];
z[92] = abb[26] * z[92];
z[93] = abb[25] + -z[68];
z[93] = z[70] * z[93];
z[7] = z[7] + z[33] + z[37] + z[58] + z[78] + -z[81] + z[89] + z[92] + z[93];
z[7] = abb[16] * z[7];
z[33] = z[5] + -z[63] + z[83];
z[33] = abb[28] * z[33];
z[37] = abb[8] + -z[31] + -z[40] + -z[76];
z[37] = z[34] * z[37];
z[58] = -abb[0] + z[52] + -z[63] + z[80];
z[58] = abb[31] * z[58];
z[73] = abb[27] * z[73];
z[33] = z[33] + z[37] + -z[57] + z[58] + z[60] + z[73];
z[33] = abb[15] * z[33];
z[37] = 5 * abb[8];
z[58] = -z[1] + -z[37] + z[38] + z[46];
z[58] = abb[31] * z[58];
z[60] = -abb[26] + abb[30];
z[60] = z[1] * z[60];
z[73] = -abb[28] + z[34];
z[76] = -z[47] * z[73];
z[78] = -z[15] * z[41];
z[80] = abb[27] * z[66];
z[58] = z[58] + z[60] + z[65] + z[69] + z[76] + z[78] + z[80];
z[58] = abb[14] * z[58];
z[60] = -abb[27] + -abb[31] + z[73];
z[60] = z[47] * z[60];
z[60] = z[60] + -z[65];
z[60] = abb[18] * z[60];
z[33] = z[33] + z[58] + z[60];
z[7] = z[7] + 2 * z[33];
z[7] = abb[16] * z[7];
z[6] = -2 * z[6] + z[7] + z[23] + z[54];
z[7] = -abb[8] + z[87];
z[7] = -abb[0] + 4 * z[7];
z[7] = abb[29] * z[7];
z[23] = abb[2] * z[50];
z[33] = 13 * abb[0] + -z[39];
z[33] = 35 * abb[1] + 2 * z[33];
z[33] = abb[26] * z[33];
z[7] = z[7] + z[23] + z[33];
z[23] = 5 * abb[0];
z[33] = abb[7] * (T(-14) / T(3)) + abb[2] * (T(-1) / T(3)) + abb[1] * (T(13) / T(3)) + abb[6] * (T(26) / T(3)) + -z[23] + z[26] + z[61];
z[33] = z[19] * z[33];
z[54] = 5 * abb[3];
z[58] = abb[0] * (T(-40) / T(3)) + abb[4] * (T(-1) / T(3)) + z[8] + z[54];
z[58] = abb[28] * z[58];
z[60] = abb[8] + -z[3] + z[75];
z[60] = abb[30] * z[60];
z[65] = 5 * abb[4];
z[69] = z[26] + z[65];
z[73] = -7 * abb[6] + abb[7] + z[5] + -z[69];
z[76] = 4 * abb[32];
z[73] = z[73] * z[76];
z[78] = 5 * abb[33];
z[83] = 4 * z[21];
z[87] = z[78] + z[83];
z[89] = -abb[11] * z[87];
z[56] = abb[9] * z[56];
z[55] = abb[6] * z[55];
z[92] = 6 * abb[0] + -abb[3] + abb[4] * (T(-5) / T(3));
z[92] = abb[25] * z[92];
z[93] = abb[3] + abb[7] * (T(-2) / T(3)) + abb[0] * (T(-1) / T(3)) + abb[4] * (T(4) / T(3));
z[93] = abb[27] * z[93];
z[94] = abb[1] * z[85];
z[95] = -19 * abb[33] + -16 * z[21];
z[95] = abb[10] * z[95];
z[7] = (T(2) / T(3)) * z[7] + z[33] + (T(32) / T(3)) * z[55] + -8 * z[56] + z[58] + (T(8) / T(3)) * z[60] + z[73] + z[89] + z[92] + 4 * z[93] + (T(26) / T(3)) * z[94] + (T(1) / T(3)) * z[95];
z[7] = prod_pow(m1_set::bc<T>[0], 2) * z[7];
z[33] = -z[3] + -z[5] + -z[35] + z[37];
z[33] = z[9] * z[33];
z[58] = -12 * abb[7] + -z[20] + z[23] + z[69] + z[77];
z[58] = z[19] * z[58];
z[60] = 9 * abb[4];
z[54] = 12 * abb[8] + -z[54] + z[60] + -z[79];
z[54] = abb[28] * z[54];
z[69] = z[29] + -z[61] + z[63];
z[69] = z[18] * z[69];
z[73] = 2 * z[21];
z[89] = abb[33] + z[73];
z[89] = abb[10] * z[89];
z[42] = abb[3] + z[42];
z[42] = abb[25] * z[42];
z[89] = -z[42] + z[89];
z[92] = 4 * z[24];
z[93] = z[89] + z[92];
z[94] = z[44] + z[88];
z[37] = -z[37] + z[94];
z[95] = 4 * abb[30];
z[37] = z[37] * z[95];
z[96] = 16 * abb[32];
z[97] = abb[6] + z[41];
z[97] = z[96] * z[97];
z[98] = 3 * abb[33];
z[83] = -z[83] + -z[98];
z[83] = abb[11] * z[83];
z[55] = 8 * z[55];
z[99] = -abb[0] + z[27];
z[99] = abb[26] * z[99];
z[33] = z[33] + z[37] + z[54] + -z[55] + z[58] + z[69] + z[83] + -z[93] + z[97] + 8 * z[99];
z[33] = abb[15] * z[33];
z[37] = abb[27] + 3 * z[50] + -z[86];
z[37] = z[20] * z[37];
z[50] = abb[8] + z[26];
z[54] = -z[50] + z[61];
z[54] = z[9] * z[54];
z[58] = -z[40] + z[88];
z[58] = abb[25] * z[58];
z[69] = z[73] + z[98];
z[73] = abb[11] * z[69];
z[37] = z[37] + z[54] + -z[58] + -z[73];
z[54] = 6 * abb[6];
z[58] = 4 * abb[0];
z[73] = -z[1] + -z[54] + z[58] + z[77] + -z[88];
z[73] = z[19] * z[73];
z[77] = 8 * abb[32];
z[83] = z[71] * z[77];
z[83] = z[83] + z[92];
z[86] = abb[1] + -z[63] + -z[84];
z[86] = z[86] * z[95];
z[92] = 10 * abb[0] + abb[3] + z[59] + -z[61];
z[92] = abb[28] * z[92];
z[97] = -z[18] * z[84];
z[69] = abb[10] * z[69];
z[98] = 4 * abb[26];
z[99] = -abb[1] + -z[5];
z[99] = z[98] * z[99];
z[43] = -abb[25] + z[43] + -z[68];
z[68] = 4 * abb[5];
z[43] = z[43] * z[68];
z[43] = -z[37] + z[43] + z[69] + z[73] + -z[83] + z[86] + z[92] + z[97] + z[99];
z[43] = abb[14] * z[43];
z[69] = abb[31] * z[71];
z[22] = z[22] + -z[24];
z[66] = abb[28] * z[66];
z[71] = abb[29] * z[28];
z[66] = -z[22] + -z[66] + -z[69] + -z[71] + z[74] + -z[80];
z[69] = 4 * abb[17];
z[66] = z[66] * z[69];
z[33] = z[33] + z[43] + z[66];
z[33] = abb[14] * z[33];
z[30] = -z[0] + z[30] + z[38] + z[54];
z[30] = abb[32] * z[30];
z[43] = z[8] + z[46];
z[26] = z[0] + -z[26] + -z[43];
z[26] = abb[28] * z[26];
z[29] = z[0] + -z[29] + -z[44];
z[29] = z[29] * z[49];
z[69] = -z[8] + -z[27] + z[82];
z[69] = abb[31] * z[69];
z[64] = abb[3] + -z[64];
z[15] = z[15] * z[64];
z[2] = -abb[0] + -abb[4] + z[2] + -z[44];
z[2] = abb[26] * z[2];
z[28] = abb[27] * z[28];
z[2] = z[2] + z[15] + z[26] + z[28] + z[29] + z[30] + 2 * z[56] + z[67] + z[69];
z[2] = abb[21] * z[2];
z[0] = -z[0] + z[17] + z[88];
z[0] = abb[31] * z[0];
z[15] = z[16] * z[49];
z[17] = abb[4] + z[16];
z[17] = abb[28] * z[17];
z[26] = abb[3] + z[27];
z[28] = abb[27] * z[26];
z[0] = z[0] + z[15] + z[17] + -z[22] + z[28] + z[62];
z[0] = abb[20] * z[0];
z[0] = z[0] + z[2];
z[2] = z[18] + z[19];
z[15] = z[43] + -z[63];
z[15] = z[2] * z[15];
z[17] = z[58] + z[84];
z[22] = z[17] + z[53];
z[22] = abb[28] * z[22];
z[28] = -abb[3] + z[27];
z[28] = z[28] * z[95];
z[29] = -z[47] * z[77];
z[30] = abb[11] * abb[33];
z[10] = z[10] + z[15] + z[22] + z[28] + z[29] + -z[30] + z[93];
z[10] = abb[15] * z[10];
z[15] = z[9] * z[13];
z[22] = z[26] * z[95];
z[15] = z[15] + z[22] + z[30] + z[55];
z[17] = -z[17] + -z[59];
z[17] = abb[28] * z[17];
z[22] = z[46] + z[63];
z[28] = -z[18] * z[22];
z[22] = z[20] + -z[22];
z[22] = z[19] * z[22];
z[17] = z[15] + z[17] + z[22] + z[28] + z[83] + -z[89];
z[17] = abb[14] * z[17];
z[22] = z[46] * z[85];
z[28] = -abb[10] * abb[33];
z[22] = z[22] + z[28];
z[22] = abb[18] * z[22];
z[10] = z[10] + z[17] + 2 * z[22] + -z[66];
z[10] = abb[18] * z[10];
z[17] = z[9] + 3 * z[85] + -z[95];
z[17] = abb[11] * z[17];
z[22] = abb[4] + -4 * abb[13] + z[35] + z[58];
z[22] = abb[33] * z[22];
z[2] = 3 * abb[25] + -abb[28] + z[2] + -z[95];
z[2] = abb[10] * z[2];
z[28] = -abb[29] + abb[30];
z[29] = 4 * abb[12];
z[28] = z[28] * z[29];
z[29] = z[21] * z[46];
z[2] = z[2] + z[17] + z[22] + -z[28] + 2 * z[29];
z[17] = -abb[38] * z[2];
z[0] = 4 * z[0] + 2 * z[6] + z[7] + z[10] + z[17] + z[33];
z[0] = 2 * z[0];
z[6] = 12 * abb[0];
z[7] = -7 * abb[3] + -16 * abb[7] + -z[6] + -z[59] + -z[60];
z[7] = abb[28] * z[7];
z[10] = abb[6] + z[32] + z[40] + z[50];
z[10] = z[10] * z[77];
z[13] = -z[8] + z[13];
z[9] = z[9] * z[13];
z[13] = 4 * abb[3] + z[5] + z[25];
z[17] = -z[13] * z[18];
z[11] = -z[11] + -z[41];
z[11] = z[11] * z[95];
z[22] = 8 * abb[6];
z[22] = z[22] * z[51];
z[13] = -z[8] + -z[13];
z[13] = z[13] * z[19];
z[25] = z[1] + -z[39];
z[25] = z[25] * z[98];
z[28] = 7 * abb[33] + 8 * z[21];
z[28] = abb[11] * z[28];
z[7] = z[7] + z[9] + z[10] + z[11] + z[13] + z[17] + z[22] + z[25] + z[28] + 16 * z[56] + z[93];
z[7] = abb[15] * z[7];
z[3] = z[3] + -z[16] + -z[23] + -z[38] + z[54] + z[91];
z[3] = z[3] * z[19];
z[6] = -z[6] + -z[14] + -z[63] + z[65];
z[6] = abb[28] * z[6];
z[9] = -abb[3] + z[12];
z[9] = -z[9] * z[18];
z[10] = abb[7] + z[26];
z[10] = z[10] * z[95];
z[11] = -z[5] + z[61];
z[12] = z[11] + z[20];
z[13] = abb[8] + -z[12] + z[44];
z[13] = z[13] * z[76];
z[14] = -abb[10] * z[87];
z[1] = z[1] + z[5];
z[1] = z[1] * z[98];
z[5] = -abb[25] + abb[26];
z[16] = -z[5] * z[68];
z[17] = abb[1] * abb[30];
z[1] = z[1] + z[3] + z[6] + z[9] + z[10] + z[13] + z[14] + z[16] + -8 * z[17] + 2 * z[24] + z[37];
z[1] = abb[14] * z[1];
z[3] = z[27] + z[36];
z[3] = abb[28] * z[3];
z[6] = -z[72] + z[75];
z[6] = abb[30] * z[6];
z[9] = abb[30] + z[85];
z[9] = z[9] * z[91];
z[10] = -abb[7] + -abb[8] + -z[31] + -2 * z[88];
z[10] = z[10] * z[34];
z[13] = abb[8] + z[52] + z[94];
z[13] = abb[31] * z[13];
z[5] = z[5] * z[70];
z[14] = 6 * abb[1] + z[90];
z[14] = abb[26] * z[14];
z[3] = z[3] + z[5] + z[6] + z[9] + z[10] + z[13] + z[14] + -z[45] + -z[57] + z[81];
z[3] = abb[16] * z[3];
z[5] = 8 * abb[0] + abb[3] + -z[65] + z[79];
z[5] = abb[28] * z[5];
z[6] = -z[8] + z[11];
z[6] = -z[6] * z[18];
z[9] = abb[6] + -abb[7] + -z[46];
z[9] = z[9] * z[96];
z[8] = z[8] + -z[12];
z[8] = z[8] * z[19];
z[10] = 6 * z[21] + z[78];
z[10] = abb[10] * z[10];
z[5] = z[5] + z[6] + z[8] + z[9] + z[10] + -z[15] + -z[42];
z[5] = abb[18] * z[5];
z[1] = 2 * z[1] + 4 * z[3] + z[5] + z[7] + -z[66];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = -abb[22] * z[4];
z[4] = -abb[23] * z[48];
z[3] = z[3] + z[4];
z[2] = -abb[24] * z[2];
z[1] = z[1] + z[2] + 4 * z[3];
z[1] = 2 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_714_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("85.82822959517301995078194075540569081690467905248014305106918761"),stof<T>("-366.43469794955730345844814666486935900427346815089729014825310729")}, std::complex<T>{stof<T>("232.18735846025921327567013477702508848363922227496353877722946668"),stof<T>("-823.28416564635633107893853281595104419078496080655446182045962663")}, std::complex<T>{stof<T>("-126.11753992477710467166202521111767537498211495474425056915864937"),stof<T>("88.96363152578016165881121951381556885721771681265924607429383083")}, std::complex<T>{stof<T>("93.85589216423002651182880577469059468519922103990123909442774407"),stof<T>("609.97011300738261028666790827059220779398129628538179217229221387")}, std::complex<T>{stof<T>("-58.40535519430817132563839452376319890757321082250547804238438999"),stof<T>("134.10500528781477266732047825680726792267244666368546132175184221")}, std::complex<T>{stof<T>("384.10914853743112808016389538318408834666528638499415049169738852"),stof<T>("17.39048144276290153731999166414718397513742991027880698384190062")}, std::complex<T>{stof<T>("-34.75890261420412643425156483659560672383789549234103999562307134"),stof<T>("165.96207114656786581941024704963228908192526270524446731427995173")}, std::complex<T>{stof<T>("100.60661801771934845868193049851908584354998543391942344277196114"),stof<T>("-548.98522805482844115327074434794131287337655971971950939978923857")}, std::complex<T>{stof<T>("-130.72586531069217163315292815009494834029388148660811588739721211"),stof<T>("132.70228300699386288619514669861036132946966565585383257501129525")}, std::complex<T>{stof<T>("-115.62569373563779910527655210486707388642848124061625145027161581"),stof<T>("108.11066495425534745776941889956021115989000493009439046011185339")}, std::complex<T>{stof<T>("-115.62569373563779910527655210486707388642848124061625145027161581"),stof<T>("108.11066495425534745776941889956021115989000493009439046011185339")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[66].real()/kbase.W[66].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[125].real()/kbase.W[125].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_714_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_714_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-1130.49176660135460829438949945197123403494112467894635669473629902"),stof<T>("-134.29655721993883351473215285705959998810039377027119388057613643")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,39> abb = {dl[0], dl[1], dl[5], dlog_W8(k,dl), dl[3], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W67(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_7(k), f_2_3(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[66].real()/k.W[66].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[125].real()/k.W[125].real()), f_2_4_re(k), f_2_6_re(k), f_2_24_re(k)};

                    
            return f_4_714_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_714_DLogXconstant_part(base_point<T>, kend);
	value += f_4_714_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_714_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_714_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_714_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_714_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_714_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_714_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
