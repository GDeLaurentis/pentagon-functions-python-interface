/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_675.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_675_abbreviated (const std::array<T,68>& abb) {
T z[96];
z[0] = abb[57] + abb[58];
z[1] = abb[54] * (T(3) / T(2));
z[2] = abb[55] * (T(3) / T(2));
z[3] = z[0] + z[1] + z[2];
z[4] = (T(1) / T(2)) * z[3];
z[4] = abb[21] * z[4];
z[5] = 3 * abb[54];
z[6] = -abb[53] + z[5];
z[7] = 3 * z[0];
z[8] = z[6] + z[7];
z[8] = abb[55] + (T(1) / T(2)) * z[8];
z[9] = abb[19] * (T(1) / T(2));
z[8] = z[8] * z[9];
z[9] = 3 * abb[53];
z[10] = z[5] + z[9];
z[11] = z[0] + z[10];
z[12] = abb[18] * (T(1) / T(4));
z[11] = z[11] * z[12];
z[13] = abb[47] + abb[52];
z[14] = abb[49] + abb[50] + abb[51];
z[15] = -z[13] + 3 * z[14];
z[15] = -abb[60] + abb[48] * (T(3) / T(4)) + (T(1) / T(4)) * z[15];
z[16] = abb[28] * z[15];
z[17] = abb[55] * (T(1) / T(2));
z[1] = -z[1] + z[17];
z[18] = -abb[53] + z[1];
z[19] = abb[20] * z[18];
z[20] = abb[29] * abb[59];
z[21] = abb[59] * (T(1) / T(2));
z[22] = abb[0] * z[21];
z[23] = abb[59] * (T(3) / T(4));
z[24] = abb[7] * z[23];
z[4] = -z[4] + z[8] + -z[11] + z[16] + (T(1) / T(2)) * z[19] + -z[20] + z[22] + z[24];
z[8] = abb[43] * z[4];
z[11] = abb[42] * abb[56];
z[16] = abb[53] + abb[54];
z[19] = -abb[42] * z[16];
z[19] = -z[11] + z[19];
z[19] = abb[13] * z[19];
z[8] = -z[8] + z[19];
z[1] = -abb[16] * z[1];
z[19] = abb[16] * abb[53];
z[1] = z[1] + z[19];
z[20] = abb[18] * z[21];
z[22] = z[1] + z[20];
z[24] = abb[22] * z[15];
z[25] = (T(1) / T(2)) * z[22] + -z[24];
z[26] = 5 * abb[54];
z[7] = abb[53] + z[7] + z[26];
z[7] = abb[55] + (T(1) / T(4)) * z[7];
z[7] = abb[7] * z[7];
z[27] = abb[26] * z[15];
z[7] = z[7] + -z[25] + z[27];
z[28] = abb[19] * abb[59];
z[29] = (T(1) / T(8)) * z[28];
z[7] = (T(1) / T(2)) * z[7] + -z[29];
z[30] = abb[25] * z[15];
z[31] = abb[54] + abb[55];
z[32] = (T(1) / T(2)) * z[31];
z[33] = -abb[56] + z[32];
z[34] = abb[8] * z[33];
z[35] = z[30] + (T(1) / T(2)) * z[34];
z[36] = abb[23] * z[15];
z[37] = abb[7] * abb[56];
z[38] = z[35] + z[36] + z[37];
z[39] = abb[17] * z[3];
z[40] = abb[54] + z[0];
z[41] = abb[0] * z[40];
z[39] = z[39] + z[41];
z[42] = (T(1) / T(4)) * z[39];
z[43] = abb[24] * z[15];
z[44] = (T(1) / T(2)) * z[43];
z[45] = abb[1] * (T(1) / T(2));
z[46] = -abb[54] + abb[55];
z[47] = abb[53] + -abb[56] + -z[46];
z[47] = z[45] * z[47];
z[47] = z[7] + z[38] + -z[42] + z[44] + z[47];
z[47] = abb[35] * z[47];
z[43] = z[27] + z[43];
z[39] = (T(1) / T(2)) * z[39];
z[48] = -z[39] + z[43];
z[49] = 5 * z[0];
z[6] = z[6] + z[49];
z[6] = abb[55] + (T(1) / T(4)) * z[6];
z[6] = abb[7] * z[6];
z[28] = (T(1) / T(4)) * z[28];
z[50] = abb[1] * z[16];
z[51] = z[28] + -z[50];
z[6] = z[6] + z[25] + z[48] + z[51];
z[25] = abb[34] * (T(1) / T(2));
z[52] = -z[6] * z[25];
z[53] = z[15] * z[25];
z[54] = abb[33] * z[15];
z[55] = abb[35] * (T(1) / T(2));
z[56] = z[15] * z[55];
z[53] = z[53] + -z[54] + -z[56];
z[53] = abb[27] * z[53];
z[57] = z[0] + z[31];
z[58] = abb[7] * z[57];
z[59] = (T(1) / T(4)) * z[46];
z[60] = abb[15] * z[59];
z[61] = z[36] + -z[60];
z[58] = z[48] + z[58] + z[61];
z[58] = abb[33] * z[58];
z[62] = abb[33] + -abb[34];
z[62] = z[59] * z[62];
z[63] = abb[35] * abb[56];
z[64] = -abb[32] * abb[56];
z[62] = z[62] + z[63] + z[64];
z[62] = abb[6] * z[62];
z[64] = abb[55] + abb[56];
z[65] = z[45] * z[64];
z[65] = -z[35] + z[65];
z[66] = -z[37] + z[65];
z[67] = -z[60] + z[66];
z[68] = abb[4] * z[59];
z[69] = abb[6] * abb[56];
z[69] = -z[67] + -z[68] + z[69];
z[69] = abb[31] * z[69];
z[47] = -z[47] + -z[52] + z[53] + -z[58] + -z[62] + -z[69];
z[52] = abb[54] * (T(1) / T(2));
z[2] = -abb[57] + -z[2] + z[52];
z[62] = abb[5] * z[2];
z[69] = abb[54] + abb[57];
z[70] = abb[53] + abb[55];
z[71] = z[69] + z[70];
z[72] = abb[9] * z[71];
z[73] = abb[53] + -z[40];
z[73] = -abb[55] + (T(1) / T(2)) * z[73];
z[73] = abb[7] * z[73];
z[72] = -z[20] + z[62] + z[72] + z[73];
z[73] = abb[27] * z[15];
z[74] = -z[39] + z[73];
z[75] = abb[54] + abb[58];
z[75] = abb[10] * z[75];
z[76] = z[74] + z[75];
z[77] = abb[53] + z[0];
z[77] = abb[54] + z[17] + (T(1) / T(2)) * z[77];
z[78] = abb[4] * (T(1) / T(2));
z[79] = -z[77] * z[78];
z[80] = (T(1) / T(2)) * z[50];
z[28] = -z[28] + -z[61] + (T(1) / T(2)) * z[72] + -z[76] + z[79] + -z[80];
z[72] = abb[30] * (T(1) / T(2));
z[28] = z[28] * z[72];
z[79] = z[1] + -z[62];
z[79] = -z[24] + -z[36] + -z[43] + (T(1) / T(2)) * z[79];
z[81] = abb[9] * (T(1) / T(2));
z[71] = z[71] * z[81];
z[82] = -z[71] + z[79];
z[83] = abb[57] + z[64];
z[83] = abb[14] * z[83];
z[84] = (T(1) / T(2)) * z[83];
z[85] = (T(1) / T(2)) * z[37] + z[84];
z[86] = abb[56] + z[16];
z[86] = abb[13] * z[86];
z[87] = (T(1) / T(2)) * z[86];
z[35] = z[35] + z[85] + z[87];
z[64] = (T(-1) / T(2)) * z[16] + z[64];
z[64] = z[45] * z[64];
z[88] = (T(1) / T(8)) * z[46];
z[89] = -abb[15] * z[88];
z[64] = -z[35] + z[64] + (T(1) / T(2)) * z[82] + z[89];
z[64] = abb[32] * z[64];
z[82] = abb[32] * z[46];
z[89] = abb[33] * z[46];
z[82] = z[82] + z[89];
z[10] = -z[0] + z[10];
z[10] = -abb[55] + (T(1) / T(2)) * z[10];
z[90] = abb[34] + abb[35];
z[90] = z[10] * z[90];
z[90] = z[82] + z[90];
z[91] = abb[4] * (T(1) / T(8));
z[90] = z[90] * z[91];
z[92] = abb[2] * z[57];
z[93] = -abb[33] + z[25] + -z[55];
z[94] = -z[72] + -z[93];
z[94] = z[92] * z[94];
z[28] = z[28] + (T(-1) / T(2)) * z[47] + z[64] + z[90] + z[94];
z[28] = m1_set::bc<T>[0] * z[28];
z[47] = (T(1) / T(2)) * z[62];
z[43] = z[43] + z[47];
z[2] = (T(1) / T(2)) * z[2];
z[2] = abb[15] * z[2];
z[64] = (T(1) / T(4)) * z[31];
z[90] = abb[7] * z[64];
z[65] = z[2] + -z[43] + z[65] + -z[85] + z[90];
z[64] = abb[56] + z[64];
z[85] = -abb[6] * z[64];
z[85] = z[65] + z[85];
z[85] = abb[41] * z[85];
z[3] = abb[7] * z[3];
z[3] = z[3] + -z[62];
z[94] = (T(1) / T(2)) * z[75];
z[2] = z[2] + (T(1) / T(2)) * z[3] + z[36] + z[94];
z[3] = -z[2] + -z[74] + (T(-3) / T(2)) * z[92];
z[3] = abb[40] * z[3];
z[74] = z[16] + -z[17];
z[95] = -abb[42] * z[74];
z[95] = (T(1) / T(2)) * z[11] + z[95];
z[95] = abb[1] * z[95];
z[3] = z[3] + z[85] + z[95];
z[85] = (T(1) / T(2)) * z[1] + -z[24];
z[90] = z[85] + -z[90];
z[30] = z[30] + z[36] + z[37] + -z[90];
z[30] = (T(-1) / T(4)) * z[30];
z[30] = abb[42] * z[30];
z[32] = abb[42] * z[32];
z[11] = -z[11] + z[32];
z[32] = abb[6] + -abb[8];
z[32] = (T(1) / T(8)) * z[32];
z[11] = z[11] * z[32];
z[32] = abb[42] * z[18];
z[95] = -abb[43] * z[23];
z[32] = z[32] + z[95];
z[32] = z[32] * z[91];
z[95] = abb[46] * z[46];
z[3] = abb[65] + abb[66] + (T(1) / T(4)) * z[3] + (T(1) / T(8)) * z[8] + z[11] + (T(1) / T(2)) * z[28] + z[30] + z[32] + (T(1) / T(16)) * z[95];
z[8] = z[34] + z[37] + z[75] + z[83] + z[86];
z[11] = (T(-1) / T(3)) * z[0] + -z[17] + -z[52];
z[11] = abb[17] * z[11];
z[28] = abb[55] * (T(1) / T(6)) + -z[52];
z[28] = abb[16] * z[28];
z[12] = abb[59] * z[12];
z[11] = z[11] + z[12] + (T(-1) / T(3)) * z[19] + z[28];
z[12] = abb[54] + abb[56] * (T(5) / T(2));
z[12] = abb[6] * z[12];
z[12] = -z[12] + z[41];
z[13] = -abb[48] + (T(1) / T(3)) * z[13] + -z[14];
z[13] = abb[60] * (T(1) / T(3)) + (T(1) / T(4)) * z[13];
z[14] = abb[23] + abb[25];
z[14] = -abb[22] + -abb[24] + -abb[26] + -abb[27] + (T(-5) / T(2)) * z[14];
z[13] = z[13] * z[14];
z[14] = abb[55] * (T(1) / T(3));
z[19] = -abb[53] + (T(1) / T(3)) * z[40];
z[19] = z[14] + (T(1) / T(2)) * z[19];
z[28] = abb[7] * (T(1) / T(4));
z[19] = z[19] * z[28];
z[30] = abb[55] * (T(5) / T(4));
z[32] = abb[56] * (T(-5) / T(4)) + z[16] + -z[30];
z[32] = abb[1] * z[32];
z[34] = abb[53] * (T(1) / T(3)) + abb[54] * (T(11) / T(3)) + z[0];
z[34] = z[34] * z[91];
z[14] = -abb[54] + -z[14];
z[14] = abb[3] * z[14];
z[8] = (T(5) / T(12)) * z[8] + (T(1) / T(2)) * z[11] + (T(-1) / T(6)) * z[12] + z[13] + (T(1) / T(8)) * z[14] + z[19] + z[29] + (T(1) / T(3)) * z[32] + z[34] + (T(1) / T(4)) * z[92];
z[8] = prod_pow(m1_set::bc<T>[0], 2) * z[8];
z[4] = abb[64] * z[4];
z[11] = z[69] * z[81];
z[12] = -z[11] + z[43];
z[13] = abb[7] * z[59];
z[14] = z[12] + z[13];
z[19] = -z[14] + -z[37];
z[5] = abb[55] + z[5];
z[32] = -z[5] * z[91];
z[34] = abb[55] + abb[57];
z[34] = abb[15] * z[34];
z[37] = -abb[6] * z[88];
z[19] = (T(1) / T(2)) * z[19] + z[32] + -z[34] + z[37] + z[84];
z[19] = abb[31] * z[19];
z[32] = abb[54] * (T(1) / T(4));
z[37] = abb[55] * (T(3) / T(4));
z[40] = abb[57] + z[32] + z[37];
z[40] = abb[15] * z[40];
z[12] = z[12] + z[36] + z[40];
z[36] = abb[33] * z[12];
z[32] = abb[57] + z[30] + -z[32];
z[32] = abb[15] * z[32];
z[32] = z[14] + z[32] + -z[66];
z[32] = abb[32] * z[32];
z[41] = abb[34] * abb[54];
z[66] = abb[33] * z[5];
z[69] = abb[56] + z[59];
z[75] = abb[32] * z[69];
z[75] = -z[41] + -z[63] + (T(1) / T(4)) * z[66] + z[75];
z[75] = abb[6] * z[75];
z[67] = abb[35] * z[67];
z[83] = -z[13] + z[61];
z[84] = -abb[34] * z[83];
z[46] = abb[35] * z[46];
z[46] = z[46] + z[66];
z[41] = -z[41] + (T(1) / T(4)) * z[46];
z[41] = abb[4] * z[41];
z[19] = z[19] + z[32] + z[36] + z[41] + z[67] + z[75] + z[84];
z[19] = abb[31] * z[19];
z[32] = abb[54] * (T(-5) / T(4)) + -z[0] + -z[37];
z[32] = abb[7] * z[32];
z[32] = z[11] + -z[27] + z[32] + -z[47];
z[36] = z[44] + z[61];
z[32] = (T(1) / T(2)) * z[32] + -z[36] + z[94];
z[37] = prod_pow(abb[33], 2);
z[32] = z[32] * z[37];
z[41] = -abb[62] * z[65];
z[6] = z[6] * z[55];
z[44] = abb[0] * z[77];
z[44] = z[44] + -z[50];
z[46] = abb[18] * abb[59];
z[47] = -abb[53] + z[0];
z[61] = abb[55] + z[47];
z[65] = abb[7] * z[61];
z[46] = z[46] + z[65];
z[46] = -z[44] + (T(1) / T(2)) * z[46];
z[46] = z[25] * z[46];
z[65] = abb[33] * z[83];
z[6] = z[6] + z[46] + z[65];
z[6] = abb[34] * z[6];
z[46] = z[70] * z[81];
z[65] = (T(3) / T(2)) * z[50];
z[46] = -z[46] + z[65] + z[83] + -z[85];
z[75] = abb[34] * z[46];
z[77] = abb[57] * (T(-1) / T(2)) + -z[52] + -z[70];
z[77] = abb[9] * z[77];
z[13] = -z[13] + -z[43] + z[77];
z[31] = abb[53] + abb[56] + z[31];
z[31] = z[31] * z[45];
z[13] = (T(1) / T(2)) * z[13] + z[31] + -z[35] + -z[60];
z[13] = abb[32] * z[13];
z[13] = z[13] + -z[67] + z[75];
z[13] = abb[32] * z[13];
z[31] = abb[33] * abb[54];
z[35] = abb[35] * z[59];
z[43] = -abb[34] * z[52];
z[43] = z[31] + z[35] + z[43];
z[43] = abb[34] * z[43];
z[45] = abb[32] * (T(1) / T(2));
z[52] = -z[45] * z[69];
z[60] = -abb[34] * z[59];
z[52] = z[52] + z[60] + z[63];
z[52] = abb[32] * z[52];
z[60] = abb[38] * (T(1) / T(2)) + (T(1) / T(4)) * z[37];
z[60] = z[5] * z[60];
z[33] = -abb[63] * z[33];
z[33] = z[33] + -z[60];
z[63] = -z[63] + (T(-1) / T(2)) * z[89];
z[63] = z[55] * z[63];
z[64] = abb[62] * z[64];
z[33] = (T(1) / T(2)) * z[33] + z[43] + z[52] + z[63] + z[64];
z[33] = abb[6] * z[33];
z[43] = abb[54] * (T(3) / T(4));
z[52] = abb[53] + abb[55] * (T(1) / T(4)) + z[43];
z[63] = abb[4] * z[52];
z[40] = z[40] + z[63] + z[65] + -z[71] + -z[79];
z[40] = abb[36] * z[40];
z[30] = z[0] + z[30] + z[43];
z[30] = abb[7] * z[30];
z[43] = abb[6] * (T(1) / T(4));
z[63] = abb[4] * (T(-1) / T(4)) + -z[43];
z[64] = -abb[55] + z[26];
z[63] = z[63] * z[64];
z[30] = z[30] + z[48] + z[63] + z[73];
z[30] = abb[37] * z[30];
z[48] = abb[56] * (T(-1) / T(2)) + z[74];
z[48] = abb[1] * z[48];
z[38] = z[38] + z[48] + z[87] + -z[90];
z[38] = abb[63] * z[38];
z[12] = -abb[38] * z[12];
z[2] = z[2] + -z[39];
z[2] = abb[61] * z[2];
z[21] = abb[19] * z[21];
z[21] = z[21] + z[44] + z[86];
z[21] = z[21] * z[55];
z[21] = z[21] + -z[58];
z[21] = abb[35] * z[21];
z[18] = -abb[63] * z[18];
z[39] = abb[35] * z[61];
z[39] = z[39] + -z[89];
z[39] = z[39] * z[55];
z[23] = abb[64] * z[23];
z[18] = z[18] + z[23] + z[39] + -z[60];
z[23] = z[45] * z[52];
z[39] = abb[34] * z[16];
z[23] = z[23] + -z[35] + z[39];
z[23] = abb[32] * z[23];
z[35] = abb[35] * (T(1) / T(4));
z[10] = -z[10] * z[35];
z[10] = z[10] + z[31];
z[10] = abb[34] * z[10];
z[10] = z[10] + (T(1) / T(2)) * z[18] + z[23];
z[10] = abb[4] * z[10];
z[15] = abb[61] * z[15];
z[18] = -abb[35] * z[54];
z[23] = abb[34] * z[56];
z[15] = z[15] + z[18] + z[23];
z[15] = abb[27] * z[15];
z[18] = abb[6] * z[59];
z[14] = z[14] + z[18] + z[34] + z[68];
z[23] = abb[39] * z[14];
z[31] = -abb[67] * z[59];
z[2] = -z[2] + (T(-1) / T(2)) * z[4] + -z[6] + -z[8] + -z[10] + -z[12] + -z[13] + -z[15] + -z[19] + -z[21] + z[23] + -z[30] + -z[31] + -z[32] + -z[33] + -z[38] + -z[40] + -z[41];
z[4] = -z[22] + z[62];
z[6] = -3 * abb[55] + -z[47];
z[6] = z[6] * z[28];
z[8] = -z[0] + z[9];
z[9] = abb[54] + (T(1) / T(2)) * z[8] + -z[17];
z[9] = z[9] * z[78];
z[4] = (T(1) / T(2)) * z[4] + z[6] + z[9] + z[11] + -z[18] + z[24] + -z[51] + -z[76];
z[4] = z[4] * z[72];
z[6] = z[36] + -z[42];
z[7] = z[6] + z[7] + z[80];
z[7] = abb[35] * z[7];
z[9] = abb[53] + -z[26] + -z[49];
z[9] = -abb[55] + (T(1) / T(2)) * z[9];
z[9] = abb[7] * z[9];
z[1] = z[1] + z[9] + -z[20];
z[9] = abb[9] * z[70];
z[1] = (T(1) / T(2)) * z[1] + z[9] + -z[24] + -z[27];
z[1] = (T(1) / T(2)) * z[1] + -z[6] + -z[29] + -z[50];
z[1] = abb[34] * z[1];
z[6] = -abb[32] * z[46];
z[9] = -abb[31] * z[14];
z[8] = abb[54] + z[8];
z[8] = z[8] * z[55];
z[0] = -5 * abb[53] + -z[0] + -z[26];
z[0] = -abb[55] + (T(1) / T(2)) * z[0];
z[0] = abb[34] * z[0];
z[0] = z[0] + z[8] + z[89];
z[8] = -abb[32] * z[16];
z[0] = (T(1) / T(4)) * z[0] + z[8];
z[0] = abb[4] * z[0];
z[8] = z[43] * z[82];
z[0] = z[0] + z[1] + z[4] + z[6] + z[7] + z[8] + z[9] + -z[53] + z[58];
z[1] = abb[30] * (T(1) / T(4));
z[0] = z[0] * z[1];
z[4] = abb[35] + z[25];
z[4] = z[4] * z[25];
z[1] = -z[1] + -z[93];
z[1] = abb[30] * z[1];
z[6] = 3 * abb[61] + -z[37];
z[7] = -abb[33] + -z[35];
z[7] = abb[35] * z[7];
z[1] = abb[37] + z[1] + z[4] + (T(1) / T(4)) * z[6] + z[7];
z[1] = abb[2] * z[1] * z[57];
z[4] = -abb[33] * abb[34];
z[4] = abb[37] + abb[38] + z[4] + z[37];
z[4] = z[4] * z[5];
z[5] = abb[34] * z[5];
z[5] = z[5] + -z[66];
z[5] = abb[31] * z[5];
z[4] = z[4] + z[5];
z[4] = abb[3] * z[4];
z[0] = abb[44] + abb[45] + z[0] + (T(1) / T(2)) * z[1] + (T(-1) / T(4)) * z[2] + (T(1) / T(16)) * z[4];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_675_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.24113092074640388332875526100108878623837288398082771037094547575"),stof<T>("-0.01624583897832471480970255068419892981349816475670169103204217271")}, std::complex<T>{stof<T>("-0.72339276223921164998626578300326635871511865194248313111283642725"),stof<T>("0.04873751693497414442910765205259678944049449427010507309612651814")}, std::complex<T>{stof<T>("-0.72339276223921164998626578300326635871511865194248313111283642725"),stof<T>("0.04873751693497414442910765205259678944049449427010507309612651814")}, std::complex<T>{stof<T>("-0.72339276223921164998626578300326635871511865194248313111283642725"),stof<T>("0.04873751693497414442910765205259678944049449427010507309612651814")}, std::complex<T>{stof<T>("-0.72339276223921164998626578300326635871511865194248313111283642725"),stof<T>("0.04873751693497414442910765205259678944049449427010507309612651814")}, std::complex<T>{stof<T>("0.24113092074640388332875526100108878623837288398082771037094547575"),stof<T>("-0.01624583897832471480970255068419892981349816475670169103204217271")}, std::complex<T>{stof<T>("0.50848798595424516483157710279185818768886065923555722773440723966"),stof<T>("-0.08189922606535734414673475568025848565764618468363685624210463349")}, std::complex<T>{stof<T>("1.0902605573903321210124714450873887881207121287000015604047394347"),stof<T>("0.9184235249078516096857262458356413663775515103996381993892529282")}, std::complex<T>{stof<T>("0.89267228851662951625356985182200612250416544593074411528871297207"),stof<T>("0.76204548091203623737723229293467209624973074503386825463690901941")}, std::complex<T>{stof<T>("0.6416084133721912851433680017848845638959752803241144362225539355"),stof<T>("1.5415327359264153138372793781012715931025506542461286663396272344")}, std::complex<T>{stof<T>("1.00822295432971722699045911516767213532510979017080854043942249461"),stof<T>("0.73453563721963770783458819391456523649899966276761096503012094893")}, std::complex<T>{stof<T>("0.53243756452186122178903707120331913415343410525939776939193670792"),stof<T>("0.34209081986619431596925285610376664019794131280571792569097552237")}, std::complex<T>{stof<T>("0.23594018086022457074806837570668553834164687884362288183008744216"),stof<T>("-0.38424403207403930356915199686015859639968219883999128304530377912")}, std::complex<T>{stof<T>("0.964523682985615533315021044004355144953491535923310841483781903"),stof<T>("-0.06498335591329885923881020273679571925399265902680676412816869085")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}, rlog(k.W[197].imag()/kbase.W[197].imag())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_675_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_675_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_675_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(1) / T(32)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[1] * (T(1) / T(8)) * (-v[3] + v[5])) / tend;


		return (abb[54] + abb[55] + abb[57]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[31], 2);
z[1] = prod_pow(abb[33], 2);
z[0] = z[0] + -z[1];
z[1] = abb[54] + abb[55] + abb[57];
return abb[11] * (T(1) / T(8)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_675_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(64)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(16)) * (v[2] + v[3])) / tend;


		return (abb[55] + abb[57] + abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[55] + -abb[57] + -abb[58];
z[1] = abb[33] + -abb[34];
return abb[12] * m1_set::bc<T>[0] * (T(1) / T(8)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_675_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(128)) * (v[2] + v[3]) * (24 + 8 * v[0] + 4 * v[1] + -5 * v[2] + -9 * v[3] + -4 * v[4] + 6 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + 12 * v[5])) / prod_pow(tend, 2);
c[1] = ((-1 + 3 * m1_set::bc<T>[1]) * (T(-1) / T(16)) * (v[2] + v[3])) / tend;


		return (abb[55] + abb[57] + abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[30] + -abb[35];
z[1] = -abb[33] + z[0];
z[1] = abb[33] * z[1];
z[0] = -abb[34] + z[0];
z[0] = abb[34] * z[0];
z[0] = abb[37] + -z[0] + z[1];
z[1] = -abb[55] + -abb[57] + -abb[58];
return abb[12] * (T(1) / T(8)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_675_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("1.7473496627151255302394487382629645369969312453172269325706763971"),stof<T>("-0.0886793076580519834066832989332257060121304060459830094571041872")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,68> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W19(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_3(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), rlog(kend.W[197].imag()/k.W[197].imag()), f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_25_re(k), T{0}, T{0}, T{0}};
abb[44] = SpDLog_f_4_675_W_16_Im(t, path, abb);
abb[45] = SpDLog_f_4_675_W_19_Im(t, path, abb);
abb[46] = SpDLogQ_W_72(k,dl,dlr).imag();
abb[65] = SpDLog_f_4_675_W_16_Re(t, path, abb);
abb[66] = SpDLog_f_4_675_W_19_Re(t, path, abb);
abb[67] = SpDLogQ_W_72(k,dl,dlr).real();

                    
            return f_4_675_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_675_DLogXconstant_part(base_point<T>, kend);
	value += f_4_675_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_675_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_675_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_675_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_675_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_675_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_675_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
