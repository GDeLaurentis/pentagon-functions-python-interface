/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_101.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_101_abbreviated (const std::array<T,59>& abb) {
T z[95];
z[0] = -abb[33] + abb[34];
z[1] = (T(1) / T(2)) * z[0];
z[2] = abb[29] * (T(1) / T(2));
z[3] = z[1] + z[2];
z[4] = abb[30] * (T(1) / T(2));
z[5] = -abb[31] + z[4];
z[6] = z[3] + z[5];
z[6] = m1_set::bc<T>[0] * z[6];
z[7] = abb[41] * (T(1) / T(2));
z[6] = z[6] + -z[7];
z[6] = abb[1] * z[6];
z[8] = abb[40] + -abb[41];
z[9] = abb[29] + -abb[31];
z[10] = m1_set::bc<T>[0] * z[9];
z[11] = z[8] + z[10];
z[12] = abb[5] * (T(1) / T(4));
z[13] = z[11] * z[12];
z[6] = z[6] + z[13];
z[13] = abb[30] + -abb[33];
z[13] = abb[4] * z[13];
z[14] = abb[30] + -abb[31];
z[15] = abb[29] + z[14];
z[16] = -abb[32] + z[15];
z[16] = abb[15] * z[16];
z[17] = abb[31] + -abb[34];
z[18] = abb[7] * z[17];
z[13] = z[13] + -z[16] + -z[18];
z[16] = abb[12] * z[17];
z[13] = (T(1) / T(2)) * z[13] + z[16];
z[13] = m1_set::bc<T>[0] * z[13];
z[16] = abb[7] * abb[41];
z[18] = -abb[15] * z[8];
z[16] = z[16] + z[18];
z[19] = abb[4] * (T(1) / T(2));
z[20] = -abb[13] + z[19];
z[21] = abb[42] * z[20];
z[13] = z[13] + (T(-1) / T(2)) * z[16] + -z[21];
z[16] = z[2] + z[14];
z[21] = abb[34] * (T(1) / T(2));
z[22] = -abb[33] + z[21];
z[23] = z[16] + z[22];
z[23] = m1_set::bc<T>[0] * z[23];
z[23] = -abb[42] + z[23];
z[23] = abb[3] * z[23];
z[24] = abb[10] * abb[40];
z[23] = z[13] + z[23] + -z[24];
z[25] = 3 * abb[33];
z[26] = z[17] + z[25];
z[27] = abb[32] * (T(1) / T(2));
z[26] = -abb[30] + (T(1) / T(2)) * z[26] + -z[27];
z[26] = m1_set::bc<T>[0] * z[26];
z[28] = abb[42] * (T(1) / T(2));
z[26] = z[7] + z[26] + z[28];
z[29] = abb[6] * (T(1) / T(2));
z[26] = z[26] * z[29];
z[30] = (T(1) / T(2)) * z[10];
z[31] = abb[40] * (T(1) / T(2));
z[32] = -abb[41] + z[30] + z[31];
z[32] = abb[14] * z[32];
z[33] = abb[8] * (T(1) / T(4));
z[34] = -abb[33] + z[15];
z[34] = m1_set::bc<T>[0] * z[34];
z[35] = abb[41] + -z[34];
z[35] = z[33] * z[35];
z[23] = z[6] + (T(1) / T(2)) * z[23] + z[26] + -z[32] + z[35];
z[26] = abb[51] * (T(1) / T(8));
z[23] = z[23] * z[26];
z[35] = -abb[29] + z[0];
z[36] = abb[32] + z[35];
z[37] = m1_set::bc<T>[0] * z[36];
z[38] = abb[2] * z[37];
z[13] = z[13] + -z[38];
z[39] = abb[33] * (T(1) / T(2));
z[40] = -z[17] + z[27] + -z[39];
z[40] = m1_set::bc<T>[0] * z[40];
z[40] = -z[28] + z[40];
z[40] = abb[3] * z[40];
z[40] = z[13] + z[40];
z[41] = -abb[32] + z[2];
z[42] = -abb[34] + abb[33] * (T(3) / T(2));
z[43] = abb[31] * (T(1) / T(2));
z[44] = -z[4] + z[41] + z[42] + z[43];
z[44] = m1_set::bc<T>[0] * z[44];
z[45] = abb[40] + abb[41];
z[44] = z[28] + z[44] + (T(1) / T(2)) * z[45];
z[45] = abb[8] * (T(1) / T(2));
z[44] = z[44] * z[45];
z[46] = -abb[40] + z[37];
z[47] = -abb[42] + z[46];
z[48] = abb[17] * (T(1) / T(4));
z[49] = z[47] * z[48];
z[32] = -z[32] + z[49];
z[49] = -abb[31] + z[0];
z[50] = abb[32] + z[49];
z[50] = m1_set::bc<T>[0] * z[50];
z[50] = -abb[41] + z[50];
z[51] = -abb[42] + z[50];
z[52] = abb[6] * (T(1) / T(4));
z[53] = -z[51] * z[52];
z[6] = z[6] + z[32] + (T(1) / T(2)) * z[40] + z[44] + z[53];
z[40] = abb[50] * (T(1) / T(8));
z[6] = z[6] * z[40];
z[41] = z[22] + -z[41];
z[41] = m1_set::bc<T>[0] * z[41];
z[41] = -abb[42] + z[41];
z[41] = abb[3] * z[41];
z[44] = abb[5] * (T(1) / T(2));
z[11] = -z[11] * z[44];
z[53] = abb[32] + -abb[33];
z[54] = -z[17] + -z[53];
z[54] = m1_set::bc<T>[0] * z[54];
z[54] = abb[41] + abb[42] + z[54];
z[54] = z[29] * z[54];
z[55] = abb[33] + z[15];
z[55] = -abb[32] + (T(1) / T(2)) * z[55];
z[55] = m1_set::bc<T>[0] * z[55];
z[7] = z[7] + z[55];
z[7] = abb[8] * z[7];
z[10] = abb[9] * z[10];
z[7] = z[7] + z[10] + z[11] + z[13] + z[41] + z[54];
z[7] = abb[49] * z[7];
z[8] = -z[8] + -z[34];
z[8] = abb[24] * z[8];
z[10] = -abb[22] * m1_set::bc<T>[0] * z[53];
z[11] = -abb[25] * z[50];
z[13] = abb[22] + abb[24] + abb[25];
z[34] = abb[42] * z[13];
z[41] = abb[26] * z[47];
z[8] = z[8] + z[10] + z[11] + z[34] + z[41];
z[10] = abb[45] * (T(1) / T(32));
z[8] = z[8] * z[10];
z[11] = -abb[42] + z[37];
z[34] = abb[3] * (T(1) / T(2));
z[41] = z[11] * z[34];
z[50] = abb[13] * abb[42];
z[38] = -z[38] + z[41] + z[50];
z[18] = -z[18] + z[38];
z[28] = z[28] + z[31] + -z[37];
z[31] = z[28] * z[45];
z[30] = abb[9] * z[30];
z[18] = (T(1) / T(2)) * z[18] + z[30] + z[31] + z[32];
z[30] = abb[52] * (T(1) / T(8));
z[18] = z[18] * z[30];
z[31] = abb[19] * abb[54];
z[32] = z[31] * z[36];
z[37] = -abb[44] + abb[46] + abb[47] + -abb[48];
z[41] = -abb[32] * z[37];
z[50] = -abb[33] * z[37];
z[53] = z[41] + -z[50];
z[53] = abb[22] * z[53];
z[32] = z[32] + z[53];
z[32] = m1_set::bc<T>[0] * z[32];
z[53] = -abb[34] * z[37];
z[54] = -z[50] + z[53];
z[55] = -abb[31] * z[37];
z[56] = -z[54] + z[55];
z[57] = z[41] + -z[56];
z[57] = m1_set::bc<T>[0] * z[57];
z[58] = abb[41] * z[37];
z[57] = z[57] + z[58];
z[59] = abb[25] * z[57];
z[13] = z[13] * z[37];
z[60] = abb[18] + abb[19];
z[61] = abb[54] * z[60];
z[13] = z[13] + -z[61];
z[13] = abb[42] * z[13];
z[62] = abb[18] * abb[40] * abb[54];
z[13] = z[13] + z[32] + z[59] + z[62];
z[28] = -abb[8] * z[28];
z[32] = abb[17] * (T(1) / T(2));
z[47] = -z[32] * z[47];
z[24] = -z[24] + z[28] + -z[38] + z[47];
z[28] = abb[53] * (T(1) / T(16));
z[24] = z[24] * z[28];
z[38] = -abb[30] * z[37];
z[47] = z[38] + -z[55];
z[59] = -abb[29] * z[37];
z[62] = z[47] + -z[50] + z[59];
z[62] = m1_set::bc<T>[0] * z[62];
z[63] = -abb[40] * z[37];
z[58] = z[58] + z[62] + z[63];
z[62] = abb[24] * (T(1) / T(32));
z[58] = z[58] * z[62];
z[63] = abb[40] + abb[42];
z[63] = -z[37] * z[63];
z[59] = -z[54] + z[59];
z[41] = -z[41] + z[59];
z[64] = m1_set::bc<T>[0] * z[41];
z[63] = z[63] + z[64];
z[64] = abb[26] * (T(1) / T(32));
z[63] = z[63] * z[64];
z[65] = abb[42] * z[37];
z[51] = -abb[45] * z[51];
z[51] = z[51] + z[57] + z[65];
z[57] = abb[23] * (T(1) / T(32));
z[51] = z[51] * z[57];
z[65] = abb[21] * (T(1) / T(32));
z[65] = abb[54] * z[65];
z[11] = -abb[40] + z[11];
z[11] = z[11] * z[65];
z[66] = abb[49] + -abb[51] + abb[52] + -abb[53];
z[67] = abb[0] * (T(1) / T(32));
z[66] = z[66] * z[67];
z[46] = z[46] * z[66];
z[6] = abb[58] + z[6] + (T(1) / T(16)) * z[7] + z[8] + z[11] + (T(1) / T(32)) * z[13] + z[18] + z[23] + z[24] + z[46] + z[51] + z[58] + z[63];
z[7] = z[0] + z[14];
z[7] = abb[30] * z[7];
z[8] = -abb[35] + abb[37];
z[11] = abb[55] + -abb[56];
z[13] = -z[8] + z[11];
z[18] = abb[31] + z[35];
z[18] = abb[29] * z[18];
z[23] = abb[30] + z[35];
z[24] = abb[32] * z[23];
z[46] = abb[31] * z[0];
z[7] = z[7] + -z[13] + z[18] + -z[24] + -z[46];
z[18] = abb[15] * (T(1) / T(2));
z[7] = z[7] * z[18];
z[3] = -abb[31] + z[3];
z[3] = abb[29] * z[3];
z[18] = z[0] * z[21];
z[24] = abb[36] + z[18];
z[51] = -z[0] + z[43];
z[51] = abb[31] * z[51];
z[3] = abb[35] + z[3] + z[24] + z[51];
z[58] = abb[16] * (T(1) / T(2));
z[3] = z[3] * z[58];
z[58] = abb[30] * abb[33];
z[63] = -abb[30] + z[27];
z[67] = abb[32] * z[63];
z[68] = prod_pow(abb[33], 2);
z[69] = (T(1) / T(2)) * z[68];
z[58] = abb[37] + z[58] + z[67] + -z[69];
z[58] = abb[4] * z[58];
z[70] = prod_pow(abb[34], 2);
z[71] = prod_pow(abb[31], 2);
z[70] = z[70] + -z[71];
z[17] = abb[30] * z[17];
z[70] = abb[36] + abb[56] + z[17] + (T(1) / T(2)) * z[70];
z[70] = abb[7] * z[70];
z[58] = z[58] + z[70];
z[70] = abb[34] * z[22];
z[17] = z[17] + -z[51] + -z[70];
z[17] = abb[12] * z[17];
z[19] = abb[13] + z[19];
z[72] = prod_pow(m1_set::bc<T>[0], 2);
z[73] = (T(1) / T(6)) * z[72];
z[19] = z[19] * z[73];
z[74] = abb[39] * (T(1) / T(4));
z[60] = z[60] * z[74];
z[20] = abb[57] * z[20];
z[3] = z[3] + -z[7] + z[17] + -z[19] + -z[20] + (T(-1) / T(2)) * z[58] + z[60];
z[7] = z[27] * z[63];
z[17] = abb[37] * (T(1) / T(2));
z[7] = z[7] + z[17];
z[19] = abb[34] * z[42];
z[19] = z[19] + -z[68];
z[20] = abb[35] + -abb[38] + z[19];
z[42] = -abb[30] + z[2];
z[58] = -z[1] + z[42];
z[58] = z[2] * z[58];
z[60] = abb[57] + z[73];
z[63] = abb[36] * (T(1) / T(2));
z[22] = abb[31] + abb[30] * (T(-3) / T(4)) + -z[22];
z[22] = abb[30] * z[22];
z[20] = -z[7] + (T(1) / T(2)) * z[20] + z[22] + -z[51] + z[58] + z[60] + -z[63];
z[20] = abb[3] * z[20];
z[22] = -abb[55] + z[73];
z[22] = abb[10] * z[22];
z[20] = -z[3] + z[20] + -z[22];
z[58] = z[2] * z[16];
z[75] = abb[57] * (T(1) / T(2));
z[76] = z[63] + -z[75];
z[49] = abb[31] * z[49];
z[77] = -abb[38] + abb[56];
z[49] = z[49] + z[77];
z[78] = z[49] + -z[70];
z[79] = (T(3) / T(2)) * z[68];
z[80] = -z[78] + z[79];
z[81] = abb[30] * (T(1) / T(4));
z[82] = -z[43] + z[81];
z[83] = -abb[33] + z[82];
z[83] = abb[30] * z[83];
z[23] = z[23] * z[27];
z[84] = (T(5) / T(12)) * z[72];
z[23] = -z[17] + z[23] + z[58] + z[76] + (T(1) / T(2)) * z[80] + z[83] + -z[84];
z[23] = z[23] * z[29];
z[29] = z[2] * z[14];
z[80] = abb[38] + z[71];
z[83] = abb[31] * z[4];
z[85] = abb[55] * (T(1) / T(2));
z[86] = (T(1) / T(12)) * z[72];
z[29] = -abb[56] + z[29] + (T(1) / T(2)) * z[80] + -z[83] + z[85] + z[86];
z[29] = abb[14] * z[29];
z[80] = -abb[31] + z[81];
z[83] = z[1] + z[80];
z[83] = abb[30] * z[83];
z[78] = -z[69] + z[78];
z[87] = abb[35] + z[78];
z[88] = z[2] * z[42];
z[83] = -z[83] + z[86] + (T(1) / T(2)) * z[87] + z[88];
z[83] = abb[1] * z[83];
z[71] = (T(1) / T(2)) * z[71];
z[87] = z[8] + z[71];
z[88] = abb[30] * z[5];
z[89] = -z[77] + z[87] + z[88];
z[42] = abb[29] * z[42];
z[42] = -abb[55] + z[42] + -z[67] + -z[89];
z[12] = z[12] * z[42];
z[12] = z[12] + z[83];
z[14] = z[1] + z[14];
z[14] = abb[29] * z[14];
z[83] = -abb[33] + z[5];
z[83] = abb[30] * z[83];
z[49] = -z[49] + z[83];
z[14] = z[14] + z[24] + z[49];
z[83] = z[69] + -z[73];
z[90] = z[14] + z[83];
z[33] = z[33] * z[90];
z[90] = abb[39] * (T(1) / T(8));
z[91] = -abb[20] * z[90];
z[20] = z[12] + (T(1) / T(2)) * z[20] + z[23] + z[29] + z[33] + z[91];
z[20] = z[20] * z[26];
z[23] = -z[0] + z[2];
z[23] = abb[29] * z[23];
z[26] = z[27] + z[35];
z[26] = abb[32] * z[26];
z[23] = z[23] + z[26] + z[70];
z[33] = z[23] + z[69];
z[72] = (T(1) / T(3)) * z[72];
z[91] = -z[33] + z[72];
z[91] = abb[2] * z[91];
z[3] = z[3] + z[91];
z[36] = -abb[30] + z[36];
z[92] = z[27] * z[36];
z[17] = z[17] + z[76] + -z[86] + z[92];
z[76] = 5 * abb[33] + -3 * abb[34];
z[76] = z[21] * z[76];
z[92] = -abb[35] + z[69];
z[76] = -abb[38] + z[76] + -z[92];
z[80] = -z[21] + -z[80];
z[80] = abb[30] * z[80];
z[93] = -abb[30] + z[1];
z[94] = z[2] * z[93];
z[51] = -z[17] + -z[51] + (T(1) / T(2)) * z[76] + z[80] + z[94];
z[51] = abb[3] * z[51];
z[51] = -z[3] + z[51];
z[76] = abb[33] * abb[34];
z[80] = abb[29] * z[35];
z[76] = -z[68] + z[76] + z[80];
z[76] = (T(1) / T(2)) * z[76];
z[80] = abb[55] + z[60];
z[94] = -z[26] + z[76] + z[80];
z[48] = z[48] * z[94];
z[29] = z[29] + z[48];
z[36] = abb[32] * z[36];
z[36] = -z[36] + z[60];
z[16] = abb[29] * z[16];
z[48] = abb[36] + abb[37];
z[16] = z[16] + -z[36] + z[48] + -z[78] + z[88];
z[52] = z[16] * z[52];
z[25] = abb[34] + -z[25];
z[25] = z[21] * z[25];
z[1] = -z[1] + z[15];
z[1] = abb[29] * z[1];
z[1] = abb[36] + z[1] + z[25] + z[49] + z[79];
z[25] = -z[26] + z[75] + z[85];
z[1] = (T(1) / T(2)) * z[1] + -z[25] + -z[84];
z[1] = z[1] * z[45];
z[49] = -abb[20] + -abb[21];
z[49] = z[49] * z[90];
z[1] = z[1] + z[12] + z[29] + z[49] + (T(1) / T(2)) * z[51] + z[52];
z[1] = z[1] * z[40];
z[12] = abb[45] * z[16];
z[16] = -z[4] * z[37];
z[40] = z[16] + -z[55];
z[49] = abb[30] * z[40];
z[41] = z[38] + z[41];
z[41] = abb[32] * z[41];
z[51] = abb[38] * z[37];
z[41] = z[41] + z[51];
z[49] = -z[41] + z[49];
z[52] = z[2] * z[37];
z[52] = -z[47] + z[52];
z[52] = abb[29] * z[52];
z[55] = abb[31] * z[56];
z[56] = -abb[57] + z[83];
z[75] = -abb[56] + z[56];
z[78] = -z[48] + -z[75];
z[78] = -z[37] * z[78];
z[79] = z[21] * z[37];
z[79] = z[50] + z[79];
z[79] = abb[34] * z[79];
z[12] = z[12] + -z[49] + z[52] + -z[55] + z[78] + z[79];
z[12] = z[12] * z[57];
z[52] = prod_pow(abb[30], 2);
z[57] = abb[31] + -z[2];
z[57] = abb[29] * z[57];
z[13] = -z[13] + (T(1) / T(2)) * z[52] + z[57] + z[67] + -z[71];
z[13] = abb[15] * z[13];
z[52] = -z[33] + z[60];
z[34] = z[34] * z[52];
z[52] = -abb[57] + z[73];
z[52] = abb[13] * z[52];
z[34] = z[34] + z[52] + -z[91];
z[13] = z[13] + z[34];
z[9] = -abb[30] + z[9];
z[9] = z[2] * z[9];
z[5] = z[4] * z[5];
z[5] = abb[35] + abb[38] * (T(-1) / T(2)) + -z[5] + -z[7] + z[9];
z[5] = abb[9] * z[5];
z[7] = z[25] + z[72] + z[76];
z[9] = -z[7] * z[45];
z[25] = -abb[21] * z[90];
z[9] = z[5] + z[9] + (T(1) / T(2)) * z[13] + z[25] + z[29];
z[9] = z[9] * z[30];
z[13] = z[46] + z[69] + -z[77];
z[18] = z[13] + z[18];
z[25] = -abb[34] + z[39] + -z[82];
z[25] = abb[30] * z[25];
z[0] = (T(3) / T(2)) * z[0];
z[15] = -z[0] + z[15];
z[15] = z[2] * z[15];
z[15] = z[15] + (T(1) / T(2)) * z[18] + z[25] + z[26] + z[63] + -z[86];
z[15] = abb[8] * z[15];
z[13] = z[13] + -z[70];
z[18] = -abb[34] + z[43] + z[81];
z[18] = abb[30] * z[18];
z[13] = (T(1) / T(2)) * z[13] + z[17] + z[18] + z[58];
z[13] = abb[6] * z[13];
z[17] = -abb[34] + z[4];
z[17] = abb[30] * z[17];
z[8] = abb[38] + z[8] + z[17];
z[0] = -abb[30] + z[0] + -z[2];
z[0] = abb[29] * z[0];
z[0] = -abb[36] + z[0] + -z[8] + z[19];
z[2] = abb[32] * (T(-3) / T(4)) + z[4] + -z[35];
z[2] = abb[32] * z[2];
z[0] = (T(1) / T(2)) * z[0] + z[2] + z[60];
z[0] = abb[3] * z[0];
z[2] = -z[42] * z[44];
z[4] = -abb[20] * z[74];
z[0] = z[0] + z[2] + -z[3] + z[4] + z[13] + z[15];
z[0] = (T(1) / T(2)) * z[0] + z[5];
z[0] = abb[49] * z[0];
z[2] = (T(1) / T(2)) * z[54];
z[3] = z[2] + -z[38];
z[3] = abb[29] * z[3];
z[4] = z[21] * z[54];
z[5] = -z[48] + -z[92];
z[5] = -z[5] * z[37];
z[13] = -z[16] + z[53];
z[13] = abb[30] * z[13];
z[5] = z[3] + -z[4] + z[5] + z[13] + z[41];
z[5] = abb[22] * z[5];
z[13] = -z[75] + -z[87];
z[13] = -z[13] * z[37];
z[15] = z[21] * z[50];
z[3] = z[3] + z[13] + z[15] + -z[49];
z[3] = abb[25] * z[3];
z[13] = -abb[19] * z[33];
z[15] = -abb[18] * abb[55];
z[13] = z[13] + z[15];
z[13] = abb[54] * z[13];
z[15] = -abb[22] * z[37];
z[16] = z[15] + z[61];
z[16] = abb[57] * z[16];
z[15] = z[15] + z[31];
z[15] = z[15] * z[73];
z[3] = z[3] + z[5] + z[13] + z[15] + z[16];
z[2] = -z[2] + -z[47];
z[2] = abb[29] * z[2];
z[5] = -abb[36] + -z[11] + -z[56];
z[5] = -z[5] * z[37];
z[11] = -z[40] + z[50];
z[11] = abb[30] * z[11];
z[2] = z[2] + -z[4] + z[5] + z[11] + z[51] + -z[55];
z[2] = z[2] * z[62];
z[4] = abb[29] * z[93];
z[4] = z[4] + z[36] + -z[69];
z[5] = -z[4] + z[8] + z[24];
z[5] = abb[22] * z[5];
z[8] = -abb[33] * z[21];
z[4] = -z[4] + z[8] + z[89];
z[4] = abb[25] * z[4];
z[8] = abb[55] + z[14] + -z[60] + z[69];
z[8] = abb[24] * z[8];
z[11] = abb[26] * z[94];
z[13] = abb[27] * abb[39];
z[4] = z[4] + z[5] + z[8] + z[11] + (T(-1) / T(2)) * z[13];
z[4] = z[4] * z[10];
z[5] = abb[8] * z[7];
z[7] = -z[32] * z[94];
z[8] = abb[21] * z[74];
z[5] = z[5] + z[7] + z[8] + -z[22] + -z[34];
z[5] = z[5] * z[28];
z[7] = -z[37] * z[68];
z[8] = abb[29] * z[59];
z[10] = -abb[34] * z[50];
z[7] = z[7] + z[8] + z[10];
z[8] = -z[27] * z[37];
z[8] = z[8] + -z[59];
z[8] = abb[32] * z[8];
z[10] = z[37] * z[80];
z[7] = (T(1) / T(2)) * z[7] + z[8] + z[10];
z[7] = z[7] * z[64];
z[8] = z[65] * z[94];
z[10] = -abb[55] + z[23] + z[83];
z[10] = -z[10] * z[66];
z[11] = abb[8] * (T(-1) / T(32)) + abb[17] * (T(-1) / T(64)) + abb[28] * (T(1) / T(16));
z[11] = abb[54] * z[11];
z[13] = -abb[27] * z[37];
z[11] = z[11] + (T(1) / T(64)) * z[13];
z[11] = abb[39] * z[11];
z[0] = abb[43] + (T(1) / T(8)) * z[0] + z[1] + z[2] + (T(1) / T(32)) * z[3] + z[4] + z[5] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[20];
return {z[6], z[0]};
}


template <typename T> std::complex<T> f_4_101_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.10746598666593017218683252641944139356118324561600579196572995152"),stof<T>("0.18245957001093500747421295345731237331702366547953166912504524402")}, std::complex<T>{stof<T>("0.023232893629103218492861634487972709377537351945760751703708081949"),stof<T>("0.011800148241401312385504272009097879687920687373457521451119707958")}, std::complex<T>{stof<T>("0.2229406852754450308600876055638796938568208328197201004665447706"),stof<T>("-0.10132680948732982130675139559947531325797989241265149128864421455")}, std::complex<T>{stof<T>("0.045640778737490954344508248003943561676565183962648544838120203141"),stof<T>("0.07877905257269198306319046062124553040205961825462748678296930227")}, std::complex<T>{stof<T>("0.027078297507034053871185928587716811995264245383652167487882277839"),stof<T>("-0.065958593387465625013622675084755567328522146666352131071790205943")}, std::complex<T>{stof<T>("-0.04972339456069860063975792774574482256343578028833931988485229454"),stof<T>("0.13801363516870842049040280265772312585990829028334043153424807411")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_101_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_101_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_101_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(256)) * (-v[3] + v[5]) * (8 + 3 * v[3] + v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(32)) * (-v[3] + v[5])) / tend;


		return (abb[49] + abb[50] + abb[52]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[30] + abb[32] * (T(1) / T(2));
z[0] = abb[32] * z[0];
z[1] = prod_pow(abb[30], 2);
z[0] = abb[37] + z[0] + (T(1) / T(2)) * z[1];
z[1] = -abb[49] + -abb[50] + -abb[52];
return abb[11] * (T(1) / T(16)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_101_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.12859412000614909217962997931210223124328802000465183632759617216"),stof<T>("0.16932750399970493041450531401030140763628697790480269671613473188")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,59> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W21(k,dl), dlog_W22(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), T{0}};
abb[43] = SpDLog_f_4_101_W_16_Im(t, path, abb);
abb[58] = SpDLog_f_4_101_W_16_Re(t, path, abb);

                    
            return f_4_101_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_101_DLogXconstant_part(base_point<T>, kend);
	value += f_4_101_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_101_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_101_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_101_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_101_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_101_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_101_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
