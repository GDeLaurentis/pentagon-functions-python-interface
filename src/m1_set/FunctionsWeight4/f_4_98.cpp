/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_98.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_98_abbreviated (const std::array<T,57>& abb) {
T z[63];
z[0] = abb[48] + abb[49];
z[1] = abb[47] + abb[50];
z[2] = z[0] + z[1];
z[3] = abb[17] * z[2];
z[4] = -abb[49] + z[1];
z[5] = abb[0] * z[4];
z[6] = abb[44] + abb[45] + -abb[46];
z[7] = abb[25] * z[6];
z[8] = z[3] + z[5] + -z[7];
z[9] = abb[3] + abb[17];
z[10] = abb[0] + z[9];
z[10] = abb[51] * z[10];
z[11] = abb[19] + abb[21];
z[12] = abb[52] * z[11];
z[10] = z[10] + -z[12];
z[12] = (T(-1) / T(2)) * z[10];
z[13] = -abb[51] + z[2];
z[14] = abb[13] * z[13];
z[15] = -abb[51] + z[1];
z[16] = abb[48] + z[15];
z[17] = abb[2] * z[16];
z[18] = z[14] + z[17];
z[19] = abb[47] + z[0];
z[19] = abb[12] * z[19];
z[20] = abb[48] * (T(1) / T(2));
z[21] = abb[7] * z[20];
z[19] = z[19] + -z[21];
z[21] = (T(1) / T(2)) * z[6];
z[22] = abb[26] * z[21];
z[23] = abb[23] * z[21];
z[24] = abb[1] * z[0];
z[25] = abb[49] + z[1];
z[26] = abb[48] + (T(1) / T(2)) * z[25];
z[27] = abb[3] * z[26];
z[28] = -abb[47] + z[20];
z[29] = -abb[6] * z[28];
z[8] = (T(1) / T(2)) * z[8] + z[12] + -z[18] + -z[19] + z[22] + -z[23] + z[24] + z[27] + z[29];
z[27] = abb[34] * (T(1) / T(16));
z[8] = z[8] * z[27];
z[22] = (T(1) / T(2)) * z[3] + z[22];
z[12] = (T(1) / T(2)) * z[5] + z[12] + z[22];
z[29] = abb[3] * (T(1) / T(2));
z[30] = z[4] * z[29];
z[1] = abb[9] * z[1];
z[31] = abb[5] * z[20];
z[1] = z[1] + z[31];
z[30] = -z[1] + z[12] + z[30];
z[15] = z[15] + z[20];
z[15] = abb[8] * z[15];
z[25] = abb[48] * (T(3) / T(2)) + z[25];
z[25] = abb[15] * z[25];
z[31] = abb[49] + abb[51];
z[31] = abb[10] * z[31];
z[32] = abb[24] * z[21];
z[15] = z[15] + z[24] + -z[25] + -z[30] + z[31] + -z[32];
z[15] = (T(1) / T(2)) * z[15] + z[17];
z[15] = abb[29] * z[15];
z[16] = abb[8] * z[16];
z[33] = abb[49] + z[20];
z[34] = abb[6] * z[33];
z[35] = z[16] + z[23] + z[31] + z[34];
z[36] = abb[22] * z[6];
z[36] = z[7] + z[36];
z[37] = abb[26] * z[6];
z[3] = z[3] + z[37];
z[37] = -z[3] + z[36];
z[38] = -z[5] + z[10];
z[39] = abb[48] + z[4];
z[39] = abb[3] * z[39];
z[39] = -z[37] + -z[38] + z[39];
z[39] = z[25] + -z[35] + (T(1) / T(2)) * z[39];
z[39] = -z[17] + (T(1) / T(2)) * z[39];
z[39] = abb[32] * z[39];
z[28] = abb[8] * z[28];
z[25] = z[25] + z[28] + z[32];
z[40] = abb[3] * z[0];
z[40] = -z[19] + z[40];
z[41] = abb[25] * z[21];
z[42] = -z[1] + z[41];
z[43] = z[23] + z[42];
z[44] = -abb[47] + z[33];
z[44] = abb[6] * z[44];
z[44] = z[25] + -z[40] + z[43] + z[44];
z[45] = abb[50] + z[0];
z[45] = abb[14] * z[45];
z[46] = (T(1) / T(2)) * z[45];
z[44] = -z[24] + (T(1) / T(2)) * z[44] + -z[46];
z[44] = abb[31] * z[44];
z[47] = abb[3] * z[2];
z[37] = z[37] + -z[47];
z[48] = abb[4] * abb[48];
z[38] = z[37] + z[38] + -z[48];
z[49] = abb[24] * z[6];
z[50] = abb[23] * z[6];
z[51] = z[49] + z[50];
z[52] = abb[8] * abb[48];
z[53] = z[51] + z[52];
z[54] = z[38] + z[53];
z[55] = -z[24] + z[34];
z[54] = z[18] + (T(1) / T(2)) * z[54] + z[55];
z[54] = abb[33] * z[54];
z[15] = z[15] + z[39] + z[44] + (T(1) / T(2)) * z[54];
z[54] = abb[3] + -abb[6];
z[54] = abb[49] * z[54];
z[56] = abb[4] * z[20];
z[25] = z[24] + -z[25] + z[45] + z[54] + z[56];
z[25] = abb[30] * z[25];
z[8] = z[8] + (T(1) / T(8)) * z[15] + (T(1) / T(16)) * z[25];
z[8] = m1_set::bc<T>[0] * z[8];
z[15] = abb[15] * abb[48];
z[5] = z[5] + z[15];
z[13] = abb[8] * z[13];
z[25] = abb[18] + -abb[21];
z[25] = abb[52] * z[25];
z[54] = abb[0] + abb[17];
z[54] = abb[51] * z[54];
z[57] = abb[5] * abb[48];
z[3] = -z[3] + -z[5] + z[13] + z[25] + -z[49] + z[54] + z[57];
z[3] = (T(1) / T(32)) * z[3] + (T(1) / T(16)) * z[17];
z[25] = abb[40] * z[3];
z[53] = z[15] + z[53];
z[54] = abb[7] * abb[48];
z[7] = z[7] + z[53] + -z[54] + -z[57];
z[7] = (T(1) / T(2)) * z[7] + -z[24] + z[34];
z[7] = (T(1) / T(16)) * z[7];
z[34] = abb[41] * z[7];
z[54] = abb[18] + z[11];
z[57] = abb[52] * z[54];
z[58] = abb[51] * z[9];
z[57] = z[13] + z[57] + -z[58];
z[58] = abb[6] * abb[48];
z[59] = z[37] + z[58];
z[60] = -z[48] + z[51] + -z[57] + z[59];
z[60] = z[14] + (T(1) / T(4)) * z[60];
z[61] = (T(1) / T(8)) * z[60];
z[62] = abb[42] * z[61];
z[8] = abb[56] + z[8] + z[25] + z[34] + z[62];
z[13] = z[13] + -z[47];
z[11] = -abb[18] + z[11];
z[25] = abb[52] * (T(1) / T(2));
z[11] = z[11] * z[25];
z[25] = abb[16] * z[20];
z[34] = z[25] + -z[32];
z[29] = abb[0] + abb[17] * (T(1) / T(2)) + z[29];
z[29] = abb[51] * z[29];
z[21] = abb[22] * z[21];
z[11] = z[5] + z[11] + (T(-1) / T(2)) * z[13] + -z[21] + z[22] + -z[29] + -z[34] + -z[41];
z[11] = (T(1) / T(2)) * z[11] + -z[17];
z[11] = abb[29] * z[11];
z[5] = -z[5] + z[10] + z[50] + z[59];
z[5] = (T(1) / T(2)) * z[5] + z[18];
z[5] = abb[32] * z[5];
z[5] = z[5] + z[11];
z[10] = abb[33] * z[60];
z[10] = -z[5] + z[10];
z[10] = abb[33] * z[10];
z[11] = -abb[47] + abb[48];
z[11] = abb[8] * z[11];
z[11] = z[11] + -z[40] + z[42];
z[13] = abb[16] * abb[48];
z[18] = abb[15] * z[26];
z[22] = abb[47] * (T(-1) / T(2)) + z[33];
z[22] = abb[6] * z[22];
z[11] = (T(1) / T(2)) * z[11] + (T(-1) / T(4)) * z[13] + z[18] + z[22] + z[23] + (T(-3) / T(2)) * z[24] + z[32] + -z[46];
z[11] = abb[31] * z[11];
z[22] = -z[25] + (T(1) / T(2)) * z[53] + z[55];
z[26] = -abb[29] + abb[33];
z[26] = z[22] * z[26];
z[11] = z[11] + z[26];
z[11] = abb[31] * z[11];
z[4] = -abb[48] + (T(-1) / T(2)) * z[4];
z[4] = abb[3] * z[4];
z[0] = abb[6] * z[0];
z[0] = z[0] + -z[1] + z[4] + -z[12] + z[16] + z[31] + z[36];
z[0] = (T(1) / T(2)) * z[0] + z[17] + -z[18] + z[23] + (T(1) / T(4)) * z[48];
z[0] = prod_pow(abb[32], 2) * z[0];
z[1] = abb[15] * z[20];
z[4] = abb[3] * z[20];
z[12] = z[1] + z[4] + -z[21] + -z[42];
z[12] = abb[35] * z[12];
z[16] = z[21] + z[43];
z[18] = -z[4] + z[16];
z[26] = abb[6] * z[20];
z[26] = -z[1] + z[18] + z[26] + z[56];
z[29] = abb[37] * z[26];
z[32] = z[32] + z[55];
z[36] = abb[8] * z[20];
z[18] = z[18] + z[32] + z[36];
z[40] = abb[38] * z[18];
z[0] = z[0] + z[10] + z[11] + z[12] + z[29] + z[40];
z[10] = -z[37] + z[57];
z[10] = (T(1) / T(2)) * z[10] + z[34] + -z[50] + -z[58];
z[10] = (T(1) / T(2)) * z[10] + -z[14];
z[10] = abb[33] * z[10];
z[4] = -z[4] + z[19] + -z[21];
z[11] = -abb[6] * abb[47];
z[1] = z[1] + z[4] + z[11] + -z[24];
z[1] = abb[30] * z[1];
z[11] = abb[47] + abb[49];
z[12] = z[11] + z[20];
z[12] = abb[6] * z[12];
z[4] = z[4] + -z[12] + -z[23] + -z[36];
z[12] = -z[4] + -z[34];
z[12] = abb[34] * z[12];
z[19] = -abb[31] * z[22];
z[1] = z[1] + z[5] + z[10] + (T(1) / T(2)) * z[12] + z[19];
z[1] = z[1] * z[27];
z[3] = -abb[53] * z[3];
z[5] = abb[15] * z[2];
z[10] = -abb[3] * z[33];
z[10] = z[5] + z[10] + z[16] + z[28] + z[32] + -z[45];
z[10] = abb[30] * z[10];
z[12] = -abb[32] * z[26];
z[15] = -z[15] + z[48] + -z[49] + -z[52];
z[16] = -abb[6] * abb[49];
z[15] = (T(1) / T(2)) * z[15] + z[16] + z[24];
z[15] = abb[33] * z[15];
z[16] = abb[29] * z[18];
z[12] = z[12] + z[15] + z[16];
z[10] = (T(1) / T(4)) * z[10] + (T(1) / T(2)) * z[12] + -z[44];
z[10] = abb[30] * z[10];
z[5] = -z[5] + -z[25] + -z[30] + z[35];
z[5] = (T(1) / T(2)) * z[5] + z[17];
z[5] = abb[29] * z[5];
z[5] = (T(1) / T(2)) * z[5] + z[39];
z[5] = abb[29] * z[5];
z[12] = z[38] + z[51];
z[15] = -abb[50] + abb[51] + abb[48] * (T(-7) / T(6));
z[15] = abb[8] * z[15];
z[12] = (T(-1) / T(6)) * z[12] + z[15] + -z[31] + z[45];
z[15] = -abb[49] + abb[48] * (T(-1) / T(8));
z[15] = abb[6] * z[15];
z[15] = -z[15] + z[17];
z[12] = (T(1) / T(4)) * z[12] + (T(1) / T(6)) * z[14] + (T(-1) / T(3)) * z[15] + (T(1) / T(12)) * z[24];
z[12] = prod_pow(m1_set::bc<T>[0], 2) * z[12];
z[5] = z[5] + z[10] + z[12];
z[10] = -abb[21] * z[2];
z[2] = abb[18] * z[2];
z[11] = -abb[50] + -z[11];
z[11] = abb[19] * z[11];
z[12] = -abb[19] + -abb[20];
z[12] = abb[48] * z[12];
z[14] = abb[8] * abb[52];
z[6] = abb[27] * z[6];
z[2] = z[2] + z[6] + -z[10] + -z[11] + -z[12] + z[14];
z[6] = abb[28] + (T(-1) / T(4)) * z[9];
z[6] = abb[52] * z[6];
z[9] = abb[51] * z[54];
z[2] = (T(-1) / T(64)) * z[2] + (T(1) / T(16)) * z[6] + (T(1) / T(64)) * z[9];
z[2] = abb[39] * z[2];
z[6] = -abb[54] * z[7];
z[4] = (T(-1) / T(16)) * z[4] + (T(1) / T(32)) * z[49];
z[4] = abb[36] * z[4];
z[7] = -abb[55] * z[61];
z[9] = -abb[35] + -abb[36];
z[9] = z[9] * z[13];
z[0] = abb[43] + (T(1) / T(16)) * z[0] + z[1] + z[2] + z[3] + z[4] + (T(1) / T(8)) * z[5] + z[6] + z[7] + (T(1) / T(32)) * z[9];
return {z[8], z[0]};
}


template <typename T> std::complex<T> f_4_98_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.1306505686045385620075262106798967689942021315288860025707059245"),stof<T>("0.16009782692239804887998605163870631930564000576413761888228600169")}, std::complex<T>{stof<T>("0.054959279326463920455433521924557460188511729554029494389678095792"),stof<T>("-0.024735305286324315450120188093674934292960353648416181589803239922")}, std::complex<T>{stof<T>("0.33701468064558185446866412470019830319937022914251869924624946374"),stof<T>("-0.14561875400977267489976611882910214047667878027641329937096211061")}, std::complex<T>{stof<T>("0.074676460257017031162052597692907892008474808988933519979409772064"),stof<T>("0.04224359904496635522756600051847271642117857723275378374204635439")}, std::complex<T>{stof<T>("0.08118023641783776518816272675970876612021329015729065179285492045"),stof<T>("-0.051353341293645892176786036393003526554956333792846185870425911794")}, std::complex<T>{stof<T>("-0.09878707965036109190184511942008702383650409213743360019279322877"),stof<T>("0.13801363516870842049040280265772312585990829028334043153424807411")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_98_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_98_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(128)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(32)) * (-v[3] + v[5])) / tend;


		return (abb[47] + abb[48] + abb[50]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[47] + abb[48] + abb[50];
z[1] = abb[30] + -abb[32];
return abb[11] * m1_set::bc<T>[0] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_98_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(512)) * (-v[3] + v[5]) * (4 * (-4 + -2 * v[0] + 2 * v[1] + v[3] + -v[5] + -m1_set::bc<T>[1] * (4 + v[3] + v[5])) + m1_set::bc<T>[2] * (12 * v[0] + -4 * v[1] + 3 * (8 + v[3] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = ((2 + 2 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (T(-1) / T(64)) * (-v[3] + v[5])) / tend;


		return (abb[47] + abb[48] + abb[50]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[29] + -abb[30] + abb[32];
z[0] = abb[32] * z[0];
z[1] = abb[29] * abb[30];
z[0] = -abb[35] + abb[37] + abb[38] + z[0] + z[1];
z[1] = abb[47] + abb[48] + abb[50];
return abb[11] * (T(1) / T(16)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_98_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.14873367333180748396883801988726134710133084534528226533555621794"),stof<T>("0.291845677891083424916745273505581526316684254359053906837983272")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,57> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W21(k,dl), dlog_W22(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), T{0}, rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), T{0}};
abb[43] = SpDLog_f_4_98_W_16_Im(t, path, abb);
abb[56] = SpDLog_f_4_98_W_16_Re(t, path, abb);

                    
            return f_4_98_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_98_DLogXconstant_part(base_point<T>, kend);
	value += f_4_98_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_98_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_98_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_98_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_98_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_98_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_98_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
