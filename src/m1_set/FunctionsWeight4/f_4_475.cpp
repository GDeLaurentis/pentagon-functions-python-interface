/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_475.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_475_abbreviated (const std::array<T,62>& abb) {
T z[155];
z[0] = 2 * abb[55];
z[1] = 2 * abb[50];
z[2] = abb[54] + z[1];
z[3] = 2 * abb[57];
z[4] = -abb[47] + z[0] + -z[2] + z[3];
z[5] = 2 * abb[17];
z[5] = z[4] * z[5];
z[6] = abb[7] + -abb[13];
z[7] = 3 * abb[5];
z[8] = z[6] + z[7];
z[9] = 2 * abb[8];
z[10] = 2 * abb[4];
z[11] = abb[2] + z[8] + -z[9] + -z[10];
z[12] = 2 * abb[47];
z[13] = z[11] * z[12];
z[14] = -abb[48] + abb[54];
z[15] = 2 * abb[51];
z[16] = z[14] + z[15];
z[17] = abb[7] * z[16];
z[16] = abb[16] * z[16];
z[17] = -z[16] + z[17];
z[18] = -abb[48] + abb[51];
z[19] = 2 * abb[2];
z[20] = z[18] * z[19];
z[21] = 4 * abb[50];
z[22] = z[14] + z[21];
z[23] = abb[5] * z[22];
z[6] = -abb[0] + -abb[2] + z[6];
z[24] = 2 * abb[45];
z[6] = z[6] * z[24];
z[8] = abb[0] + -abb[8] + z[8];
z[8] = z[3] * z[8];
z[25] = abb[0] + z[19];
z[26] = abb[13] + -abb[16] + z[25];
z[27] = 2 * abb[43];
z[26] = z[26] * z[27];
z[27] = abb[48] + abb[54];
z[28] = z[3] + z[27];
z[28] = abb[4] * z[28];
z[29] = -abb[7] + abb[16];
z[30] = abb[4] + abb[8];
z[31] = abb[5] + z[29] + -z[30];
z[31] = abb[46] * z[31];
z[32] = -abb[2] + z[30];
z[33] = -abb[5] + z[32];
z[34] = 6 * abb[52];
z[33] = z[33] * z[34];
z[32] = abb[0] + z[32];
z[35] = 4 * abb[55];
z[32] = z[32] * z[35];
z[36] = 2 * abb[0];
z[2] = z[2] * z[36];
z[37] = abb[25] * abb[58];
z[38] = abb[8] * z[27];
z[39] = abb[27] * abb[58];
z[2] = z[2] + z[5] + -z[6] + -z[8] + z[13] + z[17] + z[20] + z[23] + -z[26] + z[28] + z[31] + -z[32] + z[33] + -z[37] + z[38] + -z[39];
z[6] = abb[23] * abb[58];
z[8] = z[2] + -z[6];
z[8] = abb[60] * z[8];
z[13] = 2 * abb[38];
z[23] = 2 * abb[36];
z[26] = z[13] + z[23];
z[28] = -abb[39] + z[26];
z[31] = abb[34] * (T(1) / T(2));
z[32] = -abb[32] + z[31];
z[33] = abb[34] * z[32];
z[37] = -abb[37] + z[33];
z[38] = abb[32] * abb[33];
z[40] = (T(1) / T(2)) * z[38];
z[41] = abb[29] + -abb[32];
z[42] = abb[30] * z[41];
z[43] = -abb[32] + abb[33];
z[44] = -abb[29] + (T(-1) / T(2)) * z[43];
z[44] = abb[29] * z[44];
z[45] = abb[29] + abb[32];
z[46] = -abb[31] + z[45];
z[46] = abb[31] * z[46];
z[44] = -z[28] + z[37] + z[40] + z[42] + z[44] + z[46];
z[44] = abb[7] * z[44];
z[46] = abb[29] + z[43];
z[47] = 2 * z[46];
z[48] = -abb[30] + z[47];
z[48] = abb[30] * z[48];
z[49] = 4 * abb[38];
z[48] = z[48] + z[49];
z[50] = -abb[29] + abb[30];
z[51] = 4 * abb[31];
z[52] = z[50] * z[51];
z[53] = 2 * z[43];
z[54] = abb[29] + z[53];
z[55] = abb[29] * z[54];
z[56] = 4 * abb[39];
z[52] = z[23] + z[48] + z[52] + -z[55] + -z[56];
z[55] = abb[9] * z[52];
z[57] = 2 * abb[31];
z[58] = z[43] + -4 * z[50] + z[57];
z[58] = abb[31] * z[58];
z[59] = abb[30] * (T(1) / T(2));
z[60] = -z[46] + z[59];
z[60] = abb[30] * z[60];
z[61] = abb[29] * z[43];
z[62] = -abb[37] + z[61];
z[63] = z[60] + z[62];
z[64] = -abb[29] + z[43];
z[65] = abb[34] * (T(5) / T(2)) + z[64];
z[65] = abb[34] * z[65];
z[66] = 3 * abb[36];
z[58] = -3 * abb[38] + z[56] + z[58] + z[63] + -z[65] + -z[66];
z[65] = abb[15] * z[58];
z[67] = 3 * abb[31];
z[68] = 2 * z[64];
z[69] = z[67] + z[68];
z[69] = abb[31] * z[69];
z[70] = 3 * abb[34];
z[71] = z[68] + z[70];
z[71] = abb[34] * z[71];
z[72] = 2 * abb[39];
z[73] = -z[23] + z[72];
z[69] = -z[49] + -z[69] + z[71] + z[73];
z[74] = abb[11] * z[69];
z[75] = abb[38] + -abb[39];
z[76] = abb[29] + -abb[33];
z[77] = abb[34] * z[76];
z[78] = z[75] + z[77];
z[79] = prod_pow(abb[29], 2);
z[80] = (T(1) / T(2)) * z[79];
z[81] = z[78] + -z[80];
z[82] = abb[31] * (T(1) / T(2));
z[83] = -abb[29] + z[82];
z[83] = abb[31] * z[83];
z[83] = abb[37] + z[83];
z[84] = z[38] + z[42] + z[81] + z[83];
z[85] = abb[8] * z[84];
z[86] = z[38] + z[61];
z[87] = abb[31] * z[76];
z[59] = -abb[33] + z[59];
z[59] = abb[30] * z[59];
z[78] = abb[36] + z[59] + z[78] + (T(1) / T(2)) * z[86] + -z[87];
z[86] = abb[4] * z[78];
z[75] = abb[36] + z[75] + z[80] + z[83];
z[80] = abb[3] * z[75];
z[83] = prod_pow(abb[34], 2);
z[87] = -z[79] + z[83];
z[87] = abb[10] * z[87];
z[88] = -abb[33] + z[82];
z[88] = abb[31] * z[88];
z[88] = abb[37] + z[88];
z[59] = -z[59] + z[88];
z[89] = abb[6] * z[59];
z[90] = -abb[32] + z[82];
z[90] = abb[31] * z[90];
z[33] = abb[38] + -z[33] + z[90];
z[91] = abb[5] * z[33];
z[92] = abb[29] * z[64];
z[92] = z[38] + z[92];
z[93] = (T(1) / T(2)) * z[92];
z[77] = z[77] + z[93];
z[94] = -abb[16] * z[77];
z[95] = -abb[3] + abb[15];
z[96] = -4 * abb[10] + z[29] + -z[95];
z[96] = abb[35] * z[96];
z[97] = -abb[30] + abb[31];
z[98] = abb[31] * z[97];
z[98] = abb[37] + z[98];
z[98] = abb[12] * z[98];
z[99] = 4 * z[98];
z[44] = z[44] + z[55] + z[65] + z[74] + z[80] + z[85] + z[86] + 2 * z[87] + z[89] + z[91] + z[94] + z[96] + -z[99];
z[44] = abb[46] * z[44];
z[65] = 2 * abb[16];
z[80] = abb[4] * (T(5) / T(2));
z[85] = 11 * abb[13];
z[36] = abb[7] * (T(13) / T(2)) + abb[2] * (T(31) / T(2)) + -z[36] + z[65] + z[80] + -z[85];
z[36] = abb[43] * z[36];
z[86] = abb[1] + -abb[8];
z[89] = -abb[0] + z[86];
z[91] = abb[2] * (T(17) / T(2));
z[94] = -z[85] + (T(-5) / T(2)) * z[89] + z[91];
z[94] = abb[47] * z[94];
z[96] = 2 * abb[56];
z[100] = z[27] + -z[96];
z[101] = -z[1] + -z[100];
z[102] = abb[4] * z[101];
z[103] = z[15] + -z[27];
z[103] = abb[7] * z[103];
z[29] = -abb[4] + -z[9] + z[29];
z[29] = abb[46] * z[29];
z[104] = abb[24] + abb[26];
z[105] = -abb[22] + -2 * abb[25] + -z[104];
z[105] = abb[58] * z[105];
z[16] = z[16] + -z[29] + -z[36] + z[39] + -z[94] + z[102] + -z[103] + -z[105];
z[29] = abb[7] * abb[56];
z[36] = z[6] + -z[29];
z[94] = abb[7] + abb[8];
z[103] = 5 * abb[0];
z[94] = -17 * z[94] + z[103];
z[94] = abb[1] + abb[4] * (T(-5) / T(3)) + (T(1) / T(3)) * z[94];
z[94] = abb[44] * z[94];
z[105] = abb[2] * z[18];
z[106] = 2 * abb[54] + abb[48] * (T(13) / T(2));
z[107] = abb[51] * (T(-13) / T(2)) + z[106];
z[107] = -3 * abb[50] + (T(1) / T(3)) * z[107];
z[107] = abb[0] * z[107];
z[106] = abb[53] * (T(-3) / T(2)) + (T(1) / T(3)) * z[106];
z[106] = abb[8] * z[106];
z[108] = -abb[48] + abb[53];
z[109] = abb[56] * (T(-4) / T(3)) + abb[50] * (T(11) / T(3)) + (T(3) / T(2)) * z[108];
z[109] = abb[1] * z[109];
z[110] = abb[13] + z[89];
z[110] = abb[57] * z[110];
z[89] = -abb[2] + -z[89];
z[89] = abb[55] * z[89];
z[111] = abb[2] + -abb[8];
z[111] = abb[52] * z[111];
z[85] = abb[7] * (T(17) / T(2)) + -z[85];
z[112] = abb[0] * (T(17) / T(2)) + -z[85];
z[112] = -8 * abb[2] + abb[4] * (T(-5) / T(6)) + (T(1) / T(3)) * z[112];
z[112] = abb[45] * z[112];
z[16] = (T(-1) / T(3)) * z[16] + (T(-2) / T(3)) * z[36] + (T(31) / T(6)) * z[89] + (T(1) / T(2)) * z[94] + (T(17) / T(6)) * z[105] + z[106] + z[107] + z[109] + (T(11) / T(3)) * z[110] + (T(7) / T(3)) * z[111] + z[112];
z[36] = prod_pow(m1_set::bc<T>[0], 2);
z[16] = z[16] * z[36];
z[89] = z[72] + z[79];
z[94] = 2 * abb[32];
z[106] = -abb[33] + z[94];
z[107] = abb[33] * z[106];
z[109] = z[89] + -z[107];
z[110] = -z[76] + z[94];
z[111] = 2 * abb[34];
z[112] = z[110] * z[111];
z[54] = 2 * z[54] + z[67];
z[54] = abb[31] * z[54];
z[47] = abb[30] + z[47];
z[47] = abb[30] * z[47];
z[113] = prod_pow(abb[32], 2);
z[114] = 2 * z[113];
z[54] = -6 * abb[37] + z[13] + -z[47] + z[54] + z[109] + z[112] + -z[114];
z[54] = abb[8] * z[54];
z[112] = 2 * z[74];
z[73] = -z[13] + z[73];
z[115] = 2 * abb[29];
z[116] = -abb[31] + z[115];
z[116] = abb[31] * z[116];
z[117] = 2 * abb[37];
z[116] = z[73] + -z[79] + z[116] + -z[117];
z[118] = abb[3] * z[116];
z[119] = -abb[7] * z[69];
z[71] = -z[13] + z[56] + -z[71];
z[120] = -abb[36] + z[61];
z[121] = 3 * abb[37];
z[122] = -z[120] + z[121];
z[123] = z[43] + z[115];
z[124] = -z[57] * z[123];
z[122] = z[47] + -z[71] + 2 * z[122] + z[124];
z[122] = abb[15] * z[122];
z[124] = -abb[31] + z[94];
z[125] = abb[31] * z[124];
z[126] = -abb[34] + z[94];
z[126] = abb[34] * z[126];
z[125] = -z[13] + z[125] + -z[126];
z[127] = z[7] * z[125];
z[128] = -abb[29] + z[53];
z[128] = abb[29] * z[128];
z[128] = z[107] + z[128];
z[129] = -abb[34] + z[76];
z[130] = abb[34] * z[129];
z[130] = z[114] + z[128] + 6 * z[130];
z[130] = abb[2] * z[130];
z[131] = 3 * abb[29];
z[132] = z[53] + z[131];
z[132] = abb[29] * z[132];
z[47] = z[23] + -z[47] + z[132];
z[132] = abb[9] * z[47];
z[133] = abb[31] + -abb[34];
z[134] = -z[110] * z[133];
z[134] = -abb[36] + abb[38] + abb[39] + z[134];
z[134] = z[10] * z[134];
z[54] = z[54] + -z[112] + 3 * z[118] + z[119] + z[122] + z[127] + z[130] + z[132] + z[134];
z[54] = abb[52] * z[54];
z[119] = -z[43] + z[50];
z[122] = 2 * abb[30];
z[119] = z[119] * z[122];
z[130] = -abb[30] + z[43];
z[132] = abb[31] + 2 * z[130];
z[132] = abb[31] * z[132];
z[134] = abb[34] + z[68];
z[134] = abb[34] * z[134];
z[135] = abb[36] + z[61];
z[132] = z[13] + z[119] + z[132] + -z[134] + 2 * z[135];
z[132] = abb[15] * z[132];
z[134] = abb[34] + z[76];
z[135] = z[111] * z[134];
z[135] = z[128] + z[135];
z[136] = abb[2] * z[135];
z[137] = -z[38] + z[113];
z[138] = abb[29] * abb[32];
z[134] = abb[34] * z[134];
z[134] = -z[134] + z[137] + z[138];
z[138] = abb[13] * z[134];
z[139] = 2 * z[138];
z[74] = z[74] + -z[127] + z[132] + -z[136] + -z[139];
z[132] = z[76] * z[111];
z[136] = -abb[31] + -z[50];
z[136] = z[57] * z[136];
z[109] = -z[13] + z[109] + z[119] + -z[132] + z[136];
z[109] = abb[7] * z[109];
z[136] = abb[34] * z[94];
z[140] = z[113] + z[121] + -z[136];
z[53] = abb[30] + z[53];
z[53] = 5 * abb[31] + 2 * z[53];
z[53] = abb[31] * z[53];
z[141] = prod_pow(abb[30], 2);
z[53] = -z[49] + -z[53] + 2 * z[140] + 3 * z[141];
z[53] = abb[8] * z[53];
z[140] = 2 * abb[33];
z[142] = abb[29] * z[140];
z[142] = z[23] + -z[107] + z[142];
z[143] = z[41] + z[140];
z[143] = abb[30] * (T(3) / T(2)) + 2 * z[143];
z[143] = abb[30] * z[143];
z[144] = -abb[30] + z[82] + -z[140];
z[144] = abb[31] * z[144];
z[121] = z[121] + -z[142] + z[143] + z[144];
z[121] = abb[1] * z[121];
z[143] = 4 * abb[34];
z[144] = -z[51] + z[143];
z[144] = abb[32] * z[144];
z[145] = prod_pow(abb[33], 2);
z[146] = -z[114] + z[145];
z[147] = -abb[30] + z[140];
z[147] = abb[30] * z[147];
z[49] = -z[49] + -z[144] + -z[146] + z[147];
z[144] = abb[4] * z[49];
z[148] = -abb[30] + 2 * z[41];
z[148] = abb[30] * z[148];
z[149] = -abb[29] + z[94];
z[150] = abb[29] * z[149];
z[151] = z[148] + z[150];
z[152] = -z[72] + z[151];
z[153] = -abb[30] + z[41];
z[154] = abb[31] + 2 * z[153];
z[154] = abb[31] * z[154];
z[126] = z[126] + -z[152] + z[154];
z[154] = abb[17] * z[126];
z[80] = abb[1] * (T(-17) / T(2)) + abb[8] * (T(31) / T(2)) + z[80] + z[85] + z[91];
z[36] = (T(1) / T(3)) * z[36];
z[80] = z[36] * z[80];
z[11] = -abb[17] + z[11];
z[85] = abb[60] * z[11];
z[91] = abb[30] + z[82];
z[91] = abb[31] * z[91];
z[91] = -abb[37] + z[91] + (T(-3) / T(2)) * z[141];
z[91] = abb[12] * z[91];
z[80] = z[53] + z[74] + z[80] + 2 * z[85] + 3 * z[91] + z[109] + z[121] + z[144] + -z[154];
z[80] = abb[49] * z[80];
z[85] = -z[61] + -z[66];
z[91] = -z[43] + z[115];
z[109] = -abb[30] + z[91];
z[109] = abb[31] + 2 * z[109];
z[109] = abb[31] * z[109];
z[121] = 6 * abb[38];
z[68] = -abb[34] + z[68];
z[68] = abb[34] * z[68];
z[56] = z[56] + z[68] + 2 * z[85] + z[109] + -z[119] + -z[121];
z[56] = abb[15] * z[56];
z[68] = z[76] + z[94];
z[85] = z[68] * z[111];
z[85] = z[85] + z[121];
z[109] = z[50] + -z[124];
z[109] = z[57] * z[109];
z[89] = z[85] + -z[89] + z[109] + -2 * z[137] + z[148];
z[89] = abb[8] * z[89];
z[109] = 3 * abb[32];
z[121] = -abb[33] + z[109] + z[122] + -z[131];
z[121] = abb[30] * z[121];
z[124] = abb[33] + -z[149];
z[124] = abb[29] * z[124];
z[144] = abb[30] + -abb[33];
z[148] = -abb[31] + -z[144];
z[148] = abb[31] * z[148];
z[66] = z[66] + z[121] + z[124] + z[137] + z[148];
z[121] = 2 * abb[1];
z[66] = z[66] * z[121];
z[124] = -abb[0] * z[134];
z[124] = z[124] + z[138];
z[126] = -abb[7] * z[126];
z[134] = z[50] * z[57];
z[13] = z[13] + z[134] + z[152];
z[134] = abb[9] * z[13];
z[56] = z[56] + z[66] + z[89] + 2 * z[124] + z[126] + z[127] + 3 * z[134];
z[56] = abb[57] * z[56];
z[66] = z[64] + z[111];
z[66] = abb[34] * z[66];
z[62] = z[23] + -z[62] + z[66];
z[66] = 3 * abb[30];
z[89] = -z[43] + z[66] + -z[131];
z[89] = -z[67] + 2 * z[89];
z[89] = abb[31] * z[89];
z[48] = -6 * abb[39] + z[48] + 2 * z[62] + z[89];
z[48] = abb[15] * z[48];
z[62] = 2 * z[76];
z[89] = z[62] + -z[70];
z[89] = abb[34] * z[89];
z[79] = -z[79] + z[89] + z[113];
z[79] = abb[0] * z[79];
z[124] = -abb[31] + 2 * z[50];
z[124] = abb[31] * z[124];
z[124] = -z[73] + z[83] + z[124];
z[124] = abb[7] * z[124];
z[126] = -z[76] + z[111];
z[126] = z[111] * z[126];
z[126] = -z[113] + z[126];
z[127] = abb[2] * z[126];
z[48] = z[48] + -z[55] + z[79] + -z[87] + 2 * z[98] + z[118] + z[124] + z[127];
z[55] = abb[31] * z[94];
z[26] = -z[26] + z[55] + -z[136] + z[151];
z[26] = z[10] * z[26];
z[55] = 5 * abb[37] + z[23];
z[79] = -z[55] + -z[113] + z[150];
z[98] = z[66] + z[140];
z[113] = abb[31] + 2 * z[98];
z[113] = abb[31] * z[113];
z[118] = -5 * abb[30] + 4 * z[41];
z[118] = abb[30] * z[118];
z[79] = 2 * z[79] + z[113] + z[118];
z[79] = abb[1] * z[79];
z[48] = z[26] + 2 * z[48] + z[53] + z[79] + -z[112];
z[48] = abb[55] * z[48];
z[53] = z[110] * z[115];
z[79] = z[41] + -z[140];
z[79] = abb[30] * (T(-9) / T(2)) + 2 * z[79];
z[79] = abb[30] * z[79];
z[82] = z[82] + z[98];
z[82] = abb[31] * z[82];
z[53] = z[53] + -z[55] + z[79] + z[82] + -z[107] + -z[114];
z[53] = abb[1] * z[53];
z[55] = abb[31] + z[122];
z[55] = abb[31] * z[55];
z[55] = z[55] + -z[117];
z[79] = z[55] + -z[119] + -z[142];
z[79] = abb[7] * z[79];
z[82] = abb[33] + z[94];
z[94] = 2 * z[82] + -z[131];
z[94] = abb[29] * z[94];
z[82] = z[82] + -z[115];
z[82] = 2 * z[82];
z[98] = abb[30] + z[82];
z[98] = abb[30] * z[98];
z[110] = 4 * abb[36];
z[98] = z[98] + z[110];
z[94] = z[94] + -z[98];
z[94] = abb[9] * z[94];
z[112] = -abb[29] + z[140];
z[112] = abb[29] * z[112];
z[113] = z[112] + -z[146];
z[118] = abb[0] * z[113];
z[94] = z[94] + -z[118];
z[118] = -abb[31] + z[140];
z[118] = abb[31] * z[118];
z[118] = -z[117] + z[118] + -z[147];
z[119] = -abb[6] * z[118];
z[124] = abb[31] + abb[33];
z[124] = z[57] * z[124];
z[124] = -z[117] + z[124] + -z[141];
z[124] = z[9] * z[124];
z[127] = z[115] * z[149];
z[127] = z[127] + z[146];
z[134] = -z[98] + z[127];
z[136] = abb[4] * z[134];
z[137] = abb[30] + abb[31] * (T(-11) / T(2));
z[137] = abb[31] * z[137];
z[137] = -abb[37] + z[137] + (T(9) / T(2)) * z[141];
z[137] = abb[12] * z[137];
z[53] = z[53] + z[79] + -z[94] + z[119] + z[124] + z[136] + z[137];
z[53] = abb[44] * z[53];
z[79] = z[57] * z[76];
z[73] = z[73] + z[79] + -z[112] + -z[132] + z[147];
z[73] = abb[7] * z[73];
z[49] = abb[8] * z[49];
z[79] = abb[1] * z[134];
z[26] = z[26] + z[49] + z[73] + z[74] + z[79] + -z[94];
z[26] = abb[47] * z[26];
z[49] = abb[4] * z[113];
z[49] = z[49] + -z[139];
z[73] = abb[29] * z[123];
z[73] = z[38] + z[73] + z[89];
z[73] = abb[0] * z[73];
z[74] = abb[33] * z[43];
z[79] = z[61] + -z[74] + 2 * z[83];
z[83] = -abb[7] * z[79];
z[89] = z[92] + z[132];
z[92] = -abb[16] * z[89];
z[19] = -z[19] * z[126];
z[65] = -6 * abb[2] + -2 * abb[7] + -abb[10] + z[65] + -z[103];
z[65] = abb[35] * z[65];
z[19] = z[19] + z[49] + z[65] + z[73] + z[83] + 5 * z[87] + z[92];
z[19] = abb[43] * z[19];
z[61] = z[38] + 3 * z[61];
z[65] = z[115] + -z[140];
z[32] = -z[32] + z[65];
z[32] = abb[34] * z[32];
z[73] = -z[140] + -z[153];
z[73] = abb[30] * z[73];
z[45] = -z[45] + z[140];
z[45] = abb[31] * z[45];
z[28] = -abb[37] + z[28] + z[32] + z[45] + (T(1) / T(2)) * z[61] + z[73];
z[28] = abb[48] * z[28];
z[32] = abb[31] + z[41] + -z[122];
z[32] = abb[31] * z[32];
z[45] = abb[33] + z[109];
z[61] = abb[29] + (T(-1) / T(2)) * z[45];
z[61] = abb[29] * z[61];
z[73] = -abb[30] * z[153];
z[32] = abb[39] + z[32] + -z[37] + z[40] + z[61] + z[73];
z[32] = abb[54] * z[32];
z[37] = -abb[50] * z[52];
z[40] = abb[51] * z[89];
z[61] = abb[53] * z[118];
z[28] = z[28] + z[32] + z[37] + -z[40] + -z[61];
z[28] = abb[7] * z[28];
z[32] = z[66] + z[82];
z[32] = abb[30] * z[32];
z[37] = abb[30] + abb[33];
z[37] = -z[37] * z[51];
z[32] = 8 * abb[37] + z[32] + z[37] + z[110] + -z[127];
z[32] = abb[50] * z[32];
z[37] = z[122] * z[144];
z[37] = z[37] + -z[55] + z[145];
z[55] = abb[53] * z[37];
z[37] = -abb[48] * z[37];
z[73] = -abb[54] * z[118];
z[42] = -abb[36] + z[42];
z[82] = -abb[29] * abb[33];
z[82] = z[38] + z[42] + z[82];
z[82] = z[82] * z[96];
z[32] = z[32] + z[37] + z[55] + z[73] + z[82];
z[32] = abb[1] * z[32];
z[37] = -abb[50] + abb[53] + -z[100];
z[37] = z[37] * z[121];
z[73] = -abb[6] + abb[7];
z[82] = -z[73] + z[86];
z[83] = 2 * abb[44];
z[82] = z[82] * z[83];
z[92] = 2 * abb[53];
z[94] = -z[92] + z[100];
z[94] = abb[7] * z[94];
z[30] = z[30] + z[73];
z[30] = abb[46] * z[30];
z[73] = z[14] + z[92];
z[100] = -abb[6] + abb[8];
z[100] = z[73] * z[100];
z[109] = abb[22] + abb[25];
z[109] = abb[58] * z[109];
z[30] = z[30] + z[37] + -z[82] + z[94] + -z[100] + z[102] + z[109];
z[37] = -abb[59] * z[30];
z[82] = (T(1) / T(2)) * z[27];
z[94] = abb[50] + z[82];
z[100] = -abb[19] + abb[20];
z[94] = z[94] * z[100];
z[102] = -abb[18] + abb[21];
z[109] = z[100] + z[102];
z[110] = abb[46] * (T(1) / T(2));
z[109] = z[109] * z[110];
z[110] = abb[51] + (T(1) / T(2)) * z[14];
z[110] = abb[21] * z[110];
z[82] = -abb[51] + z[82];
z[82] = abb[18] * z[82];
z[100] = abb[18] + z[100];
z[100] = abb[56] * z[100];
z[112] = abb[28] * abb[58];
z[102] = abb[43] * z[102];
z[82] = z[82] + z[94] + -z[100] + -z[102] + -z[109] + z[110] + (T(1) / T(2)) * z[112];
z[94] = -abb[61] * z[82];
z[100] = abb[25] * z[84];
z[31] = z[31] + z[64];
z[31] = abb[34] * z[31];
z[31] = -abb[38] + z[31];
z[64] = -z[31] + z[90] + z[93];
z[64] = abb[26] * z[64];
z[90] = abb[29] * z[46];
z[90] = -z[38] + z[90];
z[93] = (T(1) / T(2)) * z[90];
z[60] = -abb[36] + -z[60] + z[88] + -z[93];
z[60] = abb[24] * z[60];
z[88] = -z[42] + z[93];
z[93] = -abb[22] * z[88];
z[102] = -abb[35] + z[77];
z[102] = abb[27] * z[102];
z[60] = z[60] + z[64] + z[93] + z[100] + z[102];
z[60] = abb[58] * z[60];
z[58] = -abb[48] * z[58];
z[64] = -z[117] + z[120];
z[93] = -z[46] * z[122];
z[100] = z[115] + z[130];
z[100] = abb[31] + 2 * z[100];
z[100] = abb[31] * z[100];
z[64] = 2 * z[64] + z[71] + z[93] + z[100];
z[64] = z[1] * z[64];
z[71] = abb[31] * z[43];
z[31] = abb[36] + -z[31] + z[63] + z[71];
z[31] = abb[54] * z[31];
z[31] = z[31] + z[58] + z[64];
z[31] = abb[15] * z[31];
z[58] = 8 * abb[50] + z[14];
z[58] = -z[58] * z[95];
z[63] = abb[51] + z[22];
z[63] = abb[0] * z[63];
z[64] = abb[10] * z[15];
z[63] = z[63] + z[64];
z[64] = abb[10] + z[95];
z[64] = 3 * abb[2] + 2 * z[64] + -z[103];
z[0] = z[0] * z[64];
z[64] = -abb[2] + z[95];
z[34] = z[34] * z[64];
z[64] = -abb[26] * abb[58];
z[0] = z[0] + z[17] + z[34] + z[58] + 2 * z[63] + z[64];
z[0] = abb[35] * z[0];
z[17] = -z[46] * z[115];
z[34] = abb[34] + -z[62];
z[34] = abb[34] * z[34];
z[17] = z[17] + z[34] + -z[107];
z[17] = abb[0] * z[17];
z[34] = abb[7] * z[135];
z[46] = 5 * abb[34] + -z[76];
z[46] = z[46] * z[111];
z[46] = z[46] + -z[114] + z[128];
z[46] = abb[2] * z[46];
z[25] = -abb[10] + z[25];
z[25] = abb[35] * z[25];
z[25] = -z[25] + z[87];
z[17] = z[17] + -3 * z[25] + z[34] + z[46] + -z[49];
z[17] = abb[45] * z[17];
z[25] = abb[29] * z[91];
z[34] = -z[129] * z[143];
z[25] = z[25] + z[34] + z[74] + -z[114];
z[25] = abb[50] * z[25];
z[34] = z[18] * z[79];
z[46] = -abb[54] * z[89];
z[25] = z[25] + z[34] + z[46];
z[25] = abb[0] * z[25];
z[34] = -abb[48] * z[52];
z[13] = -abb[54] * z[13];
z[46] = -abb[50] * z[47];
z[47] = -abb[29] * z[41];
z[42] = z[42] + z[47];
z[42] = z[42] * z[96];
z[13] = z[13] + z[34] + z[42] + z[46];
z[13] = abb[9] * z[13];
z[34] = z[122] + -z[140];
z[42] = z[34] + -z[41];
z[42] = abb[30] * z[42];
z[46] = abb[29] + abb[31] * (T(-3) / T(2)) + -z[122];
z[46] = abb[31] * z[46];
z[42] = abb[37] + z[42] + z[46] + z[74] + -z[81];
z[42] = abb[48] * z[42];
z[46] = -abb[54] * z[84];
z[42] = z[42] + z[46] + -z[55];
z[42] = abb[8] * z[42];
z[46] = z[106] + -z[115];
z[46] = 2 * z[46] + z[66];
z[46] = abb[30] * z[46];
z[47] = -z[57] * z[68];
z[49] = abb[29] + -2 * z[106];
z[49] = abb[29] * z[49];
z[46] = 6 * abb[36] + z[46] + z[47] + z[49] + -z[72] + z[85];
z[46] = abb[57] * z[46];
z[47] = -z[27] * z[78];
z[45] = -z[45] + z[115];
z[45] = abb[29] * z[45];
z[45] = -z[38] + z[45] + z[98];
z[45] = abb[50] * z[45];
z[49] = -abb[32] + z[76];
z[52] = abb[29] * z[49];
z[38] = z[38] + z[52];
z[52] = abb[56] * z[38];
z[45] = z[45] + z[46] + z[47] + z[52];
z[45] = abb[4] * z[45];
z[46] = -z[21] * z[116];
z[47] = z[14] * z[75];
z[46] = z[46] + z[47];
z[46] = abb[3] * z[46];
z[47] = -abb[48] + z[1];
z[52] = abb[11] * z[47] * z[69];
z[4] = z[4] * z[154];
z[55] = -abb[59] + z[84];
z[55] = z[6] * z[55];
z[58] = z[41] * z[122];
z[23] = z[23] + -z[58] + z[90];
z[58] = -abb[50] + abb[56];
z[23] = z[23] * z[58];
z[58] = abb[46] + z[101];
z[36] = abb[59] + z[36];
z[36] = z[36] * z[58];
z[62] = -abb[46] + z[27];
z[63] = -z[62] * z[88];
z[23] = z[23] + z[36] + z[63];
z[23] = abb[14] * z[23];
z[36] = -z[14] * z[59];
z[36] = z[36] + z[61];
z[36] = abb[6] * z[36];
z[59] = -z[1] * z[125];
z[33] = z[14] * z[33];
z[33] = z[33] + z[59];
z[33] = abb[5] * z[33];
z[59] = -z[105] * z[135];
z[61] = z[14] * z[77];
z[40] = z[40] + z[61];
z[40] = abb[16] * z[40];
z[15] = -z[15] * z[87];
z[38] = -z[29] * z[38];
z[61] = abb[53] * z[99];
z[0] = z[0] + z[4] + z[8] + z[13] + z[15] + z[16] + z[17] + z[19] + z[23] + z[25] + z[26] + z[28] + z[31] + z[32] + z[33] + z[36] + z[37] + z[38] + z[40] + z[42] + z[44] + z[45] + z[46] + z[48] + z[52] + z[53] + z[54] + z[55] + z[56] + z[59] + z[60] + z[61] + z[80] + z[94];
z[4] = -z[50] + z[133];
z[8] = abb[15] * z[4];
z[13] = z[70] + -z[76];
z[13] = abb[2] * z[13];
z[15] = -abb[29] + abb[34];
z[15] = abb[10] * z[15];
z[16] = abb[12] * z[97];
z[17] = z[15] + -z[16];
z[19] = abb[7] * z[4];
z[23] = z[67] + -z[144];
z[25] = abb[8] * z[23];
z[26] = abb[9] * z[50];
z[28] = 2 * z[26];
z[31] = abb[11] * z[133];
z[32] = 2 * z[31];
z[33] = abb[33] + z[111];
z[33] = abb[0] * z[33];
z[36] = abb[33] + z[57];
z[37] = -abb[1] * z[36];
z[25] = 3 * z[8] + -z[13] + z[17] + z[19] + z[25] + z[28] + -z[32] + z[33] + z[37];
z[25] = z[25] * z[35];
z[33] = -abb[32] + abb[34];
z[34] = z[33] + z[34] + -z[51];
z[34] = abb[8] * z[34];
z[7] = z[7] * z[133];
z[35] = -abb[7] * z[133];
z[35] = z[8] + z[35];
z[37] = abb[32] + -z[65] + z[70];
z[37] = abb[2] * z[37];
z[31] = 4 * z[31];
z[38] = -abb[4] * z[133];
z[28] = z[7] + z[28] + -z[31] + z[34] + 2 * z[35] + z[37] + z[38];
z[28] = abb[52] * z[28];
z[1] = 2 * abb[48] + abb[54] + -abb[56] + -z[1];
z[1] = z[1] * z[26];
z[1] = z[1] + z[28];
z[28] = -abb[32] + z[70];
z[34] = z[28] + z[76];
z[35] = abb[13] * z[34];
z[7] = z[7] + z[35];
z[34] = -abb[0] * z[34];
z[37] = abb[32] + abb[33];
z[38] = -abb[30] + -abb[34] + z[37];
z[40] = z[38] + -z[57];
z[40] = abb[8] * z[40];
z[42] = z[49] + z[67];
z[42] = abb[1] * z[42];
z[34] = z[7] + -2 * z[8] + z[19] + -3 * z[26] + z[34] + z[40] + z[42];
z[34] = z[3] * z[34];
z[40] = abb[0] * z[76];
z[40] = z[26] + z[40];
z[42] = -z[41] + -z[57];
z[42] = abb[7] * z[42];
z[44] = -z[57] + z[144];
z[45] = z[9] * z[44];
z[41] = z[41] + z[122];
z[46] = z[41] + -z[57];
z[46] = abb[1] * z[46];
z[48] = abb[6] * z[97];
z[52] = abb[4] * z[144];
z[42] = 5 * z[16] + z[40] + z[42] + z[45] + z[46] + -z[48] + -z[52];
z[42] = z[42] * z[83];
z[8] = 4 * z[8] + -z[31];
z[17] = z[17] + z[26];
z[26] = (T(1) / T(2)) * z[37];
z[31] = -abb[30] + z[26];
z[45] = z[31] + z[133];
z[46] = abb[4] + abb[7];
z[46] = z[45] * z[46];
z[53] = abb[5] * z[133];
z[54] = -abb[34] + z[26];
z[55] = -abb[16] * z[54];
z[56] = abb[8] * z[38];
z[17] = -z[8] + -4 * z[17] + z[46] + -z[48] + -z[53] + z[55] + z[56];
z[17] = abb[46] * z[17];
z[46] = -5 * z[15] + -z[35];
z[55] = -6 * abb[29] + z[37] + z[143];
z[55] = abb[0] * z[55];
z[43] = z[43] + z[143];
z[56] = abb[7] * z[43];
z[59] = -z[37] + z[111];
z[60] = abb[16] * z[59];
z[10] = z[10] * z[76];
z[10] = z[10] + 4 * z[13] + 2 * z[46] + z[55] + z[56] + z[60];
z[10] = abb[43] * z[10];
z[13] = abb[30] + z[26] + -z[115] + -z[133];
z[13] = abb[54] * z[13];
z[46] = abb[51] * z[59];
z[55] = abb[48] * z[45];
z[56] = -z[92] * z[97];
z[21] = z[21] * z[50];
z[13] = z[13] + z[21] + z[46] + z[55] + z[56];
z[13] = abb[7] * z[13];
z[21] = abb[32] + -9 * abb[34] + z[65];
z[21] = abb[2] * z[21];
z[50] = -abb[32] + z[131];
z[50] = abb[0] * z[50];
z[55] = -abb[7] * z[28];
z[56] = -abb[4] * z[76];
z[21] = 3 * z[15] + z[21] + z[35] + z[50] + z[55] + z[56];
z[21] = z[21] * z[24];
z[24] = -z[27] * z[45];
z[27] = -z[37] + z[122];
z[35] = abb[50] * z[27];
z[37] = -z[37] + z[115];
z[45] = -abb[56] * z[37];
z[3] = z[3] * z[4];
z[3] = z[3] + z[24] + z[35] + z[45];
z[3] = abb[4] * z[3];
z[24] = abb[2] * z[28];
z[7] = z[7] + -z[24] + -z[32];
z[24] = -z[86] * z[144];
z[19] = -z[7] + -z[19] + z[24] + z[40];
z[12] = z[12] * z[19];
z[19] = -z[44] * z[108];
z[24] = -abb[54] * z[97];
z[32] = abb[30] + z[36];
z[32] = abb[50] * z[32];
z[36] = -abb[30] + -z[49];
z[36] = abb[56] * z[36];
z[19] = z[19] + z[24] + z[32] + z[36];
z[19] = z[19] * z[121];
z[18] = -z[18] * z[43];
z[24] = abb[54] * z[59];
z[32] = -abb[32] + -3 * abb[33] + -z[143];
z[32] = abb[50] * z[32];
z[18] = z[18] + z[24] + z[32];
z[18] = abb[0] * z[18];
z[14] = z[14] * z[54];
z[14] = z[14] + -z[46];
z[14] = abb[16] * z[14];
z[24] = -abb[31] + z[26];
z[24] = z[24] * z[104];
z[26] = abb[25] * z[38];
z[32] = abb[22] * z[31];
z[24] = z[24] + z[26] + z[32];
z[24] = abb[58] * z[24];
z[26] = abb[34] + z[51] + z[130];
z[26] = abb[48] * z[26];
z[32] = z[44] * z[92];
z[36] = -abb[54] * z[38];
z[26] = z[26] + z[32] + z[36];
z[26] = abb[8] * z[26];
z[5] = -z[4] * z[5];
z[8] = -z[8] * z[47];
z[32] = 4 * abb[53];
z[32] = -z[16] * z[32];
z[36] = z[48] * z[73];
z[22] = -z[22] * z[53];
z[20] = z[20] * z[28];
z[28] = z[29] * z[37];
z[29] = z[39] * z[54];
z[37] = z[6] * z[38];
z[15] = abb[51] * z[15];
z[1] = 2 * z[1] + z[3] + z[5] + z[8] + z[10] + z[12] + z[13] + z[14] + 4 * z[15] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + z[24] + z[25] + z[26] + z[28] + z[29] + z[32] + z[34] + z[36] + z[37] + z[42];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[41] * z[2];
z[3] = -abb[40] * z[30];
z[5] = -abb[42] * z[82];
z[8] = z[33] + z[57];
z[8] = abb[7] * z[8];
z[9] = z[9] * z[23];
z[10] = -abb[1] * z[41];
z[4] = abb[17] * z[4];
z[4] = z[4] + -z[7] + z[8] + z[9] + z[10] + -3 * z[16] + z[52];
z[4] = m1_set::bc<T>[0] * z[4];
z[7] = abb[41] * z[11];
z[4] = z[4] + z[7];
z[4] = abb[49] * z[4];
z[7] = z[31] * z[62];
z[8] = abb[56] * z[27];
z[7] = z[7] + z[8] + -z[35];
z[7] = m1_set::bc<T>[0] * z[7];
z[8] = abb[40] * z[58];
z[7] = z[7] + z[8];
z[7] = abb[14] * z[7];
z[8] = -abb[40] + -abb[41];
z[6] = z[6] * z[8];
z[1] = z[1] + z[2] + z[3] + 2 * z[4] + z[5] + z[6] + z[7];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_475_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("26.768985058639030131879448466204587843194209849485869459626654332"),stof<T>("-71.09976665845057414018308566493259719417126011945365390520978474")}, std::complex<T>{stof<T>("-68.998865259681302114571280956760981179530158456884751627751743829"),stof<T>("33.462032315675796419244727867436187632553147222387032094853331986")}, std::complex<T>{stof<T>("-42.916313497125379960109039975440756951706763485975498496114291434"),stof<T>("50.097203268269536149451995402961805638110798703170824860421667433")}, std::complex<T>{stof<T>("-7.7599537243468496712143941279548803188791498218471930075541938561"),stof<T>("2.8861775862569761974940276305852226522135036017093535981858642458")}, std::complex<T>{stof<T>("-18.595137544313140632617906208723942820387168351627831868387623813"),stof<T>("-14.550432555754452788706755917838872146834908138660943021255930274")}, std::complex<T>{stof<T>("-5.0575627556733391880167629940750347462215625757424485869048046118"),stof<T>("-4.5664558505429241331392596983340719583697070838177303934981038314")}, std::complex<T>{stof<T>("53.733539673834322450951809135243292402554831344156907201392758649"),stof<T>("-28.690179745535159152865625591053117529483797426873522866633384539")}, std::complex<T>{stof<T>("34.457700471840500033963841532815684720944437378278081686748100531"),stof<T>("22.23167431373846896959060183318350230037359763187093672121801076")}, std::complex<T>{stof<T>("16.147328438486349828229591509236169108512553636489629036487637101"),stof<T>("21.002563390181037990731090261970791556060461416282829044788117307")}, std::complex<T>{stof<T>("-17.271625219662090912607795267992703281726162263884459014102458665"),stof<T>("-21.138988593369706307284751557739988174950239183665002331673644356")}, std::complex<T>{stof<T>("-3.329811958466160968998434387206254043411841238899987442028638633"),stof<T>("-19.322285125895090055085858194221942249904257934174452249475877721")}, std::complex<T>{stof<T>("-2.9218224286660744113546553976309690196603579241088665283397350529"),stof<T>("-6.6126383472427798164134010891832985216066551560037079983624411642")}, std::complex<T>{stof<T>("-14.483684283504898638158415130943135866560687868288878251996252622"),stof<T>("-8.545319157168662992939137604362808736006569133733018381228334482")}, std::complex<T>{stof<T>("2.2707742825045578234848655102323880671024945024242466871796446223"),stof<T>("4.0356141500127251131740803438635949520553790971436215987294526578")}, std::complex<T>{stof<T>("24.303748446147996408504459090197558519166594349054900296452518855"),stof<T>("21.693912603527431625085336361492647674755891281338759814387022612")}, std::complex<T>{stof<T>("-1.2575190271596537991920991302195900456713969230739235361053132628"),stof<T>("1.262423601462443931986568394750185544768544624668046192565869342")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(abs(k.W[34])) - rlog(abs(kbase.W[34])), rlog(k.W[72].real()/kbase.W[72].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_475_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_475_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-7.350668572901619165007643210938794348163179690218144484593632625"),stof<T>("16.985472462834486492024365887645929422402519759398212833850673346")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({34});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,62> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W73(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(abs(kend.W[34])) - rlog(abs(k.W[34])), rlog(kend.W[72].real()/k.W[72].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_475_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_475_DLogXconstant_part(base_point<T>, kend);
	value += f_4_475_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_475_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_475_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_475_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_475_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_475_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_475_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
