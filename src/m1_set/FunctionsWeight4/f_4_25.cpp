/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_25.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_25_abbreviated (const std::array<T,9>& abb) {
T z[6];
z[0] = abb[1] + abb[2];
z[1] = abb[6] + abb[7];
z[0] = z[0] * z[1];
z[2] = prod_pow(abb[4], 2);
z[2] = -abb[8] + z[2];
z[2] = z[0] * z[2];
z[3] = -abb[1] + abb[2] * (T(-5) / T(6));
z[3] = z[1] * z[3];
z[4] = abb[6] + -abb[7];
z[4] = abb[0] * z[4];
z[3] = z[3] + (T(-1) / T(6)) * z[4];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[5] = abb[1] + -abb[2];
z[1] = z[1] * z[5];
z[1] = z[1] + 2 * z[4];
z[1] = prod_pow(abb[3], 2) * z[1];
z[1] = z[1] + z[2] + z[3];
z[2] = abb[4] * m1_set::bc<T>[0];
z[2] = -abb[5] + -2 * z[2];
z[0] = z[0] * z[2];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_25_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-7.5622380660705743894746219887445304495811624597824558703195410604"),stof<T>("8.7108938750076768380150066101209595122101499136975628868840538663")}, std::complex<T>{stof<T>("-18.547848414472447225081140770364272386182591875816614974184009492"),stof<T>("8.710893875007676838015006610120959512210149913697562886884053866")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[21].real()/kbase.W[21].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_25_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_25_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-1.0687013275636378086030554212271009507349824839377323578760741417"),stof<T>("-5.1987223898501484255682212988060120795291194463086008118910466762")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,9> abb = {dl[5], dl[2], dlog_W22(k,dl), f_1_4(k), f_1_5(k), f_2_7_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[21].real()/k.W[21].real()), f_2_7_re(k)};

                    
            return f_4_25_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_25_DLogXconstant_part(base_point<T>, kend);
	value += f_4_25_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_25_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_25_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_25_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_25_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_25_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_25_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
