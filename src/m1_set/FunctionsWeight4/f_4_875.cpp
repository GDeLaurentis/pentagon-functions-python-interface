/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_875.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_875_abbreviated (const std::array<T,86>& abb) {
T z[119];
z[0] = abb[34] + -abb[36];
z[1] = -abb[38] + z[0];
z[1] = abb[60] * z[1];
z[2] = 2 * abb[63] + -abb[66];
z[3] = 2 * z[2];
z[4] = abb[38] * z[3];
z[5] = abb[60] + z[3];
z[6] = abb[40] * z[5];
z[1] = -z[1] + z[4] + -z[6];
z[1] = m1_set::bc<T>[0] * z[1];
z[4] = abb[48] + abb[50];
z[6] = abb[49] + abb[52];
z[7] = z[4] + -z[6];
z[8] = -abb[38] + abb[40];
z[9] = m1_set::bc<T>[0] * z[8];
z[10] = 2 * z[9];
z[11] = 2 * abb[51];
z[12] = z[7] + z[10] + z[11];
z[12] = abb[65] * z[12];
z[13] = z[0] + -z[8];
z[13] = m1_set::bc<T>[0] * z[13];
z[14] = -abb[49] + abb[51];
z[13] = -abb[50] + z[13] + -z[14];
z[13] = abb[62] * z[13];
z[15] = z[0] + z[8];
z[15] = m1_set::bc<T>[0] * z[15];
z[16] = -abb[51] + abb[52];
z[17] = z[15] + -z[16];
z[17] = abb[64] * z[17];
z[18] = abb[51] * z[2];
z[19] = abb[49] * z[2];
z[18] = z[18] + -z[19];
z[20] = abb[52] * z[2];
z[21] = z[18] + -z[20];
z[22] = abb[50] * abb[61];
z[23] = -abb[60] + abb[64];
z[24] = abb[61] + z[23];
z[25] = abb[48] * z[24];
z[22] = z[22] + z[25];
z[25] = abb[61] + z[2];
z[25] = abb[53] * z[25];
z[26] = abb[60] * z[16];
z[27] = abb[61] * z[6];
z[1] = -z[1] + z[12] + z[13] + -z[17] + 2 * z[21] + -z[22] + -z[25] + -z[26] + -z[27];
z[12] = abb[26] + abb[28];
z[13] = -2 * z[12];
z[1] = z[1] * z[13];
z[13] = abb[37] + abb[39];
z[17] = z[2] * z[13];
z[21] = abb[36] * z[2];
z[25] = z[17] + -z[21];
z[26] = abb[38] * z[2];
z[28] = -z[25] + z[26];
z[29] = abb[34] * z[2];
z[30] = z[28] + z[29];
z[31] = abb[61] + z[3];
z[32] = abb[35] * z[31];
z[33] = abb[38] + -abb[39];
z[34] = abb[41] + 2 * z[33];
z[34] = abb[61] * z[34];
z[35] = abb[36] + -abb[37];
z[36] = abb[34] + z[35];
z[37] = -abb[41] + z[36];
z[38] = abb[60] * z[37];
z[30] = 2 * z[30] + -z[32] + z[34] + z[38];
z[30] = m1_set::bc<T>[0] * z[30];
z[34] = 2 * z[36];
z[38] = -abb[35] + z[34];
z[39] = -abb[41] + z[38];
z[39] = m1_set::bc<T>[0] * z[39];
z[4] = z[4] + z[6] + z[39];
z[39] = abb[65] * z[4];
z[19] = z[19] + z[20];
z[20] = abb[52] * abb[60];
z[37] = m1_set::bc<T>[0] * z[37];
z[40] = -abb[52] + -z[37];
z[40] = abb[64] * z[40];
z[41] = -abb[35] + z[36];
z[42] = m1_set::bc<T>[0] * z[41];
z[43] = abb[49] + abb[50] + z[42];
z[44] = -abb[62] * z[43];
z[19] = 2 * z[19] + z[20] + -z[22] + z[27] + z[30] + z[39] + z[40] + z[44];
z[19] = abb[31] * z[19];
z[20] = -abb[24] * z[4];
z[22] = -abb[17] * z[7];
z[27] = abb[35] + -abb[41];
z[27] = m1_set::bc<T>[0] * z[27];
z[30] = -abb[50] + abb[52] + z[27];
z[39] = abb[20] * z[30];
z[20] = z[20] + z[22] + z[39];
z[20] = abb[73] * z[20];
z[22] = 2 * abb[34];
z[39] = -z[22] + z[33];
z[40] = abb[35] + abb[41] + z[39];
z[40] = m1_set::bc<T>[0] * z[40];
z[40] = -abb[48] + -abb[49] + z[40];
z[44] = abb[67] + abb[69];
z[40] = -z[40] * z[44];
z[45] = z[22] + z[33];
z[46] = 2 * abb[35];
z[47] = -z[45] + z[46];
z[47] = m1_set::bc<T>[0] * z[47];
z[47] = -2 * abb[49] + z[47];
z[47] = abb[70] * z[47];
z[48] = -abb[34] + abb[35];
z[49] = -m1_set::bc<T>[0] * z[48];
z[49] = abb[49] + z[49];
z[49] = abb[72] * z[49];
z[40] = z[40] + z[47] + 4 * z[49];
z[40] = abb[0] * z[40];
z[47] = 2 * abb[72];
z[49] = -abb[67] + -z[47];
z[50] = -abb[39] + z[35];
z[51] = abb[40] + z[50];
z[51] = m1_set::bc<T>[0] * z[51];
z[51] = abb[51] + abb[52] + z[51];
z[49] = z[49] * z[51];
z[51] = 2 * abb[37];
z[52] = abb[36] + -abb[39];
z[53] = z[51] + -z[52];
z[54] = 2 * abb[40];
z[55] = z[53] + -z[54];
z[55] = m1_set::bc<T>[0] * z[55];
z[55] = -abb[52] + -z[11] + z[55];
z[55] = abb[69] * z[55];
z[49] = z[49] + z[55];
z[49] = abb[4] * z[49];
z[19] = z[19] + z[20] + z[40] + z[49];
z[20] = -z[22] + z[51];
z[40] = abb[38] + abb[39];
z[49] = z[40] + -z[54];
z[55] = -z[20] + -z[49];
z[55] = m1_set::bc<T>[0] * z[55];
z[56] = 2 * abb[48];
z[55] = z[11] + z[55] + z[56];
z[55] = abb[3] * z[55];
z[57] = abb[53] + 2 * z[6];
z[58] = 4 * abb[16];
z[59] = z[57] * z[58];
z[60] = abb[53] + z[6];
z[37] = z[37] + -z[60];
z[37] = abb[11] * z[37];
z[61] = m1_set::bc<T>[0] * z[49];
z[61] = -z[11] + z[61];
z[62] = abb[14] * z[61];
z[63] = 2 * abb[52];
z[64] = 2 * abb[41];
z[65] = abb[35] + abb[36] + -z[64];
z[65] = m1_set::bc<T>[0] * z[65];
z[65] = -abb[50] + z[63] + z[65];
z[66] = 2 * abb[2];
z[65] = z[65] * z[66];
z[67] = 2 * abb[15];
z[43] = z[43] * z[67];
z[68] = abb[57] + abb[58];
z[43] = -abb[55] + -4 * z[37] + z[43] + z[55] + -z[59] + z[62] + z[65] + z[68];
z[43] = abb[67] * z[43];
z[55] = -abb[34] + z[8];
z[62] = abb[35] + z[55];
z[62] = m1_set::bc<T>[0] * z[62];
z[14] = z[14] + z[62];
z[62] = abb[14] * z[14];
z[65] = z[33] + z[41];
z[65] = m1_set::bc<T>[0] * z[65];
z[65] = z[6] + z[65];
z[65] = abb[15] * z[65];
z[62] = z[62] + z[65];
z[9] = abb[51] + z[9];
z[65] = abb[1] * z[9];
z[65] = 8 * z[65] + -z[68];
z[68] = -abb[39] + abb[40] + z[41];
z[69] = 2 * m1_set::bc<T>[0];
z[68] = z[68] * z[69];
z[68] = -abb[53] + z[11] + z[68];
z[70] = abb[3] * z[68];
z[48] = z[48] * z[69];
z[63] = abb[53] + z[63];
z[48] = z[48] + z[63];
z[48] = z[48] * z[66];
z[66] = abb[51] + z[6];
z[10] = abb[53] + z[10] + 2 * z[66];
z[66] = 2 * abb[7];
z[69] = z[10] * z[66];
z[48] = 3 * abb[55] + z[48] + -z[59] + 4 * z[62] + -z[65] + z[69] + 2 * z[70];
z[48] = abb[72] * z[48];
z[59] = z[26] + z[29];
z[62] = abb[61] * z[45];
z[69] = -z[5] * z[54];
z[31] = -z[31] * z[46];
z[70] = abb[60] * z[40];
z[31] = z[31] + 4 * z[59] + z[62] + z[69] + z[70];
z[31] = m1_set::bc<T>[0] * z[31];
z[59] = abb[49] * abb[61];
z[62] = -abb[51] * abb[60];
z[18] = -2 * z[18] + z[59] + z[62];
z[59] = -z[22] + z[46];
z[62] = 4 * abb[40];
z[69] = 3 * abb[38];
z[70] = abb[39] + -z[59] + -z[62] + z[69];
z[70] = m1_set::bc<T>[0] * z[70];
z[71] = -abb[49] + z[11];
z[70] = z[70] + -2 * z[71];
z[71] = abb[65] * z[70];
z[72] = -abb[64] * z[61];
z[73] = 2 * abb[62];
z[14] = z[14] * z[73];
z[14] = z[14] + 2 * z[18] + z[31] + z[71] + z[72];
z[14] = abb[30] * z[14];
z[4] = abb[15] * z[4];
z[18] = abb[16] * z[57];
z[18] = z[18] + z[37];
z[7] = abb[7] * z[7];
z[27] = abb[48] + -abb[49] + z[27];
z[27] = abb[0] * z[27];
z[30] = abb[2] * z[30];
z[4] = -z[4] + z[7] + 2 * z[18] + z[27] + -z[30];
z[7] = abb[68] + abb[71];
z[18] = -2 * z[7];
z[4] = z[4] * z[18];
z[18] = -z[40] + z[62];
z[27] = 2 * abb[36];
z[30] = -4 * abb[37] + z[18] + z[27] + -z[59];
z[30] = m1_set::bc<T>[0] * z[30];
z[31] = 2 * abb[50];
z[37] = z[31] + z[56];
z[30] = 4 * abb[51] + z[30] + z[37];
z[30] = abb[3] * z[30];
z[57] = -abb[14] * z[70];
z[59] = -abb[36] + abb[38];
z[59] = m1_set::bc<T>[0] * z[59];
z[59] = -abb[50] + z[59];
z[59] = abb[5] * z[59];
z[62] = -abb[18] + abb[19] + -abb[22];
z[70] = abb[23] + z[62];
z[71] = abb[54] * z[70];
z[30] = abb[55] + z[30] + z[57] + 2 * z[59] + -z[65] + z[71];
z[30] = abb[69] * z[30];
z[57] = abb[49] + abb[51];
z[59] = abb[34] + abb[39];
z[65] = abb[35] + abb[40] + -z[59];
z[65] = m1_set::bc<T>[0] * z[65];
z[63] = z[57] + z[63] + z[65];
z[63] = abb[67] * z[63];
z[42] = z[42] + -z[60];
z[42] = -z[7] * z[42];
z[60] = abb[34] + -abb[39];
z[65] = -abb[35] + abb[40] + z[60];
z[65] = m1_set::bc<T>[0] * z[65];
z[65] = z[57] + z[65];
z[65] = abb[70] * z[65];
z[68] = -abb[72] * z[68];
z[42] = z[42] + z[63] + z[65] + z[68];
z[42] = abb[10] * z[42];
z[63] = -z[27] + z[64];
z[49] = z[49] + -z[63];
z[49] = m1_set::bc<T>[0] * z[49];
z[16] = 2 * z[16] + z[49];
z[16] = abb[67] * z[16];
z[49] = -z[22] + z[63];
z[18] = -z[18] + -z[49];
z[18] = m1_set::bc<T>[0] * z[18];
z[6] = z[6] + -z[11];
z[6] = 2 * z[6] + z[18];
z[6] = abb[69] * z[6];
z[11] = 2 * abb[70];
z[18] = z[11] * z[61];
z[10] = z[10] * z[47];
z[6] = z[6] + z[10] + z[16] + z[18];
z[6] = abb[6] * z[6];
z[10] = z[39] + z[64];
z[10] = m1_set::bc<T>[0] * z[10];
z[16] = abb[60] + -abb[61];
z[18] = -abb[64] + z[16];
z[39] = z[10] * z[18];
z[61] = z[24] * z[56];
z[10] = z[10] + -z[56];
z[63] = abb[65] * z[10];
z[39] = z[39] + z[61] + z[63];
z[39] = abb[25] * z[39];
z[61] = -z[22] + z[64];
z[63] = abb[27] + abb[29];
z[61] = z[61] * z[63];
z[65] = abb[38] * z[63];
z[68] = abb[39] * z[63];
z[61] = z[61] + z[65] + -z[68];
z[61] = -z[23] * z[61];
z[49] = z[49] + z[69];
z[49] = z[49] * z[63];
z[49] = z[49] + -z[68];
z[71] = -abb[61] * z[49];
z[61] = z[61] + z[71];
z[61] = m1_set::bc<T>[0] * z[61];
z[62] = -abb[23] + z[62];
z[62] = abb[67] * z[62];
z[71] = abb[18] * z[11];
z[24] = -abb[65] + z[24];
z[72] = abb[33] * z[24];
z[62] = z[62] + -z[71] + z[72];
z[71] = abb[54] * z[62];
z[37] = -z[37] * z[63];
z[49] = m1_set::bc<T>[0] * z[49];
z[37] = z[37] + z[49];
z[37] = abb[65] * z[37];
z[49] = abb[61] * z[63];
z[31] = z[31] * z[49];
z[72] = z[23] * z[63];
z[72] = z[49] + z[72];
z[56] = z[56] * z[72];
z[72] = abb[36] * z[63];
z[74] = -z[65] + z[72];
z[74] = m1_set::bc<T>[0] * z[74];
z[75] = abb[50] * z[63];
z[74] = z[74] + z[75];
z[74] = z[73] * z[74];
z[75] = -abb[65] + z[23];
z[76] = -z[2] + z[75];
z[77] = abb[85] * z[76];
z[15] = -abb[50] + z[15] + z[57];
z[15] = abb[67] * z[15];
z[9] = abb[69] * z[9];
z[9] = 2 * z[9] + z[15];
z[9] = z[9] * z[66];
z[15] = abb[12] * z[44];
z[10] = z[10] * z[15];
z[57] = -abb[72] + z[44];
z[66] = -abb[56] * z[57];
z[1] = z[1] + z[4] + z[6] + z[9] + z[10] + z[14] + 2 * z[19] + z[30] + z[31] + z[37] + z[39] + 4 * z[42] + z[43] + z[48] + z[56] + z[61] + z[66] + z[71] + z[74] + z[77];
z[4] = prod_pow(m1_set::bc<T>[0], 2);
z[6] = (T(4) / T(3)) * z[4];
z[9] = 2 * abb[46];
z[10] = z[6] + -z[9];
z[14] = abb[38] + z[13];
z[19] = z[14] + -z[27];
z[19] = abb[38] * z[19];
z[30] = -z[54] + z[69];
z[31] = z[30] + -z[50];
z[31] = abb[40] * z[31];
z[37] = 2 * abb[78];
z[39] = 2 * abb[75];
z[42] = z[37] + z[39];
z[43] = z[33] + z[35];
z[48] = abb[41] * z[43];
z[56] = abb[36] * z[13];
z[61] = -abb[34] + z[27];
z[61] = abb[34] * z[61];
z[66] = -z[10] + z[19] + -z[31] + z[42] + z[48] + z[56] + -z[61];
z[66] = abb[65] * z[66];
z[55] = z[35] + z[55];
z[69] = z[54] * z[55];
z[71] = abb[34] * z[35];
z[74] = abb[36] * z[35];
z[77] = z[71] + -z[74];
z[78] = abb[38] + 2 * z[0];
z[78] = abb[38] * z[78];
z[79] = prod_pow(abb[37], 2);
z[80] = 2 * abb[43] + z[79];
z[81] = (T(1) / T(3)) * z[4];
z[69] = z[9] + z[69] + -z[77] + z[78] + z[80] + -z[81];
z[69] = abb[62] * z[69];
z[78] = z[35] + z[40];
z[82] = -z[22] + z[78];
z[83] = abb[60] * z[82];
z[30] = z[2] * z[30];
z[30] = z[25] + z[30] + z[83];
z[30] = abb[40] * z[30];
z[83] = -z[13] + z[22];
z[84] = abb[38] * z[83];
z[84] = -z[48] + z[80] + z[84];
z[53] = abb[36] * z[53];
z[85] = abb[34] + -abb[37];
z[86] = -abb[36] + z[85];
z[86] = abb[34] * z[86];
z[86] = z[37] + z[86];
z[53] = z[53] + -z[84] + z[86];
z[82] = abb[40] * z[82];
z[87] = z[4] + z[82];
z[88] = -z[53] + z[87];
z[88] = abb[64] * z[88];
z[53] = abb[60] * z[53];
z[89] = z[2] * z[27];
z[17] = z[17] + z[26] + -z[89];
z[17] = abb[38] * z[17];
z[89] = prod_pow(abb[36], 2);
z[61] = z[61] + -z[89];
z[90] = 2 * abb[79];
z[91] = z[37] + z[61] + z[90];
z[91] = abb[61] * z[91];
z[92] = -abb[62] + z[3];
z[93] = 2 * abb[65] + -z[23] + z[92];
z[94] = 2 * abb[77];
z[93] = z[93] * z[94];
z[92] = abb[61] + z[92];
z[92] = z[39] * z[92];
z[93] = -z[92] + z[93];
z[95] = abb[36] * z[25];
z[96] = abb[41] * z[28];
z[95] = z[95] + z[96];
z[96] = 2 * abb[61] + z[2];
z[96] = -abb[60] + (T(4) / T(3)) * z[96];
z[96] = z[4] * z[96];
z[97] = 4 * abb[78];
z[98] = z[9] + z[97];
z[98] = z[2] * z[98];
z[3] = abb[79] * z[3];
z[3] = z[3] + z[17] + -z[30] + z[53] + z[66] + -z[69] + z[88] + z[91] + -z[93] + z[95] + z[96] + z[98];
z[3] = -z[3] * z[12];
z[17] = -abb[60] * z[60];
z[30] = -abb[40] * z[2];
z[17] = z[17] + z[25] + z[26] + z[30];
z[17] = z[17] * z[54];
z[26] = abb[40] * z[55];
z[30] = -abb[38] * z[35];
z[41] = abb[38] + z[41];
z[53] = abb[35] * z[41];
z[26] = abb[42] + z[26] + z[30] + z[53] + -z[71];
z[26] = z[26] * z[73];
z[30] = z[51] + -z[60];
z[53] = -z[27] + z[30];
z[55] = abb[34] + abb[38];
z[53] = z[53] * z[55];
z[8] = z[8] + z[50];
z[66] = z[8] * z[54];
z[69] = (T(2) / T(3)) * z[4];
z[53] = z[39] + z[53] + z[66] + -z[69];
z[41] = z[41] * z[46];
z[73] = -z[41] + -z[53];
z[73] = abb[65] * z[73];
z[25] = -z[25] + z[29];
z[88] = abb[34] * z[25];
z[25] = abb[38] * z[25];
z[25] = z[25] + z[88];
z[91] = -abb[35] + z[22];
z[96] = -z[2] * z[91];
z[98] = -abb[61] * z[60];
z[96] = -z[28] + z[96] + z[98];
z[96] = z[46] * z[96];
z[98] = z[54] * z[60];
z[99] = z[55] * z[60];
z[100] = 2 * abb[42];
z[101] = z[99] + z[100];
z[98] = z[98] + -z[101];
z[102] = -z[69] + z[98];
z[102] = abb[64] * z[102];
z[99] = abb[61] * z[99];
z[101] = abb[60] * z[101];
z[16] = z[16] * z[69];
z[103] = -abb[62] + abb[65];
z[104] = -abb[61] + z[103];
z[105] = 2 * abb[44];
z[106] = z[104] * z[105];
z[16] = z[16] + z[17] + 2 * z[25] + z[26] + z[73] + z[93] + z[96] + z[99] + z[101] + z[102] + z[106];
z[16] = abb[30] * z[16];
z[17] = abb[40] * z[43];
z[25] = z[17] + z[80];
z[26] = 3 * abb[37];
z[52] = -z[26] + z[52];
z[52] = abb[36] * z[52];
z[0] = -z[0] + z[13];
z[0] = z[0] * z[22];
z[26] = 3 * abb[36] + -z[26];
z[73] = -4 * abb[34] + abb[41] + -z[26] + z[40];
z[73] = abb[41] * z[73];
z[93] = abb[38] * z[13];
z[38] = abb[35] * z[38];
z[96] = 2 * abb[45];
z[0] = z[0] + z[25] + z[38] + z[52] + z[73] + -z[81] + -z[93] + -z[96] + -z[97] + -z[100];
z[0] = abb[2] * z[0];
z[52] = 2 * abb[39];
z[73] = z[52] + -z[85];
z[73] = abb[38] * z[73];
z[85] = z[69] + z[94];
z[78] = -abb[41] + z[78];
z[78] = abb[41] * z[78];
z[78] = z[78] + -z[96];
z[30] = -abb[34] * z[30];
z[30] = z[30] + z[56] + z[73] + -z[78] + -z[80] + -z[82] + -z[85];
z[30] = abb[3] * z[30];
z[56] = abb[36] * z[22];
z[82] = -abb[36] + z[51];
z[82] = abb[36] * z[82];
z[99] = (T(7) / T(3)) * z[4];
z[34] = -abb[41] + z[34];
z[34] = abb[41] * z[34];
z[56] = -z[34] + z[42] + z[56] + -z[82] + z[90] + z[99];
z[56] = abb[11] * z[56];
z[101] = z[80] + z[100];
z[102] = abb[36] * abb[37];
z[106] = -abb[34] + z[35];
z[107] = -abb[34] * z[106];
z[106] = abb[40] + 2 * z[106];
z[106] = abb[40] * z[106];
z[106] = z[101] + -z[102] + z[106] + z[107];
z[106] = abb[13] * z[106];
z[98] = z[85] + -z[98];
z[98] = abb[14] * z[98];
z[107] = abb[34] * z[36];
z[74] = z[74] + z[107];
z[107] = z[74] + -z[81];
z[108] = z[38] + z[39];
z[109] = z[107] + -z[108];
z[109] = abb[15] * z[109];
z[110] = abb[79] + z[6] + z[42];
z[58] = z[58] * z[110];
z[111] = abb[83] + abb[84];
z[58] = z[58] + -z[111];
z[0] = z[0] + z[30] + -2 * z[56] + z[58] + z[98] + z[106] + z[109];
z[0] = abb[67] * z[0];
z[30] = -z[2] * z[37];
z[21] = z[21] + z[29];
z[21] = abb[38] * z[21];
z[21] = z[21] + z[30] + z[88] + -z[95];
z[29] = z[34] + z[37];
z[30] = -z[29] + z[74];
z[30] = abb[60] * z[30];
z[2] = -z[2] * z[22];
z[88] = abb[34] + z[33];
z[95] = -abb[61] * z[88];
z[2] = z[2] + -z[28] + z[95];
z[2] = 2 * z[2] + z[32];
z[2] = abb[35] * z[2];
z[28] = z[29] + -z[107];
z[28] = abb[64] * z[28];
z[29] = -abb[78] + z[74];
z[29] = 2 * z[29] + -z[34] + -z[69];
z[32] = z[29] + -z[108];
z[74] = abb[65] * z[32];
z[95] = abb[34] + abb[36];
z[98] = abb[38] * z[95];
z[95] = -abb[39] * z[95];
z[95] = -abb[78] + z[95] + z[98];
z[106] = -abb[34] + z[33];
z[106] = abb[41] + 2 * z[106];
z[106] = abb[41] * z[106];
z[95] = 2 * z[95] + -z[106];
z[95] = abb[61] * z[95];
z[5] = -z[5] * z[81];
z[107] = z[38] + -z[107];
z[107] = abb[62] * z[107];
z[2] = z[2] + z[5] + 2 * z[21] + z[28] + z[30] + z[74] + -z[92] + z[95] + z[107];
z[2] = abb[31] * z[2];
z[5] = 2 * abb[38];
z[21] = -abb[35] + z[5];
z[21] = abb[35] * z[21];
z[28] = -z[9] + z[21];
z[30] = z[28] + -z[105];
z[74] = -z[35] + z[52];
z[92] = abb[34] * z[74];
z[95] = -abb[37] + -z[45];
z[95] = abb[38] * z[95];
z[107] = abb[39] + -z[51];
z[107] = abb[36] * z[107];
z[79] = 4 * abb[43] + 3 * z[17] + z[30] + -z[78] + 2 * z[79] + z[92] + z[95] + z[107];
z[79] = abb[2] * z[79];
z[92] = 4 * abb[77];
z[31] = -z[9] + z[31] + z[92];
z[95] = abb[34] + abb[37];
z[107] = abb[38] + z[52] + z[95];
z[107] = abb[38] * z[107];
z[107] = -z[31] + z[107];
z[41] = z[41] + -z[105];
z[20] = -abb[39] + -z[20];
z[20] = abb[34] * z[20];
z[109] = abb[39] + z[35];
z[109] = abb[36] * z[109];
z[20] = -z[4] + z[20] + -z[41] + -z[78] + z[107] + z[109];
z[20] = abb[3] * z[20];
z[109] = abb[36] * z[50];
z[48] = -z[48] + z[109];
z[19] = z[19] + -z[31] + -z[48];
z[112] = -z[6] + z[19];
z[113] = abb[1] * z[112];
z[113] = 2 * z[113];
z[114] = abb[38] * z[27];
z[114] = -z[21] + z[81] + -z[89] + z[114];
z[114] = abb[5] * z[114];
z[115] = abb[38] + z[22];
z[115] = abb[38] * z[115];
z[116] = -abb[34] + z[52];
z[116] = abb[34] * z[116];
z[115] = z[115] + -z[116];
z[88] = 2 * z[88];
z[116] = -abb[40] + z[88];
z[116] = abb[40] * z[116];
z[116] = -z[100] + z[116];
z[28] = -z[28] + z[115] + -z[116];
z[117] = 2 * abb[9];
z[118] = z[28] * z[117];
z[41] = z[41] + z[53] + -z[92];
z[41] = abb[14] * z[41];
z[53] = abb[5] + z[117];
z[53] = z[53] * z[105];
z[20] = z[20] + z[41] + z[53] + z[79] + -z[111] + -z[113] + z[114] + z[118];
z[20] = abb[69] * z[20];
z[36] = -abb[39] + z[36];
z[41] = z[22] * z[36];
z[43] = z[43] + z[91];
z[46] = z[43] * z[46];
z[41] = z[41] + -z[46];
z[14] = z[14] + z[22];
z[14] = abb[38] * z[14];
z[14] = z[14] + -z[31] + z[41] + z[48] + z[69] + z[90];
z[31] = abb[3] * z[14];
z[43] = abb[35] * z[43];
z[46] = abb[34] * z[36];
z[46] = -z[42] + -z[43] + z[46] + z[48] + -z[81] + z[98];
z[46] = z[46] * z[67];
z[36] = -z[36] * z[55];
z[8] = abb[40] * z[8];
z[8] = z[8] + -z[94];
z[36] = z[8] + z[36] + z[39] + z[43];
z[36] = abb[14] * z[36];
z[43] = abb[38] + z[83];
z[43] = abb[38] * z[43];
z[17] = -z[17] + z[41] + z[43];
z[41] = z[90] + z[97];
z[43] = z[41] + -z[48];
z[53] = -z[6] + -z[9] + -z[17] + -z[43];
z[53] = abb[2] * z[53];
z[55] = 4 * abb[75];
z[19] = 4 * z[4] + -z[19] + z[41] + z[55];
z[41] = -abb[7] * z[19];
z[31] = z[31] + 2 * z[36] + z[41] + z[46] + z[53] + z[58] + -z[113];
z[31] = abb[72] * z[31];
z[36] = -abb[40] + z[5];
z[36] = abb[40] * z[36];
z[41] = z[36] + z[94];
z[46] = z[37] + z[81];
z[53] = -abb[36] + z[52];
z[53] = abb[36] * z[53];
z[58] = -abb[38] * z[51];
z[67] = abb[41] + -z[52];
z[67] = abb[41] * z[67];
z[53] = z[41] + z[46] + z[53] + z[58] + z[67] + z[80] + z[96];
z[53] = abb[67] * z[53];
z[58] = z[81] + -z[96];
z[13] = -z[5] * z[13];
z[67] = abb[38] + z[35];
z[67] = -abb[41] + 2 * z[67];
z[67] = abb[41] * z[67];
z[13] = z[13] + z[37] + z[58] + -z[66] + z[67] + z[82] + z[92];
z[13] = abb[69] * z[13];
z[66] = z[8] + z[93];
z[46] = z[46] + -z[48] + -z[66];
z[46] = z[46] * z[47];
z[13] = z[13] + z[46] + z[53];
z[13] = abb[4] * z[13];
z[14] = -abb[72] * z[14];
z[46] = -abb[35] + z[88];
z[46] = abb[35] * z[46];
z[46] = z[39] + z[46];
z[41] = -z[10] + -z[41] + -z[46] + z[115];
z[41] = abb[70] * z[41];
z[53] = abb[34] + 2 * z[35];
z[53] = abb[34] * z[53];
z[53] = -z[38] + z[39] + z[53];
z[43] = (T(-8) / T(3)) * z[4] + -z[43] + -z[53] + z[66];
z[43] = abb[67] * z[43];
z[66] = abb[78] + abb[79];
z[53] = z[53] + 2 * z[66] + z[99];
z[53] = -z[7] * z[53];
z[14] = z[14] + z[41] + z[43] + z[53];
z[14] = abb[10] * z[14];
z[41] = abb[7] + -abb[15];
z[43] = abb[0] + z[41];
z[7] = z[7] * z[43];
z[43] = -abb[0] + -abb[3] + abb[12];
z[43] = z[43] * z[44];
z[53] = -z[63] * z[75];
z[12] = -abb[31] + z[12];
z[66] = -abb[25] + -z[12];
z[24] = z[24] * z[66];
z[66] = abb[17] + abb[24];
z[67] = abb[73] * z[66];
z[7] = z[7] + z[24] + z[43] + -z[49] + z[53] + z[67];
z[7] = abb[74] * z[7];
z[24] = abb[36] + abb[38];
z[43] = abb[37] + -abb[39];
z[24] = z[24] * z[43];
z[24] = z[24] + -z[25] + z[78];
z[24] = abb[8] * z[24] * z[44];
z[7] = z[7] + z[14] + z[24];
z[14] = abb[29] * abb[36];
z[24] = abb[29] * abb[37];
z[14] = z[14] + -z[24];
z[25] = abb[29] * abb[34];
z[25] = -z[14] + z[25];
z[43] = abb[29] * abb[40];
z[43] = 2 * z[25] + -z[43];
z[43] = abb[40] * z[43];
z[53] = abb[29] * z[101];
z[67] = z[43] + -z[53];
z[75] = z[21] * z[63];
z[72] = -z[24] + z[72];
z[72] = abb[36] * z[72];
z[25] = abb[34] * z[25];
z[78] = -z[27] * z[65];
z[79] = -z[63] * z[81];
z[25] = z[25] + -z[67] + z[72] + z[75] + z[78] + z[79];
z[25] = abb[62] * z[25];
z[35] = -abb[40] + -z[35] + z[45];
z[35] = z[35] * z[54];
z[26] = z[26] + z[52];
z[26] = abb[34] * z[26];
z[5] = z[5] * z[95];
z[45] = abb[36] * z[74];
z[5] = 4 * abb[42] + z[5] + z[6] + -z[26] + -z[35] + -z[45] + -z[106] + z[108];
z[5] = -z[5] * z[44];
z[6] = 3 * abb[39] + -z[22];
z[6] = abb[34] * z[6];
z[26] = -3 * abb[34] + -z[33];
z[26] = abb[38] * z[26];
z[6] = z[6] + -z[9] + z[26] + z[46] + z[69] + z[116];
z[6] = z[6] * z[11];
z[9] = -z[10] + z[17] + -z[48] + -z[55];
z[9] = z[9] * z[47];
z[10] = abb[47] * abb[73];
z[5] = z[5] + z[6] + z[9] + z[10];
z[5] = abb[0] * z[5];
z[6] = abb[16] * z[110];
z[6] = 2 * z[6] + -z[56];
z[9] = z[34] + -z[38] + z[86] + z[102];
z[10] = abb[2] * z[9];
z[17] = z[34] + z[77] + -z[108];
z[17] = abb[0] * z[17];
z[26] = -z[42] + z[61];
z[26] = abb[7] * z[26];
z[32] = abb[15] * z[32];
z[33] = abb[20] * abb[47];
z[6] = 2 * z[6] + -z[10] + z[17] + z[26] + z[32] + -z[33];
z[10] = abb[68] * z[6];
z[17] = abb[2] + z[41];
z[26] = 2 * abb[76];
z[32] = z[17] * z[26];
z[6] = z[6] + z[32];
z[6] = abb[71] * z[6];
z[22] = -abb[38] + z[22] + z[50];
z[22] = abb[41] * z[22];
z[22] = z[22] + -z[58] + -z[109];
z[32] = z[27] + -z[60];
z[32] = abb[34] * z[32];
z[8] = -z[8] + -z[22] + z[32] + -z[37] + -z[73] + -z[100];
z[8] = abb[67] * z[8];
z[32] = abb[39] + z[27];
z[32] = abb[34] * z[32];
z[22] = z[21] + -z[22] + z[32] + -z[42] + -z[105] + -z[107];
z[22] = abb[69] * z[22];
z[32] = abb[34] * abb[39];
z[33] = -abb[34] + -z[40];
z[33] = abb[38] * z[33];
z[30] = z[30] + z[32] + z[33] + z[36] + z[85];
z[30] = z[11] * z[30];
z[19] = -abb[72] * z[19];
z[8] = z[8] + z[19] + z[22] + z[30];
z[8] = abb[6] * z[8];
z[19] = abb[39] + -z[27] + z[51];
z[19] = abb[36] * z[19];
z[19] = z[19] + -z[39] + z[71] + -z[84] + -z[87] + -z[94];
z[19] = abb[67] * z[19];
z[22] = abb[69] * z[112];
z[19] = z[19] + z[22];
z[19] = abb[7] * z[19];
z[9] = -abb[20] * z[9];
z[22] = -z[29] + z[38];
z[22] = abb[24] * z[22];
z[29] = -z[37] + z[61];
z[29] = abb[17] * z[29];
z[30] = -abb[17] + abb[24];
z[32] = z[30] * z[39];
z[9] = z[9] + z[22] + z[29] + z[32];
z[9] = abb[73] * z[9];
z[22] = z[63] * z[103];
z[22] = z[22] + -z[49];
z[29] = abb[67] + abb[68];
z[17] = z[17] * z[29];
z[12] = z[12] * z[104];
z[29] = abb[20] + z[66];
z[29] = abb[73] * z[29];
z[32] = -abb[3] + abb[5];
z[32] = abb[69] * z[32];
z[12] = z[12] + z[17] + z[22] + z[29] + z[32];
z[12] = z[12] * z[26];
z[17] = abb[27] * abb[34];
z[14] = z[14] + z[17] + z[68];
z[14] = abb[34] * z[14];
z[17] = abb[34] * z[63];
z[26] = -z[17] + z[65];
z[26] = z[26] * z[64];
z[29] = z[63] * z[96];
z[26] = z[26] + -z[29];
z[17] = z[17] + z[68];
z[29] = abb[38] * z[17];
z[24] = abb[36] * z[24];
z[14] = z[14] + z[24] + z[26] + -z[29];
z[24] = -z[14] + -z[43] + z[53];
z[24] = abb[60] * z[24];
z[27] = z[27] * z[63];
z[27] = z[17] + z[27];
z[27] = abb[38] * z[27];
z[17] = abb[34] * z[17];
z[17] = -z[17] + -z[26] + z[27];
z[26] = z[63] * z[89];
z[26] = -z[17] + z[26];
z[26] = abb[61] * z[26];
z[27] = -z[63] * z[69];
z[14] = z[14] + z[27] + z[67];
z[14] = abb[64] * z[14];
z[27] = z[4] + -z[89];
z[27] = z[27] * z[63];
z[17] = z[17] + z[27] + -z[75];
z[17] = abb[65] * z[17];
z[27] = z[28] + z[105];
z[11] = abb[9] * z[11] * z[27];
z[27] = -abb[21] + z[30];
z[27] = abb[67] * z[27];
z[23] = -abb[62] + z[23];
z[23] = abb[32] * z[23];
z[28] = -abb[2] * abb[73];
z[29] = abb[20] * abb[69];
z[23] = z[23] + z[27] + z[28] + z[29];
z[23] = abb[47] * z[23];
z[27] = -z[59] + z[64];
z[28] = -abb[34] + abb[38];
z[27] = z[27] * z[28];
z[27] = z[27] + -z[69] + -z[96];
z[18] = -abb[65] + -z[18];
z[18] = abb[25] * z[18];
z[15] = -z[15] + z[18];
z[15] = z[15] * z[27];
z[18] = -abb[69] * z[70];
z[18] = z[18] + -z[62];
z[18] = abb[80] * z[18];
z[21] = z[21] * z[49];
z[27] = abb[60] * z[63];
z[27] = (T(2) / T(3)) * z[27] + -z[49];
z[4] = z[4] * z[27];
z[22] = z[22] * z[105];
z[27] = abb[59] * z[76];
z[28] = abb[67] + -abb[69] + -3 * abb[72];
z[28] = abb[81] * z[28];
z[29] = abb[82] * z[57];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + 2 * z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[25] + z[26] + z[27] + z[28] + z[29] + z[31];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_875_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-5.1156137591021159324521833062370106254853630667671669020404408748"),stof<T>("8.1590970221738957400031467760741852677013306533114953925001605429")}, std::complex<T>{stof<T>("-16.47842230034473081754505708294186236852641337158586282664896645"),stof<T>("5.378501027856830991306270504351673603260283837929384966945923568")}, std::complex<T>{stof<T>("-2.6005757047828083340679850457978305341425692206193198298298143492"),stof<T>("3.1923472445362258158409460442271726429200038553757622486786770783")}, std::complex<T>{stof<T>("-37.98692070932807683185851068676208491973841443546741979771918595"),stof<T>("20.690501610989001830936942472397372456083221271730236221534814065")}, std::complex<T>{stof<T>("5.1156137591021159324521833062370106254853630667671669020404408748"),stof<T>("-8.1590970221738957400031467760741852677013306533114953925001605429")}, std::complex<T>{stof<T>("-2.5150380543193075983841982604391800913427938461478470722106265256"),stof<T>("4.9667497776376699241622007318470126247813267979357331438214834646")}, std::complex<T>{stof<T>("18.993460354664038415929255343381042459869207217733709898859592975"),stof<T>("-10.345250805494500915468471236198686228041610635865118110767407032")}, std::complex<T>{stof<T>("6.5937903615661172599246510308839945783831792589348865259051431407"),stof<T>("-0.6851071199826447026934952274028708191805336917612923775219861944")}, std::complex<T>{stof<T>("-7.2482624397851669117432020977460080394528971966674795341054174613"),stof<T>("-10.574837918957414784871519894863917500940669196137683998015786893")}, std::complex<T>{stof<T>("-1.5989605077297489580016035629255256720417930231806017886082957364"),stof<T>("-0.8803149981567529453646111686434195120233946325202332674172182553")}, std::complex<T>{stof<T>("-13.633032417376269935413032560656827828225362359303341922595635174"),stof<T>("-6.797664124413273680744588049712544846318567323356181378238250271")}, std::complex<T>{stof<T>("-7.2482624397851669117432020977460080394528971966674795341054174613"),stof<T>("-10.574837918957414784871519894863917500940669196137683998015786893")}, std::complex<T>{stof<T>("20.523250107294926341396894056949444843312209006547638570380774428"),stof<T>("6.560706813548703261724628750836650834595117488298331087187823333")}, stof<T>("1.7373874978377483206255380856904256285442446922242066650446628043")};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[115].real()/kbase.W[115].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[143]) - log_iphi_im(kbase.W[143]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_875_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_875_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("11.10534222871100003410115492599771904833386898505089999463676076"),stof<T>("-18.213604283265595539917224204777688430841391446621955188903577487")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({202});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,86> abb = {dl[0], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W15(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W69(k,dl), dlog_W118(k,dv), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_15(k), f_2_19(k), f_2_23(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_11_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, T{0}, T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[115].real()/k.W[115].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[143]) - log_iphi_im(k.W[143])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_11_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), T{0}, T{0}, T{0}, T{0}, T{0}};
{
auto c = dlog_W169(k,dv) * f_2_34(k);
abb[81] = c.real();
abb[55] = c.imag();
SpDLog_Sigma5<T,202,168>(k, dv, abb[81], abb[55], f_2_34_series_coefficients<T>);
}
{
auto c = dlog_W172(k,dv) * f_2_34(k);
abb[82] = c.real();
abb[56] = c.imag();
SpDLog_Sigma5<T,202,171>(k, dv, abb[82], abb[56], f_2_34_series_coefficients<T>);
}
{
auto c = dlog_W175(k,dv) * f_2_34(k);
abb[83] = c.real();
abb[57] = c.imag();
SpDLog_Sigma5<T,202,174>(k, dv, abb[83], abb[57], f_2_34_series_coefficients<T>);
}
{
auto c = dlog_W178(k,dv) * f_2_34(k);
abb[84] = c.real();
abb[58] = c.imag();
SpDLog_Sigma5<T,202,177>(k, dv, abb[84], abb[58], f_2_34_series_coefficients<T>);
}
{
auto c = dlog_W193(k,dv) * f_2_34(k);
abb[85] = c.real();
abb[59] = c.imag();
SpDLog_Sigma5<T,202,192>(k, dv, abb[85], abb[59], f_2_34_series_coefficients<T>);
}

                    
            return f_4_875_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_875_DLogXconstant_part(base_point<T>, kend);
	value += f_4_875_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_875_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_875_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_875_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_875_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_875_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_875_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
