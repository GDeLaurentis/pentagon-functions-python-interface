/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_10.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_10_abbreviated (const std::array<T,29>& abb) {
T z[37];
z[0] = abb[21] + abb[23];
z[1] = abb[20] * (T(13) / T(3));
z[2] = 3 * abb[22];
z[0] = abb[24] + abb[19] * (T(-16) / T(3)) + abb[18] * (T(-4) / T(3)) + (T(-8) / T(3)) * z[0] + z[1] + z[2];
z[0] = abb[0] * z[0];
z[3] = 2 * abb[19] + abb[21];
z[4] = -abb[18] + abb[20];
z[5] = 2 * abb[23] + -z[4];
z[6] = z[3] + -z[5];
z[6] = abb[2] * z[6];
z[7] = abb[18] + abb[23];
z[8] = 4 * abb[19] + 2 * abb[21];
z[9] = -abb[24] + -z[7] + z[8];
z[10] = abb[6] * z[9];
z[11] = -abb[18] + abb[22];
z[12] = -abb[24] + z[3] + (T(1) / T(2)) * z[11];
z[12] = abb[4] * z[12];
z[13] = 3 * abb[24];
z[1] = abb[18] * (T(-8) / T(3)) + abb[19] * (T(-2) / T(3)) + abb[21] * (T(-1) / T(3)) + abb[23] * (T(5) / T(3)) + -z[1] + z[13];
z[1] = abb[3] * z[1];
z[14] = -abb[24] + abb[21] * (T(5) / T(3)) + abb[19] * (T(10) / T(3)) + (T(-2) / T(3)) * z[7];
z[14] = abb[5] * z[14];
z[15] = -abb[22] + abb[18] * (T(1) / T(3));
z[15] = abb[24] + abb[19] * (T(-4) / T(3)) + abb[21] * (T(-2) / T(3)) + abb[23] * (T(-1) / T(3)) + (T(1) / T(2)) * z[15];
z[15] = abb[1] * z[15];
z[0] = z[0] + z[1] + (T(-13) / T(3)) * z[6] + -z[10] + 5 * z[12] + 4 * z[14] + z[15];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[1] = 3 * z[10];
z[12] = 8 * abb[19] + 4 * abb[21];
z[14] = z[12] + -z[13];
z[15] = z[7] + -z[14];
z[16] = 4 * abb[1];
z[16] = z[15] * z[16];
z[16] = z[1] + z[16];
z[17] = abb[18] + 15 * abb[24];
z[18] = 16 * abb[19] + 8 * abb[21];
z[19] = abb[20] * (T(1) / T(2));
z[20] = -abb[23] + z[19];
z[17] = (T(-1) / T(2)) * z[17] + z[18] + z[20];
z[17] = abb[3] * z[17];
z[21] = abb[18] + z[13];
z[20] = -z[8] + -z[20] + (T(1) / T(2)) * z[21];
z[21] = abb[2] * z[20];
z[22] = abb[25] + abb[26];
z[23] = (T(3) / T(2)) * z[22];
z[24] = abb[7] * z[23];
z[21] = z[21] + -z[24];
z[24] = z[8] + -z[13];
z[25] = z[5] + z[24];
z[25] = abb[0] * z[25];
z[26] = z[7] + z[24];
z[27] = abb[5] * z[26];
z[23] = abb[8] * z[23];
z[17] = z[16] + -z[17] + z[21] + -z[23] + z[25] + 2 * z[27];
z[17] = abb[13] * z[17];
z[25] = abb[0] * z[26];
z[26] = 2 * abb[3];
z[28] = z[15] * z[26];
z[25] = z[16] + z[25] + z[27] + z[28];
z[28] = abb[12] * z[25];
z[29] = 2 * z[28];
z[17] = z[17] + -z[29];
z[30] = 3 * abb[23];
z[31] = 9 * abb[24];
z[32] = -7 * abb[18] + -z[31];
z[18] = z[18] + -z[19] + -z[30] + (T(1) / T(2)) * z[32];
z[18] = abb[3] * z[18];
z[21] = z[21] + -z[23];
z[23] = abb[18] + abb[20];
z[32] = -z[23] + -z[24];
z[32] = abb[0] * z[32];
z[16] = -z[16] + z[18] + z[21] + z[32];
z[16] = abb[11] * z[16];
z[5] = z[5] + -z[14];
z[18] = abb[2] * z[5];
z[32] = abb[8] * z[22];
z[18] = z[18] + -3 * z[32];
z[32] = 2 * abb[1];
z[33] = z[15] * z[32];
z[34] = -z[3] + z[23];
z[35] = 4 * abb[3];
z[34] = z[34] * z[35];
z[23] = -z[14] + z[23];
z[36] = -abb[0] * z[23];
z[34] = -z[18] + z[33] + z[34] + z[36];
z[34] = abb[14] * z[34];
z[16] = z[16] + -z[17] + z[34];
z[16] = abb[14] * z[16];
z[8] = 2 * abb[24] + -z[8];
z[34] = -z[8] + z[11];
z[34] = abb[4] * z[34];
z[36] = z[10] + z[34];
z[15] = z[15] * z[35];
z[7] = 20 * abb[19] + 10 * abb[21] + -z[7] + -z[31];
z[7] = abb[5] * z[7];
z[14] = 2 * abb[18] + -z[2] + z[14];
z[35] = -abb[23] + z[14];
z[35] = abb[0] * z[35];
z[2] = 4 * abb[18] + -32 * abb[19] + -16 * abb[21] + 7 * abb[23] + z[2] + z[31];
z[2] = z[2] * z[32];
z[2] = z[2] + z[7] + z[15] + z[35] + 3 * z[36];
z[2] = prod_pow(abb[12], 2) * z[2];
z[1] = z[1] + z[33];
z[3] = -abb[18] + z[3];
z[3] = 2 * z[3];
z[7] = abb[20] + z[3] + -z[30];
z[7] = abb[0] * z[7];
z[15] = abb[5] * z[9];
z[23] = abb[3] * z[23];
z[31] = abb[7] * z[22];
z[31] = 3 * z[31];
z[6] = -z[1] + -4 * z[6] + z[7] + 3 * z[15] + -z[23] + -z[31];
z[6] = abb[13] * z[6];
z[7] = -z[6] + -z[29];
z[7] = abb[13] * z[7];
z[13] = 5 * abb[18] + z[13];
z[12] = -z[12] + (T(1) / T(2)) * z[13] + -z[19] + z[30];
z[12] = abb[3] * z[12];
z[13] = -z[10] + z[34];
z[13] = 3 * z[13] + -z[33];
z[15] = -abb[20] + z[14];
z[15] = abb[0] * z[15];
z[12] = z[12] + -z[13] + z[15] + z[21];
z[12] = abb[11] * z[12];
z[12] = z[12] + z[17];
z[12] = abb[11] * z[12];
z[8] = z[8] + z[11];
z[8] = abb[0] * z[8];
z[11] = z[9] * z[26];
z[8] = z[8] + -2 * z[10] + z[11] + -z[33] + z[34];
z[8] = 3 * z[8];
z[10] = abb[27] * z[8];
z[11] = abb[28] * z[25];
z[15] = -abb[8] * z[20];
z[17] = abb[2] + abb[3];
z[17] = abb[0] + -2 * abb[10] + (T(3) / T(2)) * z[17];
z[17] = z[17] * z[22];
z[9] = abb[9] * z[9];
z[4] = abb[24] + z[4];
z[4] = -abb[23] + (T(1) / T(2)) * z[4];
z[4] = abb[7] * z[4];
z[4] = z[4] + z[9] + z[15] + z[17];
z[4] = abb[15] * z[4];
z[0] = z[0] + z[2] + 3 * z[4] + z[7] + z[10] + -2 * z[11] + z[12] + z[16];
z[2] = z[18] + z[27];
z[4] = -abb[3] * z[5];
z[5] = 2 * abb[20] + -abb[23];
z[7] = z[5] + -z[14];
z[7] = abb[0] * z[7];
z[4] = -z[2] + z[4] + z[7] + z[13] + z[31];
z[4] = abb[11] * z[4];
z[5] = -abb[18] + -z[5] + -z[24];
z[5] = z[5] * z[26];
z[3] = abb[20] + abb[23] + -z[3];
z[3] = abb[0] * z[3];
z[1] = z[1] + z[2] + z[3] + z[5];
z[1] = abb[14] * z[1];
z[1] = z[1] + z[4] + z[6] + z[28];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[17] * z[25];
z[1] = z[1] + z[2];
z[2] = abb[16] * z[8];
z[1] = 2 * z[1] + z[2];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_10_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-24.99956298879195724646334096950701232722700192918398818690128959"),stof<T>("-47.354596369017377296165638330941865319511630445591128968004822401")}, std::complex<T>{stof<T>("-14.862061020734381865570907662947353228716358449602989244427518"),stof<T>("206.80392685970067009658626671375513608524240666940609631979498808")}, std::complex<T>{stof<T>("-73.500543323593783447196206033191752221167670670468919847632667344"),stof<T>("-10.418592166951844358583751536142331540900590163798333987904556194")}, std::complex<T>{stof<T>("-7.431030510367190932785453831473676614358179224801494622213759"),stof<T>("103.401963429850335048293133356877568042621203334703048159897494039")}, std::complex<T>{stof<T>("31.378019525416473642312525441353061252554471972194097149142889437"),stof<T>("-40.821802037629766081497972504998765526290901285783586822188641969")}, std::complex<T>{stof<T>("79.878999860218299843045390505037801146495140713479028809874267191"),stof<T>("-77.757806239695299019079859299798299304901941567576381802288908176")}, std::complex<T>{stof<T>("1.052573973742674536936269359627627689030709181791385659972159153"),stof<T>("-15.225565023203191670629522520936937196818671603328332369704029669")}, std::complex<T>{stof<T>("-3.072668366318546599157628038775032785955441755808356421746132133"),stof<T>("47.246795076652142140372750836458559900495186049435789623190676203")}, std::complex<T>{stof<T>("-3.072668366318546599157628038775032785955441755808356421746132133"),stof<T>("47.246795076652142140372750836458559900495186049435789623190676203")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_10_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_10_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("106.481314249209121611074205382197676437821409470914134683314817812"),stof<T>("-25.893739974231519378713504071337890986924851543338068036664474094")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dl[5], dl[2], dl[4], dlog_W15(k,dl), dlog_W22(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_4_re(k), f_2_7_re(k)};

                    
            return f_4_10_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_10_DLogXconstant_part(base_point<T>, kend);
	value += f_4_10_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_10_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_10_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_10_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_10_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_10_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_10_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
