/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_295.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_295_abbreviated (const std::array<T,30>& abb) {
T z[20];
z[0] = abb[21] + -abb[24];
z[1] = abb[20] + z[0];
z[1] = abb[8] * z[1];
z[2] = abb[23] + -abb[24];
z[3] = -abb[19] + z[2];
z[3] = abb[2] * z[3];
z[4] = z[1] + z[3];
z[5] = abb[20] + abb[22] + -abb[24];
z[5] = abb[1] * z[5];
z[6] = -abb[19] + abb[24];
z[6] = abb[0] * z[6];
z[4] = 5 * z[4] + z[5] + 13 * z[6];
z[7] = abb[21] + abb[23];
z[8] = -2 * abb[24] + z[7];
z[9] = abb[4] * z[8];
z[0] = abb[19] + z[0];
z[0] = abb[6] * z[0];
z[10] = -z[0] + z[9];
z[11] = abb[5] * z[8];
z[7] = -abb[24] + (T(1) / T(2)) * z[7];
z[12] = abb[7] * z[7];
z[13] = abb[9] * z[7];
z[2] = -abb[20] + z[2];
z[2] = abb[3] * z[2];
z[14] = abb[0] * abb[22];
z[4] = -z[2] + (T(1) / T(6)) * z[4] + (T(-4) / T(3)) * z[10] + z[11] + (T(-1) / T(3)) * z[12] + z[13] + (T(-13) / T(6)) * z[14];
z[4] = prod_pow(m1_set::bc<T>[0], 2) * z[4];
z[6] = z[6] + -z[14];
z[7] = abb[4] * z[7];
z[10] = (T(-3) / T(2)) * z[0] + (T(-1) / T(2)) * z[3] + -2 * z[6] + z[7] + -z[11] + z[13];
z[10] = abb[10] * z[10];
z[14] = abb[7] * z[8];
z[15] = abb[9] * z[8];
z[14] = z[14] + -z[15];
z[16] = z[0] + -z[3];
z[17] = z[14] + z[16];
z[17] = abb[11] * z[17];
z[18] = -abb[4] + -abb[7] + 2 * abb[9];
z[18] = z[8] * z[18];
z[19] = abb[12] * z[18];
z[10] = z[10] + z[17] + z[19];
z[10] = abb[10] * z[10];
z[5] = z[1] + z[2] + -2 * z[5] + z[11] + -z[15];
z[5] = prod_pow(abb[13], 2) * z[5];
z[1] = z[1] + -z[2];
z[2] = -z[1] + z[7] + -z[12];
z[2] = prod_pow(abb[12], 2) * z[2];
z[7] = z[12] + -z[13] + (T(1) / T(2)) * z[16];
z[7] = prod_pow(abb[11], 2) * z[7];
z[9] = z[9] + -z[15];
z[11] = z[9] + -z[16];
z[12] = -abb[26] * z[11];
z[9] = z[1] + -z[9];
z[13] = abb[28] * z[9];
z[15] = abb[25] + abb[27] + abb[29];
z[15] = z[15] * z[18];
z[2] = z[2] + z[4] + z[5] + z[7] + z[10] + z[12] + z[13] + z[15];
z[4] = 2 * abb[5] + abb[7] + -3 * abb[9];
z[4] = z[4] * z[8];
z[0] = 3 * z[0] + z[3] + z[4] + 4 * z[6];
z[0] = abb[10] * z[0];
z[1] = z[1] + z[14];
z[1] = abb[12] * z[1];
z[0] = z[0] + 2 * z[1] + -z[17];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = -abb[15] * z[11];
z[3] = abb[17] * z[9];
z[4] = abb[14] + abb[16] + abb[18];
z[4] = z[4] * z[18];
z[0] = z[0] + z[1] + z[3] + z[4];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_295_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("17.092293306056192057057633858270108648520582965299943688790563879"),stof<T>("23.27933632906989773930226187286161777982511987753463968767487195")}, std::complex<T>{stof<T>("7.5622380660705743894746219887445304495811624597824558703195410604"),stof<T>("-8.7108938750076768380150066101209595122101499136975628868840538663")}, std::complex<T>{stof<T>("13.42775786523829691250111395106213141786863125318576733409825116"),stof<T>("-16.346271679199142167712778478337626213889107075330948061628495939")}, std::complex<T>{stof<T>("0.918164975983566437662010803917862315779212184465581150959880746"),stof<T>("25.838563900249850146230262555917829335187740471570923078453781753")}, std::complex<T>{stof<T>("-10.308608530904903096369131092034645364453901987431051074051973034"),stof<T>("-5.0761502330115129227697711851604551463163365675971017839655322696")}, std::complex<T>{stof<T>("-4.0373143103169602537939936629453483691939414502202974110061588719"),stof<T>("-4.4161419880391950557477128924197479749822968286428732328597535447")}};
	
	std::vector<C> intdlogs = {rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[32].real()/kbase.W[32].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_295_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_295_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("17.300234841162093627242916713379088679168360301723477372897146887"),stof<T>("28.971393927049163164684458534700703891651748279274568310716689202")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,30> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), f_1_1(k), f_1_4(k), f_1_5(k), f_1_11(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[32].real()/k.W[32].real()), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_4_295_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_295_DLogXconstant_part(base_point<T>, kend);
	value += f_4_295_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_295_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_295_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_295_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_295_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_295_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_295_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
