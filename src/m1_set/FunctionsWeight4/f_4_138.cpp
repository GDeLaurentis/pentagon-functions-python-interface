/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_138.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_138_abbreviated (const std::array<T,56>& abb) {
T z[100];
z[0] = abb[3] * (T(1) / T(2));
z[1] = abb[4] * (T(1) / T(2));
z[2] = -abb[7] + z[1];
z[3] = abb[5] * (T(1) / T(2));
z[4] = abb[0] * (T(1) / T(2));
z[5] = -abb[9] + z[0] + z[2] + -z[3] + -z[4];
z[6] = abb[33] * (T(1) / T(2));
z[5] = z[5] * z[6];
z[7] = abb[30] * z[4];
z[8] = abb[31] * z[4];
z[9] = z[7] + -z[8];
z[10] = abb[32] * z[1];
z[11] = abb[32] * (T(1) / T(2));
z[12] = abb[5] * z[11];
z[13] = z[10] + -z[12];
z[14] = abb[9] * abb[32];
z[15] = z[13] + -z[14];
z[16] = -abb[30] + abb[31];
z[17] = abb[28] + abb[32];
z[18] = -z[16] + z[17];
z[19] = abb[7] * (T(1) / T(2));
z[20] = z[18] * z[19];
z[21] = abb[28] * z[4];
z[22] = abb[3] * z[11];
z[5] = z[5] + z[9] + -z[15] + z[20] + z[21] + -z[22];
z[5] = abb[33] * z[5];
z[23] = 4 * abb[31];
z[24] = 4 * abb[30];
z[25] = -4 * abb[28] + -abb[32] + abb[29] * (T(7) / T(2)) + -z[23] + z[24];
z[25] = abb[29] * z[25];
z[26] = prod_pow(abb[32], 2);
z[26] = (T(1) / T(2)) * z[26];
z[27] = -abb[37] + abb[54];
z[28] = z[26] + -z[27];
z[29] = abb[28] + -abb[30];
z[30] = 2 * z[29];
z[31] = abb[31] + z[30];
z[31] = abb[31] * z[31];
z[32] = 2 * abb[30];
z[33] = -abb[28] + z[32];
z[34] = abb[28] * z[33];
z[35] = prod_pow(abb[30], 2);
z[25] = -2 * abb[34] + -z[25] + -z[28] + -z[31] + z[34] + -z[35];
z[25] = abb[1] * z[25];
z[31] = 2 * abb[11];
z[34] = z[4] + z[31];
z[36] = -abb[28] * z[34];
z[37] = abb[32] * z[4];
z[38] = abb[0] + z[31];
z[39] = abb[30] * z[38];
z[36] = -z[8] + z[36] + z[37] + z[39];
z[36] = abb[31] * z[36];
z[39] = abb[0] * (T(3) / T(2));
z[40] = z[31] + z[39];
z[41] = abb[28] + z[16];
z[40] = z[40] * z[41];
z[42] = 2 * abb[3];
z[43] = z[41] * z[42];
z[44] = -z[31] + z[42];
z[45] = abb[0] * (T(3) / T(4));
z[46] = -abb[9] + z[44] + -z[45];
z[46] = abb[29] * z[46];
z[46] = z[40] + -z[43] + z[46];
z[46] = abb[29] * z[46];
z[47] = abb[36] + z[28];
z[48] = abb[31] * (T(1) / T(2));
z[49] = abb[28] + z[48];
z[50] = abb[31] * z[49];
z[51] = (T(1) / T(2)) * z[35];
z[52] = abb[28] * (T(1) / T(2));
z[53] = -abb[32] + z[52];
z[54] = -abb[30] + z[53];
z[55] = abb[28] * z[54];
z[55] = -z[47] + z[50] + -z[51] + z[55];
z[56] = -abb[32] + z[41];
z[57] = abb[29] * (T(1) / T(2));
z[56] = z[56] * z[57];
z[55] = abb[34] + (T(1) / T(2)) * z[55] + -z[56];
z[55] = abb[7] * z[55];
z[58] = -abb[32] + z[57];
z[58] = abb[29] * z[58];
z[59] = z[28] + z[58];
z[60] = z[35] + z[59];
z[61] = abb[28] * abb[30];
z[62] = abb[31] * z[29];
z[62] = -z[60] + z[61] + z[62];
z[63] = abb[8] * (T(1) / T(2));
z[62] = z[62] * z[63];
z[64] = z[4] * z[35];
z[65] = -abb[32] + z[48];
z[65] = abb[31] * z[65];
z[58] = abb[54] + -z[58] + z[65];
z[65] = abb[6] * (T(1) / T(2));
z[58] = z[58] * z[65];
z[66] = abb[28] * z[53];
z[59] = abb[34] + -abb[36] + -z[59] + z[66];
z[59] = z[1] * z[59];
z[66] = abb[30] * abb[32];
z[67] = abb[36] + z[66];
z[68] = z[51] + -z[67];
z[68] = z[3] * z[68];
z[69] = abb[0] * abb[30];
z[70] = -z[14] + -z[69];
z[70] = z[11] * z[70];
z[71] = -abb[0] + abb[9];
z[72] = abb[28] * z[71];
z[7] = z[7] + z[72];
z[7] = abb[28] * z[7];
z[72] = abb[36] + abb[37];
z[26] = z[26] + z[72];
z[73] = z[26] + -z[61];
z[74] = abb[31] + 3 * z[29];
z[74] = abb[31] * z[74];
z[74] = z[73] + z[74];
z[74] = z[0] * z[74];
z[75] = 2 * abb[9];
z[76] = -z[39] + z[75];
z[77] = abb[34] * z[76];
z[78] = -abb[16] + abb[17];
z[79] = abb[38] * (T(1) / T(2));
z[80] = z[78] * z[79];
z[72] = -abb[9] * z[72];
z[81] = -abb[5] + abb[7];
z[81] = (T(1) / T(2)) * z[81];
z[82] = abb[55] * z[81];
z[5] = z[5] + z[7] + -z[25] + z[36] + z[46] + z[55] + -z[58] + -z[59] + z[62] + -z[64] + z[68] + z[70] + z[72] + z[74] + z[77] + z[80] + z[82];
z[5] = abb[49] * z[5];
z[7] = z[30] + z[48];
z[7] = abb[31] * z[7];
z[30] = -z[11] + z[41];
z[36] = abb[29] * (T(1) / T(4)) + -z[30];
z[36] = abb[29] * z[36];
z[46] = abb[32] * (T(1) / T(4));
z[55] = abb[30] + z[46];
z[55] = abb[32] * z[55];
z[62] = abb[30] + abb[32];
z[70] = abb[28] * z[62];
z[7] = z[7] + (T(1) / T(2)) * z[27] + -z[35] + z[36] + -z[55] + z[70];
z[7] = abb[8] * z[7];
z[36] = z[27] + (T(-3) / T(2)) * z[35];
z[70] = -z[52] * z[54];
z[72] = abb[36] * (T(1) / T(2));
z[74] = -abb[30] + abb[31] * (T(1) / T(4)) + abb[28] * (T(3) / T(2));
z[74] = abb[31] * z[74];
z[36] = (T(1) / T(2)) * z[36] + -z[55] + -z[56] + z[70] + -z[72] + z[74];
z[36] = abb[7] * z[36];
z[56] = abb[5] * (T(1) / T(4));
z[70] = 2 * abb[12];
z[74] = z[56] + -z[70];
z[77] = abb[4] * (T(1) / T(4));
z[80] = z[74] + -z[77];
z[82] = abb[0] * (T(7) / T(4));
z[83] = abb[15] * (T(1) / T(2));
z[84] = abb[10] + abb[3] * (T(5) / T(4)) + -z[19] + z[80] + -z[82] + z[83];
z[84] = abb[33] * z[84];
z[85] = -abb[28] + z[16];
z[86] = z[11] + z[85];
z[86] = abb[3] * z[86];
z[87] = z[39] + z[70];
z[88] = -z[85] * z[87];
z[89] = abb[15] * z[85];
z[13] = z[13] + z[20] + z[84] + z[86] + z[88] + z[89];
z[13] = abb[33] * z[13];
z[20] = -z[32] + -z[46];
z[20] = abb[32] * z[20];
z[46] = abb[37] + -z[35];
z[84] = abb[28] * z[17];
z[33] = abb[32] + z[33];
z[88] = -abb[31] * z[33];
z[20] = z[20] + (T(1) / T(2)) * z[46] + -z[72] + z[84] + z[88];
z[20] = abb[3] * z[20];
z[46] = abb[28] * z[29];
z[72] = abb[31] * z[41];
z[46] = z[46] + z[72];
z[72] = z[41] + -z[57];
z[72] = abb[29] * z[72];
z[46] = abb[34] + (T(1) / T(2)) * z[46] + -z[72];
z[46] = abb[14] * z[46];
z[84] = -abb[28] + abb[31];
z[88] = abb[28] + abb[30];
z[90] = -z[84] * z[88];
z[90] = z[35] + z[90];
z[91] = z[83] * z[90];
z[46] = z[46] + z[64] + -z[91];
z[64] = abb[11] + abb[12];
z[64] = z[32] * z[64];
z[91] = -z[31] + z[39];
z[91] = abb[28] * z[91];
z[92] = abb[32] * z[39];
z[8] = z[8] + z[64] + z[91] + z[92];
z[8] = abb[31] * z[8];
z[34] = z[34] * z[41];
z[44] = abb[0] * (T(-5) / T(4)) + abb[10] * (T(1) / T(2)) + z[44];
z[44] = abb[29] * z[44];
z[91] = abb[10] * abb[32];
z[34] = z[34] + -z[43] + z[44] + -z[91];
z[34] = abb[29] * z[34];
z[43] = z[58] + -z[59];
z[44] = z[43] + z[68];
z[58] = z[3] + -z[19];
z[59] = abb[0] + abb[12];
z[92] = 3 * abb[3] + abb[8];
z[59] = abb[15] + -z[58] + -2 * z[59] + z[92];
z[93] = -abb[55] * z[59];
z[28] = abb[10] * z[28];
z[94] = z[4] * z[66];
z[95] = -z[4] + z[70];
z[96] = -abb[30] * z[95];
z[97] = -abb[0] + -abb[10];
z[97] = abb[28] * z[97];
z[96] = z[96] + z[97];
z[96] = abb[28] * z[96];
z[97] = -abb[0] * abb[37];
z[98] = abb[3] + -z[4];
z[98] = abb[34] * z[98];
z[7] = z[7] + z[8] + z[13] + z[20] + -z[25] + z[28] + z[34] + z[36] + -z[44] + -z[46] + z[93] + z[94] + z[96] + z[97] + z[98];
z[7] = abb[47] * z[7];
z[8] = 2 * abb[8];
z[13] = abb[2] * (T(7) / T(2)) + z[8];
z[20] = abb[3] * (T(1) / T(4));
z[25] = abb[9] + abb[0] * (T(-5) / T(2));
z[25] = z[13] + z[20] + (T(1) / T(2)) * z[25] + z[80] + -z[83];
z[25] = abb[33] * z[25];
z[34] = abb[32] + z[85];
z[34] = z[19] * z[34];
z[36] = z[8] * z[85];
z[24] = abb[32] + z[24];
z[24] = abb[2] * z[24];
z[24] = z[24] + -z[34] + -z[36];
z[34] = z[4] + z[70];
z[36] = abb[30] * z[34];
z[80] = 4 * abb[2];
z[34] = z[34] + -z[80];
z[93] = -z[34] * z[84];
z[15] = z[15] + -z[24] + z[25] + z[36] + z[86] + -z[89] + z[93];
z[15] = abb[33] * z[15];
z[25] = -z[48] + -2 * z[62];
z[25] = abb[31] * z[25];
z[30] = abb[29] * (T(5) / T(4)) + -z[30];
z[30] = abb[29] * z[30];
z[62] = abb[37] + abb[54];
z[86] = abb[30] + z[17];
z[86] = abb[28] * z[86];
z[25] = z[25] + z[30] + -z[55] + (T(1) / T(2)) * z[62] + z[86];
z[25] = abb[8] * z[25];
z[30] = -abb[2] + z[4];
z[30] = abb[31] * z[30];
z[55] = 2 * abb[2];
z[62] = abb[30] * z[55];
z[30] = z[30] + z[62];
z[86] = -z[4] + z[55];
z[93] = -z[31] + -z[86];
z[93] = abb[28] * z[93];
z[64] = -z[30] + z[37] + z[64] + z[93];
z[64] = abb[31] * z[64];
z[93] = z[39] + -z[70];
z[93] = abb[30] * z[93];
z[94] = -abb[0] + abb[2];
z[96] = -abb[9] + z[94];
z[96] = abb[28] * z[96];
z[93] = z[62] + z[93] + z[96];
z[93] = abb[28] * z[93];
z[96] = -z[26] + z[35];
z[97] = abb[32] + z[32];
z[97] = abb[28] * z[97];
z[33] = -abb[31] + -z[33];
z[33] = abb[31] * z[33];
z[33] = z[33] + (T(1) / T(2)) * z[96] + z[97];
z[33] = abb[3] * z[33];
z[82] = abb[9] + -z[31] + -z[82];
z[82] = abb[29] * z[82];
z[40] = z[40] + z[82];
z[40] = abb[29] * z[40];
z[47] = z[47] + -z[51];
z[82] = 3 * abb[30];
z[96] = -z[53] + z[82];
z[96] = abb[28] * z[96];
z[96] = -z[47] + z[96];
z[97] = -abb[29] + z[17];
z[98] = z[16] + z[97];
z[99] = z[57] * z[98];
z[54] = abb[31] * (T(-3) / T(4)) + z[54];
z[54] = abb[31] * z[54];
z[54] = -abb[34] + z[54] + (T(1) / T(2)) * z[96] + z[99];
z[54] = abb[7] * z[54];
z[26] = z[26] + z[35];
z[26] = abb[2] * z[26];
z[14] = z[14] + 3 * z[69];
z[14] = z[11] * z[14];
z[96] = -abb[3] + -z[39] + -z[75];
z[96] = abb[34] * z[96];
z[71] = abb[37] * z[71];
z[99] = abb[9] * abb[36];
z[14] = z[14] + z[15] + z[25] + z[26] + z[33] + z[40] + -z[44] + z[46] + z[54] + z[64] + z[71] + z[93] + z[96] + z[99];
z[14] = abb[48] * z[14];
z[13] = abb[10] + -z[13] + z[20] + z[45] + -z[74] + -z[77];
z[13] = abb[33] * z[13];
z[15] = z[80] + -z[87];
z[20] = -z[15] * z[84];
z[25] = -abb[30] * z[87];
z[10] = z[10] + z[12] + z[13] + z[20] + -z[22] + z[24] + z[25];
z[10] = abb[33] * z[10];
z[12] = abb[28] * z[86];
z[13] = -abb[0] + -z[70];
z[13] = abb[30] * z[13];
z[12] = z[12] + z[13] + z[30] + z[37];
z[12] = abb[31] * z[12];
z[13] = abb[10] + z[4];
z[13] = z[13] * z[57];
z[9] = z[9] + z[13] + -z[21] + -z[91];
z[9] = abb[29] * z[9];
z[13] = abb[29] * z[98];
z[20] = -abb[30] + -z[53];
z[20] = abb[28] * z[20];
z[20] = -z[13] + z[20] + z[47] + z[50];
z[20] = z[19] * z[20];
z[21] = -abb[10] + -z[94];
z[21] = abb[28] * z[21];
z[21] = z[21] + z[36] + -z[62];
z[21] = abb[28] * z[21];
z[22] = abb[28] + z[82];
z[22] = abb[31] * z[22];
z[22] = z[22] + -z[60] + -3 * z[61];
z[22] = z[22] * z[63];
z[24] = z[35] + -z[66];
z[24] = abb[0] * z[24];
z[25] = abb[31] + -z[29];
z[25] = abb[31] * z[25];
z[25] = z[25] + z[73];
z[25] = z[0] * z[25];
z[30] = abb[34] * z[4];
z[9] = z[9] + z[10] + z[12] + z[20] + z[21] + z[22] + (T(1) / T(2)) * z[24] + z[25] + -z[26] + z[28] + z[30] + -z[43] + z[68];
z[9] = abb[50] * z[9];
z[10] = z[41] * z[48];
z[12] = abb[30] * (T(1) / T(2));
z[20] = -abb[32] + z[12];
z[20] = abb[28] * z[20];
z[21] = -abb[30] + z[11];
z[21] = abb[32] * z[21];
z[10] = z[10] + -z[13] + -z[20] + z[21] + -z[27] + z[51];
z[20] = abb[21] * z[10];
z[10] = -abb[55] + z[10];
z[10] = abb[23] * z[10];
z[21] = -abb[34] + z[61] + z[67];
z[22] = -abb[33] + z[18];
z[22] = abb[33] * z[22];
z[24] = abb[31] * z[17];
z[13] = abb[54] + z[13] + z[21] + -z[22] + -z[24];
z[24] = -abb[20] * z[13];
z[21] = -z[21] + z[50] + -z[72];
z[21] = abb[22] * z[21];
z[18] = abb[22] * z[18];
z[25] = abb[25] * z[85];
z[26] = abb[25] * (T(1) / T(2));
z[27] = -abb[22] + z[26];
z[27] = abb[33] * z[27];
z[18] = z[18] + z[25] + z[27];
z[18] = abb[33] * z[18];
z[27] = z[48] + -z[52];
z[27] = z[27] * z[88];
z[22] = abb[53] + z[22] + z[27] + -z[67];
z[22] = abb[24] * z[22];
z[26] = z[26] * z[90];
z[27] = prod_pow(m1_set::bc<T>[0], 2);
z[28] = abb[21] + abb[23];
z[30] = -abb[25] + -z[28];
z[30] = z[27] * z[30];
z[33] = abb[21] + abb[25];
z[36] = -abb[55] * z[33];
z[37] = -abb[26] * z[79];
z[40] = abb[22] + z[28];
z[41] = abb[35] * z[40];
z[40] = -abb[25] + z[40];
z[43] = abb[20] + z[40];
z[43] = abb[53] * z[43];
z[10] = z[10] + z[18] + z[20] + z[21] + z[22] + z[24] + z[26] + (T(1) / T(6)) * z[30] + z[36] + z[37] + z[41] + z[43];
z[18] = abb[51] * (T(1) / T(2));
z[10] = z[10] * z[18];
z[20] = abb[42] + abb[44] + -abb[45] + -abb[46];
z[21] = abb[43] + (T(1) / T(2)) * z[20];
z[22] = 2 * abb[52] + z[21];
z[24] = abb[31] * z[22];
z[26] = abb[30] * z[22];
z[24] = z[24] + -z[26];
z[21] = abb[52] + (T(1) / T(2)) * z[21];
z[30] = abb[29] * z[21];
z[36] = abb[28] * z[22];
z[30] = z[24] + -z[30] + z[36];
z[30] = abb[29] * z[30];
z[35] = z[21] * z[35];
z[37] = -z[22] * z[66];
z[41] = abb[32] * z[22];
z[43] = abb[30] * z[21];
z[44] = z[41] + z[43];
z[44] = abb[28] * z[44];
z[45] = abb[28] * z[21];
z[46] = -z[43] + z[45];
z[47] = abb[31] * z[21];
z[50] = 3 * z[46] + z[47];
z[50] = abb[31] * z[50];
z[37] = -z[30] + -z[35] + z[37] + z[44] + z[50];
z[37] = abb[16] * z[37];
z[44] = abb[17] * z[22];
z[50] = abb[16] * z[22];
z[51] = z[44] + z[50];
z[52] = -z[19] + z[65];
z[53] = abb[0] + abb[11];
z[54] = 2 * z[53];
z[60] = z[52] + z[54];
z[61] = abb[3] + 3 * abb[8];
z[64] = abb[14] + -z[60] + z[61];
z[64] = abb[48] * z[64];
z[66] = -abb[6] + abb[7];
z[67] = abb[50] * (T(1) / T(2));
z[68] = z[66] * z[67];
z[60] = 4 * abb[1] + -abb[14] + -z[60] + z[92];
z[60] = abb[47] * z[60];
z[71] = 2 * abb[1] + abb[3] + -z[53];
z[71] = -z[52] + 2 * z[71];
z[71] = abb[49] * z[71];
z[72] = abb[18] * z[22];
z[60] = z[51] + z[60] + z[64] + z[68] + z[71] + z[72];
z[60] = abb[35] * z[60];
z[24] = z[24] + -z[36];
z[64] = abb[33] * z[21];
z[64] = z[24] + z[64];
z[64] = abb[33] * z[64];
z[68] = z[43] + z[45];
z[68] = z[68] * z[84];
z[71] = abb[47] + -abb[48];
z[72] = z[71] * z[79];
z[73] = z[21] * z[27];
z[74] = abb[55] * z[22];
z[68] = -z[35] + -z[64] + z[68] + -z[72] + (T(1) / T(3)) * z[73] + z[74];
z[68] = abb[19] * z[68];
z[41] = z[41] + 3 * z[43];
z[43] = -z[41] + z[45] + -z[47];
z[43] = abb[31] * z[43];
z[41] = abb[28] * z[41];
z[45] = abb[34] * z[22];
z[35] = z[35] + z[41] + z[43] + -z[45] + z[64];
z[35] = abb[17] * z[35];
z[41] = -abb[47] + -abb[48];
z[43] = abb[16] + abb[17];
z[41] = z[41] * z[43];
z[43] = abb[8] + -abb[15];
z[64] = -abb[3] + abb[14] + -z[43];
z[64] = z[21] * z[64];
z[67] = z[67] * z[78];
z[20] = 2 * abb[43] + 4 * abb[52] + z[20];
z[73] = -abb[27] * z[20];
z[74] = -abb[7] * z[22];
z[41] = z[41] + z[64] + z[67] + z[73] + z[74];
z[41] = abb[38] * z[41];
z[64] = abb[28] * z[46];
z[46] = z[46] + z[47];
z[46] = abb[31] * z[46];
z[30] = -z[30] + z[45] + z[46] + z[64] + -z[72];
z[30] = abb[18] * z[30];
z[45] = 2 * abb[10];
z[2] = z[2] + -z[45];
z[46] = z[2] + z[39] + -z[55];
z[46] = abb[50] * z[46];
z[2] = z[2] + -z[39] + -z[43];
z[2] = abb[47] * z[2];
z[39] = z[1] + z[43] + z[86];
z[39] = abb[48] * z[39];
z[43] = abb[0] + abb[4];
z[47] = abb[49] * (T(1) / T(2));
z[43] = z[43] * z[47];
z[2] = z[2] + z[39] + -z[43] + z[46] + -z[50];
z[39] = abb[13] * (T(1) / T(2));
z[43] = abb[49] + abb[50] + z[71];
z[46] = z[39] * z[43];
z[47] = abb[19] * z[22];
z[47] = -z[2] + -z[46] + z[47];
z[47] = abb[53] * z[47];
z[50] = -abb[0] + z[55];
z[64] = -abb[12] + z[50];
z[58] = -z[58] + 2 * z[64];
z[61] = -abb[15] + z[58] + z[61];
z[64] = -abb[48] * z[61];
z[58] = z[8] + z[58];
z[67] = abb[50] * z[58];
z[64] = -z[51] + z[64] + z[67];
z[64] = abb[55] * z[64];
z[67] = -abb[10] + z[63];
z[56] = -abb[12] + z[56];
z[71] = abb[7] * (T(1) / T(4));
z[72] = -z[4] + -z[56] + -z[67] + z[71];
z[72] = abb[50] * z[72];
z[21] = -z[21] * z[78];
z[73] = abb[0] + abb[8] + -z[3];
z[73] = -abb[1] + z[71] + (T(1) / T(2)) * z[73];
z[73] = abb[49] * z[73];
z[21] = z[21] + z[72] + z[73];
z[0] = -5 * abb[0] + -z[0] + z[56] + z[83];
z[0] = abb[7] * (T(-1) / T(12)) + abb[8] * (T(3) / T(2)) + (T(1) / T(3)) * z[0];
z[0] = abb[48] * z[0];
z[56] = -4 * abb[0] + -abb[1] + 5 * abb[3] + abb[8] + abb[10] + z[56] + -z[83];
z[56] = (T(1) / T(3)) * z[56] + z[71];
z[56] = abb[47] * z[56];
z[0] = z[0] + (T(1) / T(3)) * z[21] + z[56];
z[0] = z[0] * z[27];
z[13] = z[13] * z[46];
z[0] = z[0] + z[5] + z[7] + z[9] + z[10] + z[13] + z[14] + z[30] + z[35] + z[37] + z[41] + z[47] + z[60] + z[64] + z[68];
z[5] = abb[29] + -abb[31];
z[5] = z[5] * z[65];
z[7] = -abb[28] + abb[29];
z[7] = z[1] * z[7];
z[9] = z[5] + -z[7];
z[10] = 2 * abb[32];
z[13] = abb[0] * z[10];
z[13] = z[9] + z[13];
z[14] = 3 * abb[31];
z[10] = z[10] + z[14] + -z[29];
z[10] = abb[3] * z[10];
z[11] = z[11] + -z[57];
z[21] = 2 * abb[28];
z[27] = z[21] + -z[32];
z[30] = -z[11] + -z[27];
z[30] = abb[8] * z[30];
z[32] = 2 * abb[31];
z[27] = -3 * abb[29] + abb[32] + z[27] + z[32];
z[27] = abb[1] * z[27];
z[35] = -abb[30] + abb[32];
z[3] = z[3] * z[35];
z[35] = z[3] + -z[69];
z[37] = z[45] + z[95];
z[37] = abb[28] * z[37];
z[41] = -3 * abb[0] + -z[31] + -z[70];
z[41] = abb[31] * z[41];
z[42] = -abb[10] + -z[42] + z[54];
z[42] = abb[29] * z[42];
z[46] = abb[30] * (T(3) / T(2)) + -z[49] + z[57];
z[46] = abb[7] * z[46];
z[47] = abb[3] + abb[15] + -z[19] + -z[87];
z[47] = abb[33] * z[47];
z[10] = z[10] + -z[13] + z[27] + z[30] + -z[35] + z[37] + z[41] + z[42] + z[46] + z[47] + z[89] + z[91];
z[10] = abb[47] * z[10];
z[30] = -z[55] + z[70] + z[76];
z[30] = abb[28] * z[30];
z[37] = abb[29] + -abb[32];
z[23] = -z[21] + z[23] + (T(-3) / T(2)) * z[37];
z[23] = abb[8] * z[23];
z[8] = z[8] + z[19];
z[34] = abb[3] + -abb[15] + z[8] + -z[34];
z[34] = abb[33] * z[34];
z[3] = z[3] + z[69];
z[37] = z[50] + -z[70];
z[31] = -z[31] + z[37];
z[31] = abb[31] * z[31];
z[41] = 2 * abb[29];
z[42] = -abb[9] + z[53];
z[42] = z[41] * z[42];
z[12] = z[12] + -z[57];
z[21] = abb[31] * (T(3) / T(2)) + -z[12] + -z[21];
z[21] = abb[7] * z[21];
z[14] = -3 * abb[28] + -abb[30] + z[14];
z[14] = abb[3] * z[14];
z[13] = -z[3] + -z[13] + z[14] + z[21] + z[23] + z[30] + z[31] + z[34] + z[42] + -z[62] + -z[89];
z[13] = abb[48] * z[13];
z[14] = z[45] + z[55] + -z[87];
z[14] = abb[28] * z[14];
z[21] = -z[11] + -z[32] + z[88];
z[21] = abb[8] * z[21];
z[8] = -z[8] + -z[15];
z[8] = abb[33] * z[8];
z[15] = -abb[31] * z[37];
z[12] = abb[28] + z[12] + -z[48];
z[12] = abb[7] * z[12];
z[23] = -abb[3] * z[84];
z[30] = -abb[10] * abb[29];
z[8] = z[8] + -z[9] + z[12] + z[14] + z[15] + z[21] + z[23] + z[30] + z[35] + z[62] + z[91];
z[8] = abb[50] * z[8];
z[4] = z[4] + -z[75];
z[4] = abb[28] * z[4];
z[9] = -abb[3] + abb[9] + abb[11];
z[9] = z[9] * z[41];
z[11] = -z[11] + -z[29];
z[11] = abb[8] * z[11];
z[12] = abb[29] + -z[16];
z[12] = z[12] * z[19];
z[14] = -abb[0] + -abb[7];
z[6] = z[6] * z[14];
z[14] = -abb[31] * z[38];
z[15] = abb[28] + abb[31];
z[15] = abb[3] * z[15];
z[3] = z[3] + z[4] + -z[5] + z[6] + -z[7] + z[9] + z[11] + z[12] + z[14] + z[15] + z[27];
z[3] = abb[49] * z[3];
z[4] = z[26] + -z[36];
z[4] = abb[16] * z[4];
z[5] = z[20] * z[84];
z[5] = z[5] + -z[26];
z[5] = abb[17] * z[5];
z[6] = abb[33] * z[44];
z[3] = z[3] + z[4] + z[5] + z[6] + z[8] + z[10] + z[13];
z[3] = m1_set::bc<T>[0] * z[3];
z[4] = -abb[30] + z[97];
z[4] = z[4] * z[28];
z[5] = -abb[31] + z[17];
z[6] = abb[22] * z[5];
z[7] = -abb[22] + abb[25];
z[7] = abb[33] * z[7];
z[4] = z[4] + z[6] + z[7] + z[25];
z[4] = m1_set::bc<T>[0] * z[4];
z[6] = -abb[41] * z[33];
z[7] = -abb[21] * abb[40];
z[8] = -abb[40] + -abb[41];
z[8] = abb[23] * z[8];
z[9] = abb[39] * z[40];
z[10] = -abb[33] + z[97];
z[10] = m1_set::bc<T>[0] * z[10];
z[10] = abb[39] + -abb[40] + z[10];
z[11] = abb[20] * z[10];
z[5] = -abb[33] + z[5];
z[5] = m1_set::bc<T>[0] * z[5];
z[5] = abb[39] + z[5];
z[5] = abb[24] * z[5];
z[4] = z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[11];
z[4] = z[4] * z[18];
z[2] = -abb[39] * z[2];
z[5] = -abb[41] * z[59];
z[6] = abb[1] + z[52];
z[7] = z[1] + z[67];
z[8] = -z[6] + z[7];
z[8] = abb[40] * z[8];
z[5] = z[5] + z[8];
z[5] = abb[47] * z[5];
z[7] = z[7] + -z[19] + -z[65];
z[7] = abb[40] * z[7];
z[8] = abb[41] * z[58];
z[7] = z[7] + z[8];
z[7] = abb[50] * z[7];
z[1] = -z[1] + -z[6] + z[63];
z[1] = abb[40] * z[1];
z[6] = abb[41] * z[81];
z[1] = z[1] + z[6];
z[1] = abb[49] * z[1];
z[6] = -z[10] * z[39] * z[43];
z[8] = abb[39] + abb[41];
z[8] = z[8] * z[22];
z[9] = -abb[33] * z[22];
z[9] = z[9] + -z[24];
z[9] = m1_set::bc<T>[0] * z[9];
z[8] = z[8] + z[9];
z[8] = abb[19] * z[8];
z[9] = -abb[41] * z[61];
z[10] = abb[4] + abb[8] + z[66];
z[10] = abb[40] * z[10];
z[9] = z[9] + (T(1) / T(2)) * z[10];
z[9] = abb[48] * z[9];
z[10] = -abb[41] * z[51];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_138_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("3.0212403926155634861881016532672724583386885189432450770666360627"),stof<T>("-6.1454510857536799062108407455617833387997981486530960398138588694")}, std::complex<T>{stof<T>("6.0424807852311269723762033065345449166773770378864901541332721254"),stof<T>("-12.2909021715073598124216814911235666775995962973061920796277177388")}, std::complex<T>{stof<T>("3.0212403926155634861881016532672724583386885189432450770666360627"),stof<T>("-6.1454510857536799062108407455617833387997981486530960398138588694")}, std::complex<T>{stof<T>("-3.0212403926155634861881016532672724583386885189432450770666360627"),stof<T>("6.1454510857536799062108407455617833387997981486530960398138588694")}, std::complex<T>{stof<T>("-3.0212403926155634861881016532672724583386885189432450770666360627"),stof<T>("6.1454510857536799062108407455617833387997981486530960398138588694")}, std::complex<T>{stof<T>("-6.9677034655081426145254938301974874778519076555609150602244021842"),stof<T>("-2.4670173279991011741196022669295648127619777470011308985795000179")}, std::complex<T>{stof<T>("22.530656495243534782469072771938344985092784825473562749524909763"),stof<T>("2.078533442339936977156719992571484005950635767989453659081975135")}, std::complex<T>{stof<T>("1.1267222078670382437443633273720852016511522812744344454673585005"),stof<T>("2.0332855638317073435153816395082973838495214591324097321920347361")}, std::complex<T>{stof<T>("-0.0620301157883849391947034340858736621477752766585777805754366075"),stof<T>("1.6123378821367580291422256596178032175735468016471634226558303079")}, std::complex<T>{stof<T>("0.65014392619570208351699626144945763353564230515482995745745358731"),stof<T>("-0.79808681113405645396023651105679316073000096384394056216966926958")}, std::complex<T>{stof<T>("12.084961570462253944752406613069089833354754075772980308266544251"),stof<T>("-24.581804343014719624843362982247133355199192594612384159255435478")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[123].real()/kbase.W[123].real()), rlog(k.W[128].real()/kbase.W[128].real()), C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}, rlog(k.W[194].real()/kbase.W[194].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_138_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_138_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-42.890624163918968045148916689928925894404543873940612323993144384"),stof<T>("-9.990433145954740370336546595559296574844265280949463489817276881")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,56> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W21(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_9_im(k), f_2_12_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real()), rlog(kend.W[128].real()/k.W[128].real()), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), rlog(kend.W[194].real()/k.W[194].real()), f_2_2_re(k), f_2_9_re(k), f_2_12_re(k)};

                    
            return f_4_138_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_138_DLogXconstant_part(base_point<T>, kend);
	value += f_4_138_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_138_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_138_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_138_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_138_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_138_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_138_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
