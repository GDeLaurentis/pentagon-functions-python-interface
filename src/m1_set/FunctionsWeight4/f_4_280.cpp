/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_280.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_280_abbreviated (const std::array<T,29>& abb) {
T z[44];
z[0] = 2 * abb[21];
z[1] = abb[22] * (T(3) / T(4));
z[2] = abb[18] + -abb[20];
z[3] = abb[23] + (T(-1) / T(4)) * z[2];
z[4] = abb[19] * (T(1) / T(2)) + z[0] + -z[1] + -z[3];
z[4] = abb[0] * z[4];
z[5] = -abb[18] + abb[20] + abb[23];
z[6] = abb[19] * (T(3) / T(2));
z[7] = abb[22] * (T(1) / T(2)) + -z[6];
z[8] = abb[21] * (T(3) / T(2));
z[9] = -z[5] + z[7] + z[8];
z[10] = abb[6] * z[9];
z[11] = abb[24] + abb[25] * (T(1) / T(2)) + abb[26] * (T(1) / T(2));
z[12] = abb[7] * z[11];
z[13] = z[10] + z[12];
z[14] = abb[23] + (T(-5) / T(4)) * z[2];
z[15] = abb[22] * (T(5) / T(4));
z[16] = abb[21] * (T(7) / T(2)) + -z[14] + -z[15];
z[16] = abb[2] * z[16];
z[17] = abb[19] + -abb[22];
z[18] = abb[21] + z[17];
z[19] = abb[3] * z[18];
z[20] = abb[8] * z[11];
z[19] = z[19] + z[20];
z[19] = (T(1) / T(2)) * z[19];
z[21] = 2 * z[18];
z[22] = abb[1] * z[21];
z[23] = 2 * z[5];
z[24] = abb[19] + -5 * abb[21] + abb[22] + z[23];
z[24] = abb[5] * z[24];
z[4] = -z[4] + z[13] + z[16] + z[19] + z[22] + z[24];
z[4] = abb[13] * z[4];
z[16] = abb[21] * (T(1) / T(2));
z[7] = z[2] + z[7] + z[16];
z[7] = abb[3] * z[7];
z[22] = 2 * abb[19];
z[25] = abb[22] * (T(-7) / T(4)) + z[3] + z[16] + z[22];
z[25] = abb[2] * z[25];
z[15] = -z[3] + -z[6] + z[15];
z[15] = abb[0] * z[15];
z[26] = 3 * abb[22];
z[27] = 5 * abb[19] + -abb[21] + z[23] + -z[26];
z[28] = abb[1] * z[27];
z[15] = z[15] + z[28];
z[20] = (T(3) / T(2)) * z[20];
z[21] = abb[5] * z[21];
z[7] = z[7] + -z[13] + -z[15] + -z[20] + z[21] + -z[25];
z[13] = abb[11] * z[7];
z[25] = abb[21] + -abb[23];
z[28] = z[1] + (T(3) / T(4)) * z[2] + -z[6] + z[25];
z[28] = abb[0] * z[28];
z[29] = abb[22] * (T(3) / T(2));
z[5] = abb[19] * (T(-5) / T(2)) + -z[5] + z[16] + z[29];
z[30] = abb[5] * z[5];
z[16] = abb[19] + -z[1] + z[3] + -z[16];
z[16] = abb[2] * z[16];
z[31] = -abb[1] * z[5];
z[32] = -abb[19] + abb[21];
z[33] = -z[2] + -z[32];
z[33] = abb[3] * z[33];
z[16] = z[16] + z[20] + z[28] + z[30] + z[31] + (T(3) / T(2)) * z[33];
z[16] = abb[14] * z[16];
z[13] = z[4] + z[13] + z[16];
z[13] = abb[14] * z[13];
z[16] = -abb[22] + z[23];
z[20] = 3 * abb[19];
z[28] = 3 * abb[21];
z[31] = z[16] + z[20] + -z[28];
z[33] = abb[6] * z[31];
z[34] = abb[22] + z[2];
z[35] = -z[22] + z[34];
z[36] = abb[4] * z[35];
z[27] = abb[2] * z[27];
z[28] = 4 * abb[23] + -z[28];
z[37] = 11 * abb[19] + -6 * abb[22] + -5 * z[2] + z[28];
z[37] = abb[1] * z[37];
z[17] = abb[23] + z[17];
z[17] = abb[0] * z[17];
z[38] = 2 * z[17];
z[21] = -z[21] + z[27] + -z[33] + z[36] + z[37] + -z[38];
z[21] = abb[12] * z[21];
z[27] = abb[2] * z[9];
z[37] = 2 * abb[23];
z[39] = -z[8] + z[37];
z[40] = 2 * abb[22] + abb[19] * (T(-7) / T(2)) + (T(3) / T(2)) * z[2] + -z[39];
z[40] = abb[1] * z[40];
z[34] = -abb[19] + (T(1) / T(2)) * z[34];
z[41] = abb[4] * z[34];
z[17] = -z[10] + z[17] + z[27] + z[40] + z[41];
z[17] = abb[11] * z[17];
z[17] = z[17] + z[21];
z[17] = abb[11] * z[17];
z[1] = -z[1] + -z[8] + z[14] + z[22];
z[1] = abb[2] * z[1];
z[1] = z[1] + z[10] + -z[12] + z[15] + -z[19];
z[1] = abb[11] * z[1];
z[10] = abb[2] * z[18];
z[14] = abb[0] * z[18];
z[15] = z[10] + -z[14];
z[19] = 2 * z[15] + z[24];
z[21] = 7 * abb[19];
z[22] = -abb[21] + 5 * abb[22] + -z[21] + -z[23];
z[23] = abb[1] * z[22];
z[27] = z[19] + -z[23] + -z[33];
z[40] = -abb[12] * z[27];
z[34] = -abb[0] * z[34];
z[42] = -abb[1] * z[18];
z[43] = -abb[22] + z[2];
z[43] = abb[21] + (T(1) / T(2)) * z[43];
z[43] = abb[3] * z[43];
z[12] = z[12] + z[34] + z[42] + z[43];
z[12] = abb[13] * z[12];
z[1] = z[1] + z[12] + z[40];
z[1] = abb[13] * z[1];
z[6] = -abb[22] + (T(-1) / T(2)) * z[2] + z[6] + z[39];
z[6] = abb[7] * z[6];
z[9] = abb[9] * z[9];
z[12] = abb[3] + -abb[10];
z[34] = 2 * abb[24] + abb[25] + abb[26];
z[12] = z[12] * z[34];
z[3] = abb[22] * (T(1) / T(4)) + z[3] + -z[8];
z[3] = abb[8] * z[3];
z[8] = abb[0] * (T(1) / T(2)) + abb[2] * (T(3) / T(2));
z[8] = z[8] * z[11];
z[3] = z[3] + z[6] + z[8] + -z[9] + z[12];
z[6] = abb[28] * z[3];
z[5] = abb[2] * z[5];
z[0] = -4 * abb[19] + -abb[23] + z[0] + (T(5) / T(2)) * z[2] + z[29];
z[0] = abb[1] * z[0];
z[8] = -abb[0] * z[25];
z[0] = z[0] + z[5] + z[8] + -z[30] + -3 * z[41];
z[0] = prod_pow(abb[12], 2) * z[0];
z[5] = z[23] + -z[24];
z[8] = (T(1) / T(3)) * z[16] + -z[32];
z[8] = abb[6] * z[8];
z[5] = (T(1) / T(3)) * z[5] + z[8] + (T(-2) / T(3)) * z[15];
z[5] = prod_pow(m1_set::bc<T>[0], 2) * z[5];
z[8] = -abb[2] * z[31];
z[8] = z[8] + z[36] + z[38];
z[8] = abb[15] * z[8];
z[9] = abb[15] + abb[27];
z[9] = z[9] * z[33];
z[2] = 4 * abb[22] + 3 * z[2] + -z[21] + -z[28];
z[2] = abb[15] * z[2];
z[11] = abb[27] * z[22];
z[2] = z[2] + z[11];
z[2] = abb[1] * z[2];
z[11] = -abb[27] * z[19];
z[0] = z[0] + z[1] + z[2] + z[5] + z[6] + z[8] + z[9] + z[11] + z[13] + z[17];
z[1] = -abb[14] * z[7];
z[2] = -abb[5] * z[31];
z[5] = -abb[21] + -z[20] + z[26] + -z[37];
z[5] = abb[1] * z[5];
z[6] = z[14] + z[36];
z[2] = z[2] + z[5] + 2 * z[6] + z[33];
z[2] = abb[12] * z[2];
z[5] = abb[1] * z[35];
z[6] = abb[5] * z[18];
z[5] = z[5] + z[6] + -z[10] + -z[36];
z[5] = abb[11] * z[5];
z[1] = z[1] + z[2] + z[4] + 2 * z[5];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[17] * z[3];
z[3] = -abb[16] * z[27];
z[1] = z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_280_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-3.4196200539686499290855411171457372292124491044235190751052025697"),stof<T>("0.5962110675485313984640198168892890990631591794626600213856577033")}, std::complex<T>{stof<T>("2.8439121391173544631584656661632247000508687230543569634638209414"),stof<T>("-7.4977925536325702398108340422123768399784456559844023132278085238")}, std::complex<T>{stof<T>("3.4196200539686499290855411171457372292124491044235190751052025697"),stof<T>("-0.5962110675485313984640198168892890990631591794626600213856577033")}, std::complex<T>{stof<T>("-24.304981669340233451367398615815031096773874893103117716340620236"),stof<T>("-14.34088325755864559999643683914407833832648205559921145810565621")}, std::complex<T>{stof<T>("0.5757079148512954659270754509825125291615803813691621116413816283"),stof<T>("6.9015814860840388413468142253230877409152864765217422918421508204")}, std::complex<T>{stof<T>("20.309653700520288056354782047686781338399845407310436529594036038"),stof<T>("8.035512839023138157113642430710279696474354758540129187649163093")}, std::complex<T>{stof<T>("-1.7593518334562059155443784923489128405387359026509918272931048777"),stof<T>("1.2838897552460855887140042025642743542767977026226544070286342799")}, std::complex<T>{stof<T>("-0.87967591672810295777218924617445642026936795132549591364655243886"),stof<T>("0.64194487762304279435700210128213717713839885131132720351431713993")}, std::complex<T>{stof<T>("-0.87967591672810295777218924617445642026936795132549591364655243886"),stof<T>("0.64194487762304279435700210128213717713839885131132720351431713993")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[23].real()/kbase.W[23].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[54].real()/kbase.W[54].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[124].real()/kbase.W[124].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_280_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_280_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("16.698764443474069206160665201944608905560321237595181428785895705"),stof<T>("-25.391305441754378670488762034206475720902068452424866888124944241")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dl[1], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W12(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlr[2], f_1_1(k), f_1_3(k), f_1_8(k), f_1_9(k), f_2_3(k), f_2_9_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[23].real()/k.W[23].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[54].real()/k.W[54].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[124].real()/k.W[124].real()), f_2_9_re(k), f_2_25_re(k)};

                    
            return f_4_280_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_280_DLogXconstant_part(base_point<T>, kend);
	value += f_4_280_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_280_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_280_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_280_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_280_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_280_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_280_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
