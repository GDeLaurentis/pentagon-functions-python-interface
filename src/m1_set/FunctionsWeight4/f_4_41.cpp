/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_41.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_41_abbreviated (const std::array<T,26>& abb) {
T z[34];
z[0] = abb[2] * (T(1) / T(2));
z[1] = abb[19] + -abb[21];
z[2] = abb[18] + abb[20] + z[1];
z[3] = z[0] * z[2];
z[1] = abb[18] + (T(1) / T(2)) * z[1];
z[1] = abb[1] * z[1];
z[4] = abb[23] + abb[24] + abb[25];
z[5] = abb[7] * z[4];
z[6] = (T(1) / T(2)) * z[5];
z[7] = abb[21] + abb[22];
z[8] = abb[19] + abb[18] * (T(3) / T(2)) + (T(-1) / T(2)) * z[7];
z[9] = abb[6] * z[8];
z[10] = abb[19] + -abb[22];
z[11] = abb[18] + z[10];
z[12] = abb[4] * z[11];
z[13] = abb[8] * z[4];
z[10] = -abb[20] + z[10];
z[14] = abb[18] + (T(1) / T(2)) * z[10];
z[15] = abb[3] * z[14];
z[3] = z[1] + z[3] + z[6] + -z[9] + (T(3) / T(2)) * z[12] + (T(1) / T(2)) * z[13] + z[15];
z[3] = abb[11] * z[3];
z[15] = 3 * abb[19];
z[16] = 4 * abb[18];
z[17] = z[15] + z[16];
z[18] = 2 * abb[22];
z[19] = abb[21] + -z[17] + z[18];
z[19] = abb[1] * z[19];
z[7] = 3 * abb[18] + 2 * abb[19] + -z[7];
z[20] = abb[2] * z[7];
z[21] = abb[6] * z[7];
z[19] = -z[12] + z[19] + -z[20] + z[21];
z[22] = abb[12] * z[19];
z[3] = z[3] + z[22];
z[3] = abb[11] * z[3];
z[22] = abb[20] * (T(1) / T(2));
z[23] = -abb[21] + abb[22] * (T(-3) / T(2)) + abb[19] * (T(5) / T(2)) + -z[22];
z[24] = 2 * abb[18];
z[23] = (T(1) / T(2)) * z[23] + z[24];
z[23] = abb[2] * z[23];
z[25] = abb[3] * (T(1) / T(2));
z[26] = z[2] * z[25];
z[27] = (T(1) / T(4)) * z[13] + z[26];
z[28] = abb[1] * z[7];
z[23] = -z[9] + z[23] + -z[27] + z[28];
z[29] = abb[11] + abb[14];
z[30] = z[23] * z[29];
z[31] = 2 * abb[2];
z[32] = z[7] * z[31];
z[7] = abb[5] * z[7];
z[21] = -z[7] + -z[21] + 3 * z[28] + z[32];
z[32] = abb[12] * z[21];
z[9] = z[7] + z[9];
z[33] = 3 * abb[21] + abb[19] * (T(-11) / T(2)) + abb[22] * (T(5) / T(2)) + -z[22];
z[16] = -z[16] + (T(1) / T(2)) * z[33];
z[16] = abb[2] * z[16];
z[16] = z[9] + z[16] + -z[27] + -2 * z[28];
z[16] = abb[13] * z[16];
z[16] = z[16] + z[30] + -z[32];
z[16] = abb[14] * z[16];
z[23] = -abb[11] * z[23];
z[27] = abb[22] * (T(1) / T(2));
z[22] = abb[21] + abb[19] * (T(-3) / T(2)) + -z[22] + -z[24] + z[27];
z[22] = abb[3] * z[22];
z[30] = abb[18] + -abb[20];
z[31] = z[30] * z[31];
z[31] = z[28] + z[31];
z[6] = -z[6] + z[22] + z[31];
z[6] = abb[13] * z[6];
z[6] = z[6] + z[23] + z[32];
z[6] = abb[13] * z[6];
z[21] = abb[16] * z[21];
z[18] = 7 * abb[18] + abb[21] * (T(-5) / T(2)) + abb[19] * (T(9) / T(2)) + -z[18];
z[18] = abb[1] * z[18];
z[22] = abb[2] * z[8];
z[9] = -z[9] + (T(-1) / T(2)) * z[12] + z[18] + 3 * z[22];
z[9] = prod_pow(abb[12], 2) * z[9];
z[18] = -abb[15] * z[19];
z[19] = -abb[7] + -abb[9];
z[8] = z[8] * z[19];
z[19] = abb[10] + abb[2] * (T(-3) / T(4)) + -z[25];
z[19] = z[4] * z[19];
z[22] = abb[21] + abb[20] * (T(-3) / T(2)) + abb[19] * (T(-1) / T(2)) + -z[27];
z[22] = abb[8] * z[22];
z[8] = z[8] + z[19] + (T(1) / T(2)) * z[22];
z[8] = abb[17] * z[8];
z[0] = -z[0] * z[30];
z[0] = z[0] + -z[1] + z[26];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[1] = -abb[13] + abb[11] * (T(-1) / T(2));
z[1] = abb[13] * z[1];
z[19] = abb[13] + z[29];
z[19] = abb[14] * z[19];
z[1] = z[1] + (T(1) / T(2)) * z[19];
z[1] = z[1] * z[14];
z[4] = abb[17] * z[4];
z[1] = z[1] + (T(-3) / T(4)) * z[4];
z[1] = abb[0] * z[1];
z[0] = (T(13) / T(3)) * z[0] + z[1] + z[3] + z[6] + z[8] + z[9] + z[16] + z[18] + z[21];
z[1] = 2 * abb[21] + abb[22];
z[3] = -5 * abb[18] + z[1] + -z[15];
z[3] = abb[1] * z[3];
z[3] = z[3] + z[7] + z[12] + -z[20];
z[3] = abb[12] * z[3];
z[4] = z[10] + z[24];
z[6] = abb[2] + -abb[3];
z[6] = z[4] * z[6];
z[8] = abb[1] * z[11];
z[8] = z[8] + -z[12];
z[6] = -z[5] + z[6] + 2 * z[8] + -z[13];
z[6] = abb[11] * z[6];
z[1] = -abb[20] + z[1] + -z[17];
z[8] = -abb[3] * z[1];
z[5] = z[5] + z[8] + -2 * z[31];
z[5] = abb[13] * z[5];
z[2] = abb[3] * z[2];
z[2] = z[2] + -z[7] + z[28];
z[1] = -abb[2] * z[1];
z[1] = z[1] + 2 * z[2] + z[13];
z[1] = abb[14] * z[1];
z[2] = abb[13] + -abb[14];
z[2] = abb[0] * z[2] * z[4];
z[1] = z[1] + z[2] + 2 * z[3] + z[5] + z[6];
z[1] = m1_set::bc<T>[0] * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_41_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-7.59660876952188996493987335959357458113274970302704075684786845"),stof<T>("44.92315551738148996583379306223854899253445167343576227734341254")}, std::complex<T>{stof<T>("13.780539453339843080512717961018304846754565414675143341469468871"),stof<T>("7.160642496754507509799061141921817870640235490663369327732884559")}, std::complex<T>{stof<T>("34.30404789367057429314896268930121616894520955295101905065550335"),stof<T>("-32.525116712934588171580476021822783196100577822176404195642525877")}, std::complex<T>{stof<T>("-12.9268996708088412476963713686893367410578944352488349523381660289"),stof<T>("-5.2373963076923942844542558984939479257936383605959887539680021045")}, std::complex<T>{stof<T>("-0.8536397825310018328163465923289681056966709794263083891313028426"),stof<T>("-1.9232461890621132253448052434278699448465971300673805737648824541")}, std::complex<T>{stof<T>("-0.70725444150761250715684158174957616317924720334016741423053196"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("-0.70725444150761250715684158174957616317924720334016741423053196"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("-0.70725444150761250715684158174957616317924720334016741423053196"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[123].real()/kbase.W[123].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_41_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_41_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("7.9090437933738880439689323462917636086026218311815516063913715993"),stof<T>("-11.1646343728995867123394443091897460145050054726097157046847632798")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,26> abb = {dl[0], dl[1], dl[2], dl[4], dlog_W12(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_23(k), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real())};

                    
            return f_4_41_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_41_DLogXconstant_part(base_point<T>, kend);
	value += f_4_41_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_41_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_41_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_41_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_41_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_41_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_41_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
