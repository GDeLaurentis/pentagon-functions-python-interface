/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_271.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_271_abbreviated (const std::array<T,61>& abb) {
T z[124];
z[0] = abb[52] * (T(4) / T(3)) + abb[44] * (T(7) / T(6));
z[1] = abb[48] * (T(3) / T(2));
z[2] = abb[51] * (T(1) / T(2));
z[3] = abb[46] * (T(1) / T(3));
z[4] = -abb[50] + abb[43] * (T(11) / T(3)) + -z[3];
z[4] = abb[45] * (T(-2) / T(3)) + z[0] + -z[1] + -z[2] + (T(1) / T(2)) * z[4];
z[4] = abb[2] * z[4];
z[5] = abb[44] * (T(3) / T(4));
z[6] = abb[51] * (T(3) / T(4));
z[7] = z[5] + z[6];
z[8] = abb[42] + -abb[49];
z[9] = abb[48] * (T(1) / T(2));
z[10] = abb[46] + abb[50];
z[11] = abb[45] * (T(-23) / T(12)) + abb[47] * (T(5) / T(4)) + abb[43] * (T(7) / T(6)) + -z[7] + (T(-2) / T(3)) * z[8] + -z[9] + (T(3) / T(4)) * z[10];
z[11] = abb[4] * z[11];
z[12] = abb[47] * (T(1) / T(2));
z[13] = abb[44] * (T(1) / T(2));
z[14] = z[12] + z[13];
z[15] = z[2] + z[14];
z[16] = -abb[48] + z[15];
z[17] = abb[45] * (T(1) / T(2));
z[18] = -abb[43] + z[17];
z[19] = (T(1) / T(2)) * z[10];
z[20] = z[18] + z[19];
z[21] = z[16] + -z[20];
z[22] = abb[12] * z[21];
z[23] = abb[53] + abb[54] + -abb[55] + -abb[56];
z[24] = (T(1) / T(2)) * z[23];
z[25] = abb[20] * z[24];
z[26] = abb[22] * z[24];
z[27] = z[25] + z[26];
z[28] = abb[24] * z[24];
z[29] = z[22] + z[27] + z[28];
z[30] = abb[46] + -abb[50];
z[31] = (T(1) / T(2)) * z[30];
z[32] = -z[2] + z[14] + z[31];
z[18] = -z[18] + z[32];
z[33] = abb[3] * z[18];
z[34] = z[8] + z[17];
z[32] = z[32] + z[34];
z[35] = abb[13] * z[32];
z[36] = abb[43] * (T(1) / T(2));
z[37] = z[13] + z[36];
z[38] = abb[47] + abb[51];
z[39] = abb[48] + z[38];
z[40] = 3 * abb[50];
z[41] = -5 * abb[52] + z[37] + (T(5) / T(2)) * z[39] + -z[40];
z[41] = abb[9] * z[41];
z[42] = (T(3) / T(2)) * z[10];
z[43] = abb[44] * (T(3) / T(2));
z[44] = z[42] + -z[43];
z[45] = abb[47] * (T(3) / T(2));
z[46] = abb[51] * (T(3) / T(2));
z[47] = z[45] + -z[46];
z[48] = -abb[43] + z[8];
z[49] = abb[45] * (T(-1) / T(6)) + -z[44] + -z[47] + (T(-5) / T(3)) * z[48];
z[50] = abb[7] * (T(1) / T(2));
z[49] = z[49] * z[50];
z[3] = abb[50] + -z[3];
z[3] = 5 * z[3] + (T(11) / T(3)) * z[8];
z[51] = abb[43] + abb[44];
z[52] = abb[48] * (T(9) / T(2));
z[3] = -abb[51] + abb[52] * (T(11) / T(3)) + (T(1) / T(2)) * z[3] + (T(-2) / T(3)) * z[51] + -z[52];
z[3] = abb[0] * z[3];
z[53] = abb[21] + abb[23];
z[53] = z[23] * z[53];
z[54] = abb[44] + abb[45];
z[55] = -abb[50] + z[54];
z[56] = z[8] + z[55];
z[57] = abb[1] * z[56];
z[58] = abb[50] * (T(1) / T(2));
z[0] = 2 * abb[47] + abb[45] * (T(-4) / T(3)) + abb[43] * (T(1) / T(6)) + abb[46] * (T(2) / T(3)) + -z[0] + z[58];
z[0] = abb[5] * z[0];
z[59] = 3 * abb[44];
z[60] = -3 * abb[45] + 5 * abb[47] + abb[51] + z[10] + -z[59];
z[61] = -abb[52] + (T(1) / T(4)) * z[60];
z[62] = abb[8] * z[61];
z[55] = -abb[47] + abb[48] + z[55];
z[55] = abb[10] * z[55];
z[63] = 3 * z[55];
z[64] = abb[46] + z[8];
z[65] = -abb[44] + -abb[45] + z[64];
z[65] = abb[47] + (T(1) / T(3)) * z[65];
z[65] = abb[52] * (T(-1) / T(3)) + (T(1) / T(2)) * z[65];
z[65] = abb[11] * z[65];
z[66] = -abb[26] * z[24];
z[0] = z[0] + z[3] + z[4] + z[11] + -z[29] + (T(-1) / T(2)) * z[33] + -z[35] + z[41] + z[49] + (T(-5) / T(4)) * z[53] + (T(7) / T(2)) * z[57] + -5 * z[62] + -z[63] + 11 * z[65] + z[66];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[3] = abb[45] + abb[46];
z[4] = abb[52] + z[3] + (T(3) / T(2)) * z[8] + z[13] + -z[36] + -z[46];
z[4] = abb[7] * z[4];
z[11] = 3 * abb[47];
z[36] = -z[11] + z[54];
z[41] = 3 * abb[51];
z[49] = z[10] + z[36] + -z[41];
z[62] = 2 * abb[52];
z[65] = (T(1) / T(2)) * z[49] + z[62];
z[66] = abb[15] * z[65];
z[67] = -z[57] + z[66];
z[68] = z[25] + -z[35];
z[68] = (T(3) / T(2)) * z[68];
z[69] = z[2] + z[12];
z[70] = -abb[50] + z[3];
z[71] = -abb[48] + -z[13] + z[69] + (T(-1) / T(2)) * z[70];
z[72] = abb[6] * z[71];
z[73] = (T(3) / T(2)) * z[72];
z[74] = (T(3) / T(2)) * z[22];
z[75] = abb[52] + (T(1) / T(4)) * z[49];
z[75] = abb[5] * z[75];
z[76] = 3 * abb[46];
z[77] = 3 * abb[48];
z[78] = -z[41] + z[76] + z[77];
z[79] = -abb[50] + z[51];
z[80] = z[78] + z[79];
z[81] = abb[0] * (T(1) / T(2));
z[80] = z[80] * z[81];
z[82] = 2 * abb[43];
z[83] = z[1] + -z[82];
z[84] = z[8] + z[10];
z[84] = -abb[44] + abb[45] + -abb[52] + z[83] + (T(1) / T(2)) * z[84];
z[84] = abb[4] * z[84];
z[85] = -abb[50] + z[39];
z[85] = -abb[52] + (T(1) / T(2)) * z[85];
z[86] = 3 * abb[9];
z[86] = z[85] * z[86];
z[87] = 2 * abb[44] + -z[10] + z[62];
z[88] = -z[77] + z[87];
z[89] = 3 * abb[43] + -abb[45] + z[88];
z[90] = abb[2] * z[89];
z[91] = (T(3) / T(4)) * z[23];
z[92] = abb[26] * z[91];
z[4] = z[4] + -z[67] + z[68] + -z[73] + z[74] + z[75] + z[80] + z[84] + -z[86] + -z[90] + -z[92];
z[4] = abb[28] * z[4];
z[75] = 5 * abb[50];
z[76] = z[75] + -z[76];
z[80] = abb[45] * (T(3) / T(2));
z[84] = abb[44] * (T(5) / T(2));
z[47] = -abb[43] + -3 * z[8] + -z[47] + (T(1) / T(2)) * z[76] + -z[80] + -z[84];
z[47] = z[47] * z[50];
z[76] = abb[45] * (T(3) / T(4));
z[6] = abb[47] * (T(3) / T(4)) + z[6];
z[93] = z[6] + z[76];
z[94] = z[8] + z[19];
z[95] = abb[44] * (T(-5) / T(4)) + -z[62] + z[83] + z[93] + (T(1) / T(2)) * z[94];
z[95] = abb[4] * z[95];
z[96] = z[25] + z[35];
z[97] = (T(3) / T(2)) * z[96];
z[98] = abb[22] + abb[24];
z[98] = z[91] * z[98];
z[74] = z[74] + z[98];
z[99] = z[74] + -z[90];
z[100] = abb[14] * z[65];
z[23] = (T(3) / T(2)) * z[23];
z[101] = abb[25] * z[23];
z[100] = z[100] + z[101];
z[78] = -z[78] + z[79];
z[78] = z[78] * z[81];
z[81] = 2 * z[57];
z[47] = z[47] + z[78] + -z[81] + z[92] + z[95] + z[97] + z[99] + z[100];
z[47] = abb[31] * z[47];
z[78] = z[39] + -z[51] + -z[62];
z[78] = abb[9] * z[78];
z[27] = -z[27] + z[78] + z[90];
z[95] = abb[7] * z[65];
z[102] = 4 * abb[52];
z[49] = z[49] + z[102];
z[49] = abb[15] * z[49];
z[95] = -z[49] + z[95];
z[103] = z[95] + z[100];
z[104] = 3 * z[22];
z[105] = abb[5] * z[65];
z[106] = abb[24] * z[23];
z[107] = z[105] + z[106];
z[108] = 2 * abb[4];
z[89] = z[89] * z[108];
z[27] = 3 * z[27] + z[89] + -z[103] + -z[104] + -z[107];
z[27] = abb[33] * z[27];
z[89] = -z[62] + z[77];
z[109] = 2 * abb[50];
z[110] = -abb[44] + -z[3] + -z[89] + z[109];
z[111] = 2 * abb[14];
z[110] = z[110] * z[111];
z[26] = z[26] + z[72];
z[111] = abb[50] + z[62];
z[39] = z[39] + -z[111];
z[39] = abb[9] * z[39];
z[112] = -z[26] + z[39];
z[95] = z[95] + z[107] + -z[110] + -3 * z[112];
z[107] = abb[4] * z[65];
z[113] = z[95] + z[107];
z[113] = abb[29] * z[113];
z[36] = z[36] + z[62] + -z[64];
z[36] = abb[11] * z[36];
z[64] = 2 * abb[46] + -z[41] + z[111];
z[114] = z[8] + z[64];
z[114] = abb[0] * z[114];
z[115] = abb[26] * z[23];
z[114] = -z[36] + 4 * z[57] + z[114] + -z[115];
z[107] = -z[100] + z[107];
z[116] = 2 * abb[7];
z[56] = z[56] * z[116];
z[56] = -3 * z[35] + z[56] + z[105] + z[107] + z[114];
z[117] = -abb[32] * z[56];
z[4] = z[4] + z[27] + z[47] + z[113] + z[117];
z[4] = abb[28] * z[4];
z[47] = z[73] + z[98];
z[73] = (T(1) / T(2)) * z[60] + -z[62];
z[98] = abb[8] * z[73];
z[113] = 3 * z[98];
z[117] = (T(3) / T(2)) * z[53] + z[113];
z[118] = z[5] + z[6];
z[75] = abb[46] + z[75];
z[119] = abb[52] + (T(1) / T(4)) * z[75] + -z[118];
z[76] = z[8] + z[76] + -z[119];
z[76] = abb[7] * z[76];
z[120] = -z[8] + z[30] + z[89];
z[120] = abb[0] * z[120];
z[61] = 3 * z[61];
z[121] = abb[4] + abb[5];
z[122] = -z[61] * z[121];
z[63] = z[36] + z[47] + z[63] + z[67] + z[76] + -z[86] + -z[100] + z[117] + z[120] + z[122];
z[63] = abb[32] * z[63];
z[67] = -z[3] + z[40];
z[76] = 2 * abb[48];
z[86] = -z[62] + z[76];
z[15] = -z[15] + (T(1) / T(2)) * z[67] + -z[86];
z[15] = abb[14] * z[15];
z[67] = abb[25] * z[24];
z[15] = z[15] + z[67];
z[120] = abb[5] * z[73];
z[122] = (T(1) / T(2)) * z[53];
z[98] = z[15] + -z[28] + -z[98] + z[112] + z[120] + -z[122];
z[120] = abb[4] * z[73];
z[120] = z[98] + z[120];
z[120] = 3 * z[120];
z[123] = abb[29] * z[120];
z[56] = abb[31] * z[56];
z[56] = z[56] + z[63] + z[123];
z[56] = abb[32] * z[56];
z[16] = z[16] + -z[19] + z[34];
z[16] = abb[17] * z[16];
z[31] = z[31] + z[34];
z[31] = abb[19] * z[31];
z[34] = abb[48] + z[20];
z[34] = abb[18] * z[34];
z[63] = -abb[18] + abb[19];
z[63] = z[2] * z[63];
z[123] = abb[18] + abb[19];
z[14] = z[14] * z[123];
z[14] = -z[14] + -z[16] + -z[31] + z[34] + z[63];
z[16] = abb[60] * z[14];
z[31] = abb[58] * z[98];
z[34] = -abb[5] * z[18];
z[63] = -abb[51] + z[10];
z[98] = -abb[48] + z[51] + -z[63];
z[98] = abb[2] * z[98];
z[63] = abb[47] + -z[54] + z[63];
z[123] = z[50] * z[63];
z[29] = -z[29] + z[33] + z[34] + z[98] + z[123];
z[29] = abb[36] * z[29];
z[34] = abb[14] * z[71];
z[26] = z[26] + z[28] + -z[34] + z[67];
z[28] = abb[7] * z[32];
z[28] = -z[26] + z[28] + -z[35] + z[57];
z[28] = abb[34] * z[28];
z[18] = -abb[7] * z[18];
z[32] = abb[5] * z[63];
z[34] = -abb[46] + -abb[48] + abb[51];
z[63] = abb[2] * z[34];
z[18] = z[18] + z[26] + (T(1) / T(2)) * z[32] + z[33] + z[63];
z[18] = abb[35] * z[18];
z[16] = (T(1) / T(2)) * z[16] + z[18] + z[28] + z[29] + z[31];
z[17] = -z[17] + z[43] + z[82];
z[18] = z[46] + -z[62];
z[26] = z[17] + z[18] + z[45] + (T(-1) / T(2)) * z[75];
z[28] = abb[5] + abb[7];
z[26] = z[26] * z[28];
z[29] = -z[46] + z[84];
z[31] = 7 * abb[50];
z[3] = -5 * z[3] + z[31];
z[3] = -6 * abb[48] + (T(1) / T(2)) * z[3] + -z[29] + z[45] + z[62];
z[3] = abb[14] * z[3];
z[32] = 3 * z[72];
z[43] = -z[33] + z[39];
z[63] = abb[43] + -abb[45] + z[64];
z[63] = abb[2] * z[63];
z[64] = -abb[22] * z[23];
z[3] = z[3] + z[26] + -z[32] + 3 * z[43] + z[49] + z[63] + z[64] + -z[101] + -z[106];
z[3] = abb[29] * z[3];
z[9] = abb[52] + -z[9] + z[51] + -z[58] + -z[69];
z[9] = abb[9] * z[9];
z[26] = z[10] + z[38];
z[38] = abb[45] * (T(1) / T(4));
z[43] = -abb[43] + z[38];
z[5] = abb[48] + -abb[52] + -z[5] + (T(1) / T(4)) * z[26] + z[43];
z[5] = abb[4] * z[5];
z[5] = z[5] + z[9] + z[33];
z[9] = -z[77] + z[102];
z[26] = 2 * abb[45];
z[30] = -5 * abb[43] + -z[9] + z[26] + -z[30] + z[41] + -z[59];
z[30] = abb[2] * z[30];
z[47] = z[47] + -z[66];
z[43] = z[43] + z[119];
z[28] = z[28] * z[43];
z[5] = 3 * z[5] + z[28] + z[30] + z[47] + z[100];
z[5] = abb[33] * z[5];
z[3] = z[3] + z[5];
z[3] = abb[33] * z[3];
z[5] = 2 * z[90];
z[28] = z[5] + -z[74] + 3 * z[78];
z[1] = -z[1] + -z[18] + z[37] + z[94];
z[1] = abb[0] * z[1];
z[30] = -z[36] + z[81];
z[1] = z[1] + z[30] + -z[92];
z[37] = z[49] + -z[100];
z[58] = z[45] + z[80];
z[59] = (T(5) / T(2)) * z[10];
z[64] = abb[51] * (T(9) / T(2)) + z[13] + z[48] + z[58] + -z[59];
z[64] = -z[62] + (T(1) / T(2)) * z[64];
z[64] = abb[7] * z[64];
z[66] = 4 * abb[43];
z[52] = -z[52] + z[66];
z[59] = z[8] + -z[59];
z[59] = abb[44] * (T(13) / T(4)) + z[52] + (T(1) / T(2)) * z[59] + -z[93] + z[102];
z[59] = abb[4] * z[59];
z[59] = z[1] + z[28] + z[37] + z[59] + z[64] + -z[97];
z[59] = abb[28] * z[59];
z[45] = abb[45] * (T(5) / T(2)) + -z[45];
z[44] = z[44] + -z[45] + -z[46] + -z[48];
z[44] = z[44] * z[50];
z[1] = -z[1] + z[44];
z[42] = z[8] + z[42];
z[42] = (T(1) / T(2)) * z[42];
z[44] = abb[45] * (T(5) / T(4)) + z[42] + z[83] + -z[118];
z[44] = abb[4] * z[44];
z[44] = -z[1] + z[44] + z[68] + z[99] + z[105];
z[44] = abb[31] * z[44];
z[11] = -z[11] + z[26] + z[87];
z[26] = z[11] * z[108];
z[46] = 2 * abb[5];
z[50] = z[11] * z[46];
z[26] = z[26] + z[50] + -z[103];
z[64] = z[26] + z[117];
z[67] = abb[29] * z[64];
z[27] = z[27] + z[44] + -z[67];
z[11] = abb[5] * z[11];
z[44] = z[8] + z[82] + z[88];
z[44] = abb[0] * z[44];
z[67] = -abb[7] + z[108];
z[48] = abb[45] + z[48];
z[67] = z[48] * z[67];
z[11] = z[11] + -z[36] + z[44] + -z[57] + z[67] + -z[90];
z[11] = abb[30] * z[11];
z[36] = -abb[32] * z[64];
z[11] = z[11] + -z[27] + z[36] + z[59];
z[11] = abb[30] * z[11];
z[31] = abb[46] + -z[31];
z[6] = abb[52] + abb[44] * (T(9) / T(4)) + -z[6] + (T(1) / T(4)) * z[31] + z[38] + z[82];
z[6] = abb[5] * z[6];
z[31] = -abb[9] * z[85];
z[31] = z[31] + -z[55];
z[36] = abb[43] + -z[70] + -z[89];
z[36] = abb[2] * z[36];
z[38] = abb[7] * z[43];
z[43] = -abb[4] * z[61];
z[6] = z[6] + 3 * z[31] + z[36] + z[38] + z[43] + z[47] + -z[110];
z[6] = prod_pow(abb[29], 2) * z[6];
z[31] = -abb[46] + z[40];
z[18] = 2 * z[8] + -z[18] + (T(-1) / T(2)) * z[31] + z[45] + z[84];
z[18] = abb[7] * z[18];
z[31] = z[35] + z[122];
z[36] = -9 * abb[47] + -5 * z[10] + z[41] + 7 * z[54];
z[36] = (T(1) / T(2)) * z[36] + z[62];
z[36] = z[36] * z[121];
z[18] = z[18] + -3 * z[31] + -z[36] + -z[49] + -z[113] + z[114];
z[31] = abb[59] * z[18];
z[17] = -z[17] + z[19] + z[69] + z[86];
z[17] = abb[4] * z[17];
z[19] = abb[9] * z[79];
z[15] = z[15] + z[17] + z[19] + z[22] + z[25] + -z[72] + -z[90];
z[15] = 3 * z[15];
z[17] = -abb[57] * z[15];
z[19] = abb[37] * z[95];
z[2] = z[2] + -z[8] + -z[12] + z[13] + -z[20];
z[2] = abb[16] * z[2];
z[12] = -abb[27] * z[91];
z[12] = (T(3) / T(2)) * z[2] + z[12];
z[12] = abb[60] * z[12];
z[13] = z[46] + z[116];
z[13] = z[13] * z[79];
z[13] = -z[13] + 3 * z[33] + -z[63] + -z[107];
z[20] = abb[29] + -abb[33];
z[13] = z[13] * z[20];
z[20] = -abb[0] + abb[2] + -abb[5];
z[20] = z[20] * z[79];
z[22] = -abb[4] * z[48];
z[20] = z[20] + z[22] + z[57];
z[20] = abb[31] * z[20];
z[13] = z[13] + z[20];
z[13] = abb[31] * z[13];
z[20] = abb[36] * z[21];
z[21] = abb[58] * z[73];
z[20] = z[20] + z[21];
z[21] = abb[37] * z[65];
z[20] = 3 * z[20] + z[21];
z[20] = abb[4] * z[20];
z[21] = 3 * abb[0];
z[22] = -z[21] * z[34];
z[22] = z[22] + -z[115];
z[22] = abb[34] * z[22];
z[0] = z[0] + z[3] + z[4] + z[6] + z[11] + z[12] + z[13] + 3 * z[16] + z[17] + z[19] + z[20] + z[22] + z[31] + z[56];
z[3] = z[9] + z[29] + -z[58] + z[66] + -z[94];
z[3] = abb[4] * z[3];
z[4] = -z[8] + z[51] + -z[62];
z[6] = z[4] + -2 * z[10] + z[41];
z[6] = abb[7] * z[6];
z[4] = -abb[46] + -z[4] + -z[77] + z[109];
z[4] = abb[0] * z[4];
z[9] = -abb[20] * z[23];
z[3] = z[3] + z[4] + z[5] + z[6] + z[9] + z[30] + z[32] + z[37] + 3 * z[39] + -z[104];
z[3] = abb[28] * z[3];
z[4] = abb[47] * (T(-21) / T(4)) + abb[45] * (T(19) / T(4)) + z[7] + -z[42] + -z[52];
z[4] = abb[4] * z[4];
z[5] = z[53] + z[96];
z[1] = z[1] + z[4] + (T(3) / T(2)) * z[5] + -z[28] + z[50] + z[113];
z[1] = abb[30] * z[1];
z[4] = -z[60] + z[102];
z[4] = abb[8] * z[4];
z[4] = z[4] + z[30] + -z[35] + -z[53] + -2 * z[55] + z[112];
z[5] = -abb[51] + z[8] + -z[76] + z[111];
z[5] = z[5] * z[21];
z[4] = 3 * z[4] + z[5] + -z[26] + -z[106] + -z[115];
z[4] = abb[32] * z[4];
z[1] = z[1] + z[3] + z[4] + -z[27];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = abb[40] * z[18];
z[4] = -abb[38] * z[15];
z[5] = -abb[27] * z[24];
z[2] = z[2] + z[5] + z[14];
z[2] = abb[41] * z[2];
z[5] = abb[39] * z[120];
z[1] = z[1] + (T(3) / T(2)) * z[2] + z[3] + z[4] + z[5];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_271_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("18.637792491674668814750023818104117471071713161681467043320545904"),stof<T>("-60.341860181793144151011350264853477041651685238276703111950883009")}, std::complex<T>{stof<T>("-15.254538031344221882234898815395995301210979956066736334501994495"),stof<T>("4.964525490228577077160159552622320638799579908595529274791218172")}, std::complex<T>{stof<T>("8.4124856135380201588587852463544668943566313660778329186119688761"),stof<T>("-6.2054549665462334704876011592708855319908336813078397410419910577")}, std::complex<T>{stof<T>("23.667023644882242041093684061750462195567611322144569253113963371"),stof<T>("-11.16998045677481054764776071189320617079041358990336901583320923")}, std::complex<T>{stof<T>("1.9230263948588290453996676827664011057810529065392366644334176496"),stof<T>("9.307535070333749016582503752162411395015383528204727111045930336")}, std::complex<T>{stof<T>("-5.531856137578063120391541743576013059963026160707794451212596247"),stof<T>("-33.915119523662488760840652524024513749197737097890297224342025657")}, std::complex<T>{stof<T>("6.449632563695912377695446426483077494809823066757646632807656336"),stof<T>("73.736174996707927462469030334058439387540189727061098078939252239")}, std::complex<T>{stof<T>("-18.637792491674668814750023818104117471071713161681467043320545904"),stof<T>("60.341860181793144151011350264853477041651685238276703111950883009")}, std::complex<T>{stof<T>("-7.9098606291675302648109037464247985588895033658331406771927900959"),stof<T>("-9.0513052348096113720353358696648715896727008691751971307336570652")}, std::complex<T>{stof<T>("-2.4256513792293189394475491826960694412481809067839289058525964298"),stof<T>("5.949225131022095825940433276773345726648151022278309760729717787")}, std::complex<T>{stof<T>("1.00524996874097978809576299985933667093425600048938448283835756"),stof<T>("-30.513520402711689685045874057871514243327069100966073743551296246")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[82].real()/kbase.W[82].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_271_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_271_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-47.823770164394209598422715464646930589120511918748693474957142454"),stof<T>("32.905444792205210045668180505886577453864605846940315980172814194")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,61> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W23(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W83(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_15(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[82].real()/k.W[82].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_10_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_271_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_271_DLogXconstant_part(base_point<T>, kend);
	value += f_4_271_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_271_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_271_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_271_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_271_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_271_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_271_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
