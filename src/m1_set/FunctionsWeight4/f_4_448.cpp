/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_448.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_448_abbreviated (const std::array<T,61>& abb) {
T z[72];
z[0] = abb[41] * (T(1) / T(2));
z[1] = abb[31] + abb[33];
z[2] = -abb[30] + z[1];
z[3] = -abb[32] + z[2];
z[3] = m1_set::bc<T>[0] * z[3];
z[4] = abb[42] * (T(-1) / T(2)) + -z[0] + z[3];
z[4] = abb[8] * z[4];
z[5] = abb[3] * (T(1) / T(2));
z[6] = -abb[42] + z[3];
z[7] = -z[5] * z[6];
z[8] = -abb[41] + z[3];
z[9] = -abb[42] + z[8];
z[10] = abb[17] * (T(1) / T(2));
z[11] = -z[9] * z[10];
z[12] = abb[0] * z[8];
z[12] = (T(1) / T(2)) * z[12];
z[3] = abb[2] * z[3];
z[13] = abb[13] * abb[42];
z[14] = -abb[10] * abb[41];
z[4] = z[3] + z[4] + z[7] + z[11] + -z[12] + -z[13] + z[14];
z[7] = abb[55] * (T(1) / T(16));
z[4] = z[4] * z[7];
z[11] = abb[33] * (T(1) / T(2));
z[14] = abb[34] + z[11];
z[15] = abb[35] * (T(1) / T(2));
z[16] = z[14] + -z[15];
z[17] = abb[31] * (T(1) / T(2));
z[18] = z[16] + z[17];
z[19] = abb[32] * (T(3) / T(2)) + -z[18];
z[19] = m1_set::bc<T>[0] * z[19];
z[20] = abb[42] + abb[43];
z[19] = z[19] + (T(1) / T(2)) * z[20];
z[19] = abb[4] * z[19];
z[20] = abb[30] * (T(1) / T(2));
z[21] = z[11] + z[20];
z[22] = -abb[34] + abb[35];
z[23] = -abb[32] + z[21] + -z[22];
z[23] = m1_set::bc<T>[0] * z[23];
z[23] = -abb[42] + z[23];
z[23] = abb[3] * z[23];
z[24] = abb[6] * (T(1) / T(2));
z[25] = abb[32] + -abb[34];
z[25] = m1_set::bc<T>[0] * z[25];
z[26] = abb[42] + z[25];
z[27] = z[24] * z[26];
z[28] = -abb[30] + z[22];
z[29] = abb[32] + z[28];
z[29] = m1_set::bc<T>[0] * z[29];
z[30] = abb[43] + z[29];
z[31] = abb[8] * (T(1) / T(2));
z[32] = z[30] * z[31];
z[33] = -abb[33] + abb[35];
z[34] = m1_set::bc<T>[0] * z[33];
z[35] = abb[43] + z[34];
z[35] = abb[5] * z[35];
z[19] = z[13] + z[19] + z[23] + -z[27] + z[32] + (T(-1) / T(2)) * z[35];
z[23] = abb[0] * (T(1) / T(4));
z[8] = z[8] * z[23];
z[27] = abb[30] + -abb[35];
z[32] = m1_set::bc<T>[0] * z[27];
z[35] = abb[41] + -abb[43];
z[36] = z[32] + z[35];
z[37] = abb[7] * (T(1) / T(4));
z[38] = z[36] * z[37];
z[8] = -z[8] + (T(1) / T(2)) * z[19] + z[38];
z[19] = abb[33] + abb[34];
z[38] = abb[32] * (T(1) / T(2));
z[39] = -abb[35] + (T(1) / T(2)) * z[19] + z[20] + -z[38];
z[39] = m1_set::bc<T>[0] * z[39];
z[39] = abb[43] * (T(-1) / T(2)) + z[39];
z[39] = abb[1] * z[39];
z[32] = (T(1) / T(2)) * z[32];
z[40] = -abb[43] + z[0] + z[32];
z[40] = abb[14] * z[40];
z[0] = abb[10] * z[0];
z[0] = z[0] + -z[39] + z[40];
z[28] = abb[31] + z[28];
z[40] = m1_set::bc<T>[0] * z[28];
z[41] = z[35] + z[40];
z[42] = abb[16] * (T(1) / T(4));
z[41] = z[41] * z[42];
z[41] = -z[0] + z[41];
z[43] = z[8] + z[41];
z[44] = abb[11] * (T(1) / T(16));
z[45] = z[34] * z[44];
z[43] = (T(1) / T(8)) * z[43] + z[45];
z[43] = abb[52] * z[43];
z[45] = abb[11] * (T(1) / T(2));
z[34] = z[34] * z[45];
z[8] = z[8] + z[34];
z[34] = -z[35] + z[40];
z[46] = z[34] * z[42];
z[32] = abb[9] * z[32];
z[39] = z[8] + -z[32] + z[39] + z[46];
z[46] = abb[53] * (T(1) / T(8));
z[39] = z[39] * z[46];
z[8] = z[8] + z[41];
z[41] = abb[50] * (T(1) / T(8));
z[8] = z[8] * z[41];
z[47] = abb[45] + abb[49];
z[48] = -abb[48] + z[47];
z[49] = abb[31] * z[48];
z[50] = abb[33] * z[48];
z[51] = z[49] + z[50];
z[52] = abb[30] * z[48];
z[53] = abb[32] * z[48];
z[54] = -z[51] + z[52] + z[53];
z[54] = m1_set::bc<T>[0] * z[54];
z[55] = -abb[46] + z[48];
z[56] = abb[41] * z[55];
z[57] = -abb[42] * z[48];
z[58] = abb[46] * z[6];
z[59] = abb[47] * z[9];
z[54] = z[54] + z[56] + -z[57] + z[58] + z[59];
z[54] = abb[27] * z[54];
z[58] = abb[43] * z[48];
z[56] = -z[56] + z[58];
z[58] = -abb[34] * z[48];
z[59] = abb[35] * z[48];
z[60] = z[58] + z[59];
z[52] = z[52] + -z[60];
z[61] = -z[49] + z[52];
z[62] = m1_set::bc<T>[0] * z[61];
z[34] = abb[47] * z[34];
z[63] = abb[43] + z[40];
z[63] = abb[46] * z[63];
z[34] = z[34] + -z[56] + z[62] + z[63];
z[34] = abb[26] * z[34];
z[62] = abb[41] + -abb[42];
z[62] = abb[18] * z[62];
z[9] = abb[21] * z[9];
z[6] = abb[19] * z[6];
z[6] = z[6] + z[9] + z[62];
z[6] = abb[56] * z[6];
z[6] = z[6] + z[34] + z[54];
z[9] = abb[42] + z[29] + -z[35];
z[9] = abb[47] * z[9];
z[29] = z[52] + -z[53];
z[29] = m1_set::bc<T>[0] * z[29];
z[30] = abb[42] + z[30];
z[30] = abb[46] * z[30];
z[9] = z[9] + z[29] + z[30] + -z[56] + z[57];
z[29] = abb[22] + abb[24];
z[29] = (T(1) / T(32)) * z[29];
z[9] = z[9] * z[29];
z[30] = abb[3] + -abb[8];
z[34] = -z[30] * z[40];
z[35] = -abb[33] + z[22];
z[40] = abb[32] + z[35];
z[40] = abb[4] * m1_set::bc<T>[0] * z[40];
z[3] = z[3] + z[34] + z[40];
z[34] = abb[7] * (T(1) / T(2));
z[36] = z[34] * z[36];
z[0] = -z[0] + (T(1) / T(2)) * z[3] + -z[12] + -z[32] + z[36];
z[3] = abb[54] * (T(1) / T(8));
z[0] = z[0] * z[3];
z[12] = z[53] + z[58];
z[12] = m1_set::bc<T>[0] * z[12];
z[32] = abb[46] + abb[47];
z[36] = z[26] * z[32];
z[12] = z[12] + -z[36] + -z[57];
z[36] = abb[23] * (T(1) / T(32));
z[40] = abb[25] * (T(-1) / T(32)) + -z[36];
z[12] = z[12] * z[40];
z[25] = abb[4] * z[25];
z[26] = -abb[3] * z[26];
z[13] = z[13] + z[25] + z[26];
z[13] = abb[51] * z[13];
z[0] = abb[60] + z[0] + z[4] + (T(1) / T(32)) * z[6] + z[8] + z[9] + z[12] + (T(1) / T(16)) * z[13] + z[39] + z[43];
z[4] = abb[33] * abb[34];
z[6] = abb[32] * z[28];
z[4] = z[4] + z[6];
z[6] = -abb[35] + z[1];
z[6] = abb[30] * z[6];
z[8] = -abb[34] + z[1];
z[8] = abb[31] * z[8];
z[9] = abb[35] * z[35];
z[6] = z[4] + z[6] + -z[8] + z[9];
z[8] = abb[38] + abb[59];
z[12] = z[6] + -z[8];
z[12] = abb[46] * z[12];
z[13] = -abb[36] + z[8];
z[25] = z[6] + -z[13];
z[25] = abb[47] * z[25];
z[26] = z[50] + -z[60];
z[26] = abb[35] * z[26];
z[28] = abb[59] * z[47];
z[39] = -abb[47] + z[55];
z[40] = abb[57] * z[39];
z[26] = z[26] + z[28] + -z[40];
z[28] = z[51] + z[58];
z[28] = abb[31] * z[28];
z[43] = -z[51] + z[59];
z[43] = abb[30] * z[43];
z[51] = abb[32] * z[61];
z[52] = abb[36] * z[55];
z[53] = abb[38] * z[47];
z[8] = -abb[48] * z[8];
z[54] = abb[33] * z[58];
z[8] = z[8] + z[12] + z[25] + z[26] + z[28] + z[43] + z[51] + -z[52] + z[53] + z[54];
z[8] = abb[26] * z[8];
z[12] = z[11] * z[48];
z[25] = z[12] + -z[60];
z[28] = z[38] * z[48];
z[43] = z[20] * z[48];
z[51] = z[25] + -z[28] + z[43];
z[51] = abb[32] * z[51];
z[53] = prod_pow(abb[33], 2);
z[53] = (T(1) / T(2)) * z[53];
z[54] = z[48] * z[53];
z[55] = prod_pow(m1_set::bc<T>[0], 2);
z[56] = (T(1) / T(6)) * z[55];
z[57] = abb[58] + z[56];
z[60] = z[48] * z[57];
z[61] = abb[37] * z[39];
z[51] = z[51] + -z[54] + z[60] + -z[61];
z[54] = z[22] + z[38];
z[61] = -z[21] + z[54];
z[61] = abb[32] * z[61];
z[62] = -abb[34] + z[17];
z[62] = abb[31] * z[62];
z[62] = abb[38] + z[62];
z[63] = z[57] + z[62];
z[64] = -z[61] + z[63];
z[21] = -abb[35] + z[21];
z[65] = abb[30] * z[21];
z[66] = -abb[33] + z[15];
z[66] = abb[35] * z[66];
z[65] = z[53] + z[65] + z[66];
z[66] = -z[64] + z[65];
z[66] = abb[46] * z[66];
z[65] = abb[36] + z[65];
z[64] = -z[64] + z[65];
z[64] = abb[47] * z[64];
z[59] = -z[12] + -z[43] + z[59];
z[59] = abb[30] * z[59];
z[67] = z[17] * z[48];
z[68] = z[58] + z[67];
z[68] = abb[31] * z[68];
z[69] = abb[38] * z[48];
z[68] = z[68] + z[69];
z[69] = -z[15] * z[48];
z[69] = z[50] + z[69];
z[69] = abb[35] * z[69];
z[52] = z[51] + -z[52] + z[59] + z[64] + z[66] + z[68] + z[69];
z[52] = abb[25] * z[52];
z[59] = -abb[31] + z[20];
z[64] = -z[11] + z[59];
z[66] = z[38] + z[64];
z[66] = abb[32] * z[66];
z[64] = abb[30] * z[64];
z[17] = abb[33] + z[17];
z[17] = abb[31] * z[17];
z[64] = z[17] + z[64] + z[66];
z[66] = -z[57] + z[64];
z[69] = -z[32] * z[66];
z[12] = -z[12] + z[43] + -z[49];
z[43] = abb[30] * z[12];
z[12] = z[12] + z[28];
z[12] = abb[32] * z[12];
z[49] = z[50] + z[67];
z[49] = abb[31] * z[49];
z[12] = z[12] + -z[40] + z[43] + z[49] + -z[60] + z[69];
z[12] = abb[27] * z[12];
z[8] = z[8] + z[12] + z[52];
z[12] = -abb[57] + z[66];
z[40] = -abb[21] * z[12];
z[1] = z[1] + -z[20];
z[1] = abb[30] * z[1];
z[1] = z[1] + -z[17];
z[17] = z[2] + -z[38];
z[17] = abb[32] * z[17];
z[17] = -z[1] + -z[17] + z[53];
z[43] = z[17] + -z[57];
z[49] = -abb[19] * z[43];
z[50] = -abb[57] + abb[58];
z[50] = abb[18] * z[50];
z[52] = -abb[8] + -z[10];
z[52] = abb[40] * z[52];
z[40] = z[40] + z[49] + z[50] + z[52];
z[49] = abb[29] * abb[40];
z[40] = (T(1) / T(2)) * z[40] + z[49];
z[40] = abb[56] * z[40];
z[49] = -abb[34] + z[38];
z[49] = abb[32] * z[49];
z[50] = prod_pow(abb[34], 2);
z[52] = (T(1) / T(2)) * z[50];
z[66] = -z[49] + -z[52] + z[57];
z[66] = abb[3] * z[66];
z[67] = (T(1) / T(3)) * z[55];
z[69] = z[62] + z[67];
z[70] = z[49] + -z[69];
z[70] = abb[4] * z[70];
z[71] = -abb[58] + z[56];
z[71] = abb[13] * z[71];
z[66] = z[66] + z[70] + z[71];
z[66] = abb[51] * z[66];
z[8] = (T(1) / T(2)) * z[8] + z[40] + z[66];
z[40] = -z[17] + z[67];
z[40] = abb[2] * z[40];
z[17] = -abb[57] + z[17] + -z[56];
z[66] = abb[0] * z[17];
z[66] = (T(1) / T(2)) * z[66];
z[70] = abb[58] * (T(1) / T(2));
z[64] = -z[64] + z[67] + z[70];
z[64] = abb[8] * z[64];
z[5] = z[5] * z[43];
z[10] = z[10] * z[12];
z[12] = -abb[57] + z[56];
z[12] = abb[10] * z[12];
z[43] = abb[57] * z[31];
z[67] = abb[21] * abb[40];
z[5] = z[5] + z[10] + -z[12] + z[40] + z[43] + z[64] + z[66] + (T(1) / T(4)) * z[67] + -z[71];
z[5] = z[5] * z[7];
z[7] = abb[33] * z[19];
z[7] = z[7] + (T(3) / T(2)) * z[50] + z[62];
z[10] = z[14] + -z[20];
z[10] = z[10] * z[20];
z[43] = abb[30] * (T(1) / T(4)) + abb[33] * (T(3) / T(4)) + -z[54];
z[43] = abb[32] * z[43];
z[54] = -z[15] + z[19];
z[54] = abb[35] * z[54];
z[64] = abb[39] * (T(1) / T(2));
z[67] = abb[36] * (T(1) / T(2)) + -z[64];
z[7] = (T(1) / T(2)) * z[7] + z[10] + -z[43] + -z[54] + -z[57] + -z[67];
z[7] = abb[3] * z[7];
z[9] = z[9] + z[53];
z[10] = -z[22] + z[59];
z[10] = abb[30] * z[10];
z[43] = abb[31] * z[19];
z[53] = -abb[59] + z[52];
z[10] = -abb[38] + z[9] + z[10] + z[43] + z[53];
z[18] = abb[32] * (T(3) / T(4)) + -z[18];
z[18] = abb[32] * z[18];
z[10] = (T(1) / T(2)) * z[10] + z[18] + (T(-5) / T(12)) * z[55] + z[64] + -z[70];
z[10] = abb[4] * z[10];
z[18] = z[11] + -z[22];
z[18] = abb[30] * z[18];
z[59] = abb[39] + z[53];
z[9] = z[9] + z[18] + z[59] + z[61];
z[18] = z[9] + -z[56];
z[18] = z[18] * z[31];
z[21] = abb[32] * z[21];
z[21] = -abb[37] + z[21] + -z[65];
z[31] = abb[15] * (T(1) / T(2));
z[21] = z[21] * z[31];
z[31] = -abb[4] + -abb[5] + z[30];
z[56] = abb[37] * (T(1) / T(2));
z[31] = z[31] * z[56];
z[11] = -abb[34] + z[11];
z[11] = abb[33] * z[11];
z[56] = -abb[34] + z[15];
z[56] = abb[35] * z[56];
z[11] = abb[59] + z[11] + -z[56];
z[61] = abb[5] * (T(1) / T(2));
z[11] = z[11] * z[61];
z[49] = z[49] + -z[63];
z[24] = z[24] * z[49];
z[7] = z[7] + -z[10] + -z[11] + -z[18] + -z[21] + z[24] + z[31] + -z[71];
z[10] = -abb[34] + z[20];
z[11] = abb[30] * z[10];
z[11] = -abb[36] + abb[57] + -z[11] + z[56] + z[59] + z[62];
z[18] = z[11] * z[37];
z[17] = z[17] * z[23];
z[21] = abb[18] + abb[19] + -abb[20];
z[23] = abb[40] * (T(1) / T(8));
z[21] = z[21] * z[23];
z[7] = (T(1) / T(2)) * z[7] + -z[17] + z[18] + z[21];
z[2] = abb[35] + z[2];
z[2] = abb[30] * z[2];
z[17] = abb[35] * z[19];
z[13] = -abb[57] + z[13];
z[2] = -z[2] + -z[4] + -z[13] + z[17] + z[43] + -z[50];
z[2] = z[2] * z[42];
z[14] = abb[33] * z[14];
z[17] = z[14] + z[53];
z[18] = z[35] + z[38];
z[19] = z[18] * z[38];
z[16] = abb[35] * z[16];
z[10] = z[10] * z[20];
z[21] = (T(1) / T(12)) * z[55];
z[10] = -z[10] + -z[16] + (T(1) / T(2)) * z[17] + z[19] + -z[21] + -z[67];
z[10] = abb[1] * z[10];
z[15] = z[15] + -z[20];
z[15] = z[15] * z[22];
z[15] = -abb[59] + abb[57] * (T(1) / T(2)) + z[15] + z[21] + z[64];
z[15] = abb[14] * z[15];
z[12] = -z[10] + (T(-1) / T(2)) * z[12] + z[15];
z[2] = z[2] + -z[12];
z[15] = -z[2] + -z[7];
z[14] = z[14] + -z[54];
z[16] = abb[32] * z[33];
z[16] = z[14] + z[16];
z[17] = z[16] * z[44];
z[15] = (T(1) / T(8)) * z[15] + z[17];
z[15] = abb[52] * z[15];
z[16] = z[16] * z[45];
z[7] = z[7] + -z[16];
z[16] = -abb[34] + z[27];
z[16] = abb[30] * z[16];
z[17] = abb[34] * abb[35];
z[16] = -abb[39] + z[16] + z[17] + -z[52] + -z[62];
z[16] = abb[36] + (T(1) / T(2)) * z[16];
z[16] = abb[9] * z[16];
z[6] = z[6] + -z[13];
z[6] = z[6] * z[42];
z[6] = z[6] + -z[7] + -z[10] + -z[16];
z[6] = z[6] * z[46];
z[2] = -z[2] + -z[7];
z[2] = z[2] * z[41];
z[7] = z[9] + -z[57];
z[7] = z[7] * z[32];
z[9] = abb[30] * z[25];
z[10] = z[47] * z[52];
z[13] = abb[48] * z[53];
z[17] = abb[39] * z[48];
z[7] = z[7] + -z[9] + -z[10] + z[13] + -z[17] + z[26] + z[51];
z[7] = z[7] * z[29];
z[1] = z[1] + z[4] + z[52] + -z[54];
z[1] = -z[1] * z[30];
z[4] = abb[32] * z[18];
z[4] = z[4] + z[14] + -z[69];
z[4] = abb[4] * z[4];
z[1] = z[1] + z[4] + z[40];
z[4] = -z[11] * z[34];
z[1] = (T(1) / T(2)) * z[1] + z[4] + z[12] + -z[16] + z[66];
z[1] = z[1] * z[3];
z[3] = z[32] * z[49];
z[4] = -z[28] + -z[58];
z[4] = abb[32] * z[4];
z[3] = z[3] + z[4] + z[60] + z[68];
z[3] = z[3] * z[36];
z[4] = abb[28] * abb[40] * z[39];
z[1] = abb[44] + z[1] + z[2] + z[3] + (T(1) / T(64)) * z[4] + z[5] + z[6] + z[7] + (T(1) / T(16)) * z[8] + z[15];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_448_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.2229406852754450308600876055638796938568208328197201004665447706"),stof<T>("-0.10132680948732982130675139559947531325797989241265149128864421455")}, std::complex<T>{stof<T>("0.17262949413930775849604004248819017248401923549030718127495441082"),stof<T>("-0.047168364341265508678632992523817625617378433119756881667973716567")}, std::complex<T>{stof<T>("0.2229406852754450308600876055638796938568208328197201004665447706"),stof<T>("-0.10132680948732982130675139559947531325797989241265149128864421455")}, std::complex<T>{stof<T>("0.15022160903092002264439342897221932018499140347341938814054228962"),stof<T>("-0.11414726867255617935631918113596527633151736400092684699982331088")}, std::complex<T>{stof<T>("0.11547469860951485867325507914443830029563758720371430850081481908"),stof<T>("-0.28378637949826482878096434905678768657500355789218316041368945857")}, std::complex<T>{stof<T>("0.027078297507034053871185928587716811995264245383652167487882277839"),stof<T>("-0.065958593387465625013622675084755567328522146666352131071790205943")}, std::complex<T>{stof<T>("-0.04972339456069860063975792774574482256343578028833931988485229454"),stof<T>("0.13801363516870842049040280265772312585990829028334043153424807411")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_448_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_448_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_448_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(256)) * (v[2] + v[3]) * (-8 + 3 * v[2] + 3 * v[3] + -4 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(-1) / T(32)) * (v[2] + v[3])) / tend;


		return (abb[51] + abb[53] + abb[54]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[31] + abb[34] * (T(1) / T(2));
z[0] = abb[34] * z[0];
z[1] = prod_pow(abb[31], 2);
z[0] = abb[38] + z[0] + (T(1) / T(2)) * z[1];
z[1] = abb[51] + abb[53] + abb[54];
return abb[12] * (T(1) / T(16)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_448_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.12859412000614909217962997931210223124328802000465183632759617216"),stof<T>("-0.16932750399970493041450531401030140763628697790480269671613473188")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,61> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W15(k,dl), dlog_W18(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W26(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_19(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_11_im(k), T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_4_re(k), f_2_7_re(k), f_2_11_re(k), T{0}};
abb[44] = SpDLog_f_4_448_W_19_Im(t, path, abb);
abb[60] = SpDLog_f_4_448_W_19_Re(t, path, abb);

                    
            return f_4_448_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_448_DLogXconstant_part(base_point<T>, kend);
	value += f_4_448_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_448_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_448_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_448_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_448_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_448_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_448_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
