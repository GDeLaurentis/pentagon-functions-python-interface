/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_16.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_16_abbreviated (const std::array<T,27>& abb) {
T z[32];
z[0] = 3 * abb[19];
z[1] = -abb[21] + z[0];
z[2] = abb[22] * (T(1) / T(2));
z[1] = abb[20] + (T(1) / T(2)) * z[1] + -z[2];
z[3] = abb[6] * z[1];
z[4] = -2 * abb[20] + abb[21] + abb[22];
z[0] = -z[0] + z[4];
z[5] = abb[4] * z[0];
z[6] = z[3] + -z[5];
z[7] = abb[20] * (T(1) / T(2));
z[8] = abb[19] + z[7];
z[9] = abb[18] * (T(1) / T(2));
z[10] = -z[2] + z[8] + -z[9];
z[11] = abb[2] * z[10];
z[12] = abb[19] + abb[20];
z[13] = abb[18] + -abb[21] + z[12];
z[14] = abb[0] * z[13];
z[15] = z[11] + -z[14];
z[16] = -abb[23] + abb[24];
z[17] = abb[7] + abb[8];
z[18] = (T(1) / T(4)) * z[17];
z[18] = -z[16] * z[18];
z[15] = (T(-1) / T(2)) * z[15] + z[18];
z[18] = abb[1] * z[0];
z[19] = abb[18] * (T(1) / T(4));
z[20] = 4 * abb[19];
z[21] = abb[20] * (T(-11) / T(4)) + abb[21] * (T(3) / T(2)) + abb[22] * (T(5) / T(4)) + -z[19] + -z[20];
z[21] = abb[3] * z[21];
z[21] = z[6] + -z[15] + 2 * z[18] + z[21];
z[21] = abb[11] * z[21];
z[22] = abb[21] * (T(1) / T(2));
z[23] = 2 * abb[19];
z[19] = abb[20] * (T(-5) / T(4)) + abb[22] * (T(3) / T(4)) + z[19] + z[22] + -z[23];
z[19] = abb[3] * z[19];
z[15] = z[3] + z[15] + z[18] + z[19];
z[19] = abb[13] * z[15];
z[24] = -abb[21] + z[23];
z[9] = abb[20] * (T(-3) / T(2)) + z[2] + -z[9] + -z[24];
z[9] = abb[0] * z[9];
z[25] = 2 * abb[3];
z[26] = -abb[18] + abb[19];
z[26] = z[25] * z[26];
z[27] = (T(-1) / T(2)) * z[16];
z[28] = abb[8] * z[27];
z[9] = z[9] + -z[11] + -z[18] + z[26] + z[28];
z[9] = abb[14] * z[9];
z[11] = abb[6] * z[0];
z[26] = z[5] + z[11];
z[28] = z[0] * z[25];
z[28] = 3 * z[18] + -z[26] + z[28];
z[29] = abb[12] * z[28];
z[9] = z[9] + z[19] + z[21] + -z[29];
z[9] = abb[14] * z[9];
z[19] = abb[0] * z[10];
z[8] = z[8] + -z[22];
z[8] = abb[1] * z[8];
z[21] = abb[7] * z[27];
z[12] = -abb[22] + z[12];
z[22] = abb[5] * z[12];
z[30] = (T(1) / T(2)) * z[13];
z[30] = abb[3] * z[30];
z[3] = -z[3] + z[8] + z[19] + z[21] + (T(3) / T(2)) * z[22] + z[30];
z[3] = abb[13] * z[3];
z[8] = abb[11] * z[15];
z[15] = 3 * abb[20];
z[19] = 2 * abb[22];
z[20] = -abb[21] + z[15] + -z[19] + z[20];
z[20] = abb[1] * z[20];
z[20] = z[11] + z[20] + z[22];
z[21] = abb[3] * z[0];
z[21] = -z[20] + z[21];
z[30] = abb[12] * z[21];
z[3] = z[3] + -z[8] + z[30];
z[3] = abb[13] * z[3];
z[8] = -z[8] + z[29];
z[8] = abb[11] * z[8];
z[29] = 7 * abb[4] + -abb[6];
z[31] = -abb[19] + abb[21] * (T(1) / T(3));
z[31] = abb[20] * (T(-1) / T(3)) + abb[22] * (T(1) / T(6)) + (T(1) / T(2)) * z[31];
z[29] = z[29] * z[31];
z[12] = abb[1] * z[12];
z[31] = abb[19] * (T(5) / T(6)) + abb[18] * (T(13) / T(6)) + -z[4];
z[31] = abb[3] * z[31];
z[12] = (T(5) / T(6)) * z[12] + (T(13) / T(6)) * z[14] + (T(-4) / T(3)) * z[22] + z[29] + z[31];
z[12] = prod_pow(m1_set::bc<T>[0], 2) * z[12];
z[29] = abb[25] * z[28];
z[2] = -abb[21] + abb[18] * (T(3) / T(2)) + z[2] + z[7];
z[7] = abb[7] * (T(1) / T(2));
z[2] = z[2] * z[7];
z[7] = -abb[9] * z[1];
z[31] = abb[10] * z[16];
z[2] = z[2] + z[7] + z[31];
z[2] = abb[15] * z[2];
z[7] = abb[15] * (T(3) / T(2));
z[10] = -abb[8] * z[7] * z[10];
z[19] = 7 * abb[19] + abb[21] * (T(-5) / T(2)) + abb[20] * (T(9) / T(2)) + -z[19];
z[19] = abb[1] * z[19];
z[6] = -z[6] + z[19] + (T(-1) / T(2)) * z[22];
z[19] = prod_pow(abb[12], 2);
z[6] = z[6] * z[19];
z[20] = -abb[26] * z[20];
z[1] = z[1] * z[19];
z[0] = abb[26] * z[0];
z[19] = (T(-3) / T(4)) * z[16];
z[19] = abb[15] * z[19];
z[0] = z[0] + 3 * z[1] + z[19];
z[0] = abb[3] * z[0];
z[1] = abb[0] * abb[15];
z[7] = abb[2] * z[7];
z[1] = z[1] + z[7];
z[1] = z[1] * z[27];
z[0] = z[0] + z[1] + z[2] + z[3] + z[6] + z[8] + z[9] + z[10] + z[12] + z[20] + z[29];
z[1] = -z[16] * z[17];
z[2] = -abb[2] + -abb[3];
z[3] = -abb[18] + abb[20] + -abb[22] + z[23];
z[2] = z[2] * z[3];
z[1] = z[1] + z[2] + z[5] + -z[11] + 2 * z[14] + z[18];
z[1] = abb[11] * z[1];
z[2] = abb[7] * z[16];
z[5] = -abb[0] * z[3];
z[6] = -abb[20] + -z[24];
z[6] = abb[1] * z[6];
z[7] = -abb[3] * z[13];
z[2] = z[2] + z[5] + z[6] + z[7] + -z[11] + -3 * z[22];
z[2] = abb[13] * z[2];
z[5] = abb[18] + -abb[22] + z[15] + 2 * z[24];
z[5] = abb[0] * z[5];
z[4] = 2 * abb[18] + abb[19] + -z[4];
z[4] = z[4] * z[25];
z[6] = abb[8] * z[16];
z[3] = abb[2] * z[3];
z[3] = z[3] + z[4] + z[5] + z[6] + -z[18] + z[26];
z[3] = abb[14] * z[3];
z[1] = z[1] + z[2] + z[3] + -z[30];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[16] * z[28];
z[3] = abb[17] * z[21];
z[1] = z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_16_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("32.015977321136837966284538661812631100330654656089269105255837833"),stof<T>("-32.122816881741839243981794978395847211048767046928903357999216613")}, std::complex<T>{stof<T>("4.683283497861546278651986269909679207920067075210126545359418151"),stof<T>("-39.019428720353443245310117563289509346254382037918199426266848152")}, std::complex<T>{stof<T>("19.120557438804397220157315782008366555720457694346631444035997676"),stof<T>("-36.938098932542842045530900361220766719457244428197764994817263735")}, std::complex<T>{stof<T>("-17.578703380193987024779209149713943752530264036952764206579258308"),stof<T>("34.204146669552440443761012180464589837845904656649337789448801031")}, std::complex<T>{stof<T>("-1.5418540586104101953781066322944228031901936573938672374567393684"),stof<T>("2.733952262990401601769888180756176881611339771548427205368462704")}, std::complex<T>{stof<T>("-0.5121113943864244331929380064625054643259069593013927369576886889"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("0.5121113943864244331929380064625054643259069593013927369576886889"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_16_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_16_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-63.502924434683764737822309351167793723641827451725360888160485152"),stof<T>("-25.881607803670492600657417830775082824927970530620364848287740584")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,27> abb = {dl[0], dl[5], dl[2], dl[4], dlog_W15(k,dl), dlog_W22(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_4_re(k), f_2_7_re(k)};

                    
            return f_4_16_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_16_DLogXconstant_part(base_point<T>, kend);
	value += f_4_16_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_16_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_16_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_16_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_16_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_16_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_16_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
