/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_299.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_299_abbreviated (const std::array<T,54>& abb) {
T z[96];
z[0] = abb[25] * abb[50];
z[1] = -abb[43] + abb[49];
z[2] = -abb[44] + z[1];
z[3] = abb[7] * z[2];
z[4] = abb[20] * abb[50];
z[5] = z[0] + -z[3] + z[4];
z[6] = abb[45] + abb[46];
z[7] = abb[43] * (T(23) / T(4)) + 4 * z[6];
z[8] = abb[44] * (T(1) / T(4));
z[9] = abb[48] * (T(1) / T(2));
z[10] = abb[49] * (T(7) / T(12)) + (T(-1) / T(3)) * z[7] + z[8] + z[9];
z[10] = abb[5] * z[10];
z[11] = abb[43] * (T(1) / T(2));
z[12] = abb[49] * (T(1) / T(2));
z[13] = z[11] + z[12];
z[14] = z[6] + z[13];
z[15] = abb[41] * (T(1) / T(2));
z[16] = abb[47] * (T(-1) / T(2)) + -z[9] + (T(1) / T(3)) * z[14] + z[15];
z[16] = abb[1] * z[16];
z[17] = abb[49] * (T(1) / T(4));
z[18] = abb[41] * (T(13) / T(6)) + -z[17];
z[19] = abb[43] * (T(-43) / T(4)) + abb[42] * (T(13) / T(2)) + -5 * z[6];
z[8] = -z[8] + z[18] + (T(1) / T(3)) * z[19];
z[8] = abb[3] * z[8];
z[19] = abb[44] * (T(1) / T(2));
z[20] = z[13] + -z[19];
z[21] = -abb[42] + z[20];
z[22] = abb[15] * z[21];
z[7] = abb[42] + -z[7];
z[7] = abb[44] * (T(-13) / T(4)) + abb[47] * (T(13) / T(2)) + (T(1) / T(3)) * z[7] + -z[18];
z[7] = abb[8] * z[7];
z[18] = -abb[44] + z[6];
z[23] = -abb[48] + abb[49] + z[18];
z[23] = abb[11] * z[23];
z[24] = abb[22] * abb[50];
z[25] = (T(1) / T(4)) * z[24];
z[26] = abb[42] + -abb[43];
z[27] = abb[12] * z[26];
z[28] = 3 * z[27];
z[29] = -abb[42] + abb[49];
z[30] = -abb[48] + z[29];
z[30] = abb[9] * z[30];
z[31] = 3 * z[30];
z[29] = -abb[41] + z[29];
z[32] = abb[0] * z[29];
z[33] = -abb[43] + z[6];
z[34] = -abb[49] + z[33];
z[34] = abb[42] + (T(1) / T(3)) * z[34];
z[34] = abb[2] * z[34];
z[5] = (T(-1) / T(4)) * z[5] + z[7] + z[8] + z[10] + 13 * z[16] + (T(-1) / T(2)) * z[22] + -3 * z[23] + -z[25] + -z[28] + z[31] + (T(-13) / T(6)) * z[32] + z[34];
z[5] = prod_pow(m1_set::bc<T>[0], 2) * z[5];
z[7] = abb[21] * abb[50];
z[8] = abb[23] * abb[50];
z[10] = z[3] + z[7] + z[8];
z[16] = (T(1) / T(2)) * z[10];
z[34] = -abb[44] + abb[49];
z[35] = -abb[48] + z[6];
z[36] = z[34] + z[35];
z[37] = abb[14] * z[36];
z[38] = abb[50] * (T(1) / T(2));
z[39] = abb[25] * z[38];
z[40] = abb[24] * abb[50];
z[41] = z[39] + z[40];
z[36] = abb[5] * z[36];
z[42] = -abb[49] + z[6];
z[43] = 3 * abb[42] + -abb[43] + z[42];
z[44] = abb[2] * z[43];
z[45] = 3 * z[44];
z[43] = abb[8] * z[43];
z[46] = abb[44] + z[1];
z[46] = -abb[48] + (T(1) / T(2)) * z[46];
z[47] = abb[6] * z[46];
z[18] = abb[43] + z[18];
z[48] = abb[3] * z[18];
z[36] = z[16] + z[22] + z[36] + -z[37] + z[41] + z[43] + z[45] + -z[47] + -z[48];
z[36] = -z[27] + z[30] + (T(1) / T(2)) * z[36];
z[36] = abb[32] * z[36];
z[48] = abb[14] * z[46];
z[48] = -z[47] + z[48];
z[49] = (T(1) / T(2)) * z[2];
z[50] = abb[3] * z[49];
z[46] = abb[5] * z[46];
z[51] = abb[24] * z[38];
z[46] = -z[46] + z[48] + -z[50] + z[51];
z[16] = -z[16] + -z[46];
z[16] = abb[30] * z[16];
z[16] = z[16] + z[36];
z[16] = abb[32] * z[16];
z[36] = abb[44] * (T(3) / T(2));
z[50] = -z[14] + z[36];
z[52] = abb[8] * z[50];
z[53] = abb[5] * z[50];
z[54] = z[52] + z[53];
z[55] = abb[4] * z[50];
z[56] = -z[54] + z[55];
z[57] = abb[43] * (T(3) / T(2));
z[58] = -z[12] + z[57];
z[59] = -z[6] + z[19];
z[60] = z[58] + -z[59];
z[61] = 2 * abb[47];
z[62] = 2 * abb[41];
z[63] = -z[60] + z[61] + -z[62];
z[63] = abb[3] * z[63];
z[4] = z[4] + z[24];
z[64] = z[4] + z[8];
z[65] = (T(1) / T(2)) * z[64];
z[66] = abb[41] + -abb[47];
z[18] = z[18] + z[66];
z[18] = abb[10] * z[18];
z[67] = 2 * abb[43];
z[42] = z[42] + z[67];
z[68] = 3 * abb[47];
z[69] = 3 * abb[41] + z[42] + -z[68];
z[70] = abb[1] * z[69];
z[49] = z[49] + -z[66];
z[71] = abb[13] * z[49];
z[63] = z[18] + -z[56] + z[63] + z[65] + -z[70] + -z[71];
z[63] = abb[34] * z[63];
z[72] = abb[20] + abb[21];
z[73] = abb[50] * z[72];
z[74] = abb[3] * z[2];
z[73] = z[8] + z[73] + -z[74];
z[20] = z[20] + z[35];
z[20] = abb[5] * z[20];
z[20] = -z[20] + z[52];
z[35] = abb[48] * (T(3) / T(2));
z[74] = -abb[49] + (T(-1) / T(2)) * z[33] + z[35];
z[74] = abb[14] * z[74];
z[75] = (T(1) / T(2)) * z[47];
z[20] = (T(-1) / T(2)) * z[20] + -z[23] + z[25] + (T(1) / T(4)) * z[73] + -z[74] + -z[75];
z[20] = prod_pow(abb[30], 2) * z[20];
z[25] = abb[48] + -z[1] + z[66];
z[25] = abb[1] * z[25];
z[7] = -z[7] + -z[40];
z[73] = abb[3] * z[49];
z[7] = (T(1) / T(2)) * z[7] + -z[25] + -z[48] + -z[71] + z[73];
z[7] = abb[33] * z[7];
z[10] = abb[35] * z[10];
z[34] = -abb[43] + z[34];
z[34] = (T(1) / T(2)) * z[34] + -z[66];
z[34] = abb[18] * z[34];
z[48] = -abb[19] * z[21];
z[73] = -abb[26] * z[38];
z[34] = z[34] + z[48] + z[73];
z[34] = abb[37] * z[34];
z[13] = -abb[42] + -abb[47] + z[13] + z[19];
z[13] = abb[17] * abb[37] * z[13];
z[10] = z[10] + z[13] + z[34];
z[13] = abb[49] * (T(3) / T(2));
z[19] = 2 * abb[48] + z[11] + -z[13] + z[59];
z[19] = abb[14] * z[19];
z[34] = abb[22] + abb[23];
z[34] = z[34] * z[38];
z[48] = -z[23] + z[34];
z[59] = z[38] * z[72];
z[72] = z[48] + z[59];
z[51] = -z[51] + z[72];
z[19] = z[19] + z[47] + -z[51];
z[21] = abb[8] * z[21];
z[21] = -z[21] + z[22] + z[44];
z[73] = z[19] + z[21] + z[39];
z[76] = -abb[51] * z[73];
z[46] = abb[35] * z[46];
z[19] = z[19] + -z[56];
z[56] = abb[52] * z[19];
z[7] = z[7] + (T(1) / T(2)) * z[10] + z[16] + z[20] + z[46] + z[56] + z[63] + z[76];
z[10] = z[6] + z[62];
z[16] = abb[42] * (T(1) / T(2));
z[20] = abb[49] * (T(3) / T(4));
z[46] = z[10] + z[16] + -z[20];
z[56] = abb[43] * (T(5) / T(4));
z[63] = abb[44] * (T(3) / T(4));
z[76] = z[46] + z[56] + z[63] + -z[68];
z[76] = abb[8] * z[76];
z[77] = z[20] + z[63];
z[78] = abb[43] * (T(7) / T(2)) + z[6];
z[79] = -2 * abb[42] + -z[62] + z[77] + (T(1) / T(2)) * z[78];
z[79] = abb[3] * z[79];
z[80] = (T(3) / T(2)) * z[22];
z[3] = z[3] + -z[4];
z[4] = z[56] + -z[63];
z[56] = z[4] + z[6];
z[81] = -z[17] + z[56];
z[81] = abb[5] * z[81];
z[82] = (T(3) / T(4)) * z[0];
z[28] = (T(-3) / T(4)) * z[3] + z[28] + -z[32] + (T(1) / T(2)) * z[44] + -z[70] + z[76] + z[79] + z[80] + z[81] + z[82];
z[28] = abb[28] * z[28];
z[76] = 2 * abb[3];
z[79] = abb[5] + z[76];
z[42] = z[42] * z[79];
z[79] = (T(3) / T(2)) * z[3];
z[81] = abb[14] * z[50];
z[83] = abb[50] * (T(3) / T(2));
z[84] = abb[24] * z[83];
z[85] = z[81] + z[84];
z[42] = z[42] + -z[52] + z[79] + z[85];
z[42] = abb[30] * z[42];
z[50] = abb[3] * z[50];
z[52] = z[50] + z[85];
z[22] = 3 * z[22];
z[86] = abb[25] * z[83];
z[86] = z[22] + z[86];
z[87] = -z[53] + z[86];
z[43] = z[43] + z[45] + z[52] + z[87];
z[43] = abb[32] * z[43];
z[42] = z[42] + -z[43];
z[28] = z[28] + z[42];
z[28] = abb[28] * z[28];
z[45] = 4 * abb[41];
z[88] = 2 * z[6];
z[89] = z[45] + z[88];
z[16] = abb[47] * (T(-9) / T(2)) + abb[43] * (T(13) / T(4)) + -z[16] + -z[77] + z[89];
z[16] = abb[3] * z[16];
z[77] = abb[47] * (T(3) / T(2));
z[15] = z[15] + -z[77];
z[90] = -abb[42] + z[15];
z[20] = z[20] + z[56] + z[90];
z[20] = abb[8] * z[20];
z[56] = (T(1) / T(2)) * z[32];
z[80] = -z[56] + z[80];
z[91] = -z[44] + 2 * z[70];
z[92] = 3 * z[18];
z[93] = (T(3) / T(2)) * z[71];
z[16] = z[16] + z[20] + -z[53] + (T(-3) / T(4)) * z[64] + -z[80] + -z[82] + z[91] + -z[92] + z[93];
z[16] = abb[28] * z[16];
z[20] = z[69] * z[76];
z[20] = z[20] + -z[54] + z[85];
z[82] = 3 * z[71];
z[64] = (T(3) / T(2)) * z[64];
z[85] = -z[20] + z[64] + -3 * z[70] + -z[82] + z[92];
z[92] = -abb[31] * z[85];
z[94] = abb[41] + abb[42] + abb[44] * (T(9) / T(2)) + -z[6] + -z[11] + -z[13] + -z[68];
z[94] = abb[8] * z[94];
z[95] = 3 * z[55];
z[64] = z[44] + z[53] + z[64] + z[94] + -z[95];
z[70] = z[70] + z[93];
z[4] = -z[4] + -z[46] + z[77];
z[4] = abb[3] * z[4];
z[4] = z[4] + z[56] + (T(1) / T(2)) * z[64] + -z[70];
z[4] = abb[29] * z[4];
z[46] = 3 * abb[44];
z[56] = abb[43] + abb[49] + -z[46] + z[88];
z[64] = abb[5] + abb[8];
z[56] = z[56] * z[64];
z[52] = -z[52] + z[56] + z[95];
z[52] = abb[30] * z[52];
z[4] = z[4] + z[16] + z[43] + z[52] + z[92];
z[4] = abb[29] * z[4];
z[16] = (T(1) / T(2)) * z[6];
z[15] = -z[12] + -z[15] + -z[16] + z[26];
z[15] = abb[3] * z[15];
z[26] = -abb[21] * z[83];
z[26] = z[26] + z[44] + z[87];
z[52] = abb[41] * (T(3) / T(2)) + -z[77];
z[14] = -z[14] + z[35] + -z[52];
z[14] = abb[1] * z[14];
z[1] = abb[42] + z[1] + z[6];
z[1] = -abb[41] + (T(1) / T(2)) * z[1];
z[1] = abb[8] * z[1];
z[35] = 2 * z[32];
z[1] = z[1] + z[14] + z[15] + (T(1) / T(2)) * z[26] + -z[31] + z[35] + (T(3) / T(2)) * z[47] + -z[74] + -z[93];
z[1] = abb[27] * z[1];
z[14] = abb[49] * (T(5) / T(2));
z[15] = abb[42] + z[36];
z[26] = -z[14] + z[15] + z[57] + -z[68];
z[26] = (T(1) / T(2)) * z[26] + z[62];
z[26] = abb[3] * z[26];
z[17] = abb[43] * (T(3) / T(4)) + z[17] + z[63] + z[90];
z[17] = abb[8] * z[17];
z[31] = z[41] + -z[65];
z[41] = z[44] + z[81];
z[17] = z[17] + -z[26] + (T(-3) / T(2)) * z[31] + -z[41] + -z[70] + -z[80];
z[26] = -abb[28] + abb[29];
z[17] = z[17] * z[26];
z[26] = -z[36] + z[88];
z[31] = 3 * abb[48];
z[11] = z[11] + -z[14] + -z[26] + z[31];
z[11] = abb[14] * z[11];
z[14] = -abb[20] * z[38];
z[14] = z[14] + z[25] + -z[48];
z[25] = abb[3] * z[69];
z[11] = z[11] + 3 * z[14] + z[25] + z[54] + z[82] + z[84];
z[11] = abb[31] * z[11];
z[14] = 2 * abb[49] + -z[31] + z[33];
z[14] = 2 * z[14];
z[25] = abb[14] * z[14];
z[31] = 3 * z[47];
z[25] = z[25] + -z[31] + z[50] + -z[54] + 3 * z[72];
z[33] = abb[30] * z[25];
z[1] = z[1] + z[11] + z[17] + z[33] + -z[43];
z[1] = abb[27] * z[1];
z[11] = abb[20] + abb[21] * (T(1) / T(2));
z[11] = abb[50] * z[11];
z[8] = z[8] + z[11] + z[24] + z[37] + -z[40] + z[55] + -z[71];
z[6] = -z[6] + z[9] + -z[58] + (T(-5) / T(2)) * z[66];
z[6] = abb[1] * z[6];
z[9] = -abb[43] + z[12] + -z[16] + -z[52];
z[9] = abb[3] * z[9];
z[6] = z[6] + (T(1) / T(2)) * z[8] + z[9] + z[18] + -z[75];
z[6] = abb[31] * z[6];
z[8] = abb[30] * z[19];
z[6] = z[6] + z[8];
z[8] = abb[28] * z[85];
z[6] = 3 * z[6] + z[8];
z[6] = abb[31] * z[6];
z[8] = abb[36] * z[25];
z[2] = abb[5] * z[2];
z[0] = z[0] + z[2] + -z[3];
z[2] = abb[3] * z[60];
z[0] = (T(1) / T(2)) * z[0] + -z[2] + z[21];
z[0] = 3 * z[0];
z[2] = -abb[53] * z[0];
z[3] = abb[16] * abb[37] * z[49];
z[1] = z[1] + z[2] + (T(-3) / T(2)) * z[3] + z[4] + z[5] + z[6] + 3 * z[7] + z[8] + z[28];
z[2] = -z[39] + z[51];
z[3] = -z[15] + z[58] + z[62];
z[3] = abb[8] * z[3];
z[4] = abb[1] * z[14];
z[5] = z[29] * z[76];
z[2] = 3 * z[2] + z[3] + z[4] + z[5] + -z[22] + 6 * z[30] + -z[31] + -4 * z[32] + -z[41];
z[2] = abb[27] * z[2];
z[3] = abb[43] * (T(5) / T(2));
z[4] = 6 * abb[47];
z[5] = -z[3] + z[4] + z[13] + -z[15] + -z[89];
z[5] = abb[8] * z[5];
z[6] = 4 * abb[42] + -z[13] + -z[36] + z[45] + -z[78];
z[6] = abb[3] * z[6];
z[7] = z[35] + -z[86] + z[91];
z[3] = -z[3] + z[12] + -z[26];
z[3] = abb[5] * z[3];
z[3] = z[3] + z[5] + z[6] + z[7] + -6 * z[27] + z[79];
z[3] = abb[28] * z[3];
z[5] = 6 * z[18] + z[95];
z[6] = -abb[42] + z[10] + z[67];
z[4] = z[4] + -z[6] + -z[46];
z[4] = abb[8] * z[4];
z[6] = -z[6] + z[68];
z[6] = z[6] * z[76];
z[4] = z[4] + z[5] + z[6] + -z[7] + z[53];
z[4] = abb[29] * z[4];
z[6] = -z[23] + -z[34] + -z[59];
z[7] = abb[43] + -abb[48] + z[10] + -z[61];
z[7] = abb[1] * z[7];
z[5] = -z[5] + 3 * z[6] + 6 * z[7] + z[20] + z[31];
z[5] = abb[31] * z[5];
z[2] = z[2] + z[3] + z[4] + z[5] + -z[42];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = -abb[38] * z[73];
z[4] = abb[39] * z[19];
z[3] = z[3] + z[4];
z[0] = -abb[40] * z[0];
z[0] = z[0] + z[2] + 3 * z[3];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_299_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("15.22705487121682531585184216070836765633796551931412506268836417"),stof<T>("-83.781197666787351751956231260651738209094290445805215079939375928")}, std::complex<T>{stof<T>("-6.82070293910296763206809565657268841048751017346517091363243205"),stof<T>("-48.273888092845965450964722256267332118280718923943817868695212391")}, std::complex<T>{stof<T>("-6.487778227773556684859704839822798395323266192295462149335665204"),stof<T>("50.985310504584355545386129673624559246764242442805024950460033881")}, std::complex<T>{stof<T>("-9.254686010775785804542390477783609725658270984679616371327311634"),stof<T>("-15.55469254458090349004541917808688501560192144786321451017521826")}, std::complex<T>{stof<T>("-7.184124888314354108986160328487616470637305501138732813068445575"),stof<T>("18.6213156475939604861182307099586211964636631965117156096363888")}, std::complex<T>{stof<T>("-7.184124888314354108986160328487616470637305501138732813068445575"),stof<T>("18.6213156475939604861182307099586211964636631965117156096363888")}, std::complex<T>{stof<T>("38.780699012426523743089114106068010223173683305746504857014498087"),stof<T>("15.712188923077182853362767695481843777380915081787966261904006313")}, std::complex<T>{stof<T>("-43.693299263115379353354045628413022018389543203864180985206756727"),stof<T>("49.092492404841541510823828740696764182871493937718239190702631764")}, std::complex<T>{stof<T>("6.124356278562170207941640167907870335173470864621900249899651679"),stof<T>("15.90989323585557039169682329260139406798013967765050852787156731")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[22].real()/kbase.W[22].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_299_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_299_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("41.335149241672406577830469226200241270730304505594626944805218864"),stof<T>("-4.110441131842011108727343692266235889087615146785555234126705769")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,54> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W18(k,dl), dlog_W23(k,dl), dlog_W25(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_14(k), f_2_20(k), f_2_23(k), f_2_2_im(k), f_2_10_im(k), f_2_12_im(k), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[22].real()/k.W[22].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_2_re(k), f_2_10_re(k), f_2_12_re(k)};

                    
            return f_4_299_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_299_DLogXconstant_part(base_point<T>, kend);
	value += f_4_299_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_299_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_299_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_299_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_299_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_299_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_299_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
