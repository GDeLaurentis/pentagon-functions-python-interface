/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_694.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_694_abbreviated (const std::array<T,33>& abb) {
T z[69];
z[0] = -abb[24] + 3 * abb[29];
z[1] = 3 * z[0];
z[2] = -abb[27] + z[1];
z[3] = 5 * abb[25];
z[4] = 5 * abb[28];
z[5] = 6 * abb[26];
z[2] = abb[23] + -2 * z[2] + z[3] + z[4] + z[5];
z[2] = abb[6] * z[2];
z[6] = 2 * abb[27];
z[1] = z[1] + -z[6];
z[7] = 3 * abb[25];
z[8] = 4 * abb[26];
z[9] = z[7] + z[8];
z[10] = 3 * abb[23];
z[1] = 2 * z[1] + -z[4] + -z[9] + -z[10];
z[1] = abb[8] * z[1];
z[11] = abb[30] + 2 * abb[31] + -abb[32];
z[12] = 2 * z[11];
z[13] = abb[10] * z[12];
z[14] = abb[11] * z[12];
z[13] = z[13] + -z[14];
z[15] = 2 * z[1] + -z[13];
z[16] = 3 * abb[28];
z[17] = abb[23] + -abb[25];
z[8] = z[6] + z[8] + -z[16] + -z[17];
z[18] = abb[7] * z[8];
z[19] = 2 * abb[25];
z[20] = 5 * abb[26];
z[21] = 2 * z[0];
z[22] = abb[28] + z[21];
z[23] = 3 * abb[27];
z[24] = z[19] + z[20] + -z[22] + z[23];
z[25] = 2 * abb[3];
z[24] = z[24] * z[25];
z[26] = 2 * abb[26];
z[27] = -abb[28] + -z[17] + z[26];
z[28] = abb[1] * z[27];
z[29] = 7 * abb[28] + -6 * z[0];
z[30] = 5 * abb[27];
z[31] = 6 * abb[23] + -abb[26] + z[29] + z[30];
z[31] = abb[5] * z[31];
z[32] = abb[23] + abb[25];
z[33] = z[21] + -z[32];
z[34] = abb[26] + abb[27];
z[35] = 2 * abb[28] + -z[33] + z[34];
z[36] = abb[0] * z[35];
z[24] = z[2] + z[15] + -2 * z[18] + z[24] + 5 * z[28] + z[31] + z[36];
z[24] = abb[18] * z[24];
z[31] = abb[25] + z[26];
z[36] = 5 * abb[23];
z[37] = -abb[27] + z[0];
z[4] = -z[4] + -z[31] + -z[36] + 6 * z[37];
z[4] = abb[5] * z[4];
z[38] = 2 * abb[23];
z[39] = 3 * abb[26];
z[30] = -z[22] + z[30] + z[38] + z[39];
z[30] = z[25] * z[30];
z[40] = z[16] + -z[26];
z[41] = 4 * abb[27];
z[42] = z[17] + -z[40] + z[41];
z[43] = abb[7] * z[42];
z[9] = abb[23] + -abb[28] + -z[9] + z[21];
z[9] = abb[0] * z[9];
z[21] = -abb[28] + z[6] + z[17];
z[44] = abb[2] * z[21];
z[20] = 6 * abb[25] + -abb[27] + z[20] + z[29];
z[20] = abb[6] * z[20];
z[15] = -z[4] + -z[9] + z[15] + z[20] + z[30] + -2 * z[43] + 5 * z[44];
z[15] = abb[20] * z[15];
z[20] = -abb[25] + z[10];
z[29] = 2 * z[37];
z[30] = z[20] + -z[29] + z[40];
z[30] = abb[7] * z[30];
z[40] = 2 * z[28];
z[45] = abb[5] * z[35];
z[46] = z[40] + -z[45];
z[47] = abb[28] + z[26] + -z[29] + z[32];
z[48] = abb[4] * z[47];
z[49] = -z[14] + z[48];
z[50] = -z[16] + z[33];
z[51] = abb[0] * z[50];
z[52] = z[25] * z[27];
z[53] = abb[9] * z[12];
z[30] = -z[30] + -2 * z[46] + z[49] + z[51] + -z[52] + -z[53];
z[46] = abb[13] * z[30];
z[51] = abb[3] * z[8];
z[54] = abb[10] * z[11];
z[51] = z[51] + z[54];
z[55] = z[28] + z[44];
z[56] = abb[7] * z[50];
z[57] = -z[55] + z[56];
z[58] = abb[0] * z[47];
z[14] = -z[14] + z[58];
z[47] = 2 * z[47];
z[59] = -abb[6] * z[47];
z[60] = -z[0] + z[23];
z[38] = -abb[25] + z[38] + z[60];
z[61] = 2 * abb[4];
z[38] = z[38] * z[61];
z[38] = -z[14] + z[38] + z[51] + -z[57] + z[59];
z[38] = abb[15] * z[38];
z[34] = -abb[28] + z[34];
z[59] = abb[3] * z[34];
z[62] = abb[11] * z[11];
z[59] = z[55] + z[59] + z[62];
z[63] = abb[6] * z[35];
z[31] = z[31] + -z[37];
z[31] = abb[0] * z[31];
z[31] = z[31] + -z[59] + z[63];
z[64] = abb[23] + abb[28];
z[37] = z[37] + -z[64];
z[37] = z[37] * z[61];
z[65] = abb[9] * z[11];
z[31] = 2 * z[31] + z[37] + z[56] + z[65];
z[31] = abb[14] * z[31];
z[31] = z[31] + z[38] + z[46];
z[31] = abb[15] * z[31];
z[30] = -abb[14] * z[30];
z[37] = abb[4] * z[21];
z[38] = z[37] + z[65];
z[18] = -z[18] + z[38] + z[62];
z[27] = abb[0] * z[27];
z[66] = abb[5] * z[34];
z[28] = z[28] + z[66];
z[28] = z[18] + -z[27] + 2 * z[28] + z[52];
z[28] = abb[13] * z[28];
z[28] = z[28] + z[30];
z[28] = abb[13] * z[28];
z[30] = abb[27] + z[0];
z[30] = -abb[23] + z[7] + z[16] + z[26] + -2 * z[30];
z[30] = abb[7] * z[30];
z[66] = 2 * z[44];
z[67] = -z[63] + z[66];
z[50] = abb[4] * z[50];
z[68] = z[21] * z[25];
z[14] = -z[14] + z[30] + -z[50] + -z[53] + 2 * z[67] + z[68];
z[30] = abb[14] + -abb[15];
z[30] = z[14] * z[30];
z[43] = -z[27] + z[43] + -z[62];
z[50] = abb[6] * z[34];
z[44] = z[44] + z[50];
z[38] = -z[38] + -z[43] + 2 * z[44] + z[68];
z[38] = abb[16] * z[38];
z[30] = z[30] + z[38];
z[30] = abb[16] * z[30];
z[18] = z[18] + z[40] + z[51];
z[18] = abb[19] * z[18];
z[38] = abb[3] * z[42];
z[38] = z[38] + z[54];
z[40] = z[38] + -z[43] + z[66];
z[40] = abb[17] * z[40];
z[18] = z[18] + z[40];
z[27] = z[27] + z[37] + -z[55];
z[37] = -prod_pow(abb[14], 2) * z[27];
z[6] = z[0] + -z[6];
z[20] = -abb[28] + 2 * z[6] + -z[20];
z[40] = -abb[18] * z[20];
z[35] = abb[20] * z[35];
z[11] = abb[22] * z[11];
z[35] = z[11] + z[35] + z[40];
z[35] = abb[4] * z[35];
z[40] = abb[10] + -abb[11];
z[40] = -2 * z[40];
z[17] = -abb[26] + abb[27] + z[17];
z[17] = abb[22] * z[17] * z[40];
z[21] = -abb[22] * z[21];
z[12] = -abb[20] * z[12];
z[12] = z[12] + z[21];
z[12] = abb[9] * z[12];
z[11] = -abb[0] * z[11];
z[11] = z[11] + z[12] + z[15] + z[17] + 2 * z[18] + z[24] + z[28] + z[30] + z[31] + z[35] + z[37];
z[1] = -z[1] + -z[2] + z[4] + -z[55];
z[2] = 2 * z[60];
z[4] = abb[25] + abb[28];
z[12] = -z[2] + z[4] + -z[10] + -z[26];
z[12] = abb[4] * z[12];
z[15] = -z[5] + -z[7] + z[29] + z[64];
z[15] = abb[0] * z[15];
z[2] = z[2] + z[5] + -z[16] + z[32];
z[5] = abb[7] * z[2];
z[2] = -z[2] * z[25];
z[1] = 2 * z[1] + z[2] + 3 * z[5] + z[12] + z[15];
z[1] = abb[21] * z[1];
z[1] = z[1] + 2 * z[11];
z[2] = -z[45] + z[55] + -z[63];
z[8] = -abb[0] * z[8];
z[11] = z[23] + -z[33] + z[39];
z[16] = 2 * z[11];
z[17] = abb[7] * z[16];
z[18] = -abb[4] * z[42];
z[21] = -z[25] * z[34];
z[2] = 2 * z[2] + z[8] + -z[13] + z[17] + z[18] + z[21] + -z[65];
z[2] = abb[15] * z[2];
z[8] = -abb[5] * z[47];
z[13] = -abb[23] + -z[0] + z[19] + z[39];
z[13] = abb[0] * z[13];
z[8] = z[8] + 2 * z[13] + z[38] + -z[49] + -z[57] + z[65];
z[8] = abb[12] * z[8];
z[13] = -abb[16] * z[14];
z[4] = -abb[26] + z[0] + -z[4];
z[4] = abb[0] * z[4];
z[4] = z[4] + z[45] + -z[59];
z[6] = abb[23] + abb[26] + -z[6];
z[6] = z[6] * z[61];
z[4] = 2 * z[4] + z[6] + z[56] + -z[65];
z[4] = abb[14] * z[4];
z[2] = z[2] + z[4] + z[8] + z[13] + z[46];
z[2] = abb[12] * z[2];
z[4] = prod_pow(m1_set::bc<T>[0], 2) * z[27];
z[1] = 2 * z[1] + 4 * z[2] + (T(-13) / T(3)) * z[4];
z[1] = 2 * z[1];
z[2] = z[5] + -z[68];
z[4] = abb[6] * z[11];
z[4] = z[4] + z[55];
z[0] = z[0] + -z[41];
z[0] = abb[28] + 2 * z[0] + z[7] + -z[36];
z[0] = abb[4] * z[0];
z[0] = z[0] + -z[2] + 2 * z[4] + z[58];
z[0] = abb[15] * z[0];
z[4] = z[5] + -z[52];
z[5] = abb[5] * z[11];
z[5] = z[5] + z[55];
z[3] = -8 * abb[26] + -z[3] + z[10] + z[22];
z[3] = abb[0] * z[3];
z[3] = z[3] + -z[4] + 2 * z[5] + z[48];
z[3] = abb[12] * z[3];
z[5] = -abb[5] * z[16];
z[4] = z[4] + z[5] + -z[9] + z[12];
z[4] = abb[13] * z[4];
z[5] = -abb[6] * z[16];
z[6] = -abb[4] * z[20];
z[2] = z[2] + z[5] + z[6] + z[15];
z[2] = abb[16] * z[2];
z[5] = abb[14] * z[27];
z[0] = z[0] + z[2] + z[3] + z[4] + 2 * z[5];
z[0] = 8 * m1_set::bc<T>[0] * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_694_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-266.9579582544494608139824559723510841574797868132201437383725761"),stof<T>("-153.61843653835937357183600043533197142372571790167914132575884259")}, std::complex<T>{stof<T>("56.611071279280149677517551746040688980512321378000910280907792569"),stof<T>("-62.16608835360832014089949208782580435261682896469285264296329567")}, std::complex<T>{stof<T>("323.56902953372961049150000771839177313799210819122105401928036866"),stof<T>("91.45234818475105343093650834750616707110888893698628868279554692")}, std::complex<T>{stof<T>("397.98865284334672419183817772416869096295165536357225515658739415"),stof<T>("65.28411642882423018957667424933632761396888255077272653356449274")}, std::complex<T>{stof<T>("-192.53833494483234711364428596657416633252023964086894260106555061"),stof<T>("-179.78666829428619681319583453350181088086572428789270347498989677")}, std::complex<T>{stof<T>("-17.808552030336964022820618259736228844447225794350290856399232914"),stof<T>("-35.997856597681496899539657989655964895476822578479290493732241489")}, std::complex<T>{stof<T>("-169.83321383784044903255265523812206694153696413400273084272337771"),stof<T>("186.49826506082496042269847626347741305785048689407855792888988701")}, stof<T>("-39.019706748609182091192946822786612080164933204529924838023060765"), stof<T>("-78.039413497218364182385893645573224160329866409059849676046121531"), stof<T>("39.019706748609182091192946822786612080164933204529924838023060765")};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[57].real()/kbase.W[57].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[123].real()/kbase.W[123].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_694_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_694_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("199.01949475753051196094904639897841001586696252468794649702126928"),stof<T>("177.84892564283465641827563917905974693796888203327039400250418259")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,33> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dl[4], dlog_W12(k,dl), dlog_W18(k,dl), dlog_W30(k,dl), dlog_W58(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[57].real()/k.W[57].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real())};

                    
            return f_4_694_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_694_DLogXconstant_part(base_point<T>, kend);
	value += f_4_694_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_694_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_694_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_694_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_694_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_694_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_694_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
