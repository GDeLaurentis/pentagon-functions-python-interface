/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_323.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_323_abbreviated (const std::array<T,59>& abb) {
T z[102];
z[0] = abb[45] * (T(1) / T(2));
z[1] = abb[44] * (T(1) / T(2));
z[2] = z[0] + z[1];
z[3] = abb[47] * (T(1) / T(2));
z[4] = abb[48] * (T(1) / T(2));
z[5] = abb[42] + abb[43] * (T(1) / T(2));
z[6] = z[2] + -z[3] + -z[4] + z[5];
z[6] = abb[11] * z[6];
z[7] = abb[41] + -abb[44];
z[8] = abb[49] * (T(7) / T(3));
z[9] = abb[46] * (T(13) / T(2));
z[10] = 2 * abb[42] + abb[43];
z[11] = abb[48] * (T(-1) / T(4)) + (T(25) / T(12)) * z[7] + z[8] + -z[9] + (T(7) / T(3)) * z[10];
z[11] = abb[0] * z[11];
z[12] = abb[48] * (T(3) / T(2));
z[13] = abb[49] * (T(1) / T(2));
z[3] = abb[44] * (T(-5) / T(6)) + abb[45] * (T(-2) / T(3)) + abb[41] * (T(1) / T(6)) + z[3] + (T(-7) / T(3)) * z[5] + z[12] + -z[13];
z[3] = abb[7] * z[3];
z[14] = -z[1] + -z[10] + z[12];
z[15] = -z[0] + -z[13] + z[14];
z[16] = abb[12] * z[15];
z[17] = abb[52] + abb[53] + abb[54] + abb[55];
z[18] = (T(1) / T(2)) * z[17];
z[19] = abb[19] * z[18];
z[16] = z[16] + -z[19];
z[19] = abb[21] * z[18];
z[20] = z[16] + -z[19];
z[21] = (T(4) / T(3)) * z[10];
z[22] = abb[48] * (T(3) / T(4));
z[23] = abb[45] * (T(-11) / T(6)) + abb[41] * (T(1) / T(12)) + abb[44] * (T(7) / T(12)) + z[13] + -z[21] + z[22];
z[23] = abb[4] * z[23];
z[24] = abb[44] + abb[45];
z[25] = abb[49] + z[24];
z[9] = abb[48] * (T(-5) / T(2)) + abb[47] * (T(13) / T(2)) + z[9] + (T(-8) / T(3)) * z[10] + (T(-4) / T(3)) * z[25];
z[9] = abb[1] * z[9];
z[26] = abb[20] + abb[22];
z[26] = z[17] * z[26];
z[27] = abb[23] + abb[24];
z[18] = -z[18] * z[27];
z[8] = -abb[48] + abb[45] * (T(-11) / T(3)) + abb[44] * (T(-7) / T(6)) + abb[47] * (T(7) / T(2)) + z[8] + -z[21];
z[8] = abb[8] * z[8];
z[21] = abb[45] + -abb[49];
z[28] = abb[41] + z[21];
z[29] = (T(1) / T(2)) * z[28];
z[30] = abb[14] * z[29];
z[31] = abb[50] + abb[51];
z[32] = abb[18] * z[31];
z[33] = abb[15] * z[31];
z[34] = abb[16] * z[31];
z[35] = abb[2] * z[28];
z[3] = z[3] + 7 * z[6] + z[8] + z[9] + z[11] + z[18] + z[20] + z[23] + -z[26] + -z[30] + (T(-1) / T(2)) * z[32] + -2 * z[33] + (T(-9) / T(4)) * z[34] + (T(10) / T(3)) * z[35];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[6] = abb[45] * (T(3) / T(4));
z[8] = abb[41] * (T(1) / T(4));
z[9] = abb[49] * (T(1) / T(4));
z[11] = z[6] + z[8] + z[9] + -z[14];
z[11] = abb[4] * z[11];
z[18] = abb[44] * (T(1) / T(4));
z[23] = z[18] + -z[22];
z[36] = z[5] + z[23];
z[37] = abb[49] * (T(5) / T(4));
z[6] = -abb[41] + -z[6] + z[36] + z[37];
z[6] = abb[7] * z[6];
z[38] = abb[45] * (T(1) / T(4));
z[9] = z[9] + z[36] + z[38];
z[39] = abb[12] * z[9];
z[40] = (T(1) / T(4)) * z[17];
z[41] = abb[19] * z[40];
z[42] = abb[21] * z[40];
z[39] = z[39] + z[41] + z[42];
z[27] = z[27] * z[40];
z[40] = (T(1) / T(4)) * z[31];
z[41] = abb[18] * z[40];
z[27] = z[27] + z[41];
z[41] = (T(1) / T(4)) * z[34];
z[43] = z[27] + z[41];
z[44] = -z[39] + z[43];
z[45] = z[8] + -z[13] + -z[36];
z[45] = abb[0] * z[45];
z[46] = 2 * z[35];
z[47] = abb[14] * z[28];
z[48] = (T(3) / T(4)) * z[47];
z[49] = z[45] + -z[46] + z[48];
z[50] = abb[13] * z[15];
z[51] = 3 * abb[48];
z[52] = 2 * z[10];
z[53] = z[51] + -z[52];
z[54] = z[25] + -z[53];
z[55] = abb[1] * z[54];
z[6] = z[6] + z[11] + 3 * z[44] + z[49] + -z[50] + z[55];
z[6] = abb[27] * z[6];
z[11] = abb[41] * (T(1) / T(2));
z[44] = z[0] + -z[1] + z[5] + -z[11];
z[44] = abb[4] * z[44];
z[56] = abb[1] * z[15];
z[57] = z[0] + z[8] + z[36];
z[58] = -abb[7] * z[57];
z[59] = (T(3) / T(2)) * z[33];
z[60] = z[35] + z[59];
z[61] = z[1] + z[13];
z[62] = abb[45] + z[5] + -z[61];
z[63] = abb[8] * z[62];
z[44] = z[44] + z[45] + -z[56] + z[58] + (T(-1) / T(2)) * z[60] + z[63];
z[44] = abb[30] * z[44];
z[58] = abb[29] * z[15];
z[63] = abb[28] * z[15];
z[64] = z[58] + -z[63];
z[65] = abb[32] * z[29];
z[65] = -z[64] + z[65];
z[65] = abb[7] * z[65];
z[66] = abb[13] * z[64];
z[67] = -abb[49] + z[11] + z[14];
z[67] = abb[0] * z[67];
z[50] = -z[50] + z[67];
z[68] = abb[44] + -abb[48];
z[69] = z[21] + -z[68];
z[70] = -abb[5] * z[69];
z[71] = abb[23] * z[17];
z[72] = z[70] + -z[71];
z[73] = (T(3) / T(2)) * z[72];
z[74] = -abb[4] * z[28];
z[74] = z[50] + -z[60] + -z[73] + z[74];
z[74] = abb[32] * z[74];
z[75] = abb[29] * z[54];
z[76] = abb[28] * z[54];
z[77] = z[75] + 2 * z[76];
z[77] = abb[1] * z[77];
z[78] = (T(1) / T(2)) * z[26];
z[16] = z[16] + -z[78];
z[79] = z[16] + -z[19];
z[79] = abb[28] * z[79];
z[80] = (T(1) / T(2)) * z[72];
z[81] = abb[29] * z[80];
z[81] = z[79] + z[81];
z[82] = 2 * abb[45];
z[83] = -abb[49] + z[10] + z[82];
z[84] = -abb[44] + z[83];
z[85] = abb[29] * z[84];
z[85] = z[76] + z[85];
z[85] = abb[4] * z[85];
z[86] = 2 * abb[29];
z[87] = -abb[32] + z[86];
z[84] = z[84] * z[87];
z[84] = -z[63] + z[84];
z[84] = abb[8] * z[84];
z[44] = -z[6] + z[44] + -z[65] + z[66] + z[74] + z[77] + 3 * z[81] + z[84] + z[85];
z[44] = abb[30] * z[44];
z[66] = abb[49] * (T(3) / T(4));
z[8] = -z[8] + -z[14] + z[38] + z[66];
z[8] = abb[4] * z[8];
z[43] = z[39] + z[43] + z[78];
z[43] = 3 * z[43];
z[74] = abb[45] * (T(5) / T(4));
z[81] = abb[41] + z[36] + -z[66] + z[74];
z[81] = abb[7] * z[81];
z[84] = abb[8] * z[15];
z[8] = z[8] + -z[43] + -z[49] + z[55] + z[81] + -z[84];
z[49] = -abb[30] * z[8];
z[14] = abb[45] + z[11] + -z[14];
z[81] = -abb[4] * z[14];
z[85] = -abb[48] + z[10];
z[87] = -abb[47] + z[24] + z[85];
z[87] = abb[11] * z[87];
z[88] = (T(-1) / T(2)) * z[34] + z[87];
z[89] = 3 * abb[47];
z[90] = 2 * abb[44] + -z[89];
z[91] = abb[41] + -abb[45] + -z[10] + -z[90];
z[92] = 2 * abb[7];
z[91] = z[91] * z[92];
z[83] = z[83] + z[90];
z[93] = -abb[8] * z[83];
z[67] = z[35] + -z[55] + -z[67] + z[81] + 3 * z[88] + z[91] + z[93];
z[67] = abb[31] * z[67];
z[81] = abb[24] * z[17];
z[32] = z[32] + z[81];
z[81] = z[26] + z[32] + z[34];
z[88] = z[71] + z[81];
z[47] = (T(3) / T(2)) * z[47];
z[91] = abb[4] * z[29];
z[50] = -4 * z[35] + z[47] + z[50] + (T(3) / T(2)) * z[88] + z[91];
z[50] = abb[32] * z[50];
z[88] = abb[4] + -abb[13];
z[88] = z[64] * z[88];
z[93] = abb[28] * z[83];
z[83] = abb[29] * z[83];
z[93] = -z[83] + z[93];
z[94] = abb[32] * z[15];
z[95] = 2 * z[93] + -z[94];
z[95] = abb[8] * z[95];
z[96] = abb[28] + -abb[29];
z[97] = 3 * abb[6];
z[98] = abb[44] + abb[48] + z[21];
z[98] = -abb[47] + (T(1) / T(2)) * z[98];
z[99] = z[96] * z[97] * z[98];
z[100] = abb[32] * z[28];
z[93] = z[93] + z[100];
z[92] = z[92] * z[93];
z[93] = -z[75] + z[76];
z[93] = abb[1] * z[93];
z[19] = -z[19] + z[87];
z[101] = -z[19] * z[96];
z[6] = z[6] + z[49] + -z[50] + z[67] + z[88] + z[92] + z[93] + z[95] + -z[99] + 3 * z[101];
z[6] = abb[31] * z[6];
z[49] = z[5] + z[13];
z[67] = abb[46] * (T(-1) / T(2)) + -z[4] + z[49];
z[67] = abb[9] * z[67];
z[27] = (T(-1) / T(4)) * z[26] + -z[27] + -z[39] + z[41] + z[67];
z[39] = 3 * abb[46];
z[41] = abb[44] * (T(5) / T(4)) + -z[22] + z[39];
z[88] = abb[41] * (T(5) / T(4));
z[49] = z[41] + -z[49] + -z[88];
z[49] = abb[0] * z[49];
z[57] = abb[4] * z[57];
z[14] = abb[7] * z[14];
z[92] = abb[8] * z[9];
z[93] = -abb[49] + z[68];
z[93] = abb[46] + z[0] + (T(1) / T(2)) * z[93];
z[95] = abb[3] * z[93];
z[14] = z[14] + 3 * z[27] + -z[48] + z[49] + -z[56] + z[57] + z[60] + z[92] + (T(3) / T(2)) * z[95];
z[14] = abb[27] * z[14];
z[27] = -abb[46] + abb[49] + z[85];
z[27] = abb[9] * z[27];
z[56] = z[27] + -z[95];
z[57] = -z[56] + z[78];
z[60] = -abb[29] * z[57];
z[60] = z[60] + -z[79];
z[78] = -z[10] + z[39];
z[79] = 2 * abb[49];
z[85] = z[24] + z[78] + -z[79];
z[92] = 2 * z[85];
z[101] = abb[29] * z[92];
z[101] = z[63] + z[101];
z[101] = abb[13] * z[101];
z[100] = -z[64] + -2 * z[100];
z[100] = abb[7] * z[100];
z[64] = -z[64] + z[94];
z[64] = abb[8] * z[64];
z[58] = z[58] + -z[76];
z[58] = abb[4] * z[58];
z[14] = z[14] + z[50] + z[58] + 3 * z[60] + z[64] + -z[77] + z[100] + z[101];
z[14] = abb[27] * z[14];
z[50] = z[70] + z[81];
z[58] = (T(-1) / T(2)) * z[69];
z[60] = abb[8] * z[58];
z[30] = z[30] + -z[35];
z[29] = abb[7] * z[29];
z[64] = (T(1) / T(2)) * z[33];
z[29] = -z[29] + z[30] + (T(1) / T(2)) * z[50] + -z[60] + z[64] + z[91];
z[60] = abb[57] * z[29];
z[69] = abb[13] * z[93];
z[69] = z[69] + -z[95];
z[32] = -z[32] + -z[71];
z[68] = abb[46] + -z[11] + (T(1) / T(2)) * z[68];
z[68] = abb[0] * z[68];
z[30] = -z[30] + (T(1) / T(2)) * z[32] + z[64] + z[68] + -z[69];
z[30] = abb[33] * z[30];
z[32] = abb[15] + abb[17];
z[32] = z[9] * z[32];
z[64] = abb[16] + abb[18];
z[68] = (T(1) / T(4)) * z[28];
z[64] = z[64] * z[68];
z[68] = abb[25] * z[17];
z[40] = abb[14] * z[40];
z[32] = z[32] + z[40] + z[64] + (T(1) / T(4)) * z[68];
z[40] = abb[0] * (T(3) / T(4));
z[64] = -z[31] * z[40];
z[64] = -z[32] + z[64];
z[64] = abb[58] * z[64];
z[68] = -z[26] + z[72];
z[17] = abb[21] * z[17];
z[70] = z[17] + -z[68];
z[70] = (T(1) / T(2)) * z[70] + z[95];
z[71] = prod_pow(abb[29], 2);
z[70] = z[70] * z[71];
z[72] = -z[27] + -z[87];
z[72] = abb[29] * z[72];
z[42] = -z[42] + -z[67];
z[42] = abb[28] * z[42];
z[42] = z[42] + z[72];
z[42] = abb[28] * z[42];
z[67] = prod_pow(abb[32], 2);
z[72] = z[67] + -z[71];
z[21] = abb[46] + z[21];
z[21] = abb[10] * z[21];
z[72] = z[21] * z[72];
z[30] = z[30] + z[42] + -z[60] + z[64] + (T(1) / T(2)) * z[70] + z[72];
z[42] = abb[13] * z[92];
z[60] = abb[4] * z[15];
z[15] = -abb[7] * z[15];
z[15] = z[15] + z[42] + -z[55] + -3 * z[57] + z[60] + -z[84];
z[15] = abb[37] * z[15];
z[57] = 4 * z[10] + -z[39] + -z[51] + z[79] + z[82] + z[90];
z[57] = abb[29] * z[57];
z[64] = abb[46] * (T(3) / T(2));
z[70] = -z[10] + z[64];
z[72] = abb[47] * (T(3) / T(2));
z[0] = z[0] + z[61] + -z[70] + -z[72];
z[76] = abb[28] * z[0];
z[57] = z[57] + z[76];
z[57] = abb[28] * z[57];
z[0] = z[0] * z[71];
z[76] = 3 * abb[34];
z[77] = 3 * abb[56];
z[81] = z[76] + z[77];
z[54] = z[54] * z[81];
z[81] = 3 * abb[35];
z[82] = abb[46] + abb[47];
z[84] = -abb[48] + z[82];
z[84] = z[81] * z[84];
z[0] = z[0] + z[54] + z[57] + z[84];
z[0] = abb[1] * z[0];
z[18] = -z[18] + -z[22] + z[37] + -z[38] + -z[70];
z[18] = abb[28] * z[18];
z[22] = -abb[29] * z[85];
z[18] = z[18] + z[22];
z[18] = abb[28] * z[18];
z[22] = -abb[49] + z[2] + -z[5] + z[64];
z[22] = z[22] * z[71];
z[18] = z[18] + z[22];
z[18] = abb[13] * z[18];
z[4] = z[4] + -z[10];
z[2] = 2 * abb[46] + abb[49] * (T(-3) / T(2)) + z[2] + z[4];
z[2] = abb[13] * z[2];
z[2] = z[2] + z[56];
z[20] = -z[2] + z[20] + -z[60];
z[20] = z[20] * z[76];
z[22] = abb[49] * (T(7) / T(4));
z[37] = z[5] + z[22] + -z[41] + -z[74];
z[37] = abb[13] * z[37];
z[41] = -z[35] + z[59];
z[54] = z[41] + z[73];
z[37] = z[37] + z[49] + (T(1) / T(2)) * z[54] + -z[91];
z[37] = z[37] * z[67];
z[5] = -z[5] + z[13] + -z[24] + z[72];
z[24] = abb[28] * z[5];
z[24] = z[24] + z[83];
z[24] = abb[28] * z[24];
z[49] = z[81] * z[98];
z[4] = abb[45] * (T(3) / T(2)) + -z[4];
z[13] = -2 * abb[47] + abb[44] * (T(3) / T(2)) + z[4] + -z[13];
z[54] = z[13] * z[77];
z[24] = z[24] + z[49] + z[54];
z[5] = z[5] * z[71];
z[49] = abb[33] * (T(3) / T(2)) + (T(5) / T(4)) * z[67];
z[28] = z[28] * z[49];
z[49] = abb[58] * z[31];
z[5] = z[5] + z[24] + z[28] + -3 * z[49];
z[5] = abb[7] * z[5];
z[10] = abb[45] * (T(11) / T(4)) + z[10] + -z[22] + -z[23] + -z[72];
z[10] = z[10] * z[71];
z[4] = -z[4] + z[61];
z[22] = 3 * abb[36];
z[4] = z[4] * z[22];
z[23] = -z[62] * z[67];
z[4] = z[4] + z[10] + z[23] + z[24];
z[4] = abb[8] * z[4];
z[10] = -z[16] + z[60] + z[87];
z[23] = -z[10] * z[77];
z[24] = abb[4] * z[58];
z[2] = -z[2] + z[24] + (T(-1) / T(2)) * z[68];
z[2] = z[2] * z[22];
z[9] = abb[28] * z[9];
z[9] = z[9] + z[75];
z[9] = abb[28] * z[9];
z[22] = z[62] * z[71];
z[9] = z[9] + z[22] + (T(-9) / T(4)) * z[49];
z[9] = abb[4] * z[9];
z[17] = z[17] + z[26];
z[17] = (T(1) / T(2)) * z[17] + -z[69];
z[17] = z[17] * z[81];
z[22] = prod_pow(abb[28], 2);
z[22] = z[22] + -z[71];
z[22] = -abb[35] + -abb[56] + (T(1) / T(2)) * z[22];
z[22] = z[22] * z[97] * z[98];
z[24] = abb[26] * z[49];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[9] + z[14] + z[15] + z[17] + z[18] + z[20] + z[22] + z[23] + 6 * z[24] + 3 * z[30] + z[37] + z[44];
z[2] = -z[12] + z[52];
z[1] = z[1] + -z[2] + -z[66] + -z[74] + z[88];
z[1] = abb[4] * z[1];
z[3] = -z[11] + z[36] + -z[38] + z[66];
z[3] = abb[7] * z[3];
z[2] = abb[45] * (T(5) / T(2)) + z[2] + -z[61];
z[4] = -abb[8] * z[2];
z[5] = 2 * z[55];
z[1] = z[1] + z[3] + z[4] + -z[5] + z[41] + z[43] + -z[45] + z[48];
z[1] = abb[30] * z[1];
z[3] = abb[49] + z[7] + -z[78];
z[4] = 2 * abb[0];
z[3] = z[3] * z[4];
z[4] = z[3] + 5 * z[35] + z[42] + -z[47] + (T(-3) / T(2)) * z[50] + -z[59] + z[91];
z[4] = abb[32] * z[4];
z[6] = -abb[31] * z[8];
z[8] = 2 * z[27];
z[9] = z[8] + z[33] + z[34];
z[7] = z[7] + z[53] + -z[79];
z[11] = -abb[4] + abb[7];
z[7] = z[7] * z[11];
z[3] = z[3] + z[5] + z[7] + -3 * z[9] + -z[42] + z[46];
z[3] = abb[27] * z[3];
z[5] = -abb[44] + -4 * abb[45] + -z[52] + z[79] + z[89];
z[5] = z[5] * z[86];
z[7] = abb[32] * z[2];
z[5] = z[5] + z[7] + -z[63];
z[5] = abb[8] * z[5];
z[7] = z[19] + z[80];
z[7] = abb[29] * z[7];
z[8] = z[8] + z[16] + z[87];
z[8] = abb[28] * z[8];
z[7] = z[7] + z[8];
z[2] = -abb[29] * z[2];
z[2] = z[2] + z[63];
z[2] = abb[4] * z[2];
z[8] = z[25] + z[52];
z[9] = -z[8] + z[39] + z[89];
z[9] = z[9] * z[86];
z[8] = -z[8] + -z[51] + 6 * z[82];
z[8] = abb[28] * z[8];
z[8] = z[8] + z[9];
z[8] = abb[1] * z[8];
z[9] = 2 * abb[13];
z[9] = z[9] * z[85] * z[96];
z[11] = abb[29] + -abb[32];
z[11] = z[11] * z[21];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + 3 * z[7] + z[8] + z[9] + 6 * z[11] + -z[65] + -z[99];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[39] * z[29];
z[3] = -abb[7] + 2 * abb[26] + abb[4] * (T(-3) / T(4)) + -z[40];
z[3] = z[3] * z[31];
z[3] = z[3] + -z[32];
z[3] = abb[40] * z[3];
z[4] = abb[7] + abb[8];
z[4] = z[4] * z[13];
z[5] = -abb[6] * z[98];
z[4] = z[4] + z[5] + -z[10] + z[55];
z[4] = abb[38] * z[4];
z[2] = z[2] + z[3] + z[4];
z[1] = z[1] + 3 * z[2];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_323_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("10.269512493905512545637669731671240915263504226021180975992455577"),stof<T>("-58.554694814178373260477368471541400794702884064369990307851491817")}, std::complex<T>{stof<T>("-87.808318409424196712863513489722099758364160450913732976760279726"),stof<T>("12.043475572765425361043016237067971883919211911958527719860594222")}, std::complex<T>{stof<T>("-43.904159204712098356431756744861049879182080225456866488380139863"),stof<T>("6.021737786382712680521508118533985941959605955979263859930297111")}, std::complex<T>{stof<T>("-17.470784366395097099421394687436125969479083356039415516819461543"),stof<T>("42.993029214271078330685831479865254404261186486712346526888485666")}, std::complex<T>{stof<T>("-38.148814222331855297655024941017786507008320111885051484873249098"),stof<T>("8.117688644546420305100701584260566079796731294693622101679823984")}, std::complex<T>{stof<T>("65.477352306750047753539962526075242143578640739751693118275195217"),stof<T>("42.537230464296366813635157249429466831643957923240539923670222409")}, std::complex<T>{stof<T>("87.462304184670414339822295141339345656147468208240474607580221573"),stof<T>("-61.106444422316792402107236167703768505157237966556155152819281946")}, std::complex<T>{stof<T>("-26.087360613563218883969143709042169807486304626744192602380620166"),stof<T>("12.091677421663001390899896569304528158936445523864544766000499169")}, std::complex<T>{stof<T>("-5.7553449823802430587767318038432633721737601135718150035068907648"),stof<T>("-2.0959508581637076245791934657265801378371253387143582417495268727")}, std::complex<T>{stof<T>("-16.512200902804655404527292095446812870862691003762646777629453187"),stof<T>("24.063143698697434894401075584092701938234885903411332628943547409")}, std::complex<T>{stof<T>("-16.512200902804655404527292095446812870862691003762646777629453187"),stof<T>("24.063143698697434894401075584092701938234885903411332628943547409")}, std::complex<T>{stof<T>("-1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("-1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("-1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("1.893635402193665897979852592125278317152816937002069288848804013")}, std::complex<T>{stof<T>("-1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("1.893635402193665897979852592125278317152816937002069288848804013")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[23].real()/kbase.W[23].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[124].real()/kbase.W[124].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_323_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_323_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("2.048661529930890250814984560956898326304568318487319572884966684"),stof<T>("78.33165179290404870354153341096227691479327643828139069085250629")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,59> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W18(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[23].real()/k.W[23].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[124].real()/k.W[124].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_323_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_323_DLogXconstant_part(base_point<T>, kend);
	value += f_4_323_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_323_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_323_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_323_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_323_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_323_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_323_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
