/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_531.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_531_abbreviated (const std::array<T,30>& abb) {
T z[26];
z[0] = abb[23] + -abb[25];
z[1] = abb[1] * z[0];
z[2] = abb[24] * (T(1) / T(2)) + z[0];
z[2] = abb[3] * z[2];
z[2] = z[1] + z[2];
z[3] = abb[19] + abb[21];
z[4] = -abb[20] + 2 * abb[26];
z[5] = -abb[22] + 4 * z[4];
z[6] = -z[3] + (T(1) / T(3)) * z[5];
z[6] = abb[7] * z[6];
z[7] = abb[22] * (T(-1) / T(4)) + z[4];
z[8] = (T(-1) / T(4)) * z[3] + (T(1) / T(3)) * z[7];
z[8] = abb[8] * z[8];
z[4] = abb[22] * (T(-1) / T(2)) + 2 * z[4];
z[9] = (T(-1) / T(2)) * z[3] + (T(1) / T(3)) * z[4];
z[9] = abb[9] * z[9];
z[10] = (T(1) / T(2)) * z[0];
z[11] = abb[6] * z[10];
z[12] = abb[24] + abb[25];
z[12] = abb[4] * z[12];
z[13] = -abb[25] + abb[23] * (T(1) / T(3));
z[13] = abb[2] * z[13];
z[14] = -abb[24] + abb[23] * (T(-13) / T(12)) + abb[25] * (T(1) / T(12));
z[14] = abb[0] * z[14];
z[2] = (T(1) / T(3)) * z[2] + z[6] + 5 * z[8] + z[9] + -z[11] + (T(-5) / T(6)) * z[12] + (T(1) / T(4)) * z[13] + z[14];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[4] = (T(-3) / T(2)) * z[3] + z[4];
z[6] = abb[7] * z[4];
z[8] = abb[3] * (T(1) / T(2));
z[9] = z[0] * z[8];
z[9] = z[6] + z[9];
z[10] = abb[0] * z[10];
z[13] = z[9] + z[10];
z[14] = abb[9] * z[4];
z[15] = abb[6] * z[0];
z[16] = -z[14] + (T(3) / T(2)) * z[15];
z[17] = 2 * abb[1];
z[17] = z[0] * z[17];
z[18] = abb[2] * z[0];
z[17] = z[17] + z[18];
z[19] = -z[13] + -z[16] + 2 * z[17];
z[19] = abb[14] * z[19];
z[7] = (T(-3) / T(4)) * z[3] + z[7];
z[20] = abb[9] * z[7];
z[15] = (T(-3) / T(4)) * z[15] + z[20];
z[20] = abb[7] * z[7];
z[21] = (T(1) / T(4)) * z[0];
z[22] = abb[3] * z[21];
z[20] = z[20] + z[22];
z[21] = abb[0] * z[21];
z[21] = -z[15] + -z[17] + z[20] + z[21];
z[22] = abb[13] * z[21];
z[19] = z[19] + z[22];
z[21] = abb[12] * z[21];
z[21] = -z[19] + z[21];
z[4] = abb[8] * z[4];
z[22] = z[4] + -z[12];
z[23] = z[1] + (T(1) / T(2)) * z[18];
z[24] = abb[24] + abb[25] * (T(-1) / T(4)) + abb[23] * (T(5) / T(4));
z[24] = abb[0] * z[24];
z[15] = -z[15] + -z[20] + -z[22] + -z[23] + z[24];
z[15] = abb[11] * z[15];
z[15] = z[15] + -z[21];
z[15] = abb[11] * z[15];
z[20] = abb[8] * z[7];
z[18] = -3 * z[1] + (T(-5) / T(2)) * z[18];
z[8] = abb[24] * z[8];
z[18] = -z[8] + -z[12] + (T(1) / T(2)) * z[18] + -z[20] + z[24];
z[18] = prod_pow(abb[14], 2) * z[18];
z[24] = abb[7] + -abb[9];
z[0] = (T(3) / T(4)) * z[0];
z[0] = z[0] * z[24];
z[24] = 3 * z[7];
z[25] = abb[0] + abb[3];
z[24] = z[24] * z[25];
z[3] = 3 * z[3] + -z[5];
z[5] = -abb[2] + 2 * abb[10];
z[5] = z[3] * z[5];
z[7] = abb[6] * z[7];
z[0] = z[0] + z[5] + z[7] + z[24];
z[5] = abb[28] * z[0];
z[7] = abb[2] * abb[23];
z[1] = -z[1] + -2 * z[7] + z[13];
z[1] = abb[12] * z[1];
z[1] = z[1] + -z[19];
z[1] = abb[12] * z[1];
z[7] = z[11] + -z[23];
z[7] = 3 * z[7] + -z[14];
z[11] = abb[25] * (T(1) / T(2));
z[13] = abb[24] + abb[23] * (T(-1) / T(2)) + z[11];
z[13] = abb[3] * z[13];
z[6] = -z[4] + -z[6] + -z[7] + z[13];
z[6] = abb[15] * z[6];
z[10] = -z[10] + z[23];
z[13] = -abb[3] * abb[24];
z[4] = z[4] + z[10] + z[13];
z[4] = abb[14] * z[4];
z[8] = -z[8] + (T(1) / T(2)) * z[10] + z[20];
z[8] = abb[13] * z[8];
z[4] = z[4] + z[8];
z[4] = abb[13] * z[4];
z[8] = abb[24] + abb[23] * (T(3) / T(2)) + -z[11];
z[8] = abb[0] * z[8];
z[7] = z[7] + z[8] + -z[22];
z[8] = -abb[27] * z[7];
z[1] = abb[29] + z[1] + z[2] + z[4] + z[5] + z[6] + z[8] + z[15] + z[18];
z[2] = -abb[8] * z[3];
z[3] = -2 * abb[24] + abb[23] * (T(-5) / T(2)) + z[11];
z[3] = abb[0] * z[3];
z[2] = z[2] + z[3] + z[9] + -2 * z[12] + -z[16] + z[17];
z[2] = abb[11] * z[2];
z[2] = z[2] + z[21];
z[2] = m1_set::bc<T>[0] * z[2];
z[0] = abb[17] * z[0];
z[3] = -abb[16] * z[7];
z[0] = abb[18] + z[0] + z[2] + z[3];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_531_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-3.133975418202266643558147331268058698164979717370536535565281151"),stof<T>("14.940085422420628914799963768915380190763079305112406821302817017")}, std::complex<T>{stof<T>("-4.178633890936355524744196441690744930886639623160715380753708202"),stof<T>("19.920113896560838553066618358553840254350772406816542428403756023")}, std::complex<T>{stof<T>("-3.133975418202266643558147331268058698164979717370536535565281151"),stof<T>("14.940085422420628914799963768915380190763079305112406821302817017")}, std::complex<T>{stof<T>("-1.0446584727340888811860491104226862327216599057901788451884270504"),stof<T>("4.9800284741402096382666545896384600635876931017041356071009390058")}, std::complex<T>{stof<T>("-9.439962838033076084443857239233543068292819518527908957068107646"),stof<T>("11.503301599144359277331882382491209212882318268364094153005434161")}, std::complex<T>{stof<T>("-10.459339841805491214104175147117687084184823990731365716380963146"),stof<T>("13.607267345876588693832657501666255175430300428594528940729547323")}, std::complex<T>{stof<T>("-2.0976829218985263592097394643573927155602778625922623826286278938"),stof<T>("2.1039657467322294165007751191750459625479821602304347877241131616")}, std::complex<T>{stof<T>("8.357267781872711049488392883381489861773279246321430761507416403"),stof<T>("-39.840227793121677106133236717107680508701544813633084856807512047")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[124].real()/kbase.W[124].real()), rlog(k.W[195].real()/kbase.W[195].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_531_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_531_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (-8 + -3 * v[0] + -v[1] + -3 * v[2] + v[3] + 4 * v[4] + 4 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + 3 * v[5])) / prod_pow(tend, 2);
c[1] = ((1 + 2 * m1_set::bc<T>[1]) * (T(-1) / T(2)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[23] + abb[24] + -abb[25]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[13], 2);
z[1] = -abb[13] + abb[14] * (T(3) / T(2));
z[1] = abb[14] * z[1];
z[0] = abb[15] + (T(-1) / T(2)) * z[0] + z[1];
z[1] = abb[23] + abb[24] + -abb[25];
return abb[5] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_531_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_531_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-7.2283651339613855442054378870254758741519914445991089874898264432"),stof<T>("-11.54256942681906380840258338699372926839284184512541650985778916")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,30> abb = {dl[0], dlog_W4(k,dl), dlog_W7(k,dl), dlog_W10(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W29(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlr[2], f_1_1(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_15(k), f_2_2_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[124].real()/k.W[124].real()), rlog(kend.W[195].real()/k.W[195].real()), f_2_2_re(k), f_2_25_re(k), T{0}};
abb[18] = SpDLog_f_4_531_W_17_Im(t, path, abb);
abb[29] = SpDLog_f_4_531_W_17_Re(t, path, abb);

                    
            return f_4_531_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_531_DLogXconstant_part(base_point<T>, kend);
	value += f_4_531_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_531_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_531_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_531_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_531_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_531_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_531_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
