/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_31.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_31_abbreviated (const std::array<T,17>& abb) {
T z[14];
z[0] = 2 * abb[6];
z[1] = abb[7] + -abb[8];
z[0] = z[0] * z[1];
z[2] = -abb[7] + 2 * abb[8];
z[2] = abb[7] * z[2];
z[3] = 2 * abb[16];
z[4] = prod_pow(abb[8], 2);
z[2] = -abb[9] + -z[0] + -z[2] + -z[3] + z[4];
z[5] = abb[1] * z[2];
z[6] = prod_pow(abb[7], 2);
z[7] = 3 * z[4] + -z[6];
z[8] = -abb[6] + 3 * z[1];
z[8] = abb[6] * z[8];
z[9] = prod_pow(m1_set::bc<T>[0], 2);
z[10] = (T(2) / T(3)) * z[9];
z[7] = -3 * abb[9] + -abb[16] + (T(-1) / T(2)) * z[7] + -z[8] + z[10];
z[7] = abb[3] * z[7];
z[8] = z[3] + -z[6];
z[0] = 2 * abb[9] + z[0] + z[4] + z[8];
z[11] = abb[0] * z[0];
z[12] = prod_pow(abb[6], 2);
z[8] = z[8] + z[10] + z[12];
z[8] = abb[5] * z[8];
z[10] = -z[4] + z[6];
z[13] = abb[6] * z[1];
z[10] = abb[9] + abb[16] + (T(-1) / T(2)) * z[10] + z[13];
z[10] = abb[2] * z[10];
z[13] = abb[7] * abb[8];
z[4] = -z[4] + z[13];
z[4] = -abb[9] + 2 * z[4];
z[4] = abb[4] * z[4];
z[5] = z[4] + z[5] + z[7] + -z[8] + z[10] + z[11];
z[7] = abb[13] + -abb[14];
z[5] = -z[5] * z[7];
z[2] = abb[3] * z[2];
z[6] = -abb[16] + -z[6] + (T(7) / T(6)) * z[9] + z[12];
z[10] = abb[0] + -abb[5];
z[6] = -z[6] * z[10];
z[11] = abb[9] + z[12];
z[3] = -z[3] + (T(1) / T(6)) * z[9] + 2 * z[11];
z[9] = abb[1] * z[3];
z[11] = abb[2] * z[0];
z[2] = z[2] + z[4] + z[6] + -z[9] + z[11];
z[4] = abb[11] + -abb[12];
z[2] = -z[2] * z[4];
z[6] = abb[0] + -abb[1];
z[3] = z[3] * z[6];
z[0] = -abb[3] * z[0];
z[0] = z[0] + z[3] + -2 * z[8] + z[11];
z[0] = abb[15] * z[0];
z[0] = z[0] + z[2] + z[5];
z[2] = abb[7] * m1_set::bc<T>[0];
z[3] = abb[10] + z[2];
z[3] = abb[5] * z[3];
z[1] = m1_set::bc<T>[0] * z[1];
z[5] = abb[10] + z[1];
z[6] = abb[2] * z[5];
z[8] = abb[1] * abb[10];
z[9] = -abb[3] * z[5];
z[11] = -abb[0] * abb[10];
z[9] = -2 * z[3] + z[6] + z[8] + z[9] + z[11];
z[9] = abb[15] * z[9];
z[1] = abb[4] * z[1];
z[3] = z[1] + z[3] + z[8];
z[11] = -abb[7] + 3 * abb[8];
z[11] = m1_set::bc<T>[0] * z[11];
z[11] = -abb[10] + z[11];
z[11] = abb[3] * z[11];
z[5] = 2 * z[5];
z[5] = abb[0] * z[5];
z[3] = -2 * z[3] + z[5] + z[6] + z[11];
z[3] = -z[3] * z[7];
z[5] = abb[3] * abb[10];
z[1] = z[1] + z[5] + -z[6] + -z[8];
z[2] = -abb[10] + 2 * z[2];
z[2] = -z[2] * z[10];
z[1] = -2 * z[1] + z[2];
z[1] = -z[1] * z[4];
z[1] = z[1] + z[3] + 2 * z[9];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_31_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-1.2336919833744357587032356240808644257737988249459559960128816351"),stof<T>("-9.1174588560798001240076671028928954377750879326236074307441061956")}, std::complex<T>{stof<T>("1.2336919833744357587032356240808644257737988249459559960128816351"),stof<T>("9.1174588560798001240076671028928954377750879326236074307441061956")}, std::complex<T>{stof<T>("0.2663335300692475084027515580273526830887623817298575951106346787"),stof<T>("-5.70088458594914161109053074240705028557784691171449821243025061")}, std::complex<T>{stof<T>("-0.2663335300692475084027515580273526830887623817298575951106346787"),stof<T>("5.70088458594914161109053074240705028557784691171449821243025061")}, std::complex<T>{stof<T>("-31.472211494394455128107256109418940386978529379426070389522817104"),stof<T>("-19.743206069790808139077087813560942550894939419463439767378055971")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[26].real()/kbase.W[26].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_31_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_31_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-55.783731212032887089614983201183450092422408914170962560033487226"),stof<T>("26.484061140074606861532357515590517298560818207370372107869751073")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,17> abb = {dl[1], dl[3], dlog_W10(k,dl), dl[4], dlog_W21(k,dl), dlog_W27(k,dl), f_1_2(k), f_1_3(k), f_1_6(k), f_2_5(k), f_2_6_im(k), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[26].real()/k.W[26].real()), f_2_6_re(k)};

                    
            return f_4_31_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_31_DLogXconstant_part(base_point<T>, kend);
	value += f_4_31_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_31_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_31_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_31_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_31_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_31_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_31_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
