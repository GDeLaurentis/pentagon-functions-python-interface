/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_136.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_136_abbreviated (const std::array<T,31>& abb) {
T z[106];
z[0] = abb[1] + abb[4];
z[1] = abb[6] * (T(1) / T(2));
z[2] = 2 * abb[2];
z[3] = abb[3] + z[2];
z[4] = 3 * abb[0];
z[0] = (T(5) / T(6)) * z[0] + z[1] + (T(-4) / T(3)) * z[3] + -z[4];
z[0] = abb[22] * z[0];
z[5] = abb[0] * (T(1) / T(2));
z[6] = abb[2] + z[5];
z[7] = abb[3] * (T(5) / T(2));
z[8] = 2 * abb[5];
z[9] = z[6] + -z[7] + z[8];
z[10] = 2 * abb[4];
z[11] = abb[1] * (T(-13) / T(3)) + z[9] + z[10];
z[11] = abb[24] * z[11];
z[12] = abb[2] * (T(1) / T(2));
z[13] = 5 * abb[3];
z[14] = -abb[5] + z[12] + z[13];
z[15] = abb[0] * (T(3) / T(2));
z[16] = abb[1] * (T(5) / T(2));
z[14] = abb[4] * (T(-5) / T(2)) + (T(1) / T(3)) * z[14] + z[15] + z[16];
z[14] = abb[21] * z[14];
z[17] = 2 * abb[3];
z[18] = -abb[5] + z[17];
z[19] = 2 * abb[0];
z[20] = z[18] + -z[19];
z[20] = abb[1] + (T(1) / T(3)) * z[20];
z[20] = abb[26] * z[20];
z[21] = abb[0] + -abb[5];
z[22] = 3 * abb[2];
z[21] = -abb[3] + abb[1] * (T(-1) / T(3)) + (T(13) / T(3)) * z[21] + z[22];
z[21] = abb[4] * (T(-8) / T(3)) + (T(1) / T(2)) * z[21];
z[21] = abb[20] * z[21];
z[23] = 7 * abb[5];
z[24] = abb[2] * (T(-11) / T(2)) + z[13] + -z[23];
z[25] = 4 * abb[0];
z[24] = (T(1) / T(2)) * z[24] + -z[25];
z[24] = abb[1] * (T(5) / T(4)) + z[1] + (T(1) / T(3)) * z[24];
z[24] = abb[23] * z[24];
z[26] = 5 * abb[0] + -4 * abb[5] + abb[2] * (T(13) / T(2)) + z[7];
z[1] = -abb[1] + z[1] + (T(1) / T(3)) * z[26];
z[1] = abb[18] * z[1];
z[26] = abb[4] * abb[25];
z[27] = abb[2] + -abb[3];
z[28] = -abb[25] * z[27];
z[29] = abb[1] * abb[25];
z[7] = abb[4] * (T(-10) / T(3)) + abb[2] * (T(-3) / T(4)) + abb[5] * (T(4) / T(3)) + abb[0] * (T(8) / T(3)) + abb[1] * (T(49) / T(12)) + z[7];
z[7] = abb[19] * z[7];
z[30] = abb[25] + abb[26];
z[31] = -abb[24] + z[30];
z[31] = abb[20] + abb[19] * (T(-11) / T(6)) + abb[21] * (T(-2) / T(3)) + (T(-1) / T(3)) * z[31];
z[31] = abb[6] * z[31];
z[0] = z[0] + z[1] + z[7] + z[11] + z[14] + z[20] + z[21] + z[24] + (T(-1) / T(3)) * z[26] + (T(2) / T(3)) * z[28] + z[29] + z[31];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[1] = abb[0] + -abb[3];
z[7] = 2 * abb[1];
z[11] = z[1] + -z[7];
z[11] = abb[26] * z[11];
z[14] = abb[25] * z[7];
z[14] = z[14] + z[28];
z[20] = z[11] + -z[14];
z[21] = 4 * abb[3];
z[24] = -abb[4] + z[21];
z[31] = abb[5] * (T(3) / T(2));
z[32] = abb[1] * (T(13) / T(2));
z[33] = abb[2] + z[24] + -z[31] + z[32];
z[33] = abb[20] * z[33];
z[34] = 6 * abb[2];
z[35] = -37 * abb[3] + abb[5] * (T(55) / T(2));
z[35] = 11 * abb[4] + abb[1] * (T(-175) / T(4)) + z[19] + z[34] + (T(1) / T(2)) * z[35];
z[35] = abb[19] * z[35];
z[36] = 13 * abb[1];
z[37] = 6 * abb[4];
z[9] = -3 * z[9] + z[36] + -z[37];
z[9] = abb[24] * z[9];
z[38] = abb[0] + z[22];
z[39] = 4 * abb[4];
z[40] = 7 * abb[3];
z[41] = abb[1] * (T(-37) / T(2)) + abb[5] * (T(13) / T(2)) + z[38] + z[39] + -z[40];
z[41] = abb[21] * z[41];
z[30] = 3 * abb[20] + abb[24] + -z[30];
z[42] = abb[19] * (T(11) / T(2));
z[43] = -z[30] + z[42];
z[44] = 2 * abb[21];
z[45] = z[43] + z[44];
z[46] = abb[6] * z[45];
z[9] = z[9] + z[20] + z[33] + z[35] + z[41] + z[46];
z[9] = abb[14] * z[9];
z[33] = 15 * abb[1];
z[35] = -abb[5] + -z[4] + z[24] + z[33] + -z[34];
z[35] = abb[24] * z[35];
z[26] = z[26] + z[29];
z[29] = abb[1] + abb[5];
z[29] = abb[26] * z[29];
z[41] = z[26] + z[29];
z[46] = 4 * abb[1];
z[47] = z[17] + z[46];
z[48] = abb[2] + abb[4] + z[47];
z[48] = abb[20] * z[48];
z[49] = -z[41] + z[48];
z[50] = 4 * abb[2];
z[51] = z[19] + z[50];
z[52] = -z[8] + z[40];
z[53] = z[51] + -z[52];
z[54] = 37 * abb[1];
z[53] = -abb[4] + 2 * z[53] + -z[54];
z[53] = abb[19] * z[53];
z[35] = z[35] + 2 * z[49] + z[53];
z[35] = abb[13] * z[35];
z[49] = abb[5] + z[10];
z[53] = 7 * abb[1];
z[55] = z[21] + z[49] + z[53];
z[55] = abb[20] * z[55];
z[56] = 23 * abb[3] + abb[5] * (T(-25) / T(2)) + abb[1] * (T(101) / T(2)) + -z[50];
z[57] = 8 * abb[4];
z[58] = -z[25] + z[56] + -z[57];
z[58] = abb[19] * z[58];
z[59] = abb[0] + z[2];
z[60] = 3 * abb[3];
z[61] = z[8] + z[59] + -z[60];
z[61] = -20 * abb[1] + z[37] + 3 * z[61];
z[61] = abb[24] * z[61];
z[29] = z[14] + z[29];
z[29] = 2 * z[29];
z[58] = z[29] + -z[55] + z[58] + z[61];
z[58] = abb[12] * z[58];
z[26] = -z[11] + z[26];
z[48] = -z[26] + z[48];
z[48] = 2 * z[48];
z[61] = abb[0] + -z[50] + z[52];
z[62] = 5 * abb[4];
z[54] = z[54] + 2 * z[61] + -z[62];
z[54] = abb[19] * z[54];
z[61] = z[2] + -z[17];
z[63] = abb[0] + abb[5];
z[64] = z[61] + z[63];
z[65] = 3 * abb[4];
z[64] = -17 * abb[1] + 3 * z[64] + z[65];
z[64] = abb[24] * z[64];
z[66] = -z[53] + z[61];
z[67] = -abb[5] + -z[66];
z[67] = z[44] * z[67];
z[54] = -z[48] + z[54] + z[64] + z[67];
z[54] = abb[11] * z[54];
z[64] = 23 * abb[1];
z[23] = -10 * abb[3] + z[23] + -z[64];
z[39] = z[2] + z[39];
z[67] = -z[19] + -z[23] + -z[39];
z[67] = abb[12] * z[67];
z[66] = -abb[4] + z[63] + z[66];
z[68] = 2 * abb[13];
z[66] = z[66] * z[68];
z[66] = z[66] + z[67];
z[66] = abb[21] * z[66];
z[30] = 11 * abb[19] + 4 * abb[21] + -2 * z[30];
z[67] = abb[6] * z[30];
z[69] = abb[12] + -abb[13];
z[70] = abb[11] + z[69];
z[71] = z[67] * z[70];
z[9] = z[9] + z[35] + z[54] + z[58] + z[66] + -z[71];
z[9] = abb[14] * z[9];
z[35] = abb[6] * (T(3) / T(2));
z[54] = -abb[0] + z[10];
z[58] = abb[3] * (T(1) / T(2));
z[66] = -abb[2] + abb[1] * (T(15) / T(2)) + -z[35] + z[54] + -z[58];
z[66] = abb[14] * z[66];
z[72] = 3 * abb[6];
z[73] = z[70] * z[72];
z[27] = -abb[1] + 2 * z[27] + z[62];
z[74] = abb[13] * z[27];
z[75] = z[2] + z[10];
z[76] = abb[3] + z[75];
z[77] = 5 * abb[1];
z[78] = -z[76] + -z[77];
z[78] = abb[12] * z[78];
z[79] = abb[1] + z[62];
z[80] = -abb[2] + -z[1];
z[80] = -z[79] + 2 * z[80];
z[80] = abb[11] * z[80];
z[66] = z[66] + z[73] + z[74] + z[78] + z[80];
z[66] = abb[14] * z[66];
z[74] = z[5] + z[7];
z[62] = abb[3] * (T(7) / T(2)) + -z[2] + -z[62] + z[74];
z[62] = abb[13] * z[62];
z[78] = abb[0] * (T(5) / T(2));
z[80] = abb[1] + -z[58] + z[75] + z[78];
z[80] = abb[12] * z[80];
z[81] = abb[2] + -z[5] + -z[58];
z[81] = -z[7] + z[65] + 3 * z[81];
z[81] = abb[11] * z[81];
z[62] = z[62] + z[80] + z[81];
z[62] = abb[11] * z[62];
z[80] = abb[12] * z[69];
z[70] = abb[11] * z[70];
z[70] = z[70] + z[80];
z[80] = -abb[29] + (T(1) / T(2)) * z[70];
z[80] = z[72] * z[80];
z[81] = prod_pow(abb[13], 2);
z[82] = abb[0] + -abb[1];
z[82] = z[81] * z[82];
z[83] = -abb[0] + -z[60];
z[83] = -abb[1] + (T(1) / T(2)) * z[83];
z[83] = abb[13] * z[83];
z[84] = abb[1] * (T(1) / T(2));
z[85] = abb[0] + abb[3] * (T(3) / T(2)) + z[84];
z[85] = abb[12] * z[85];
z[83] = z[83] + z[85];
z[83] = abb[12] * z[83];
z[85] = -abb[29] * z[27];
z[86] = abb[15] * (T(3) / T(2));
z[87] = abb[7] + abb[9];
z[87] = z[86] * z[87];
z[62] = z[62] + z[66] + -z[80] + z[82] + z[83] + z[85] + z[87];
z[62] = abb[22] * z[62];
z[66] = abb[0] + z[17];
z[82] = -abb[2] + z[66];
z[83] = abb[5] * (T(1) / T(2));
z[85] = abb[1] * (T(9) / T(2)) + -z[72] + z[82] + z[83];
z[85] = abb[23] * z[85];
z[87] = 6 * abb[3] + -abb[5];
z[33] = -z[33] + z[51] + -z[87];
z[33] = abb[21] * z[33];
z[51] = 8 * abb[2] + -15 * abb[3] + abb[1] * (T(-69) / T(2)) + z[25] + z[83];
z[51] = abb[19] * z[51];
z[88] = abb[5] + z[60];
z[88] = abb[2] + z[53] + 2 * z[88];
z[88] = abb[20] * z[88];
z[89] = z[47] + z[63];
z[90] = -z[72] + z[89];
z[90] = abb[18] * z[90];
z[91] = z[1] + z[2];
z[92] = 14 * abb[1];
z[91] = 3 * z[91] + -z[92];
z[91] = abb[24] * z[91];
z[93] = -z[67] + z[91];
z[94] = abb[1] + abb[3];
z[95] = -abb[6] + z[94];
z[95] = 3 * z[95];
z[96] = abb[22] * z[95];
z[33] = -z[29] + z[33] + z[51] + z[85] + z[88] + z[90] + -z[93] + z[96];
z[51] = -abb[30] * z[33];
z[85] = z[17] + z[53];
z[88] = -z[12] + -z[37] + z[78] + -z[85];
z[88] = abb[19] * z[88];
z[90] = abb[25] * z[10];
z[11] = -z[11] + -z[28] + z[90];
z[28] = z[7] + -z[19] + z[65];
z[90] = 5 * abb[2];
z[96] = abb[3] + z[90];
z[96] = z[28] + (T(1) / T(2)) * z[96];
z[96] = abb[20] * z[96];
z[97] = -abb[0] + -abb[2];
z[97] = z[46] + z[65] + (T(3) / T(2)) * z[97];
z[97] = abb[24] * z[97];
z[98] = -abb[1] + -z[54];
z[98] = z[44] * z[98];
z[99] = abb[7] + abb[8];
z[100] = abb[27] * z[99];
z[88] = -z[11] + z[88] + z[96] + z[97] + z[98] + (T(-3) / T(2)) * z[100];
z[88] = abb[11] * z[88];
z[21] = abb[4] + z[21];
z[92] = abb[2] * (T(-9) / T(2)) + z[5] + z[21] + z[92];
z[92] = abb[19] * z[92];
z[96] = z[10] + z[46];
z[22] = -abb[3] + -z[22];
z[22] = (T(1) / T(2)) * z[22] + -z[96];
z[22] = abb[20] * z[22];
z[97] = -6 * abb[1] + abb[4] + abb[2] * (T(5) / T(2)) + z[5] + -z[17];
z[97] = abb[24] * z[97];
z[11] = z[11] + z[22] + z[92] + z[97];
z[11] = abb[13] * z[11];
z[22] = 11 * abb[1];
z[92] = abb[2] * (T(7) / T(2));
z[97] = -z[5] + 3 * z[18] + z[22] + -z[37] + -z[92];
z[97] = abb[24] * z[97];
z[98] = -z[8] + z[13];
z[101] = z[64] + -z[92] + 2 * z[98];
z[102] = abb[0] * (T(-7) / T(2)) + z[57] + -z[101];
z[102] = abb[19] * z[102];
z[103] = abb[2] + z[60];
z[103] = (T(1) / T(2)) * z[103];
z[96] = z[96] + z[103];
z[96] = abb[20] * z[96];
z[96] = z[20] + z[96] + z[97] + z[102];
z[96] = abb[12] * z[96];
z[49] = z[49] + -z[77] + -z[82];
z[49] = abb[12] * z[49];
z[97] = abb[4] + z[7];
z[102] = -abb[2] + z[97];
z[102] = abb[13] * z[102];
z[49] = z[49] + z[102];
z[49] = z[44] * z[49];
z[102] = 3 * abb[7] + abb[8];
z[102] = abb[12] * z[102];
z[104] = -abb[7] + abb[8];
z[104] = abb[13] * z[104];
z[102] = z[102] + z[104];
z[105] = abb[27] * z[102];
z[11] = z[11] + z[49] + z[88] + z[96] + (T(1) / T(2)) * z[105];
z[11] = abb[11] * z[11];
z[1] = z[1] + z[8];
z[1] = abb[26] * z[1];
z[1] = z[1] + z[14];
z[8] = abb[0] * (T(-9) / T(2)) + z[101];
z[8] = abb[19] * z[8];
z[14] = -9 * abb[1] + -z[18] + z[78] + z[92];
z[14] = abb[24] * z[14];
z[18] = -abb[2] + -z[40];
z[18] = (T(1) / T(2)) * z[18] + -z[46];
z[18] = abb[20] * z[18];
z[8] = z[1] + z[8] + z[14] + z[18];
z[8] = abb[13] * z[8];
z[14] = abb[5] * (T(5) / T(2));
z[16] = z[14] + z[16] + z[82];
z[16] = abb[20] * z[16];
z[18] = 13 * abb[3] + z[14];
z[38] = abb[1] * (T(-55) / T(4)) + (T(-1) / T(2)) * z[18] + z[38];
z[38] = abb[19] * z[38];
z[40] = -z[6] + z[58];
z[40] = 3 * z[40] + z[53];
z[40] = abb[24] * z[40];
z[1] = -z[1] + z[16] + z[38] + z[40];
z[1] = abb[12] * z[1];
z[1] = z[1] + z[8];
z[1] = abb[12] * z[1];
z[8] = z[2] + z[31] + z[32] + z[66];
z[16] = -abb[12] * z[8];
z[5] = abb[5] * (T(1) / T(4)) + abb[1] * (T(39) / T(4)) + -z[5] + -z[35] + -z[61];
z[5] = abb[14] * z[5];
z[38] = -abb[11] + abb[13];
z[38] = z[38] * z[89];
z[5] = z[5] + z[16] + z[38] + z[73];
z[5] = abb[14] * z[5];
z[16] = abb[1] + -abb[2];
z[16] = z[16] * z[81];
z[38] = abb[29] * z[94];
z[38] = -z[16] + 3 * z[38] + z[80];
z[40] = z[7] + z[63];
z[12] = z[12] + z[40] + z[58];
z[46] = abb[13] * z[12];
z[15] = abb[2] + abb[1] * (T(-1) / T(4)) + abb[5] * (T(5) / T(4)) + z[15];
z[15] = abb[12] * z[15];
z[15] = z[15] + -z[46];
z[15] = abb[12] * z[15];
z[12] = abb[12] * z[12];
z[49] = -abb[2] + z[60];
z[60] = z[7] + (T(1) / T(2)) * z[49];
z[66] = abb[13] * z[60];
z[78] = abb[1] + z[103];
z[78] = abb[11] * z[78];
z[12] = z[12] + -z[66] + z[78];
z[12] = abb[11] * z[12];
z[5] = z[5] + z[12] + z[15] + -z[38];
z[5] = abb[23] * z[5];
z[12] = -abb[9] * z[43];
z[15] = -abb[25] + abb[20] * (T(3) / T(2));
z[42] = 2 * abb[26] + abb[24] * (T(-7) / T(2)) + -z[15] + z[42];
z[42] = abb[8] * z[42];
z[43] = abb[8] + -abb[9];
z[43] = z[43] * z[44];
z[78] = -2 * abb[10] + z[19] + z[103];
z[80] = abb[27] * z[78];
z[15] = abb[26] + z[15];
z[15] = abb[7] * z[15];
z[82] = -abb[9] + z[99];
z[88] = abb[23] * z[82];
z[12] = z[12] + z[15] + z[42] + z[43] + z[80] + (T(-3) / T(2)) * z[88];
z[12] = abb[15] * z[12];
z[15] = z[17] + -z[35] + z[74] + -z[83];
z[15] = abb[14] * z[15];
z[35] = -z[69] * z[89];
z[7] = -abb[0] + z[7];
z[42] = abb[5] + z[7];
z[43] = z[17] + z[42];
z[69] = -abb[11] * z[43];
z[15] = z[15] + z[35] + z[69] + z[73];
z[15] = abb[14] * z[15];
z[35] = abb[2] * (T(-3) / T(2)) + z[42] + z[58];
z[35] = abb[12] * z[35];
z[42] = abb[11] * z[60];
z[35] = z[35] + z[42] + -z[66];
z[35] = abb[11] * z[35];
z[6] = abb[1] + z[6] + z[31];
z[6] = abb[12] * z[6];
z[6] = z[6] + -z[46];
z[6] = abb[12] * z[6];
z[31] = -z[82] * z[86];
z[6] = z[6] + z[15] + z[31] + z[35] + -z[38];
z[6] = abb[18] * z[6];
z[13] = z[13] + -z[50];
z[13] = 27 * abb[1] + 2 * z[13] + z[54];
z[13] = abb[19] * z[13];
z[15] = -abb[3] + z[2];
z[15] = -abb[0] + 10 * abb[1] + -2 * z[15] + z[65];
z[15] = abb[21] * z[15];
z[13] = z[13] + z[15] + -z[48];
z[15] = z[13] + z[91];
z[15] = abb[29] * z[15];
z[31] = abb[8] * z[81];
z[35] = abb[12] * z[104];
z[31] = -z[31] + (T(1) / T(2)) * z[35];
z[35] = abb[11] * z[99];
z[38] = 3 * z[35] + -z[102];
z[38] = abb[11] * z[38];
z[42] = -abb[15] * z[78];
z[38] = z[31] + (T(1) / T(2)) * z[38] + z[42];
z[38] = abb[28] * z[38];
z[42] = z[19] + -z[85] + z[90];
z[42] = abb[19] * z[42];
z[46] = -abb[2] + z[17];
z[48] = z[7] + z[46];
z[48] = abb[20] * z[48];
z[54] = -abb[2] + z[7];
z[54] = abb[24] * z[54];
z[42] = z[42] + z[48] + z[54];
z[42] = z[42] * z[81];
z[32] = abb[0] + -z[32] + -z[49] + z[83];
z[32] = abb[12] * z[32];
z[48] = -z[63] + z[77];
z[46] = z[46] + z[48];
z[46] = z[46] * z[68];
z[32] = z[32] + z[46];
z[32] = abb[12] * z[32];
z[16] = -2 * z[16] + z[32];
z[16] = abb[21] * z[16];
z[32] = z[45] * z[70];
z[30] = -abb[29] * z[30];
z[30] = z[30] + z[32];
z[30] = abb[6] * z[30];
z[31] = -abb[27] * z[31];
z[0] = z[0] + z[1] + z[5] + z[6] + z[9] + z[11] + z[12] + z[15] + z[16] + z[30] + z[31] + z[38] + z[42] + z[51] + z[62];
z[1] = z[2] + z[23] + z[37];
z[1] = abb[21] * z[1];
z[5] = 14 * abb[4] + -z[19] + -z[56];
z[5] = abb[19] * z[5];
z[6] = z[34] + z[57];
z[9] = 22 * abb[1] + 11 * abb[3] + -8 * abb[5] + -z[4] + -z[6];
z[9] = abb[24] * z[9];
z[1] = z[1] + z[5] + z[9] + 2 * z[20] + z[55] + z[67];
z[1] = abb[14] * z[1];
z[5] = -z[2] + -z[4] + -z[14] + z[84];
z[5] = abb[12] * z[5];
z[8] = z[8] + -z[72];
z[8] = abb[14] * z[8];
z[2] = z[2] + z[17] + z[40];
z[9] = abb[13] * z[2];
z[9] = z[9] + z[73];
z[2] = -abb[11] * z[2];
z[2] = z[2] + z[5] + z[8] + z[9];
z[2] = abb[23] * z[2];
z[5] = z[19] + z[53] + -z[72] + z[76];
z[5] = abb[14] * z[5];
z[8] = -z[25] + -z[75] + -z[94];
z[8] = abb[12] * z[8];
z[11] = abb[0] + -abb[2];
z[12] = -abb[3] + -z[11];
z[12] = 2 * z[12] + z[79];
z[12] = abb[13] * z[12];
z[14] = abb[1] + -z[34] + -z[65];
z[14] = abb[11] * z[14];
z[5] = z[5] + z[8] + z[12] + z[14] + z[73];
z[5] = abb[22] * z[5];
z[8] = -5 * abb[5] + -z[10] + -z[19] + z[61] + -z[77];
z[8] = abb[20] * z[8];
z[10] = abb[0] + -16 * abb[1] + z[34] + z[37] + -z[52];
z[10] = abb[24] * z[10];
z[6] = 6 * abb[0] + abb[1] * (T(55) / T(2)) + -z[6] + z[18];
z[6] = abb[19] * z[6];
z[6] = z[6] + z[8] + z[10] + z[29];
z[6] = abb[12] * z[6];
z[8] = 3 * abb[5];
z[4] = z[4] + z[8] + -z[21] + -z[36] + z[50];
z[4] = abb[24] * z[4];
z[3] = -z[3] + -z[28];
z[3] = abb[20] * z[3];
z[3] = z[3] + z[26] + z[100];
z[10] = -z[11] + z[98];
z[10] = 2 * z[10] + z[64] + z[65];
z[10] = abb[19] * z[10];
z[11] = abb[4] + z[17] + z[48];
z[12] = z[11] * z[44];
z[3] = 2 * z[3] + z[4] + z[10] + z[12];
z[3] = abb[11] * z[3];
z[4] = -z[7] + -z[8];
z[4] = abb[12] * z[4];
z[7] = -abb[5] + -z[47] + z[59];
z[7] = abb[11] * z[7];
z[8] = z[43] + -z[72];
z[8] = abb[14] * z[8];
z[4] = z[4] + z[7] + z[8] + z[9];
z[4] = abb[18] * z[4];
z[7] = z[22] + z[24] + -z[50] + -z[63];
z[7] = abb[24] * z[7];
z[8] = z[59] + z[97];
z[8] = abb[20] * z[8];
z[8] = z[8] + -z[41];
z[9] = -abb[2] + -z[98];
z[9] = -abb[4] + 2 * z[9] + -z[64];
z[9] = abb[19] * z[9];
z[7] = z[7] + 2 * z[8] + z[9];
z[7] = abb[13] * z[7];
z[8] = z[19] + z[36] + -z[39] + z[87];
z[8] = abb[12] * z[8];
z[9] = -z[11] * z[68];
z[8] = z[8] + z[9];
z[8] = abb[21] * z[8];
z[9] = abb[8] * abb[13];
z[10] = abb[7] * abb[12];
z[9] = z[9] + z[10];
z[10] = abb[27] * z[9];
z[9] = z[9] + -z[35];
z[9] = abb[28] * z[9];
z[9] = -z[9] + z[10];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + -2 * z[9] + -z[71];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[17] * z[33];
z[3] = -abb[18] + -abb[23];
z[3] = z[3] * z[95];
z[4] = -z[27] + z[72];
z[4] = abb[22] * z[4];
z[3] = z[3] + z[4] + z[13] + z[93];
z[3] = abb[16] * z[3];
z[1] = z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_136_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("19.41623453853780028698025779065661235934823321217577385896473805"),stof<T>("-51.593554194500100927367260964893535618269068177263476583711005089")}, std::complex<T>{stof<T>("-31.112408857449162162820902330596198586614579180223540198291252113"),stof<T>("-5.016219844894110565857842510082190904723122146683180677452108094")}, std::complex<T>{stof<T>("-29.006944527169033586830342480274432595735645797054048307194491421"),stof<T>("-61.472691819233626491026609157648456022345951093923711419439879435")}, std::complex<T>{stof<T>("-11.657725264505475646950364444636532108715978917685059532394633038"),stof<T>("-19.791254198795233876075082314214905519246455321759718700103307841")}, std::complex<T>{stof<T>("-30.172839827928545857140366050872256663071743432994220872410061409"),stof<T>("58.196347945882126691167650246402877571092708493470283800060931742")}, std::complex<T>{stof<T>("19.873653304818004945212052330386636653423004833149042258944224894"),stof<T>("18.343284375432144881595711913310615889901085718625894924797655502")}, std::complex<T>{stof<T>("-8.7625642998753829055653831503864181261679065796663289702162609523"),stof<T>("-4.6001936811189033580504661104371885262131050713405611816794491319")}, std::complex<T>{stof<T>("15.108892572745061412213892796520457261086464323838870134816341142"),stof<T>("8.354975580390796395232248397316582697642181201437503469674320267")}, std::complex<T>{stof<T>("16.068002281026542710461963058718137277520931394330965381239876208"),stof<T>("9.433678548141315075092910745826169394993738875455969195758510053")}, std::complex<T>{stof<T>("0.713164709064899454239662072765414699892430773621421191129285427"),stof<T>("15.748931692217380713457583612152853300165062016478596541063558734")}, std::complex<T>{stof<T>("-0.713164709064899454239662072765414699892430773621421191129285427"),stof<T>("-15.748931692217380713457583612152853300165062016478596541063558734")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[41].real()/kbase.W[41].real()), rlog(k.W[46].real()/kbase.W[46].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_136_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_136_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-20.089033506193447050221675173121441799805462296243676304679951539"),stof<T>("17.376668577651907018590661786128036544284007796029769455005504063")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,31> abb = {dl[0], dlog_W4(k,dl), dl[2], dl[4], dlog_W14(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_11(k), f_2_23(k), f_2_2_im(k), f_2_12_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[41].real()/k.W[41].real()), rlog(kend.W[46].real()/k.W[46].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_2_re(k), f_2_12_re(k)};

                    
            return f_4_136_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_136_DLogXconstant_part(base_point<T>, kend);
	value += f_4_136_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_136_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_136_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_136_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_136_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_136_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_136_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
