/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_316.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_316_abbreviated (const std::array<T,29>& abb) {
T z[70];
z[0] = 2 * abb[21];
z[1] = 5 * abb[19] + z[0];
z[2] = 2 * z[1];
z[3] = 3 * abb[18];
z[4] = z[2] + -z[3];
z[5] = -abb[24] + z[4];
z[6] = 3 * abb[20];
z[7] = abb[22] + z[6];
z[8] = z[5] + -z[7];
z[9] = -abb[6] * z[8];
z[10] = abb[8] * abb[26];
z[11] = z[9] + -z[10];
z[12] = abb[9] * abb[26];
z[13] = z[11] + z[12];
z[14] = abb[19] + abb[21];
z[14] = 2 * z[14];
z[15] = abb[18] + z[14];
z[16] = 2 * abb[0];
z[17] = -z[15] * z[16];
z[18] = 2 * abb[2];
z[19] = z[16] + z[18];
z[20] = 7 * abb[5] + z[19];
z[20] = abb[24] * z[20];
z[21] = abb[18] + abb[20];
z[22] = 2 * abb[19];
z[23] = abb[21] + z[22];
z[24] = 4 * z[23];
z[25] = z[21] + -z[24];
z[26] = -2 * z[25];
z[27] = -abb[22] + z[26];
z[28] = 5 * abb[24] + -z[27];
z[29] = 2 * abb[3];
z[30] = -z[28] * z[29];
z[31] = 7 * abb[24];
z[32] = -5 * abb[22] + -4 * z[25] + -z[31];
z[33] = 2 * abb[1];
z[32] = z[32] * z[33];
z[34] = abb[0] + abb[2];
z[35] = -10 * abb[1] + abb[6] + -z[29] + z[34];
z[35] = abb[23] * z[35];
z[36] = abb[5] * z[6];
z[37] = abb[0] * abb[22];
z[38] = 9 * abb[19] + 4 * abb[21];
z[38] = 2 * z[38];
z[39] = abb[18] + -z[38];
z[39] = abb[5] * z[39];
z[40] = abb[20] + z[14];
z[41] = abb[22] + -2 * z[40];
z[41] = abb[2] * z[41];
z[17] = z[13] + z[17] + z[20] + z[30] + z[32] + z[35] + z[36] + z[37] + z[39] + z[41];
z[17] = abb[14] * z[17];
z[20] = abb[20] + abb[22];
z[14] = z[14] + z[20];
z[14] = abb[2] * z[14];
z[30] = 3 * abb[5];
z[32] = z[19] + z[30];
z[32] = abb[24] * z[32];
z[28] = z[28] * z[33];
z[35] = 3 * abb[24];
z[39] = z[25] + z[35];
z[41] = z[29] * z[39];
z[15] = abb[0] * z[15];
z[42] = 2 * z[23];
z[43] = abb[18] + z[42];
z[44] = abb[5] * z[43];
z[14] = z[14] + z[15] + z[28] + -z[32] + z[41] + z[44];
z[28] = -z[12] + z[14];
z[32] = abb[11] + -abb[13];
z[28] = z[28] * z[32];
z[41] = z[10] * z[32];
z[45] = abb[6] * z[32];
z[46] = z[8] * z[45];
z[47] = abb[0] + abb[5];
z[48] = z[33] + z[47];
z[49] = z[32] * z[48];
z[49] = -z[45] + z[49];
z[49] = abb[23] * z[49];
z[28] = z[28] + z[41] + z[46] + z[49];
z[17] = z[17] + 2 * z[28];
z[17] = abb[14] * z[17];
z[28] = 2 * abb[5];
z[49] = 3 * abb[0] + z[28];
z[50] = -5 * abb[2] + -abb[6] + -z[29] + z[49];
z[50] = abb[23] * z[50];
z[4] = abb[5] * z[4];
z[51] = z[33] * z[39];
z[4] = -z[4] + -z[11] + z[36] + z[50] + z[51];
z[11] = abb[24] * (T(5) / T(2));
z[50] = -abb[18] + z[24];
z[52] = -z[7] + -z[11] + z[50];
z[52] = abb[3] * z[52];
z[53] = 2 * abb[20];
z[54] = -z[50] + -z[53];
z[54] = abb[2] * z[54];
z[55] = -abb[5] + z[18];
z[56] = abb[0] * (T(3) / T(2));
z[57] = z[55] + z[56];
z[57] = abb[24] * z[57];
z[58] = abb[7] * abb[25];
z[59] = -abb[18] + z[22];
z[59] = abb[0] * z[59];
z[60] = abb[7] * (T(1) / T(2));
z[61] = abb[9] + z[60];
z[61] = abb[26] * z[61];
z[52] = -z[4] + -z[37] + z[52] + z[54] + z[57] + -z[58] + z[59] + z[61];
z[52] = abb[12] * z[52];
z[48] = -abb[6] + z[48];
z[48] = abb[23] * z[48];
z[13] = -z[13] + z[14] + z[48];
z[14] = 2 * abb[14];
z[13] = z[13] * z[14];
z[14] = abb[1] * z[39];
z[14] = 4 * z[14];
z[39] = 3 * abb[19];
z[0] = z[0] + z[39];
z[48] = abb[0] * z[0];
z[48] = z[44] + z[48];
z[54] = abb[18] + -abb[19];
z[54] = z[18] * z[54];
z[28] = abb[2] * (T(1) / T(2)) + z[28];
z[56] = -z[28] + -z[56];
z[56] = z[35] * z[56];
z[57] = 6 * abb[24];
z[27] = -z[27] + z[57];
z[27] = abb[3] * z[27];
z[27] = z[14] + z[27] + z[37] + 2 * z[48] + z[54] + z[56];
z[27] = abb[13] * z[27];
z[14] = z[14] + 3 * z[37];
z[48] = abb[0] * abb[19];
z[54] = -z[44] + z[48];
z[56] = abb[19] + -abb[22];
z[56] = z[18] * z[56];
z[28] = abb[0] * (T(1) / T(2)) + z[28];
z[28] = z[28] * z[35];
z[59] = 9 * abb[24];
z[61] = abb[22] + 8 * z[23] + -z[59];
z[61] = abb[3] * z[61];
z[28] = -z[14] + z[28] + 2 * z[54] + z[56] + z[61];
z[28] = abb[11] * z[28];
z[54] = 3 * abb[7];
z[56] = -abb[9] + z[54];
z[56] = abb[13] * z[56];
z[61] = abb[9] * abb[11];
z[56] = z[56] + z[61];
z[56] = abb[26] * z[56];
z[61] = 3 * abb[2];
z[62] = -abb[3] + 2 * z[47] + z[61];
z[62] = -z[32] * z[62];
z[62] = z[45] + z[62];
z[62] = abb[23] * z[62];
z[63] = 2 * abb[13];
z[64] = -z[58] * z[63];
z[27] = z[13] + z[27] + z[28] + (T(-3) / T(2)) * z[41] + -z[46] + z[52] + z[56] + z[62] + z[64];
z[27] = abb[12] * z[27];
z[28] = abb[21] + z[39];
z[39] = abb[18] + z[28];
z[39] = abb[5] * z[39];
z[52] = abb[0] * z[43];
z[39] = z[10] + z[39] + z[52];
z[52] = 15 * abb[19] + 7 * abb[21];
z[56] = 2 * abb[18] + -z[52];
z[56] = z[7] + 2 * z[56] + z[59];
z[56] = z[33] * z[56];
z[62] = 7 * abb[19];
z[64] = 3 * abb[21] + z[62];
z[65] = -abb[18] + z[64];
z[66] = 4 * abb[24];
z[7] = -z[7] + 2 * z[65] + -z[66];
z[7] = z[7] * z[29];
z[49] = z[18] + z[49];
z[65] = 2 * abb[24];
z[49] = z[49] * z[65];
z[67] = abb[7] + -abb[9];
z[68] = 2 * abb[26];
z[69] = z[67] * z[68];
z[40] = z[40] * z[61];
z[9] = 2 * z[9];
z[7] = -z[7] + -z[9] + z[36] + 2 * z[39] + z[40] + -z[49] + z[56] + z[69];
z[36] = abb[28] * z[7];
z[39] = z[3] + 4 * z[20] + -2 * z[52] + z[59];
z[33] = z[33] * z[39];
z[39] = z[3] + z[53];
z[40] = -z[39] + 2 * z[64] + -z[66];
z[40] = z[29] * z[40];
z[49] = z[16] + z[61];
z[49] = z[49] * z[65];
z[52] = abb[7] + abb[9];
z[52] = z[52] * z[68];
z[53] = z[20] + z[42];
z[56] = z[18] * z[53];
z[9] = -z[9] + 3 * z[15] + z[33] + -z[37] + -z[40] + -z[49] + -z[52] + z[56];
z[15] = abb[27] * z[9];
z[33] = -7 * abb[22] + 8 * z[21] + z[23] + -z[65];
z[33] = abb[3] * z[33];
z[40] = abb[22] * (T(-7) / T(2)) + (T(-1) / T(2)) * z[21] + z[42] + z[65];
z[40] = abb[1] * z[40];
z[12] = -z[10] + z[12] + -z[33] + -z[40];
z[33] = abb[24] * (T(1) / T(3));
z[1] = abb[22] * (T(-1) / T(3)) + (T(2) / T(3)) * z[1] + -z[21] + -z[33];
z[1] = abb[6] * z[1];
z[19] = 5 * abb[5] + -z[19];
z[19] = z[19] * z[33];
z[33] = 11 * abb[19] + abb[21] * (T(13) / T(3));
z[40] = abb[18] * (T(8) / T(3)) + -z[33];
z[40] = abb[5] * z[40];
z[49] = abb[21] * (T(8) / T(3)) + z[62];
z[52] = abb[18] * (T(4) / T(3)) + z[49];
z[52] = abb[0] * z[52];
z[56] = abb[5] * (T(5) / T(2));
z[59] = abb[0] * (T(-13) / T(3)) + z[56];
z[59] = abb[20] * z[59];
z[49] = abb[18] * (T(-13) / T(3)) + abb[20] * (T(4) / T(3)) + abb[22] * (T(8) / T(3)) + z[49];
z[49] = abb[2] * z[49];
z[1] = z[1] + (T(-1) / T(3)) * z[12] + z[19] + (T(-16) / T(3)) * z[37] + z[40] + z[49] + z[52] + z[59];
z[12] = prod_pow(m1_set::bc<T>[0], 2);
z[1] = z[1] * z[12];
z[19] = abb[0] * abb[20];
z[40] = z[19] + -z[48];
z[0] = abb[22] + z[0];
z[0] = z[0] * z[18];
z[26] = -abb[22] + -z[26] + z[57];
z[26] = abb[3] * z[26];
z[48] = -abb[0] + -z[61];
z[48] = abb[24] * z[48];
z[0] = z[0] + z[14] + z[26] + 2 * z[40] + (T(3) / T(2)) * z[48];
z[0] = abb[13] * z[0];
z[3] = abb[20] + z[3];
z[14] = 2 * abb[22];
z[11] = -z[3] + -z[11] + z[14] + z[24];
z[11] = abb[3] * z[11];
z[19] = z[19] + -z[51];
z[24] = z[16] * z[43];
z[24] = -z[24] + 5 * z[37];
z[26] = 3 * abb[22];
z[22] = -abb[20] + z[22] + -z[26];
z[22] = abb[2] * z[22];
z[37] = abb[2] * (T(3) / T(2)) + z[16];
z[37] = abb[24] * z[37];
z[11] = z[11] + z[19] + z[22] + z[24] + z[37];
z[11] = abb[11] * z[11];
z[22] = -abb[9] + -z[54];
z[22] = abb[13] * z[22];
z[37] = abb[9] + -z[60];
z[37] = abb[11] * z[37];
z[22] = z[22] + z[37];
z[22] = abb[26] * z[22];
z[0] = z[0] + z[11] + z[22] + -z[46];
z[0] = abb[11] * z[0];
z[11] = -z[35] + z[53];
z[22] = abb[13] * z[11];
z[37] = abb[11] * z[11];
z[40] = -z[22] + z[37];
z[3] = z[3] + z[31] + -z[38];
z[3] = abb[14] * z[3];
z[3] = z[3] + 2 * z[40];
z[3] = abb[14] * z[3];
z[5] = z[5] + -z[6] + -z[14];
z[5] = abb[11] * z[5];
z[6] = z[11] * z[63];
z[6] = z[5] + z[6];
z[6] = abb[11] * z[6];
z[14] = 2 * z[28];
z[26] = -z[14] + z[26];
z[31] = z[26] + -z[39] + z[66];
z[38] = -abb[27] * z[31];
z[39] = abb[14] * z[11];
z[37] = -z[37] + z[39];
z[40] = 2 * abb[12];
z[37] = z[37] * z[40];
z[43] = abb[18] + abb[22];
z[33] = abb[24] * (T(5) / T(3)) + abb[20] * (T(8) / T(3)) + -z[33] + (T(5) / T(2)) * z[43];
z[33] = z[12] * z[33];
z[3] = z[3] + z[6] + z[33] + z[37] + z[38];
z[3] = abb[4] * z[3];
z[6] = abb[2] * z[25];
z[25] = z[34] * z[35];
z[23] = -z[21] + z[23];
z[23] = abb[3] * z[23];
z[33] = -abb[0] * z[50];
z[6] = z[6] + z[19] + 4 * z[23] + z[25] + z[33];
z[19] = prod_pow(abb[13], 2);
z[6] = z[6] * z[19];
z[23] = abb[2] + abb[3];
z[23] = -z[23] * z[32];
z[23] = z[23] + z[45];
z[23] = abb[11] * z[23];
z[25] = -8 * abb[1] + abb[2] + 2 * abb[6] + -z[16] + z[30];
z[33] = -abb[28] * z[25];
z[37] = abb[2] * (T(-16) / T(3)) + abb[3] * (T(-7) / T(3)) + abb[1] * (T(-7) / T(6)) + abb[6] * (T(-1) / T(3)) + abb[0] * (T(8) / T(3)) + z[56];
z[12] = z[12] * z[37];
z[37] = abb[1] + abb[3] + -abb[6];
z[38] = abb[27] * z[37];
z[43] = abb[8] + z[67];
z[43] = abb[15] * z[43];
z[12] = z[12] + z[23] + z[33] + 2 * z[38] + z[43];
z[12] = abb[23] * z[12];
z[8] = abb[9] * z[8];
z[23] = abb[3] + (T(1) / T(2)) * z[34];
z[23] = -abb[6] + 3 * z[23];
z[23] = abb[26] * z[23];
z[33] = -abb[7] * abb[22];
z[2] = abb[24] * (T(9) / T(2)) + -z[2];
z[2] = abb[8] * z[2];
z[2] = z[2] + z[8] + z[23] + z[33];
z[2] = abb[15] * z[2];
z[8] = -z[29] + -z[34];
z[8] = 4 * abb[10] + 2 * z[8];
z[8] = abb[15] * z[8];
z[23] = abb[11] + z[63];
z[23] = abb[11] * z[23];
z[33] = -3 * z[19] + z[23];
z[33] = abb[8] * z[33];
z[23] = abb[7] * z[23];
z[8] = z[8] + z[23] + z[33];
z[8] = abb[25] * z[8];
z[23] = -abb[11] + -abb[13];
z[23] = abb[11] * z[23];
z[19] = z[19] + (T(1) / T(2)) * z[23];
z[10] = z[10] * z[19];
z[0] = z[0] + z[1] + z[2] + z[3] + z[6] + z[8] + 3 * z[10] + z[12] + z[15] + z[17] + z[27] + z[36];
z[1] = abb[16] * z[9];
z[2] = abb[17] * z[7];
z[3] = z[21] + -z[35] + z[42];
z[3] = z[3] * z[29];
z[6] = z[44] + z[51];
z[7] = -z[16] * z[20];
z[8] = -abb[18] + z[28];
z[8] = 2 * z[8] + -z[20];
z[8] = abb[2] * z[8];
z[9] = -abb[18] + z[14];
z[9] = abb[0] * z[9];
z[10] = abb[5] * z[35];
z[3] = z[3] + -z[6] + z[7] + z[8] + z[9] + z[10];
z[3] = abb[13] * z[3];
z[7] = abb[20] + z[26];
z[7] = abb[2] * z[7];
z[8] = -z[16] + -z[30];
z[8] = abb[24] * z[8];
z[10] = -z[42] + z[65];
z[12] = abb[18] + -abb[22] + z[10];
z[12] = z[12] * z[29];
z[14] = -abb[20] * z[16];
z[6] = z[6] + z[7] + z[8] + z[12] + z[14] + -z[24];
z[6] = abb[11] * z[6];
z[7] = abb[9] * abb[13];
z[8] = 2 * abb[7];
z[12] = -abb[9] + z[8];
z[12] = abb[11] * z[12];
z[7] = z[7] + z[12];
z[7] = abb[26] * z[7];
z[3] = z[3] + z[6] + z[7] + 3 * z[41] + z[46];
z[6] = 2 * m1_set::bc<T>[0];
z[3] = z[3] * z[6];
z[7] = abb[20] + z[10];
z[7] = z[7] * z[29];
z[10] = abb[22] * z[16];
z[12] = -abb[18] + abb[20] + z[42];
z[12] = abb[22] + 2 * z[12];
z[12] = abb[2] * z[12];
z[14] = -abb[24] * z[55];
z[8] = -abb[9] + -z[8];
z[8] = abb[26] * z[8];
z[4] = z[4] + z[7] + z[8] + -z[9] + z[10] + z[12] + z[14];
z[4] = m1_set::bc<T>[0] * z[4];
z[7] = z[6] * z[58];
z[4] = z[4] + z[7];
z[4] = z[4] * z[40];
z[7] = abb[7] * abb[11];
z[8] = -abb[8] * z[32];
z[7] = -z[7] + z[8];
z[7] = abb[25] * z[7];
z[7] = 4 * z[7] + -z[13];
z[7] = m1_set::bc<T>[0] * z[7];
z[8] = abb[12] * z[11];
z[5] = -z[5] + z[8] + -z[22] + -z[39];
z[5] = z[5] * z[6];
z[8] = -abb[16] * z[31];
z[5] = z[5] + z[8];
z[5] = abb[4] * z[5];
z[8] = -abb[17] * z[25];
z[9] = z[18] + z[47];
z[9] = z[9] * z[32];
z[9] = z[9] + -z[45];
z[6] = z[6] * z[9];
z[9] = abb[16] * z[37];
z[6] = z[6] + z[8] + 2 * z[9];
z[6] = abb[23] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_316_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("26.373769617953217667716020207179467700365148014113187721891810058"),stof<T>("41.898597939731451129987253183441337752224748227662862548007745164")}, std::complex<T>{stof<T>("33.62188404722948466792582347305678920743084924471908427300399933"),stof<T>("-211.66196507500450055083955282584383528213104900712130431905297317")}, std::complex<T>{stof<T>("58.913388864566206565800439326398107036139518695173343635576621579"),stof<T>("69.525131143524278696168520150953893778548523699885597754512996055")}, std::complex<T>{stof<T>("36.972875356851305572128968448576604352429443594556765655566542541"),stof<T>("-82.29962708020841922761993782936017835616594867046359880692329042")}, std::complex<T>{stof<T>("-36.85339432915201201447618681762485695036579198520044115191725646"),stof<T>("52.637071061571859861364794159718644407672252435595051140478045808")}, std::complex<T>{stof<T>("-80.329740369799917464993057748295273681733510582586534857226661488"),stof<T>("25.611181058092712537581225861321427821154681516040708534912914545")}, std::complex<T>{stof<T>("-0.8629539153984897109865192203571166559181066931093198106642565075"),stof<T>("10.994991936042099162868879735204265761265421563968542170570635796")}, std::complex<T>{stof<T>("5.188521423284097173545332293007922738779999795547604887538974547"),stof<T>("-31.497863384434761426915167224305706600330124032957193082127117469")}, std::complex<T>{stof<T>("-12.379588269826081215576188978738705078588384038455458632400072591"),stof<T>("40.330736035232100338300946594399986655364254611090980700318994212")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_316_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_316_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-52.382219104125583088931925520357193911671952622499932687873647478"),stof<T>("-61.636011103887433877882181052780224115976576427195742835250824176")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dlog_W4(k,dl), dl[2], dl[4], dlog_W14(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_11(k), f_2_23(k), f_2_2_im(k), f_2_12_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_2_re(k), f_2_12_re(k)};

                    
            return f_4_316_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_316_DLogXconstant_part(base_point<T>, kend);
	value += f_4_316_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_316_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_316_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_316_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_316_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_316_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_316_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
