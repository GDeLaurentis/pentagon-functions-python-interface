/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_552.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_552_abbreviated (const std::array<T,31>& abb) {
T z[41];
z[0] = 3 * abb[23];
z[1] = 2 * abb[25];
z[2] = 5 * abb[19];
z[3] = 4 * abb[21] + -z[2];
z[3] = abb[20] * (T(-20) / T(3)) + abb[22] * (T(-10) / T(3)) + z[0] + z[1] + (T(1) / T(3)) * z[3];
z[3] = abb[0] * z[3];
z[4] = 4 * abb[20] + 2 * abb[22];
z[5] = abb[19] + abb[24];
z[6] = -abb[25] + z[4] + -z[5];
z[7] = -2 * z[6];
z[8] = abb[6] * z[7];
z[9] = abb[7] + abb[8];
z[10] = abb[26] + -abb[27];
z[11] = (T(3) / T(2)) * z[10];
z[9] = z[9] * z[11];
z[12] = 2 * abb[20] + abb[22];
z[13] = -abb[19] + abb[23];
z[14] = -abb[25] + z[12] + (T(1) / T(2)) * z[13];
z[14] = abb[4] * z[14];
z[15] = 5 * abb[24];
z[16] = abb[19] * (T(7) / T(2));
z[17] = -z[15] + -z[16];
z[18] = 3 * abb[25];
z[17] = abb[23] * (T(-1) / T(2)) + abb[22] * (T(14) / T(3)) + abb[20] * (T(28) / T(3)) + (T(1) / T(3)) * z[17] + -z[18];
z[17] = abb[1] * z[17];
z[19] = abb[21] * (T(1) / T(2));
z[20] = -abb[24] + abb[19] * (T(-1) / T(2)) + z[19];
z[21] = abb[25] * (T(3) / T(2));
z[22] = abb[22] * (T(5) / T(3)) + abb[20] * (T(10) / T(3)) + (T(1) / T(3)) * z[20] + -z[21];
z[22] = abb[2] * z[22];
z[23] = abb[25] * (T(1) / T(2));
z[24] = 2 * abb[24];
z[25] = abb[19] * (T(-11) / T(6)) + abb[21] * (T(1) / T(6)) + abb[22] * (T(7) / T(3)) + abb[20] * (T(14) / T(3)) + -z[23] + -z[24];
z[25] = abb[3] * z[25];
z[3] = z[3] + z[8] + z[9] + 5 * z[14] + z[17] + z[22] + z[25];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[14] = -z[4] + -z[20] + z[21];
z[17] = abb[2] * z[14];
z[9] = -z[9] + z[17];
z[17] = 8 * abb[20] + 4 * abb[22];
z[22] = z[17] + -z[18];
z[25] = z[5] + -z[22];
z[26] = 4 * abb[1];
z[26] = z[25] * z[26];
z[27] = 3 * abb[6];
z[27] = -z[6] * z[27];
z[26] = z[26] + -z[27];
z[28] = 16 * abb[20] + 8 * abb[22];
z[29] = abb[25] * (T(-15) / T(2)) + z[20] + z[28];
z[29] = abb[3] * z[29];
z[30] = z[4] + -z[18];
z[31] = abb[19] + -abb[21] + z[24];
z[32] = z[30] + z[31];
z[32] = abb[0] * z[32];
z[29] = -z[9] + -z[26] + z[29] + -z[32];
z[29] = abb[13] * z[29];
z[32] = 3 * abb[24];
z[16] = abb[25] * (T(9) / T(2)) + z[16] + z[19] + -z[28] + z[32];
z[16] = abb[3] * z[16];
z[33] = abb[19] + abb[21];
z[34] = z[30] + z[33];
z[34] = abb[0] * z[34];
z[16] = -z[9] + z[16] + z[26] + z[34];
z[34] = -abb[11] * z[16];
z[35] = z[22] + -z[31];
z[36] = abb[2] * z[35];
z[37] = 3 * z[10];
z[38] = abb[8] * z[37];
z[36] = z[36] + z[38];
z[25] = 2 * z[25];
z[38] = abb[1] * z[25];
z[39] = z[22] + -z[33];
z[39] = abb[0] * z[39];
z[33] = -z[12] + z[33];
z[33] = abb[3] * z[33];
z[33] = 4 * z[33] + z[36] + z[38] + z[39];
z[33] = abb[12] * z[33];
z[33] = z[29] + z[33] + z[34];
z[33] = abb[12] * z[33];
z[34] = 2 * abb[19];
z[22] = -abb[21] + -z[0] + z[22] + z[34];
z[22] = abb[0] * z[22];
z[1] = z[1] + -z[4];
z[39] = z[1] + -z[13];
z[39] = abb[4] * z[39];
z[40] = 3 * z[39];
z[22] = z[22] + z[38] + z[40];
z[17] = abb[19] * (T(5) / T(2)) + -z[17] + -z[19] + z[21] + z[32];
z[17] = abb[3] * z[17];
z[9] = z[9] + z[17] + z[22] + -z[27];
z[9] = abb[11] * z[9];
z[9] = z[9] + -z[29];
z[9] = abb[11] * z[9];
z[1] = z[1] + z[13];
z[1] = abb[0] * z[1];
z[7] = abb[3] * z[7];
z[1] = z[1] + -z[7] + z[8] + -z[38] + -z[39];
z[1] = 3 * z[1];
z[7] = abb[28] * z[1];
z[8] = abb[8] * z[14];
z[13] = -abb[9] * z[6];
z[14] = z[20] + z[23];
z[14] = abb[7] * z[14];
z[17] = -abb[0] + 2 * abb[10];
z[10] = z[10] * z[17];
z[17] = abb[2] + abb[3];
z[11] = z[11] * z[17];
z[8] = -z[8] + -z[10] + z[11] + z[13] + z[14];
z[8] = 3 * z[8];
z[10] = abb[29] * z[8];
z[11] = -z[18] + z[28];
z[13] = z[0] + -z[11] + z[15] + z[34];
z[13] = abb[1] * z[13];
z[0] = abb[19] + -z[0] + z[4] + -z[24];
z[0] = abb[0] * z[0];
z[4] = abb[3] * z[25];
z[0] = z[0] + z[4] + 2 * z[13] + -z[40];
z[0] = abb[14] * z[0];
z[5] = z[5] + z[30];
z[5] = abb[0] * z[5];
z[4] = z[4] + z[5] + z[26];
z[5] = -abb[11] + abb[12];
z[5] = z[4] * z[5];
z[0] = z[0] + 2 * z[5];
z[0] = abb[14] * z[0];
z[5] = -abb[0] + abb[3];
z[5] = z[5] * z[35];
z[12] = z[12] + -z[31];
z[12] = abb[2] * z[12];
z[13] = abb[7] * z[37];
z[5] = z[5] + 4 * z[12] + z[13] + -z[38];
z[5] = prod_pow(abb[13], 2) * z[5];
z[4] = 2 * z[4];
z[12] = -abb[15] * z[4];
z[0] = abb[30] + z[0] + z[3] + z[5] + z[7] + z[9] + z[10] + z[12] + z[33];
z[3] = -abb[6] * z[6];
z[2] = abb[21] + -6 * abb[24] + -z[2] + z[11];
z[2] = abb[3] * z[2];
z[2] = z[2] + 6 * z[3] + z[13] + -2 * z[22] + z[36];
z[2] = abb[11] * z[2];
z[3] = abb[12] * z[16];
z[4] = abb[14] * z[4];
z[2] = z[2] + z[3] + z[4] + z[29];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = abb[17] * z[8];
z[1] = abb[16] * z[1];
z[1] = abb[18] + z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_552_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("4.741084585886098363799887901432834186432700211557178247051356217"),stof<T>("26.188501553111871075378785503313158137165148347855729212926252287")}, std::complex<T>{stof<T>("-21.124202028506466720477267850053153999244670262600364930762567346"),stof<T>("62.107319676190444345286648189332812237253871905204636084858429512")}, std::complex<T>{stof<T>("36.119104111302572006112413342785895438987172183751275396194245654"),stof<T>("-14.633300484517895006119187001685607389125752937927857609262389682")}, std::complex<T>{stof<T>("-10.562101014253233360238633925026576999622335131300182465381283673"),stof<T>("31.053659838095222172643324094666406118626935952602318042429214756")}, std::complex<T>{stof<T>("31.378019525416473642312525441353061252554471972194097149142889437"),stof<T>("-40.821802037629766081497972504998765526290901285783586822188641969")}, std::complex<T>{stof<T>("-25.55700309704933864587377941775931843936483705245109293081296198"),stof<T>("-16.420359353577327166524137092980798729501183014674460433166825074")}, std::complex<T>{stof<T>("11.325128681290779395907282033920905840399050184493898327844197224"),stof<T>("-18.443713539553886571319295849287612627184745544319581586174581398")}, std::complex<T>{stof<T>("-11.325128681290779395907282033920905840399050184493898327844197224"),stof<T>("18.443713539553886571319295849287612627184745544319581586174581398")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[126].real()/kbase.W[126].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_552_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_552_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,4> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(8)) * (v[1] + v[2] + -v[3] + -v[4]) * (-8 + 4 * v[0] + 3 * v[1] + -v[2] + v[3] + v[4] + 2 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 4 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(2)) * (v[1] + v[2] + -v[3] + -v[4]) * (40 + 28 * v[0] + -9 * v[1] + 11 * v[2] + 13 * v[3] + -11 * v[4] + -10 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + -20 * v[5])) / prod_pow(tend, 2);
c[2] = ((1 + m1_set::bc<T>[1]) * (v[1] + v[2] + -v[3] + -v[4])) / tend;
c[3] = (-4 * (-1 + 5 * m1_set::bc<T>[1]) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (2 * t * abb[24] * c[0] + t * abb[22] * c[1] + 2 * abb[24] * c[2] + 2 * abb[19] * (t * c[0] + c[2]) + abb[22] * c[3] + 2 * abb[20] * (t * c[1] + c[3]) + -abb[25] * (2 * t * c[0] + t * c[1] + 2 * c[2] + c[3])) * (T(1) / T(2));
	}
	{
T z[5];
z[0] = -abb[11] + abb[12];
z[1] = abb[13] + -abb[14];
z[0] = z[0] * z[1];
z[0] = abb[15] + z[0];
z[1] = prod_pow(abb[14], 2);
z[2] = prod_pow(abb[13], 2);
z[1] = z[1] + -z[2];
z[2] = z[0] + -2 * z[1];
z[3] = -4 * abb[20] + -2 * abb[22];
z[2] = z[2] * z[3];
z[3] = z[0] + z[1];
z[4] = -abb[19] + -abb[24];
z[3] = z[3] * z[4];
z[0] = z[0] + -z[1];
z[0] = abb[25] * z[0];
z[0] = 3 * z[0] + z[2] + z[3];
return 2 * abb[5] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_552_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (abb[19] + 4 * abb[20] + 2 * abb[22] + abb[24] + -3 * abb[25]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[19] + -4 * abb[20] + -2 * abb[22] + -abb[24] + 3 * abb[25];
z[1] = abb[13] + -abb[14];
return 2 * abb[5] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_552_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("4.307453173047782950395972937717766379269458989649149565213982161"),stof<T>("-44.944449678233335655816912950450857787443557140122044073458724994")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,31> abb = {dl[0], dlog_W4(k,dl), dlog_W8(k,dl), dl[3], dlog_W14(k,dl), dlog_W20(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W127(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_2_im(k), f_2_24_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[13].real()/k.W[13].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[126].real()/k.W[126].real()), f_2_2_re(k), f_2_24_re(k), T{0}};
abb[18] = SpDLog_f_4_552_W_20_Im(t, path, abb);
abb[30] = SpDLog_f_4_552_W_20_Re(t, path, abb);

                    
            return f_4_552_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_552_DLogXconstant_part(base_point<T>, kend);
	value += f_4_552_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_552_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_552_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_552_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_552_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_552_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_552_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
