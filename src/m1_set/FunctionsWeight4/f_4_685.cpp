/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_685.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_685_abbreviated (const std::array<T,36>& abb) {
T z[89];
z[0] = 5 * abb[28];
z[1] = -6 * abb[24] + 18 * abb[29];
z[2] = z[0] + -z[1];
z[3] = 4 * abb[26];
z[4] = 3 * abb[25];
z[5] = z[3] + z[4];
z[6] = 3 * abb[23];
z[7] = 4 * abb[27];
z[8] = z[6] + z[7];
z[9] = z[2] + z[5] + z[8];
z[10] = abb[8] * z[9];
z[11] = abb[23] + abb[27];
z[12] = abb[25] * (T(5) / T(3));
z[13] = 2 * abb[28];
z[14] = -2 * abb[24] + 6 * abb[29];
z[11] = abb[26] * (T(5) / T(3)) + (T(1) / T(3)) * z[11] + z[12] + z[13] + -z[14];
z[15] = 4 * abb[3];
z[11] = z[11] * z[15];
z[16] = 7 * abb[28];
z[17] = -11 * abb[23] + z[16];
z[12] = -8 * abb[27] + abb[26] * (T(-8) / T(3)) + z[12] + z[14] + (T(1) / T(3)) * z[17];
z[12] = abb[6] * z[12];
z[17] = -abb[23] + abb[25];
z[18] = 2 * abb[26];
z[19] = -abb[28] + z[17] + z[18];
z[20] = abb[2] * z[19];
z[21] = 2 * abb[27];
z[22] = -abb[28] + -z[17] + z[21];
z[23] = abb[1] * z[22];
z[24] = -14 * abb[24] + 42 * abb[29];
z[25] = 28 * abb[23] + 41 * abb[27] + 29 * abb[28];
z[25] = 9 * abb[26] + abb[25] * (T(14) / T(3)) + -z[24] + (T(1) / T(3)) * z[25];
z[26] = 2 * abb[5];
z[25] = z[25] * z[26];
z[27] = abb[30] + 2 * abb[31] + abb[32];
z[28] = abb[9] * z[27];
z[29] = abb[10] * z[27];
z[30] = 5 * abb[25];
z[31] = 15 * abb[23] + 20 * abb[24] + -60 * abb[29] + abb[26] * (T(28) / T(3)) + abb[28] * (T(47) / T(3)) + abb[27] * (T(58) / T(3)) + z[30];
z[31] = abb[4] * z[31];
z[32] = 3 * abb[28];
z[33] = -12 * abb[24] + 36 * abb[29] + abb[26] * (T(-104) / T(3)) + abb[25] * (T(-59) / T(3)) + abb[27] * (T(-22) / T(3)) + abb[23] * (T(23) / T(3)) + z[32];
z[33] = abb[0] * z[33];
z[34] = abb[26] + -abb[27] + z[17];
z[35] = abb[7] * z[34];
z[10] = -4 * z[10] + z[11] + 2 * z[12] + (T(-1) / T(3)) * z[20] + 11 * z[23] + z[25] + (T(-32) / T(3)) * z[28] + -8 * z[29] + z[31] + z[33] + (T(16) / T(3)) * z[35];
z[10] = prod_pow(m1_set::bc<T>[0], 2) * z[10];
z[11] = z[14] + -z[18];
z[12] = 6 * abb[27];
z[25] = abb[25] + abb[28];
z[31] = z[6] + -z[11] + z[12] + -z[25];
z[31] = abb[4] * z[31];
z[33] = abb[23] + z[21];
z[35] = 6 * abb[26];
z[2] = z[2] + z[30] + z[33] + z[35];
z[2] = abb[6] * z[2];
z[36] = -abb[23] + -abb[25] + z[14];
z[37] = z[12] + -z[32] + z[35] + -z[36];
z[38] = abb[7] * z[37];
z[39] = 2 * abb[8];
z[39] = z[9] * z[39];
z[40] = abb[25] + z[18];
z[41] = abb[23] + abb[28];
z[12] = -z[1] + z[12] + z[40] + 5 * z[41];
z[42] = -z[12] * z[26];
z[43] = z[14] + -z[21];
z[35] = -z[4] + -z[35] + z[41] + z[43];
z[35] = abb[0] * z[35];
z[44] = 2 * abb[3];
z[45] = -z[37] * z[44];
z[46] = 2 * z[23];
z[47] = 2 * z[20];
z[48] = z[46] + z[47];
z[35] = -2 * z[2] + -z[31] + z[35] + 3 * z[38] + z[39] + z[42] + z[45] + -z[48];
z[35] = abb[19] * z[35];
z[12] = abb[5] * z[12];
z[42] = 2 * z[27];
z[45] = abb[10] * z[42];
z[49] = z[39] + z[45];
z[12] = z[12] + -z[49];
z[50] = abb[28] + z[14];
z[51] = 2 * abb[23];
z[52] = 5 * abb[27];
z[53] = 3 * abb[26];
z[54] = z[50] + -z[51] + -z[52] + -z[53];
z[54] = z[44] * z[54];
z[30] = -abb[23] + -8 * abb[26] + z[1] + -z[7] + -z[30] + -z[32];
z[30] = abb[0] * z[30];
z[55] = 4 * abb[23];
z[56] = 11 * abb[28];
z[57] = 7 * abb[27];
z[24] = -10 * abb[25] + -13 * abb[26] + z[24] + -z[55] + -z[56] + -z[57];
z[24] = abb[6] * z[24];
z[58] = 2 * abb[7];
z[37] = z[37] * z[58];
z[59] = abb[26] + z[14];
z[60] = 5 * abb[23] + -z[4] + z[57] + -z[59];
z[60] = abb[4] * z[60];
z[61] = 3 * z[23];
z[24] = -z[12] + z[24] + z[30] + z[37] + -z[47] + z[54] + z[60] + -z[61];
z[24] = abb[15] * z[24];
z[30] = z[7] + -z[17] + z[18] + -z[32];
z[37] = -abb[4] * z[30];
z[60] = abb[26] + abb[27];
z[62] = z[13] + -z[36] + z[60];
z[63] = 2 * z[62];
z[64] = abb[6] * z[63];
z[65] = -z[46] + z[64];
z[66] = -z[28] + z[47];
z[67] = -z[65] + z[66];
z[63] = abb[5] * z[63];
z[68] = -z[21] + z[32];
z[3] = -z[3] + -z[17] + z[68];
z[69] = abb[0] * z[3];
z[70] = 3 * abb[27];
z[71] = -z[36] + z[53] + z[70];
z[72] = z[58] * z[71];
z[60] = -abb[28] + z[60];
z[73] = z[44] * z[60];
z[74] = abb[11] * z[27];
z[75] = 2 * z[74];
z[37] = z[37] + -z[45] + -z[63] + z[67] + z[69] + z[72] + -z[73] + -z[75];
z[37] = abb[12] * z[37];
z[69] = z[44] * z[71];
z[71] = 2 * abb[25];
z[53] = z[53] + z[71];
z[72] = -abb[28] + z[14];
z[76] = abb[27] + z[53] + -z[72];
z[77] = 2 * abb[0];
z[78] = z[76] * z[77];
z[79] = 10 * abb[24] + 8 * abb[25] + 7 * abb[26] + abb[27] + -30 * abb[29] + z[51] + z[56];
z[79] = abb[6] * z[79];
z[80] = abb[4] * z[60];
z[78] = z[12] + -z[38] + z[69] + z[78] + z[79] + z[80];
z[78] = abb[13] * z[78];
z[36] = -z[32] + z[36];
z[79] = abb[0] * z[36];
z[80] = z[63] + z[79];
z[15] = z[15] * z[60];
z[15] = z[15] + z[45];
z[31] = z[15] + z[31] + -z[38] + z[80];
z[31] = abb[16] * z[31];
z[38] = abb[13] * z[23];
z[31] = z[31] + z[37] + z[38] + z[78];
z[24] = z[24] + 2 * z[31];
z[24] = abb[15] * z[24];
z[31] = abb[9] * z[42];
z[37] = z[31] + z[75];
z[42] = -z[12] + z[37];
z[45] = z[30] * z[58];
z[78] = abb[4] * z[62];
z[81] = -z[1] + z[16];
z[82] = 6 * abb[25];
z[83] = 5 * abb[26];
z[84] = -abb[27] + z[81] + z[82] + z[83];
z[84] = abb[6] * z[84];
z[85] = abb[23] + -z[5] + z[72];
z[85] = abb[0] * z[85];
z[45] = 5 * z[23] + -z[42] + -z[45] + -z[54] + z[78] + z[84] + -z[85];
z[54] = abb[34] * z[45];
z[78] = abb[10] + abb[11];
z[78] = z[34] * z[78];
z[85] = abb[0] + -abb[4];
z[27] = z[27] * z[85];
z[85] = abb[9] * z[22];
z[27] = -z[27] + 2 * z[78] + -z[85];
z[78] = abb[35] * z[27];
z[85] = abb[0] * z[19];
z[86] = z[29] + z[85];
z[87] = z[26] * z[60];
z[88] = -abb[3] * z[22];
z[87] = -z[86] + z[87] + z[88];
z[87] = prod_pow(abb[16], 2) * z[87];
z[54] = z[54] + z[78] + z[87];
z[78] = -abb[26] + z[52];
z[56] = z[1] + -z[56] + z[78] + -z[82];
z[56] = abb[6] * z[56];
z[52] = abb[26] + z[52];
z[14] = abb[25] + -z[6] + z[14] + -z[52];
z[14] = abb[4] * z[14];
z[76] = -z[44] * z[76];
z[12] = -z[12] + z[14] + -z[23] + z[56] + z[76] + z[79];
z[12] = prod_pow(abb[13], 2) * z[12];
z[12] = z[12] + z[24] + z[35] + 2 * z[54];
z[14] = -z[50] + z[70] + z[71] + z[83];
z[14] = z[14] * z[44];
z[24] = abb[0] * z[62];
z[35] = z[3] * z[58];
z[50] = 6 * abb[23] + z[78] + z[81];
z[50] = abb[5] * z[50];
z[8] = abb[25] + -z[8] + z[72];
z[54] = abb[4] * z[8];
z[2] = z[2] + z[14] + 5 * z[20] + z[24] + z[35] + -z[49] + z[50] + -z[54] + -z[75];
z[14] = abb[33] * z[2];
z[24] = abb[3] * z[30];
z[23] = z[20] + z[23];
z[35] = -z[40] + -z[41] + z[43];
z[49] = abb[4] * z[35];
z[49] = z[49] + -z[75];
z[26] = z[26] * z[35];
z[50] = z[28] + z[29];
z[54] = abb[7] * z[36];
z[56] = -abb[24] + 3 * abb[29];
z[53] = -abb[23] + z[53] + -z[56];
z[53] = z[53] * z[77];
z[24] = z[23] + z[24] + z[26] + z[49] + z[50] + z[53] + -z[54];
z[24] = abb[12] * z[24];
z[26] = abb[0] * z[35];
z[11] = abb[23] + -z[4] + z[11] + -z[68];
z[11] = abb[7] * z[11];
z[53] = z[22] * z[44];
z[36] = abb[4] * z[36];
z[26] = z[11] + -z[26] + z[36] + z[37] + -z[53] + z[64];
z[26] = abb[13] * z[26];
z[36] = 3 * z[41];
z[37] = z[36] + -z[40] + -z[43];
z[37] = abb[7] * z[37];
z[19] = z[19] * z[44];
z[19] = z[19] + 4 * z[20] + z[31] + z[37] + z[49] + -z[80];
z[19] = abb[16] * z[19];
z[20] = -z[19] + z[26] + -4 * z[38];
z[24] = z[20] + z[24];
z[24] = abb[12] * z[24];
z[26] = -z[54] + z[73];
z[31] = abb[5] * z[62];
z[43] = -abb[26] + z[56];
z[49] = z[33] + -z[43];
z[49] = abb[4] * z[49];
z[31] = z[31] + z[49] + z[74];
z[25] = -z[25] + z[43];
z[25] = z[25] * z[77];
z[25] = z[25] + -z[26] + -z[28] + 2 * z[31] + -z[48];
z[25] = abb[12] * z[25];
z[31] = -abb[27] + z[56];
z[41] = z[31] + -z[41];
z[41] = abb[4] * z[41];
z[41] = z[41] + z[74];
z[31] = -z[31] + z[40];
z[31] = z[31] * z[77];
z[26] = -z[26] + z[31] + 2 * z[41] + -z[67];
z[26] = abb[15] * z[26];
z[22] = abb[4] * z[22];
z[23] = -z[22] + z[23] + -z[85];
z[23] = abb[14] * z[23];
z[20] = -z[20] + z[23] + z[25] + z[26];
z[20] = abb[14] * z[20];
z[14] = -z[14] + z[20] + z[24];
z[20] = -abb[3] + abb[7];
z[3] = -z[3] * z[20];
z[3] = z[3] + -z[22] + -z[47] + -z[50] + z[74];
z[3] = abb[18] * z[3];
z[20] = -z[20] * z[30];
z[20] = z[20] + z[46] + -z[74] + z[86];
z[20] = abb[17] * z[20];
z[3] = z[3] + z[20];
z[3] = 8 * z[3] + z[10] + 2 * z[12] + 4 * z[14];
z[3] = 2 * z[3];
z[10] = -4 * abb[24] + 12 * abb[29];
z[0] = -abb[23] + -z[0] + -z[4] + z[10] + -z[18];
z[0] = abb[0] * z[0];
z[4] = abb[26] + -z[13] + -z[17] + z[70];
z[4] = abb[4] * z[4];
z[12] = z[35] * z[44];
z[8] = abb[5] * z[8];
z[0] = z[0] + z[4] + z[8] + z[12] + 4 * z[29] + z[39] + z[54] + -z[61] + -z[66] + z[75] + -z[84];
z[0] = abb[15] * z[0];
z[4] = 4 * abb[25] + z[16] + -z[51] + -z[57] + -z[59];
z[4] = abb[6] * z[4];
z[8] = z[10] + -z[32];
z[12] = -z[8] + z[52] + z[55];
z[12] = abb[4] * z[12];
z[13] = -z[60] * z[77];
z[4] = z[4] + -z[11] + z[12] + z[13] + -z[42] + z[69];
z[4] = abb[13] * z[4];
z[9] = abb[4] * z[9];
z[11] = abb[5] * z[35];
z[11] = z[11] + z[28];
z[12] = z[47] + z[65];
z[1] = -9 * abb[25] + -14 * abb[26] + -abb[28] + z[1] + z[6] + -z[21];
z[1] = abb[0] * z[1];
z[1] = z[1] + z[9] + -4 * z[11] + -z[12] + -z[15] + -z[37] + z[75];
z[1] = abb[12] * z[1];
z[6] = -z[7] + z[10] + -z[36] + -z[40];
z[6] = abb[4] * z[6];
z[5] = z[5] + -z[8] + z[33];
z[5] = abb[0] * z[5];
z[7] = z[44] + -z[58];
z[7] = z[7] * z[34];
z[5] = z[5] + z[6] + z[7] + z[12] + 3 * z[28] + -z[63];
z[5] = abb[14] * z[5];
z[0] = z[0] + z[1] + z[4] + z[5] + z[19] + 5 * z[38];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = -abb[20] * z[2];
z[2] = abb[21] * z[45];
z[4] = abb[22] * z[27];
z[0] = z[0] + z[1] + z[2] + z[4];
z[0] = 8 * z[0];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_685_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-263.33262288473358337202484754657244520510744637236115983987312298"),stof<T>("-298.95789615119850372338946976591115219117466839723852143290881727")}, std::complex<T>{stof<T>("-100.60661801771934845868193049851908584354998543391942344277196114"),stof<T>("548.98522805482844115327074434794131287337655971971950939978923857")}, std::complex<T>{stof<T>("162.72600486701423491334291704805335936155746093844173639710116183"),stof<T>("847.94312420602694487666021411385246506455122811695803083269805584")}, std::complex<T>{stof<T>("307.96754876328492364355691049788781402777177830184667996957578932"),stof<T>("1208.03935454322419776383790846184916374120975019977354194701230096")}, std::complex<T>{stof<T>("-118.091078988462894641810854096737990538893129008956216267398495489"),stof<T>("61.138334185998749163788224582085546485483853685576989681405427849")}, std::complex<T>{stof<T>("-245.84816191399003718889592394835354050976430279732436701524658863"),stof<T>("188.88899771763118826609304999994461419671803763690399828547499344")}, std::complex<T>{stof<T>("301.8198540531580453760457914955572575306499563017582703283158834"),stof<T>("-1646.9556841644853234598122330438239386201296791591585281993677157")}, std::complex<T>{stof<T>("-231.25138747127559821055310420973414777285696248123250290054323162"),stof<T>("216.22132990851069491553883779912042231978000986018878092022370678")}, std::complex<T>{stof<T>("-462.50277494255119642110620841946829554571392496246500580108646324"),stof<T>("432.44265981702138983107767559824084463956001972037756184044741356")}, std::complex<T>{stof<T>("-231.25138747127559821055310420973414777285696248123250290054323162"),stof<T>("216.22132990851069491553883779912042231978000986018878092022370678")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[23].real()/kbase.W[23].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[63].real()/kbase.W[63].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[124].real()/kbase.W[124].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_685_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_685_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-1019.19420729805664792165950726822624222034828534551616808288915037"),stof<T>("413.59806743649773252620629018170576701896586356026704184743281917")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,36> abb = {dl[0], dl[1], dlog_W4(k,dl), dlog_W7(k,dl), dlog_W10(k,dl), dlog_W14(k,dl), dlog_W24(k,dl), dlog_W29(k,dl), dlog_W64(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), f_1_1(k), f_1_3(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_9_im(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[23].real()/k.W[23].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[63].real()/k.W[63].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[124].real()/k.W[124].real()), f_2_2_re(k), f_2_9_re(k), f_2_25_re(k)};

                    
            return f_4_685_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_685_DLogXconstant_part(base_point<T>, kend);
	value += f_4_685_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_685_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_685_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_685_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_685_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_685_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_685_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
