/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_635.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_635_abbreviated (const std::array<T,66>& abb) {
T z[143];
z[0] = -abb[43] + abb[50];
z[1] = abb[44] * (T(5) / T(6));
z[2] = abb[52] * (T(3) / T(4));
z[3] = abb[47] * (T(5) / T(4));
z[4] = abb[51] * (T(3) / T(4));
z[5] = abb[53] + abb[48] * (T(-11) / T(4));
z[5] = abb[45] * (T(1) / T(12)) + abb[46] * (T(11) / T(12)) + z[0] + z[1] + -z[2] + -z[3] + z[4] + (T(1) / T(3)) * z[5];
z[5] = abb[7] * z[5];
z[6] = abb[49] * (T(1) / T(2));
z[7] = abb[53] * (T(2) / T(3)) + -z[6];
z[8] = abb[45] * (T(1) / T(3));
z[9] = abb[52] * (T(3) / T(2));
z[10] = abb[51] * (T(1) / T(2));
z[1] = abb[48] * (T(-1) / T(6)) + abb[46] * (T(7) / T(6)) + z[1] + -z[7] + z[8] + -z[9] + z[10];
z[1] = abb[1] * z[1];
z[11] = 4 * abb[53];
z[12] = abb[48] + z[11];
z[13] = abb[47] + abb[51];
z[14] = abb[46] * (T(3) / T(2));
z[8] = abb[44] * (T(-11) / T(6)) + abb[52] * (T(13) / T(6)) + z[8] + (T(1) / T(3)) * z[12] + -z[13] + -z[14];
z[8] = abb[3] * z[8];
z[15] = abb[48] + abb[52];
z[16] = abb[47] * (T(3) / T(4));
z[7] = abb[45] * (T(-7) / T(4)) + abb[46] * (T(-7) / T(12)) + abb[51] * (T(-5) / T(4)) + abb[44] * (T(7) / T(6)) + (T(2) / T(3)) * z[0] + z[7] + (T(11) / T(12)) * z[15] + z[16];
z[7] = abb[4] * z[7];
z[17] = abb[47] * (T(1) / T(2)) + z[10];
z[18] = -abb[49] + z[17];
z[19] = abb[46] * (T(1) / T(2));
z[20] = z[18] + z[19];
z[21] = abb[48] * (T(1) / T(2));
z[22] = abb[52] * (T(1) / T(2));
z[23] = z[21] + z[22];
z[24] = abb[45] * (T(1) / T(2));
z[25] = z[23] + z[24];
z[26] = abb[44] + z[20] + -z[25];
z[27] = abb[12] * z[26];
z[28] = z[19] + z[21] + z[24];
z[29] = abb[49] * (T(3) / T(2));
z[30] = abb[52] + abb[53] + -z[28] + -z[29];
z[30] = abb[14] * z[30];
z[31] = abb[48] * (T(1) / T(4));
z[32] = abb[52] * (T(1) / T(4)) + z[31];
z[33] = abb[46] * (T(3) / T(4));
z[34] = -abb[53] + abb[51] * (T(1) / T(4)) + z[32] + -z[33];
z[35] = abb[45] * (T(3) / T(4));
z[3] = z[3] + z[34] + -z[35];
z[3] = abb[6] * z[3];
z[36] = 2 * abb[53];
z[13] = z[13] + -z[36];
z[37] = abb[49] + z[13];
z[38] = abb[44] * (T(1) / T(2));
z[39] = z[19] + z[38];
z[40] = z[22] + -z[37] + z[39];
z[40] = abb[9] * z[40];
z[41] = abb[20] + abb[22];
z[42] = -2 * z[41];
z[43] = -abb[54] + -abb[55] + abb[56] + abb[57];
z[42] = z[42] * z[43];
z[44] = abb[21] + abb[23];
z[45] = (T(3) / T(4)) * z[43];
z[46] = z[44] * z[45];
z[47] = -abb[46] + abb[52];
z[48] = -abb[45] + z[47];
z[49] = abb[47] + z[48];
z[50] = -abb[49] + z[49];
z[50] = abb[11] * z[50];
z[51] = z[46] + 3 * z[50];
z[52] = -abb[48] + abb[51];
z[53] = -abb[47] + z[52];
z[54] = z[48] + z[53];
z[54] = z[0] + (T(1) / T(2)) * z[54];
z[55] = abb[13] * z[54];
z[56] = z[0] + z[48];
z[57] = abb[2] * z[56];
z[58] = -abb[44] + z[47];
z[59] = -abb[49] + z[52];
z[60] = (T(2) / T(3)) * z[58] + z[59];
z[60] = abb[0] * z[60];
z[49] = -2 * abb[49] + z[49] + z[52];
z[49] = abb[8] * z[49];
z[61] = (T(1) / T(2)) * z[43];
z[62] = abb[24] * z[61];
z[63] = abb[19] * z[61];
z[1] = z[1] + z[3] + z[5] + z[7] + z[8] + -z[27] + -z[30] + z[40] + z[42] + 2 * z[49] + -z[51] + -z[55] + (T(1) / T(3)) * z[57] + z[60] + -z[62] + -z[63];
z[5] = prod_pow(m1_set::bc<T>[0], 2);
z[1] = z[1] * z[5];
z[7] = abb[45] + abb[46];
z[8] = abb[47] * (T(9) / T(2));
z[40] = abb[52] * (T(5) / T(2));
z[42] = abb[48] * (T(5) / T(2));
z[49] = abb[51] * (T(3) / T(2));
z[60] = (T(7) / T(2)) * z[7] + -z[8] + z[36] + -z[40] + -z[42] + z[49];
z[64] = abb[33] * z[60];
z[65] = abb[49] * (T(9) / T(2));
z[66] = 4 * abb[44];
z[67] = (T(-1) / T(2)) * z[0];
z[68] = z[65] + -z[66] + -z[67];
z[69] = z[4] + z[16];
z[70] = z[35] + z[69];
z[71] = abb[48] * (T(5) / T(4));
z[72] = abb[52] * (T(5) / T(4));
z[73] = z[71] + z[72];
z[74] = abb[46] * (T(13) / T(4)) + z[11] + -z[68] + -z[70] + -z[73];
z[74] = abb[30] * z[74];
z[75] = -z[36] + z[49];
z[76] = abb[47] * (T(3) / T(2));
z[77] = z[75] + z[76];
z[78] = z[19] + z[25] + -z[77];
z[79] = abb[29] * z[78];
z[80] = 2 * abb[46];
z[81] = -abb[48] + z[36];
z[82] = -abb[52] + z[80] + z[81];
z[83] = 3 * abb[49];
z[84] = z[82] + -z[83];
z[85] = 3 * abb[44] + -abb[45] + z[84];
z[86] = abb[32] * z[85];
z[87] = z[79] + 2 * z[86];
z[88] = 2 * abb[44];
z[89] = z[67] + -z[88];
z[90] = z[29] + z[89];
z[91] = abb[45] + -abb[46] + -abb[53] + z[23] + z[90];
z[91] = abb[27] * z[91];
z[74] = z[74] + z[87] + z[91];
z[74] = abb[27] * z[74];
z[91] = -abb[49] + abb[53];
z[91] = -z[17] + 2 * z[91];
z[25] = z[14] + -z[25] + z[88] + z[91];
z[92] = 3 * abb[58];
z[93] = z[25] * z[92];
z[10] = abb[47] * (T(-5) / T(2)) + abb[45] * (T(3) / T(2)) + -z[10] + z[14] + -z[23] + z[36];
z[94] = 3 * abb[60];
z[95] = z[10] * z[94];
z[96] = abb[62] * z[78];
z[97] = 3 * abb[34];
z[98] = z[26] * z[97];
z[99] = abb[45] * (T(1) / T(4));
z[100] = abb[44] + -abb[49];
z[34] = abb[47] * (T(1) / T(4)) + z[34] + z[99] + -z[100];
z[101] = prod_pow(abb[32], 2);
z[34] = z[34] * z[101];
z[102] = 3 * abb[47];
z[103] = 2 * abb[45];
z[104] = z[102] + -z[103];
z[82] = -z[82] + z[104];
z[105] = -abb[29] * z[82];
z[106] = abb[44] + z[0];
z[107] = -abb[45] + z[106];
z[108] = -abb[30] * z[107];
z[86] = -z[86] + z[105] + z[108];
z[105] = 2 * abb[30];
z[86] = z[86] * z[105];
z[34] = 3 * z[34] + z[64] + z[74] + z[86] + z[93] + z[95] + z[96] + z[98];
z[34] = abb[4] * z[34];
z[64] = -z[19] + z[49];
z[74] = abb[48] + abb[53];
z[86] = abb[45] + (T(-3) / T(2)) * z[0] + -z[38] + -z[64] + z[74];
z[86] = abb[7] * z[86];
z[93] = -z[18] + -z[22] + z[28];
z[95] = abb[8] * z[93];
z[96] = (T(3) / T(2)) * z[95];
z[98] = abb[19] * z[45];
z[98] = (T(3) / T(2)) * z[27] + z[98];
z[46] = z[46] + z[98];
z[85] = abb[1] * z[85];
z[108] = z[46] + -z[85];
z[109] = abb[24] + z[41];
z[109] = (T(1) / T(4)) * z[109];
z[109] = z[43] * z[109];
z[6] = -abb[53] + z[6] + z[17];
z[17] = -z[6] + z[22];
z[17] = abb[9] * z[17];
z[17] = z[17] + z[109];
z[109] = abb[46] * (T(1) / T(4));
z[110] = z[99] + z[109];
z[111] = abb[53] + z[32] + -z[69] + z[110];
z[111] = abb[3] * z[111];
z[112] = (T(3) / T(2)) * z[55];
z[113] = 3 * z[59];
z[114] = z[58] + z[113];
z[115] = abb[0] * (T(1) / T(2));
z[116] = -z[114] * z[115];
z[17] = 3 * z[17] + -z[57] + z[86] + z[96] + z[108] + z[111] + z[112] + z[116];
z[17] = abb[27] * z[17];
z[67] = -z[38] + z[67];
z[86] = abb[51] * (T(9) / T(4));
z[16] = z[16] + z[35] + -z[36] + z[67] + -z[73] + z[86] + z[109];
z[16] = abb[7] * z[16];
z[73] = z[29] + z[75];
z[39] = z[0] + -z[23] + -z[39] + z[73];
z[39] = abb[0] * z[39];
z[75] = 2 * z[85];
z[111] = z[39] + -z[75];
z[116] = 2 * z[57];
z[117] = abb[24] * z[45];
z[112] = z[112] + -z[116] + z[117];
z[46] = -z[46] + z[112];
z[117] = abb[14] * z[78];
z[118] = (T(3) / T(2)) * z[43];
z[119] = abb[25] * z[118];
z[117] = z[117] + -z[119];
z[13] = -abb[46] + z[13] + -z[100];
z[120] = 3 * abb[9];
z[13] = z[13] * z[120];
z[121] = z[13] + -z[117];
z[16] = z[16] + z[46] + -z[111] + z[121];
z[16] = abb[30] * z[16];
z[122] = abb[7] * z[78];
z[123] = abb[3] * z[78];
z[124] = z[122] + z[123];
z[125] = -z[81] + z[83];
z[126] = 2 * abb[52];
z[127] = -z[7] + -z[125] + z[126];
z[127] = abb[14] * z[127];
z[128] = z[41] * z[118];
z[129] = -abb[52] + z[37];
z[130] = z[120] * z[129];
z[128] = 3 * z[95] + -z[124] + 2 * z[127] + z[128] + z[130];
z[131] = -abb[29] * z[128];
z[132] = z[44] * z[118];
z[133] = abb[19] * z[118];
z[133] = 3 * z[27] + z[132] + z[133];
z[121] = 3 * z[85] + z[121] + -z[124] + -z[133];
z[121] = abb[32] * z[121];
z[16] = z[16] + z[17] + z[121] + z[131];
z[16] = abb[27] * z[16];
z[17] = z[31] + -z[36];
z[124] = abb[47] * (T(9) / T(4));
z[131] = abb[52] * (T(7) / T(4));
z[134] = z[124] + -z[131];
z[135] = abb[45] * (T(5) / T(4));
z[136] = z[4] + z[17] + z[33] + z[88] + z[134] + -z[135];
z[136] = abb[3] * z[136];
z[38] = -z[22] + z[24] + -z[38] + z[49] + -z[74];
z[38] = abb[7] * z[38];
z[3] = 3 * z[3] + -z[51];
z[51] = abb[44] + -abb[45] + abb[52];
z[73] = z[21] + z[51] + -z[73];
z[73] = abb[1] * z[73];
z[137] = z[41] * z[45];
z[30] = -z[3] + -z[30] + -z[38] + z[73] + z[96] + z[136] + z[137];
z[73] = prod_pow(abb[29], 2);
z[30] = z[30] * z[73];
z[96] = -5 * abb[45] + -3 * z[53];
z[96] = z[9] + -z[14] + (T(1) / T(2)) * z[96] + z[106];
z[106] = abb[7] * (T(1) / T(2));
z[96] = z[96] * z[106];
z[39] = z[39] + z[96] + -z[108] + -z[112] + -z[123];
z[39] = abb[30] * z[39];
z[96] = abb[46] * (T(5) / T(4));
z[36] = z[32] + -z[36] + z[70] + z[90] + -z[96];
z[36] = abb[27] * z[36];
z[18] = z[18] + -z[21];
z[18] = 3 * z[18];
z[70] = abb[45] * (T(5) / T(2));
z[108] = z[18] + -z[70];
z[2] = -z[2] + z[33] + -z[89] + (T(1) / T(2)) * z[108];
z[2] = abb[30] * z[2];
z[89] = abb[32] * z[78];
z[2] = z[2] + z[36] + -z[79] + z[89];
z[2] = abb[4] * z[2];
z[36] = 2 * abb[7];
z[79] = z[36] * z[58];
z[89] = 3 * abb[51];
z[74] = 2 * z[74] + -z[89];
z[108] = z[51] + z[74];
z[136] = abb[1] * z[108];
z[137] = 2 * abb[3];
z[138] = z[58] * z[137];
z[79] = z[79] + z[117] + -z[136] + z[138];
z[136] = -abb[29] + abb[32];
z[79] = -z[79] * z[136];
z[113] = -z[58] + z[113];
z[113] = z[113] * z[115];
z[46] = -z[46] + z[113];
z[113] = -3 * z[0];
z[115] = -abb[45] + z[53];
z[139] = -z[113] + (T(3) / T(2)) * z[115];
z[140] = abb[46] * (T(5) / T(2));
z[40] = -abb[44] + z[40] + z[139] + -z[140];
z[40] = z[40] * z[106];
z[40] = z[40] + z[46] + -z[85] + z[117];
z[40] = abb[27] * z[40];
z[141] = abb[0] + -abb[1] + abb[3];
z[141] = z[58] * z[141];
z[142] = abb[4] * z[107];
z[141] = -z[57] + z[141] + z[142];
z[141] = abb[31] * z[141];
z[2] = z[2] + z[39] + z[40] + z[79] + z[141];
z[2] = abb[31] * z[2];
z[39] = abb[53] + z[69] + -z[71] + -z[83] + -z[96] + z[131] + -z[135];
z[39] = abb[14] * z[39];
z[40] = -z[69] + z[72];
z[31] = -abb[53] + -z[0] + -z[31] + z[33] + z[35] + -z[40];
z[31] = abb[7] * z[31];
z[33] = -abb[52] + z[0];
z[35] = z[33] + z[125];
z[35] = abb[0] * z[35];
z[45] = abb[25] * z[45];
z[3] = z[3] + z[31] + z[35] + z[39] + z[45] + z[57];
z[3] = abb[28] * z[3];
z[31] = abb[6] * z[10];
z[35] = z[82] * z[137];
z[35] = 3 * z[31] + z[35];
z[39] = z[35] + z[117] + z[122] + z[132];
z[69] = abb[30] * z[39];
z[33] = z[33] + -z[74];
z[33] = abb[0] * z[33];
z[71] = 3 * z[55];
z[33] = z[33] + 4 * z[57] + -z[71];
z[36] = z[36] * z[56];
z[56] = abb[24] * z[118];
z[36] = z[33] + z[36] + -z[56] + z[117] + -z[123];
z[72] = abb[27] * z[36];
z[74] = abb[4] * z[78];
z[36] = z[36] + -z[74];
z[79] = -abb[31] * z[36];
z[96] = z[82] * z[105];
z[105] = -abb[27] * z[78];
z[96] = z[96] + z[105];
z[96] = abb[4] * z[96];
z[3] = z[3] + z[69] + z[72] + z[79] + z[96];
z[3] = abb[28] * z[3];
z[60] = abb[3] * z[60];
z[69] = 2 * z[0] + z[9] + -z[21] + -z[70] + z[77] + -z[140];
z[69] = abb[7] * z[69];
z[72] = -z[31] + -z[62];
z[33] = z[33] + z[60] + z[69] + 3 * z[72] + -z[132];
z[33] = abb[33] * z[33];
z[39] = -abb[29] * z[39];
z[60] = -z[0] + z[84] + z[88];
z[60] = abb[0] * z[60];
z[69] = -abb[3] * z[82];
z[72] = abb[7] * z[107];
z[60] = z[57] + z[60] + z[69] + z[72] + -z[85];
z[60] = abb[30] * z[60];
z[39] = z[39] + z[60] + -z[121];
z[39] = abb[30] * z[39];
z[60] = -abb[62] * z[128];
z[69] = z[11] + -z[21];
z[72] = 5 * abb[44];
z[79] = 3 * abb[46] + -abb[52] + z[72];
z[49] = z[49] + z[65] + -z[69] + -z[79] + z[103];
z[49] = abb[1] * z[49];
z[17] = -z[17] + -z[29] + z[40] + -z[110];
z[17] = abb[14] * z[17];
z[29] = abb[44] + abb[46];
z[6] = -z[6] + -z[22] + z[29];
z[6] = z[6] * z[120];
z[40] = abb[44] + -z[22] + z[76];
z[21] = abb[45] + abb[53] + -z[21] + -z[40];
z[21] = abb[3] * z[21];
z[6] = z[6] + z[17] + z[21] + -z[38] + -z[45] + z[49];
z[6] = abb[32] * z[6];
z[17] = z[127] + z[130];
z[21] = z[51] + -z[125];
z[21] = abb[1] * z[21];
z[38] = -z[81] + z[104];
z[45] = -abb[52] + z[38] + z[88];
z[45] = abb[3] * z[45];
z[49] = -abb[7] * z[108];
z[21] = z[17] + z[21] + z[45] + z[49];
z[21] = abb[29] * z[21];
z[6] = z[6] + z[21];
z[6] = abb[32] * z[6];
z[21] = abb[3] * z[10];
z[45] = z[9] + -z[28] + z[91];
z[45] = abb[14] * z[45];
z[41] = z[41] * z[61];
z[41] = z[41] + z[95];
z[49] = z[44] * z[61];
z[51] = abb[25] * z[61];
z[45] = z[41] + z[45] + z[49] + -z[51];
z[65] = abb[9] * z[129];
z[21] = -z[21] + z[31] + z[45] + z[65];
z[81] = -z[21] * z[94];
z[82] = abb[14] * z[93];
z[41] = -z[41] + -z[51] + z[82];
z[51] = abb[7] * z[54];
z[84] = abb[0] * z[59];
z[51] = z[41] + z[51] + z[57] + -z[62] + z[84];
z[57] = 3 * z[51] + -z[71];
z[57] = abb[59] * z[57];
z[52] = -z[47] + z[52] + z[100];
z[52] = abb[1] * z[52];
z[19] = abb[44] + z[19];
z[62] = z[19] + -z[22];
z[84] = abb[45] + z[53];
z[84] = -z[62] + (T(1) / T(2)) * z[84];
z[91] = abb[3] * z[84];
z[48] = -z[48] + z[53];
z[53] = -z[48] * z[106];
z[49] = -z[49] + z[52] + z[53] + -z[63] + z[91];
z[49] = z[49] * z[97];
z[20] = z[0] + -z[20] + z[23] + -z[24];
z[20] = abb[16] * z[20];
z[23] = z[0] + z[62] + (T(1) / T(2)) * z[115];
z[23] = abb[15] * z[23];
z[26] = abb[17] * z[26];
z[52] = abb[18] * z[54];
z[53] = abb[26] * z[61];
z[20] = z[20] + z[23] + -z[26] + -z[52] + -z[53];
z[20] = (T(3) / T(2)) * z[20];
z[23] = abb[63] * z[20];
z[26] = abb[9] * z[58];
z[26] = -z[26] + z[45] + z[63] + -z[85];
z[45] = -z[26] * z[92];
z[52] = abb[7] * z[84];
z[48] = abb[3] * z[48];
z[53] = abb[1] * z[59];
z[41] = z[41] + (T(-1) / T(2)) * z[48] + z[52] + z[53];
z[41] = 3 * z[41];
z[48] = abb[61] * z[41];
z[52] = abb[65] * z[78];
z[53] = -abb[34] + -abb[58];
z[53] = z[27] * z[53];
z[54] = z[73] + -z[101];
z[58] = abb[31] * z[136];
z[54] = -abb[34] + -abb[61] + (T(1) / T(2)) * z[54] + z[58];
z[5] = (T(-1) / T(2)) * z[5] + 3 * z[54];
z[5] = abb[5] * z[5] * z[84];
z[1] = abb[64] + z[1] + z[2] + z[3] + z[5] + z[6] + z[16] + z[23] + z[30] + z[33] + z[34] + z[39] + z[45] + z[48] + z[49] + z[52] + 3 * z[53] + z[57] + z[60] + z[81];
z[2] = -z[11] + z[99];
z[3] = abb[51] * (T(-15) / T(4)) + abb[48] * (T(7) / T(4)) + -z[2] + -z[67] + z[109] + -z[134];
z[3] = abb[7] * z[3];
z[5] = z[12] + -z[89];
z[6] = -abb[52] + -z[5] + -z[7] + z[102];
z[7] = abb[14] * z[6];
z[12] = 3 * abb[25];
z[16] = -z[12] + (T(9) / T(4)) * z[44];
z[16] = z[16] * z[43];
z[3] = z[3] + -z[7] + -z[13] + z[16] + z[35] + z[98] + z[111] + -z[112];
z[3] = abb[30] * z[3];
z[11] = abb[51] * (T(9) / T(2)) + -z[11] + -z[42];
z[13] = z[11] + -z[14] + z[40] + -z[70] + -z[113];
z[13] = abb[7] * z[13];
z[6] = abb[3] * z[6];
z[16] = abb[0] * z[114];
z[6] = z[6] + z[13] + z[16] + 2 * z[17] + -z[56] + -z[71] + z[75] + z[116] + -z[133];
z[6] = abb[27] * z[6];
z[2] = abb[46] * (T(7) / T(4)) + -z[2] + z[32] + -z[86] + -z[90] + -z[124];
z[2] = abb[4] * z[2];
z[5] = -z[5] + z[83] + z[103];
z[13] = z[66] + z[80];
z[16] = -z[5] + z[13];
z[16] = abb[1] * z[16];
z[17] = (T(-13) / T(2)) * z[47] + z[72] + -z[139];
z[17] = z[17] * z[106];
z[12] = z[12] * z[43];
z[2] = z[2] + z[7] + z[12] + z[16] + z[17] + -z[46] + -z[138];
z[2] = abb[31] * z[2];
z[7] = -z[9] + z[11] + -z[19] + z[24] + z[76];
z[7] = abb[7] * z[7];
z[11] = -z[31] + z[65] + z[127];
z[5] = z[5] + -z[88] + -z[126];
z[5] = abb[1] * z[5];
z[12] = 3 * abb[52] + -z[13] + -z[38];
z[12] = abb[3] * z[12];
z[5] = z[5] + z[7] + 3 * z[11] + z[12] + -6 * z[50] + -z[132];
z[5] = abb[29] * z[5];
z[9] = z[9] + z[70];
z[8] = -z[8] + z[9] + -z[64] + z[69] + -z[88];
z[8] = abb[3] * z[8];
z[11] = abb[52] + z[29] + -2 * z[37];
z[11] = abb[9] * z[11];
z[11] = z[11] + z[82];
z[12] = abb[45] + z[125];
z[12] = 2 * z[12] + -z[79];
z[12] = abb[1] * z[12];
z[7] = -z[7] + z[8] + 3 * z[11] + 2 * z[12] + -z[119] + z[133];
z[7] = abb[32] * z[7];
z[0] = z[0] + -z[9] + z[14] + z[18] + z[66];
z[0] = abb[27] * z[0];
z[8] = -abb[45] + z[15];
z[4] = -8 * abb[53] + abb[46] * (T(-29) / T(4)) + abb[47] * (T(27) / T(4)) + z[4] + (T(13) / T(4)) * z[8] + z[68];
z[4] = abb[30] * z[4];
z[0] = z[0] + z[4] + -z[87];
z[0] = abb[4] * z[0];
z[4] = -abb[28] * z[36];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = abb[4] * z[10];
z[2] = z[2] + -z[21];
z[2] = abb[37] * z[2];
z[3] = z[51] + -z[55];
z[3] = abb[36] * z[3];
z[4] = abb[4] * z[25];
z[4] = z[4] + -z[26] + -z[27];
z[4] = abb[35] * z[4];
z[5] = -abb[29] + abb[31];
z[5] = m1_set::bc<T>[0] * z[5];
z[5] = -abb[38] + z[5];
z[5] = abb[5] * z[5] * z[84];
z[2] = z[2] + z[3] + z[4] + z[5];
z[3] = z[74] + -z[128];
z[3] = abb[39] * z[3];
z[4] = abb[40] * z[20];
z[5] = abb[38] * z[41];
z[6] = z[22] + z[28] + -z[77];
z[6] = abb[42] * z[6];
z[0] = abb[41] + z[0] + 3 * z[2] + z[3] + z[4] + z[5] + z[6];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_635_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-16.315117475882714975232606427433962540778181982867325106331406861"),stof<T>("25.200457682612449653334884331424039000956326613344919618649303889")}, std::complex<T>{stof<T>("13.520532721999173173545069256485226228627114083772312677716535739"),stof<T>("-17.386759367193904843959491212689517294226368958474589454510441549")}, std::complex<T>{stof<T>("-2.7945847538835418016875371709487363121510678990950124286148711218"),stof<T>("7.8136983154185448093753931187345217067299576548703301641388623408")}, std::complex<T>{stof<T>("-9.987310931381147525231323173252721588276601788362186881344933583"),stof<T>("13.472297104405035124574425314534006023606117373844394579709013962")}, std::complex<T>{stof<T>("4.5363404964470617690693169990853290478736060834504222195264249611"),stof<T>("2.6010685132538211552717620307685730920260478585891584247501089139")}, std::complex<T>{stof<T>("21.590095009064260590928132338803059916851232462632860693614562856"),stof<T>("-18.700153116728953408072795080076455472820572684515625904561760222")}, std::complex<T>{stof<T>("-1.0031187058290361207555709158528244075230937880402964231548228049"),stof<T>("-6.5155307760426908746568279289240843626462994432193532995515365006")}, std::complex<T>{stof<T>("-0.7386370367344838466262089122837683281994443963151133677567310344"),stof<T>("-3.899236052629675089990327220579010436109706070240135289337434754")}, std::complex<T>{stof<T>("-7.0664435812360512966274921664650092807010245908202515927432043125"),stof<T>("7.8289245255777394387701317963110225412405031692603897496028551735")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(abs(k.W[81])) - rlog(abs(kbase.W[81])), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_635_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_635_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (v[2] + v[3]) * (-8 + 8 * v[0] + 4 * v[1] + 3 * v[2] + -v[3] + -4 * v[4] + -2 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -4 * v[5])) / prod_pow(tend, 2);
c[1] = (-(1 + m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;


		return (abb[43] + -abb[45] + -abb[46] + 3 * abb[47] + abb[48] + -abb[50] + -2 * abb[53]) * (2 * t * c[0] + -c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = -abb[27] + abb[31];
z[1] = -abb[28] + z[0];
z[1] = abb[28] * z[1];
z[0] = -abb[30] + z[0];
z[0] = abb[30] * z[0];
z[0] = -abb[33] + -z[0] + z[1];
z[1] = abb[43] + -abb[45] + -abb[46] + 3 * abb[47] + abb[48] + -abb[50] + -2 * abb[53];
return abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_635_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(8)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[43] + -abb[45] + -abb[46] + 3 * abb[47] + abb[48] + -abb[50] + -2 * abb[53]) * (2 * t * c[0] + -c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = abb[43] + -abb[45] + -abb[46] + 3 * abb[47] + abb[48] + -abb[50] + -2 * abb[53];
z[1] = abb[28] + -abb[30];
return abb[10] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_635_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("7.663911052344174559143628823059281987488577398391067804090459948"),stof<T>("22.959153459357400100908915587616044701579534801928719703358948151")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18, 81});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,66> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[13].real()/k.W[13].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(abs(kend.W[81])) - rlog(abs(k.W[81])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), T{0}, T{0}};
abb[41] = SpDLog_f_4_635_W_19_Im(t, path, abb);
abb[42] = SpDLogQ_W_82(k,dl,dlr).imag();
abb[64] = SpDLog_f_4_635_W_19_Re(t, path, abb);
abb[65] = SpDLogQ_W_82(k,dl,dlr).real();

                    
            return f_4_635_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_635_DLogXconstant_part(base_point<T>, kend);
	value += f_4_635_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_635_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_635_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_635_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_635_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_635_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_635_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
