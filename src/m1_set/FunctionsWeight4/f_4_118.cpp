/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_118.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_118_abbreviated (const std::array<T,58>& abb) {
T z[71];
z[0] = abb[52] * (T(1) / T(2));
z[1] = abb[53] * (T(1) / T(2)) + z[0];
z[2] = abb[48] + abb[50];
z[3] = z[1] + z[2];
z[4] = abb[49] + abb[51];
z[5] = (T(3) / T(2)) * z[4];
z[6] = z[3] + z[5];
z[6] = abb[4] * z[6];
z[7] = -abb[44] + abb[45] + -abb[46] + abb[47];
z[8] = abb[23] + abb[25];
z[9] = z[7] * z[8];
z[10] = (T(1) / T(2)) * z[7];
z[11] = abb[26] * z[10];
z[12] = z[9] + z[11];
z[6] = z[6] + -z[12];
z[13] = z[2] + z[4];
z[14] = (T(1) / T(2)) * z[13];
z[15] = abb[15] * z[14];
z[16] = abb[24] * z[10];
z[17] = z[6] + -z[15] + -z[16];
z[18] = abb[50] * (T(1) / T(2));
z[19] = abb[52] + abb[53];
z[5] = abb[48] * (T(1) / T(2)) + z[5] + z[18] + z[19];
z[20] = abb[7] * (T(1) / T(2));
z[5] = z[5] * z[20];
z[21] = z[4] + z[19];
z[22] = abb[17] * z[21];
z[23] = abb[27] * z[7];
z[22] = z[22] + z[23];
z[23] = abb[22] * z[7];
z[24] = z[22] + z[23];
z[25] = -abb[18] + abb[20];
z[26] = abb[54] * (T(1) / T(4));
z[25] = z[25] * z[26];
z[27] = z[2] + -z[19];
z[28] = abb[0] * z[27];
z[29] = (T(1) / T(4)) * z[28];
z[30] = abb[52] + z[14];
z[31] = -abb[8] * z[30];
z[32] = abb[53] + z[4];
z[33] = abb[14] * z[32];
z[5] = z[5] + (T(1) / T(2)) * z[17] + (T(-1) / T(4)) * z[24] + z[25] + z[29] + z[31] + -z[33];
z[5] = abb[33] * z[5];
z[17] = (T(1) / T(2)) * z[21];
z[24] = abb[6] * z[17];
z[25] = abb[54] * (T(1) / T(2));
z[31] = abb[19] * z[25];
z[34] = abb[48] + z[4];
z[35] = abb[13] * z[34];
z[31] = z[24] + -z[31] + -z[35];
z[36] = z[13] * z[20];
z[37] = z[31] + z[36];
z[14] = abb[4] * z[14];
z[38] = abb[1] * z[13];
z[14] = z[14] + z[38];
z[39] = (T(1) / T(2)) * z[4];
z[1] = -z[1] + z[39];
z[40] = abb[48] + z[1];
z[40] = abb[8] * z[40];
z[41] = (T(1) / T(2)) * z[9];
z[42] = abb[22] * z[10];
z[15] = z[15] + z[42];
z[42] = abb[18] * z[25];
z[43] = z[15] + -z[42];
z[44] = abb[20] * z[25];
z[40] = -z[14] + z[37] + z[40] + z[41] + z[43] + -z[44];
z[45] = abb[31] * z[40];
z[46] = abb[5] * z[17];
z[47] = z[11] + z[46];
z[30] = -abb[4] * z[30];
z[48] = abb[50] + abb[52];
z[48] = abb[8] * z[48];
z[30] = z[16] + z[30] + -z[37] + z[42] + z[47] + z[48];
z[30] = abb[32] * z[30];
z[37] = z[20] * z[21];
z[37] = z[33] + -z[37] + -z[46];
z[48] = abb[17] * z[17];
z[49] = abb[27] * z[10];
z[48] = z[48] + z[49];
z[32] = -abb[52] + z[32];
z[32] = abb[4] * z[32];
z[32] = -z[9] + z[32];
z[49] = abb[8] * z[17];
z[32] = (T(1) / T(2)) * z[32] + -z[37] + -z[48] + -z[49];
z[49] = -abb[35] * z[32];
z[50] = (T(1) / T(2)) * z[28];
z[51] = z[16] + z[50];
z[43] = -z[36] + -z[43] + z[44] + -z[51];
z[52] = abb[8] * z[13];
z[6] = z[6] + z[43] + -z[52];
z[52] = z[6] + -z[48];
z[52] = z[38] + (T(1) / T(2)) * z[52];
z[52] = abb[34] * z[52];
z[53] = abb[2] * z[21];
z[54] = abb[33] + -abb[35];
z[54] = z[53] * z[54];
z[5] = z[5] + z[30] + z[45] + z[49] + z[52] + z[54];
z[5] = m1_set::bc<T>[0] * z[5];
z[30] = abb[41] * z[40];
z[45] = (T(1) / T(4)) * z[13];
z[49] = abb[18] * z[45];
z[45] = abb[20] * z[45];
z[52] = (T(1) / T(4)) * z[21];
z[54] = abb[21] * z[52];
z[55] = abb[29] * abb[54];
z[56] = abb[0] * z[26];
z[45] = -z[45] + -z[49] + -z[54] + -z[55] + z[56];
z[49] = abb[19] * z[21];
z[54] = abb[28] * z[7];
z[49] = z[49] + z[54];
z[55] = abb[15] * z[25];
z[56] = -abb[4] * abb[54];
z[49] = (T(1) / T(2)) * z[49] + -z[55] + z[56];
z[56] = abb[8] * z[25];
z[57] = -abb[7] * z[26];
z[49] = -z[45] + (T(1) / T(2)) * z[49] + z[56] + z[57];
z[49] = abb[43] * z[49];
z[32] = z[32] + z[53];
z[56] = -abb[42] * z[32];
z[5] = z[5] + z[30] + z[49] + z[56];
z[5] = (T(1) / T(16)) * z[5];
z[30] = abb[16] * z[17];
z[49] = abb[8] * abb[50];
z[14] = -z[14] + z[30] + -z[31] + z[49] + z[51];
z[31] = abb[31] + -abb[32];
z[14] = z[14] * z[31];
z[31] = -abb[4] + abb[8];
z[31] = z[21] * z[31];
z[56] = abb[26] * z[7];
z[56] = z[9] + z[56];
z[57] = abb[16] * z[21];
z[31] = z[22] + z[31] + z[56] + -z[57];
z[31] = abb[35] * z[31];
z[58] = abb[33] * (T(1) / T(2));
z[59] = z[22] * z[58];
z[31] = z[31] + -z[59];
z[6] = -abb[33] * z[6];
z[6] = z[6] + -z[31];
z[27] = abb[8] * z[27];
z[1] = z[1] + z[2];
z[59] = abb[4] * z[1];
z[27] = z[11] + -z[27] + z[43] + -z[57] + z[59];
z[59] = z[27] + z[48];
z[59] = z[38] + (T(1) / T(2)) * z[59];
z[59] = abb[30] * z[59];
z[60] = abb[33] * z[38];
z[6] = (T(1) / T(2)) * z[6] + z[14] + z[59] + -z[60];
z[6] = abb[34] * z[6];
z[14] = abb[55] * z[40];
z[40] = abb[57] * z[45];
z[45] = abb[33] * z[21];
z[59] = -abb[35] * z[17];
z[45] = z[45] + z[59];
z[45] = abb[35] * z[45];
z[59] = abb[36] + -abb[40];
z[61] = z[21] * z[59];
z[62] = abb[39] * z[21];
z[63] = prod_pow(abb[33], 2);
z[64] = -z[17] * z[63];
z[45] = z[45] + z[61] + z[62] + z[64];
z[45] = abb[2] * z[45];
z[32] = abb[56] * z[32];
z[61] = abb[10] * z[19];
z[64] = -abb[8] * z[19];
z[64] = -z[11] + -z[48] + -z[61] + z[64];
z[64] = abb[36] * z[64];
z[65] = -abb[19] * z[17];
z[54] = (T(-1) / T(2)) * z[54] + z[55] + z[65];
z[54] = abb[57] * z[54];
z[55] = abb[39] * z[56];
z[65] = abb[54] * abb[57];
z[66] = -z[62] + z[65];
z[66] = abb[4] * z[66];
z[54] = z[54] + z[55] + z[66];
z[55] = -z[4] + z[19];
z[66] = -abb[38] + abb[40];
z[66] = z[55] * z[66];
z[67] = abb[52] + z[4];
z[68] = -abb[53] + z[67];
z[69] = abb[39] * z[68];
z[70] = abb[52] * z[63];
z[65] = -z[65] + z[66] + z[69] + z[70];
z[66] = abb[8] * (T(1) / T(2));
z[65] = z[65] * z[66];
z[69] = abb[38] * z[55];
z[70] = abb[36] * z[55];
z[62] = -z[62] + z[69] + z[70];
z[69] = abb[40] * z[4];
z[62] = (T(1) / T(2)) * z[62] + z[69];
z[62] = abb[16] * z[62];
z[69] = abb[48] + abb[53];
z[0] = z[0] + z[4] + z[18] + (T(1) / T(2)) * z[69];
z[69] = -z[0] * z[63];
z[59] = abb[38] + z[59];
z[70] = z[21] * z[59];
z[25] = abb[57] * z[25];
z[25] = z[25] + z[69] + z[70];
z[25] = z[20] * z[25];
z[69] = abb[4] * abb[52];
z[11] = -z[11] + z[37] + z[69];
z[37] = -abb[33] * z[11];
z[11] = z[11] + z[30];
z[11] = abb[35] * z[11];
z[57] = z[57] * z[58];
z[11] = (T(1) / T(2)) * z[11] + z[37] + -z[57];
z[11] = abb[35] * z[11];
z[37] = abb[15] * z[13];
z[7] = abb[24] * z[7];
z[23] = z[7] + z[23] + z[37];
z[13] = abb[4] * z[13];
z[37] = abb[19] * abb[54];
z[58] = z[13] + -z[23] + -z[28] + -z[37];
z[70] = abb[9] * z[2];
z[44] = z[38] + z[44] + z[70];
z[2] = -abb[8] * z[2];
z[2] = z[2] + -z[30] + z[44] + (T(1) / T(2)) * z[58];
z[2] = abb[37] * z[2];
z[16] = -z[16] + z[24];
z[24] = -z[41] + z[61];
z[58] = -z[16] + -z[24];
z[58] = abb[38] * z[58];
z[42] = -z[42] + z[50];
z[50] = z[33] + z[38] + -z[42];
z[50] = (T(1) / T(2)) * z[50];
z[50] = z[50] * z[63];
z[61] = abb[4] * z[17];
z[24] = z[24] + z[61];
z[24] = abb[40] * z[24];
z[61] = -abb[11] * z[67];
z[46] = z[46] + z[61];
z[46] = abb[39] * z[46];
z[2] = z[2] + z[6] + z[11] + -z[14] + z[24] + z[25] + z[32] + z[40] + z[45] + z[46] + z[50] + (T(1) / T(2)) * z[54] + z[58] + z[62] + z[64] + z[65];
z[6] = z[35] + -z[38] + z[49];
z[11] = -3 * z[4] + -z[19];
z[11] = abb[16] * z[11];
z[7] = z[7] + -z[11];
z[11] = abb[50] + z[4];
z[11] = abb[12] * z[11];
z[14] = z[11] + -z[70];
z[19] = abb[6] * z[52];
z[24] = abb[19] * z[26];
z[20] = -abb[50] * z[20];
z[7] = -z[6] + (T(-1) / T(4)) * z[7] + (T(3) / T(4)) * z[13] + (T(1) / T(2)) * z[14] + z[19] + z[20] + -z[24] + -z[29];
z[7] = abb[31] * z[7];
z[1] = abb[8] * z[1];
z[1] = -z[1] + z[13] + -z[41] + z[43];
z[14] = abb[33] * z[1];
z[7] = z[7] + z[14] + -z[57];
z[7] = (T(1) / T(2)) * z[7] + z[60];
z[7] = abb[31] * z[7];
z[0] = abb[4] * z[0];
z[14] = -z[37] + -z[56];
z[3] = -z[3] + -z[39];
z[3] = abb[8] * z[3];
z[0] = z[0] + z[3] + (T(1) / T(2)) * z[14] + -z[15] + z[44] + -z[48] + -z[51];
z[0] = abb[30] * z[0];
z[3] = -abb[33] * z[27];
z[3] = z[3] + z[31];
z[3] = (T(1) / T(2)) * z[3] + -z[60];
z[1] = -z[1] + z[30];
z[1] = (T(1) / T(2)) * z[1] + -z[38];
z[1] = abb[31] * z[1];
z[0] = (T(1) / T(4)) * z[0] + z[1] + (T(1) / T(2)) * z[3];
z[0] = abb[30] * z[0];
z[1] = abb[7] * z[21];
z[3] = abb[20] * abb[54];
z[1] = z[1] + z[3] + -z[22] + -z[23] + z[53];
z[3] = z[12] + z[24] + z[33] + (T(1) / T(2)) * z[35];
z[12] = abb[52] + abb[53] + abb[48] * (T(5) / T(3));
z[12] = abb[50] * (T(5) / T(24)) + (T(1) / T(3)) * z[4] + (T(1) / T(8)) * z[12];
z[12] = abb[4] * z[12];
z[14] = abb[52] + z[34];
z[14] = (T(-1) / T(3)) * z[14] + -z[18];
z[14] = z[14] * z[66];
z[1] = (T(1) / T(12)) * z[1] + (T(-1) / T(6)) * z[3] + z[12] + z[14] + (T(-1) / T(24)) * z[28] + (T(1) / T(4)) * z[38];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[3] = abb[7] * abb[50];
z[4] = abb[16] * z[4];
z[3] = z[3] + z[4] + -z[11];
z[4] = -z[47] + z[69];
z[8] = z[8] * z[10];
z[10] = z[66] * z[68];
z[8] = -z[3] + -z[4] + z[8] + z[10] + -z[16];
z[10] = abb[32] * (T(1) / T(2));
z[8] = z[8] * z[10];
z[11] = abb[8] * z[55];
z[12] = abb[18] * abb[54];
z[9] = -z[9] + z[11] + z[12] + z[13] + z[37];
z[9] = (T(1) / T(2)) * z[9] + -z[36] + z[38] + -z[70];
z[9] = abb[30] * z[9];
z[11] = -abb[8] * abb[52];
z[4] = z[4] + z[11] + z[36] + z[42];
z[4] = abb[33] * z[4];
z[3] = z[3] + z[6] + -z[13] + z[70];
z[3] = abb[31] * z[3];
z[3] = z[3] + z[4] + z[8] + z[9] + z[57] + -z[60];
z[3] = z[3] * z[10];
z[4] = -abb[30] * z[21];
z[6] = abb[32] * z[17];
z[4] = z[4] + z[6];
z[4] = abb[32] * z[4];
z[6] = abb[37] + abb[39] + z[59];
z[6] = z[6] * z[21];
z[8] = prod_pow(abb[30], 2) * z[17];
z[4] = z[4] + z[6] + z[8];
z[4] = abb[3] * z[4];
z[0] = z[0] + z[1] + (T(1) / T(2)) * z[2] + z[3] + (T(1) / T(4)) * z[4] + z[7];
z[0] = (T(1) / T(8)) * z[0];
return {z[5], z[0]};
}


template <typename T> std::complex<T> f_4_118_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.37388469366181402260500207566371944127149650153181200311633521232"),stof<T>("-0.14991815194018177781029840987663503331173284279264369156672139289")}, std::complex<T>{stof<T>("-0.74201942046551508413928863172573184438203149039584953997042159767"),stof<T>("-0.239300485380256874911974132104265884203365260508724706707923318")}, std::complex<T>{stof<T>("-0.40933152775077074588134176019310321407712382124744510035144698706"),stof<T>("-0.16103286779284859038451897686828411058535844111275139476877162809")}, std::complex<T>{stof<T>("-0.74201942046551508413928863172573184438203149039584953997042159767"),stof<T>("-0.239300485380256874911974132104265884203365260508724706707923318")}, std::complex<T>{stof<T>("-0.42266180509443308150510225805200736682110758760045804868042735089"),stof<T>("-0.16103286779284859038451897686828411058535844111275139476877162809")}, std::complex<T>{stof<T>("-0.35352611360417871083052009615230804847780643118374766429767337313"),stof<T>("-0.0893823334400750971016757222276308508916324177160810151412019251")}, std::complex<T>{stof<T>("-0.34913764372941623396282949923612243757713194499727521855068316693"),stof<T>("-0.10954301550664013236867633762511965301720890386360536038993468996")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_118_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_118_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.05228749863635302844347072576415118845455338957185586304271570779"),stof<T>("-0.17378114512730445888008483338244308103428996613747556767710587947")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,58> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_118_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_118_DLogXconstant_part(base_point<T>, kend);
	value += f_4_118_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_118_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_118_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_118_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_118_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_118_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_118_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
