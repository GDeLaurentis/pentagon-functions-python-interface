/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_47.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_47_abbreviated (const std::array<T,28>& abb) {
T z[36];
z[0] = abb[23] + abb[24];
z[1] = abb[3] * z[0];
z[2] = abb[1] * z[0];
z[3] = (T(1) / T(2)) * z[1] + z[2];
z[4] = abb[18] + abb[19];
z[5] = abb[21] + 3 * z[4];
z[6] = abb[20] + -2 * abb[25] + (T(1) / T(4)) * z[5];
z[7] = abb[9] * z[6];
z[8] = (T(3) / T(4)) * z[0];
z[9] = abb[6] * z[8];
z[7] = z[7] + -z[9];
z[9] = -abb[22] + abb[24];
z[9] = abb[4] * z[9];
z[10] = abb[24] * (T(1) / T(4));
z[11] = abb[22] + abb[23] * (T(5) / T(4)) + z[10];
z[11] = abb[0] * z[11];
z[12] = 2 * abb[20] + -4 * abb[25] + (T(1) / T(2)) * z[5];
z[13] = abb[7] * z[12];
z[14] = abb[8] * z[6];
z[15] = (T(1) / T(4)) * z[0];
z[16] = abb[2] * z[15];
z[17] = -z[3] + -z[7] + -z[9] + z[11] + z[13] + z[14] + -z[16];
z[17] = abb[11] * z[17];
z[7] = z[7] + z[14];
z[14] = 2 * z[2];
z[18] = z[1] + z[14];
z[19] = abb[0] * z[15];
z[16] = -z[7] + z[16] + -z[18] + z[19];
z[20] = abb[13] + -abb[14];
z[20] = z[16] * z[20];
z[21] = abb[8] * z[12];
z[12] = abb[9] * z[12];
z[22] = z[12] + z[21];
z[23] = abb[6] * (T(3) / T(2));
z[23] = z[0] * z[23];
z[24] = z[22] + -z[23];
z[25] = abb[0] * (T(1) / T(2));
z[25] = z[0] * z[25];
z[26] = abb[2] * (T(1) / T(2));
z[26] = z[0] * z[26];
z[27] = z[25] + z[26];
z[28] = 2 * abb[3];
z[29] = z[0] * z[28];
z[30] = 4 * z[2] + z[24] + -z[27] + z[29];
z[30] = abb[12] * z[30];
z[17] = z[17] + z[20] + z[30];
z[17] = abb[11] * z[17];
z[0] = abb[22] + z[0];
z[0] = abb[5] * z[0];
z[20] = z[0] + z[9];
z[31] = abb[8] + abb[9];
z[4] = abb[21] * (T(1) / T(3)) + z[4];
z[4] = abb[25] * (T(-2) / T(3)) + abb[20] * (T(1) / T(3)) + (T(1) / T(4)) * z[4];
z[4] = z[4] * z[31];
z[15] = -abb[6] * z[15];
z[32] = abb[24] * (T(1) / T(12));
z[33] = abb[22] + abb[23] * (T(-1) / T(12)) + -z[32];
z[33] = abb[2] * z[33];
z[34] = abb[24] * (T(1) / T(2));
z[35] = abb[23] * (T(4) / T(3)) + -z[34];
z[35] = abb[3] * z[35];
z[32] = -abb[22] + abb[23] * (T(-13) / T(12)) + -z[32];
z[32] = abb[0] * z[32];
z[4] = (T(-1) / T(6)) * z[2] + z[4] + z[15] + (T(5) / T(6)) * z[20] + z[32] + z[33] + z[35];
z[4] = prod_pow(m1_set::bc<T>[0], 2) * z[4];
z[15] = -abb[14] * z[16];
z[10] = -abb[22] + abb[23] * (T(1) / T(4)) + z[10];
z[10] = abb[2] * z[10];
z[7] = -z[7] + z[10];
z[10] = z[0] + z[13];
z[3] = -z[3] + z[7] + -z[10] + -z[19];
z[3] = abb[13] * z[3];
z[3] = z[3] + z[15] + z[30];
z[3] = abb[13] * z[3];
z[15] = 3 * z[2];
z[7] = z[0] + z[7] + z[11] + -z[15] + -z[29];
z[11] = prod_pow(abb[12], 2);
z[7] = z[7] * z[11];
z[16] = -abb[23] * z[28];
z[2] = -z[2] + z[16] + -z[21] + z[27];
z[2] = abb[14] * z[2];
z[2] = z[2] + -z[30];
z[2] = abb[14] * z[2];
z[16] = abb[0] + abb[2];
z[16] = -abb[6] + -3 * z[16];
z[6] = z[6] * z[16];
z[16] = abb[3] + -2 * abb[10];
z[5] = -4 * abb[20] + 8 * abb[25] + -z[5];
z[16] = z[5] * z[16];
z[8] = z[8] * z[31];
z[6] = z[6] + z[8] + z[16];
z[6] = abb[15] * z[6];
z[1] = (T(3) / T(2)) * z[1] + z[15] + -z[23];
z[8] = abb[22] + abb[23] * (T(3) / T(2)) + z[34];
z[8] = abb[0] * z[8];
z[8] = -z[1] + z[8] + -z[12] + z[13];
z[13] = -abb[26] * z[8];
z[15] = abb[23] * (T(1) / T(2)) + z[34];
z[16] = -abb[22] + z[15];
z[16] = abb[2] * z[16];
z[1] = z[1] + z[10] + -z[16] + z[22];
z[10] = abb[27] * z[1];
z[11] = abb[26] + z[11];
z[11] = z[9] * z[11];
z[2] = z[2] + z[3] + z[4] + z[6] + z[7] + z[10] + z[11] + z[13] + z[17];
z[3] = 2 * abb[22];
z[4] = z[3] + -z[15];
z[4] = abb[2] * z[4];
z[5] = abb[7] * z[5];
z[0] = 2 * z[0] + z[4] + -z[5] + z[18] + z[24] + z[25];
z[0] = abb[13] * z[0];
z[4] = z[12] + -z[21] + -z[23];
z[3] = abb[23] * (T(-5) / T(2)) + -z[3] + -z[34];
z[3] = abb[0] * z[3];
z[3] = z[3] + z[4] + z[5] + 2 * z[9] + z[18] + z[26];
z[3] = abb[11] * z[3];
z[5] = abb[23] + -abb[24];
z[5] = z[5] * z[28];
z[4] = -z[4] + z[5] + -z[14] + -z[27];
z[4] = abb[14] * z[4];
z[0] = z[0] + z[3] + z[4] + -z[30];
z[0] = m1_set::bc<T>[0] * z[0];
z[3] = -z[8] + z[9];
z[3] = abb[16] * z[3];
z[1] = abb[17] * z[1];
z[0] = z[0] + z[1] + z[3];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_47_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-4.508486263436370352178905277044479972582659063860789597164506018"),stof<T>("-16.998743050228066886647040890658569908971995091117554097951430544")}, std::complex<T>{stof<T>("-4.508486263436370352178905277044479972582659063860789597164506018"),stof<T>("-16.998743050228066886647040890658569908971995091117554097951430544")}, std::complex<T>{stof<T>("-6.01131501791516046957187370272597329677687875181438612955267469"),stof<T>("-22.664990733637422515529387854211426545295993454823405463935240726")}, std::complex<T>{stof<T>("-1.5028287544787901173929684256814933241942196879535965323881686726"),stof<T>("-5.6662476834093556288823469635528566363239983637058513659838101814")}, std::complex<T>{stof<T>("8.0885085726669560109769656232465853019977678850852492578030463463"),stof<T>("4.8963734708689118558176508915452956632201505148969660538454934567")}, std::complex<T>{stof<T>("20.209707128256641876316532731718935318378538752336729170370716894"),stof<T>("23.947443884086211780067193519173468889068404920911748446283022327")}, std::complex<T>{stof<T>("0.0875340684049758888213211171268003955782961357385096042439748947"),stof<T>("2.0516170860382127646885150831056720520244839102504771303436996145")}, std::complex<T>{stof<T>("12.022630035830320939143747405451946593553757503628772259105349381"),stof<T>("45.329981467274845031058775708422853090591986909646810927870481451")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real()), rlog(k.W[194].real()/kbase.W[194].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_47_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_47_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-16.285582109358444254325096166346334524939744028479101696852962418"),stof<T>("3.080614740548346782102164991542588383326623578300370203666636236")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,28> abb = {dl[0], dl[5], dl[2], dl[4], dlog_W15(k,dl), dlog_W22(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), rlog(kend.W[194].real()/k.W[194].real()), f_2_4_re(k), f_2_7_re(k)};

                    
            return f_4_47_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_47_DLogXconstant_part(base_point<T>, kend);
	value += f_4_47_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_47_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_47_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_47_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_47_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_47_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_47_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
