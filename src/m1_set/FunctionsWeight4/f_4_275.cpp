/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_275.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_275_abbreviated (const std::array<T,54>& abb) {
T z[84];
z[0] = abb[43] * (T(1) / T(2));
z[1] = abb[49] * (T(1) / T(2));
z[2] = z[0] + z[1];
z[3] = abb[44] * (T(1) / T(2));
z[4] = abb[45] + abb[46];
z[5] = -z[2] + -z[3] + z[4];
z[5] = abb[42] + (T(1) / T(2)) * z[5];
z[5] = abb[7] * z[5];
z[6] = abb[9] * abb[49];
z[7] = abb[9] * abb[47];
z[6] = z[6] + -z[7];
z[8] = abb[43] + abb[49];
z[9] = -z[4] + z[8];
z[10] = 3 * abb[42];
z[11] = z[9] + -z[10];
z[12] = abb[2] * z[11];
z[13] = (T(1) / T(2)) * z[4];
z[14] = -abb[47] + z[1];
z[15] = z[13] + z[14];
z[15] = abb[8] * z[15];
z[16] = z[0] + z[3];
z[17] = -z[4] + z[16];
z[18] = abb[49] * (T(3) / T(4));
z[19] = abb[47] + (T(1) / T(2)) * z[17] + -z[18];
z[19] = abb[14] * z[19];
z[20] = abb[12] * abb[43];
z[21] = abb[24] * abb[50];
z[22] = abb[9] + abb[12];
z[23] = -abb[42] * z[22];
z[5] = z[5] + z[6] + -z[12] + z[15] + z[19] + z[20] + (T(-1) / T(4)) * z[21] + z[23];
z[15] = prod_pow(abb[32], 2);
z[5] = z[5] * z[15];
z[19] = abb[44] * (T(3) / T(2));
z[23] = -z[2] + -z[4] + z[19];
z[24] = abb[7] * z[23];
z[25] = abb[8] * z[23];
z[26] = z[24] + z[25];
z[27] = abb[41] + -abb[48];
z[28] = -z[1] + z[16] + z[27];
z[29] = abb[13] * z[28];
z[30] = abb[50] * (T(1) / T(2));
z[31] = abb[20] * z[30];
z[31] = z[29] + -z[31];
z[32] = abb[21] + abb[23];
z[33] = z[30] * z[32];
z[34] = -z[31] + z[33];
z[35] = 2 * abb[43];
z[36] = z[4] + z[35];
z[37] = -abb[49] + z[36];
z[38] = 3 * abb[48];
z[39] = -3 * abb[41] + -z[37] + z[38];
z[40] = abb[1] * z[39];
z[41] = z[34] + -z[40];
z[42] = abb[6] * z[23];
z[43] = z[1] + z[3];
z[44] = abb[43] * (T(-3) / T(2)) + -z[4] + z[43];
z[45] = 2 * abb[41];
z[46] = 2 * abb[48] + z[44] + -z[45];
z[46] = abb[4] * z[46];
z[47] = z[4] + z[27];
z[48] = abb[43] + -abb[44];
z[49] = z[47] + z[48];
z[49] = abb[11] * z[49];
z[46] = z[26] + -z[41] + -z[42] + z[46] + z[49];
z[50] = abb[51] * z[46];
z[51] = -abb[44] + abb[49];
z[52] = -abb[47] + z[4] + z[51];
z[52] = abb[10] * z[52];
z[14] = -z[14] + (T(1) / T(2)) * z[48];
z[48] = abb[3] * z[14];
z[53] = -z[48] + z[52];
z[54] = z[33] + z[53];
z[55] = abb[22] * z[30];
z[56] = -z[42] + z[55];
z[57] = 2 * abb[47];
z[17] = abb[49] * (T(-3) / T(2)) + z[17] + z[57];
z[17] = abb[14] * z[17];
z[58] = z[17] + z[26] + z[54] + z[56];
z[58] = abb[35] * z[58];
z[59] = abb[4] * z[28];
z[60] = abb[14] * z[14];
z[59] = z[59] + -z[60];
z[61] = z[42] + z[49] + z[52] + -z[59];
z[13] = abb[47] * (T(1) / T(2)) + -z[0] + -z[13] + -z[27];
z[13] = abb[1] * z[13];
z[62] = abb[22] * abb[50];
z[13] = z[13] + (T(1) / T(2)) * z[61] + (T(-1) / T(4)) * z[62];
z[13] = prod_pow(abb[28], 2) * z[13];
z[61] = abb[25] * abb[50];
z[63] = z[21] + z[61];
z[63] = (T(1) / T(2)) * z[63];
z[64] = -abb[44] + z[8];
z[64] = -abb[42] + (T(1) / T(2)) * z[64];
z[65] = abb[15] * z[64];
z[66] = -z[12] + z[65];
z[67] = abb[7] * z[64];
z[67] = -z[66] + z[67];
z[17] = z[17] + z[53] + -z[63] + -z[67];
z[17] = abb[33] * z[17];
z[53] = abb[43] + abb[47] + -abb[49] + z[27];
z[53] = abb[1] * z[53];
z[53] = z[53] + z[55];
z[59] = z[31] + -z[48] + -z[53] + -z[59];
z[59] = abb[34] * z[59];
z[68] = abb[24] + z[32];
z[69] = z[30] * z[68];
z[69] = -z[48] + z[69];
z[70] = abb[4] + -abb[5];
z[51] = -abb[43] + z[51];
z[71] = (T(1) / T(2)) * z[51];
z[70] = z[70] * z[71];
z[14] = -abb[8] * z[14];
z[14] = z[14] + z[60] + z[69] + z[70];
z[14] = abb[36] * z[14];
z[60] = -z[52] + z[56] + z[69];
z[69] = 3 * abb[44];
z[70] = -abb[43] + abb[49] + z[69];
z[70] = -abb[47] + (T(1) / T(4)) * z[70];
z[70] = abb[8] * z[70];
z[60] = (T(1) / T(2)) * z[60] + z[70];
z[70] = prod_pow(abb[29], 2);
z[60] = z[60] * z[70];
z[72] = abb[50] * z[32];
z[72] = z[61] + z[72];
z[44] = abb[4] * z[44];
z[73] = abb[8] * z[71];
z[44] = z[44] + -z[67] + (T(-1) / T(2)) * z[72] + z[73];
z[67] = abb[52] * z[44];
z[5] = z[5] + z[13] + z[14] + z[17] + z[50] + z[58] + z[59] + z[60] + z[67];
z[13] = abb[43] * (T(1) / T(6));
z[14] = abb[47] * (T(13) / T(2));
z[17] = abb[49] * (T(19) / T(6)) + (T(10) / T(3)) * z[4] + z[13] + -z[14] + (T(7) / T(2)) * z[27];
z[17] = abb[1] * z[17];
z[27] = abb[24] * (T(1) / T(2)) + z[32];
z[27] = abb[50] * z[27];
z[32] = abb[25] * z[30];
z[27] = z[27] + -z[31] + z[32];
z[43] = -abb[48] + z[43];
z[50] = -abb[41] + z[4];
z[13] = abb[42] * (T(-5) / T(6)) + z[13] + z[43] + (T(-1) / T(6)) * z[50];
z[13] = abb[4] * z[13];
z[50] = (T(4) / T(3)) * z[4];
z[14] = abb[49] * (T(-13) / T(6)) + abb[43] * (T(5) / T(6)) + z[14] + -z[50] + -z[69];
z[14] = abb[8] * z[14];
z[3] = abb[42] * (T(-7) / T(6)) + abb[48] * (T(1) / T(2)) + abb[43] * (T(1) / T(3)) + abb[41] * (T(5) / T(6)) + -z[1] + z[3] + -z[50];
z[3] = abb[7] * z[3];
z[9] = -abb[42] + (T(1) / T(3)) * z[9];
z[9] = abb[2] * z[9];
z[50] = abb[41] + abb[42];
z[58] = -abb[49] + z[50];
z[59] = abb[0] * z[58];
z[60] = abb[12] * z[10];
z[3] = z[3] + (T(7) / T(2)) * z[9] + z[13] + z[14] + z[17] + -3 * z[20] + -z[27] + -2 * z[49] + -z[55] + (T(13) / T(6)) * z[59] + z[60] + z[65];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[9] = abb[50] * z[68];
z[9] = z[9] + z[61];
z[13] = abb[20] * abb[50];
z[13] = z[9] + z[13];
z[14] = abb[9] * abb[42];
z[6] = -z[6] + z[14];
z[13] = z[6] + (T(-1) / T(4)) * z[13] + (T(1) / T(2)) * z[29];
z[14] = abb[48] * (T(3) / T(2)) + -z[4];
z[17] = abb[41] * (T(3) / T(2));
z[29] = abb[47] * (T(3) / T(2)) + -z[2] + z[14] + -z[17];
z[29] = abb[1] * z[29];
z[38] = -z[4] + z[38];
z[60] = -abb[43] + (T(1) / T(2)) * z[38];
z[61] = abb[41] * (T(1) / T(2));
z[67] = -abb[42] + z[61];
z[68] = -z[1] + z[60] + -z[67];
z[68] = abb[4] * z[68];
z[72] = abb[42] * (T(1) / T(2));
z[73] = -abb[41] + z[72];
z[74] = -abb[43] + z[4];
z[75] = z[1] + z[73] + (T(1) / T(2)) * z[74];
z[75] = abb[7] * z[75];
z[76] = (T(1) / T(2)) * z[12];
z[77] = (T(3) / T(2)) * z[65];
z[62] = (T(3) / T(4)) * z[62];
z[78] = -z[62] + z[77];
z[79] = 3 * abb[47];
z[80] = -z[74] + z[79];
z[81] = abb[49] + (T(-1) / T(2)) * z[80];
z[81] = abb[14] * z[81];
z[82] = 2 * z[59];
z[13] = 3 * z[13] + (T(-1) / T(2)) * z[25] + z[29] + (T(-3) / T(2)) * z[48] + z[68] + z[75] + -z[76] + z[78] + z[81] + -z[82];
z[13] = abb[27] * z[13];
z[29] = z[45] + z[72];
z[16] = -abb[48] + z[16];
z[48] = abb[49] * (T(5) / T(4)) + (T(-3) / T(2)) * z[16] + -z[29];
z[48] = abb[4] * z[48];
z[31] = z[31] + z[63];
z[63] = (T(1) / T(2)) * z[59];
z[68] = -z[12] + z[63] + z[77];
z[75] = abb[14] * z[23];
z[16] = abb[41] + z[1] + 3 * z[16];
z[16] = -abb[42] + (T(1) / T(2)) * z[16];
z[16] = abb[7] * z[16];
z[16] = z[16] + (T(3) / T(2)) * z[31] + z[40] + z[48] + -z[62] + -z[68] + -z[75];
z[31] = -abb[30] + abb[31];
z[16] = z[16] * z[31];
z[23] = abb[4] * z[23];
z[31] = z[23] + z[75];
z[9] = (T(1) / T(2)) * z[9] + -z[66];
z[11] = abb[7] * z[11];
z[9] = 3 * z[9] + z[11] + z[25] + -z[31];
z[9] = abb[32] * z[9];
z[11] = z[34] + z[52] + z[53];
z[4] = 2 * z[4];
z[48] = z[4] + -z[19];
z[53] = abb[49] * (T(5) / T(2)) + -z[0];
z[66] = -z[48] + -z[53] + z[79];
z[66] = abb[14] * z[66];
z[77] = -abb[4] * z[39];
z[11] = 3 * z[11] + z[26] + z[66] + z[77];
z[11] = abb[28] * z[11];
z[66] = 2 * abb[49];
z[77] = -z[66] + z[80];
z[79] = abb[14] * z[77];
z[23] = -z[23] + 2 * z[79];
z[80] = z[23] + z[26];
z[54] = 3 * z[54] + z[80];
z[81] = -abb[29] * z[54];
z[11] = z[9] + z[11] + z[13] + z[16] + z[81];
z[11] = abb[27] * z[11];
z[13] = abb[44] * (T(3) / T(4));
z[16] = z[13] + z[18];
z[81] = 4 * abb[41] + abb[48] * (T(-9) / T(2)) + abb[43] * (T(13) / T(4)) + z[4] + -z[16] + -z[72];
z[81] = abb[4] * z[81];
z[13] = z[13] + -z[18];
z[18] = abb[43] * (T(5) / T(4)) + -z[14];
z[67] = z[13] + -z[18] + -z[67];
z[67] = abb[7] * z[67];
z[27] = (T(3) / T(2)) * z[27];
z[83] = 3 * z[49];
z[62] = -z[25] + z[27] + -2 * z[40] + z[62] + -z[67] + -z[68] + z[81] + -z[83];
z[67] = abb[31] * z[62];
z[35] = -abb[42] + z[35] + -z[38] + z[45];
z[35] = abb[7] * z[35];
z[38] = abb[8] * z[37];
z[45] = abb[12] * abb[42];
z[45] = -z[20] + z[45];
z[36] = z[36] + -z[50];
z[50] = 2 * abb[4];
z[36] = z[36] * z[50];
z[35] = z[12] + z[35] + z[36] + z[38] + z[40] + 3 * z[45] + z[59];
z[35] = abb[30] * z[35];
z[36] = -z[41] + -z[55];
z[39] = z[39] * z[50];
z[26] = z[26] + 3 * z[36] + z[39] + -z[75] + z[83];
z[26] = abb[28] * z[26];
z[36] = z[37] * z[50];
z[21] = (T(-3) / T(2)) * z[21] + -z[24] + z[36] + z[38] + z[75];
z[36] = abb[29] + -abb[32];
z[21] = z[21] * z[36];
z[21] = z[21] + z[26] + z[35] + z[67];
z[21] = abb[30] * z[21];
z[25] = z[25] + z[63];
z[1] = -z[1] + z[17] + -z[60];
z[1] = abb[1] * z[1];
z[14] = z[14] + -z[61];
z[17] = z[0] + -z[14] + z[72];
z[17] = abb[7] * z[17];
z[26] = -z[60] + -z[73];
z[26] = abb[4] * z[26];
z[1] = z[1] + z[17] + -z[25] + z[26] + (T(-3) / T(2)) * z[49] + -z[76];
z[1] = abb[31] * z[1];
z[8] = -z[4] + -z[8] + z[69];
z[17] = abb[7] + abb[8];
z[8] = z[8] * z[17];
z[8] = z[8] + z[31] + 3 * z[56];
z[17] = abb[28] + -abb[29];
z[8] = z[8] * z[17];
z[1] = z[1] + z[8] + -z[9];
z[1] = abb[31] * z[1];
z[8] = abb[16] + abb[18];
z[8] = z[8] * z[28];
z[0] = abb[42] + -z[0] + -z[43];
z[0] = abb[17] * z[0];
z[9] = abb[26] * z[30];
z[17] = abb[19] * z[64];
z[0] = -z[0] + z[8] + -z[9] + z[17];
z[8] = abb[53] * z[0];
z[9] = -abb[37] * z[54];
z[15] = z[15] + -z[70];
z[15] = -abb[52] + (T(1) / T(2)) * z[15];
z[15] = z[15] * z[51];
z[17] = z[36] * z[51];
z[26] = abb[30] * z[17];
z[15] = z[15] + z[26];
z[26] = abb[5] * (T(3) / T(2));
z[15] = z[15] * z[26];
z[1] = z[1] + z[3] + 3 * z[5] + (T(3) / T(2)) * z[8] + z[9] + z[11] + z[15] + z[21];
z[3] = -z[16] + z[18] + z[29];
z[3] = abb[4] * z[3];
z[5] = 2 * abb[42];
z[8] = abb[43] * (T(1) / T(4)) + -z[5] + -z[13] + z[14];
z[8] = abb[7] * z[8];
z[3] = z[3] + z[8] + 2 * z[12] + z[25] + z[27] + -z[40] + -z[78];
z[3] = abb[31] * z[3];
z[8] = abb[30] * z[62];
z[7] = z[7] + -z[20];
z[5] = z[5] * z[22];
z[9] = -abb[9] * z[66];
z[5] = z[5] + 2 * z[7] + z[9] + z[12] + -z[32] + -z[33] + z[65];
z[7] = 6 * abb[47];
z[4] = -z[4] + z[7] + -z[19] + -z[53];
z[4] = abb[8] * z[4];
z[2] = z[2] + -z[10] + -z[48];
z[2] = abb[7] * z[2];
z[2] = z[2] + z[4] + 3 * z[5] + -z[23];
z[2] = abb[32] * z[2];
z[4] = z[7] + -z[66] + -z[69] + -z[74];
z[4] = abb[8] * z[4];
z[5] = 2 * z[52];
z[7] = -abb[24] * z[30];
z[7] = z[5] + z[7] + -z[56];
z[4] = z[4] + 3 * z[7] + z[23] + z[24];
z[4] = abb[29] * z[4];
z[7] = -abb[4] + abb[7];
z[7] = z[7] * z[58];
z[9] = -abb[1] * z[77];
z[6] = -3 * z[6] + z[7] + z[9] + -z[12] + z[79] + z[82];
z[6] = abb[27] * z[6];
z[7] = abb[49] + z[47] + -z[57];
z[7] = abb[1] * z[7];
z[5] = -z[5] + z[7] + -z[34] + -z[42];
z[5] = 3 * z[5] + -z[80] + -z[83];
z[5] = abb[28] * z[5];
z[7] = z[17] * z[26];
z[2] = z[2] + z[3] + z[4] + z[5] + 2 * z[6] + z[7] + z[8];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = abb[38] * z[46];
z[4] = -abb[5] * z[71];
z[4] = z[4] + z[44];
z[4] = abb[39] * z[4];
z[0] = abb[40] * z[0];
z[0] = (T(1) / T(2)) * z[0] + z[3] + z[4];
z[0] = 3 * z[0] + z[2];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_275_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("15.450078917274434293101657209467872430583744502505932285414849113"),stof<T>("-53.134356214699376742634265603799826706847615954368603431025468477")}, std::complex<T>{stof<T>("-29.812321165349265736397436937642014984677930848764465516646227806"),stof<T>("21.120501999199564205866697115437052890912296680443702455469877917")}, std::complex<T>{stof<T>("6.649017022093725609138036969297300005090047660717341708608668748"),stof<T>("-13.09577886334928700723304252220086849418291639961518254186002122")}, std::complex<T>{stof<T>("-7.687120263558075704086300984559788078432254748391678327741697953"),stof<T>("-14.488192071346316483026338377651072600386143246861793296830825326")}, std::complex<T>{stof<T>("-17.615507745935394102840818345096835064136201461388195528260961886"),stof<T>("8.584324298882360639391283163367212198296924741406416600296859453")}, std::complex<T>{stof<T>("-17.615507745935394102840818345096835064136201461388195528260961886"),stof<T>("8.584324298882360639391283163367212198296924741406416600296859453")}, std::complex<T>{stof<T>("45.329731165513963189616948820715415230753058547167730857428348457"),stof<T>("33.446911998062466965682072841213907965343579306196733408363483264")}, std::complex<T>{stof<T>("-41.024978470615073700210068323774544434220028566552717566617134902"),stof<T>("25.031710826068782179829619406738751342025710693049350032508948328")}, std::complex<T>{stof<T>("5.5477963973201460244185816232478799154516817266589282797765971709"),stof<T>("0.5596011630320834407576285701310278015675444605778966866870027563")}, std::complex<T>{stof<T>("-1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("1.893635402193665897979852592125278317152816937002069288848804013")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[23].real()/kbase.W[23].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_275_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_275_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("20.169288915866978759965121300858341356942001402230393024982752214"),stof<T>("32.215972030982383726121371183691864231929787980224831705591448018")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,54> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[23].real()/k.W[23].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_275_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_275_DLogXconstant_part(base_point<T>, kend);
	value += f_4_275_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_275_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_275_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_275_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_275_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_275_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_275_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
