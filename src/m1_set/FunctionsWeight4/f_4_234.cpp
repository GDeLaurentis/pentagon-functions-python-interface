/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_234.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_234_abbreviated (const std::array<T,29>& abb) {
T z[40];
z[0] = 4 * abb[27];
z[1] = prod_pow(abb[13], 2);
z[2] = z[0] + -z[1];
z[3] = -abb[12] + 2 * abb[13];
z[3] = abb[12] * z[3];
z[4] = abb[12] + -abb[13];
z[5] = -abb[11] + 2 * z[4];
z[5] = abb[11] * z[5];
z[6] = 3 * abb[15];
z[3] = -z[2] + z[3] + z[5] + -z[6];
z[5] = abb[11] + -abb[12];
z[7] = 4 * z[5];
z[8] = abb[14] + -z[7];
z[8] = abb[14] * z[8];
z[8] = z[3] + -z[8];
z[9] = prod_pow(m1_set::bc<T>[0], 2);
z[8] = -3 * z[8] + 4 * z[9];
z[10] = 2 * abb[1];
z[8] = z[8] * z[10];
z[11] = abb[11] * (T(1) / T(2));
z[12] = 3 * abb[12];
z[13] = abb[11] + 5 * abb[13] + -z[12];
z[13] = z[11] * z[13];
z[14] = 2 * abb[15];
z[15] = abb[12] * abb[13];
z[13] = z[2] + z[13] + z[14] + (T(-5) / T(2)) * z[15];
z[16] = -abb[14] + 2 * z[5];
z[17] = 6 * abb[14];
z[18] = -z[16] * z[17];
z[13] = (T(5) / T(2)) * z[9] + 3 * z[13] + z[18];
z[13] = abb[3] * z[13];
z[18] = abb[13] + z[5];
z[18] = abb[11] * z[18];
z[19] = 2 * abb[27];
z[14] = z[14] + z[18] + z[19];
z[18] = z[14] + -z[15];
z[20] = z[5] * z[17];
z[20] = 2 * z[9] + -z[20];
z[18] = 3 * z[18] + z[20];
z[18] = abb[6] * z[18];
z[21] = abb[11] * abb[13];
z[21] = abb[27] + -z[15] + z[21];
z[22] = z[1] + -z[21];
z[23] = -abb[14] + z[5];
z[17] = z[17] * z[23];
z[17] = z[9] + z[17] + 6 * z[22];
z[17] = abb[5] * z[17];
z[22] = abb[12] * z[4];
z[14] = z[1] + -z[14] + -z[22];
z[14] = 3 * z[14] + -z[20];
z[14] = abb[0] * z[14];
z[20] = prod_pow(abb[11], 2);
z[20] = abb[15] + z[20];
z[23] = prod_pow(abb[14], 2);
z[24] = z[20] + -z[23];
z[25] = abb[4] * z[24];
z[26] = abb[12] + abb[13];
z[27] = abb[11] + z[26];
z[28] = z[11] * z[27];
z[29] = (T(1) / T(2)) * z[9];
z[30] = -z[28] + z[29];
z[31] = abb[13] * (T(1) / T(2));
z[32] = abb[12] + z[31];
z[32] = abb[12] * z[32];
z[33] = z[30] + z[32];
z[34] = abb[2] * z[33];
z[35] = abb[7] * (T(1) / T(2));
z[36] = -abb[9] + abb[8] * (T(-3) / T(2)) + z[35];
z[37] = 3 * abb[28];
z[38] = z[36] * z[37];
z[8] = z[8] + z[13] + z[14] + z[17] + -z[18] + 6 * z[25] + -3 * z[34] + z[38];
z[8] = abb[24] * z[8];
z[13] = 2 * abb[14];
z[14] = z[13] * z[16];
z[17] = 4 * abb[12];
z[25] = z[17] + -z[31];
z[25] = abb[12] * z[25];
z[34] = 5 * abb[11] + -7 * abb[12] + abb[13];
z[11] = z[11] * z[34];
z[34] = (T(3) / T(2)) * z[9];
z[2] = 6 * abb[15] + z[2] + z[11] + -z[14] + z[25] + -z[34];
z[2] = abb[3] * z[2];
z[11] = 2 * abb[11];
z[4] = -z[4] + z[11];
z[4] = abb[11] * z[4];
z[4] = z[4] + z[6] + z[19];
z[16] = abb[14] * z[16];
z[19] = -abb[12] * z[26];
z[19] = z[1] + z[4] + -z[16] + z[19] + -z[34];
z[19] = abb[0] * z[19];
z[25] = -2 * z[3] + -4 * z[16] + z[29];
z[25] = abb[1] * z[25];
z[28] = (T(1) / T(6)) * z[9] + -z[28];
z[29] = 4 * z[1] + z[28] + z[32];
z[29] = abb[2] * z[29];
z[32] = abb[14] + z[5];
z[32] = z[13] * z[32];
z[34] = z[1] + z[21];
z[32] = (T(11) / T(3)) * z[9] + -z[32] + 2 * z[34];
z[34] = abb[5] * z[32];
z[38] = 3 * abb[4];
z[24] = z[24] * z[38];
z[2] = z[2] + -z[18] + z[19] + z[24] + z[25] + -z[29] + z[34];
z[2] = abb[18] * z[2];
z[6] = -z[1] + z[6];
z[19] = 3 * abb[11];
z[12] = abb[13] + -z[12] + z[19];
z[12] = abb[11] * z[12];
z[25] = (T(5) / T(3)) * z[9];
z[0] = z[0] + 2 * z[6] + z[12] + -z[14] + -z[15] + -z[25];
z[0] = abb[3] * z[0];
z[6] = 3 * z[9];
z[7] = 5 * abb[14] + -z[7];
z[7] = abb[14] * z[7];
z[7] = -z[3] + -z[6] + z[7];
z[7] = z[7] * z[10];
z[12] = abb[0] + abb[5];
z[12] = z[12] * z[32];
z[32] = 2 * abb[12];
z[34] = abb[13] + z[32];
z[34] = abb[12] * z[34];
z[27] = abb[11] * z[27];
z[34] = -z[27] + z[34];
z[39] = -8 * z[1] + (T(-1) / T(3)) * z[9] + -z[34];
z[39] = abb[2] * z[39];
z[0] = z[0] + z[7] + z[12] + -z[18] + z[39];
z[0] = abb[23] * z[0];
z[3] = 4 * z[3] + -z[9] + 8 * z[16];
z[7] = abb[1] * z[3];
z[5] = -z[5] + z[13];
z[5] = z[5] * z[13];
z[12] = 2 * z[1];
z[13] = -z[12] + z[21];
z[5] = z[5] + (T(-7) / T(3)) * z[9] + 2 * z[13];
z[5] = abb[5] * z[5];
z[5] = z[5] + z[7] + z[18] + -z[24];
z[3] = abb[3] * z[3];
z[7] = -abb[13] + z[32];
z[7] = abb[12] * z[7];
z[4] = z[4] + z[7] + -z[12];
z[4] = 2 * z[4] + -z[6] + -z[14];
z[4] = abb[0] * z[4];
z[6] = z[12] + z[34];
z[6] = 2 * z[6] + z[25];
z[6] = abb[2] * z[6];
z[7] = abb[8] + abb[9];
z[12] = abb[28] * z[7];
z[3] = z[3] + z[4] + 2 * z[5] + z[6] + 6 * z[12];
z[4] = 2 * abb[19] + abb[21];
z[3] = z[3] * z[4];
z[4] = -z[1] + (T(4) / T(3)) * z[9] + -z[22] + -z[27];
z[4] = abb[0] * z[4];
z[5] = z[17] + z[31];
z[5] = abb[12] * z[5];
z[5] = z[1] + z[5] + z[28];
z[5] = abb[3] * z[5];
z[4] = z[4] + z[5] + z[29];
z[4] = abb[20] * z[4];
z[5] = (T(13) / T(2)) * z[9];
z[6] = -z[5] + 6 * z[23];
z[6] = abb[1] * z[6];
z[9] = z[20] + z[23];
z[5] = z[5] + -3 * z[9];
z[5] = abb[0] * z[5];
z[5] = z[5] + z[6] + -z[24];
z[5] = abb[22] * z[5];
z[1] = z[1] + (T(1) / T(2)) * z[15] + z[30];
z[1] = abb[7] * z[1];
z[6] = abb[8] * z[33];
z[1] = z[1] + z[6];
z[6] = 3 * abb[26];
z[9] = 3 * abb[25];
z[12] = z[6] + z[9];
z[1] = z[1] * z[12];
z[12] = abb[7] + z[7];
z[12] = abb[23] * z[12];
z[13] = abb[7] + abb[8];
z[14] = abb[20] * z[13];
z[15] = abb[2] + abb[3];
z[15] = abb[0] + -2 * abb[10] + (T(3) / T(2)) * z[15];
z[16] = abb[26] * z[15];
z[12] = -z[12] + (T(1) / T(2)) * z[14] + z[16];
z[14] = abb[9] + abb[8] * (T(1) / T(2)) + z[35];
z[16] = -abb[18] * z[14];
z[17] = abb[25] * z[15];
z[16] = z[12] + z[16] + z[17];
z[16] = z[16] * z[37];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[8] + z[16];
z[1] = -abb[11] + z[26];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[16] + z[1];
z[3] = abb[14] * m1_set::bc<T>[0];
z[4] = 3 * z[3];
z[5] = z[2] + -z[4];
z[8] = abb[5] * z[5];
z[16] = abb[11] * m1_set::bc<T>[0];
z[17] = -z[3] + z[16];
z[18] = z[17] * z[38];
z[20] = 2 * abb[16];
z[1] = z[1] + -z[20];
z[21] = -z[1] * z[10];
z[21] = -z[8] + -z[18] + z[21];
z[22] = m1_set::bc<T>[0] * z[26];
z[23] = 2 * z[3];
z[22] = -z[20] + z[22] + -z[23];
z[24] = abb[6] * z[22];
z[25] = 3 * z[24];
z[11] = z[11] + z[26];
z[11] = m1_set::bc<T>[0] * z[11];
z[20] = z[11] + -z[20];
z[20] = abb[0] * z[20];
z[27] = 4 * abb[16];
z[28] = abb[11] + (T(1) / T(2)) * z[26];
z[28] = m1_set::bc<T>[0] * z[28];
z[29] = z[27] + -z[28];
z[29] = abb[3] * z[29];
z[30] = 3 * abb[17];
z[14] = -z[14] * z[30];
z[31] = abb[2] * z[28];
z[14] = z[14] + -z[20] + 2 * z[21] + z[25] + z[29] + -z[31];
z[14] = abb[18] * z[14];
z[17] = abb[4] * z[17];
z[21] = -z[1] + z[3];
z[21] = z[10] * z[21];
z[29] = z[2] + z[3];
z[29] = abb[5] * z[29];
z[21] = -2 * z[17] + z[21] + z[29];
z[22] = abb[0] * z[22];
z[19] = z[19] + (T(-5) / T(2)) * z[26];
z[19] = m1_set::bc<T>[0] * z[19];
z[19] = z[19] + z[27];
z[19] = abb[3] * z[19];
z[26] = abb[17] * z[36];
z[19] = z[19] + 2 * z[21] + z[22] + z[24] + z[26] + -z[31];
z[19] = abb[24] * z[19];
z[21] = 4 * abb[1] + 2 * abb[3];
z[21] = z[1] * z[21];
z[2] = z[2] + z[4];
z[2] = abb[5] * z[2];
z[2] = z[2] + -z[18] + -z[21];
z[18] = abb[2] * z[11];
z[18] = z[18] + -z[25];
z[7] = z[7] * z[30];
z[2] = 2 * z[2] + -z[7] + -z[18] + z[20];
z[7] = -4 * abb[19] + -2 * abb[21];
z[2] = z[2] * z[7];
z[1] = -z[1] + -z[4];
z[1] = z[1] * z[10];
z[4] = abb[0] * z[5];
z[1] = z[1] + -z[4] + -z[8];
z[4] = -z[11] + z[27];
z[4] = abb[3] * z[4];
z[1] = 2 * z[1] + z[4] + -z[18];
z[1] = abb[23] * z[1];
z[4] = abb[0] * z[11];
z[5] = abb[3] * z[28];
z[4] = z[4] + z[5] + z[31];
z[4] = abb[20] * z[4];
z[5] = z[12] * z[30];
z[3] = z[3] + z[16];
z[3] = abb[0] * z[3];
z[7] = -abb[1] * z[23];
z[3] = z[3] + z[7] + z[17];
z[3] = abb[22] * z[3];
z[7] = z[13] * z[28];
z[6] = z[6] * z[7];
z[8] = abb[17] * z[15];
z[7] = z[7] + z[8];
z[7] = z[7] * z[9];
z[1] = z[1] + z[2] + 6 * z[3] + z[4] + z[5] + z[6] + z[7] + z[14] + 3 * z[19];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_234_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("3.508862352110978398310391353967766071212729710858481400189726276"),stof<T>("24.045941609756647997954947970461859237862056548997006386324344054")}, std::complex<T>{stof<T>("66.833315334292606050743335599373092070596392846213730148712656711"),stof<T>("31.424797079481630950874404688350799425144821369133679797927646393")}, std::complex<T>{stof<T>("36.119104111302572006112413342785895438987172183751275396194245654"),stof<T>("-14.633300484517895006119187001685607389125752937927857609262389682")}, std::complex<T>{stof<T>("33.416657667146303025371667799686546035298196423106865074356328356"),stof<T>("15.712398539740815475437202344175399712572410684566839898963823196")}, std::complex<T>{stof<T>("-60.258600392267208941144756463508485964514530533149917808617801578"),stof<T>("-49.83248291892144388244726093157406291554098963840532950099113073")}, std::complex<T>{stof<T>("-92.868842151458802548946778452326615332288973006042711804622320956"),stof<T>("-11.153240824646900878373125959426596288553180151480465505404396994")}, std::complex<T>{stof<T>("23.333080373009927517462697309854173858003604399184571334071746946"),stof<T>("10.074142769423980409055110616936803965106522404841483215702963479")}, std::complex<T>{stof<T>("11.325128681290779395907282033920905840399050184493898327844197224"),stof<T>("-18.443713539553886571319295849287612627184745544319581586174581398")}, std::complex<T>{stof<T>("11.325128681290779395907282033920905840399050184493898327844197224"),stof<T>("-18.443713539553886571319295849287612627184745544319581586174581398")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[22].real()/kbase.W[22].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[126].real()/kbase.W[126].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_234_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_234_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.960748092348177948200254904612633439158577914928516962611069813"),stof<T>("-22.143350086688050869494648096161648198035852901427627348054137434")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dlog_W3(k,dl), dlog_W8(k,dl), dl[3], dlog_W13(k,dl), dlog_W23(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W127(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[22].real()/k.W[22].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[126].real()/k.W[126].real()), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_234_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_234_DLogXconstant_part(base_point<T>, kend);
	value += f_4_234_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_234_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_234_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_234_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_234_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_234_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_234_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
