/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_693.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_693_abbreviated (const std::array<T,37>& abb) {
T z[146];
z[0] = 3 * abb[18];
z[1] = -abb[16] + abb[17];
z[2] = abb[14] + z[1];
z[3] = 2 * z[2];
z[4] = z[0] + -z[3];
z[4] = abb[18] * z[4];
z[5] = 3 * abb[17];
z[6] = abb[14] + -abb[16];
z[7] = -z[5] + -z[6];
z[7] = abb[14] * z[7];
z[8] = abb[20] + -abb[23];
z[9] = 2 * abb[21];
z[10] = 3 * abb[22];
z[11] = prod_pow(abb[15], 2);
z[12] = 4 * abb[17];
z[13] = abb[16] + z[12];
z[13] = abb[17] * z[13];
z[7] = -abb[19] + -z[4] + z[7] + z[8] + z[9] + -z[10] + z[11] + z[13];
z[7] = abb[4] * z[7];
z[13] = abb[16] + abb[17];
z[14] = 2 * abb[14];
z[15] = z[13] + -z[14];
z[15] = abb[14] * z[15];
z[16] = abb[17] * z[13];
z[17] = prod_pow(abb[16], 2);
z[18] = 2 * z[17];
z[19] = z[16] + -z[18];
z[20] = -abb[15] + z[3];
z[20] = abb[15] * z[20];
z[21] = -abb[18] + z[3];
z[22] = abb[18] * z[21];
z[23] = 3 * abb[20];
z[24] = -abb[21] + abb[23];
z[25] = abb[19] + -abb[22] + z[15] + -z[19] + z[20] + -z[22] + -z[23] + z[24];
z[25] = abb[0] * z[25];
z[26] = 4 * abb[22];
z[27] = 2 * abb[20];
z[28] = z[26] + z[27];
z[29] = -abb[14] + abb[18];
z[30] = -z[1] + z[29];
z[31] = 2 * abb[18];
z[32] = z[30] * z[31];
z[33] = abb[17] * z[1];
z[34] = z[32] + z[33];
z[35] = 2 * abb[19];
z[36] = abb[21] + z[35];
z[37] = 2 * z[36];
z[38] = z[34] + z[37];
z[39] = -abb[17] + z[6];
z[40] = abb[14] * z[39];
z[41] = 3 * abb[23];
z[42] = z[11] + z[28] + z[38] + z[40] + -z[41];
z[42] = abb[9] * z[42];
z[43] = abb[13] * abb[24];
z[42] = z[42] + -z[43];
z[44] = 5 * abb[18];
z[45] = 4 * z[2];
z[46] = z[44] + -z[45];
z[47] = abb[18] * z[46];
z[48] = abb[14] * abb[17];
z[49] = 4 * z[48];
z[50] = 5 * abb[20];
z[51] = 4 * abb[16];
z[52] = -abb[17] + -z[51];
z[52] = abb[17] * z[52];
z[52] = 5 * abb[23] + -z[26] + z[47] + z[49] + -z[50] + z[52];
z[52] = abb[7] * z[52];
z[53] = prod_pow(abb[17], 2);
z[54] = 2 * z[11];
z[55] = -abb[19] + abb[23];
z[56] = -abb[21] + -2 * z[53] + z[54] + -z[55];
z[56] = abb[8] * z[56];
z[57] = prod_pow(abb[18], 2);
z[58] = prod_pow(abb[14], 2);
z[59] = z[57] + -z[58];
z[60] = z[24] + -z[35] + z[59];
z[61] = abb[6] * z[60];
z[62] = 2 * abb[22];
z[63] = z[27] + z[62];
z[64] = -abb[23] + z[63];
z[65] = abb[10] * z[64];
z[66] = 2 * z[65];
z[67] = abb[15] * z[2];
z[68] = abb[14] * z[2];
z[69] = z[67] + -z[68];
z[69] = 2 * z[69];
z[70] = abb[22] + -abb[23] + z[69];
z[71] = -abb[5] * z[70];
z[72] = abb[12] * abb[24];
z[73] = abb[11] * abb[24];
z[7] = z[7] + z[25] + z[42] + z[52] + -z[56] + z[61] + z[66] + z[71] + -2 * z[72] + z[73];
z[7] = abb[29] * z[7];
z[25] = z[1] + z[14];
z[52] = 5 * abb[15];
z[61] = 2 * z[25] + -z[52];
z[61] = abb[15] * z[61];
z[71] = 10 * abb[20];
z[74] = z[61] + -z[71];
z[75] = 7 * abb[19];
z[76] = 4 * abb[21];
z[77] = z[75] + z[76];
z[78] = 6 * abb[14];
z[79] = -abb[17] + z[78];
z[80] = 5 * abb[16];
z[81] = z[79] + -z[80];
z[81] = abb[14] * z[81];
z[82] = 16 * abb[22];
z[83] = 7 * abb[23];
z[84] = 8 * z[2];
z[85] = 7 * abb[18] + -z[84];
z[85] = abb[18] * z[85];
z[77] = 5 * z[33] + -z[74] + 2 * z[77] + z[81] + z[82] + -z[83] + z[85];
z[77] = abb[9] * z[77];
z[81] = 2 * abb[16];
z[85] = z[5] + z[81];
z[86] = abb[17] * z[85];
z[87] = 4 * abb[19];
z[88] = 3 * abb[21];
z[89] = 4 * abb[23];
z[61] = -z[49] + z[61] + z[86] + z[87] + z[88] + -z[89];
z[61] = abb[8] * z[61];
z[90] = abb[14] + abb[17];
z[80] = z[80] + z[90];
z[80] = abb[14] * z[80];
z[91] = 5 * abb[19] + abb[23];
z[92] = 2 * z[57];
z[93] = 3 * abb[16];
z[94] = -abb[17] + -z[93];
z[94] = abb[17] * z[94];
z[74] = 4 * z[17] + z[74] + z[80] + -z[91] + -z[92] + z[94];
z[74] = abb[0] * z[74];
z[80] = abb[18] * z[30];
z[36] = z[36] + z[80];
z[94] = 8 * abb[23];
z[95] = -8 * abb[17] + z[93];
z[95] = abb[17] * z[95];
z[36] = -8 * z[36] + -5 * z[40] + -z[71] + -z[82] + z[94] + z[95];
z[36] = abb[3] * z[36];
z[40] = -abb[17] + z[81];
z[82] = abb[17] * z[40];
z[95] = -z[17] + z[82];
z[96] = 2 * z[1];
z[97] = abb[14] + z[96];
z[98] = abb[14] * z[97];
z[99] = -z[95] + z[98];
z[100] = 3 * abb[19];
z[80] = z[26] + 4 * z[80] + z[99] + z[100];
z[101] = 4 * abb[2];
z[102] = z[80] * z[101];
z[103] = 2 * abb[23];
z[104] = z[9] + -z[103];
z[105] = 3 * z[58];
z[106] = 3 * z[57];
z[107] = -z[100] + -z[104] + -z[105] + z[106];
z[107] = abb[6] * z[107];
z[60] = abb[4] * z[60];
z[108] = z[43] + z[72];
z[109] = prod_pow(m1_set::bc<T>[0], 2);
z[109] = (T(13) / T(3)) * z[109];
z[110] = -abb[0] + abb[2];
z[110] = z[109] * z[110];
z[111] = 2 * z[24];
z[112] = abb[1] * z[111];
z[36] = z[36] + z[60] + z[61] + z[74] + z[77] + -z[102] + z[107] + -3 * z[108] + z[110] + z[112];
z[36] = abb[31] * z[36];
z[23] = z[23] + -z[41];
z[60] = z[5] + -z[81];
z[61] = -abb[14] + -z[60];
z[61] = z[14] * z[61];
z[47] = -abb[19] + 6 * abb[21] + 3 * z[11] + -z[18] + z[23] + -z[26] + -z[47] + 4 * z[53] + z[61];
z[47] = abb[4] * z[47];
z[53] = 5 * abb[17];
z[61] = z[51] + -z[53];
z[74] = 2 * abb[17];
z[61] = z[61] * z[74];
z[77] = 2 * abb[15];
z[107] = -abb[14] + z[1];
z[110] = 4 * z[107];
z[112] = abb[15] + z[110];
z[112] = z[77] * z[112];
z[113] = 9 * abb[19];
z[114] = 9 * abb[23];
z[61] = -23 * abb[21] + 8 * z[48] + z[61] + z[112] + -z[113] + z[114];
z[61] = abb[8] * z[61];
z[112] = z[1] * z[74];
z[53] = -abb[14] + z[53] + -z[81];
z[115] = abb[14] * z[53];
z[116] = abb[19] + abb[21];
z[115] = z[112] + z[115] + -z[116];
z[117] = -z[1] + z[14];
z[118] = 7 * abb[15];
z[119] = 4 * z[117] + -z[118];
z[119] = abb[15] * z[119];
z[120] = abb[20] + abb[22];
z[121] = -abb[18] + -z[45];
z[121] = abb[18] * z[121];
z[115] = 15 * abb[23] + 2 * z[115] + z[119] + -14 * z[120] + z[121];
z[115] = abb[9] * z[115];
z[119] = abb[17] * z[60];
z[120] = abb[17] * z[14];
z[121] = abb[18] + z[3];
z[121] = abb[18] * z[121];
z[121] = -z[119] + -z[120] + z[121];
z[122] = 7 * abb[20];
z[123] = 5 * abb[22];
z[121] = -z[83] + 2 * z[121] + z[122] + z[123];
z[121] = abb[7] * z[121];
z[45] = -z[0] + z[45];
z[45] = z[31] * z[45];
z[124] = 4 * z[1];
z[125] = abb[14] + z[124];
z[126] = -z[14] * z[125];
z[127] = -abb[19] + abb[21];
z[126] = z[45] + -z[83] + z[126] + 7 * z[127];
z[126] = abb[6] * z[126];
z[128] = abb[15] + z[1];
z[129] = abb[15] * z[128];
z[129] = -z[68] + z[129];
z[130] = 7 * abb[22];
z[129] = z[50] + -z[83] + 4 * z[129] + z[130];
z[129] = abb[5] * z[129];
z[47] = z[47] + z[61] + -z[66] + z[115] + z[121] + z[126] + z[129];
z[47] = abb[34] * z[47];
z[61] = -abb[16] + z[5];
z[61] = abb[17] * z[61];
z[57] = -z[57] + z[61];
z[61] = abb[15] + z[2];
z[61] = abb[15] * z[61];
z[66] = -z[9] + z[17] + -z[57] + -z[61] + z[98];
z[115] = 4 * abb[20];
z[66] = -abb[22] + z[41] + 2 * z[66] + -z[115];
z[66] = abb[4] * z[66];
z[121] = -z[2] + z[31];
z[126] = abb[18] * z[121];
z[129] = 3 * abb[14];
z[131] = -abb[17] * z[129];
z[37] = z[37] + z[61] + z[126] + z[131];
z[114] = z[114] + -z[115];
z[126] = 8 * abb[22];
z[37] = 2 * z[37] + -z[114] + z[126];
z[37] = abb[9] * z[37];
z[131] = -abb[16] + z[74];
z[132] = abb[17] * z[131];
z[133] = abb[18] + z[2];
z[134] = abb[18] * z[133];
z[135] = z[48] + z[132] + -z[134];
z[136] = -z[8] + z[135];
z[136] = abb[22] + 2 * z[136];
z[136] = abb[7] * z[136];
z[137] = abb[14] * z[25];
z[61] = z[41] + -z[61] + z[137];
z[61] = -6 * abb[22] + -z[50] + 2 * z[61];
z[61] = abb[5] * z[61];
z[138] = -z[43] + z[72];
z[138] = 2 * z[138];
z[139] = abb[18] * z[2];
z[139] = -z[33] + z[139];
z[48] = -z[48] + z[139];
z[48] = 2 * z[48];
z[8] = z[8] + z[48];
z[140] = -abb[0] * z[8];
z[141] = 2 * z[73];
z[37] = z[37] + z[61] + 4 * z[65] + z[66] + z[136] + z[138] + z[140] + z[141];
z[37] = abb[32] * z[37];
z[61] = abb[19] + z[9];
z[61] = 2 * z[61];
z[66] = -abb[14] + -z[5];
z[66] = abb[14] * z[66];
z[136] = abb[15] + -z[107];
z[136] = abb[15] * z[136];
z[66] = z[61] + z[66] + z[134] + z[136];
z[66] = 2 * z[66] + -z[83] + z[115];
z[66] = abb[9] * z[66];
z[83] = -z[23] + z[135];
z[83] = 2 * z[83] + -z[123];
z[83] = abb[7] * z[83];
z[134] = 6 * z[65];
z[70] = -abb[4] * z[70];
z[135] = z[1] + z[129];
z[136] = -abb[14] + abb[15];
z[135] = -z[135] * z[136];
z[135] = abb[23] + z[135];
z[135] = abb[20] + -z[62] + 2 * z[135];
z[135] = abb[5] * z[135];
z[66] = z[66] + z[70] + z[83] + z[134] + z[135] + -z[138];
z[66] = abb[30] * z[66];
z[70] = z[10] + z[23];
z[48] = z[48] + z[70];
z[48] = abb[7] * z[48];
z[69] = z[69] + z[70];
z[69] = abb[5] * z[69];
z[69] = z[48] + z[69];
z[83] = z[6] + z[74];
z[83] = abb[14] * z[83];
z[83] = -z[67] + z[83] + -z[139];
z[83] = z[41] + 2 * z[83];
z[83] = abb[9] * z[83];
z[135] = abb[14] * z[6];
z[67] = z[67] + -z[135] + -z[139];
z[67] = 2 * z[67];
z[135] = z[64] + z[67];
z[135] = abb[4] * z[135];
z[67] = z[64] + -z[67];
z[67] = abb[0] * z[67];
z[138] = 2 * abb[3];
z[64] = z[64] * z[138];
z[64] = z[64] + z[67] + 2 * z[69] + z[83] + -z[134] + z[135];
z[64] = abb[36] * z[64];
z[67] = -z[17] + z[100];
z[67] = 2 * z[67];
z[69] = z[40] * z[74];
z[83] = abb[17] + z[81];
z[134] = -2 * z[83] + z[129];
z[134] = abb[14] * z[134];
z[135] = abb[15] * z[136];
z[106] = 12 * abb[20] + -abb[21] + -abb[23] + z[10] + z[67] + z[69] + z[106] + z[134] + 6 * z[135];
z[106] = abb[34] * z[106];
z[134] = -z[35] + -z[95];
z[135] = z[96] + z[129];
z[139] = 3 * abb[15];
z[135] = 2 * z[135] + -z[139];
z[135] = abb[15] * z[135];
z[133] = -z[31] * z[133];
z[140] = 7 * abb[14];
z[142] = 6 * abb[16] + -z[140];
z[142] = abb[14] * z[142];
z[133] = -9 * abb[20] + abb[23] + -z[26] + z[133] + 2 * z[134] + z[135] + z[142];
z[133] = abb[30] * z[133];
z[106] = z[106] + z[133];
z[106] = abb[0] * z[106];
z[133] = z[27] + z[68];
z[20] = -z[9] + z[20] + -z[33] + -z[133];
z[20] = abb[9] * z[20];
z[134] = -abb[15] + z[14];
z[135] = abb[15] * z[134];
z[142] = z[33] + -z[133] + z[135];
z[143] = abb[0] + -abb[3];
z[142] = z[142] * z[143];
z[143] = abb[21] + z[33];
z[144] = -abb[15] * z[1];
z[144] = z[143] + z[144];
z[145] = 2 * abb[8];
z[144] = z[144] * z[145];
z[20] = z[20] + -z[108] + -z[141] + z[142] + z[144];
z[20] = abb[35] * z[20];
z[7] = z[7] + z[20] + z[36] + z[37] + z[47] + z[64] + z[66] + z[106];
z[20] = -z[18] + z[69];
z[36] = -z[96] + z[129];
z[36] = 2 * z[36] + -z[118];
z[36] = abb[15] * z[36];
z[36] = 7 * abb[21] + -z[20] + z[36] + -z[41] + z[58] + -z[122];
z[36] = abb[34] * z[36];
z[37] = -z[76] + z[95];
z[47] = -abb[14] + z[96];
z[64] = 2 * z[47];
z[66] = z[64] + z[139];
z[66] = abb[15] * z[66];
z[37] = -z[27] + 2 * z[37] + z[58] + z[66] + z[89];
z[37] = abb[30] * z[37];
z[66] = -abb[20] + z[24] + z[54];
z[66] = abb[29] * z[66];
z[36] = z[36] + z[37] + z[66];
z[9] = z[9] + -z[95];
z[37] = -z[124] + -z[129];
z[37] = abb[14] * z[37];
z[66] = 9 * abb[14] + -z[96];
z[66] = -19 * abb[15] + 2 * z[66];
z[66] = abb[15] * z[66];
z[9] = 4 * z[9] + z[37] + z[66] + -z[71] + -z[89];
z[9] = abb[26] * z[9];
z[37] = z[88] + -z[95];
z[37] = 2 * z[37];
z[64] = -abb[15] + z[64];
z[64] = abb[15] * z[64];
z[64] = z[37] + -z[58] + -z[64] + -z[103];
z[66] = 2 * abb[28];
z[71] = z[64] * z[66];
z[95] = 5 * z[1] + -z[134];
z[95] = abb[15] * z[95];
z[37] = abb[23] + z[27] + -z[37] + -z[68] + z[95];
z[95] = 4 * abb[33];
z[37] = z[37] * z[95];
z[95] = 13 * abb[15] + -z[78];
z[95] = abb[15] * z[95];
z[95] = z[95] + z[115];
z[105] = z[95] + z[104] + -z[105];
z[105] = abb[25] * z[105];
z[106] = abb[26] + -abb[29];
z[118] = abb[25] + abb[28];
z[122] = 2 * abb[33];
z[134] = -abb[30] + z[106] + -z[118] + z[122];
z[134] = z[109] * z[134];
z[142] = abb[15] + -z[25];
z[142] = abb[15] * z[142];
z[133] = abb[21] + z[133] + z[142];
z[133] = abb[35] * z[133];
z[9] = z[9] + 2 * z[36] + z[37] + z[71] + z[105] + 4 * z[133] + z[134];
z[9] = abb[1] * z[9];
z[25] = -z[25] + z[77];
z[25] = z[25] * z[77];
z[36] = -abb[21] + abb[22];
z[37] = 8 * abb[20];
z[71] = abb[17] * z[81];
z[93] = abb[17] + -z[93];
z[93] = -abb[14] + 2 * z[93];
z[93] = abb[14] * z[93];
z[4] = z[4] + z[25] + z[36] + z[37] + z[67] + z[71] + z[93] + z[103];
z[25] = 2 * abb[0];
z[4] = z[4] * z[25];
z[67] = abb[14] * z[1];
z[93] = abb[19] + z[67];
z[105] = -z[93] + -z[143];
z[133] = abb[15] + 2 * z[107];
z[133] = abb[15] * z[133];
z[105] = z[22] + z[63] + -z[103] + 2 * z[105] + z[133];
z[105] = abb[9] * z[105];
z[11] = -abb[20] + -z[11] + z[18] + z[36] + -z[57] + z[93];
z[11] = abb[4] * z[11];
z[8] = -abb[7] * z[8];
z[36] = -abb[19] + -2 * z[58] + z[92];
z[57] = -z[24] + -z[36];
z[57] = abb[6] * z[57];
z[8] = z[8] + z[11] + z[57] + 3 * z[72] + z[105];
z[11] = z[77] * z[107];
z[11] = z[11] + z[55] + -z[76] + -z[112] + z[120];
z[57] = -z[11] * z[145];
z[72] = -z[62] + z[103];
z[58] = 7 * z[58] + z[72] + -z[95];
z[58] = abb[5] * z[58];
z[4] = z[4] + 2 * z[8] + z[57] + z[58] + 6 * z[73];
z[4] = abb[25] * z[4];
z[8] = z[2] * z[31];
z[57] = abb[15] + z[96];
z[58] = abb[15] * z[57];
z[76] = 2 * z[131];
z[95] = -abb[14] + -z[76];
z[95] = abb[14] * z[95];
z[8] = z[8] + z[58] + z[63] + -z[89] + z[95] + -z[112];
z[8] = abb[9] * z[8];
z[63] = abb[20] + z[34];
z[93] = z[10] + z[17] + z[63] + z[93];
z[95] = -abb[4] * z[93];
z[58] = -z[58] + -z[70] + z[98];
z[58] = abb[5] * z[58];
z[36] = z[24] + -z[36];
z[36] = abb[6] * z[36];
z[8] = z[8] + z[36] + -z[48] + -z[56] + z[58] + z[65] + -z[73] + z[95];
z[36] = abb[21] + z[17];
z[48] = abb[16] * abb[17];
z[48] = -z[36] + z[48];
z[56] = abb[14] * z[47];
z[27] = z[27] + 2 * z[48] + z[56] + z[72] + -z[135];
z[27] = abb[0] * z[27];
z[8] = 2 * z[8] + z[27];
z[8] = z[8] * z[66];
z[27] = 5 * abb[21];
z[48] = -z[27] + -z[112] + -z[113];
z[56] = 9 * abb[18] + -z[84];
z[56] = abb[18] * z[56];
z[58] = 12 * abb[22];
z[65] = z[56] + z[58];
z[70] = -abb[14] + 2 * z[85];
z[70] = abb[14] * z[70];
z[84] = -abb[15] + -z[129];
z[84] = z[77] * z[84];
z[48] = 2 * z[48] + -z[65] + z[70] + z[84] + z[114];
z[48] = abb[9] * z[48];
z[70] = z[18] + z[100];
z[84] = z[97] * z[129];
z[54] = abb[23] + -z[54] + z[65] + 2 * z[70] + -z[71] + z[84] + -z[88];
z[54] = abb[4] * z[54];
z[11] = -abb[0] * z[11];
z[65] = -abb[14] + 8 * z[1];
z[65] = abb[14] * z[65];
z[56] = z[56] + z[65] + z[75] + -z[104];
z[56] = abb[6] * z[56];
z[65] = 9 * abb[15];
z[70] = -4 * z[47] + z[65];
z[70] = abb[15] * z[70];
z[71] = -8 * abb[16] + -abb[17];
z[71] = abb[17] * z[71];
z[49] = 8 * abb[19] + 15 * abb[21] + -z[49] + z[70] + z[71] + -z[94];
z[49] = abb[8] * z[49];
z[70] = z[73] + z[108];
z[11] = z[11] + z[48] + z[49] + z[54] + z[56] + 4 * z[70];
z[11] = z[11] * z[122];
z[48] = -abb[15] + z[96];
z[48] = abb[15] * z[48];
z[49] = abb[14] * z[13];
z[10] = -z[10] + z[41] + z[48] + z[49] + -z[50] + -z[61] + -z[132];
z[10] = abb[30] * z[10];
z[41] = z[89] + -z[115];
z[48] = -z[32] + z[48];
z[49] = z[14] * z[39];
z[26] = z[26] + -z[41] + -z[48] + z[49] + 6 * z[116] + z[119];
z[26] = abb[34] * z[26];
z[49] = -abb[29] * z[93];
z[15] = z[15] + -z[23] + -z[38] + -z[123];
z[15] = abb[32] * z[15];
z[10] = z[10] + z[15] + z[26] + z[49];
z[5] = z[5] + z[51];
z[5] = abb[17] * z[5];
z[15] = -z[14] * z[53];
z[23] = -z[52] + 2 * z[125];
z[23] = abb[15] * z[23];
z[5] = z[5] + z[15] + z[23] + -z[41] + z[58] + -4 * z[127];
z[5] = abb[33] * z[5];
z[15] = -abb[14] * z[117];
z[15] = z[15] + z[16] + -2 * z[36] + z[72];
z[15] = abb[25] * z[15];
z[16] = abb[26] * z[64];
z[23] = z[1] * z[14];
z[23] = z[23] + -z[48] + -z[82] + 2 * z[116];
z[23] = z[23] * z[66];
z[5] = z[5] + 2 * z[10] + z[15] + z[16] + z[23];
z[5] = z[5] * z[138];
z[10] = z[22] + -z[33] + -z[35] + -z[62] + -z[68];
z[10] = abb[9] * z[10];
z[15] = abb[19] + -z[59];
z[15] = abb[6] * z[15];
z[10] = -z[10] + -z[15] + -z[43] + z[73];
z[15] = z[19] + -z[32] + -z[62] + -z[100] + -z[137];
z[15] = abb[0] * z[15];
z[3] = z[3] + -z[44];
z[3] = abb[18] * z[3];
z[3] = z[3] + -z[62] + z[86] + -z[120];
z[3] = abb[7] * z[3];
z[16] = z[80] * z[138];
z[19] = -abb[16] + -z[74];
z[19] = abb[17] * z[19];
z[22] = abb[16] + z[90];
z[22] = abb[14] * z[22];
z[18] = -z[18] + z[19] + z[22];
z[18] = abb[4] * z[18];
z[3] = z[3] + -3 * z[10] + z[15] + z[16] + z[18] + z[102];
z[10] = abb[0] + abb[4];
z[15] = 2 * abb[2];
z[16] = -abb[3] + z[10] + -z[15];
z[16] = z[16] * z[109];
z[3] = 2 * z[3] + z[16];
z[3] = abb[27] * z[3];
z[16] = -abb[19] + -z[17];
z[16] = 2 * z[16] + z[24] + -z[28] + -z[34] + -z[67];
z[16] = abb[4] * z[16];
z[17] = abb[6] * z[111];
z[16] = z[16] + z[17] + z[42];
z[17] = -abb[15] + z[47];
z[17] = z[17] * z[77];
z[17] = z[17] + -z[27] + -z[35] + z[69] + z[103] + z[120];
z[17] = z[17] * z[145];
z[18] = z[124] + -z[140];
z[18] = abb[14] * z[18];
z[19] = z[65] + -2 * z[97];
z[19] = abb[15] * z[19];
z[18] = z[18] + z[19] + -z[37] + z[94] + -z[126];
z[18] = abb[5] * z[18];
z[13] = 5 * abb[14] + z[13];
z[13] = abb[14] * z[13];
z[19] = -abb[15] + -z[14];
z[19] = abb[15] * z[19];
z[13] = -abb[23] + z[13] + z[19] + -z[63] + -z[87];
z[13] = z[13] * z[25];
z[13] = z[13] + 2 * z[16] + z[17] + z[18] + -z[141];
z[13] = abb[26] * z[13];
z[16] = -abb[29] + abb[32] + -z[122];
z[16] = abb[4] * z[16];
z[17] = abb[0] * abb[30];
z[18] = abb[25] * z[25];
z[19] = abb[4] + -z[25];
z[19] = abb[26] * z[19];
z[10] = abb[28] * z[10];
z[22] = 2 * abb[29] + -abb[32] + -z[118];
z[22] = abb[2] * z[22];
z[23] = abb[25] + -z[106];
z[23] = abb[3] * z[23];
z[10] = z[10] + z[16] + z[17] + z[18] + z[19] + z[22] + z[23];
z[10] = z[10] * z[109];
z[16] = z[14] * z[97];
z[16] = z[16] + -z[20];
z[17] = -z[31] * z[46];
z[17] = -z[16] + z[17] + -z[91] + -z[130];
z[17] = abb[29] * z[17];
z[18] = z[21] * z[31];
z[18] = abb[23] + z[18] + -z[87] + -z[99] + -z[123];
z[18] = abb[32] * z[18];
z[19] = z[66] * z[80];
z[16] = 9 * abb[22] + -abb[23] + z[16] + -z[45] + z[75];
z[16] = abb[34] * z[16];
z[20] = -abb[22] + z[55] + z[92];
z[20] = abb[25] * z[20];
z[16] = z[16] + z[17] + 2 * z[18] + z[19] + z[20];
z[16] = z[15] * z[16];
z[3] = z[3] + z[4] + z[5] + 2 * z[7] + z[8] + z[9] + z[10] + z[11] + z[13] + z[16];
z[3] = 8 * z[3];
z[4] = -z[12] + z[31] + -z[136];
z[4] = abb[4] * z[4];
z[5] = abb[15] + abb[18];
z[7] = z[5] + -z[90];
z[8] = abb[9] * z[7];
z[9] = -z[81] + z[90];
z[9] = abb[0] * z[9];
z[10] = abb[15] + -abb[17];
z[11] = abb[8] * z[10];
z[12] = 2 * z[11];
z[13] = abb[6] * z[29];
z[16] = -abb[17] + abb[18];
z[17] = abb[7] * z[16];
z[18] = abb[5] * z[136];
z[4] = z[4] + -z[8] + z[9] + z[12] + -z[13] + -3 * z[17] + z[18];
z[4] = abb[29] * z[4];
z[9] = -abb[28] + -abb[34];
z[7] = z[7] * z[9];
z[9] = z[10] * z[122];
z[19] = abb[30] * z[136];
z[6] = -abb[18] + z[6];
z[20] = -abb[29] * z[6];
z[21] = abb[32] * z[16];
z[22] = -abb[25] * z[107];
z[1] = -abb[15] + z[1];
z[23] = abb[26] * z[1];
z[7] = z[7] + z[9] + z[19] + z[20] + z[21] + z[22] + z[23];
z[7] = z[7] * z[138];
z[5] = abb[16] + -z[5] + z[129];
z[5] = z[5] * z[25];
z[9] = 2 * z[13];
z[19] = -abb[18] + z[57];
z[19] = abb[4] * z[19];
z[5] = z[5] + z[9] + -z[11] + z[17] + 5 * z[18] + z[19];
z[5] = abb[25] * z[5];
z[19] = -abb[14] + z[139];
z[20] = z[0] + -z[19] + -z[60];
z[20] = abb[4] * z[20];
z[9] = z[9] + z[12];
z[12] = 3 * z[8];
z[21] = 4 * z[18];
z[20] = z[9] + z[12] + -4 * z[17] + z[20] + -z[21];
z[20] = abb[34] * z[20];
z[22] = z[16] + z[77] + -z[78];
z[22] = abb[0] * z[22];
z[23] = z[29] + z[81];
z[23] = abb[4] * z[23];
z[24] = 3 * z[11];
z[21] = -z[8] + -z[21] + z[22] + z[23] + z[24];
z[21] = abb[26] * z[21];
z[22] = -abb[34] * z[30];
z[23] = z[0] + -z[2];
z[23] = abb[29] * z[23];
z[25] = -abb[28] * z[121];
z[2] = -abb[32] * z[2];
z[26] = -abb[18] * abb[25];
z[2] = z[2] + z[22] + z[23] + z[25] + z[26];
z[2] = z[2] * z[15];
z[15] = z[77] + -z[117];
z[15] = abb[34] * z[15];
z[22] = -abb[30] * z[128];
z[23] = -abb[15] * abb[29];
z[15] = z[15] + z[22] + z[23];
z[22] = z[52] + -z[129];
z[23] = -abb[25] * z[22];
z[22] = z[22] + z[96];
z[22] = abb[26] * z[22];
z[1] = z[1] * z[66];
z[25] = -abb[33] * z[110];
z[1] = z[1] + 2 * z[15] + z[22] + z[23] + z[25];
z[1] = abb[1] * z[1];
z[15] = z[31] + -z[51] + -z[79] + z[139];
z[15] = abb[0] * z[15];
z[22] = z[101] * z[121];
z[22] = 3 * z[13] + -z[22];
z[23] = 4 * abb[3] + -abb[4];
z[23] = z[23] * z[29];
z[12] = -z[12] + z[15] + -z[22] + z[23] + z[24];
z[12] = abb[31] * z[12];
z[15] = -z[8] + z[17] + z[18];
z[23] = 3 * z[15];
z[19] = z[19] + -z[31] + z[76];
z[19] = abb[4] * z[19];
z[24] = abb[0] * z[16];
z[19] = z[19] + z[23] + z[24];
z[19] = abb[32] * z[19];
z[6] = -abb[4] * z[6];
z[24] = -abb[0] * z[107];
z[6] = z[6] + z[9] + z[15] + z[24];
z[6] = z[6] * z[66];
z[9] = abb[14] + -2 * z[40] + -z[44] + z[77];
z[9] = abb[4] * z[9];
z[13] = -z[8] + z[13];
z[10] = -abb[0] * z[10];
z[9] = z[9] + z[10] + -11 * z[11] + -5 * z[13];
z[9] = abb[33] * z[9];
z[10] = -z[16] + z[136];
z[11] = abb[0] + -abb[4];
z[10] = z[10] * z[11];
z[11] = 2 * z[17];
z[8] = z[8] + z[10] + -z[11] + -2 * z[18];
z[8] = abb[36] * z[8];
z[10] = -abb[4] * z[39];
z[10] = z[10] + z[11];
z[11] = -z[121] * z[138];
z[13] = abb[18] + -z[47];
z[13] = abb[0] * z[13];
z[10] = 2 * z[10] + z[11] + z[13] + z[22];
z[10] = abb[27] * z[10];
z[11] = abb[4] * z[136];
z[11] = z[11] + z[23];
z[11] = abb[30] * z[11];
z[0] = -z[0] + z[83];
z[13] = z[0] + z[129] + -z[139];
z[13] = abb[34] * z[13];
z[0] = -z[0] + z[14];
z[0] = abb[30] * z[0];
z[0] = z[0] + z[13];
z[0] = abb[0] * z[0];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[19] + z[20] + z[21];
z[0] = 32 * m1_set::bc<T>[0] * z[0];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_693_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-107.91630891102871071499118276154306850616432867410450995206356577"),stof<T>("330.74517493859993424443586554723080017690252018527129567470052443")}, std::complex<T>{stof<T>("907.10363434393288856292005730475115247513227761423826063094174571"),stof<T>("24.58730796900699445037737278845759588821383553432865143031275415")}, std::complex<T>{stof<T>("-257.60046156022420881109479999113180278235428192667544867495017673"),stof<T>("481.76255222791782450704101394124954127805550144519613402683372254")}, std::complex<T>{stof<T>("95.747465315977911710714812327535275123052259033069030113439777683"),stof<T>("99.187652735500434174129457065096199896893491083595440186983587486")}, std::complex<T>{stof<T>("-338.31263886708605231335387565107359942266653299443939089399937548"),stof<T>("-737.90738240002431902772479521184173744627836608120064094486341364")}, std::complex<T>{stof<T>("-902.49699628093373973232359167879774686805340959356082404920778086"),stof<T>("-251.90397649114102958910535053927697047160777177981904707306317614")}, std::complex<T>{stof<T>("259.6361249386068980527323378738872231482381860743430948858189592"),stof<T>("-788.57194318938406834011991169114742932205905400566052631458019943")}, std::complex<T>{stof<T>("385.07666988966469422728857193314833266504047928173788520213110122"),stof<T>("359.57333658857239362639166906700362176173144857578540694997979355")}, std::complex<T>{stof<T>("-237.76267683442113811496860853081744683581827530827482916597022606"),stof<T>("596.18950386583134213555496183104961645139937434069386850059782583")}, std::complex<T>{stof<T>("87.52013424853760090641184511983804678437839807018771896820320507"),stof<T>("114.098456878803033206526589174241796085491262570976022880975275")}, stof<T>("-107.52675968961600501276538657346740664089487498594988099648885006"), std::complex<T>{stof<T>("113.22214255856029935503510349208137796102464275600182056181558514"),stof<T>("-124.33217670721664028179898417565160870523365792938570528592659134")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[56].real()/kbase.W[56].real()), rlog(k.W[57].real()/kbase.W[57].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_693_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_693_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("48.8040659152727748516407243560573660122945502758650022476190139"),stof<T>("-134.52454099919375970410263956532710620307066452768992994106150346")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,37> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), dlog_W58(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[56].real()/k.W[56].real()), rlog(kend.W[57].real()/k.W[57].real())};

                    
            return f_4_693_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_693_DLogXconstant_part(base_point<T>, kend);
	value += f_4_693_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_693_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_693_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_693_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_693_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_693_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_693_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
