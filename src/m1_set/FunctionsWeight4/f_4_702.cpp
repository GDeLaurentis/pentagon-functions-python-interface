/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_702.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_702_abbreviated (const std::array<T,71>& abb) {
T z[110];
z[0] = abb[21] * abb[65];
z[1] = abb[18] * abb[65];
z[2] = -abb[60] + abb[63];
z[3] = abb[62] + z[2];
z[4] = abb[8] * z[3];
z[4] = z[0] + z[1] + -z[4];
z[5] = 2 * abb[63];
z[6] = abb[61] + abb[62];
z[7] = z[5] + z[6];
z[8] = 2 * abb[60];
z[9] = z[7] + z[8];
z[10] = -abb[64] + z[9];
z[11] = abb[5] * z[10];
z[12] = abb[57] + abb[59];
z[13] = abb[53] + abb[54] + -abb[55];
z[14] = -2 * abb[58] + z[12] + z[13];
z[15] = abb[23] * z[14];
z[11] = z[11] + -z[15];
z[16] = abb[26] * z[14];
z[17] = abb[60] + abb[63];
z[18] = -abb[62] + z[17];
z[19] = abb[14] * z[18];
z[20] = z[16] + z[19];
z[21] = abb[24] * z[14];
z[22] = z[20] + z[21];
z[23] = 2 * abb[61];
z[24] = abb[62] + z[17];
z[25] = z[23] + z[24];
z[26] = abb[6] * z[25];
z[27] = -z[22] + z[26];
z[28] = 2 * abb[64];
z[29] = -z[24] + z[28];
z[30] = abb[17] * z[29];
z[31] = 2 * abb[25];
z[32] = -abb[27] + z[31];
z[32] = z[14] * z[32];
z[33] = -abb[64] + z[17];
z[34] = z[6] + z[33];
z[34] = abb[12] * z[34];
z[35] = 4 * z[34];
z[36] = abb[63] + z[6];
z[36] = abb[13] * z[36];
z[37] = 4 * z[36];
z[38] = abb[1] * z[6];
z[39] = 4 * z[38];
z[40] = -abb[61] + z[33];
z[40] = abb[0] * z[40];
z[41] = abb[19] * abb[65];
z[4] = -2 * z[4] + -z[11] + -z[27] + z[30] + z[32] + z[35] + -z[37] + z[39] + z[40] + -z[41];
z[4] = abb[31] * z[4];
z[32] = 2 * abb[56] + -z[12] + z[13];
z[42] = abb[27] * z[32];
z[43] = abb[17] * z[24];
z[42] = -z[42] + z[43];
z[35] = -z[35] + 3 * z[41] + -z[42];
z[43] = abb[61] + z[17];
z[44] = abb[8] * z[43];
z[45] = z[1] + z[44];
z[46] = abb[56] + -abb[58] + z[13];
z[47] = abb[25] * z[46];
z[48] = z[45] + -z[47];
z[49] = 4 * abb[63];
z[50] = 4 * abb[60] + -3 * abb[64] + z[49];
z[51] = 5 * abb[62];
z[52] = 3 * abb[61] + z[50] + z[51];
z[52] = abb[5] * z[52];
z[53] = abb[23] * z[32];
z[54] = 2 * z[6];
z[55] = abb[7] * z[54];
z[27] = -z[27] + -z[35] + -z[40] + -2 * z[48] + -z[52] + z[53] + z[55];
z[48] = abb[34] * z[27];
z[52] = -abb[56] + -abb[58] + z[12];
z[53] = abb[22] * z[52];
z[55] = abb[15] * z[6];
z[55] = z[53] + -z[55];
z[56] = abb[27] * z[52];
z[57] = abb[62] + z[33];
z[58] = abb[17] * z[57];
z[56] = z[56] + z[58];
z[58] = z[55] + z[56];
z[59] = z[23] + z[57];
z[59] = abb[6] * z[59];
z[60] = 2 * z[34];
z[61] = -z[0] + z[60];
z[62] = -z[1] + z[40];
z[63] = abb[4] * z[6];
z[59] = z[58] + z[59] + -z[61] + -z[62] + z[63];
z[59] = 2 * z[59];
z[64] = abb[30] * z[59];
z[4] = z[4] + z[48] + z[64];
z[4] = m1_set::bc<T>[0] * z[4];
z[48] = 3 * abb[58];
z[64] = abb[56] + -2 * z[12] + -z[13] + z[48];
z[64] = abb[25] * z[64];
z[65] = abb[23] * z[52];
z[66] = z[55] + z[65];
z[67] = z[39] + z[66];
z[68] = abb[5] * z[18];
z[69] = -z[67] + z[68];
z[70] = 2 * abb[62];
z[71] = z[43] + z[70];
z[72] = abb[7] * z[71];
z[73] = abb[61] + abb[63];
z[74] = -abb[60] + z[73];
z[75] = abb[6] * z[74];
z[75] = z[72] + z[75];
z[76] = 3 * abb[63];
z[77] = abb[60] + -abb[61] + -z[70] + -z[76];
z[77] = abb[8] * z[77];
z[78] = 6 * z[36];
z[22] = -z[22] + -z[63] + z[64] + z[69] + -z[75] + z[77] + z[78];
z[22] = abb[32] * z[22];
z[64] = abb[25] * z[52];
z[77] = 2 * z[38];
z[66] = z[64] + z[66] + z[77];
z[7] = abb[8] * z[7];
z[79] = abb[26] * z[52];
z[80] = abb[24] * z[52];
z[79] = z[79] + z[80];
z[81] = abb[5] * z[6];
z[82] = 2 * z[36];
z[83] = z[7] + z[66] + z[79] + z[81] + -z[82];
z[83] = abb[35] * z[83];
z[84] = abb[7] * z[6];
z[79] = z[79] + z[84];
z[85] = abb[5] * z[57];
z[85] = -z[0] + z[41] + -z[79] + z[85];
z[58] = z[40] + -z[58] + z[85];
z[58] = abb[37] * z[58];
z[86] = abb[5] * z[43];
z[87] = abb[26] * z[46];
z[86] = z[82] + z[86] + -z[87];
z[88] = abb[4] * z[43];
z[89] = abb[24] * z[46];
z[75] = z[75] + -z[86] + z[88] + z[89];
z[75] = abb[33] * z[75];
z[90] = z[6] + z[17];
z[91] = -abb[64] + 2 * z[90];
z[91] = abb[5] * z[91];
z[92] = abb[17] * abb[64];
z[93] = abb[27] * z[46];
z[92] = z[92] + -z[93];
z[61] = z[41] + -z[61] + z[91] + -z[92];
z[91] = abb[23] * z[46];
z[93] = z[47] + z[91];
z[94] = z[87] + z[93];
z[44] = -z[44] + z[94];
z[95] = -z[44] + z[61];
z[96] = abb[6] * abb[64];
z[88] = -z[88] + z[96];
z[97] = z[88] + -z[89];
z[98] = z[1] + z[95] + z[97];
z[99] = abb[36] * z[98];
z[22] = z[22] + z[58] + z[75] + z[83] + z[99];
z[22] = m1_set::bc<T>[0] * z[22];
z[75] = abb[62] + z[76];
z[76] = -abb[60] + z[23] + z[75];
z[76] = abb[8] * z[76];
z[99] = z[17] + z[23];
z[100] = 3 * abb[62] + z[99];
z[100] = abb[7] * z[100];
z[3] = abb[6] * z[3];
z[3] = z[3] + z[20] + -z[68] + z[76] + z[77] + -z[78] + z[100];
z[20] = -abb[25] * z[14];
z[20] = -z[3] + z[20] + -z[21];
z[20] = abb[49] * z[20];
z[21] = abb[51] * z[98];
z[20] = z[20] + z[21] + z[22];
z[21] = abb[28] * z[14];
z[22] = abb[18] * z[25];
z[68] = abb[21] * z[29];
z[10] = abb[19] * z[10];
z[76] = abb[6] + abb[17];
z[76] = abb[0] + abb[5] + -8 * abb[29] + 2 * z[76];
z[76] = abb[65] * z[76];
z[78] = abb[20] * z[18];
z[10] = z[10] + z[21] + z[22] + -z[68] + z[76] + -z[78];
z[21] = abb[52] * z[10];
z[22] = abb[48] * z[59];
z[68] = abb[6] + -abb[8];
z[76] = -abb[61] + abb[62];
z[68] = z[68] * z[76];
z[66] = z[63] + z[66] + -z[68] + -z[84];
z[66] = 2 * z[66];
z[68] = -abb[50] * z[66];
z[4] = z[4] + 2 * z[20] + z[21] + z[22] + z[68];
z[4] = 2 * z[4];
z[3] = abb[67] * z[3];
z[20] = z[70] + z[99];
z[20] = abb[5] * z[20];
z[21] = abb[6] * z[6];
z[21] = z[21] + z[89];
z[22] = abb[16] * z[90];
z[68] = z[22] + -z[77];
z[78] = abb[8] * z[17];
z[78] = -z[68] + z[78];
z[90] = abb[4] * z[90];
z[98] = abb[3] * z[17];
z[99] = abb[9] * z[17];
z[100] = 2 * z[99];
z[20] = -z[20] + z[21] + -z[78] + z[90] + z[94] + z[98] + -z[100];
z[94] = abb[46] * z[20];
z[101] = abb[60] + z[6];
z[101] = abb[11] * z[101];
z[102] = 2 * z[101];
z[72] = -z[72] + z[102];
z[103] = abb[16] * z[43];
z[104] = z[72] + -z[103];
z[7] = z[7] + z[16] + -z[37] + z[64] + -z[69] + z[80] + -z[104];
z[7] = abb[32] * z[7];
z[37] = abb[61] + abb[64];
z[37] = abb[10] * z[37];
z[69] = 2 * z[37];
z[80] = z[40] + z[69] + z[95] + -z[103];
z[80] = abb[36] * z[80];
z[58] = z[58] + z[80];
z[80] = -z[77] + z[104];
z[86] = z[80] + z[86];
z[86] = abb[33] * z[86];
z[7] = z[7] + -z[58] + -z[83] + z[86];
z[7] = abb[31] * z[7];
z[83] = -abb[61] + z[17];
z[86] = abb[3] * z[83];
z[86] = z[86] + -z[100] + z[103];
z[9] = abb[4] * z[9];
z[95] = abb[5] * z[24];
z[95] = -z[9] + -z[16] + z[19] + -z[55] + -z[86] + z[95];
z[95] = abb[39] * z[95];
z[104] = abb[5] * abb[61];
z[105] = abb[3] * abb[61];
z[106] = z[104] + -z[105];
z[107] = z[90] + z[106];
z[108] = -z[22] + z[107];
z[29] = abb[6] * z[29];
z[57] = abb[2] * z[57];
z[109] = abb[8] * abb[61];
z[29] = z[29] + -2 * z[57] + z[60] + -z[69] + -z[108] + -z[109];
z[29] = abb[45] * z[29];
z[45] = -z[45] + -z[61] + z[87] + -z[88] + z[91];
z[45] = abb[69] * z[45];
z[60] = abb[3] * z[43];
z[60] = z[60] + z[89];
z[61] = z[62] + z[69];
z[62] = z[61] + -z[103];
z[87] = -z[0] + -z[60] + -z[62] + z[92] + -z[96];
z[87] = abb[38] * z[87];
z[88] = -abb[6] * z[71];
z[74] = -abb[8] * z[74];
z[60] = -z[60] + z[74] + -z[80] + z[88] + -z[93];
z[60] = abb[40] * z[60];
z[69] = -z[1] + z[69];
z[74] = abb[64] + z[25];
z[74] = abb[6] * z[74];
z[80] = abb[3] * z[6];
z[80] = z[74] + z[80];
z[56] = -z[56] + -z[69] + z[80] + z[85];
z[56] = abb[42] * z[56];
z[68] = z[68] + z[109];
z[85] = abb[6] * z[43];
z[85] = -z[68] + z[85];
z[79] = z[79] + -z[100];
z[88] = abb[5] * abb[62];
z[91] = abb[4] * z[17];
z[88] = -z[79] + z[85] + z[88] + -z[91] + z[105];
z[92] = abb[44] * z[88];
z[96] = -abb[4] + abb[5];
z[96] = z[71] * z[96];
z[44] = -z[44] + -z[86] + z[96];
z[44] = abb[41] * z[44];
z[86] = -abb[39] * z[14];
z[96] = abb[47] * abb[61];
z[86] = z[86] + -z[96];
z[86] = abb[23] * z[86];
z[105] = -abb[26] * z[96];
z[3] = z[3] + z[7] + z[29] + z[44] + z[45] + z[56] + z[60] + z[86] + z[87] + -z[92] + z[94] + z[95] + z[105];
z[7] = -z[23] + -z[33] + -z[70];
z[7] = abb[5] * z[7];
z[29] = z[19] + z[47];
z[33] = -z[41] + z[42];
z[42] = -abb[64] + z[6];
z[42] = abb[6] * z[42];
z[7] = z[7] + z[29] + z[33] + -2 * z[40] + z[42] + -z[65] + -z[69] + -z[78] + z[84] + -z[91] + -z[98];
z[7] = abb[30] * z[7];
z[20] = abb[33] * z[20];
z[42] = abb[5] * z[54];
z[42] = z[42] + z[67] + -z[84] + z[100];
z[44] = abb[16] * z[71];
z[45] = abb[6] * z[18];
z[47] = abb[8] * z[83];
z[29] = -z[29] + z[42] + -z[44] + z[45] + z[47] + -z[89];
z[29] = abb[32] * z[29];
z[44] = abb[35] * z[88];
z[20] = z[20] + z[29] + -z[44] + z[58];
z[32] = abb[24] * z[32];
z[32] = z[19] + z[32] + 4 * z[37] + 3 * z[40];
z[37] = abb[15] * z[54];
z[37] = -z[37] + 2 * z[53];
z[16] = z[16] + z[37];
z[40] = -z[1] + -z[103];
z[11] = z[11] + -z[16] + -z[26] + z[32] + -z[33] + 2 * z[40];
z[11] = abb[31] * z[11];
z[7] = z[7] + z[11] + 2 * z[20];
z[7] = abb[30] * z[7];
z[11] = -abb[8] * z[75];
z[11] = -z[11] + z[65] + z[101];
z[16] = z[16] + 4 * z[99];
z[20] = -z[49] + z[76];
z[20] = abb[6] * z[20];
z[26] = -4 * abb[61] + z[17] + -z[51];
z[26] = abb[5] * z[26];
z[33] = 4 * abb[62] + 3 * z[43];
z[33] = abb[16] * z[33];
z[40] = -abb[7] * z[24];
z[11] = -2 * z[11] + -z[16] + z[20] + z[26] + z[33] + 8 * z[36] + -10 * z[38] + z[40] + -z[63];
z[20] = prod_pow(abb[32], 2);
z[11] = z[11] * z[20];
z[0] = -z[0] + z[38] + -z[41] + z[57];
z[26] = abb[3] * abb[62];
z[26] = z[26] + z[90];
z[8] = -z[8] + z[73];
z[8] = 2 * z[8] + z[51];
z[8] = abb[8] * z[8];
z[1] = 4 * z[1] + -z[8] + -z[22] + z[26];
z[8] = abb[56] * (T(1) / T(3));
z[33] = -z[8] + (T(5) / T(3)) * z[12] + (T(4) / T(3)) * z[13] + -z[48];
z[33] = abb[25] * z[33];
z[40] = abb[27] * z[14];
z[19] = z[19] + z[30] + -z[40];
z[13] = 5 * abb[58] + -2 * z[13];
z[8] = z[8] + -z[12] + (T(1) / T(3)) * z[13];
z[12] = -abb[23] + -abb[24] + -abb[26];
z[8] = z[8] * z[12];
z[12] = abb[61] * (T(-5) / T(3)) + abb[62] * (T(-2) / T(3)) + -z[17];
z[12] = abb[6] * z[12];
z[13] = abb[61] * (T(-2) / T(3)) + -z[17];
z[13] = abb[62] * (T(-5) / T(3)) + abb[64] * (T(4) / T(3)) + 2 * z[13];
z[13] = abb[5] * z[13];
z[0] = (T(4) / T(3)) * z[0] + (T(-1) / T(3)) * z[1] + z[8] + z[12] + z[13] + (T(2) / T(3)) * z[19] + z[33] + (T(8) / T(3)) * z[34] + -z[82];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[1] = -abb[16] * abb[62];
z[1] = -z[1] + z[90] + z[109];
z[8] = z[18] + z[28];
z[8] = abb[6] * z[8];
z[12] = -z[6] + -z[50];
z[12] = abb[5] * z[12];
z[13] = abb[3] * z[23];
z[1] = -2 * z[1] + z[8] + z[12] + z[13] + z[15] + z[16] + -z[32] + -z[35] + z[39];
z[1] = abb[30] * z[1];
z[8] = abb[4] + -abb[16];
z[8] = abb[62] * z[8];
z[8] = z[8] + -z[21] + z[77] + z[106] + -z[109];
z[8] = abb[33] * z[8];
z[12] = z[26] + z[55] + z[85] + z[104];
z[12] = abb[35] * z[12];
z[13] = -z[55] + z[61] + -z[80];
z[13] = abb[37] * z[13];
z[15] = z[62] + -z[97];
z[16] = abb[36] * z[15];
z[8] = z[8] + z[12] + z[13] + z[16] + -z[29];
z[12] = -abb[31] * z[27];
z[6] = abb[64] + z[6];
z[6] = abb[6] * z[6];
z[6] = z[6] + -z[61] + z[68] + -z[107];
z[6] = abb[34] * z[6];
z[1] = z[1] + z[6] + 2 * z[8] + z[12];
z[1] = abb[34] * z[1];
z[6] = -abb[8] * z[2];
z[8] = -abb[6] * z[24];
z[12] = -abb[4] * abb[62];
z[6] = z[6] + z[8] + z[12] + z[22] + -z[72] + -z[93] + -z[98] + -z[104];
z[6] = abb[33] * z[6];
z[8] = abb[6] + abb[8];
z[8] = abb[63] * z[8];
z[8] = z[8] + -z[22] + -z[36] + z[38] + z[81] + z[99] + z[101];
z[8] = abb[32] * z[8];
z[6] = z[6] + 4 * z[8];
z[6] = abb[33] * z[6];
z[8] = z[5] + z[76];
z[8] = abb[8] * z[8];
z[5] = z[5] + -z[76];
z[5] = abb[6] * z[5];
z[5] = z[5] + z[8] + -2 * z[22] + z[42] + z[63] + z[64] + -z[82] + z[102];
z[5] = abb[32] * z[5];
z[2] = abb[62] + -z[2];
z[2] = abb[6] * z[2];
z[2] = z[2] + -z[102] + -z[108] + z[109];
z[2] = abb[33] * z[2];
z[2] = z[2] + z[5];
z[2] = 2 * z[2] + -z[44];
z[2] = abb[35] * z[2];
z[5] = -abb[70] * z[10];
z[8] = -abb[66] * z[59];
z[10] = abb[5] * z[76];
z[9] = -z[9] + z[10] + -z[79];
z[10] = abb[3] * z[70];
z[10] = -z[9] + z[10] + z[37] + -z[61] + z[74];
z[10] = abb[37] * z[10];
z[12] = abb[3] * z[76];
z[9] = -z[9] + z[12] + z[55];
z[9] = 2 * z[9];
z[12] = -abb[35] * z[9];
z[10] = z[10] + z[12];
z[10] = abb[37] * z[10];
z[12] = abb[67] * z[14];
z[13] = z[20] * z[52];
z[14] = abb[69] * z[46];
z[16] = z[12] + -z[13] + z[14] + -z[96];
z[16] = z[16] * z[31];
z[17] = -abb[47] * z[25];
z[18] = -abb[39] * z[52];
z[12] = z[12] + z[14] + z[17] + z[18];
z[12] = 2 * z[12] + -z[13];
z[12] = abb[24] * z[12];
z[13] = abb[68] * z[66];
z[9] = abb[43] * z[9];
z[14] = -prod_pow(abb[36], 2) * z[15];
z[0] = z[0] + z[1] + z[2] + 2 * z[3] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[16];
z[0] = 2 * z[0];
return {z[4], z[0]};
}


template <typename T> std::complex<T> f_4_702_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("-5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("-5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("-5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("-10.4023028191312333362719401831913221365702768824772793193192573969"),stof<T>("-4.0099607112369939801253103352120106710102959542432765442127783144")}, std::complex<T>{stof<T>("15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("-1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("-20.462455036408463729808733224948042501941452267068667608161763499"),stof<T>("6.089428100462557475767236822789473687138061043101092996314176422")}, std::complex<T>{stof<T>("15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("-1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("-22.321247684068818373749629395812920184418503085940393472811993357"),stof<T>("-60.534604902327698016837525872710323376839196109014958708311727643")}, std::complex<T>{stof<T>("-14.897827988788396353183658562684984226756992194539273163699551436"),stof<T>("-37.603689858958450862522766595083803819138105860622441272503833676")}, std::complex<T>{stof<T>("-22.654164929948585638211946740953033747896296345994313018361085116"),stof<T>("-75.54408695380354582728424912221723787090618345078168742786077269")}, std::complex<T>{stof<T>("-16.567913731632433249499466392282724728612136552544704134525437244"),stof<T>("-35.843059862644944971793544257796964795091316595581974737669397166")}, std::complex<T>{stof<T>("5.957391057342135285693590092575649429567304212121563924157947368"),stof<T>("39.07861243167864131057641461106806128740498002050804085489030253")}, std::complex<T>{stof<T>("-0.051881463697184215754529699493716126989310907775594881303416497"),stof<T>("25.098197810448281517736238067280671084815067568294361227844109094")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_702_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_702_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("4.8390887666948276009127427655288467457089182538746770689892713919"),stof<T>("-9.9247761861834175663192947100875963928828506255281830520561068422")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,71> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W21(k,dl), dlog_W23(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_20(k), f_2_21(k), f_2_29_im(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_702_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_702_DLogXconstant_part(base_point<T>, kend);
	value += f_4_702_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_702_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_702_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_702_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_702_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_702_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_702_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
