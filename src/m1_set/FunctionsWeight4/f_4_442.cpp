/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_442.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_442_abbreviated (const std::array<T,62>& abb) {
T z[90];
z[0] = abb[22] + abb[24];
z[1] = abb[29] * z[0];
z[2] = abb[31] * z[0];
z[1] = z[1] + -z[2];
z[3] = abb[30] + -abb[33];
z[4] = -z[0] * z[3];
z[5] = abb[29] + -abb[32];
z[6] = -abb[34] + z[5];
z[7] = abb[33] + z[6];
z[8] = abb[23] * z[7];
z[8] = z[1] + z[4] + z[8];
z[8] = m1_set::bc<T>[0] * z[8];
z[9] = abb[40] + -abb[41];
z[10] = -z[0] * z[9];
z[11] = -abb[29] + abb[34];
z[12] = z[3] + z[11];
z[12] = m1_set::bc<T>[0] * z[12];
z[12] = -z[9] + z[12];
z[13] = abb[21] * z[12];
z[7] = m1_set::bc<T>[0] * z[7];
z[7] = abb[40] + z[7];
z[7] = abb[25] * z[7];
z[14] = abb[31] + -abb[32];
z[15] = abb[29] + z[14];
z[16] = -abb[34] + z[15];
z[17] = m1_set::bc<T>[0] * z[16];
z[18] = abb[40] + z[17];
z[19] = abb[26] * z[18];
z[20] = abb[23] * abb[40];
z[7] = -z[7] + -z[8] + z[10] + z[13] + z[19] + -z[20];
z[8] = abb[46] * z[7];
z[10] = abb[45] + -abb[47] + abb[49] + abb[50];
z[13] = abb[33] * z[10];
z[19] = abb[34] * z[10];
z[20] = z[13] + -z[19];
z[21] = abb[30] * z[10];
z[22] = abb[29] * z[10];
z[23] = z[20] + -z[21] + z[22];
z[23] = m1_set::bc<T>[0] * z[23];
z[24] = abb[40] * z[10];
z[25] = -abb[41] * z[10];
z[23] = z[23] + z[24] + z[25];
z[23] = abb[21] * z[23];
z[25] = abb[32] * z[10];
z[26] = -z[22] + z[25];
z[20] = z[20] + -z[26];
z[27] = m1_set::bc<T>[0] * z[20];
z[27] = z[24] + z[27];
z[27] = abb[25] * z[27];
z[28] = abb[31] * z[10];
z[19] = z[19] + -z[28];
z[29] = z[19] + z[26];
z[30] = m1_set::bc<T>[0] * z[29];
z[30] = -z[24] + z[30];
z[30] = abb[26] * z[30];
z[31] = -abb[29] + abb[31];
z[32] = z[3] + z[31];
z[32] = m1_set::bc<T>[0] * z[32];
z[33] = z[9] + z[32];
z[33] = abb[51] * z[33];
z[34] = abb[41] + z[32];
z[35] = -abb[55] * z[34];
z[36] = abb[40] * abb[56];
z[33] = z[33] + z[35] + z[36];
z[33] = abb[8] * z[33];
z[35] = abb[18] + -abb[20];
z[36] = -z[17] * z[35];
z[37] = abb[17] + abb[20];
z[37] = abb[40] * z[37];
z[36] = z[36] + z[37];
z[36] = abb[57] * z[36];
z[37] = -abb[29] + abb[30];
z[37] = m1_set::bc<T>[0] * z[37];
z[38] = -z[9] + z[37];
z[39] = -abb[51] + abb[55];
z[39] = abb[4] * z[39];
z[38] = z[38] * z[39];
z[40] = abb[51] + abb[55];
z[41] = abb[6] * z[40];
z[42] = abb[30] + -abb[32];
z[42] = m1_set::bc<T>[0] * z[42];
z[43] = abb[41] + z[42];
z[43] = z[41] * z[43];
z[24] = abb[23] * z[24];
z[8] = z[8] + z[23] + z[24] + z[27] + z[30] + z[33] + z[36] + z[38] + z[43];
z[23] = z[0] * z[10];
z[24] = -abb[7] * z[40];
z[27] = -abb[16] * abb[51];
z[30] = -abb[17] + -z[35];
z[30] = abb[57] * z[30];
z[24] = -z[23] + z[24] + z[27] + z[30];
z[27] = abb[26] * (T(1) / T(2));
z[30] = -z[10] * z[27];
z[33] = -abb[55] + abb[51] * (T(-1) / T(2));
z[33] = abb[3] * z[33];
z[24] = (T(1) / T(2)) * z[24] + z[30] + z[33];
z[30] = -abb[51] + abb[56];
z[33] = -abb[55] + z[30];
z[36] = abb[2] * (T(1) / T(2));
z[33] = z[33] * z[36];
z[36] = abb[16] * (T(1) / T(4));
z[38] = abb[0] * (T(1) / T(2));
z[43] = -abb[13] + z[36] + -z[38];
z[44] = abb[8] * (T(1) / T(4));
z[45] = abb[3] * (T(3) / T(4)) + z[43] + z[44];
z[46] = abb[52] + abb[54];
z[45] = -z[45] * z[46];
z[47] = abb[3] * (T(1) / T(4));
z[43] = -z[43] + -z[47];
z[43] = abb[56] * z[43];
z[30] = -z[30] * z[44];
z[48] = abb[46] * (T(1) / T(4));
z[49] = abb[26] + z[0];
z[50] = z[48] * z[49];
z[51] = abb[53] * (T(1) / T(2));
z[52] = abb[2] + -abb[3];
z[52] = z[51] * z[52];
z[24] = (T(1) / T(2)) * z[24] + z[30] + -z[33] + z[43] + z[45] + z[50] + z[52];
z[24] = abb[42] * z[24];
z[30] = abb[51] + abb[53];
z[43] = z[30] + z[46];
z[43] = abb[1] * z[43];
z[45] = -abb[29] + z[14];
z[50] = -abb[33] + z[45];
z[50] = abb[30] + (T(1) / T(2)) * z[50];
z[50] = m1_set::bc<T>[0] * z[50];
z[52] = abb[41] * (T(1) / T(2));
z[50] = z[50] + z[52];
z[50] = -z[43] * z[50];
z[24] = z[24] + z[50];
z[50] = 3 * abb[31];
z[53] = abb[29] + abb[32];
z[54] = -abb[34] + z[50] + -z[53];
z[54] = z[3] + (T(1) / T(2)) * z[54];
z[54] = m1_set::bc<T>[0] * z[54];
z[55] = abb[40] * (T(1) / T(2));
z[56] = abb[41] + z[54] + -z[55];
z[56] = abb[0] * z[56];
z[54] = abb[3] * z[54];
z[57] = abb[13] * z[17];
z[58] = abb[16] * z[18];
z[57] = -z[57] + (T(1) / T(2)) * z[58];
z[59] = abb[8] * z[55];
z[60] = abb[12] * z[9];
z[54] = -z[54] + z[56] + -z[57] + z[59] + -z[60];
z[34] = abb[7] * z[34];
z[42] = abb[11] * z[42];
z[56] = z[34] + z[42];
z[59] = z[54] + z[56];
z[59] = abb[54] * z[59];
z[18] = z[18] * z[38];
z[61] = abb[3] * (T(1) / T(2));
z[17] = -z[17] * z[61];
z[17] = z[17] + z[18] + -z[57];
z[17] = abb[56] * z[17];
z[18] = abb[31] * (T(1) / T(2));
z[57] = -abb[29] + z[18];
z[62] = abb[34] * (T(1) / T(2));
z[63] = z[57] + z[62];
z[64] = -abb[33] + z[63];
z[65] = abb[32] * (T(1) / T(2));
z[66] = abb[30] * (T(1) / T(2));
z[67] = z[64] + z[65] + z[66];
z[67] = m1_set::bc<T>[0] * z[67];
z[67] = z[52] + z[67];
z[67] = abb[7] * z[67];
z[6] = abb[31] + -z[6];
z[6] = -abb[33] + (T(1) / T(2)) * z[6];
z[6] = m1_set::bc<T>[0] * z[6];
z[6] = z[6] + -z[55];
z[6] = abb[0] * z[6];
z[6] = z[6] + z[67];
z[6] = abb[55] * z[6];
z[32] = -abb[3] * z[32];
z[55] = -abb[31] + abb[33];
z[67] = m1_set::bc<T>[0] * z[55];
z[68] = -abb[0] * z[67];
z[32] = z[32] + z[56] + z[68];
z[32] = abb[53] * z[32];
z[56] = z[9] * z[46];
z[12] = -z[12] * z[40];
z[12] = (T(1) / T(2)) * z[12] + z[56];
z[12] = abb[14] * z[12];
z[30] = abb[9] * z[30];
z[37] = z[30] * z[37];
z[6] = z[6] + z[12] + z[17] + z[32] + z[37] + z[59];
z[12] = abb[42] * z[49];
z[7] = z[7] + z[12];
z[12] = abb[44] * (T(1) / T(16)) + abb[48] * (T(1) / T(32));
z[7] = z[7] * z[12];
z[17] = z[34] + z[42] + z[54];
z[17] = (T(1) / T(16)) * z[17];
z[17] = abb[52] * z[17];
z[32] = z[0] * z[22];
z[34] = abb[31] * z[23];
z[32] = z[32] + -z[34];
z[20] = abb[23] * z[20];
z[37] = z[3] * z[23];
z[20] = z[20] + z[32] + -z[37];
z[42] = -z[3] + -z[63];
z[42] = abb[51] * z[42];
z[49] = (T(1) / T(2)) * z[5];
z[54] = z[49] + z[55];
z[54] = abb[55] * z[54];
z[42] = z[42] + z[54];
z[42] = abb[3] * z[42];
z[20] = (T(1) / T(32)) * z[20] + (T(1) / T(16)) * z[42];
z[20] = m1_set::bc<T>[0] * z[20];
z[42] = abb[30] * (T(3) / T(2)) + z[64] + -z[65];
z[42] = m1_set::bc<T>[0] * z[42];
z[42] = z[42] + z[52];
z[42] = abb[7] * z[42];
z[52] = z[3] + z[14];
z[52] = m1_set::bc<T>[0] * z[52];
z[52] = abb[41] + z[52];
z[52] = abb[0] * z[52];
z[42] = -z[42] + -z[52] + z[60];
z[42] = (T(-1) / T(16)) * z[42] + (T(-1) / T(32)) * z[58];
z[42] = abb[51] * z[42];
z[9] = (T(1) / T(32)) * z[9];
z[9] = z[9] * z[23];
z[52] = abb[5] * (T(1) / T(32));
z[52] = z[40] * z[52];
z[54] = abb[42] + -z[67];
z[54] = z[52] * z[54];
z[6] = abb[61] + (T(1) / T(16)) * z[6] + z[7] + (T(1) / T(32)) * z[8] + z[9] + z[17] + z[20] + (T(1) / T(8)) * z[24] + z[42] + z[54];
z[7] = abb[34] * z[29];
z[8] = abb[58] * z[10];
z[7] = z[7] + -z[8];
z[9] = z[14] * z[22];
z[9] = z[7] + z[9];
z[17] = z[22] + z[25];
z[20] = z[17] + -z[28];
z[22] = -z[10] * z[66];
z[22] = z[20] + z[22];
z[22] = abb[30] * z[22];
z[24] = abb[33] * z[19];
z[29] = abb[35] + abb[36];
z[42] = prod_pow(abb[32], 2);
z[54] = (T(1) / T(2)) * z[42];
z[55] = abb[37] + -z[29] + -z[54];
z[55] = z[10] * z[55];
z[22] = z[9] + z[22] + -z[24] + z[55];
z[22] = abb[23] * z[22];
z[13] = z[13] + z[20] + -z[21];
z[13] = abb[30] * z[13];
z[20] = -abb[35] + abb[37];
z[21] = abb[59] + z[20];
z[21] = z[10] * z[21];
z[19] = -z[19] + -z[25];
z[19] = abb[33] * z[19];
z[9] = z[9] + z[13] + z[19] + z[21];
z[9] = abb[21] * z[9];
z[13] = abb[33] * (T(1) / T(2));
z[11] = -z[11] + z[13];
z[19] = abb[33] * z[11];
z[21] = prod_pow(abb[34], 2);
z[55] = (T(1) / T(2)) * z[21];
z[56] = z[20] + z[55];
z[58] = -abb[33] + z[66];
z[59] = abb[30] * z[58];
z[60] = prod_pow(abb[29], 2);
z[60] = (T(1) / T(2)) * z[60];
z[63] = abb[38] + -abb[59];
z[64] = abb[58] + z[63];
z[19] = z[19] + z[56] + z[59] + -z[60] + z[64];
z[19] = z[19] * z[39];
z[39] = z[15] * z[18];
z[67] = abb[29] * (T(1) / T(2));
z[68] = z[5] * z[67];
z[68] = -abb[58] + z[68];
z[39] = z[39] + z[68];
z[69] = z[15] + -z[62];
z[70] = -abb[34] * z[69];
z[71] = -abb[60] + z[70];
z[72] = prod_pow(m1_set::bc<T>[0], 2);
z[73] = (T(1) / T(6)) * z[72];
z[74] = z[39] + z[71] + -z[73];
z[75] = -abb[16] * z[74];
z[76] = abb[19] * abb[39];
z[77] = abb[20] * abb[39];
z[78] = abb[17] * abb[39];
z[79] = z[76] + z[77] + z[78];
z[80] = abb[18] * abb[39];
z[81] = (T(1) / T(2)) * z[80];
z[79] = z[75] + (T(1) / T(2)) * z[79] + -z[81];
z[79] = abb[51] * z[79];
z[82] = -z[18] + -z[67];
z[82] = z[26] * z[82];
z[83] = abb[37] * z[10];
z[7] = z[7] + -z[24] + z[82] + z[83];
z[7] = abb[25] * z[7];
z[24] = z[54] + z[63];
z[82] = z[10] * z[24];
z[25] = z[25] * z[67];
z[25] = z[8] + z[25] + z[82];
z[25] = -z[0] * z[25];
z[17] = z[0] * z[17];
z[17] = z[17] + -z[34];
z[34] = z[17] * z[18];
z[82] = -z[13] * z[23];
z[32] = -z[32] + z[82];
z[32] = abb[33] * z[32];
z[17] = z[17] + -z[37];
z[17] = abb[30] * z[17];
z[37] = abb[60] + z[73];
z[82] = -abb[36] + z[37];
z[23] = z[23] * z[82];
z[7] = z[7] + z[9] + z[17] + z[19] + z[22] + z[23] + z[25] + z[32] + z[34] + z[79];
z[9] = z[16] * z[62];
z[17] = abb[60] * (T(1) / T(2));
z[19] = (T(1) / T(12)) * z[72];
z[9] = z[9] + z[17] + -z[19];
z[22] = abb[33] * (T(1) / T(4)) + z[62];
z[23] = -z[14] + z[22] + z[67];
z[23] = abb[33] * z[23];
z[24] = abb[36] + z[24];
z[3] = z[3] + z[45];
z[3] = abb[30] * z[3];
z[25] = abb[31] * (T(1) / T(4));
z[32] = -abb[29] + z[25];
z[34] = z[32] + -z[65];
z[34] = abb[31] * z[34];
z[79] = abb[37] * (T(1) / T(2));
z[83] = abb[29] * (T(1) / T(4));
z[84] = abb[32] + z[83];
z[84] = abb[29] * z[84];
z[23] = (T(3) / T(2)) * z[3] + z[9] + z[23] + (T(1) / T(2)) * z[24] + z[34] + -z[79] + z[84];
z[23] = abb[7] * z[23];
z[24] = z[18] * z[45];
z[34] = z[24] + -z[56] + -z[73];
z[84] = -abb[34] + z[14];
z[84] = abb[33] * z[84];
z[84] = abb[59] + -z[3] + z[84];
z[85] = z[53] * z[67];
z[85] = z[34] + -z[84] + z[85];
z[85] = abb[0] * z[85];
z[86] = abb[12] * z[64];
z[23] = z[23] + z[85] + z[86];
z[23] = abb[51] * z[23];
z[85] = abb[36] + z[54];
z[63] = z[63] + z[85];
z[87] = -abb[37] + z[3] + z[60] + z[63];
z[32] = z[32] + z[65];
z[32] = abb[31] * z[32];
z[22] = -abb[31] + z[22];
z[65] = z[22] + z[67];
z[65] = abb[33] * z[65];
z[9] = z[9] + z[32] + z[65] + (T(1) / T(2)) * z[87];
z[9] = abb[7] * z[9];
z[32] = abb[31] + -z[5];
z[32] = z[25] * z[32];
z[65] = abb[34] * (T(-3) / T(2)) + z[15];
z[65] = z[62] * z[65];
z[87] = -abb[31] + abb[34];
z[87] = abb[33] * z[87];
z[88] = -abb[37] + z[87];
z[32] = -z[19] + z[32] + z[65] + (T(-1) / T(2)) * z[68] + z[88];
z[32] = abb[0] * z[32];
z[65] = z[76] + -z[80];
z[9] = z[9] + z[32] + (T(1) / T(4)) * z[65];
z[9] = abb[55] * z[9];
z[32] = z[62] * z[69];
z[65] = z[5] * z[83];
z[15] = z[15] * z[25];
z[69] = abb[58] * (T(1) / T(2));
z[15] = z[15] + -z[17] + -z[32] + z[65] + -z[69];
z[15] = abb[20] * z[15];
z[17] = z[5] * z[18];
z[65] = -abb[60] + z[17] + -z[68];
z[76] = abb[17] * z[65];
z[5] = z[5] + z[18];
z[5] = abb[31] * z[5];
z[71] = z[5] + z[71];
z[83] = abb[18] * z[71];
z[76] = z[76] + z[83];
z[35] = z[19] * z[35];
z[36] = abb[28] + abb[0] * (T(-1) / T(4)) + -z[36];
z[36] = abb[39] * z[36];
z[15] = z[15] + z[35] + z[36] + (T(-1) / T(2)) * z[76];
z[15] = abb[57] * z[15];
z[16] = abb[34] * z[16];
z[20] = z[16] + -z[20];
z[14] = 3 * abb[29] + -z[14];
z[14] = z[14] * z[18];
z[35] = -abb[60] + z[85];
z[36] = abb[29] * abb[32];
z[14] = abb[38] + z[14] + -z[20] + -z[35] + (T(-3) / T(2)) * z[36];
z[76] = -z[3] + z[19];
z[83] = -z[22] + (T(-1) / T(2)) * z[53];
z[83] = abb[33] * z[83];
z[14] = (T(1) / T(2)) * z[14] + z[76] + z[83];
z[14] = abb[51] * z[14];
z[83] = abb[32] * z[67];
z[89] = -abb[31] + z[49];
z[89] = abb[31] * z[89];
z[56] = abb[38] + z[56] + z[83] + -z[85] + z[89];
z[22] = -z[22] + -z[49];
z[22] = abb[33] * z[22];
z[22] = z[22] + z[37] + (T(1) / T(2)) * z[56];
z[22] = abb[55] * z[22];
z[14] = z[14] + z[22];
z[14] = abb[3] * z[14];
z[22] = z[26] + -z[28];
z[28] = -z[10] * z[62];
z[28] = -z[22] + z[28];
z[28] = abb[34] * z[28];
z[49] = z[10] * z[37];
z[26] = z[26] * z[67];
z[22] = z[18] * z[22];
z[8] = z[8] + z[22] + z[26] + z[28] + z[49];
z[8] = z[8] * z[27];
z[22] = abb[29] * abb[31];
z[20] = abb[58] + z[20] + -z[22] + z[36];
z[22] = z[20] + -z[84];
z[26] = -z[22] * z[40];
z[27] = -z[46] * z[64];
z[26] = (T(1) / T(2)) * z[26] + z[27];
z[26] = abb[14] * z[26];
z[7] = (T(1) / T(2)) * z[7] + z[8] + z[9] + z[14] + z[15] + z[23] + z[26];
z[8] = z[54] + z[64] + -z[82] + z[83];
z[8] = z[0] * z[8];
z[9] = z[16] + -z[17] + -z[68] + z[88];
z[9] = abb[25] * z[9];
z[14] = z[0] * z[53];
z[2] = -z[2] + z[14];
z[4] = z[2] + z[4];
z[4] = abb[30] * z[4];
z[14] = abb[26] * z[74];
z[15] = z[45] + z[66];
z[15] = abb[30] * z[15];
z[16] = z[15] + z[87];
z[17] = z[16] + z[20] + z[85];
z[17] = abb[23] * z[17];
z[0] = z[0] * z[13];
z[0] = z[0] + z[1];
z[0] = abb[33] * z[0];
z[1] = abb[21] * z[22];
z[2] = z[2] * z[18];
z[0] = z[0] + z[1] + -z[2] + -z[4] + z[8] + z[9] + z[14] + z[17];
z[1] = z[0] * z[48];
z[2] = abb[31] * abb[32];
z[4] = abb[32] * abb[33];
z[2] = z[2] + -z[3] + -z[4] + -z[36];
z[2] = abb[11] * z[2];
z[8] = z[13] + -z[31];
z[8] = abb[33] * z[8];
z[3] = z[3] + z[8];
z[8] = z[3] + z[63];
z[9] = abb[31] * z[57];
z[9] = z[8] + z[9] + z[60] + -z[73];
z[14] = abb[7] * z[9];
z[17] = z[77] + -z[78];
z[17] = (T(1) / T(2)) * z[17] + z[75];
z[20] = z[17] + -z[80];
z[20] = -z[2] + z[14] + (T(1) / T(2)) * z[20] + z[86];
z[22] = -abb[29] + -3 * abb[32] + z[50];
z[22] = z[22] * z[25];
z[23] = z[32] + (T(1) / T(4)) * z[72];
z[25] = abb[29] * z[53];
z[8] = -abb[60] + z[8] + z[22] + -z[23] + (T(1) / T(4)) * z[25] + z[69];
z[8] = z[8] * z[38];
z[22] = z[36] + z[42];
z[22] = abb[36] + (T(1) / T(2)) * z[22];
z[26] = -abb[32] + abb[31] * (T(3) / T(4));
z[26] = abb[31] * z[26];
z[23] = abb[60] * (T(-3) / T(2)) + z[3] + z[22] + -z[23] + z[26];
z[23] = z[23] * z[61];
z[26] = z[44] * z[65];
z[5] = z[5] + z[70];
z[19] = abb[60] + z[19];
z[5] = (T(1) / T(2)) * z[5] + -z[19];
z[5] = abb[13] * z[5];
z[8] = z[5] + z[8] + (T(1) / T(2)) * z[20] + -z[23] + -z[26];
z[8] = z[8] * z[46];
z[20] = z[39] + z[70];
z[19] = -z[19] + (T(1) / T(2)) * z[20];
z[19] = z[19] * z[38];
z[20] = -z[71] + z[73];
z[20] = z[20] * z[47];
z[5] = z[5] + (T(1) / T(4)) * z[17] + z[19] + z[20];
z[5] = abb[56] * z[5];
z[17] = z[25] + z[42];
z[17] = (T(1) / T(2)) * z[17];
z[16] = abb[36] + z[16] + z[17] + z[34];
z[16] = abb[0] * z[16];
z[3] = z[3] + z[24] + -z[73];
z[19] = abb[60] + -z[3] + -z[22];
z[19] = abb[3] * z[19];
z[20] = -abb[2] * abb[60];
z[2] = -z[2] + z[14] + z[16] + z[19] + z[20] + -z[81];
z[2] = z[2] * z[51];
z[3] = z[3] + z[35] + -z[64] + z[83];
z[3] = abb[51] * z[3];
z[9] = -abb[55] * z[9];
z[14] = -abb[56] * z[65];
z[16] = abb[39] * (T(1) / T(2));
z[19] = -abb[57] * z[16];
z[3] = z[3] + z[9] + z[14] + z[19];
z[3] = z[3] * z[44];
z[9] = z[58] * z[66];
z[11] = z[11] * z[13];
z[14] = -abb[38] + z[60];
z[9] = -abb[35] + z[9] + z[11] + (T(-1) / T(2)) * z[14] + (T(1) / T(4)) * z[21] + z[79];
z[9] = z[9] * z[30];
z[11] = z[18] * z[53];
z[11] = -z[11] + z[15] + z[17] + z[29];
z[11] = abb[15] * z[11] * z[40];
z[4] = abb[59] + -z[4] + -z[59] + z[85];
z[4] = -z[4] * z[41];
z[4] = z[4] + z[11];
z[11] = z[13] + -z[45];
z[11] = abb[33] * z[11];
z[13] = z[18] + -z[53];
z[13] = abb[31] * z[13];
z[14] = abb[32] + z[67];
z[14] = abb[29] * z[14];
z[11] = z[11] + z[13] + z[14] + z[63];
z[11] = (T(1) / T(2)) * z[11] + -z[76];
z[11] = -z[11] * z[43];
z[13] = abb[60] * z[33];
z[10] = -abb[46] + z[10];
z[10] = abb[27] * abb[39] * z[10];
z[1] = z[1] + z[2] + z[3] + (T(1) / T(4)) * z[4] + z[5] + (T(1) / T(2)) * z[7] + z[8] + z[9] + (T(1) / T(8)) * z[10] + z[11] + z[13];
z[2] = abb[27] * z[16];
z[0] = z[0] + -z[2];
z[0] = z[0] * z[12];
z[2] = prod_pow(abb[31], 2);
z[2] = (T(-1) / T(2)) * z[2] + z[37] + z[55] + -z[88];
z[2] = -z[2] * z[52];
z[0] = abb[43] + z[0] + (T(1) / T(8)) * z[1] + z[2];
return {z[6], z[0]};
}


template <typename T> std::complex<T> f_4_442_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.11233733165981908115865460940883609131886566373610456584169226952"),stof<T>("-0.13217945716397632975053233715317979298955879724657241970870660756")}, std::complex<T>{stof<T>("0.0427802290817057885374239026824539953270740463452897187726221166"),stof<T>("0.26794842810340937909538814468734169242255837066297445670922101667")}, std::complex<T>{stof<T>("0.065716509415200746458588630906325460558733699975773029505722989386"),stof<T>("0.088945173517021160711300376168561761688114968993162493795765266075")}, std::complex<T>{stof<T>("0.0427802290817057885374239026824539953270740463452897187726221166"),stof<T>("0.26794842810340937909538814468734169242255837066297445670922101667")}, std::complex<T>{stof<T>("0.09550342832573759374219620835243952316870225829874964124278833186"),stof<T>("0.15592407784831183138898656478070941240225389987433245912761486039")}, std::complex<T>{stof<T>("-0.016435191501797186144418708983212813151046729127317117556625088764"),stof<T>("0.030373567780978282109358666898988592548664251477348641537935749809")}, std::complex<T>{stof<T>("-0.12593375930407590941161915200899992428250791545343682933411608509"),stof<T>("0.13801363516870842049040280265772312585990829028334043153424807411")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_442_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_442_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_442_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(512)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (-30 * v[0] + 14 * v[1] + -6 * v[2] + -6 * v[3] + 6 * v[5] + m1_set::bc<T>[2] * (21 * v[0] + -v[1] + -3 * (-8 + -3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[2] * (T(-3) / T(64)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[51] + abb[53] + abb[55]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[51] + -abb[53] + -abb[55];
z[1] = -abb[35] + abb[38];
return abb[10] * (T(1) / T(16)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_442_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.00897161861057647797021460969078765113333079215822420734776380894"),stof<T>("-0.21201960730796774152001785807014792714958978032196034346572313048")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,62> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W17(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_9_im(k), f_2_12_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_2_re(k), f_2_9_re(k), f_2_12_re(k), T{0}};
abb[43] = SpDLog_f_4_442_W_17_Im(t, path, abb);
abb[61] = SpDLog_f_4_442_W_17_Re(t, path, abb);

                    
            return f_4_442_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_442_DLogXconstant_part(base_point<T>, kend);
	value += f_4_442_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_442_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_442_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_442_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_442_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_442_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_442_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
