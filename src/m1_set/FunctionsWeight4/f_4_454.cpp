/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_454.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_454_abbreviated (const std::array<T,59>& abb) {
T z[73];
z[0] = abb[4] + abb[14];
z[1] = abb[29] * (T(1) / T(2));
z[2] = z[0] * z[1];
z[3] = abb[14] * abb[31];
z[3] = -z[2] + z[3];
z[3] = abb[48] * z[3];
z[4] = abb[45] + abb[46];
z[4] = -abb[42] + abb[43] * (T(-1) / T(2)) + abb[44] * (T(-1) / T(2)) + (T(1) / T(2)) * z[4];
z[5] = z[1] * z[4];
z[6] = abb[31] * z[4];
z[7] = z[5] + -z[6];
z[8] = abb[23] * z[7];
z[3] = z[3] + z[8];
z[8] = abb[28] + -abb[30];
z[9] = abb[31] + z[8];
z[10] = z[1] + -z[9];
z[11] = abb[20] + abb[22];
z[11] = z[4] * z[11];
z[10] = z[10] * z[11];
z[12] = abb[26] * abb[48];
z[9] = abb[48] * z[9];
z[9] = z[9] + -z[12];
z[13] = abb[13] * z[9];
z[14] = abb[23] * z[4];
z[15] = -z[11] + z[14];
z[16] = abb[48] * z[0];
z[17] = z[15] + -z[16];
z[18] = abb[27] * (T(1) / T(2));
z[19] = -z[17] * z[18];
z[20] = abb[31] * abb[48];
z[20] = -z[12] + z[20];
z[21] = abb[8] * z[20];
z[22] = -abb[4] * z[12];
z[10] = -z[3] + z[10] + -z[13] + z[19] + z[21] + z[22];
z[19] = abb[27] * (T(1) / T(4));
z[21] = abb[28] * (T(1) / T(2));
z[22] = abb[30] * (T(1) / T(2));
z[23] = abb[26] * (T(1) / T(2));
z[24] = z[19] + -z[21] + z[22] + z[23];
z[24] = z[4] * z[24];
z[25] = abb[29] * z[4];
z[24] = -z[6] + z[24] + (T(1) / T(4)) * z[25];
z[24] = abb[19] * z[24];
z[26] = z[4] * z[22];
z[7] = z[7] + z[26];
z[7] = abb[21] * z[7];
z[7] = z[7] + (T(1) / T(2)) * z[10] + z[24];
z[7] = m1_set::bc<T>[0] * z[7];
z[10] = abb[21] * z[4];
z[24] = abb[19] * z[4];
z[26] = z[10] + z[24];
z[17] = z[17] + -z[26];
z[17] = abb[37] * z[17];
z[27] = abb[30] * z[4];
z[28] = abb[28] * z[4];
z[27] = z[27] + -z[28];
z[29] = abb[26] * z[4];
z[30] = -z[6] + z[27] + z[29];
z[31] = m1_set::bc<T>[0] * z[30];
z[32] = abb[36] + -abb[38];
z[32] = z[4] * z[32];
z[31] = z[31] + z[32];
z[31] = abb[24] * z[31];
z[32] = abb[25] * z[4];
z[33] = abb[16] + abb[18];
z[34] = abb[48] * z[33];
z[32] = z[32] + -z[34];
z[34] = abb[39] * z[32];
z[35] = abb[41] * abb[48];
z[34] = z[34] + z[35];
z[26] = -abb[38] * z[26];
z[35] = abb[36] * z[24];
z[17] = z[17] + z[26] + z[31] + (T(1) / T(2)) * z[34] + z[35];
z[26] = abb[26] + -abb[31];
z[31] = -z[8] + z[26];
z[31] = abb[13] * z[31];
z[34] = abb[28] + z[1];
z[35] = -abb[26] + z[34];
z[35] = abb[12] * z[35];
z[36] = -abb[4] + abb[6];
z[37] = abb[26] * z[36];
z[38] = abb[6] + abb[14];
z[39] = abb[31] * z[38];
z[35] = z[2] + z[31] + z[35] + z[37] + -z[39];
z[37] = -abb[12] + z[0];
z[37] = z[19] * z[37];
z[39] = -abb[31] + z[1];
z[40] = z[22] + z[39];
z[41] = abb[7] * z[40];
z[35] = (T(1) / T(2)) * z[35] + z[37] + -z[41];
z[37] = abb[8] * z[26];
z[41] = -z[35] + z[37];
z[41] = m1_set::bc<T>[0] * z[41];
z[42] = abb[4] + abb[6];
z[43] = abb[13] + z[42];
z[44] = abb[7] * (T(1) / T(2));
z[45] = -abb[11] + (T(1) / T(2)) * z[43] + -z[44];
z[45] = abb[38] * z[45];
z[46] = -abb[7] + z[0];
z[47] = abb[37] * (T(1) / T(2));
z[48] = z[46] * z[47];
z[49] = abb[11] * abb[36];
z[45] = z[45] + z[48] + z[49];
z[33] = abb[17] + z[33];
z[48] = -abb[15] + z[33];
z[49] = abb[39] * (T(1) / T(2));
z[50] = -z[48] * z[49];
z[51] = -abb[12] + z[42];
z[52] = -abb[36] * z[51];
z[53] = abb[13] * abb[36];
z[50] = -abb[41] + z[50] + -z[52] + z[53];
z[50] = -z[41] + -z[45] + (T(1) / T(2)) * z[50];
z[54] = z[1] + z[8];
z[55] = -abb[31] + -z[54];
z[19] = abb[26] + -z[19] + (T(1) / T(2)) * z[55];
z[19] = m1_set::bc<T>[0] * z[19];
z[19] = z[19] + z[47];
z[47] = abb[5] * (T(1) / T(2));
z[19] = z[19] * z[47];
z[55] = -abb[37] + abb[38];
z[56] = -abb[27] + abb[30] + z[26];
z[56] = m1_set::bc<T>[0] * z[56];
z[56] = -z[55] + z[56];
z[57] = abb[1] * (T(1) / T(2));
z[56] = z[56] * z[57];
z[19] = z[19] + (T(1) / T(2)) * z[50] + z[56];
z[19] = abb[52] * z[19];
z[33] = abb[15] + z[33];
z[50] = z[33] * z[49];
z[50] = z[50] + z[52];
z[46] = -abb[37] * z[46];
z[52] = abb[7] + abb[13] + -z[42];
z[52] = abb[38] * z[52];
z[46] = z[46] + -z[50] + z[52] + -z[53];
z[52] = -z[8] + z[39];
z[56] = z[18] + z[52];
z[56] = m1_set::bc<T>[0] * z[56];
z[58] = -abb[37] + z[56];
z[58] = z[47] * z[58];
z[41] = -z[41] + (T(1) / T(2)) * z[46] + z[58];
z[46] = -abb[30] + z[21];
z[59] = z[18] + -z[26] + z[46];
z[59] = m1_set::bc<T>[0] * z[59];
z[55] = (T(1) / T(2)) * z[55] + z[59];
z[59] = abb[3] * (T(1) / T(2));
z[60] = z[55] * z[59];
z[40] = m1_set::bc<T>[0] * z[40];
z[61] = abb[37] + abb[38];
z[40] = -z[40] + (T(1) / T(2)) * z[61];
z[61] = abb[1] * z[40];
z[62] = -abb[29] + abb[31];
z[63] = abb[26] + z[62];
z[64] = -abb[27] + z[63];
z[65] = m1_set::bc<T>[0] * (T(1) / T(2));
z[64] = z[64] * z[65];
z[64] = abb[37] + z[64];
z[66] = abb[10] * z[64];
z[60] = z[60] + -z[61] + z[66];
z[61] = abb[0] * (T(1) / T(2));
z[63] = -abb[30] + z[63];
z[63] = m1_set::bc<T>[0] * z[63];
z[63] = abb[36] + abb[37] + z[63];
z[63] = z[61] * z[63];
z[41] = (T(1) / T(2)) * z[41] + z[60] + z[63];
z[41] = abb[50] * z[41];
z[63] = abb[26] + -z[1] + -z[18];
z[63] = m1_set::bc<T>[0] * z[63];
z[63] = abb[37] + z[63];
z[63] = abb[5] * z[63];
z[49] = abb[15] * z[49];
z[37] = -m1_set::bc<T>[0] * z[37];
z[37] = z[37] + z[49] + z[63];
z[49] = z[18] + z[39];
z[49] = m1_set::bc<T>[0] * z[49];
z[49] = -abb[37] + z[49];
z[49] = z[49] * z[61];
z[63] = abb[1] * z[64];
z[37] = (T(1) / T(2)) * z[37] + z[49] + z[63] + -z[66];
z[37] = abb[51] * z[37];
z[35] = m1_set::bc<T>[0] * z[35];
z[49] = z[50] + -z[53];
z[35] = -z[35] + z[45] + (T(1) / T(2)) * z[49] + -z[58];
z[40] = abb[0] * z[40];
z[35] = (T(1) / T(2)) * z[35] + -z[40] + -z[60];
z[40] = -abb[49] * z[35];
z[45] = abb[52] * z[55];
z[9] = m1_set::bc<T>[0] * z[9];
z[9] = z[9] + z[45];
z[9] = z[9] * z[59];
z[45] = -abb[48] * z[56];
z[49] = -abb[30] + z[18] + -z[39];
z[49] = m1_set::bc<T>[0] * z[49];
z[49] = abb[38] + z[49];
z[49] = abb[52] * z[49];
z[50] = abb[37] * abb[48];
z[45] = z[45] + z[49] + z[50];
z[45] = z[45] * z[61];
z[49] = -abb[27] + z[62];
z[49] = abb[48] * z[49];
z[49] = z[12] + z[49];
z[49] = z[49] * z[65];
z[49] = z[49] + z[50];
z[49] = abb[10] * z[49];
z[7] = z[7] + z[9] + (T(1) / T(2)) * z[17] + z[19] + z[37] + z[40] + z[41] + z[45] + z[49];
z[9] = abb[47] * (T(1) / T(8));
z[17] = -z[9] * z[35];
z[7] = abb[57] + (T(1) / T(8)) * z[7] + z[17];
z[17] = -abb[29] + abb[31] * (T(3) / T(2));
z[19] = abb[26] * (T(1) / T(4)) + z[22];
z[35] = -z[17] + z[19] + z[21];
z[35] = abb[26] * z[35];
z[37] = abb[30] * (T(1) / T(4));
z[17] = -z[17] + z[37];
z[17] = abb[30] * z[17];
z[40] = -abb[29] + abb[30] + abb[28] * (T(-3) / T(2));
z[40] = z[21] * z[40];
z[41] = -abb[26] + z[8];
z[45] = -abb[29] + 3 * abb[31];
z[45] = z[41] + (T(1) / T(2)) * z[45];
z[45] = abb[27] * z[45];
z[49] = abb[35] + -abb[55];
z[50] = abb[33] * (T(1) / T(2));
z[53] = abb[54] * (T(1) / T(2)) + -z[50];
z[55] = abb[34] * (T(1) / T(2));
z[56] = -abb[29] + abb[31] * (T(5) / T(4));
z[56] = abb[31] * z[56];
z[17] = z[17] + z[35] + z[40] + z[45] + (T(1) / T(2)) * z[49] + z[53] + -z[55] + z[56];
z[17] = abb[52] * z[17];
z[35] = -abb[29] + abb[31] * (T(1) / T(2));
z[40] = abb[31] * z[35];
z[45] = -abb[29] + z[22];
z[45] = abb[30] * z[45];
z[40] = z[40] + -z[45];
z[45] = abb[29] + z[46];
z[56] = abb[28] * z[45];
z[56] = z[40] + -z[56];
z[58] = -abb[34] + -z[56];
z[60] = abb[35] + z[58];
z[63] = prod_pow(m1_set::bc<T>[0], 2);
z[64] = (T(1) / T(6)) * z[63];
z[65] = -abb[33] + -z[60] + -z[64];
z[65] = abb[48] * z[65];
z[66] = abb[29] + z[8];
z[67] = -z[23] + z[66];
z[67] = z[12] * z[67];
z[17] = z[17] + z[65] + z[67];
z[17] = abb[3] * z[17];
z[30] = abb[27] * z[30];
z[6] = -z[6] + z[25];
z[65] = -abb[30] + abb[31];
z[65] = z[6] * z[65];
z[67] = abb[32] + -abb[33];
z[68] = abb[53] + -abb[55];
z[69] = -z[67] + -z[68];
z[69] = z[4] * z[69];
z[27] = z[25] + -z[27];
z[27] = abb[28] * z[27];
z[6] = -z[6] + -z[28];
z[6] = abb[26] * z[6];
z[6] = z[6] + z[27] + z[30] + z[65] + z[69];
z[6] = abb[24] * z[6];
z[1] = -z[1] * z[11];
z[27] = z[11] + z[14];
z[30] = -abb[4] + abb[14];
z[30] = abb[48] * z[30];
z[69] = z[27] + -z[30];
z[69] = z[23] * z[69];
z[1] = z[1] + z[3] + -z[13] + z[69];
z[1] = abb[27] * z[1];
z[13] = z[4] * z[23];
z[5] = -z[5] + z[13] + -z[28];
z[5] = abb[26] * z[5];
z[13] = -abb[34] + abb[54] + -z[68];
z[13] = z[4] * z[13];
z[28] = -z[25] + z[29];
z[28] = z[18] * z[28];
z[25] = abb[28] * z[25];
z[5] = z[5] + z[13] + z[25] + z[28] + z[65];
z[5] = abb[19] * z[5];
z[13] = z[23] + z[39];
z[25] = abb[26] * z[13];
z[28] = abb[30] * z[62];
z[29] = abb[31] * z[62];
z[28] = z[28] + -z[29];
z[39] = abb[55] + z[28];
z[68] = -z[39] + z[67];
z[52] = -z[23] + -z[52];
z[52] = abb[27] * z[52];
z[69] = (T(1) / T(3)) * z[63];
z[8] = -abb[28] * z[8];
z[8] = -abb[34] + z[8] + z[25] + z[52] + z[68] + -z[69];
z[8] = abb[52] * z[8];
z[52] = (T(1) / T(2)) * z[63];
z[60] = -abb[54] + -z[52] + -z[60] + z[67];
z[70] = abb[48] * z[60];
z[71] = z[12] * z[54];
z[72] = abb[29] * abb[48];
z[72] = -z[12] + z[72];
z[72] = z[18] * z[72];
z[8] = z[8] + z[70] + z[71] + z[72];
z[8] = abb[0] * z[8];
z[70] = -z[11] * z[54];
z[30] = -z[14] + z[30];
z[30] = z[23] * z[30];
z[3] = -z[3] + z[30] + z[70];
z[3] = abb[26] * z[3];
z[30] = abb[33] + abb[35];
z[56] = -abb[34] + z[30] + -z[56];
z[56] = z[11] * z[56];
z[70] = abb[28] * z[66];
z[28] = -abb[32] + z[28] + z[30] + z[70];
z[28] = abb[48] * z[28];
z[71] = -abb[28] + z[62];
z[12] = z[12] * z[71];
z[12] = z[12] + z[28];
z[12] = abb[13] * z[12];
z[28] = abb[54] + abb[55];
z[4] = z[4] * z[28];
z[4] = z[4] + z[65];
z[4] = abb[21] * z[4];
z[11] = z[11] + z[24];
z[14] = -z[14] + z[16];
z[10] = (T(5) / T(6)) * z[10] + (T(1) / T(2)) * z[11] + (T(1) / T(3)) * z[14];
z[10] = z[10] * z[63];
z[11] = abb[54] * z[0];
z[14] = -abb[4] * abb[35];
z[14] = abb[58] * (T(-1) / T(2)) + z[11] + z[14];
z[14] = abb[48] * z[14];
z[15] = -abb[54] * z[15];
z[16] = abb[14] * abb[48];
z[16] = z[16] + -z[27];
z[16] = abb[32] * z[16];
z[1] = z[1] + z[3] + z[4] + z[5] + z[6] + z[8] + z[10] + z[12] + z[14] + z[15] + z[16] + z[17] + z[56];
z[3] = z[19] + -z[21] + z[35];
z[3] = abb[26] * z[3];
z[4] = z[35] + z[37];
z[4] = abb[30] * z[4];
z[5] = z[21] * z[45];
z[6] = z[18] * z[62];
z[8] = abb[35] + abb[55];
z[10] = -abb[29] + abb[31] * (T(3) / T(4));
z[10] = abb[31] * z[10];
z[3] = z[3] + z[4] + z[5] + -z[6] + (T(1) / T(2)) * z[8] + -z[10] + -z[53] + -z[55] + z[64];
z[3] = z[3] * z[59];
z[4] = z[23] * z[62];
z[5] = abb[54] + z[69];
z[4] = -z[4] + z[5] + z[6];
z[6] = abb[10] * z[4];
z[8] = abb[54] + z[39];
z[10] = (T(5) / T(6)) * z[63];
z[12] = z[8] + z[10];
z[14] = z[12] * z[57];
z[15] = abb[56] * z[33];
z[3] = z[3] + z[6] + -z[14] + (T(-1) / T(8)) * z[15];
z[14] = z[23] * z[38];
z[15] = abb[31] * z[0];
z[2] = -z[2] + z[15];
z[15] = abb[30] * z[36];
z[14] = z[2] + -z[14] + z[15];
z[14] = abb[26] * z[14];
z[15] = abb[2] + (T(1) / T(2)) * z[42];
z[15] = abb[28] * z[15];
z[16] = abb[6] * abb[30];
z[17] = abb[2] * abb[30];
z[15] = z[15] + -z[16] + -z[17];
z[15] = abb[28] * z[15];
z[16] = z[42] * z[49];
z[19] = -abb[31] + z[22];
z[19] = abb[30] * z[19];
z[24] = prod_pow(abb[31], 2);
z[19] = z[19] + (T(1) / T(2)) * z[24];
z[24] = z[19] * z[36];
z[27] = abb[33] + abb[34];
z[27] = abb[2] * z[27];
z[28] = abb[6] * abb[33];
z[33] = abb[4] * abb[34];
z[35] = abb[32] * z[38];
z[11] = -z[11] + z[14] + z[15] + z[16] + z[24] + z[27] + z[28] + z[33] + -z[35];
z[14] = abb[13] + -z[51];
z[14] = abb[53] * z[14];
z[14] = z[11] + -z[14];
z[0] = z[0] * z[23];
z[15] = abb[12] * (T(1) / T(2));
z[16] = abb[26] + -abb[29];
z[24] = z[15] * z[16];
z[27] = abb[2] + abb[4];
z[27] = abb[28] * z[27];
z[28] = abb[4] * abb[30];
z[0] = -z[0] + z[2] + -z[17] + -z[24] + z[27] + -z[28] + z[31];
z[0] = z[0] * z[18];
z[2] = z[8] * z[44];
z[0] = z[0] + -z[2];
z[2] = z[22] * z[62];
z[8] = z[21] * z[66];
z[17] = z[23] * z[71];
z[21] = abb[32] + abb[55] + z[29];
z[2] = abb[35] + z[2] + z[8] + z[17] + (T(-1) / T(2)) * z[21] + z[50];
z[2] = abb[13] * z[2];
z[8] = -z[23] + z[34];
z[8] = abb[26] * z[8];
z[17] = abb[28] * abb[29];
z[8] = abb[34] + z[8] + -z[17];
z[15] = z[8] * z[15];
z[2] = z[0] + z[2] + (T(-1) / T(2)) * z[14] + z[15];
z[14] = abb[26] * z[54];
z[15] = z[16] * z[18];
z[14] = z[14] + -z[15] + z[60];
z[17] = abb[5] * z[14];
z[21] = -abb[30] + z[23];
z[21] = abb[26] * z[21];
z[22] = abb[28] * z[46];
z[21] = z[21] + -z[22];
z[10] = -abb[35] + -abb[53] + -abb[54] + -z[10] + z[21] + z[40] + z[67];
z[10] = z[10] * z[61];
z[19] = z[19] + -z[21] + z[30];
z[19] = -abb[32] + (T(1) / T(2)) * z[19];
z[19] = abb[8] * z[19];
z[21] = abb[12] + abb[14];
z[21] = abb[7] * (T(-5) / T(6)) + abb[4] * (T(1) / T(2)) + abb[2] * (T(1) / T(6)) + (T(1) / T(3)) * z[21];
z[22] = z[21] * z[63];
z[19] = z[19] + (T(1) / T(4)) * z[22];
z[2] = (T(1) / T(2)) * z[2] + -z[3] + z[10] + (T(-1) / T(4)) * z[17] + z[19];
z[2] = abb[50] * z[2];
z[10] = z[16] * z[23];
z[10] = abb[32] + -z[5] + z[10] + -z[15];
z[10] = z[10] * z[47];
z[13] = -abb[27] * z[13];
z[5] = abb[32] + z[5] + z[13] + z[25];
z[5] = z[5] * z[61];
z[4] = -abb[1] * z[4];
z[13] = -z[18] + z[23];
z[16] = -z[13] * z[26];
z[16] = -abb[32] + z[16];
z[16] = abb[8] * z[16];
z[17] = abb[15] * abb[56];
z[4] = z[4] + z[5] + z[6] + z[10] + z[16] + (T(-1) / T(4)) * z[17];
z[4] = abb[51] * z[4];
z[5] = z[13] * z[20];
z[10] = -abb[32] * abb[48];
z[5] = z[5] + z[10];
z[5] = abb[8] * z[5];
z[6] = -abb[48] * z[6];
z[2] = z[2] + z[4] + z[5] + z[6];
z[4] = abb[26] * z[71];
z[4] = z[4] + -z[68] + z[70];
z[4] = abb[13] * z[4];
z[5] = abb[12] * z[8];
z[4] = z[4] + z[5] + -z[11];
z[5] = -abb[12] + z[43];
z[5] = -abb[11] + (T(1) / T(2)) * z[5];
z[5] = abb[53] * z[5];
z[6] = abb[11] * z[49];
z[0] = -z[0] + z[5] + -z[6];
z[5] = z[14] * z[47];
z[6] = z[21] * z[52];
z[5] = -z[0] + (T(1) / T(2)) * z[4] + -z[5] + z[6];
z[6] = z[12] * z[61];
z[3] = -z[3] + (T(1) / T(2)) * z[5] + -z[6];
z[5] = abb[49] * (T(1) / T(8)) + z[9];
z[3] = z[3] * z[5];
z[4] = abb[58] + z[4];
z[0] = -z[0] + (T(1) / T(2)) * z[4];
z[0] = (T(1) / T(2)) * z[0] + z[19];
z[4] = abb[29] * (T(-3) / T(2)) + -z[41];
z[4] = abb[26] * z[4];
z[4] = 3 * abb[35] + -abb[54] + z[4] + -z[15] + z[58] + -z[64] + -z[67];
z[4] = abb[5] * z[4];
z[5] = abb[26] + -abb[27];
z[5] = z[5] * z[62];
z[5] = -abb[54] + z[5] + z[39] + z[64];
z[5] = abb[1] * z[5];
z[0] = (T(1) / T(8)) * z[0] + (T(1) / T(32)) * z[4] + (T(1) / T(16)) * z[5];
z[0] = abb[52] * z[0];
z[4] = abb[52] * z[48];
z[4] = (T(1) / T(2)) * z[4] + -z[32];
z[4] = abb[56] * z[4];
z[0] = abb[40] + z[0] + (T(1) / T(16)) * z[1] + (T(1) / T(8)) * z[2] + z[3] + (T(1) / T(32)) * z[4];
return {z[7], z[0]};
}


template <typename T> std::complex<T> f_4_454_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.24113092074640388332875526100108878623837288398082771037094547575"),stof<T>("0.01624583897832471480970255068419892981349816475670169103204217271")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.47545341071950454568343583010671366567649273593187729114674517307"),stof<T>("0.40687630736128679929112184295536286802749357323732339648371288076")}, std::complex<T>{stof<T>("0.42417916664465846209173721767453931134663348831501127174786619582"),stof<T>("0.27200154240207612778771479084562495971707996417798871991158691521")}, std::complex<T>{stof<T>("0.47545341071950454568343583010671366567649273593187729114674517307"),stof<T>("0.40687630736128679929112184295536286802749357323732339648371288076")}, std::complex<T>{stof<T>("0.48195449955120231746018184934737231775710566043504348432322095709"),stof<T>("0.25824662055587686301639274133557152984171442304486007510819287997")}, std::complex<T>{stof<T>("0.01119107242493583744221878775161478282355945045960599580395625581"),stof<T>("-0.14588860021417846800102478009988761746534936739225054495129151965")}, std::complex<T>{stof<T>("0.25882026428478713445743298464361446146557975334912340494472285905"),stof<T>("0.2609877071471083312900970628554752505621442058450728515324213611")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_454_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_454_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_454_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(512)) * (v[2] + v[3]) * (2 * (8 * v[0] + 4 * v[1] + 7 * v[2] + 3 * v[3] + -4 * v[4]) + -m1_set::bc<T>[2] * (24 + 8 * v[0] + 4 * v[1] + v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5]))) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[2] * (T(-3) / T(64)) * (v[2] + v[3])) / tend;


		return (abb[48] + abb[50] + abb[52]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[48] + -abb[50] + -abb[52];
z[1] = -abb[32] + abb[35];
return abb[9] * (T(1) / T(16)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_454_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.48248148640221261076385217222013159313429013656826037603043918887"),stof<T>("0.00681280026213762734428781509628845927330893600981912544301744637")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,59> abb = {dl[0], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W19(k,dl), dlog_W23(k,dl), dlog_W26(k,dl), dlog_W28(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_4(k), f_1_7(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_16(k), f_2_19(k), f_2_4_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_4_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k), T{0}, T{0}};
abb[40] = SpDLog_f_4_454_W_19_Im(t, path, abb);
abb[41] = SpDLogQ_W_78(k,dl,dlr).imag();
abb[57] = SpDLog_f_4_454_W_19_Re(t, path, abb);
abb[58] = SpDLogQ_W_78(k,dl,dlr).real();

                    
            return f_4_454_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_454_DLogXconstant_part(base_point<T>, kend);
	value += f_4_454_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_454_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_454_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_454_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_454_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_454_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_454_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
