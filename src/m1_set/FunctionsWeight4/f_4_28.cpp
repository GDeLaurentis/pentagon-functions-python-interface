/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_28.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_28_abbreviated (const std::array<T,53>& abb) {
T z[83];
z[0] = -abb[40] + abb[46];
z[1] = abb[44] * (T(1) / T(2));
z[2] = abb[48] * (T(1) / T(2));
z[3] = abb[42] * (T(1) / T(2));
z[4] = z[0] + -z[1] + z[2] + -z[3];
z[5] = -abb[14] * z[4];
z[6] = abb[3] * z[4];
z[7] = -abb[47] + z[1] + z[2] + -z[3];
z[8] = abb[13] * z[7];
z[9] = abb[5] * z[7];
z[10] = -abb[42] + z[0];
z[11] = -abb[47] + abb[48] + z[10];
z[11] = abb[1] * z[11];
z[6] = z[5] + z[6] + -z[8] + z[9] + z[11];
z[6] = abb[32] * z[6];
z[12] = z[2] + z[3];
z[13] = abb[43] + abb[45];
z[14] = z[12] + z[13];
z[15] = abb[44] * (T(3) / T(2));
z[16] = -z[14] + z[15];
z[17] = abb[8] * z[16];
z[18] = abb[6] * z[16];
z[19] = z[17] + z[18];
z[20] = abb[7] * z[16];
z[21] = -z[19] + z[20];
z[22] = -abb[44] + z[13];
z[10] = z[10] + -z[22];
z[10] = abb[10] * z[10];
z[23] = 2 * abb[42];
z[24] = -abb[48] + z[13];
z[25] = z[23] + z[24];
z[26] = 3 * z[0];
z[27] = -z[25] + z[26];
z[28] = abb[1] * z[27];
z[29] = z[1] + -z[13];
z[30] = abb[42] * (T(3) / T(2)) + -z[2];
z[31] = -z[29] + z[30];
z[32] = 2 * z[0];
z[33] = -z[31] + z[32];
z[33] = abb[3] * z[33];
z[33] = z[5] + -z[10] + -z[21] + z[28] + z[33];
z[33] = abb[33] * z[33];
z[34] = z[5] + -z[9] + z[20];
z[35] = abb[47] + 5 * z[0];
z[35] = -z[13] + -z[30] + (T(1) / T(2)) * z[35];
z[35] = abb[1] * z[35];
z[36] = -abb[47] + z[13];
z[37] = -abb[44] + abb[48];
z[38] = z[36] + z[37];
z[39] = abb[13] * z[38];
z[39] = (T(1) / T(2)) * z[39];
z[26] = -z[24] + z[26];
z[26] = -abb[42] + (T(1) / T(2)) * z[26];
z[26] = abb[3] * z[26];
z[26] = -z[10] + z[26] + (T(1) / T(2)) * z[34] + z[35] + z[39];
z[26] = abb[28] * z[26];
z[34] = abb[48] * (T(3) / T(2));
z[29] = -2 * abb[47] + -z[3] + -z[29] + z[34];
z[29] = abb[13] * z[29];
z[35] = abb[12] * z[38];
z[29] = -z[9] + z[29] + -z[35];
z[21] = z[21] + z[29];
z[40] = -abb[27] * z[21];
z[26] = z[26] + z[40];
z[26] = abb[28] * z[26];
z[7] = abb[6] * z[7];
z[37] = -abb[42] + z[37];
z[40] = (T(1) / T(2)) * z[37];
z[41] = abb[3] * z[40];
z[41] = z[9] + z[41];
z[7] = z[7] + -z[8] + z[41];
z[8] = -abb[34] * z[7];
z[38] = abb[6] * z[38];
z[24] = 3 * abb[41] + -abb[42] + z[24];
z[42] = abb[8] * z[24];
z[22] = -abb[42] + -z[22];
z[22] = abb[3] * z[22];
z[22] = -z[9] + z[22] + z[38] + z[42];
z[38] = -z[1] + z[12];
z[43] = -abb[41] + z[38];
z[44] = abb[15] * z[43];
z[45] = (T(1) / T(2)) * z[44];
z[46] = -abb[41] + abb[42];
z[47] = abb[11] * z[46];
z[24] = abb[2] * z[24];
z[22] = (T(1) / T(2)) * z[22] + (T(3) / T(2)) * z[24] + -z[39] + z[45] + z[47];
z[39] = prod_pow(abb[29], 2);
z[22] = z[22] * z[39];
z[48] = abb[4] * z[40];
z[7] = z[7] + -z[48];
z[7] = abb[29] * z[7];
z[38] = z[36] + z[38];
z[38] = abb[6] * z[38];
z[38] = -z[17] + z[38] + -z[41];
z[41] = 3 * abb[47];
z[49] = -abb[45] + z[41];
z[50] = abb[43] * (T(1) / T(2));
z[49] = abb[48] + -z[3] + (T(-1) / T(2)) * z[49] + z[50];
z[49] = abb[13] * z[49];
z[38] = -z[35] + (T(1) / T(2)) * z[38] + z[49];
z[38] = abb[27] * z[38];
z[7] = z[7] + z[38];
z[7] = abb[27] * z[7];
z[38] = abb[16] + abb[18];
z[4] = -z[4] * z[38];
z[38] = abb[19] * z[43];
z[51] = -abb[46] + z[1];
z[12] = -abb[41] + z[12] + z[51];
z[12] = abb[17] * z[12];
z[4] = z[4] + z[12] + z[38];
z[12] = abb[36] * (T(1) / T(2));
z[4] = z[4] * z[12];
z[38] = abb[8] * z[43];
z[38] = -z[24] + z[38] + -z[44];
z[29] = z[29] + z[38];
z[43] = abb[50] * z[29];
z[52] = -abb[51] * z[21];
z[31] = abb[3] * z[31];
z[53] = abb[6] * z[40];
z[31] = z[31] + z[38] + z[48] + -z[53];
z[38] = abb[52] * z[31];
z[40] = z[39] * z[40];
z[48] = abb[34] * z[37];
z[40] = z[40] + z[48];
z[40] = abb[4] * z[40];
z[4] = z[4] + z[6] + z[7] + z[8] + z[22] + z[26] + z[33] + z[38] + (T(1) / T(2)) * z[40] + z[43] + z[52];
z[6] = abb[22] * abb[27];
z[7] = abb[21] + abb[23];
z[8] = abb[20] + z[7];
z[22] = -abb[24] + z[8];
z[26] = abb[29] * z[22];
z[6] = z[6] + z[26];
z[33] = abb[21] + abb[22];
z[38] = abb[28] * z[33];
z[22] = abb[22] + z[22];
z[40] = (T(1) / T(2)) * z[22];
z[43] = abb[30] * z[40];
z[43] = -z[6] + z[38] + z[43];
z[43] = abb[30] * z[43];
z[48] = abb[24] * (T(1) / T(2));
z[52] = z[8] + -z[48];
z[52] = z[39] * z[52];
z[53] = abb[22] + z[8];
z[54] = abb[27] * z[53];
z[55] = -abb[29] * z[8];
z[55] = (T(1) / T(2)) * z[54] + z[55];
z[55] = abb[27] * z[55];
z[56] = abb[23] * (T(1) / T(2));
z[57] = z[33] + z[56];
z[57] = abb[28] * z[57];
z[58] = abb[22] + z[7];
z[59] = -abb[27] * z[58];
z[57] = z[57] + z[59];
z[57] = abb[28] * z[57];
z[8] = abb[34] * z[8];
z[59] = abb[20] + abb[23];
z[60] = -abb[32] * z[59];
z[61] = abb[22] + abb[24];
z[62] = abb[50] * z[61];
z[33] = abb[20] + z[33];
z[63] = abb[33] * z[33];
z[64] = -abb[51] * z[58];
z[65] = -abb[52] * z[22];
z[53] = abb[35] * z[53];
z[12] = -abb[25] * z[12];
z[8] = z[8] + z[12] + z[43] + z[52] + z[53] + z[55] + z[57] + z[60] + z[62] + z[63] + z[64] + z[65];
z[12] = prod_pow(m1_set::bc<T>[0], 2);
z[43] = -z[12] * z[40];
z[8] = 3 * z[8] + z[43];
z[8] = abb[49] * z[8];
z[43] = abb[42] * (T(23) / T(12)) + (T(4) / T(3)) * z[13];
z[52] = abb[48] * (T(1) / T(4));
z[51] = abb[40] * (T(-1) / T(3)) + -z[51];
z[51] = abb[41] * (T(1) / T(3)) + -z[43] + (T(13) / T(2)) * z[51] + z[52];
z[51] = abb[8] * z[51];
z[53] = abb[40] + abb[41];
z[53] = -z[1] + (T(13) / T(3)) * z[53];
z[53] = abb[42] * (T(-43) / T(12)) + (T(-5) / T(3)) * z[13] + -z[52] + (T(1) / T(2)) * z[53];
z[53] = abb[3] * z[53];
z[55] = abb[42] + abb[48];
z[0] = abb[47] + z[0];
z[57] = (T(1) / T(3)) * z[13];
z[60] = (T(-1) / T(2)) * z[0] + (T(1) / T(6)) * z[55] + z[57];
z[60] = abb[1] * z[60];
z[35] = 3 * z[35];
z[1] = abb[47] + z[1];
z[43] = abb[48] * (T(7) / T(12)) + (T(1) / T(2)) * z[1] + -z[43];
z[43] = abb[6] * z[43];
z[57] = abb[41] + (T(-1) / T(3)) * z[55] + z[57];
z[57] = abb[2] * z[57];
z[62] = 3 * z[47];
z[63] = -abb[41] + abb[48];
z[64] = -abb[47] + z[63];
z[64] = abb[9] * z[64];
z[65] = 3 * z[64];
z[37] = abb[4] * z[37];
z[63] = -abb[40] + z[63];
z[66] = abb[0] * z[63];
z[43] = -z[35] + (T(1) / T(4)) * z[37] + z[43] + -z[45] + z[51] + z[53] + z[57] + 13 * z[60] + z[62] + z[65] + (T(-13) / T(6)) * z[66];
z[12] = z[12] * z[43];
z[43] = (T(3) / T(2)) * z[5];
z[9] = 3 * z[9];
z[45] = z[9] + -z[18];
z[50] = abb[45] * (T(1) / T(2)) + z[50];
z[51] = z[2] + z[50];
z[53] = abb[41] * (T(1) / T(2));
z[57] = -abb[40] + -z[3] + z[51] + z[53];
z[57] = abb[8] * z[57];
z[60] = 3 * abb[46];
z[67] = -abb[40] + z[60];
z[46] = -z[46] + -z[51] + (T(1) / T(2)) * z[67];
z[46] = abb[3] * z[46];
z[51] = (T(3) / T(2)) * z[44];
z[68] = (T(1) / T(2)) * z[24];
z[69] = z[51] + z[68];
z[0] = (T(3) / T(2)) * z[0] + -z[14];
z[0] = abb[1] * z[0];
z[14] = abb[49] * (T(3) / T(4));
z[70] = abb[21] + -abb[24];
z[70] = z[14] * z[70];
z[71] = 2 * z[66];
z[0] = z[0] + z[43] + (T(1) / T(2)) * z[45] + z[46] + z[49] + z[57] + -z[65] + z[69] + z[70] + z[71];
z[0] = abb[26] * z[0];
z[45] = z[28] + z[43];
z[46] = -z[15] + z[67];
z[49] = abb[42] * (T(3) / T(4));
z[46] = -abb[41] + (T(-1) / T(2)) * z[46] + z[49] + z[52];
z[46] = abb[8] * z[46];
z[57] = abb[44] * (T(3) / T(4));
z[70] = z[53] + z[57];
z[72] = 2 * abb[40];
z[73] = z[70] + z[72];
z[74] = abb[46] * (T(3) / T(2));
z[49] = abb[48] * (T(5) / T(4)) + -z[49] + -z[73] + z[74];
z[49] = abb[3] * z[49];
z[75] = (T(1) / T(2)) * z[66];
z[51] = -z[51] + z[75];
z[76] = abb[13] * z[16];
z[46] = -z[24] + z[45] + z[46] + z[49] + z[51] + -z[76];
z[49] = -abb[30] * z[46];
z[59] = -z[59] + z[61];
z[77] = z[14] * z[59];
z[46] = z[46] + z[77];
z[46] = abb[31] * z[46];
z[44] = 3 * z[44];
z[77] = z[44] + z[76];
z[16] = abb[3] * z[16];
z[42] = z[16] + -z[18] + 3 * z[24] + z[42] + z[77];
z[42] = abb[29] * z[42];
z[78] = z[19] + z[35];
z[41] = -abb[42] + 2 * abb[48] + z[13] + -z[41];
z[41] = 2 * z[41];
z[79] = abb[13] * z[41];
z[79] = z[9] + -z[16] + z[78] + -z[79];
z[80] = -abb[27] * z[79];
z[5] = 3 * z[5];
z[81] = 2 * z[13];
z[1] = abb[48] * (T(-5) / T(2)) + 3 * z[1] + z[3] + -z[81];
z[1] = abb[13] * z[1];
z[82] = -abb[3] * z[27];
z[1] = z[1] + -z[5] + -3 * z[11] + z[78] + z[82];
z[1] = abb[28] * z[1];
z[11] = abb[30] * z[59];
z[11] = (T(-1) / T(2)) * z[11] + -z[26] + -z[38] + z[54];
z[54] = abb[49] * (T(3) / T(2));
z[11] = z[11] * z[54];
z[0] = z[0] + z[1] + z[11] + -z[42] + z[46] + z[49] + z[80];
z[0] = abb[26] * z[0];
z[1] = abb[42] * (T(5) / T(4)) + z[13];
z[11] = z[1] + -z[52] + -z[57];
z[11] = abb[6] * z[11];
z[46] = abb[48] * (T(3) / T(4));
z[49] = z[1] + -z[46];
z[52] = z[49] + -z[60] + z[73];
z[52] = abb[8] * z[52];
z[57] = z[57] + -z[72];
z[50] = -2 * abb[41] + abb[42] * (T(7) / T(4)) + z[46] + z[50] + z[57];
z[50] = abb[3] * z[50];
z[11] = z[11] + z[28] + (T(-3) / T(4)) * z[37] + z[50] + z[52] + -z[62] + -z[66] + z[69];
z[11] = abb[30] * z[11];
z[50] = 2 * abb[3];
z[27] = z[27] * z[50];
z[27] = z[27] + -z[76];
z[52] = 3 * z[10];
z[5] = z[5] + z[19] + z[27] + 3 * z[28] + -z[52];
z[5] = abb[28] * z[5];
z[5] = z[5] + -z[42];
z[19] = abb[6] + z[50];
z[19] = z[19] * z[25];
z[25] = (T(3) / T(2)) * z[37];
z[17] = -z[17] + z[19] + z[25] + z[76];
z[17] = abb[27] * z[17];
z[11] = z[5] + z[11] + z[17];
z[11] = abb[30] * z[11];
z[19] = 4 * abb[40];
z[37] = abb[46] * (T(-9) / T(2)) + abb[42] * (T(13) / T(4)) + z[19] + -z[46] + -z[70] + z[81];
z[37] = abb[3] * z[37];
z[59] = -z[15] + -z[67];
z[1] = -abb[41] + z[1] + z[46] + (T(1) / T(2)) * z[59];
z[1] = abb[8] * z[1];
z[28] = z[24] + 2 * z[28];
z[1] = z[1] + -z[18] + -z[28] + z[37] + -z[43] + z[51] + z[52];
z[1] = abb[30] * z[1];
z[37] = -abb[41] + z[34];
z[3] = abb[44] * (T(9) / T(2)) + -z[3] + -z[13] + -z[37] + -z[67];
z[3] = abb[8] * z[3];
z[3] = z[3] + z[18];
z[43] = -z[49] + -z[53] + z[57] + z[74];
z[43] = abb[3] * z[43];
z[14] = z[14] * z[33];
z[3] = (T(1) / T(2)) * z[3] + z[14] + (T(-3) / T(2)) * z[20] + z[43] + z[45] + z[68] + z[75];
z[3] = abb[31] * z[3];
z[14] = 3 * abb[44];
z[33] = -z[14] + z[55] + z[81];
z[43] = abb[6] + abb[8];
z[33] = z[33] * z[43];
z[20] = 3 * z[20];
z[16] = -z[16] + z[20] + z[33] + -z[76];
z[16] = abb[27] * z[16];
z[33] = abb[20] + abb[22];
z[33] = -abb[21] + (T(-1) / T(2)) * z[33] + z[48] + -z[56];
z[33] = abb[30] * z[33];
z[43] = -abb[20] * abb[27];
z[26] = z[26] + z[33] + -z[38] + z[43];
z[26] = z[26] * z[54];
z[1] = z[1] + z[3] + -z[5] + z[16] + z[26];
z[1] = abb[31] * z[1];
z[3] = -abb[35] * z[79];
z[5] = z[39] * z[65];
z[0] = z[0] + z[1] + z[3] + 3 * z[4] + z[5] + (T(1) / T(2)) * z[8] + z[11] + z[12];
z[1] = -abb[41] + -z[15] + z[30] + z[72];
z[1] = abb[8] * z[1];
z[3] = z[50] * z[63];
z[4] = abb[1] * z[41];
z[1] = z[1] + z[3] + z[4] + -z[9] + -z[24] + -z[35] + 6 * z[64] + -4 * z[66] + -z[77];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = m1_set::bc<T>[0] * z[54];
z[4] = z[3] * z[61];
z[1] = z[1] + z[4];
z[1] = abb[26] * z[1];
z[4] = 6 * z[10] + -z[20];
z[5] = abb[42] + -z[32] + z[36];
z[5] = abb[1] * z[5];
z[5] = z[4] + 6 * z[5] + z[9] + -z[27] + -z[78];
z[5] = abb[28] * z[5];
z[8] = -z[60] + z[72];
z[9] = abb[42] * (T(5) / T(2)) + z[81];
z[10] = -2 * z[8] + -z[9] + -z[15] + z[37];
z[10] = abb[8] * z[10];
z[11] = 4 * abb[41] + abb[42] * (T(-7) / T(2)) + -z[13] + -z[15] + z[19] + -z[34];
z[11] = abb[3] * z[11];
z[12] = z[28] + z[44] + -z[71];
z[2] = z[2] + -z[9] + z[15];
z[2] = abb[6] * z[2];
z[2] = z[2] + z[10] + z[11] + -z[12] + z[25] + 6 * z[47];
z[2] = abb[30] * z[2];
z[2] = z[2] + z[5] + -z[17] + z[42];
z[2] = m1_set::bc<T>[0] * z[2];
z[5] = -abb[38] * z[21];
z[9] = abb[37] * z[29];
z[10] = -abb[49] * z[40];
z[10] = z[10] + z[31];
z[10] = abb[39] * z[10];
z[5] = z[5] + z[9] + z[10];
z[9] = -abb[41] + z[13] + z[23];
z[10] = -z[9] + -z[14] + 2 * z[67];
z[10] = abb[8] * z[10];
z[8] = -z[8] + -z[9];
z[8] = z[8] * z[50];
z[4] = -z[4] + z[8] + z[10] + z[12] + z[18];
z[4] = m1_set::bc<T>[0] * z[4];
z[7] = -abb[24] + z[7];
z[3] = z[3] * z[7];
z[3] = z[3] + z[4];
z[3] = abb[31] * z[3];
z[4] = -abb[28] * z[58];
z[7] = -abb[30] * z[22];
z[4] = z[4] + z[6] + z[7];
z[4] = m1_set::bc<T>[0] * z[4];
z[6] = -abb[38] * z[58];
z[7] = abb[37] * z[61];
z[4] = z[4] + z[6] + z[7];
z[4] = z[4] * z[54];
z[1] = z[1] + z[2] + z[3] + z[4] + 3 * z[5];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_28_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("15.22705487121682531585184216070836765633796551931412506268836417"),stof<T>("-83.781197666787351751956231260651738209094290445805215079939375928")}, std::complex<T>{stof<T>("-6.82070293910296763206809565657268841048751017346517091363243205"),stof<T>("-48.273888092845965450964722256267332118280718923943817868695212391")}, std::complex<T>{stof<T>("-6.487778227773556684859704839822798395323266192295462149335665204"),stof<T>("50.985310504584355545386129673624559246764242442805024950460033881")}, std::complex<T>{stof<T>("-7.184124888314354108986160328487616470637305501138732813068445575"),stof<T>("18.6213156475939604861182307099586211964636631965117156096363888")}, std::complex<T>{stof<T>("-9.254686010775785804542390477783609725658270984679616371327311634"),stof<T>("-15.55469254458090349004541917808688501560192144786321451017521826")}, std::complex<T>{stof<T>("-7.184124888314354108986160328487616470637305501138732813068445575"),stof<T>("18.6213156475939604861182307099586211964636631965117156096363888")}, std::complex<T>{stof<T>("38.780699012426523743089114106068010223173683305746504857014498087"),stof<T>("15.712188923077182853362767695481843777380915081787966261904006313")}, std::complex<T>{stof<T>("-43.693299263115379353354045628413022018389543203864180985206756727"),stof<T>("49.092492404841541510823828740696764182871493937718239190702631764")}, std::complex<T>{stof<T>("6.124356278562170207941640167907870335173470864621900249899651679"),stof<T>("15.90989323585557039169682329260139406798013967765050852787156731")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[26].real()/kbase.W[26].real()), rlog(k.W[27].real()/kbase.W[27].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_28_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_28_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("41.335149241672406577830469226200241270730304505594626944805218864"),stof<T>("-4.110441131842011108727343692266235889087615146785555234126705769")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,53> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W15(k,dl), dlog_W21(k,dl), dlog_W22(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[26].real()/k.W[26].real()), rlog(kend.W[27].real()/k.W[27].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k)};

                    
            return f_4_28_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_28_DLogXconstant_part(base_point<T>, kend);
	value += f_4_28_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_28_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_28_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_28_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_28_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_28_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_28_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
