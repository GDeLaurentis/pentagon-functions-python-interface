/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_447.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_447_abbreviated (const std::array<T,61>& abb) {
T z[95];
z[0] = abb[55] * (T(1) / T(2));
z[1] = abb[54] * (T(1) / T(2));
z[2] = z[0] + -z[1];
z[3] = abb[51] + abb[53];
z[4] = (T(1) / T(2)) * z[3];
z[5] = abb[50] + abb[52];
z[6] = z[2] + -z[4] + -z[5];
z[7] = abb[0] * z[6];
z[8] = abb[54] + -abb[55];
z[9] = z[5] + z[8];
z[10] = abb[2] * z[9];
z[11] = z[7] + z[10];
z[12] = abb[52] * (T(1) / T(2));
z[13] = abb[50] + z[12];
z[14] = z[4] + z[13];
z[15] = abb[4] * z[14];
z[16] = abb[9] * z[5];
z[17] = -z[15] + z[16];
z[5] = z[3] + z[5];
z[18] = abb[1] * z[5];
z[19] = z[17] + -z[18];
z[20] = abb[50] + z[3];
z[20] = abb[13] * z[20];
z[21] = z[19] + z[20];
z[22] = abb[21] * abb[56];
z[23] = abb[52] + z[8];
z[24] = abb[17] * z[23];
z[22] = z[22] + z[24];
z[24] = abb[19] * abb[56];
z[25] = abb[45] + -abb[48] + abb[49];
z[26] = abb[27] * z[25];
z[27] = z[24] + z[26];
z[28] = abb[26] * z[25];
z[29] = z[27] + -z[28];
z[30] = z[22] + -z[29];
z[31] = abb[23] + abb[25];
z[32] = z[25] * z[31];
z[33] = abb[22] * z[25];
z[34] = z[32] + z[33];
z[35] = abb[52] + z[3];
z[36] = abb[15] * z[35];
z[37] = z[34] + -z[36];
z[38] = abb[24] * z[25];
z[39] = z[30] + z[37] + z[38];
z[40] = abb[52] * (T(3) / T(2));
z[41] = -abb[50] + -z[4] + -z[8] + -z[40];
z[41] = abb[8] * z[41];
z[42] = -abb[3] * z[6];
z[39] = -z[11] + -z[21] + (T(1) / T(2)) * z[39] + z[41] + z[42];
z[41] = abb[30] * (T(1) / T(2));
z[39] = z[39] * z[41];
z[42] = abb[54] + z[5];
z[43] = abb[3] + -abb[7];
z[42] = z[42] * z[43];
z[43] = z[28] + z[37];
z[44] = abb[8] * z[14];
z[45] = (T(1) / T(2)) * z[35];
z[46] = abb[5] * z[45];
z[47] = (T(1) / T(2)) * z[38];
z[42] = z[18] + z[42] + (T(1) / T(2)) * z[43] + -z[44] + z[46] + z[47];
z[42] = abb[34] * z[42];
z[43] = z[33] + z[38];
z[48] = -z[36] + z[43];
z[49] = abb[7] * z[35];
z[30] = z[30] + z[48] + z[49];
z[9] = abb[8] * z[9];
z[50] = z[9] + z[11];
z[13] = z[2] + -z[13];
z[13] = abb[3] * z[13];
z[13] = -z[13] + (T(1) / T(2)) * z[30] + -z[50];
z[30] = abb[35] * z[13];
z[51] = -z[30] + z[42];
z[52] = abb[12] * z[35];
z[52] = -z[18] + z[52];
z[53] = abb[7] * z[14];
z[54] = z[52] + z[53];
z[55] = -z[11] + z[54];
z[56] = (T(1) / T(2)) * z[22];
z[29] = (T(1) / T(2)) * z[29] + -z[56];
z[57] = z[3] + -z[8];
z[58] = abb[3] * (T(1) / T(2));
z[57] = z[57] * z[58];
z[59] = abb[6] * z[45];
z[60] = abb[8] * z[23];
z[57] = z[29] + -z[47] + -z[55] + z[57] + z[59] + z[60];
z[61] = abb[33] * (T(1) / T(2));
z[57] = z[57] * z[61];
z[62] = abb[3] * z[5];
z[37] = (T(1) / T(2)) * z[37] + -z[44] + z[62];
z[53] = z[16] + z[20] + -z[37] + z[53] + -z[59];
z[63] = (T(1) / T(2)) * z[15];
z[64] = abb[12] * z[45];
z[64] = -z[18] + -z[63] + z[64];
z[53] = (T(1) / T(2)) * z[53] + z[64];
z[65] = abb[31] * z[53];
z[39] = z[39] + (T(1) / T(2)) * z[51] + z[57] + z[65];
z[51] = -z[4] + z[8] + z[12];
z[51] = abb[8] * z[51];
z[57] = (T(3) / T(2)) * z[3];
z[65] = abb[50] + abb[54];
z[40] = z[40] + z[57] + z[65];
z[66] = abb[7] * z[40];
z[67] = z[3] + z[12];
z[0] = z[0] + z[1] + z[67];
z[0] = abb[3] * z[0];
z[1] = z[0] + z[46] + -z[56];
z[68] = z[27] + z[32];
z[69] = abb[46] + abb[47];
z[70] = abb[27] + z[31];
z[71] = (T(1) / T(2)) * z[70];
z[71] = z[69] * z[71];
z[66] = z[1] + z[11] + z[18] + z[51] + -z[66] + (T(1) / T(2)) * z[68] + -z[71];
z[71] = abb[32] * (T(1) / T(16));
z[72] = -z[66] * z[71];
z[39] = (T(1) / T(8)) * z[39] + z[72];
z[39] = m1_set::bc<T>[0] * z[39];
z[72] = abb[22] + abb[24];
z[73] = abb[26] + z[72];
z[74] = -abb[27] + z[73];
z[75] = z[31] + z[74];
z[76] = abb[30] * z[75];
z[77] = z[31] + z[73];
z[77] = abb[34] * z[77];
z[78] = abb[26] + -abb[27];
z[79] = abb[24] + z[78];
z[79] = abb[33] * z[79];
z[80] = abb[22] + z[31];
z[81] = abb[31] * z[80];
z[82] = abb[35] * z[74];
z[76] = z[76] + z[77] + -z[79] + -z[81] + -z[82];
z[76] = m1_set::bc<T>[0] * z[76];
z[79] = abb[43] * z[70];
z[81] = abb[42] * z[80];
z[76] = z[76] + -z[79] + -z[81];
z[79] = abb[47] * (T(1) / T(32));
z[81] = -z[76] * z[79];
z[83] = -abb[52] + z[3];
z[83] = abb[15] * z[83];
z[84] = z[34] + z[83];
z[26] = -z[26] + z[28];
z[85] = z[22] + z[26] + z[38] + -z[60] + z[84];
z[7] = z[7] + z[20];
z[86] = z[5] + z[8];
z[86] = abb[10] * z[86];
z[87] = abb[56] * (T(1) / T(2));
z[87] = abb[18] * z[87];
z[85] = z[7] + -z[15] + (T(-1) / T(2)) * z[85] + z[86] + -z[87];
z[88] = abb[47] * (T(1) / T(2));
z[89] = -z[75] * z[88];
z[89] = -z[85] + z[89];
z[89] = abb[41] * z[89];
z[90] = -abb[41] * z[75];
z[76] = -z[76] + z[90];
z[90] = abb[46] * (T(1) / T(32));
z[76] = z[76] * z[90];
z[60] = -z[49] + z[60] + z[68];
z[91] = abb[55] + z[3];
z[91] = abb[14] * z[91];
z[1] = -z[1] + (T(-1) / T(2)) * z[60] + -z[87] + z[91];
z[1] = (T(1) / T(16)) * z[1];
z[60] = abb[43] * z[1];
z[87] = abb[8] * z[35];
z[49] = z[49] + z[87];
z[84] = -z[49] + z[84];
z[63] = (T(1) / T(2)) * z[18] + z[63];
z[87] = (T(1) / T(4)) * z[35];
z[92] = abb[6] * z[87];
z[92] = -z[20] + z[92];
z[93] = z[63] + (T(1) / T(4)) * z[84] + z[92];
z[94] = abb[42] * z[93];
z[39] = abb[60] + z[39] + z[60] + z[76] + z[81] + (T(1) / T(16)) * z[89] + (T(-1) / T(8)) * z[94];
z[17] = z[17] + z[46];
z[46] = -abb[50] + z[2];
z[4] = -z[4] + -z[46];
z[4] = abb[3] * z[4];
z[60] = abb[15] * abb[52];
z[76] = -abb[7] * z[65];
z[4] = z[4] + z[17] + (T(-1) / T(2)) * z[27] + z[28] + z[43] + -z[50] + z[56] + -z[60] + z[76];
z[4] = abb[35] * z[4];
z[13] = -abb[30] * z[13];
z[27] = z[28] + z[33] + z[83];
z[28] = abb[54] + z[14];
z[28] = abb[7] * z[28];
z[27] = (T(-1) / T(2)) * z[27] + z[28] + -z[47];
z[28] = abb[3] * z[45];
z[17] = -z[17] + z[27] + z[28];
z[17] = abb[34] * z[17];
z[4] = (T(1) / T(2)) * z[4] + z[13] + z[17];
z[4] = abb[35] * z[4];
z[13] = z[26] + z[32];
z[13] = (T(1) / T(2)) * z[13] + -z[24] + z[48] + z[56];
z[17] = (T(1) / T(4)) * z[3];
z[46] = abb[52] * (T(-3) / T(4)) + -z[17] + z[46];
z[48] = abb[3] + -abb[8];
z[46] = z[46] * z[48];
z[13] = -z[11] + (T(1) / T(2)) * z[13] + -z[46];
z[13] = abb[30] * z[13];
z[46] = -z[47] + z[54];
z[37] = -z[37] + z[46];
z[37] = abb[31] * z[37];
z[13] = z[13] + -z[30] + z[37];
z[2] = -z[2] + -z[67];
z[2] = abb[3] * z[2];
z[30] = -z[24] + -z[32];
z[37] = abb[8] * z[45];
z[2] = z[2] + -z[11] + (T(1) / T(2)) * z[30] + z[37] + z[46] + z[59];
z[2] = z[2] * z[61];
z[30] = -z[33] + z[36];
z[14] = -abb[3] * z[14];
z[33] = abb[7] + abb[8];
z[36] = abb[50] * z[33];
z[14] = z[14] + (T(1) / T(2)) * z[30] + z[36] + z[52] + -z[59];
z[14] = abb[34] * z[14];
z[2] = z[2] + -z[13] + z[14];
z[2] = abb[33] * z[2];
z[0] = z[0] + z[7] + z[18] + -z[86];
z[7] = (T(1) / T(4)) * z[22];
z[14] = (T(-5) / T(4)) * z[35] + -z[65];
z[14] = abb[7] * z[14];
z[10] = -z[7] + z[10] + z[14] + (T(1) / T(4)) * z[68] + (T(1) / T(2)) * z[91];
z[14] = abb[54] + -abb[55] + -z[17];
z[14] = abb[52] * (T(1) / T(4)) + (T(1) / T(3)) * z[14];
z[14] = abb[8] * z[14];
z[17] = abb[5] * z[35];
z[0] = (T(1) / T(6)) * z[0] + (T(1) / T(3)) * z[10] + z[14] + (T(1) / T(12)) * z[17];
z[10] = prod_pow(m1_set::bc<T>[0], 2);
z[0] = z[0] * z[10];
z[3] = abb[15] * z[3];
z[14] = abb[7] * z[45];
z[9] = z[3] + z[9] + z[11] + -z[14] + z[29];
z[11] = -z[16] + z[63];
z[6] = -z[6] * z[58];
z[6] = z[6] + (T(-1) / T(2)) * z[9] + z[11];
z[6] = abb[30] * z[6];
z[9] = -z[32] + z[49];
z[17] = (T(1) / T(2)) * z[9] + z[21] + -z[28];
z[18] = abb[34] * z[17];
z[21] = z[38] + z[84];
z[20] = z[16] + -z[20] + (T(1) / T(2)) * z[21];
z[20] = abb[31] * z[20];
z[6] = z[6] + z[18] + z[20];
z[6] = abb[30] * z[6];
z[18] = abb[57] * z[85];
z[17] = abb[39] * z[17];
z[20] = -abb[19] * z[35];
z[21] = -abb[21] * z[23];
z[22] = -abb[17] * abb[56];
z[20] = z[20] + z[21] + z[22];
z[20] = abb[40] * z[20];
z[21] = abb[40] * abb[56];
z[22] = -abb[8] * z[21];
z[20] = (T(1) / T(2)) * z[20] + z[22];
z[16] = z[16] + -z[27];
z[16] = abb[38] * z[16];
z[15] = abb[38] * z[15];
z[22] = -abb[18] * abb[40] * z[87];
z[21] = abb[29] * z[21];
z[0] = z[0] + z[2] + z[4] + z[6] + -z[15] + z[16] + z[17] + z[18] + (T(1) / T(2)) * z[20] + z[21] + z[22];
z[2] = abb[27] * (T(1) / T(2)) + -z[73];
z[2] = abb[35] * z[2];
z[4] = abb[34] * z[73];
z[6] = abb[30] * z[74];
z[2] = z[2] + z[4] + z[6];
z[2] = abb[35] * z[2];
z[4] = z[41] * z[78];
z[6] = z[31] + z[72];
z[6] = abb[31] * z[6];
z[15] = abb[34] * z[31];
z[4] = z[4] + z[6] + -z[15];
z[4] = abb[30] * z[4];
z[15] = z[31] + z[78];
z[15] = (T(1) / T(2)) * z[15] + z[72];
z[15] = abb[30] * z[15];
z[6] = -z[6] + z[15] + -z[82];
z[15] = abb[24] + z[31];
z[16] = z[15] * z[61];
z[17] = abb[22] * abb[34];
z[16] = z[6] + z[16] + z[17];
z[16] = abb[33] * z[16];
z[10] = -abb[59] + (T(-1) / T(6)) * z[10];
z[10] = z[10] * z[70];
z[17] = prod_pow(abb[34], 2);
z[18] = z[17] * z[31];
z[20] = abb[28] * abb[40];
z[18] = z[18] + -z[20];
z[21] = abb[24] * (T(1) / T(2)) + z[80];
z[21] = abb[31] * z[21];
z[22] = abb[34] * z[80];
z[21] = z[21] + -z[22];
z[21] = abb[31] * z[21];
z[22] = abb[57] * z[75];
z[23] = abb[39] * z[31];
z[27] = abb[36] * z[72];
z[28] = abb[38] * z[73];
z[2] = z[2] + -z[4] + z[10] + z[16] + (T(1) / T(2)) * z[18] + z[21] + z[22] + z[23] + z[27] + -z[28];
z[4] = z[79] + z[90];
z[2] = z[2] * z[4];
z[4] = abb[32] * z[66];
z[10] = abb[31] * z[35];
z[16] = abb[30] * z[45];
z[10] = z[10] + -z[16];
z[16] = abb[33] * z[45];
z[16] = -z[10] + z[16];
z[18] = abb[16] * z[16];
z[4] = -z[4] + z[18];
z[18] = -z[26] + z[32];
z[18] = (T(1) / T(2)) * z[18] + z[24] + z[51];
z[8] = -z[8] + z[12] + z[57];
z[8] = z[8] * z[58];
z[7] = -z[7] + z[8] + (T(1) / T(2)) * z[18] + -z[55];
z[7] = abb[33] * z[7];
z[8] = -abb[26] + z[70];
z[8] = z[8] * z[61];
z[6] = z[6] + z[8] + z[77];
z[8] = abb[46] * (T(-1) / T(2)) + -z[88];
z[6] = z[6] * z[8];
z[4] = (T(1) / T(2)) * z[4] + z[6] + z[7] + z[13] + z[42];
z[4] = z[4] * z[71];
z[6] = -abb[3] * z[40];
z[3] = z[3] + z[6] + z[14] + z[19] + (T(-1) / T(2)) * z[32] + z[44];
z[3] = z[3] * z[17];
z[6] = abb[30] * z[10];
z[7] = -abb[33] * z[16];
z[8] = -prod_pow(abb[31], 2) * z[45];
z[10] = -abb[36] * z[35];
z[6] = z[6] + z[7] + z[8] + z[10];
z[6] = abb[16] * z[6];
z[7] = z[15] * z[69];
z[8] = -abb[3] + abb[6] + -abb[16];
z[8] = z[8] * z[35];
z[7] = z[7] + z[8] + z[9] + -z[38];
z[7] = abb[37] * z[7];
z[3] = z[3] + z[6] + z[7];
z[6] = -abb[34] * z[53];
z[5] = z[5] * z[33];
z[5] = -z[5] + z[34] + z[47] + -z[60] + z[62];
z[5] = (T(-1) / T(2)) * z[5] + z[64] + -z[92];
z[5] = abb[31] * z[5];
z[5] = (T(1) / T(2)) * z[5] + z[6];
z[5] = abb[31] * z[5];
z[6] = (T(-1) / T(4)) * z[69];
z[6] = z[6] * z[80];
z[6] = z[6] + z[93];
z[6] = abb[58] * z[6];
z[7] = -z[43] + -z[83];
z[8] = abb[3] * z[87];
z[7] = (T(1) / T(4)) * z[7] + z[8] + z[11];
z[7] = abb[36] * z[7];
z[5] = z[5] + z[6] + z[7];
z[1] = -abb[59] * z[1];
z[6] = -abb[3] + abb[5];
z[6] = (T(1) / T(32)) * z[6];
z[6] = abb[38] * z[6];
z[7] = abb[20] * abb[40];
z[6] = z[6] + (T(-1) / T(64)) * z[7];
z[6] = z[6] * z[35];
z[7] = z[20] * z[25];
z[0] = abb[44] + (T(1) / T(16)) * z[0] + z[1] + z[2] + (T(1) / T(32)) * z[3] + z[4] + (T(1) / T(8)) * z[5] + z[6] + (T(1) / T(64)) * z[7];
return {z[39], z[0]};
}


template <typename T> std::complex<T> f_4_447_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.11547469860951485867325507914443830029563758720371430850081481908"),stof<T>("-0.28378637949826482878096434905678768657500355789218316041368945857")}, std::complex<T>{stof<T>("0.2229406852754450308600876055638796938568208328197201004665447706"),stof<T>("-0.10132680948732982130675139559947531325797989241265149128864421455")}, std::complex<T>{stof<T>("0.17729990653795407651557935755993613218025564885707155562842456746"),stof<T>("-0.18010586206002180436994185622072084366003951066727897807161351682")}, std::complex<T>{stof<T>("0.2229406852754450308600876055638796938568208328197201004665447706"),stof<T>("-0.10132680948732982130675139559947531325797989241265149128864421455")}, std::complex<T>{stof<T>("0.19970779164634181236722597107590698447928348087395934876283668865"),stof<T>("-0.11312695772873113369225566760857319294590057978610901273976392251")}, std::complex<T>{stof<T>("-0.027078297507034053871185928587716811995264245383652167487882277839"),stof<T>("0.065958593387465625013622675084755567328522146666352131071790205943")}, std::complex<T>{stof<T>("-0.04972339456069860063975792774574482256343578028833931988485229454"),stof<T>("0.13801363516870842049040280265772312585990829028334043153424807411")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_447_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_447_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_447_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(256)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (8 + v[0] + 3 * v[1] + v[2] + -3 * v[3] + -4 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = ((T(-1) / T(32)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[50] + abb[52] + abb[54]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[34] + abb[35] * (T(1) / T(2));
z[0] = abb[35] * z[0];
z[1] = prod_pow(abb[34], 2);
z[0] = abb[38] + z[0] + (T(1) / T(2)) * z[1];
z[1] = abb[50] + abb[52] + abb[54];
return abb[11] * (T(1) / T(16)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_447_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.12859412000614909217962997931210223124328802000465183632759617216"),stof<T>("-0.16932750399970493041450531401030140763628697790480269671613473188")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,61> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_9_im(k), f_2_12_im(k), T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_2_re(k), f_2_9_re(k), f_2_12_re(k), T{0}};
abb[44] = SpDLog_f_4_447_W_17_Im(t, path, abb);
abb[60] = SpDLog_f_4_447_W_17_Re(t, path, abb);

                    
            return f_4_447_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_447_DLogXconstant_part(base_point<T>, kend);
	value += f_4_447_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_447_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_447_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_447_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_447_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_447_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_447_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
