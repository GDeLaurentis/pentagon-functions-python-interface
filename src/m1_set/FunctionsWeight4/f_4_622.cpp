/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_622.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_622_abbreviated (const std::array<T,69>& abb) {
T z[138];
z[0] = abb[47] + abb[56];
z[1] = 4 * abb[55];
z[2] = abb[53] * (T(1) / T(2));
z[3] = 4 * abb[58];
z[4] = -8 * abb[50] + abb[54] + -abb[57] + abb[48] * (T(-23) / T(2)) + abb[49] * (T(-17) / T(4)) + (T(1) / T(4)) * z[0] + z[1] + -z[2] + z[3];
z[4] = abb[2] * z[4];
z[5] = abb[50] * (T(1) / T(2));
z[6] = abb[55] * (T(1) / T(2));
z[7] = abb[48] + z[5] + -z[6];
z[8] = abb[52] * (T(1) / T(2));
z[9] = abb[57] * (T(1) / T(2));
z[10] = z[8] + -z[9];
z[11] = -z[7] + -z[10];
z[11] = abb[3] * z[11];
z[12] = abb[49] + abb[50];
z[13] = 2 * abb[48];
z[14] = z[12] + z[13];
z[15] = abb[52] + abb[57];
z[16] = abb[47] + z[15];
z[17] = 2 * z[14] + -z[16];
z[17] = abb[15] * z[17];
z[18] = abb[59] + abb[60];
z[19] = abb[22] * z[18];
z[20] = abb[47] + abb[52];
z[21] = -abb[57] + z[20];
z[22] = abb[17] * z[21];
z[23] = abb[21] * z[18];
z[4] = z[4] + z[11] + z[17] + -z[19] + -z[22] + -z[23];
z[11] = 4 * abb[50];
z[24] = abb[49] * (T(5) / T(2));
z[25] = -abb[53] + -z[11] + -z[24];
z[26] = -abb[47] + abb[56];
z[27] = abb[54] * (T(3) / T(4));
z[28] = 3 * abb[52];
z[25] = abb[58] * (T(-4) / T(3)) + abb[55] * (T(2) / T(3)) + (T(1) / T(3)) * z[25] + (T(-1) / T(12)) * z[26] + -z[27] + z[28];
z[25] = abb[4] * z[25];
z[29] = abb[48] * (T(1) / T(2));
z[30] = 2 * abb[57];
z[31] = -abb[50] + abb[55];
z[32] = -5 * abb[52] + 11 * abb[58] + abb[49] * (T(1) / T(4)) + abb[47] * (T(5) / T(4)) + abb[54] * (T(11) / T(2)) + z[2] + z[29] + -z[30] + -z[31];
z[32] = abb[56] * (T(-13) / T(4)) + (T(1) / T(3)) * z[32];
z[32] = abb[0] * z[32];
z[33] = abb[55] + abb[57];
z[34] = abb[47] + -abb[49];
z[33] = abb[58] + abb[52] * (T(-7) / T(6)) + z[5] + (T(-1) / T(6)) * z[33] + (T(-1) / T(3)) * z[34];
z[33] = abb[8] * z[33];
z[35] = abb[48] + abb[53];
z[36] = abb[51] * (T(-2) / T(3)) + abb[56] * (T(19) / T(6));
z[37] = -abb[50] + abb[52];
z[38] = abb[49] * (T(-5) / T(6)) + abb[54] * (T(7) / T(3)) + abb[47] * (T(11) / T(6)) + (T(1) / T(3)) * z[35] + -z[36] + z[37];
z[38] = abb[14] * z[38];
z[39] = abb[61] + abb[62];
z[40] = abb[24] + abb[26];
z[41] = abb[23] + z[40];
z[42] = abb[25] * (T(-5) / T(6)) + abb[27] * (T(1) / T(3)) + (T(-1) / T(2)) * z[41];
z[42] = z[39] * z[42];
z[43] = abb[50] + -abb[53];
z[44] = 2 * abb[55];
z[45] = abb[49] + z[44];
z[46] = 10 * abb[52] + 5 * abb[57] + abb[48] * (T(5) / T(2)) + 2 * z[43] + -z[45];
z[36] = -5 * abb[58] + abb[54] * (T(-13) / T(6)) + z[36] + (T(1) / T(3)) * z[46];
z[36] = abb[10] * z[36];
z[46] = 8 * abb[48];
z[47] = -4 * abb[49] + -7 * abb[50] + -z[46];
z[47] = abb[55] + abb[47] * (T(5) / T(6)) + abb[52] * (T(11) / T(6)) + -z[9] + (T(1) / T(3)) * z[47];
z[47] = abb[5] * z[47];
z[48] = -abb[54] + z[26];
z[48] = abb[1] * z[48];
z[49] = abb[19] * z[18];
z[50] = (T(1) / T(2)) * z[49];
z[21] = abb[6] * z[21];
z[20] = -abb[58] + z[20];
z[51] = abb[13] * z[20];
z[52] = abb[20] * z[18];
z[4] = (T(1) / T(3)) * z[4] + (T(-1) / T(2)) * z[21] + z[25] + z[32] + 5 * z[33] + z[36] + z[38] + z[42] + z[47] + (T(13) / T(4)) * z[48] + -z[50] + (T(11) / T(3)) * z[51] + (T(-5) / T(6)) * z[52];
z[4] = prod_pow(m1_set::bc<T>[0], 2) * z[4];
z[25] = abb[52] * (T(7) / T(2));
z[32] = 3 * abb[58];
z[33] = z[25] + -z[32];
z[36] = abb[49] * (T(3) / T(4));
z[27] = z[6] + -z[9] + -z[13] + z[27] + -z[33] + z[36] + (T(1) / T(2)) * z[43];
z[27] = abb[4] * z[27];
z[38] = 2 * abb[56];
z[42] = 2 * abb[54];
z[43] = z[38] + -z[42];
z[47] = 2 * abb[52];
z[53] = abb[57] + z[47];
z[54] = z[32] + -z[53];
z[55] = z[31] + z[54];
z[56] = abb[51] * (T(1) / T(2));
z[57] = abb[49] * (T(1) / T(2));
z[35] = -z[35] + -z[43] + z[55] + z[56] + -z[57];
z[35] = abb[10] * z[35];
z[58] = abb[50] * (T(3) / T(2)) + -z[9];
z[33] = -z[6] + -z[33] + -z[34] + z[58];
z[33] = abb[5] * z[33];
z[33] = z[33] + z[35];
z[35] = abb[47] * (T(1) / T(2));
z[59] = 2 * abb[58];
z[60] = -z[7] + z[35] + z[43] + z[47] + -z[59];
z[60] = abb[0] * z[60];
z[2] = z[2] + -z[36] + z[37] + z[56];
z[36] = 2 * abb[47];
z[37] = 3 * abb[56] + abb[54] * (T(-9) / T(4)) + -z[2] + -z[36];
z[37] = abb[14] * z[37];
z[5] = z[5] + z[57];
z[56] = z[8] + z[9];
z[61] = -abb[53] + z[6];
z[62] = abb[56] + z[5] + -z[56] + z[61];
z[62] = abb[7] * z[62];
z[63] = abb[4] + -abb[7];
z[63] = abb[51] * z[63];
z[62] = z[62] + (T(1) / T(2)) * z[63];
z[53] = z[53] + -z[59];
z[64] = -z[12] + z[53];
z[65] = abb[18] * z[64];
z[66] = z[62] + z[65];
z[67] = 2 * abb[49];
z[68] = 3 * abb[50];
z[69] = -abb[55] + z[67] + z[68];
z[70] = -abb[57] + z[69];
z[71] = 6 * abb[58];
z[72] = 7 * abb[52] + -z[71];
z[73] = z[36] + -z[70] + z[72];
z[74] = abb[8] * z[73];
z[75] = z[15] + -z[59];
z[76] = -z[13] + z[31];
z[77] = z[75] + -z[76];
z[78] = abb[16] * z[77];
z[79] = -abb[58] + z[35];
z[80] = (T(1) / T(2)) * z[15] + z[79];
z[80] = abb[6] * z[80];
z[81] = 2 * z[51];
z[27] = z[27] + z[33] + z[37] + -3 * z[48] + z[50] + z[60] + -z[66] + z[74] + z[78] + z[80] + -z[81];
z[27] = abb[36] * z[27];
z[37] = abb[47] + z[75];
z[60] = abb[6] * z[37];
z[75] = 2 * abb[18];
z[64] = z[64] * z[75];
z[60] = -z[49] + -z[60] + z[64];
z[75] = z[60] + -z[78];
z[80] = abb[47] + z[13] + -z[47] + z[69];
z[82] = abb[0] * z[80];
z[83] = -abb[47] + z[59];
z[84] = -abb[48] + -z[47] + z[83];
z[85] = 2 * abb[4];
z[84] = z[84] * z[85];
z[86] = 4 * abb[5];
z[87] = z[20] * z[86];
z[82] = z[74] + -z[75] + -z[82] + z[84] + -z[87];
z[82] = abb[34] * z[82];
z[27] = z[27] + -z[82];
z[27] = abb[36] * z[27];
z[84] = -abb[58] + abb[52] * (T(3) / T(2));
z[87] = -abb[53] + abb[49] * (T(3) / T(2));
z[6] = z[6] + z[29] + z[35] + z[58] + -z[84] + z[87];
z[6] = abb[4] * z[6];
z[24] = -z[3] + z[24];
z[29] = abb[56] * (T(1) / T(2));
z[58] = 6 * abb[50] + -abb[53] + -3 * abb[55] + z[24] + -z[29] + z[30] + z[35] + z[46];
z[58] = abb[2] * z[58];
z[7] = -abb[58] + z[7] + z[56];
z[7] = abb[6] * z[7];
z[7] = z[7] + z[62] + -z[65];
z[62] = 5 * abb[48];
z[65] = -abb[49] + z[44];
z[88] = -abb[50] + -abb[53];
z[88] = abb[56] + z[54] + -z[62] + z[65] + 2 * z[88];
z[88] = abb[10] * z[88];
z[89] = z[35] + z[56];
z[90] = -z[44] + z[89];
z[91] = 2 * abb[50];
z[92] = abb[49] + z[91];
z[93] = 3 * abb[48];
z[94] = z[92] + z[93];
z[95] = 2 * z[94];
z[32] = -z[32] + z[90] + z[95];
z[32] = abb[5] * z[32];
z[96] = (T(1) / T(2)) * z[40];
z[97] = abb[28] + z[96];
z[97] = -z[39] * z[97];
z[98] = abb[52] + -abb[57];
z[99] = -z[76] + z[98];
z[100] = abb[3] * z[99];
z[5] = -z[5] + z[61];
z[61] = -abb[48] + z[29];
z[101] = -z[5] + -z[61];
z[101] = abb[0] * z[101];
z[102] = -abb[51] + z[26];
z[102] = abb[9] * z[102];
z[103] = (T(1) / T(2)) * z[52];
z[6] = z[6] + -z[7] + z[32] + z[58] + -z[78] + z[88] + z[97] + z[100] + z[101] + (T(1) / T(2)) * z[102] + z[103];
z[6] = abb[33] * z[6];
z[32] = -2 * abb[51] + 5 * abb[56];
z[58] = 3 * abb[49];
z[88] = z[32] + -z[42] + z[58];
z[97] = 2 * abb[53];
z[101] = -abb[55] + z[97];
z[104] = z[15] + -z[68] + z[83] + -z[88] + z[101];
z[104] = abb[16] * z[104];
z[105] = z[30] + -z[59];
z[45] = -z[26] + z[45] + -z[97] + -z[105];
z[45] = abb[2] * z[45];
z[106] = abb[51] + abb[54];
z[107] = -z[38] + z[54] + z[106];
z[108] = 2 * abb[10];
z[107] = z[107] * z[108];
z[109] = abb[6] * z[77];
z[109] = -z[64] + z[109];
z[110] = z[28] + -z[59];
z[111] = -z[42] + z[110];
z[112] = -abb[57] + z[31];
z[113] = -abb[56] + z[93] + z[111] + -z[112];
z[113] = abb[4] * z[113];
z[114] = z[12] + -z[15];
z[115] = -z[101] + z[114];
z[116] = z[38] + z[115];
z[116] = abb[7] * z[116];
z[63] = z[63] + z[116];
z[45] = z[45] + z[63] + -z[100] + -z[102] + z[104] + -z[107] + z[109] + z[113];
z[45] = abb[35] * z[45];
z[70] = -z[13] + -z[70] + z[110];
z[104] = abb[4] * z[70];
z[113] = abb[55] + abb[58];
z[94] = -z[94] + z[113];
z[116] = z[86] * z[94];
z[55] = z[55] + -z[93];
z[55] = z[55] * z[108];
z[17] = z[17] + -z[23];
z[94] = abb[2] * z[94];
z[117] = -abb[47] + z[31];
z[118] = abb[0] * z[117];
z[104] = z[17] + -z[55] + -z[75] + 6 * z[94] + z[104] + z[116] + -z[118];
z[116] = -abb[34] * z[104];
z[118] = abb[28] + z[41];
z[119] = abb[61] * z[118];
z[119] = z[104] + z[119];
z[119] = abb[31] * z[119];
z[120] = abb[5] * z[37];
z[121] = -z[78] + z[120];
z[122] = abb[47] + z[76];
z[123] = abb[0] * z[122];
z[123] = -z[52] + z[123];
z[124] = -abb[57] + abb[58];
z[125] = 2 * z[124];
z[126] = abb[2] * z[125];
z[126] = z[100] + z[121] + -z[123] + -z[126];
z[127] = abb[28] * z[39];
z[127] = -z[126] + z[127];
z[127] = abb[32] * z[127];
z[128] = abb[28] + z[40];
z[129] = abb[35] * z[128];
z[130] = -abb[34] * z[118];
z[130] = z[129] + z[130];
z[130] = abb[61] * z[130];
z[131] = abb[31] + -abb[34];
z[131] = z[118] * z[131];
z[129] = z[129] + z[131];
z[129] = abb[62] * z[129];
z[6] = z[6] + z[45] + z[116] + z[119] + z[127] + z[129] + z[130];
z[6] = abb[33] * z[6];
z[45] = -z[14] + z[89];
z[116] = abb[15] * z[45];
z[23] = (T(1) / T(2)) * z[23] + z[116];
z[116] = 2 * z[94];
z[119] = -z[23] + z[116];
z[5] = -z[5] + -z[9] + -z[84];
z[5] = abb[4] * z[5];
z[84] = abb[50] + z[13];
z[84] = 2 * z[84];
z[34] = -z[34] + z[84] + -z[113];
z[34] = abb[5] * z[34];
z[19] = z[19] + z[22];
z[22] = (T(1) / T(2)) * z[19];
z[113] = -abb[48] + abb[56];
z[112] = z[112] + z[113];
z[112] = abb[0] * z[112];
z[127] = -abb[56] + z[97];
z[129] = -abb[48] + abb[49] + z[54] + z[127];
z[130] = abb[10] * z[129];
z[124] = abb[6] * z[124];
z[131] = -abb[23] + abb[27];
z[131] = (T(1) / T(2)) * z[131];
z[132] = abb[61] * z[131];
z[5] = z[5] + -z[22] + z[34] + z[66] + -z[102] + z[112] + -z[119] + z[124] + z[130] + z[132];
z[5] = abb[31] * z[5];
z[34] = 4 * z[94];
z[23] = -z[23] + z[34] + -z[55];
z[55] = z[64] + -z[78];
z[64] = z[22] + (T(3) / T(2)) * z[52] + -z[81];
z[66] = z[35] + z[76];
z[8] = abb[57] * (T(3) / T(2)) + z[8] + -z[59] + -z[66];
z[8] = abb[6] * z[8];
z[80] = -abb[4] * z[80];
z[94] = 5 * abb[50];
z[46] = z[46] + z[94];
z[58] = z[46] + z[58];
z[90] = z[3] + -z[58] + -z[90];
z[90] = abb[5] * z[90];
z[14] = -abb[57] + -z[14] + z[59];
z[14] = abb[0] * z[14];
z[8] = z[8] + z[14] + z[23] + -z[50] + -z[55] + -z[64] + z[80] + z[90];
z[8] = abb[34] * z[8];
z[14] = abb[48] + -abb[53] + z[91];
z[50] = -abb[47] + z[3];
z[14] = 2 * z[14] + -z[50] + z[88];
z[14] = abb[16] * z[14];
z[80] = -abb[57] + z[26] + z[76] + -z[111];
z[80] = abb[4] * z[80];
z[90] = z[52] + z[63];
z[111] = abb[48] + abb[50];
z[112] = -abb[53] + z[111];
z[124] = abb[49] + 2 * z[112];
z[130] = z[26] + -z[124];
z[130] = abb[0] * z[130];
z[80] = z[14] + z[60] + z[80] + -z[90] + z[107] + -z[120] + z[130];
z[80] = abb[35] * z[80];
z[105] = z[105] + -z[122];
z[105] = abb[0] * z[105];
z[77] = abb[4] * z[77];
z[107] = z[19] + z[52];
z[120] = -z[81] + z[107];
z[77] = z[77] + z[105] + z[120] + z[121];
z[77] = abb[36] * z[77];
z[105] = abb[35] * z[40];
z[121] = abb[27] + z[41];
z[121] = abb[28] + (T(1) / T(2)) * z[121];
z[130] = abb[34] * z[121];
z[132] = abb[27] + abb[28];
z[132] = abb[36] * z[132];
z[105] = -z[105] + z[130] + -z[132];
z[130] = abb[61] * z[105];
z[5] = z[5] + z[8] + z[77] + z[80] + z[130];
z[5] = abb[31] * z[5];
z[8] = abb[53] + z[111];
z[8] = 2 * z[8] + -z[65];
z[80] = 4 * abb[52];
z[130] = 4 * abb[56];
z[133] = -abb[51] + -4 * abb[54] + z[8] + z[30] + -z[71] + z[80] + z[130];
z[133] = abb[10] * z[133];
z[133] = z[63] + z[133];
z[134] = abb[47] * (T(3) / T(2));
z[11] = -3 * abb[53] + abb[56] * (T(7) / T(2)) + z[11] + z[24] + z[93] + -z[106] + z[134];
z[11] = abb[16] * z[11];
z[24] = abb[55] + -abb[57];
z[93] = abb[50] + z[24] + z[87];
z[106] = abb[54] + z[35] + z[61] + z[93] + -z[110];
z[106] = abb[4] * z[106];
z[135] = -z[26] + z[42];
z[124] = -z[47] + z[124] + z[135];
z[136] = abb[14] * z[124];
z[70] = -abb[5] * z[70];
z[137] = z[8] + -z[135];
z[137] = abb[2] * z[137];
z[35] = z[35] + z[57];
z[57] = abb[53] + -abb[54] + z[35] + z[61];
z[57] = abb[0] * z[57];
z[11] = z[11] + z[57] + z[70] + z[106] + -z[109] + -z[133] + -z[136] + z[137];
z[11] = abb[40] * z[11];
z[57] = 4 * abb[48];
z[61] = abb[55] + -z[16] + -z[57] + z[71] + -z[88] + -z[94] + z[97];
z[61] = abb[16] * z[61];
z[70] = abb[54] * (T(1) / T(2)) + z[26] + -z[57] + -z[72] + z[93];
z[70] = abb[4] * z[70];
z[72] = z[87] + z[91];
z[87] = abb[54] * (T(3) / T(2));
z[88] = z[72] + z[87];
z[91] = abb[51] + z[47];
z[94] = z[36] + z[88] + -z[91];
z[94] = abb[14] * z[94];
z[94] = -z[74] + z[94];
z[73] = abb[5] * z[73];
z[97] = abb[0] * z[124];
z[70] = -z[61] + z[70] + -z[73] + -z[94] + -z[97] + -z[133];
z[73] = -abb[36] * z[70];
z[2] = -abb[56] + abb[54] * (T(1) / T(4)) + z[2];
z[2] = abb[14] * z[2];
z[93] = -7 * abb[48] + -z[28] + z[93];
z[79] = abb[54] * (T(-13) / T(4)) + -z[79] + (T(1) / T(2)) * z[93] + z[130];
z[79] = abb[4] * z[79];
z[29] = z[29] + -z[35] + -z[112];
z[29] = abb[0] * z[29];
z[35] = abb[53] + -abb[55] + abb[56] * (T(-3) / T(2)) + -z[35] + z[59];
z[35] = abb[2] * z[35];
z[2] = 3 * z[2] + -z[7] + z[14] + z[29] + z[33] + z[35] + z[79];
z[2] = abb[35] * z[2];
z[2] = z[2] + z[73] + z[82];
z[2] = abb[35] * z[2];
z[7] = z[10] + z[76] + z[134];
z[7] = abb[6] * z[7];
z[7] = z[7] + (T(3) / T(2)) * z[49];
z[14] = z[57] + -z[65] + z[68];
z[29] = z[14] + -z[59] + z[89];
z[29] = abb[5] * z[29];
z[33] = -abb[4] * z[122];
z[35] = z[12] + z[36];
z[65] = abb[57] + -z[35];
z[65] = abb[0] * z[65];
z[68] = -abb[61] * z[121];
z[22] = -z[7] + z[22] + z[29] + z[33] + z[65] + z[68] + -z[78] + -z[103] + -z[119];
z[22] = abb[31] * z[22];
z[7] = z[7] + z[64];
z[24] = -abb[49] + z[24] + z[83] + -2 * z[111];
z[24] = abb[0] * z[24];
z[29] = z[14] + -z[89];
z[29] = abb[5] * z[29];
z[33] = abb[4] * z[37];
z[24] = -z[7] + z[24] + z[29] + -z[33] + -z[119];
z[29] = -abb[34] * z[24];
z[33] = abb[35] * z[126];
z[37] = abb[5] * z[122];
z[64] = 2 * abb[2];
z[65] = -abb[48] + z[31];
z[64] = z[64] * z[65];
z[37] = z[37] + z[64] + -z[123];
z[37] = abb[32] * z[37];
z[64] = abb[28] * abb[35];
z[68] = -abb[27] + z[41];
z[73] = (T(1) / T(2)) * z[68];
z[76] = abb[34] * z[73];
z[64] = -z[64] + z[76] + z[132];
z[76] = abb[61] * z[64];
z[78] = -abb[31] * z[121];
z[64] = z[64] + z[78];
z[64] = abb[62] * z[64];
z[22] = z[22] + z[29] + z[33] + z[37] + z[64] + z[76] + -z[77];
z[22] = abb[32] * z[22];
z[29] = z[62] + z[92];
z[33] = abb[47] + z[44];
z[28] = -abb[57] + -z[28] + -2 * z[29] + z[33] + z[71];
z[28] = abb[5] * z[28];
z[29] = -4 * abb[53] + 5 * abb[54] + -z[32] + z[62] + -z[67];
z[29] = abb[10] * z[29];
z[32] = -z[17] + z[90];
z[37] = -abb[54] + z[113] + -z[117];
z[37] = abb[0] * z[37];
z[42] = z[8] + -z[42];
z[62] = abb[4] * z[42];
z[28] = z[28] + z[29] + -z[32] + z[37] + -z[61] + -z[62] + -z[136];
z[29] = abb[63] * z[28];
z[37] = -abb[65] * z[70];
z[61] = -abb[57] + z[110];
z[31] = z[31] + z[36] + z[61];
z[31] = abb[4] * z[31];
z[36] = abb[47] + abb[58] + z[114];
z[62] = 2 * abb[0];
z[36] = z[36] * z[62];
z[61] = 3 * abb[47] + z[61];
z[61] = abb[5] * z[61];
z[64] = abb[25] + z[68];
z[64] = z[39] * z[64];
z[31] = z[31] + z[36] + z[60] + z[61] + -z[64] + -z[74] + -z[120];
z[36] = abb[64] * z[31];
z[12] = z[12] + -z[56] + z[134];
z[12] = abb[20] * z[12];
z[10] = -z[10] + z[66];
z[10] = abb[19] * z[10];
z[45] = abb[21] * z[45];
z[56] = abb[4] + 2 * abb[30];
z[56] = z[18] * z[56];
z[60] = abb[6] + -abb[15];
z[60] = z[18] * z[60];
z[61] = abb[47] + z[98];
z[61] = abb[22] * z[61];
z[60] = -z[60] + z[61];
z[61] = abb[5] + -abb[17];
z[18] = (T(1) / T(2)) * z[18];
z[18] = z[18] * z[61];
z[61] = abb[29] * (T(1) / T(2));
z[61] = z[39] * z[61];
z[10] = z[10] + z[12] + z[18] + -z[45] + z[56] + (T(-1) / T(2)) * z[60] + z[61];
z[12] = abb[66] * z[10];
z[16] = z[16] + z[44] + -z[95];
z[16] = abb[5] * z[16];
z[18] = z[21] + z[49];
z[21] = -abb[4] * z[99];
z[45] = -z[62] * z[65];
z[16] = z[16] + z[17] + z[18] + z[21] + z[45] + z[52] + z[100];
z[16] = abb[39] * z[16];
z[17] = -z[57] + -z[69] + z[83];
z[17] = abb[0] * z[17];
z[21] = abb[6] * z[122];
z[20] = -z[20] * z[85];
z[45] = -abb[47] + abb[48];
z[45] = z[45] * z[86];
z[17] = z[17] + z[20] + z[21] + z[45] + z[49] + z[81] + -z[116];
z[17] = prod_pow(abb[34], 2) * z[17];
z[20] = abb[25] + abb[28];
z[21] = abb[23] + z[20];
z[45] = z[21] + z[96];
z[45] = abb[36] * z[45];
z[20] = z[20] + z[41];
z[49] = abb[34] * z[20];
z[45] = z[45] + -z[49];
z[45] = abb[36] * z[45];
z[56] = abb[36] * z[21];
z[57] = abb[35] * z[96];
z[49] = -z[49] + z[56] + z[57];
z[49] = abb[35] * z[49];
z[56] = abb[38] + abb[40];
z[40] = z[40] * z[56];
z[57] = abb[28] * abb[38];
z[41] = abb[39] * z[41];
z[60] = abb[23] + abb[28];
z[61] = abb[63] * z[60];
z[64] = abb[65] * z[21];
z[40] = z[40] + -z[41] + -z[45] + z[49] + z[57] + -z[61] + z[64];
z[41] = -abb[61] * z[40];
z[45] = abb[31] * z[131];
z[45] = z[45] + z[105];
z[45] = abb[31] * z[45];
z[40] = -z[40] + z[45];
z[40] = abb[62] * z[40];
z[0] = z[0] + z[115];
z[0] = abb[16] * z[0];
z[0] = -z[0] + z[63];
z[35] = abb[56] + -z[30] + z[35] + -z[101];
z[35] = abb[0] * z[35];
z[45] = abb[27] + z[128];
z[45] = z[39] * z[45];
z[49] = 2 * z[102];
z[26] = -abb[4] * z[26];
z[18] = z[0] + z[18] + -z[19] + z[26] + z[35] + z[45] + -z[49];
z[18] = abb[37] * z[18];
z[19] = -abb[6] * z[99];
z[26] = abb[56] + z[65] + z[98];
z[26] = abb[4] * z[26];
z[0] = -z[0] + z[19] + z[26] + z[100];
z[0] = abb[38] * z[0];
z[19] = -abb[53] + z[46];
z[1] = -5 * abb[49] + z[1] + -2 * z[19] + z[71] + -z[135];
z[19] = abb[63] * z[1];
z[26] = -abb[49] + z[30] + -z[33] + z[127];
z[26] = abb[38] * z[26];
z[14] = abb[57] + -z[14];
z[14] = abb[39] * z[14];
z[14] = 2 * z[14] + z[19] + z[26];
z[14] = abb[2] * z[14];
z[19] = prod_pow(abb[36], 2);
z[26] = prod_pow(abb[35], 2);
z[19] = z[19] + (T(1) / T(2)) * z[26] + z[56];
z[19] = z[19] * z[102];
z[0] = abb[67] + abb[68] + z[0] + z[2] + z[4] + z[5] + z[6] + z[11] + z[12] + z[14] + z[16] + z[17] + z[18] + z[19] + z[22] + z[27] + z[29] + z[36] + z[37] + z[40] + z[41];
z[2] = -abb[49] + -z[84];
z[2] = 2 * z[2] + z[3] + -z[15] + z[33];
z[2] = abb[5] * z[2];
z[4] = -z[38] + z[59] + -z[117];
z[4] = abb[0] * z[4];
z[5] = -z[8] + z[47];
z[5] = abb[4] * z[5];
z[6] = -z[108] * z[129];
z[8] = -abb[6] * z[125];
z[11] = abb[61] * z[60];
z[2] = z[2] + z[4] + z[5] + z[6] + z[8] + z[11] + -z[32] + z[34] + z[49] + -z[55] + z[81];
z[2] = abb[31] * z[2];
z[4] = 6 * abb[52];
z[5] = z[4] + -z[50] + -z[69];
z[6] = abb[5] * z[5];
z[6] = z[6] + z[75];
z[3] = z[3] + -z[13];
z[4] = -z[3] + z[4] + -z[88];
z[4] = abb[4] * z[4];
z[8] = -z[43] + z[54];
z[8] = z[8] * z[62];
z[11] = z[48] + z[51];
z[12] = 4 * abb[47] + -6 * abb[56] + abb[54] * (T(9) / T(2)) + -z[72] + z[91];
z[12] = abb[14] * z[12];
z[4] = z[4] + z[6] + z[8] + 6 * z[11] + z[12] + -z[49] + -2 * z[74] + -z[107] + z[133];
z[4] = abb[36] * z[4];
z[8] = abb[47] * (T(-7) / T(2)) + z[9] + -z[25] + -z[44] + z[58];
z[8] = abb[5] * z[8];
z[5] = -abb[4] * z[5];
z[9] = -abb[47] + abb[55] + z[53] + -z[92];
z[9] = abb[0] * z[9];
z[5] = z[5] + z[7] + z[8] + z[9] + -z[23] + z[74];
z[5] = abb[34] * z[5];
z[7] = -z[39] * z[118];
z[7] = z[7] + -z[104];
z[7] = abb[33] * z[7];
z[3] = z[3] + z[72] + -z[80] + -z[87];
z[3] = abb[4] * z[3];
z[8] = abb[52] + -abb[54];
z[8] = z[8] * z[62];
z[9] = -abb[51] + -z[42];
z[9] = abb[10] * z[9];
z[3] = z[3] + -z[6] + z[8] + z[9] + z[52] + -z[94];
z[3] = abb[35] * z[3];
z[6] = z[39] * z[73];
z[6] = z[6] + -z[24];
z[6] = abb[32] * z[6];
z[8] = abb[23] + abb[25];
z[8] = -abb[27] + 2 * z[8] + z[128];
z[8] = abb[36] * z[8];
z[9] = abb[35] * z[20];
z[11] = abb[25] + z[73];
z[11] = abb[34] * z[11];
z[8] = z[8] + -z[9] + -z[11];
z[9] = -abb[61] * z[8];
z[11] = abb[31] * z[60];
z[8] = -z[8] + z[11];
z[8] = abb[62] * z[8];
z[2] = z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9];
z[2] = m1_set::bc<T>[0] * z[2];
z[1] = abb[2] * z[1];
z[1] = z[1] + z[28];
z[1] = abb[41] * z[1];
z[3] = -abb[43] * z[70];
z[4] = abb[44] * z[10];
z[5] = abb[42] * z[31];
z[6] = abb[41] * z[60];
z[7] = abb[43] * z[21];
z[6] = z[6] + -z[7];
z[6] = z[6] * z[39];
z[1] = abb[45] + abb[46] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_622_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("5.9774397511529179802997387369139227017165594383839600374100177879"),stof<T>("11.8367534676969739343806030894873906706023057614400113063956259684")}, std::complex<T>{stof<T>("8.2861292699155869180309086126658826292129614456267461729623720355"),stof<T>("-10.6744899155518829582490373153703271212507137453329969960568582647")}, std::complex<T>{stof<T>("24.89477730353180478042627473227128517171704751141128618134633834"),stof<T>("-2.654793899918186853379126191813447075753359855666673284926142546")}, std::complex<T>{stof<T>("26.445405116115057114924857186385649670607629023424897539415891893"),stof<T>("-9.969527398626024166443602409512637766992698017839383915171608094")}, std::complex<T>{stof<T>("4.0112116105539524936223891773353189346808160419222997293234304071"),stof<T>("2.6939930366061245724129316045958625281073645984417128132573124383")}, std::complex<T>{stof<T>("-25.663422160763816399910803394654122772488100205717523338347639103"),stof<T>("7.134780617343927633769819683563684830505822752787002819380254686")}, std::complex<T>{stof<T>("-13.436557943712315213858885826101965082466130259222992954387179377"),stof<T>("-5.81184027331700236285387486193389311149684600980470557177901155")}, std::complex<T>{stof<T>("-15.424402848368493090732070351758325673587871487030253901365631714"),stof<T>("3.957811196739249789771543087283601033484462538384355877798951219")}, std::complex<T>{stof<T>("5.1676511592729052724308604589366180423424836175978851191240361353"),stof<T>("10.2206536353663384944914136486661372469877611670750634161349713234")}, std::complex<T>{stof<T>("12.793687045791785054796812958743887296242639410114092783440208796"),stof<T>("15.41557936459251065851041702305958244255067838550080234422110859")}, std::complex<T>{stof<T>("-1.6171009194862126262983661217973796274987872711892859372350642865"),stof<T>("3.966150087348063883960288851182230484432100681518873173819811858")}, std::complex<T>{stof<T>("1.00524996874097978809576299985933667093425600048938448283835756"),stof<T>("-30.513520402711689685045874057871514243327069100966073743551296246")}, std::complex<T>{stof<T>("-1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("-1.7723221993805019694830109172223418722860636295948905476189590019"),stof<T>("7.5584392078581559681480637193844421254805645946962447139896615533")}, std::complex<T>{stof<T>("3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("-0.2599334236531954369552408109471828770159706361072270565126747634")}, std::complex<T>{stof<T>("3.858094731942462133260084176017420579813966143693243365935127612"),stof<T>("-0.2599334236531954369552408109471828770159706361072270565126747634")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[25].real()/kbase.W[25].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[88].real()/kbase.W[88].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[125].real()/kbase.W[125].real()), C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_622_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_622_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(4)) * (-v[3] + v[5]) * (-6 * v[0] + 2 * v[1] + -4 * (2 + v[3]) + 3 * m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (-v[3] + v[5])) / tend;


		return (abb[48] + abb[50] + -abb[55]) * (t * c[0] + c[1]);
	}
	{
T z[3];
z[0] = abb[31] + -abb[34];
z[1] = abb[48] + abb[50] + -abb[55];
z[0] = z[0] * z[1];
z[2] = abb[32] * z[1];
z[2] = -z[0] + z[2];
z[2] = abb[32] * z[2];
z[1] = -abb[33] * z[1];
z[0] = z[0] + z[1];
z[0] = abb[33] * z[0];
z[0] = z[0] + z[2];
return 2 * abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_622_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(4)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (-v[3] + v[5])) / tend;


		return (abb[48] + abb[50] + -abb[55]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[32] + -abb[33];
z[1] = abb[48] + abb[50] + -abb[55];
return 2 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_622_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(16)) * (v[2] + v[3]) * (-8 + 5 * v[2] + 5 * v[3] + -4 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -4 * v[5])) / prod_pow(tend, 2);
c[1] = ((1 + 2 * m1_set::bc<T>[1]) * (T(1) / T(2)) * (v[2] + v[3])) / tend;


		return (abb[48] + abb[51] + -abb[56]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[35], 2);
z[1] = -abb[35] + abb[33] * (T(-1) / T(2));
z[1] = abb[33] * z[1];
z[0] = -abb[38] + (T(3) / T(2)) * z[0] + z[1];
z[1] = abb[48] + abb[51] + -abb[56];
return abb[12] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_622_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_622_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("11.580790525380879613256674745941693541220262226672894187127604504"),stof<T>("10.394636789430696190471654211718991835555947424804313986355844731")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,69> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W19(k,dl), dlog_W23(k,dl), dlog_W26(k,dl), dlog_W28(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W89(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_4(k), f_1_7(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_16(k), f_2_19(k), f_2_4_im(k), f_2_10_im(k), f_2_11_im(k), f_2_24_im(k), T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[25].real()/k.W[25].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[88].real()/k.W[88].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[125].real()/k.W[125].real()), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_4_re(k), f_2_10_re(k), f_2_11_re(k), f_2_24_re(k), T{0}, T{0}};
abb[45] = SpDLog_f_4_622_W_16_Im(t, path, abb);
abb[46] = SpDLog_f_4_622_W_19_Im(t, path, abb);
abb[67] = SpDLog_f_4_622_W_16_Re(t, path, abb);
abb[68] = SpDLog_f_4_622_W_19_Re(t, path, abb);

                    
            return f_4_622_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_622_DLogXconstant_part(base_point<T>, kend);
	value += f_4_622_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_622_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_622_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_622_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_622_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_622_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_622_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
