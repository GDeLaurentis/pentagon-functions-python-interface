/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_453.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_453_abbreviated (const std::array<T,67>& abb) {
T z[106];
z[0] = -abb[30] + abb[33];
z[0] = m1_set::bc<T>[0] * z[0];
z[0] = -abb[41] + z[0];
z[0] = abb[1] * z[0];
z[1] = -abb[32] + abb[34];
z[2] = m1_set::bc<T>[0] * z[1];
z[3] = abb[13] * z[2];
z[0] = z[0] + -z[3];
z[4] = abb[35] * (T(1) / T(2));
z[5] = -z[1] + z[4];
z[6] = abb[31] * (T(1) / T(2));
z[7] = z[5] + -z[6];
z[7] = m1_set::bc<T>[0] * z[7];
z[8] = -abb[41] + abb[42];
z[8] = (T(1) / T(2)) * z[8];
z[7] = z[7] + z[8];
z[7] = abb[7] * z[7];
z[9] = abb[30] + -abb[32];
z[10] = m1_set::bc<T>[0] * z[9];
z[11] = abb[9] * z[10];
z[7] = z[7] + -z[11];
z[12] = -abb[30] + abb[35];
z[12] = m1_set::bc<T>[0] * z[12];
z[12] = -abb[40] + z[12];
z[13] = abb[0] * (T(1) / T(2));
z[12] = z[12] * z[13];
z[14] = -abb[34] + abb[35];
z[15] = z[9] + -z[14];
z[16] = m1_set::bc<T>[0] * z[15];
z[17] = abb[40] + -abb[42];
z[18] = z[16] + z[17];
z[19] = abb[16] * (T(1) / T(2));
z[18] = z[18] * z[19];
z[10] = z[10] + z[17];
z[20] = abb[4] * (T(1) / T(2));
z[21] = z[10] * z[20];
z[22] = abb[34] * (T(1) / T(2));
z[23] = -abb[32] + z[6];
z[24] = z[22] + z[23];
z[24] = m1_set::bc<T>[0] * z[24];
z[25] = abb[41] + abb[42];
z[26] = -z[24] + (T(1) / T(2)) * z[25];
z[27] = abb[8] * z[26];
z[28] = abb[18] * (T(1) / T(2));
z[29] = abb[19] * (T(1) / T(2));
z[30] = abb[20] + z[28] + -z[29];
z[31] = abb[43] * (T(1) / T(2));
z[32] = -z[30] * z[31];
z[33] = -abb[30] + z[6];
z[34] = abb[33] * (T(1) / T(2));
z[35] = z[33] + z[34];
z[36] = m1_set::bc<T>[0] * (T(1) / T(2));
z[37] = z[35] * z[36];
z[38] = abb[41] + -z[37];
z[38] = abb[5] * z[38];
z[39] = z[23] + z[34];
z[40] = m1_set::bc<T>[0] * z[39];
z[40] = -abb[41] + z[40];
z[41] = abb[15] * z[40];
z[32] = -z[0] + z[7] + z[12] + z[18] + -z[21] + z[27] + z[32] + z[38] + z[41];
z[32] = abb[54] * z[32];
z[38] = abb[15] * (T(1) / T(2));
z[41] = z[38] * z[40];
z[42] = abb[35] + z[33] + -z[34];
z[42] = m1_set::bc<T>[0] * z[42];
z[43] = -abb[40] + z[42];
z[44] = abb[17] * z[43];
z[41] = z[41] + (T(1) / T(2)) * z[44];
z[39] = -z[14] + z[39];
z[39] = m1_set::bc<T>[0] * z[39];
z[45] = -abb[7] * z[39];
z[46] = abb[20] + abb[21];
z[47] = -abb[19] + (T(1) / T(2)) * z[46];
z[48] = -z[31] * z[47];
z[49] = abb[30] + -abb[31];
z[50] = abb[32] + -abb[33] + z[14] + z[49];
z[50] = m1_set::bc<T>[0] * z[50];
z[50] = abb[41] + z[50];
z[51] = abb[5] * (T(1) / T(2));
z[52] = z[50] * z[51];
z[53] = abb[16] * z[16];
z[54] = abb[46] * (T(1) / T(2));
z[55] = abb[2] * abb[40];
z[56] = abb[14] * abb[41];
z[12] = -z[11] + z[12] + z[41] + z[45] + z[48] + z[52] + z[53] + z[54] + z[55] + z[56];
z[12] = abb[57] * z[12];
z[12] = z[12] + z[32];
z[32] = z[2] + z[17];
z[32] = abb[10] * z[32];
z[27] = z[27] + z[55];
z[18] = z[18] + z[21] + z[27] + -z[32];
z[21] = -abb[32] + -z[6];
z[21] = abb[30] + abb[33] * (T(-1) / T(4)) + -z[4] + (T(1) / T(2)) * z[21] + z[22];
z[21] = m1_set::bc<T>[0] * z[21];
z[32] = abb[41] * (T(1) / T(2));
z[21] = z[21] + z[32];
z[21] = abb[5] * z[21];
z[45] = -abb[30] + z[34];
z[5] = z[5] + z[45];
z[5] = m1_set::bc<T>[0] * z[5];
z[8] = -abb[40] + z[8];
z[5] = z[5] + z[8];
z[5] = abb[7] * z[5];
z[48] = -abb[31] + abb[33];
z[36] = abb[0] * z[36] * z[48];
z[48] = m1_set::bc<T>[0] * z[35];
z[25] = abb[40] + -z[25] + z[48];
z[48] = abb[6] * (T(1) / T(2));
z[25] = z[25] * z[48];
z[46] = abb[18] + -abb[19] + z[46];
z[52] = abb[43] * z[46];
z[0] = -z[0] + z[5] + -z[11] + z[18] + z[21] + z[25] + z[36] + z[41] + (T(-1) / T(4)) * z[52] + -z[54];
z[5] = abb[53] * (T(1) / T(16));
z[0] = z[0] * z[5];
z[11] = -abb[21] + z[28] + z[29];
z[21] = z[11] * z[31];
z[25] = abb[5] * z[37];
z[21] = z[21] + -z[25] + z[44];
z[7] = z[7] + z[21] + z[27];
z[25] = abb[1] * z[26];
z[27] = abb[14] * (T(1) / T(2));
z[28] = abb[41] * z[27];
z[25] = z[25] + -z[28];
z[28] = abb[4] * (T(1) / T(4));
z[10] = z[10] * z[28];
z[36] = abb[33] + -abb[35];
z[37] = z[36] + z[49];
z[37] = m1_set::bc<T>[0] * z[37];
z[37] = abb[40] + z[37];
z[41] = abb[0] * (T(1) / T(4));
z[44] = z[37] * z[41];
z[17] = z[16] + -z[17];
z[52] = abb[16] * (T(1) / T(4));
z[17] = z[17] * z[52];
z[7] = (T(1) / T(2)) * z[7] + z[10] + z[17] + -z[25] + z[44];
z[10] = abb[55] * (T(1) / T(8));
z[7] = z[7] * z[10];
z[17] = z[4] + z[33];
z[17] = m1_set::bc<T>[0] * z[17];
z[8] = z[8] + z[17];
z[8] = abb[7] * z[8];
z[17] = z[13] * z[37];
z[33] = -z[1] + z[35];
z[33] = m1_set::bc<T>[0] * z[33];
z[37] = -abb[42] + -z[33];
z[37] = abb[40] + (T(1) / T(2)) * z[37];
z[37] = abb[6] * z[37];
z[8] = z[8] + z[17] + z[18] + z[21] + z[37];
z[17] = abb[58] * (T(1) / T(16));
z[8] = z[8] * z[17];
z[18] = -z[1] + -z[6] + z[34];
z[18] = m1_set::bc<T>[0] * z[18];
z[18] = abb[42] + z[18];
z[18] = abb[23] * z[18];
z[16] = -abb[40] + abb[42] + -z[16];
z[16] = abb[22] * z[16];
z[21] = -abb[41] + z[39];
z[37] = abb[24] + abb[26];
z[21] = -z[21] * z[37];
z[39] = abb[27] * z[43];
z[43] = abb[28] * (T(1) / T(2));
z[44] = abb[43] * z[43];
z[16] = z[16] + z[18] + z[21] + z[39] + -z[44];
z[18] = abb[25] * z[26];
z[16] = (T(1) / T(2)) * z[16] + z[18];
z[18] = -abb[47] + abb[48] + abb[49] + -abb[50] + -abb[51] + abb[52];
z[21] = (T(1) / T(16)) * z[18];
z[16] = z[16] * z[21];
z[21] = abb[21] * z[42];
z[26] = abb[20] * z[40];
z[39] = abb[18] + abb[21];
z[39] = abb[40] * z[39];
z[21] = z[21] + z[26] + -z[39];
z[26] = abb[31] + -abb[32];
z[39] = -z[22] + (T(-1) / T(2)) * z[26] + -z[45];
z[39] = m1_set::bc<T>[0] * z[39];
z[32] = z[32] + z[39];
z[32] = abb[18] * z[32];
z[29] = z[29] * z[50];
z[21] = (T(-1) / T(2)) * z[21] + z[29] + z[32];
z[29] = abb[15] + abb[17];
z[29] = -abb[29] + abb[7] * (T(-1) / T(4)) + (T(1) / T(8)) * z[29];
z[32] = abb[5] * (T(1) / T(4));
z[39] = z[29] + z[32] + z[41];
z[39] = abb[43] * z[39];
z[21] = (T(1) / T(2)) * z[21] + z[39];
z[39] = abb[59] * (T(1) / T(8));
z[21] = z[21] * z[39];
z[33] = -abb[42] + z[33];
z[40] = abb[54] * (T(1) / T(2));
z[33] = z[33] * z[40];
z[35] = -z[1] + -z[35];
z[35] = m1_set::bc<T>[0] * z[35];
z[35] = -abb[42] + z[35];
z[35] = abb[40] + (T(1) / T(2)) * z[35];
z[35] = abb[55] * z[35];
z[31] = abb[59] * z[31];
z[42] = abb[40] + -abb[41] + -z[2];
z[42] = abb[57] * z[42];
z[31] = z[31] + z[33] + z[35] + (T(1) / T(2)) * z[42];
z[33] = abb[6] * (T(1) / T(16));
z[31] = z[31] * z[33];
z[24] = abb[42] * (T(1) / T(2)) + -z[24];
z[24] = abb[7] * z[24];
z[2] = -z[2] * z[48];
z[2] = z[2] + (T(1) / T(2)) * z[3] + z[24] + -z[25];
z[2] = abb[56] * z[2];
z[0] = abb[64] + abb[65] + z[0] + (T(1) / T(8)) * z[2] + z[7] + z[8] + (T(1) / T(16)) * z[12] + z[16] + z[21] + z[31];
z[2] = -abb[31] + abb[32] * (T(3) / T(2));
z[3] = abb[30] * (T(1) / T(2));
z[7] = abb[34] * (T(1) / T(4)) + z[3];
z[8] = -z[2] + z[7];
z[8] = abb[34] * z[8];
z[12] = abb[34] + abb[35] * (T(-3) / T(2)) + z[49];
z[12] = z[4] * z[12];
z[16] = -abb[31] + 3 * abb[32];
z[16] = -abb[30] + z[14] + (T(1) / T(2)) * z[16];
z[16] = abb[33] * z[16];
z[21] = abb[39] * (T(1) / T(2));
z[24] = abb[60] + z[21];
z[25] = abb[37] * (T(1) / T(2));
z[31] = abb[61] * (T(1) / T(2));
z[35] = abb[38] * (T(1) / T(2));
z[42] = prod_pow(m1_set::bc<T>[0], 2);
z[44] = (T(1) / T(6)) * z[42];
z[45] = abb[31] * abb[32];
z[50] = prod_pow(abb[32], 2);
z[2] = abb[30] * (T(1) / T(4)) + -z[2];
z[2] = abb[30] * z[2];
z[2] = z[2] + z[8] + z[12] + z[16] + z[24] + -z[25] + z[31] + -z[35] + -z[44] + -z[45] + (T(5) / T(4)) * z[50];
z[2] = abb[7] * z[2];
z[8] = z[14] + -z[49];
z[12] = abb[35] * z[8];
z[16] = abb[33] * z[15];
z[53] = abb[30] * z[26];
z[16] = z[16] + -z[53];
z[54] = -abb[60] + z[12] + z[16];
z[55] = abb[34] * z[26];
z[55] = -z[45] + z[50] + z[55];
z[56] = -abb[62] + z[55];
z[57] = abb[36] + -abb[38];
z[58] = z[54] + -z[56] + -z[57];
z[59] = z[19] * z[58];
z[60] = abb[61] + (T(5) / T(6)) * z[42] + -z[56];
z[61] = abb[8] * z[60];
z[36] = -z[14] * z[36];
z[62] = -abb[38] + z[44];
z[36] = -abb[37] + -z[36] + z[62];
z[36] = abb[3] * z[36];
z[63] = abb[7] * abb[62];
z[36] = z[36] + -z[61] + -z[63];
z[61] = z[9] + z[22];
z[64] = abb[34] * z[61];
z[65] = (T(1) / T(2)) * z[50];
z[64] = z[64] + z[65];
z[66] = abb[38] + abb[39];
z[67] = -abb[34] + z[4];
z[68] = abb[35] * z[67];
z[69] = z[66] + z[68];
z[70] = abb[60] + z[69];
z[71] = prod_pow(abb[30], 2);
z[72] = (T(1) / T(2)) * z[71];
z[73] = abb[36] + z[72];
z[74] = abb[62] + -z[64] + -z[70] + z[73];
z[20] = z[20] * z[74];
z[75] = prod_pow(abb[34], 2);
z[76] = (T(1) / T(2)) * z[75];
z[70] = -z[44] + z[70] + z[76];
z[70] = abb[2] * z[70];
z[77] = (T(1) / T(2)) * z[42];
z[78] = -z[65] + z[77];
z[79] = -abb[32] + z[22];
z[80] = abb[34] * z[79];
z[81] = z[78] + -z[80];
z[82] = -abb[39] + abb[62];
z[83] = -abb[60] + z[82];
z[84] = z[81] + z[83];
z[84] = abb[10] * z[84];
z[20] = -z[20] + (T(-1) / T(2)) * z[36] + -z[59] + z[70] + z[84];
z[59] = -z[45] + z[65];
z[84] = abb[36] + abb[61] + z[59];
z[85] = -abb[31] + z[22];
z[85] = abb[34] * z[85];
z[86] = abb[33] * z[26];
z[85] = -abb[39] + z[44] + z[53] + z[84] + -z[85] + -z[86];
z[85] = abb[1] * z[85];
z[86] = z[22] + z[49];
z[86] = abb[34] * z[86];
z[87] = -z[49] + z[67];
z[88] = abb[35] * z[87];
z[86] = z[86] + z[88];
z[88] = z[34] * z[49];
z[89] = abb[30] + abb[31] * (T(-3) / T(2));
z[89] = abb[30] * z[89];
z[62] = -abb[37] + 3 * abb[39] + -z[62] + -z[84] + z[86] + -z[88] + z[89];
z[62] = z[51] * z[62];
z[84] = z[3] + z[23];
z[89] = -z[14] + z[84];
z[89] = abb[33] * z[89];
z[90] = prod_pow(abb[35], 2);
z[91] = abb[37] + (T(1) / T(2)) * z[90];
z[89] = z[89] + z[91];
z[23] = abb[30] * z[23];
z[23] = abb[61] + z[23] + z[64] + z[77] + z[83] + -z[89];
z[23] = z[23] * z[48];
z[48] = z[3] * z[49];
z[64] = -abb[37] + z[88];
z[83] = abb[35] * z[49];
z[92] = (T(1) / T(3)) * z[42];
z[93] = abb[60] + z[92];
z[48] = -z[48] + -z[64] + z[83] + z[93];
z[94] = abb[17] * z[48];
z[95] = (T(1) / T(2)) * z[94];
z[96] = abb[31] * abb[34];
z[96] = z[59] + z[96];
z[97] = abb[30] * z[84];
z[98] = abb[36] + z[97];
z[89] = -z[89] + z[96] + z[98];
z[89] = abb[0] * z[89];
z[84] = abb[33] * z[84];
z[98] = -z[84] + z[98];
z[99] = abb[61] + z[92];
z[100] = z[98] + z[99];
z[38] = z[38] * z[100];
z[101] = -abb[13] * z[81];
z[71] = -z[50] + z[71];
z[102] = abb[34] * z[9];
z[71] = (T(1) / T(2)) * z[71] + -z[102];
z[102] = -abb[9] * z[71];
z[103] = abb[63] * (T(1) / T(4));
z[46] = z[46] * z[103];
z[104] = abb[7] + -abb[9];
z[105] = abb[36] * z[104];
z[2] = abb[66] * (T(1) / T(2)) + z[2] + -z[20] + z[23] + z[38] + z[46] + z[62] + -z[85] + z[89] + z[95] + -z[101] + z[102] + z[105];
z[2] = z[2] * z[5];
z[5] = z[9] * z[34];
z[23] = z[25] + z[31];
z[5] = z[5] + -z[23];
z[25] = z[3] + z[26];
z[25] = z[3] * z[25];
z[31] = z[4] * z[87];
z[7] = -abb[31] + abb[32] * (T(1) / T(2)) + z[7];
z[7] = abb[34] * z[7];
z[7] = z[5] + z[7] + z[21] + -z[25] + z[31] + z[45] + (T(-3) / T(4)) * z[50];
z[7] = abb[38] * (T(1) / T(4)) + (T(1) / T(2)) * z[7] + z[92];
z[7] = abb[7] * z[7];
z[7] = z[7] + (T(-1) / T(4)) * z[36];
z[25] = abb[60] + -z[53] + -z[55];
z[15] = z[15] * z[34];
z[8] = z[4] * z[8];
z[31] = abb[62] * (T(1) / T(2));
z[36] = abb[36] * (T(1) / T(2));
z[8] = abb[39] + z[8] + z[15] + (T(1) / T(2)) * z[25] + -z[31] + z[35] + -z[36];
z[8] = z[8] * z[19];
z[25] = abb[30] * z[9];
z[46] = -abb[60] + z[25] + z[96];
z[53] = z[4] + z[49];
z[53] = z[4] * z[53];
z[14] = -z[14] + z[26];
z[34] = z[14] * z[34];
z[34] = abb[37] + z[34] + -z[36] + z[44] + (T(-1) / T(2)) * z[46] + z[53];
z[13] = -z[13] * z[34];
z[46] = -abb[61] + z[44];
z[27] = z[27] * z[46];
z[36] = z[36] * z[104];
z[27] = z[27] + z[36] + (T(-1) / T(2)) * z[70];
z[53] = abb[30] * z[49];
z[62] = z[53] + z[75];
z[62] = (T(1) / T(2)) * z[62];
z[70] = -z[62] + -z[69] + z[77] + z[88];
z[75] = -z[32] * z[70];
z[87] = abb[1] * z[60];
z[28] = z[28] * z[74];
z[74] = abb[9] * (T(1) / T(2));
z[71] = -z[71] * z[74];
z[89] = -z[11] * z[103];
z[8] = -z[7] + z[8] + z[13] + z[27] + z[28] + z[71] + z[75] + (T(1) / T(2)) * z[87] + z[89] + z[95];
z[8] = z[8] * z[10];
z[9] = abb[33] * z[9];
z[9] = -z[9] + z[25] + z[69] + z[76];
z[10] = -abb[9] * z[9];
z[10] = -z[10] + z[85] + z[101];
z[13] = z[4] + -z[49];
z[13] = abb[35] * z[13];
z[16] = -abb[36] + z[16];
z[13] = z[13] + z[16] + -z[93] + -z[96];
z[13] = z[13] * z[41];
z[25] = abb[39] + z[62] + z[68];
z[41] = (T(1) / T(12)) * z[42];
z[62] = abb[33] * z[49];
z[68] = (T(1) / T(4)) * z[62];
z[25] = -abb[61] + (T(1) / T(2)) * z[25] + z[35] + -z[41] + -z[68];
z[25] = z[25] * z[51];
z[52] = z[52] * z[58];
z[30] = z[30] * z[103];
z[7] = -z[7] + (T(-1) / T(2)) * z[10] + -z[13] + z[25] + -z[28] + z[30] + z[36] + z[38] + z[52];
z[7] = abb[54] * z[7];
z[10] = abb[34] * z[49];
z[25] = abb[61] + z[45];
z[10] = -abb[37] + z[10] + z[25] + z[42] + z[62] + -z[65] + -z[83];
z[28] = -z[10] + z[73];
z[28] = z[28] * z[32];
z[30] = abb[15] * z[100];
z[30] = z[30] + z[94];
z[9] = -z[9] * z[74];
z[12] = z[12] + z[16] + -z[55] + z[66];
z[12] = z[12] * z[19];
z[6] = abb[30] * z[6];
z[6] = abb[39] + -z[6] + z[64] + z[86];
z[16] = -z[6] + z[59];
z[16] = (T(1) / T(2)) * z[16] + -z[35] + -z[92];
z[16] = abb[7] * z[16];
z[19] = z[47] * z[103];
z[9] = z[9] + z[12] + -z[13] + z[16] + z[19] + z[27] + z[28] + (T(1) / T(4)) * z[30];
z[9] = abb[66] * (T(-1) / T(32)) + (T(1) / T(8)) * z[9];
z[9] = abb[57] * z[9];
z[12] = -z[3] + z[26];
z[13] = z[3] * z[12];
z[16] = z[22] * z[61];
z[19] = z[49] + z[67];
z[4] = z[4] * z[19];
z[19] = (T(1) / T(4)) * z[50];
z[4] = z[4] + -z[5] + z[13] + z[16] + z[19] + z[21] + z[35] + z[93];
z[4] = abb[7] * z[4];
z[5] = -abb[0] * z[34];
z[13] = -z[51] * z[70];
z[16] = z[24] + -z[31] + (T(-1) / T(4)) * z[53] + z[68];
z[21] = z[22] * z[79];
z[19] = z[19] + z[21];
z[21] = -z[16] + -z[19] + z[41];
z[21] = abb[6] * z[21];
z[22] = abb[63] * (T(1) / T(2));
z[11] = -z[11] * z[22];
z[4] = z[4] + z[5] + z[11] + z[13] + -z[20] + z[21] + z[94];
z[4] = z[4] * z[17];
z[5] = abb[62] + z[54] + -z[55];
z[5] = abb[22] * z[5];
z[11] = z[56] + -z[77] + z[98];
z[11] = abb[23] * z[11];
z[13] = abb[27] * z[48];
z[6] = z[6] + z[25] + z[78];
z[6] = z[6] * z[37];
z[17] = abb[25] * z[60];
z[20] = abb[22] + z[37];
z[20] = -z[20] * z[57];
z[21] = abb[63] * z[43];
z[5] = z[5] + z[6] + -z[11] + -z[13] + z[17] + z[20] + -z[21];
z[6] = (T(-1) / T(32)) * z[18];
z[5] = z[5] * z[6];
z[6] = -z[10] + z[72];
z[6] = abb[19] * z[6];
z[10] = abb[21] * z[48];
z[11] = z[84] + -z[97] + -z[99];
z[11] = abb[20] * z[11];
z[13] = abb[19] + -abb[20];
z[13] = abb[36] * z[13];
z[6] = -z[6] + z[10] + -z[11] + -z[13];
z[10] = -abb[34] + -z[12];
z[10] = abb[30] * z[10];
z[11] = abb[33] * z[14];
z[10] = -abb[60] + -abb[61] + z[10] + z[11] + z[91];
z[10] = (T(1) / T(4)) * z[10] + -z[92];
z[10] = abb[18] * z[10];
z[11] = -abb[0] + -abb[5];
z[11] = z[11] * z[103];
z[12] = -abb[63] * z[29];
z[6] = (T(-1) / T(4)) * z[6] + z[10] + z[11] + z[12];
z[6] = z[6] * z[39];
z[10] = z[50] + -z[53];
z[10] = (T(1) / T(2)) * z[10] + -z[44] + z[80] + z[82] + z[88];
z[10] = z[10] * z[40];
z[1] = -z[1] + -z[3];
z[1] = abb[30] * z[1];
z[1] = -abb[60] + z[1];
z[1] = -abb[39] + (T(1) / T(2)) * z[1] + z[15] + z[23] + (T(1) / T(4)) * z[90] + -z[92];
z[1] = abb[57] * z[1];
z[3] = -z[16] + z[19] + (T(-5) / T(12)) * z[42];
z[3] = abb[55] * z[3];
z[11] = -abb[59] * z[22];
z[1] = z[1] + z[3] + z[10] + z[11];
z[1] = z[1] * z[33];
z[3] = -z[42] + z[55];
z[3] = abb[7] * z[3];
z[10] = -abb[6] * z[81];
z[11] = abb[14] * z[46];
z[3] = z[3] + z[10] + z[11] + -z[63] + z[87] + -z[101];
z[3] = abb[56] * z[3];
z[1] = abb[44] + abb[45] + z[1] + z[2] + (T(1) / T(16)) * z[3] + z[4] + z[5] + z[6] + (T(1) / T(8)) * z[7] + z[8] + z[9];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_453_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.26656263877907991845461319281555070891546579427963225953001072141"),stof<T>("0.07984104287839893522719076974151389202289490888349452169639869681")}, std::complex<T>{stof<T>("0.44498258313119427072674025127778667676124684482877749052607252016"),stof<T>("-0.61767294778093917886350818673996289985916949706738809856634302397")}, std::complex<T>{stof<T>("0.48198701984387537070976822042785697668542757033430351711570659107"),stof<T>("-0.64180221181897536994389210785041656909479591289700449973039851008")}, std::complex<T>{stof<T>("0.71139208474349293419636000264885361495551924473021304202603627221"),stof<T>("-0.90547547218371322812297658047063936132169816395277400480248082064")}, std::complex<T>{stof<T>("0.4242116869373315153413235887550239702749553982142713045403518298"),stof<T>("-0.62804728997277610517257005834036313921943037176387585492700447484")}, std::complex<T>{stof<T>("-0.06056477147887062478373321163429803283291100248108698106600679549"),stof<T>("0.36839802510583079515909488377541018596343621481210405529255070113")}, std::complex<T>{stof<T>("-0.17335515916075297192037827901654095267976292784665177052813618989"),stof<T>("0.4283232412825870257892029896608431146211087430042532888348288131")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_453_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_453_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_453_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(256)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (8 + v[0] + 3 * v[1] + v[2] + -3 * v[3] + -4 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = ((T(-1) / T(32)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[53] + abb[55] + abb[57]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[34] + abb[35] * (T(1) / T(2));
z[0] = abb[35] * z[0];
z[1] = prod_pow(abb[34], 2);
z[0] = abb[38] + z[0] + (T(1) / T(2)) * z[1];
z[1] = abb[53] + abb[55] + abb[57];
return abb[11] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_453_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(128)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[55] + abb[57] + abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[55] + abb[57] + abb[58];
z[1] = abb[33] + -abb[35];
return abb[12] * m1_set::bc<T>[0] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_453_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(256)) * (v[1] + v[2] + -v[3] + -v[4]) * (8 + 12 * v[0] + -v[1] + 3 * v[2] + 5 * v[3] + -3 * v[4] + -2 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + -4 * v[5])) / prod_pow(tend, 2);
c[1] = ((-1 + m1_set::bc<T>[1]) * (T(-1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[55] + abb[57] + abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[33] + -abb[35];
z[1] = -abb[30] + abb[31];
z[0] = z[0] * z[1];
z[0] = abb[37] + z[0];
z[1] = -abb[55] + -abb[57] + -abb[58];
return abb[12] * (T(1) / T(16)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_453_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.08185470931319214302098396445149171010454903495629637998694045074"),stof<T>("-0.30396595199793837434803493689148570053049865254484109109333483208")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,67> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W20(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_7(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_24_im(k), T{0}, T{0}, T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_24_re(k), T{0}, T{0}, T{0}};
abb[44] = SpDLog_f_4_453_W_17_Im(t, path, abb);
abb[45] = SpDLog_f_4_453_W_20_Im(t, path, abb);
abb[46] = SpDLogQ_W_74(k,dl,dlr).imag();
abb[64] = SpDLog_f_4_453_W_17_Re(t, path, abb);
abb[65] = SpDLog_f_4_453_W_20_Re(t, path, abb);
abb[66] = SpDLogQ_W_74(k,dl,dlr).real();

                    
            return f_4_453_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_453_DLogXconstant_part(base_point<T>, kend);
	value += f_4_453_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_453_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_453_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_453_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_453_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_453_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_453_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
