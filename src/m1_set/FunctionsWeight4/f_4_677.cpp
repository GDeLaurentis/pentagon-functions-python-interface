/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_677.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_677_abbreviated (const std::array<T,41>& abb) {
T z[131];
z[0] = 6 * abb[26];
z[1] = 6 * abb[28];
z[2] = z[0] + z[1];
z[3] = 9 * abb[31];
z[4] = -z[2] + z[3];
z[5] = 4 * abb[25];
z[6] = 3 * abb[27];
z[7] = z[5] + z[6];
z[8] = abb[29] + -abb[32];
z[9] = 5 * abb[30];
z[10] = z[4] + -z[7] + z[8] + z[9];
z[10] = abb[9] * z[10];
z[11] = -abb[25] + abb[32];
z[12] = -abb[27] + z[11];
z[13] = abb[4] * z[12];
z[14] = abb[1] * z[12];
z[13] = 6 * z[13] + 12 * z[14];
z[15] = abb[27] + z[11];
z[4] = abb[29] + z[4] + -2 * z[15];
z[4] = abb[3] * z[4];
z[16] = abb[27] + z[3] + -10 * z[11];
z[16] = abb[5] * z[16];
z[17] = abb[34] + abb[35];
z[18] = abb[12] * z[17];
z[19] = abb[27] + -abb[32];
z[20] = abb[30] + z[19];
z[21] = abb[8] * z[20];
z[22] = abb[13] * z[17];
z[4] = z[4] + -z[10] + z[13] + z[16] + -z[18] + 5 * z[21] + z[22];
z[16] = prod_pow(abb[18], 2);
z[4] = z[4] * z[16];
z[23] = 4 * abb[26];
z[24] = -abb[32] + z[23];
z[25] = 6 * abb[31];
z[26] = 3 * abb[30];
z[27] = 4 * abb[28];
z[7] = z[7] + z[24] + -z[25] + -z[26] + z[27];
z[7] = abb[9] * z[7];
z[28] = 4 * abb[30];
z[29] = -abb[25] + z[28];
z[30] = 3 * abb[32];
z[31] = z[6] + -z[25] + -z[29] + z[30];
z[31] = abb[4] * z[31];
z[32] = -z[15] + z[25];
z[33] = -z[23] + -z[27] + z[32];
z[33] = abb[3] * z[33];
z[34] = abb[11] * z[17];
z[35] = -z[22] + z[34];
z[36] = 2 * abb[30];
z[37] = -abb[25] + z[19] + z[36];
z[37] = abb[10] * z[37];
z[38] = z[35] + z[37];
z[39] = abb[26] + abb[28];
z[40] = z[11] + -z[39];
z[40] = abb[6] * z[40];
z[41] = 6 * z[14];
z[42] = z[18] + -z[41];
z[7] = z[7] + z[21] + -z[31] + z[33] + -z[38] + 4 * z[40] + -z[42];
z[31] = 2 * abb[39];
z[33] = -z[7] * z[31];
z[43] = 2 * abb[33];
z[44] = 2 * abb[27];
z[45] = z[43] + z[44];
z[46] = 7 * abb[30];
z[47] = 15 * abb[25];
z[48] = 4 * abb[32];
z[49] = -z[0] + z[45] + -z[46] + z[47] + -z[48];
z[49] = abb[0] * z[49];
z[50] = 4 * abb[33];
z[51] = 4 * abb[31];
z[52] = z[50] + z[51];
z[53] = 4 * abb[27] + z[52];
z[54] = 12 * abb[30];
z[55] = 22 * abb[28];
z[56] = 5 * abb[25];
z[57] = -53 * abb[26] + 10 * abb[32] + z[53] + z[54] + -z[55] + z[56];
z[57] = abb[2] * z[57];
z[58] = 6 * abb[32] + -z[1];
z[59] = 5 * abb[26];
z[60] = z[5] + z[26] + -z[58] + z[59];
z[60] = abb[6] * z[60];
z[61] = 2 * abb[31];
z[62] = -z[27] + z[61];
z[63] = abb[33] + z[62];
z[64] = -abb[25] + z[26];
z[65] = 10 * abb[26];
z[66] = abb[32] + z[63] + 2 * z[64] + -z[65];
z[66] = abb[9] * z[66];
z[67] = -abb[25] + abb[26];
z[68] = abb[3] * z[67];
z[38] = -z[38] + -z[66] + z[68];
z[29] = 2 * z[29];
z[68] = -11 * abb[26] + abb[27] + z[29] + z[63];
z[69] = 2 * abb[4];
z[68] = z[68] * z[69];
z[38] = -2 * z[38] + z[49] + -z[57] + -z[60] + -z[68];
z[49] = abb[37] * z[38];
z[57] = 12 * abb[26];
z[60] = -abb[33] + z[1];
z[68] = z[57] + z[60];
z[70] = 5 * abb[31];
z[71] = z[8] + z[29] + -z[68] + z[70];
z[71] = abb[9] * z[71];
z[72] = z[22] + z[34];
z[71] = z[71] + -z[72];
z[73] = 7 * abb[26];
z[74] = 3 * abb[31];
z[75] = 3 * abb[25];
z[76] = abb[29] + -z[27] + -z[73] + z[74] + z[75];
z[76] = abb[3] * z[76];
z[77] = 8 * abb[30];
z[78] = abb[27] + z[77];
z[79] = abb[26] + z[30] + z[62] + -z[78];
z[79] = z[69] * z[79];
z[80] = 2 * abb[29];
z[81] = 2 * abb[28];
z[82] = z[80] + z[81];
z[83] = 2 * abb[26];
z[84] = z[82] + z[83];
z[85] = abb[27] + z[61];
z[86] = 7 * z[11] + -z[84] + -z[85];
z[87] = abb[5] * z[86];
z[88] = 2 * z[17];
z[88] = abb[12] * z[88];
z[71] = 2 * z[71] + z[76] + z[79] + -z[87] + -z[88];
z[76] = abb[38] * z[71];
z[10] = z[10] + -z[72];
z[72] = abb[25] + -abb[30];
z[79] = z[44] + -z[61] + 5 * z[72];
z[79] = z[69] * z[79];
z[32] = z[32] + -z[84];
z[32] = abb[3] * z[32];
z[84] = 7 * abb[31] + -z[44];
z[87] = 5 * z[11] + -z[84];
z[87] = abb[5] * z[87];
z[10] = -2 * z[10] + 9 * z[14] + z[32] + -z[79] + -z[87];
z[32] = -abb[36] * z[10];
z[79] = 2 * abb[25];
z[87] = -abb[33] + z[0];
z[89] = -z[6] + -z[26] + z[51] + -z[79] + z[87];
z[89] = abb[13] * z[89];
z[90] = 2 * abb[32];
z[91] = 3 * abb[26];
z[85] = -abb[33] + z[85] + -z[90] + z[91];
z[85] = abb[12] * z[85];
z[92] = -abb[0] + abb[3];
z[17] = z[17] * z[92];
z[92] = -z[30] + z[51];
z[93] = -abb[33] + z[26];
z[94] = abb[25] + z[92] + z[93];
z[94] = abb[11] * z[94];
z[17] = z[17] + 2 * z[85] + z[89] + z[94];
z[17] = abb[19] * z[17];
z[5] = -27 * abb[26] + 5 * abb[29] + 15 * abb[31] + z[5] + z[19] + z[43] + -z[55];
z[55] = -abb[38] * z[5];
z[85] = 8 * abb[31];
z[89] = -z[15] + z[85];
z[94] = 10 * abb[28] + z[65];
z[95] = 4 * abb[29];
z[96] = -z[89] + z[94] + -z[95];
z[97] = abb[36] * z[96];
z[55] = z[55] + z[97];
z[55] = abb[7] * z[55];
z[4] = z[4] + z[17] + z[32] + z[33] + z[49] + z[55] + z[76];
z[17] = 7 * abb[32];
z[32] = 18 * abb[31] + -z[6] + -z[17] + z[54] + -z[56] + z[80] + -z[94];
z[32] = abb[9] * z[32];
z[33] = 11 * abb[32];
z[49] = abb[27] + z[80];
z[47] = -z[2] + z[28] + z[33] + -z[47] + -z[49] + -z[61];
z[47] = abb[0] * z[47];
z[54] = z[33] + -z[75];
z[55] = z[2] + z[51] + -z[54] + z[78];
z[55] = z[55] * z[69];
z[76] = z[61] + z[80];
z[78] = z[76] + -z[83];
z[15] = -z[15] + z[78] + -z[81];
z[15] = abb[3] * z[15];
z[94] = 2 * abb[5];
z[86] = z[86] * z[94];
z[97] = 2 * abb[7];
z[98] = z[96] * z[97];
z[99] = abb[29] + abb[32];
z[100] = abb[30] + z[91];
z[101] = -3 * abb[28] + abb[31] + z[99] + -z[100];
z[102] = 4 * abb[2];
z[101] = z[101] * z[102];
z[15] = z[15] + z[32] + 2 * z[37] + -16 * z[40] + z[47] + -z[55] + -z[86] + z[98] + z[101];
z[32] = abb[40] * z[15];
z[40] = -8 * abb[32] + z[0] + z[25] + z[49] + z[93];
z[40] = abb[9] * z[40];
z[47] = -abb[25] + abb[27];
z[55] = -z[47] + z[74] + -z[81] + -z[91];
z[86] = 2 * abb[3];
z[55] = z[55] * z[86];
z[98] = -abb[33] + z[27];
z[101] = z[57] + z[98];
z[102] = z[44] + z[75];
z[103] = 15 * abb[32];
z[104] = -10 * abb[31] + -z[9] + -z[101] + -z[102] + z[103];
z[104] = abb[4] * z[104];
z[99] = abb[25] + -z[0] + z[62] + z[99];
z[99] = z[97] * z[99];
z[27] = -z[27] + z[48] + -z[75];
z[105] = z[27] + -z[59];
z[106] = 2 * abb[6];
z[107] = z[105] * z[106];
z[108] = -abb[31] + z[11];
z[108] = abb[5] * z[108];
z[109] = 8 * z[108];
z[110] = 2 * z[21];
z[111] = z[109] + -z[110];
z[35] = -2 * z[35] + z[40] + -z[42] + z[55] + -z[99] + z[104] + -z[107] + -z[111];
z[35] = abb[16] * z[35];
z[40] = 7 * abb[25];
z[55] = -9 * abb[32] + -z[9] + z[40] + z[44] + z[51] + z[101];
z[55] = abb[4] * z[55];
z[101] = -z[51] + z[102];
z[9] = z[9] + z[30];
z[68] = -z[9] + z[68] + z[101];
z[68] = abb[9] * z[68];
z[104] = 2 * z[22];
z[112] = z[68] + z[104];
z[109] = z[107] + z[109];
z[113] = -z[1] + z[36];
z[114] = 5 * abb[32];
z[115] = -abb[31] + z[114];
z[116] = abb[25] + abb[27];
z[117] = -15 * abb[26] + abb[33] + z[113] + z[115] + z[116];
z[118] = abb[2] * z[117];
z[119] = z[30] + z[47];
z[120] = -z[25] + z[81] + z[119];
z[120] = abb[3] * z[120];
z[121] = 9 * abb[30];
z[23] = 16 * abb[25] + -abb[27] + abb[33] + z[23] + -z[58] + -z[121];
z[23] = abb[0] * z[23];
z[18] = -z[18] + z[23] + -z[41] + z[55] + z[109] + -z[112] + -2 * z[118] + z[120];
z[18] = abb[17] * z[18];
z[23] = -z[43] + z[82];
z[55] = z[28] + z[47];
z[120] = 14 * abb[26];
z[122] = z[17] + -z[23] + -z[25] + z[55] + -z[120];
z[122] = abb[9] * z[122];
z[123] = 17 * abb[26];
z[124] = 12 * abb[28];
z[125] = -12 * abb[32] + z[28] + z[75] + z[123] + z[124];
z[125] = abb[6] * z[125];
z[126] = z[69] * z[117];
z[127] = z[81] + -z[116];
z[24] = z[24] + z[127];
z[24] = abb[3] * z[24];
z[24] = z[24] + z[88] + z[104];
z[122] = -z[24] + z[99] + z[122] + -z[125] + -z[126];
z[122] = abb[15] * z[122];
z[125] = z[30] + z[36];
z[126] = 5 * abb[27];
z[128] = -z[2] + -z[56] + z[85] + z[125] + -z[126];
z[128] = abb[9] * z[128];
z[89] = -z[2] + z[89];
z[89] = abb[3] * z[89];
z[13] = z[13] + -z[88] + z[89] + z[104] + -z[111] + -z[128];
z[13] = abb[18] * z[13];
z[89] = 10 * abb[30];
z[53] = -69 * abb[26] + -30 * abb[28] + 18 * abb[32] + z[40] + z[53] + z[89];
z[53] = abb[15] * z[53];
z[111] = 2 * abb[16];
z[117] = z[111] * z[117];
z[53] = z[53] + -z[117];
z[53] = abb[2] * z[53];
z[53] = z[13] + z[53] + -z[122];
z[62] = z[43] + z[62];
z[117] = 8 * abb[26];
z[49] = -z[40] + -z[49] + -z[62] + z[117] + z[125];
z[49] = abb[15] * z[49];
z[122] = z[40] + -z[114];
z[125] = -abb[27] + z[2] + -z[36] + z[122];
z[125] = abb[18] * z[125];
z[49] = z[49] + -z[125];
z[128] = -abb[30] + abb[33];
z[78] = -z[78] + -z[122] + -z[128];
z[78] = abb[16] * z[78];
z[78] = -z[49] + z[78];
z[78] = abb[0] * z[78];
z[18] = z[18] + z[35] + -z[53] + z[78];
z[35] = abb[29] + z[44];
z[72] = -abb[31] + z[35] + z[65] + 3 * z[72] + z[98] + -z[114];
z[72] = abb[9] * z[72];
z[67] = -z[35] + -z[67] + z[74];
z[67] = abb[3] * z[67];
z[78] = 3 * z[14];
z[104] = z[78] + z[104];
z[122] = -abb[27] + -abb[31] + 2 * z[11];
z[129] = abb[5] * z[122];
z[67] = z[34] + z[67] + z[72] + z[104] + z[129];
z[72] = 9 * abb[26];
z[129] = z[26] + z[63] + -z[72] + z[90] + -z[116];
z[129] = z[69] * z[129];
z[130] = 39 * abb[26];
z[101] = -18 * abb[28] + 16 * abb[32] + -z[36] + z[50] + z[101] + -z[130];
z[101] = abb[2] * z[101];
z[27] = -z[27] + -z[59] + z[89];
z[27] = abb[6] * z[27];
z[27] = z[27] + z[101];
z[8] = -abb[28] + -abb[33] + -z[8] + z[44] + -z[56] + z[100];
z[56] = 2 * abb[0];
z[8] = z[8] * z[56];
z[8] = z[8] + z[27] + 2 * z[67] + z[88] + z[129];
z[8] = abb[14] * z[8];
z[8] = z[8] + 2 * z[18];
z[8] = abb[14] * z[8];
z[18] = 18 * abb[26] + -z[43];
z[67] = -z[1] + -z[18] + z[77] + z[119];
z[67] = abb[9] * z[67];
z[77] = z[11] + -z[51] + z[59] + z[98];
z[100] = z[77] * z[97];
z[55] = -z[0] + z[55] + -z[92];
z[101] = z[55] * z[69];
z[24] = -z[24] + z[67] + z[100] + -z[101] + z[107];
z[24] = abb[15] * z[24];
z[13] = -z[13] + z[24];
z[12] = -z[12] + -z[61] + z[81] + z[83];
z[12] = abb[3] * z[12];
z[24] = z[44] + -z[64] + z[92] + -z[98];
z[24] = abb[4] * z[24];
z[12] = z[12] + z[24] + z[42] + z[100] + -z[110] + -z[112];
z[12] = abb[16] * z[12];
z[24] = -z[30] + z[75];
z[42] = abb[27] + z[81];
z[64] = -z[0] + -z[24] + z[28] + -z[42];
z[67] = -abb[0] * z[64];
z[100] = z[92] + -z[127];
z[100] = abb[3] * z[100];
z[101] = abb[2] * z[55];
z[107] = abb[28] + -z[47];
z[107] = z[69] * z[107];
z[67] = z[67] + z[78] + z[100] + z[101] + z[107];
z[67] = abb[17] * z[67];
z[78] = -z[81] + z[83];
z[19] = 9 * abb[25] + z[19] + -z[28] + -z[61] + -z[78];
z[19] = abb[15] * z[19];
z[19] = z[19] + z[125];
z[28] = z[42] + -z[61] + z[93];
z[28] = abb[16] * z[28];
z[28] = -z[19] + z[28];
z[28] = abb[0] * z[28];
z[26] = abb[27] + z[26] + z[48];
z[26] = -21 * abb[26] + 2 * z[26] + -z[60] + -z[70];
z[42] = abb[15] * z[26];
z[55] = abb[16] * z[55];
z[60] = z[42] + -z[55];
z[60] = abb[2] * z[60];
z[12] = z[12] + -z[13] + z[28] + 2 * z[60] + z[67];
z[28] = 2 * abb[17];
z[12] = z[12] * z[28];
z[60] = z[68] + z[104];
z[29] = -abb[27] + -abb[33] + z[29] + -z[30] + z[74] + z[81];
z[29] = abb[4] * z[29];
z[45] = -8 * abb[28] + z[45] + z[79] + z[115] + -z[123];
z[45] = abb[7] * z[45];
z[67] = -z[44] + z[90];
z[68] = z[59] + -z[67] + -z[75];
z[68] = abb[3] * z[68];
z[29] = -3 * z[21] + z[29] + z[34] + z[45] + z[60] + z[68];
z[29] = abb[16] * z[29];
z[13] = z[13] + z[29];
z[13] = z[13] * z[111];
z[29] = abb[30] + -z[0] + z[30] + z[63] + -z[102];
z[29] = abb[16] * z[29];
z[19] = z[19] + z[29];
z[19] = abb[16] * z[19];
z[29] = z[78] + z[80] + z[92] + -z[116];
z[34] = -abb[38] * z[29];
z[45] = z[6] + z[80];
z[9] = 8 * abb[25] + -z[9];
z[63] = -abb[31] + 2 * z[9] + -z[45] + z[57] + z[124];
z[68] = abb[36] * z[63];
z[40] = abb[28] + -abb[33] + z[36] + -z[40] + z[59] + z[67];
z[59] = prod_pow(abb[15], 2);
z[40] = z[40] * z[59];
z[2] = -z[2] + -z[9] + z[35];
z[2] = z[2] * z[16];
z[9] = -z[20] * z[31];
z[2] = z[2] + z[9] + z[19] + z[34] + z[40] + z[68];
z[2] = z[2] * z[56];
z[9] = abb[28] + -abb[32];
z[16] = 20 * z[9] + -z[36] + z[75] + z[130];
z[16] = abb[6] * z[16];
z[3] = z[3] + -z[17] + -z[44] + z[73];
z[3] = z[3] * z[97];
z[19] = -abb[25] + abb[28] + -abb[31] + z[83];
z[19] = z[19] * z[86];
z[19] = z[19] + z[22] + -z[66];
z[26] = z[26] * z[69];
z[3] = z[3] + z[16] + 2 * z[19] + z[26] + z[88];
z[3] = z[3] * z[59];
z[16] = 16 * abb[30];
z[19] = 28 * abb[26] + 12 * abb[31] + -z[6] + -z[16] + z[23] + -z[54];
z[23] = abb[38] * z[19];
z[26] = -2 * z[42] + z[55];
z[26] = z[26] * z[111];
z[31] = z[75] + z[89];
z[31] = -111 * abb[26] + 8 * abb[27] + -42 * abb[28] + 26 * abb[32] + 3 * z[31] + z[52];
z[31] = z[31] * z[59];
z[23] = 2 * z[23] + z[26] + z[31];
z[23] = abb[2] * z[23];
z[2] = z[2] + z[3] + 2 * z[4] + z[8] + z[12] + z[13] + z[23] + z[32];
z[3] = abb[31] * (T(5) / T(3));
z[4] = -abb[25] + abb[26] * (T(-10) / T(3)) + abb[29] * (T(-5) / T(3)) + abb[28] * (T(-4) / T(3)) + z[3] + z[30] + -z[44] + (T(1) / T(3)) * z[128];
z[4] = abb[9] * z[4];
z[4] = z[4] + (T(-2) / T(3)) * z[22];
z[8] = abb[29] + z[39];
z[8] = abb[31] * (T(35) / T(3)) + (T(22) / T(3)) * z[8] + (T(-94) / T(3)) * z[11] + z[126];
z[8] = z[8] * z[94];
z[12] = 13 * abb[25];
z[13] = abb[27] + -z[12] + z[16];
z[13] = -22 * abb[26] + -28 * abb[28] + 20 * abb[31] + abb[29] * (T(44) / T(3)) + (T(1) / T(3)) * z[13] + -z[30] + -z[50];
z[13] = abb[2] * z[13];
z[16] = abb[31] * (T(10) / T(3));
z[17] = 10 * abb[29] + abb[27] * (T(-31) / T(3)) + abb[26] * (T(-26) / T(3)) + abb[28] * (T(-10) / T(3)) + abb[25] * (T(-5) / T(3)) + -z[16] + z[17];
z[17] = abb[3] * z[17];
z[9] = 36 * abb[26] + abb[25] * (T(83) / T(3)) + (T(82) / T(3)) * z[9] + -z[121];
z[9] = z[9] * z[106];
z[23] = -77 * abb[25] + -34 * abb[30];
z[23] = 36 * abb[32] + abb[31] * (T(-50) / T(3)) + abb[28] * (T(-49) / T(3)) + abb[27] * (T(-5) / T(3)) + abb[33] * (T(-4) / T(3)) + (T(1) / T(3)) * z[23] + -z[120];
z[23] = z[23] * z[69];
z[26] = abb[25] + -z[48];
z[3] = -9 * abb[29] + abb[33] * (T(2) / T(3)) + abb[28] * (T(40) / T(3)) + abb[26] * (T(53) / T(3)) + z[3] + (T(5) / T(3)) * z[26];
z[3] = z[3] * z[97];
z[16] = -7 * abb[27] + -54 * abb[28] + 58 * abb[30] + abb[25] * (T(-295) / T(3)) + abb[26] * (T(-194) / T(3)) + abb[33] * (T(8) / T(3)) + abb[29] * (T(20) / T(3)) + abb[32] * (T(145) / T(3)) + z[16];
z[16] = abb[0] * z[16];
z[3] = z[3] + 4 * z[4] + z[8] + z[9] + z[13] + z[14] + z[16] + z[17] + (T(40) / T(3)) * z[21] + z[23] + (T(16) / T(3)) * z[37];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[2] = 2 * z[2] + z[3];
z[2] = 4 * z[2];
z[3] = -abb[28] + -z[24] + z[36] + -z[61] + -z[91];
z[3] = z[3] * z[69];
z[4] = -abb[7] * z[77];
z[8] = -abb[6] * z[105];
z[9] = -11 * abb[25] + -abb[27] + abb[31] + z[46] + z[58] + -z[117];
z[9] = abb[0] * z[9];
z[13] = -abb[25] + -abb[26] + z[67];
z[13] = abb[3] * z[13];
z[3] = z[3] + z[4] + z[8] + z[9] + z[13] + z[21] + z[60] + -4 * z[108] + z[118];
z[3] = z[3] * z[28];
z[4] = z[75] + z[81];
z[8] = -z[4] + -z[6] + -z[36] + z[43] + -z[85] + -z[95] + z[103] + -z[120];
z[8] = abb[9] * z[8];
z[9] = 6 * abb[30];
z[11] = -z[6] + z[9] + -z[11] + -z[57] + z[62] + z[95];
z[11] = abb[0] * z[11];
z[13] = abb[25] + abb[32];
z[13] = -3 * z[13] + -z[25] + z[82] + z[117] + z[126];
z[13] = abb[3] * z[13];
z[14] = z[36] + z[47] + z[72] + z[98] + -z[115];
z[14] = z[14] * z[69];
z[16] = 4 * z[22] + z[41];
z[17] = -z[94] * z[122];
z[8] = z[8] + z[11] + z[13] + z[14] + -z[16] + z[17] + -z[27] + z[99] + -z[110];
z[8] = abb[14] * z[8];
z[11] = -z[6] + -z[18] + z[54] + -z[76] + z[113];
z[11] = abb[9] * z[11];
z[0] = -z[0] + z[4] + -z[6] + -z[51] + z[114];
z[0] = abb[3] * z[0];
z[4] = -z[64] * z[69];
z[6] = abb[29] + z[84] + z[87] + -z[114];
z[6] = z[6] * z[97];
z[0] = z[0] + z[4] + z[6] + z[11] + -z[16] + 6 * z[21] + z[109];
z[0] = abb[16] * z[0];
z[1] = z[1] + -z[9] + z[12] + -z[33] + z[45] + z[65];
z[1] = abb[16] * z[1];
z[1] = z[1] + z[49];
z[1] = abb[0] * z[1];
z[0] = z[0] + z[1] + z[3] + z[8] + z[53];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[21] * z[38];
z[3] = -abb[0] * z[20];
z[3] = z[3] + -z[7];
z[3] = abb[23] * z[3];
z[4] = -abb[7] * z[5];
z[5] = -abb[0] * z[29];
z[6] = abb[2] * z[19];
z[4] = z[4] + z[5] + z[6] + z[71];
z[4] = abb[22] * z[4];
z[5] = abb[0] * z[63];
z[6] = abb[7] * z[96];
z[5] = z[5] + z[6] + -z[10];
z[5] = abb[20] * z[5];
z[0] = z[0] + z[1] + 2 * z[3] + z[4] + z[5];
z[1] = abb[24] * z[15];
z[0] = 2 * z[0] + z[1];
z[0] = 8 * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_677_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("637.0761889038387725731974651331162271310219419679747166480820512"),stof<T>("4031.1781285682026535995632717109914662445940241837646947072174181")}, std::complex<T>{stof<T>("401.5295976983805568002252058686292962091736753628017218020459795"),stof<T>("3803.7018106044276899516769382611433357348161907807136737440094343")}, std::complex<T>{stof<T>("690.44355177179759468203220784868110066378692825656862971210767189"),stof<T>("726.62417573608429684206349387714672211231562358006426356180903845")}, std::complex<T>{stof<T>("473.3480059880724584620391561949540588132644128986817338106273582"),stof<T>("2710.1078375540594347154792388613364989379260056769728015403007763")}, std::complex<T>{stof<T>("23.52192830417613104223088940300867749149465382856570481087931659"),stof<T>("-995.91750269459288656917693128734520395072702579482445546870831536")}, std::complex<T>{stof<T>("501.4905125071612986623249388644376857874876522696704168372847709"),stof<T>("-4715.0925752653135740968765066960074120119385380175634042669215099")}, std::complex<T>{stof<T>("-120.5651929427380146125937997754146866541271382820509468568289034"),stof<T>("-2031.1349842422935102494813370363471260699689321922704469893969333")}, std::complex<T>{stof<T>("-809.66025662488348620631704473173895387408395439246967537694742398"),stof<T>("-258.74066958299629353739789248157218070964584926264665551186840532")}, std::complex<T>{stof<T>("-257.08803649642468336739140893949019644033490230929544609983801932"),stof<T>("-150.93885677026104120148657193321871031989982200729550713213616085")}, stof<T>("-27.798199965403973130008609371046810056707915075587306640714604869"), stof<T>("-27.798199965403973130008609371046810056707915075587306640714604869")};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[45].real()/kbase.W[45].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_677_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_677_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("2817.491278756908939899510450726241651740607345999699291432071195"),stof<T>("-2572.511054660116824599426000048445764395827697412662768419257969")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,41> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), dlog_W69(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_11(k), f_2_23(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[45].real()/k.W[45].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_4_677_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_677_DLogXconstant_part(base_point<T>, kend);
	value += f_4_677_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_677_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_677_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_677_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_677_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_677_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_677_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
