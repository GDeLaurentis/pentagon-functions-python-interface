/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_615.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_615_abbreviated (const std::array<T,62>& abb) {
T z[111];
z[0] = abb[47] + abb[50];
z[1] = abb[46] * (T(2) / T(3)) + abb[43] * (T(4) / T(3));
z[2] = abb[44] * (T(1) / T(3));
z[3] = abb[48] * (T(1) / T(2));
z[4] = abb[45] + abb[49];
z[0] = (T(1) / T(2)) * z[0] + -z[1] + -z[2] + z[3] + (T(-1) / T(3)) * z[4];
z[0] = abb[2] * z[0];
z[4] = 3 * abb[49];
z[2] = 5 * abb[50] + abb[45] * (T(1) / T(3)) + -z[2] + -z[4];
z[1] = abb[42] * (T(1) / T(6)) + -z[1] + (T(1) / T(4)) * z[2] + -z[3];
z[1] = abb[4] * z[1];
z[2] = abb[45] + -abb[49];
z[5] = abb[42] + z[2];
z[6] = abb[13] * z[5];
z[7] = -abb[51] + abb[52];
z[8] = abb[17] * z[7];
z[6] = z[6] + -z[8];
z[8] = (T(-1) / T(2)) * z[6];
z[9] = abb[44] + abb[45];
z[10] = abb[49] + z[9];
z[11] = 3 * abb[50];
z[12] = z[10] + -z[11];
z[13] = 2 * abb[43] + abb[46];
z[14] = (T(1) / T(2)) * z[12] + z[13];
z[15] = abb[14] * z[14];
z[16] = -abb[49] + abb[50];
z[17] = abb[47] + z[16];
z[18] = -z[13] + z[17];
z[18] = abb[9] * z[18];
z[19] = -abb[15] * z[7];
z[20] = z[18] + (T(-3) / T(4)) * z[19];
z[21] = abb[44] * (T(5) / T(2));
z[22] = 2 * abb[45] + z[13];
z[23] = -abb[49] + z[22];
z[24] = z[21] + -z[23];
z[3] = -z[3] + (T(1) / T(3)) * z[24];
z[3] = abb[6] * z[3];
z[24] = abb[46] + abb[49];
z[25] = abb[42] + -abb[44];
z[24] = abb[47] * (T(-13) / T(2)) + abb[50] * (T(-1) / T(4)) + abb[43] * (T(14) / T(3)) + (T(7) / T(3)) * z[24] + (T(25) / T(12)) * z[25];
z[24] = abb[0] * z[24];
z[16] = z[9] + z[16];
z[16] = -abb[48] + (T(1) / T(2)) * z[16];
z[26] = abb[3] * z[16];
z[27] = abb[47] + z[2];
z[27] = abb[11] * z[27];
z[28] = 3 * z[27];
z[29] = abb[16] * z[7];
z[30] = abb[44] + abb[49];
z[31] = -abb[45] + -abb[50] + z[30];
z[32] = abb[8] * z[31];
z[33] = abb[1] * z[5];
z[34] = abb[45] * (T(-13) / T(3)) + z[4];
z[34] = -abb[50] + abb[44] * (T(5) / T(3)) + (T(1) / T(2)) * z[34];
z[34] = abb[43] * (T(-2) / T(3)) + abb[46] * (T(-1) / T(3)) + abb[42] * (T(1) / T(12)) + (T(1) / T(2)) * z[34];
z[34] = abb[7] * z[34];
z[0] = z[0] + z[1] + z[3] + z[8] + z[15] + 3 * z[20] + z[24] + (T(-1) / T(2)) * z[26] + -z[28] + 2 * z[29] + (T(-5) / T(4)) * z[32] + (T(10) / T(3)) * z[33] + z[34];
z[1] = prod_pow(m1_set::bc<T>[0], 2);
z[0] = z[0] * z[1];
z[3] = abb[19] + -abb[24];
z[20] = abb[27] * (T(1) / T(2));
z[3] = z[3] * z[20];
z[20] = abb[21] + abb[23];
z[24] = abb[24] + z[20];
z[26] = abb[19] + z[24];
z[34] = (T(1) / T(2)) * z[26];
z[35] = abb[31] * z[34];
z[36] = -abb[30] + abb[31];
z[37] = abb[20] * z[36];
z[38] = abb[30] * z[24];
z[3] = z[3] + -z[35] + -z[37] + z[38];
z[3] = abb[27] * z[3];
z[35] = -abb[19] + z[24];
z[39] = abb[31] * (T(1) / T(2));
z[35] = z[35] * z[39];
z[39] = abb[20] * abb[30];
z[35] = z[35] + -z[38] + -z[39];
z[38] = abb[22] * abb[29];
z[34] = abb[20] + z[34];
z[39] = abb[27] * z[34];
z[38] = z[35] + z[38] + z[39];
z[38] = abb[32] * z[38];
z[39] = -abb[35] + -abb[36];
z[39] = z[20] * z[39];
z[40] = abb[27] + -abb[31];
z[41] = abb[19] + abb[20];
z[42] = -z[40] * z[41];
z[43] = (T(1) / T(2)) * z[20];
z[44] = abb[22] + z[43];
z[44] = abb[29] * z[44];
z[42] = -z[42] + z[44];
z[42] = abb[29] * z[42];
z[44] = z[20] + z[41];
z[44] = abb[33] * z[44];
z[45] = abb[30] * abb[31];
z[46] = prod_pow(abb[30], 2);
z[45] = -abb[35] + z[45] + -z[46];
z[47] = abb[57] + z[45];
z[47] = abb[20] * z[47];
z[48] = z[43] * z[46];
z[49] = abb[24] * abb[57];
z[24] = abb[34] * z[24];
z[50] = abb[25] * (T(1) / T(2));
z[51] = abb[60] * z[50];
z[41] = abb[22] + z[41];
z[52] = abb[59] * z[41];
z[3] = z[3] + z[24] + z[38] + z[39] + -z[42] + z[44] + z[47] + -z[48] + z[49] + z[51] + z[52];
z[24] = abb[27] + -abb[30];
z[24] = z[20] * z[24];
z[38] = abb[22] * abb[32];
z[39] = abb[28] * z[43];
z[42] = abb[22] + z[20];
z[43] = abb[29] * z[42];
z[24] = z[24] + z[37] + z[38] + z[39] + -z[43];
z[37] = 3 * abb[28];
z[24] = z[24] * z[37];
z[37] = abb[20] + abb[22];
z[26] = z[26] + (T(5) / T(2)) * z[37];
z[1] = z[1] * z[26];
z[1] = z[1] + 3 * z[3] + -z[24];
z[3] = 3 * abb[58];
z[24] = z[3] * z[42];
z[24] = z[1] + z[24];
z[26] = abb[54] + abb[55];
z[37] = (T(1) / T(2)) * z[26];
z[24] = z[24] * z[37];
z[37] = abb[53] + abb[56];
z[38] = (T(1) / T(2)) * z[37];
z[1] = z[1] * z[38];
z[39] = abb[49] + abb[50];
z[43] = -z[9] + z[39];
z[43] = -abb[47] + (T(1) / T(2)) * z[43];
z[44] = abb[5] * z[43];
z[47] = (T(1) / T(2)) * z[44];
z[48] = abb[43] + abb[46] * (T(1) / T(2));
z[17] = (T(1) / T(2)) * z[17] + -z[48];
z[17] = abb[9] * z[17];
z[49] = -z[17] + z[47];
z[49] = 3 * z[49];
z[51] = 7 * abb[45];
z[52] = 5 * abb[49];
z[53] = z[11] + z[51] + -z[52];
z[54] = 5 * abb[44];
z[55] = z[53] + -z[54];
z[55] = z[48] + (T(1) / T(4)) * z[55];
z[55] = abb[7] * z[55];
z[56] = 7 * abb[49];
z[57] = 7 * abb[44];
z[58] = 17 * abb[45] + -z[11] + -z[56] + -z[57];
z[58] = 5 * abb[43] + abb[46] * (T(5) / T(2)) + (T(1) / T(4)) * z[58];
z[58] = abb[6] * z[58];
z[53] = z[53] + z[57];
z[57] = 3 * abb[48];
z[59] = z[48] + (T(1) / T(4)) * z[53] + -z[57];
z[60] = -abb[4] * z[59];
z[10] = z[10] + z[11];
z[61] = 3 * abb[47];
z[62] = -z[13] + z[61];
z[10] = (T(1) / T(2)) * z[10] + -z[57] + -z[62];
z[63] = abb[2] * z[10];
z[55] = -z[28] + z[49] + z[55] + z[58] + z[60] + z[63];
z[55] = abb[28] * z[55];
z[58] = abb[4] * z[14];
z[22] = z[22] + -z[30];
z[60] = abb[7] * z[22];
z[63] = 4 * abb[43] + 2 * abb[46];
z[64] = z[12] + z[63];
z[65] = abb[2] * z[64];
z[66] = (T(3) / T(2)) * z[32];
z[67] = z[65] + z[66];
z[22] = abb[6] * z[22];
z[58] = 2 * z[22] + -z[58] + z[60] + z[67];
z[60] = abb[32] * z[58];
z[68] = z[18] + -z[44];
z[69] = 3 * z[68];
z[53] = 6 * abb[48] + -z[13] + (T(-1) / T(2)) * z[53];
z[70] = -abb[6] * z[53];
z[70] = z[69] + z[70];
z[70] = abb[30] * z[70];
z[71] = abb[7] * z[14];
z[72] = abb[6] * z[14];
z[69] = z[69] + z[71] + -z[72];
z[73] = -abb[27] * z[69];
z[74] = 3 * abb[45];
z[75] = -abb[50] + -z[30] + z[74];
z[75] = z[13] + (T(1) / T(2)) * z[75];
z[75] = abb[6] * z[75];
z[76] = abb[7] * (T(1) / T(2));
z[77] = z[31] * z[76];
z[75] = -z[68] + z[75] + -z[77];
z[77] = (T(1) / T(2)) * z[32];
z[78] = z[75] + z[77];
z[79] = 3 * abb[29];
z[80] = -z[78] * z[79];
z[53] = -abb[30] * z[53];
z[81] = abb[27] * z[14];
z[57] = 2 * abb[44] + -z[57];
z[23] = z[23] + z[57];
z[82] = 2 * abb[31];
z[83] = -z[23] * z[82];
z[53] = z[53] + z[81] + z[83];
z[53] = abb[4] * z[53];
z[83] = abb[31] * z[64];
z[84] = abb[30] * z[64];
z[85] = -z[83] + 2 * z[84];
z[86] = abb[27] * z[64];
z[86] = -z[85] + z[86];
z[86] = abb[2] * z[86];
z[87] = abb[6] * z[23];
z[88] = 2 * z[87];
z[89] = -z[71] + -z[88];
z[89] = abb[31] * z[89];
z[90] = abb[7] * z[84];
z[91] = 3 * abb[3];
z[91] = z[16] * z[91];
z[36] = z[36] * z[91];
z[36] = z[36] + z[53] + z[55] + z[60] + z[70] + z[73] + z[80] + -z[86] + z[89] + z[90];
z[36] = abb[28] * z[36];
z[53] = 2 * abb[49];
z[55] = -z[9] + z[53] + -z[62];
z[60] = abb[27] + abb[28];
z[60] = -2 * z[60];
z[60] = z[55] * z[60];
z[70] = 5 * abb[45];
z[73] = -z[11] + z[70];
z[54] = -z[54] + z[56] + -z[73];
z[54] = -6 * abb[47] + z[13] + (T(1) / T(2)) * z[54];
z[54] = abb[30] * z[54];
z[56] = abb[31] * z[14];
z[9] = -abb[50] + z[4] + -z[9];
z[9] = -2 * abb[47] + (T(1) / T(2)) * z[9] + z[13];
z[79] = z[9] * z[79];
z[80] = -abb[32] * z[14];
z[54] = z[54] + z[56] + z[60] + z[79] + z[80];
z[54] = abb[28] * z[54];
z[60] = 3 * abb[57];
z[79] = z[3] + -z[60];
z[79] = z[9] * z[79];
z[80] = abb[30] * z[14];
z[89] = z[56] + -z[80];
z[89] = abb[27] * z[89];
z[92] = abb[29] * z[14];
z[92] = -z[81] + z[92];
z[56] = -z[56] + -z[92];
z[56] = abb[29] * z[56];
z[55] = abb[36] * z[55];
z[92] = z[80] + z[92];
z[93] = abb[32] * z[92];
z[94] = 3 * abb[33];
z[95] = -3 * abb[35] + z[94];
z[43] = z[43] * z[95];
z[95] = z[14] * z[46];
z[96] = -abb[31] * z[80];
z[43] = z[43] + z[54] + -2 * z[55] + z[56] + z[79] + z[89] + z[93] + z[95] + z[96];
z[43] = abb[12] * z[43];
z[54] = abb[44] * (T(1) / T(2));
z[55] = abb[50] * (T(3) / T(2));
z[56] = z[54] + -z[55];
z[79] = abb[49] + z[56];
z[89] = abb[42] * (T(1) / T(4));
z[93] = -z[48] + (T(-1) / T(2)) * z[79] + z[89];
z[93] = abb[0] * z[93];
z[56] = abb[45] + z[56];
z[95] = z[48] + (T(1) / T(2)) * z[56] + z[89];
z[96] = -abb[4] * z[95];
z[97] = abb[42] * (T(1) / T(2));
z[98] = -z[48] + z[97];
z[99] = -abb[44] + abb[45];
z[99] = -z[98] + (T(1) / T(2)) * z[99];
z[99] = abb[7] * z[99];
z[54] = abb[45] + abb[49] * (T(-1) / T(2)) + z[48] + -z[54];
z[54] = abb[6] * z[54];
z[100] = abb[2] * z[14];
z[101] = (T(1) / T(2)) * z[33];
z[102] = (T(3) / T(4)) * z[29];
z[54] = z[54] + z[93] + z[96] + z[99] + z[100] + -z[101] + z[102];
z[54] = abb[32] * z[54];
z[96] = abb[45] + z[4];
z[99] = -abb[44] + z[11];
z[96] = (T(1) / T(2)) * z[96] + -z[99];
z[96] = z[13] + -z[89] + (T(1) / T(2)) * z[96];
z[96] = abb[7] * z[96];
z[100] = z[6] + z[19];
z[103] = (T(1) / T(4)) * z[100];
z[12] = (T(1) / T(4)) * z[12] + z[48];
z[104] = abb[14] * z[12];
z[105] = z[103] + z[104];
z[106] = 2 * z[33];
z[96] = z[72] + z[96] + -3 * z[105] + z[106];
z[96] = abb[31] * z[96];
z[105] = -abb[6] + 3 * abb[14];
z[105] = z[80] * z[105];
z[90] = -z[90] + z[105];
z[105] = abb[31] * z[93];
z[86] = z[86] + z[90] + z[96] + -z[105];
z[79] = z[13] + z[79] + -z[97];
z[79] = abb[0] * z[79];
z[96] = (T(3) / T(2)) * z[29];
z[22] = -z[22] + z[96];
z[107] = -abb[7] * z[5];
z[107] = z[22] + -z[33] + -z[66] + -z[79] + z[107];
z[107] = abb[29] * z[107];
z[108] = abb[49] + z[74];
z[108] = -z[99] + (T(1) / T(2)) * z[108];
z[89] = z[13] + z[89] + (T(1) / T(2)) * z[108];
z[89] = abb[7] * z[89];
z[103] = z[103] + -z[104];
z[103] = 3 * z[103];
z[89] = z[89] + z[103] + -z[106];
z[108] = -z[89] + -z[93];
z[108] = abb[27] * z[108];
z[70] = -z[4] + z[70] + -z[99];
z[70] = abb[42] + z[48] + (T(1) / T(4)) * z[70];
z[70] = abb[31] * z[70];
z[109] = abb[29] * z[5];
z[70] = z[70] + -z[80] + (T(1) / T(2)) * z[109];
z[52] = z[52] + -z[74] + -z[99];
z[52] = -abb[42] + z[48] + (T(1) / T(4)) * z[52];
z[110] = -abb[27] * z[52];
z[110] = -z[70] + z[110];
z[110] = abb[4] * z[110];
z[54] = z[54] + -z[86] + z[107] + z[108] + z[110];
z[54] = abb[32] * z[54];
z[21] = -abb[49] + z[21] + -z[55];
z[107] = abb[42] * (T(5) / T(4));
z[21] = (T(1) / T(2)) * z[21] + -z[48] + z[61] + -z[107];
z[21] = abb[0] * z[21];
z[48] = 3 * abb[44];
z[11] = z[2] + z[11] + -z[48];
z[11] = -abb[42] + (T(1) / T(2)) * z[11];
z[11] = z[11] * z[76];
z[30] = -z[30] + z[73];
z[30] = z[13] + (T(1) / T(4)) * z[30];
z[30] = abb[6] * z[30];
z[11] = z[11] + z[21] + z[30] + z[49] + z[66] + -z[101] + -z[102];
z[11] = abb[29] * z[11];
z[30] = z[5] * z[76];
z[49] = z[30] + -4 * z[33] + -z[72] + -z[79] + (T(3) / T(2)) * z[100];
z[49] = z[40] * z[49];
z[11] = z[11] + z[49];
z[11] = abb[29] * z[11];
z[39] = -z[39] + z[48] + z[74];
z[39] = -2 * abb[48] + z[13] + (T(1) / T(2)) * z[39];
z[39] = abb[34] * z[39];
z[16] = abb[35] * z[16];
z[16] = z[16] + z[39];
z[39] = z[46] * z[59];
z[16] = 3 * z[16] + z[39];
z[39] = abb[31] * z[52];
z[48] = z[13] + z[56] + z[97];
z[49] = abb[27] * z[48];
z[39] = z[39] + z[49] + -z[80];
z[39] = abb[27] * z[39];
z[23] = abb[30] * z[23];
z[49] = abb[42] + -abb[45] + -z[13] + -z[57];
z[49] = abb[31] * z[49];
z[23] = z[23] + z[49];
z[23] = z[23] * z[82];
z[49] = abb[33] + abb[59];
z[49] = (T(3) / T(2)) * z[49];
z[49] = z[5] * z[49];
z[40] = -z[5] * z[40];
z[40] = 2 * z[40] + (T(5) / T(4)) * z[109];
z[40] = abb[29] * z[40];
z[52] = abb[36] * z[14];
z[23] = -z[16] + z[23] + z[39] + z[40] + z[49] + z[52];
z[23] = abb[4] * z[23];
z[39] = (T(1) / T(2)) * z[19];
z[40] = z[39] + -z[44];
z[6] = (T(-1) / T(4)) * z[6] + -z[17] + (T(1) / T(2)) * z[40] + -z[104];
z[40] = abb[7] * z[95];
z[49] = abb[6] * z[12];
z[6] = 3 * z[6] + z[21] + z[33] + z[40] + z[49] + -z[96];
z[6] = abb[27] * z[6];
z[21] = abb[31] * z[89];
z[6] = z[6] + z[21] + z[90] + z[105];
z[6] = abb[27] * z[6];
z[10] = z[10] * z[46];
z[21] = -z[83] + z[84];
z[21] = abb[31] * z[21];
z[40] = z[81] + -z[85];
z[40] = abb[27] * z[40];
z[49] = -abb[36] + -z[60];
z[49] = z[49] * z[64];
z[52] = -abb[34] * z[64];
z[56] = -abb[47] + -abb[48] + abb[50];
z[56] = abb[35] * z[56];
z[52] = z[52] + z[56];
z[10] = z[10] + z[21] + z[40] + z[49] + 3 * z[52];
z[10] = abb[2] * z[10];
z[21] = abb[6] * z[31];
z[21] = z[21] + -z[100];
z[31] = (T(1) / T(2)) * z[29];
z[21] = (T(-1) / T(2)) * z[21] + z[30] + -z[31] + -z[33] + z[77];
z[30] = abb[59] * z[21];
z[17] = z[17] + z[47];
z[17] = z[17] * z[46];
z[40] = abb[35] * z[44];
z[47] = abb[34] * z[15];
z[17] = z[17] + -z[30] + z[40] + z[47];
z[30] = abb[44] + -abb[50];
z[30] = abb[47] + (T(1) / T(2)) * z[30] + -z[97];
z[30] = abb[0] * z[30];
z[30] = z[8] + z[30] + -z[31] + z[33] + -z[44];
z[30] = z[30] * z[94];
z[16] = -abb[6] * z[16];
z[31] = -abb[7] * z[48];
z[31] = (T(-3) / T(2)) * z[19] + z[31] + z[33] + -z[87];
z[31] = abb[31] * z[31];
z[40] = abb[7] * z[80];
z[47] = abb[30] * z[88];
z[31] = z[31] + z[40] + z[47];
z[31] = abb[31] * z[31];
z[40] = z[15] + -z[68] + -z[71];
z[47] = z[40] * z[60];
z[48] = -abb[36] * z[69];
z[38] = z[38] * z[42];
z[38] = z[38] + -z[78];
z[3] = z[3] * z[38];
z[38] = abb[15] + abb[17];
z[38] = z[5] * z[38];
z[49] = abb[16] + -abb[18];
z[49] = z[12] * z[49];
z[38] = (T(1) / T(4)) * z[38] + z[49];
z[49] = abb[0] + abb[7];
z[49] = abb[13] * (T(1) / T(4)) + (T(3) / T(4)) * z[49];
z[52] = abb[4] + z[49];
z[52] = z[7] * z[52];
z[52] = -z[38] + z[52];
z[56] = 6 * abb[26];
z[57] = -z[7] * z[56];
z[52] = 3 * z[52] + z[57];
z[52] = abb[60] * z[52];
z[57] = abb[34] * z[14];
z[12] = z[12] * z[46];
z[12] = z[12] + -3 * z[57];
z[12] = abb[7] * z[12];
z[46] = prod_pow(abb[31], 2) * z[79];
z[45] = abb[34] + -z[45];
z[45] = z[45] * z[91];
z[28] = prod_pow(abb[29], 2) * z[28];
z[0] = abb[61] + z[0] + z[1] + z[3] + z[6] + z[10] + z[11] + z[12] + z[16] + 3 * z[17] + z[23] + z[24] + z[28] + z[30] + z[31] + z[36] + z[43] + z[45] + z[46] + z[47] + z[48] + z[52] + z[54];
z[1] = -z[4] + z[51];
z[1] = (T(1) / T(2)) * z[1] + -z[99];
z[1] = (T(1) / T(2)) * z[1] + z[13] + z[107];
z[1] = abb[7] * z[1];
z[3] = -abb[45] + z[4] + -z[99];
z[3] = (T(1) / T(4)) * z[3] + -z[98];
z[3] = abb[4] * z[3];
z[1] = z[1] + z[3] + -z[22] + -z[33] + z[67] + -z[93] + z[103];
z[1] = abb[32] * z[1];
z[3] = -z[8] + z[32] + z[39] + -z[68];
z[2] = abb[44] * (T(3) / T(2)) + -z[2] + -z[55] + z[97];
z[2] = abb[7] * z[2];
z[4] = -abb[49] + -z[25] + z[62];
z[6] = 2 * abb[0];
z[4] = z[4] * z[6];
z[2] = z[2] + -3 * z[3] + -z[4] + z[22] + -6 * z[27] + 5 * z[33];
z[2] = abb[29] * z[2];
z[3] = z[15] + z[18] + -z[19] + z[29] + z[44];
z[6] = abb[49] + -z[74] + z[99];
z[6] = -abb[42] + (T(1) / T(2)) * z[6] + -z[13];
z[6] = abb[7] * z[6];
z[3] = 3 * z[3] + -z[4] + z[6] + z[106];
z[3] = abb[27] * z[3];
z[4] = abb[42] + -z[53] + -z[63] + z[99];
z[4] = abb[27] * z[4];
z[4] = z[4] + -z[70];
z[4] = abb[4] * z[4];
z[1] = z[1] + z[2] + z[3] + z[4] + -z[86];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[4] * z[5];
z[2] = (T(1) / T(2)) * z[2] + -z[21];
z[2] = abb[39] * z[2];
z[3] = z[40] + -z[65];
z[3] = abb[37] * z[3];
z[2] = z[2] + z[3];
z[3] = abb[38] * z[42];
z[4] = abb[40] * z[50];
z[5] = abb[39] * z[41];
z[6] = abb[20] + abb[24];
z[8] = abb[37] * z[6];
z[10] = abb[28] * m1_set::bc<T>[0];
z[11] = abb[22] * z[10];
z[3] = z[3] + z[4] + z[5] + z[8] + -z[11];
z[4] = abb[27] * z[6];
z[4] = z[4] + z[35];
z[5] = abb[22] + z[34];
z[6] = abb[32] * (T(1) / T(2));
z[5] = z[5] * z[6];
z[6] = abb[19] + abb[20] + z[20];
z[6] = abb[22] + (T(1) / T(2)) * z[6];
z[6] = abb[29] * z[6];
z[4] = (T(1) / T(2)) * z[4] + -z[5] + z[6];
z[4] = m1_set::bc<T>[0] * z[4];
z[3] = (T(1) / T(2)) * z[3] + z[4];
z[4] = z[26] + z[37];
z[4] = 3 * z[4];
z[3] = z[3] * z[4];
z[4] = z[10] * z[58];
z[5] = -z[66] + -3 * z[75];
z[5] = abb[38] * z[5];
z[6] = m1_set::bc<T>[0] * z[92];
z[8] = -abb[37] + abb[38];
z[8] = z[8] * z[9];
z[9] = -z[10] * z[14];
z[6] = z[6] + 3 * z[8] + z[9];
z[6] = abb[12] * z[6];
z[8] = z[7] * z[49];
z[8] = z[8] + -z[38];
z[9] = 3 * abb[4] + -z[56];
z[7] = z[7] * z[9];
z[7] = z[7] + 3 * z[8];
z[7] = abb[40] * z[7];
z[1] = abb[41] + z[1] + 3 * z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_615_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("10.269512493905512545637669731671240915263504226021180975992455577"),stof<T>("-58.554694814178373260477368471541400794702884064369990307851491817")}, std::complex<T>{stof<T>("3.864737663879488353265753160505984425095431179935268491788284964"),stof<T>("-47.706147699074941751564270035516253070252721807743778987143419911")}, std::complex<T>{stof<T>("-18.268293638099721913065127805053267735940324506964062333523932316"),stof<T>("30.85558242216324742086829337306750850632132959989719188070629115")}, std::complex<T>{stof<T>("9.878152653184990155537763548562580744476206783623852519577901265"),stof<T>("-19.92935011442469201137941997400066842068505199455458671913663283")}, std::complex<T>{stof<T>("1.932368831939744176632876580252992212547715589967634245894142482"),stof<T>("-23.853073849537470875782135017758126535126360903871889493571709955")}, std::complex<T>{stof<T>("-20.592022310759988479797910568414920119275337539329025035832629111"),stof<T>("93.334000971454399545748376888366367415465522573584484962992860093")}, std::complex<T>{stof<T>("-7.9457838212452459789048869683095885319284911936562182736837587825"),stof<T>("-3.9237237351127788644027150437574581144413089093173027744350771258")}, std::complex<T>{stof<T>("16.335924806159977736432251224800275523392608916996428087629789834"),stof<T>("-7.002508572625776545086158355309381971194968696025302387134581194")}, std::complex<T>{stof<T>("-16.512200902804655404527292095446812870862691003762646777629453187"),stof<T>("24.063143698697434894401075584092701938234885903411332628943547409")}, std::complex<T>{stof<T>("16.512200902804655404527292095446812870862691003762646777629453187"),stof<T>("-24.063143698697434894401075584092701938234885903411332628943547409")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[127].real()/kbase.W[127].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_615_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_615_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(8)) * (v[2] + v[3]) * (-24 + 5 * v[2] + v[3] + -4 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -8 * v[5])) / prod_pow(tend, 2);
c[1] = (3 * (-1 + 2 * m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;


		return (2 * abb[43] + abb[44] + abb[45] + abb[46] + -abb[48] + -abb[50]) * (t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[3];
z[0] = -abb[30] + abb[31];
z[1] = 2 * abb[43] + abb[44] + abb[45] + abb[46] + -abb[48] + -abb[50];
z[0] = z[0] * z[1];
z[2] = abb[28] * z[1];
z[0] = z[0] + z[2];
z[0] = abb[31] * z[0];
z[1] = abb[34] * z[1];
z[2] = -abb[30] * z[2];
z[0] = z[0] + z[1] + z[2];
return 3 * abb[10] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_615_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_615_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-32.796205031715046932025671777143204995350881675711372250712685776"),stof<T>("48.465065132210457705915045996313609616872406605829586846137138145")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,62> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W15(k,dl), dlog_W19(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_3(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[14].real()/k.W[14].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[127].real()/k.W[127].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_25_re(k), T{0}};
abb[41] = SpDLog_f_4_615_W_19_Im(t, path, abb);
abb[61] = SpDLog_f_4_615_W_19_Re(t, path, abb);

                    
            return f_4_615_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_615_DLogXconstant_part(base_point<T>, kend);
	value += f_4_615_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_615_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_615_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_615_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_615_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_615_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_615_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
