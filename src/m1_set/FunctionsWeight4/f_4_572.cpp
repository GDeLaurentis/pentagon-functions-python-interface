/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_572.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_572_abbreviated (const std::array<T,20>& abb) {
T z[33];
z[0] = 3 * abb[15];
z[1] = -abb[12] + z[0];
z[2] = abb[13] + abb[14];
z[3] = 2 * z[2];
z[4] = z[1] + -z[3];
z[5] = abb[2] * z[4];
z[6] = abb[12] * (T(1) / T(2));
z[7] = z[2] + z[6];
z[8] = z[0] + -z[7];
z[8] = abb[0] * z[8];
z[9] = -abb[15] + z[2];
z[6] = z[6] + -z[9];
z[10] = abb[5] * z[6];
z[1] = z[1] + z[2];
z[11] = 2 * abb[1];
z[12] = -z[1] * z[11];
z[13] = -abb[12] + -abb[15] + z[3];
z[14] = abb[3] * z[13];
z[15] = 3 * z[14];
z[8] = z[5] + z[8] + -9 * z[10] + z[12] + -z[15];
z[10] = prod_pow(abb[6], 2);
z[8] = z[8] * z[10];
z[7] = -3 * abb[16] + -z[0] + 5 * z[7];
z[7] = abb[0] * z[7];
z[12] = 3 * abb[5];
z[6] = z[6] * z[12];
z[16] = -abb[12] + z[2];
z[17] = -z[11] * z[16];
z[5] = z[5] + z[6] + z[7] + z[15] + z[17];
z[5] = abb[8] * z[5];
z[6] = 2 * abb[15];
z[7] = z[3] + -z[6];
z[17] = abb[12] + -z[7];
z[18] = z[12] * z[17];
z[19] = 2 * abb[2];
z[20] = z[4] * z[19];
z[3] = abb[12] + z[3];
z[21] = abb[0] * z[3];
z[18] = z[18] + -z[20] + z[21];
z[22] = 4 * abb[1];
z[23] = z[16] * z[22];
z[23] = z[18] + z[23];
z[23] = abb[6] * z[23];
z[24] = z[11] + z[19];
z[25] = -abb[12] + 4 * z[2];
z[0] = z[0] + -z[25];
z[24] = z[0] * z[24];
z[4] = abb[0] * z[4];
z[4] = -z[4] + z[15];
z[15] = z[4] + z[24];
z[24] = 2 * abb[7];
z[26] = -z[15] * z[24];
z[5] = z[5] + z[23] + z[26];
z[5] = abb[8] * z[5];
z[17] = abb[5] * z[17];
z[23] = 6 * abb[15];
z[2] = 22 * z[2];
z[26] = abb[12] * (T(-5) / T(2)) + z[2];
z[26] = -z[23] + (T(1) / T(3)) * z[26];
z[26] = abb[2] * z[26];
z[27] = -abb[15] + (T(-2) / T(3)) * z[3];
z[27] = abb[0] * z[27];
z[26] = z[14] + 4 * z[17] + z[26] + z[27];
z[27] = prod_pow(m1_set::bc<T>[0], 2);
z[26] = z[26] * z[27];
z[15] = abb[6] * z[15];
z[28] = z[0] * z[11];
z[25] = -z[19] * z[25];
z[21] = z[21] + z[25] + z[28];
z[21] = abb[7] * z[21];
z[15] = 2 * z[15] + z[21];
z[15] = abb[7] * z[15];
z[12] = -abb[0] + z[12];
z[11] = z[11] + (T(1) / T(2)) * z[12];
z[10] = z[10] * z[11];
z[11] = abb[0] + abb[5];
z[21] = abb[8] * z[11];
z[25] = -abb[6] * z[11];
z[25] = (T(-1) / T(2)) * z[21] + z[25];
z[25] = abb[8] * z[25];
z[10] = z[10] + z[25];
z[25] = -4 * abb[5] + abb[1] * (T(-13) / T(2)) + abb[0] * (T(5) / T(2));
z[25] = z[25] * z[27];
z[29] = 3 * abb[18];
z[30] = -z[11] * z[29];
z[10] = 3 * z[10] + z[25] + z[30];
z[10] = abb[17] * z[10];
z[7] = -abb[12] + -z[7];
z[7] = abb[0] * z[7];
z[7] = -z[7] + 2 * z[14];
z[25] = -z[7] + -z[17];
z[29] = -z[25] * z[29];
z[30] = 2 * z[9];
z[31] = -abb[18] * z[30];
z[32] = -abb[9] * z[13];
z[31] = z[31] + z[32];
z[2] = abb[12] * (T(-17) / T(2)) + z[2];
z[2] = (T(1) / T(3)) * z[2] + z[6];
z[2] = z[2] * z[27];
z[2] = z[2] + 6 * z[31];
z[2] = abb[1] * z[2];
z[6] = abb[0] + -abb[2];
z[6] = z[6] * z[27];
z[27] = 3 * abb[9];
z[31] = -abb[0] * z[27];
z[32] = -abb[0] + z[19];
z[32] = prod_pow(abb[7], 2) * z[32];
z[6] = (T(1) / T(2)) * z[6] + z[31] + 3 * z[32];
z[6] = abb[16] * z[6];
z[7] = z[7] * z[27];
z[27] = -abb[18] * z[13];
z[30] = -abb[9] * z[30];
z[27] = z[27] + z[30];
z[27] = abb[2] * z[27];
z[2] = abb[19] + z[2] + z[5] + z[6] + z[7] + z[8] + z[10] + z[15] + z[26] + 6 * z[27] + z[29];
z[5] = -z[13] * z[19];
z[6] = -z[9] * z[22];
z[5] = z[5] + z[6] + -z[25];
z[5] = abb[10] * z[5];
z[6] = m1_set::bc<T>[0] * z[22];
z[7] = -m1_set::bc<T>[0] * z[12];
z[7] = -z[6] + z[7];
z[7] = abb[6] * z[7];
z[8] = -abb[10] * z[11];
z[9] = m1_set::bc<T>[0] * z[21];
z[7] = z[7] + z[8] + z[9];
z[7] = abb[17] * z[7];
z[5] = z[5] + z[7];
z[3] = z[3] + -z[23];
z[3] = abb[0] * z[3];
z[3] = z[3] + 6 * z[14] + 9 * z[17] + -z[20];
z[3] = m1_set::bc<T>[0] * z[3];
z[1] = z[1] * z[6];
z[1] = z[1] + z[3];
z[1] = abb[6] * z[1];
z[0] = -z[0] * z[19];
z[0] = z[0] + -z[4] + -z[28];
z[0] = m1_set::bc<T>[0] * z[0] * z[24];
z[3] = -m1_set::bc<T>[0] * z[18];
z[4] = -z[6] * z[16];
z[3] = z[3] + z[4];
z[3] = abb[8] * z[3];
z[0] = abb[11] + z[0] + z[1] + z[3] + 3 * z[5];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_572_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("36.18811896518412984154477816327548000061128578274149383716349675"),stof<T>("23.804770464212639615177564217742599709771248512019765020635643168")}, std::complex<T>{stof<T>("-25.939781124589168626687344890670796988737420733150786202792068998"),stof<T>("-103.584543653342934470442735526639333744968634716480864576059726822")}, std::complex<T>{stof<T>("-25.939781124589168626687344890670796988737420733150786202792068998"),stof<T>("-103.584543653342934470442735526639333744968634716480864576059726822")}, std::complex<T>{stof<T>("57.247709265415794917153769187476229112974382190341890718910917046"),stof<T>("7.1376765436575340780056788778817626952290036145407493535551149")}, std::complex<T>{stof<T>("-67.496047106010756132011202460080912124848247239932598353282344798"),stof<T>("72.642096645472760777259492431014971339968382589920350201868968755")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[24].real()/kbase.W[24].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_572_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_572_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(16)) * (v[1] + v[2] + -v[3] + -v[4]) * (8 + 4 * v[0] + -3 * v[1] + v[2] + 3 * v[3] + -v[4] + -4 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + -4 * v[5])) / prod_pow(tend, 2);
c[1] = (3 * (1 + 2 * m1_set::bc<T>[1]) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[12] + -2 * (abb[13] + abb[14] + -abb[15]) + -abb[16]) * (-2 * t * c[0] + c[1]) * (T(-1) / T(2));
	}
	{
T z[2];
z[0] = prod_pow(abb[7], 2);
z[1] = prod_pow(abb[8], 2);
z[0] = -abb[9] + z[0] + -z[1];
z[1] = abb[14] + -abb[15];
z[1] = -abb[12] + 2 * abb[13] + abb[16] + 2 * z[1];
return 3 * abb[4] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_572_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_572_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-66.450122433971321290351053509670182938543007223669909756065766748"),stof<T>("106.45666854519571547154178917244714993693045549733528966880285692")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,20> abb = {dlog_W4(k,dl), dl[2], dlog_W8(k,dl), dlog_W10(k,dl), dlog_W20(k,dl), dlog_W25(k,dl), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_12_im(k), T{0}, rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[24].real()/k.W[24].real()), f_2_12_re(k), T{0}};
abb[11] = SpDLog_f_4_572_W_20_Im(t, path, abb);
abb[19] = SpDLog_f_4_572_W_20_Re(t, path, abb);

                    
            return f_4_572_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_572_DLogXconstant_part(base_point<T>, kend);
	value += f_4_572_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_572_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_572_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_572_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_572_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_572_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_572_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
