/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_354.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_354_abbreviated (const std::array<T,56>& abb) {
T z[80];
z[0] = abb[28] * m1_set::bc<T>[0];
z[0] = abb[39] + z[0];
z[1] = abb[29] * m1_set::bc<T>[0];
z[1] = -z[0] + z[1];
z[2] = abb[0] * z[1];
z[3] = abb[32] + -abb[33];
z[4] = -abb[29] + z[3];
z[5] = m1_set::bc<T>[0] * z[4];
z[6] = -abb[41] + z[0];
z[5] = z[5] + z[6];
z[7] = abb[14] * z[5];
z[8] = abb[33] * m1_set::bc<T>[0];
z[6] = -z[6] + z[8];
z[8] = abb[7] * z[6];
z[9] = -abb[30] + abb[32];
z[9] = abb[6] * z[9];
z[10] = -m1_set::bc<T>[0] * z[9];
z[11] = abb[30] + -abb[31];
z[12] = -abb[29] + z[11];
z[13] = abb[33] + z[12];
z[14] = -m1_set::bc<T>[0] * z[13];
z[14] = -abb[41] + z[14];
z[14] = abb[4] * z[14];
z[15] = abb[4] + -abb[6];
z[16] = -abb[40] * z[15];
z[17] = m1_set::bc<T>[0] * z[3];
z[17] = -abb[41] + z[17];
z[18] = abb[8] * z[17];
z[19] = -abb[31] + abb[33];
z[20] = m1_set::bc<T>[0] * z[19];
z[20] = abb[41] + z[20];
z[21] = abb[5] * z[20];
z[7] = z[2] + z[7] + z[8] + z[10] + z[14] + z[16] + z[18] + z[21];
z[8] = abb[1] * z[17];
z[7] = (T(1) / T(2)) * z[7] + -z[8];
z[7] = abb[49] * z[7];
z[10] = -abb[33] + z[12];
z[14] = m1_set::bc<T>[0] * (T(1) / T(2));
z[16] = z[10] * z[14];
z[18] = abb[41] * (T(1) / T(2));
z[16] = z[0] + z[16] + -z[18];
z[16] = abb[4] * z[16];
z[9] = z[9] * z[14];
z[14] = abb[5] * (T(1) / T(2));
z[20] = z[14] * z[20];
z[21] = abb[14] * (T(1) / T(2));
z[5] = z[5] * z[21];
z[22] = abb[40] * (T(1) / T(2));
z[15] = z[15] * z[22];
z[23] = abb[10] * z[17];
z[5] = -z[5] + z[9] + z[15] + z[16] + -z[20] + z[23];
z[9] = abb[32] * (T(1) / T(2));
z[15] = -abb[29] + z[9];
z[16] = abb[30] + -abb[33];
z[16] = z[15] + (T(1) / T(2)) * z[16];
z[16] = m1_set::bc<T>[0] * z[16];
z[20] = abb[28] * (T(1) / T(2));
z[23] = m1_set::bc<T>[0] * z[20];
z[24] = abb[39] + -abb[41];
z[16] = z[16] + z[22] + z[23] + (T(1) / T(2)) * z[24];
z[16] = abb[8] * z[16];
z[22] = abb[7] * (T(1) / T(2));
z[6] = z[6] * z[22];
z[8] = -z[6] + z[8];
z[24] = m1_set::bc<T>[0] * z[12];
z[25] = abb[40] + z[0];
z[24] = z[24] + z[25];
z[26] = abb[2] * z[24];
z[27] = abb[29] + -abb[30];
z[28] = m1_set::bc<T>[0] * z[27];
z[28] = -abb[40] + z[28];
z[29] = abb[3] * (T(1) / T(2));
z[30] = z[28] * z[29];
z[2] = z[2] + -z[5] + -z[8] + z[16] + z[26] + z[30];
z[16] = -abb[33] + z[9];
z[26] = m1_set::bc<T>[0] * z[16];
z[23] = -abb[41] + abb[39] * (T(1) / T(2)) + z[23] + z[26];
z[23] = abb[12] * z[23];
z[26] = abb[15] * z[24];
z[2] = (T(1) / T(2)) * z[2] + z[23] + (T(-1) / T(4)) * z[26];
z[2] = abb[50] * z[2];
z[26] = -abb[45] + abb[46];
z[30] = -abb[43] + abb[44] + z[26];
z[31] = -abb[42] + z[30];
z[32] = abb[32] * z[31];
z[33] = abb[33] * z[31];
z[34] = z[32] + -z[33];
z[35] = abb[30] * z[31];
z[36] = z[34] + -z[35];
z[36] = m1_set::bc<T>[0] * z[36];
z[37] = z[0] * z[31];
z[38] = abb[41] * z[31];
z[38] = -z[37] + z[38];
z[39] = abb[40] * z[31];
z[36] = z[36] + -z[38] + -z[39];
z[40] = -abb[22] * z[36];
z[41] = -abb[31] * z[31];
z[41] = z[35] + z[41];
z[42] = abb[29] * z[31];
z[43] = -z[41] + z[42];
z[44] = m1_set::bc<T>[0] * z[43];
z[37] = -z[37] + -z[39] + z[44];
z[37] = abb[25] * z[37];
z[44] = -z[34] + z[42];
z[44] = m1_set::bc<T>[0] * z[44];
z[38] = z[38] + z[44];
z[38] = abb[24] * z[38];
z[44] = abb[19] * z[24];
z[45] = abb[30] * m1_set::bc<T>[0];
z[0] = abb[40] + -z[0] + z[45];
z[0] = abb[16] * z[0];
z[28] = -abb[17] * z[28];
z[0] = z[0] + z[28] + z[44];
z[0] = abb[52] * z[0];
z[28] = z[32] + -z[35];
z[28] = m1_set::bc<T>[0] * z[28];
z[28] = z[28] + -z[39];
z[32] = -abb[21] + -abb[23];
z[28] = z[28] * z[32];
z[0] = z[0] + z[28] + z[37] + z[38] + z[40];
z[28] = abb[0] * (T(1) / T(2));
z[1] = z[1] * z[28];
z[1] = -z[1] + z[5];
z[5] = abb[8] * (T(1) / T(2));
z[17] = z[5] * z[17];
z[8] = -z[1] + -z[8] + z[17];
z[8] = (T(1) / T(2)) * z[8] + z[23];
z[17] = abb[47] + abb[48];
z[8] = z[8] * z[17];
z[23] = abb[33] * (T(1) / T(2));
z[32] = z[9] + z[12] + -z[23];
z[32] = m1_set::bc<T>[0] * z[32];
z[18] = -z[18] + z[25] + z[32];
z[18] = abb[8] * z[18];
z[25] = abb[2] + -abb[15];
z[24] = z[24] * z[25];
z[1] = -z[1] + -z[6] + z[18] + z[24];
z[6] = abb[51] * (T(1) / T(2));
z[1] = z[1] * z[6];
z[18] = abb[20] * (T(1) / T(4));
z[24] = -z[18] * z[36];
z[0] = (T(1) / T(4)) * z[0] + z[1] + z[2] + (T(1) / T(2)) * z[7] + z[8] + z[24];
z[0] = (T(1) / T(8)) * z[0];
z[1] = abb[29] * (T(1) / T(2));
z[2] = z[1] + -z[11];
z[7] = abb[29] * z[2];
z[8] = -abb[53] + z[7];
z[24] = z[11] + z[23];
z[24] = abb[33] * z[24];
z[25] = abb[34] + z[24];
z[32] = abb[30] * abb[31];
z[36] = prod_pow(abb[31], 2);
z[37] = z[32] + -z[36];
z[38] = (T(1) / T(2)) * z[37];
z[39] = (T(3) / T(2)) * z[11];
z[40] = -abb[29] + abb[33] + z[39];
z[40] = abb[28] * z[40];
z[44] = abb[32] * z[11];
z[45] = prod_pow(m1_set::bc<T>[0], 2);
z[46] = (T(1) / T(3)) * z[45];
z[40] = z[8] + -z[25] + -z[38] + z[40] + z[44] + -z[46];
z[28] = z[28] * z[40];
z[44] = z[12] + z[20];
z[47] = abb[28] * z[44];
z[48] = abb[30] * (T(1) / T(2));
z[49] = -abb[31] + z[48];
z[50] = abb[30] * z[49];
z[51] = (T(1) / T(6)) * z[45];
z[52] = abb[54] + z[51];
z[53] = z[50] + -z[52];
z[54] = abb[32] * z[15];
z[55] = abb[36] + abb[37];
z[54] = z[54] + z[55];
z[56] = abb[29] * z[11];
z[57] = (T(1) / T(2)) * z[36];
z[47] = -abb[53] + z[47] + z[53] + -z[54] + -z[56] + z[57];
z[47] = abb[2] * z[47];
z[44] = -z[3] + z[44];
z[44] = abb[28] * z[44];
z[58] = prod_pow(abb[30], 2);
z[58] = (T(1) / T(2)) * z[58];
z[59] = -z[52] + z[58];
z[60] = abb[35] + z[57];
z[61] = z[59] + -z[60];
z[62] = -abb[37] + abb[55];
z[63] = -abb[36] + z[62];
z[64] = -abb[29] + z[16];
z[65] = abb[32] * z[64];
z[66] = z[63] + -z[65];
z[67] = abb[33] + z[11];
z[67] = abb[33] * z[67];
z[44] = z[44] + -z[56] + z[61] + z[66] + -z[67];
z[44] = abb[4] * z[44];
z[27] = abb[32] * z[27];
z[68] = prod_pow(abb[29], 2);
z[68] = (T(1) / T(2)) * z[68];
z[69] = abb[36] + z[68];
z[27] = z[27] + z[59] + -z[69];
z[27] = abb[6] * z[27];
z[44] = -z[27] + z[44];
z[59] = abb[32] * z[16];
z[46] = -z[46] + z[59];
z[59] = prod_pow(abb[33], 2);
z[70] = (T(1) / T(2)) * z[59];
z[71] = -z[62] + z[70];
z[72] = z[46] + z[71];
z[73] = abb[10] * z[72];
z[74] = abb[4] * abb[53];
z[73] = z[73] + z[74];
z[44] = (T(1) / T(2)) * z[44] + -z[73];
z[74] = (T(1) / T(2)) * z[11];
z[75] = -abb[29] + z[20] + z[74];
z[75] = abb[28] * z[75];
z[76] = abb[30] * z[74];
z[8] = z[8] + z[75] + z[76];
z[75] = -z[9] * z[16];
z[77] = abb[55] * (T(1) / T(2));
z[78] = abb[37] * (T(1) / T(2));
z[75] = -abb[54] + z[8] + (T(-1) / T(4)) * z[59] + z[75] + z[77] + -z[78];
z[75] = abb[8] * z[75];
z[79] = -abb[32] + z[20];
z[79] = abb[28] * z[79];
z[65] = -abb[34] + abb[53] + z[65] + z[69] + z[71] + -z[79];
z[22] = z[22] * z[65];
z[32] = (T(1) / T(2)) * z[32] + -z[57];
z[54] = z[54] + z[68];
z[65] = -z[11] * z[20];
z[65] = z[32] + z[54] + z[65];
z[29] = z[29] * z[65];
z[65] = abb[28] + abb[32];
z[13] = z[13] * z[65];
z[69] = abb[29] * z[12];
z[13] = -abb[34] + abb[36] + -abb[53] + abb[55] + z[13] + -z[67] + -z[69];
z[13] = z[13] * z[21];
z[8] = z[8] + -z[52];
z[21] = abb[15] * z[8];
z[19] = abb[32] * z[19];
z[19] = abb[55] + z[19] + z[60] + -z[70];
z[14] = z[14] * z[19];
z[19] = abb[38] * (T(1) / T(2));
z[67] = -abb[19] + abb[17] * (T(-1) / T(2));
z[67] = z[19] * z[67];
z[29] = z[13] + -z[14] + -z[21] + -z[22] + -z[28] + z[29] + -z[44] + z[47] + z[67] + z[75];
z[6] = z[6] * z[29];
z[29] = abb[35] + abb[54] + -z[50] + -z[57] + -z[63];
z[50] = z[9] * z[64];
z[2] = abb[33] * (T(-3) / T(2)) + abb[28] * (T(1) / T(4)) + z[2] + z[9];
z[2] = abb[28] * z[2];
z[45] = (T(1) / T(12)) * z[45];
z[63] = abb[29] * z[74];
z[64] = abb[33] + z[74];
z[64] = abb[33] * z[64];
z[2] = z[2] + (T(1) / T(2)) * z[29] + z[45] + z[50] + z[63] + z[64];
z[2] = abb[4] * z[2];
z[29] = z[60] + z[76];
z[50] = -abb[54] + z[29];
z[60] = abb[33] * z[11];
z[64] = z[50] + -z[60] + z[62];
z[67] = abb[32] * (T(1) / T(4));
z[49] = z[23] + z[49] + -z[67];
z[49] = abb[32] * z[49];
z[39] = -abb[33] + z[39];
z[39] = z[15] + z[20] + (T(1) / T(2)) * z[39];
z[39] = abb[28] * z[39];
z[71] = abb[53] * (T(1) / T(2));
z[39] = z[7] + z[39] + -z[45] + z[49] + (T(1) / T(2)) * z[64] + -z[71];
z[39] = abb[8] * z[39];
z[40] = -abb[0] * z[40];
z[15] = abb[31] + z[15];
z[15] = abb[32] * z[15];
z[15] = z[15] + -z[29] + z[52] + z[55] + z[56] + -z[59];
z[29] = (T(1) / T(4)) * z[11];
z[16] = z[1] + -z[16] + -z[20] + -z[29];
z[16] = abb[28] * z[16];
z[45] = abb[34] * (T(1) / T(2));
z[15] = (T(1) / T(2)) * z[15] + z[16] + -z[45];
z[15] = abb[3] * z[15];
z[16] = -abb[33] + z[20];
z[49] = z[16] + -z[74];
z[49] = abb[28] * z[49];
z[25] = abb[35] + z[25] + -z[38] + z[49];
z[38] = abb[13] * z[25];
z[52] = z[27] + z[38];
z[55] = abb[28] * z[16];
z[55] = -z[54] + z[55] + z[70];
z[55] = abb[9] * z[55];
z[56] = abb[4] + abb[9];
z[56] = abb[34] * z[56];
z[13] = z[13] + z[55] + z[56];
z[14] = -z[14] + z[22];
z[22] = abb[16] + -abb[18];
z[55] = -abb[17] + -abb[19] + z[22];
z[56] = abb[38] * (T(1) / T(4));
z[55] = z[55] * z[56];
z[2] = z[2] + z[13] + z[14] + z[15] + z[39] + z[40] + z[47] + (T(1) / T(2)) * z[52] + z[55] + z[73];
z[15] = z[3] * z[20];
z[39] = -abb[33] + z[67];
z[39] = abb[32] * z[39];
z[15] = -abb[55] + z[15] + z[39] + -z[51] + (T(3) / T(4)) * z[59] + z[71] + z[78];
z[15] = abb[12] * z[15];
z[39] = -abb[35] + -z[24] + z[46] + -z[62];
z[39] = -abb[34] + (T(1) / T(2)) * z[39] + -z[49];
z[39] = abb[1] * z[39];
z[15] = z[15] + -z[39];
z[2] = (T(1) / T(2)) * z[2] + -z[15] + (T(-1) / T(4)) * z[21];
z[2] = abb[50] * z[2];
z[21] = z[41] * z[48];
z[40] = z[31] * z[51];
z[46] = abb[54] * z[30];
z[40] = z[40] + z[46];
z[21] = z[21] + -z[40];
z[23] = z[23] * z[31];
z[23] = z[23] + z[41];
z[23] = abb[33] * z[23];
z[46] = z[20] * z[31];
z[47] = (T(1) / T(2)) * z[41];
z[48] = -z[33] + z[46] + -z[47];
z[48] = abb[28] * z[48];
z[49] = -z[35] + z[42];
z[49] = abb[32] * z[49];
z[26] = -abb[43] + z[26];
z[52] = abb[36] * z[26];
z[49] = z[49] + -z[52];
z[52] = abb[34] + abb[35] + -z[68];
z[52] = z[31] * z[52];
z[55] = z[26] * z[36];
z[62] = -abb[36] + z[57];
z[64] = abb[44] * z[62];
z[62] = abb[54] + -z[62];
z[62] = abb[42] * z[62];
z[23] = z[21] + z[23] + z[48] + z[49] + z[52] + (T(1) / T(2)) * z[55] + z[62] + z[64];
z[23] = abb[23] * z[23];
z[48] = z[9] * z[31];
z[35] = -z[33] + -z[35] + z[48];
z[35] = abb[32] * z[35];
z[34] = z[34] + -z[47];
z[34] = abb[28] * z[34];
z[33] = z[33] + z[41];
z[48] = abb[33] * z[33];
z[52] = abb[55] * z[31];
z[55] = abb[53] * z[31];
z[48] = -z[48] + z[52] + -z[55];
z[52] = abb[37] + z[57];
z[30] = z[30] * z[52];
z[52] = -abb[54] + z[52];
z[52] = abb[42] * z[52];
z[57] = abb[35] * z[31];
z[30] = z[21] + z[30] + z[34] + z[35] + -z[48] + -z[52] + z[57];
z[34] = abb[22] * z[30];
z[1] = -z[1] * z[31];
z[1] = z[1] + z[41];
z[1] = abb[29] * z[1];
z[35] = z[42] + -z[46] + -z[47];
z[35] = abb[28] * z[35];
z[41] = -abb[42] * abb[54];
z[1] = z[1] + -z[21] + z[35] + z[41] + z[55];
z[1] = abb[25] * z[1];
z[21] = z[33] + -z[42];
z[21] = -z[21] * z[65];
z[33] = -abb[29] * z[43];
z[26] = abb[42] + -abb[44] + -z[26];
z[26] = abb[36] * z[26];
z[35] = abb[34] * z[31];
z[21] = z[21] + z[26] + z[33] + z[35] + -z[48];
z[21] = abb[24] * z[21];
z[26] = z[58] + -z[68];
z[26] = z[26] * z[31];
z[33] = -abb[36] * abb[44];
z[35] = abb[36] + abb[54];
z[35] = abb[42] * z[35];
z[26] = z[26] + z[33] + z[35] + -z[40] + z[49];
z[26] = abb[21] * z[26];
z[31] = -abb[26] * z[19] * z[31];
z[1] = z[1] + z[21] + z[23] + z[26] + z[31] + z[34];
z[21] = z[32] + -z[54] + z[59];
z[23] = -z[16] + z[29];
z[23] = abb[28] * z[23];
z[21] = abb[34] + (T(1) / T(2)) * z[21] + -z[23];
z[21] = abb[3] * z[21];
z[5] = z[5] * z[72];
z[23] = abb[35] + -z[37] + z[60];
z[23] = abb[11] * z[23];
z[22] = -z[19] * z[22];
z[26] = abb[17] * z[56];
z[5] = z[5] + -z[14] + z[21] + z[22] + z[23] + -z[26] + z[28] + -z[38];
z[10] = abb[28] + z[10];
z[10] = z[10] * z[20];
z[4] = z[4] + -z[11];
z[4] = z[4] * z[9];
z[9] = abb[33] * z[74];
z[4] = abb[37] + abb[36] * (T(1) / T(2)) + z[4] + z[9] + -z[10] + -z[45] + z[63] + z[71] + -z[77];
z[4] = abb[14] * z[4];
z[4] = z[4] + z[5] + z[44];
z[4] = (T(1) / T(2)) * z[4] + z[15];
z[4] = -z[4] * z[17];
z[9] = abb[32] + z[12];
z[10] = -z[9] + z[16];
z[10] = abb[28] * z[10];
z[10] = z[10] + z[60] + -z[61] + z[66] + z[69];
z[10] = abb[4] * z[10];
z[10] = z[10] + z[27];
z[5] = -z[5] + (T(1) / T(2)) * z[10] + z[13];
z[5] = (T(1) / T(2)) * z[5] + z[39];
z[5] = abb[49] * z[5];
z[10] = z[18] * z[30];
z[11] = -abb[31] * abb[32];
z[9] = abb[28] * z[9];
z[7] = -abb[34] + abb[35] + z[7] + z[9] + z[11] + z[36] + z[53];
z[7] = abb[17] * z[7];
z[3] = z[3] + -z[74];
z[3] = abb[28] * z[3];
z[9] = -abb[30] * abb[32];
z[3] = abb[53] + z[3] + z[9] + z[24] + z[50] + z[51];
z[3] = abb[16] * z[3];
z[9] = -abb[18] * z[25];
z[8] = abb[19] * z[8];
z[11] = -abb[4] + abb[8] * (T(3) / T(2));
z[11] = abb[38] * z[11];
z[12] = abb[0] * z[19];
z[3] = z[3] + z[7] + z[8] + z[9] + z[11] + z[12];
z[7] = abb[13] + abb[15];
z[7] = -abb[27] + (T(1) / T(8)) * z[7];
z[7] = abb[38] * z[7];
z[8] = abb[3] * z[56];
z[3] = (T(1) / T(4)) * z[3] + z[7] + z[8];
z[3] = abb[52] * z[3];
z[1] = (T(1) / T(4)) * z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[10];
z[1] = (T(1) / T(8)) * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_354_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("-0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("-0.040633995387231380219812266340591102095977644072176872341090849207"),stof<T>("0.049880425695878528372514781941049572545625060240246285135604329348")}, std::complex<T>{stof<T>("0.098430801155425983306243512074946629640624871321852072834017440721"),stof<T>("0.098781766478966211316103650373724145600679262734623516910986311868")}, std::complex<T>{stof<T>("0.098430801155425983306243512074946629640624871321852072834017440721"),stof<T>("0.098781766478966211316103650373724145600679262734623516910986311868")}, std::complex<T>{stof<T>("0.002309583979204052819079118079908397875273628753330241315697036049"),stof<T>("0.0068774609230996323856610247550267149376827705665643224016970176199")}, std::complex<T>{stof<T>("0.004605007544597006482579738436334038473114117462899111926165312795"),stof<T>("-0.029539741934420897032474281210353464953804536315967589131039813275")}, std::complex<T>{stof<T>("-0.0035556760155247333333500450720833143010502504038358939258453192"),stof<T>("-0.022243492418331860176737747681213419921989925576052258734573125343")}, std::complex<T>{stof<T>("0.080610197645943882040336503662042835406726712312518261807545420308"),stof<T>("0.108063422522188153157371941282165206955170803724137639419870031113")}};
	
	std::vector<C> intdlogs = {rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_354_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_354_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.1048986139193034620440683042478487250690525310754507657177961592"),stof<T>("0.10270139769601509863597051635081703607726541005692490421616384826")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,56> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W15(k,dl), dlog_W18(k,dl), dlog_W26(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_19(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_11_im(k), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_4_re(k), f_2_7_re(k), f_2_11_re(k)};

                    
            return f_4_354_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_354_DLogXconstant_part(base_point<T>, kend);
	value += f_4_354_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_354_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_354_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_354_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_354_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_354_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_354_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
