/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_203.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_203_abbreviated (const std::array<T,8>& abb) {
T z[8];
z[0] = 2 * abb[1];
z[1] = abb[6] + -abb[7];
z[0] = z[0] * z[1];
z[2] = abb[2] * (T(-3) / T(2)) + abb[0] * (T(1) / T(2));
z[3] = abb[6] + abb[7];
z[2] = z[2] * z[3];
z[2] = z[0] + z[2];
z[2] = prod_pow(abb[3], 2) * z[2];
z[4] = abb[0] + abb[2];
z[4] = z[3] * z[4];
z[5] = abb[4] * z[4];
z[6] = abb[3] * z[4];
z[6] = (T(1) / T(2)) * z[5] + z[6];
z[6] = abb[4] * z[6];
z[4] = -abb[5] * z[4];
z[7] = -abb[0] * z[3];
z[1] = -abb[1] * z[1];
z[1] = z[1] + z[7];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[1] = (T(13) / T(6)) * z[1] + z[2] + z[4] + z[6];
z[2] = -abb[0] + abb[2];
z[2] = z[2] * z[3];
z[0] = -z[0] + z[2];
z[0] = abb[3] * z[0];
z[0] = z[0] + -z[5];
z[0] = 2 * m1_set::bc<T>[0] * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_203_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-50.419172892063490390390915606005077334153600823961937838957660498"),stof<T>("26.858784701253073517521367133391742486838213377713421699132211851")}, std::complex<T>{stof<T>("10.157704337356942627600027155305108702462045779003043711398481525"),stof<T>("22.765052485396014043629198957460562341915262164590167986788737228")}};
	
	std::vector<C> intdlogs = {rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[17].real()/kbase.W[17].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_203_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_203_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("44.237447415847377136938132777731276347738007331175360596501514171"),stof<T>("35.648129602680363844961571430001968809407775463586805496747011229")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,8> abb = {dlog_W3(k,dl), dl[4], dlog_W18(k,dl), f_1_6(k), f_1_10(k), f_2_8(k), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[17].real()/k.W[17].real())};

                    
            return f_4_203_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_203_DLogXconstant_part(base_point<T>, kend);
	value += f_4_203_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_203_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_203_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_203_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_203_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_203_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_203_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
