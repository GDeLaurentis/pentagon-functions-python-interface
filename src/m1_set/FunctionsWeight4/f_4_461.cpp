/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_461.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_461_abbreviated (const std::array<T,70>& abb) {
T z[110];
z[0] = -abb[47] + abb[48] + abb[50] + -abb[51] + abb[52];
z[1] = abb[23] + abb[25];
z[2] = -z[0] * z[1];
z[3] = -abb[53] + abb[59];
z[4] = abb[6] * z[3];
z[4] = z[2] + z[4];
z[5] = abb[55] + abb[57];
z[6] = -abb[59] + z[5];
z[7] = -abb[54] + -z[6];
z[8] = abb[58] + z[7];
z[9] = -abb[53] + z[8];
z[10] = abb[7] * z[9];
z[11] = -z[4] + z[10];
z[1] = abb[49] * z[1];
z[12] = (T(1) / T(2)) * z[1];
z[11] = (T(1) / T(2)) * z[11] + -z[12];
z[13] = z[3] + -z[5];
z[14] = abb[1] * z[13];
z[15] = z[11] + -z[14];
z[16] = abb[56] + abb[59];
z[17] = abb[54] + z[16];
z[17] = abb[14] * z[17];
z[18] = abb[54] + z[5];
z[19] = abb[56] + z[18];
z[20] = abb[17] * z[19];
z[21] = z[17] + -z[20];
z[22] = -abb[53] + z[7];
z[23] = -abb[56] + z[22];
z[23] = abb[9] * z[23];
z[24] = z[21] + -z[23];
z[25] = abb[59] * (T(3) / T(2));
z[26] = abb[53] * (T(-3) / T(2)) + -z[5] + z[25];
z[27] = -abb[5] * z[26];
z[28] = abb[18] + abb[19];
z[29] = abb[60] * (T(1) / T(2));
z[30] = -z[28] * z[29];
z[31] = (T(1) / T(2)) * z[9];
z[32] = abb[4] * z[31];
z[33] = -abb[59] + -z[5];
z[33] = abb[3] * z[33];
z[27] = z[15] + z[24] + z[27] + z[30] + -z[32] + z[33];
z[30] = abb[33] * (T(1) / T(2));
z[27] = z[27] * z[30];
z[33] = (T(1) / T(2)) * z[18];
z[34] = abb[58] * (T(1) / T(2));
z[35] = z[33] + z[34];
z[36] = abb[53] + z[35];
z[37] = abb[0] * z[36];
z[38] = abb[53] * (T(1) / T(2));
z[39] = abb[59] * (T(1) / T(2));
z[40] = z[38] + -z[39];
z[41] = z[18] + z[40];
z[42] = abb[15] * z[41];
z[37] = z[37] + -z[42];
z[0] = abb[49] + -z[0];
z[43] = (T(1) / T(2)) * z[0];
z[44] = abb[22] * z[43];
z[45] = z[12] + z[44];
z[46] = abb[53] + abb[58];
z[47] = z[7] + z[46];
z[48] = (T(1) / T(2)) * z[47];
z[49] = abb[17] * z[48];
z[50] = abb[27] * z[43];
z[49] = z[49] + -z[50];
z[51] = z[45] + z[49];
z[52] = abb[3] * z[48];
z[53] = (T(1) / T(2)) * z[4];
z[52] = z[37] + -z[51] + z[52] + -z[53];
z[52] = abb[32] * z[52];
z[54] = abb[6] * z[47];
z[54] = -z[2] + z[54];
z[55] = -z[1] + z[54];
z[56] = -abb[53] + z[18];
z[56] = -abb[58] + -z[25] + (T(1) / T(2)) * z[56];
z[56] = abb[0] * z[56];
z[47] = abb[17] * z[47];
z[56] = z[47] + z[56];
z[57] = abb[9] * z[46];
z[55] = (T(-1) / T(4)) * z[55] + z[56] + z[57];
z[58] = z[38] + z[39];
z[59] = abb[58] + z[58];
z[60] = abb[16] * z[59];
z[61] = -z[42] + z[60];
z[62] = abb[22] * z[0];
z[63] = (T(1) / T(8)) * z[62];
z[64] = abb[26] * z[0];
z[61] = z[50] + (T(1) / T(4)) * z[61] + -z[63] + (T(1) / T(8)) * z[64];
z[65] = 3 * abb[59];
z[66] = -5 * abb[53] + -abb[58] + -z[18] + z[65];
z[67] = abb[5] * (T(1) / T(8));
z[66] = z[66] * z[67];
z[68] = abb[4] * z[9];
z[69] = -abb[58] + z[5];
z[70] = abb[54] + z[69];
z[71] = abb[59] + (T(-1) / T(4)) * z[70];
z[71] = abb[3] * z[71];
z[55] = (T(-1) / T(2)) * z[55] + z[61] + z[66] + (T(1) / T(4)) * z[68] + z[71];
z[55] = abb[31] * z[55];
z[66] = -abb[56] + z[34];
z[71] = z[58] + z[66];
z[72] = (T(3) / T(2)) * z[18] + -z[71];
z[72] = abb[17] * z[72];
z[73] = -abb[59] + (T(1) / T(2)) * z[70];
z[74] = abb[0] * z[73];
z[72] = z[72] + -z[74];
z[75] = 5 * abb[59];
z[76] = abb[53] + z[70];
z[77] = z[75] + -z[76];
z[78] = abb[3] * (T(1) / T(2));
z[77] = z[77] * z[78];
z[79] = abb[58] + abb[59];
z[80] = abb[1] * z[79];
z[81] = abb[26] * z[43];
z[77] = -z[50] + -z[72] + -z[77] + z[80] + -z[81];
z[82] = -abb[53] + (T(-1) / T(2)) * z[19] + -z[34] + z[39];
z[82] = abb[9] * z[82];
z[83] = (T(1) / T(2)) * z[60];
z[84] = abb[5] * (T(1) / T(2));
z[26] = z[26] * z[84];
z[85] = (T(1) / T(2)) * z[17];
z[26] = z[26] + (T(-1) / T(2)) * z[77] + z[82] + z[83] + -z[85];
z[26] = abb[35] * z[26];
z[77] = 3 * abb[53];
z[75] = -z[70] + z[75] + -z[77];
z[75] = abb[6] * z[75];
z[75] = (T(3) / T(2)) * z[2] + -z[10] + (T(1) / T(2)) * z[75];
z[56] = (T(3) / T(4)) * z[1] + z[56] + (T(1) / T(2)) * z[75];
z[75] = abb[59] + z[46];
z[18] = z[18] + z[75];
z[67] = -z[18] * z[67];
z[82] = abb[54] + -abb[58] + 3 * z[5];
z[82] = -abb[59] + (T(1) / T(4)) * z[82];
z[82] = abb[3] * z[82];
z[69] = -abb[53] + -z[69];
z[69] = abb[59] + (T(1) / T(2)) * z[69];
z[69] = abb[1] * z[69];
z[56] = (T(1) / T(2)) * z[56] + -z[61] + z[67] + z[69] + z[82];
z[56] = abb[34] * z[56];
z[61] = (T(1) / T(2)) * z[42];
z[67] = z[61] + z[83];
z[65] = -z[65] + z[76];
z[69] = abb[6] * z[65];
z[69] = -z[2] + z[69];
z[69] = (T(1) / T(2)) * z[69];
z[76] = -z[45] + z[69];
z[22] = abb[0] * z[22];
z[82] = z[22] + z[81];
z[86] = abb[3] * z[3];
z[86] = z[82] + z[86];
z[87] = z[76] + -z[86];
z[77] = abb[58] + -z[7] + z[77];
z[88] = abb[5] * (T(1) / T(4));
z[77] = z[77] * z[88];
z[77] = z[57] + z[77];
z[87] = -z[67] + z[77] + (T(1) / T(2)) * z[87];
z[87] = abb[30] * z[87];
z[89] = abb[19] + -abb[21];
z[90] = abb[35] * z[89];
z[91] = abb[31] + -abb[32];
z[92] = abb[18] * z[91];
z[93] = abb[20] * abb[32];
z[90] = z[90] + -z[92] + -z[93];
z[92] = -abb[20] + abb[21];
z[92] = (T(1) / T(2)) * z[92];
z[94] = -abb[19] + z[92];
z[95] = -abb[31] * z[94];
z[96] = abb[20] + abb[21];
z[97] = abb[30] * z[96];
z[98] = abb[18] + z[94];
z[98] = abb[34] * z[98];
z[95] = z[90] + z[95] + z[97] + z[98];
z[95] = abb[60] * z[95];
z[26] = z[26] + z[27] + (T(-1) / T(2)) * z[52] + z[55] + z[56] + z[87] + (T(1) / T(4)) * z[95];
z[26] = m1_set::bc<T>[0] * z[26];
z[27] = -z[33] + z[66];
z[33] = z[27] + z[40];
z[33] = abb[3] * z[33];
z[55] = abb[5] * z[48];
z[56] = abb[2] * z[7];
z[87] = abb[10] * z[16];
z[95] = z[56] + -z[87];
z[97] = -z[7] + z[46];
z[97] = abb[8] * z[97];
z[98] = (T(1) / T(2)) * z[97];
z[55] = z[20] + z[33] + z[55] + z[57] + z[76] + z[95] + -z[98];
z[76] = -z[55] + z[80];
z[76] = abb[42] * z[76];
z[99] = -abb[54] + z[5];
z[99] = (T(1) / T(2)) * z[99];
z[66] = z[40] + z[66] + z[99];
z[66] = abb[3] * z[66];
z[100] = abb[7] * z[31];
z[100] = z[44] + z[100];
z[66] = -z[66] + z[87] + z[100];
z[35] = abb[56] + z[35] + z[58];
z[35] = abb[17] * z[35];
z[35] = z[35] + -z[50];
z[101] = abb[24] * z[43];
z[101] = z[98] + z[101];
z[102] = z[35] + -z[101];
z[103] = -z[9] * z[84];
z[46] = z[5] + z[46];
z[104] = abb[1] * z[46];
z[103] = z[66] + -z[102] + z[103] + z[104];
z[103] = abb[41] * z[103];
z[76] = z[76] + z[103];
z[58] = z[27] + z[58];
z[58] = abb[3] * z[58];
z[103] = abb[6] * z[9];
z[103] = z[2] + z[103];
z[7] = -abb[56] + z[7];
z[7] = abb[13] * z[7];
z[51] = -z[7] + z[32] + -z[51] + z[56] + z[57] + z[58] + (T(-1) / T(2)) * z[103];
z[58] = (T(-1) / T(16)) * z[51] + (T(1) / T(32)) * z[97];
z[58] = abb[40] * z[58];
z[53] = z[45] + z[53];
z[103] = -z[60] + z[80] + -z[81];
z[104] = -abb[3] + abb[9];
z[8] = -abb[56] + z[8];
z[8] = z[8] * z[104];
z[49] = z[49] + z[74];
z[8] = z[8] + z[49] + z[53] + z[101] + z[103];
z[8] = (T(1) / T(16)) * z[8];
z[74] = abb[38] * z[8];
z[48] = abb[18] * z[48];
z[104] = abb[21] * z[59];
z[105] = abb[20] * z[41];
z[106] = abb[19] * (T(1) / T(2));
z[107] = z[18] * z[106];
z[43] = abb[28] * z[43];
z[43] = z[43] + z[48] + z[104] + z[105] + -z[107];
z[48] = abb[43] * z[43];
z[41] = abb[5] * z[41];
z[16] = abb[3] * z[16];
z[16] = z[16] + z[37] + z[41] + -z[95] + -z[102];
z[16] = (T(1) / T(16)) * z[16];
z[37] = abb[39] * z[16];
z[41] = -abb[0] + abb[3] + -abb[6];
z[95] = abb[15] + abb[16];
z[41] = -abb[29] + (T(-1) / T(4)) * z[41] + z[88] + (T(1) / T(8)) * z[95];
z[95] = abb[43] * z[41];
z[102] = abb[19] + abb[20];
z[104] = abb[39] * z[102];
z[105] = abb[18] + abb[21];
z[107] = abb[38] * z[105];
z[104] = z[104] + z[107];
z[95] = z[95] + (T(1) / T(4)) * z[104];
z[104] = abb[60] * (T(1) / T(8));
z[95] = z[95] * z[104];
z[107] = abb[24] * (T(1) / T(32));
z[107] = z[0] * z[107];
z[108] = abb[40] + abb[42];
z[108] = z[107] * z[108];
z[109] = (T(1) / T(32)) * z[3];
z[109] = abb[46] * z[109];
z[26] = abb[67] + abb[68] + (T(1) / T(8)) * z[26] + z[37] + (T(-1) / T(32)) * z[48] + z[58] + z[74] + (T(1) / T(16)) * z[76] + z[95] + z[108] + z[109];
z[37] = abb[5] * z[13];
z[48] = -z[71] + -z[99];
z[48] = abb[3] * z[48];
z[21] = -z[14] + z[21] + -z[32] + -z[37] + z[48] + z[100] + z[101];
z[21] = z[21] * z[30];
z[30] = z[3] * z[84];
z[4] = -z[1] + -z[4];
z[4] = (T(1) / T(2)) * z[4] + -z[23] + -z[30] + z[33] + -z[44] + -z[101];
z[4] = abb[30] * z[4];
z[30] = z[30] + -z[49];
z[33] = abb[3] * abb[59];
z[44] = z[30] + z[32] + z[33];
z[48] = abb[31] * z[44];
z[24] = -z[24] + z[33] + z[37];
z[24] = abb[35] * z[24];
z[6] = abb[3] * z[6];
z[6] = z[6] + -z[15] + z[49];
z[15] = abb[34] * z[6];
z[33] = abb[19] * abb[31];
z[37] = -abb[30] * z[28];
z[49] = abb[18] * abb[34];
z[33] = z[33] + z[37] + z[49];
z[29] = z[29] * z[33];
z[4] = z[4] + z[15] + z[21] + z[24] + z[29] + z[48];
z[4] = abb[33] * z[4];
z[15] = abb[65] * z[55];
z[21] = -z[27] + z[40];
z[21] = abb[3] * z[21];
z[24] = abb[6] * z[73];
z[21] = z[21] + z[24] + -z[82];
z[24] = abb[58] + z[39];
z[27] = abb[53] + -z[19];
z[27] = z[24] + (T(1) / T(2)) * z[27];
z[27] = abb[9] * z[27];
z[29] = z[36] * z[84];
z[33] = (T(1) / T(4)) * z[0];
z[33] = abb[24] * z[33];
z[21] = (T(1) / T(2)) * z[21] + z[27] + z[29] + z[33] + -z[67] + (T(1) / T(4)) * z[97];
z[21] = abb[30] * z[21];
z[27] = -z[62] + z[64];
z[29] = abb[3] * z[73];
z[27] = (T(1) / T(4)) * z[27] + -z[29] + z[50] + -z[61] + z[83];
z[29] = abb[0] * z[75];
z[29] = -z[12] + z[29] + -z[47];
z[33] = (T(1) / T(2)) * z[54];
z[36] = z[29] + z[33];
z[36] = z[27] + (T(1) / T(2)) * z[36] + -z[77];
z[36] = abb[31] * z[36];
z[37] = z[65] * z[78];
z[30] = z[30] + -z[37] + -z[57] + -z[103];
z[37] = abb[35] * z[30];
z[21] = z[21] + z[36] + z[37] + -z[52];
z[21] = abb[30] * z[21];
z[36] = z[51] + -z[101];
z[36] = abb[63] * z[36];
z[37] = z[18] * z[84];
z[33] = -z[33] + z[37] + z[42] + z[45] + z[86];
z[33] = (T(1) / T(2)) * z[33] + -z[80] + z[83];
z[33] = abb[31] * z[33];
z[29] = -z[29] + -z[69];
z[18] = -z[18] * z[88];
z[18] = z[18] + -z[27] + (T(1) / T(2)) * z[29] + z[80];
z[18] = abb[30] * z[18];
z[6] = -abb[35] * z[6];
z[6] = z[6] + z[18] + z[33] + z[52];
z[6] = abb[34] * z[6];
z[18] = z[3] + z[70];
z[18] = z[18] * z[78];
z[18] = z[18] + z[22] + z[32] + z[42] + z[53];
z[18] = abb[37] * z[18];
z[11] = z[11] + -z[14] + z[23] + z[72];
z[13] = -z[13] * z[84];
z[14] = abb[27] * z[0];
z[23] = -abb[59] + (T(1) / T(2)) * z[5];
z[23] = abb[3] * z[23];
z[11] = (T(-1) / T(2)) * z[11] + z[13] + (T(-1) / T(4)) * z[14] + z[23] + z[85];
z[11] = abb[35] * z[11];
z[13] = -abb[31] * z[30];
z[11] = z[11] + z[13];
z[11] = abb[35] * z[11];
z[13] = z[35] + -z[66] + -z[98];
z[13] = abb[64] * z[13];
z[14] = abb[31] * abb[32];
z[23] = prod_pow(abb[32], 2);
z[27] = -z[14] + (T(1) / T(2)) * z[23];
z[27] = z[27] * z[44];
z[9] = abb[64] * z[9];
z[29] = abb[37] * z[3];
z[9] = z[9] + z[29];
z[9] = z[9] * z[84];
z[29] = -abb[64] * z[46];
z[30] = -abb[65] * z[79];
z[29] = z[29] + z[30];
z[29] = abb[1] * z[29];
z[3] = abb[69] * z[3];
z[3] = (T(-1) / T(2)) * z[3] + z[4] + z[6] + z[9] + z[11] + z[13] + z[15] + z[18] + z[21] + z[27] + z[29] + z[36];
z[4] = -5 * z[2] + -z[10];
z[6] = abb[59] * (T(-23) / T(8)) + abb[53] * (T(5) / T(8)) + z[70];
z[6] = abb[6] * z[6];
z[0] = abb[24] * z[0];
z[0] = z[0] + (T(5) / T(8)) * z[1] + (T(-1) / T(8)) * z[4] + -z[6] + (T(-13) / T(4)) * z[20] + (T(-1) / T(2)) * z[22] + (T(-5) / T(2)) * z[56] + z[97];
z[1] = z[42] + z[60];
z[4] = abb[18] * (T(1) / T(2));
z[6] = z[4] + (T(-1) / T(3)) * z[96] + z[106];
z[6] = z[6] * z[104];
z[7] = z[7] + z[64];
z[9] = abb[59] + -z[19] + z[38];
z[9] = (T(1) / T(3)) * z[9] + z[34];
z[9] = abb[9] * z[9];
z[11] = -abb[54] + abb[53] * (T(-1) / T(4)) + (T(-3) / T(2)) * z[5];
z[11] = abb[58] * (T(1) / T(3)) + abb[59] * (T(7) / T(8)) + (T(1) / T(2)) * z[11];
z[11] = z[11] * z[84];
z[13] = abb[53] + z[5];
z[13] = (T(1) / T(2)) * z[13];
z[15] = -5 * abb[58] + -z[13];
z[15] = (T(1) / T(3)) * z[15] + -z[25];
z[15] = abb[1] * z[15];
z[5] = abb[54] * (T(-1) / T(3)) + (T(-1) / T(4)) * z[5];
z[5] = abb[56] * (T(-1) / T(3)) + abb[58] * (T(1) / T(6)) + abb[53] * (T(1) / T(12)) + (T(1) / T(2)) * z[5];
z[5] = abb[3] * z[5];
z[0] = (T(-1) / T(6)) * z[0] + (T(1) / T(12)) * z[1] + z[5] + z[6] + (T(1) / T(24)) * z[7] + (T(1) / T(2)) * z[9] + z[11] + (T(1) / T(4)) * z[15] + (T(-1) / T(8)) * z[17] + -z[63] + (T(1) / T(48)) * z[68] + (T(-5) / T(12)) * z[87];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[1] = -abb[35] + z[91];
z[1] = abb[18] * z[1];
z[5] = abb[19] + (T(-1) / T(2)) * z[96];
z[5] = abb[31] * z[5];
z[6] = abb[30] * z[94];
z[1] = z[1] + z[5] + z[6] + z[93];
z[1] = abb[34] * z[1];
z[5] = z[28] + z[96];
z[5] = abb[30] * z[5];
z[6] = -abb[31] * z[92];
z[5] = (T(1) / T(2)) * z[5] + z[6] + z[90];
z[5] = abb[30] * z[5];
z[6] = z[23] * z[106];
z[7] = -abb[31] * z[89];
z[4] = abb[35] * z[4];
z[4] = z[4] + z[7];
z[4] = abb[35] * z[4];
z[7] = -abb[20] + z[28];
z[7] = abb[37] * z[7];
z[9] = -abb[19] * z[14];
z[11] = -abb[62] * z[102];
z[14] = -abb[61] * z[105];
z[15] = -abb[21] + z[28];
z[15] = abb[36] * z[15];
z[1] = z[1] + z[4] + z[5] + z[6] + z[7] + z[9] + z[11] + z[14] + z[15];
z[1] = abb[60] * z[1];
z[2] = z[2] + -z[10];
z[4] = -abb[6] * z[59];
z[5] = abb[53] + abb[59];
z[5] = z[5] * z[84];
z[6] = abb[3] * z[31];
z[2] = (T(1) / T(2)) * z[2] + z[4] + z[5] + z[6] + z[12] + z[81];
z[4] = -z[13] + -z[24];
z[4] = abb[1] * z[4];
z[2] = (T(1) / T(2)) * z[2] + z[4] + z[83];
z[2] = abb[36] * z[2];
z[4] = -abb[60] * z[41];
z[4] = z[4] + (T(1) / T(4)) * z[43];
z[4] = abb[66] * z[4];
z[2] = z[2] + z[4];
z[4] = -abb[61] * z[8];
z[5] = -abb[62] * z[16];
z[6] = -abb[64] + -abb[65];
z[6] = z[6] * z[107];
z[0] = abb[44] + abb[45] + (T(1) / T(4)) * z[0] + (T(1) / T(32)) * z[1] + (T(1) / T(8)) * z[2] + (T(1) / T(16)) * z[3] + z[4] + z[5] + z[6];
return {z[26], z[0]};
}


template <typename T> std::complex<T> f_4_461_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("-0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("-0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("-0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("-0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("-0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("-0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("0.020898306371438252515741060746933841823397787250839508815725475101"),stof<T>("0.13574022450088939322201724851925175755513123840040319374065701043")}, std::complex<T>{stof<T>("-1.4187523569869519635718247783702132063759690051962032255435742278"),stof<T>("1.0567222651493884791517758113144928706421413629366677576276558249")}, std::complex<T>{stof<T>("-1.1486219251558110894172283284882984858307421774868700431056823784"),stof<T>("1.0567222651493884791517758113144928706421413629366677576276558249")}, std::complex<T>{stof<T>("-1.7798653066495442555263104971744319037124502713053472400541109944"),stof<T>("1.1895763819512404333644989539543325399053424697035422542982443026")}, std::complex<T>{stof<T>("-1.1486219251558110894172283284882984858307421774868700431056823784"),stof<T>("1.0567222651493884791517758113144928706421413629366677576276558249")}, std::complex<T>{stof<T>("0.31694803760781059560716478417862647691587782901046602085320133733"),stof<T>("-0.35290212573558182940358777794631638635724044151074178305440908611")}, std::complex<T>{stof<T>("-0.0762911296698978992321854584363258363070662741253910680252919535"),stof<T>("-0.28234313269784647430148255327235801478488005727145989535697007576")}, std::complex<T>{stof<T>("0.53292590075953324465277968402598076370767558113159512088929230139"),stof<T>("-0.13189088590090173429233144762814158075844575014136775230633239188")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_461_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_461_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(128)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(32)) * (-v[3] + v[5])) / tend;


		return (abb[54] + abb[55] + abb[57]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[54] + abb[55] + abb[57];
z[1] = abb[31] + -abb[32];
return abb[11] * m1_set::bc<T>[0] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_461_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(256)) * (-v[3] + v[5]) * (-8 + -12 * v[0] + 4 * v[1] + -5 * v[3] + v[5] + 2 * m1_set::bc<T>[1] * (4 + v[3] + v[5]))) / prod_pow(tend, 2);
c[1] = ((-1 + m1_set::bc<T>[1]) * (T(-1) / T(32)) * (-v[3] + v[5])) / tend;


		return (abb[54] + abb[55] + abb[57]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[31] + -abb[32];
z[1] = -abb[30] + abb[34];
z[0] = z[0] * z[1];
z[0] = abb[37] + z[0];
z[1] = -abb[54] + -abb[55] + -abb[57];
return abb[11] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_461_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_461_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(256)) * (v[1] + v[2] + -v[3] + -v[4]) * (-8 + -4 * v[0] + v[1] + -3 * v[2] + -v[3] + 3 * v[4] + 4 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(1) / T(32)) * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (abb[55] + abb[57] + abb[58]) * (t * c[0] + c[1]);
	}
	{
T z[1];
z[0] = abb[55] + abb[57] + abb[58];
return abb[12] * abb[36] * (T(1) / T(16)) * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_461_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.00645055266154450454213029911343284818956889132907362851392357641"),stof<T>("-1.14637034490985288354694267843293867683304132458347186369401577377")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,70> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W20(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W28(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_16(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), T{0}, T{0}, T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k), T{0}, T{0}, T{0}};
abb[44] = SpDLog_f_4_461_W_16_Im(t, path, abb);
abb[45] = SpDLog_f_4_461_W_20_Im(t, path, abb);
abb[46] = SpDLogQ_W_84(k,dl,dlr).imag();
abb[67] = SpDLog_f_4_461_W_16_Re(t, path, abb);
abb[68] = SpDLog_f_4_461_W_20_Re(t, path, abb);
abb[69] = SpDLogQ_W_84(k,dl,dlr).real();

                    
            return f_4_461_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_461_DLogXconstant_part(base_point<T>, kend);
	value += f_4_461_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_461_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_461_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_461_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_461_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_461_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_461_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
