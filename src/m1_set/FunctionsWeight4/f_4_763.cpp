/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_763.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_763_abbreviated (const std::array<T,38>& abb) {
T z[56];
z[0] = -abb[24] + abb[25];
z[1] = 4 * abb[27];
z[2] = 3 * abb[29];
z[3] = 2 * abb[28];
z[4] = z[0] + z[1] + -z[2] + z[3];
z[5] = abb[7] * z[4];
z[6] = abb[9] + -abb[11];
z[7] = 2 * abb[31];
z[8] = z[6] * z[7];
z[9] = -abb[32] + abb[33];
z[10] = abb[9] * z[9];
z[11] = -abb[11] * z[9];
z[8] = z[8] + -z[10] + -z[11];
z[12] = 2 * z[8];
z[13] = 3 * abb[24];
z[14] = 4 * abb[28];
z[15] = z[13] + z[14];
z[16] = 3 * abb[25];
z[1] = z[1] + z[16];
z[17] = -abb[26] + 3 * abb[30];
z[18] = 6 * z[17];
z[19] = 5 * abb[29] + -z[18];
z[20] = z[1] + z[15] + z[19];
z[20] = abb[8] * z[20];
z[21] = 2 * z[20];
z[22] = 2 * abb[25];
z[23] = 3 * abb[28];
z[24] = 2 * z[17];
z[25] = 5 * abb[27] + -abb[29] + z[22] + z[23] + -z[24];
z[26] = 2 * abb[4];
z[25] = z[25] * z[26];
z[27] = -abb[29] + z[17];
z[28] = abb[27] + abb[28];
z[29] = abb[24] + abb[25];
z[27] = 2 * z[27] + -z[28] + -z[29];
z[30] = -abb[0] * z[27];
z[31] = 2 * abb[27];
z[32] = -abb[29] + z[0] + z[31];
z[33] = abb[1] * z[32];
z[34] = 5 * abb[28];
z[18] = 6 * abb[24] + -abb[27] + 7 * abb[29] + -z[18] + z[34];
z[18] = abb[5] * z[18];
z[35] = -abb[29] + z[24];
z[15] = -abb[25] + z[15] + -z[35];
z[15] = abb[3] * z[15];
z[15] = -2 * z[5] + -z[12] + z[15] + z[18] + -z[21] + z[25] + z[30] + 5 * z[33];
z[15] = abb[17] * z[15];
z[18] = abb[29] + z[0] + -z[3];
z[25] = -abb[4] * z[18];
z[30] = abb[9] * z[7];
z[30] = -z[10] + z[30];
z[36] = abb[0] * z[32];
z[28] = -abb[29] + z[28];
z[37] = 2 * abb[5];
z[38] = z[28] * z[37];
z[38] = -z[25] + -z[30] + -z[36] + z[38];
z[38] = prod_pow(abb[16], 2) * z[38];
z[15] = z[15] + z[38];
z[38] = 6 * abb[27];
z[39] = abb[24] + abb[29];
z[40] = -z[3] + -z[16] + z[24] + -z[38] + z[39];
z[40] = abb[0] * z[40];
z[41] = z[24] + -z[31];
z[42] = 6 * abb[28];
z[43] = abb[25] + abb[29];
z[44] = -z[13] + z[41] + -z[42] + z[43];
z[44] = abb[3] * z[44];
z[29] = z[24] + -z[29];
z[38] = -z[2] + -z[29] + z[38] + z[42];
z[45] = abb[7] * z[38];
z[46] = -abb[2] * z[18];
z[47] = z[33] + z[46];
z[20] = z[20] + -z[47];
z[38] = -z[26] * z[38];
z[48] = 5 * abb[24];
z[49] = abb[25] + z[31];
z[19] = z[19] + z[42] + z[48] + z[49];
z[42] = -z[19] * z[37];
z[20] = 2 * z[20] + z[38] + z[40] + z[42] + z[44] + 3 * z[45];
z[20] = abb[19] * z[20];
z[38] = z[30] + -z[33] + z[45];
z[19] = abb[5] * z[19];
z[19] = z[19] + -z[21];
z[21] = -z[24] + z[34];
z[34] = 3 * abb[27];
z[40] = 2 * abb[24] + -abb[29] + z[21] + z[34];
z[40] = z[26] * z[40];
z[40] = z[19] + z[40];
z[42] = -abb[27] + 7 * abb[28] + -z[16] + -z[24] + z[48];
z[42] = abb[3] * z[42];
z[50] = -abb[24] + -5 * abb[25] + -8 * abb[27] + -z[14] + 3 * z[35];
z[50] = abb[0] * z[50];
z[38] = 2 * z[38] + -z[40] + z[42] + -3 * z[46] + z[50];
z[38] = abb[14] * z[38];
z[42] = z[27] * z[37];
z[50] = -z[2] + z[29];
z[51] = abb[0] * z[50];
z[52] = -z[42] + z[51];
z[30] = 2 * z[30];
z[53] = abb[4] * z[28];
z[54] = 4 * z[53];
z[44] = z[30] + -z[44] + -z[45] + z[52] + z[54];
z[44] = abb[16] * z[44];
z[38] = z[38] + 2 * z[44];
z[38] = abb[14] * z[38];
z[19] = -z[19] + z[30] + -z[46];
z[23] = z[23] + -z[29] + z[34];
z[29] = z[23] * z[26];
z[22] = z[22] + z[34];
z[30] = abb[28] + z[22] + -z[35];
z[34] = 2 * abb[0];
z[44] = z[30] * z[34];
z[55] = abb[3] * z[28];
z[29] = -z[19] + z[29] + z[44] + -z[45] + z[55];
z[29] = abb[14] * z[29];
z[30] = -z[26] * z[30];
z[21] = abb[25] + -abb[27] + -z[13] + -z[21];
z[21] = abb[3] * z[21];
z[19] = z[19] + z[21] + z[30] + z[51];
z[19] = abb[13] * z[19];
z[19] = z[19] + 2 * z[29];
z[19] = abb[13] * z[19];
z[15] = 2 * z[15] + z[19] + z[20] + z[38];
z[14] = -z[0] + -z[2] + z[14] + z[31];
z[19] = abb[4] * z[14];
z[20] = abb[24] + z[3];
z[21] = z[20] + -z[41] + z[43];
z[29] = abb[3] * z[21];
z[30] = -z[21] * z[37];
z[9] = -z[7] + z[9];
z[38] = abb[10] * z[9];
z[41] = abb[7] * z[50];
z[44] = abb[9] + 2 * abb[11];
z[44] = z[7] * z[44];
z[22] = -abb[24] + -z[17] + z[22];
z[22] = z[22] * z[34];
z[22] = -z[10] + 2 * z[11] + z[19] + z[22] + -z[29] + z[30] + -z[38] + -z[41] + z[44] + z[47];
z[22] = abb[12] * z[22];
z[30] = -z[2] + z[24];
z[16] = z[16] + -z[20] + -z[30] + z[31];
z[16] = abb[7] * z[16];
z[31] = 2 * z[46];
z[44] = abb[11] * z[7];
z[44] = z[11] + z[44];
z[45] = z[31] + z[44];
z[21] = abb[0] * z[21];
z[51] = -z[18] * z[26];
z[50] = abb[3] * z[50];
z[21] = z[16] + -z[21] + 2 * z[45] + -z[50] + z[51];
z[21] = abb[13] * z[21];
z[3] = -z[3] + -z[13] + z[30] + z[49];
z[3] = abb[7] * z[3];
z[3] = z[3] + z[29];
z[13] = 2 * z[33];
z[30] = z[13] + z[44];
z[32] = z[26] * z[32];
z[30] = -z[3] + 2 * z[30] + z[32] + -z[52];
z[30] = abb[16] * z[30];
z[30] = z[21] + z[30];
z[32] = abb[7] * z[23];
z[45] = abb[5] * z[27];
z[8] = -z[8] + z[32] + z[45] + z[47] + -z[53];
z[32] = -abb[3] * z[14];
z[45] = -abb[0] * z[4];
z[8] = 2 * z[8] + z[32] + z[45];
z[8] = abb[14] * z[8];
z[32] = z[44] + z[47];
z[26] = z[26] * z[28];
z[26] = z[26] + 2 * z[32] + -z[41];
z[28] = -abb[27] + z[17];
z[20] = z[20] + -z[28];
z[32] = 2 * abb[3];
z[20] = z[20] * z[32];
z[28] = z[28] + -z[43];
z[28] = z[28] * z[34];
z[20] = z[20] + -z[26] + z[28] + -z[42];
z[20] = abb[15] * z[20];
z[28] = 2 * z[9];
z[41] = -abb[13] + abb[16];
z[28] = z[28] * z[41];
z[41] = abb[14] * z[9];
z[28] = z[28] + z[41];
z[41] = abb[15] * z[9];
z[42] = z[28] + z[41];
z[42] = abb[10] * z[42];
z[8] = z[8] + z[20] + z[22] + -z[30] + z[42];
z[20] = 8 * abb[12];
z[8] = z[8] * z[20];
z[14] = abb[7] * z[14];
z[22] = abb[3] * z[27];
z[1] = abb[24] + -z[1] + z[35];
z[1] = abb[0] * z[1];
z[12] = z[1] + z[12] + 2 * z[14] + z[22] + -z[40] + -5 * z[46];
z[12] = abb[18] * z[12];
z[22] = abb[0] + -abb[3];
z[27] = z[9] * z[22];
z[35] = abb[24] + abb[28];
z[40] = abb[25] + abb[27];
z[42] = -z[35] + z[40];
z[42] = z[6] * z[42];
z[43] = abb[10] * z[18];
z[27] = z[27] + 2 * z[42] + z[43];
z[27] = abb[36] * z[27];
z[12] = z[12] + z[27];
z[27] = abb[9] + abb[11];
z[7] = z[7] * z[27];
z[7] = z[7] + -z[10] + z[11];
z[10] = z[7] + -z[14] + z[19] + z[31] + z[36];
z[10] = 16 * z[10];
z[11] = -abb[34] * z[10];
z[14] = abb[4] + -abb[7];
z[19] = (T(16) / T(3)) * z[14];
z[0] = abb[27] + -abb[28] + z[0];
z[19] = z[0] * z[19];
z[27] = -abb[3] * z[18];
z[19] = z[19] + (T(-5) / T(3)) * z[27] + (T(29) / T(3)) * z[33] + -7 * z[36] + -z[46];
z[31] = prod_pow(m1_set::bc<T>[0], 2);
z[19] = z[19] * z[31];
z[17] = -abb[28] + z[17];
z[42] = z[17] + -z[49];
z[34] = z[34] * z[42];
z[17] = z[17] + -z[39];
z[17] = z[17] * z[32];
z[17] = -z[17] + z[26] + z[34];
z[17] = abb[14] * z[17];
z[26] = -z[27] + -z[36] + z[47];
z[26] = abb[15] * z[26];
z[26] = -z[17] + z[26] + z[30];
z[30] = 8 * abb[15];
z[26] = z[26] * z[30];
z[4] = abb[4] * z[4];
z[13] = z[7] + z[13];
z[4] = z[4] + -z[5] + z[13] + z[27];
z[5] = abb[35] * z[4];
z[32] = -abb[15] * z[28];
z[31] = -abb[18] + -abb[35] + (T(-1) / T(3)) * z[31];
z[31] = z[9] * z[31];
z[31] = 2 * z[31] + z[32];
z[32] = 8 * abb[10];
z[31] = z[31] * z[32];
z[5] = abb[37] + 16 * z[5] + z[8] + z[11] + 8 * z[12] + 4 * z[15] + 2 * z[19] + z[26] + z[31];
z[8] = z[33] + -z[46];
z[7] = -z[7] + z[8];
z[11] = z[23] * z[37];
z[2] = -7 * abb[25] + -12 * abb[27] + z[2] + z[24] + z[48];
z[2] = abb[0] * z[2];
z[2] = z[2] + 2 * z[7] + z[11] + -z[16] + -4 * z[25] + z[29];
z[2] = z[2] * z[20];
z[1] = -z[1] + -z[3] + -z[11] + 2 * z[13] + z[54];
z[1] = abb[16] * z[1];
z[1] = z[1] + -z[17] + z[21];
z[0] = -z[0] * z[14];
z[0] = z[0] + -z[8];
z[0] = 2 * z[0] + -z[27] + z[36];
z[0] = z[0] * z[30];
z[0] = z[0] + 8 * z[1] + z[2];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = -abb[20] * z[10];
z[2] = z[4] + -z[38];
z[2] = abb[21] * z[2];
z[3] = -16 * z[35] + 16 * z[40];
z[3] = z[3] * z[6];
z[4] = 8 * z[22];
z[4] = z[4] * z[9];
z[3] = z[3] + z[4];
z[3] = abb[22] * z[3];
z[4] = -z[28] + z[41];
z[4] = m1_set::bc<T>[0] * z[4];
z[6] = abb[22] * z[18];
z[4] = z[4] + z[6];
z[4] = z[4] * z[32];
z[0] = abb[23] + z[0] + z[1] + 16 * z[2] + z[3] + z[4];
return {z[5], z[0]};
}


template <typename T> std::complex<T> f_4_763_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-125.934588131562867027987201068199528416981152218300294786977756903"),stof<T>("-34.158397358460124271203705157856657604278286740724799878152553044")}, std::complex<T>{stof<T>("165.97338886676057611709948864949997904676468221544752326953837312"),stof<T>("-38.06035194191744147701298963937201921532934895588594845485670024")}, std::complex<T>{stof<T>("40.038800735197709089112287581300450629783529997147228482560616218"),stof<T>("-72.218749300377565748216694797228676819607635696610748333009253288")}, std::complex<T>{stof<T>("291.90797699832344314508668971769950746374583443374781805651613002"),stof<T>("-3.9019545834573172058092844815153616110510622151611485767041472")}, std::complex<T>{stof<T>("-85.895787396365157938874913486899077787197622221153066304417140684"),stof<T>("-106.377146658837690019420399955085334423885922437335548211161806332")}, std::complex<T>{stof<T>("-120.11640220559312726733686274390135188935058999144168544768184866"),stof<T>("216.65624790113269724465008439168603045882290708983224499902775986")}, std::complex<T>{stof<T>("108.301512400484057380243968185491882489388423256931697860318210292"),stof<T>("43.192173362544334701430238150675116747429604859623394047776250175")}, std::complex<T>{stof<T>("54.150756200242028690121984092745941244694211628465848930159105146"),stof<T>("21.596086681272167350715119075337558373714802429811697023888125087")}, std::complex<T>{stof<T>("-54.150756200242028690121984092745941244694211628465848930159105146"),stof<T>("-21.596086681272167350715119075337558373714802429811697023888125087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[62].real()/kbase.W[62].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[127].real()/kbase.W[127].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_763_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_763_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,6> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(4)) * (v[2] + v[3]) * (-2 * (8 * v[0] + 4 * v[1] + v[2] + -3 * v[3] + -4 * (4 + v[4]) + -8 * v[5]) + -m1_set::bc<T>[2] * (24 + 8 * v[0] + 4 * v[1] + v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5]))) / prod_pow(tend, 2);
c[1] = ((T(1) / T(2)) * (v[2] + v[3]) * (8 * (10 + 2 * v[0] + v[1] + v[2] + -v[4] + 5 * v[5]) + -3 * m1_set::bc<T>[2] * (24 + 8 * v[0] + 4 * v[1] + v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5]))) / prod_pow(tend, 2);
c[2] = ((T(1) / T(2)) * (v[2] + v[3]) * (40 + -32 * m1_set::bc<T>[1] + -4 * m1_set::bc<T>[2] * (6 + 2 * v[0] + v[1]) + v[2] + v[3] + 8 * m1_set::bc<T>[1] * (v[2] + v[3] + -2 * v[5]) + 20 * v[5] + -m1_set::bc<T>[2] * (v[2] + -3 * v[3] + -4 * v[4] + 12 * v[5]))) / prod_pow(tend, 2);
c[3] = (-6 * m1_set::bc<T>[2] * (v[2] + v[3])) / tend;
c[4] = (-6 * (-4 + 5 * m1_set::bc<T>[2]) * (v[2] + v[3])) / tend;
c[5] = (-4 * (-5 + 4 * m1_set::bc<T>[1] + 9 * m1_set::bc<T>[2]) * (v[2] + v[3])) / tend;


		return t * (abb[24] * c[0] + abb[26] * c[1] + -3 * abb[30] * c[1] + abb[25] * (-c[0] + c[1]) + abb[29] * (c[0] + c[1] + -c[2]) + abb[28] * c[2] + abb[27] * (-2 * c[0] + c[1] + c[2])) + abb[24] * c[3] + abb[25] * c[4] + abb[26] * (c[3] + c[4]) + -3 * abb[30] * (c[3] + c[4]) + abb[29] * (c[3] + 2 * c[4] + -c[5]) + abb[27] * c[5] + abb[28] * (c[3] + -c[4] + c[5]);
	}
	{
T z[7];
z[0] = -abb[26] + 3 * abb[30];
z[1] = -abb[27] + z[0];
z[1] = abb[24] + -6 * z[1];
z[2] = abb[17] + -abb[19];
z[1] = z[1] * z[2];
z[3] = 6 * z[0];
z[4] = -5 * abb[27] + z[3];
z[4] = abb[18] * z[4];
z[5] = 5 * z[2];
z[6] = -6 * abb[18] + z[5];
z[6] = abb[25] * z[6];
z[5] = -7 * abb[18] + z[5];
z[5] = abb[29] * z[5];
z[2] = abb[18] + 2 * z[2];
z[2] = abb[28] * z[2];
z[1] = z[1] + z[2] + z[4] + z[5] + z[6];
z[2] = abb[12] + -abb[15];
z[4] = -abb[29] + z[0];
z[4] = abb[24] + abb[25] + abb[27] + abb[28] + -2 * z[4];
z[2] = z[2] * z[4];
z[4] = 11 * abb[29];
z[5] = -4 * abb[24] + -10 * abb[25] + -13 * abb[27] + -7 * abb[28] + 14 * z[0] + -z[4];
z[5] = abb[14] * z[5];
z[5] = -4 * z[2] + z[5];
z[5] = abb[14] * z[5];
z[0] = 2 * abb[24] + 8 * abb[25] + 7 * abb[27] + abb[28] + -10 * z[0] + z[4];
z[0] = abb[14] * z[0];
z[0] = z[0] + 2 * z[2];
z[2] = -6 * abb[25] + -abb[27] + 5 * abb[28] + z[3] + -z[4];
z[2] = abb[13] * z[2];
z[0] = 2 * z[0] + z[2];
z[0] = abb[13] * z[0];
z[0] = z[0] + 2 * z[1] + z[5];
return 4 * abb[6] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_763_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (-2 * m1_set::bc<T>[0] * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (8 * m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[24] + abb[25] + 2 * abb[26] + abb[27] + abb[28] + 2 * abb[29] + -6 * abb[30]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[26] + abb[29] + -3 * abb[30];
z[0] = abb[24] + abb[25] + abb[27] + abb[28] + 2 * z[0];
z[1] = -abb[13] + abb[14];
return 16 * abb[6] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_763_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("91.644058808541862660899329202980337040404451777336878251363872215"),stof<T>("45.855323522092799986241253727861356024870009159400494559808716944")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,38> abb = {dl[0], dlog_W3(k,dl), dl[5], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W13(k,dl), dlog_W19(k,dl), dlog_W31(k,dl), dlog_W63(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W128(k,dv), f_1_1(k), f_1_4(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_13(k), f_2_19(k), f_2_4_im(k), f_2_11_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[12].real()/k.W[12].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[62].real()/k.W[62].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[127].real()/k.W[127].real()), f_2_4_re(k), f_2_11_re(k), f_2_25_re(k), T{0}};
abb[23] = SpDLog_f_4_763_W_19_Im(t, path, abb);
abb[37] = SpDLog_f_4_763_W_19_Re(t, path, abb);

                    
            return f_4_763_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_763_DLogXconstant_part(base_point<T>, kend);
	value += f_4_763_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_763_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_763_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_763_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_763_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_763_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_763_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
