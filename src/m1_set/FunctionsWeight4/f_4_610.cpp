/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_610.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_610_abbreviated (const std::array<T,62>& abb) {
T z[110];
z[0] = 2 * abb[43] + abb[46];
z[1] = abb[45] * (T(1) / T(2));
z[2] = z[0] + z[1];
z[3] = abb[44] + abb[50];
z[4] = abb[47] + abb[48];
z[5] = 3 * abb[49];
z[3] = (T(-11) / T(3)) * z[2] + (T(-11) / T(6)) * z[3] + (T(5) / T(2)) * z[4] + z[5];
z[3] = abb[1] * z[3];
z[6] = abb[44] * (T(1) / T(2));
z[7] = z[1] + z[6];
z[8] = abb[47] * (T(3) / T(2));
z[9] = abb[43] + abb[46] * (T(1) / T(2));
z[10] = -abb[50] + z[7] + z[8] + -z[9];
z[10] = abb[14] * z[10];
z[11] = abb[50] * (T(1) / T(2));
z[12] = z[0] + z[7] + z[11];
z[13] = abb[49] * (T(3) / T(2));
z[14] = -z[12] + z[13];
z[15] = abb[13] * z[14];
z[16] = abb[51] + -abb[52];
z[17] = abb[17] * z[16];
z[18] = abb[44] + -abb[50];
z[19] = abb[42] + z[18];
z[20] = abb[12] * z[19];
z[17] = z[17] + -z[20];
z[20] = (T(1) / T(2)) * z[17];
z[21] = z[15] + z[20];
z[22] = abb[49] * (T(1) / T(2));
z[23] = abb[47] * (T(1) / T(2)) + -z[9] + -z[11] + z[22];
z[24] = -abb[9] * z[23];
z[25] = abb[53] + abb[54];
z[26] = abb[20] + abb[22];
z[27] = 3 * abb[19] + -abb[23] + (T(5) / T(2)) * z[26];
z[27] = 2 * abb[21] + (T(1) / T(2)) * z[27];
z[27] = -z[25] * z[27];
z[28] = -abb[45] + abb[49];
z[29] = z[18] + z[28];
z[30] = abb[4] * z[29];
z[7] = z[7] + z[22];
z[31] = -abb[48] + z[7] + -z[11];
z[32] = abb[7] * z[31];
z[33] = abb[50] * (T(5) / T(2));
z[34] = abb[45] * (T(-13) / T(4)) + 5 * z[9] + z[33];
z[35] = 3 * abb[47];
z[34] = abb[49] * (T(1) / T(4)) + abb[42] * (T(13) / T(12)) + (T(1) / T(3)) * z[34] + -z[35];
z[34] = abb[0] * z[34];
z[36] = abb[50] * (T(3) / T(4));
z[37] = abb[45] * (T(7) / T(4)) + -11 * z[9];
z[37] = abb[44] * (T(-31) / T(12)) + abb[42] * (T(-1) / T(6)) + abb[49] * (T(5) / T(4)) + z[36] + (T(1) / T(3)) * z[37];
z[37] = abb[6] * z[37];
z[38] = abb[48] * (T(1) / T(2));
z[39] = abb[45] + z[9];
z[39] = -z[38] + (T(1) / T(3)) * z[39];
z[39] = abb[42] * (T(-1) / T(12)) + abb[44] * (T(19) / T(12)) + -z[36] + 5 * z[39];
z[39] = abb[5] * z[39];
z[40] = abb[45] * (T(11) / T(2)) + -4 * z[0];
z[38] = -abb[49] + abb[44] * (T(-11) / T(3)) + abb[50] * (T(7) / T(3)) + z[38] + (T(1) / T(3)) * z[40];
z[38] = abb[3] * z[38];
z[40] = -z[18] + z[28];
z[41] = 2 * abb[47];
z[42] = z[40] + -z[41];
z[42] = abb[8] * z[42];
z[18] = abb[47] + z[18];
z[18] = abb[11] * z[18];
z[43] = 3 * z[18];
z[44] = abb[16] * z[16];
z[45] = abb[15] * z[16];
z[46] = abb[2] * z[19];
z[3] = z[3] + z[10] + -z[21] + z[24] + z[27] + (T(-1) / T(4)) * z[30] + (T(1) / T(2)) * z[32] + z[34] + z[37] + z[38] + z[39] + 2 * z[42] + z[43] + (T(-5) / T(4)) * z[44] + -z[45] + (T(-1) / T(6)) * z[46];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[24] = abb[19] + z[26];
z[27] = abb[23] + z[24];
z[34] = abb[31] * (T(1) / T(2));
z[27] = z[27] * z[34];
z[37] = abb[23] * abb[27];
z[38] = abb[27] * z[24];
z[39] = z[37] + -z[38];
z[39] = (T(1) / T(2)) * z[39];
z[42] = -abb[27] + -abb[30] + abb[32];
z[47] = abb[24] * z[42];
z[48] = abb[23] * abb[32];
z[27] = -z[27] + -z[39] + z[47] + z[48];
z[27] = abb[28] * z[27];
z[42] = abb[31] * z[42];
z[47] = abb[32] * (T(1) / T(2));
z[49] = -abb[27] + z[47];
z[49] = abb[32] * z[49];
z[50] = abb[55] + abb[57];
z[42] = -abb[56] + z[42] + -z[49] + z[50];
z[42] = abb[24] * z[42];
z[49] = -abb[27] + abb[31];
z[51] = abb[29] * (T(1) / T(2));
z[52] = -abb[28] + z[49] + z[51];
z[52] = abb[24] * z[52];
z[53] = abb[31] * z[24];
z[52] = z[38] + z[52] + -z[53];
z[52] = abb[29] * z[52];
z[53] = abb[31] * z[26];
z[54] = abb[21] + z[24];
z[55] = abb[27] * z[54];
z[56] = abb[19] + abb[21];
z[57] = abb[30] * z[56];
z[53] = z[53] + -z[55] + (T(1) / T(2)) * z[57];
z[53] = abb[30] * z[53];
z[55] = abb[34] + -abb[59] + -z[50];
z[55] = z[24] * z[55];
z[57] = prod_pow(abb[27], 2);
z[58] = (T(1) / T(2)) * z[57];
z[59] = -abb[56] + -abb[59] + -z[50] + z[58];
z[59] = abb[21] * z[59];
z[60] = abb[32] * z[26];
z[39] = z[39] + z[60];
z[39] = abb[31] * z[39];
z[60] = z[26] * z[47];
z[60] = z[37] + z[60];
z[60] = abb[32] * z[60];
z[58] = -abb[33] + -abb[55] + z[58];
z[58] = abb[23] * z[58];
z[61] = abb[33] * z[26];
z[62] = abb[25] * (T(1) / T(2));
z[63] = abb[60] * z[62];
z[27] = -z[27] + -z[39] + z[42] + z[52] + z[53] + z[55] + -z[58] + z[59] + z[60] + z[61] + z[63];
z[39] = abb[53] * (T(3) / T(2));
z[42] = abb[54] * (T(3) / T(2));
z[52] = z[39] + z[42];
z[27] = z[27] * z[52];
z[52] = abb[45] * (T(5) / T(4));
z[53] = abb[49] * (T(3) / T(4));
z[55] = -z[9] + z[35] + z[52] + -z[53];
z[58] = abb[42] * (T(-5) / T(4)) + -z[11] + z[55];
z[58] = abb[0] * z[58];
z[59] = abb[45] * (T(1) / T(4));
z[60] = -z[53] + z[59];
z[61] = z[9] + z[60];
z[63] = abb[50] * (T(1) / T(4));
z[64] = abb[44] * (T(1) / T(4));
z[65] = z[61] + z[63] + z[64];
z[66] = abb[13] * z[65];
z[67] = (T(1) / T(4)) * z[17];
z[68] = -z[66] + z[67];
z[68] = 3 * z[68];
z[69] = abb[1] * z[14];
z[70] = 3 * abb[9];
z[23] = z[23] * z[70];
z[71] = abb[42] * (T(1) / T(4));
z[72] = z[6] + z[61] + z[71];
z[73] = abb[6] * z[72];
z[74] = abb[3] * z[65];
z[75] = -abb[47] + (T(1) / T(2)) * z[40];
z[76] = abb[8] * z[75];
z[77] = (T(3) / T(2)) * z[76];
z[78] = -z[1] + z[13];
z[79] = -z[0] + z[78];
z[80] = abb[42] * (T(1) / T(2));
z[81] = -abb[44] + z[79] + -z[80];
z[82] = -abb[5] * z[81];
z[83] = (T(3) / T(2)) * z[44];
z[84] = (T(3) / T(4)) * z[45];
z[73] = -z[23] + z[46] + z[58] + z[68] + -z[69] + z[73] + z[74] + -z[77] + z[82] + z[83] + z[84];
z[57] = z[57] * z[73];
z[73] = -z[36] + -z[64] + z[71] + z[79];
z[73] = abb[6] * z[73];
z[74] = 2 * z[0];
z[82] = abb[44] + abb[45];
z[85] = abb[50] + -z[5] + z[74] + z[82];
z[86] = abb[1] * z[85];
z[87] = -z[11] + -z[61] + z[71];
z[87] = abb[0] * z[87];
z[88] = 2 * z[46];
z[84] = -z[84] + -z[87] + z[88];
z[89] = abb[3] * z[14];
z[90] = abb[44] * (T(5) / T(4));
z[36] = -abb[42] + z[36] + -z[61] + -z[90];
z[36] = abb[5] * z[36];
z[36] = z[36] + -z[68] + z[73] + -z[84] + -z[86] + z[89];
z[36] = abb[31] * z[36];
z[68] = abb[44] * (T(3) / T(4));
z[63] = z[63] + z[68] + z[71] + -z[79];
z[63] = abb[6] * z[63];
z[66] = z[66] + z[67];
z[66] = 3 * z[66] + z[84];
z[67] = abb[50] * (T(5) / T(4));
z[61] = -abb[42] + z[61] + z[67] + -z[68];
z[61] = abb[5] * z[61];
z[68] = abb[14] * z[14];
z[61] = z[61] + z[63] + -z[66] + -z[68] + z[86];
z[61] = abb[27] * z[61];
z[63] = abb[5] * z[14];
z[71] = z[63] + z[89];
z[73] = 3 * z[15];
z[79] = abb[6] * z[85];
z[84] = z[68] + z[71] + -z[73] + -z[79] + -2 * z[86];
z[91] = -abb[32] * z[84];
z[92] = (T(3) / T(2)) * z[30];
z[93] = 2 * abb[44];
z[94] = -abb[50] + z[0] + z[93];
z[95] = -abb[45] + z[94];
z[96] = abb[3] * z[95];
z[97] = -z[92] + 2 * z[96];
z[95] = abb[6] * z[95];
z[63] = z[63] + z[68] + z[86] + z[95] + z[97];
z[63] = abb[30] * z[63];
z[95] = -z[1] + z[6] + z[9] + -z[80];
z[95] = abb[6] * z[95];
z[72] = -abb[5] * z[72];
z[98] = z[1] + z[11];
z[99] = abb[44] + z[9] + -z[98];
z[100] = abb[3] * z[99];
z[100] = (T(3) / T(4)) * z[44] + -z[100];
z[101] = (T(1) / T(2)) * z[46];
z[69] = -z[69] + z[72] + z[87] + z[95] + -z[100] + -z[101];
z[69] = abb[28] * z[69];
z[36] = z[36] + -z[61] + z[63] + z[69] + z[91];
z[36] = abb[28] * z[36];
z[63] = abb[48] * (T(3) / T(2));
z[69] = -z[9] + z[11] + z[63] + -z[82];
z[72] = abb[5] * z[69];
z[87] = abb[50] * (T(7) / T(4));
z[60] = abb[44] * (T(11) / T(4)) + z[0] + -z[60] + -z[63] + -z[87];
z[60] = abb[3] * z[60];
z[12] = -z[8] + z[12] + -z[63];
z[12] = abb[1] * z[12];
z[43] = (T(3) / T(4)) * z[30] + -z[43];
z[63] = (T(3) / T(2)) * z[32];
z[91] = abb[6] * z[99];
z[10] = z[10] + z[12] + z[43] + z[60] + -z[63] + z[72] + -z[77] + z[91];
z[10] = abb[30] * z[10];
z[35] = 2 * abb[50] + -z[35];
z[60] = 3 * abb[48];
z[72] = abb[45] + z[74];
z[72] = -z[5] + z[35] + -z[60] + 2 * z[72] + z[93];
z[72] = abb[1] * z[72];
z[35] = z[0] + z[35] + -z[82];
z[74] = abb[14] * z[35];
z[60] = 2 * abb[45] + -z[60];
z[77] = z[60] + z[94];
z[82] = abb[3] * z[77];
z[91] = abb[5] * z[77];
z[93] = abb[50] + z[0];
z[95] = -abb[49] + z[93];
z[99] = -abb[47] + z[95];
z[99] = abb[9] * z[99];
z[102] = 3 * z[99];
z[72] = z[72] + z[74] + z[79] + z[82] + z[91] + -z[102];
z[72] = abb[32] * z[72];
z[79] = abb[6] * z[14];
z[91] = z[79] + -z[86];
z[103] = 2 * z[74];
z[71] = -z[71] + 3 * z[76] + z[91] + z[102] + -z[103];
z[104] = abb[27] * z[71];
z[105] = 3 * z[32] + -2 * z[82];
z[77] = 2 * z[77];
z[77] = abb[5] * z[77];
z[77] = -z[68] + -z[77] + z[91] + z[105];
z[106] = abb[31] * z[77];
z[10] = z[10] + z[72] + z[104] + z[106];
z[10] = abb[30] * z[10];
z[72] = z[78] + z[80] + -z[93];
z[72] = abb[0] * z[72];
z[78] = (T(3) / T(2)) * z[45];
z[104] = z[72] + z[78];
z[106] = abb[6] * z[19];
z[107] = (T(1) / T(2)) * z[106];
z[108] = abb[5] * z[19];
z[89] = (T(3) / T(2)) * z[17] + 4 * z[46] + z[68] + -z[89] + -z[104] + -z[107] + 2 * z[108];
z[49] = z[49] * z[89];
z[109] = z[46] + (T(1) / T(2)) * z[108];
z[68] = -z[68] + z[72] + -z[83] + z[92] + -z[96] + -z[106] + -z[109];
z[68] = abb[28] * z[68];
z[55] = -z[55] + z[87] + -z[90];
z[55] = abb[14] * z[55];
z[43] = -z[43] + z[55] + z[58] + z[100] + -z[101] + -z[107] + (T(5) / T(4)) * z[108];
z[43] = abb[29] * z[43];
z[43] = z[43] + z[49] + z[68];
z[43] = abb[29] * z[43];
z[49] = abb[27] * z[84];
z[8] = z[0] + -z[8] + -z[53] + -z[59] + -z[64] + z[67];
z[8] = abb[14] * z[8];
z[55] = abb[3] + abb[5];
z[55] = z[55] * z[69];
z[58] = abb[6] * z[65];
z[8] = z[8] + z[12] + z[23] + z[55] + z[58] + z[63];
z[8] = abb[32] * z[8];
z[8] = z[8] + z[49];
z[8] = abb[32] * z[8];
z[12] = abb[14] * z[75];
z[23] = z[12] + -z[76];
z[28] = abb[42] + z[28];
z[28] = -abb[47] + (T(1) / T(2)) * z[28];
z[28] = abb[0] * z[28];
z[20] = -z[20] + -z[23] + z[28] + (T(-1) / T(2)) * z[44] + -z[109];
z[28] = abb[56] * z[20];
z[17] = -z[17] + z[44] + z[45] + z[106] + -z[108];
z[30] = (T(1) / T(2)) * z[30];
z[29] = (T(1) / T(2)) * z[29];
z[45] = abb[3] * z[29];
z[17] = (T(1) / T(2)) * z[17] + -z[30] + z[45] + -z[46];
z[17] = abb[34] * z[17];
z[45] = abb[44] * abb[57];
z[49] = abb[50] * abb[57];
z[45] = z[45] + -z[49];
z[55] = abb[45] * abb[57];
z[58] = abb[49] * abb[57];
z[55] = z[55] + -z[58];
z[40] = abb[55] * z[40];
z[40] = z[40] + -z[45] + -z[55];
z[63] = abb[59] * z[75];
z[67] = abb[47] * z[50];
z[40] = (T(1) / T(2)) * z[40] + z[63] + -z[67];
z[40] = abb[8] * z[40];
z[63] = abb[0] + abb[6];
z[68] = 3 * z[16];
z[63] = z[63] * z[68];
z[68] = abb[12] * z[16];
z[69] = abb[17] * z[19];
z[19] = abb[15] * z[19];
z[19] = z[19] + z[63] + z[68] + -z[69];
z[63] = abb[16] + -abb[18];
z[63] = z[63] * z[65];
z[65] = -abb[5] + 2 * abb[26];
z[16] = z[16] * z[65];
z[16] = -z[16] + (T(1) / T(4)) * z[19] + z[63];
z[19] = abb[60] * z[16];
z[17] = z[17] + -z[19] + z[28] + z[40];
z[19] = abb[6] * z[81];
z[28] = abb[42] + -abb[44] + -z[0] + -z[60];
z[28] = abb[5] * z[28];
z[19] = z[19] + 2 * z[28] + z[46] + -z[82] + -z[86] + -z[104];
z[19] = abb[31] * z[19];
z[28] = -abb[32] * z[77];
z[19] = z[19] + z[28] + z[61];
z[19] = abb[31] * z[19];
z[28] = abb[24] + z[56];
z[28] = (T(1) / T(2)) * z[28];
z[28] = z[25] * z[28];
z[40] = abb[5] * z[31];
z[4] = -abb[49] + z[4];
z[4] = abb[1] * z[4];
z[46] = z[4] + z[40];
z[31] = abb[3] * z[31];
z[23] = z[23] + z[28] + z[31] + -z[32] + z[46];
z[23] = 3 * z[23];
z[28] = -abb[58] * z[23];
z[56] = abb[57] * z[0];
z[60] = abb[55] * z[95];
z[49] = z[49] + z[56] + -z[58] + z[60] + -z[67];
z[49] = z[49] * z[70];
z[56] = -z[15] + z[32] + z[79];
z[58] = 3 * abb[33];
z[56] = z[56] * z[58];
z[45] = z[45] + -z[55];
z[55] = abb[55] * z[14];
z[55] = (T(1) / T(2)) * z[45] + z[55];
z[14] = abb[59] * z[14];
z[55] = z[14] + 3 * z[55];
z[55] = abb[6] * z[55];
z[1] = z[0] + -z[1];
z[60] = abb[50] * (T(3) / T(2));
z[6] = -z[1] + z[6] + z[22] + -z[60];
z[6] = abb[57] * z[6];
z[7] = z[0] + -z[7] + z[60];
z[60] = -abb[55] * z[7];
z[50] = z[41] * z[50];
z[6] = z[6] + z[50] + z[60];
z[35] = 2 * z[35];
z[35] = -abb[59] * z[35];
z[6] = 3 * z[6] + z[35];
z[6] = abb[14] * z[6];
z[22] = abb[44] * (T(3) / T(2)) + -z[22];
z[0] = z[0] + z[22];
z[35] = 2 * abb[48];
z[50] = abb[45] * (T(3) / T(2)) + z[0] + -z[11] + -z[35];
z[50] = z[50] * z[58];
z[14] = z[14] + z[50];
z[50] = -abb[5] * z[14];
z[1] = z[1] + -z[11] + z[22];
z[1] = abb[57] * z[1];
z[1] = 3 * z[1] + -z[14];
z[1] = abb[3] * z[1];
z[14] = -abb[55] * z[73];
z[22] = -3 * abb[55] + -abb[59] + -z[58];
z[22] = z[22] * z[86];
z[45] = abb[4] * z[45];
z[58] = abb[59] * z[102];
z[1] = abb[61] + z[1] + z[3] + z[6] + z[8] + z[10] + z[14] + 3 * z[17] + z[19] + z[22] + z[27] + z[28] + z[36] + z[43] + (T(-3) / T(2)) * z[45] + z[49] + z[50] + z[55] + z[56] + z[57] + z[58];
z[3] = z[21] + z[44];
z[6] = abb[42] + -abb[45];
z[6] = -6 * abb[47] + (T(5) / T(2)) * z[6] + z[13] + z[93];
z[6] = abb[0] * z[6];
z[8] = abb[44] * (T(5) / T(2));
z[2] = -abb[42] + abb[49] * (T(9) / T(2)) + -3 * z[2] + -z[8] + -z[11];
z[2] = abb[5] * z[2];
z[10] = -abb[3] * z[85];
z[2] = z[2] + -3 * z[3] + z[6] + z[10] + -z[78] + -z[88] + -6 * z[99] + z[103] + -z[107];
z[2] = abb[27] * z[2];
z[3] = abb[6] * z[29];
z[3] = z[3] + z[99];
z[6] = abb[48] + -z[94];
z[6] = abb[3] * z[6];
z[6] = -z[3] + z[6] + 2 * z[18] + -z[30] + z[32] + z[46] + z[74];
z[6] = abb[30] * z[6];
z[4] = z[4] + -z[12] + z[15] + -z[31] + -z[40] + z[99];
z[4] = abb[32] * z[4];
z[4] = z[4] + z[6];
z[6] = abb[29] * z[89];
z[10] = abb[14] * z[85];
z[10] = z[10] + -z[66];
z[5] = 3 * abb[45] + -z[5] + -z[8] + z[33] + z[80];
z[5] = abb[6] * z[5];
z[8] = z[9] + z[59];
z[8] = -abb[42] + abb[49] * (T(-9) / T(4)) + 3 * z[8] + -z[64] + z[87];
z[8] = abb[5] * z[8];
z[5] = (T(1) / T(2)) * z[5] + z[8] + z[10] + -z[97];
z[5] = abb[28] * z[5];
z[8] = -abb[19] + abb[23] + -3 * z[26];
z[8] = z[8] * z[34];
z[11] = abb[30] * z[26];
z[8] = z[8] + z[11] + z[37] + z[38] + -z[48];
z[11] = -abb[24] + z[24];
z[11] = z[11] * z[51];
z[12] = -abb[23] + z[24];
z[12] = -abb[24] + (T(1) / T(4)) * z[12];
z[12] = abb[28] * z[12];
z[13] = -abb[31] + z[47];
z[13] = abb[24] * z[13];
z[8] = (T(1) / T(2)) * z[8] + -z[11] + z[12] + -z[13];
z[11] = 3 * z[25];
z[8] = z[8] * z[11];
z[9] = z[9] + -z[35] + z[52];
z[9] = abb[42] + abb[50] * (T(-13) / T(4)) + abb[44] * (T(19) / T(4)) + 3 * z[9] + z[53];
z[9] = abb[5] * z[9];
z[9] = z[9] + -z[10] + -z[105] + (T(-1) / T(4)) * z[106];
z[9] = abb[31] * z[9];
z[2] = z[2] + 3 * z[4] + z[5] + z[6] + z[8] + z[9];
z[2] = m1_set::bc<T>[0] * z[2];
z[4] = z[7] + -z[41];
z[4] = abb[14] * z[4];
z[4] = z[4] + -z[76];
z[0] = z[0] + -z[98];
z[0] = abb[3] * z[0];
z[0] = z[0] + z[3] + -z[4] + -z[30];
z[0] = abb[37] * z[0];
z[3] = abb[36] * z[20];
z[5] = -abb[23] + -abb[24] + z[54];
z[6] = abb[53] * z[5];
z[4] = -z[4] + (T(-1) / T(2)) * z[6] + -z[15] + z[91] + z[99];
z[4] = abb[35] * z[4];
z[6] = -abb[40] * z[16];
z[0] = z[0] + z[3] + z[4] + z[6];
z[3] = -abb[38] * z[23];
z[4] = abb[39] * z[71];
z[6] = abb[36] + abb[37];
z[6] = abb[21] * z[6];
z[7] = abb[36] + -abb[37];
z[7] = abb[24] * z[7];
z[8] = abb[37] * z[24];
z[9] = abb[39] * z[54];
z[10] = abb[40] * z[62];
z[6] = z[6] + z[7] + z[8] + z[9] + -z[10];
z[7] = -z[6] * z[39];
z[5] = -abb[35] * z[5];
z[5] = z[5] + -z[6];
z[5] = z[5] * z[42];
z[0] = abb[41] + 3 * z[0] + z[2] + z[3] + z[4] + z[5] + z[7];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_610_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-1.5456475103687967511626615423861988683086098975062501467963607855"),stof<T>("-11.5033015991443592773318823824912092128823182683640941530054341614")}, std::complex<T>{stof<T>("3.402800050253254215055999418538193991107424795644827539591794253"),stof<T>("53.005038414406225442918449250355425377080589699495969496998979947")}, std::complex<T>{stof<T>("4.506327512280110241669155214187873596628702543494398012230264319"),stof<T>("37.078855698803254118411860117702453011443658874134524866902840008")}, std::complex<T>{stof<T>("-9.802134862913920151869041703399913166617528186632045364256450569"),stof<T>("-21.181965232726981999077966400784476980680922130742873291916936564")}, std::complex<T>{stof<T>("1.701400025126627107527999709269096995553712397822413769795897126"),stof<T>("26.502519207203112721459224625177712688540294849747984748499489973")}, std::complex<T>{stof<T>("-5.4515598653916402665652246560949376972339281434538109750257225907"),stof<T>("0.8976728580175186752065514742314725551047601620077609794918476323")}, std::complex<T>{stof<T>("8.1007348377872930443410419941308161710638157888096315944605534422"),stof<T>("-5.3205539744761307223812582243932357078593727190051114565825534096")}, std::complex<T>{stof<T>("-2.8049274871534831341411555049187766010749901456719842424343671925"),stof<T>("-10.5763364916001413969526354925247403229033640243865401184033500348")}, std::complex<T>{stof<T>("-3.133975418202266643558147331268058698164979717370536535565281151"),stof<T>("14.940085422420628914799963768915380190763079305112406821302817017")}, std::complex<T>{stof<T>("3.133975418202266643558147331268058698164979717370536535565281151"),stof<T>("-14.940085422420628914799963768915380190763079305112406821302817017")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[126].real()/kbase.W[126].real()), C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_610_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_610_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(8)) * (v[1] + v[2] + -v[3] + -v[4]) * (-24 + -8 * v[0] + 5 * v[1] + v[2] + -5 * v[3] + 3 * v[4] + 4 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 8 * v[5])) / prod_pow(tend, 2);
c[1] = (3 * (-1 + 2 * m1_set::bc<T>[1]) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (2 * abb[43] + abb[44] + abb[45] + abb[46] + -abb[48] + -abb[49]) * (t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[3];
z[0] = abb[31] + -abb[32];
z[1] = 2 * abb[43] + abb[44] + abb[45] + abb[46] + -abb[48] + -abb[49];
z[0] = z[0] * z[1];
z[2] = abb[30] * z[1];
z[0] = z[0] + z[2];
z[0] = abb[31] * z[0];
z[1] = abb[33] * z[1];
z[2] = -abb[32] * z[2];
z[0] = z[0] + z[1] + z[2];
return 3 * abb[10] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_610_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(3) / T(4)) * (v[1] + v[2] + -v[3] + -v[4]) * (-4 + -2 * v[0] + v[1] + -v[2] + -v[3] + v[4] + 2 * v[5])) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[0] * (-v[1] + -v[2] + v[3] + v[4])) / tend;


		return (2 * abb[43] + abb[44] + abb[45] + abb[46] + -abb[48] + -abb[49]) * (t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = -2 * abb[43] + -abb[44] + -abb[45] + -abb[46] + abb[48] + abb[49];
z[1] = abb[31] + -abb[32];
return 3 * abb[10] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_610_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("60.151579686058178482824320303096999919365554354263710139575613413"),stof<T>("31.89417336517168893789946242170821194401905998580203096991505474")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,62> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W20(k,dl), dlog_W22(k,dl), dlog_W28(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_16(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[13].real()/k.W[13].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[126].real()/k.W[126].real()), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k), T{0}};
abb[41] = SpDLog_f_4_610_W_20_Im(t, path, abb);
abb[61] = SpDLog_f_4_610_W_20_Re(t, path, abb);

                    
            return f_4_610_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_610_DLogXconstant_part(base_point<T>, kend);
	value += f_4_610_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_610_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_610_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_610_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_610_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_610_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_610_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
