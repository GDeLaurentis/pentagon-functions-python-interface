/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_631.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_631_abbreviated (const std::array<T,62>& abb) {
T z[80];
z[0] = abb[28] + -abb[32];
z[1] = abb[21] + abb[22];
z[2] = z[0] * z[1];
z[3] = abb[25] * (T(1) / T(2));
z[4] = abb[23] * (T(1) / T(2)) + z[3];
z[5] = abb[24] + z[4];
z[5] = abb[30] * z[5];
z[6] = abb[24] * abb[33];
z[7] = abb[24] + abb[25];
z[8] = abb[29] * z[7];
z[9] = abb[23] * abb[29];
z[2] = z[2] + z[5] + -z[6] + -z[8] + -z[9];
z[2] = abb[30] * z[2];
z[5] = -abb[32] + abb[33];
z[6] = abb[21] * (T(1) / T(2)) + z[4];
z[8] = abb[22] + abb[26] * (T(1) / T(2)) + z[6];
z[10] = -z[5] * z[8];
z[11] = abb[28] * (T(1) / T(2));
z[12] = -abb[21] + abb[26];
z[12] = z[11] * z[12];
z[13] = abb[25] * abb[29];
z[14] = abb[23] + abb[25];
z[15] = abb[22] + z[14];
z[16] = abb[26] + z[15];
z[16] = abb[31] * z[16];
z[10] = z[9] + z[10] + z[12] + z[13] + -z[16];
z[10] = abb[28] * z[10];
z[12] = abb[22] + abb[25];
z[12] = abb[29] * z[12];
z[4] = abb[22] + z[4];
z[4] = abb[31] * z[4];
z[4] = -z[4] + z[9] + z[12];
z[4] = abb[31] * z[4];
z[9] = abb[26] + z[14];
z[12] = -abb[21] + z[9];
z[13] = abb[32] * (T(1) / T(2));
z[12] = z[12] * z[13];
z[17] = abb[24] * abb[29];
z[12] = z[12] + -z[16] + -z[17];
z[16] = abb[33] * z[12];
z[17] = prod_pow(abb[29], 2);
z[3] = z[3] * z[17];
z[18] = -abb[29] + abb[31];
z[19] = -abb[32] * z[18];
z[19] = abb[56] + -z[19];
z[19] = abb[22] * z[19];
z[20] = abb[21] + z[15];
z[20] = abb[34] * z[20];
z[21] = abb[35] + abb[56];
z[21] = abb[26] * z[21];
z[22] = abb[37] + (T(1) / T(2)) * z[17];
z[22] = abb[23] * z[22];
z[23] = abb[25] * abb[37];
z[24] = abb[35] * z[14];
z[15] = abb[36] * z[15];
z[25] = abb[27] * (T(1) / T(2));
z[26] = abb[59] * z[25];
z[2] = -z[2] + -z[3] + z[4] + -z[10] + -z[15] + z[16] + z[19] + z[20] + z[21] + -z[22] + -z[23] + z[24] + z[26];
z[3] = abb[24] + z[14];
z[4] = 3 * abb[57];
z[3] = z[3] * z[4];
z[10] = abb[22] + abb[24];
z[9] = abb[21] + z[9] + (T(5) / T(2)) * z[10];
z[10] = prod_pow(m1_set::bc<T>[0], 2);
z[9] = z[9] * z[10];
z[2] = 3 * z[2] + z[3] + z[9];
z[3] = abb[53] + abb[55];
z[9] = (T(-1) / T(2)) * z[3];
z[9] = z[2] * z[9];
z[14] = 3 * abb[58];
z[1] = abb[24] + z[1];
z[15] = -z[1] * z[14];
z[2] = -z[2] + z[15];
z[2] = abb[54] * z[2];
z[15] = 3 * abb[44];
z[16] = abb[46] + abb[52];
z[19] = abb[48] + z[16];
z[20] = abb[49] + 2 * abb[51];
z[21] = z[15] + -z[19] + z[20];
z[22] = abb[31] * z[21];
z[23] = abb[45] + -abb[47];
z[24] = abb[46] + abb[48];
z[26] = -z[23] + z[24];
z[27] = 3 * z[26];
z[28] = abb[52] + z[27];
z[29] = abb[44] + abb[50];
z[28] = (T(1) / T(4)) * z[28] + -z[29];
z[28] = z[5] * z[28];
z[30] = 3 * abb[45];
z[31] = 3 * abb[47];
z[32] = z[30] + z[31];
z[33] = -z[19] + z[32];
z[34] = (T(1) / T(2)) * z[33];
z[35] = -z[20] + z[34];
z[36] = abb[29] * z[35];
z[37] = 3 * abb[50];
z[38] = -abb[44] + z[37];
z[39] = -abb[48] + z[31] + -z[38];
z[11] = z[11] * z[39];
z[11] = z[11] + -z[22] + z[28] + -z[36];
z[11] = abb[28] * z[11];
z[28] = abb[46] + abb[47];
z[28] = 3 * z[28] + -z[30];
z[39] = 3 * abb[52];
z[40] = z[28] + -z[39];
z[41] = -abb[48] + z[40];
z[41] = abb[44] + (T(1) / T(4)) * z[41];
z[41] = abb[32] * z[41];
z[42] = 2 * abb[48] + -z[16] + z[20];
z[43] = abb[29] * z[42];
z[22] = -z[22] + z[41] + z[43];
z[41] = abb[33] * (T(1) / T(2));
z[44] = abb[44] + -abb[48];
z[45] = z[41] * z[44];
z[46] = -z[22] + z[45];
z[46] = abb[33] * z[46];
z[47] = -abb[48] + abb[52];
z[48] = abb[46] + -abb[47];
z[48] = -z[30] + z[47] + 3 * z[48];
z[48] = abb[50] + (T(1) / T(2)) * z[48];
z[49] = abb[30] * (T(1) / T(2));
z[48] = z[48] * z[49];
z[50] = z[28] + -z[47];
z[50] = -abb[50] + (T(1) / T(2)) * z[50];
z[51] = -z[0] * z[50];
z[52] = -abb[45] + -abb[48] + z[16];
z[53] = -abb[47] + z[52];
z[54] = abb[29] * z[53];
z[55] = -abb[50] + z[47];
z[56] = -abb[33] * z[55];
z[48] = z[48] + z[51] + (T(-3) / T(2)) * z[54] + z[56];
z[48] = abb[30] * z[48];
z[51] = 3 * abb[56];
z[54] = 3 * abb[35];
z[56] = z[51] + z[54];
z[57] = abb[45] + abb[47];
z[58] = -z[19] + z[57];
z[58] = abb[44] + (T(1) / T(2)) * z[58];
z[59] = -z[56] * z[58];
z[60] = 7 * abb[48];
z[61] = 5 * abb[52];
z[62] = z[60] + -z[61];
z[63] = 5 * abb[46];
z[64] = -z[32] + -z[62] + z[63];
z[64] = -abb[49] + (T(1) / T(2)) * z[64];
z[64] = -abb[51] + (T(1) / T(2)) * z[64];
z[64] = z[17] * z[64];
z[65] = 9 * abb[47];
z[66] = 9 * abb[45];
z[67] = -z[19] + -z[65] + -z[66];
z[68] = 5 * abb[49];
z[67] = (T(1) / T(2)) * z[67] + z[68];
z[69] = 5 * abb[51];
z[15] = z[15] + (T(1) / T(2)) * z[67] + z[69];
z[15] = abb[31] * z[15];
z[33] = 2 * abb[49] + 4 * abb[51] + -z[33];
z[67] = -abb[29] * z[33];
z[15] = z[15] + z[67];
z[15] = abb[31] * z[15];
z[67] = abb[31] * z[35];
z[36] = z[36] + -z[67];
z[70] = abb[32] * z[44];
z[71] = -z[36] + -z[70];
z[71] = abb[32] * z[71];
z[72] = abb[37] * z[35];
z[73] = z[20] + -z[31];
z[74] = -abb[46] + abb[50] + abb[52];
z[75] = z[73] + z[74];
z[76] = -abb[34] * z[75];
z[33] = abb[36] * z[33];
z[77] = -abb[45] + abb[46] + abb[48] * (T(11) / T(3)) + z[31] + -z[39];
z[77] = abb[44] * (T(-1) / T(3)) + (T(1) / T(2)) * z[77];
z[77] = z[10] * z[77];
z[11] = z[11] + z[15] + z[33] + z[46] + z[48] + z[59] + z[64] + z[71] + -z[72] + z[76] + (T(1) / T(2)) * z[77];
z[11] = abb[7] * z[11];
z[15] = 7 * abb[46];
z[33] = -z[15] + -z[31] + -z[62] + z[66];
z[33] = (T(1) / T(2)) * z[33];
z[46] = -z[20] + z[33];
z[48] = abb[29] * z[46];
z[33] = -abb[49] + z[33];
z[33] = -abb[51] + (T(1) / T(2)) * z[33];
z[59] = abb[31] * z[33];
z[48] = z[48] + -z[59];
z[48] = abb[31] * z[48];
z[46] = abb[36] * z[46];
z[59] = 3 * abb[48];
z[62] = 5 * abb[45] + -3 * abb[46] + abb[47] + abb[52] + -z[59];
z[62] = (T(1) / T(2)) * z[62];
z[64] = -z[20] + z[62];
z[54] = z[54] * z[64];
z[46] = z[46] + -z[48] + z[54] + -z[72];
z[33] = -z[17] * z[33];
z[48] = z[13] + -z[41];
z[27] = z[27] + -z[61];
z[27] = (T(1) / T(2)) * z[27] + z[29];
z[27] = z[27] * z[48];
z[44] = abb[28] * z[44];
z[27] = z[27] + z[36] + z[44];
z[27] = abb[28] * z[27];
z[44] = 5 * abb[48];
z[48] = -z[28] + -z[44] + z[61];
z[48] = -abb[50] + (T(1) / T(2)) * z[48];
z[48] = z[48] * z[49];
z[49] = 2 * abb[32];
z[54] = 2 * abb[28];
z[66] = z[49] + -z[54];
z[66] = z[55] * z[66];
z[71] = abb[33] * z[50];
z[48] = z[48] + z[66] + z[71];
z[48] = abb[30] * z[48];
z[66] = -abb[46] + z[32];
z[61] = -z[59] + z[61] + -z[66];
z[71] = 2 * abb[50];
z[61] = z[20] + (T(1) / T(2)) * z[61] + -z[71];
z[61] = abb[34] * z[61];
z[72] = -abb[52] + z[20] + 2 * z[24] + -z[30];
z[76] = z[18] * z[72];
z[70] = -z[70] + -z[76];
z[49] = z[49] * z[70];
z[28] = z[28] + z[39];
z[70] = z[28] + -z[44];
z[38] = -z[38] + (T(1) / T(2)) * z[70];
z[13] = z[13] * z[38];
z[13] = z[13] + z[36];
z[38] = -z[13] + -z[45];
z[38] = abb[33] * z[38];
z[45] = abb[48] * (T(-5) / T(3)) + z[39] + z[66];
z[66] = abb[51] + abb[49] * (T(1) / T(2));
z[45] = -abb[50] + abb[44] * (T(-5) / T(6)) + (T(1) / T(4)) * z[45] + -z[66];
z[45] = z[10] * z[45];
z[27] = z[27] + z[33] + z[38] + z[45] + -z[46] + z[48] + z[49] + z[61];
z[27] = abb[4] * z[27];
z[33] = 7 * abb[52];
z[30] = -z[30] + -z[33] + z[44] + z[63] + z[65];
z[30] = -z[20] + (T(1) / T(2)) * z[30];
z[38] = -abb[34] + abb[36];
z[45] = -z[30] * z[38];
z[30] = abb[29] * z[30];
z[30] = z[30] + z[67];
z[30] = abb[31] * z[30];
z[48] = abb[32] * z[35];
z[49] = z[48] + -z[67];
z[61] = 2 * abb[52] + -z[24] + z[73];
z[63] = abb[29] * z[61];
z[65] = -abb[33] * z[35];
z[63] = z[49] + 2 * z[63] + z[65];
z[63] = abb[28] * z[63];
z[65] = -abb[28] + abb[30];
z[67] = abb[33] + -z[65];
z[67] = z[35] * z[67];
z[39] = abb[45] + 5 * abb[47] + z[24] + -z[39];
z[39] = (T(1) / T(2)) * z[39];
z[70] = -z[20] + z[39];
z[73] = abb[29] * z[70];
z[67] = -z[48] + z[67] + 3 * z[73];
z[67] = abb[30] * z[67];
z[77] = -z[5] * z[36];
z[78] = abb[37] + z[17];
z[61] = z[61] * z[78];
z[51] = -z[51] * z[70];
z[30] = z[30] + z[45] + z[51] + 2 * z[61] + z[63] + z[67] + z[77];
z[30] = abb[14] * z[30];
z[45] = -z[16] + -z[57] + z[59];
z[45] = z[20] + (T(1) / T(2)) * z[45];
z[51] = 3 * z[45];
z[59] = abb[29] * z[51];
z[44] = z[16] + z[32] + -z[44];
z[20] = -z[20] + (T(1) / T(4)) * z[44];
z[20] = abb[30] * z[20];
z[44] = abb[33] * z[42];
z[61] = -abb[28] * z[35];
z[20] = z[20] + z[44] + z[48] + z[59] + z[61];
z[20] = abb[30] * z[20];
z[44] = abb[32] * z[72];
z[44] = z[44] + -2 * z[76];
z[44] = abb[32] * z[44];
z[15] = -17 * abb[48] + z[15] + z[32] + z[33];
z[15] = (T(1) / T(2)) * z[15] + -z[68];
z[15] = (T(1) / T(2)) * z[15] + -z[69];
z[15] = z[15] * z[17];
z[32] = 2 * z[43] + z[49];
z[16] = -abb[48] + (T(1) / T(2)) * z[16] + -z[66];
z[16] = abb[33] * z[16];
z[16] = z[16] + -z[32];
z[16] = abb[33] * z[16];
z[33] = -abb[49] + z[34];
z[33] = -abb[51] + (T(1) / T(2)) * z[33];
z[33] = abb[28] * z[33];
z[33] = z[33] + z[36];
z[33] = abb[28] * z[33];
z[34] = abb[49] + -abb[52];
z[43] = abb[48] + abb[51] + abb[46] * (T(-7) / T(2));
z[34] = abb[45] + (T(1) / T(6)) * z[34] + (T(1) / T(3)) * z[43];
z[34] = z[10] * z[34];
z[15] = z[15] + z[16] + z[20] + z[33] + z[34] + z[44] + -z[46];
z[15] = abb[6] * z[15];
z[16] = abb[16] * z[58];
z[20] = abb[8] * z[53];
z[33] = abb[1] * z[55];
z[34] = (T(5) / T(2)) * z[20] + (T(19) / T(3)) * z[33];
z[43] = -abb[47] + z[47];
z[43] = abb[13] * z[43];
z[44] = 3 * z[43];
z[46] = abb[44] + abb[47] + -abb[52];
z[46] = abb[10] * z[46];
z[48] = 3 * z[46];
z[49] = -abb[52] + z[29];
z[55] = abb[0] * z[49];
z[52] = -abb[47] + -z[52];
z[52] = abb[50] + (T(1) / T(2)) * z[52];
z[59] = abb[15] * z[52];
z[61] = -abb[49] + z[19];
z[61] = -abb[51] + (T(1) / T(2)) * z[61];
z[63] = abb[44] * (T(-1) / T(2)) + (T(1) / T(3)) * z[61];
z[63] = abb[2] * z[63];
z[34] = z[16] + (T(1) / T(2)) * z[34] + -z[44] + -z[48] + (T(13) / T(6)) * z[55] + z[59] + z[63];
z[34] = z[10] * z[34];
z[39] = -abb[49] + z[39];
z[39] = -abb[51] + (T(1) / T(2)) * z[39];
z[63] = abb[31] * z[39];
z[63] = z[63] + -z[73];
z[63] = abb[31] * z[63];
z[66] = -abb[28] * z[39];
z[66] = z[66] + z[73];
z[66] = abb[28] * z[66];
z[67] = abb[30] * z[39];
z[67] = z[67] + -z[73];
z[67] = abb[30] * z[67];
z[38] = abb[37] + abb[56] + z[38];
z[38] = z[38] * z[70];
z[39] = z[17] * z[39];
z[38] = z[38] + z[39] + z[63] + z[66] + z[67];
z[39] = 3 * abb[5];
z[38] = z[38] * z[39];
z[63] = abb[49] + -z[31] + z[74];
z[63] = abb[51] + (T(1) / T(2)) * z[63];
z[63] = abb[9] * z[63];
z[66] = (T(3) / T(2)) * z[59];
z[67] = z[33] + z[66];
z[48] = -z[48] + 2 * z[55];
z[68] = (T(3) / T(2)) * z[16];
z[61] = abb[44] * (T(3) / T(2)) + -z[61];
z[61] = abb[2] * z[61];
z[69] = -z[48] + z[61] + -3 * z[63] + z[67] + -z[68];
z[69] = abb[28] * z[69];
z[21] = abb[2] * z[21];
z[68] = z[21] + (T(1) / T(2)) * z[55] + -z[68];
z[66] = 2 * z[33] + z[66];
z[72] = abb[9] * z[75];
z[73] = z[66] + -z[68] + -z[72];
z[5] = z[5] * z[73];
z[73] = z[16] + -z[21];
z[74] = -abb[31] * z[73];
z[5] = z[5] + z[69] + -3 * z[74];
z[5] = abb[28] * z[5];
z[69] = 3 * z[59];
z[75] = 4 * z[33] + z[69] + -z[72];
z[0] = -z[0] * z[75];
z[76] = (T(3) / T(2)) * z[20];
z[77] = -z[33] + z[72] + z[76];
z[77] = abb[33] * z[77];
z[78] = 3 * z[20];
z[79] = -z[33] + -z[78];
z[44] = z[44] + z[63] + (T(1) / T(2)) * z[79];
z[44] = abb[30] * z[44];
z[63] = abb[29] * z[76];
z[0] = z[0] + z[44] + z[63] + z[77];
z[0] = abb[30] * z[0];
z[26] = -abb[52] + z[26];
z[26] = (T(1) / T(2)) * z[26];
z[44] = abb[7] * z[26];
z[53] = (T(1) / T(2)) * z[53];
z[63] = abb[6] * z[53];
z[20] = (T(1) / T(2)) * z[20];
z[44] = z[20] + z[33] + z[44] + z[59] + -z[63];
z[1] = (T(1) / T(2)) * z[1];
z[59] = -z[1] * z[3];
z[63] = -abb[4] * z[52];
z[59] = z[44] + z[59] + z[63];
z[14] = z[14] * z[59];
z[59] = z[66] + z[68];
z[59] = abb[32] * z[59];
z[63] = abb[29] * z[20];
z[63] = z[63] + -z[74];
z[59] = z[59] + 3 * z[63];
z[55] = z[33] + z[55];
z[61] = (T(-1) / T(2)) * z[55] + z[61];
z[61] = abb[33] * z[61];
z[61] = -z[59] + z[61];
z[61] = abb[33] * z[61];
z[19] = z[19] + -z[23];
z[19] = (T(1) / T(2)) * z[19] + -z[29];
z[19] = abb[18] * z[19];
z[23] = abb[20] * z[58];
z[26] = abb[17] * z[26];
z[29] = abb[19] * z[52];
z[19] = z[19] + z[23] + z[26] + z[29];
z[19] = (T(3) / T(2)) * z[19];
z[23] = abb[59] * z[19];
z[26] = abb[6] * z[45];
z[29] = -abb[5] + abb[14];
z[29] = z[29] * z[70];
z[45] = -abb[7] * z[53];
z[26] = z[20] + z[26] + z[29] + z[45];
z[4] = z[4] * z[26];
z[26] = z[56] * z[73];
z[29] = z[21] + -z[46];
z[29] = prod_pow(abb[31], 2) * z[29];
z[17] = -z[17] * z[43];
z[17] = z[17] + z[29];
z[29] = abb[34] * z[75];
z[45] = -z[21] + z[55];
z[45] = prod_pow(abb[32], 2) * z[45];
z[46] = abb[31] + -abb[32];
z[18] = z[18] * z[46];
z[18] = abb[35] + abb[36] + z[18];
z[18] = z[18] * z[64];
z[46] = abb[49] + -z[62];
z[46] = abb[51] + (T(1) / T(2)) * z[46];
z[10] = z[10] * z[46];
z[10] = z[10] + 3 * z[18];
z[10] = abb[3] * z[10];
z[0] = abb[60] + abb[61] + z[0] + (T(1) / T(2)) * z[2] + z[4] + z[5] + z[9] + z[10] + z[11] + z[14] + z[15] + 3 * z[17] + z[23] + z[26] + z[27] + z[29] + z[30] + z[34] + z[38] + z[45] + z[61];
z[2] = 5 * z[33] + -6 * z[43] + z[69] + -2 * z[72] + z[78];
z[2] = abb[30] * z[2];
z[4] = z[33] + z[48] + z[72];
z[4] = 2 * z[4] + 3 * z[16] + -z[21];
z[4] = abb[28] * z[4];
z[5] = -z[67] + z[68] + -z[76];
z[5] = abb[33] * z[5];
z[2] = z[2] + z[4] + z[5] + -z[59];
z[2] = m1_set::bc<T>[0] * z[2];
z[4] = z[31] + -z[47] + -z[71];
z[4] = abb[30] * z[4];
z[5] = -z[40] + -z[60];
z[5] = abb[44] + (T(1) / T(4)) * z[5];
z[5] = abb[33] * z[5];
z[9] = z[24] + -z[57];
z[9] = -abb[52] + 3 * z[9];
z[9] = -abb[44] + (T(1) / T(2)) * z[9] + z[71];
z[9] = abb[28] * z[9];
z[4] = z[4] + z[5] + z[9] + -z[22];
z[4] = m1_set::bc<T>[0] * z[4];
z[5] = -abb[38] * z[58];
z[9] = -abb[39] * z[53];
z[5] = z[5] + z[9];
z[4] = z[4] + 3 * z[5];
z[4] = abb[7] * z[4];
z[5] = 3 * abb[40];
z[9] = z[5] * z[44];
z[10] = abb[48] + -z[28];
z[10] = abb[44] + (T(1) / T(2)) * z[10] + z[37];
z[10] = z[10] * z[41];
z[11] = -z[49] * z[54];
z[14] = abb[30] * z[50];
z[10] = z[10] + z[11] + -z[13] + z[14];
z[10] = m1_set::bc<T>[0] * z[10];
z[5] = -z[5] * z[52];
z[5] = z[5] + z[10];
z[5] = abb[4] * z[5];
z[10] = abb[22] + abb[26];
z[11] = abb[28] * z[10];
z[8] = abb[24] + z[8];
z[8] = abb[33] * z[8];
z[8] = z[8] + -z[11] + -z[12];
z[6] = abb[24] + abb[22] * (T(1) / T(2)) + z[6];
z[6] = abb[30] * z[6];
z[6] = -z[6] + (T(1) / T(2)) * z[8];
z[6] = m1_set::bc<T>[0] * z[6];
z[7] = abb[23] + z[7];
z[7] = abb[39] * z[7];
z[8] = abb[38] * z[10];
z[10] = abb[41] * z[25];
z[7] = z[7] + z[8] + z[10];
z[1] = abb[40] * z[1];
z[1] = -z[1] + z[6] + (T(-1) / T(2)) * z[7];
z[3] = abb[54] + z[3];
z[3] = 3 * z[3];
z[1] = z[1] * z[3];
z[3] = abb[41] * z[19];
z[6] = abb[39] * z[51];
z[7] = abb[30] + -abb[33];
z[7] = z[7] * z[42];
z[7] = z[7] + -z[32];
z[7] = m1_set::bc<T>[0] * z[7];
z[6] = z[6] + z[7];
z[6] = abb[6] * z[6];
z[7] = abb[38] * z[73];
z[8] = abb[39] * z[20];
z[7] = z[7] + z[8];
z[8] = z[35] * z[65];
z[8] = z[8] + -z[36];
z[8] = m1_set::bc<T>[0] * z[8];
z[10] = abb[38] + -abb[39];
z[10] = z[10] * z[70];
z[8] = z[8] + -3 * z[10];
z[8] = abb[14] * z[8];
z[11] = -m1_set::bc<T>[0] * z[65] * z[70];
z[10] = z[10] + z[11];
z[10] = z[10] * z[39];
z[1] = abb[42] + abb[43] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + 3 * z[7] + z[8] + z[9] + z[10];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_631_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-3.842303589037347753716178442501725703371338453839769594880634106"),stof<T>("-15.122073742449383876612057114347449851412820922669855048999498447")}, std::complex<T>{stof<T>("-5.8353936399983307679581584692371563069721225375490980027885774203"),stof<T>("-0.2944280064184412209788379563404994829815088917188172714968998916")}, std::complex<T>{stof<T>("5.8353936399983307679581584692371563069721225375490980027885774203"),stof<T>("0.2944280064184412209788379563404994829815088917188172714968998916")}, std::complex<T>{stof<T>("9.6910558069224228983851695190974950420840410831893798120008510992"),stof<T>("5.5052373750092180789417719145809011286940907604500039130053537553")}, std::complex<T>{stof<T>("-7.627599979258414394697256932890284247236991584379268680010266196"),stof<T>("14.761637334374721982133367462817102553647503900240402103445876479")}, std::complex<T>{stof<T>("-21.160959375218185046798604894489504992692371121408418086891751401"),stof<T>("-5.865673783083879973420461566111248426459407782879456858558975723")}, std::complex<T>{stof<T>("11.4699035682957621484134353753920099506083300382190382748909003019"),stof<T>("0.3604364080746618944786896515303472977653170224294529455536219678")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("-5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("0.3899001354797931554328612164207743155239559541608405847690121451")}};
	
	std::vector<C> intdlogs = {rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[27].real()/kbase.W[27].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_631_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_631_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(32)) * (-v[3] + v[5]) * ((8 + -12 * m1_set::bc<T>[2]) * v[0] + 4 * (-4 + m1_set::bc<T>[2]) * v[1] + -8 * v[2] + -6 * v[3] + 8 * v[4] + 6 * v[5] + 12 * m1_set::bc<T>[1] * (4 + v[3] + v[5]) + -3 * m1_set::bc<T>[2] * (8 + v[3] + 3 * v[5]))) / prod_pow(tend, 2);
c[1] = (-(4 + 6 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (-1 * v[3] + v[5])) / tend;


		return (3 * abb[45] + -abb[46] + 3 * abb[47] + -abb[48] + -2 * abb[49] + -4 * abb[51] + -abb[52]) * (4 * t * c[0] + -3 * c[1]) * (T(1) / T(12));
	}
	{
T z[6];
z[0] = abb[28] + abb[32] + -abb[33];
z[1] = abb[29] + -abb[31];
z[2] = z[0] * z[1];
z[3] = -abb[34] + abb[37];
z[4] = 2 * abb[31] + abb[29] * (T(1) / T(2));
z[4] = abb[29] * z[4];
z[5] = prod_pow(abb[31], 2);
z[2] = 2 * abb[36] + z[2] + z[3] + -z[4] + (T(5) / T(2)) * z[5];
z[4] = abb[45] + abb[47];
z[4] = abb[46] + abb[48] + abb[52] + -3 * z[4];
z[2] = z[2] * z[4];
z[0] = 2 * z[0];
z[0] = z[0] * z[1];
z[1] = abb[29] + 4 * abb[31];
z[1] = abb[29] * z[1];
z[0] = 4 * abb[36] + z[0] + -z[1] + 2 * z[3] + 5 * z[5];
z[1] = abb[49] + 2 * abb[51];
z[0] = z[0] * z[1];
z[0] = z[0] + z[2];
return abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_631_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(3) / T(8)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[3] + -v[5])) / tend;


		return (3 * abb[45] + -abb[46] + 3 * abb[47] + -abb[48] + -2 * abb[49] + -4 * abb[51] + -abb[52]) * (2 * t * c[0] + -3 * c[1]) * (T(1) / T(6));
	}
	{
T z[2];
z[0] = abb[45] + abb[47];
z[0] = abb[46] + abb[48] + 2 * abb[49] + 4 * abb[51] + abb[52] + -3 * z[0];
z[1] = -abb[29] + abb[31];
return abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_631_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(-3) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (v[2] + v[3])) / tend;


		return (abb[44] + abb[45] + -abb[46] + -abb[48]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -prod_pow(abb[32], 2);
z[1] = prod_pow(abb[31], 2);
z[0] = z[0] + z[1];
z[1] = -abb[44] + -abb[45] + abb[46] + abb[48];
return 3 * abb[12] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_631_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_631_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = T(0);
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,62> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W19(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_3(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_25_im(k), T{0}, T{0}, rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[11].real()/k.W[11].real()), IntDLogL_W_16(t,k,kend,dl), rlog(kend.W[27].real()/k.W[27].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_25_re(k), T{0}, T{0}};
abb[42] = SpDLog_f_4_631_W_16_Im(t, path, abb);
abb[43] = SpDLog_f_4_631_W_19_Im(t, path, abb);
abb[60] = SpDLog_f_4_631_W_16_Re(t, path, abb);
abb[61] = SpDLog_f_4_631_W_19_Re(t, path, abb);

                    
            return f_4_631_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_631_DLogXconstant_part(base_point<T>, kend);
	value += f_4_631_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_631_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_631_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_631_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_631_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_631_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_631_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
