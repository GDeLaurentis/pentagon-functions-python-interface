/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_510.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_510_abbreviated (const std::array<T,62>& abb) {
T z[112];
z[0] = 7 * abb[45];
z[1] = abb[51] * (T(7) / T(3));
z[2] = 5 * abb[44];
z[3] = 5 * abb[48];
z[4] = -z[0] + z[1] + -z[2] + z[3];
z[4] = abb[43] * (T(-5) / T(3)) + (T(1) / T(2)) * z[4];
z[5] = 3 * abb[50] + abb[46] * (T(3) / T(2));
z[6] = abb[49] * (T(1) / T(3));
z[4] = abb[47] * (T(13) / T(4)) + (T(1) / T(2)) * z[4] + -z[5] + -z[6];
z[4] = abb[5] * z[4];
z[7] = 2 * abb[46] + 4 * abb[50];
z[8] = 3 * abb[47];
z[9] = z[7] + -z[8];
z[10] = abb[44] + abb[45];
z[11] = abb[51] + z[10];
z[12] = 3 * abb[48];
z[13] = z[11] + -z[12];
z[14] = z[9] + z[13];
z[15] = abb[13] * z[14];
z[16] = 2 * z[15];
z[17] = -abb[45] + abb[51];
z[18] = abb[46] * (T(1) / T(3));
z[19] = -abb[48] + abb[50] * (T(2) / T(3)) + z[18];
z[6] = z[6] + (T(1) / T(3)) * z[17] + z[19];
z[6] = abb[10] * z[6];
z[20] = 3 * abb[51];
z[3] = z[3] + z[10] + -z[20];
z[21] = abb[47] + z[3] + -z[7];
z[21] = abb[8] * z[21];
z[22] = abb[52] + abb[53] + abb[54];
z[23] = 3 * abb[21] + -abb[25] + abb[24] * (T(5) / T(2));
z[23] = 2 * abb[23] + (T(1) / T(2)) * z[23];
z[23] = z[22] * z[23];
z[24] = -abb[48] + z[11];
z[25] = abb[47] * (T(1) / T(2));
z[26] = abb[43] + z[25];
z[24] = (T(1) / T(2)) * z[24] + -z[26];
z[27] = abb[15] * z[24];
z[18] = -abb[43] + (T(1) / T(3)) * z[11] + -z[18];
z[18] = abb[50] * (T(-1) / T(3)) + (T(1) / T(2)) * z[18];
z[18] = abb[1] * z[18];
z[28] = -abb[44] + abb[48];
z[1] = -abb[45] + -z[1] + -11 * z[28];
z[1] = abb[43] * (T(-1) / T(3)) + (T(1) / T(2)) * z[1];
z[1] = abb[47] * (T(-7) / T(4)) + abb[49] * (T(4) / T(3)) + (T(1) / T(2)) * z[1] + z[7];
z[1] = abb[6] * z[1];
z[28] = 2 * abb[51];
z[29] = -z[10] + z[28];
z[19] = z[19] + (T(1) / T(3)) * z[29];
z[30] = 2 * abb[16];
z[19] = z[19] * z[30];
z[31] = 4 * abb[44];
z[5] = abb[48] + abb[51] * (T(-5) / T(2)) + abb[45] * (T(-3) / T(2)) + z[5] + z[31];
z[5] = abb[3] * z[5];
z[32] = abb[47] + abb[48];
z[33] = abb[45] + abb[51];
z[34] = -abb[44] + z[33];
z[35] = z[32] + -z[34];
z[36] = abb[4] * z[35];
z[37] = abb[48] + z[34];
z[37] = abb[49] + z[25] + (T(-1) / T(2)) * z[37];
z[38] = -abb[14] * z[37];
z[39] = abb[43] + abb[49];
z[40] = -abb[51] + z[39];
z[41] = abb[0] * z[40];
z[42] = -abb[44] + abb[51];
z[43] = -abb[48] + z[42];
z[43] = abb[12] * z[43];
z[44] = 3 * z[10];
z[45] = -abb[48] + -abb[51] + z[44];
z[45] = abb[47] * (T(-5) / T(2)) + (T(1) / T(2)) * z[45];
z[46] = abb[46] + z[45];
z[46] = abb[50] + (T(1) / T(2)) * z[46];
z[47] = abb[7] * z[46];
z[48] = -abb[49] + z[42];
z[49] = abb[2] * z[48];
z[50] = abb[22] * z[22];
z[1] = z[1] + z[4] + z[5] + 4 * z[6] + z[16] + z[18] + z[19] + 2 * z[21] + z[23] + -z[27] + (T(1) / T(4)) * z[36] + z[38] + (T(13) / T(6)) * z[41] + 3 * z[43] + -z[47] + (T(-1) / T(6)) * z[49] + (T(5) / T(4)) * z[50];
z[4] = prod_pow(m1_set::bc<T>[0], 2);
z[1] = z[1] * z[4];
z[5] = 2 * abb[50];
z[6] = abb[46] + z[5];
z[8] = -abb[51] + z[6] + -z[8] + 2 * z[10];
z[18] = abb[31] * z[8];
z[19] = abb[30] * z[48];
z[21] = abb[43] + -abb[44];
z[23] = abb[32] * z[21];
z[18] = z[18] + z[19] + -z[23];
z[23] = z[2] + -z[20];
z[36] = 3 * abb[45];
z[38] = z[12] + z[36];
z[51] = z[23] + -z[38];
z[52] = 3 * abb[49];
z[53] = -abb[43] + z[52];
z[54] = abb[47] * (T(3) / T(2));
z[51] = (T(1) / T(2)) * z[51] + z[53] + z[54];
z[55] = abb[29] * (T(1) / T(2));
z[51] = z[51] * z[55];
z[56] = 5 * abb[51];
z[57] = -z[12] + z[56];
z[58] = -z[44] + z[57];
z[58] = -z[39] + z[54] + (T(1) / T(2)) * z[58];
z[59] = abb[28] * (T(1) / T(2));
z[60] = -z[58] * z[59];
z[18] = 2 * z[18] + z[51] + z[60];
z[18] = abb[32] * z[18];
z[13] = (T(1) / T(2)) * z[13];
z[51] = z[6] + -z[54];
z[60] = z[13] + z[51];
z[61] = abb[28] * z[60];
z[62] = abb[32] * z[8];
z[63] = 2 * z[62];
z[64] = abb[29] * z[60];
z[61] = z[61] + -z[63] + -z[64];
z[65] = abb[33] * z[61];
z[66] = abb[31] * z[60];
z[42] = z[38] + -z[42];
z[42] = abb[49] + (T(-1) / T(2)) * z[42] + z[54];
z[42] = abb[30] * z[42];
z[67] = -z[21] * z[55];
z[67] = -z[42] + z[66] + z[67];
z[67] = abb[29] * z[67];
z[68] = 2 * z[19];
z[69] = z[66] + z[68];
z[58] = z[55] * z[58];
z[70] = abb[28] * z[21];
z[58] = z[58] + -z[69] + z[70];
z[58] = abb[28] * z[58];
z[36] = -z[2] + -z[36] + z[57];
z[36] = -abb[49] + (T(1) / T(2)) * z[36] + z[54];
z[70] = prod_pow(abb[30], 2);
z[71] = (T(1) / T(2)) * z[70];
z[36] = z[36] * z[71];
z[72] = 7 * z[10] + -z[57];
z[72] = abb[47] * (T(-9) / T(2)) + z[6] + (T(1) / T(2)) * z[72];
z[73] = abb[58] * z[72];
z[45] = -z[6] + -z[45];
z[74] = 3 * abb[34];
z[75] = -z[45] * z[74];
z[76] = 3 * abb[35];
z[77] = z[37] * z[76];
z[18] = z[18] + z[36] + z[58] + z[65] + z[67] + z[73] + z[75] + z[77];
z[18] = abb[5] * z[18];
z[36] = abb[13] * z[60];
z[3] = (T(1) / T(2)) * z[3];
z[58] = -abb[46] + z[3] + z[25];
z[58] = -abb[50] + (T(1) / T(2)) * z[58];
z[65] = abb[16] * z[58];
z[67] = (T(1) / T(4)) * z[22];
z[73] = -abb[24] + abb[26];
z[67] = z[67] * z[73];
z[73] = 3 * abb[43] + -z[11];
z[75] = -z[6] + -z[73];
z[77] = abb[1] * z[75];
z[11] = abb[48] + z[11];
z[78] = abb[47] + z[11];
z[79] = abb[50] + abb[46] * (T(1) / T(2));
z[78] = abb[43] + (T(-1) / T(4)) * z[78] + z[79];
z[78] = abb[6] * z[78];
z[47] = z[36] + -z[47] + (T(-1) / T(4)) * z[50] + z[65] + z[67] + -z[77] + z[78];
z[47] = abb[33] * z[47];
z[65] = abb[16] * z[60];
z[67] = -z[15] + z[65];
z[75] = abb[6] * z[75];
z[78] = 3 * z[27];
z[75] = z[67] + z[75] + 3 * z[77] + -z[78];
z[80] = abb[28] + -abb[29];
z[81] = z[75] * z[80];
z[61] = abb[3] * z[61];
z[82] = abb[6] * z[60];
z[83] = -z[67] + z[82];
z[84] = abb[22] * (T(3) / T(2));
z[84] = z[22] * z[84];
z[85] = -z[83] + z[84];
z[85] = abb[32] * z[85];
z[86] = -abb[29] + abb[32];
z[87] = abb[28] + z[86];
z[87] = abb[26] * z[87];
z[88] = -abb[25] * z[80];
z[89] = abb[24] * abb[32];
z[87] = z[87] + -z[88] + -z[89];
z[88] = (T(3) / T(2)) * z[22];
z[87] = -z[87] * z[88];
z[89] = abb[32] * z[45];
z[90] = 3 * abb[7];
z[91] = -z[89] * z[90];
z[47] = 3 * z[47] + z[61] + z[81] + z[85] + z[87] + z[91];
z[47] = abb[33] * z[47];
z[25] = z[6] + -z[25];
z[3] = z[3] + -z[25];
z[61] = abb[8] * z[3];
z[81] = 3 * z[61];
z[85] = z[15] + z[81];
z[87] = 3 * abb[44];
z[57] = -abb[45] + -z[57] + z[87];
z[57] = 2 * abb[49] + -z[51] + (T(1) / T(2)) * z[57];
z[57] = abb[5] * z[57];
z[17] = -abb[46] + -abb[49] + z[12] + -z[17];
z[5] = -z[5] + z[17];
z[91] = abb[6] * z[5];
z[92] = abb[23] + abb[26];
z[93] = z[88] * z[92];
z[94] = 9 * abb[48];
z[95] = 7 * abb[51] + -z[94];
z[96] = 5 * z[10] + -z[95];
z[96] = -z[54] + (T(1) / T(2)) * z[96];
z[97] = -z[6] + z[96];
z[97] = abb[16] * z[97];
z[5] = abb[10] * z[5];
z[57] = -z[5] + -4 * z[49] + z[57] + z[85] + -z[91] + z[93] + -z[97];
z[91] = abb[56] * z[57];
z[93] = -abb[30] + abb[31];
z[98] = abb[29] * z[93];
z[99] = abb[29] + -abb[30];
z[99] = abb[28] * z[99];
z[100] = abb[28] + z[93];
z[100] = abb[32] * z[100];
z[99] = z[71] + z[98] + z[99] + -z[100];
z[99] = abb[26] * z[99];
z[100] = abb[23] + -abb[25];
z[100] = z[59] * z[100];
z[101] = abb[21] + abb[24];
z[102] = -abb[25] + z[101];
z[103] = z[55] * z[102];
z[104] = abb[23] + z[101];
z[105] = abb[31] * z[104];
z[106] = abb[30] * z[101];
z[100] = z[100] + -z[103] + -z[105] + z[106];
z[100] = abb[28] * z[100];
z[102] = z[59] * z[102];
z[105] = abb[25] + z[101];
z[107] = z[55] * z[105];
z[108] = abb[24] * abb[31];
z[106] = -z[106] + z[108];
z[102] = z[102] + z[106] + z[107];
z[102] = abb[32] * z[102];
z[107] = abb[21] + abb[23];
z[108] = prod_pow(abb[31], 2);
z[109] = (T(1) / T(2)) * z[108];
z[107] = z[107] * z[109];
z[99] = z[99] + z[100] + z[102] + z[107];
z[99] = -z[88] * z[99];
z[29] = z[6] + -z[12] + z[29];
z[29] = z[29] * z[30];
z[30] = z[29] + z[82] + z[85];
z[82] = abb[31] * z[30];
z[44] = abb[51] + z[12] + z[44];
z[85] = abb[47] * (T(3) / T(4));
z[100] = z[39] + z[85];
z[44] = (T(1) / T(4)) * z[44] + -z[100];
z[44] = abb[6] * z[44];
z[102] = (T(3) / T(2)) * z[27];
z[107] = -z[77] + z[102];
z[109] = -z[5] + z[107];
z[44] = z[44] + z[65] + -z[109];
z[110] = abb[29] * z[44];
z[58] = abb[8] * z[58];
z[17] = -abb[50] + (T(1) / T(2)) * z[17];
z[17] = abb[10] * z[17];
z[111] = z[17] + -z[58];
z[52] = z[12] + z[21] + -z[52];
z[52] = abb[6] * z[52];
z[73] = abb[46] + z[73];
z[73] = abb[50] + (T(1) / T(2)) * z[73];
z[73] = abb[1] * z[73];
z[52] = (T(1) / T(2)) * z[52] + z[73] + z[102] + 3 * z[111];
z[52] = abb[28] * z[52];
z[65] = z[5] + z[65];
z[65] = abb[30] * z[65];
z[42] = abb[6] * z[42];
z[42] = z[42] + -z[65];
z[52] = z[42] + z[52] + z[82] + z[110];
z[52] = abb[28] * z[52];
z[44] = -abb[28] * z[44];
z[82] = abb[31] * z[83];
z[20] = abb[44] + z[20] + -z[38];
z[20] = -abb[43] + (T(1) / T(4)) * z[20] + z[85];
z[20] = abb[6] * z[20];
z[20] = z[20] + -z[107];
z[20] = abb[29] * z[20];
z[21] = abb[6] * z[21];
z[38] = -z[21] + z[77];
z[38] = abb[32] * z[38];
z[20] = z[20] + z[38] + -z[42] + z[44] + z[82];
z[20] = abb[32] * z[20];
z[38] = abb[30] * z[60];
z[8] = 2 * z[8];
z[8] = abb[31] * z[8];
z[8] = z[8] + -z[38] + z[62] + z[64];
z[8] = abb[32] * z[8];
z[44] = -abb[46] + z[54];
z[13] = -z[13] + z[44];
z[13] = -abb[50] + (T(1) / T(2)) * z[13];
z[13] = abb[28] * z[13];
z[13] = z[13] + z[38] + -z[66];
z[13] = abb[28] * z[13];
z[62] = 2 * abb[44] + z[6] + -z[33];
z[64] = 2 * abb[31];
z[66] = abb[30] + -z[64];
z[66] = z[62] * z[66];
z[79] = abb[44] + (T(-1) / T(2)) * z[33] + z[79];
z[82] = -abb[29] * z[79];
z[66] = z[66] + z[82];
z[66] = abb[29] * z[66];
z[79] = z[70] * z[79];
z[23] = -abb[45] + -abb[48] + -z[23];
z[23] = (T(1) / T(2)) * z[23] + z[44];
z[23] = -abb[50] + (T(1) / T(2)) * z[23];
z[44] = 3 * z[108];
z[23] = z[23] * z[44];
z[8] = z[8] + z[13] + z[23] + z[66] + z[79];
z[8] = abb[3] * z[8];
z[13] = abb[6] * z[14];
z[23] = abb[21] + z[92];
z[23] = z[23] * z[88];
z[13] = -z[13] + -z[16] + -z[23] + -z[81] + z[97];
z[23] = abb[3] * z[72];
z[23] = -z[13] + z[23];
z[23] = abb[58] * z[23];
z[3] = abb[16] * z[3];
z[66] = (T(1) / T(2)) * z[50];
z[3] = z[3] + -z[61] + -z[66];
z[61] = -abb[48] + -z[33] + z[87];
z[25] = z[25] + (T(1) / T(2)) * z[61];
z[25] = abb[3] * z[25];
z[61] = (T(1) / T(2)) * z[35];
z[79] = abb[6] * z[61];
z[81] = -abb[26] + z[104];
z[82] = (T(1) / T(2)) * z[81];
z[82] = z[22] * z[82];
z[61] = abb[4] * z[61];
z[25] = z[3] + z[25] + -z[61] + z[79] + -z[82];
z[25] = 3 * z[25];
z[79] = -abb[57] * z[25];
z[71] = z[48] * z[71];
z[82] = -z[48] * z[55];
z[82] = -z[19] + z[82];
z[82] = abb[29] * z[82];
z[83] = abb[29] * z[48];
z[83] = -z[68] + z[83];
z[87] = abb[28] * z[48];
z[83] = 2 * z[83] + z[87];
z[83] = abb[28] * z[83];
z[87] = abb[28] + abb[29];
z[87] = -z[48] * z[87];
z[87] = z[68] + z[87];
z[92] = abb[32] * z[48];
z[87] = 2 * z[87] + z[92];
z[87] = abb[32] * z[87];
z[82] = -z[71] + z[82] + z[83] + z[87];
z[82] = abb[2] * z[82];
z[83] = abb[5] * z[60];
z[87] = abb[3] * z[60];
z[92] = z[88] * z[104];
z[30] = z[30] + (T(3) / T(2)) * z[50] + -z[83] + -z[87] + z[92];
z[50] = abb[59] * z[30];
z[83] = abb[6] * z[24];
z[27] = -z[27] + z[77] + z[83];
z[81] = -abb[25] + z[81];
z[81] = (T(1) / T(2)) * z[81];
z[81] = z[22] * z[81];
z[3] = -z[3] + z[27] + z[81];
z[3] = 3 * z[3];
z[81] = abb[55] * z[3];
z[11] = -abb[49] + (T(1) / T(2)) * z[11] + -z[26];
z[11] = abb[18] * z[11];
z[24] = abb[20] * z[24];
z[26] = abb[19] * z[37];
z[11] = z[11] + -z[24] + -z[26];
z[24] = abb[48] + -abb[51];
z[26] = -abb[47] + z[10] + z[24];
z[83] = abb[17] * z[26];
z[83] = (T(3) / T(4)) * z[83];
z[92] = abb[27] * z[22];
z[92] = (T(3) / T(2)) * z[11] + z[83] + (T(-3) / T(4)) * z[92];
z[92] = abb[60] * z[92];
z[32] = -abb[44] + -abb[46] + z[32];
z[32] = -abb[50] + (T(1) / T(2)) * z[32];
z[32] = abb[6] * z[32];
z[32] = z[32] + -z[36] + -z[58];
z[32] = z[32] * z[44];
z[36] = -abb[6] * z[62];
z[36] = z[36] + z[67];
z[36] = abb[31] * z[36];
z[21] = (T(1) / T(2)) * z[21] + z[73];
z[21] = abb[29] * z[21];
z[19] = -abb[6] * z[19];
z[19] = z[19] + z[21] + z[36] + -z[65];
z[19] = abb[29] * z[19];
z[21] = z[46] * z[108];
z[36] = abb[31] * z[89];
z[44] = abb[58] * z[45];
z[21] = z[21] + z[36] + z[44];
z[21] = z[21] * z[90];
z[36] = -abb[3] + abb[7];
z[36] = z[36] * z[45];
z[44] = (T(1) / T(2)) * z[22];
z[46] = abb[24] + abb[25];
z[46] = -z[44] * z[46];
z[27] = z[27] + z[36] + z[46] + -z[66];
z[27] = z[27] * z[74];
z[36] = -z[22] * z[101];
z[26] = -abb[6] * z[26];
z[46] = -abb[3] * z[35];
z[26] = z[26] + z[36] + z[46];
z[26] = (T(1) / T(2)) * z[26] + -z[49] + z[61] + -z[66];
z[26] = z[26] * z[76];
z[36] = prod_pow(abb[28], 2);
z[46] = -prod_pow(abb[33], 2);
z[4] = -z[4] + z[36] + z[46];
z[24] = abb[43] + z[24];
z[24] = abb[9] * z[24];
z[4] = z[4] * z[24];
z[36] = -z[70] + z[108];
z[46] = -z[36] * z[43];
z[4] = z[4] + z[46];
z[46] = -abb[46] + z[96];
z[46] = -abb[50] + (T(1) / T(2)) * z[46];
z[46] = abb[16] * z[46];
z[17] = -z[17] + z[46];
z[17] = z[17] * z[70];
z[36] = (T(-1) / T(2)) * z[36] + z[98];
z[46] = abb[4] * (T(3) / T(2));
z[36] = z[35] * z[36] * z[46];
z[49] = prod_pow(abb[29], 2);
z[58] = -2 * abb[28] + -z[55];
z[58] = abb[28] * z[58];
z[49] = (T(-1) / T(2)) * z[49] + z[58];
z[49] = z[40] * z[49];
z[58] = abb[32] + (T(1) / T(2)) * z[80];
z[40] = abb[32] * z[40] * z[58];
z[40] = z[40] + z[49];
z[40] = abb[0] * z[40];
z[49] = z[55] + z[93];
z[58] = z[49] + z[59];
z[58] = abb[32] * z[58];
z[61] = abb[28] * z[49];
z[58] = z[58] + -z[61];
z[58] = -z[58] * z[84];
z[61] = -abb[30] + z[55];
z[59] = z[59] + z[61];
z[65] = abb[28] + -abb[32];
z[59] = z[59] * z[65];
z[59] = -abb[35] + -abb[56] + z[59];
z[65] = 3 * abb[14];
z[59] = z[37] * z[59] * z[65];
z[66] = -abb[6] * z[71];
z[1] = abb[61] + z[1] + 3 * z[4] + z[8] + z[17] + z[18] + z[19] + z[20] + z[21] + z[23] + z[26] + z[27] + z[32] + z[36] + z[40] + z[47] + z[50] + z[52] + z[58] + z[59] + z[66] + z[79] + z[81] + z[82] + z[91] + z[92] + z[99];
z[4] = -z[12] + -z[34];
z[4] = (T(1) / T(2)) * z[4] + -z[51] + z[53];
z[4] = abb[6] * z[4];
z[4] = z[4] + -3 * z[5] + -z[15] + -6 * z[24] + -z[29] + z[77] + -z[78];
z[4] = abb[28] * z[4];
z[5] = abb[45] + z[9] + -z[28] + z[31];
z[5] = z[5] * z[64];
z[8] = abb[28] * z[14];
z[9] = abb[29] * z[62];
z[5] = z[5] + z[8] + 2 * z[9] + -z[38] + -z[63];
z[5] = abb[3] * z[5];
z[8] = -z[6] + z[85];
z[9] = z[10] + -z[95];
z[9] = (T(1) / T(2)) * z[9] + z[39];
z[9] = z[8] + (T(1) / T(2)) * z[9];
z[9] = abb[29] * z[9];
z[2] = z[2] + -z[12];
z[17] = z[2] + z[33];
z[17] = -2 * abb[43] + (T(1) / T(2)) * z[17] + z[51];
z[17] = abb[28] * z[17];
z[18] = -abb[33] * z[60];
z[12] = 13 * abb[51] + -19 * z[10] + -z[12];
z[12] = (T(1) / T(2)) * z[12] + -z[39];
z[12] = abb[47] * (T(27) / T(4)) + -z[7] + (T(1) / T(2)) * z[12];
z[12] = abb[32] * z[12];
z[9] = z[9] + z[12] + z[17] + z[18] + z[69];
z[9] = abb[5] * z[9];
z[12] = z[2] + -z[33];
z[7] = z[7] + (T(1) / T(2)) * z[12] + -z[54];
z[7] = abb[6] * z[7];
z[7] = z[7] + z[16] + -z[29] + 6 * z[43];
z[7] = abb[31] * z[7];
z[12] = abb[16] * z[14];
z[12] = z[12] + -z[15] + -z[109];
z[0] = -z[0] + z[2] + -z[56];
z[0] = (T(1) / T(4)) * z[0] + z[6] + z[100];
z[0] = abb[6] * z[0];
z[0] = z[0] + -z[12];
z[0] = abb[29] * z[0];
z[2] = -abb[51] + z[10] + z[94];
z[2] = (T(1) / T(4)) * z[2] + z[8] + -z[39];
z[2] = abb[6] * z[2];
z[2] = z[2] + z[12];
z[2] = abb[32] * z[2];
z[6] = abb[25] + abb[26];
z[6] = z[6] * z[88];
z[6] = z[6] + -z[75] + -z[87];
z[6] = abb[33] * z[6];
z[8] = abb[21] + 3 * abb[24] + -abb[25];
z[10] = abb[32] * (T(1) / T(2));
z[8] = z[8] * z[10];
z[12] = abb[28] * z[105];
z[8] = z[8] + -z[12] + -z[103] + -z[106];
z[12] = abb[30] * (T(1) / T(2)) + z[86];
z[12] = abb[26] * z[12];
z[8] = (T(1) / T(2)) * z[8] + -z[12];
z[12] = 3 * z[22];
z[8] = z[8] * z[12];
z[12] = -abb[29] + abb[31];
z[12] = z[12] * z[35] * z[46];
z[14] = -abb[28] + abb[32] * (T(3) / T(2)) + -z[49];
z[14] = z[14] * z[84];
z[15] = abb[31] * z[45];
z[15] = z[15] + -z[89];
z[15] = z[15] * z[90];
z[16] = z[37] * z[65];
z[17] = -abb[28] + z[10] + -z[61];
z[17] = z[16] * z[17];
z[18] = -abb[28] + z[86];
z[18] = z[18] * z[48];
z[18] = z[18] + z[68];
z[18] = abb[2] * z[18];
z[10] = 4 * abb[28] + -z[10] + z[55];
z[10] = z[10] * z[41];
z[0] = z[0] + z[2] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[12] + z[14] + z[15] + z[17] + 2 * z[18] + -z[42];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = -z[16] + z[57];
z[2] = abb[37] * z[2];
z[4] = abb[3] + abb[5];
z[4] = z[4] * z[72];
z[5] = z[45] * z[90];
z[4] = z[4] + z[5] + -z[13];
z[4] = abb[39] * z[4];
z[5] = -abb[38] * z[25];
z[6] = abb[40] * z[30];
z[3] = abb[36] * z[3];
z[7] = -abb[27] * z[44];
z[7] = z[7] + z[11];
z[7] = (T(3) / T(2)) * z[7] + z[83];
z[7] = abb[41] * z[7];
z[0] = abb[42] + z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_510_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-3.842303589037347753716178442501725703371338453839769594880634106"),stof<T>("-15.122073742449383876612057114347449851412820922669855048999498447")}, std::complex<T>{stof<T>("3.893440894758377307040439996812365251846651570533894560844200437"),stof<T>("-34.933243350698998362134881502959126997562135208514762011002938503")}, std::complex<T>{stof<T>("12.383522700868492018002740053250414132546953604409824868966319568"),stof<T>("41.918269727917038272763595363361715822075714238259674331726658824")}, std::complex<T>{stof<T>("4.262265169913628654295379489958629188151132392251783412781500992"),stof<T>("-52.458930299853228537316550805892197671105966515254623505216716107")}, std::complex<T>{stof<T>("-16.64578787078212067229811954320904332069808599666160828174782056"),stof<T>("10.540660571936190264552955442530481849030252276994949173490057283")}, std::complex<T>{stof<T>("9.737478439781699218103355285164116424954793802480580206441893593"),stof<T>("90.052579443130110638767577156881597472689832334176827379851902006")}, std::complex<T>{stof<T>("-18.210871473887926973369852710770735252781745470290022066041329246"),stof<T>("-35.190035936572035802881414162403778979452855434852197429420906742")}, std::complex<T>{stof<T>("8.524530339827257308590758979917258376302264784503566825563001984"),stof<T>("-104.917860599706457074633101611784395342211933030509247010433432214")}, std::complex<T>{stof<T>("4.2111278641925991009711179356479896396758192755576584468179346614"),stof<T>("-2.4036132067048462985696121885856208221310103840700064452142791569")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_510_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_510_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(3) / T(4)) * (v[1] + v[2] + -v[3] + -v[4]) * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[43] + -abb[44] + -abb[45] + abb[47]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -prod_pow(abb[33], 2);
z[1] = prod_pow(abb[32], 2);
z[0] = z[0] + z[1];
z[1] = abb[43] + -abb[44] + -abb[45] + abb[47];
return 3 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_510_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_510_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-81.199048507109854769332774817313508379120960152831767735111172146"),stof<T>("-4.056165117565185265226046033670404533420903427368236749070554199")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,62> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W20(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W28(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_16(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), T{0}, rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k), T{0}};
abb[42] = SpDLog_f_4_510_W_20_Im(t, path, abb);
abb[61] = SpDLog_f_4_510_W_20_Re(t, path, abb);

                    
            return f_4_510_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_510_DLogXconstant_part(base_point<T>, kend);
	value += f_4_510_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_510_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_510_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_510_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_510_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_510_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_510_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
