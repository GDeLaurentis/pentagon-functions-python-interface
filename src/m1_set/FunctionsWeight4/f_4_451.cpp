/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_451.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_451_abbreviated (const std::array<T,58>& abb) {
T z[65];
z[0] = abb[26] * m1_set::bc<T>[0];
z[1] = abb[31] * (T(1) / T(2));
z[2] = m1_set::bc<T>[0] * z[1];
z[0] = z[0] + -z[2];
z[3] = abb[29] * m1_set::bc<T>[0];
z[4] = m1_set::bc<T>[0] * (T(1) / T(2));
z[5] = abb[30] * z[4];
z[6] = z[3] + -z[5];
z[7] = z[0] + -z[6];
z[8] = abb[36] + z[7];
z[9] = abb[14] * (T(1) / T(2));
z[8] = z[8] * z[9];
z[10] = abb[28] * m1_set::bc<T>[0];
z[2] = -z[2] + z[10];
z[10] = abb[27] * z[4];
z[11] = abb[37] * (T(1) / T(2));
z[10] = -z[2] + z[10] + -z[11];
z[12] = abb[7] * z[10];
z[13] = abb[3] + abb[5];
z[14] = -abb[7] + z[13];
z[15] = abb[38] * (T(1) / T(2));
z[16] = z[14] * z[15];
z[17] = abb[4] + abb[12];
z[18] = (T(1) / T(2)) * z[17];
z[19] = abb[36] * z[18];
z[8] = z[8] + z[12] + z[16] + -z[19];
z[12] = abb[27] * m1_set::bc<T>[0];
z[16] = -abb[26] + abb[28];
z[19] = m1_set::bc<T>[0] * z[16];
z[20] = z[12] + -z[19];
z[4] = abb[29] * z[4];
z[4] = z[4] + z[5] + z[11] + -z[20];
z[4] = abb[5] * z[4];
z[11] = -z[2] + -z[6] + z[12];
z[12] = abb[3] * (T(1) / T(2));
z[21] = z[11] * z[12];
z[20] = -z[3] + z[20];
z[22] = -abb[37] + z[20];
z[22] = abb[12] * z[22];
z[23] = abb[37] + z[19];
z[23] = abb[4] * z[23];
z[22] = z[22] + -z[23];
z[4] = z[4] + -z[8] + z[21] + (T(1) / T(2)) * z[22];
z[10] = -z[10] + z[15];
z[15] = abb[0] * z[10];
z[4] = (T(1) / T(2)) * z[4] + z[15];
z[4] = abb[50] * z[4];
z[15] = -abb[43] + -abb[44] + abb[45] + abb[46];
z[15] = -abb[42] + (T(1) / T(2)) * z[15];
z[21] = -abb[22] * z[10] * z[15];
z[7] = -abb[24] * z[7] * z[15];
z[22] = abb[24] * z[15];
z[24] = -abb[36] * z[22];
z[7] = z[7] + z[24];
z[24] = abb[49] + abb[50];
z[25] = abb[1] * z[24];
z[10] = -z[10] * z[25];
z[4] = z[4] + (T(1) / T(2)) * z[7] + z[10] + z[21];
z[7] = abb[3] * z[11];
z[10] = -abb[37] + -z[20];
z[10] = abb[12] * z[10];
z[11] = abb[30] * m1_set::bc<T>[0];
z[20] = abb[37] + -z[3] + z[11];
z[20] = abb[5] * z[20];
z[7] = z[7] + z[10] + z[20] + -z[23];
z[6] = -abb[37] + -z[2] + z[6];
z[10] = -abb[0] * z[6];
z[20] = abb[41] * (T(1) / T(2));
z[21] = abb[8] * z[19];
z[7] = (T(1) / T(2)) * z[7] + -z[8] + z[10] + -z[20] + -z[21];
z[7] = abb[49] * z[7];
z[8] = abb[36] + -abb[37];
z[10] = -z[8] + z[19];
z[10] = abb[0] * z[10];
z[19] = abb[12] * z[8];
z[10] = z[10] + z[19] + -z[21];
z[10] = abb[51] * z[10];
z[7] = z[7] + z[10];
z[10] = abb[21] + abb[23];
z[10] = (T(1) / T(16)) * z[10];
z[6] = z[6] * z[10] * z[15];
z[2] = abb[38] + z[2] + -z[5];
z[19] = abb[19] * (T(1) / T(16));
z[23] = -z[2] * z[15] * z[19];
z[0] = abb[38] + z[0] + -z[5];
z[5] = -z[0] + z[8];
z[5] = abb[50] * z[5];
z[26] = z[0] + z[8];
z[26] = abb[49] * z[26];
z[5] = z[5] + z[26];
z[26] = abb[6] * (T(1) / T(32));
z[5] = z[5] * z[26];
z[0] = abb[3] * z[0];
z[27] = abb[0] * z[2];
z[0] = z[0] + z[21] + -z[27];
z[21] = abb[26] + abb[28];
z[27] = -abb[31] + z[21];
z[27] = m1_set::bc<T>[0] * z[27];
z[11] = -z[11] + z[27];
z[11] = abb[38] + (T(1) / T(2)) * z[11];
z[28] = -abb[1] * z[11];
z[28] = (T(-1) / T(2)) * z[0] + z[28];
z[29] = abb[47] * (T(1) / T(8));
z[28] = z[28] * z[29];
z[3] = -abb[36] + -abb[38] + z[3] + -z[27];
z[27] = abb[20] * (T(1) / T(16));
z[3] = z[3] * z[15] * z[27];
z[30] = abb[47] + abb[50];
z[31] = abb[10] * (T(1) / T(8));
z[30] = z[30] * z[31];
z[31] = z[11] * z[30];
z[0] = -z[0] + z[20];
z[20] = -abb[1] + abb[10];
z[11] = z[11] * z[20];
z[0] = (T(1) / T(2)) * z[0] + z[11];
z[11] = abb[48] * (T(1) / T(8));
z[0] = z[0] * z[11];
z[32] = abb[49] + -abb[50];
z[33] = abb[13] * (T(1) / T(32));
z[32] = z[32] * z[33];
z[2] = z[2] * z[32];
z[33] = abb[16] + abb[18];
z[34] = abb[15] + abb[17];
z[35] = z[33] + z[34];
z[36] = abb[50] * (T(1) / T(2));
z[35] = z[35] * z[36];
z[36] = abb[25] * z[15];
z[35] = z[35] + z[36];
z[33] = z[33] + -z[34];
z[34] = abb[49] * z[33];
z[36] = -abb[47] + -abb[48];
z[36] = abb[16] * z[36];
z[34] = (T(-1) / T(2)) * z[34] + -z[35] + z[36];
z[34] = abb[39] * z[34];
z[36] = abb[51] + z[24];
z[37] = abb[11] * (T(1) / T(16));
z[36] = z[36] * z[37];
z[8] = -z[8] * z[36];
z[0] = abb[56] + z[0] + z[2] + z[3] + (T(1) / T(8)) * z[4] + z[5] + z[6] + (T(1) / T(16)) * z[7] + z[8] + z[23] + z[28] + z[31] + (T(1) / T(32)) * z[34];
z[2] = -abb[26] + abb[31];
z[3] = abb[29] + z[2];
z[4] = abb[27] * (T(1) / T(2));
z[5] = z[3] + -z[4];
z[5] = abb[27] * z[5];
z[6] = abb[29] * (T(1) / T(2));
z[7] = z[2] + z[6];
z[8] = abb[29] * z[7];
z[23] = abb[30] * (T(1) / T(2));
z[28] = z[2] * z[23];
z[31] = abb[26] * (T(1) / T(2));
z[34] = -abb[28] + z[31];
z[37] = abb[31] * z[34];
z[38] = prod_pow(m1_set::bc<T>[0], 2);
z[39] = (T(1) / T(2)) * z[38];
z[40] = prod_pow(abb[28], 2);
z[41] = (T(1) / T(2)) * z[40];
z[42] = abb[32] + -abb[34];
z[5] = z[5] + -z[8] + z[28] + z[37] + -z[39] + z[41] + z[42];
z[37] = -abb[3] * z[5];
z[43] = abb[29] + z[16];
z[44] = z[4] + -z[43];
z[45] = abb[27] * z[44];
z[46] = prod_pow(abb[26], 2);
z[47] = (T(1) / T(2)) * z[46];
z[48] = abb[34] + z[47];
z[49] = -abb[26] + abb[28] * (T(1) / T(2));
z[49] = abb[28] * z[49];
z[50] = -abb[28] + abb[31];
z[51] = -abb[30] * z[50];
z[51] = z[8] + z[45] + z[48] + z[49] + z[51];
z[51] = abb[5] * z[51];
z[52] = abb[28] * abb[31];
z[53] = abb[27] * z[50];
z[54] = (T(5) / T(6)) * z[38];
z[52] = -z[40] + z[52] + -z[53] + z[54];
z[53] = abb[53] + z[52];
z[55] = abb[7] * z[53];
z[56] = -abb[5] + z[17];
z[56] = abb[53] * z[56];
z[14] = abb[54] * z[14];
z[14] = z[14] + -z[55] + z[56];
z[3] = abb[29] * z[3];
z[43] = abb[30] * z[43];
z[3] = z[3] + -z[42] + -z[43];
z[55] = -abb[29] + abb[30];
z[56] = -z[50] + z[55];
z[56] = abb[27] * z[56];
z[57] = abb[28] * z[16];
z[58] = abb[31] * z[16];
z[56] = -z[3] + -z[56] + z[57] + -z[58];
z[56] = abb[12] * z[56];
z[40] = z[40] + -z[46];
z[57] = prod_pow(abb[29], 2);
z[59] = (T(1) / T(2)) * z[57];
z[40] = (T(1) / T(2)) * z[40] + z[59];
z[45] = -z[42] + z[45];
z[60] = z[40] + z[45];
z[61] = abb[4] * z[60];
z[17] = -abb[52] * z[17];
z[62] = -abb[33] * z[13];
z[17] = abb[57] + z[14] + z[17] + z[37] + z[51] + z[56] + -z[61] + z[62];
z[37] = (T(1) / T(3)) * z[38];
z[51] = abb[26] * z[1];
z[62] = z[37] + z[51];
z[63] = z[28] + z[62];
z[8] = z[8] + z[41] + z[45] + -z[63];
z[41] = abb[35] + -abb[53];
z[45] = -abb[33] + z[8] + z[41];
z[45] = abb[0] * z[45];
z[2] = abb[29] * z[2];
z[2] = -z[2] + -z[47] + z[63];
z[63] = abb[33] + abb[52] + z[2];
z[9] = z[9] * z[63];
z[63] = -abb[4] + abb[5];
z[63] = -abb[12] + z[12] + (T(1) / T(2)) * z[63];
z[63] = abb[35] * z[63];
z[17] = z[9] + (T(1) / T(2)) * z[17] + z[45] + z[63];
z[45] = abb[26] * abb[28];
z[63] = z[45] + -z[46];
z[64] = z[16] * z[23];
z[63] = -abb[32] + (T(1) / T(2)) * z[63] + -z[64];
z[63] = abb[8] * z[63];
z[33] = abb[55] * z[33];
z[17] = (T(1) / T(2)) * z[17] + (T(1) / T(8)) * z[33] + -z[63];
z[17] = abb[49] * z[17];
z[33] = abb[34] + z[40];
z[40] = z[4] * z[44];
z[44] = abb[35] * (T(1) / T(2));
z[33] = -abb[32] + (T(1) / T(2)) * z[33] + z[40] + z[44];
z[33] = abb[8] * z[33];
z[40] = abb[52] + z[41];
z[41] = z[40] + z[60];
z[41] = abb[0] * z[41];
z[60] = -abb[12] * z[40];
z[41] = z[41] + z[60];
z[41] = -z[33] + (T(1) / T(2)) * z[41];
z[41] = abb[51] * z[41];
z[17] = z[17] + z[41];
z[5] = -z[5] * z[12];
z[12] = abb[3] + -abb[5];
z[41] = abb[33] * z[12];
z[14] = z[14] + -z[41] + -z[56] + -z[61];
z[41] = -abb[26] + abb[28] * (T(3) / T(2));
z[41] = abb[28] * z[41];
z[37] = -z[37] + z[41] + -z[48];
z[23] = z[23] * z[50];
z[7] = -z[6] * z[7];
z[6] = abb[31] + abb[27] * (T(-1) / T(4)) + z[6] + (T(-1) / T(2)) * z[21];
z[6] = abb[27] * z[6];
z[6] = z[6] + z[7] + -z[23] + (T(1) / T(2)) * z[37] + -z[58];
z[6] = abb[5] * z[6];
z[7] = abb[54] + z[53];
z[21] = -abb[0] * z[7];
z[12] = -abb[4] + z[12];
z[12] = z[12] * z[44];
z[18] = -abb[52] * z[18];
z[5] = z[5] + z[6] + z[9] + z[12] + (T(1) / T(2)) * z[14] + z[18] + z[21];
z[5] = abb[50] * z[5];
z[6] = z[15] * z[52];
z[9] = abb[53] * z[15];
z[12] = abb[54] * z[15];
z[6] = z[6] + z[9] + z[12];
z[6] = abb[22] * z[6];
z[7] = z[7] * z[25];
z[2] = z[2] * z[15];
z[14] = abb[33] * z[15];
z[2] = z[2] + z[14];
z[2] = abb[24] * z[2];
z[18] = abb[52] * z[22];
z[2] = z[2] + z[5] + z[6] + z[7] + z[18];
z[4] = -abb[30] + z[4] + -z[16];
z[4] = abb[27] * z[4];
z[4] = -abb[33] + z[4] + z[49] + -z[59];
z[5] = abb[54] + z[51];
z[6] = (T(1) / T(6)) * z[38];
z[7] = abb[28] + abb[29] + abb[26] * (T(-3) / T(2)) + z[1];
z[7] = abb[30] * z[7];
z[7] = abb[35] + -abb[52] + abb[53] + z[4] + -z[5] + -z[6] + z[7] + z[46];
z[7] = abb[49] * z[7];
z[1] = z[1] + z[34];
z[16] = -abb[29] + z[1];
z[16] = abb[30] * z[16];
z[5] = -z[5] + z[16] + -z[39];
z[16] = z[4] + -z[5] + -z[40];
z[16] = abb[50] * z[16];
z[7] = z[7] + z[16];
z[7] = z[7] * z[26];
z[1] = abb[30] * z[1];
z[16] = z[45] + -z[47];
z[1] = -abb[32] + z[1] + z[16] + -z[62];
z[18] = -abb[54] + z[1];
z[21] = abb[0] * z[18];
z[22] = -abb[54] + z[28] + z[47] + -z[62];
z[25] = -abb[32] + -z[22];
z[25] = abb[3] * z[25];
z[21] = z[21] + z[25];
z[25] = abb[28] * z[31];
z[23] = -abb[54] + z[23] + z[25] + -z[62];
z[25] = -abb[1] * z[23];
z[26] = abb[16] * abb[55];
z[26] = (T(1) / T(4)) * z[26];
z[21] = (T(1) / T(2)) * z[21] + z[25] + z[26] + -z[63];
z[21] = z[21] * z[29];
z[25] = abb[27] * z[55];
z[28] = abb[26] * abb[31];
z[3] = abb[52] + -z[3] + -z[25] + z[28] + -z[45] + z[54];
z[3] = z[3] * z[15];
z[3] = z[3] + z[12];
z[3] = z[3] * z[27];
z[4] = -z[4] + -z[6] + -z[43] + -z[47];
z[4] = abb[5] * z[4];
z[25] = abb[33] + z[25] + z[57];
z[5] = z[5] + z[16] + z[25] + -z[42];
z[5] = abb[0] * z[5];
z[16] = -abb[3] * z[22];
z[13] = -abb[35] * z[13];
z[4] = abb[57] * (T(-1) / T(2)) + z[4] + z[5] + z[13] + z[16];
z[5] = z[20] * z[23];
z[4] = (T(1) / T(2)) * z[4] + z[5] + z[26] + -z[33];
z[4] = z[4] * z[11];
z[5] = -abb[35] + -z[8];
z[5] = z[5] * z[15];
z[5] = z[5] + z[9] + z[14];
z[5] = z[5] * z[10];
z[1] = -z[1] * z[15];
z[1] = z[1] + z[12];
z[1] = z[1] * z[19];
z[8] = z[18] * z[32];
z[9] = abb[29] * abb[30];
z[6] = -abb[34] + z[6] + z[9] + -z[25];
z[6] = abb[2] * z[6] * z[24];
z[9] = abb[55] * z[35];
z[6] = z[6] + z[9];
z[9] = z[23] * z[30];
z[10] = z[36] * z[40];
z[1] = abb[40] + z[1] + (T(1) / T(16)) * z[2] + z[3] + z[4] + z[5] + (T(1) / T(32)) * z[6] + z[7] + z[8] + z[9] + z[10] + (T(1) / T(8)) * z[17] + z[21];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_451_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.24113092074640388332875526100108878623837288398082771037094547575"),stof<T>("-0.01624583897832471480970255068419892981349816475670169103204217271")}, std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.01119107242493583744221878775161478282355945045960599580395625581"),stof<T>("0.14588860021417846800102478009988761746534936739225054495129151965")}, std::complex<T>{stof<T>("0.21663314643471741122600284546309920421091298258275388620202231402"),stof<T>("0.14588860021417846800102478009988761746534936739225054495129151965")}, std::complex<T>{stof<T>("0.05127424407484608359169861243217435432985924761686601939887897725"),stof<T>("0.13487476495921067150340705210973790831041360905933467657212596554")}, std::complex<T>{stof<T>("0.47545341071950454568343583010671366567649273593187729114674517307"),stof<T>("0.40687630736128679929112184295536286802749357323732339648371288076")}, std::complex<T>{stof<T>("-0.00650108883169777177674601924065865208061292450316619317647578402"),stof<T>("0.14862968680540993627472910161979133818577915019246332137552000078")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_451_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_451_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_451_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(512)) * (-v[3] + v[5]) * (-24 * v[0] + 8 * v[1] + 6 * v[3] + -6 * v[5] + m1_set::bc<T>[2] * (12 * v[0] + -4 * v[1] + 3 * (8 + v[3] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[2] * (T(3) / T(64)) * (-v[3] + v[5])) / tend;


		return (abb[48] + abb[49] + abb[51]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[48] + abb[49] + abb[51];
z[1] = -abb[32] + abb[35];
return abb[9] * (T(1) / T(16)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_451_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.48248148640221261076385217222013159313429013656826037603043918887"),stof<T>("-0.00681280026213762734428781509628845927330893600981912544301744637")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,58> abb = {dl[0], dl[1], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W16(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_3(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_25_im(k), T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_25_re(k), T{0}, T{0}};
abb[40] = SpDLog_f_4_451_W_16_Im(t, path, abb);
abb[41] = SpDLogQ_W_72(k,dl,dlr).imag();
abb[56] = SpDLog_f_4_451_W_16_Re(t, path, abb);
abb[57] = SpDLogQ_W_72(k,dl,dlr).real();

                    
            return f_4_451_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_451_DLogXconstant_part(base_point<T>, kend);
	value += f_4_451_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_451_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_451_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_451_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_451_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_451_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_451_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
