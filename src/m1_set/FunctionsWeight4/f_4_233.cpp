/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_233.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_233_abbreviated (const std::array<T,27>& abb) {
T z[35];
z[0] = 5 * abb[22];
z[1] = -abb[18] + z[0];
z[2] = abb[19] * (T(5) / T(4));
z[3] = 2 * abb[20];
z[1] = abb[21] * (T(-1) / T(2)) + (T(1) / T(4)) * z[1] + z[2] + -z[3];
z[1] = abb[3] * z[1];
z[4] = abb[7] + abb[8];
z[5] = abb[23] + abb[24];
z[6] = (T(1) / T(4)) * z[5];
z[4] = z[4] * z[6];
z[6] = abb[19] * (T(1) / T(2));
z[7] = abb[18] + -abb[22];
z[8] = -abb[21] + z[6] + (T(-1) / T(2)) * z[7];
z[9] = abb[2] * (T(1) / T(2));
z[10] = z[8] * z[9];
z[4] = z[4] + -z[10];
z[10] = abb[21] * (T(3) / T(2)) + -z[3];
z[11] = 3 * abb[18];
z[12] = -abb[22] + z[11];
z[13] = abb[19] + z[10] + (T(1) / T(2)) * z[12];
z[14] = abb[6] * z[13];
z[15] = -z[4] + z[14];
z[16] = abb[18] + abb[22];
z[17] = -z[3] + (T(1) / T(2)) * z[16];
z[6] = abb[21] + z[6] + z[17];
z[6] = abb[0] * z[6];
z[18] = 4 * abb[20];
z[19] = 2 * abb[19] + -z[18];
z[20] = abb[18] + abb[21];
z[21] = abb[22] + z[19] + z[20];
z[22] = abb[5] * z[21];
z[23] = abb[21] + z[7];
z[24] = abb[1] * z[23];
z[25] = 2 * z[24];
z[1] = z[1] + -z[6] + z[15] + -z[22] + -z[25];
z[6] = abb[13] * z[1];
z[26] = 3 * abb[21];
z[12] = z[12] + z[19] + z[26];
z[19] = abb[6] * z[12];
z[27] = abb[3] * z[23];
z[28] = z[25] + z[27];
z[21] = abb[0] * z[21];
z[21] = z[19] + -z[21] + -z[22] + -2 * z[28];
z[28] = abb[14] * z[21];
z[6] = z[6] + -z[28];
z[28] = abb[19] * (T(3) / T(4));
z[29] = 5 * abb[18] + -abb[22];
z[29] = z[10] + z[28] + (T(1) / T(4)) * z[29];
z[29] = abb[3] * z[29];
z[7] = abb[19] + z[7];
z[30] = abb[4] * z[7];
z[31] = z[24] + z[30];
z[32] = abb[19] * (T(3) / T(2));
z[17] = z[17] + z[32];
z[17] = abb[0] * z[17];
z[15] = -z[15] + z[17] + z[29] + z[31];
z[15] = abb[11] * z[15];
z[15] = -z[6] + z[15];
z[15] = abb[11] * z[15];
z[29] = 3 * abb[22];
z[33] = 7 * abb[18];
z[34] = z[29] + -z[33];
z[2] = -z[2] + -z[10] + (T(1) / T(4)) * z[34];
z[2] = abb[3] * z[2];
z[2] = z[2] + z[4] + z[14] + -z[17] + -z[25];
z[2] = abb[11] * z[2];
z[14] = abb[2] * z[8];
z[17] = (T(1) / T(2)) * z[5];
z[25] = abb[8] * z[17];
z[14] = z[14] + -z[25];
z[7] = abb[0] * z[7];
z[7] = (T(-1) / T(2)) * z[7] + z[14] + z[24];
z[7] = abb[12] * z[7];
z[2] = z[2] + z[6] + z[7];
z[2] = abb[12] * z[2];
z[6] = abb[3] * z[12];
z[7] = z[16] + -z[18];
z[16] = 3 * abb[19] + z[7];
z[16] = abb[0] * z[16];
z[6] = z[6] + z[16] + -z[19] + 3 * z[24] + z[30];
z[6] = abb[15] * z[6];
z[16] = abb[21] * (T(11) / T(2));
z[25] = 11 * abb[18] + -abb[22];
z[25] = 5 * abb[19] + -10 * abb[20] + z[16] + (T(1) / T(2)) * z[25];
z[25] = abb[5] * z[25];
z[16] = abb[19] * (T(-5) / T(2)) + z[7] + z[16];
z[16] = abb[0] * z[16];
z[16] = z[16] + z[25];
z[0] = -z[0] + -z[33];
z[0] = abb[21] * (T(-5) / T(6)) + (T(1) / T(12)) * z[0] + z[3] + -z[28];
z[0] = abb[3] * z[0];
z[3] = abb[19] * (T(-2) / T(3)) + abb[22] * (T(1) / T(3)) + abb[20] * (T(4) / T(3)) + -z[20];
z[3] = abb[6] * z[3];
z[0] = z[0] + z[3] + -z[4] + (T(1) / T(3)) * z[16] + (T(4) / T(3)) * z[24];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[3] = -abb[25] * z[21];
z[4] = abb[5] * z[23];
z[4] = z[4] + -z[24];
z[16] = abb[19] + -abb[21];
z[16] = abb[0] * z[16];
z[20] = -z[4] + z[16] + z[27] + -z[30];
z[20] = prod_pow(abb[14], 2) * z[20];
z[23] = abb[8] * z[8];
z[9] = z[5] * z[9];
z[25] = abb[3] * z[17];
z[9] = -z[9] + z[23] + -z[25];
z[23] = abb[0] * z[5];
z[9] = 3 * z[9] + -z[23];
z[11] = abb[19] + abb[22] + z[11];
z[10] = z[10] + (T(1) / T(4)) * z[11];
z[10] = abb[7] * z[10];
z[11] = abb[9] * z[13];
z[5] = abb[10] * z[5];
z[5] = z[5] + (T(1) / T(2)) * z[9] + -z[10] + -z[11];
z[9] = abb[26] * z[5];
z[10] = -abb[0] + abb[3];
z[8] = z[8] * z[10];
z[10] = abb[7] * z[17];
z[4] = z[4] + z[8] + -z[10];
z[4] = prod_pow(abb[13], 2) * z[4];
z[0] = z[0] + z[2] + z[3] + z[4] + z[6] + z[9] + z[15] + z[20];
z[2] = -abb[18] + -z[29];
z[2] = -abb[21] + (T(1) / T(2)) * z[2] + z[18] + -z[32];
z[2] = abb[3] * z[2];
z[3] = z[24] + -z[30];
z[2] = z[2] + 2 * z[3] + -z[10] + z[14] + -z[16] + z[22];
z[2] = abb[11] * z[2];
z[3] = abb[12] + abb[13];
z[1] = z[1] * z[3];
z[3] = abb[5] * z[12];
z[4] = z[7] + z[26];
z[4] = abb[0] * z[4];
z[3] = z[3] + z[4] + -z[19] + 2 * z[31];
z[3] = abb[14] * z[3];
z[1] = z[1] + z[2] + z[3];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[16] * z[21];
z[3] = abb[17] * z[5];
z[1] = z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_233_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("7.909263792278041320902281223678406118958628280121449161936897453"),stof<T>("-14.537522938098481218705331931744807166117501719414541843419244599")}, std::complex<T>{stof<T>("14.982626219847782603885226759032807502917009220264943706751859972"),stof<T>("11.332710587535620408149905536827669082773896344461882102745671728")}, std::complex<T>{stof<T>("-8.040834126886106802650330010738754285249388427181374545849879258"),stof<T>("32.43309346600495590709570073580188232060384424044291142540614369")}, std::complex<T>{stof<T>("-3.05294536412668788165778052998502424133368672655280727189002289"),stof<T>("-42.086780258636579580403087836473417409193320184097879658867988172")}, std::complex<T>{stof<T>("-3.8888467288349879195771162183090289763339340665307618890119578244"),stof<T>("-1.6790237949039967348425184361561339941844204008069138692838272465")}, std::complex<T>{stof<T>("-1.8875214468817965659845470056534843067331750307489830546406995373"),stof<T>("3.073952256592314428553215974881268771197457590719930264362430233")}, std::complex<T>{stof<T>("-1.8875214468817965659845470056534843067331750307489830546406995373"),stof<T>("3.073952256592314428553215974881268771197457590719930264362430233")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[22].real()/kbase.W[22].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[126].real()/kbase.W[126].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_233_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_233_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-27.166652420175560841150926957791956306991572885180823253947715592"),stof<T>("5.55132547512658621891976612634307207230559629635751190185306417")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,27> abb = {dl[0], dlog_W3(k,dl), dlog_W8(k,dl), dl[3], dlog_W13(k,dl), dlog_W23(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W127(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[22].real()/k.W[22].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[126].real()/k.W[126].real()), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_233_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_233_DLogXconstant_part(base_point<T>, kend);
	value += f_4_233_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_233_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_233_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_233_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_233_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_233_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_233_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
