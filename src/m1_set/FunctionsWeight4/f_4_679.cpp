/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_679.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_679_abbreviated (const std::array<T,38>& abb) {
T z[54];
z[0] = abb[0] + abb[4];
z[1] = 4 * abb[10];
z[2] = 2 * abb[8];
z[3] = 2 * abb[6];
z[4] = z[0] + -z[1] + z[2] + z[3];
z[4] = abb[29] * z[4];
z[5] = abb[0] + abb[9];
z[6] = 2 * abb[4];
z[7] = z[5] + -z[6];
z[8] = abb[3] + z[7];
z[9] = 4 * abb[1];
z[10] = -z[2] + z[9];
z[11] = -z[3] + -z[8] + z[10];
z[11] = abb[31] * z[11];
z[12] = abb[0] + 2 * abb[9];
z[13] = -abb[4] + z[12];
z[14] = 2 * abb[10];
z[10] = -z[10] + z[13] + -z[14];
z[10] = abb[30] * z[10];
z[15] = abb[26] + abb[27];
z[16] = -abb[28] + abb[32];
z[17] = 2 * z[16];
z[18] = -z[15] + z[17];
z[19] = abb[11] * z[18];
z[20] = abb[13] * z[18];
z[21] = abb[31] * z[14];
z[21] = z[19] + -z[20] + z[21];
z[22] = abb[12] * z[18];
z[4] = z[4] + z[10] + z[11] + z[21] + z[22];
z[10] = abb[36] * z[4];
z[11] = z[19] + z[20];
z[19] = abb[31] * z[8];
z[23] = 3 * abb[4];
z[12] = -z[12] + z[23];
z[24] = abb[30] * z[12];
z[25] = 2 * abb[3];
z[26] = abb[0] + -abb[4];
z[27] = z[25] + -z[26];
z[27] = abb[29] * z[27];
z[28] = abb[30] + abb[31];
z[29] = abb[2] * z[28];
z[19] = -z[11] + -z[19] + -z[22] + z[24] + z[27] + 4 * z[29];
z[22] = abb[35] * z[19];
z[24] = abb[8] + z[3];
z[27] = abb[3] + abb[4] + -z[14] + z[24];
z[27] = abb[29] * z[27];
z[28] = abb[29] + z[28];
z[28] = abb[7] * z[28];
z[30] = abb[4] + abb[8] + -abb[10];
z[30] = abb[30] * z[30];
z[24] = -abb[4] + z[24];
z[24] = abb[31] * z[24];
z[31] = abb[10] * abb[31];
z[24] = z[24] + -z[27] + z[28] + -z[29] + -z[30] + -z[31];
z[27] = abb[37] * z[24];
z[30] = 3 * abb[6] + abb[8] + z[25];
z[1] = -z[1] + -z[26] + z[30];
z[1] = abb[29] * z[1];
z[12] = -abb[8] + -z[12] + z[14];
z[12] = abb[30] * z[12];
z[14] = z[7] + z[30];
z[14] = abb[31] * z[14];
z[1] = -z[1] + z[12] + z[14] + -z[21] + -5 * z[29];
z[12] = abb[34] * z[1];
z[14] = -abb[17] + abb[18];
z[21] = -abb[15] + 2 * z[14];
z[21] = abb[15] * z[21];
z[26] = prod_pow(abb[17], 2);
z[21] = z[21] + -z[26];
z[30] = 2 * abb[17];
z[32] = -abb[18] + z[30];
z[32] = abb[18] * z[32];
z[33] = z[21] + z[32];
z[34] = -abb[15] + z[14];
z[35] = 3 * abb[16] + 4 * z[34];
z[35] = abb[16] * z[35];
z[35] = -z[33] + z[35];
z[35] = z[29] * z[35];
z[36] = prod_pow(abb[16], 2);
z[37] = -abb[33] + -z[26] + z[36];
z[37] = -z[28] * z[37];
z[10] = -z[10] + z[12] + -z[22] + z[27] + z[35] + z[37];
z[12] = abb[17] * abb[18];
z[22] = 2 * z[12];
z[21] = -z[21] + -z[22];
z[21] = abb[4] * z[21];
z[27] = z[12] + -z[26];
z[35] = abb[15] * z[34];
z[37] = z[27] + z[35];
z[38] = abb[9] * z[37];
z[39] = -abb[18] * z[14];
z[39] = z[35] + z[39];
z[39] = abb[0] * z[39];
z[40] = abb[15] + -abb[18];
z[14] = z[14] * z[40];
z[14] = z[14] + -z[26];
z[14] = abb[3] * z[14];
z[41] = abb[33] * z[7];
z[42] = abb[8] * z[26];
z[43] = prod_pow(abb[15], 2);
z[44] = abb[6] * z[43];
z[14] = z[14] + z[21] + z[38] + z[39] + z[41] + z[42] + z[44];
z[6] = -abb[0] + -abb[3] + z[6];
z[21] = z[6] * z[34];
z[38] = abb[9] * z[34];
z[21] = z[21] + -z[38];
z[5] = abb[3] + -4 * abb[4] + z[5];
z[39] = -z[3] + -z[5];
z[39] = abb[16] * z[39];
z[21] = 2 * z[21] + z[39];
z[21] = abb[16] * z[21];
z[39] = -abb[33] + z[43];
z[41] = 2 * abb[5];
z[39] = z[39] * z[41];
z[33] = 3 * abb[33] + z[33];
z[42] = 2 * abb[1];
z[44] = -z[33] * z[42];
z[14] = 2 * z[14] + z[21] + z[39] + z[44];
z[14] = abb[31] * z[14];
z[21] = abb[6] * (T(13) / T(3));
z[44] = abb[1] * (T(1) / T(3));
z[45] = abb[5] * (T(5) / T(3));
z[46] = abb[8] + (T(2) / T(3)) * z[8] + z[21] + z[44] + -z[45];
z[46] = abb[31] * z[46];
z[47] = abb[8] + abb[4] * (T(8) / T(3));
z[21] = abb[3] * (T(-8) / T(3)) + abb[10] * (T(16) / T(3)) + -z[21] + -z[45] + -z[47];
z[21] = abb[29] * z[21];
z[44] = abb[10] * (T(8) / T(3)) + -z[44] + -z[47];
z[44] = abb[30] * z[44];
z[21] = (T(2) / T(3)) * z[20] + z[21] + (T(5) / T(3)) * z[28] + -3 * z[29] + (T(-8) / T(3)) * z[31] + z[44] + z[46];
z[21] = prod_pow(m1_set::bc<T>[0], 2) * z[21];
z[27] = z[16] * z[27];
z[31] = abb[18] * z[16];
z[44] = abb[15] * z[16];
z[31] = z[31] + -z[44];
z[45] = abb[17] * z[16];
z[46] = -z[31] + z[45];
z[47] = -abb[15] * z[46];
z[27] = z[27] + z[47];
z[37] = -z[15] * z[37];
z[47] = abb[33] * z[18];
z[27] = 2 * z[27] + z[37] + z[47];
z[15] = z[15] * z[34];
z[15] = z[15] + 2 * z[46];
z[37] = 2 * z[15];
z[46] = abb[16] * z[18];
z[48] = z[37] + -z[46];
z[49] = abb[16] * z[48];
z[27] = 2 * z[27] + z[49];
z[27] = abb[13] * z[27];
z[14] = z[14] + z[21] + z[27];
z[21] = -abb[0] + abb[3];
z[27] = abb[6] + z[21];
z[27] = z[27] * z[36];
z[36] = abb[33] * z[0];
z[49] = -abb[5] + abb[8] + -z[21];
z[49] = prod_pow(abb[19], 2) * z[49];
z[27] = -z[27] + z[36] + -z[49];
z[36] = 3 * abb[15];
z[49] = 2 * abb[18];
z[50] = z[36] + -z[49];
z[50] = abb[15] * z[50];
z[22] = z[22] + -3 * z[26] + z[50];
z[22] = abb[0] * z[22];
z[51] = 2 * z[26];
z[35] = -z[12] + z[35] + z[51];
z[25] = z[25] * z[35];
z[30] = abb[15] + -z[30];
z[30] = abb[15] * z[30];
z[30] = z[26] + z[30];
z[30] = abb[4] * z[30];
z[35] = -z[2] * z[26];
z[3] = -z[3] * z[43];
z[3] = z[3] + z[22] + z[25] + -2 * z[27] + z[30] + z[35] + z[39];
z[22] = 2 * abb[29];
z[3] = z[3] * z[22];
z[25] = abb[4] * z[34];
z[25] = z[25] + -z[38];
z[27] = -abb[16] * z[7];
z[27] = 2 * z[25] + z[27];
z[27] = abb[16] * z[27];
z[30] = z[34] * z[42];
z[25] = -z[25] + -z[30];
z[35] = 3 * abb[1];
z[7] = -z[7] + z[35];
z[39] = abb[8] + -z[7];
z[39] = abb[19] * z[39];
z[25] = 2 * z[25] + z[39];
z[25] = abb[19] * z[25];
z[39] = abb[33] * z[13];
z[25] = -z[25] + -z[27] + z[39];
z[27] = 3 * abb[17];
z[39] = -z[27] + -z[40];
z[39] = abb[15] * z[39];
z[43] = 4 * z[26];
z[39] = -z[12] + z[39] + z[43];
z[39] = abb[4] * z[39];
z[32] = z[32] + z[50] + -z[51];
z[32] = abb[0] * z[32];
z[50] = abb[17] + z[49];
z[50] = abb[18] * z[50];
z[51] = abb[15] + abb[17];
z[52] = -abb[18] + -z[51];
z[52] = abb[15] * z[52];
z[50] = z[50] + z[52];
z[50] = abb[3] * z[50];
z[33] = z[9] * z[33];
z[52] = -abb[8] * z[43];
z[25] = -4 * z[25] + 2 * z[32] + z[33] + z[39] + z[50] + z[52];
z[25] = abb[30] * z[25];
z[32] = -z[12] * z[16];
z[31] = z[31] + z[45];
z[31] = abb[15] * z[31];
z[31] = z[31] + z[32];
z[32] = 2 * abb[16];
z[33] = z[32] * z[48];
z[18] = abb[19] * z[18];
z[37] = z[18] + -z[37];
z[39] = 2 * abb[19];
z[37] = z[37] * z[39];
z[45] = z[27] + z[49];
z[45] = abb[18] * z[45];
z[50] = abb[17] + abb[18];
z[52] = abb[15] + -3 * z[50];
z[52] = abb[15] * z[52];
z[45] = z[45] + z[52];
z[45] = abb[27] * z[45];
z[52] = z[36] + -z[50];
z[52] = abb[15] * z[52];
z[53] = abb[17] + -z[49];
z[53] = abb[18] * z[53];
z[52] = z[52] + z[53];
z[52] = abb[26] * z[52];
z[31] = 4 * z[31] + z[33] + z[37] + z[45] + z[52];
z[31] = abb[12] * z[31];
z[23] = 4 * abb[14] + -z[23];
z[33] = -abb[27] * z[23];
z[37] = 4 * z[16];
z[45] = abb[0] * z[37];
z[23] = -4 * abb[0] + z[23];
z[23] = abb[26] * z[23];
z[37] = -abb[26] + 5 * abb[27] + -z[37];
z[37] = abb[3] * z[37];
z[52] = 5 * abb[11] + -8 * abb[13];
z[52] = abb[30] * z[52];
z[53] = 4 * abb[29] + 3 * abb[30] + -8 * abb[31];
z[53] = abb[12] * z[53];
z[23] = z[23] + z[33] + z[37] + z[45] + z[52] + z[53];
z[23] = abb[20] * z[23];
z[16] = -z[16] * z[26];
z[17] = abb[17] * z[17];
z[17] = z[17] + -z[44];
z[17] = abb[15] * z[17];
z[16] = z[16] + z[17] + z[47];
z[17] = -5 * abb[17] + z[40];
z[17] = abb[15] * z[17];
z[17] = z[12] + z[17] + z[43];
z[17] = abb[27] * z[17];
z[26] = abb[18] + -z[27] + z[36];
z[26] = abb[15] * z[26];
z[12] = -z[12] + z[26];
z[12] = abb[26] * z[12];
z[12] = z[12] + 4 * z[16] + z[17];
z[12] = abb[11] * z[12];
z[15] = abb[13] * z[15];
z[6] = z[6] + z[9];
z[6] = z[6] * z[34];
z[6] = z[6] + -z[38];
z[6] = abb[31] * z[6];
z[6] = z[6] + z[15];
z[5] = 6 * abb[1] + -z[2] + -z[5] + -z[41];
z[5] = abb[31] * z[5];
z[5] = z[5] + -z[20];
z[5] = abb[19] * z[5];
z[5] = z[5] + 2 * z[6];
z[5] = z[5] * z[39];
z[3] = z[3] + z[5] + 4 * z[10] + z[12] + 2 * z[14] + z[23] + z[25] + z[31];
z[3] = 4 * z[3];
z[5] = -abb[4] + abb[9];
z[6] = -z[5] * z[32];
z[5] = z[5] + -z[42];
z[5] = z[5] * z[39];
z[10] = abb[15] + -abb[17];
z[12] = abb[4] * z[10];
z[14] = -2 * abb[15] + z[50];
z[14] = abb[0] * z[14];
z[2] = abb[17] * z[2];
z[15] = abb[3] * z[40];
z[2] = z[2] + z[5] + z[6] + z[12] + z[14] + z[15] + -z[30];
z[2] = abb[30] * z[2];
z[5] = z[49] + -z[51];
z[5] = abb[4] * z[5];
z[6] = abb[3] + -abb[8];
z[6] = abb[17] * z[6];
z[12] = abb[0] + -abb[6];
z[12] = abb[15] * z[12];
z[5] = z[5] + z[6] + z[12] + -z[38];
z[6] = -abb[16] * z[8];
z[12] = -abb[15] * z[41];
z[5] = 2 * z[5] + z[6] + z[12] + z[30];
z[5] = abb[31] * z[5];
z[6] = abb[26] + -abb[27];
z[12] = -z[6] * z[40];
z[12] = z[12] + z[18] + -z[46];
z[12] = abb[12] * z[12];
z[14] = abb[13] * z[48];
z[8] = -z[8] + z[9];
z[8] = abb[31] * z[8];
z[8] = z[8] + -z[20];
z[8] = abb[19] * z[8];
z[9] = z[10] * z[21];
z[15] = -abb[5] + abb[6];
z[15] = abb[15] * z[15];
z[16] = abb[8] * abb[17];
z[9] = z[9] + z[15] + z[16];
z[9] = z[9] * z[22];
z[15] = z[32] + z[34];
z[15] = z[15] * z[29];
z[16] = -abb[17] * z[28];
z[15] = z[15] + z[16];
z[6] = -abb[11] * z[6] * z[10];
z[2] = z[2] + z[5] + z[6] + z[8] + z[9] + z[12] + z[14] + 2 * z[15];
z[2] = m1_set::bc<T>[0] * z[2];
z[5] = abb[25] * z[24];
z[6] = -abb[23] * z[19];
z[4] = -abb[24] * z[4];
z[7] = -abb[5] + -z[7];
z[7] = abb[31] * z[7];
z[8] = -z[13] + z[35];
z[8] = abb[30] * z[8];
z[0] = -abb[5] + -z[0];
z[0] = abb[29] * z[0];
z[0] = z[0] + z[7] + z[8] + z[11] + z[28];
z[0] = abb[21] * z[0];
z[1] = abb[22] * z[1];
z[0] = z[0] + z[1] + z[2] + z[4] + z[5] + z[6];
z[0] = 16 * z[0];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_679_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-5.705317672519195633917296582123317599139446188971369529034283413"),stof<T>("-125.991453537739045707660668897222826401320496131828772328508469875")}, std::complex<T>{stof<T>("-22.092882292884777496091312788923492457568468886615937111680321457"),stof<T>("125.991453537739045707660668897222826401320496131828772328508469875")}, stof<T>("-27.798199965403973130008609371046810056707915075587306640714604869"), std::complex<T>{stof<T>("423.83192844570989376038795905092657361938670148555978987012769625"),stof<T>("-188.56745743608156690960237938370586139384591512463871505505349955")}, std::complex<T>{stof<T>("221.47021266764823351389311525483808379997310665654329269975418311"),stof<T>("-268.85443304831246246612245655468847877469264136452050468818546893")}, std::complex<T>{stof<T>("-9.530254697506458330366177977756489869201017321208368866048547077"),stof<T>("-63.980943890468741000571269798176635800199154956095095053712380312")}, stof<T>("27.798199965403973130008609371046810056707915075587306640714604869")};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real()), rlog(k.W[194].real()/kbase.W[194].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_679_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_679_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-2.08928139868399970115768256193022330714661590170312565769830851"),stof<T>("227.55140511399561263144370373508185988214480508860657294840459586")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,38> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), dlog_W69(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_11(k), f_2_23(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), rlog(kend.W[194].real()/k.W[194].real()), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k)};

                    
            return f_4_679_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_679_DLogXconstant_part(base_point<T>, kend);
	value += f_4_679_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_679_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_679_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_679_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_679_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_679_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_679_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
