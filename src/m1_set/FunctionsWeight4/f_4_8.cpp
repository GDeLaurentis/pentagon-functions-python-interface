/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_8.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_8_abbreviated (const std::array<T,27>& abb) {
T z[40];
z[0] = abb[21] * (T(1) / T(2));
z[1] = -abb[23] + z[0];
z[2] = abb[18] * (T(1) / T(2));
z[3] = -z[1] + z[2];
z[4] = abb[24] * (T(3) / T(2));
z[5] = 2 * abb[19] + abb[20];
z[6] = 2 * z[5];
z[7] = z[3] + z[4] + -z[6];
z[8] = abb[3] * z[7];
z[9] = abb[25] + abb[26];
z[10] = abb[8] * (T(3) / T(2));
z[10] = z[9] * z[10];
z[11] = abb[7] * z[9];
z[8] = -z[8] + z[10] + (T(3) / T(2)) * z[11];
z[10] = 4 * z[5];
z[12] = 3 * abb[24];
z[13] = z[10] + -z[12];
z[14] = abb[18] + abb[23];
z[15] = z[13] + -z[14];
z[16] = 4 * abb[1];
z[16] = z[15] * z[16];
z[17] = -abb[24] + z[6] + -z[14];
z[18] = 3 * abb[6];
z[18] = z[17] * z[18];
z[16] = z[16] + -z[18];
z[19] = 8 * z[5];
z[3] = abb[24] * (T(15) / T(2)) + z[3] + -z[19];
z[3] = abb[2] * z[3];
z[20] = z[6] + -z[12];
z[21] = 2 * abb[23];
z[22] = -abb[18] + abb[21] + -z[21];
z[23] = z[20] + -z[22];
z[23] = abb[0] * z[23];
z[24] = z[14] + z[20];
z[25] = abb[5] * z[24];
z[3] = z[3] + -z[8] + -z[16] + z[23] + 2 * z[25];
z[3] = abb[14] * z[3];
z[23] = abb[0] * z[24];
z[23] = z[16] + -z[23] + -z[25];
z[24] = abb[12] * z[23];
z[25] = 2 * abb[2];
z[26] = abb[12] * z[15] * z[25];
z[24] = z[24] + z[26];
z[24] = 2 * z[24];
z[3] = z[3] + z[24];
z[27] = 3 * abb[23];
z[4] = abb[18] * (T(5) / T(2)) + -z[0] + z[4] + -z[10] + z[27];
z[4] = abb[2] * z[4];
z[28] = 2 * abb[1];
z[29] = z[15] * z[28];
z[30] = -z[18] + z[29];
z[31] = abb[0] + abb[4];
z[32] = 3 * abb[22];
z[33] = z[31] * z[32];
z[34] = 2 * abb[24] + -z[6];
z[35] = abb[18] + z[34];
z[35] = abb[4] * z[35];
z[36] = 3 * z[35];
z[33] = z[33] + -z[36];
z[37] = abb[18] + z[6];
z[38] = -z[12] + 2 * z[37];
z[39] = -abb[21] + z[38];
z[39] = abb[0] * z[39];
z[4] = z[4] + -z[8] + -z[30] + -z[33] + z[39];
z[4] = abb[11] * z[4];
z[0] = abb[24] * (T(-9) / T(2)) + abb[18] * (T(-7) / T(2)) + -z[0] + z[19] + -z[27];
z[0] = abb[2] * z[0];
z[19] = abb[18] + abb[21];
z[20] = -z[19] + -z[20];
z[20] = abb[0] * z[20];
z[0] = z[0] + -z[8] + z[16] + z[20];
z[0] = abb[13] * z[0];
z[0] = z[0] + z[3] + z[4];
z[0] = abb[11] * z[0];
z[4] = 9 * abb[24];
z[8] = -z[4] + 10 * z[5] + -z[14];
z[8] = abb[5] * z[8];
z[14] = -abb[0] + abb[4] + z[28];
z[14] = z[14] * z[32];
z[14] = z[14] + -z[36];
z[10] = -abb[18] + z[10];
z[4] = 7 * abb[23] + z[4] + -4 * z[10];
z[4] = z[4] * z[28];
z[16] = -abb[23] + z[38];
z[16] = abb[0] * z[16];
z[4] = z[4] + z[8] + z[14] + z[16] + z[18];
z[8] = prod_pow(abb[12], 2);
z[4] = z[4] * z[8];
z[16] = z[13] + -z[19];
z[18] = abb[2] * z[16];
z[20] = abb[8] * z[9];
z[20] = 3 * z[20];
z[18] = z[18] + -z[20];
z[32] = z[5] + z[22];
z[32] = abb[3] * z[32];
z[36] = abb[18] + -z[5];
z[27] = -abb[21] + z[27] + 2 * z[36];
z[27] = abb[0] * z[27];
z[36] = 3 * z[17];
z[38] = -abb[5] * z[36];
z[27] = -z[18] + z[27] + -z[30] + 4 * z[32] + z[38];
z[27] = abb[14] * z[27];
z[24] = z[24] + z[27];
z[24] = abb[14] * z[24];
z[13] = z[13] + z[22];
z[22] = abb[3] * z[13];
z[11] = 3 * z[11] + z[22] + -z[29];
z[16] = abb[0] * z[16];
z[19] = -z[5] + z[19];
z[19] = abb[2] * z[19];
z[16] = z[11] + z[16] + 4 * z[19];
z[16] = abb[13] * z[16];
z[3] = -z[3] + z[16];
z[3] = abb[13] * z[3];
z[22] = 2 * abb[6] + -z[25];
z[17] = z[17] * z[22];
z[22] = abb[18] + -z[34];
z[22] = abb[0] * z[22];
z[25] = -abb[22] * z[31];
z[17] = z[17] + z[22] + z[25] + -z[29] + z[35];
z[17] = abb[15] * z[17];
z[6] = -z[2] + z[6] + -z[21];
z[6] = abb[1] * z[6];
z[2] = abb[21] + -z[2] + -z[5];
z[2] = abb[0] * z[2];
z[2] = -z[2] + -z[6] + z[19] + z[32];
z[5] = abb[0] + -abb[1];
z[5] = abb[22] * z[5];
z[2] = (T(-1) / T(3)) * z[2] + (T(1) / T(2)) * z[5];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[5] = abb[7] * z[7];
z[6] = -abb[0] + 2 * abb[10] + abb[3] * (T(-3) / T(2));
z[6] = -z[6] * z[9];
z[7] = -abb[18] + abb[24];
z[1] = z[1] + (T(1) / T(2)) * z[7];
z[1] = abb[8] * z[1];
z[1] = z[1] + -z[5] + z[6];
z[5] = abb[9] * z[36];
z[1] = 3 * z[1] + z[5];
z[1] = abb[17] * z[1];
z[5] = -abb[16] * z[23];
z[6] = -abb[16] + -z[8];
z[6] = z[6] * z[15];
z[7] = abb[17] * z[9];
z[6] = 4 * z[6] + (T(9) / T(2)) * z[7];
z[6] = abb[2] * z[6];
z[0] = z[0] + z[1] + 13 * z[2] + z[3] + z[4] + 2 * z[5] + z[6] + 3 * z[17] + z[24];
z[1] = abb[5] * z[15];
z[1] = 2 * z[1];
z[2] = -5 * abb[23] + 2 * z[10] + -z[12];
z[2] = z[2] * z[28];
z[3] = z[21] + -z[37];
z[3] = abb[0] * z[3];
z[2] = -z[1] + z[2] + z[3] + -z[14];
z[2] = abb[12] * z[2];
z[3] = -abb[1] * z[15];
z[3] = z[3] + -2 * z[32];
z[4] = abb[0] + -abb[2];
z[4] = z[4] * z[13];
z[1] = z[1] + 2 * z[3] + z[4] + -z[20];
z[1] = abb[14] * z[1];
z[3] = 2 * abb[21] + -z[37];
z[3] = abb[0] * z[3];
z[3] = z[3] + z[11] + -z[18] + z[33];
z[3] = abb[11] * z[3];
z[1] = z[1] + z[2] + z[3] + -z[16] + z[26];
z[1] = 2 * m1_set::bc<T>[0] * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_8_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("22.228316757046857145391574373349453792689909690373109803606503188"),stof<T>("98.513324813038492593758556213663035906218344538650562811756051322")}, std::complex<T>{stof<T>("64.400115390056052202773699997782950695588570481668862168737544182"),stof<T>("-120.440638056979456126760253485312385319513875361299033506708430635")}, std::complex<T>{stof<T>("32.200057695028026101386849998891475347794285240834431084368772091"),stof<T>("-60.220319028489728063380126742656192659756937680649516753354215317")}, std::complex<T>{stof<T>("-73.500543323593783447196206033191752221167670670468919847632667344"),stof<T>("-10.418592166951844358583751536142331540900590163798333987904556194")}, std::complex<T>{stof<T>("-60.258600392267208941144756463508485964514530533149917808617801578"),stof<T>("-49.83248291892144388244726093157406291554098963840532950099113073")}, std::complex<T>{stof<T>("35.470259688373431651443023943032720049343049827692111842621368954"),stof<T>("59.099434061068893069895046818231304531577945064043567298669476786")}, std::complex<T>{stof<T>("5.8302259401923256943663320912675568240303356019423769206425262988"),stof<T>("11.5394771343726793520688314605672196690795827804042834425892947249")}, std::complex<T>{stof<T>("-3.072668366318546599157628038775032785955441755808356421746132133"),stof<T>("47.246795076652142140372750836458559900495186049435789623190676203")}, std::complex<T>{stof<T>("-3.072668366318546599157628038775032785955441755808356421746132133"),stof<T>("47.246795076652142140372750836458559900495186049435789623190676203")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[123].real()/kbase.W[123].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_8_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_8_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-62.085876659032015333469436369146306477021467409772806176215269499"),stof<T>("-23.068163479492762161040580676017565785188012657461548298583363714")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,27> abb = {dl[0], dl[1], dl[2], dl[4], dlog_W12(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[123].real()/k.W[123].real())};

                    
            return f_4_8_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_8_DLogXconstant_part(base_point<T>, kend);
	value += f_4_8_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_8_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_8_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_8_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_8_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_8_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_8_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
