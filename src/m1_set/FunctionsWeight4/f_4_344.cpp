/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_344.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_344_abbreviated (const std::array<T,59>& abb) {
T z[91];
z[0] = abb[49] * (T(3) / T(2));
z[1] = 2 * abb[42] + abb[45];
z[2] = z[0] + -z[1];
z[3] = abb[43] * (T(1) / T(2));
z[4] = abb[44] * (T(1) / T(2));
z[5] = z[3] + z[4];
z[6] = abb[48] * (T(1) / T(2));
z[7] = -z[2] + z[5] + z[6];
z[8] = abb[14] * z[7];
z[9] = abb[8] * z[7];
z[10] = 2 * z[1];
z[11] = 3 * abb[49];
z[12] = z[10] + -z[11];
z[13] = abb[43] + abb[44];
z[14] = abb[48] + z[12] + z[13];
z[15] = abb[2] * z[14];
z[9] = z[9] + z[15];
z[16] = -abb[52] + -abb[53] + -abb[54] + abb[55];
z[17] = (T(-1) / T(2)) * z[16];
z[18] = abb[24] * z[17];
z[8] = z[8] + -z[9] + -z[18];
z[18] = abb[49] * (T(1) / T(2));
z[19] = -z[1] + z[18];
z[20] = abb[44] * (T(3) / T(2));
z[21] = -z[6] + -z[19] + z[20];
z[22] = -2 * abb[47] + abb[43] * (T(3) / T(2)) + z[21];
z[23] = abb[3] + abb[7];
z[22] = z[22] * z[23];
z[24] = -abb[47] + -abb[49] + z[1] + z[13];
z[25] = abb[11] * z[24];
z[26] = abb[48] + -abb[49];
z[27] = -abb[43] + z[26];
z[27] = abb[47] + -z[4] + (T(1) / T(2)) * z[27];
z[27] = abb[5] * z[27];
z[22] = z[8] + -z[22] + z[25] + -z[27];
z[28] = abb[58] * z[22];
z[29] = abb[48] * (T(3) / T(2));
z[19] = 2 * abb[46] + z[5] + z[19] + -z[29];
z[19] = abb[12] * z[19];
z[26] = -abb[46] + z[1] + z[26];
z[30] = abb[9] * z[26];
z[31] = -abb[43] + abb[49];
z[32] = abb[44] + -abb[48];
z[33] = z[31] + -z[32];
z[33] = -abb[46] + (T(1) / T(2)) * z[33];
z[34] = abb[4] * z[33];
z[35] = abb[19] * z[17];
z[19] = z[19] + z[30] + z[34] + z[35];
z[30] = abb[22] * z[17];
z[36] = z[19] + z[30];
z[37] = abb[21] * z[17];
z[8] = z[8] + z[36] + z[37];
z[38] = abb[23] * z[17];
z[38] = z[8] + z[38];
z[38] = abb[56] * z[38];
z[39] = -abb[50] + abb[51];
z[40] = -abb[17] * z[39];
z[41] = abb[41] + z[32];
z[42] = abb[13] * z[41];
z[40] = z[40] + z[42];
z[42] = abb[3] * z[41];
z[43] = z[40] + -z[42];
z[44] = -abb[41] + abb[48];
z[45] = abb[44] + -z[44];
z[45] = abb[1] * z[45];
z[37] = z[37] + (T(-1) / T(2)) * z[43] + z[45];
z[33] = abb[12] * z[33];
z[43] = -abb[41] + -z[31];
z[43] = abb[46] + (T(1) / T(2)) * z[43];
z[43] = abb[0] * z[43];
z[33] = z[33] + -z[34] + z[35] + z[37] + z[43];
z[33] = abb[33] * z[33];
z[35] = z[31] + z[32];
z[43] = abb[6] * z[35];
z[46] = abb[7] * z[35];
z[46] = z[43] + -z[46];
z[47] = abb[8] * (T(1) / T(2));
z[48] = z[41] * z[47];
z[49] = (T(1) / T(2)) * z[39];
z[50] = abb[15] * z[49];
z[37] = z[30] + z[37] + (T(1) / T(2)) * z[46] + -z[48] + z[50];
z[37] = abb[34] * z[37];
z[46] = abb[44] * (T(1) / T(4));
z[44] = (T(1) / T(4)) * z[44] + -z[46];
z[44] = abb[17] * z[44];
z[50] = abb[25] * (T(1) / T(4));
z[50] = z[16] * z[50];
z[51] = -2 * abb[26] + abb[8] * (T(3) / T(4));
z[51] = z[39] * z[51];
z[52] = (T(1) / T(4)) * z[39];
z[53] = abb[13] * z[52];
z[44] = z[44] + z[50] + z[51] + z[53];
z[44] = abb[37] * z[44];
z[50] = abb[35] * abb[44];
z[51] = abb[43] + -abb[48];
z[51] = abb[35] * z[51];
z[53] = abb[35] * abb[49];
z[50] = z[50] + z[51] + -z[53];
z[54] = abb[35] * abb[46];
z[50] = (T(1) / T(2)) * z[50] + z[54];
z[55] = -abb[4] + abb[12];
z[50] = z[50] * z[55];
z[51] = z[51] + z[53];
z[55] = abb[35] * z[4];
z[56] = abb[35] * abb[47];
z[51] = (T(1) / T(2)) * z[51] + z[55] + -z[56];
z[55] = abb[5] + -abb[7];
z[55] = z[51] * z[55];
z[53] = z[53] + -z[54] + -z[56];
z[53] = abb[2] * z[53];
z[54] = abb[37] * z[39];
z[51] = -z[51] + z[54];
z[51] = abb[3] * z[51];
z[56] = abb[42] + abb[45] * (T(1) / T(2));
z[57] = abb[49] * (T(3) / T(4));
z[58] = abb[43] * (T(1) / T(4)) + z[56] + -z[57];
z[59] = abb[48] * (T(1) / T(4));
z[60] = z[46] + z[58] + z[59];
z[61] = abb[37] * z[60];
z[62] = abb[18] * z[61];
z[63] = abb[19] + abb[21] + abb[23];
z[63] = (T(-1) / T(2)) * z[63];
z[63] = -abb[35] * z[16] * z[63];
z[64] = prod_pow(abb[28], 2);
z[65] = -prod_pow(abb[31], 2);
z[64] = z[64] + z[65];
z[65] = abb[46] + z[32];
z[65] = abb[10] * z[65];
z[64] = z[64] * z[65];
z[28] = z[28] + z[33] + z[37] + z[38] + z[44] + z[50] + z[51] + z[53] + z[55] + z[62] + z[63] + z[64];
z[33] = z[1] + z[3];
z[37] = abb[44] + abb[48];
z[38] = abb[47] * (T(1) / T(2));
z[44] = abb[46] * (T(1) / T(2));
z[33] = -abb[49] + (T(1) / T(3)) * z[33] + (T(1) / T(6)) * z[37] + z[38] + z[44];
z[33] = abb[2] * z[33];
z[37] = abb[41] * (T(25) / T(12));
z[50] = abb[43] * (T(11) / T(2)) + z[56];
z[50] = -2 * abb[49] + abb[44] * (T(5) / T(12)) + z[37] + (T(1) / T(3)) * z[50] + -z[59];
z[50] = abb[8] * z[50];
z[51] = abb[14] * z[60];
z[53] = abb[49] * (T(1) / T(4));
z[55] = abb[43] * (T(35) / T(4)) + 4 * z[1];
z[55] = abb[41] * (T(-11) / T(6)) + abb[47] * (T(-9) / T(2)) + abb[44] * (T(13) / T(12)) + z[53] + (T(1) / T(3)) * z[55] + z[59];
z[55] = abb[3] * z[55];
z[62] = abb[43] * (T(-25) / T(4)) + 7 * z[1];
z[37] = abb[46] * (T(-13) / T(2)) + abb[48] * (T(7) / T(3)) + z[37] + -z[53] + (T(1) / T(3)) * z[62];
z[37] = abb[0] * z[37];
z[62] = abb[22] + abb[24];
z[62] = (T(-1) / T(4)) * z[62];
z[62] = -z[16] * z[62];
z[63] = 3 * abb[9];
z[26] = z[26] * z[63];
z[64] = abb[43] * (T(13) / T(4)) + z[56];
z[53] = abb[48] * (T(-5) / T(12)) + abb[47] * (T(-3) / T(2)) + abb[44] * (T(7) / T(12)) + z[53] + (T(1) / T(3)) * z[64];
z[53] = abb[7] * z[53];
z[52] = abb[15] * z[52];
z[25] = -2 * z[25] + -z[26] + (T(-1) / T(2)) * z[27] + z[33] + z[37] + (T(7) / T(3)) * z[45] + z[50] + z[51] + z[52] + z[53] + z[55] + z[62] + -3 * z[65];
z[33] = prod_pow(m1_set::bc<T>[0], 2);
z[25] = z[25] * z[33];
z[37] = 3 * z[34];
z[50] = z[26] + z[37];
z[51] = 3 * z[27];
z[52] = 3 * abb[11];
z[24] = z[24] * z[52];
z[53] = -z[24] + z[51];
z[14] = abb[8] * z[14];
z[55] = z[14] + 2 * z[15];
z[0] = z[0] + z[1];
z[62] = 6 * abb[47] + abb[43] * (T(-7) / T(2)) + -z[0];
z[64] = abb[44] * (T(-7) / T(2)) + abb[48] * (T(5) / T(2)) + z[62];
z[64] = -z[23] * z[64];
z[66] = (T(-3) / T(2)) * z[16];
z[67] = abb[23] * z[66];
z[68] = abb[21] * z[66];
z[69] = z[67] + z[68];
z[70] = -6 * abb[46] + abb[48] * (T(7) / T(2)) + z[0] + (T(-5) / T(2)) * z[13];
z[70] = abb[12] * z[70];
z[71] = abb[19] * z[66];
z[64] = -z[50] + z[53] + z[55] + z[64] + z[69] + z[70] + z[71];
z[64] = abb[32] * z[64];
z[70] = 3 * abb[46];
z[72] = -z[6] + z[70];
z[73] = 3 * abb[47];
z[0] = z[0] + z[5] + -z[72] + -z[73];
z[0] = abb[2] * z[0];
z[18] = z[18] + -z[56];
z[44] = -z[6] + z[18] + z[44];
z[44] = z[44] * z[63];
z[34] = (T(3) / T(2)) * z[34];
z[63] = -z[34] + z[44];
z[74] = -z[1] + z[70];
z[75] = 2 * abb[48];
z[13] = z[13] + z[74] + -z[75];
z[76] = 2 * abb[12];
z[13] = z[13] * z[76];
z[76] = abb[43] * (T(7) / T(4));
z[77] = abb[48] * (T(-7) / T(4)) + abb[44] * (T(17) / T(4)) + 5 * z[56] + -z[57] + -z[76];
z[77] = abb[7] * z[77];
z[57] = z[56] + z[57];
z[76] = z[57] + -z[73] + z[76];
z[78] = abb[48] * (T(5) / T(4));
z[79] = abb[44] * (T(7) / T(4)) + -z[78];
z[80] = -z[76] + -z[79];
z[80] = abb[3] * z[80];
z[57] = abb[43] * (T(5) / T(4)) + -z[57];
z[79] = -z[57] + z[79];
z[79] = abb[8] * z[79];
z[0] = z[0] + z[13] + -z[63] + z[77] + z[79] + z[80];
z[0] = abb[31] * z[0];
z[77] = abb[3] * z[7];
z[79] = abb[7] * z[7];
z[80] = z[77] + z[79];
z[13] = -z[9] + z[13] + z[50] + z[80];
z[81] = abb[27] * z[13];
z[82] = 2 * abb[44];
z[83] = -abb[48] + z[1] + z[82];
z[84] = 2 * abb[43];
z[85] = -z[73] + z[83] + z[84];
z[85] = 2 * z[85];
z[23] = z[23] * z[85];
z[85] = abb[12] * z[7];
z[71] = -z[71] + z[85];
z[68] = z[68] + -z[71];
z[67] = z[67] + z[68];
z[9] = z[9] + z[23] + z[53] + z[67];
z[23] = -abb[29] * z[9];
z[53] = (T(3) / T(2)) * z[43];
z[85] = z[53] + z[71];
z[83] = -abb[43] + z[83];
z[86] = 2 * abb[7] + abb[8];
z[83] = z[83] * z[86];
z[77] = z[15] + -z[77] + z[83] + -z[85];
z[77] = abb[30] * z[77];
z[21] = -z[3] + z[21];
z[21] = abb[7] * z[21];
z[35] = abb[8] * z[35];
z[35] = z[35] + -z[43];
z[21] = z[21] + (T(1) / T(2)) * z[35];
z[19] = z[19] + z[21];
z[19] = 3 * z[19];
z[35] = -abb[28] * z[19];
z[0] = z[0] + z[23] + z[35] + z[64] + z[77] + z[81];
z[0] = abb[31] * z[0];
z[2] = z[2] + -z[3];
z[23] = abb[44] * (T(3) / T(4));
z[35] = abb[41] * (T(1) / T(4));
z[59] = z[2] + -z[23] + -z[35] + -z[59];
z[59] = abb[8] * z[59];
z[23] = -abb[41] + -z[23] + z[58] + z[78];
z[23] = abb[3] * z[23];
z[64] = 3 * abb[14];
z[77] = z[60] * z[64];
z[78] = (T(-3) / T(4)) * z[16];
z[81] = abb[24] * z[78];
z[77] = -z[77] + z[81];
z[81] = abb[23] * z[78];
z[81] = -z[77] + z[81];
z[83] = -z[15] + z[81];
z[86] = (T(3) / T(4)) * z[40];
z[87] = 2 * z[45];
z[88] = -z[86] + z[87];
z[23] = z[23] + -z[59] + -z[68] + -z[83] + -z[88];
z[23] = abb[27] * z[23];
z[59] = abb[48] * (T(3) / T(4));
z[89] = z[2] + z[35] + -z[46] + -z[59];
z[89] = abb[8] * z[89];
z[90] = -abb[41] + abb[44] * (T(-5) / T(4)) + -z[58] + z[59];
z[90] = abb[3] * z[90];
z[83] = -z[79] + z[83] + -z[88] + z[89] + z[90];
z[83] = abb[29] * z[83];
z[88] = abb[2] * z[7];
z[60] = abb[7] * z[60];
z[60] = z[45] + z[60] + z[88];
z[46] = -z[46] + z[59];
z[59] = abb[41] * (T(5) / T(4));
z[3] = -z[3] + z[46] + z[56] + -z[59];
z[3] = abb[8] * z[3];
z[56] = abb[41] * (T(1) / T(2));
z[88] = z[46] + -z[56] + z[58];
z[89] = -abb[3] * z[88];
z[78] = abb[21] * z[78];
z[78] = z[78] + -z[86];
z[43] = (T(3) / T(4)) * z[43];
z[3] = z[3] + z[43] + z[60] + z[78] + z[89];
z[3] = abb[30] * z[3];
z[3] = z[3] + -z[23] + z[83];
z[3] = abb[30] * z[3];
z[10] = abb[43] + z[10];
z[10] = 2 * z[10] + -z[11] + -z[70] + -z[73] + z[75] + z[82];
z[10] = abb[2] * z[10];
z[34] = z[34] + z[44];
z[5] = -z[5] + z[18] + z[38];
z[5] = z[5] * z[52];
z[5] = z[5] + (T(-3) / T(2)) * z[27];
z[10] = z[5] + z[10] + z[14] + z[34] + -z[67] + z[77] + z[80];
z[10] = abb[32] * z[10];
z[7] = z[7] * z[64];
z[14] = abb[24] * z[66];
z[7] = -z[7] + z[14];
z[14] = z[7] + -z[67];
z[18] = z[14] + z[55] + z[80];
z[27] = -abb[29] + abb[30];
z[38] = -abb[27] + z[27];
z[38] = z[18] * z[38];
z[10] = z[10] + z[38];
z[10] = abb[32] * z[10];
z[38] = z[4] + z[35] + z[58];
z[38] = abb[8] * z[38];
z[44] = abb[44] + -z[2] + z[56];
z[44] = abb[3] * z[44];
z[34] = -z[34] + z[38] + z[44] + z[60] + -z[81] + -z[86];
z[38] = prod_pow(abb[27], 2);
z[34] = z[34] * z[38];
z[44] = abb[8] * z[88];
z[52] = 2 * abb[41];
z[46] = -z[46] + z[52] + -z[76];
z[46] = abb[3] * z[46];
z[5] = -z[5] + z[44] + z[46] + z[60] + z[77];
z[5] = abb[29] * z[5];
z[5] = z[5] + z[23];
z[5] = abb[29] * z[5];
z[23] = 2 * z[42] + z[79];
z[42] = -5 * abb[41] + 3 * z[31];
z[32] = -z[32] + (T(1) / T(2)) * z[42];
z[32] = z[32] * z[47];
z[32] = z[23] + z[32] + -z[43] + z[45] + -z[63] + -z[71] + z[78];
z[32] = abb[28] * z[32];
z[40] = z[23] + (T(-3) / T(2)) * z[40] + 4 * z[45] + -z[48] + z[68];
z[42] = abb[27] + z[27];
z[40] = -z[40] * z[42];
z[32] = z[32] + z[40];
z[32] = abb[28] * z[32];
z[13] = abb[36] * z[13];
z[40] = z[57] + -z[59] + z[72];
z[43] = z[38] * z[40];
z[35] = -z[6] + z[35] + -z[58];
z[44] = abb[27] * z[35];
z[45] = -abb[48] + z[2] + z[56];
z[46] = -abb[29] * z[45];
z[44] = z[44] + z[46];
z[44] = abb[29] * z[44];
z[46] = -abb[27] + abb[29];
z[47] = abb[30] + z[46];
z[35] = abb[30] * z[35] * z[47];
z[40] = abb[28] * z[40];
z[42] = z[42] * z[45];
z[40] = z[40] + z[42];
z[40] = abb[28] * z[40];
z[35] = z[35] + z[40] + z[43] + z[44] + (T(9) / T(4)) * z[54];
z[35] = abb[0] * z[35];
z[19] = -abb[57] * z[19];
z[40] = z[16] * z[46];
z[42] = -abb[30] * z[16];
z[43] = z[40] + z[42];
z[43] = abb[30] * z[43];
z[44] = abb[29] * z[40];
z[43] = z[43] + -z[44];
z[44] = -abb[28] * z[16];
z[40] = -z[40] + z[44];
z[44] = abb[31] * z[17];
z[44] = -z[40] + z[44];
z[44] = abb[31] * z[44];
z[42] = -z[40] + z[42];
z[42] = abb[28] * z[42];
z[42] = -z[42] + (T(1) / T(2)) * z[43] + z[44];
z[43] = -abb[36] + abb[57] + abb[58];
z[44] = z[16] * z[43];
z[44] = z[42] + z[44];
z[45] = abb[22] * (T(3) / T(2));
z[44] = z[44] * z[45];
z[46] = -z[38] * z[39];
z[47] = abb[27] * z[39];
z[48] = abb[29] * z[39];
z[54] = z[47] + -z[48];
z[39] = abb[30] * z[39];
z[55] = z[39] + z[54];
z[56] = abb[30] * z[55];
z[41] = -abb[37] * z[41];
z[41] = z[41] + z[46] + z[56];
z[46] = -abb[27] * z[49];
z[46] = z[46] + z[48];
z[46] = abb[29] * z[46];
z[48] = abb[28] * z[49];
z[48] = z[48] + -z[55];
z[48] = abb[28] * z[48];
z[41] = (T(1) / T(2)) * z[41] + z[46] + z[48];
z[41] = abb[15] * z[41];
z[43] = abb[34] + abb[56] + -z[43];
z[43] = -z[16] * z[43];
z[42] = z[42] + z[43];
z[33] = -z[17] * z[33];
z[33] = z[33] + 3 * z[42];
z[33] = abb[20] * z[33];
z[42] = prod_pow(abb[30], 2);
z[38] = -abb[33] + abb[34] + -z[38] + z[42];
z[38] = z[38] * z[49];
z[38] = z[38] + -z[61];
z[42] = 3 * abb[16];
z[38] = z[38] * z[42];
z[0] = z[0] + z[3] + z[5] + z[10] + z[13] + z[19] + z[25] + 3 * z[28] + z[32] + (T(1) / T(2)) * z[33] + z[34] + z[35] + z[38] + (T(3) / T(2)) * z[41] + z[44];
z[3] = -abb[41] + z[2] + z[6] + -z[20];
z[3] = abb[8] * z[3];
z[5] = abb[41] + -abb[43];
z[10] = z[5] + -z[12] + -z[75];
z[13] = abb[3] * z[10];
z[3] = z[3] + z[13] + -z[14] + -z[15] + -z[26] + z[37] + z[87];
z[3] = abb[27] * z[3];
z[6] = z[4] + -z[6] + (T(-3) / T(2)) * z[31] + z[52];
z[6] = abb[8] * z[6];
z[6] = z[6] + -z[23] + -z[50] + -6 * z[65] + z[85] + z[87];
z[6] = abb[28] * z[6];
z[7] = z[7] + z[15] + z[79] + z[87];
z[4] = -z[4] + z[29];
z[2] = abb[41] + z[2] + -z[4];
z[2] = abb[8] * z[2];
z[4] = -4 * abb[41] + z[4] + -z[62];
z[4] = abb[3] * z[4];
z[2] = z[2] + z[4] + -z[7] + -z[24] + -z[51];
z[2] = abb[29] * z[2];
z[1] = abb[44] + z[1] + -z[11] + z[52] + z[84];
z[1] = abb[8] * z[1];
z[4] = abb[41] + abb[43] + z[12] + z[82];
z[4] = abb[3] * z[4];
z[1] = z[1] + z[4] + z[7] + -z[53] + -z[69];
z[1] = abb[30] * z[1];
z[4] = abb[31] * z[9];
z[7] = abb[32] * z[18];
z[9] = -z[10] * z[27];
z[5] = abb[48] + z[5] + -z[74];
z[10] = abb[27] + abb[28];
z[10] = 2 * z[10];
z[5] = z[5] * z[10];
z[5] = z[5] + z[9];
z[5] = abb[0] * z[5];
z[9] = -abb[31] * z[16];
z[9] = z[9] + -z[40];
z[10] = z[9] * z[45];
z[11] = abb[15] * z[54];
z[12] = -z[39] + z[47];
z[12] = z[12] * z[42];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[10] + 3 * z[11] + z[12];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = z[22] + -z[30];
z[2] = abb[40] * z[2];
z[3] = -z[21] + -z[36];
z[3] = abb[39] * z[3];
z[4] = abb[20] + abb[23];
z[4] = z[4] * z[17];
z[4] = z[4] + z[8];
z[4] = abb[38] * z[4];
z[2] = z[2] + z[3] + z[4];
z[3] = -abb[39] + -abb[40];
z[3] = -z[3] * z[16];
z[4] = m1_set::bc<T>[0] * z[9];
z[3] = z[3] + z[4];
z[3] = abb[20] * z[3];
z[1] = z[1] + 3 * z[2] + (T(3) / T(2)) * z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_344_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("33.271302908052661481167078569208983297177814471470482193923039048"),stof<T>("-36.458798005758148808683659217590063869288248014628839802993076557")}, std::complex<T>{stof<T>("-11.284444247253300634450668680231391920085313346835503668065877295"),stof<T>("-93.978475082956705979382675503518827943607543947852490139337101957")}, std::complex<T>{stof<T>("25.996096719050313163028523272983761939281517389342543909164783863"),stof<T>("1.909467438863238212162253636809716512380364447035697400795706822")}, std::complex<T>{stof<T>("-0.411189830911843253803548841941122335239105254802223610923303082"),stof<T>("-28.517266376639371267559883910526324911154307779614351608082408499")}, std::complex<T>{stof<T>("-5.642222123626650317225334340115695960042656673417751834032938648"),stof<T>("-46.989237541478352989691337751759413971803771973926245069668550978")}, std::complex<T>{stof<T>("-20.592022310759988479797910568414920119275337539329025035832629111"),stof<T>("93.334000971454399545748376888366367415465522573584484962992860093")}, std::complex<T>{stof<T>("-18.547848414472447225081140770364272386182591875816614974184009492"),stof<T>("36.493764361994030802771010192733497973147445917608054297617934235")}, std::complex<T>{stof<T>("-5.231032292714807063421785498174573624803551418615528223109635565"),stof<T>("-18.471971164838981722131453841233089060649464194311893461586142479")}, std::complex<T>{stof<T>("-1.8060261809512156207220481625037935930562688401081771009478357238"),stof<T>("8.5860057406210839747580739222161994862759616092824933712549099219")}, std::complex<T>{stof<T>("-2.301289249236299788229142824963213884607522828012936456197368814"),stof<T>("-23.623397538326071070186375418229279950247593024717894811595338102")}, std::complex<T>{stof<T>("2.301289249236299788229142824963213884607522828012936456197368814"),stof<T>("23.623397538326071070186375418229279950247593024717894811595338102")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_344_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_344_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-81.701678748927859828747716895376446215485408716061030251777447478"),stof<T>("53.719723605513469360500246047387446211830147738387670947090835096")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,59> abb = {dl[0], dl[1], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W24(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_5(k), f_2_15(k), f_2_18(k), f_2_23(k), f_2_2_im(k), f_2_9_im(k), f_2_12_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_9_re(k), f_2_12_re(k)};

                    
            return f_4_344_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_344_DLogXconstant_part(base_point<T>, kend);
	value += f_4_344_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_344_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_344_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_344_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_344_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_344_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_344_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
