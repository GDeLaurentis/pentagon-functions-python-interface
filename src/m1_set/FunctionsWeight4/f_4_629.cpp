/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_629.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_629_abbreviated (const std::array<T,63>& abb) {
T z[94];
z[0] = abb[51] * (T(1) / T(2));
z[1] = abb[47] * (T(1) / T(2));
z[2] = z[0] + z[1];
z[3] = abb[45] + abb[46];
z[4] = abb[48] * (T(3) / T(2));
z[5] = -z[2] + -z[3] + z[4];
z[6] = abb[7] * z[5];
z[7] = abb[51] * (T(3) / T(2));
z[8] = abb[48] * (T(1) / T(2));
z[9] = z[7] + -z[8];
z[10] = -abb[43] + z[3];
z[11] = abb[47] * (T(1) / T(6));
z[10] = abb[49] + abb[44] * (T(-1) / T(3)) + -z[9] + (T(-5) / T(3)) * z[10] + z[11];
z[10] = abb[5] * z[10];
z[12] = abb[47] + abb[51];
z[13] = -z[3] + z[12];
z[13] = -abb[44] + (T(1) / T(3)) * z[13];
z[13] = abb[2] * z[13];
z[10] = z[6] + z[10] + z[13];
z[13] = abb[43] + -abb[49];
z[14] = z[1] + z[13];
z[15] = -z[0] + z[8] + z[14];
z[16] = abb[14] * z[15];
z[17] = -abb[52] + -abb[53] + abb[54];
z[18] = (T(1) / T(2)) * z[17];
z[19] = abb[24] * z[18];
z[16] = -z[16] + z[19];
z[19] = 3 * z[13];
z[20] = abb[51] * (T(7) / T(3)) + z[19];
z[11] = abb[50] * (T(-5) / T(2)) + (T(4) / T(3)) * z[3] + z[11] + (T(1) / T(2)) * z[20];
z[11] = abb[1] * z[11];
z[20] = 2 * abb[51];
z[21] = abb[45] * (T(-5) / T(2)) + -z[1] + -z[20];
z[22] = 3 * abb[50];
z[21] = abb[46] * (T(-5) / T(6)) + -z[4] + (T(1) / T(3)) * z[21] + z[22];
z[21] = abb[3] * z[21];
z[23] = abb[23] * z[17];
z[24] = abb[21] * z[17];
z[25] = z[23] + z[24];
z[26] = 2 * abb[50];
z[27] = abb[47] + -abb[51];
z[28] = -abb[48] + z[27];
z[29] = z[26] + z[28];
z[29] = abb[8] * z[29];
z[30] = abb[22] * z[17];
z[29] = -z[29] + z[30];
z[31] = abb[46] * (T(1) / T(2));
z[32] = abb[45] * (T(1) / T(2)) + z[31];
z[33] = -z[1] + z[32];
z[34] = abb[50] * (T(3) / T(2));
z[35] = abb[51] + z[33] + -z[34];
z[35] = abb[15] * z[35];
z[36] = abb[50] + -abb[51];
z[37] = abb[48] + -z[3] + z[36];
z[37] = abb[12] * z[37];
z[38] = (T(1) / T(2)) * z[37];
z[2] = -abb[44] + z[2];
z[39] = z[2] + -z[8];
z[40] = abb[13] * z[39];
z[41] = abb[51] * (T(3) / T(4));
z[42] = -abb[49] + abb[47] * (T(-13) / T(12)) + abb[44] * (T(-5) / T(6)) + abb[43] * (T(1) / T(6)) + abb[48] * (T(5) / T(4)) + (T(-7) / T(6)) * z[3] + z[41];
z[42] = abb[6] * z[42];
z[36] = abb[44] + z[36];
z[36] = abb[9] * z[36];
z[43] = 3 * z[36];
z[44] = abb[43] + abb[44];
z[45] = -abb[51] + z[44];
z[45] = abb[0] * z[45];
z[46] = abb[48] + z[27];
z[47] = abb[4] * z[46];
z[48] = abb[20] * z[17];
z[10] = (T(1) / T(2)) * z[10] + z[11] + z[16] + z[21] + (T(-5) / T(4)) * z[25] + -2 * z[29] + z[35] + z[38] + -z[40] + z[42] + -z[43] + (T(13) / T(6)) * z[45] + (T(-1) / T(4)) * z[47] + (T(-3) / T(2)) * z[48];
z[10] = prod_pow(m1_set::bc<T>[0], 2) * z[10];
z[11] = abb[47] * (T(1) / T(4));
z[21] = abb[51] * (T(1) / T(4));
z[25] = abb[44] + abb[48] * (T(-1) / T(4)) + -z[11] + -z[21] + z[32];
z[25] = abb[5] * z[25];
z[29] = abb[25] * z[17];
z[42] = -z[29] + z[47];
z[9] = -z[1] + z[9];
z[49] = -abb[45] + -z[9];
z[50] = -abb[50] + z[31];
z[49] = (T(1) / T(2)) * z[49] + -z[50];
z[49] = abb[15] * z[49];
z[51] = abb[45] + abb[51];
z[50] = z[50] + (T(1) / T(2)) * z[51];
z[50] = abb[3] * z[50];
z[51] = -abb[51] + z[3];
z[52] = 3 * abb[44] + -abb[47] + z[51];
z[53] = abb[2] * z[52];
z[25] = z[25] + -z[36] + (T(-1) / T(4)) * z[42] + z[49] + z[50] + z[53];
z[25] = prod_pow(abb[29], 2) * z[25];
z[49] = abb[5] * z[5];
z[50] = abb[3] * z[5];
z[54] = z[49] + z[50];
z[55] = abb[21] * z[18];
z[56] = z[16] + z[55];
z[57] = abb[47] * (T(-3) / T(2)) + z[0] + -z[3] + z[8];
z[58] = 2 * z[13] + -z[57];
z[58] = abb[6] * z[58];
z[59] = 2 * abb[47];
z[51] = z[51] + z[59];
z[60] = z[19] + z[51];
z[61] = abb[1] * z[60];
z[62] = abb[23] * z[18];
z[58] = z[6] + -z[54] + z[56] + z[58] + z[61] + z[62];
z[58] = abb[33] * z[58];
z[9] = z[3] + z[9] + -z[26];
z[9] = abb[15] * z[9];
z[26] = abb[22] * z[18];
z[63] = abb[25] * z[18];
z[9] = z[9] + -z[26] + z[37] + -z[63];
z[28] = abb[50] + (T(1) / T(2)) * z[28];
z[64] = abb[8] * z[28];
z[65] = abb[20] * z[18];
z[66] = z[64] + -z[65];
z[67] = -z[9] + z[54] + -z[66];
z[68] = z[6] + -z[67];
z[68] = abb[58] * z[68];
z[55] = -z[16] + z[55];
z[13] = abb[50] + z[13] + z[27];
z[13] = abb[1] * z[13];
z[69] = z[13] + -z[26] + -z[55];
z[70] = abb[15] * z[28];
z[71] = -z[63] + -z[66] + z[70];
z[72] = z[69] + -z[71];
z[72] = abb[55] * z[72];
z[73] = z[24] + -z[29];
z[14] = abb[50] * (T(1) / T(2)) + -z[14] + -z[32];
z[14] = abb[1] * z[14];
z[14] = z[14] + -z[38] + (T(1) / T(2)) * z[70] + (T(1) / T(4)) * z[73];
z[38] = prod_pow(abb[32], 2);
z[14] = z[14] * z[38];
z[70] = z[30] + z[47];
z[70] = z[37] + -z[66] + (T(1) / T(2)) * z[70];
z[74] = 3 * abb[48];
z[27] = -z[27] + z[74];
z[27] = -abb[50] + (T(1) / T(4)) * z[27];
z[27] = abb[3] * z[27];
z[27] = z[27] + (T(1) / T(2)) * z[70];
z[70] = prod_pow(abb[30], 2);
z[27] = z[27] * z[70];
z[75] = abb[55] + (T(-1) / T(2)) * z[38];
z[75] = z[15] * z[75];
z[57] = -abb[34] * z[57];
z[57] = z[57] + z[75];
z[57] = abb[6] * z[57];
z[14] = z[14] + z[25] + z[27] + z[57] + z[58] + z[68] + z[72];
z[25] = -z[16] + z[26] + -z[64];
z[26] = z[1] + z[3];
z[27] = -abb[51] + -z[19];
z[27] = -z[26] + (T(1) / T(2)) * z[27] + z[34];
z[27] = abb[1] * z[27];
z[34] = abb[44] * (T(1) / T(2));
z[57] = -abb[43] + z[34];
z[33] = z[0] + z[33] + z[57];
z[33] = abb[5] * z[33];
z[32] = abb[47] + z[32];
z[58] = 3 * abb[49];
z[68] = -abb[43] + z[58];
z[72] = abb[44] + (T(1) / T(2)) * z[68];
z[0] = -z[0] + -z[32] + z[72];
z[0] = abb[6] * z[0];
z[75] = (T(3) / T(2)) * z[40];
z[76] = (T(1) / T(2)) * z[53];
z[0] = z[0] + (T(3) / T(2)) * z[25] + z[27] + z[33] + z[35] + z[43] + -2 * z[45] + (T(-1) / T(2)) * z[50] + z[75] + z[76];
z[0] = abb[27] * z[0];
z[25] = (T(3) / T(2)) * z[23];
z[27] = z[25] + z[50];
z[33] = abb[15] * z[5];
z[35] = z[48] + z[73];
z[43] = 3 * z[40];
z[52] = abb[5] * z[52];
z[77] = 3 * z[53];
z[35] = -z[27] + z[33] + (T(-3) / T(2)) * z[35] + z[43] + z[52] + z[77];
z[35] = abb[29] * z[35];
z[52] = z[16] + z[63];
z[13] = z[13] + z[52];
z[63] = 2 * z[3];
z[4] = -z[4] + z[63];
z[1] = -z[1] + z[4];
z[78] = abb[51] * (T(5) / T(2)) + z[1] + -z[22];
z[78] = abb[15] * z[78];
z[79] = 3 * z[37];
z[13] = 3 * z[13] + -z[78] + -z[79];
z[13] = abb[32] * z[13];
z[78] = abb[29] * z[5];
z[80] = abb[30] * z[5];
z[78] = z[78] + -z[80];
z[60] = abb[32] * z[60];
z[81] = -z[60] + z[78];
z[81] = abb[6] * z[81];
z[54] = abb[32] * z[54];
z[13] = z[13] + -z[35] + z[54] + -z[81];
z[81] = abb[48] * (T(3) / T(4));
z[82] = abb[47] * (T(3) / T(4)) + z[81];
z[83] = z[21] + -z[72] + z[82];
z[83] = abb[5] * z[83];
z[84] = abb[49] * (T(3) / T(2));
z[85] = 2 * abb[43];
z[86] = -z[34] + z[84] + -z[85];
z[87] = abb[51] * (T(5) / T(4));
z[82] = -z[82] + z[86] + z[87];
z[82] = abb[6] * z[82];
z[55] = z[55] + z[65];
z[88] = -z[29] + z[55];
z[89] = (T(1) / T(2)) * z[45];
z[75] = z[53] + z[75] + z[89];
z[90] = z[61] + z[75];
z[91] = (T(3) / T(4)) * z[23];
z[92] = z[90] + -z[91];
z[82] = -z[33] + z[82] + z[83] + (T(3) / T(2)) * z[88] + -z[92];
z[83] = abb[31] * z[82];
z[20] = -abb[47] + z[3] + z[20];
z[88] = z[20] + -z[22];
z[88] = abb[15] * z[88];
z[93] = -z[49] + 2 * z[88];
z[30] = z[24] + z[30];
z[66] = (T(1) / T(2)) * z[30] + -z[66];
z[27] = -z[27] + -3 * z[66] + z[79] + z[93];
z[66] = abb[30] * z[27];
z[0] = z[0] + z[13] + z[66] + z[83];
z[0] = abb[27] * z[0];
z[66] = abb[47] * (T(5) / T(4)) + z[3] + z[41] + -z[72] + -z[81];
z[66] = abb[5] * z[66];
z[34] = 4 * abb[43] + abb[49] * (T(-9) / T(2)) + abb[47] * (T(13) / T(4)) + -z[34] + -z[41] + z[63] + -z[81];
z[34] = abb[6] * z[34];
z[41] = z[56] + z[65];
z[34] = z[34] + (T(3) / T(2)) * z[41] + -z[50] + 2 * z[61] + z[66] + -z[75] + z[91];
z[34] = abb[31] * z[34];
z[56] = -abb[27] * z[82];
z[59] = z[3] + z[59];
z[66] = -abb[44] + z[85];
z[75] = -z[58] + z[59] + z[66];
z[75] = abb[5] * z[75];
z[82] = abb[3] * z[51];
z[59] = -z[44] + z[59];
z[83] = 2 * abb[6];
z[59] = z[59] * z[83];
z[59] = z[45] + -z[53] + z[59] + -z[61] + z[75] + z[82];
z[59] = abb[28] * z[59];
z[42] = z[33] + (T(-3) / T(2)) * z[42] + -z[49] + z[82];
z[49] = abb[29] + -abb[30];
z[42] = -z[42] * z[49];
z[52] = -z[52] + -z[61];
z[52] = -z[33] + 3 * z[52];
z[52] = abb[32] * z[52];
z[49] = -z[49] * z[51];
z[49] = z[49] + -z[60];
z[49] = z[49] * z[83];
z[34] = z[34] + z[42] + z[49] + z[52] + z[54] + z[56] + z[59];
z[34] = abb[28] * z[34];
z[42] = z[44] + -z[58];
z[26] = z[26] + (T(1) / T(2)) * z[42];
z[26] = abb[5] * z[26];
z[32] = z[32] + -z[57] + -z[84];
z[32] = abb[6] * z[32];
z[42] = abb[45] + -abb[51] + z[19];
z[31] = abb[47] + z[31] + (T(1) / T(2)) * z[42];
z[31] = abb[1] * z[31];
z[26] = z[26] + z[31] + z[32] + -z[50] + z[76] + -z[89];
z[26] = abb[31] * z[26];
z[12] = z[12] + z[63];
z[31] = z[12] + -z[74];
z[32] = abb[5] * z[31];
z[42] = abb[3] * z[31];
z[32] = z[25] + z[32] + -z[33] + z[42] + (T(3) / T(2)) * z[73];
z[33] = abb[30] + -abb[32];
z[32] = z[32] * z[33];
z[33] = abb[32] * z[5];
z[44] = -z[33] + z[80];
z[44] = abb[7] * z[44];
z[33] = z[33] + z[78];
z[33] = abb[6] * z[33];
z[26] = z[26] + z[32] + z[33] + z[35] + 3 * z[44];
z[26] = abb[31] * z[26];
z[32] = abb[17] * z[15];
z[33] = abb[19] * z[15];
z[2] = -abb[49] + z[2] + z[8];
z[2] = abb[16] * z[2];
z[8] = abb[18] * z[39];
z[35] = abb[26] * z[18];
z[2] = z[2] + -z[8] + z[32] + -z[33] + z[35];
z[2] = (T(3) / T(2)) * z[2];
z[8] = abb[60] * z[2];
z[32] = abb[6] * z[5];
z[32] = z[27] + z[32];
z[32] = abb[59] * z[32];
z[33] = abb[5] * z[39];
z[9] = -z[9] + -z[33] + z[40] + z[53] + -z[64];
z[9] = 3 * z[9];
z[35] = -abb[56] * z[9];
z[39] = -z[24] + z[47];
z[44] = abb[3] * z[46];
z[44] = z[23] + -z[39] + z[44] + z[48];
z[33] = z[33] + -z[40] + (T(1) / T(2)) * z[44];
z[33] = 3 * z[33] + -z[77];
z[33] = abb[34] * z[33];
z[40] = abb[6] * z[46];
z[30] = z[30] + -z[40] + z[47];
z[40] = z[62] + z[71];
z[28] = abb[3] * z[28];
z[28] = z[28] + (T(-1) / T(2)) * z[30] + -z[40];
z[28] = 3 * z[28];
z[30] = abb[57] * z[28];
z[44] = z[38] + -z[70];
z[44] = z[6] * z[44];
z[18] = z[18] * z[38];
z[17] = -abb[55] * z[17];
z[17] = z[17] + z[18];
z[17] = abb[23] * z[17];
z[17] = z[17] + z[44];
z[0] = abb[61] + abb[62] + z[0] + z[8] + z[10] + 3 * z[14] + (T(3) / T(2)) * z[17] + z[26] + z[30] + z[32] + z[33] + z[34] + z[35];
z[8] = z[41] + -z[88];
z[10] = z[12] + z[19] + -z[22];
z[10] = abb[1] * z[10];
z[1] = -z[1] + -z[7] + z[66];
z[1] = abb[5] * z[1];
z[4] = -2 * abb[44] + abb[47] * (T(5) / T(2)) + z[4] + z[7] + -z[68];
z[4] = abb[6] * z[4];
z[1] = z[1] + z[4] + 3 * z[8] + z[10] + z[25] + -6 * z[36] + -z[42] + -z[43] + 4 * z[45] + -z[53] + -z[79];
z[1] = abb[27] * z[1];
z[4] = abb[48] * (T(9) / T(4));
z[7] = abb[47] * (T(-7) / T(4)) + z[4] + -z[63] + z[72] + -z[87];
z[7] = abb[5] * z[7];
z[3] = -z[3] + z[4] + z[11];
z[4] = abb[51] * (T(-7) / T(4)) + z[3] + -z[86];
z[4] = abb[6] * z[4];
z[6] = 3 * z[6];
z[8] = z[16] + (T(-3) / T(2)) * z[24] + -z[65];
z[8] = (T(1) / T(2)) * z[8] + z[29];
z[10] = abb[15] * z[31];
z[4] = z[4] + -z[6] + z[7] + 3 * z[8] + -z[10] + (T(-9) / T(4)) * z[23] + -z[42] + z[90];
z[4] = abb[31] * z[4];
z[7] = z[47] + z[55];
z[7] = (T(1) / T(2)) * z[7] + -z[29];
z[3] = z[3] + -z[21] + -z[72];
z[3] = abb[5] * z[3];
z[8] = abb[47] * (T(-19) / T(4)) + abb[51] * (T(13) / T(4)) + -z[63] + -z[81] + z[86];
z[8] = abb[6] * z[8];
z[3] = z[3] + 3 * z[7] + z[8] + z[10] + -z[82] + -z[92];
z[3] = abb[28] * z[3];
z[7] = 6 * abb[50] + -z[20] + -z[74];
z[7] = abb[3] * z[7];
z[7] = z[6] + z[7] + z[25] + -6 * z[37] + (T(-3) / T(2)) * z[39] + -z[93];
z[7] = abb[30] * z[7];
z[1] = z[1] + z[3] + z[4] + z[7] + -z[13];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[40] * z[2];
z[3] = abb[39] * z[27];
z[4] = z[6] + -3 * z[67];
z[4] = abb[38] * z[4];
z[6] = -abb[36] * z[9];
z[7] = -z[40] + z[69];
z[8] = 3 * abb[35];
z[7] = z[7] * z[8];
z[5] = abb[39] * z[5];
z[8] = z[8] * z[15];
z[5] = z[5] + z[8];
z[5] = abb[6] * z[5];
z[8] = abb[37] * z[28];
z[1] = abb[41] + abb[42] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_629_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-25.574899553340639407108411114306672003636284064046785281202285788"),stof<T>("-28.102645388630594562804646197061075364821905261319253398516520148")}, std::complex<T>{stof<T>("-3.842303589037347753716178442501725703371338453839769594880634106"),stof<T>("-15.122073742449383876612057114347449851412820922669855048999498447")}, std::complex<T>{stof<T>("2.6839523787930476751568555039135095976803968152661836106764841582"),stof<T>("9.0919584727082030491411070293242072619099732739702616849568244715")}, std::complex<T>{stof<T>("2.6839523787930476751568555039135095976803968152661836106764841582"),stof<T>("9.0919584727082030491411070293242072619099732739702616849568244715")}, std::complex<T>{stof<T>("2.424030856902938820802202570692558006174986576563953068994389074"),stof<T>("20.520678711206664342152845385697853535847425093237616012383059314")}, std::complex<T>{stof<T>("-12.1371035290945741652266690414334509822825674060545516881928466627"),stof<T>("0.0888827129369301617539068542816990864875531811551856400898352636")}, std::complex<T>{stof<T>("30.925825592714709289107393276103936093361705962293153222155919102"),stof<T>("15.228450699034538768309313555481365438949009702791305351896596809")}, std::complex<T>{stof<T>("4.1022251109274566080708313757226772948767486925420001365627291908"),stof<T>("3.6933535039509225836003187579738035774753691034025007215732636044")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("-4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_629_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_629_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((-1 + m1_set::bc<T>[1]) * (T(3) / T(4)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (3 * m1_set::bc<T>[1] * (-v[3] + v[5])) / tend;


		return (abb[44] + -abb[47]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = prod_pow(abb[28], 2);
z[1] = -prod_pow(abb[29], 2);
z[0] = z[0] + z[1];
z[1] = abb[44] + -abb[47];
return 3 * abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_629_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_629_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(16)) * (v[1] + v[2] + -v[3] + -v[4]) * (24 + 8 * v[0] + -3 * v[1] + 5 * v[2] + 7 * v[3] + -5 * v[4] + -4 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + -8 * v[5])) / prod_pow(tend, 2);
c[1] = ((-1 + 2 * m1_set::bc<T>[1]) * (T(-3) / T(2)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[43] + abb[45] + abb[46] + abb[47] + -abb[48] + -abb[49]) * (t * c[0] + c[1]);
	}
	{
T z[4];
z[0] = abb[43] + abb[45] + abb[46] + abb[47] + -abb[48] + -abb[49];
z[1] = abb[28] * z[0];
z[2] = (T(1) / T(2)) * z[0];
z[3] = abb[32] * z[2];
z[3] = z[1] + z[3];
z[3] = abb[32] * z[3];
z[2] = -abb[31] * z[2];
z[1] = -z[1] + z[2];
z[1] = abb[31] * z[1];
z[0] = -abb[33] * z[0];
z[0] = z[0] + z[1] + z[3];
return 3 * abb[11] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_629_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_629_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("7.663520514862896713432643040624959344618227012637414280328929803"),stof<T>("-51.448641858424355831654255257271394480942083378565447635601733442")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,63> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W15(k,dl), dlog_W16(k,dl), dlog_W20(k,dl), dlog_W25(k,dl), dlog_W28(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_4(k), f_1_5(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_16(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_24_im(k), T{0}, T{0}, rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_24_re(k), T{0}, T{0}};
abb[41] = SpDLog_f_4_629_W_16_Im(t, path, abb);
abb[42] = SpDLog_f_4_629_W_20_Im(t, path, abb);
abb[61] = SpDLog_f_4_629_W_16_Re(t, path, abb);
abb[62] = SpDLog_f_4_629_W_20_Re(t, path, abb);

                    
            return f_4_629_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_629_DLogXconstant_part(base_point<T>, kend);
	value += f_4_629_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_629_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_629_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_629_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_629_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_629_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_629_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
