/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_614.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_614_abbreviated (const std::array<T,63>& abb) {
T z[111];
z[0] = abb[45] + abb[51];
z[1] = abb[47] * (T(11) / T(3));
z[2] = abb[46] * (T(11) / T(6));
z[3] = abb[49] * (T(5) / T(2));
z[4] = 3 * abb[50];
z[0] = abb[44] * (T(-22) / T(3)) + abb[48] * (T(5) / T(2)) + (T(-11) / T(6)) * z[0] + -z[1] + -z[2] + z[3] + z[4];
z[0] = abb[2] * z[0];
z[5] = abb[19] * abb[54];
z[6] = abb[19] * abb[55];
z[7] = abb[12] * abb[45];
z[8] = abb[52] + -abb[53];
z[9] = abb[17] * z[8];
z[5] = z[5] + z[6] + z[7] + -z[9];
z[6] = abb[45] + abb[46];
z[7] = abb[50] + z[6];
z[9] = abb[51] * (T(1) / T(2));
z[7] = -abb[49] + (T(1) / T(2)) * z[7] + -z[9];
z[10] = abb[6] * z[7];
z[11] = z[5] + z[10];
z[12] = 2 * abb[48];
z[13] = abb[46] + -abb[50];
z[14] = -abb[45] + abb[51];
z[15] = -z[12] + -z[13] + z[14];
z[15] = abb[8] * z[15];
z[16] = abb[54] + abb[55];
z[17] = abb[22] * z[16];
z[18] = abb[20] * z[16];
z[15] = z[15] + z[17] + z[18];
z[18] = abb[46] * (T(1) / T(2));
z[19] = abb[45] * (T(1) / T(2));
z[20] = z[18] + z[19];
z[21] = abb[48] * (T(3) / T(2));
z[22] = abb[44] + abb[47] * (T(1) / T(2));
z[23] = -abb[51] + z[20] + z[21] + -z[22];
z[23] = abb[14] * z[23];
z[24] = 2 * abb[44] + abb[47];
z[25] = abb[50] * (T(3) / T(2));
z[26] = z[18] + z[24] + -z[25];
z[27] = z[9] + z[19] + z[26];
z[28] = abb[13] * z[27];
z[29] = (T(1) / T(2)) * z[16];
z[30] = abb[24] * z[29];
z[28] = z[28] + z[30];
z[30] = abb[45] * (T(19) / T(4));
z[31] = abb[46] + z[22];
z[31] = z[30] + 5 * z[31];
z[32] = abb[51] * (T(3) / T(4));
z[3] = abb[43] * (T(-1) / T(12)) + -z[3] + (T(1) / T(3)) * z[31] + -z[32];
z[3] = abb[4] * z[3];
z[31] = abb[43] + -abb[46];
z[33] = 3 * abb[48];
z[34] = abb[50] * (T(1) / T(2));
z[35] = abb[47] * (T(5) / T(3)) + z[34];
z[35] = abb[44] * (T(5) / T(3)) + abb[51] * (T(5) / T(6)) + (T(13) / T(12)) * z[31] + -z[33] + (T(1) / T(2)) * z[35];
z[35] = abb[0] * z[35];
z[36] = -abb[47] + abb[48] + abb[50];
z[37] = abb[44] + z[9];
z[36] = (T(1) / T(2)) * z[36] + -z[37];
z[36] = abb[9] * z[36];
z[38] = abb[21] + abb[23];
z[39] = (T(3) / T(4)) * z[16];
z[40] = z[38] * z[39];
z[41] = z[13] + z[14];
z[42] = abb[5] * z[41];
z[1] = abb[50] * (T(5) / T(2)) + -z[1];
z[1] = abb[45] * (T(-31) / T(12)) + abb[44] * (T(-11) / T(3)) + abb[43] * (T(-1) / T(6)) + abb[46] * (T(7) / T(12)) + (T(1) / T(2)) * z[1] + z[32];
z[1] = abb[7] * z[1];
z[43] = -abb[48] + z[14];
z[43] = abb[11] * z[43];
z[44] = abb[12] * abb[43];
z[45] = (T(1) / T(2)) * z[44];
z[46] = abb[16] * z[8];
z[47] = abb[15] * z[8];
z[14] = -abb[43] + z[14];
z[48] = abb[1] * z[14];
z[49] = -abb[12] * z[9];
z[2] = -abb[50] + abb[45] * (T(-11) / T(3)) + abb[44] * (T(-8) / T(3)) + abb[47] * (T(-4) / T(3)) + abb[49] * (T(1) / T(2)) + abb[51] * (T(7) / T(3)) + z[2];
z[2] = abb[3] * z[2];
z[0] = z[0] + z[1] + z[2] + z[3] + (T(1) / T(2)) * z[11] + 2 * z[15] + z[23] + z[28] + z[35] + -z[36] + z[40] + (T(1) / T(4)) * z[42] + -3 * z[43] + z[45] + (T(-5) / T(4)) * z[46] + -z[47] + (T(1) / T(6)) * z[48] + z[49];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[1] = abb[12] * abb[51];
z[1] = z[1] + -z[5];
z[2] = z[1] + -z[44];
z[3] = -z[2] + z[17];
z[5] = -z[3] + z[47];
z[11] = abb[46] * (T(1) / T(4));
z[15] = -abb[47] + z[25];
z[15] = abb[44] + z[11] + (T(-1) / T(2)) * z[15];
z[35] = abb[51] * (T(1) / T(4));
z[44] = abb[45] * (T(1) / T(4));
z[49] = z[15] + z[35] + z[44];
z[50] = 3 * abb[13];
z[51] = z[49] * z[50];
z[52] = abb[24] * z[39];
z[51] = z[51] + z[52];
z[52] = z[40] + z[51];
z[53] = abb[47] + z[25];
z[54] = abb[46] * (T(5) / T(4));
z[53] = -z[33] + (T(1) / T(2)) * z[53] + -z[54];
z[37] = abb[43] * (T(5) / T(4)) + z[37] + z[53];
z[37] = abb[0] * z[37];
z[55] = abb[48] + (T(1) / T(2)) * z[13];
z[56] = -z[9] + z[19] + z[55];
z[57] = abb[8] * z[56];
z[58] = abb[20] * z[39];
z[58] = (T(3) / T(2)) * z[57] + -z[58];
z[59] = abb[2] * z[27];
z[60] = abb[43] * (T(1) / T(2));
z[61] = abb[45] + z[26] + z[60];
z[62] = abb[4] * z[61];
z[63] = abb[3] * z[49];
z[64] = abb[43] * (T(1) / T(4));
z[65] = z[15] + z[19] + z[64];
z[66] = abb[7] * z[65];
z[67] = (T(3) / T(2)) * z[46];
z[68] = -z[48] + z[67];
z[36] = 3 * z[36];
z[5] = (T(3) / T(4)) * z[5] + -z[36] + -z[37] + -z[52] + z[58] + z[59] + z[62] + z[63] + z[66] + z[68];
z[5] = abb[28] * z[5];
z[62] = abb[4] * z[27];
z[63] = abb[3] * z[27];
z[66] = z[62] + z[63];
z[50] = z[27] * z[50];
z[69] = (T(3) / T(2)) * z[16];
z[70] = abb[24] * z[69];
z[50] = z[50] + z[70];
z[70] = abb[14] * z[27];
z[71] = abb[25] * z[69];
z[70] = -z[70] + z[71];
z[71] = 4 * abb[44] + 2 * abb[47] + abb[51] + -z[4] + z[6];
z[72] = abb[7] * z[71];
z[73] = abb[2] * z[71];
z[74] = -z[50] + z[66] + -z[70] + z[72] + 2 * z[73];
z[74] = abb[29] * z[74];
z[33] = 2 * abb[51] + -z[33];
z[75] = -z[6] + z[24] + z[33];
z[75] = abb[14] * z[75];
z[76] = 2 * z[75];
z[27] = abb[7] * z[27];
z[27] = z[27] + z[73];
z[77] = abb[20] + abb[22];
z[77] = z[69] * z[77];
z[78] = abb[51] + z[24];
z[79] = -abb[48] + -abb[50] + z[78];
z[79] = abb[9] * z[79];
z[80] = 3 * z[79];
z[66] = -z[27] + -3 * z[57] + z[66] + -z[76] + z[77] + z[80];
z[77] = abb[30] * z[66];
z[5] = z[5] + -z[74] + z[77];
z[5] = abb[28] * z[5];
z[77] = abb[20] * z[29];
z[57] = -z[57] + z[77];
z[77] = -z[24] + z[34];
z[81] = abb[45] * (T(3) / T(2)) + -z[77];
z[82] = z[9] + z[18];
z[83] = z[81] + -z[82];
z[83] = abb[3] * z[83];
z[17] = z[17] + z[42];
z[41] = (T(1) / T(2)) * z[41];
z[84] = abb[7] * z[41];
z[85] = abb[25] * z[29];
z[83] = (T(1) / T(2)) * z[17] + z[57] + z[79] + z[83] + -z[84] + z[85];
z[86] = abb[59] * z[83];
z[87] = z[2] + -z[47];
z[88] = z[42] + -z[87];
z[89] = z[29] * z[38];
z[90] = (T(1) / T(2)) * z[14];
z[91] = abb[4] * z[90];
z[91] = z[48] + z[91];
z[41] = -abb[3] * z[41];
z[92] = (T(1) / T(2)) * z[46];
z[90] = abb[7] * z[90];
z[41] = z[41] + (T(1) / T(2)) * z[88] + z[89] + -z[90] + z[91] + z[92];
z[41] = abb[35] * z[41];
z[55] = -z[55] + z[60];
z[55] = abb[0] * z[55];
z[3] = (T(1) / T(2)) * z[3] + z[55] + z[57] + z[91] + -z[92];
z[55] = -z[85] + z[89];
z[88] = z[3] + z[55];
z[88] = abb[56] * z[88];
z[29] = abb[22] * z[29];
z[29] = z[29] + z[57];
z[57] = abb[4] * z[7];
z[92] = abb[48] + abb[49] + -abb[50];
z[92] = abb[2] * z[92];
z[93] = z[57] + z[92];
z[7] = abb[3] * z[7];
z[94] = z[7] + -z[10] + -z[29] + z[93];
z[95] = abb[14] * z[56];
z[96] = z[55] + z[95];
z[97] = -z[94] + z[96];
z[97] = abb[58] * z[97];
z[98] = -z[27] + z[28];
z[29] = z[29] + z[79] + z[85] + z[98];
z[99] = abb[57] * z[29];
z[89] = z[10] + z[89];
z[81] = -2 * abb[49] + abb[46] * (T(3) / T(2)) + -z[9] + z[81];
z[100] = abb[3] + abb[4];
z[81] = -z[81] * z[100];
z[81] = z[81] + z[89] + z[98];
z[81] = abb[34] * z[81];
z[12] = abb[51] * (T(3) / T(2)) + -z[12] + -z[20] + -z[77];
z[77] = -abb[57] + -abb[59];
z[77] = z[12] * z[77];
z[98] = abb[56] * z[56];
z[77] = z[77] + z[98];
z[77] = abb[14] * z[77];
z[98] = prod_pow(abb[30], 2);
z[101] = -prod_pow(abb[33], 2);
z[101] = z[98] + z[101];
z[101] = z[43] * z[101];
z[41] = z[41] + z[77] + z[81] + z[86] + z[88] + z[97] + z[99] + z[101];
z[77] = abb[45] * (T(3) / T(4));
z[81] = z[26] + z[35] + z[64] + z[77];
z[81] = abb[7] * z[81];
z[86] = -z[9] + -z[15] + z[64];
z[86] = abb[0] * z[86];
z[88] = 2 * z[48];
z[97] = z[86] + z[88];
z[99] = (T(3) / T(4)) * z[87] + -z[97];
z[101] = z[51] + z[99];
z[102] = abb[51] * (T(5) / T(4));
z[77] = -abb[43] + z[15] + -z[77] + z[102];
z[77] = abb[4] * z[77];
z[77] = z[40] + -z[70] + z[73] + z[77] + z[81] + -z[101];
z[77] = abb[28] * z[77];
z[81] = -abb[51] + -z[26] + z[60];
z[81] = abb[0] * z[81];
z[69] = z[38] * z[69];
z[103] = z[69] + -z[70];
z[104] = abb[4] * z[14];
z[87] = -4 * z[48] + z[63] + -z[81] + (T(3) / T(2)) * z[87] + z[90] + -z[103] + -2 * z[104];
z[105] = abb[33] * z[87];
z[106] = -z[26] + -z[32] + -z[44] + z[64];
z[106] = abb[7] * z[106];
z[107] = abb[45] * (T(5) / T(4));
z[32] = -abb[43] + -z[15] + z[32] + -z[107];
z[32] = abb[4] * z[32];
z[32] = z[32] + z[52] + -z[63] + -z[73] + -z[99] + z[106];
z[32] = abb[32] * z[32];
z[52] = -abb[7] * z[61];
z[61] = 2 * abb[45];
z[63] = -abb[51] + z[24] + z[61];
z[99] = 2 * abb[46] + -3 * abb[49];
z[106] = z[63] + z[99];
z[108] = abb[3] * z[106];
z[109] = abb[43] + -abb[45] + -z[24] + -z[99];
z[109] = abb[4] * z[109];
z[52] = (T(-3) / T(2)) * z[47] + -z[48] + z[52] + -z[73] + -z[81] + -z[108] + 2 * z[109];
z[52] = abb[31] * z[52];
z[106] = abb[4] * z[106];
z[106] = z[106] + z[108];
z[109] = 3 * z[10];
z[27] = -z[27] + z[103] + -2 * z[106] + z[109];
z[103] = -abb[29] + abb[30];
z[27] = z[27] * z[103];
z[27] = z[27] + z[32] + z[52] + z[77] + z[105];
z[27] = abb[31] * z[27];
z[32] = abb[50] * (T(3) / T(4));
z[52] = abb[51] * (T(7) / T(4));
z[103] = abb[49] * (T(3) / T(2));
z[110] = abb[45] * (T(11) / T(4)) + -z[11] + z[24] + z[32] + -z[52] + -z[103];
z[110] = abb[3] * z[110];
z[21] = z[21] + -z[24];
z[20] = z[9] + z[20] + -z[21] + -z[103];
z[20] = abb[2] * z[20];
z[6] = -z[6] + z[9] + -z[22] + z[103];
z[24] = abb[4] * z[6];
z[10] = (T(3) / T(2)) * z[10] + z[40];
z[82] = abb[45] + z[22] + -z[82];
z[103] = abb[7] * z[82];
z[17] = -z[10] + (T(-3) / T(4)) * z[17] + z[20] + z[23] + z[24] + z[58] + z[103] + z[110];
z[17] = z[17] * z[98];
z[23] = -abb[46] + z[63];
z[24] = abb[3] * z[23];
z[58] = 2 * z[24];
z[98] = (T(3) / T(2)) * z[42];
z[23] = abb[7] * z[23];
z[23] = z[23] + z[58] + -z[62] + z[70] + z[73] + z[98];
z[23] = abb[30] * z[23];
z[18] = -z[18] + z[19] + z[22] + -z[60];
z[18] = abb[7] * z[18];
z[19] = -abb[4] * z[65];
z[22] = abb[3] * z[82];
z[18] = z[18] + z[19] + z[22] + z[59] + (T(-1) / T(2)) * z[68] + z[86];
z[18] = abb[32] * z[18];
z[18] = z[18] + z[23] + z[74] + -z[77];
z[18] = abb[32] * z[18];
z[19] = -abb[28] * z[87];
z[23] = z[67] + z[98];
z[14] = abb[7] * z[14];
z[24] = z[14] + -z[23] + -z[24] + -z[70] + z[81] + z[91];
z[24] = abb[32] * z[24];
z[23] = z[23] + z[48] + (T(-5) / T(2)) * z[104];
z[48] = abb[44] + z[52] + z[53] + -z[107];
z[48] = abb[14] * z[48];
z[39] = abb[25] * z[39];
z[22] = -z[22] + (T(1) / T(2)) * z[23] + -z[37] + z[39] + z[48] + z[90];
z[22] = abb[33] * z[22];
z[19] = z[19] + z[22] + z[24];
z[19] = abb[33] * z[19];
z[11] = -z[11] + -z[21] + -z[32] + -z[44] + z[102];
z[11] = abb[14] * z[11];
z[6] = z[6] * z[100];
z[21] = abb[7] * z[49];
z[6] = z[6] + z[10] + z[11] + z[20] + z[21] + z[36] + -z[39];
z[6] = abb[29] * z[6];
z[4] = 8 * abb[44] + 4 * abb[47] + -z[4] + z[33] + z[61] + z[99];
z[4] = abb[2] * z[4];
z[4] = z[4] + z[72] + z[75] + -z[80] + z[106];
z[4] = abb[30] * z[4];
z[4] = z[4] + z[6];
z[4] = abb[29] * z[4];
z[6] = abb[60] * z[66];
z[10] = -z[35] + z[44] + z[64];
z[11] = abb[15] + -abb[17];
z[10] = z[10] * z[11];
z[11] = abb[16] + -abb[18];
z[11] = z[11] * z[49];
z[20] = abb[0] + abb[7];
z[20] = abb[4] + -2 * abb[27] + abb[12] * (T(1) / T(4)) + (T(3) / T(4)) * z[20];
z[8] = z[8] * z[20];
z[20] = abb[26] * z[16];
z[8] = z[8] + z[10] + z[11] + (T(-1) / T(4)) * z[20];
z[8] = 3 * z[8];
z[10] = -abb[61] * z[8];
z[0] = abb[62] + z[0] + z[4] + z[5] + z[6] + z[10] + z[17] + z[18] + z[19] + z[27] + 3 * z[41];
z[2] = -z[2] + -z[47];
z[4] = abb[45] * (T(5) / T(2));
z[5] = -abb[43] + -z[4] + -z[9] + -3 * z[26];
z[5] = abb[4] * z[5];
z[6] = -6 * abb[48] + z[25] + (T(5) / T(2)) * z[31] + z[78];
z[6] = abb[0] * z[6];
z[9] = -abb[3] * z[71];
z[2] = (T(3) / T(2)) * z[2] + z[5] + z[6] + z[9] + -3 * z[46] + z[50] + z[69] + z[76] + -6 * z[79] + z[88] + z[90];
z[2] = abb[28] * z[2];
z[1] = -z[1] + z[47];
z[1] = (T(1) / T(2)) * z[1] + -z[42] + z[45];
z[5] = -abb[43] + 3 * z[15] + -z[44] + z[52];
z[5] = abb[4] * z[5];
z[4] = abb[51] * (T(5) / T(2)) + -z[4] + 3 * z[13] + z[60];
z[6] = abb[7] * (T(1) / T(2));
z[4] = z[4] * z[6];
z[6] = abb[14] * z[71];
z[9] = abb[25] * z[16];
z[6] = -z[6] + 3 * z[9];
z[1] = (T(3) / T(2)) * z[1] + z[4] + z[5] + -z[6] + z[40] + -z[51] + -z[58] + z[97];
z[1] = abb[32] * z[1];
z[4] = abb[49] + -z[63];
z[4] = abb[3] * z[4];
z[4] = z[4] + (T(1) / T(2)) * z[42] + z[75] + -z[79] + z[84] + z[89] + z[93];
z[4] = abb[30] * z[4];
z[5] = -z[7] + -z[28] + -z[57] + z[79] + -z[85] + z[92] + z[95];
z[5] = abb[29] * z[5];
z[4] = z[4] + z[5];
z[5] = (T(-9) / T(4)) * z[38];
z[5] = z[5] * z[16];
z[7] = abb[47] + z[34];
z[7] = abb[44] + (T(1) / T(2)) * z[7] + z[54];
z[7] = abb[43] + -6 * abb[49] + abb[51] * (T(-13) / T(4)) + 3 * z[7] + z[30];
z[7] = abb[4] * z[7];
z[5] = z[5] + z[6] + z[7] + (T(1) / T(4)) * z[14] + z[101] + 2 * z[108] + -z[109];
z[5] = abb[31] * z[5];
z[6] = abb[30] * z[43];
z[1] = z[1] + z[2] + 3 * z[4] + z[5] + -6 * z[6] + z[105];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[39] * z[83];
z[4] = z[55] + -z[94];
z[4] = abb[38] * z[4];
z[3] = z[3] + z[96];
z[3] = abb[36] * z[3];
z[5] = -abb[39] * z[12];
z[6] = abb[38] * z[56];
z[5] = z[5] + z[6];
z[5] = abb[14] * z[5];
z[6] = -abb[14] * z[12];
z[6] = z[6] + z[29];
z[6] = abb[37] * z[6];
z[2] = z[2] + z[3] + z[4] + z[5] + z[6];
z[3] = abb[40] * z[66];
z[4] = -abb[41] * z[8];
z[1] = abb[42] + z[1] + 3 * z[2] + z[3] + z[4];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_614_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-1.5456475103687967511626615423861988683086098975062501467963607855"),stof<T>("-11.5033015991443592773318823824912092128823182683640941530054341614")}, std::complex<T>{stof<T>("3.402800050253254215055999418538193991107424795644827539591794253"),stof<T>("53.005038414406225442918449250355425377080589699495969496998979947")}, std::complex<T>{stof<T>("4.506327512280110241669155214187873596628702543494398012230264319"),stof<T>("37.078855698803254118411860117702453011443658874134524866902840008")}, std::complex<T>{stof<T>("-9.802134862913920151869041703399913166617528186632045364256450569"),stof<T>("-21.181965232726981999077966400784476980680922130742873291916936564")}, std::complex<T>{stof<T>("1.701400025126627107527999709269096995553712397822413769795897126"),stof<T>("26.502519207203112721459224625177712688540294849747984748499489973")}, std::complex<T>{stof<T>("-5.4515598653916402665652246560949376972339281434538109750257225907"),stof<T>("0.8976728580175186752065514742314725551047601620077609794918476323")}, std::complex<T>{stof<T>("8.1007348377872930443410419941308161710638157888096315944605534422"),stof<T>("-5.3205539744761307223812582243932357078593727190051114565825534096")}, std::complex<T>{stof<T>("-2.8049274871534831341411555049187766010749901456719842424343671925"),stof<T>("-10.5763364916001413969526354925247403229033640243865401184033500348")}, std::complex<T>{stof<T>("-3.133975418202266643558147331268058698164979717370536535565281151"),stof<T>("14.940085422420628914799963768915380190763079305112406821302817017")}, std::complex<T>{stof<T>("3.133975418202266643558147331268058698164979717370536535565281151"),stof<T>("-14.940085422420628914799963768915380190763079305112406821302817017")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}, std::complex<T>{stof<T>("-12.3588167252585481131587928122063967763948100286893971199867248372"),stof<T>("4.0338757708926232434797028782637552024452128784470387252094426759")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[127].real()/kbase.W[127].real()), C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_614_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_614_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(8)) * (v[2] + v[3]) * (-24 + 9 * v[2] + 5 * v[3] + -4 * m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -8 * v[5])) / prod_pow(tend, 2);
c[1] = (3 * (-1 + 2 * m1_set::bc<T>[1]) * (v[2] + v[3])) / tend;


		return (2 * abb[44] + abb[45] + abb[46] + abb[47] + -abb[49] + -abb[50]) * (t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = abb[29] + -abb[31];
z[1] = -abb[30] + -abb[31];
z[0] = z[0] * z[1];
z[0] = abb[34] + z[0];
z[1] = 2 * abb[44] + abb[45] + abb[46] + abb[47] + -abb[49] + -abb[50];
return 3 * abb[10] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_614_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(3) / T(4)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (-3 * m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (2 * abb[44] + abb[45] + abb[46] + abb[47] + -abb[49] + -abb[50]) * (t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[2];
z[0] = 2 * abb[44] + abb[45] + abb[46] + abb[47] + -abb[49] + -abb[50];
z[1] = abb[29] + -abb[31];
return 3 * abb[10] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_614_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("60.151579686058178482824320303096999919365554354263710139575613413"),stof<T>("31.89417336517168893789946242170821194401905998580203096991505474")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,63> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W15(k,dl), dlog_W19(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[14].real()/k.W[14].real()), IntDLogL_W_19(t,k,kend,dl), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[127].real()/k.W[127].real()), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), T{0}};
abb[42] = SpDLog_f_4_614_W_19_Im(t, path, abb);
abb[62] = SpDLog_f_4_614_W_19_Re(t, path, abb);

                    
            return f_4_614_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_614_DLogXconstant_part(base_point<T>, kend);
	value += f_4_614_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_614_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_614_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_614_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_614_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_614_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_614_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
