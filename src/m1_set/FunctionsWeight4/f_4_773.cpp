/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_773.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_773_abbreviated (const std::array<T,82>& abb) {
T z[136];
z[0] = 3 * abb[7];
z[1] = 4 * abb[17] + -z[0];
z[2] = 2 * abb[6];
z[3] = 4 * abb[9];
z[4] = abb[4] + -abb[16];
z[5] = -abb[15] + -z[1] + -z[2] + z[3] + -z[4];
z[6] = abb[64] + abb[66];
z[5] = -z[5] * z[6];
z[7] = -abb[58] + abb[59] + -abb[60];
z[8] = 2 * abb[61];
z[9] = abb[56] + abb[62];
z[10] = z[7] + z[8] + -z[9];
z[11] = abb[22] * z[10];
z[12] = abb[27] * z[10];
z[13] = abb[68] + abb[69];
z[14] = abb[2] * z[13];
z[15] = 4 * z[14];
z[16] = z[12] + z[15];
z[17] = abb[4] + abb[16];
z[2] = z[2] + -z[17];
z[18] = abb[7] + -abb[15];
z[19] = z[3] + z[18];
z[20] = z[2] + z[19];
z[20] = abb[69] * z[20];
z[21] = 2 * abb[69];
z[22] = abb[65] + abb[67];
z[23] = abb[63] + z[22];
z[24] = abb[68] + z[21] + -z[23];
z[24] = abb[0] * z[24];
z[25] = abb[57] + -abb[61] + -z[7];
z[26] = abb[28] * z[25];
z[27] = -z[24] + 4 * z[26];
z[28] = abb[18] * abb[70];
z[29] = abb[7] * z[23];
z[30] = z[28] + z[29];
z[31] = z[3] * z[23];
z[32] = abb[19] * abb[70];
z[33] = 2 * z[32];
z[34] = z[31] + z[33];
z[35] = -abb[69] + z[6] + z[22];
z[36] = abb[13] * z[35];
z[37] = abb[20] * abb[70];
z[38] = abb[15] * z[23];
z[37] = z[37] + -z[38];
z[38] = 2 * abb[16];
z[39] = z[1] + -z[38];
z[39] = abb[68] * z[39];
z[5] = z[5] + z[11] + -z[16] + z[20] + z[27] + -z[30] + -z[34] + -4 * z[36] + -2 * z[37] + z[39];
z[5] = abb[36] * z[5];
z[20] = 4 * abb[3];
z[39] = 4 * abb[7];
z[40] = 3 * abb[4] + -abb[16];
z[41] = abb[6] + abb[14];
z[42] = -z[20] + z[39] + -z[40] + -z[41];
z[42] = abb[69] * z[42];
z[43] = abb[3] * z[22];
z[44] = abb[4] * z[22];
z[45] = z[43] + z[44];
z[46] = 2 * abb[13];
z[35] = z[35] * z[46];
z[46] = -z[35] + 2 * z[45];
z[47] = z[12] + -z[26];
z[48] = abb[3] + abb[14];
z[49] = abb[6] + abb[17];
z[50] = 2 * z[4] + z[48] + z[49];
z[39] = z[39] + -z[50];
z[39] = abb[68] * z[39];
z[51] = 3 * abb[17];
z[52] = abb[3] + abb[4];
z[53] = -abb[16] + z[52];
z[54] = z[51] + -z[53];
z[54] = z[6] * z[54];
z[55] = z[6] + z[13];
z[56] = abb[10] * z[55];
z[57] = 4 * z[56];
z[58] = -abb[57] + -abb[61] + z[9];
z[59] = abb[23] * z[58];
z[39] = 6 * z[14] + z[39] + z[42] + z[46] + z[47] + z[54] + -z[57] + -z[59];
z[39] = abb[34] * z[39];
z[42] = -z[24] + z[30];
z[54] = abb[22] * z[25];
z[60] = z[37] + z[54];
z[61] = 2 * abb[9];
z[62] = abb[69] * z[61];
z[62] = z[26] + z[62];
z[63] = z[60] + -z[62];
z[64] = abb[3] + z[18];
z[65] = -abb[17] + z[61];
z[66] = z[64] + z[65];
z[66] = z[6] * z[66];
z[67] = z[23] * z[61];
z[68] = -abb[3] + abb[17];
z[68] = abb[68] * z[68];
z[66] = z[42] + -z[63] + -z[66] + -z[67] + z[68];
z[66] = abb[38] * z[66];
z[68] = -abb[8] + abb[14];
z[69] = abb[4] + z[68];
z[69] = abb[68] * z[69];
z[70] = abb[25] * z[58];
z[71] = 2 * z[14];
z[69] = z[69] + -z[70] + -z[71];
z[72] = 2 * abb[7];
z[73] = -abb[14] + z[72];
z[74] = abb[4] + abb[8];
z[75] = 2 * abb[3];
z[76] = -z[73] + z[74] + z[75];
z[76] = abb[69] * z[76];
z[77] = 2 * z[44];
z[78] = -z[35] + z[77];
z[79] = 2 * z[6];
z[80] = -abb[5] + abb[17];
z[81] = -z[79] * z[80];
z[76] = z[69] + z[76] + -z[78] + z[81];
z[76] = abb[32] * z[76];
z[39] = z[39] + z[66] + z[76];
z[76] = -abb[4] + abb[9];
z[81] = -abb[5] + z[76];
z[79] = z[79] * z[81];
z[81] = 2 * abb[10];
z[55] = z[55] * z[81];
z[81] = 2 * z[58];
z[82] = abb[25] * z[81];
z[79] = z[55] + z[79] + z[82];
z[83] = z[37] + z[59];
z[84] = abb[22] * z[58];
z[85] = z[83] + z[84];
z[67] = z[32] + z[67];
z[86] = abb[4] * z[23];
z[87] = z[67] + -z[86];
z[88] = 2 * abb[8];
z[89] = -abb[7] + z[88];
z[90] = abb[5] + abb[14];
z[91] = z[89] + -z[90];
z[91] = abb[68] * z[91];
z[92] = -abb[4] + abb[5];
z[93] = z[61] + z[92];
z[94] = -abb[14] + abb[15] + -z[93];
z[94] = abb[69] * z[94];
z[91] = z[30] + z[79] + z[85] + z[87] + z[91] + z[94];
z[94] = 2 * abb[33];
z[91] = z[91] * z[94];
z[5] = z[5] + 2 * z[39] + z[91];
z[39] = 2 * m1_set::bc<T>[0];
z[5] = z[5] * z[39];
z[91] = -2 * abb[57] + z[7] + z[9];
z[95] = abb[22] * z[91];
z[96] = 3 * z[30] + z[95];
z[97] = 2 * z[59];
z[57] = z[57] + z[97];
z[98] = z[57] + z[96];
z[99] = 5 * abb[7];
z[20] = -5 * abb[4] + -abb[15] + abb[16] + -z[20] + -z[88] + z[99];
z[20] = abb[69] * z[20];
z[100] = z[17] + z[18];
z[101] = 2 * abb[5];
z[102] = z[100] + z[101];
z[102] = z[6] * z[102];
z[103] = abb[24] + abb[26];
z[104] = -z[91] * z[103];
z[105] = 2 * abb[4];
z[106] = abb[5] + -abb[8] + abb[16] + -z[105];
z[1] = -z[1] + 2 * z[106];
z[1] = abb[68] * z[1];
z[106] = 8 * z[14];
z[1] = z[1] + z[12] + z[20] + -z[27] + -z[33] + -z[82] + -z[98] + z[102] + z[104] + z[106];
z[20] = 2 * abb[37];
z[1] = z[1] * z[20];
z[27] = -abb[16] + z[90];
z[82] = -z[0] + z[27] + -z[61] + z[75] + z[105];
z[82] = abb[69] * z[82];
z[102] = z[4] + z[75];
z[104] = z[18] + z[102];
z[107] = 2 * abb[17];
z[108] = z[61] + z[104] + -z[107];
z[108] = z[6] * z[108];
z[109] = z[24] + z[67];
z[110] = z[86] + z[109];
z[111] = z[16] + -z[55];
z[112] = 2 * z[53] + -z[72] + z[90];
z[112] = abb[68] * z[112];
z[54] = z[54] + z[82] + z[83] + z[108] + z[110] + -z[111] + z[112];
z[54] = abb[31] * z[54];
z[82] = z[25] * z[103];
z[108] = z[35] + z[82];
z[112] = -abb[6] + abb[7];
z[113] = -z[92] + -z[107] + z[112];
z[113] = z[6] * z[113];
z[114] = z[92] + z[112];
z[114] = abb[68] * z[114];
z[115] = -z[55] + z[71];
z[114] = z[114] + z[115];
z[22] = z[22] * z[75];
z[116] = -abb[7] + z[52];
z[117] = z[21] * z[116];
z[113] = -z[22] + z[108] + z[113] + -z[114] + z[117];
z[113] = abb[35] * z[113];
z[54] = z[54] + z[113];
z[1] = z[1] + 4 * z[54];
z[1] = m1_set::bc<T>[0] * z[1];
z[54] = abb[23] * z[10];
z[113] = abb[7] + -abb[8];
z[117] = -abb[16] + abb[17] + z[101] + -z[113];
z[117] = z[6] * z[117];
z[90] = abb[8] + abb[17] + -z[38] + z[90];
z[90] = abb[68] * z[90];
z[27] = z[27] + z[89];
z[27] = abb[69] * z[27];
z[118] = abb[25] * z[25];
z[27] = z[27] + -z[47] + -z[54] + -z[55] + z[90] + z[117] + z[118];
z[27] = 4 * z[27];
z[47] = abb[46] * z[27];
z[54] = abb[29] * z[10];
z[90] = -abb[15] + 4 * abb[30];
z[90] = abb[70] * z[90];
z[117] = abb[20] * z[23];
z[90] = z[90] + z[117];
z[117] = abb[18] + abb[21];
z[119] = abb[19] + abb[20];
z[120] = z[117] + -z[119];
z[120] = abb[69] * z[120];
z[117] = z[117] + z[119];
z[117] = z[6] * z[117];
z[105] = abb[0] + abb[7] + z[105];
z[105] = abb[70] * z[105];
z[119] = abb[18] + 2 * abb[21];
z[119] = abb[68] * z[119];
z[121] = abb[18] * z[23];
z[54] = z[54] + 2 * z[90] + -z[105] + z[117] + z[119] + z[120] + -z[121];
z[54] = 2 * z[54];
z[90] = -abb[52] * z[54];
z[105] = z[61] + z[113];
z[117] = abb[6] + -abb[15];
z[119] = -abb[4] + z[105] + z[117];
z[119] = abb[69] * z[119];
z[76] = 2 * z[76];
z[76] = z[6] * z[76];
z[76] = z[70] + z[76];
z[120] = abb[6] + -abb[8];
z[120] = abb[68] * z[120];
z[87] = z[30] + z[76] + z[87] + -z[119] + -z[120];
z[119] = z[85] + z[87];
z[119] = abb[49] * z[119];
z[120] = abb[8] + -abb[15] + z[65];
z[121] = abb[4] + z[120];
z[121] = z[6] * z[121];
z[122] = abb[23] * z[25];
z[118] = z[118] + z[122];
z[122] = -abb[8] + abb[17];
z[122] = abb[68] * z[122];
z[63] = -z[63] + -z[110] + -z[118] + -z[121] + z[122];
z[110] = abb[45] * z[63];
z[121] = abb[3] + -abb[8];
z[122] = -abb[4] + z[121];
z[49] = z[49] + z[122];
z[49] = z[6] * z[49];
z[49] = z[22] + z[49] + -z[118];
z[123] = -abb[6] + abb[17];
z[124] = -abb[8] + z[52] + z[123];
z[124] = abb[68] * z[124];
z[124] = z[49] + z[124];
z[125] = -z[26] + z[35];
z[126] = z[71] + -z[124] + z[125];
z[126] = abb[48] * z[126];
z[110] = -z[110] + z[119] + z[126];
z[119] = -abb[7] + abb[17];
z[126] = -abb[5] + -z[119] + z[121];
z[126] = z[6] * z[126];
z[126] = -z[118] + z[126];
z[127] = abb[7] + z[80] + -z[121];
z[127] = abb[68] * z[127];
z[21] = abb[8] * z[21];
z[21] = z[21] + z[26] + -z[55] + -z[126] + z[127];
z[21] = 4 * z[21];
z[127] = abb[50] * z[21];
z[128] = -abb[5] + abb[6];
z[129] = -abb[14] + z[128];
z[130] = -z[113] + z[129];
z[130] = abb[68] * z[130];
z[129] = z[113] + z[129];
z[129] = abb[69] * z[129];
z[131] = z[6] * z[101];
z[70] = z[55] + z[70] + z[129] + z[130] + -z[131];
z[129] = 4 * z[70];
z[130] = abb[47] * z[129];
z[8] = 4 * abb[57] + -3 * z[7] + -z[8] + -z[9];
z[8] = abb[36] * z[8];
z[9] = abb[34] * z[58];
z[131] = abb[38] * z[25];
z[9] = z[9] + z[131];
z[132] = abb[33] * z[81];
z[8] = z[8] + 2 * z[9] + -z[132];
z[8] = m1_set::bc<T>[0] * z[8];
z[132] = abb[45] + abb[48];
z[132] = z[25] * z[132];
z[133] = abb[47] * z[58];
z[132] = z[132] + z[133];
z[133] = abb[31] * z[25];
z[39] = z[39] * z[133];
z[8] = z[8] + -z[39] + -2 * z[132];
z[39] = 2 * abb[24];
z[132] = 2 * abb[26];
z[134] = -z[39] + -z[132];
z[8] = z[8] * z[134];
z[134] = -abb[8] + z[116];
z[13] = z[13] * z[134];
z[13] = z[13] + z[126];
z[106] = -4 * z[13] + -8 * z[56] + z[106];
z[106] = abb[51] * z[106];
z[126] = abb[23] + abb[25];
z[134] = abb[69] * z[126];
z[135] = abb[28] * z[6];
z[134] = z[134] + z[135];
z[134] = 4 * z[134];
z[135] = abb[79] * z[134];
z[1] = abb[80] + abb[81] + z[1] + z[5] + z[8] + z[47] + z[90] + z[106] + 4 * z[110] + z[127] + z[130] + z[135];
z[5] = z[30] + z[95];
z[8] = abb[5] + z[53];
z[47] = z[8] + -z[105] + z[117];
z[47] = abb[69] * z[47];
z[8] = z[8] + z[120];
z[8] = z[6] * z[8];
z[52] = -z[52] + z[113];
z[38] = abb[5] + abb[6] + -z[38] + -z[52];
z[38] = abb[68] * z[38];
z[8] = -z[5] + z[8] + -z[12] + 2 * z[24] + z[38] + z[47] + z[67] + -z[71] + z[86] + z[118];
z[8] = abb[31] * z[8];
z[38] = 2 * abb[14];
z[3] = z[3] + z[38];
z[47] = z[3] + -z[100];
z[47] = abb[69] * z[47];
z[67] = -3 * z[24] + 2 * z[26];
z[90] = -z[4] + z[19];
z[95] = z[90] + -z[107];
z[95] = -z[6] * z[95];
z[100] = abb[14] + -abb[16];
z[100] = -abb[7] + 2 * z[100] + z[107];
z[100] = abb[68] * z[100];
z[5] = z[5] + -z[12] + -z[34] + z[47] + z[67] + z[95] + z[97] + z[100];
z[5] = abb[36] * z[5];
z[34] = z[50] + -z[72];
z[34] = abb[68] * z[34];
z[47] = z[41] + -z[72];
z[50] = z[47] + z[102];
z[50] = abb[69] * z[50];
z[72] = -abb[17] + z[53];
z[72] = z[6] * z[72];
z[34] = z[26] + z[34] + z[50] + z[59] + z[72] + -z[111];
z[34] = abb[34] * z[34];
z[50] = abb[17] + z[92];
z[50] = -z[6] * z[50];
z[72] = z[121] + z[128];
z[72] = abb[69] * z[72];
z[50] = z[50] + z[72] + -z[114];
z[72] = abb[32] * z[50];
z[95] = -z[34] + z[66] + z[72];
z[97] = -abb[6] + abb[14];
z[100] = -z[18] + z[97];
z[100] = abb[69] * z[100];
z[97] = abb[68] * z[97];
z[42] = z[42] + z[85] + z[97] + z[100];
z[97] = z[42] * z[94];
z[5] = z[5] + z[8] + 2 * z[95] + z[97];
z[5] = abb[31] * z[5];
z[8] = abb[4] + z[75];
z[8] = 3 * z[8] + z[38] + -z[99] + z[128];
z[8] = abb[69] * z[8];
z[95] = -abb[17] + z[92];
z[97] = -6 * abb[7] + z[38] + -z[75] + -z[95];
z[97] = abb[68] * z[97];
z[51] = z[51] + z[112];
z[51] = -z[6] * z[51];
z[43] = -3 * z[43] + -z[77];
z[8] = z[8] + -10 * z[14] + z[26] + z[35] + 2 * z[43] + z[51] + z[57] + z[97];
z[35] = prod_pow(abb[34], 2);
z[8] = z[8] * z[35];
z[28] = z[28] + z[29] + z[32] + z[37];
z[29] = 4 * abb[8] + -z[75];
z[37] = 2 * abb[1];
z[43] = -z[29] + -z[37] + z[90];
z[43] = abb[69] * z[43];
z[51] = z[23] * z[37];
z[11] = z[11] + z[12] + -2 * z[28] + -z[31] + z[43] + z[45] + z[51];
z[28] = z[36] + -z[56];
z[29] = abb[15] + abb[16] + -z[29] + -z[92];
z[29] = -8 * abb[9] + 2 * z[29] + z[99] + -z[123];
z[43] = (T(1) / T(3)) * z[6];
z[29] = z[29] * z[43];
z[43] = -13 * abb[61] + 5 * abb[62];
z[43] = abb[57] + abb[56] * (T(5) / T(3)) + (T(-8) / T(3)) * z[7] + (T(1) / T(3)) * z[43];
z[43] = -z[43] * z[126];
z[17] = 5 * abb[3] + -13 * abb[8] + 4 * z[17] + -z[128];
z[17] = abb[68] * z[17];
z[11] = (T(2) / T(3)) * z[11] + (T(-8) / T(3)) * z[14] + (T(1) / T(3)) * z[17] + (T(-4) / T(3)) * z[28] + z[29] + z[43];
z[17] = prod_pow(m1_set::bc<T>[0], 2);
z[11] = z[11] * z[17];
z[15] = z[15] + -z[59];
z[28] = z[6] * z[107];
z[29] = z[47] + -z[92];
z[43] = abb[3] + -z[29];
z[43] = abb[68] * z[43];
z[29] = -3 * abb[3] + -z[29];
z[29] = abb[69] * z[29];
z[28] = z[15] + z[28] + z[29] + z[43] + z[46] + -z[55];
z[29] = 2 * abb[34];
z[28] = z[28] * z[29];
z[28] = z[28] + z[72];
z[28] = abb[32] * z[28];
z[43] = -z[86] + z[109];
z[46] = z[65] + z[92];
z[46] = z[6] * z[46];
z[46] = z[43] + z[46];
z[47] = abb[68] * z[80];
z[47] = -z[46] + z[47] + z[62];
z[51] = prod_pow(abb[38], 2) * z[47];
z[5] = z[5] + z[8] + z[11] + z[28] + -z[51];
z[8] = -abb[5] + abb[14];
z[11] = -abb[68] * z[8];
z[8] = abb[4] + -abb[15] + -z[8];
z[8] = abb[69] * z[8];
z[28] = -abb[24] * z[58];
z[8] = z[8] + z[11] + z[24] + z[28] + z[32] + -z[85] + z[86];
z[8] = abb[39] * z[8];
z[11] = z[64] + -z[92];
z[11] = -z[6] * z[11];
z[28] = -abb[3] + abb[5];
z[28] = abb[68] * z[28];
z[11] = z[11] + z[28] + z[30] + z[32] + -z[60] + -z[82] + -z[86];
z[11] = abb[41] * z[11];
z[28] = abb[6] + z[119];
z[28] = z[6] * z[28];
z[28] = z[28] + -z[125];
z[30] = -abb[7] + z[48];
z[30] = abb[69] * z[30];
z[32] = -z[22] + z[30];
z[51] = abb[3] + z[73] + -z[123];
z[51] = abb[68] * z[51];
z[15] = z[15] + -z[28] + -z[32] + z[51];
z[15] = abb[34] * z[15];
z[51] = -abb[3] + -abb[7] + abb[14];
z[51] = abb[68] * z[51];
z[32] = z[32] + z[51] + z[59] + -z[71];
z[32] = abb[32] * z[32];
z[15] = z[15] + z[32] + -z[66];
z[15] = abb[36] * z[15];
z[32] = abb[6] + abb[7];
z[51] = -z[32] + -z[53];
z[51] = abb[68] * z[51];
z[22] = -z[22] + -z[44] + z[51];
z[44] = z[53] + z[112];
z[44] = -z[6] * z[44];
z[51] = abb[6] + -abb[16];
z[53] = -z[51] + z[116];
z[53] = abb[69] * z[53];
z[22] = z[12] + 2 * z[22] + z[44] + z[53] + -z[71];
z[22] = abb[40] * z[22];
z[44] = z[63] + -z[82];
z[44] = abb[71] * z[44];
z[50] = abb[43] * z[50];
z[37] = -abb[7] + z[37] + -z[61] + -z[122];
z[37] = abb[44] * z[37];
z[48] = -abb[4] + z[48] + z[128];
z[48] = abb[42] * z[48];
z[37] = z[37] + z[48];
z[37] = abb[69] * z[37];
z[13] = z[13] + -z[115];
z[48] = abb[77] * z[13];
z[8] = z[8] + z[11] + z[15] + z[22] + z[37] + z[44] + z[48] + z[50];
z[3] = -abb[15] + z[0] + -z[3] + z[88] + -z[102];
z[3] = abb[69] * z[3];
z[11] = -z[19] + z[40] + -z[101];
z[11] = -z[6] * z[11];
z[4] = -abb[14] + -z[4];
z[0] = z[0] + 2 * z[4] + -z[107];
z[0] = abb[68] * z[0];
z[0] = z[0] + z[3] + z[11] + z[16] + z[31] + -z[67] + -2 * z[86] + -z[98];
z[0] = abb[31] * z[0];
z[3] = -z[6] * z[104];
z[2] = -z[2] + -z[18];
z[2] = abb[69] * z[2];
z[4] = -abb[3] + -z[51];
z[4] = abb[7] + 2 * z[4];
z[4] = abb[68] * z[4];
z[2] = z[2] + z[3] + z[4] + z[12] + -z[24] + z[33] + z[96];
z[2] = abb[36] * z[2];
z[3] = abb[38] * z[47];
z[4] = -abb[4] + z[80];
z[4] = z[4] * z[6];
z[4] = z[4] + -z[30] + -z[69];
z[4] = abb[32] * z[4];
z[3] = z[3] + z[4] + z[34];
z[4] = abb[14] + z[61] + -z[74];
z[4] = abb[69] * z[4];
z[11] = abb[68] * z[68];
z[4] = z[4] + z[11] + -z[43] + -z[76];
z[4] = z[4] * z[94];
z[11] = abb[69] * z[52];
z[12] = abb[68] * z[95];
z[12] = z[11] + z[12] + -z[26] + z[71];
z[15] = 2 * abb[35];
z[12] = z[12] * z[15];
z[16] = -z[52] + -z[61];
z[16] = abb[69] * z[16];
z[18] = abb[4] * abb[68];
z[16] = z[16] + z[18] + z[46] + -z[71];
z[16] = abb[37] * z[16];
z[18] = abb[36] * z[10];
z[19] = abb[31] * z[91];
z[22] = abb[34] * z[81];
z[18] = z[18] + -z[19] + z[22];
z[18] = z[18] * z[103];
z[0] = z[0] + z[2] + 2 * z[3] + z[4] + z[12] + z[16] + z[18];
z[0] = z[0] * z[20];
z[2] = abb[40] * z[10];
z[3] = abb[42] + abb[43];
z[3] = z[3] * z[58];
z[4] = abb[74] * z[25];
z[2] = z[2] + z[3] + -z[4];
z[3] = abb[32] * z[58];
z[4] = -z[3] + z[9];
z[9] = abb[36] * z[91];
z[4] = 2 * z[4] + -z[9] + -z[133];
z[4] = abb[31] * z[4];
z[9] = abb[57] + -5 * abb[61] + 2 * abb[62];
z[7] = abb[56] * (T(2) / T(3)) + -z[7] + (T(1) / T(3)) * z[9];
z[7] = z[7] * z[17];
z[9] = abb[34] * z[25];
z[9] = z[9] + z[131];
z[10] = 2 * abb[36];
z[9] = z[9] * z[10];
z[10] = abb[33] * z[58];
z[12] = abb[32] * z[81];
z[10] = z[10] + -z[12];
z[10] = abb[33] * z[10];
z[3] = z[3] + -z[22];
z[3] = abb[32] * z[3];
z[12] = z[35] * z[91];
z[2] = 2 * z[2] + z[3] + -z[4] + -z[7] + z[9] + z[10] + z[12];
z[3] = -abb[73] * z[81];
z[3] = z[2] + z[3];
z[3] = z[3] * z[39];
z[4] = -abb[39] + -abb[73];
z[4] = z[4] * z[81];
z[2] = z[2] + z[4];
z[2] = z[2] * z[132];
z[4] = abb[78] * z[54];
z[7] = -abb[72] * z[27];
z[9] = abb[17] * z[6];
z[10] = abb[7] * abb[68];
z[12] = -abb[69] * z[116];
z[9] = z[9] + z[10] + z[12] + z[14] + -z[36] + z[45] + -z[56];
z[9] = z[9] * z[29];
z[10] = -abb[6] + z[119];
z[10] = abb[68] * z[10];
z[10] = z[10] + z[28] + -z[71] + -z[82];
z[10] = abb[36] * z[10];
z[12] = z[6] * z[95];
z[12] = -z[11] + z[12] + -z[78];
z[12] = abb[32] * z[12];
z[13] = abb[31] * z[13];
z[9] = z[9] + z[10] + z[12] + z[13];
z[10] = -z[121] + z[128];
z[10] = abb[68] * z[10];
z[10] = z[10] + -z[11] + -z[49] + z[108];
z[10] = abb[35] * z[10];
z[9] = 2 * z[9] + z[10];
z[9] = z[9] * z[15];
z[10] = -z[38] + z[89] + z[128];
z[10] = abb[68] * z[10];
z[11] = z[32] + -z[38] + -z[93];
z[11] = abb[69] * z[11];
z[10] = z[10] + z[11] + z[43] + z[79];
z[10] = abb[33] * z[10];
z[11] = -abb[32] * z[70];
z[12] = -abb[36] * z[42];
z[11] = z[11] + z[12];
z[10] = z[10] + 2 * z[11];
z[10] = z[10] * z[94];
z[11] = z[83] + z[84] + z[87];
z[11] = -4 * z[11];
z[11] = abb[75] * z[11];
z[12] = -abb[73] * z[129];
z[13] = -abb[76] * z[21];
z[15] = z[26] + z[124];
z[15] = 4 * z[15] + -8 * z[36];
z[15] = abb[74] * z[15];
z[16] = -abb[17] + z[93];
z[6] = 4 * z[6];
z[6] = z[6] * z[16];
z[16] = -abb[1] + abb[9];
z[16] = z[16] * z[23];
z[16] = z[16] + -z[86];
z[6] = z[6] + 8 * z[16];
z[6] = abb[44] * z[6];
z[16] = -abb[3] + z[41] + -z[92];
z[16] = abb[68] * z[16];
z[16] = z[16] + z[59];
z[16] = 4 * z[16];
z[16] = abb[42] * z[16];
z[17] = -abb[42] + -abb[74];
z[14] = z[14] * z[17];
z[17] = abb[53] * z[134];
z[0] = abb[54] + abb[55] + z[0] + z[2] + z[3] + z[4] + 2 * z[5] + z[6] + z[7] + 4 * z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + 8 * z[14] + z[15] + z[16] + z[17];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_773_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("-1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("-48.389223528459310168130450869953407056308691317944699117038443347"),stof<T>("11.796735750326443730433504252492078714584450220287678159942546189")}, std::complex<T>{stof<T>("-32.956844600689461635090114165883724737052826743171725653297932899"),stof<T>("10.757002055713661982612541008703347206520567675858769933891847136")}, std::complex<T>{stof<T>("32.956844600689461635090114165883724737052826743171725653297932899"),stof<T>("-10.757002055713661982612541008703347206520567675858769933891847136")}, std::complex<T>{stof<T>("-32.956844600689461635090114165883724737052826743171725653297932899"),stof<T>("10.757002055713661982612541008703347206520567675858769933891847136")}, std::complex<T>{stof<T>("17.524465672919613102049777461814042417796962168398752189557422451"),stof<T>("-9.717268361100880234791577764914615698456685131429861707841148082")}, std::complex<T>{stof<T>("15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("-1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("12.899763255503401807831752917559270039767543191084734233311071807"),stof<T>("45.801061316667604309533417689350606448565159031953801252301863585")}, std::complex<T>{stof<T>("-1.132699960112929679730911814059604533150465662833572213217989601"),stof<T>("36.105115637866145651414687547245681925152780039840012880878284257")}, std::complex<T>{stof<T>("29.269008906779025853130248647437327666292642403221379247469596994"),stof<T>("81.10521292138537943263681473577047730584110434713839732432727867")}, std::complex<T>{stof<T>("-1.132699960112929679730911814059604533150465662833572213217989601"),stof<T>("36.105115637866145651414687547245681925152780039840012880878284257")}, std::complex<T>{stof<T>("29.269008906779025853130248647437327666292642403221379247469596994"),stof<T>("81.10521292138537943263681473577047730584110434713839732432727867")}, std::complex<T>{stof<T>("3.9318187404805197820521584578624244708205094635939562581493146446"),stof<T>("1.0306356838971807472985840279556750782218872016936305185446502025")}, std::complex<T>{stof<T>("-14.977800989703972452159484215422222710863785581005329288414524704"),stof<T>("-67.777713173891206101367114103938205383416903529136989134497599514")}, std::complex<T>{stof<T>("-0.051881463697184215754529699493716126989310907775594881303416497"),stof<T>("25.098197810448281517736238067280671084815067568294361227844109094")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[30].real()/kbase.W[30].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_773_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_773_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (4 * m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[63] + abb[65] + abb[67]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[63] + abb[65] + abb[67];
z[1] = abb[37] + -abb[38];
return 8 * abb[11] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_773_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + 8 * (-2 + v[0] + -2 * v[1] + v[2] + 2 * v[3] + v[4] + -v[5]) + m1_set::bc<T>[2] * (-21 * v[0] + v[1] + 3 * (-8 + -3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = (-2 * (-2 + 2 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[63] + abb[65] + abb[67]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[37] + abb[38];
z[1] = abb[31] + -abb[36];
z[0] = z[0] * z[1];
z[0] = abb[39] + abb[41] + -abb[44] + z[0];
z[1] = -abb[63] + -abb[65] + -abb[67];
return 8 * abb[11] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_773_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (-m1_set::bc<T>[0] * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (4 * m1_set::bc<T>[0] * (v[2] + v[3])) / tend;


		return (abb[65] + abb[67] + abb[68]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[65] + -abb[67] + -abb[68];
z[1] = abb[34] + -abb[36];
return 8 * abb[12] * m1_set::bc<T>[0] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_773_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(-1) / T(2)) * (v[2] + v[3]) * (24 + -7 * v[2] + v[3] + 4 * v[5])) / prod_pow(tend, 2);
c[1] = (-12 * (v[2] + v[3])) / tend;


		return (abb[65] + abb[67] + abb[68]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[32] + 2 * abb[34] + -abb[35];
z[1] = abb[34] + -abb[36];
z[0] = z[0] * z[1];
z[0] = 3 * abb[40] + z[0];
z[1] = abb[65] + abb[67] + abb[68];
return 8 * abb[12] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_773_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("37.733026168577779341887047151199953132821300782053757322306070688"),stof<T>("11.447849602437662442185975274660668748242668699790036546348267891")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16, 18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,82> abb = {dl[0], dlog_W4(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W15(k,dl), dlog_W17(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_13(k), f_2_15(k), f_2_16(k), f_2_17(k), f_2_18(k), f_2_2_im(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_9_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), f_2_27_im(k), T{0}, T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[30].real()/k.W[30].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), f_2_2_re(k), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_9_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), f_2_27_re(k), T{0}, T{0}};
abb[54] = SpDLog_f_4_773_W_17_Im(t, path, abb);
abb[55] = SpDLog_f_4_773_W_19_Im(t, path, abb);
abb[80] = SpDLog_f_4_773_W_17_Re(t, path, abb);
abb[81] = SpDLog_f_4_773_W_19_Re(t, path, abb);

                    
            return f_4_773_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_773_DLogXconstant_part(base_point<T>, kend);
	value += f_4_773_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_773_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_773_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_773_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_773_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_773_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_773_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
