/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_712.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_712_abbreviated (const std::array<T,61>& abb) {
T z[95];
z[0] = -abb[49] + abb[50];
z[1] = -abb[48] + abb[51];
z[2] = z[0] + z[1];
z[3] = abb[20] * z[2];
z[4] = abb[17] + abb[19];
z[5] = z[2] * z[4];
z[3] = z[3] + z[5];
z[6] = abb[22] * z[2];
z[7] = abb[47] + -abb[52];
z[8] = z[1] + -z[7];
z[9] = abb[21] * z[8];
z[10] = z[6] + z[9];
z[11] = abb[18] * z[2];
z[12] = z[0] + z[7];
z[13] = abb[16] * z[12];
z[11] = z[3] + -z[10] + z[11] + z[13];
z[14] = abb[24] * z[11];
z[15] = 2 * abb[1];
z[16] = 2 * abb[5];
z[17] = -abb[3] + -abb[4] + abb[11] + -z[15] + z[16];
z[17] = abb[27] * z[17];
z[18] = -abb[0] + abb[11];
z[19] = -abb[4] + abb[8];
z[20] = z[18] + -z[19];
z[21] = abb[31] * z[20];
z[22] = abb[2] + abb[5];
z[23] = -abb[8] + z[22];
z[23] = abb[29] * z[23];
z[24] = z[21] + -z[23];
z[25] = abb[1] + -abb[5];
z[26] = -abb[8] + abb[11] + z[25];
z[27] = abb[24] * z[26];
z[28] = 2 * z[25];
z[29] = abb[30] * z[28];
z[17] = z[17] + z[24] + -z[27] + z[29];
z[27] = abb[54] + abb[56];
z[17] = z[17] * z[27];
z[30] = abb[18] * z[12];
z[31] = z[13] + z[30];
z[32] = abb[20] * z[12];
z[33] = z[31] + z[32];
z[10] = z[10] + -z[33];
z[10] = abb[31] * z[10];
z[12] = z[4] * z[12];
z[34] = z[12] + z[33];
z[34] = abb[29] * z[34];
z[35] = z[5] + z[6];
z[36] = abb[0] + -abb[4];
z[37] = -abb[11] + z[22] + z[36];
z[37] = z[27] * z[37];
z[37] = -z[35] + z[37];
z[38] = abb[26] * z[37];
z[39] = z[4] * z[8];
z[40] = abb[20] * z[8];
z[41] = z[39] + z[40];
z[42] = -z[9] + z[41];
z[8] = abb[18] * z[8];
z[43] = z[8] + z[42];
z[44] = abb[30] * z[43];
z[32] = -z[6] + z[32];
z[45] = -z[30] + -z[32];
z[45] = abb[27] * z[45];
z[46] = z[12] + z[13];
z[47] = abb[3] + -z[19] + z[25];
z[48] = z[27] * z[47];
z[48] = -z[46] + z[48];
z[49] = abb[25] * z[48];
z[39] = -z[9] + z[39];
z[50] = abb[53] + abb[55];
z[51] = abb[4] + abb[5];
z[52] = -abb[10] + z[51];
z[53] = z[50] * z[52];
z[53] = z[39] + z[53];
z[53] = abb[28] * z[53];
z[14] = z[10] + z[14] + z[17] + z[34] + z[38] + -z[44] + z[45] + z[49] + z[53];
z[14] = m1_set::bc<T>[0] * z[14];
z[17] = abb[1] + -abb[3];
z[45] = -abb[5] + z[19];
z[49] = -z[17] + z[45];
z[53] = abb[25] * z[49];
z[54] = abb[8] + -abb[10];
z[55] = -z[17] + z[54];
z[56] = abb[24] * z[55];
z[57] = abb[2] + -abb[4];
z[58] = -z[17] + z[57];
z[59] = abb[27] * z[58];
z[60] = abb[2] + -abb[10];
z[61] = abb[3] + z[60];
z[62] = -z[25] + z[61];
z[63] = abb[30] * z[62];
z[60] = z[19] + z[60];
z[64] = abb[31] * z[60];
z[65] = abb[26] * z[57];
z[53] = z[23] + z[53] + -z[56] + -z[59] + z[63] + z[64] + -2 * z[65];
z[53] = m1_set::bc<T>[0] * z[53];
z[56] = abb[45] * z[62];
z[59] = abb[44] * z[49];
z[66] = abb[43] * z[55];
z[67] = 2 * z[57];
z[67] = abb[46] * z[67];
z[53] = z[53] + z[56] + z[59] + -z[66] + -z[67];
z[53] = -z[50] * z[53];
z[56] = abb[43] * z[11];
z[59] = abb[44] * z[47];
z[66] = abb[45] * z[28];
z[67] = abb[43] * z[26];
z[59] = z[59] + z[66] + -z[67];
z[59] = z[27] * z[59];
z[66] = -abb[45] * z[43];
z[67] = -abb[44] * z[46];
z[68] = abb[46] * z[37];
z[14] = z[14] + z[53] + z[56] + z[59] + z[66] + z[67] + z[68];
z[14] = 4 * z[14];
z[53] = abb[1] + abb[2];
z[56] = 2 * abb[7];
z[59] = -abb[4] + z[56];
z[66] = 2 * abb[6];
z[67] = -abb[3] + z[53] + z[59] + -z[66];
z[67] = abb[40] * z[67];
z[68] = abb[0] + abb[3];
z[69] = -abb[9] + z[56];
z[70] = -abb[10] + z[68] + -z[69];
z[70] = abb[32] * z[70];
z[62] = abb[59] * z[62];
z[55] = abb[57] * z[55];
z[71] = -abb[0] + abb[2];
z[72] = -abb[1] + z[19];
z[73] = z[71] + z[72];
z[74] = abb[37] * z[73];
z[61] = z[36] + z[61];
z[75] = abb[36] * z[61];
z[76] = abb[3] + z[36];
z[77] = -abb[5] + z[69] + z[76];
z[77] = abb[34] * z[77];
z[78] = abb[14] * abb[41];
z[55] = z[55] + -z[62] + z[67] + z[70] + -z[74] + -z[75] + z[77] + -z[78];
z[62] = -abb[9] + z[66];
z[67] = abb[10] + -z[22] + z[62];
z[67] = abb[30] * z[67];
z[70] = -z[64] + z[67];
z[74] = -z[56] + z[66];
z[75] = -z[57] + z[74];
z[75] = abb[27] * z[75];
z[54] = abb[24] * z[54];
z[23] = -z[23] + z[54] + z[65] + z[70] + -z[75];
z[54] = 2 * abb[26];
z[23] = z[23] * z[54];
z[45] = z[45] + z[69];
z[54] = abb[26] * z[45];
z[65] = abb[29] * z[49];
z[69] = abb[24] + abb[27];
z[75] = z[45] * z[69];
z[54] = z[54] + -z[65] + -z[75];
z[65] = -abb[7] + z[51];
z[79] = 2 * abb[8];
z[80] = -abb[9] + z[79];
z[65] = z[17] + 2 * z[65] + -z[80];
z[65] = abb[25] * z[65];
z[54] = 2 * z[54] + -z[65];
z[54] = abb[25] * z[54];
z[60] = -z[17] + -z[60] + z[74];
z[60] = abb[24] * z[60];
z[65] = abb[29] * z[73];
z[74] = abb[6] + -abb[7];
z[74] = abb[27] * z[74];
z[60] = -z[60] + z[65] + z[70] + -z[74];
z[65] = 2 * abb[27];
z[60] = z[60] * z[65];
z[22] = -z[22] + z[68];
z[65] = abb[29] * z[22];
z[64] = z[64] + z[65] + z[67];
z[67] = 2 * abb[24];
z[64] = z[64] * z[67];
z[67] = 2 * abb[10] + z[17];
z[68] = -2 * abb[2] + -z[16] + z[62] + z[67];
z[70] = prod_pow(abb[30], 2);
z[68] = z[68] * z[70];
z[67] = -3 * z[57] + z[67] + -z[79];
z[74] = prod_pow(abb[31], 2);
z[67] = z[67] * z[74];
z[81] = -z[25] + -z[62] + z[71];
z[82] = 2 * abb[35];
z[81] = z[81] * z[82];
z[83] = 2 * abb[31];
z[73] = z[73] * z[83];
z[65] = z[65] + z[73];
z[65] = abb[29] * z[65];
z[17] = (T(-1) / T(3)) * z[17] + z[57];
z[73] = prod_pow(m1_set::bc<T>[0], 2);
z[17] = z[17] * z[73];
z[84] = abb[18] + abb[20];
z[85] = 2 * z[4] + z[84];
z[86] = 2 * abb[42];
z[85] = z[85] * z[86];
z[87] = z[25] + z[36];
z[88] = 2 * abb[39];
z[89] = z[87] * z[88];
z[90] = 2 * abb[38];
z[22] = z[22] * z[90];
z[91] = 2 * abb[58];
z[49] = z[49] * z[91];
z[92] = -abb[1] + abb[8];
z[62] = -abb[0] + z[62] + z[92];
z[93] = 2 * abb[33];
z[94] = z[62] * z[93];
z[57] = abb[60] * z[57];
z[17] = z[17] + z[22] + -z[23] + -z[49] + z[54] + 2 * z[55] + 4 * z[57] + z[60] + z[64] + z[65] + z[67] + -z[68] + z[81] + z[85] + -z[89] + z[94];
z[17] = -z[17] * z[50];
z[22] = -z[9] + z[13];
z[6] = z[6] + -2 * z[22];
z[3] = -z[3] + z[6] + -2 * z[30];
z[3] = abb[24] * z[3];
z[22] = abb[30] * z[42];
z[23] = -z[10] + z[22];
z[34] = -z[23] + z[34];
z[49] = -z[0] + z[1] + -2 * z[7];
z[49] = abb[20] * z[49];
z[49] = z[35] + z[49];
z[49] = abb[27] * z[49];
z[54] = -z[18] + z[80];
z[55] = -abb[5] + z[54];
z[55] = abb[24] * z[55];
z[57] = 2 * z[59];
z[59] = -abb[5] + -abb[9] + z[18] + z[57];
z[59] = abb[27] * z[59];
z[55] = 2 * z[24] + z[55] + z[59];
z[55] = abb[54] * z[55];
z[59] = -abb[5] + abb[6];
z[60] = abb[30] * z[59];
z[60] = 2 * z[60];
z[24] = z[24] + z[60];
z[64] = abb[0] + abb[11];
z[65] = z[64] + -z[80];
z[67] = abb[5] + -z[65];
z[67] = abb[24] * z[67];
z[68] = 2 * abb[4] + 4 * abb[6];
z[64] = -abb[9] + z[64];
z[81] = 5 * abb[5] + z[64] + -z[68];
z[81] = abb[27] * z[81];
z[24] = 2 * z[24] + z[67] + z[81];
z[24] = abb[56] * z[24];
z[24] = -z[3] + z[24] + 2 * z[34] + z[38] + z[49] + z[55];
z[24] = abb[26] * z[24];
z[34] = abb[27] + abb[31];
z[34] = z[34] * z[61];
z[38] = abb[29] * z[58];
z[49] = abb[24] * z[87];
z[52] = abb[26] * z[52];
z[34] = -z[34] + z[38] + z[49] + z[52] + z[63];
z[34] = z[34] * z[50];
z[25] = -z[25] + z[36];
z[38] = abb[27] + -abb[29];
z[25] = z[25] * z[38];
z[38] = z[36] * z[83];
z[25] = -z[25] + -z[29] + -z[38] + z[49];
z[25] = z[25] * z[27];
z[29] = z[8] + z[9];
z[38] = z[29] + z[40];
z[49] = abb[31] * z[38];
z[52] = z[8] + z[41];
z[55] = -abb[24] * z[52];
z[39] = abb[26] * z[39];
z[9] = abb[27] * z[9];
z[9] = z[9] + z[25] + z[34] + z[39] + z[44] + z[49] + z[55];
z[25] = z[27] + z[50];
z[25] = z[25] * z[87];
z[25] = z[25] + -z[52];
z[25] = abb[28] * z[25];
z[9] = 2 * z[9] + z[25];
z[9] = abb[28] * z[9];
z[11] = abb[57] * z[11];
z[25] = -z[12] + -z[30];
z[25] = abb[34] * z[25];
z[34] = abb[37] * z[33];
z[39] = abb[59] * z[43];
z[37] = abb[60] * z[37];
z[11] = -z[11] + z[25] + z[34] + -z[37] + z[39];
z[25] = z[12] + z[31];
z[34] = -abb[54] * z[45];
z[37] = -abb[5] + abb[9];
z[19] = -z[19] + z[37];
z[39] = abb[56] * z[19];
z[34] = -z[25] + z[34] + z[39];
z[34] = abb[26] * z[34];
z[25] = z[25] * z[69];
z[39] = abb[29] * z[46];
z[43] = abb[29] * z[47];
z[44] = -z[43] + z[75];
z[44] = abb[54] * z[44];
z[19] = -z[19] * z[69];
z[19] = z[19] + -z[43];
z[19] = abb[56] * z[19];
z[19] = z[19] + z[25] + z[34] + z[39] + z[44];
z[25] = abb[1] + abb[3];
z[34] = z[25] + -z[80];
z[39] = abb[4] + -abb[7];
z[39] = z[34] + 2 * z[39];
z[39] = abb[54] * z[39];
z[44] = abb[4] + -abb[5];
z[34] = z[34] + 2 * z[44];
z[34] = abb[56] * z[34];
z[34] = -z[30] + z[34] + z[39] + -2 * z[46];
z[34] = abb[25] * z[34];
z[19] = 2 * z[19] + z[34];
z[19] = abb[25] * z[19];
z[13] = -abb[29] * z[13];
z[13] = z[13] + z[23];
z[5] = -z[5] + -z[30] + -z[40];
z[5] = abb[27] * z[5];
z[3] = z[3] + z[5] + 2 * z[13];
z[3] = abb[27] * z[3];
z[5] = z[71] + -z[72];
z[13] = abb[37] * z[5];
z[23] = 2 * abb[36];
z[30] = z[23] * z[36];
z[34] = abb[59] * z[28];
z[26] = abb[57] * z[26];
z[13] = z[13] + -z[26] + -z[30] + z[34];
z[26] = abb[0] + -abb[1] + z[51] + -z[56];
z[26] = abb[40] * z[26];
z[30] = 2 * abb[32];
z[34] = -abb[0] + abb[7];
z[34] = z[30] * z[34];
z[26] = -z[13] + z[26] + z[34] + -z[77];
z[34] = abb[11] + -4 * z[36] + z[53] + -z[79];
z[34] = z[34] * z[74];
z[5] = abb[29] * z[5] * z[83];
z[4] = z[4] * z[86];
z[4] = z[4] + -z[5] + z[34] + -z[78];
z[5] = 3 * abb[5];
z[34] = z[5] + -z[15] + -z[57] + z[65];
z[34] = abb[24] * z[34];
z[39] = -z[21] + z[43];
z[25] = -abb[9] + z[25];
z[40] = abb[5] + -abb[7];
z[40] = -z[25] + 2 * z[40];
z[40] = abb[27] * z[40];
z[34] = z[34] + 2 * z[39] + z[40];
z[34] = abb[27] * z[34];
z[28] = z[28] * z[70];
z[20] = z[20] * z[83];
z[40] = abb[5] + -z[64];
z[40] = abb[24] * z[40];
z[20] = z[20] + z[40];
z[20] = abb[24] * z[20];
z[20] = -z[4] + z[20] + 2 * z[26] + z[28] + z[34];
z[20] = abb[54] * z[20];
z[26] = abb[5] + z[36];
z[28] = -abb[1] + -z[26] + z[66];
z[28] = abb[40] * z[28];
z[34] = z[37] + -z[76];
z[34] = abb[34] * z[34];
z[36] = abb[1] + abb[6] + -z[16];
z[36] = z[36] * z[70];
z[13] = -z[13] + z[28] + z[34] + z[36];
z[5] = -z[5] + -z[15] + -z[54] + z[68];
z[5] = abb[24] * z[5];
z[15] = z[39] + -z[60];
z[16] = -z[16] + -z[25] + z[66];
z[16] = abb[27] * z[16];
z[5] = z[5] + 2 * z[15] + z[16];
z[5] = abb[27] * z[5];
z[15] = z[21] + -z[60];
z[16] = -z[18] + z[37];
z[16] = abb[24] * z[16];
z[15] = 2 * z[15] + z[16];
z[15] = abb[24] * z[15];
z[16] = abb[35] * z[59];
z[4] = -z[4] + z[5] + 2 * z[13] + z[15] + 4 * z[16];
z[4] = abb[56] * z[4];
z[0] = 2 * z[0] + -z[1] + 3 * z[7];
z[0] = z[0] * z[84];
z[0] = z[0] + -z[6];
z[0] = z[0] * z[74];
z[1] = abb[29] * z[12];
z[5] = -z[1] + z[10] + z[22];
z[6] = z[8] + z[32];
z[6] = abb[24] * z[6];
z[5] = 2 * z[5] + z[6];
z[5] = abb[24] * z[5];
z[6] = z[29] * z[30];
z[7] = -z[23] * z[38];
z[10] = -z[33] * z[83];
z[1] = -z[1] + z[10];
z[1] = abb[29] * z[1];
z[10] = -z[41] * z[82];
z[8] = -z[8] + -2 * z[42];
z[8] = z[8] * z[70];
z[13] = -z[48] * z[91];
z[15] = -abb[56] * z[62];
z[16] = -abb[0] + abb[9] + -z[92];
z[16] = abb[54] * z[16];
z[15] = z[15] + z[16] + -z[31];
z[15] = z[15] * z[93];
z[16] = abb[1] + -abb[2] + abb[11] + -2 * z[26];
z[16] = z[16] * z[27];
z[16] = z[16] + z[35];
z[16] = z[16] * z[73];
z[18] = z[27] * z[87];
z[18] = z[18] + -z[52];
z[18] = z[18] * z[88];
z[21] = abb[41] * abb[54];
z[22] = abb[41] * abb[56];
z[23] = z[21] + -z[22];
z[25] = abb[12] + abb[13];
z[23] = z[23] * z[25];
z[2] = abb[23] * abb[41] * z[2];
z[12] = -z[12] * z[90];
z[21] = z[21] + z[22];
z[21] = abb[15] * z[21];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + 2 * z[11] + z[12] + z[13] + z[15] + (T(1) / T(3)) * z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[23] + z[24];
z[0] = 2 * z[0];
return {z[14], z[0]};
}


template <typename T> std::complex<T> f_4_712_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("10.402302819131233336271940183191322136570276882477279319319257397"),stof<T>("-12.769388978144903263363784176908690571680015421503048994714708313")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("-6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-10.402302819131233336271940183191322136570276882477279319319257397"),stof<T>("12.769388978144903263363784176908690571680015421503048994714708313")}, std::complex<T>{stof<T>("-14.828489720484507046897453457450770565450591257440239914391065281"),stof<T>("-10.877054322280359479773219977897823112402103767095470461713939626")}, std::complex<T>{stof<T>("-16.537165260607952306513862350575882959560418493699168394850268043"),stof<T>("-10.877054322280359479773219977897823112402103767095470461713939626")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_712_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_712_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("16.318242825496372264672583411332668116429845160004445986730124149"),stof<T>("-21.397581756938163177290323382231810987414296529853350017825968654")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,61> abb = {dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W18(k,dl), dlog_W21(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_20(k), f_2_21(k), f_2_23(k), f_2_29_im(k), f_2_2_im(k), f_2_9_im(k), f_2_10_im(k), f_2_12_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), f_2_2_re(k), f_2_9_re(k), f_2_10_re(k), f_2_12_re(k)};

                    
            return f_4_712_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_712_DLogXconstant_part(base_point<T>, kend);
	value += f_4_712_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_712_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_712_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_712_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_712_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_712_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_712_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
