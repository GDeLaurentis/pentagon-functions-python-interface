/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_703.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_703_abbreviated (const std::array<T,66>& abb) {
T z[117];
z[0] = abb[6] * abb[56];
z[1] = abb[5] * abb[56];
z[2] = z[0] + z[1];
z[3] = abb[24] * abb[52];
z[4] = 2 * z[3];
z[5] = abb[56] + abb[58];
z[6] = abb[1] * z[5];
z[7] = 4 * z[6];
z[8] = z[4] + -z[7];
z[9] = -abb[51] + abb[54];
z[10] = abb[52] + -abb[55];
z[11] = z[9] + z[10];
z[12] = abb[23] * z[11];
z[13] = abb[4] * z[5];
z[14] = abb[13] * z[5];
z[15] = z[13] + -z[14];
z[16] = abb[3] * abb[58];
z[17] = 2 * abb[56];
z[18] = abb[12] * z[17];
z[19] = abb[14] * abb[58];
z[20] = 2 * abb[58];
z[21] = -abb[56] + z[20];
z[21] = abb[7] * z[21];
z[22] = abb[53] + -2 * abb[55] + z[9];
z[22] = abb[24] * z[22];
z[21] = z[2] + z[8] + z[12] + -z[15] + -z[16] + z[18] + z[19] + z[21] + z[22];
z[22] = 2 * abb[30];
z[21] = z[21] * z[22];
z[23] = 2 * abb[60];
z[24] = z[5] + z[23];
z[25] = abb[5] * z[24];
z[26] = z[15] + -z[25];
z[27] = abb[26] * z[11];
z[28] = abb[56] + -abb[58];
z[29] = abb[7] * z[28];
z[30] = -abb[55] + z[9];
z[31] = abb[24] * z[30];
z[29] = z[29] + -z[31];
z[31] = abb[56] + abb[60];
z[31] = abb[10] * z[31];
z[32] = 2 * z[31];
z[33] = z[29] + z[32];
z[34] = abb[16] * z[28];
z[35] = abb[12] * abb[56];
z[8] = -z[8] + z[26] + z[27] + 2 * z[33] + -z[34] + -4 * z[35];
z[8] = abb[29] * z[8];
z[33] = abb[7] * abb[58];
z[36] = abb[53] + -abb[55];
z[36] = abb[24] * z[36];
z[33] = z[3] + z[33] + z[36];
z[37] = z[18] + z[33];
z[38] = 2 * z[5];
z[39] = abb[1] * z[38];
z[40] = z[37] + -z[39];
z[41] = abb[4] * abb[58];
z[42] = z[19] + z[40] + -z[41];
z[42] = abb[33] * z[42];
z[43] = abb[3] * abb[56];
z[18] = z[18] + -z[43];
z[44] = abb[4] * abb[56];
z[2] = -z[2] + z[18] + z[44];
z[2] = abb[31] * z[2];
z[45] = abb[5] * abb[58];
z[46] = -z[19] + z[45];
z[47] = z[16] + z[46];
z[48] = abb[16] * abb[58];
z[49] = abb[53] + z[10];
z[50] = abb[26] * z[49];
z[48] = z[48] + z[50];
z[50] = z[47] + -z[48];
z[51] = -z[32] + z[50];
z[51] = abb[28] * z[51];
z[2] = z[2] + -z[42] + z[51];
z[51] = abb[7] * abb[56];
z[52] = -abb[53] + z[9];
z[53] = abb[24] * z[52];
z[51] = z[51] + -z[53];
z[53] = abb[6] * abb[58];
z[54] = z[51] + z[53];
z[55] = -2 * abb[53] + z[9] + -z[10];
z[56] = abb[26] * z[55];
z[57] = abb[16] * z[5];
z[56] = -z[56] + z[57];
z[26] = z[26] + 2 * z[54] + -z[56];
z[57] = 2 * abb[0];
z[58] = abb[60] + z[5];
z[59] = z[57] * z[58];
z[60] = -abb[22] * z[55];
z[60] = -z[12] + z[26] + z[59] + z[60];
z[60] = abb[32] * z[60];
z[61] = z[44] + z[51];
z[62] = abb[16] * abb[56];
z[63] = abb[26] * z[52];
z[62] = z[62] + -z[63];
z[63] = z[61] + -z[62];
z[64] = abb[56] + z[23];
z[64] = abb[5] * z[64];
z[65] = -z[32] + z[43] + -z[63] + z[64];
z[66] = abb[23] * z[52];
z[67] = z[65] + z[66];
z[68] = abb[22] * z[52];
z[68] = z[67] + z[68];
z[69] = 2 * abb[34];
z[68] = z[68] * z[69];
z[41] = z[41] + z[53];
z[48] = -z[41] + z[48];
z[70] = -z[19] + -z[48];
z[71] = -z[32] + z[59];
z[72] = -z[70] + -z[71];
z[73] = 2 * abb[35];
z[72] = z[72] * z[73];
z[74] = abb[31] * z[52];
z[75] = abb[33] * z[49];
z[76] = z[74] + z[75];
z[77] = 2 * z[76];
z[78] = abb[29] * z[11];
z[79] = 2 * z[49];
z[80] = abb[35] * z[79];
z[80] = z[77] + z[78] + -z[80];
z[81] = -abb[23] * z[80];
z[82] = abb[30] * z[79];
z[83] = -2 * z[75] + -z[78] + z[82];
z[83] = abb[22] * z[83];
z[84] = abb[28] * z[58];
z[85] = 2 * z[84];
z[86] = abb[29] * z[58];
z[87] = z[85] + -z[86];
z[87] = z[57] * z[87];
z[2] = 2 * z[2] + z[8] + z[21] + z[60] + z[68] + z[72] + z[81] + z[83] + z[87];
z[2] = m1_set::bc<T>[0] * z[2];
z[8] = abb[6] * z[28];
z[21] = abb[5] * z[5];
z[8] = z[3] + z[8] + -z[15] + z[21] + -z[29] + -z[39];
z[12] = z[8] + z[12];
z[12] = abb[47] * z[12];
z[15] = abb[22] + abb[25];
z[29] = z[15] * z[52];
z[29] = z[29] + z[67];
z[29] = abb[49] * z[29];
z[50] = z[50] + z[71];
z[60] = abb[46] * z[50];
z[40] = z[40] + -z[47] + z[53];
z[47] = abb[22] * z[49];
z[47] = z[40] + z[47];
z[47] = abb[48] * z[47];
z[12] = z[12] + z[29] + z[47] + z[60];
z[29] = 2 * abb[1] + -abb[12];
z[47] = -abb[5] + z[29];
z[47] = abb[30] * z[47];
z[60] = -abb[5] + abb[12];
z[60] = abb[31] * z[60];
z[67] = -abb[1] + abb[12];
z[68] = abb[29] * z[67];
z[71] = abb[33] * z[67];
z[47] = z[47] + -z[60] + z[68] + z[71];
z[47] = m1_set::bc<T>[0] * z[47];
z[60] = abb[48] * z[67];
z[68] = abb[1] + -abb[5];
z[72] = abb[47] * z[68];
z[47] = z[47] + -z[60] + z[72];
z[60] = abb[57] + abb[59];
z[72] = -4 * z[60];
z[47] = z[47] * z[72];
z[72] = abb[18] + -abb[19];
z[72] = z[5] * z[72];
z[24] = abb[17] * z[24];
z[30] = abb[52] + z[30];
z[30] = abb[27] * z[30];
z[28] = abb[20] * z[28];
z[24] = -z[24] + -z[28] + z[30] + z[72];
z[28] = -abb[50] * z[24];
z[30] = z[11] * z[22];
z[72] = 2 * z[52];
z[81] = abb[34] * z[72];
z[30] = z[30] + -z[80] + z[81];
z[30] = m1_set::bc<T>[0] * z[30];
z[80] = -abb[32] * m1_set::bc<T>[0];
z[80] = 2 * abb[47] + z[80];
z[80] = z[11] * z[80];
z[30] = z[30] + z[80];
z[30] = abb[25] * z[30];
z[80] = -abb[46] + abb[48];
z[80] = z[49] * z[80];
z[81] = abb[28] * z[49];
z[83] = z[75] + z[81];
z[87] = abb[30] * z[49];
z[87] = -z[83] + z[87];
z[88] = abb[35] * z[49];
z[89] = z[87] + z[88];
z[90] = m1_set::bc<T>[0] * z[89];
z[80] = z[80] + z[90];
z[90] = 2 * abb[21];
z[80] = z[80] * z[90];
z[2] = z[2] + 2 * z[12] + z[28] + z[30] + z[47] + z[80];
z[2] = 2 * z[2];
z[12] = z[11] * z[15];
z[15] = z[13] + z[14];
z[28] = abb[15] * abb[56];
z[30] = z[19] + z[28];
z[38] = abb[8] * z[38];
z[47] = -z[30] + z[38];
z[80] = abb[2] * abb[58];
z[91] = 2 * z[80];
z[92] = abb[23] * z[49];
z[93] = abb[58] + z[17];
z[94] = abb[3] * z[93];
z[95] = abb[2] * abb[56];
z[96] = abb[21] * z[49];
z[12] = z[12] + -z[15] + -z[47] + z[91] + z[92] + z[94] + z[95] + z[96];
z[12] = abb[37] * z[12];
z[96] = -z[28] + z[66];
z[97] = abb[58] + abb[60];
z[97] = abb[9] * z[97];
z[98] = 2 * z[97];
z[62] = z[1] + z[59] + -z[62] + z[95] + z[96] + -z[98];
z[62] = abb[36] * z[62];
z[99] = z[38] + -z[80];
z[45] = z[30] + -z[45] + z[95] + -z[99];
z[100] = -z[45] + z[51];
z[100] = abb[44] * z[100];
z[51] = -z[51] + z[95];
z[91] = z[28] + -z[38] + z[51] + z[91];
z[91] = abb[39] * z[91];
z[65] = -abb[64] * z[65];
z[101] = z[41] + z[99];
z[102] = abb[3] * z[5];
z[103] = -z[1] + z[30] + -z[101] + z[102];
z[104] = -abb[25] * z[49];
z[104] = -z[92] + -z[103] + z[104];
z[104] = abb[42] * z[104];
z[105] = z[32] + z[98];
z[106] = -z[59] + z[105];
z[107] = -z[43] + z[106];
z[108] = z[30] + -z[80];
z[109] = -z[25] + z[107] + z[108];
z[110] = abb[43] * z[109];
z[111] = abb[58] + z[23];
z[111] = abb[5] * z[111];
z[112] = z[80] + z[111];
z[48] = z[48] + -z[98] + z[112];
z[48] = abb[40] * z[48];
z[101] = z[19] + z[94] + -z[101];
z[113] = abb[41] * z[101];
z[50] = abb[61] * z[50];
z[114] = abb[3] + -abb[4];
z[115] = abb[39] + -abb[44];
z[114] = abb[56] * z[114] * z[115];
z[115] = abb[23] * abb[56];
z[116] = abb[21] * abb[58];
z[115] = z[115] + z[116];
z[115] = abb[45] * z[115];
z[12] = z[12] + z[48] + -z[50] + z[62] + z[65] + z[91] + z[100] + z[104] + -z[110] + -z[113] + z[114] + z[115];
z[26] = -abb[29] * z[26];
z[15] = z[15] + -z[56];
z[48] = -z[99] + -z[105];
z[23] = -z[5] + z[23];
z[23] = abb[5] * z[23];
z[17] = abb[3] * z[17];
z[17] = z[15] + z[17] + z[23] + 2 * z[48];
z[17] = abb[28] * z[17];
z[23] = -abb[32] * z[109];
z[46] = -z[46] + -z[80];
z[46] = abb[31] * z[46];
z[1] = z[1] + -z[28];
z[48] = -z[1] + -z[43];
z[48] = abb[33] * z[48];
z[46] = z[46] + z[48];
z[48] = z[19] + z[106] + -z[112];
z[48] = z[48] * z[73];
z[50] = abb[28] * z[55];
z[62] = 2 * z[74];
z[50] = z[50] + z[62] + z[78];
z[50] = abb[23] * z[50];
z[54] = z[14] + -z[54];
z[47] = -z[21] + -z[47] + z[54];
z[65] = -z[47] + -z[66];
z[65] = z[22] * z[65];
z[66] = abb[28] * z[11];
z[55] = abb[29] * z[55];
z[73] = z[55] + -z[66] + z[82];
z[73] = abb[22] * z[73];
z[64] = z[64] + z[96] + -z[107];
z[74] = -z[64] * z[69];
z[80] = 3 * z[84] + -z[86];
z[80] = z[57] * z[80];
z[17] = z[17] + z[23] + z[26] + 2 * z[46] + z[48] + z[50] + z[65] + z[73] + z[74] + z[80];
z[17] = abb[32] * z[17];
z[23] = abb[11] * z[20];
z[16] = z[16] + z[23];
z[26] = -z[19] + z[38];
z[36] = z[26] + z[36];
z[38] = abb[7] * z[20];
z[46] = 4 * abb[56];
z[48] = 3 * abb[58] + z[46];
z[48] = abb[5] * z[48];
z[50] = abb[6] * z[5];
z[4] = z[4] + -z[13] + -z[16] + -3 * z[28] + 2 * z[36] + z[38] + -z[39] + z[48] + z[50] + z[92];
z[4] = abb[30] * z[4];
z[5] = abb[8] * z[5];
z[36] = abb[11] * abb[58];
z[36] = -z[5] + -z[6] + -z[21] + z[30] + z[35] + z[36];
z[38] = 2 * abb[31];
z[36] = z[36] * z[38];
z[48] = -abb[5] * z[93];
z[16] = z[16] + -z[26] + 2 * z[28] + -z[33] + z[48] + -z[53];
z[16] = abb[33] * z[16];
z[0] = z[0] + z[23];
z[7] = -z[0] + z[7] + z[13] + -z[30] + -z[37];
z[7] = abb[29] * z[7];
z[13] = abb[28] * z[47];
z[26] = abb[28] * z[52];
z[33] = abb[29] * z[49];
z[37] = z[26] + -z[33];
z[37] = abb[23] * z[37];
z[7] = z[7] + z[13] + z[16] + z[36] + z[37];
z[4] = z[4] + 2 * z[7];
z[4] = abb[30] * z[4];
z[7] = z[33] + -z[83];
z[13] = 2 * z[7] + z[88];
z[13] = abb[35] * z[13];
z[16] = -abb[40] + abb[41];
z[16] = z[16] * z[79];
z[36] = 2 * abb[62];
z[37] = z[11] * z[36];
z[47] = prod_pow(abb[33], 2) * z[49];
z[13] = z[13] + z[16] + z[37] + z[47];
z[16] = -z[77] + z[78];
z[16] = abb[28] * z[16];
z[11] = abb[30] * z[11];
z[11] = z[11] + -2 * z[78];
z[11] = abb[30] * z[11];
z[37] = -z[66] + z[78];
z[37] = abb[32] * z[37];
z[47] = abb[39] * z[52];
z[48] = abb[44] * z[52];
z[47] = z[47] + -z[48];
z[47] = 2 * z[47];
z[50] = 2 * abb[29];
z[53] = z[50] * z[76];
z[65] = abb[29] * z[52];
z[26] = -z[26] + z[65];
z[65] = -z[26] * z[69];
z[10] = abb[53] * (T(1) / T(3)) + (T(2) / T(3)) * z[9] + z[10];
z[66] = prod_pow(m1_set::bc<T>[0], 2);
z[69] = -z[10] * z[66];
z[73] = abb[64] * z[72];
z[11] = z[11] + -z[13] + z[16] + z[37] + z[47] + z[53] + z[65] + z[69] + -z[73];
z[11] = abb[25] * z[11];
z[16] = -5 * abb[58] + z[46];
z[16] = abb[7] * z[16];
z[9] = -abb[53] + 5 * abb[55] + -4 * z[9];
z[9] = abb[24] * z[9];
z[20] = -abb[56] + -4 * abb[60] + -z[20];
z[20] = abb[5] * z[20];
z[6] = 10 * z[6] + z[9] + z[16] + z[20] + -z[30] + 8 * z[31] + -2 * z[34] + z[43];
z[9] = -z[14] + z[27];
z[14] = -abb[22] + -abb[23];
z[10] = z[10] * z[14];
z[14] = abb[58] + abb[56] * (T(2) / T(3));
z[14] = abb[4] * z[14];
z[16] = abb[0] * z[58];
z[3] = (T(-5) / T(3)) * z[3] + (T(1) / T(3)) * z[6] + (T(2) / T(3)) * z[9] + z[10] + z[14] + (T(-4) / T(3)) * z[16] + (T(-10) / T(3)) * z[35];
z[3] = z[3] * z[66];
z[6] = -abb[5] + abb[11];
z[9] = z[6] + z[67];
z[10] = abb[31] * z[9];
z[14] = -abb[11] + z[29];
z[14] = abb[29] * z[14];
z[6] = abb[33] * z[6];
z[6] = z[6] + z[10] + z[14];
z[10] = abb[1] + -2 * abb[5] + abb[11];
z[10] = abb[30] * z[10];
z[6] = 2 * z[6] + -z[10];
z[6] = abb[30] * z[6];
z[10] = 2 * abb[63];
z[14] = -z[10] + (T(-5) / T(3)) * z[66];
z[14] = z[14] * z[67];
z[16] = 2 * abb[38];
z[20] = prod_pow(abb[31], 2);
z[27] = -z[16] + -z[20];
z[29] = -abb[1] + abb[11];
z[27] = z[27] * z[29];
z[29] = abb[31] * z[29];
z[29] = z[29] + z[71];
z[29] = z[29] * z[50];
z[9] = abb[33] * z[9] * z[38];
z[34] = z[36] * z[68];
z[6] = z[6] + -z[9] + z[14] + z[27] + z[29] + z[34];
z[9] = 2 * z[60];
z[6] = z[6] * z[9];
z[9] = -z[62] + z[78] + z[81];
z[9] = abb[28] * z[9];
z[14] = z[50] * z[75];
z[14] = z[14] + -z[73];
z[27] = z[20] * z[52];
z[29] = -z[33] + z[87];
z[22] = z[22] * z[29];
z[33] = -abb[63] * z[79];
z[34] = abb[38] * z[72];
z[9] = z[9] + z[14] + z[22] + z[27] + z[33] + z[34] + z[47];
z[9] = abb[22] * z[9];
z[21] = -z[21] + z[108];
z[22] = -z[21] + -z[54] + -z[56] + z[95] + z[102] + -z[105];
z[22] = abb[28] * z[22];
z[27] = abb[33] * z[103];
z[33] = -z[43] + -z[45] + z[61];
z[33] = abb[31] * z[33];
z[33] = -z[27] + z[33];
z[30] = z[30] + z[105];
z[15] = -z[15] + -z[25] + 2 * z[30];
z[15] = abb[29] * z[15];
z[25] = z[85] + -3 * z[86];
z[25] = z[25] * z[57];
z[15] = z[15] + z[22] + z[25] + 2 * z[33];
z[15] = abb[28] * z[15];
z[8] = -z[8] * z[36];
z[22] = abb[29] * z[83];
z[25] = -abb[41] + abb[61] + -abb[63];
z[25] = z[25] * z[49];
z[7] = -z[7] + -z[88];
z[7] = abb[35] * z[7];
z[29] = abb[30] * z[29];
z[30] = abb[32] * z[89];
z[7] = z[7] + z[22] + z[25] + z[29] + z[30];
z[7] = z[7] * z[90];
z[18] = -z[18] + -z[21] + -z[23] + z[39];
z[18] = z[18] * z[38];
z[18] = z[18] + -z[27];
z[18] = abb[33] * z[18];
z[5] = z[5] + -z[19] + -z[31] + -z[97];
z[5] = 2 * z[5] + z[41] + z[59] + -z[94] + z[111];
z[5] = abb[35] * z[5];
z[19] = -z[32] + z[70];
z[22] = abb[28] + -abb[29];
z[19] = -z[19] * z[22];
z[23] = abb[33] * z[101];
z[25] = -z[84] + z[86];
z[25] = z[25] * z[57];
z[19] = z[19] + z[23] + z[25];
z[5] = z[5] + 2 * z[19];
z[5] = abb[35] * z[5];
z[19] = -z[55] + -z[77];
z[19] = abb[28] * z[19];
z[23] = abb[38] * z[52];
z[23] = z[23] + -z[48];
z[13] = -z[13] + z[14] + z[19] + 2 * z[23];
z[13] = abb[23] * z[13];
z[14] = abb[65] * z[24];
z[0] = z[0] + -z[39];
z[19] = z[0] + z[28] + -z[44];
z[19] = abb[31] * z[19];
z[19] = z[19] + z[42];
z[19] = z[19] * z[50];
z[23] = z[28] + -z[63] + z[98];
z[22] = z[22] * z[23];
z[23] = -abb[22] * z[26];
z[22] = z[22] + z[23] + z[25];
z[23] = abb[34] * z[64];
z[22] = 2 * z[22] + z[23];
z[22] = abb[34] * z[22];
z[0] = -z[0] + z[51];
z[21] = z[0] + -z[21];
z[20] = z[20] * z[21];
z[10] = -z[10] * z[40];
z[0] = z[0] + z[1];
z[0] = z[0] * z[16];
z[0] = z[0] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + 2 * z[12] + z[13] + z[14] + z[15] + z[17] + z[18] + z[19] + z[20] + z[22];
z[0] = 2 * z[0];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_703_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("-5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("-15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("-10.4023028191312333362719401831913221365702768824772793193192573969"),stof<T>("-4.0099607112369939801253103352120106710102959542432765442127783144")}, std::complex<T>{stof<T>("-5.0300761086386151967683965208783601826855876922956941444212530511"),stof<T>("5.049694405849775727946273579000742179074178498672184770263477368")}, std::complex<T>{stof<T>("15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("-1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("18.115715729748419661476374537553446400941967581790848506914123715"),stof<T>("-1.865231470881982908015474912760416863211443942791354825564564092")}, std::complex<T>{stof<T>("-25.514205731730803297405729886997480010359956010634664886931192621"),stof<T>("-36.916552190357586975765762273827768645282794678010635435462489747")}, std::complex<T>{stof<T>("13.2741244426627002273774269247087048151321272454496985707493460998"),stof<T>("-1.7606299963135058907292223372868390240467892650404665348344365107")}, std::complex<T>{stof<T>("-25.514205731730803297405729886997480010359956010634664886931192621"),stof<T>("-36.916552190357586975765762273827768645282794678010635435462489747")}, std::complex<T>{stof<T>("27.266064834752539870826065121313655656450724718606683845191270348"),stof<T>("13.595328248826547361489176099425089692637134646712362756476500542")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_703_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_703_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("15.027648461811750172159449090228423730552511801388229864895411365"),stof<T>("-18.707462405770759069511983873352611993254053234583799179192624201")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,66> abb = {dl[0], dl[1], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W14(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_14(k), f_2_15(k), f_2_18(k), f_2_20(k), f_2_21(k), f_2_29_im(k), f_2_2_im(k), f_2_6_im(k), f_2_9_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_2_re(k), f_2_6_re(k), f_2_9_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_703_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_703_DLogXconstant_part(base_point<T>, kend);
	value += f_4_703_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_703_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_703_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_703_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_703_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_703_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_703_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
