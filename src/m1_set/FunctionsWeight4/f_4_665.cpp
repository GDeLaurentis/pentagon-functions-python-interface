/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_665.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_665_abbreviated (const std::array<T,60>& abb) {
T z[113];
z[0] = abb[33] * (T(1) / T(2));
z[1] = -abb[30] + z[0];
z[2] = abb[35] * (T(1) / T(2));
z[3] = abb[34] + z[1] + -z[2];
z[3] = abb[2] * z[3];
z[4] = -abb[35] + z[0];
z[5] = abb[34] * (T(1) / T(2));
z[6] = z[4] + z[5];
z[7] = abb[17] * z[6];
z[3] = z[3] + (T(-3) / T(4)) * z[7];
z[7] = abb[32] + -abb[35];
z[8] = abb[11] * z[7];
z[8] = -z[3] + z[8];
z[9] = abb[31] + abb[32];
z[10] = abb[13] * (T(1) / T(2));
z[9] = z[9] * z[10];
z[10] = abb[31] + -abb[32];
z[11] = -abb[35] + z[10];
z[12] = abb[34] * (T(5) / T(2)) + -z[11];
z[13] = z[0] + z[12];
z[14] = abb[7] * (T(1) / T(4));
z[13] = z[13] * z[14];
z[14] = z[0] + z[5];
z[15] = -abb[31] + z[14];
z[16] = abb[15] * (T(3) / T(4));
z[15] = z[15] * z[16];
z[17] = abb[5] * (T(1) / T(4));
z[18] = z[7] * z[17];
z[19] = abb[6] * (T(1) / T(4));
z[20] = z[10] * z[19];
z[21] = -abb[31] + z[7];
z[22] = abb[30] + z[21];
z[22] = abb[16] * z[22];
z[23] = abb[12] * abb[31];
z[24] = abb[12] * abb[32];
z[13] = -z[8] + -z[9] + z[13] + -z[15] + -z[18] + -z[20] + z[22] + z[23] + -z[24];
z[13] = m1_set::bc<T>[0] * z[13];
z[15] = abb[15] * (T(3) / T(2));
z[18] = abb[6] * (T(1) / T(2));
z[20] = abb[7] * (T(1) / T(2));
z[22] = -abb[13] + -z[15] + z[18] + z[20];
z[22] = abb[41] * z[22];
z[23] = -abb[2] + abb[17] * (T(1) / T(2));
z[25] = abb[7] * (T(3) / T(2)) + -3 * z[23];
z[26] = abb[5] * (T(1) / T(2));
z[27] = z[25] + -z[26];
z[27] = abb[42] * z[27];
z[28] = abb[19] + abb[21];
z[29] = abb[18] + z[28];
z[30] = abb[20] + z[29];
z[31] = abb[43] * (T(3) / T(4));
z[32] = z[30] * z[31];
z[22] = z[22] + z[27] + -z[32];
z[27] = abb[8] * (T(1) / T(4));
z[32] = abb[31] + abb[35];
z[33] = -abb[33] + -abb[34] + z[32];
z[33] = m1_set::bc<T>[0] * z[33];
z[33] = abb[41] + abb[42] + z[33];
z[34] = z[27] * z[33];
z[13] = -z[13] + (T(1) / T(2)) * z[22] + z[34];
z[22] = abb[50] + abb[52];
z[13] = -z[13] * z[22];
z[34] = abb[49] + abb[51];
z[35] = z[0] * z[34];
z[36] = z[5] * z[34];
z[37] = abb[31] * z[34];
z[38] = z[36] + -z[37];
z[39] = z[35] + z[38];
z[16] = -z[16] * z[39];
z[40] = z[35] + -z[38];
z[41] = abb[32] * z[34];
z[42] = z[40] + -z[41];
z[43] = abb[30] * z[34];
z[44] = (T(-1) / T(2)) * z[42] + z[43];
z[44] = z[20] * z[44];
z[45] = abb[9] * abb[51];
z[46] = abb[9] * abb[49];
z[45] = z[45] + z[46];
z[47] = abb[30] * z[45];
z[48] = -abb[9] + abb[12];
z[48] = abb[51] * z[48];
z[46] = -z[46] + z[48];
z[46] = abb[31] * z[46];
z[9] = -abb[49] * z[9];
z[48] = abb[35] * z[34];
z[49] = -z[41] + z[48];
z[50] = z[17] * z[49];
z[51] = 3 * abb[51];
z[52] = abb[49] + z[51];
z[53] = z[10] * z[52];
z[54] = -z[19] * z[53];
z[55] = abb[32] * abb[51];
z[56] = -abb[12] * z[55];
z[9] = z[9] + z[16] + z[44] + z[46] + z[47] + z[50] + z[54] + z[56];
z[9] = m1_set::bc<T>[0] * z[9];
z[16] = -abb[32] + z[14];
z[16] = abb[26] * z[16];
z[44] = abb[22] + abb[24];
z[50] = z[0] * z[44];
z[54] = z[5] * z[44];
z[56] = abb[22] * abb[31];
z[57] = abb[24] * abb[32];
z[16] = z[16] + z[50] + z[54] + -z[56] + -z[57];
z[16] = m1_set::bc<T>[0] * z[16];
z[56] = abb[23] + abb[25];
z[33] = -z[33] * z[56];
z[58] = m1_set::bc<T>[0] * z[6];
z[58] = -abb[42] + z[58];
z[58] = abb[27] * z[58];
z[59] = abb[22] * abb[41];
z[60] = abb[28] * abb[43];
z[16] = z[16] + z[33] + z[58] + -z[59] + (T(-1) / T(2)) * z[60];
z[33] = abb[45] + abb[46] + abb[48];
z[58] = -abb[56] + abb[47] * (T(-1) / T(4)) + abb[44] * (T(1) / T(2));
z[59] = (T(3) / T(4)) * z[33] + z[58];
z[16] = z[16] * z[59];
z[59] = abb[33] * z[34];
z[60] = abb[34] * z[34];
z[61] = z[37] + z[41] + z[59] + z[60];
z[61] = -z[43] + (T(1) / T(2)) * z[61];
z[61] = m1_set::bc<T>[0] * z[61];
z[32] = abb[32] + z[14] + z[32];
z[32] = m1_set::bc<T>[0] * z[32];
z[32] = -3 * abb[41] + abb[42] + z[32];
z[62] = (T(1) / T(2)) * z[22];
z[32] = z[32] * z[62];
z[6] = (T(1) / T(2)) * z[6];
z[62] = -abb[30] + z[6];
z[63] = -m1_set::bc<T>[0] * z[62];
z[64] = abb[42] * (T(1) / T(2));
z[63] = z[63] + z[64];
z[63] = abb[54] * z[63];
z[65] = (T(3) / T(2)) * z[34];
z[66] = abb[41] * z[65];
z[67] = -abb[55] * z[31];
z[32] = z[32] + z[61] + z[63] + -z[66] + z[67];
z[61] = abb[4] * (T(1) / T(2));
z[32] = z[32] * z[61];
z[63] = abb[34] + z[2];
z[1] = z[1] + z[63];
z[1] = z[1] * z[20];
z[67] = abb[30] + -abb[35];
z[67] = abb[10] * z[67];
z[1] = z[1] + z[67];
z[67] = z[7] * z[26];
z[8] = z[1] + -z[8] + z[67];
z[8] = m1_set::bc<T>[0] * z[8];
z[67] = z[28] * z[31];
z[25] = -abb[5] + -z[25];
z[25] = abb[42] * z[25];
z[25] = z[25] + z[67];
z[67] = -abb[32] + z[0] + -z[2];
z[67] = m1_set::bc<T>[0] * z[67];
z[67] = z[64] + z[67];
z[67] = abb[8] * z[67];
z[68] = abb[34] * (T(1) / T(4));
z[69] = abb[33] * (T(1) / T(4));
z[70] = z[68] + z[69];
z[2] = -abb[32] + z[2];
z[71] = abb[30] + -z[2] + -z[70];
z[71] = m1_set::bc<T>[0] * z[71];
z[71] = abb[42] * (T(3) / T(2)) + z[71];
z[61] = z[61] * z[71];
z[8] = z[8] + (T(1) / T(2)) * z[25] + z[61] + z[67];
z[8] = abb[53] * z[8];
z[25] = -abb[49] + z[51];
z[51] = z[20] * z[25];
z[18] = -z[18] * z[52];
z[61] = abb[15] * z[65];
z[67] = abb[13] * abb[49];
z[18] = z[18] + z[51] + z[61] + z[67];
z[18] = abb[41] * z[18];
z[51] = abb[18] + abb[20];
z[51] = z[34] * z[51];
z[31] = z[31] * z[51];
z[67] = abb[54] + z[22];
z[67] = abb[14] * z[67];
z[71] = -abb[33] + 3 * abb[35];
z[71] = m1_set::bc<T>[0] * z[71];
z[71] = abb[42] + z[71];
z[71] = -z[67] * z[71];
z[72] = (T(1) / T(2)) * z[34];
z[73] = abb[5] * abb[42] * z[72];
z[18] = z[18] + z[31] + z[71] + z[73];
z[31] = (T(1) / T(4)) * z[48] + z[55];
z[70] = z[34] * z[70];
z[71] = (T(1) / T(4)) * z[37];
z[70] = -z[31] + z[70] + -z[71];
z[70] = m1_set::bc<T>[0] * z[70];
z[73] = abb[41] * z[25];
z[74] = -abb[42] * z[34];
z[73] = z[73] + z[74];
z[70] = z[70] + (T(1) / T(4)) * z[73];
z[70] = abb[8] * z[70];
z[1] = z[1] + z[3];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = -z[20] + z[23];
z[3] = abb[42] * z[3];
z[23] = abb[43] * z[28];
z[3] = z[3] + (T(1) / T(4)) * z[23];
z[1] = z[1] + (T(3) / T(2)) * z[3];
z[1] = abb[54] * z[1];
z[3] = abb[21] * z[6];
z[6] = abb[19] * z[62];
z[23] = abb[33] + -abb[35];
z[23] = -abb[30] + (T(1) / T(2)) * z[23];
z[23] = abb[18] * z[23];
z[3] = z[3] + z[6] + z[23];
z[3] = m1_set::bc<T>[0] * z[3];
z[6] = -z[29] * z[64];
z[23] = abb[7] + abb[17] * (T(1) / T(4));
z[29] = -abb[43] * z[23];
z[3] = z[3] + z[6] + z[29];
z[6] = abb[29] * abb[43];
z[3] = (T(1) / T(2)) * z[3] + z[6];
z[3] = abb[55] * z[3];
z[6] = -z[37] + z[60];
z[29] = (T(1) / T(2)) * z[6] + -z[43] + z[59];
z[29] = m1_set::bc<T>[0] * z[29];
z[62] = -abb[31] + abb[34];
z[64] = abb[30] + -abb[33] + (T(-1) / T(2)) * z[62];
z[64] = m1_set::bc<T>[0] * z[64];
z[64] = abb[41] * (T(3) / T(2)) + z[64];
z[64] = -z[22] * z[64];
z[29] = z[29] + z[64] + -z[66];
z[29] = abb[1] * z[29];
z[64] = abb[33] + -abb[34];
z[66] = abb[53] + abb[54];
z[73] = z[64] * z[66];
z[73] = -z[59] + z[60] + z[73];
z[73] = m1_set::bc<T>[0] * z[73];
z[74] = abb[43] * abb[55];
z[73] = z[73] + -3 * z[74];
z[74] = abb[0] * (T(1) / T(8));
z[73] = z[73] * z[74];
z[1] = z[1] + z[3] + z[8] + z[9] + z[13] + z[16] + (T(1) / T(2)) * z[18] + z[29] + z[32] + z[70] + z[73];
z[1] = (T(1) / T(8)) * z[1];
z[3] = abb[31] * (T(1) / T(4));
z[8] = abb[30] * (T(1) / T(2));
z[9] = abb[35] + abb[32] * (T(-1) / T(4)) + z[3] + -z[8] + (T(-3) / T(8)) * z[64];
z[9] = abb[30] * z[9];
z[12] = abb[33] + z[12];
z[12] = z[12] * z[69];
z[13] = 3 * abb[58];
z[16] = prod_pow(abb[35], 2);
z[18] = 3 * abb[36] + (T(5) / T(2)) * z[16];
z[29] = z[13] + z[18];
z[32] = abb[34] * abb[35];
z[29] = (T(1) / T(4)) * z[29] + z[32];
z[70] = abb[38] * (T(1) / T(4));
z[73] = abb[57] * (T(1) / T(4));
z[75] = abb[40] * (T(1) / T(4));
z[9] = z[9] + z[12] + -z[29] + z[70] + -z[73] + -z[75];
z[9] = z[9] * z[20];
z[12] = prod_pow(abb[32], 2);
z[18] = 3 * abb[39] + -z[12] + z[18];
z[76] = z[0] + -z[5];
z[77] = z[21] * z[76];
z[78] = abb[32] + z[3];
z[78] = abb[31] * z[78];
z[18] = (T(1) / T(2)) * z[18] + z[77] + z[78];
z[3] = -abb[32] + abb[35] * (T(-1) / T(4)) + z[3] + -z[68] + z[69];
z[3] = abb[30] * z[3];
z[3] = -abb[40] + abb[38] * (T(3) / T(4)) + z[3] + (T(1) / T(2)) * z[18];
z[18] = abb[16] * (T(1) / T(2));
z[3] = z[3] * z[18];
z[18] = -abb[38] + abb[40];
z[77] = (T(1) / T(2)) * z[12];
z[79] = abb[39] + z[77];
z[80] = -z[18] + z[79];
z[21] = z[8] + z[21];
z[21] = abb[30] * z[21];
z[81] = abb[33] * z[62];
z[82] = abb[58] + z[32];
z[21] = abb[57] + z[21] + -z[80] + -z[81] + z[82];
z[81] = prod_pow(m1_set::bc<T>[0], 2);
z[83] = (T(1) / T(3)) * z[81];
z[84] = (T(1) / T(2)) * z[21] + z[83];
z[27] = z[27] * z[84];
z[63] = z[63] + z[69];
z[63] = z[0] * z[63];
z[4] = z[4] + -z[5];
z[85] = abb[30] * (T(1) / T(4));
z[86] = z[4] + z[85];
z[86] = abb[30] * z[86];
z[63] = z[63] + -z[86];
z[86] = abb[35] + z[68];
z[86] = abb[34] * z[86];
z[87] = (T(1) / T(2)) * z[16];
z[88] = abb[36] + z[87];
z[89] = abb[58] + z[88];
z[86] = -z[63] + z[86] + (T(3) / T(4)) * z[89];
z[86] = abb[2] * z[86];
z[89] = z[4] + z[8];
z[89] = abb[30] * z[89];
z[89] = abb[36] + z[89];
z[90] = abb[33] * z[5];
z[82] = z[82] + z[89] + -z[90];
z[91] = abb[17] * z[82];
z[86] = z[86] + (T(-3) / T(8)) * z[91];
z[91] = -z[12] + z[16];
z[92] = abb[11] * z[91];
z[92] = z[86] + (T(1) / T(4)) * z[92];
z[93] = abb[7] + abb[15];
z[94] = abb[13] * (T(7) / T(3));
z[95] = -abb[17] + abb[2] * (T(7) / T(3));
z[96] = z[93] + -z[94] + -z[95];
z[97] = (T(1) / T(8)) * z[81];
z[96] = z[96] * z[97];
z[98] = -abb[31] + z[8];
z[99] = z[76] + -z[98];
z[99] = abb[30] * z[99];
z[100] = -abb[31] + z[5];
z[101] = abb[33] * z[100];
z[99] = -abb[57] + z[99] + z[101];
z[101] = abb[15] * (T(3) / T(8));
z[99] = z[99] * z[101];
z[101] = abb[31] * (T(1) / T(2));
z[102] = abb[12] * z[101];
z[24] = z[24] + z[102];
z[24] = abb[31] * z[24];
z[102] = abb[38] + (T(3) / T(2)) * z[12];
z[102] = abb[12] * z[102];
z[24] = z[24] + -z[102];
z[103] = prod_pow(abb[31], 2);
z[104] = -z[12] + z[103];
z[104] = (T(1) / T(2)) * z[104];
z[105] = -abb[38] + -abb[57] + z[104];
z[106] = abb[34] * z[10];
z[106] = -z[105] + z[106];
z[107] = abb[6] * (T(1) / T(8));
z[106] = z[106] * z[107];
z[62] = abb[32] + z[62];
z[62] = abb[34] * z[62];
z[107] = abb[31] * abb[32];
z[62] = z[62] + -z[107];
z[108] = -abb[57] + z[62];
z[109] = abb[13] * (T(1) / T(4));
z[108] = z[108] * z[109];
z[109] = abb[15] + abb[16];
z[110] = abb[37] * (T(3) / T(8));
z[109] = z[109] * z[110];
z[110] = z[79] + -z[87];
z[111] = abb[33] * z[7];
z[111] = -abb[58] + -z[110] + z[111];
z[112] = abb[5] * z[111];
z[3] = -z[3] + -z[9] + (T(1) / T(4)) * z[24] + z[27] + z[92] + -z[96] + z[99] + z[106] + z[108] + -z[109] + (T(1) / T(8)) * z[112];
z[3] = z[3] * z[22];
z[9] = z[8] * z[34];
z[24] = -z[9] + z[40];
z[24] = abb[30] * z[24];
z[27] = abb[33] * z[38];
z[24] = z[24] + z[27];
z[15] = z[15] * z[24];
z[24] = -abb[58] + -z[79] + z[87];
z[24] = z[24] * z[34];
z[79] = -abb[33] * z[49];
z[24] = z[24] + z[79];
z[24] = z[24] * z[26];
z[26] = abb[16] * z[72];
z[26] = z[26] + -z[45] + -z[61];
z[26] = abb[37] * z[26];
z[61] = -abb[39] + abb[40];
z[45] = z[45] * z[61];
z[79] = abb[32] + z[101];
z[46] = z[46] * z[79];
z[79] = abb[32] + z[8];
z[47] = z[47] * z[79];
z[10] = abb[34] + -z[10];
z[10] = abb[34] * z[10];
z[10] = -abb[57] + z[10] + -z[107];
z[10] = abb[13] * abb[49] * z[10];
z[79] = -abb[51] * z[102];
z[96] = abb[57] * z[65];
z[99] = -abb[15] * z[96];
z[10] = z[10] + z[15] + z[24] + z[26] + z[45] + z[46] + z[47] + z[79] + z[99];
z[15] = abb[31] + abb[32] * (T(1) / T(2));
z[24] = -z[5] + z[15] + z[69];
z[24] = abb[33] * z[24];
z[15] = z[15] + z[76] + -z[85];
z[15] = abb[30] * z[15];
z[26] = -abb[38] + z[62];
z[45] = abb[37] + abb[57];
z[45] = abb[40] * (T(-1) / T(2)) + (T(3) / T(2)) * z[45];
z[15] = z[15] + -z[24] + (T(1) / T(2)) * z[26] + -z[45] + (T(-5) / T(12)) * z[81];
z[15] = -z[15] * z[22];
z[24] = z[34] * z[85];
z[26] = (T(1) / T(2)) * z[41];
z[24] = z[24] + -z[26] + -z[40];
z[24] = abb[30] * z[24];
z[40] = abb[37] * z[65];
z[24] = z[24] + z[40] + z[96];
z[40] = z[34] * z[69];
z[26] = z[26] + -z[38] + z[40];
z[26] = abb[33] * z[26];
z[18] = -z[18] * z[72];
z[38] = z[34] * z[81];
z[40] = z[6] + z[41];
z[46] = -abb[34] * z[40];
z[47] = abb[32] * z[37];
z[46] = z[46] + z[47];
z[15] = z[15] + z[18] + z[24] + z[26] + (T(5) / T(12)) * z[38] + (T(1) / T(2)) * z[46];
z[15] = abb[1] * z[15];
z[18] = abb[49] * z[87];
z[26] = abb[51] * z[88];
z[46] = abb[36] * abb[49];
z[18] = z[18] + z[26] + z[46];
z[26] = z[6] + z[48] + -z[59];
z[26] = abb[30] * z[26];
z[47] = -abb[38] + abb[39];
z[47] = z[34] * z[47];
z[37] = z[37] + z[49];
z[49] = z[37] * z[64];
z[59] = z[72] * z[103];
z[26] = -z[18] + z[26] + z[47] + z[49] + z[59];
z[26] = abb[16] * z[26];
z[47] = -z[34] * z[93];
z[49] = abb[49] * z[94];
z[47] = z[47] + z[49];
z[47] = z[47] * z[81];
z[26] = z[26] + z[47];
z[47] = z[52] * z[70];
z[49] = z[34] * z[75];
z[25] = z[25] * z[73];
z[25] = z[25] + z[47] + -z[49];
z[39] = -z[39] + -z[41];
z[39] = z[39] * z[69];
z[42] = -z[42] + z[43];
z[42] = z[42] * z[85];
z[43] = abb[51] * z[101];
z[43] = z[43] + -z[55];
z[43] = abb[31] * z[43];
z[47] = abb[51] * z[77];
z[47] = z[43] + z[47];
z[49] = -abb[31] * abb[51];
z[49] = z[49] + z[55];
z[49] = abb[34] * z[49];
z[39] = -z[25] + z[39] + z[42] + (T(1) / T(2)) * z[47] + z[49];
z[39] = abb[7] * z[39];
z[42] = abb[39] + -abb[58];
z[47] = -z[34] * z[42];
z[49] = -z[52] * z[77];
z[47] = z[47] + z[49];
z[49] = abb[49] * (T(1) / T(3)) + abb[51] * (T(3) / T(2));
z[49] = z[49] * z[81];
z[43] = z[43] + (T(1) / T(2)) * z[47] + z[49];
z[37] = z[9] + -z[37];
z[37] = z[37] * z[85];
z[47] = abb[51] * z[100];
z[31] = z[31] + z[47];
z[31] = abb[34] * z[31];
z[6] = -z[6] * z[69];
z[6] = z[6] + -z[25] + z[31] + z[37] + (T(1) / T(2)) * z[43];
z[6] = abb[8] * z[6];
z[25] = abb[21] * z[82];
z[0] = abb[35] + z[0];
z[0] = abb[33] * z[0];
z[0] = abb[58] + z[0];
z[31] = prod_pow(abb[30], 2);
z[37] = z[0] + -z[31] + -z[88];
z[37] = abb[18] * z[37];
z[4] = z[4] + -z[8];
z[43] = abb[30] * z[4];
z[47] = abb[34] + abb[35];
z[49] = abb[34] * z[47];
z[43] = -abb[58] + -z[43] + -z[49] + z[90];
z[55] = -abb[19] * z[43];
z[25] = z[25] + z[37] + z[55];
z[37] = abb[18] + abb[21] * (T(1) / T(4));
z[37] = abb[19] * (T(3) / T(8)) + (T(1) / T(3)) * z[37];
z[37] = z[37] * z[81];
z[23] = -abb[29] + (T(1) / T(2)) * z[23];
z[23] = abb[59] * z[23];
z[23] = z[23] + (T(1) / T(4)) * z[25] + z[37];
z[23] = abb[55] * z[23];
z[25] = -z[52] * z[105];
z[37] = abb[34] * z[53];
z[25] = z[25] + z[37];
z[19] = z[19] * z[25];
z[6] = z[6] + (T(1) / T(2)) * z[10] + z[15] + z[19] + z[23] + (T(1) / T(4)) * z[26] + z[39];
z[10] = (T(1) / T(2)) * z[7] + (T(3) / T(4)) * z[64] + -z[98];
z[10] = abb[30] * z[10];
z[11] = -abb[34] + (T(1) / T(2)) * z[11];
z[11] = abb[34] * z[11];
z[15] = -abb[31] + z[68];
z[15] = abb[33] * z[15];
z[10] = -z[10] + z[11] + -z[15] + (T(1) / T(2)) * z[42] + z[45] + z[78] + -z[81];
z[10] = z[10] * z[22];
z[11] = z[43] + (T(-3) / T(2)) * z[81];
z[11] = abb[54] * z[11];
z[15] = -z[61] * z[72];
z[19] = z[41] + z[71];
z[19] = abb[31] * z[19];
z[23] = -z[5] * z[40];
z[25] = abb[55] * abb[59];
z[10] = z[10] + (T(1) / T(2)) * z[11] + z[15] + z[19] + z[23] + z[24] + (T(3) / T(4)) * z[25] + -z[27] + (T(-1) / T(4)) * z[38];
z[11] = abb[4] * (T(1) / T(4));
z[10] = z[10] * z[11];
z[15] = -z[5] + z[7];
z[15] = abb[33] * z[15];
z[15] = z[15] + z[32] + z[89] + -z[110];
z[15] = abb[26] * z[15];
z[19] = z[8] * z[44];
z[23] = abb[31] * z[44];
z[23] = z[23] + -z[54];
z[19] = z[19] + -z[23] + -z[50];
z[19] = abb[30] * z[19];
z[24] = -abb[38] + z[104];
z[24] = abb[24] * z[24];
z[26] = abb[24] * abb[31];
z[26] = z[26] + -z[57];
z[26] = abb[34] * z[26];
z[23] = abb[33] * z[23];
z[27] = abb[22] * abb[57];
z[32] = abb[37] * z[44];
z[15] = z[15] + z[19] + z[23] + z[24] + -z[26] + z[27] + z[32];
z[19] = z[82] + z[83];
z[19] = abb[27] * z[19];
z[23] = abb[26] + z[44];
z[24] = z[23] * z[83];
z[19] = z[15] + z[19] + z[24];
z[24] = z[56] * z[84];
z[26] = abb[28] * abb[59];
z[19] = (T(1) / T(2)) * z[19] + z[24] + (T(1) / T(4)) * z[26];
z[19] = z[19] * z[58];
z[24] = z[81] + 3 * z[82];
z[24] = abb[27] * z[24];
z[23] = z[23] * z[81];
z[15] = 3 * z[15] + z[23] + z[24];
z[21] = (T(3) / T(2)) * z[21] + z[81];
z[21] = z[21] * z[56];
z[15] = (T(1) / T(2)) * z[15] + z[21] + (T(3) / T(4)) * z[26];
z[21] = (T(1) / T(4)) * z[33];
z[15] = z[15] * z[21];
z[13] = -z[13] + -z[91];
z[5] = -z[5] * z[47];
z[7] = z[7] + z[68];
z[7] = abb[33] * z[7];
z[4] = -z[4] * z[8];
z[4] = -abb[39] + z[4] + z[5] + z[7] + (T(1) / T(2)) * z[13] + (T(-3) / T(4)) * z[81];
z[4] = z[4] * z[11];
z[5] = z[29] + -z[63];
z[5] = z[5] * z[20];
z[7] = abb[36] + -z[16] + z[31];
z[11] = abb[10] * (T(1) / T(4));
z[7] = z[7] * z[11];
z[11] = z[95] * z[97];
z[5] = z[5] + z[7] + z[11];
z[7] = -abb[58] + -z[12] + -z[87];
z[2] = -z[2] + z[69];
z[2] = abb[33] * z[2];
z[2] = -abb[39] + z[2] + (T(1) / T(2)) * z[7];
z[2] = (T(1) / T(2)) * z[2] + z[83];
z[2] = abb[8] * z[2];
z[7] = -z[17] * z[111];
z[11] = abb[59] * (T(3) / T(16));
z[12] = -z[11] * z[28];
z[2] = z[2] + z[4] + z[5] + z[7] + z[12] + z[92];
z[2] = abb[53] * z[2];
z[4] = abb[35] + z[14];
z[4] = abb[33] * z[4];
z[4] = -z[4] + z[49] + (T(1) / T(6)) * z[81] + z[87] + z[89];
z[4] = -z[4] * z[66];
z[7] = z[36] + z[48];
z[12] = -z[7] + z[9] + z[35];
z[12] = abb[30] * z[12];
z[7] = -z[7] + -z[35];
z[7] = abb[33] * z[7];
z[13] = z[48] + z[60];
z[13] = abb[34] * z[13];
z[4] = z[4] + z[7] + z[12] + z[13] + z[18] + (T(3) / T(2)) * z[25] + (T(1) / T(6)) * z[38];
z[4] = z[4] * z[74];
z[5] = z[5] + z[86];
z[5] = abb[54] * z[5];
z[7] = abb[37] + z[80];
z[8] = -abb[32] + z[8];
z[8] = abb[30] * z[8];
z[8] = abb[36] + z[7] + z[8];
z[12] = 3 * z[22];
z[8] = z[8] * z[12];
z[7] = z[7] * z[34];
z[9] = z[9] + -z[41];
z[9] = abb[30] * z[9];
z[12] = abb[36] * abb[51];
z[7] = z[7] + z[8] + z[9] + z[12] + z[46];
z[7] = abb[3] * z[7];
z[8] = -z[22] * z[30];
z[9] = -abb[54] * z[28];
z[8] = z[8] + z[9] + -z[51];
z[8] = z[8] * z[11];
z[0] = -z[0] + (T(3) / T(2)) * z[16];
z[0] = (T(1) / T(4)) * z[0] + -z[83];
z[0] = -z[0] * z[67];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + (T(1) / T(2)) * z[6] + (T(1) / T(8)) * z[7] + z[8] + z[10] + z[15] + z[19];
z[0] = (T(1) / T(4)) * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_665_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.078594939197478362449506195638724377854462307692120221006582078923"),stof<T>("0.078901475091402745749160524671886596548034039041752887035366833875")}, std::complex<T>{stof<T>("-0.11789240879621754367425929345808656678169346153818033150987311839"),stof<T>("0.11835221263710411862374078700782989482205105856262933055305025081")}, std::complex<T>{stof<T>("-0.11789240879621754367425929345808656678169346153818033150987311839"),stof<T>("0.11835221263710411862374078700782989482205105856262933055305025081")}, std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.11789240879621754367425929345808656678169346153818033150987311839"),stof<T>("0.11835221263710411862374078700782989482205105856262933055305025081")}, std::complex<T>{stof<T>("0.54348667944923372674935716060504520432263134639050401254959625346"),stof<T>("-0.25190994628464586231041451186051100331117594841384468262501039936")}, std::complex<T>{stof<T>("0.92953867732467126680803459708106683801492331367908990311706460344"),stof<T>("-0.30653772032192214364009841618265780858707552115937444600237712513")}, std::complex<T>{stof<T>("0.69675268102484726328223048609337923983728074942351396860162611353"),stof<T>("0.02578404033320494893163992498479501206326103797988926270961755216")}, std::complex<T>{stof<T>("0.92953867732467126680803459708106683801492331367908990311706460344"),stof<T>("-0.30653772032192214364009841618265780858707552115937444600237712513")}, std::complex<T>{stof<T>("0.71008295836850959890599098395228339258126451577652691693060647737"),stof<T>("0.02578404033320494893163992498479501206326103797988926270961755216")}, std::complex<T>{stof<T>("0.52312809939159841497487518109363381152894127604243967373093441427"),stof<T>("-0.31244576478475254301903719950951518573127637349040735905052986715")}, std::complex<T>{stof<T>("0.3440041854750969875943185853218086014763060625783884745339469414"),stof<T>("-0.50131549372286322696668907466859795704656012298773609643632390436")}, std::complex<T>{stof<T>("0.15718987839495672489901239127744875570892461538424044201316415785"),stof<T>("-0.15780295018280549149832104934377319309606807808350577407073366775")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}, rlog(k.W[197].imag()/kbase.W[197].imag())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_665_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_665_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.42002044008581330307653894483807892152708057766629896299684017463"),stof<T>("0.21065982564672047995608167662649569857313529072453934704168884147")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,60> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W26(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_3(k), f_1_6(k), f_1_8(k), f_1_9(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_9_im(k), f_2_11_im(k), f_2_25_im(k), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), rlog(kend.W[197].imag()/k.W[197].imag()), f_2_9_re(k), f_2_11_re(k), f_2_25_re(k)};

                    
            return f_4_665_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_665_DLogXconstant_part(base_point<T>, kend);
	value += f_4_665_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_665_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_665_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_665_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_665_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_665_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_665_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
