/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_156.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_156_abbreviated (const std::array<T,18>& abb) {
T z[13];
z[0] = -abb[11] + abb[13];
z[1] = abb[1] * z[0];
z[2] = abb[1] * abb[12];
z[1] = z[1] + -z[2];
z[3] = abb[2] * abb[14];
z[4] = -abb[2] * abb[11];
z[5] = -abb[12] + z[0];
z[6] = -abb[4] * z[5];
z[4] = z[1] + z[3] + z[4] + z[6];
z[4] = prod_pow(abb[8], 2) * z[4];
z[6] = -abb[2] + abb[3];
z[6] = z[5] * z[6];
z[6] = -z[1] + z[6];
z[7] = abb[8] * z[6];
z[8] = abb[2] + -abb[5];
z[8] = z[5] * z[8];
z[9] = abb[1] * abb[15];
z[8] = -z[2] + z[8] + z[9];
z[8] = abb[6] * z[8];
z[7] = -2 * z[7] + z[8];
z[7] = abb[6] * z[7];
z[8] = abb[4] + abb[5];
z[9] = abb[2] + -2 * abb[3] + z[8];
z[9] = z[5] * z[9];
z[10] = -abb[11] + -abb[12] + abb[14] + abb[15];
z[10] = abb[0] * z[10];
z[1] = z[1] + z[9] + -z[10];
z[1] = abb[7] * z[1];
z[9] = abb[6] + abb[8];
z[9] = z[6] * z[9];
z[11] = z[1] + 2 * z[9];
z[11] = abb[7] * z[11];
z[12] = abb[16] + abb[17];
z[12] = z[6] * z[12];
z[4] = z[4] + z[7] + z[11] + 2 * z[12];
z[7] = abb[3] * (T(10) / T(3)) + -2 * z[8];
z[5] = z[5] * z[7];
z[0] = abb[15] * (T(-1) / T(6)) + (T(-4) / T(3)) * z[0];
z[0] = abb[1] * z[0];
z[7] = abb[12] + -abb[13];
z[7] = abb[11] * (T(3) / T(2)) + (T(4) / T(3)) * z[7];
z[7] = abb[2] * z[7];
z[0] = z[0] + (T(3) / T(2)) * z[2] + (T(-1) / T(6)) * z[3] + z[5] + z[7] + (T(13) / T(6)) * z[10];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[0] = z[0] + 2 * z[4];
z[1] = -z[1] + -z[9];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[9] + abb[10];
z[2] = z[2] * z[6];
z[1] = z[1] + z[2];
z[1] = 4 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_156_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-10.9531614472425862227659634662181813967968495116738876066170398592"),stof<T>("7.8953968863693194663020555957490944755296950942878745090714710535")}, std::complex<T>{stof<T>("-10.9531614472425862227659634662181813967968495116738876066170398592"),stof<T>("7.8953968863693194663020555957490944755296950942878745090714710535")}, std::complex<T>{stof<T>("-10.737576408558770473641840881870960315608550376560386708593528935"),stof<T>("10.359683036787245104306206832630965156928669959890948944011423784")}, std::complex<T>{stof<T>("21.690737855801356696407804348089141712405399888234274315210568794"),stof<T>("-18.255079923156564570608262428380059632458365054178823453082894837")}, std::complex<T>{stof<T>("21.690737855801356696407804348089141712405399888234274315210568794"),stof<T>("-18.255079923156564570608262428380059632458365054178823453082894837")}};
	
	std::vector<C> intdlogs = {rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[23].real()/kbase.W[23].real()), rlog(k.W[26].real()/kbase.W[26].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_156_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_156_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-3.96430171794920451849226027710769062442440299168921510695504179"),stof<T>("-26.463593216838405057907652777780745465302252457457240124129820679")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,18> abb = {dl[1], dl[3], dlog_W10(k,dl), dl[4], dlog_W24(k,dl), dlog_W27(k,dl), f_1_2(k), f_1_3(k), f_1_9(k), f_2_6_im(k), f_2_9_im(k), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[23].real()/k.W[23].real()), rlog(kend.W[26].real()/k.W[26].real()), f_2_6_re(k), f_2_9_re(k)};

                    
            return f_4_156_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_156_DLogXconstant_part(base_point<T>, kend);
	value += f_4_156_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_156_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_156_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_156_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_156_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_156_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_156_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
