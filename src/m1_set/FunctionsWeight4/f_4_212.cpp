/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_212.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_212_abbreviated (const std::array<T,13>& abb) {
T z[9];
z[0] = abb[3] + abb[4];
z[1] = -abb[9] + abb[10];
z[0] = z[0] * z[1];
z[2] = abb[2] * abb[11];
z[3] = abb[1] * abb[11];
z[0] = z[0] + -z[2] + z[3];
z[4] = -prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[5] = abb[4] * z[1];
z[3] = z[3] + z[5];
z[5] = abb[0] * abb[11];
z[6] = -z[3] + z[5];
z[7] = -abb[6] * z[6];
z[8] = abb[3] * z[1];
z[2] = -z[2] + z[8];
z[8] = z[2] + z[5];
z[8] = abb[7] * z[8];
z[7] = z[7] + z[8];
z[7] = abb[7] * z[7];
z[8] = abb[0] + -abb[1];
z[1] = z[1] * z[8];
z[8] = abb[4] * abb[11];
z[1] = z[1] + 3 * z[8];
z[8] = -abb[12] * z[1];
z[4] = z[4] + z[7] + z[8];
z[3] = z[3] + z[5];
z[2] = z[2] + (T(1) / T(2)) * z[3];
z[2] = abb[7] * z[2];
z[3] = abb[6] * (T(1) / T(2));
z[3] = z[3] * z[6];
z[2] = z[2] + -z[3];
z[0] = abb[5] * z[0];
z[3] = (T(1) / T(2)) * z[0] + -z[2];
z[3] = abb[5] * z[3];
z[3] = z[3] + (T(1) / T(2)) * z[4];
z[3] = (T(1) / T(2)) * z[3];
z[0] = -z[0] + z[2];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[8] * z[1];
z[0] = z[0] + (T(-1) / T(2)) * z[1];
z[0] = (T(1) / T(2)) * z[0];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_212_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("2.7671973636098995237567362518279407270025429820744789682872519761"),stof<T>("-3.7158971342153572229102180761634059483358564420312574678767473729")}, std::complex<T>{stof<T>("-2.7671973636098995237567362518279407270025429820744789682872519761"),stof<T>("3.7158971342153572229102180761634059483358564420312574678767473729")}, std::complex<T>{stof<T>("-1.3427569402766636491166813735746629073271065237564429612567340896"),stof<T>("-1.1320407441903722082871918205716230148170823948741651600313691976")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[120].real()/kbase.W[120].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_212_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_212_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-5.184374106222332022479966083557930643957563315325385271759507566"),stof<T>("-16.618826267077393011886502445089892534249398654044316724961224376")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,13> abb = {dl[0], dlog_W8(k,dl), dl[3], dlog_W120(k,dv), dlog_W121(k,dv), f_1_1(k), f_1_2(k), f_1_7(k), f_2_24_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[120].real()/k.W[120].real()), f_2_24_re(k)};

                    
            return f_4_212_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_212_DLogXconstant_part(base_point<T>, kend);
	value += f_4_212_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_212_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_212_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_212_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_212_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_212_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_212_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
