/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_119.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_119_abbreviated (const std::array<T,55>& abb) {
T z[74];
z[0] = abb[47] + abb[49];
z[1] = abb[48] + z[0];
z[1] = abb[14] * z[1];
z[2] = abb[48] * (T(1) / T(2));
z[3] = abb[5] * z[2];
z[4] = z[1] + -z[3];
z[5] = abb[7] * abb[48];
z[6] = abb[6] * abb[48];
z[7] = -z[5] + -z[6];
z[8] = abb[23] + abb[25];
z[9] = abb[45] * (T(1) / T(2));
z[10] = z[8] * z[9];
z[11] = abb[4] * z[2];
z[12] = -abb[8] * z[0];
z[7] = z[4] + (T(1) / T(2)) * z[7] + z[10] + z[11] + z[12];
z[10] = abb[32] * (T(1) / T(2));
z[7] = z[7] * z[10];
z[12] = z[0] + z[2];
z[12] = abb[8] * z[12];
z[13] = abb[15] * abb[48];
z[14] = z[6] + -z[13];
z[5] = z[5] + z[14];
z[15] = abb[23] + abb[24];
z[16] = abb[25] + z[15];
z[17] = z[9] * z[16];
z[5] = -z[4] + (T(1) / T(2)) * z[5] + z[12] + -z[17];
z[12] = abb[31] * (T(1) / T(2));
z[17] = z[5] * z[12];
z[18] = abb[6] * abb[47];
z[19] = abb[46] + abb[49];
z[20] = abb[48] + z[19];
z[21] = -abb[50] + z[20];
z[22] = abb[6] * z[21];
z[23] = z[18] + z[22];
z[24] = abb[18] * abb[51];
z[24] = z[23] + z[24];
z[25] = -abb[50] + z[19];
z[26] = abb[47] + abb[48];
z[27] = z[25] + z[26];
z[28] = abb[17] * z[27];
z[29] = z[24] + z[28];
z[30] = abb[5] * z[27];
z[31] = z[29] + z[30];
z[32] = z[13] + z[31];
z[33] = abb[19] + abb[21];
z[34] = abb[51] * (T(1) / T(2));
z[35] = z[33] * z[34];
z[36] = abb[22] + abb[25];
z[37] = -abb[26] + z[36];
z[38] = abb[23] + z[37];
z[38] = abb[24] + (T(1) / T(2)) * z[38];
z[39] = abb[45] * z[38];
z[40] = abb[8] * abb[48];
z[32] = (T(1) / T(2)) * z[32] + z[35] + z[39] + -z[40];
z[41] = abb[13] * z[27];
z[32] = (T(1) / T(2)) * z[32] + -z[41];
z[42] = abb[30] * (T(1) / T(2));
z[43] = abb[33] * (T(1) / T(2));
z[44] = z[42] + z[43];
z[44] = z[32] * z[44];
z[45] = abb[19] * abb[51];
z[46] = abb[21] * abb[51];
z[40] = z[40] + -z[45] + -z[46];
z[47] = z[15] + z[37];
z[48] = abb[45] * z[47];
z[31] = -z[31] + z[40] + -z[48];
z[48] = abb[48] * (T(1) / T(4));
z[49] = abb[4] * z[48];
z[49] = -z[41] + z[49];
z[31] = (T(1) / T(4)) * z[31] + -z[49];
z[50] = abb[34] * z[31];
z[7] = z[7] + z[17] + z[44] + z[50];
z[7] = m1_set::bc<T>[0] * z[7];
z[17] = abb[40] * z[5];
z[44] = abb[5] + abb[6] + abb[17];
z[50] = abb[51] * (T(1) / T(4));
z[44] = z[44] * z[50];
z[50] = abb[28] * abb[51];
z[48] = abb[20] * z[48];
z[51] = abb[45] * (T(1) / T(4));
z[52] = abb[27] * z[51];
z[44] = -z[44] + -z[48] + z[50] + z[52];
z[33] = abb[18] + z[33];
z[33] = (T(1) / T(4)) * z[33];
z[27] = -z[27] * z[33];
z[27] = z[27] + z[44];
z[27] = abb[42] * z[27];
z[17] = z[17] + z[27];
z[27] = abb[34] * z[47];
z[48] = abb[33] * z[38];
z[8] = abb[32] * z[8];
z[50] = abb[30] * z[38];
z[8] = z[8] + -z[27] + z[48] + z[50];
z[8] = m1_set::bc<T>[0] * z[8];
z[27] = abb[31] * m1_set::bc<T>[0];
z[27] = abb[40] + z[27];
z[27] = z[16] * z[27];
z[48] = abb[41] * z[47];
z[52] = abb[27] * (T(1) / T(2));
z[53] = abb[42] * z[52];
z[8] = z[8] + -z[27] + -z[48] + z[53];
z[27] = abb[43] + abb[44];
z[48] = (T(-1) / T(4)) * z[27];
z[8] = z[8] * z[48];
z[48] = abb[41] * z[31];
z[7] = z[7] + z[8] + (T(1) / T(2)) * z[17] + z[48];
z[7] = (T(1) / T(8)) * z[7];
z[8] = abb[47] + abb[48] * (T(3) / T(2));
z[17] = z[8] + z[19];
z[17] = abb[16] * z[17];
z[18] = -z[18] + -3 * z[22];
z[48] = abb[23] * z[9];
z[53] = -abb[5] + -abb[8];
z[53] = abb[47] * z[53];
z[54] = abb[47] + abb[50];
z[54] = abb[10] * z[54];
z[25] = -abb[47] + z[25];
z[55] = abb[0] * z[25];
z[56] = (T(1) / T(2)) * z[55];
z[34] = abb[18] * z[34];
z[18] = -z[17] + (T(-1) / T(2)) * z[18] + z[34] + z[48] + -z[53] + z[54] + -z[56];
z[18] = (T(-1) / T(2)) * z[18] + -z[49];
z[49] = prod_pow(abb[34], 2);
z[18] = z[18] * z[49];
z[53] = abb[16] * abb[48];
z[57] = z[53] + z[55];
z[24] = -z[24] + z[57];
z[58] = abb[3] * abb[48];
z[59] = abb[23] + abb[26];
z[60] = -abb[45] * z[59];
z[60] = z[24] + z[28] + z[46] + -z[58] + z[60];
z[60] = abb[35] * z[60];
z[61] = -z[6] + z[58];
z[62] = z[15] + z[36];
z[63] = z[9] * z[62];
z[64] = abb[9] * z[19];
z[65] = abb[1] * z[26];
z[66] = z[64] + z[65];
z[67] = abb[47] + z[2];
z[67] = abb[8] * z[67];
z[3] = -z[3] + (T(-1) / T(2)) * z[61] + -z[63] + -z[66] + z[67];
z[61] = -abb[39] * z[3];
z[63] = abb[24] + z[37];
z[68] = abb[45] * z[63];
z[40] = z[28] + z[30] + -z[40] + z[57] + z[68];
z[40] = (T(1) / T(2)) * z[40] + -z[41];
z[40] = abb[34] * z[40];
z[68] = abb[2] * z[21];
z[69] = abb[34] * z[68];
z[40] = z[40] + -z[69];
z[40] = abb[30] * z[40];
z[27] = (T(1) / T(2)) * z[27];
z[16] = -z[16] * z[27];
z[5] = -z[5] + z[16];
z[5] = abb[52] * z[5];
z[16] = abb[47] + z[21];
z[16] = z[16] * z[33];
z[16] = z[16] + -z[44];
z[16] = abb[54] * z[16];
z[33] = abb[16] * z[2];
z[33] = z[33] + -z[65];
z[44] = z[33] + z[67];
z[70] = abb[3] + abb[7];
z[71] = -abb[48] * z[70];
z[6] = z[6] + z[71];
z[71] = abb[22] + z[15];
z[72] = -z[9] * z[71];
z[6] = (T(1) / T(2)) * z[6] + z[44] + z[72];
z[6] = abb[37] * z[6];
z[32] = -abb[30] * z[32];
z[72] = abb[23] * abb[45];
z[24] = -z[24] + z[72];
z[24] = z[11] + (T(1) / T(2)) * z[24] + -z[41];
z[24] = abb[34] * z[24];
z[24] = z[24] + z[32] + z[69];
z[24] = abb[33] * z[24];
z[32] = abb[5] * abb[48];
z[32] = z[32] + z[53] + z[58];
z[53] = z[13] + -z[32];
z[58] = -z[9] * z[36];
z[53] = (T(1) / T(2)) * z[53] + z[58] + -z[64];
z[53] = abb[36] * z[53];
z[58] = (T(1) / T(2)) * z[49];
z[72] = prod_pow(abb[32], 2);
z[73] = -abb[32] + abb[34];
z[73] = abb[29] * z[73];
z[72] = abb[36] + -abb[39] + -z[58] + (T(1) / T(2)) * z[72] + z[73];
z[72] = abb[11] * z[20] * z[72];
z[49] = -abb[35] + -z[49];
z[49] = z[49] * z[68];
z[5] = z[5] + z[6] + z[16] + z[18] + z[24] + z[40] + z[49] + z[53] + (T(1) / T(2)) * z[60] + z[61] + z[72];
z[6] = -abb[30] + abb[33];
z[16] = -abb[23] + z[37];
z[18] = (T(1) / T(2)) * z[16];
z[6] = z[6] * z[18];
z[18] = abb[32] * z[62];
z[24] = abb[24] + -abb[26];
z[37] = abb[29] * (T(1) / T(2));
z[40] = z[24] * z[37];
z[49] = abb[34] * z[63];
z[6] = z[6] + z[18] + z[40] + -z[49];
z[6] = abb[29] * z[6];
z[18] = abb[22] + abb[24];
z[40] = z[10] * z[18];
z[53] = abb[23] * abb[33];
z[60] = abb[25] * abb[30];
z[40] = z[40] + z[53] + z[60];
z[40] = abb[32] * z[40];
z[53] = abb[23] * abb[34];
z[50] = -z[50] + z[53];
z[50] = abb[33] * z[50];
z[47] = abb[53] * z[47];
z[53] = abb[39] * z[62];
z[61] = abb[36] * z[36];
z[49] = abb[30] * z[49];
z[52] = abb[54] * z[52];
z[58] = abb[23] * z[58];
z[62] = abb[37] * z[71];
z[59] = abb[35] * z[59];
z[6] = z[6] + -z[40] + z[47] + z[49] + z[50] + -z[52] + z[53] + -z[58] + -z[59] + -z[61] + -z[62];
z[40] = abb[29] + -abb[33];
z[47] = z[15] * z[40];
z[49] = abb[25] * z[12];
z[47] = z[47] + z[49] + -z[60];
z[47] = z[12] * z[47];
z[49] = prod_pow(m1_set::bc<T>[0], 2);
z[38] = z[38] * z[49];
z[50] = abb[38] * (T(1) / T(2));
z[36] = abb[24] + z[36];
z[52] = z[36] * z[50];
z[6] = (T(1) / T(2)) * z[6] + (T(1) / T(3)) * z[38] + -z[47] + -z[52];
z[6] = -z[6] * z[27];
z[27] = (T(1) / T(2)) * z[28] + z[35];
z[22] = -z[22] + z[27] + -z[54] + z[56] + -z[67];
z[28] = -abb[3] + abb[15];
z[28] = z[2] * z[28];
z[19] = z[19] + z[26];
z[19] = abb[16] * z[19];
z[35] = abb[5] * (T(1) / T(2));
z[25] = z[25] * z[35];
z[24] = z[9] * z[24];
z[24] = z[19] + z[22] + z[24] + z[25] + z[28] + -z[64];
z[24] = (T(1) / T(2)) * z[24] + -z[68];
z[24] = z[24] * z[37];
z[23] = z[13] + z[23] + -z[30];
z[16] = z[9] * z[16];
z[16] = z[16] + (T(-1) / T(2)) * z[23] + z[27] + -z[34] + z[57];
z[16] = (T(1) / T(2)) * z[16] + -z[68];
z[23] = -z[42] + z[43];
z[16] = z[16] * z[23];
z[21] = abb[47] + -z[21];
z[21] = z[21] * z[35];
z[23] = -z[9] * z[63];
z[21] = -z[17] + z[21] + -z[22] + z[23];
z[21] = abb[34] * z[21];
z[3] = -z[3] * z[10];
z[3] = z[3] + z[16] + (T(1) / T(2)) * z[21] + z[24] + z[69];
z[3] = abb[29] * z[3];
z[16] = abb[25] * z[9];
z[21] = abb[7] * z[2];
z[22] = abb[8] * abb[49];
z[4] = z[4] + z[16] + -z[21] + -z[22] + z[33];
z[4] = abb[30] * z[4];
z[16] = abb[6] * z[2];
z[23] = abb[8] * abb[47];
z[16] = -z[11] + z[16] + z[23] + z[33] + -z[48];
z[16] = abb[33] * z[16];
z[23] = abb[46] + z[26];
z[23] = abb[12] * z[23];
z[24] = abb[6] * abb[49];
z[19] = -z[19] + z[23] + z[24];
z[25] = abb[8] + -z[70];
z[25] = z[2] * z[25];
z[18] = -z[9] * z[18];
z[27] = abb[5] * abb[47];
z[18] = z[11] + z[18] + z[19] + z[25] + z[27];
z[18] = z[10] * z[18];
z[16] = -z[4] + z[16] + z[18];
z[10] = z[10] * z[16];
z[16] = -abb[5] * z[26];
z[16] = z[1] + z[16] + -z[19] + -z[22] + -z[66];
z[16] = abb[32] * z[16];
z[17] = -z[17] + z[21] + z[23] + z[24] + z[64];
z[1] = z[1] + -z[65];
z[8] = z[8] * z[35];
z[18] = -abb[25] * z[51];
z[8] = -z[1] + z[8] + (T(1) / T(2)) * z[17] + z[18] + z[22];
z[8] = abb[31] * z[8];
z[15] = z[9] * z[15];
z[14] = (T(-1) / T(2)) * z[14] + z[15] + -z[44];
z[14] = -z[14] * z[40];
z[4] = z[4] + z[8] + z[14] + z[16];
z[4] = z[4] * z[12];
z[8] = -z[9] * z[36];
z[2] = abb[8] * z[2];
z[9] = abb[11] * z[20];
z[2] = z[2] + z[8] + z[9] + z[11] + (T(-1) / T(2)) * z[32] + -z[64];
z[2] = z[2] * z[50];
z[1] = -z[1] + z[13] + z[29] + z[46];
z[8] = -abb[48] + abb[49] * (T(1) / T(2));
z[8] = abb[8] * z[8];
z[8] = z[8] + z[39];
z[0] = abb[46] + -abb[50] + z[0];
z[0] = abb[48] * (T(1) / T(3)) + (T(1) / T(4)) * z[0];
z[0] = abb[5] * z[0];
z[0] = z[0] + (T(1) / T(6)) * z[1] + (T(1) / T(3)) * z[8] + (T(1) / T(4)) * z[45] + (T(1) / T(12)) * z[55];
z[0] = (T(1) / T(2)) * z[0] + (T(-1) / T(3)) * z[41] + (T(-1) / T(12)) * z[68];
z[0] = z[0] * z[49];
z[1] = -abb[53] * z[31];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + (T(1) / T(2)) * z[5] + z[6] + z[10];
z[0] = (T(1) / T(8)) * z[0];
return {z[7], z[0]};
}


template <typename T> std::complex<T> f_4_119_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("0.039297469598739181224753097819362188927231153846060110503291039462"),stof<T>("-0.039450737545701372874580262335943298274017019520876443517683416938")}, std::complex<T>{stof<T>("-0.053462378557085092170024337074839653514291596028154709139524218859"),stof<T>("0.090974896267499707290176114946338951197123518808662699470873846691")}, std::complex<T>{stof<T>("-0.000269991359773439138452705105050563684037029618286530009871145226"),stof<T>("0.102089612120166519864396681937988028470749117128770402672924081893")}, std::complex<T>{stof<T>("-0.10008051447798893754928177361011678232881992032254754429695053896"),stof<T>("0.15667203808942366193361105525774746104804654211009050740095937023")}, std::complex<T>{stof<T>("-0.018015544468128368893684652545455880708664276312521611904412444125"),stof<T>("0.102089612120166519864396681937988028470749117128770402672924081893")}, std::complex<T>{stof<T>("0.073004002965430891648177869491412222559798291561667069582026037991"),stof<T>("-0.090974896267499707290176114946338951197123518808662699470873846691")}, std::complex<T>{stof<T>("0.05308425981936598198548838172921953686782368677692209810094997181"),stof<T>("-0.076949906797383748750224446478305787075727995940786863171375303992")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[27].real()/kbase.W[27].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[187]) - log_iphi_im(kbase.W[187]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_119_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_119_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.0338312628989066314987710296439447261305518220647766021407303074"),stof<T>("0.038631280347612859422852000582678734739397174701595147393936191928")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,55> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W23(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[27].real()/k.W[27].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[187]) - log_iphi_im(k.W[187])), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_119_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_119_DLogXconstant_part(base_point<T>, kend);
	value += f_4_119_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_119_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_119_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_119_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_119_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_119_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_119_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
