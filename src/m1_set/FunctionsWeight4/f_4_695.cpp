/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_695.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_695_abbreviated (const std::array<T,33>& abb) {
T z[53];
z[0] = -abb[15] + abb[17];
z[1] = -abb[18] + 2 * z[0];
z[1] = abb[18] * z[1];
z[2] = 2 * abb[15];
z[3] = -abb[17] + z[2];
z[4] = abb[17] * z[3];
z[5] = prod_pow(abb[15], 2);
z[4] = z[1] + z[4] + -z[5];
z[6] = -abb[18] + z[0];
z[7] = 4 * z[6];
z[8] = 3 * abb[16] + z[7];
z[8] = abb[16] * z[8];
z[9] = 4 * abb[22];
z[8] = -z[4] + z[8] + z[9];
z[8] = abb[1] * z[8];
z[10] = prod_pow(abb[18], 2);
z[11] = prod_pow(abb[16], 2);
z[12] = abb[20] + -z[10] + z[11];
z[12] = abb[8] * z[12];
z[8] = z[8] + -z[12];
z[7] = 3 * abb[19] + z[7];
z[7] = abb[19] * z[7];
z[13] = 4 * abb[23];
z[4] = -3 * abb[20] + z[4] + -z[7] + -z[13];
z[4] = abb[2] * z[4];
z[7] = prod_pow(abb[19], 2);
z[14] = 2 * abb[23];
z[10] = -abb[21] + -z[7] + z[10] + -z[14];
z[10] = abb[7] * z[10];
z[15] = 2 * z[6];
z[16] = abb[19] + z[15];
z[17] = abb[19] * z[16];
z[15] = abb[16] + z[15];
z[18] = abb[16] * z[15];
z[19] = z[14] + z[17] + -z[18];
z[20] = abb[20] + -abb[22];
z[21] = 2 * abb[21];
z[20] = 2 * z[20] + -z[21];
z[22] = z[19] + z[20];
z[22] = abb[9] * z[22];
z[22] = z[4] + z[8] + -z[10] + z[22];
z[23] = -abb[18] + abb[19];
z[24] = z[0] + z[23];
z[25] = abb[19] * z[24];
z[26] = abb[16] + z[6];
z[27] = abb[16] * z[26];
z[25] = -z[25] + z[27];
z[27] = abb[15] * abb[17];
z[28] = -z[5] + z[27];
z[29] = 3 * abb[15];
z[30] = abb[17] + z[29];
z[31] = 4 * abb[18];
z[32] = -z[30] + z[31];
z[32] = abb[18] * z[32];
z[33] = -abb[20] + 3 * abb[22];
z[25] = -z[13] + 8 * z[25] + z[28] + z[32] + 4 * z[33];
z[25] = abb[3] * z[25];
z[32] = -z[7] + z[11];
z[33] = 3 * z[5];
z[20] = -z[14] + -z[20] + 2 * z[32] + -z[33];
z[34] = 2 * abb[18];
z[35] = -abb[17] + abb[18];
z[36] = -z[34] * z[35];
z[37] = -abb[17] + -z[2];
z[37] = abb[17] * z[37];
z[36] = -z[20] + z[36] + z[37];
z[36] = abb[0] * z[36];
z[37] = 5 * abb[1];
z[38] = 3 * abb[3];
z[39] = z[37] + z[38];
z[40] = 4 * abb[21];
z[39] = z[39] * z[40];
z[41] = abb[18] * z[0];
z[42] = -z[5] + z[41];
z[43] = 2 * abb[17];
z[44] = -abb[15] + z[43];
z[44] = abb[17] * z[44];
z[44] = z[42] + z[44];
z[44] = abb[4] * z[44];
z[45] = 4 * abb[24];
z[46] = abb[3] + -abb[8];
z[47] = abb[7] + z[46];
z[48] = -abb[1] + abb[10] + -z[47];
z[48] = z[45] * z[48];
z[49] = abb[21] + abb[23];
z[49] = abb[10] * z[49];
z[50] = 8 * z[49];
z[51] = 3 * abb[11] + 5 * abb[12] + -8 * abb[13];
z[51] = abb[25] * z[51];
z[22] = 4 * z[22] + z[25] + 2 * z[36] + z[39] + z[44] + z[48] + -z[50] + z[51];
z[22] = abb[29] * z[22];
z[25] = abb[20] + abb[22];
z[36] = z[5] + 2 * z[25];
z[39] = 2 * abb[16];
z[44] = z[26] * z[39];
z[48] = 2 * abb[19];
z[51] = z[24] * z[48];
z[52] = 2 * z[27];
z[1] = -z[1] + z[14] + z[36] + z[44] + z[51] + -z[52];
z[1] = abb[3] * z[1];
z[37] = 2 * abb[3] + z[37];
z[37] = abb[21] * z[37];
z[1] = -z[1] + z[4] + -z[8] + -z[37];
z[4] = 3 * abb[21] + -z[5] + z[11] + z[14];
z[8] = 2 * abb[5];
z[4] = z[4] * z[8];
z[4] = z[4] + -2 * z[10];
z[10] = z[17] + z[18];
z[11] = abb[17] * z[0];
z[6] = abb[18] * z[6];
z[17] = -abb[22] + z[6] + -z[11];
z[17] = -z[10] + -z[14] + 2 * z[17] + -z[40];
z[17] = abb[4] * z[17];
z[14] = z[14] + z[21];
z[10] = z[10] + z[14];
z[11] = -z[11] + -z[25] + z[42];
z[11] = -z[10] + 2 * z[11];
z[11] = abb[0] * z[11];
z[7] = -abb[20] + -z[5] + z[7];
z[18] = 2 * abb[6];
z[7] = z[7] * z[18];
z[1] = -2 * z[1] + -z[4] + -z[7] + z[11] + z[17] + 4 * z[49];
z[1] = abb[31] * z[1];
z[11] = 2 * abb[22];
z[17] = -abb[15] + -abb[17] + z[34];
z[17] = abb[18] * z[17];
z[17] = z[11] + z[17] + z[21] + z[28] + z[32];
z[17] = abb[4] * z[17];
z[12] = z[12] + -z[17];
z[17] = -3 * abb[18] + z[43];
z[17] = abb[18] * z[17];
z[17] = z[17] + -z[20] + -z[52];
z[17] = abb[0] * z[17];
z[2] = -abb[18] + z[2];
z[2] = abb[18] * z[2];
z[2] = z[2] + -z[36];
z[18] = -z[2] + z[14];
z[18] = abb[3] * z[18];
z[4] = z[4] + -z[7] + -2 * z[12] + z[17] + z[18] + -z[50];
z[4] = abb[30] * z[4];
z[6] = z[6] + -z[25] + z[28];
z[6] = 2 * z[6] + -z[10];
z[7] = abb[13] * z[6];
z[10] = -abb[26] + -abb[28];
z[10] = z[7] * z[10];
z[1] = z[1] + z[4] + z[10];
z[4] = z[11] + -z[19] + -z[28] + z[41];
z[4] = abb[11] * z[4];
z[2] = z[2] + z[14];
z[2] = abb[12] * z[2];
z[10] = abb[0] + -abb[4];
z[11] = abb[25] * z[10];
z[2] = z[2] + -z[4] + z[7] + z[11];
z[4] = abb[27] + -abb[32];
z[4] = -4 * z[4];
z[2] = z[2] * z[4];
z[4] = z[16] * z[48];
z[7] = z[15] * z[39];
z[4] = -z[4] + z[7] + z[9] + -z[13];
z[7] = -abb[15] + -z[43];
z[7] = abb[17] * z[7];
z[7] = z[4] + z[7] + z[33] + z[41];
z[7] = abb[26] * z[7];
z[9] = -z[29] + z[43];
z[9] = abb[17] * z[9];
z[4] = z[4] + z[5] + z[9] + 3 * z[41];
z[4] = abb[28] * z[4];
z[4] = z[4] + z[7];
z[4] = abb[11] * z[4];
z[5] = -z[13] + 4 * z[25] + -z[40];
z[7] = -abb[18] * z[30];
z[7] = z[5] + z[7] + z[27] + z[33];
z[7] = abb[26] * z[7];
z[9] = -5 * abb[15] + abb[17] + z[31];
z[9] = abb[18] * z[9];
z[5] = z[5] + z[9] + -z[28];
z[5] = abb[28] * z[5];
z[5] = z[5] + z[7];
z[5] = abb[12] * z[5];
z[7] = 2 * abb[31];
z[6] = abb[9] * z[6] * z[7];
z[9] = 4 * abb[14] + -z[38];
z[11] = -4 * abb[0] + -abb[4] + z[9];
z[11] = abb[26] * z[11];
z[9] = 5 * abb[4] + -z[9];
z[9] = abb[28] * z[9];
z[7] = abb[30] + -z[7];
z[7] = abb[11] * z[7];
z[7] = 4 * z[7] + z[9] + z[11];
z[7] = abb[25] * z[7];
z[9] = -abb[4] + 2 * abb[10] + -z[8] + -z[47];
z[9] = abb[30] * z[9];
z[8] = -abb[1] + abb[7] + -abb[10] + z[8] + -z[46];
z[8] = abb[31] * z[8];
z[8] = z[8] + z[9];
z[8] = z[8] * z[45];
z[1] = 2 * z[1] + z[2] + z[4] + z[5] + z[6] + z[7] + z[8] + z[22];
z[1] = 4 * z[1];
z[2] = abb[2] * z[24];
z[4] = abb[7] * z[23];
z[2] = z[2] + -z[4];
z[5] = abb[1] * z[26];
z[6] = abb[16] + -abb[18];
z[6] = -abb[8] * z[6];
z[5] = z[5] + z[6];
z[7] = -abb[15] + abb[16];
z[8] = -z[7] + -z[23];
z[8] = abb[3] * z[8];
z[9] = -abb[15] + abb[19];
z[9] = abb[6] * z[9];
z[11] = abb[4] * z[0];
z[12] = abb[5] * z[7];
z[13] = -abb[0] * z[35];
z[8] = -z[2] + -z[5] + z[8] + z[9] + z[11] + z[12] + z[13];
z[8] = abb[31] * z[8];
z[7] = z[7] + -z[23];
z[7] = z[7] * z[10];
z[4] = -z[4] + -z[6] + z[7] + z[9] + -z[12];
z[4] = abb[30] * z[4];
z[4] = z[4] + z[8];
z[2] = z[2] + -z[5];
z[5] = z[39] + -z[48];
z[6] = -abb[15] + abb[18];
z[7] = -z[5] + -z[6];
z[7] = abb[3] * z[7];
z[3] = abb[18] + -z[3] + z[5];
z[3] = abb[0] * z[3];
z[2] = 2 * z[2] + z[3] + z[7] + -z[11];
z[2] = abb[29] * z[2];
z[0] = -abb[11] * z[0];
z[3] = -abb[12] * z[6];
z[0] = z[0] + z[3];
z[3] = -abb[26] + abb[28];
z[0] = z[0] * z[3];
z[0] = z[0] + z[2] + 2 * z[4];
z[0] = 16 * m1_set::bc<T>[0] * z[0];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_695_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-30.825924438426391160105938719386524650950421855707641046700041743"),stof<T>("-125.991453537739045707660668897222826401320496131828772328508469875")}, stof<T>("-78.039413497218364182385893645573224160329866409059849676046121531"), std::complex<T>{stof<T>("-47.213489058791973022279954926186699509379444553352208629346079787"),stof<T>("125.991453537739045707660668897222826401320496131828772328508469875")}, std::complex<T>{stof<T>("272.97725744254258316964950747838834768811079413862024514190542405"),stof<T>("-67.79818997701534977288717335219884485439281550074165477397916077")}, std::complex<T>{stof<T>("119.24210122646322797427800357607271048180458906586074378984019892"),stof<T>("-190.33358233857056327427342774361791410181011892007436977825635553")}, std::complex<T>{stof<T>("30.056692455413482947923137714092755952987418285626402693641541867"),stof<T>("61.543878049987623211033767789691838235091108162156178360476238533")}, stof<T>("78.039413497218364182385893645573224160329866409059849676046121531")};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[123].real()/kbase.W[123].real()), rlog(k.W[194].real()/kbase.W[194].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_695_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_695_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("117.85613281732487847022647049526382724393219470282048886871303912"),stof<T>("245.16804813331736726631161678146603202295374051203212941754651997")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,33> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), dlog_W58(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real()), rlog(kend.W[194].real()/k.W[194].real())};

                    
            return f_4_695_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_695_DLogXconstant_part(base_point<T>, kend);
	value += f_4_695_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_695_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_695_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_695_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_695_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_695_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_695_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
