/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_609.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_609_abbreviated (const std::array<T,63>& abb) {
T z[123];
z[0] = abb[47] * (T(5) / T(2));
z[1] = 2 * abb[44] + abb[45];
z[2] = abb[46] * (T(-13) / T(4)) + z[0] + -z[1];
z[3] = abb[51] * (T(3) / T(4));
z[4] = abb[50] * (T(1) / T(2));
z[2] = abb[43] * (T(1) / T(12)) + (T(1) / T(3)) * z[2] + z[3] + -z[4];
z[2] = abb[4] * z[2];
z[5] = abb[46] + abb[47];
z[6] = 2 * z[1];
z[7] = abb[51] + z[5] + z[6];
z[8] = abb[49] * (T(1) / T(2));
z[9] = z[4] + (T(-1) / T(3)) * z[7] + z[8];
z[9] = abb[2] * z[9];
z[10] = -abb[46] + abb[51];
z[11] = -abb[43] + z[10];
z[12] = abb[13] * z[11];
z[13] = abb[52] + -abb[53];
z[14] = abb[18] * z[13];
z[12] = z[12] + -z[14];
z[14] = (T(-1) / T(2)) * z[12];
z[15] = abb[51] + z[1];
z[16] = -abb[50] + z[15];
z[17] = 3 * abb[9];
z[18] = z[16] * z[17];
z[19] = -abb[47] + abb[50];
z[20] = -z[10] + z[19];
z[21] = abb[8] * z[20];
z[22] = abb[43] + -abb[47];
z[23] = 7 * z[15] + (T(25) / T(4)) * z[22];
z[23] = abb[50] * (T(-1) / T(4)) + (T(1) / T(3)) * z[23];
z[23] = abb[0] * z[23];
z[24] = 2 * abb[46] + -abb[51] + z[1];
z[25] = z[0] + -z[24];
z[25] = -z[8] + (T(1) / T(3)) * z[25];
z[25] = abb[5] * z[25];
z[26] = -13 * abb[0] + abb[2];
z[26] = -3 * abb[11] + z[17] + (T(1) / T(2)) * z[26];
z[26] = abb[48] * z[26];
z[27] = abb[11] * z[10];
z[28] = 3 * z[27];
z[29] = -abb[47] + -abb[50] + z[10];
z[29] = abb[49] + (T(1) / T(2)) * z[29];
z[30] = abb[3] * z[29];
z[31] = abb[16] * z[13];
z[32] = abb[15] * z[13];
z[33] = abb[1] * z[11];
z[2] = z[2] + z[9] + -z[14] + -z[18] + (T(5) / T(4)) * z[21] + z[23] + z[25] + z[26] + z[28] + (T(1) / T(2)) * z[30] + (T(-9) / T(4)) * z[31] + -2 * z[32] + (T(-10) / T(3)) * z[33];
z[9] = prod_pow(m1_set::bc<T>[0], 2);
z[2] = z[2] * z[9];
z[23] = abb[50] * (T(3) / T(2));
z[25] = abb[47] * (T(1) / T(2));
z[26] = -z[1] + z[23] + -z[25];
z[34] = abb[46] * (T(1) / T(2));
z[35] = abb[51] * (T(1) / T(2));
z[36] = z[26] + -z[34] + -z[35];
z[37] = abb[29] * z[36];
z[38] = abb[33] * z[36];
z[37] = z[37] + -z[38];
z[39] = abb[47] * (T(1) / T(4));
z[40] = abb[44] + abb[45] * (T(1) / T(2));
z[41] = abb[50] * (T(3) / T(4));
z[42] = z[39] + z[40] + -z[41];
z[43] = abb[46] * (T(3) / T(4));
z[44] = abb[51] * (T(5) / T(4));
z[45] = -abb[43] + z[42] + -z[43] + z[44];
z[46] = abb[30] + -abb[31];
z[45] = -z[45] * z[46];
z[47] = abb[43] * (T(1) / T(2));
z[48] = abb[46] + -z[26] + z[47];
z[49] = abb[28] * z[48];
z[50] = abb[32] * z[11];
z[45] = -z[37] + z[45] + z[49] + 2 * z[50];
z[45] = abb[28] * z[45];
z[49] = z[40] + z[41];
z[51] = abb[46] * (T(7) / T(4));
z[44] = -z[44] + z[49] + z[51];
z[52] = abb[47] * (T(7) / T(4));
z[53] = 3 * abb[49];
z[54] = z[44] + z[52] + -z[53];
z[55] = -abb[33] * z[54];
z[56] = 2 * abb[47] + -z[53];
z[57] = z[24] + z[56];
z[58] = 2 * z[57];
z[59] = abb[31] * z[58];
z[60] = z[1] + z[23];
z[61] = 6 * abb[49] + abb[51] * (T(5) / T(2)) + (T(-7) / T(2)) * z[5] + -z[60];
z[62] = -abb[29] * z[61];
z[55] = z[55] + z[59] + z[62];
z[55] = abb[33] * z[55];
z[62] = abb[46] * (T(5) / T(4));
z[63] = abb[43] + -z[3] + z[42] + z[62];
z[63] = abb[31] * z[63];
z[37] = -z[37] + z[63];
z[63] = abb[43] * (T(1) / T(4));
z[64] = z[34] + z[42] + z[63];
z[65] = -abb[30] * z[64];
z[65] = -z[37] + z[65];
z[65] = abb[30] * z[65];
z[66] = -abb[37] * z[36];
z[67] = -abb[29] * z[54];
z[59] = -z[59] + z[67];
z[59] = abb[29] * z[59];
z[67] = abb[46] * (T(1) / T(4));
z[68] = -z[6] + -z[39] + z[67];
z[8] = abb[43] * (T(1) / T(6)) + abb[50] * (T(5) / T(4)) + -z[3] + -z[8] + (T(1) / T(3)) * z[68];
z[8] = z[8] * z[9];
z[68] = -z[1] + z[4];
z[69] = abb[46] * (T(3) / T(2));
z[70] = -z[68] + z[69];
z[71] = abb[47] * (T(3) / T(2));
z[72] = -2 * abb[49] + -z[35] + z[70] + z[71];
z[73] = 3 * abb[36];
z[74] = -z[72] * z[73];
z[75] = -abb[34] + -abb[60];
z[76] = (T(3) / T(2)) * z[11];
z[75] = z[75] * z[76];
z[56] = abb[43] + -abb[46] + -z[1] + -z[56];
z[77] = prod_pow(abb[31], 2);
z[56] = z[56] * z[77];
z[78] = abb[30] * (T(1) / T(2));
z[79] = -2 * abb[31] + abb[32] * (T(-5) / T(4)) + z[78];
z[50] = z[50] * z[79];
z[79] = 3 * abb[35];
z[80] = z[29] * z[79];
z[81] = 3 * z[13];
z[82] = -abb[61] * z[81];
z[8] = z[8] + z[45] + z[50] + z[55] + 2 * z[56] + z[59] + z[65] + z[66] + z[74] + z[75] + z[80] + z[82];
z[8] = abb[7] * z[8];
z[45] = abb[19] + -abb[25];
z[50] = abb[28] * (T(1) / T(2));
z[45] = z[45] * z[50];
z[55] = abb[25] * abb[31];
z[56] = abb[25] * abb[30];
z[59] = z[55] + -z[56];
z[65] = abb[31] * (T(1) / T(2));
z[66] = -abb[33] + z[65];
z[74] = z[66] + -z[78];
z[75] = abb[19] * z[74];
z[78] = abb[25] * abb[32];
z[75] = z[75] + z[78];
z[80] = abb[32] + -abb[33] + -z[46];
z[80] = abb[24] * z[80];
z[45] = z[45] + (T(1) / T(2)) * z[59] + z[75] + z[80];
z[45] = abb[28] * z[45];
z[59] = -abb[29] + abb[33];
z[80] = -abb[31] + z[59];
z[80] = abb[33] * z[80];
z[82] = abb[29] * abb[31];
z[80] = z[80] + z[82];
z[82] = abb[29] + abb[30];
z[83] = -abb[32] + z[82];
z[84] = -abb[31] + z[83];
z[84] = abb[32] * z[84];
z[85] = abb[30] * z[59];
z[84] = -abb[34] + abb[59] + z[80] + z[84] + z[85];
z[84] = abb[24] * z[84];
z[85] = z[56] * z[65];
z[86] = abb[29] * abb[30];
z[83] = abb[32] * z[83];
z[83] = abb[59] + z[83] + -z[86];
z[86] = abb[20] * z[83];
z[87] = abb[25] * abb[34];
z[66] = abb[30] * z[66];
z[88] = abb[19] * z[66];
z[78] = abb[31] * z[78];
z[45] = z[45] + -z[78] + z[84] + z[85] + -z[86] + -z[87] + -z[88];
z[78] = abb[29] + z[74];
z[78] = abb[28] * z[78];
z[84] = prod_pow(abb[29], 2);
z[84] = abb[34] + -abb[37] + abb[59] + (T(-1) / T(2)) * z[84];
z[85] = abb[32] * (T(1) / T(2));
z[86] = -abb[29] + z[85];
z[86] = abb[32] * z[86];
z[87] = abb[33] * (T(1) / T(2));
z[88] = -abb[29] + z[87];
z[88] = abb[33] * z[88];
z[66] = z[66] + -z[78] + z[84] + -z[86] + -z[88];
z[78] = z[9] + z[73];
z[66] = 3 * z[66] + z[78];
z[86] = z[66] + -z[79];
z[86] = abb[21] * z[86];
z[66] = abb[23] * z[66];
z[88] = 3 * abb[60];
z[83] = (T(5) / T(2)) * z[9] + 3 * z[83] + z[88];
z[83] = abb[22] * z[83];
z[89] = abb[23] + abb[24];
z[89] = z[79] * z[89];
z[90] = abb[19] + abb[25] + abb[20] * (T(5) / T(2));
z[9] = z[9] * z[90];
z[90] = abb[20] + abb[25];
z[91] = z[88] * z[90];
z[92] = abb[19] + abb[24];
z[93] = 3 * abb[58];
z[94] = z[92] * z[93];
z[95] = abb[26] * abb[61];
z[96] = abb[19] * z[73];
z[9] = -z[9] + 3 * z[45] + -z[66] + -z[83] + -z[86] + z[89] + -z[91] + -z[94] + (T(-3) / T(2)) * z[95] + -z[96];
z[45] = abb[54] + abb[55] + abb[56] + abb[57];
z[66] = (T(-1) / T(2)) * z[45];
z[9] = z[9] * z[66];
z[66] = abb[47] * (T(5) / T(4));
z[83] = abb[43] * (T(5) / T(4));
z[49] = z[35] + z[49] + -z[66] + z[83];
z[49] = abb[0] * z[49];
z[4] = -z[4] + z[35] + z[40];
z[4] = z[4] * z[17];
z[17] = -z[4] + z[49];
z[49] = abb[2] * z[36];
z[64] = abb[4] * z[64];
z[86] = abb[51] * (T(1) / T(4));
z[89] = z[42] + z[67] + z[86];
z[91] = abb[5] * z[89];
z[94] = (T(-3) / T(4)) * z[12];
z[95] = z[33] + z[94];
z[96] = abb[9] * (T(1) / T(2));
z[97] = -abb[0] + z[96];
z[98] = 3 * abb[48];
z[99] = -z[97] * z[98];
z[100] = (T(3) / T(2)) * z[32];
z[64] = -z[17] + (T(3) / T(4)) * z[31] + -z[49] + z[64] + z[91] + -z[95] + z[99] + z[100];
z[64] = abb[28] * z[64];
z[91] = 2 * abb[51];
z[5] = z[1] + -z[5] + z[91];
z[99] = 2 * abb[14];
z[5] = z[5] * z[99];
z[101] = -abb[9] + z[99];
z[102] = z[98] * z[101];
z[102] = z[18] + z[102];
z[103] = abb[4] * z[36];
z[104] = 3 * abb[50];
z[7] = z[7] + -z[104];
z[105] = abb[2] * z[7];
z[103] = z[103] + -z[105];
z[106] = abb[5] * z[36];
z[107] = -z[5] + z[102] + z[103] + -z[106];
z[108] = abb[29] * z[107];
z[109] = -abb[51] + z[26] + z[47];
z[109] = abb[0] * z[109];
z[110] = abb[14] * z[36];
z[12] = (T(-3) / T(2)) * z[12] + (T(3) / T(2)) * z[31];
z[111] = abb[4] * z[11];
z[112] = (T(1) / T(2)) * z[111];
z[113] = z[12] + 4 * z[33] + z[106] + z[109] + -z[110] + -z[112];
z[114] = abb[32] * z[113];
z[43] = -z[26] + z[43] + z[63] + z[86];
z[43] = abb[4] * z[43];
z[115] = z[105] + -z[110];
z[116] = -z[35] + -z[42] + z[63];
z[116] = abb[0] * z[116];
z[117] = 2 * z[33];
z[94] = z[94] + z[116] + z[117];
z[43] = z[43] + z[94] + z[115];
z[43] = -z[43] * z[46];
z[7] = abb[4] * z[7];
z[7] = z[7] + 2 * z[105];
z[118] = z[7] + -z[106] + -z[110];
z[118] = abb[33] * z[118];
z[119] = abb[31] * z[13];
z[120] = abb[30] * z[13];
z[121] = z[119] + -z[120];
z[122] = abb[16] * (T(3) / T(4));
z[121] = z[121] * z[122];
z[43] = z[43] + z[64] + z[108] + z[114] + -z[118] + z[121];
z[43] = abb[28] * z[43];
z[1] = z[1] + -z[39] + -z[41] + z[62] + -z[86];
z[1] = abb[5] * z[1];
z[39] = (T(3) / T(2)) * z[21];
z[62] = z[39] + -z[110];
z[64] = -3 * abb[47] + -z[10];
z[64] = -abb[43] + z[23] + (T(1) / T(2)) * z[64];
z[64] = abb[4] * z[64];
z[86] = abb[11] + -z[97];
z[86] = z[86] * z[98];
z[97] = (T(1) / T(2)) * z[33];
z[108] = (T(3) / T(4)) * z[32];
z[1] = z[1] + -z[17] + -z[28] + -z[62] + (T(1) / T(2)) * z[64] + z[86] + z[97] + z[108];
z[1] = abb[32] * z[1];
z[17] = -abb[31] * z[113];
z[64] = z[25] + z[35];
z[70] = -z[64] + z[70];
z[70] = abb[5] * z[70];
z[86] = abb[4] * z[20];
z[86] = -z[21] + z[86];
z[113] = z[25] + z[34];
z[68] = abb[51] * (T(3) / T(2)) + -z[68] + -z[113];
z[68] = abb[14] * z[68];
z[16] = abb[9] * z[16];
z[101] = abb[48] * z[101];
z[16] = -z[16] + z[68] + -z[101];
z[68] = -z[16] + z[70] + (T(1) / T(2)) * z[86];
z[68] = 3 * z[68];
z[70] = -abb[29] * z[68];
z[86] = z[33] + z[109];
z[24] = -abb[47] + z[24];
z[101] = abb[5] * z[24];
z[100] = z[100] + z[101];
z[109] = z[62] + z[86] + -z[100] + z[111];
z[109] = abb[30] * z[109];
z[1] = z[1] + z[17] + z[70] + z[109];
z[1] = abb[32] * z[1];
z[17] = z[35] + -z[53] + z[60] + z[113];
z[17] = abb[2] * z[17];
z[44] = z[44] + -z[66];
z[44] = abb[4] * z[44];
z[41] = abb[51] * (T(-7) / T(4)) + abb[46] * (T(17) / T(4)) + 5 * z[40] + -z[41] + -z[52];
z[41] = abb[5] * z[41];
z[52] = -abb[2] + -abb[11] + -z[96] + z[99];
z[52] = z[52] * z[98];
z[5] = z[4] + -z[5] + z[17] + z[28] + z[41] + z[44] + z[52];
z[5] = abb[29] * z[5];
z[28] = abb[5] * z[58];
z[28] = z[28] + -z[103] + z[110];
z[28] = abb[31] * z[28];
z[5] = z[5] + -z[28];
z[5] = abb[29] * z[5];
z[41] = abb[4] * z[89];
z[44] = -abb[5] * z[54];
z[52] = -abb[2] + z[96];
z[52] = z[52] * z[98];
z[4] = -z[4] + z[17] + z[41] + z[44] + z[52] + -z[110];
z[4] = abb[33] * z[4];
z[0] = abb[46] * (T(-5) / T(2)) + abb[51] * (T(7) / T(2)) + -z[0] + z[60];
z[0] = abb[14] * z[0];
z[17] = -abb[5] * z[61];
z[0] = z[0] + z[7] + z[17] + -z[102];
z[0] = abb[29] * z[0];
z[0] = z[0] + z[4] + z[28];
z[0] = abb[33] * z[0];
z[4] = z[3] + -z[26] + -z[63] + z[67];
z[4] = abb[4] * z[4];
z[4] = z[4] + -z[94] + z[105] + -z[106];
z[4] = abb[31] * z[4];
z[7] = abb[4] * z[24];
z[7] = -z[7] + z[62] + -2 * z[101] + -z[105];
z[7] = abb[29] * z[7];
z[4] = z[4] + z[7] + -z[118];
z[7] = -z[25] + z[34] + z[40] + -z[47];
z[7] = abb[4] * z[7];
z[17] = abb[46] + z[40] + -z[64];
z[17] = abb[5] * z[17];
z[7] = z[7] + z[17] + -z[49] + z[97] + -z[108] + z[116];
z[7] = abb[30] * z[7];
z[7] = -z[4] + z[7];
z[7] = abb[30] * z[7];
z[17] = abb[37] * z[107];
z[24] = z[10] + z[19];
z[24] = (T(1) / T(2)) * z[24];
z[25] = abb[14] * z[24];
z[14] = z[14] + z[33];
z[28] = (T(1) / T(2)) * z[32];
z[19] = -abb[43] + -z[19];
z[19] = abb[0] * z[19];
z[34] = abb[0] + -abb[14];
z[34] = abb[48] * z[34];
z[19] = -z[14] + (T(1) / T(2)) * z[19] + z[25] + z[28] + z[34];
z[19] = abb[34] * z[19];
z[34] = -abb[3] * z[29] * z[80];
z[19] = z[19] + z[34];
z[34] = -abb[4] * z[48];
z[40] = -abb[5] * z[57];
z[34] = z[34] + z[40] + -z[86] + -z[105];
z[34] = z[34] * z[77];
z[40] = -abb[59] * z[68];
z[41] = -abb[49] + abb[50];
z[41] = abb[2] * z[41];
z[29] = abb[5] * z[29];
z[44] = -abb[2] + abb[14];
z[44] = abb[48] * z[44];
z[25] = -z[25] + z[29] + -z[30] + z[41] + z[44];
z[25] = z[25] * z[79];
z[29] = abb[31] * z[89];
z[29] = z[29] + z[38];
z[29] = abb[30] * z[29];
z[41] = -abb[28] + z[46];
z[41] = z[41] * z[89];
z[41] = -z[38] + z[41];
z[41] = abb[28] * z[41];
z[29] = z[29] + z[41];
z[41] = -z[78] + -z[93];
z[41] = z[36] * z[41];
z[29] = 3 * z[29] + z[41];
z[29] = abb[12] * z[29];
z[24] = -abb[48] + z[24];
z[41] = abb[29] * z[24];
z[44] = z[24] * z[87];
z[44] = -z[41] + z[44];
z[44] = abb[33] * z[44];
z[48] = z[24] * z[85];
z[48] = -z[41] + z[48];
z[48] = abb[32] * z[48];
z[49] = -z[24] * z[50];
z[41] = z[41] + z[49];
z[41] = abb[28] * z[41];
z[49] = abb[35] + abb[58] + -z[84];
z[49] = z[24] * z[49];
z[41] = z[41] + z[44] + z[48] + z[49];
z[44] = 3 * abb[6];
z[41] = z[41] * z[44];
z[48] = -abb[5] * z[72];
z[30] = -z[30] + z[48] + z[103];
z[30] = z[30] * z[73];
z[16] = z[16] + -z[103];
z[48] = -z[16] * z[93];
z[20] = abb[5] * z[20];
z[20] = z[20] + -z[21] + z[31];
z[14] = z[14] + (T(1) / T(2)) * z[20] + z[28] + -z[112];
z[20] = -z[14] * z[88];
z[28] = -abb[15] + abb[17];
z[28] = z[28] * z[89];
z[49] = abb[0] + abb[4];
z[49] = -2 * abb[27] + abb[13] * (T(1) / T(4)) + (T(3) / T(4)) * z[49];
z[49] = z[13] * z[49];
z[50] = abb[16] + abb[18];
z[52] = (T(1) / T(4)) * z[11];
z[50] = z[50] * z[52];
z[28] = -z[28] + z[49] + -z[50];
z[49] = 3 * abb[61];
z[49] = -z[28] * z[49];
z[13] = -z[13] * z[77];
z[50] = z[65] * z[120];
z[13] = z[13] + z[50];
z[13] = abb[16] * z[13];
z[0] = abb[62] + z[0] + z[1] + z[2] + z[5] + z[7] + z[8] + z[9] + (T(3) / T(2)) * z[13] + z[17] + 3 * z[19] + z[20] + z[25] + z[29] + z[30] + z[34] + z[40] + z[41] + z[43] + z[48] + z[49];
z[1] = z[10] + -z[23] + z[47] + z[71];
z[1] = abb[4] * z[1];
z[2] = z[15] + z[22];
z[5] = 2 * abb[0];
z[2] = z[2] * z[5];
z[2] = z[2] + -z[18];
z[7] = abb[0] + abb[11];
z[7] = abb[9] + -2 * z[7];
z[7] = z[7] * z[98];
z[1] = z[1] + z[2] + z[7] + -z[12] + 3 * z[21] + 6 * z[27] + -5 * z[33] + -z[100] + -z[110];
z[1] = abb[32] * z[1];
z[7] = -z[3] + -z[26] + z[51] + z[83];
z[7] = abb[4] * z[7];
z[7] = z[7] + -z[39] + z[95] + z[100] + z[105] + -z[116];
z[7] = abb[30] * z[7];
z[8] = -abb[43] + z[26] + z[35] + -z[69];
z[8] = abb[4] * z[8];
z[9] = z[31] + z[32];
z[5] = abb[9] + -z[5];
z[5] = z[5] * z[98];
z[2] = z[2] + z[5] + z[8] + -3 * z[9] + -z[115] + -z[117];
z[2] = abb[28] * z[2];
z[5] = z[119] + z[120];
z[5] = z[5] * z[122];
z[1] = z[1] + z[2] + -z[4] + z[5] + z[7];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -z[46] * z[89];
z[4] = -abb[28] * z[36];
z[2] = z[2] + z[4] + z[38];
z[2] = m1_set::bc<T>[0] * z[2];
z[4] = -abb[38] * z[36];
z[2] = z[2] + z[4];
z[2] = abb[12] * z[2];
z[4] = abb[38] * z[16];
z[5] = abb[40] * z[14];
z[2] = -z[2] + z[4] + z[5];
z[4] = z[55] + z[56];
z[5] = abb[32] + z[59];
z[5] = abb[24] * z[5];
z[7] = abb[28] * z[92];
z[4] = (T(1) / T(2)) * z[4] + z[5] + -z[7] + -z[75];
z[5] = -abb[32] + (T(1) / T(2)) * z[82];
z[7] = abb[20] * z[5];
z[4] = (T(1) / T(2)) * z[4] + z[7];
z[4] = m1_set::bc<T>[0] * z[4];
z[7] = abb[32] + z[74];
z[7] = m1_set::bc<T>[0] * z[7];
z[7] = abb[39] + z[7];
z[7] = (T(1) / T(2)) * z[7];
z[8] = -abb[21] + -abb[23];
z[7] = z[7] * z[8];
z[8] = abb[20] + -abb[24];
z[8] = abb[39] * z[8];
z[9] = abb[38] * z[92];
z[8] = z[8] + z[9];
z[5] = m1_set::bc<T>[0] * z[5];
z[9] = abb[40] * (T(1) / T(2));
z[5] = abb[39] * (T(1) / T(2)) + -z[5] + z[9];
z[5] = abb[22] * z[5];
z[9] = z[9] * z[90];
z[10] = abb[26] * abb[41];
z[4] = z[4] + -z[5] + z[7] + (T(-1) / T(2)) * z[8] + -z[9] + (T(-1) / T(4)) * z[10];
z[5] = -3 * z[45];
z[4] = z[4] * z[5];
z[3] = z[3] + z[42] + -z[47] + -z[67];
z[3] = abb[30] * z[3];
z[5] = -z[6] + z[22] + -z[91] + z[104];
z[5] = abb[28] * z[5];
z[6] = z[11] * z[85];
z[3] = z[3] + z[5] + z[6] + -z[37];
z[3] = m1_set::bc<T>[0] * z[3];
z[5] = -abb[40] * z[76];
z[6] = -abb[41] * z[81];
z[3] = z[3] + z[5] + z[6];
z[3] = abb[7] * z[3];
z[5] = -abb[39] * z[68];
z[6] = 3 * abb[41];
z[6] = -z[6] * z[28];
z[7] = abb[28] + -abb[32];
z[7] = m1_set::bc<T>[0] * z[7];
z[7] = abb[38] + -abb[39] + z[7];
z[7] = z[7] * z[24] * z[44];
z[1] = abb[42] + z[1] + -3 * z[2] + z[3] + z[4] + z[5] + z[6] + z[7];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_609_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("10.269512493905512545637669731671240915263504226021180975992455577"),stof<T>("-58.554694814178373260477368471541400794702884064369990307851491817")}, std::complex<T>{stof<T>("3.864737663879488353265753160505984425095431179935268491788284964"),stof<T>("-47.706147699074941751564270035516253070252721807743778987143419911")}, std::complex<T>{stof<T>("1.932368831939744176632876580252992212547715589967634245894142482"),stof<T>("-23.853073849537470875782135017758126535126360903871889493571709955")}, std::complex<T>{stof<T>("9.878152653184990155537763548562580744476206783623852519577901265"),stof<T>("-19.92935011442469201137941997400066842068505199455458671913663283")}, std::complex<T>{stof<T>("-18.268293638099721913065127805053267735940324506964062333523932316"),stof<T>("30.85558242216324742086829337306750850632132959989719188070629115")}, std::complex<T>{stof<T>("-20.592022310759988479797910568414920119275337539329025035832629111"),stof<T>("93.334000971454399545748376888366367415465522573584484962992860093")}, std::complex<T>{stof<T>("16.335924806159977736432251224800275523392608916996428087629789834"),stof<T>("-7.002508572625776545086158355309381971194968696025302387134581194")}, std::complex<T>{stof<T>("-7.9457838212452459789048869683095885319284911936562182736837587825"),stof<T>("-3.9237237351127788644027150437574581144413089093173027744350771258")}, std::complex<T>{stof<T>("-16.512200902804655404527292095446812870862691003762646777629453187"),stof<T>("24.063143698697434894401075584092701938234885903411332628943547409")}, std::complex<T>{stof<T>("16.512200902804655404527292095446812870862691003762646777629453187"),stof<T>("-24.063143698697434894401075584092701938234885903411332628943547409")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}, std::complex<T>{stof<T>("5.787142097913693199890126264026130869720949215539865048902691418"),stof<T>("-0.3899001354797931554328612164207743155239559541608405847690121451")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[124].real()/kbase.W[124].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_609_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_609_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(8)) * (24 + 7 * v[0] + v[1] + 3 * v[2] + -v[3] + -8 * v[4] + -4 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + -7 * v[5]) * (-v[0] + v[1] + -v[2] + -v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (3 * (-1 + 2 * m1_set::bc<T>[1]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (2 * abb[44] + abb[45] + abb[46] + abb[47] + -abb[49] + -abb[50]) * (t * c[0] + c[1]) * (T(1) / T(2));
	}
	{
T z[3];
z[0] = abb[31] + -abb[33];
z[1] = 2 * abb[44] + abb[45] + abb[46] + abb[47] + -abb[49] + -abb[50];
z[0] = z[0] * z[1];
z[2] = abb[29] * z[1];
z[0] = z[0] + z[2];
z[0] = abb[31] * z[0];
z[1] = abb[36] * z[1];
z[2] = -abb[33] * z[2];
z[0] = z[0] + z[1] + z[2];
return 3 * abb[10] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_609_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_609_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-32.796205031715046932025671777143204995350881675711372250712685776"),stof<T>("48.465065132210457705915045996313609616872406605829586846137138145")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,63> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W17(k,dl), dlog_W23(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W32(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), dlr[2], f_1_1(k), f_1_7(k), f_1_8(k), f_1_9(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_15(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), f_2_11_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[13].real()/k.W[13].real()), IntDLogL_W_17(t,k,kend,dl), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[124].real()/k.W[124].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_2_re(k), f_2_10_re(k), f_2_11_re(k), f_2_25_re(k), T{0}};
abb[42] = SpDLog_f_4_609_W_17_Im(t, path, abb);
abb[62] = SpDLog_f_4_609_W_17_Re(t, path, abb);

                    
            return f_4_609_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_609_DLogXconstant_part(base_point<T>, kend);
	value += f_4_609_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_609_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_609_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_609_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_609_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_609_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_609_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
