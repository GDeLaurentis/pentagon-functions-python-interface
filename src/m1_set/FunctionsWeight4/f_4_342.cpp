/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_342.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_342_abbreviated (const std::array<T,57>& abb) {
T z[83];
z[0] = 2 * abb[42] + abb[45];
z[1] = abb[43] * (T(1) / T(2));
z[2] = z[0] + z[1];
z[3] = abb[44] + abb[48];
z[4] = abb[47] * (T(1) / T(2));
z[5] = abb[46] * (T(1) / T(2));
z[2] = -abb[49] + (T(1) / T(3)) * z[2] + (T(1) / T(6)) * z[3] + z[4] + z[5];
z[2] = abb[2] * z[2];
z[3] = abb[49] * (T(3) / T(4));
z[6] = abb[45] + z[1];
z[6] = abb[42] + -z[3] + (T(1) / T(2)) * z[6];
z[7] = abb[48] * (T(1) / T(4));
z[8] = abb[44] * (T(1) / T(4));
z[9] = z[6] + z[7] + z[8];
z[10] = abb[14] * z[9];
z[11] = abb[41] * (T(25) / T(12));
z[12] = abb[49] * (T(1) / T(4));
z[13] = 14 * abb[42] + 7 * abb[45] + abb[43] * (T(-25) / T(4));
z[13] = abb[46] * (T(-13) / T(2)) + abb[48] * (T(7) / T(3)) + z[11] + -z[12] + (T(1) / T(3)) * z[13];
z[13] = abb[0] * z[13];
z[14] = 11 * abb[43] + abb[45];
z[14] = abb[42] + (T(1) / T(2)) * z[14];
z[11] = -2 * abb[49] + abb[44] * (T(5) / T(12)) + -z[7] + z[11] + (T(1) / T(3)) * z[14];
z[11] = abb[8] * z[11];
z[14] = 8 * abb[42] + 4 * abb[45] + abb[43] * (T(35) / T(4));
z[14] = abb[41] * (T(-11) / T(6)) + abb[47] * (T(-9) / T(2)) + abb[44] * (T(13) / T(12)) + z[7] + z[12] + (T(1) / T(3)) * z[14];
z[14] = abb[3] * z[14];
z[15] = abb[19] + abb[21];
z[16] = abb[24] + z[15];
z[16] = (T(1) / T(4)) * z[16];
z[17] = abb[52] + abb[53];
z[16] = z[16] * z[17];
z[18] = -abb[49] + z[0];
z[19] = abb[43] + abb[44];
z[20] = -abb[47] + z[18] + z[19];
z[21] = abb[11] * z[20];
z[22] = abb[45] + abb[43] * (T(13) / T(2));
z[22] = abb[42] + (T(1) / T(2)) * z[22];
z[12] = abb[48] * (T(-5) / T(12)) + abb[47] * (T(-3) / T(2)) + abb[44] * (T(7) / T(12)) + z[12] + (T(1) / T(3)) * z[22];
z[12] = abb[5] * z[12];
z[22] = abb[50] + -abb[51];
z[23] = abb[15] * z[22];
z[24] = abb[44] + -abb[48];
z[25] = abb[46] + z[24];
z[25] = abb[10] * z[25];
z[26] = 3 * z[25];
z[18] = -abb[46] + abb[48] + z[18];
z[18] = abb[9] * z[18];
z[27] = 3 * z[18];
z[28] = abb[49] + z[24];
z[29] = abb[43] + z[28];
z[29] = -abb[47] + (T(1) / T(2)) * z[29];
z[30] = abb[7] * z[29];
z[31] = abb[41] + z[24];
z[32] = abb[1] * z[31];
z[2] = z[2] + z[10] + z[11] + z[12] + z[13] + z[14] + z[16] + -2 * z[21] + (T(-1) / T(4)) * z[23] + -z[26] + -z[27] + (T(1) / T(2)) * z[30] + (T(7) / T(3)) * z[32];
z[2] = prod_pow(m1_set::bc<T>[0], 2) * z[2];
z[10] = -abb[16] + abb[18];
z[10] = z[9] * z[10];
z[11] = abb[15] + -abb[17];
z[11] = (T(-1) / T(4)) * z[11];
z[11] = z[11] * z[31];
z[12] = -abb[3] + abb[12] * (T(-1) / T(4));
z[12] = z[12] * z[22];
z[13] = abb[0] + abb[8];
z[14] = 3 * z[13];
z[14] = abb[51] * z[14];
z[16] = abb[25] * z[17];
z[14] = z[14] + z[16];
z[16] = 2 * abb[26];
z[13] = (T(-3) / T(4)) * z[13] + z[16];
z[13] = abb[50] * z[13];
z[16] = -abb[51] * z[16];
z[10] = z[10] + z[11] + z[12] + z[13] + (T(1) / T(4)) * z[14] + z[16];
z[10] = abb[37] * z[10];
z[11] = -z[15] * z[17];
z[12] = abb[22] * z[17];
z[13] = -abb[43] + z[28];
z[14] = abb[4] * z[13];
z[16] = -abb[8] * z[31];
z[28] = -abb[5] * z[13];
z[11] = z[11] + -z[12] + z[14] + z[16] + z[28];
z[16] = abb[17] * z[22];
z[28] = abb[12] * z[31];
z[16] = z[16] + -z[28];
z[33] = abb[3] * z[31];
z[34] = z[16] + z[33];
z[34] = z[32] + (T(1) / T(2)) * z[34];
z[11] = (T(1) / T(2)) * z[11] + z[34];
z[11] = abb[34] * z[11];
z[35] = abb[44] * (T(1) / T(2));
z[36] = z[1] + z[35];
z[37] = z[0] + z[36];
z[38] = abb[49] * (T(3) / T(2));
z[39] = abb[48] * (T(1) / T(2));
z[40] = -z[37] + z[38] + -z[39];
z[41] = abb[8] * z[40];
z[42] = 4 * abb[42] + 2 * abb[45];
z[43] = 3 * abb[49];
z[44] = abb[48] + z[19] + z[42] + -z[43];
z[45] = abb[2] * z[44];
z[41] = z[41] + -z[45];
z[46] = abb[14] * z[40];
z[47] = (T(1) / T(2)) * z[17];
z[48] = abb[24] * z[47];
z[46] = -z[41] + z[46] + -z[48];
z[48] = abb[49] * (T(1) / T(2));
z[49] = z[39] + z[48];
z[50] = abb[44] * (T(3) / T(2));
z[51] = -2 * abb[47] + abb[43] * (T(3) / T(2)) + z[0] + -z[49] + z[50];
z[52] = abb[3] + abb[5];
z[51] = z[51] * z[52];
z[53] = z[15] * z[47];
z[21] = z[21] + z[30] + -z[46] + -z[51] + z[53];
z[51] = abb[56] * z[21];
z[49] = -abb[46] + -z[36] + z[49];
z[54] = abb[13] * z[49];
z[49] = abb[6] * z[49];
z[55] = abb[20] * z[47];
z[55] = -z[49] + z[55];
z[56] = abb[23] * z[47];
z[54] = z[54] + z[55] + z[56];
z[29] = -z[29] * z[52];
z[57] = -abb[46] + -abb[47] + abb[49];
z[57] = abb[2] * z[57];
z[47] = abb[22] * z[47];
z[29] = z[29] + z[30] + -z[47] + -z[54] + z[57];
z[29] = abb[35] * z[29];
z[47] = z[47] + -z[56];
z[18] = -z[18] + z[47];
z[53] = z[18] + z[53];
z[46] = z[46] + z[53];
z[56] = -z[0] + z[48];
z[57] = abb[48] * (T(3) / T(2));
z[36] = 2 * abb[46] + z[36] + z[56] + -z[57];
z[36] = abb[13] * z[36];
z[58] = z[36] + -z[46];
z[58] = abb[54] * z[58];
z[50] = -z[39] + z[50];
z[56] = z[1] + -z[50] + z[56];
z[56] = abb[5] * z[56];
z[59] = abb[8] * (T(1) / T(2));
z[13] = z[13] * z[59];
z[13] = -z[13] + (T(1) / T(2)) * z[14] + z[53] + z[56];
z[53] = z[13] + -z[36];
z[56] = abb[55] * z[53];
z[60] = abb[41] * (T(1) / T(2));
z[61] = abb[46] + z[1] + -z[48] + -z[60];
z[61] = abb[0] * z[61];
z[34] = z[34] + z[54] + z[61];
z[34] = abb[33] * z[34];
z[54] = abb[20] * (T(1) / T(2));
z[54] = z[17] * z[54];
z[54] = -z[49] + z[54];
z[61] = -abb[54] + abb[55];
z[61] = z[54] * z[61];
z[10] = z[10] + z[11] + z[29] + z[34] + z[51] + z[56] + z[58] + z[61];
z[11] = 3 * abb[46];
z[29] = z[11] + -z[39];
z[34] = 3 * abb[47];
z[37] = -z[29] + -z[34] + z[37] + z[38];
z[37] = abb[2] * z[37];
z[51] = 2 * abb[48];
z[56] = -z[11] + z[51];
z[19] = z[0] + -z[19] + z[56];
z[58] = 2 * abb[13];
z[19] = z[19] * z[58];
z[48] = -abb[42] + z[48];
z[5] = abb[45] * (T(1) / T(2)) + -z[5] + z[39] + -z[48];
z[58] = 3 * abb[9];
z[58] = z[5] * z[58];
z[61] = (T(3) / T(4)) * z[17];
z[62] = abb[20] * z[61];
z[62] = (T(3) / T(2)) * z[49] + -z[62];
z[63] = abb[43] * (T(7) / T(2));
z[64] = abb[45] + z[63];
z[64] = abb[42] + z[3] + -z[34] + (T(1) / T(2)) * z[64];
z[65] = abb[48] * (T(5) / T(4));
z[66] = abb[44] * (T(7) / T(4)) + -z[65];
z[67] = -z[64] + -z[66];
z[67] = abb[3] * z[67];
z[68] = z[15] * z[61];
z[69] = abb[22] * z[61];
z[70] = z[68] + z[69];
z[71] = abb[43] * (T(5) / T(2));
z[72] = -abb[45] + z[71];
z[72] = -abb[42] + -z[3] + (T(1) / T(2)) * z[72];
z[66] = z[66] + -z[72];
z[66] = abb[8] * z[66];
z[73] = 5 * abb[45] + -z[63];
z[3] = 5 * abb[42] + abb[48] * (T(-7) / T(4)) + abb[44] * (T(17) / T(4)) + -z[3] + (T(1) / T(2)) * z[73];
z[3] = abb[5] * z[3];
z[3] = z[3] + -z[19] + -z[26] + z[37] + z[58] + z[62] + z[66] + z[67] + -z[70];
z[3] = abb[30] * z[3];
z[37] = 6 * abb[47] + -z[0] + -z[38] + -z[63];
z[63] = abb[48] * (T(-5) / T(2)) + abb[44] * (T(7) / T(2)) + -z[37];
z[52] = z[52] * z[63];
z[63] = (T(3) / T(2)) * z[17];
z[66] = abb[20] * z[63];
z[49] = 3 * z[49] + -z[66];
z[66] = abb[22] * z[63];
z[66] = -z[49] + z[66];
z[67] = -z[27] + z[66];
z[73] = 2 * z[45];
z[44] = abb[8] * z[44];
z[71] = -6 * abb[46] + abb[44] * (T(-5) / T(2)) + abb[48] * (T(7) / T(2)) + z[0] + z[38] + -z[71];
z[71] = abb[13] * z[71];
z[74] = 3 * z[30];
z[75] = abb[23] * z[63];
z[52] = z[44] + z[52] + z[67] + z[71] + z[73] + -z[74] + z[75];
z[52] = abb[32] * z[52];
z[71] = 2 * abb[44];
z[76] = -abb[48] + z[0] + z[71];
z[77] = 2 * abb[43];
z[78] = -z[34] + z[76] + z[77];
z[79] = 2 * abb[5];
z[80] = 2 * abb[3] + z[79];
z[78] = z[78] * z[80];
z[80] = abb[13] * z[40];
z[75] = z[75] + z[80];
z[15] = z[15] * z[63];
z[81] = z[15] + -z[75];
z[78] = z[41] + z[74] + -z[78] + z[81];
z[82] = abb[28] * z[78];
z[3] = z[3] + z[52] + z[82];
z[3] = abb[30] * z[3];
z[52] = abb[5] * z[40];
z[15] = z[15] + z[52];
z[33] = -z[15] + 2 * z[33] + z[80];
z[80] = abb[41] * (T(5) / T(4));
z[29] = -z[29] + -z[72] + z[80];
z[29] = abb[0] * z[29];
z[72] = (T(3) / T(4)) * z[22];
z[72] = abb[17] * z[72];
z[28] = (T(3) / T(4)) * z[28] + -z[72];
z[29] = z[28] + z[29];
z[5] = abb[9] * z[5];
z[5] = z[5] + -z[47];
z[47] = -abb[43] + abb[49];
z[72] = -5 * abb[41] + 3 * z[47];
z[24] = -z[24] + (T(1) / T(2)) * z[72];
z[24] = z[24] * z[59];
z[72] = (T(3) / T(4)) * z[14];
z[82] = (T(3) / T(4)) * z[23];
z[5] = 3 * z[5] + z[24] + z[26] + -z[29] + z[32] + z[33] + z[62] + -z[72] + -z[82];
z[5] = abb[31] * z[5];
z[17] = abb[23] * z[17];
z[12] = z[12] + -z[16] + -z[17] + z[23];
z[1] = -z[1] + z[38];
z[16] = abb[48] + z[0];
z[17] = z[1] + -z[16] + z[60];
z[17] = abb[0] * z[17];
z[24] = z[31] * z[59];
z[12] = (T(-3) / T(2)) * z[12] + -z[17] + -z[24] + 4 * z[32] + z[33];
z[24] = abb[28] * z[12];
z[26] = z[53] + z[55];
z[26] = abb[30] * z[26];
z[5] = z[5] + z[24] + 3 * z[26];
z[5] = abb[31] * z[5];
z[24] = abb[41] * (T(1) / T(4));
z[26] = -z[6] + z[24] + -z[39];
z[26] = abb[0] * z[26];
z[28] = z[28] + z[70];
z[31] = 2 * z[32];
z[38] = z[26] + z[28] + -z[31] + z[82];
z[1] = -z[0] + z[1];
z[53] = abb[44] * (T(3) / T(4));
z[7] = -z[1] + z[7] + z[24] + z[53];
z[7] = abb[8] * z[7];
z[55] = 3 * abb[14];
z[59] = z[9] * z[55];
z[61] = abb[24] * z[61];
z[59] = z[59] + z[61];
z[61] = -z[45] + z[59];
z[53] = -abb[41] + z[6] + -z[53] + z[65];
z[53] = abb[3] * z[53];
z[7] = z[7] + z[38] + z[53] + -z[61] + -z[75];
z[53] = abb[28] + -abb[29];
z[7] = z[7] * z[53];
z[53] = abb[3] * z[40];
z[19] = z[15] + z[19] + -z[41] + z[53] + z[67];
z[41] = -abb[30] * z[19];
z[58] = z[58] + -z[62];
z[62] = abb[2] * z[40];
z[9] = abb[5] * z[9];
z[9] = -z[9] + -z[32] + z[62];
z[32] = z[9] + z[59];
z[62] = z[6] + z[24] + z[35];
z[62] = abb[8] * z[62];
z[22] = abb[16] * z[22];
z[65] = (T(3) / T(2)) * z[22] + z[82];
z[67] = abb[44] + -z[1] + z[60];
z[67] = abb[3] * z[67];
z[29] = -z[29] + -z[32] + z[58] + z[62] + z[65] + z[67];
z[29] = abb[27] * z[29];
z[12] = abb[31] * z[12];
z[53] = z[53] + z[75];
z[44] = -z[44] + z[52] + z[53];
z[40] = z[40] * z[55];
z[55] = abb[24] * z[63];
z[40] = -z[40] + z[55];
z[55] = z[40] + z[44] + -z[73];
z[55] = abb[32] * z[55];
z[7] = z[7] + -z[12] + z[29] + z[41] + z[55];
z[7] = abb[27] * z[7];
z[29] = abb[48] * (T(3) / T(4));
z[41] = -z[8] + z[29];
z[62] = -abb[43] + abb[45];
z[62] = abb[42] + z[41] + (T(1) / T(2)) * z[62] + -z[80];
z[62] = abb[8] * z[62];
z[60] = z[6] + z[41] + -z[60];
z[63] = -abb[3] * z[60];
z[9] = -z[9] + z[26] + -z[28] + z[62] + z[63] + -z[65] + z[72];
z[9] = abb[29] * z[9];
z[8] = z[1] + -z[8] + z[24] + -z[29];
z[8] = abb[8] * z[8];
z[6] = -abb[41] + abb[44] * (T(-5) / T(4)) + -z[6] + z[29];
z[6] = abb[3] * z[6];
z[6] = z[6] + z[8] + z[38] + z[52] + z[61];
z[6] = abb[28] * z[6];
z[8] = -abb[43] + z[76];
z[24] = abb[8] + z[79];
z[8] = z[8] * z[24];
z[14] = (T(3) / T(2)) * z[14];
z[8] = z[8] + -z[14] + z[45] + z[53];
z[8] = abb[30] * z[8];
z[6] = z[6] + z[8] + z[9] + -z[12] + -z[55];
z[6] = abb[29] * z[6];
z[8] = -abb[36] * z[19];
z[9] = abb[43] + z[42];
z[12] = 2 * z[9] + -z[34] + -z[43] + z[56] + z[71];
z[12] = abb[2] * z[12];
z[19] = (T(3) / T(2)) * z[30];
z[12] = z[12] + z[19] + -z[44] + -z[58] + -z[59] + -z[69];
z[24] = prod_pow(abb[32], 2);
z[12] = z[12] * z[24];
z[26] = abb[8] * z[60];
z[28] = 2 * abb[41];
z[29] = z[28] + -z[41] + -z[64];
z[29] = abb[3] * z[29];
z[17] = -z[17] + -z[19] + z[26] + z[29] + -z[32] + -z[68];
z[17] = abb[28] * z[17];
z[17] = z[17] + z[55];
z[17] = abb[28] * z[17];
z[19] = abb[43] + abb[45];
z[4] = z[4] + (T(-1) / T(2)) * z[19] + -z[35] + z[48];
z[19] = prod_pow(abb[28], 2);
z[24] = -z[19] + z[24];
z[4] = z[4] * z[24];
z[24] = abb[28] + -abb[32];
z[24] = abb[30] * z[20] * z[24];
z[4] = z[4] + z[24];
z[24] = 3 * abb[11];
z[4] = z[4] * z[24];
z[19] = -abb[34] + -z[19];
z[19] = z[19] * z[23];
z[26] = abb[33] + -abb[34];
z[26] = z[22] * z[26];
z[19] = z[19] + z[26];
z[2] = z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8] + 3 * z[10] + z[12] + z[17] + (T(3) / T(2)) * z[19];
z[3] = 3 * z[22] + z[45];
z[4] = -abb[41] + z[1] + -z[50];
z[4] = abb[8] * z[4];
z[5] = abb[41] + -abb[43] + -z[11] + z[16];
z[6] = 2 * abb[0];
z[5] = z[5] * z[6];
z[5] = z[5] + z[31];
z[6] = z[9] + -z[43];
z[7] = -abb[41] + z[6] + z[51];
z[8] = -abb[3] * z[7];
z[9] = 3 * z[23];
z[4] = -z[3] + z[4] + z[5] + z[8] + -z[9] + -z[27] + z[40] + -z[66] + -z[81];
z[4] = abb[27] * z[4];
z[7] = abb[0] * z[7];
z[7] = z[7] + z[31] + -z[40];
z[8] = -z[35] + z[57];
z[1] = abb[41] + z[1] + -z[8];
z[1] = abb[8] * z[1];
z[8] = -4 * abb[41] + z[8] + -z[37];
z[8] = abb[3] * z[8];
z[1] = z[1] + -z[7] + z[8] + z[9] + z[15] + -z[45] + z[74];
z[1] = abb[28] * z[1];
z[8] = z[28] + z[35] + -z[39] + (T(-3) / T(2)) * z[47];
z[8] = abb[8] * z[8];
z[5] = z[5] + z[8] + z[14] + 3 * z[18] + -6 * z[25] + -z[33] + -z[49];
z[5] = abb[31] * z[5];
z[0] = abb[44] + z[0] + z[28] + -z[43] + z[77];
z[0] = abb[8] * z[0];
z[6] = abb[41] + z[6] + z[71];
z[6] = abb[3] * z[6];
z[0] = z[0] + z[3] + z[6] + z[7] + -z[14] + -z[52];
z[0] = abb[29] * z[0];
z[3] = -abb[30] * z[78];
z[6] = -abb[28] + -abb[30];
z[6] = z[6] * z[20] * z[24];
z[0] = z[0] + z[1] + z[3] + z[4] + z[5] + z[6] + -z[55];
z[0] = m1_set::bc<T>[0] * z[0];
z[1] = abb[40] * z[21];
z[3] = z[36] + -z[54];
z[4] = abb[38] + -abb[39];
z[3] = z[3] * z[4];
z[4] = abb[39] * z[13];
z[5] = -abb[38] * z[46];
z[1] = z[1] + z[3] + z[4] + z[5];
z[0] = z[0] + 3 * z[1];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_342_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("33.271302908052661481167078569208983297177814471470482193923039048"),stof<T>("-36.458798005758148808683659217590063869288248014628839802993076557")}, std::complex<T>{stof<T>("-11.284444247253300634450668680231391920085313346835503668065877295"),stof<T>("-93.978475082956705979382675503518827943607543947852490139337101957")}, std::complex<T>{stof<T>("25.996096719050313163028523272983761939281517389342543909164783863"),stof<T>("1.909467438863238212162253636809716512380364447035697400795706822")}, std::complex<T>{stof<T>("-0.411189830911843253803548841941122335239105254802223610923303082"),stof<T>("-28.517266376639371267559883910526324911154307779614351608082408499")}, std::complex<T>{stof<T>("-5.642222123626650317225334340115695960042656673417751834032938648"),stof<T>("-46.989237541478352989691337751759413971803771973926245069668550978")}, std::complex<T>{stof<T>("-20.592022310759988479797910568414920119275337539329025035832629111"),stof<T>("93.334000971454399545748376888366367415465522573584484962992860093")}, std::complex<T>{stof<T>("-18.547848414472447225081140770364272386182591875816614974184009492"),stof<T>("36.493764361994030802771010192733497973147445917608054297617934235")}, std::complex<T>{stof<T>("-5.231032292714807063421785498174573624803551418615528223109635565"),stof<T>("-18.471971164838981722131453841233089060649464194311893461586142479")}, std::complex<T>{stof<T>("-1.8060261809512156207220481625037935930562688401081771009478357238"),stof<T>("8.5860057406210839747580739222161994862759616092824933712549099219")}, std::complex<T>{stof<T>("-2.301289249236299788229142824963213884607522828012936456197368814"),stof<T>("-23.623397538326071070186375418229279950247593024717894811595338102")}, std::complex<T>{stof<T>("2.301289249236299788229142824963213884607522828012936456197368814"),stof<T>("23.623397538326071070186375418229279950247593024717894811595338102")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real()), C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_342_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_342_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-81.701678748927859828747716895376446215485408716061030251777447478"),stof<T>("53.719723605513469360500246047387446211830147738387670947090835096")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,57> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W23(k,dl), dlog_W25(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_8(k), f_2_14(k), f_2_20(k), f_2_23(k), f_2_2_im(k), f_2_10_im(k), f_2_12_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_10_re(k), f_2_12_re(k)};

                    
            return f_4_342_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_342_DLogXconstant_part(base_point<T>, kend);
	value += f_4_342_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_342_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_342_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_342_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_342_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_342_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_342_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
