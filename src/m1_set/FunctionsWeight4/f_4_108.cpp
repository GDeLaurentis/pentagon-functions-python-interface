/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_108.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_108_abbreviated (const std::array<T,26>& abb) {
T z[36];
z[0] = 2 * abb[21];
z[1] = -abb[18] + z[0];
z[2] = abb[20] * (T(3) / T(2));
z[3] = 3 * abb[19];
z[4] = -abb[22] + z[3];
z[5] = z[1] + -z[2] + (T(-1) / T(2)) * z[4];
z[6] = abb[6] * z[5];
z[7] = abb[19] + -abb[22];
z[8] = abb[20] + z[7];
z[9] = abb[1] * z[8];
z[10] = 2 * z[9];
z[11] = z[6] + z[10];
z[12] = abb[20] * (T(1) / T(2));
z[13] = z[0] + z[12];
z[14] = 5 * abb[18];
z[15] = -abb[19] + 5 * abb[22] + z[14];
z[15] = -z[13] + (T(1) / T(4)) * z[15];
z[15] = abb[2] * z[15];
z[16] = abb[19] + abb[22];
z[17] = abb[18] + z[16];
z[17] = abb[20] + -z[0] + (T(1) / T(2)) * z[17];
z[17] = abb[3] * z[17];
z[18] = -abb[18] + z[7];
z[19] = abb[20] + (T(1) / T(2)) * z[18];
z[20] = abb[0] * (T(1) / T(2));
z[20] = z[19] * z[20];
z[15] = -z[11] + z[15] + -z[17] + -z[20];
z[17] = abb[13] + -abb[14];
z[15] = z[15] * z[17];
z[21] = 2 * abb[18];
z[22] = 4 * abb[21];
z[23] = z[21] + -z[22];
z[4] = 3 * abb[20] + z[4] + z[23];
z[24] = abb[6] * z[4];
z[25] = abb[2] * z[8];
z[26] = z[10] + z[25];
z[23] = abb[20] + z[16] + z[23];
z[27] = abb[3] * z[23];
z[26] = -z[24] + 2 * z[26] + z[27];
z[27] = -abb[12] * z[26];
z[28] = z[6] + z[9];
z[2] = -z[0] + z[2];
z[29] = abb[19] + abb[18] * (T(1) / T(2)) + z[2];
z[29] = abb[3] * z[29];
z[30] = abb[23] + abb[24] + -abb[25];
z[31] = abb[7] * z[30];
z[32] = (T(1) / T(2)) * z[31];
z[7] = abb[18] + z[7];
z[33] = abb[2] * z[7];
z[29] = z[28] + z[29] + -z[32] + (T(1) / T(2)) * z[33];
z[29] = abb[11] * z[29];
z[15] = z[15] + z[27] + z[29];
z[15] = abb[11] * z[15];
z[27] = 3 * abb[18];
z[29] = z[16] + z[27];
z[0] = -z[0] + (T(1) / T(2)) * z[29];
z[0] = abb[3] * z[0];
z[27] = 5 * abb[19] + -abb[22] + z[27];
z[27] = z[2] + (T(1) / T(4)) * z[27];
z[27] = abb[2] * z[27];
z[34] = abb[5] * z[7];
z[27] = z[0] + z[20] + z[27] + z[28] + z[34];
z[28] = prod_pow(abb[14], 2);
z[27] = z[27] * z[28];
z[35] = 3 * abb[22];
z[14] = -7 * abb[19] + -z[14] + z[35];
z[14] = -z[2] + (T(1) / T(4)) * z[14];
z[14] = abb[2] * z[14];
z[0] = -z[0] + -z[11] + z[14] + z[20];
z[0] = abb[14] * z[0];
z[7] = abb[3] * z[7];
z[11] = -abb[0] * z[19];
z[11] = (T(-1) / T(2)) * z[7] + z[9] + z[11] + z[32];
z[11] = abb[13] * z[11];
z[0] = z[0] + z[11];
z[0] = abb[13] * z[0];
z[11] = -z[13] + (T(1) / T(2)) * z[16] + z[21];
z[11] = abb[3] * z[11];
z[13] = 3 * z[9];
z[14] = 2 * z[8];
z[14] = abb[2] * z[14];
z[6] = z[6] + z[11] + z[13] + z[14] + -z[34];
z[6] = abb[12] * z[6];
z[11] = z[17] * z[26];
z[6] = z[6] + z[11];
z[6] = abb[12] * z[6];
z[11] = abb[15] * z[26];
z[14] = -abb[19] + z[35];
z[12] = -z[1] + -z[12] + (T(1) / T(2)) * z[14];
z[12] = abb[12] * z[12];
z[14] = z[17] * z[23];
z[12] = z[12] + z[14];
z[12] = abb[12] * z[12];
z[14] = -abb[11] * z[5];
z[16] = -abb[12] + -z[17];
z[16] = z[16] * z[23];
z[14] = z[14] + z[16];
z[14] = abb[11] * z[14];
z[16] = abb[15] * z[23];
z[12] = z[12] + z[14] + z[16];
z[12] = abb[4] * z[12];
z[14] = z[28] * z[30];
z[16] = abb[14] * z[30];
z[17] = abb[13] * z[16];
z[14] = z[14] + z[17];
z[3] = abb[18] + abb[22] + z[3];
z[2] = z[2] + (T(1) / T(4)) * z[3];
z[2] = abb[17] * z[2];
z[3] = -abb[13] * z[30];
z[3] = z[3] + z[16];
z[17] = abb[11] * z[30];
z[3] = (T(1) / T(2)) * z[3] + -z[17];
z[3] = abb[11] * z[3];
z[2] = z[2] + (T(1) / T(2)) * z[3] + (T(1) / T(4)) * z[14];
z[2] = abb[8] * z[2];
z[3] = z[13] + -z[24] + z[34];
z[3] = abb[16] * z[3];
z[5] = abb[9] * z[5];
z[13] = -abb[10] + abb[0] * (T(3) / T(4));
z[13] = z[13] * z[30];
z[1] = abb[22] + -z[1];
z[1] = abb[7] * z[1];
z[1] = z[1] + z[5] + z[13];
z[1] = abb[17] * z[1];
z[4] = abb[16] * z[4];
z[5] = abb[17] * z[30];
z[4] = z[4] + (T(3) / T(4)) * z[5];
z[4] = abb[2] * z[4];
z[13] = -z[22] + z[29];
z[13] = abb[16] * z[13];
z[5] = (T(1) / T(2)) * z[5] + z[13];
z[5] = abb[3] * z[5];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[11] + z[12] + z[15] + z[27];
z[1] = -z[9] + z[34];
z[2] = -abb[18] + abb[20];
z[2] = abb[3] * z[2];
z[2] = z[1] + z[2] + -z[25];
z[2] = abb[12] * z[2];
z[3] = -abb[11] + abb[12];
z[3] = abb[4] * z[3] * z[8];
z[2] = z[2] + z[3];
z[3] = 2 * abb[20] + z[18];
z[4] = abb[0] * z[3];
z[1] = -2 * z[1] + -z[4] + z[33];
z[1] = abb[14] * z[1];
z[5] = z[10] + z[31];
z[4] = z[4] + -z[5] + z[7];
z[4] = abb[13] * z[4];
z[6] = abb[2] + -abb[3];
z[3] = z[3] * z[6];
z[3] = z[3] + z[5];
z[3] = abb[11] * z[3];
z[5] = -z[16] + z[17];
z[5] = abb[8] * z[5];
z[1] = z[1] + 2 * z[2] + z[3] + z[4] + z[5];
z[1] = m1_set::bc<T>[0] * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_108_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("6.479875848304627511866741355422123189446962452080545763172445338"),stof<T>("-11.9599083993501614498142263483364520947868473501216778200219850332")}, std::complex<T>{stof<T>("8.7869753643208321745289243774528638880816650548321699756505433204"),stof<T>("1.9232461890621132253448052434278699448465971300673805737648824541")}, std::complex<T>{stof<T>("9.943074230822605949179175146114333255646512636540090934717701697"),stof<T>("13.883154588412274675159031591764322039633444480189058393786867487")}, stof<T>("-15.27194942961280257303398424816718511402362006757693344447920743"), std::complex<T>{stof<T>("-1.1510006495144308880119322533692713310698550210437032534109396055"),stof<T>("-1.9232461890621132253448052434278699448465971300673805737648824541")}, std::complex<T>{stof<T>("0.70725444150761250715684158174957616317924720334016741423053196"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("0.70725444150761250715684158174957616317924720334016741423053196"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("-0.70725444150761250715684158174957616317924720334016741423053196"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[123].real()/kbase.W[123].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_108_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_108_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-3.858409355473245655047341853836653814937140635494531170359474413"),stof<T>("-23.989122067033206854956057551276871899532366605929295611204889765")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,26> abb = {dl[0], dl[1], dl[2], dl[4], dlog_W12(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlr[1], f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_23(k), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[123].real()/k.W[123].real())};

                    
            return f_4_108_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_108_DLogXconstant_part(base_point<T>, kend);
	value += f_4_108_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_108_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_108_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_108_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_108_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_108_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_108_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
