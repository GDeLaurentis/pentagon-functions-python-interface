/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_459.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_459_abbreviated (const std::array<T,63>& abb) {
T z[63];
z[0] = m1_set::bc<T>[0] * (T(1) / T(2));
z[1] = abb[32] * z[0];
z[2] = abb[31] * m1_set::bc<T>[0];
z[3] = z[1] + -z[2];
z[4] = -abb[35] + abb[36];
z[5] = abb[28] * (T(1) / T(2));
z[6] = -abb[30] + z[5];
z[7] = m1_set::bc<T>[0] * z[6];
z[8] = abb[37] * (T(1) / T(2));
z[4] = z[3] + (T(1) / T(2)) * z[4] + -z[7] + -z[8];
z[7] = abb[14] * z[4];
z[9] = abb[29] * m1_set::bc<T>[0];
z[10] = abb[32] * m1_set::bc<T>[0];
z[11] = z[9] + -z[10];
z[12] = -3 * z[11];
z[13] = -abb[27] + abb[30] * (T(3) / T(2));
z[14] = m1_set::bc<T>[0] * z[13];
z[15] = abb[36] + abb[39];
z[16] = abb[31] * (T(1) / T(2));
z[17] = m1_set::bc<T>[0] * z[16];
z[18] = z[12] + z[14] + -z[15] + -z[17];
z[19] = abb[6] * (T(1) / T(2));
z[18] = z[18] * z[19];
z[20] = abb[35] + abb[37];
z[21] = -z[9] + z[20];
z[22] = 3 * abb[27];
z[23] = abb[30] * (T(1) / T(2));
z[24] = abb[28] + -z[22] + z[23];
z[24] = m1_set::bc<T>[0] * z[24];
z[24] = 3 * abb[39] + (T(5) / T(2)) * z[2] + z[21] + z[24];
z[25] = abb[3] * (T(1) / T(2));
z[24] = z[24] * z[25];
z[26] = -abb[27] + abb[28];
z[27] = -z[23] + z[26];
z[27] = m1_set::bc<T>[0] * z[27];
z[28] = -abb[36] + z[17];
z[29] = z[27] + z[28];
z[30] = abb[13] * (T(1) / T(2));
z[31] = z[29] * z[30];
z[32] = abb[6] + abb[14];
z[33] = abb[38] * (T(1) / T(2));
z[32] = -z[32] * z[33];
z[34] = abb[35] + abb[39];
z[34] = abb[10] * z[34];
z[35] = abb[42] * (T(1) / T(2)) + -z[34];
z[36] = abb[16] + -abb[18];
z[37] = abb[40] * z[36];
z[18] = -z[7] + z[18] + z[24] + -z[31] + z[32] + z[35] + (T(-1) / T(4)) * z[37];
z[24] = -abb[37] + abb[38] + abb[39];
z[0] = abb[29] * z[0];
z[32] = abb[27] * (T(1) / T(2));
z[37] = -abb[30] + z[32];
z[38] = abb[28] * (T(-1) / T(4)) + -z[37];
z[38] = m1_set::bc<T>[0] * z[38];
z[24] = -z[0] + -z[2] + (T(5) / T(4)) * z[10] + (T(1) / T(4)) * z[24] + z[38];
z[24] = abb[2] * z[24];
z[38] = abb[30] * m1_set::bc<T>[0];
z[38] = abb[37] + z[9] + -z[38];
z[39] = abb[5] * (T(1) / T(4));
z[38] = z[38] * z[39];
z[15] = abb[38] + z[15];
z[40] = z[15] + z[20];
z[41] = abb[7] * z[40];
z[38] = z[38] + (T(1) / T(4)) * z[41];
z[23] = -abb[27] + z[23];
z[41] = m1_set::bc<T>[0] * z[23];
z[42] = -abb[35] + z[41];
z[43] = z[10] + -z[17] + z[42];
z[44] = abb[12] * (T(1) / T(4));
z[45] = z[43] * z[44];
z[46] = abb[15] * (T(1) / T(8));
z[47] = abb[40] * z[46];
z[45] = z[45] + z[47];
z[14] = z[14] + -z[20];
z[20] = (T(3) / T(2)) * z[2];
z[47] = abb[38] + z[10] + z[14] + -z[20];
z[48] = abb[0] * (T(1) / T(2));
z[47] = z[47] * z[48];
z[3] = -z[0] + -z[3] + -z[33];
z[3] = abb[1] * z[3];
z[15] = z[11] + z[15];
z[49] = abb[11] * (T(1) / T(2));
z[50] = z[15] * z[49];
z[51] = abb[39] * (T(1) / T(2));
z[8] = z[8] + z[51];
z[0] = -z[0] + z[8];
z[52] = abb[35] + z[0] + z[1];
z[52] = abb[8] * z[52];
z[9] = abb[38] + -z[2] + z[9];
z[53] = abb[4] * (T(1) / T(4));
z[54] = z[9] * z[53];
z[55] = abb[17] * abb[40];
z[3] = z[3] + (T(1) / T(2)) * z[18] + z[24] + -z[38] + z[45] + z[47] + z[50] + z[52] + z[54] + (T(-1) / T(8)) * z[55];
z[3] = abb[54] * z[3];
z[18] = abb[19] * z[43];
z[14] = -z[11] + z[14] + -z[17];
z[24] = abb[21] * z[14];
z[47] = abb[23] * z[14];
z[54] = abb[22] * z[40];
z[55] = abb[20] * z[40];
z[56] = abb[24] * z[29];
z[57] = abb[26] * (T(1) / T(2));
z[58] = abb[40] * z[57];
z[18] = z[18] + z[24] + z[47] + -z[54] + -z[55] + z[56] + -z[58];
z[4] = z[4] + z[33];
z[4] = abb[25] * z[4];
z[4] = -z[4] + (T(1) / T(2)) * z[18];
z[18] = abb[44] * (T(1) / T(2));
z[24] = -z[4] * z[18];
z[47] = -abb[27] + z[5];
z[47] = m1_set::bc<T>[0] * z[47];
z[8] = z[1] + z[8] + z[33] + z[47];
z[8] = abb[2] * z[8];
z[20] = abb[39] + z[20] + z[21] + z[27];
z[20] = abb[3] * z[20];
z[21] = abb[16] + abb[18];
z[27] = abb[40] * (T(1) / T(2));
z[47] = z[21] * z[27];
z[20] = abb[42] + z[20] + z[47];
z[47] = abb[30] * (T(5) / T(2)) + -z[22];
z[47] = m1_set::bc<T>[0] * z[47];
z[12] = abb[39] + z[12] + z[28] + z[47];
z[12] = z[12] * z[19];
z[19] = abb[6] + -abb[14];
z[19] = -z[19] * z[33];
z[28] = abb[17] * (T(1) / T(4));
z[33] = abb[40] * z[28];
z[7] = z[7] + z[8] + z[12] + z[19] + (T(1) / T(2)) * z[20] + -z[31] + z[33];
z[8] = abb[1] * (T(1) / T(2));
z[12] = -z[8] + z[53];
z[9] = z[9] * z[12];
z[0] = z[0] + z[10] + z[41];
z[0] = abb[8] * z[0];
z[12] = z[29] * z[48];
z[0] = z[0] + (T(1) / T(2)) * z[7] + z[9] + z[12] + -z[38] + -z[45];
z[0] = abb[53] * z[0];
z[7] = abb[39] + z[17] + z[41];
z[7] = abb[3] * z[7];
z[9] = abb[16] * z[27];
z[7] = z[7] + -z[9] + z[35];
z[12] = abb[27] + -abb[30];
z[19] = m1_set::bc<T>[0] * z[12];
z[11] = -z[11] + -z[19] + z[51];
z[11] = abb[2] * z[11];
z[14] = z[14] * z[48];
z[2] = -z[2] + z[10];
z[2] = z[2] * z[8];
z[7] = -z[2] + (T(1) / T(2)) * z[7] + z[11] + z[14] + z[52];
z[10] = -abb[52] * z[7];
z[7] = abb[50] * z[7];
z[11] = abb[14] * z[15];
z[11] = z[11] + -z[34];
z[14] = z[40] * z[48];
z[11] = (T(1) / T(2)) * z[11] + -z[14] + -z[50] + z[52];
z[14] = abb[49] + abb[51];
z[11] = -z[11] * z[14];
z[0] = z[0] + z[3] + -z[7] + z[10] + z[11] + z[24];
z[3] = abb[45] + -abb[46];
z[7] = -abb[47] + z[3];
z[7] = abb[43] * (T(-1) / T(8)) + (T(-1) / T(16)) * z[7];
z[4] = z[4] * z[7];
z[7] = -z[43] * z[48];
z[10] = -z[17] + -z[42];
z[10] = abb[3] * z[10];
z[9] = z[9] + z[10];
z[1] = z[1] + z[42];
z[1] = abb[8] * z[1];
z[1] = z[1] + z[2] + z[7] + (T(1) / T(2)) * z[9];
z[2] = abb[48] * (T(1) / T(8));
z[1] = z[1] * z[2];
z[0] = abb[61] + (T(1) / T(8)) * z[0] + z[1] + z[4];
z[1] = -abb[30] + abb[31];
z[4] = abb[29] * (T(1) / T(4));
z[7] = -z[1] + -z[4] + -z[32];
z[7] = abb[29] * z[7];
z[9] = z[6] + -z[32];
z[9] = abb[28] * z[9];
z[10] = z[5] + z[23];
z[10] = abb[31] * z[10];
z[11] = abb[33] + abb[34];
z[11] = (T(1) / T(2)) * z[11];
z[15] = abb[57] * (T(1) / T(2));
z[17] = abb[59] * (T(1) / T(2));
z[19] = abb[30] + abb[27] * (T(-1) / T(4));
z[19] = abb[27] * z[19];
z[20] = abb[29] + abb[31] + -abb[32] + (T(3) / T(2)) * z[12];
z[20] = abb[32] * z[20];
z[7] = z[7] + z[9] + z[10] + z[11] + z[15] + -z[17] + z[19] + z[20];
z[7] = abb[2] * z[7];
z[9] = -abb[27] + abb[31];
z[9] = abb[29] * z[9];
z[10] = abb[27] * abb[28];
z[19] = abb[33] + z[10];
z[9] = z[9] + z[19];
z[20] = abb[32] * (T(1) / T(2));
z[24] = -abb[31] + z[20];
z[27] = abb[32] * z[24];
z[29] = abb[55] + abb[57];
z[31] = abb[34] + z[29];
z[33] = prod_pow(m1_set::bc<T>[0], 2);
z[23] = abb[27] * z[23];
z[22] = -abb[30] + z[22];
z[22] = -abb[28] + (T(1) / T(2)) * z[22];
z[22] = abb[31] * z[22];
z[22] = -3 * abb[59] + z[9] + z[22] + z[23] + z[27] + -z[31] + (T(-23) / T(6)) * z[33];
z[22] = z[22] * z[25];
z[23] = z[12] * z[16];
z[25] = -abb[34] + z[23];
z[34] = abb[29] + z[12];
z[34] = abb[29] * z[34];
z[34] = -abb[56] + z[34];
z[35] = abb[30] * z[32];
z[35] = abb[59] + z[35];
z[6] = abb[28] * z[6];
z[6] = abb[33] + z[6];
z[38] = z[6] + -z[25] + (T(7) / T(2)) * z[33] + -z[34] + z[35];
z[40] = -abb[29] + z[20];
z[41] = (T(1) / T(2)) * z[12] + -z[40];
z[41] = abb[32] * z[41];
z[38] = (T(1) / T(2)) * z[38] + z[41];
z[38] = abb[6] * z[38];
z[24] = -z[12] + z[24];
z[24] = abb[32] * z[24];
z[1] = abb[29] * z[1];
z[1] = z[1] + z[24] + -z[29];
z[42] = abb[27] + abb[30];
z[5] = -z[5] + z[42];
z[5] = abb[28] * z[5];
z[43] = abb[27] * abb[30];
z[26] = abb[31] * z[26];
z[5] = z[1] + z[5] + -z[26] + -z[43];
z[26] = abb[56] + z[5];
z[26] = abb[14] * z[26];
z[43] = z[32] * z[42];
z[42] = -abb[28] + (T(1) / T(2)) * z[42];
z[42] = abb[31] * z[42];
z[45] = (T(1) / T(3)) * z[33];
z[19] = abb[56] + z[19] + z[42] + -z[43] + z[45];
z[30] = -z[19] * z[30];
z[43] = abb[60] * (T(1) / T(4));
z[36] = z[36] * z[43];
z[43] = abb[58] * (T(1) / T(2));
z[47] = abb[2] + -abb[6];
z[50] = abb[14] + -z[47];
z[50] = z[43] * z[50];
z[28] = abb[60] * z[28];
z[51] = abb[55] + abb[59] + (T(11) / T(6)) * z[33];
z[51] = abb[10] * z[51];
z[52] = abb[62] * (T(1) / T(2)) + -z[51];
z[7] = z[7] + z[22] + (T(1) / T(2)) * z[26] + z[28] + z[30] + z[36] + z[38] + z[50] + -z[52];
z[22] = z[20] * z[40];
z[15] = z[15] + z[17];
z[17] = abb[29] * (T(1) / T(2));
z[30] = abb[27] * z[17];
z[36] = (T(1) / T(2)) * z[33];
z[38] = prod_pow(abb[27], 2);
z[22] = -abb[55] + -z[15] + -z[22] + -z[30] + -z[36] + (T(1) / T(4)) * z[38];
z[22] = abb[8] * z[22];
z[30] = (T(1) / T(6)) * z[33];
z[50] = -abb[30] + z[17];
z[50] = abb[29] * z[50];
z[54] = abb[57] + z[6] + z[30] + -z[50];
z[39] = z[39] * z[54];
z[54] = abb[56] + abb[58];
z[38] = (T(1) / T(2)) * z[38];
z[55] = -abb[55] + -abb[57] + -abb[59] + z[38] + -z[54];
z[56] = -abb[27] + z[17];
z[4] = z[4] * z[56];
z[4] = z[4] + -z[45] + (T(1) / T(4)) * z[55];
z[55] = abb[7] * z[4];
z[58] = -abb[31] + z[17];
z[58] = abb[29] * z[58];
z[58] = -abb[58] + -z[30] + z[58];
z[27] = abb[34] + z[27];
z[59] = -z[27] + z[58];
z[53] = z[53] * z[59];
z[39] = z[39] + z[53] + -z[55];
z[32] = z[12] * z[32];
z[23] = z[23] + z[32];
z[1] = -abb[58] + -z[1] + z[6] + -z[23] + z[45];
z[1] = z[1] * z[48];
z[53] = prod_pow(abb[29], 2);
z[53] = (T(13) / T(3)) * z[33] + -z[53];
z[55] = abb[32] * z[40];
z[53] = abb[59] + (T(1) / T(2)) * z[53] + z[54] + -z[55];
z[49] = z[49] * z[53];
z[55] = abb[31] * z[12];
z[24] = -abb[34] + z[24] + z[55] + -z[58];
z[8] = z[8] * z[24];
z[24] = abb[32] * z[12];
z[58] = -z[23] + z[24];
z[60] = abb[55] + z[45];
z[61] = abb[34] + z[60];
z[62] = z[58] + z[61];
z[44] = z[44] * z[62];
z[46] = abb[60] * z[46];
z[1] = z[1] + (T(1) / T(2)) * z[7] + z[8] + z[22] + z[39] + z[44] + -z[46] + -z[49];
z[1] = abb[54] * z[1];
z[7] = z[12] * z[20];
z[8] = z[10] + -z[38];
z[10] = -abb[28] + abb[30];
z[10] = z[10] * z[16];
z[16] = z[17] * z[56];
z[8] = z[7] + (T(1) / T(2)) * z[8] + z[10] + z[11] + -z[15] + z[16] + -z[45];
z[8] = abb[2] * z[8];
z[9] = z[9] + z[27] + -z[29] + (T(-5) / T(6)) * z[33] + -z[35] + z[42];
z[9] = abb[3] * z[9];
z[10] = -abb[13] * z[19];
z[11] = abb[12] * z[62];
z[15] = abb[60] * (T(1) / T(2));
z[16] = -z[15] * z[21];
z[9] = -abb[62] + z[9] + z[10] + -z[11] + z[16] + -z[26];
z[10] = z[6] + z[36];
z[11] = abb[27] * z[13];
z[11] = -abb[59] + z[10] + z[11] + z[25] + -z[34];
z[11] = (T(1) / T(2)) * z[11] + z[41];
z[11] = abb[6] * z[11];
z[13] = abb[0] * z[19];
z[16] = -z[12] + z[40];
z[16] = abb[32] * z[16];
z[20] = abb[27] * z[37];
z[16] = abb[59] + z[16] + z[20];
z[20] = -abb[27] * abb[29];
z[20] = -abb[57] + -z[16] + z[20] + -z[45];
z[20] = abb[8] * z[20];
z[21] = -abb[14] + -z[47];
z[21] = z[21] * z[43];
z[26] = -abb[1] * z[59];
z[8] = z[8] + (T(1) / T(2)) * z[9] + z[11] + z[13] + z[20] + z[21] + z[26] + -z[28];
z[8] = (T(1) / T(2)) * z[8] + z[39] + z[46];
z[8] = abb[53] * z[8];
z[1] = z[1] + z[8];
z[8] = z[12] + z[17];
z[8] = abb[29] * z[8];
z[6] = -z[6] + z[8] + z[16] + z[30];
z[6] = abb[2] * z[6];
z[8] = abb[27] * z[12];
z[8] = z[8] + 3 * z[33] + -z[55];
z[8] = abb[34] + abb[59] + (T(1) / T(2)) * z[8];
z[8] = abb[3] * z[8];
z[9] = abb[16] * z[15];
z[6] = z[6] + z[8] + -z[9] + z[52];
z[8] = -z[10] + -z[31] + z[50] + -z[58];
z[10] = z[8] * z[48];
z[11] = z[7] + -z[25];
z[11] = abb[1] * z[11];
z[6] = (T(1) / T(2)) * z[6] + z[10] + z[11] + -z[22];
z[10] = abb[50] + abb[52];
z[10] = (T(1) / T(2)) * z[10];
z[6] = z[6] * z[10];
z[10] = abb[21] * z[8];
z[8] = abb[23] * z[8];
z[12] = abb[24] * z[19];
z[13] = -z[23] + z[24] + z[61];
z[13] = abb[19] * z[13];
z[15] = abb[60] * z[57];
z[8] = -z[8] + -z[10] + z[12] + z[13] + z[15];
z[5] = z[5] + z[54];
z[10] = abb[25] * (T(1) / T(4));
z[5] = z[5] * z[10];
z[10] = -abb[20] + -abb[22];
z[10] = z[4] * z[10];
z[5] = z[5] + (T(1) / T(4)) * z[8] + z[10];
z[3] = -abb[43] + (T(-1) / T(2)) * z[3] + -z[18];
z[3] = z[3] * z[5];
z[8] = abb[14] * z[53];
z[8] = z[8] + -z[51];
z[8] = (T(1) / T(2)) * z[8] + -z[22] + -z[49];
z[4] = abb[0] * z[4];
z[4] = z[4] + (T(1) / T(2)) * z[8];
z[4] = z[4] * z[14];
z[1] = (T(1) / T(2)) * z[1] + z[3] + z[4] + z[6];
z[3] = abb[47] * z[5];
z[4] = z[32] + -z[60];
z[5] = z[4] + -z[25];
z[5] = abb[3] * z[5];
z[5] = z[5] + -z[9];
z[4] = -z[4] + z[7];
z[4] = abb[8] * z[4];
z[6] = -z[48] * z[62];
z[4] = z[4] + (T(1) / T(2)) * z[5] + z[6] + z[11];
z[2] = z[2] * z[4];
z[1] = abb[41] + (T(1) / T(4)) * z[1] + z[2] + (T(1) / T(8)) * z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_459_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-0.51495069688577283804828303384193319901645041786205821333278020155"),stof<T>("0.1680781571205259684783209532609898001018838699352932802170601115")}, std::complex<T>{stof<T>("-0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("-0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("-0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("0.25747534844288641902414151692096659950822520893102910666639010078"),stof<T>("-0.08403907856026298423916047663049490005094193496764664010853005575")}, std::complex<T>{stof<T>("0.04416491205478169634732093462559222042060343709867799365733542925"),stof<T>("0.22004800893372987519086463530647671709403933474386728638382060846")}, std::complex<T>{stof<T>("-1.8894418669371645730204993893283593184686245560274281144335764771"),stof<T>("1.471792070798879671164463795263226221586895519171419839578721322")}, std::complex<T>{stof<T>("-0.56161927387202718910260778470491686084587230181460701077852007547"),stof<T>("0.50913672958225253873555512404282689927147925565251630299982334655")}, std::complex<T>{stof<T>("-1.8894418669371645730204993893283593184686245560274281144335764771"),stof<T>("1.471792070798879671164463795263226221586895519171419839578721322")}, std::complex<T>{stof<T>("-0.56161927387202718910260778470491686084587230181460701077852007547"),stof<T>("0.50913672958225253873555512404282689927147925565251630299982334655")}, std::complex<T>{stof<T>("-0.020898306371438252515741060746933841823397787250839508815725475101"),stof<T>("-0.13574022450088939322201724851925175755513123840040319374065701043")}, std::complex<T>{stof<T>("-0.0762911296698978992321854584363258363070662741253910680252919535"),stof<T>("-0.28234313269784647430148255327235801478488005727145989535697007576")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[32].real()/kbase.W[32].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_459_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_459_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(128)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(32)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[48] + abb[50] + abb[52]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[48] + abb[50] + abb[52];
z[1] = abb[31] + -abb[32];
return abb[9] * m1_set::bc<T>[0] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_459_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(128)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (-8 + -7 * v[0] + -v[1] + -v[2] + 3 * v[3] + 4 * v[4] + m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + v[5])) / prod_pow(tend, 2);
c[1] = ((-2 + m1_set::bc<T>[1]) * (T(-1) / T(32)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (abb[48] + abb[50] + abb[52]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[31] + abb[32];
z[0] = (T(1) / T(2)) * z[0];
z[1] = -abb[27] + abb[30];
z[0] = z[0] * z[1];
z[0] = -abb[34] + z[0];
z[1] = abb[48] + abb[50] + abb[52];
return abb[9] * (T(1) / T(8)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_459_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.4017171165845835044469807367406551350310648587238345235749258873"),stof<T>("1.9627230957397030912488869930680979285028291252356113868661741411")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,63> abb = {dl[0], dlog_W4(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W14(k,dl), dlog_W17(k,dl), dlog_W22(k,dl), dlog_W25(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_4(k), f_1_5(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_13(k), f_2_15(k), f_2_2_im(k), f_2_4_im(k), f_2_7_im(k), f_2_12_im(k), f_2_22_im(k), f_2_25_im(k), T{0}, T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[32].real()/k.W[32].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_2_re(k), f_2_4_re(k), f_2_7_re(k), f_2_12_re(k), f_2_22_re(k), f_2_25_re(k), T{0}, T{0}};
abb[41] = SpDLog_f_4_459_W_17_Im(t, path, abb);
abb[42] = SpDLogQ_W_82(k,dl,dlr).imag();
abb[61] = SpDLog_f_4_459_W_17_Re(t, path, abb);
abb[62] = SpDLogQ_W_82(k,dl,dlr).real();

                    
            return f_4_459_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_459_DLogXconstant_part(base_point<T>, kend);
	value += f_4_459_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_459_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_459_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_459_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_459_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_459_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_459_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
