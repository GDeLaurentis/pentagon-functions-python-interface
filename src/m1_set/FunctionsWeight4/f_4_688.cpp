/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_688.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_688_abbreviated (const std::array<T,37>& abb) {
T z[114];
z[0] = 8 * abb[28];
z[1] = 2 * abb[25];
z[2] = z[0] + z[1];
z[3] = 7 * abb[31];
z[4] = 9 * abb[32];
z[5] = 7 * abb[30] + -z[4];
z[6] = 3 * abb[36];
z[7] = 3 * abb[27];
z[8] = 3 * abb[29];
z[9] = -9 * abb[33] + 15 * abb[34] + -z[2] + -z[3] + -z[5] + z[6] + -z[7] + -z[8];
z[9] = abb[9] * z[9];
z[10] = abb[30] + abb[34];
z[11] = -abb[31] + abb[36];
z[12] = -abb[29] + -abb[33] + z[11];
z[13] = abb[27] + abb[32];
z[14] = 2 * abb[28];
z[15] = z[1] + -z[10] + -z[12] + -z[13] + z[14];
z[15] = abb[0] * z[15];
z[16] = 2 * abb[27];
z[17] = abb[25] + z[14];
z[18] = z[16] + z[17];
z[19] = 4 * abb[31];
z[20] = 3 * abb[34];
z[21] = 2 * abb[30];
z[22] = abb[29] + z[21];
z[23] = 2 * abb[32];
z[24] = -z[18] + z[19] + -z[20] + z[22] + z[23];
z[24] = abb[2] * z[24];
z[25] = 6 * abb[36];
z[26] = 6 * abb[28];
z[27] = 7 * abb[34];
z[28] = z[25] + -z[26] + z[27];
z[29] = 6 * abb[33];
z[30] = 2 * abb[31];
z[31] = abb[29] + z[30];
z[32] = 4 * abb[27];
z[33] = abb[25] + -z[28] + z[29] + z[31] + z[32];
z[33] = abb[6] * z[33];
z[34] = 5 * abb[29];
z[35] = 6 * abb[31];
z[36] = 2 * abb[33];
z[37] = abb[25] + z[36];
z[28] = -z[28] + z[34] + z[35] + z[37];
z[28] = abb[8] * z[28];
z[38] = -abb[25] + abb[27];
z[39] = 4 * abb[34];
z[40] = z[38] + z[39];
z[41] = 3 * abb[33];
z[42] = 3 * abb[31];
z[43] = z[41] + z[42];
z[44] = 4 * abb[30];
z[45] = -abb[36] + -z[23] + -z[40] + z[43] + z[44];
z[46] = 2 * abb[3];
z[45] = z[45] * z[46];
z[47] = 9 * abb[34];
z[48] = abb[29] + z[14];
z[49] = -abb[25] + z[16];
z[50] = 8 * abb[32] + z[44] + -z[47] + -z[48] + -z[49];
z[51] = -abb[7] * z[50];
z[52] = z[14] + z[22] + z[49];
z[53] = z[23] + -z[27] + z[52];
z[53] = abb[5] * z[53];
z[54] = -abb[34] + z[36];
z[6] = abb[28] + abb[29] + -z[6] + z[42] + z[54];
z[6] = abb[10] * z[6];
z[55] = abb[36] + z[20];
z[56] = -abb[31] + -z[41] + z[55];
z[57] = -abb[29] + abb[30] + z[13] + -z[56];
z[57] = abb[4] * z[57];
z[58] = -abb[29] + -abb[34] + z[37];
z[58] = abb[1] * z[58];
z[9] = -2 * z[6] + z[9] + z[15] + z[24] + z[28] + z[33] + z[45] + z[51] + z[53] + z[57] + z[58];
z[9] = abb[23] * z[9];
z[15] = 2 * abb[36];
z[24] = z[15] + z[39];
z[45] = -abb[25] + z[24];
z[51] = 8 * abb[30];
z[57] = 6 * abb[32];
z[58] = z[51] + -z[57];
z[59] = 4 * abb[26];
z[60] = -z[8] + z[59];
z[61] = 5 * abb[33];
z[62] = -z[42] + z[45] + -z[58] + z[60] + -z[61];
z[62] = abb[3] * z[62];
z[63] = 4 * abb[33];
z[64] = 3 * abb[26];
z[65] = z[63] + -z[64];
z[66] = 2 * abb[29];
z[67] = z[17] + z[66];
z[58] = z[16] + -z[27] + z[58] + z[65] + z[67];
z[58] = abb[9] * z[58];
z[58] = z[58] + z[62];
z[62] = z[8] + -z[15];
z[68] = 12 * abb[32] + -z[26] + -z[32];
z[69] = -abb[25] + z[39];
z[70] = abb[31] + abb[33];
z[71] = -z[62] + z[68] + -z[69] + -z[70];
z[71] = abb[4] * z[71];
z[72] = 5 * abb[34];
z[25] = z[25] + z[72];
z[73] = 3 * abb[28];
z[74] = -z[66] + -z[73];
z[75] = 5 * abb[31];
z[76] = 2 * abb[26];
z[74] = abb[33] + z[25] + 2 * z[74] + -z[75] + -z[76];
z[74] = abb[8] * z[74];
z[6] = 4 * z[6];
z[77] = abb[26] + -abb[30];
z[0] = -abb[25] + -7 * abb[29] + -10 * abb[33] + z[0] + z[47] + 16 * z[77];
z[0] = abb[1] * z[0];
z[47] = z[15] + z[20];
z[78] = z[47] + -z[76];
z[79] = -abb[25] + z[19];
z[80] = -z[48] + z[78] + -z[79];
z[80] = abb[0] * z[80];
z[0] = z[0] + z[6] + -z[33] + 2 * z[58] + z[71] + z[74] + z[80];
z[0] = abb[21] * z[0];
z[33] = -abb[29] + z[23];
z[58] = 5 * abb[30];
z[71] = -abb[35] + z[58];
z[41] = z[24] + z[33] + -z[41] + -z[71] + -z[75];
z[41] = abb[3] * z[41];
z[74] = -z[17] + z[27];
z[75] = abb[27] + abb[29];
z[80] = -z[23] + z[75];
z[81] = z[30] + z[36];
z[82] = z[71] + -z[74] + z[80] + z[81];
z[82] = abb[9] * z[82];
z[41] = z[41] + z[82];
z[83] = 4 * abb[35];
z[84] = 4 * abb[32];
z[85] = z[83] + z[84];
z[86] = 5 * abb[27];
z[87] = z[1] + -z[27] + -z[31] + z[85] + -z[86];
z[87] = abb[2] * z[87];
z[88] = abb[27] + abb[33];
z[89] = 2 * abb[35];
z[62] = 8 * abb[25] + -10 * abb[30] + -9 * abb[31] + 12 * abb[34] + z[14] + -z[62] + -z[88] + -z[89];
z[62] = abb[0] * z[62];
z[47] = abb[29] + -z[18] + z[47] + -z[63];
z[47] = abb[4] * z[47];
z[73] = -abb[25] + -z[16] + -z[73];
z[25] = abb[31] + z[25] + -z[61] + 2 * z[73];
z[25] = abb[6] * z[25];
z[6] = z[6] + z[25] + -z[28] + 2 * z[41] + z[47] + z[62] + z[87];
z[6] = abb[19] * z[6];
z[25] = z[30] + z[66];
z[5] = -abb[34] + z[5] + z[25] + z[49] + z[65];
z[5] = abb[9] * z[5];
z[28] = 6 * abb[34];
z[41] = z[28] + -z[63];
z[47] = -z[14] + z[30];
z[61] = z[41] + -z[47];
z[62] = -abb[29] + z[64];
z[65] = z[23] + -z[51] + z[61] + z[62];
z[65] = abb[3] * z[65];
z[65] = z[5] + z[65];
z[73] = 8 * abb[33];
z[26] = -abb[25] + z[26] + z[27] + -z[34] + -z[73] + 12 * z[77];
z[26] = abb[1] * z[26];
z[87] = 3 * abb[25];
z[16] = -z[16] + z[87];
z[90] = z[19] + -z[28];
z[91] = abb[32] + 2 * z[16] + -z[58] + -z[62] + -z[90];
z[91] = abb[0] * z[91];
z[92] = 3 * abb[30];
z[74] = 7 * abb[32] + z[64] + -z[66] + -z[74] + -z[92];
z[74] = abb[5] * z[74];
z[52] = -abb[34] + -z[52] + z[57];
z[52] = abb[4] * z[52];
z[26] = z[26] + z[52] + 2 * z[65] + z[74] + z[91];
z[26] = abb[20] * z[26];
z[52] = z[7] + z[14] + -z[23] + -z[37] + -z[44] + -z[90];
z[52] = abb[3] * z[52];
z[65] = 5 * abb[32];
z[54] = -abb[35] + z[44] + z[54] + -z[65] + z[75] + z[79];
z[54] = abb[9] * z[54];
z[52] = z[52] + z[54];
z[54] = -abb[25] + z[22];
z[27] = -8 * abb[31] + z[27] + -z[54] + -z[68] + z[89];
z[27] = abb[2] * z[27];
z[68] = z[89] + z[92];
z[74] = 4 * abb[25];
z[79] = 15 * abb[32] + -23 * abb[34] + z[48] + z[68] + z[74] + -z[86];
z[79] = abb[7] * z[79];
z[91] = 3 * abb[32];
z[93] = -abb[25] + z[66];
z[41] = -abb[27] + -abb[30] + z[41] + -z[91] + z[93];
z[41] = abb[4] * z[41];
z[94] = abb[29] + z[17];
z[95] = -abb[34] + z[84] + -z[94];
z[95] = abb[0] * z[95];
z[27] = z[27] + z[41] + 2 * z[52] + -z[53] + z[79] + z[95];
z[27] = abb[22] * z[27];
z[40] = -z[21] + -z[23] + z[40];
z[40] = abb[14] * z[40];
z[41] = -z[39] + z[84];
z[52] = -abb[30] + abb[35];
z[53] = z[41] + -z[49] + z[52];
z[79] = abb[16] * z[53];
z[40] = z[40] + -z[79];
z[53] = -abb[17] * z[53];
z[53] = -z[40] + z[53];
z[79] = z[14] + z[75];
z[95] = 2 * abb[34];
z[96] = z[4] + -z[58] + -2 * z[79] + z[95];
z[96] = abb[18] * z[96];
z[53] = 2 * z[53] + z[96];
z[53] = abb[18] * z[53];
z[50] = abb[20] * z[50];
z[96] = 10 * abb[34];
z[94] = -abb[27] + z[94];
z[68] = -abb[32] + z[68] + 2 * z[94] + -z[96];
z[68] = abb[17] * z[68];
z[40] = 2 * z[40] + z[68];
z[40] = abb[17] * z[40];
z[40] = z[40] + z[50] + z[53];
z[40] = abb[7] * z[40];
z[50] = z[36] + z[84] + z[87];
z[53] = abb[27] + z[14];
z[68] = z[50] + -z[53] + -z[62] + -z[89];
z[68] = abb[11] * z[68];
z[89] = z[64] + -z[75] + z[84];
z[94] = abb[35] + z[92];
z[97] = z[36] + z[94];
z[98] = -z[30] + -z[89] + z[97];
z[98] = abb[13] * z[98];
z[50] = -z[25] + z[50] + -z[94];
z[50] = abb[12] * z[50];
z[50] = z[50] + z[68] + z[98];
z[50] = abb[24] * z[50];
z[68] = abb[28] + abb[34];
z[98] = -abb[33] + z[68];
z[99] = -z[22] + z[76] + z[98];
z[100] = abb[1] * z[99];
z[101] = abb[26] + z[38];
z[98] = -z[23] + z[98] + z[101];
z[102] = abb[4] * z[98];
z[68] = -abb[31] + z[68];
z[54] = -abb[26] + z[54] + -z[68];
z[103] = abb[0] * z[54];
z[104] = -abb[27] + z[23];
z[68] = -z[68] + z[104];
z[105] = abb[2] * z[68];
z[106] = -abb[29] + z[101];
z[106] = abb[3] * z[106];
z[100] = -z[100] + z[102] + -z[103] + z[105] + -z[106];
z[102] = prod_pow(abb[16], 2) * z[100];
z[0] = z[0] + z[6] + z[9] + z[26] + z[27] + z[40] + z[50] + -2 * z[102];
z[6] = z[41] + -z[59] + z[81];
z[9] = abb[25] + z[66];
z[26] = 4 * abb[28];
z[27] = z[6] + z[9] + -z[26] + -z[32] + z[94];
z[27] = abb[3] * z[27];
z[40] = z[14] + z[24];
z[50] = -z[1] + z[104];
z[94] = abb[26] + -abb[29];
z[19] = -z[19] + z[40] + -z[50] + z[94] + -z[97];
z[19] = abb[0] * z[19];
z[97] = z[15] + z[95];
z[102] = -z[70] + z[97];
z[67] = abb[26] + -z[67] + z[102];
z[67] = abb[8] * z[67];
z[103] = 4 * abb[1];
z[103] = z[99] * z[103];
z[107] = 2 * z[67] + -z[103];
z[108] = -z[26] + z[71];
z[109] = -z[1] + z[24] + -z[89] + z[108];
z[109] = abb[9] * z[109];
z[110] = -abb[26] + -z[15] + z[17] + z[36] + z[80];
z[110] = abb[4] * z[110];
z[19] = z[19] + z[27] + 4 * z[105] + z[107] + -z[109] + z[110];
z[19] = abb[16] * z[19];
z[27] = 4 * abb[36];
z[110] = z[27] + -z[35];
z[64] = abb[35] + z[64];
z[2] = -abb[30] + -z[2] + -z[29] + z[57] + -z[64] + -z[75] + z[96] + z[110];
z[2] = abb[9] * z[2];
z[58] = abb[35] + z[58];
z[59] = -10 * abb[32] + z[26] + -z[39] + z[58] + z[59] + z[81] + -z[93];
z[59] = abb[3] * z[59];
z[96] = abb[26] + z[52];
z[111] = z[1] + -z[23] + z[36] + z[79] + -z[95] + -z[96];
z[111] = abb[0] * z[111];
z[61] = abb[26] + -z[8] + -z[38] + z[57] + -z[61];
z[61] = abb[4] * z[61];
z[102] = -z[79] + z[102];
z[102] = abb[6] * z[102];
z[13] = -abb[35] + z[13];
z[112] = abb[2] * z[13];
z[113] = -abb[32] + abb[34];
z[113] = abb[5] * z[113];
z[112] = 2 * z[102] + 2 * z[112] + 8 * z[113];
z[2] = z[2] + z[59] + z[61] + -z[107] + z[111] + -z[112];
z[2] = abb[14] * z[2];
z[51] = z[36] + z[51];
z[59] = -abb[35] + z[91];
z[18] = z[18] + -z[51] + z[59] + -z[66] + z[76] + -z[90];
z[18] = abb[3] * z[18];
z[61] = 4 * abb[29];
z[29] = z[24] + -z[29] + -z[53] + z[61] + -z[76] + -z[87];
z[29] = abb[4] * z[29];
z[62] = 2 * z[17] + z[62];
z[27] = -z[27] + z[62] + z[63] + z[90];
z[27] = abb[8] * z[27];
z[90] = 2 * abb[1];
z[107] = z[90] * z[99];
z[105] = 2 * z[105] + -z[107];
z[97] = z[81] + -z[97];
z[80] = -z[80] + z[96] + z[97];
z[80] = abb[0] * z[80];
z[18] = z[18] + z[27] + z[29] + z[80] + -z[105] + z[109];
z[18] = abb[17] * z[18];
z[27] = z[17] + -z[95];
z[11] = -abb[33] + -z[11] + z[27] + -z[44] + z[89];
z[11] = abb[9] * z[11];
z[29] = z[84] + -z[95];
z[80] = abb[36] + z[29] + -z[79];
z[80] = abb[4] * z[80];
z[84] = z[46] * z[99];
z[12] = z[12] + z[101];
z[12] = abb[0] * z[12];
z[11] = z[11] + z[12] + z[67] + -z[80] + -z[84] + -z[103] + 4 * z[113];
z[12] = 2 * abb[15];
z[67] = z[11] * z[12];
z[2] = z[2] + z[18] + z[19] + z[67];
z[18] = 2 * abb[17];
z[2] = z[2] * z[18];
z[19] = abb[30] + z[36];
z[8] = z[4] + -z[8] + z[19] + -z[26] + -z[49] + -z[72];
z[8] = abb[4] * z[8];
z[49] = -abb[29] + z[76];
z[16] = z[16] + z[20] + -z[21] + -z[30] + -z[49];
z[16] = abb[0] * z[16];
z[51] = abb[25] + 8 * abb[26] + z[20] + z[26] + -z[34] + -z[51];
z[51] = z[51] * z[90];
z[67] = abb[3] * z[99];
z[34] = -5 * abb[26] + z[34] + -z[81] + z[95];
z[34] = abb[8] * z[34];
z[62] = z[62] + -z[92];
z[4] = z[4] + -z[28] + -z[62];
z[4] = abb[5] * z[4];
z[4] = z[4] + z[5] + z[8] + z[16] + z[34] + z[51] + 4 * z[67];
z[4] = abb[15] * z[4];
z[5] = abb[14] + -abb[16];
z[5] = z[5] * z[11];
z[4] = z[4] + 2 * z[5];
z[4] = z[4] * z[12];
z[5] = abb[26] + -z[30] + z[45] + -z[57] + -z[63] + z[79];
z[5] = abb[4] * z[5];
z[6] = z[6] + z[93] + z[108];
z[6] = abb[3] * z[6];
z[8] = z[58] + z[75];
z[11] = -6 * abb[25] + abb[26] + z[8] + z[35] + -z[40];
z[11] = abb[0] * z[11];
z[5] = z[5] + z[6] + z[11] + -z[103] + -z[109] + z[112];
z[6] = 2 * abb[16];
z[5] = z[5] * z[6];
z[11] = abb[32] + z[95];
z[12] = 6 * abb[30];
z[15] = -z[11] + z[12] + -z[14] + z[15] + -z[30] + -z[64] + z[75];
z[15] = abb[9] * z[15];
z[16] = z[39] + z[50] + -z[63] + -z[71] + z[76];
z[16] = abb[3] * z[16];
z[3] = -abb[28] + -z[3] + -z[9] + -z[52] + z[78] + z[86];
z[3] = abb[0] * z[3];
z[9] = -abb[29] + z[77] + z[91] + z[97];
z[9] = abb[4] * z[9];
z[11] = -z[11] + z[62];
z[11] = abb[5] * z[11];
z[3] = z[3] + z[9] + z[11] + z[15] + z[16] + z[107];
z[9] = 8 * abb[34] + -z[26];
z[11] = 12 * abb[31] + -8 * abb[36] + -z[9] + -7 * z[38] + z[61] + z[73];
z[11] = abb[6] * z[11];
z[15] = -abb[25] + -abb[27];
z[15] = 3 * z[15] + -z[29] + z[47] + z[83];
z[15] = abb[2] * z[15];
z[3] = 2 * z[3] + z[11] + z[15];
z[3] = abb[14] * z[3];
z[3] = z[3] + z[5];
z[3] = abb[14] * z[3];
z[5] = z[65] + -z[95];
z[11] = -abb[27] + -abb[35] + z[5] + z[47];
z[15] = abb[2] * z[11];
z[16] = -abb[31] + abb[33] + -abb[36] + z[27] + z[52];
z[16] = abb[9] * z[16];
z[27] = z[46] * z[68];
z[29] = -abb[36] + z[31];
z[30] = -abb[25] + abb[30] + -abb[32] + z[29];
z[30] = abb[0] * z[30];
z[31] = -abb[36] + z[70];
z[34] = abb[4] * z[31];
z[15] = z[15] + z[16] + z[27] + z[30] + -z[34] + z[102];
z[16] = -z[6] + z[18];
z[15] = z[15] * z[16];
z[16] = -z[21] + z[31] + -z[59] + z[69];
z[16] = abb[9] * z[16];
z[1] = z[1] + z[55];
z[18] = -abb[28] + z[42];
z[22] = -abb[27] + abb[32] + abb[35] + -z[1] + z[18] + z[22];
z[22] = abb[0] * z[22];
z[13] = abb[3] * z[13];
z[13] = z[13] + z[16] + z[22] + -z[34];
z[16] = -abb[27] + -z[36] + -z[66] + z[87] + z[110];
z[16] = abb[6] * z[16];
z[22] = -abb[25] + z[7];
z[22] = 3 * z[22] + z[28] + -z[47] + -z[85];
z[22] = abb[2] * z[22];
z[13] = 2 * z[13] + z[16] + z[22];
z[13] = abb[14] * z[13];
z[13] = z[13] + z[15];
z[8] = -z[8] + -z[18] + z[28] + z[74];
z[8] = abb[0] * z[8];
z[11] = -abb[3] * z[11];
z[15] = z[20] + -z[33] + -z[37];
z[15] = abb[4] * z[15];
z[8] = z[8] + z[11] + z[15] + z[82];
z[11] = 13 * abb[25];
z[15] = -19 * abb[27] + -14 * abb[34] + z[11] + z[14] + z[35] + z[61] + z[85];
z[15] = abb[2] * z[15];
z[9] = 9 * abb[27] + z[9] + -z[11] + -z[63];
z[9] = abb[6] * z[9];
z[8] = 2 * z[8] + z[9] + z[15];
z[8] = abb[18] * z[8];
z[8] = z[8] + 2 * z[13];
z[8] = abb[18] * z[8];
z[9] = -abb[28] + z[21];
z[11] = -abb[33] + z[9] + -z[76] + z[93];
z[11] = abb[1] * z[11];
z[13] = abb[28] + abb[33] + z[94] + -z[104];
z[13] = abb[4] * z[13];
z[15] = 2 * z[38];
z[9] = abb[26] + abb[31] + -z[9] + -z[15];
z[9] = abb[0] * z[9];
z[16] = -abb[28] + -abb[31] + z[33] + z[38];
z[16] = abb[2] * z[16];
z[9] = z[9] + z[11] + z[13] + z[16] + -z[106];
z[9] = prod_pow(m1_set::bc<T>[0], 2) * z[9];
z[0] = 2 * z[0] + z[2] + z[3] + z[4] + z[8] + (T(13) / T(3)) * z[9];
z[0] = 8 * z[0];
z[2] = -z[43] + z[55] + z[65] + -z[79] + -z[92];
z[2] = abb[9] * z[2];
z[3] = -z[24] + z[43];
z[4] = 5 * abb[25] + z[3] + -z[32] + z[48];
z[4] = abb[6] * z[4];
z[4] = z[2] + z[4];
z[8] = -abb[32] + -z[10] + z[31] + z[49] + z[53];
z[9] = -abb[4] * z[8];
z[10] = 3 * z[38];
z[11] = abb[28] + -z[10];
z[11] = abb[26] + 2 * z[11] + -z[12] + z[20] + z[29];
z[11] = abb[0] * z[11];
z[5] = -z[5] + z[62];
z[5] = abb[5] * z[5];
z[12] = -z[46] * z[54];
z[10] = -z[10] + z[41];
z[10] = abb[2] * z[10];
z[9] = -z[4] + -z[5] + z[9] + z[10] + z[11] + z[12] + z[107];
z[9] = abb[14] * z[9];
z[10] = z[14] + -z[19] + z[38] + z[55] + -z[65] + z[66];
z[10] = abb[4] * z[10];
z[11] = z[17] + z[60];
z[3] = z[3] + z[11];
z[3] = abb[8] * z[3];
z[2] = z[2] + z[3];
z[1] = abb[26] + -z[1] + z[21] + z[42] + z[88];
z[1] = abb[0] * z[1];
z[3] = -abb[34] + -z[11] + z[44];
z[3] = z[3] * z[90];
z[1] = z[1] + z[2] + z[3] + z[5] + z[10] + -z[84];
z[1] = abb[15] * z[1];
z[3] = z[23] + -z[55] + z[63] + z[76] + -2 * z[93];
z[3] = abb[4] * z[3];
z[5] = -abb[0] * z[8];
z[8] = z[46] * z[98];
z[2] = -z[2] + z[3] + z[5] + z[8] + -z[105];
z[2] = abb[17] * z[2];
z[3] = -z[14] + -z[25] + 5 * z[38] + z[39];
z[3] = abb[2] * z[3];
z[5] = -abb[32] + abb[36] + z[15] + -z[20] + z[92];
z[5] = abb[0] * z[5];
z[8] = abb[25] + z[33] + -z[56];
z[8] = abb[4] * z[8];
z[3] = z[3] + z[4] + z[5] + z[8] + z[27];
z[3] = abb[18] * z[3];
z[4] = z[6] * z[100];
z[5] = 11 * abb[32] + -z[7] + -z[26] + -z[92] + -z[93] + -z[95];
z[6] = abb[17] + -abb[18];
z[5] = abb[7] * z[5] * z[6];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + z[9];
z[1] = 32 * m1_set::bc<T>[0] * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_688_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-107.91630891102871071499118276154306850616432867410450995206356577"),stof<T>("330.74517493859993424443586554723080017690252018527129567470052443")}, std::complex<T>{stof<T>("-257.60046156022420881109479999113180278235428192667544867495017673"),stof<T>("481.76255222791782450704101394124954127805550144519613402683372254")}, std::complex<T>{stof<T>("907.10363434393288856292005730475115247513227761423826063094174571"),stof<T>("24.58730796900699445037737278845759588821383553432865143031275415")}, std::complex<T>{stof<T>("95.747465315977911710714812327535275123052259033069030113439777683"),stof<T>("99.187652735500434174129457065096199896893491083595440186983587486")}, std::complex<T>{stof<T>("-338.31263886708605231335387565107359942266653299443939089399937548"),stof<T>("-737.90738240002431902772479521184173744627836608120064094486341364")}, std::complex<T>{stof<T>("259.6361249386068980527323378738872231482381860743430948858189592"),stof<T>("-788.57194318938406834011991169114742932205905400566052631458019943")}, std::complex<T>{stof<T>("-902.49699628093373973232359167879774686805340959356082404920778086"),stof<T>("-251.90397649114102958910535053927697047160777177981904707306317614")}, std::complex<T>{stof<T>("-237.76267683442113811496860853081744683581827530827482916597022606"),stof<T>("596.18950386583134213555496183104961645139937434069386850059782583")}, std::complex<T>{stof<T>("385.07666988966469422728857193314833266504047928173788520213110122"),stof<T>("359.57333658857239362639166906700362176173144857578540694997979355")}, std::complex<T>{stof<T>("87.52013424853760090641184511983804678437839807018771896820320507"),stof<T>("114.098456878803033206526589174241796085491262570976022880975275")}, stof<T>("-107.52675968961600501276538657346740664089487498594988099648885006"), std::complex<T>{stof<T>("113.22214255856029935503510349208137796102464275600182056181558514"),stof<T>("-124.33217670721664028179898417565160870523365792938570528592659134")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[55].real()/kbase.W[55].real()), rlog(k.W[60].real()/kbase.W[60].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_688_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_688_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("48.8040659152727748516407243560573660122945502758650022476190139"),stof<T>("-134.52454099919375970410263956532710620307066452768992994106150346")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,37> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), dlog_W61(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), f_1_1(k), f_1_3(k), f_1_5(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[11].real()/k.W[11].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[55].real()/k.W[55].real()), rlog(kend.W[60].real()/k.W[60].real())};

                    
            return f_4_688_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_688_DLogXconstant_part(base_point<T>, kend);
	value += f_4_688_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_688_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_688_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_688_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_688_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_688_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_688_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
