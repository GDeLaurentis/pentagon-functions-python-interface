/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_537.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_537_abbreviated (const std::array<T,29>& abb) {
T z[38];
z[0] = abb[20] + abb[23];
z[1] = abb[24] * (T(3) / T(2)) + -z[0];
z[2] = abb[22] * (T(1) / T(2));
z[3] = -abb[19] + abb[25];
z[4] = -abb[21] + z[3];
z[5] = z[1] + -z[2] + -z[4];
z[5] = abb[4] * z[5];
z[6] = -abb[24] + z[0];
z[7] = z[2] + z[6];
z[8] = (T(-1) / T(2)) * z[3];
z[9] = z[7] + z[8];
z[9] = abb[0] * z[9];
z[10] = abb[21] * (T(1) / T(2));
z[11] = z[7] + -z[10];
z[12] = abb[5] * z[11];
z[13] = abb[22] + -abb[24];
z[14] = abb[3] * z[13];
z[15] = (T(1) / T(2)) * z[14];
z[16] = abb[6] * z[4];
z[17] = (T(1) / T(2)) * z[16];
z[18] = z[15] + -z[17];
z[19] = abb[22] + abb[24] * (T(-5) / T(2)) + z[0] + z[10];
z[19] = abb[8] * z[19];
z[20] = -abb[22] + z[3];
z[21] = abb[1] * z[20];
z[22] = (T(1) / T(2)) * z[21];
z[23] = abb[2] * z[6];
z[19] = (T(-1) / T(2)) * z[5] + -z[9] + z[12] + -z[18] + z[19] + z[22] + z[23];
z[19] = prod_pow(abb[11], 2) * z[19];
z[10] = z[1] + z[10];
z[23] = -z[3] + z[10];
z[23] = abb[8] * z[23];
z[24] = -abb[21] + abb[24] * (T(-1) / T(2)) + z[0] + z[2];
z[24] = abb[4] * z[24];
z[1] = -z[1] + -z[8];
z[1] = abb[2] * z[1];
z[25] = -abb[0] * z[6];
z[1] = z[1] + z[12] + -z[15] + -z[17] + -z[23] + (T(1) / T(2)) * z[24] + z[25];
z[1] = abb[12] * z[1];
z[12] = 2 * abb[24];
z[15] = -abb[20] + z[12];
z[17] = 2 * abb[23];
z[24] = abb[22] + z[17];
z[15] = z[3] + -2 * z[15] + z[24];
z[15] = abb[8] * z[15];
z[15] = -z[14] + z[15];
z[25] = -abb[20] + abb[24];
z[25] = 2 * z[25];
z[26] = -z[17] + z[20] + z[25];
z[26] = abb[0] * z[26];
z[27] = z[15] + z[26];
z[25] = abb[21] + -z[24] + z[25];
z[28] = abb[5] * z[25];
z[6] = 2 * z[6];
z[29] = abb[2] * z[6];
z[28] = -z[28] + z[29];
z[5] = z[5] + -z[27] + -z[28];
z[29] = abb[11] * z[5];
z[30] = 3 * abb[24];
z[31] = 2 * abb[20];
z[32] = z[30] + -z[31];
z[33] = abb[21] + -2 * z[3] + -z[17] + z[32];
z[33] = abb[8] * z[33];
z[34] = z[16] + z[33];
z[6] = abb[0] * z[6];
z[35] = -abb[24] + z[3];
z[36] = abb[2] * z[35];
z[6] = z[6] + z[34] + -z[36];
z[37] = abb[9] * z[6];
z[1] = z[1] + z[29] + z[37];
z[1] = abb[12] * z[1];
z[29] = abb[21] + z[8];
z[30] = abb[22] * (T(3) / T(2)) + z[0] + z[29] + -z[30];
z[30] = abb[8] * z[30];
z[14] = -z[14] + z[16];
z[16] = z[14] + z[21];
z[7] = z[7] + -z[29];
z[7] = abb[4] * z[7];
z[7] = z[7] + -z[9] + z[16] + z[28] + z[30];
z[7] = abb[15] * z[7];
z[9] = abb[4] * z[4];
z[27] = z[9] + z[27];
z[27] = abb[11] * z[27];
z[28] = abb[22] + z[3];
z[12] = z[12] + -z[28];
z[12] = abb[5] * z[12];
z[18] = -z[12] + -z[18] + z[23];
z[23] = z[0] + -z[3];
z[23] = abb[0] * z[23];
z[11] = -abb[4] * z[11];
z[11] = z[11] + -z[18] + -z[23] + (T(-1) / T(2)) * z[36];
z[11] = abb[9] * z[11];
z[11] = z[11] + z[27];
z[11] = abb[9] * z[11];
z[25] = abb[4] * z[25];
z[17] = z[17] + z[31];
z[3] = abb[24] + z[3];
z[27] = -z[3] + z[17];
z[27] = abb[0] * z[27];
z[3] = -2 * abb[22] + z[3];
z[3] = abb[5] * z[3];
z[3] = z[3] + -z[15] + -z[21] + z[25] + z[27];
z[3] = abb[13] * z[3];
z[15] = abb[0] * z[20];
z[15] = z[15] + -z[36];
z[13] = z[4] + -z[13];
z[13] = abb[8] * z[13];
z[9] = -z[9] + z[13] + -z[14] + -z[15];
z[20] = -abb[26] * z[9];
z[2] = -z[2] + z[8];
z[8] = -z[2] + -z[10];
z[8] = abb[4] * z[8];
z[0] = z[0] + z[2];
z[0] = abb[0] * z[0];
z[0] = z[0] + z[8] + z[18] + -z[22];
z[0] = abb[10] * z[0];
z[2] = abb[21] + -abb[24];
z[2] = abb[4] * z[2];
z[2] = z[2] + z[13] + -z[16];
z[8] = abb[11] * z[2];
z[0] = z[0] + z[8];
z[0] = abb[10] * z[0];
z[5] = -abb[14] * z[5];
z[8] = abb[27] * z[2];
z[10] = abb[4] * z[35];
z[10] = z[10] + -z[21];
z[13] = -z[10] + -z[15];
z[13] = prod_pow(m1_set::bc<T>[0], 2) * z[13];
z[0] = abb[28] + z[0] + z[1] + z[3] + z[5] + z[7] + z[8] + z[11] + (T(3) / T(4)) * z[13] + z[19] + z[20];
z[1] = -z[10] + -z[26] + z[34];
z[1] = abb[11] * z[1];
z[3] = 2 * z[12] + -z[14] + -z[33];
z[5] = -z[3] + 2 * z[23] + -z[25] + z[36];
z[5] = abb[9] * z[5];
z[4] = -z[4] + -z[24] + z[32];
z[4] = abb[4] * z[4];
z[7] = -z[17] + z[28];
z[7] = abb[0] * z[7];
z[3] = z[3] + z[4] + z[7] + z[21];
z[3] = abb[10] * z[3];
z[4] = -abb[12] * z[6];
z[1] = z[1] + z[3] + z[4] + z[5];
z[1] = m1_set::bc<T>[0] * z[1];
z[3] = -abb[16] * z[9];
z[2] = abb[17] * z[2];
z[1] = abb[18] + z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_537_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-10.0919561902108984311358575730808313632815505936727649989189074504"),stof<T>("1.0964456061608738368758578509721348133758677656864836594416045702")}, std::complex<T>{stof<T>("-2.5227027010291868203548963308375087066797685616498828632978490141"),stof<T>("9.027343662547195718527086849653584602450954462076343541626156661")}, std::complex<T>{stof<T>("-0.2077375479342742776422919381453111569358811642975739531529310902"),stof<T>("-1.8213521118363595828676871524754912619249875250628312375815671677")}, std::complex<T>{stof<T>("-7.361515941247437333138669304098011499665900867725308182468127346"),stof<T>("-6.1095459445499622987835418462059585271500991713270286446029849231")}, std::complex<T>{stof<T>("-2.5227027010291868203548963308375087066797685616498828632978490141"),stof<T>("9.027343662547195718527086849653584602450954462076343541626156661")}, std::complex<T>{stof<T>("10.0919561902108984311358575730808313632815505936727649989189074504"),stof<T>("-1.0964456061608738368758578509721348133758677656864836594416045702")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[11].real()/kbase.W[11].real()), rlog(k.W[28].real()/kbase.W[28].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_537_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_537_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,6> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(32)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (2 * (32 + v[0] + -v[1] + 5 * v[2] + -3 * v[3] + -8 * v[4] + -5 * v[5]) + m1_set::bc<T>[2] * (-21 * v[0] + v[1] + 3 * (-8 + -3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = ((T(1) / T(16)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (-56 + -49 * v[0] + 13 * v[1] + -17 * v[2] + 3 * v[3] + 20 * v[4] + 17 * v[5] + -2 * m1_set::bc<T>[2] * (-21 * v[0] + v[1] + 3 * (-8 + -3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[2] = ((T(-1) / T(32)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * ((-35 + 4 * m1_set::bc<T>[1] + 21 * m1_set::bc<T>[2]) * v[0] + 7 * v[1] + -11 * v[2] + v[3] + 4 * (-6 + 4 * m1_set::bc<T>[1] + 6 * m1_set::bc<T>[2] + 3 * v[4]) + 4 * m1_set::bc<T>[1] * (v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + 11 * v[5] + -m1_set::bc<T>[2] * (v[1] + 3 * (-3 * v[2] + v[3] + 4 * v[4] + 3 * v[5])))) / prod_pow(tend, 2);
c[3] = ((-5 + 6 * m1_set::bc<T>[2]) * (T(-1) / T(2)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;
c[4] = ((-11 + 4 * m1_set::bc<T>[1] + 15 * m1_set::bc<T>[2]) * (T(1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;
c[5] = ((-4 + 3 * m1_set::bc<T>[2]) * (T(-1) / T(4)) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return abb[20] * c[3] + abb[23] * c[3] + abb[24] * c[4] + -abb[19] * c[5] + abb[25] * c[5] + t * (abb[19] * c[0] + -abb[25] * c[0] + abb[20] * c[1] + abb[23] * c[1] + abb[22] * c[2] + abb[24] * (c[0] + -2 * c[2] + c[1] * (T(-1) / T(2))) + abb[21] * (c[2] + c[1] * (T(-1) / T(2)))) + abb[22] * (c[3] + 2 * (c[4] + c[5])) * (T(-1) / T(4)) + abb[21] * (3 * c[3] + 2 * (c[4] + c[5])) * (T(-1) / T(4));
	}
	{
T z[11];
z[0] = 2 * abb[12];
z[1] = abb[11] * (T(1) / T(2));
z[2] = -z[0] + z[1];
z[2] = abb[11] * z[2];
z[3] = abb[13] + -abb[15];
z[4] = 2 * abb[14] + -z[3];
z[5] = abb[11] + -abb[12];
z[6] = abb[9] * z[5];
z[7] = prod_pow(abb[12], 2);
z[2] = z[2] + z[4] + z[6] + (T(3) / T(2)) * z[7];
z[2] = abb[21] * z[2];
z[8] = 2 * abb[9];
z[5] = z[5] * z[8];
z[0] = abb[11] + -z[0];
z[0] = abb[11] * z[0];
z[0] = z[0] + z[4] + z[5] + z[7];
z[0] = abb[19] * z[0];
z[4] = -11 * abb[12] + abb[11] * (T(15) / T(2));
z[4] = z[1] * z[4];
z[4] = abb[14] * (T(11) / T(2)) + -5 * z[3] + z[4] + 3 * z[6] + (T(7) / T(4)) * z[7];
z[4] = abb[24] * z[4];
z[6] = abb[12] + abb[11] * (T(-3) / T(2));
z[1] = z[1] * z[6];
z[1] = abb[14] * (T(-1) / T(2)) + z[1] + z[3] + (T(1) / T(4)) * z[7];
z[1] = abb[22] * z[1];
z[6] = abb[20] + abb[23];
z[8] = abb[25] + 4 * z[6];
z[3] = z[3] * z[8];
z[8] = abb[25] + (T(5) / T(2)) * z[6];
z[7] = -z[7] * z[8];
z[9] = 2 * abb[25] + 5 * z[6];
z[10] = abb[12] * z[9];
z[8] = -abb[11] * z[8];
z[8] = z[8] + z[10];
z[8] = abb[11] * z[8];
z[6] = abb[25] + z[6];
z[5] = -z[5] * z[6];
z[6] = -abb[14] * z[9];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[8];
return abb[7] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_537_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(-1) / T(8)) * (v[0] + -v[1] + v[2] + v[3] + -v[5]) * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (2 * abb[19] + -2 * abb[20] + abb[21] + -2 * abb[23] + 3 * abb[24] + -2 * abb[25]) * (-2 * t * c[0] + c[1]) * (T(-1) / T(2));
	}
	{
T z[2];
z[0] = -abb[19] + abb[20] + abb[23];
z[0] = -abb[21] + -3 * abb[24] + 2 * abb[25] + 2 * z[0];
z[1] = abb[11] + -abb[12];
return abb[7] * m1_set::bc<T>[0] * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_537_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("10.4842125823478708360123864430080514497761561587932154537252684372"),stof<T>("5.5679680996670128891084502588196083827430050905814694198457290535")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,29> abb = {dl[0], dl[1], dlog_W4(k,dl), dlog_W7(k,dl), dlog_W10(k,dl), dlog_W12(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W29(k,dl), f_1_1(k), f_1_3(k), f_1_9(k), f_1_11(k), f_2_3(k), f_2_15(k), f_2_18(k), f_2_2_im(k), f_2_9_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[11].real()/k.W[11].real()), IntDLogL_W_17(t,k,kend,dl), rlog(kend.W[28].real()/k.W[28].real()), f_2_2_re(k), f_2_9_re(k), T{0}};
abb[18] = SpDLog_f_4_537_W_17_Im(t, path, abb);
abb[28] = SpDLog_f_4_537_W_17_Re(t, path, abb);

                    
            return f_4_537_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_537_DLogXconstant_part(base_point<T>, kend);
	value += f_4_537_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_537_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_537_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_537_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_537_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_537_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_537_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
