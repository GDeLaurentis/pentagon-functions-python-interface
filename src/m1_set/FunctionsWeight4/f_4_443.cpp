/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_443.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_443_abbreviated (const std::array<T,61>& abb) {
T z[84];
z[0] = abb[50] + abb[53];
z[1] = (T(1) / T(2)) * z[0];
z[2] = abb[4] * z[1];
z[3] = abb[46] + -abb[47] + -abb[48] + abb[49];
z[4] = (T(-1) / T(2)) * z[3];
z[5] = abb[23] * z[4];
z[6] = z[2] + z[5];
z[7] = abb[50] + abb[54];
z[8] = abb[10] * z[7];
z[9] = abb[54] + z[1];
z[10] = abb[7] * z[9];
z[11] = z[8] + -z[10];
z[12] = abb[25] * z[4];
z[12] = z[11] + z[12];
z[13] = abb[27] * (T(1) / T(2));
z[13] = -z[3] * z[13];
z[14] = -abb[50] + abb[53];
z[14] = abb[16] * z[14];
z[15] = (T(1) / T(2)) * z[14];
z[16] = abb[52] + z[7];
z[17] = abb[14] * z[16];
z[18] = abb[8] * z[1];
z[19] = abb[5] * z[1];
z[20] = abb[52] + z[0];
z[21] = abb[54] + z[20];
z[22] = abb[1] * z[21];
z[23] = z[6] + z[12] + z[13] + z[15] + z[17] + z[18] + -z[19] + -z[22];
z[24] = abb[44] * z[23];
z[25] = abb[20] + abb[22];
z[26] = abb[19] + z[25];
z[27] = abb[56] * (T(1) / T(2));
z[26] = z[26] * z[27];
z[28] = abb[55] * (T(1) / T(2));
z[29] = abb[52] * (T(1) / T(2));
z[30] = z[28] + -z[29];
z[31] = abb[51] + z[0];
z[32] = z[30] + -z[31];
z[33] = abb[3] * z[32];
z[34] = abb[26] * z[3];
z[35] = -abb[25] * z[3];
z[36] = z[34] + -z[35];
z[37] = abb[6] * z[0];
z[38] = z[36] + z[37];
z[39] = abb[17] * abb[52];
z[40] = z[38] + -z[39];
z[40] = (T(1) / T(2)) * z[40];
z[41] = abb[24] * z[4];
z[42] = z[40] + -z[41];
z[43] = abb[8] * (T(1) / T(2));
z[44] = abb[2] + z[43];
z[45] = abb[17] * (T(1) / T(2)) + -z[44];
z[45] = abb[55] * z[45];
z[46] = abb[8] * z[29];
z[47] = abb[2] * z[31];
z[4] = abb[28] * z[4];
z[6] = z[4] + -z[6] + z[26] + -z[33] + z[42] + -z[45] + z[46] + -z[47];
z[26] = -abb[43] * z[6];
z[24] = z[24] + z[26];
z[26] = z[9] + z[29];
z[33] = abb[0] * z[26];
z[4] = z[4] + -z[33];
z[45] = abb[0] + abb[17];
z[46] = -abb[2] + -abb[10] + (T(1) / T(2)) * z[45];
z[46] = abb[55] * z[46];
z[47] = abb[2] * abb[54];
z[25] = abb[56] * z[25];
z[46] = -z[4] + (T(-1) / T(2)) * z[25] + z[46] + -z[47];
z[48] = abb[52] + z[1];
z[49] = abb[16] * z[48];
z[50] = z[13] + z[49];
z[51] = -z[22] + z[50];
z[52] = abb[8] * z[9];
z[5] = z[5] + z[52];
z[52] = z[5] + z[51];
z[53] = -z[35] + z[39];
z[53] = (T(1) / T(2)) * z[53];
z[54] = abb[53] + abb[54];
z[54] = abb[9] * z[54];
z[55] = -z[10] + z[54];
z[56] = z[26] + z[28];
z[56] = abb[3] * z[56];
z[16] = abb[10] * z[16];
z[56] = -z[16] + z[46] + -z[52] + z[53] + -z[55] + z[56];
z[56] = abb[31] * z[56];
z[57] = abb[10] * abb[52];
z[58] = abb[17] * z[29];
z[59] = abb[4] * z[9];
z[46] = z[46] + -z[57] + z[58] + z[59];
z[60] = abb[3] * (T(1) / T(2));
z[61] = -abb[55] + z[20];
z[62] = z[60] * z[61];
z[62] = z[22] + -z[46] + z[62];
z[62] = abb[34] * z[62];
z[63] = abb[54] + z[0];
z[64] = abb[51] + z[63];
z[64] = abb[2] * z[64];
z[41] = z[41] + z[64];
z[64] = abb[51] + z[21];
z[64] = abb[3] * z[64];
z[38] = (T(1) / T(2)) * z[38] + -z[41] + -z[52] + z[64];
z[52] = abb[35] * z[38];
z[64] = abb[34] * z[19];
z[52] = z[52] + z[56] + z[62] + z[64];
z[56] = abb[52] + (T(3) / T(2)) * z[0];
z[62] = abb[51] + abb[54] + z[56];
z[62] = abb[4] * z[62];
z[49] = z[33] + -z[49] + z[58] + z[62];
z[58] = (T(1) / T(4)) * z[45];
z[44] = z[44] + -z[58];
z[44] = abb[55] * z[44];
z[61] = abb[51] + z[61];
z[61] = abb[13] * z[61];
z[44] = (T(1) / T(4)) * z[25] + z[44] + (T(1) / T(2)) * z[61];
z[28] = z[28] + z[29];
z[65] = abb[54] + z[28];
z[66] = z[60] * z[65];
z[67] = (T(-1) / T(4)) * z[3];
z[68] = abb[27] * z[67];
z[69] = abb[28] * z[67];
z[70] = z[68] + z[69];
z[71] = abb[54] * z[43];
z[49] = z[44] + z[47] + (T(-1) / T(2)) * z[49] + -z[66] + z[70] + z[71];
z[49] = abb[32] * z[49];
z[66] = -abb[54] + z[31];
z[66] = abb[2] * z[66];
z[18] = -z[4] + z[18] + -z[42] + z[62] + z[66];
z[32] = z[32] * z[60];
z[42] = (T(1) / T(2)) * z[22];
z[62] = -abb[23] * z[3];
z[66] = (T(1) / T(4)) * z[62];
z[18] = (T(1) / T(2)) * z[18] + z[32] + -z[42] + -z[44] + z[66];
z[32] = abb[33] * z[18];
z[5] = z[5] + z[59];
z[21] = abb[3] * z[21];
z[21] = -z[5] + z[21] + -z[50];
z[12] = z[12] + z[54];
z[71] = z[12] + -z[21];
z[72] = (T(1) / T(4)) * z[0];
z[73] = abb[5] * z[72];
z[71] = -z[22] + (T(1) / T(2)) * z[71] + -z[73];
z[71] = abb[36] * z[71];
z[73] = abb[36] * z[20];
z[74] = abb[34] * z[20];
z[73] = z[73] + -z[74];
z[75] = abb[11] * z[73];
z[75] = (T(1) / T(2)) * z[75];
z[71] = z[71] + z[75];
z[32] = z[32] + z[49] + (T(1) / T(2)) * z[52] + z[71];
z[32] = m1_set::bc<T>[0] * z[32];
z[52] = z[33] + z[53];
z[53] = abb[8] * (T(1) / T(4));
z[76] = -abb[10] + z[53] + z[58];
z[76] = abb[55] * z[76];
z[77] = abb[19] + -abb[22];
z[78] = abb[56] * (T(1) / T(4));
z[77] = z[77] * z[78];
z[76] = z[76] + z[77];
z[77] = abb[52] * z[53];
z[77] = -z[66] + z[77];
z[78] = z[76] + z[77];
z[14] = (T(1) / T(4)) * z[14];
z[10] = (T(1) / T(2)) * z[10];
z[16] = -z[10] + z[14] + z[16] + (T(-1) / T(2)) * z[52] + z[70] + -z[78];
z[52] = -abb[42] * z[16];
z[24] = (T(1) / T(2)) * z[24] + z[32] + z[52];
z[24] = abb[60] + (T(1) / T(4)) * z[24];
z[32] = -z[10] + (T(1) / T(2)) * z[54];
z[52] = abb[54] + (T(3) / T(4)) * z[0];
z[70] = abb[52] * (T(1) / T(4));
z[79] = z[52] + z[70];
z[79] = abb[0] * z[79];
z[80] = abb[15] * z[72];
z[79] = z[79] + -z[80];
z[81] = -z[34] + z[39];
z[26] = z[26] * z[43];
z[7] = abb[52] + (T(1) / T(2)) * z[7];
z[7] = abb[10] * z[7];
z[43] = abb[52] + -abb[54];
z[43] = abb[18] * z[43];
z[82] = (T(1) / T(2)) * z[43];
z[83] = -abb[3] * z[29];
z[26] = -z[7] + z[26] + -z[32] + -z[42] + (T(1) / T(2)) * z[59] + -z[69] + z[76] + z[79] + (T(1) / T(4)) * z[81] + z[82] + z[83];
z[26] = abb[31] * z[26];
z[42] = -abb[52] + z[0];
z[42] = abb[0] * z[42];
z[42] = -z[36] + -z[39] + z[42];
z[76] = -abb[15] * z[0];
z[76] = z[42] + z[76];
z[81] = -z[43] + z[51];
z[52] = -abb[3] * z[52];
z[52] = z[52] + z[57] + z[69] + (T(1) / T(4)) * z[76] + -z[78] + z[81];
z[52] = abb[34] * z[52];
z[26] = z[26] + z[52];
z[26] = abb[31] * z[26];
z[6] = abb[58] * z[6];
z[23] = -abb[59] * z[23];
z[11] = z[11] + z[54];
z[37] = z[34] + z[37];
z[52] = z[37] + z[39];
z[57] = -3 * abb[54] + -z[31];
z[57] = abb[2] * z[57];
z[76] = -abb[8] * abb[54];
z[4] = -z[4] + z[11] + (T(1) / T(2)) * z[52] + z[57] + z[76];
z[52] = abb[51] * (T(1) / T(2));
z[57] = abb[54] + abb[55] * (T(1) / T(4)) + z[52] + z[70] + z[72];
z[57] = abb[3] * z[57];
z[29] = z[29] + z[52];
z[52] = z[29] + z[63];
z[52] = abb[4] * z[52];
z[67] = -abb[24] * z[67];
z[4] = (T(1) / T(2)) * z[4] + -z[44] + -z[50] + z[52] + z[57] + z[67] + z[82];
z[4] = abb[32] * z[4];
z[30] = abb[54] + z[30];
z[30] = abb[3] * z[30];
z[44] = z[43] + -z[50];
z[30] = z[30] + z[44] + z[46];
z[46] = -abb[31] + abb[34];
z[30] = z[30] * z[46];
z[4] = z[4] + z[30];
z[4] = abb[32] * z[4];
z[30] = (T(1) / T(2)) * z[37] + -z[41];
z[2] = z[2] + -z[13] + -z[15] + z[30] + z[55];
z[2] = abb[39] * z[2];
z[13] = abb[3] * z[48];
z[5] = -z[5] + z[13] + z[22] + -z[43];
z[13] = abb[15] * z[1];
z[15] = abb[0] * z[63];
z[13] = z[13] + -z[15];
z[15] = z[13] + (T(1) / T(2)) * z[36];
z[37] = z[5] + z[15];
z[19] = z[19] + -z[37];
z[19] = abb[38] * z[19];
z[5] = z[5] + -z[12];
z[12] = abb[40] * z[5];
z[20] = -abb[31] * z[20];
z[20] = z[20] + z[73];
z[20] = abb[36] * z[20];
z[41] = abb[31] * z[74];
z[20] = z[20] + z[41];
z[20] = abb[11] * z[20];
z[2] = z[2] + z[4] + z[6] + -z[12] + z[19] + z[20] + z[23] + z[26];
z[4] = abb[19] + abb[22];
z[4] = abb[20] + (T(1) / T(2)) * z[4];
z[4] = z[4] * z[27];
z[6] = -abb[2] + -z[53] + z[58];
z[6] = abb[55] * z[6];
z[4] = z[4] + -z[6] + z[47] + z[69];
z[6] = z[36] + -z[39];
z[12] = -abb[55] + z[56];
z[12] = z[12] * z[60];
z[6] = z[4] + (T(1) / T(4)) * z[6] + z[12] + z[22] + -z[59] + z[77] + -z[79];
z[6] = abb[34] * z[6];
z[12] = -z[9] + -z[70];
z[12] = abb[8] * z[12];
z[19] = z[65] + z[72];
z[19] = abb[3] * z[19];
z[4] = -z[4] + z[12] + z[19] + (T(-1) / T(4)) * z[42] + -z[51] + -z[66] + z[80];
z[4] = abb[31] * z[4];
z[4] = z[4] + z[6];
z[6] = abb[33] * (T(1) / T(2));
z[12] = z[6] * z[18];
z[15] = z[15] + z[21];
z[15] = (T(1) / T(2)) * z[15] + z[22];
z[18] = -abb[36] * z[15];
z[4] = (T(1) / T(2)) * z[4] + z[12] + z[18] + z[49] + z[75];
z[4] = abb[33] * z[4];
z[12] = abb[57] * z[16];
z[13] = z[13] + (T(1) / T(2)) * z[34];
z[10] = z[10] + (T(-1) / T(2)) * z[13] + z[14] + -z[54] + z[68];
z[10] = abb[37] * z[10];
z[14] = abb[31] + abb[34];
z[14] = z[14] * z[15];
z[8] = z[8] + -z[13] + z[35];
z[8] = (T(1) / T(2)) * z[8] + -z[21] + z[32];
z[13] = abb[5] * z[0];
z[8] = (T(1) / T(2)) * z[8] + (T(-1) / T(8)) * z[13] + -z[22];
z[8] = abb[36] * z[8];
z[8] = z[8] + z[14];
z[8] = abb[36] * z[8];
z[14] = -z[33] + z[40];
z[15] = abb[54] + (T(-1) / T(3)) * z[31];
z[15] = abb[2] * z[15];
z[14] = (T(1) / T(3)) * z[14] + z[15];
z[15] = abb[24] + -abb[28];
z[15] = (T(-1) / T(12)) * z[15];
z[15] = -z[3] * z[15];
z[16] = -abb[54] + (T(-7) / T(4)) * z[0];
z[16] = (T(1) / T(3)) * z[16] + -z[29];
z[16] = abb[4] * z[16];
z[1] = abb[54] + -z[1];
z[1] = abb[8] * z[1];
z[1] = (T(1) / T(6)) * z[1] + (T(-1) / T(3)) * z[7] + (T(1) / T(2)) * z[14] + z[15] + z[16];
z[7] = -z[28] + z[31];
z[7] = abb[3] * z[7];
z[7] = z[7] + z[17] + z[22] + z[43];
z[14] = z[25] + -z[62];
z[15] = abb[2] + abb[10] * (T(-1) / T(2)) + (T(-1) / T(8)) * z[45];
z[15] = (T(1) / T(3)) * z[15] + z[53];
z[15] = abb[55] * z[15];
z[1] = (T(1) / T(2)) * z[1] + (T(1) / T(12)) * z[7] + (T(1) / T(24)) * z[14] + z[15] + (T(1) / T(4)) * z[61];
z[1] = prod_pow(m1_set::bc<T>[0], 2) * z[1];
z[1] = z[1] + z[4] + z[8] + z[10] + z[12];
z[4] = -abb[51] + -z[9];
z[4] = abb[3] * z[4];
z[4] = z[4] + -z[11] + -z[30] + -z[44] + -z[59];
z[4] = abb[32] * z[4];
z[7] = -abb[31] * z[5];
z[8] = -abb[3] * z[9];
z[8] = z[8] + z[81];
z[8] = abb[34] * z[8];
z[4] = z[4] + z[7] + z[8] + -z[64];
z[6] = z[6] * z[38];
z[7] = abb[35] * (T(1) / T(4));
z[5] = -z[5] * z[7];
z[4] = (T(1) / T(2)) * z[4] + z[5] + z[6] + -z[71];
z[4] = z[4] * z[7];
z[5] = -abb[20] + abb[21];
z[5] = z[0] * z[5];
z[3] = abb[29] * z[3];
z[6] = abb[22] * abb[52];
z[7] = abb[22] * abb[55];
z[3] = z[3] + z[5] + z[6] + z[7];
z[5] = -abb[8] + -z[45];
z[5] = abb[30] * (T(1) / T(8)) + (T(1) / T(32)) * z[5];
z[5] = abb[56] * z[5];
z[6] = -abb[52] + -abb[55];
z[6] = abb[19] * z[6];
z[3] = (T(1) / T(32)) * z[3] + z[5] + (T(1) / T(32)) * z[6];
z[3] = abb[41] * z[3];
z[5] = (T(1) / T(32)) * z[13] + (T(-1) / T(16)) * z[37];
z[5] = prod_pow(abb[34], 2) * z[5];
z[6] = abb[37] + -abb[39];
z[0] = abb[3] * z[0] * z[6];
z[0] = abb[45] + (T(1) / T(16)) * z[0] + (T(1) / T(4)) * z[1] + (T(1) / T(8)) * z[2] + z[3] + z[4] + z[5];
return {z[24], z[0]};
}


template <typename T> std::complex<T> f_4_443_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.66233535001764902069552461705958085413941037085197403280494740665"),stof<T>("-0.05708088781032731453222140968726579783556278558434484928422148394")}, stof<T>("0.54026086366228174830919289976382944109045365541866636487578369885"), std::complex<T>{stof<T>("1.15043948057849416651173275012352639563720853081341291451433959248"),stof<T>("0.2690314614127623539218746127261972499728562629554907629540115299")}, std::complex<T>{stof<T>("0.48068702584120805374197774487160131587051653877271314140165301389"),stof<T>("-0.13395780866258134135537237722429530142827786176233993066369918862")}, std::complex<T>{stof<T>("0.46943758452998675192896355578828296660541952677811381006210771456"),stof<T>("-0.51633675651472834683791330164952579570436698278292195087025940511")}, std::complex<T>{stof<T>("0.20367351587692326052492846867765289984606122414154710521258928289"),stof<T>("-0.08529189711902360463517163720631310369835698266224375937809538618")}, std::complex<T>{stof<T>("-0.25186751860815181882323830401799984856501583090687365866823217018"),stof<T>("0.27602727033741684098080560531544625171981658056668086306849614823")}};
	
	std::vector<C> intdlogs = {rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_443_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_443_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}
template <typename T, typename TABB> T SpDLog_f_4_443_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(128)) * (v[2] + v[3]) * (-8 + v[2] + v[3] + -4 * v[5])) / prod_pow(tend, 2);
c[1] = ((T(-1) / T(16)) * (v[2] + v[3])) / tend;


		return (abb[51] + abb[53] + abb[54]) * (t * c[0] + c[1]);
	}
	{
T z[1];
z[0] = abb[51] + abb[53] + abb[54];
return abb[12] * abb[39] * (T(1) / T(8)) * z[0];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_443_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.47993850471359930014718455092534543292138343170674402108251270148"),stof<T>("-0.10972068855864484282781305002504959913323014567398279971250554171")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,61> abb = {dl[0], dlog_W3(k,dl), dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W13(k,dl), dlog_W15(k,dl), dlog_W18(k,dl), dlog_W19(k,dl), dlog_W22(k,dl), dlog_W26(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W92(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_13(k), f_2_19(k), f_2_23(k), f_2_4_im(k), f_2_7_im(k), f_2_11_im(k), T{0}, rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_4_re(k), f_2_7_re(k), f_2_11_re(k), T{0}};
abb[45] = SpDLog_f_4_443_W_19_Im(t, path, abb);
abb[60] = SpDLog_f_4_443_W_19_Re(t, path, abb);

                    
            return f_4_443_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_443_DLogXconstant_part(base_point<T>, kend);
	value += f_4_443_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_443_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_443_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_443_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_443_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_443_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_443_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
