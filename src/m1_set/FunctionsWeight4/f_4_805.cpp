/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_805.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_805_abbreviated (const std::array<T,81>& abb) {
T z[141];
z[0] = 2 * abb[64];
z[1] = 2 * abb[60];
z[2] = abb[61] + abb[63];
z[3] = abb[62] + abb[65];
z[4] = z[0] + z[1] + -z[2] + -z[3];
z[5] = abb[29] * z[4];
z[6] = abb[4] * abb[68];
z[7] = abb[4] * abb[66];
z[8] = abb[66] + abb[68];
z[9] = abb[13] * z[8];
z[10] = z[6] + z[7] + -z[9];
z[11] = abb[60] + abb[64];
z[12] = -z[3] + z[11];
z[13] = abb[26] * z[12];
z[14] = abb[68] + -abb[70];
z[15] = abb[14] * z[14];
z[13] = z[13] + -z[15];
z[16] = abb[27] * z[12];
z[17] = z[13] + z[16];
z[18] = -z[2] + z[11];
z[19] = abb[28] * z[18];
z[20] = abb[30] * z[18];
z[21] = z[19] + z[20];
z[22] = abb[67] + abb[69];
z[23] = z[8] + z[22];
z[24] = 4 * abb[1];
z[24] = z[23] * z[24];
z[25] = 2 * z[22];
z[26] = abb[66] + -abb[70];
z[27] = z[25] + z[26];
z[27] = abb[5] * z[27];
z[28] = abb[66] + abb[70];
z[29] = abb[6] * z[28];
z[27] = z[27] + z[29];
z[30] = abb[68] + abb[70];
z[31] = 2 * abb[66];
z[32] = z[30] + z[31];
z[33] = abb[3] * z[32];
z[34] = 2 * abb[68];
z[35] = z[26] + -z[34];
z[35] = abb[7] * z[35];
z[36] = abb[66] + z[22];
z[37] = abb[12] * z[36];
z[38] = 2 * z[37];
z[39] = abb[10] * z[28];
z[40] = 2 * z[39];
z[5] = z[5] + z[10] + z[17] + z[21] + z[24] + -z[27] + z[33] + z[35] + -z[38] + -z[40];
z[5] = abb[37] * z[5];
z[35] = abb[29] * z[18];
z[41] = -abb[66] + abb[68];
z[42] = abb[7] * z[41];
z[43] = z[9] + z[42];
z[44] = 2 * abb[1];
z[45] = z[23] * z[44];
z[46] = -abb[3] * z[8];
z[46] = -z[35] + z[38] + z[43] + -z[45] + z[46];
z[46] = abb[36] * z[46];
z[5] = z[5] + z[46];
z[46] = abb[5] * abb[66];
z[47] = z[21] + -z[46];
z[48] = abb[31] * z[12];
z[49] = 2 * abb[0];
z[50] = abb[68] + z[28];
z[49] = z[49] * z[50];
z[51] = z[48] + -z[49];
z[52] = abb[27] * z[18];
z[53] = z[9] + z[52];
z[54] = abb[62] + abb[65] + -z[2];
z[54] = abb[32] * z[54];
z[55] = abb[17] * z[28];
z[54] = z[54] + -z[55];
z[55] = abb[16] * z[30];
z[56] = -z[54] + z[55];
z[57] = abb[2] * z[28];
z[58] = abb[5] * abb[68];
z[59] = z[47] + z[51] + z[53] + z[56] + -z[57] + -z[58];
z[60] = 2 * abb[38];
z[59] = z[59] * z[60];
z[60] = abb[41] * z[30];
z[61] = abb[42] * z[30];
z[60] = z[60] + -z[61];
z[60] = abb[16] * z[60];
z[59] = z[59] + -z[60];
z[60] = abb[6] * z[30];
z[62] = z[51] + -z[60];
z[10] = -z[10] + z[62];
z[2] = z[2] + -z[3];
z[63] = abb[29] * z[2];
z[64] = abb[7] * z[28];
z[63] = z[63] + -z[64];
z[16] = z[10] + z[16] + z[56] + z[63];
z[65] = 2 * abb[35];
z[66] = z[16] * z[65];
z[67] = abb[30] * z[2];
z[68] = z[54] + z[67];
z[69] = z[40] + z[68];
z[70] = abb[28] * z[2];
z[27] = z[27] + -z[38] + z[69] + z[70];
z[27] = abb[40] * z[27];
z[71] = abb[27] * z[2];
z[72] = z[70] + z[71];
z[73] = z[63] + z[72];
z[74] = abb[5] * z[26];
z[75] = z[57] + z[69] + z[73] + z[74];
z[76] = 2 * abb[39];
z[77] = -z[75] * z[76];
z[78] = abb[41] * z[12];
z[79] = abb[42] * z[12];
z[80] = z[78] + -z[79];
z[81] = abb[28] + abb[30];
z[82] = z[80] * z[81];
z[83] = abb[5] * abb[70];
z[84] = -z[40] + z[83];
z[85] = -z[58] + -z[84];
z[85] = z[51] + z[60] + 2 * z[85];
z[85] = abb[41] * z[85];
z[86] = abb[7] * z[30];
z[86] = z[38] + z[86];
z[87] = -z[62] + 2 * z[86];
z[87] = abb[42] * z[87];
z[78] = -z[78] + -z[79];
z[78] = abb[26] * z[78];
z[88] = 2 * z[79];
z[89] = abb[27] + abb[29];
z[90] = -z[88] * z[89];
z[91] = abb[41] + abb[42];
z[92] = z[15] * z[91];
z[5] = 2 * z[5] + 2 * z[27] + -z[59] + z[66] + z[77] + z[78] + z[82] + z[85] + z[87] + z[90] + z[92];
z[5] = m1_set::bc<T>[0] * z[5];
z[16] = abb[49] * z[16];
z[27] = abb[6] * z[41];
z[43] = -z[27] + z[43];
z[66] = -abb[4] + abb[5];
z[77] = abb[68] * z[66];
z[78] = -z[7] + z[77];
z[82] = abb[66] + z[25];
z[82] = abb[5] * z[82];
z[21] = -z[21] + z[43] + z[78] + z[82];
z[85] = abb[50] * z[21];
z[87] = abb[50] * z[18];
z[90] = abb[52] * z[12];
z[87] = z[87] + z[90];
z[87] = abb[29] * z[87];
z[16] = -z[16] + z[85] + -z[87];
z[85] = z[58] + -z[60];
z[17] = z[17] + z[85];
z[84] = z[17] + z[84] + -z[86];
z[86] = 2 * abb[52];
z[87] = z[84] * z[86];
z[90] = abb[3] * z[26];
z[90] = -z[75] + z[90];
z[90] = 2 * z[90];
z[92] = abb[51] * z[90];
z[91] = -z[32] * z[91];
z[93] = z[26] * z[76];
z[94] = 2 * z[30];
z[95] = abb[38] * z[94];
z[91] = z[91] + z[93] + z[95];
z[91] = m1_set::bc<T>[0] * z[91];
z[86] = z[32] * z[86];
z[86] = z[86] + z[91];
z[86] = abb[3] * z[86];
z[91] = abb[56] + abb[57];
z[93] = -z[28] * z[91];
z[96] = -abb[34] * z[12];
z[97] = -abb[19] * abb[70];
z[97] = -z[96] + z[97];
z[97] = abb[53] * z[97];
z[98] = -abb[19] * abb[53];
z[91] = -z[91] + z[98];
z[91] = abb[68] * z[91];
z[98] = abb[53] * abb[70];
z[99] = abb[53] * abb[68];
z[100] = z[98] + -z[99];
z[100] = abb[23] * z[100];
z[101] = abb[70] + z[31];
z[102] = abb[53] * z[101];
z[102] = z[99] + z[102];
z[102] = abb[20] * z[102];
z[98] = z[98] + z[99];
z[98] = abb[24] * z[98];
z[99] = 2 * abb[59];
z[103] = -abb[64] + z[99];
z[104] = abb[63] + -abb[65];
z[105] = z[103] + -z[104];
z[106] = -abb[80] * z[105];
z[107] = -abb[42] * m1_set::bc<T>[0];
z[107] = abb[50] + abb[52] + z[107];
z[107] = z[24] * z[107];
z[108] = -abb[66] + z[30];
z[109] = abb[55] * z[108];
z[110] = -abb[68] + z[28];
z[111] = abb[54] * z[110];
z[5] = z[5] + -2 * z[16] + z[86] + z[87] + z[91] + z[92] + z[93] + z[97] + z[98] + z[100] + z[102] + z[106] + z[107] + z[109] + z[111];
z[5] = 2 * z[5];
z[16] = z[33] + z[45];
z[86] = 2 * abb[61];
z[3] = 2 * abb[63] + -z[3] + -z[11] + z[86];
z[11] = abb[28] * z[3];
z[42] = z[35] + -z[42];
z[87] = abb[15] * z[26];
z[91] = z[49] + z[87];
z[20] = z[7] + z[20];
z[92] = abb[70] + -4 * z[22] + -z[31];
z[92] = abb[5] * z[92];
z[93] = 2 * z[9];
z[97] = abb[68] + z[22];
z[98] = abb[11] * z[97];
z[98] = 2 * z[98];
z[100] = abb[8] * z[8];
z[102] = 2 * z[100];
z[27] = -z[11] + z[16] + z[20] + z[27] + -z[40] + 2 * z[42] + -z[77] + z[91] + z[92] + -z[93] + z[98] + -z[102];
z[27] = abb[37] * z[27];
z[42] = z[46] + z[70];
z[77] = 4 * z[100];
z[92] = -z[77] + z[91];
z[106] = abb[2] * z[34];
z[9] = z[9] + z[17] + z[42] + z[63] + -z[92] + -z[106];
z[9] = z[9] * z[65];
z[17] = 2 * z[70];
z[63] = abb[9] * z[94];
z[65] = z[17] + -z[63];
z[70] = abb[13] * z[34];
z[107] = abb[6] * z[50];
z[109] = z[70] + -z[107];
z[111] = abb[7] * z[110];
z[111] = -z[49] + z[65] + z[109] + -z[111];
z[112] = 2 * abb[62];
z[86] = z[86] + z[103] + z[104] + -z[112];
z[113] = -abb[29] * z[86];
z[114] = abb[27] * z[105];
z[115] = abb[2] * z[110];
z[115] = -z[114] + z[115];
z[116] = z[39] + z[46];
z[117] = z[87] + -z[116];
z[113] = -z[77] + -z[111] + z[113] + -z[115] + 2 * z[117];
z[113] = abb[39] * z[113];
z[58] = -z[39] + z[58];
z[58] = 2 * z[58];
z[117] = abb[7] * z[50];
z[118] = z[58] + -z[117];
z[119] = -z[49] + z[63];
z[120] = abb[13] * z[31];
z[121] = z[119] + z[120];
z[122] = abb[6] * z[108];
z[122] = -z[121] + z[122];
z[123] = -2 * z[13] + -z[77] + -z[118] + z[122];
z[123] = abb[41] * z[123];
z[124] = -abb[70] + z[25];
z[124] = abb[5] * z[124];
z[125] = z[39] + -z[98] + z[124];
z[126] = abb[7] * z[108];
z[121] = z[107] + z[121] + 2 * z[125] + z[126];
z[121] = abb[42] * z[121];
z[125] = z[38] + z[98];
z[126] = -z[39] + z[125];
z[82] = z[82] + -z[87] + -z[126];
z[127] = 3 * abb[68] + z[28];
z[128] = abb[2] * z[127];
z[114] = -z[114] + z[128];
z[82] = 2 * z[82] + z[111] + z[114];
z[82] = abb[40] * z[82];
z[111] = abb[40] * z[86];
z[128] = -abb[63] + z[99];
z[1] = abb[64] + -abb[65] + z[1] + -z[112] + z[128];
z[112] = abb[42] * z[1];
z[129] = abb[41] * z[105];
z[111] = z[111] + -z[112] + z[129];
z[111] = abb[29] * z[111];
z[1] = abb[41] * z[1];
z[130] = abb[42] * z[105];
z[131] = -z[1] + z[130];
z[131] = abb[27] * z[131];
z[132] = -abb[41] + abb[42];
z[133] = abb[39] + z[132];
z[133] = z[41] * z[133];
z[134] = 3 * abb[66];
z[135] = abb[68] + z[25] + z[134];
z[135] = abb[40] * z[135];
z[133] = z[133] + z[135];
z[133] = z[44] * z[133];
z[135] = abb[41] * z[108];
z[136] = abb[40] * z[108];
z[135] = z[135] + z[136];
z[137] = abb[42] * z[50];
z[137] = z[135] + z[137];
z[134] = z[30] + z[134];
z[138] = abb[39] * z[134];
z[139] = 4 * abb[66];
z[140] = -abb[35] * z[139];
z[138] = -z[137] + z[138] + z[140];
z[138] = abb[3] * z[138];
z[140] = abb[42] * z[110];
z[127] = abb[41] * z[127];
z[127] = z[127] + -z[140];
z[127] = abb[2] * z[127];
z[9] = z[9] + z[27] + z[82] + z[111] + z[113] + z[121] + z[123] + z[127] + z[131] + z[133] + z[138];
z[9] = abb[37] * z[9];
z[27] = z[107] + z[117];
z[82] = -abb[29] * z[105];
z[111] = -abb[4] + -abb[5];
z[111] = abb[68] * z[111];
z[7] = -z[7] + -z[13] + -z[27] + -z[42] + z[48] + z[56] + z[82] + z[87] + -6 * z[100] + z[111] + z[114];
z[7] = abb[35] * z[7];
z[42] = -z[68] + z[70];
z[56] = -abb[61] + abb[62];
z[0] = -4 * abb[59] + z[0] + z[56] + z[104];
z[68] = -abb[27] * z[0];
z[57] = -z[42] + -z[57] + z[64] + z[68] + z[91] + 2 * z[107];
z[57] = abb[40] * z[57];
z[68] = z[28] + z[34];
z[70] = abb[2] * z[68];
z[82] = z[70] + z[92];
z[0] = -abb[29] * z[0];
z[68] = abb[7] * z[68];
z[0] = z[0] + z[42] + z[68] + -z[71] + -z[82];
z[0] = abb[39] * z[0];
z[42] = abb[6] * z[32];
z[68] = abb[2] * z[50];
z[42] = z[42] + z[51] + -2 * z[68] + z[77] + z[120];
z[42] = abb[41] * z[42];
z[51] = abb[26] + z[81];
z[51] = z[51] * z[80];
z[62] = -z[62] + 2 * z[117] + -z[120];
z[62] = abb[42] * z[62];
z[71] = abb[40] * z[2];
z[77] = -z[71] + 2 * z[130];
z[77] = abb[29] * z[77];
z[80] = abb[60] + -abb[62] + z[128];
z[91] = abb[41] * z[80];
z[91] = -z[79] + z[91];
z[91] = abb[27] * z[91];
z[92] = z[15] * z[132];
z[0] = z[0] + z[7] + z[42] + z[51] + z[57] + -z[59] + z[62] + z[77] + 2 * z[91] + z[92];
z[0] = abb[35] * z[0];
z[3] = -abb[30] * z[3];
z[7] = z[78] + z[102];
z[42] = -abb[5] * z[101];
z[51] = -abb[2] * z[31];
z[3] = z[3] + -z[7] + -z[11] + z[42] + z[51] + 2 * z[52] + -z[54] + -z[60] + z[93] + z[119];
z[3] = abb[38] * z[3];
z[11] = abb[16] * z[94];
z[11] = z[11] + 2 * z[48] + -z[49] + -z[58] + -z[63] + z[107] + -z[117] + z[120];
z[11] = abb[42] * z[11];
z[42] = z[54] + z[67] + z[116];
z[49] = abb[27] * z[86];
z[51] = z[117] + z[119];
z[17] = -z[17] + -2 * z[42] + -z[49] + z[51] + -z[68] + z[109];
z[42] = abb[40] * z[17];
z[1] = -z[1] + z[112];
z[1] = abb[27] * z[1];
z[39] = -z[39] + z[83];
z[39] = 2 * z[39];
z[49] = z[39] + z[117] + z[122];
z[49] = abb[41] * z[49];
z[52] = z[81] * z[88];
z[57] = -abb[70] + z[8];
z[57] = abb[41] * z[57];
z[57] = z[57] + -z[140];
z[57] = abb[2] * z[57];
z[1] = z[1] + z[3] + z[11] + z[42] + z[49] + z[52] + z[57];
z[1] = abb[38] * z[1];
z[3] = -z[7] + z[47];
z[11] = -abb[3] * z[31];
z[11] = -z[3] + z[11] + -z[24] + -z[35] + z[43] + -z[106] + z[125];
z[11] = abb[37] * z[11];
z[24] = z[56] + -z[103];
z[35] = abb[29] * z[24];
z[42] = abb[7] * abb[68];
z[35] = z[35] + -z[42];
z[43] = abb[27] * z[24];
z[47] = abb[2] * abb[68];
z[49] = abb[6] * abb[66];
z[47] = z[3] + z[35] + z[43] + z[47] + -z[49];
z[52] = abb[35] * z[47];
z[56] = abb[35] * z[8];
z[57] = abb[39] * abb[68];
z[58] = -abb[41] * abb[66];
z[36] = abb[42] * z[36];
z[59] = abb[40] * z[97];
z[36] = z[36] + z[56] + -z[57] + z[58] + z[59];
z[36] = z[36] * z[44];
z[58] = abb[7] + -abb[13];
z[58] = abb[66] * z[58];
z[59] = abb[29] * z[80];
z[38] = -z[38] + z[58] + z[59];
z[38] = abb[42] * z[38];
z[58] = abb[2] + -abb[13];
z[59] = -abb[66] * z[58];
z[60] = abb[27] * z[80];
z[59] = z[49] + z[59] + z[60];
z[59] = abb[41] * z[59];
z[58] = abb[6] + z[58];
z[58] = abb[68] * z[58];
z[43] = -z[43] + z[58] + -z[98];
z[43] = abb[40] * z[43];
z[58] = -abb[2] * z[41];
z[3] = -z[3] + -z[53] + z[58];
z[3] = abb[38] * z[3];
z[41] = abb[38] * z[41];
z[58] = abb[35] + abb[42];
z[58] = abb[66] * z[58];
z[58] = z[41] + -z[57] + z[58];
z[58] = abb[3] * z[58];
z[60] = abb[13] * abb[68];
z[35] = -z[35] + z[60];
z[35] = abb[39] * z[35];
z[3] = z[3] + z[11] + z[35] + z[36] + z[38] + z[43] + z[52] + z[58] + z[59];
z[11] = z[8] * z[44];
z[35] = abb[3] * abb[66];
z[11] = z[11] + z[35] + z[47];
z[11] = abb[36] * z[11];
z[3] = 2 * z[3] + z[11];
z[3] = abb[36] * z[3];
z[11] = -abb[41] * z[32];
z[35] = -z[50] * z[76];
z[36] = abb[35] * z[134];
z[11] = z[11] + z[35] + z[36] + -z[61] + z[95];
z[11] = abb[35] * z[11];
z[35] = abb[38] * z[108];
z[36] = abb[39] * z[26];
z[35] = z[35] + z[36] + z[135];
z[35] = abb[39] * z[35];
z[36] = abb[46] * abb[66];
z[38] = abb[46] * abb[68];
z[43] = z[36] + -z[38];
z[43] = 2 * z[43];
z[47] = 2 * abb[70] + z[34];
z[52] = abb[66] + z[47];
z[58] = abb[41] * abb[42];
z[59] = z[52] * z[58];
z[60] = -z[41] + -z[137];
z[60] = abb[38] * z[60];
z[61] = prod_pow(m1_set::bc<T>[0], 2);
z[62] = abb[68] * (T(-2) / T(3)) + abb[70] * (T(-1) / T(3)) + -z[31];
z[62] = z[61] * z[62];
z[67] = abb[47] * z[31];
z[68] = abb[42] * z[136];
z[76] = abb[45] * z[94];
z[11] = z[11] + z[35] + z[43] + z[59] + z[60] + z[62] + z[67] + z[68] + z[76];
z[11] = abb[3] * z[11];
z[35] = -abb[28] * z[12];
z[6] = -z[6] + -z[13] + -z[20] + z[33] + z[35] + -z[53] + z[82];
z[6] = abb[43] * z[6];
z[20] = z[48] + -z[63];
z[33] = -z[12] * z[81];
z[35] = -abb[2] * z[14];
z[33] = -z[20] + z[33] + z[35] + -z[55] + -z[83] + z[85];
z[33] = abb[45] * z[33];
z[35] = abb[5] * z[28];
z[29] = z[29] + -z[35] + z[63] + -z[70] + z[87] + z[98];
z[35] = z[29] + -z[45] + -z[73];
z[35] = abb[44] * z[35];
z[45] = -abb[29] * z[12];
z[16] = -z[16] + z[45] + -z[84];
z[16] = abb[74] * z[16];
z[6] = z[6] + z[16] + z[33] + z[35];
z[16] = abb[62] + -abb[64];
z[33] = -abb[61] + z[16] + z[99] + -2 * z[104];
z[35] = -abb[27] * z[33];
z[45] = abb[2] + -abb[7];
z[53] = abb[68] + 2 * z[28];
z[45] = z[45] * z[53];
z[53] = abb[5] * z[31];
z[35] = z[35] + z[45] + z[53] + z[65] + z[69] + -z[87] + -z[107];
z[35] = abb[40] * z[35];
z[17] = -abb[38] * z[17];
z[27] = -z[27] + -z[39] + z[115] + z[119];
z[27] = abb[41] * z[27];
z[33] = -abb[40] * z[33];
z[39] = -abb[38] * z[105];
z[33] = z[33] + z[39] + -z[129];
z[33] = abb[29] * z[33];
z[39] = -abb[39] * z[75];
z[17] = z[17] + z[27] + z[33] + z[35] + z[39];
z[17] = abb[39] * z[17];
z[25] = -z[8] + -z[25];
z[25] = abb[42] * z[25];
z[27] = -abb[40] * z[23];
z[25] = z[25] + z[27];
z[25] = abb[40] * z[25];
z[27] = abb[40] + abb[41];
z[33] = -abb[66] * z[27];
z[35] = -abb[42] * abb[68];
z[33] = z[33] + z[35] + -z[57];
z[33] = 2 * z[33] + z[56];
z[33] = abb[35] * z[33];
z[35] = 2 * abb[47];
z[39] = z[35] + z[58];
z[39] = z[8] * z[39];
z[45] = 2 * abb[72];
z[23] = -z[23] * z[45];
z[53] = abb[42] + -z[27];
z[53] = z[41] * z[53];
z[8] = z[8] * z[27];
z[8] = z[8] + z[41];
z[8] = abb[39] * z[8];
z[22] = -4 * abb[68] + -5 * z[22] + -z[139];
z[22] = z[22] * z[61];
z[8] = z[8] + (T(1) / T(3)) * z[22] + z[23] + z[25] + z[33] + z[39] + z[53];
z[8] = z[8] * z[44];
z[22] = z[29] + z[64] + -z[72];
z[22] = abb[40] * z[22];
z[23] = -z[124] + z[126];
z[23] = 2 * z[23] + -z[51] + -z[107] + z[115];
z[23] = abb[42] * z[23];
z[22] = z[22] + z[23];
z[22] = abb[40] * z[22];
z[23] = abb[40] * z[105];
z[23] = z[23] + z[129] + -z[130];
z[23] = abb[38] * z[23];
z[25] = z[58] * z[105];
z[24] = z[24] * z[35];
z[24] = -z[24] + z[25];
z[25] = -z[71] + -z[130];
z[25] = abb[40] * z[25];
z[27] = 2 * abb[71];
z[29] = -z[2] * z[27];
z[33] = -z[18] * z[45];
z[23] = z[23] + -z[24] + z[25] + z[29] + z[33];
z[23] = abb[29] * z[23];
z[25] = -abb[66] + z[47];
z[25] = abb[6] * z[25];
z[29] = abb[2] * z[101];
z[25] = z[25] + z[29] + 10 * z[37] + z[40] + z[54] + z[74];
z[29] = (T(-1) / T(3)) * z[81];
z[4] = z[4] * z[29];
z[13] = z[13] + z[48] + z[55];
z[29] = abb[59] + -abb[63];
z[16] = abb[65] + abb[60] * (T(-5) / T(3)) + abb[61] * (T(1) / T(3)) + (T(4) / T(3)) * z[16] + (T(-2) / T(3)) * z[29];
z[16] = z[16] * z[89];
z[29] = abb[70] + abb[66] * (T(-2) / T(3)) + abb[68] * (T(4) / T(3));
z[29] = abb[7] * z[29];
z[33] = abb[0] * z[50];
z[4] = z[4] + (T(-2) / T(3)) * z[13] + z[16] + (T(1) / T(3)) * z[25] + z[29] + (T(4) / T(3)) * z[33];
z[4] = z[4] * z[61];
z[7] = -z[7] + z[19] + -z[42] + -z[46] + -z[49];
z[7] = z[7] * z[35];
z[13] = -abb[6] * z[52];
z[13] = z[13] + -z[15] + -z[20] + z[118];
z[13] = z[13] * z[58];
z[10] = -z[10] + z[54] + z[64];
z[10] = z[10] * z[27];
z[15] = z[21] * z[45];
z[16] = -abb[46] * z[18];
z[16] = 2 * z[16];
z[12] = -z[12] * z[27];
z[12] = z[12] + -z[16] + -z[24];
z[12] = abb[27] * z[12];
z[19] = abb[19] + -abb[24];
z[19] = z[19] * z[30];
z[20] = -abb[20] * z[32];
z[14] = abb[23] * z[14];
z[14] = z[14] + z[19] + z[20] + z[96];
z[14] = abb[75] * z[14];
z[19] = -abb[73] * z[90];
z[2] = abb[33] * z[2];
z[20] = abb[21] + -abb[25];
z[21] = -abb[22] + -z[20];
z[21] = abb[70] * z[21];
z[20] = abb[22] + -z[20];
z[20] = abb[66] * z[20];
z[2] = z[2] + z[20] + z[21];
z[2] = abb[48] * z[2];
z[20] = abb[41] * z[79];
z[16] = z[16] + z[20];
z[21] = -abb[28] * z[16];
z[18] = z[18] * z[35];
z[16] = -z[16] + z[18];
z[16] = abb[30] * z[16];
z[18] = z[58] * z[110];
z[24] = abb[47] * z[34];
z[18] = z[18] + z[24] + -z[43];
z[18] = abb[2] * z[18];
z[24] = z[36] + z[38];
z[25] = -4 * abb[8] + 2 * abb[13];
z[24] = z[24] * z[25];
z[25] = -abb[46] * z[31] * z[66];
z[27] = abb[18] * abb[48];
z[27] = -abb[76] + abb[78] + z[27];
z[27] = z[27] * z[28];
z[28] = -2 * z[66];
z[28] = abb[46] * z[28];
z[29] = abb[21] * abb[48];
z[28] = abb[76] + -abb[77] + abb[78] + z[28] + -2 * z[29];
z[28] = abb[68] * z[28];
z[29] = -abb[58] * z[105];
z[30] = -z[30] * z[58];
z[31] = -abb[71] * z[94];
z[30] = z[30] + z[31];
z[30] = abb[16] * z[30];
z[20] = abb[26] * z[20];
z[26] = abb[77] * z[26];
z[31] = abb[79] * z[50];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + 2 * z[6] + z[7] + z[8] + z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + z[25] + z[26] + z[27] + z[28] + z[29] + z[30] + z[31];
z[0] = 2 * z[0];
return {z[5], z[0]};
}


template <typename T> std::complex<T> f_4_805_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{T(0),stof<T>("43.326309677614922727734444793627968626357572945891214112613952377")}, std::complex<T>{stof<T>("15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("-1.039733694612781747820963243788731508063882544428908226050699054")}, std::complex<T>{stof<T>("-5.2011514095656166681359700915956610682851384412386396596596286985"),stof<T>("6.3846944890724516316818920884543452858400077107515244973573541566")}, std::complex<T>{stof<T>("-10.2312275182042318649043666124740212509707261335343338040808817496"),stof<T>("-5.344960794459669883860928844665613777776125166322616271306655103")}, std::complex<T>{stof<T>("-5.201151409565616668135970091595661068285138441238639659659628698"),stof<T>("-15.278460349735009732185330308359639027338778762194082558949622032")}, std::complex<T>{stof<T>("15.432378927769848533040336704069682319255864574772973463740510448"),stof<T>("-22.702888533420243111688185640602715821242669017374515282357675242")}, std::complex<T>{stof<T>("-10.23122751820423186490436661247402125097072613353433380408088175"),stof<T>("16.318194044347791480006293552148370535402661306622990785000321086")}, std::complex<T>{stof<T>("4.2957629656815018373855767348030281230352659938409329766615800432"),stof<T>("-4.3189018782410098948513983227365868076716924192490656589465367641")}, std::complex<T>{stof<T>("25.514205731730803297405729886997480010359956010634664886931192621"),stof<T>("36.916552190357586975765762273827768645282794678010635435462489747")}, std::complex<T>{stof<T>("0.2116482453615020981651573360484742754108311832618865884313999034"),stof<T>("1.7606299963135058907292223372868390240467892650404665348344365107")}, std::complex<T>{stof<T>("25.514205731730803297405729886997480010359956010634664886931192621"),stof<T>("36.916552190357586975765762273827768645282794678010635435462489747")}, std::complex<T>{stof<T>("-27.684105602702568343335706257260005235672152911204732120021121204"),stof<T>("-19.779461597949540164356049334922093363520271008752783240987601397")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[31].real()/kbase.W[31].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[134]) - log_iphi_im(kbase.W[134]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_805_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_805_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("17.54788233364647729229789933765235065335508026836022713055770326"),stof<T>("-38.493820526471870964829953237337169380356630871743224037035876198")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({198});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);dlr = dlog_roots(k.v,k,dl);

            std::array<T,81> abb = {dl[0], dl[1], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W13(k,dl), dlog_W14(k,dl), dlog_W21(k,dl), dlog_W24(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W30(k,dl), dlog_W31(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W129(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_1_8(k), f_1_9(k), f_2_3(k), f_2_5(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), f_2_9_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, T{0}, T{0}, rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[31].real()/k.W[31].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[134]) - log_iphi_im(k.W[134])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k), f_2_9_re(k), f_2_25_re(k), T{0}, T{0}, T{0}, T{0}, T{0}};
{
auto c = dlog_W162(k,dv) * f_2_30(k);
abb[76] = c.real();
abb[54] = c.imag();
SpDLog_Sigma5<T,198,161>(k, dv, abb[76], abb[54], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W164(k,dv) * f_2_30(k);
abb[77] = c.real();
abb[55] = c.imag();
SpDLog_Sigma5<T,198,163>(k, dv, abb[77], abb[55], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W183(k,dv) * f_2_30(k);
abb[78] = c.real();
abb[56] = c.imag();
SpDLog_Sigma5<T,198,182>(k, dv, abb[78], abb[56], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W185(k,dv) * f_2_30(k);
abb[79] = c.real();
abb[57] = c.imag();
SpDLog_Sigma5<T,198,184>(k, dv, abb[79], abb[57], f_2_30_series_coefficients<T>);
}
{
auto c = dlog_W189(k,dv) * f_2_30(k);
abb[80] = c.real();
abb[58] = c.imag();
SpDLog_Sigma5<T,198,188>(k, dv, abb[80], abb[58], f_2_30_series_coefficients<T>);
}

                    
            return f_4_805_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_805_DLogXconstant_part(base_point<T>, kend);
	value += f_4_805_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_805_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_805_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_805_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_805_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_805_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_805_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
