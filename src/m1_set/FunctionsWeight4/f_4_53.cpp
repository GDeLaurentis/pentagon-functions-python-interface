/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_53.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_53_abbreviated (const std::array<T,53>& abb) {
T z[65];
z[0] = abb[27] + -abb[32];
z[1] = abb[31] + z[0];
z[2] = -abb[30] + z[1];
z[3] = m1_set::bc<T>[0] * z[2];
z[4] = abb[38] + z[3];
z[5] = abb[0] * (T(1) / T(2));
z[6] = z[4] * z[5];
z[7] = abb[2] * abb[38];
z[8] = z[6] + z[7];
z[9] = abb[29] + -abb[32];
z[10] = m1_set::bc<T>[0] * z[9];
z[11] = abb[39] + z[10];
z[11] = abb[7] * z[11];
z[12] = abb[38] + -abb[39];
z[13] = abb[13] * z[12];
z[14] = z[11] + -z[13];
z[4] = abb[40] + z[4];
z[15] = abb[15] * (T(1) / T(2));
z[16] = z[4] * z[15];
z[17] = -abb[5] + abb[9];
z[18] = -abb[27] + abb[29];
z[19] = -z[17] * z[18];
z[20] = abb[11] * z[9];
z[19] = z[19] + z[20];
z[19] = m1_set::bc<T>[0] * z[19];
z[20] = abb[40] + z[3];
z[21] = abb[3] * (T(1) / T(2));
z[21] = -z[20] * z[21];
z[22] = abb[5] * z[12];
z[23] = abb[6] * abb[39];
z[24] = abb[40] * (T(1) / T(2));
z[25] = abb[39] + abb[38] * (T(-1) / T(2)) + -z[24];
z[25] = abb[8] * z[25];
z[19] = -z[8] + -z[14] + z[16] + z[19] + z[21] + -z[22] + z[23] + z[25];
z[21] = abb[44] * (T(1) / T(2));
z[19] = z[19] * z[21];
z[23] = -abb[39] + -abb[40];
z[23] = abb[6] * z[23];
z[11] = z[11] + z[22] + -z[23];
z[22] = -abb[28] + z[18];
z[23] = -abb[30] + -z[22];
z[23] = abb[13] * z[23];
z[25] = abb[31] + -abb[32];
z[26] = abb[29] + z[25];
z[27] = abb[30] + -z[26];
z[27] = abb[6] * z[27];
z[28] = abb[5] * z[18];
z[23] = z[23] + z[27] + z[28];
z[27] = -abb[9] * z[18];
z[23] = (T(1) / T(2)) * z[23] + z[27];
z[23] = m1_set::bc<T>[0] * z[23];
z[27] = abb[8] * (T(1) / T(2));
z[22] = -abb[31] + -z[22];
z[22] = m1_set::bc<T>[0] * z[22];
z[22] = -abb[39] + z[22];
z[22] = z[22] * z[27];
z[28] = abb[27] + abb[32];
z[29] = -abb[29] + (T(1) / T(2)) * z[28];
z[30] = -m1_set::bc<T>[0] * z[29];
z[30] = abb[39] + z[30];
z[30] = abb[3] * z[30];
z[11] = -z[8] + (T(-1) / T(2)) * z[11] + (T(3) / T(2)) * z[13] + z[22] + z[23] + z[30];
z[13] = abb[31] * (T(1) / T(2));
z[22] = -z[13] + z[29];
z[23] = abb[28] * (T(1) / T(2));
z[30] = z[22] + z[23];
z[30] = m1_set::bc<T>[0] * z[30];
z[31] = -abb[38] + 3 * abb[39];
z[31] = -z[30] + (T(1) / T(2)) * z[31];
z[31] = abb[12] * z[31];
z[32] = abb[4] * (T(1) / T(4));
z[33] = abb[28] + -abb[31];
z[33] = -m1_set::bc<T>[0] * z[33];
z[33] = abb[40] + z[33];
z[33] = z[32] * z[33];
z[11] = (T(1) / T(2)) * z[11] + z[31] + z[33];
z[11] = abb[47] * z[11];
z[33] = -abb[41] + -abb[42] + abb[43];
z[34] = -abb[32] * z[33];
z[35] = abb[31] * z[33];
z[36] = z[34] + z[35];
z[37] = abb[27] * z[33];
z[38] = z[36] + z[37];
z[39] = abb[30] * z[33];
z[40] = -z[38] + z[39];
z[41] = -m1_set::bc<T>[0] * z[40];
z[42] = abb[38] * z[33];
z[43] = abb[40] * z[33];
z[41] = z[41] + z[42] + z[43];
z[41] = abb[24] * z[41];
z[44] = abb[29] * z[33];
z[36] = -z[36] + z[39] + -z[44];
z[36] = m1_set::bc<T>[0] * z[36];
z[45] = abb[39] * z[33];
z[45] = z[43] + z[45];
z[36] = z[36] + -z[45];
z[46] = abb[21] * z[36];
z[39] = -z[35] + z[39];
z[39] = m1_set::bc<T>[0] * z[39];
z[39] = z[39] + -z[43];
z[39] = abb[20] * z[39];
z[43] = abb[38] + -abb[40];
z[47] = abb[16] * z[43];
z[4] = -abb[19] * z[4];
z[4] = z[4] + z[47];
z[4] = abb[49] * z[4];
z[4] = z[4] + z[39] + z[41] + z[46];
z[39] = z[27] * z[43];
z[16] = z[16] + z[39];
z[3] = (T(1) / T(2)) * z[3];
z[12] = -z[3] + -z[12] + -z[24];
z[12] = abb[3] * z[12];
z[10] = abb[11] * z[10];
z[10] = z[10] + -z[14];
z[6] = z[6] + z[10] + z[12] + z[16];
z[12] = abb[39] * (T(1) / T(2)) + -z[30];
z[12] = abb[1] * z[12];
z[12] = z[12] + -z[31];
z[3] = abb[38] + z[3];
z[14] = abb[10] * z[3];
z[6] = (T(1) / T(2)) * z[6] + -z[12] + -z[14];
z[6] = abb[46] * z[6];
z[3] = -z[3] + -z[24];
z[3] = abb[3] * z[3];
z[3] = z[3] + z[8] + z[16];
z[3] = (T(1) / T(2)) * z[3] + -z[14];
z[3] = abb[48] * z[3];
z[8] = abb[3] * abb[39];
z[7] = -z[7] + z[8] + z[10];
z[7] = (T(1) / T(2)) * z[7] + -z[12];
z[7] = abb[45] * z[7];
z[8] = -z[37] + z[44];
z[10] = abb[28] * z[33];
z[12] = -z[8] + z[10] + -z[35];
z[12] = m1_set::bc<T>[0] * z[12];
z[12] = z[12] + z[42] + -z[45];
z[14] = abb[22] * (T(1) / T(4));
z[12] = z[12] * z[14];
z[16] = abb[23] * (T(1) / T(4));
z[24] = z[16] * z[36];
z[30] = abb[17] * (T(1) / T(4));
z[20] = -abb[49] * z[20] * z[30];
z[3] = z[3] + (T(1) / T(4)) * z[4] + z[6] + z[7] + z[11] + z[12] + z[19] + z[20] + z[24];
z[3] = (T(1) / T(4)) * z[3];
z[4] = z[18] + -z[23];
z[6] = abb[28] * z[4];
z[7] = prod_pow(abb[27], 2);
z[7] = (T(1) / T(2)) * z[7];
z[11] = -abb[36] + z[7];
z[12] = prod_pow(abb[29], 2);
z[19] = abb[35] + (T(1) / T(2)) * z[12];
z[20] = abb[30] * (T(1) / T(2));
z[24] = -abb[28] + z[20];
z[31] = abb[30] * z[24];
z[36] = z[6] + z[11] + -z[19] + -z[31];
z[39] = abb[51] + z[36];
z[39] = abb[5] * z[39];
z[36] = abb[9] * z[36];
z[41] = abb[27] * (T(1) / T(2));
z[42] = z[25] + z[41];
z[42] = abb[27] * z[42];
z[43] = z[1] + -z[20];
z[43] = abb[30] * z[43];
z[45] = prod_pow(m1_set::bc<T>[0], 2);
z[46] = (T(1) / T(6)) * z[45];
z[47] = z[43] + z[46];
z[48] = abb[50] + z[47];
z[49] = z[42] + -z[48];
z[5] = z[5] * z[49];
z[36] = -z[5] + z[36];
z[49] = prod_pow(abb[32], 2);
z[50] = z[12] + -z[49];
z[51] = abb[28] * z[9];
z[50] = abb[34] + (T(-1) / T(2)) * z[50] + z[51];
z[51] = abb[11] * z[50];
z[50] = abb[51] + z[50];
z[50] = abb[7] * z[50];
z[52] = abb[13] * abb[51];
z[51] = -z[50] + z[51] + -z[52];
z[0] = abb[27] * z[0];
z[1] = abb[31] * z[1];
z[0] = z[0] + z[1];
z[0] = -abb[52] + (T(1) / T(2)) * z[0];
z[1] = z[0] + -z[48];
z[15] = z[1] * z[15];
z[48] = z[0] + -z[47];
z[53] = (T(1) / T(2)) * z[48];
z[54] = -abb[3] * z[53];
z[25] = -abb[27] + z[25];
z[25] = abb[31] * z[25];
z[55] = abb[27] * abb[32];
z[25] = z[25] + z[55];
z[25] = -abb[52] + (T(1) / T(2)) * z[25];
z[56] = abb[50] * (T(1) / T(2));
z[57] = -abb[51] + (T(-1) / T(2)) * z[25] + z[56];
z[57] = abb[8] * z[57];
z[58] = -abb[16] + abb[19];
z[59] = abb[37] * (T(1) / T(4));
z[60] = z[58] * z[59];
z[61] = -abb[6] * abb[51];
z[62] = -abb[2] + abb[13];
z[63] = abb[5] + -z[62];
z[63] = abb[50] * z[63];
z[17] = abb[33] * z[17];
z[64] = -abb[37] * z[30];
z[17] = z[15] + z[17] + z[36] + -z[39] + -z[51] + z[54] + z[57] + z[60] + z[61] + z[63] + z[64];
z[17] = z[17] * z[21];
z[2] = abb[28] + z[2];
z[2] = abb[30] * z[2];
z[21] = -abb[29] + z[28];
z[21] = abb[29] * z[21];
z[18] = -abb[31] * z[18];
z[26] = abb[28] * z[26];
z[18] = -z[2] + z[18] + z[21] + z[26] + -z[55];
z[18] = abb[13] * z[18];
z[26] = -abb[34] + abb[51];
z[54] = (T(1) / T(2)) * z[49];
z[55] = abb[36] + z[54];
z[21] = z[21] + z[26] + -z[55];
z[9] = -z[9] + -z[13];
z[9] = abb[31] * z[9];
z[2] = abb[52] + z[2] + z[6] + -z[7] + z[9] + z[21];
z[2] = abb[6] * z[2];
z[6] = abb[27] * z[28];
z[6] = z[6] + z[49];
z[9] = abb[33] + abb[34];
z[49] = abb[29] * (T(1) / T(2)) + -z[28];
z[49] = abb[29] * z[49];
z[57] = -abb[31] * z[29];
z[6] = (T(1) / T(2)) * z[6] + z[9] + z[49] + z[57];
z[6] = abb[14] * z[6];
z[49] = -abb[6] + abb[13];
z[49] = abb[35] * z[49];
z[2] = z[2] + z[6] + z[18] + -z[39] + z[49] + z[50] + 3 * z[52];
z[6] = abb[35] + -z[9] + -z[11] + z[12] + -z[54];
z[9] = abb[28] * (T(1) / T(4));
z[11] = z[9] + z[29];
z[11] = abb[28] * z[11];
z[12] = z[20] * z[24];
z[6] = -abb[51] + (T(1) / T(2)) * z[6] + z[11] + z[12];
z[6] = abb[3] * z[6];
z[4] = abb[31] + z[4];
z[4] = abb[28] * z[4];
z[11] = abb[29] * abb[31];
z[4] = z[4] + -z[11] + z[21] + z[46];
z[4] = z[4] * z[27];
z[12] = (T(1) / T(12)) * z[45];
z[18] = abb[6] * z[12];
z[21] = abb[2] + abb[13] * (T(-3) / T(2)) + abb[5] * (T(1) / T(2));
z[21] = abb[50] * z[21];
z[24] = -abb[5] + -abb[13];
z[24] = abb[9] + (T(1) / T(2)) * z[24];
z[24] = abb[33] * z[24];
z[29] = abb[18] * z[59];
z[2] = (T(1) / T(2)) * z[2] + z[4] + z[6] + z[18] + z[21] + z[24] + z[29] + z[36];
z[4] = abb[52] + z[46];
z[6] = prod_pow(abb[31], 2);
z[18] = -abb[28] * abb[31];
z[6] = -abb[35] + -z[4] + (T(1) / T(2)) * z[6] + z[18] + -z[31];
z[6] = z[6] * z[32];
z[18] = abb[29] * (T(3) / T(2)) + -z[28];
z[18] = abb[29] * z[18];
z[11] = abb[36] + z[11] + z[18];
z[9] = z[9] + z[22];
z[9] = abb[28] * z[9];
z[9] = z[9] + (T(1) / T(2)) * z[11] + -z[12];
z[11] = abb[51] * (T(3) / T(2)) + -z[9] + -z[56];
z[11] = abb[12] * z[11];
z[2] = (T(1) / T(2)) * z[2] + z[6] + -z[11];
z[2] = abb[47] * z[2];
z[6] = z[8] + z[34];
z[6] = abb[29] * z[6];
z[18] = z[33] * z[55];
z[6] = z[6] + z[18];
z[21] = z[23] * z[33];
z[8] = -z[8] + z[21];
z[22] = abb[28] * z[8];
z[10] = -z[10] + z[40];
z[10] = abb[30] * z[10];
z[23] = z[4] + z[26];
z[7] = -abb[35] + -z[7] + z[23];
z[7] = z[7] * z[33];
z[24] = z[13] * z[33];
z[24] = z[24] + z[44];
z[26] = -z[24] + -z[34];
z[26] = abb[31] * z[26];
z[7] = -z[6] + z[7] + -z[10] + -z[22] + z[26];
z[7] = abb[21] * z[7];
z[13] = z[13] * z[38];
z[26] = z[34] * z[41];
z[10] = z[10] + z[13] + z[26];
z[13] = z[34] + z[37];
z[21] = -z[13] + -z[21];
z[21] = abb[28] * z[21];
z[28] = abb[33] + z[4];
z[29] = -abb[34] + -abb[35] + z[28];
z[29] = z[29] * z[33];
z[18] = -z[10] + -z[18] + z[21] + z[29];
z[18] = abb[20] * z[18];
z[13] = abb[27] * z[13];
z[21] = abb[31] * z[38];
z[13] = z[13] + z[21];
z[20] = z[20] * z[33];
z[20] = z[20] + -z[38];
z[20] = abb[30] * z[20];
z[4] = -abb[50] + -z[4];
z[4] = z[4] * z[33];
z[4] = z[4] + (T(1) / T(2)) * z[13] + z[20];
z[4] = abb[24] * z[4];
z[1] = -abb[19] * z[1];
z[13] = abb[50] + z[25];
z[20] = -abb[16] * z[13];
z[1] = z[1] + z[20];
z[1] = abb[49] * z[1];
z[20] = abb[25] * z[33];
z[21] = -abb[3] + -abb[8] + -abb[15];
z[21] = abb[49] * z[21];
z[20] = z[20] + z[21];
z[21] = abb[37] * (T(1) / T(2));
z[20] = z[20] * z[21];
z[1] = z[1] + z[4] + z[7] + z[18] + z[20];
z[4] = -z[34] + z[37];
z[4] = (T(1) / T(2)) * z[4] + -z[24];
z[4] = abb[31] * z[4];
z[7] = -z[8] + z[35];
z[7] = abb[28] * z[7];
z[8] = -abb[50] + z[23];
z[8] = z[8] * z[33];
z[4] = z[4] + -z[6] + z[7] + z[8] + z[26];
z[4] = z[4] * z[14];
z[6] = z[13] * z[27];
z[5] = -z[5] + z[6] + -z[15];
z[0] = -z[0] + z[43];
z[0] = abb[50] + -abb[51] + (T(1) / T(2)) * z[0] + z[12];
z[0] = abb[3] * z[0];
z[6] = -abb[13] * abb[50];
z[0] = z[0] + -z[5] + z[6] + -z[51];
z[6] = abb[51] * (T(1) / T(2)) + -z[9];
z[6] = abb[1] * z[6];
z[6] = z[6] + -z[11];
z[7] = z[42] + -z[47];
z[7] = -abb[50] + (T(1) / T(2)) * z[7];
z[7] = abb[10] * z[7];
z[8] = -abb[17] + z[58];
z[9] = abb[37] * (T(1) / T(8));
z[8] = z[8] * z[9];
z[0] = (T(1) / T(2)) * z[0] + z[6] + -z[7] + z[8];
z[0] = abb[46] * z[0];
z[8] = abb[50] + -z[53];
z[8] = abb[3] * z[8];
z[11] = -abb[2] * abb[50];
z[5] = -z[5] + z[8] + z[11];
z[8] = z[9] * z[58];
z[5] = (T(1) / T(2)) * z[5] + -z[7] + z[8];
z[5] = abb[48] * z[5];
z[7] = -abb[50] * z[62];
z[8] = -abb[3] * abb[51];
z[7] = z[7] + z[8] + -z[51];
z[6] = z[6] + (T(1) / T(2)) * z[7];
z[6] = abb[45] * z[6];
z[7] = -abb[36] + abb[51] + -z[19] + z[28];
z[7] = z[7] * z[33];
z[7] = z[7] + -z[10] + -z[22];
z[7] = z[7] * z[16];
z[8] = -abb[49] * z[48];
z[9] = -abb[48] * z[21];
z[8] = z[8] + z[9];
z[8] = z[8] * z[30];
z[9] = abb[26] * abb[49] * z[21];
z[0] = z[0] + (T(1) / T(4)) * z[1] + z[2] + z[4] + z[5] + z[6] + z[7] + z[8] + z[9] + z[17];
z[0] = (T(1) / T(4)) * z[0];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_53_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("-0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.081267990774462760439624532681182204191955288144353744682181698414"),stof<T>("0.099760851391757056745029563882099145091250120480492570271208658697")}, std::complex<T>{stof<T>("-0.42552110457139013573176174912473632272117275176325433557444908128"),stof<T>("0.42766047927619119069515050032164696092239803311573155150475402806")}, std::complex<T>{stof<T>("-0.35634178373719340067241681473574753691040762604840669472739614819"),stof<T>("0.06411220403179017536929200344191981911694088026336784975280248553")}, std::complex<T>{stof<T>("-0.32783505779352584141901041773387745058899685637351807678096670347"),stof<T>("-0.02117969308723342926587963376439328458141610239887590962529290064")}, std::complex<T>{stof<T>("-0.31690742187608717925917866319904667327048146717859864354793279565"),stof<T>("0.19807001269437151672466438066621512054521874202570778041650167416")}, std::complex<T>{stof<T>("0.028506725943667559253406397001870086321410769674888617946429444721"),stof<T>("-0.085291897119023604635171637206313103698356982662243759378095386177")}, std::complex<T>{stof<T>("-0.19757415930072218380369023884017404767300818427486720038558645755"),stof<T>("0.27602727033741684098080560531544625171981658056668086306849614823")}};
	
	std::vector<C> intdlogs = {rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}, C{T{},(log_iphi_im(k.W[185]) - log_iphi_im(kbase.W[185]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_53_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_53_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("0.18015282672072209931603483675635724138602115369846005316782506576"),stof<T>("0.09057116692072558972886564779102762741615190556831182642412473628")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,53> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W21(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), (log_iphi_im(kend.W[185]) - log_iphi_im(k.W[185])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k)};

                    
            return f_4_53_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_53_DLogXconstant_part(base_point<T>, kend);
	value += f_4_53_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_53_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_53_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_53_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_53_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_53_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_53_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
