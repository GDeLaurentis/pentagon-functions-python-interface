/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_509.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_509_abbreviated (const std::array<T,31>& abb) {
T z[74];
z[0] = 4 * abb[21];
z[1] = 2 * abb[24];
z[2] = 2 * abb[25];
z[3] = -abb[20] + z[2];
z[4] = abb[22] + abb[27];
z[5] = 4 * abb[26] + -z[0] + z[1] + -z[3] + -z[4];
z[5] = abb[1] * z[5];
z[6] = 3 * abb[27];
z[7] = 3 * abb[22];
z[8] = 2 * abb[23];
z[9] = z[6] + z[7] + -z[8];
z[10] = 3 * abb[26];
z[11] = 3 * abb[21];
z[12] = -abb[25] + z[1] + -z[9] + z[10] + -z[11];
z[12] = abb[3] * z[12];
z[13] = 2 * abb[26];
z[14] = abb[20] + z[13];
z[15] = 2 * abb[21];
z[16] = z[14] + -z[15];
z[7] = -9 * abb[27] + z[7] + z[16];
z[17] = 6 * abb[23];
z[18] = z[2] + z[7] + z[17];
z[18] = abb[9] * z[18];
z[19] = abb[5] * abb[22];
z[20] = abb[5] * abb[21];
z[21] = abb[5] * abb[27];
z[22] = z[19] + -z[20] + 2 * z[21];
z[23] = z[4] + -z[8];
z[24] = z[2] + -z[16];
z[25] = z[23] + z[24];
z[26] = abb[6] * z[25];
z[27] = 2 * abb[22];
z[28] = 10 * abb[27] + -z[14] + -z[27];
z[29] = 9 * abb[23];
z[30] = -z[28] + z[29];
z[30] = abb[4] * z[30];
z[31] = z[26] + -z[30];
z[32] = 2 * abb[20];
z[10] = z[10] + z[32];
z[33] = z[10] + -z[11];
z[34] = 5 * abb[25];
z[35] = -4 * abb[23] + z[4] + -z[33] + z[34];
z[35] = abb[8] * z[35];
z[36] = abb[5] * abb[23];
z[37] = 4 * z[36];
z[12] = z[5] + z[12] + -z[18] + 2 * z[22] + -z[31] + z[35] + -z[37];
z[12] = abb[12] * z[12];
z[35] = abb[26] * (T(1) / T(2));
z[38] = abb[20] * (T(1) / T(2));
z[39] = abb[24] + z[35] + -z[38];
z[40] = abb[21] * (T(1) / T(2));
z[41] = abb[27] * (T(3) / T(2));
z[42] = -abb[25] + z[41];
z[43] = abb[22] * (T(3) / T(2));
z[44] = z[39] + z[40] + -z[42] + -z[43];
z[44] = abb[1] * z[44];
z[45] = -abb[20] + abb[24];
z[46] = abb[23] * (T(1) / T(2));
z[47] = abb[22] * (T(1) / T(2));
z[48] = z[42] + -z[45] + -z[46] + z[47];
z[48] = abb[0] * z[48];
z[49] = abb[26] + z[38];
z[50] = -abb[21] + z[49];
z[51] = -z[47] + z[50];
z[52] = abb[27] * (T(1) / T(2));
z[53] = abb[23] + -abb[25];
z[54] = z[51] + -z[52] + z[53];
z[54] = abb[6] * z[54];
z[55] = -abb[22] + 5 * abb[27];
z[56] = -z[49] + z[55];
z[57] = abb[23] * (T(9) / T(2)) + -z[56];
z[57] = abb[4] * z[57];
z[54] = z[54] + z[57];
z[58] = abb[25] * (T(5) / T(2));
z[59] = -abb[21] + abb[27] * (T(9) / T(2)) + -z[8] + -z[43] + -z[58];
z[59] = abb[8] * z[59];
z[60] = z[20] + -z[21];
z[61] = z[36] + z[60];
z[62] = abb[25] * (T(1) / T(2));
z[63] = -abb[23] + z[62];
z[64] = abb[21] + -abb[26];
z[65] = abb[20] + z[64];
z[65] = 6 * abb[22] + z[63] + (T(1) / T(2)) * z[65];
z[65] = abb[3] * z[65];
z[66] = 7 * abb[23] + abb[25] + z[7];
z[66] = abb[9] * z[66];
z[44] = z[44] + z[48] + -z[54] + z[59] + z[61] + z[65] + z[66];
z[44] = abb[11] * z[44];
z[48] = z[9] + -z[16];
z[48] = abb[3] * z[48];
z[7] = 8 * abb[23] + z[7];
z[59] = abb[9] * z[7];
z[48] = -z[30] + z[48] + z[59] + 2 * z[61];
z[59] = -abb[13] * z[48];
z[65] = abb[5] * z[8];
z[66] = abb[8] * z[53];
z[22] = -z[22] + z[65] + z[66];
z[67] = -z[13] + z[15];
z[1] = -abb[20] + z[1];
z[68] = z[1] + -z[67];
z[9] = z[9] + -z[68];
z[9] = abb[3] * z[9];
z[9] = z[9] + z[18] + 2 * z[22] + -z[30];
z[9] = abb[10] * z[9];
z[6] = abb[22] + z[6];
z[18] = -abb[23] + -z[2] + z[6] + -2 * z[45];
z[18] = abb[10] * z[18];
z[22] = 2 * abb[27];
z[69] = -abb[23] + z[22] + -z[24];
z[70] = -abb[12] * z[69];
z[71] = -abb[22] + abb[27];
z[72] = -abb[23] + z[71];
z[73] = -abb[13] * z[72];
z[18] = z[18] + z[70] + z[73];
z[18] = abb[0] * z[18];
z[9] = z[9] + z[12] + z[18] + z[44] + z[59];
z[9] = abb[11] * z[9];
z[8] = -abb[25] + z[8];
z[12] = -z[8] + z[41] + z[43] + -z[50];
z[12] = abb[3] * z[12];
z[16] = -abb[23] + 4 * abb[25] + -z[16] + -z[22];
z[16] = abb[8] * z[16];
z[18] = -z[4] + z[24];
z[18] = abb[1] * z[18];
z[24] = abb[25] + abb[27] * (T(-23) / T(2)) + abb[22] * (T(5) / T(2)) + z[29] + 3 * z[50];
z[24] = abb[9] * z[24];
z[29] = -abb[23] + z[42] + z[51];
z[29] = abb[0] * z[29];
z[12] = z[12] + -z[16] + z[18] + z[24] + z[29] + z[31] + 2 * z[60] + z[65];
z[12] = abb[16] * z[12];
z[18] = abb[25] * (T(3) / T(2));
z[24] = 3 * abb[23];
z[29] = z[18] + z[24] + -z[56];
z[29] = abb[6] * z[29];
z[41] = abb[9] * z[53];
z[29] = z[29] + z[41] + -z[57];
z[42] = z[45] + -z[53];
z[42] = abb[3] * z[42];
z[43] = abb[1] * z[45];
z[37] = -z[19] + -3 * z[20] + 4 * z[21] + -z[29] + -z[37] + -z[42] + z[43];
z[37] = abb[10] * z[37];
z[44] = z[21] + -z[36];
z[41] = z[20] + z[41] + z[43] + z[44] + -z[66];
z[41] = abb[12] * z[41];
z[37] = z[37] + 2 * z[41];
z[37] = abb[10] * z[37];
z[18] = z[18] + z[22] + -z[24] + z[27] + -z[49];
z[18] = abb[6] * z[18];
z[22] = z[49] + -z[62] + z[72];
z[22] = abb[0] * z[22];
z[24] = abb[3] * z[25];
z[25] = abb[22] + 10 * abb[23];
z[27] = 13 * abb[27] + -z[2] + -z[14] + -z[25];
z[27] = abb[9] * z[27];
z[16] = -z[16] + z[18] + z[22] + z[24] + -z[27] + -z[30];
z[18] = abb[28] * z[16];
z[6] = abb[21] * (T(-9) / T(2)) + abb[26] * (T(7) / T(2)) + z[6] + z[32] + -z[34];
z[6] = abb[8] * z[6];
z[3] = abb[26] * (T(-3) / T(2)) + abb[21] * (T(5) / T(2)) + z[3] + -z[4];
z[3] = abb[1] * z[3];
z[22] = abb[25] + z[64];
z[24] = z[22] + z[23];
z[24] = abb[3] * z[24];
z[32] = abb[0] * z[69];
z[3] = z[3] + z[6] + z[24] + -z[27] + z[31] + z[32];
z[6] = -abb[29] * z[3];
z[10] = z[10] + -z[15];
z[24] = abb[27] * (T(11) / T(2)) + -z[47];
z[27] = z[10] + z[17] + -z[24] + -z[58];
z[27] = abb[8] * z[27];
z[32] = abb[26] * (T(15) / T(2));
z[24] = -6 * abb[24] + abb[25] + abb[21] * (T(13) / T(2)) + z[24] + -z[32] + -z[38];
z[24] = abb[1] * z[24];
z[20] = z[19] + z[20] + z[24] + z[27] + -6 * z[44] + -z[54];
z[24] = prod_pow(abb[12], 2);
z[20] = z[20] * z[24];
z[27] = 3 * abb[24] + abb[27] * (T(-71) / T(12)) + abb[22] * (T(7) / T(12));
z[32] = abb[21] * (T(-19) / T(3)) + abb[25] * (T(-2) / T(3)) + abb[20] * (T(43) / T(12)) + z[27] + z[32];
z[32] = abb[1] * z[32];
z[38] = abb[22] + -17 * abb[27] + abb[25] * (T(7) / T(4)) + abb[23] * (T(25) / T(2)) + -z[15] + (T(11) / T(2)) * z[49];
z[38] = abb[6] * z[38];
z[41] = abb[25] * (T(1) / T(3));
z[35] = abb[20] * (T(-1) / T(3)) + -z[35];
z[35] = abb[21] * (T(19) / T(6)) + 11 * z[35];
z[35] = abb[23] * (T(-25) / T(6)) + abb[22] * (T(-1) / T(3)) + abb[27] * (T(17) / T(3)) + (T(1) / T(2)) * z[35] + z[41];
z[35] = abb[8] * z[35];
z[44] = -abb[20] + z[64];
z[44] = (T(7) / T(3)) * z[44] + z[71];
z[41] = z[41] + (T(1) / T(4)) * z[44];
z[41] = abb[3] * z[41];
z[27] = abb[26] * (T(-11) / T(6)) + abb[21] * (T(2) / T(3)) + abb[20] * (T(25) / T(12)) + -z[27] + -z[34];
z[27] = abb[0] * z[27];
z[34] = -abb[25] + z[71];
z[34] = abb[2] * z[34];
z[27] = z[27] + z[32] + (T(-1) / T(4)) * z[34] + z[35] + (T(1) / T(3)) * z[38] + z[41];
z[27] = prod_pow(m1_set::bc<T>[0], 2) * z[27];
z[32] = abb[0] * z[72];
z[32] = z[32] + z[48];
z[32] = abb[15] * z[32];
z[0] = -z[0] + z[14] + -z[71];
z[0] = abb[1] * z[0];
z[0] = z[0] + z[30] + -3 * z[61];
z[0] = abb[14] * z[0];
z[14] = -abb[3] * z[53];
z[14] = z[14] + z[29] + 3 * z[34] + z[61];
z[29] = prod_pow(abb[13], 2);
z[14] = z[14] * z[29];
z[34] = abb[24] + -abb[25] + -z[46] + -z[47] + z[52] + -z[67];
z[34] = z[24] * z[34];
z[35] = -abb[21] + abb[27];
z[38] = z[35] + -z[46] + -z[62];
z[29] = z[29] * z[38];
z[38] = abb[22] + -4 * abb[27] + abb[25] * (T(7) / T(2)) + z[11] + z[46];
z[38] = abb[10] * z[38];
z[41] = -abb[21] + abb[25] + -abb[27] + z[45];
z[41] = abb[12] * z[41];
z[38] = z[38] + 2 * z[41];
z[38] = abb[10] * z[38];
z[35] = abb[23] + -z[35];
z[35] = abb[14] * z[35];
z[29] = z[29] + z[34] + z[35] + z[38];
z[29] = abb[0] * z[29];
z[34] = z[39] + -z[40] + z[63];
z[34] = z[24] * z[34];
z[35] = abb[23] + -z[4];
z[35] = abb[14] * z[35];
z[34] = z[34] + 2 * z[35];
z[34] = abb[3] * z[34];
z[7] = -abb[14] * z[7];
z[24] = -z[24] * z[53];
z[7] = z[7] + z[24];
z[7] = abb[9] * z[7];
z[0] = abb[30] + z[0] + z[6] + z[7] + z[9] + z[12] + z[14] + z[18] + z[20] + z[27] + z[29] + z[32] + z[34] + z[37];
z[6] = abb[17] * z[16];
z[3] = -abb[18] * z[3];
z[7] = 11 * abb[27];
z[9] = 3 * abb[25];
z[10] = z[7] + z[9] + -2 * z[10] + -z[25];
z[10] = abb[8] * z[10];
z[12] = abb[5] * z[15];
z[12] = -z[12] + -z[19] + 5 * z[21];
z[8] = -z[1] + z[8] + z[64];
z[8] = abb[3] * z[8];
z[2] = 3 * abb[20] + -13 * abb[21] + abb[22] + 10 * abb[24] + 15 * abb[26] + -z[2] + -z[7];
z[2] = abb[1] * z[2];
z[2] = z[2] + z[8] + z[10] + 2 * z[12] + -z[31] + -10 * z[36];
z[2] = abb[12] * z[2];
z[7] = -z[12] + 5 * z[36] + z[42] + -2 * z[43] + z[66];
z[8] = z[9] + z[17] + -z[28];
z[8] = abb[6] * z[8];
z[7] = 2 * z[7] + z[8] + -z[30];
z[7] = abb[10] * z[7];
z[8] = -z[9] + -z[23] + z[33];
z[8] = abb[8] * z[8];
z[9] = -z[4] + z[68];
z[9] = abb[0] * z[9];
z[10] = -abb[20] + z[22];
z[10] = abb[3] * z[10];
z[5] = -z[5] + z[8] + z[9] + z[10] + z[26];
z[5] = abb[11] * z[5];
z[1] = -z[1] + z[11] + -z[13];
z[1] = abb[23] + 2 * z[1] + z[4];
z[1] = abb[12] * z[1];
z[4] = -z[15] + -z[45] + z[55];
z[4] = -abb[23] + -9 * abb[25] + 2 * z[4];
z[4] = abb[10] * z[4];
z[1] = z[1] + z[4];
z[1] = abb[0] * z[1];
z[1] = z[1] + z[2] + z[5] + z[7];
z[1] = m1_set::bc<T>[0] * z[1];
z[1] = abb[19] + z[1] + z[3] + z[6];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_509_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("19.502977447539869648397035021144659931590030895653123244316411732"),stof<T>("-54.388676555957592099065279401633141508571537521163423085343548723")}, std::complex<T>{stof<T>("-43.835608187914751945276846681308838668913523615463710323609501161"),stof<T>("25.167855523162380314045046811001116277723613549806274088920001368")}, std::complex<T>{stof<T>("4.1843893662613707477229743317377905246010223899295916125993394271"),stof<T>("1.0028794179071829898056514172515855326762007416075746582541814245")}, std::complex<T>{stof<T>("-21.863405156584002299911951591286307906613266362034977091342843597"),stof<T>("-7.499667913046966701243001280098614104314153057709516791170594485")}, std::complex<T>{stof<T>("31.241092972186233841307521873932631183157550283706767709141365516"),stof<T>("14.174064265852651295034290995152374577737902959647904062069825236")}, std::complex<T>{stof<T>("1.542996606368332516260440925799311677340510508550648419669493112"),stof<T>("62.133715638443340400228874507918565167814535481373634920907806636")}, std::complex<T>{stof<T>("54.256672939088721394135229031269416644410503599873997122370895385"),stof<T>("-33.822661965391063919514713747364995372152302977579311492713898086")}, std::complex<T>{stof<T>("-30.423661869510433706053046229590294885474825325875562281784426764"),stof<T>("-14.419435435291432894954884821339184132666747862148599106463488665")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[22].real()/kbase.W[22].real()), rlog(k.W[31].real()/kbase.W[31].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_509_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_509_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(32)) * (v[1] + v[2] + -v[3] + -v[4]) * (96 + 64 * v[0] + -10 * v[1] + 38 * v[2] + 18 * v[3] + -38 * v[4] + -m1_set::bc<T>[2] * (24 + 20 * v[0] + v[1] + 13 * v[2] + 3 * v[3] + -13 * v[4] + -12 * v[5]) + -24 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + -48 * v[5])) / prod_pow(tend, 2);
c[1] = ((4 * m1_set::bc<T>[1] + m1_set::bc<T>[2]) * (T(-3) / T(4)) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (abb[22] + abb[23] + -abb[27]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -prod_pow(abb[11], 2);
z[1] = prod_pow(abb[13], 2);
z[0] = z[0] + z[1];
z[0] = abb[14] + -abb[16] + 3 * z[0];
z[1] = abb[22] + abb[23] + -abb[27];
return abb[7] * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_509_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_509_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-36.826671473721138885765488167185927787020076953351411344740643034"),stof<T>("17.500575494865785817102539449045369004681231887502396316588174339")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,31> abb = {dl[0], dlog_W3(k,dl), dlog_W4(k,dl), dlog_W8(k,dl), dl[3], dlog_W13(k,dl), dlog_W14(k,dl), dlog_W20(k,dl), dlog_W23(k,dl), dlog_W32(k,dl), f_1_1(k), f_1_7(k), f_1_10(k), f_1_11(k), f_2_1(k), f_2_14(k), f_2_20(k), f_2_2_im(k), f_2_10_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[13].real()/k.W[13].real()), rlog(kend.W[22].real()/k.W[22].real()), rlog(kend.W[31].real()/k.W[31].real()), f_2_2_re(k), f_2_10_re(k), T{0}};
abb[19] = SpDLog_f_4_509_W_20_Im(t, path, abb);
abb[30] = SpDLog_f_4_509_W_20_Re(t, path, abb);

                    
            return f_4_509_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_509_DLogXconstant_part(base_point<T>, kend);
	value += f_4_509_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_509_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_509_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_509_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_509_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_509_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_509_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
