/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_34.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_34_abbreviated (const std::array<T,18>& abb) {
T z[32];
z[0] = 4 * abb[3];
z[1] = 4 * abb[1] + z[0];
z[2] = -abb[0] + z[1];
z[3] = 3 * abb[2];
z[4] = z[2] + -z[3];
z[5] = -abb[7] + abb[8];
z[4] = -z[4] * z[5];
z[6] = 3 * abb[5];
z[2] = -z[2] + z[6];
z[2] = abb[6] * z[2];
z[2] = z[2] + 2 * z[4];
z[4] = 2 * abb[6];
z[2] = z[2] * z[4];
z[4] = 2 * abb[1];
z[7] = 2 * abb[3];
z[8] = z[4] + z[7];
z[9] = abb[0] + z[8];
z[10] = 6 * abb[2];
z[11] = -9 * abb[4] + z[9] + z[10];
z[11] = abb[8] * z[11];
z[12] = 3 * abb[4];
z[13] = -z[9] + z[12];
z[14] = 2 * abb[7];
z[13] = z[13] * z[14];
z[11] = z[11] + z[13];
z[11] = abb[8] * z[11];
z[10] = 5 * abb[0] + -z[8] + z[10] + -z[12];
z[13] = prod_pow(abb[7], 2);
z[10] = z[10] * z[13];
z[8] = -abb[0] + z[8];
z[14] = 2 * abb[2];
z[15] = z[8] + -z[14];
z[16] = abb[5] + z[15];
z[17] = abb[17] * z[16];
z[18] = abb[5] * z[13];
z[17] = -z[17] + z[18];
z[18] = 19 * abb[0];
z[1] = -z[1] + z[18];
z[19] = 5 * abb[5];
z[1] = (T(1) / T(3)) * z[1] + -z[19];
z[20] = prod_pow(m1_set::bc<T>[0], 2);
z[1] = z[1] * z[20];
z[15] = abb[4] + z[15];
z[21] = 6 * abb[9];
z[15] = z[15] * z[21];
z[1] = z[1] + -z[2] + -z[10] + z[11] + -z[15] + 6 * z[17];
z[2] = abb[12] + abb[14];
z[1] = -z[1] * z[2];
z[10] = -abb[11] + abb[16];
z[11] = 2 * abb[13];
z[15] = -z[10] + z[11];
z[17] = abb[5] * z[15];
z[21] = abb[0] * abb[16];
z[17] = z[17] + -z[21];
z[22] = -abb[0] + z[4];
z[23] = abb[3] + z[22];
z[23] = z[11] * z[23];
z[24] = abb[11] + abb[13];
z[14] = z[14] * z[24];
z[25] = abb[0] + z[7];
z[25] = abb[11] * z[25];
z[23] = z[14] + -z[17] + -z[23] + -z[25];
z[25] = abb[17] * z[23];
z[26] = abb[4] * abb[13];
z[27] = -abb[0] + z[7];
z[28] = -abb[1] + -z[27];
z[28] = abb[13] * z[28];
z[28] = -z[26] + z[28];
z[29] = abb[0] + abb[4];
z[30] = abb[15] * z[29];
z[4] = abb[0] + z[4];
z[31] = -abb[4] + -z[4];
z[31] = abb[11] * z[31];
z[14] = z[14] + 2 * z[28] + z[30] + z[31];
z[14] = abb[9] * z[14];
z[14] = z[14] + z[25];
z[25] = -abb[0] + abb[1];
z[25] = abb[13] * z[25];
z[21] = z[21] + -z[25] + -z[26];
z[6] = z[6] * z[15];
z[3] = z[3] * z[24];
z[15] = -abb[1] + z[7];
z[25] = abb[4] * (T(3) / T(2));
z[28] = abb[0] * (T(5) / T(2)) + z[15] + z[25];
z[28] = abb[11] * z[28];
z[21] = -z[3] + z[6] + -3 * z[21] + z[28] + (T(-3) / T(2)) * z[30];
z[13] = z[13] * z[21];
z[10] = -abb[13] + (T(1) / T(2)) * z[10];
z[10] = z[10] * z[19];
z[19] = abb[3] * abb[13];
z[18] = -5 * abb[1] + abb[3] + -z[18];
z[18] = abb[11] * z[18];
z[21] = 3 * abb[0] + abb[1] * (T(-1) / T(2));
z[21] = abb[16] * z[21];
z[28] = abb[0] + -abb[3];
z[28] = abb[15] * z[28];
z[10] = z[10] + (T(1) / T(6)) * z[18] + 5 * z[19] + z[21] + (T(13) / T(2)) * z[28];
z[10] = z[10] * z[20];
z[8] = abb[13] * z[8];
z[9] = abb[11] * z[9];
z[8] = -z[3] + 3 * z[8] + z[9];
z[5] = z[5] * z[8];
z[8] = abb[16] * z[22];
z[6] = -z[6] + 3 * z[8] + z[9] + 6 * z[19];
z[6] = abb[6] * z[6];
z[5] = 2 * z[5] + z[6];
z[5] = abb[6] * z[5];
z[6] = abb[0] * (T(1) / T(2));
z[7] = -z[6] + z[7] + z[25];
z[7] = abb[15] * z[7];
z[8] = abb[1] + -z[27];
z[8] = abb[13] * z[8];
z[7] = z[7] + z[8] + -3 * z[26];
z[6] = abb[4] * (T(-9) / T(2)) + -z[6] + z[15];
z[6] = abb[11] * z[6];
z[3] = z[3] + z[6] + 3 * z[7];
z[3] = abb[8] * z[3];
z[0] = -z[0] + z[4] + z[12];
z[0] = abb[11] * z[0];
z[4] = -abb[1] * abb[13];
z[4] = z[4] + z[26];
z[0] = z[0] + 6 * z[4] + -3 * z[30];
z[0] = abb[7] * z[0];
z[0] = z[0] + z[3];
z[0] = abb[8] * z[0];
z[0] = z[0] + z[1] + z[5] + z[10] + z[13] + 3 * z[14];
z[1] = abb[4] * z[11];
z[3] = abb[2] * z[24];
z[1] = z[1] + -z[3];
z[3] = abb[0] * abb[13];
z[4] = -abb[11] * z[29];
z[3] = -z[1] + z[3] + z[4] + -z[17] + z[30];
z[3] = abb[7] * z[3];
z[4] = abb[13] * z[27];
z[5] = abb[4] * abb[11];
z[6] = -abb[4] + -z[27];
z[6] = abb[15] * z[6];
z[1] = z[1] + z[4] + z[5] + z[6];
z[1] = abb[8] * z[1];
z[1] = z[1] + z[3];
z[3] = 2 * m1_set::bc<T>[0];
z[1] = z[1] * z[3];
z[4] = abb[10] * z[23];
z[5] = abb[2] + -abb[4];
z[6] = -abb[0] + abb[5] + -z[5];
z[6] = abb[7] * z[6];
z[5] = abb[8] * z[5];
z[5] = z[5] + z[6];
z[3] = z[3] * z[5];
z[5] = abb[10] * z[16];
z[3] = z[3] + z[5];
z[2] = 2 * z[2];
z[2] = z[2] * z[3];
z[1] = z[1] + z[2] + z[4];
z[1] = 3 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_34_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-40.459848581432922931207467197990072863179711127135143606210589882"),stof<T>("19.026511521568795221439855984419417850156195720279767640177081489")}, std::complex<T>{stof<T>("-3.741094705703901590241760531245069874103332442830769601568477015"),stof<T>("31.321128477238471624229478296944626833087072053497164900832935852")}, std::complex<T>{stof<T>("4.85631944915178451110536009194755456076677285404451643788016406"),stof<T>("29.898458754576256769327617980196683885572487362677554259654482213")}, std::complex<T>{stof<T>("-3.741094705703901590241760531245069874103332442830769601568477015"),stof<T>("31.321128477238471624229478296944626833087072053497164900832935852")}, std::complex<T>{stof<T>("-30.473113012070827882800081465915326107386137337009131134195444576"),stof<T>("-68.295157456188042130887596872381687025745786493770503960366211684")}, std::complex<T>{stof<T>("69.817736850055867893143949103202914283902408052930527904094347413"),stof<T>("-11.95094129719548148410935538917904154306996864268398284029828787")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[26].real()/kbase.W[26].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_34_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_34_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("31.574662907882824633657626405938430242445622376205880794302328693"),stof<T>("-7.260179296115532470966335101995790845941237348694580413963022227")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,18> abb = {dl[1], dl[3], dlog_W10(k,dl), dl[4], dlog_W21(k,dl), dlog_W27(k,dl), f_1_2(k), f_1_3(k), f_1_6(k), f_2_5(k), f_2_6_im(k), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[26].real()/k.W[26].real()), f_2_6_re(k)};

                    
            return f_4_34_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_34_DLogXconstant_part(base_point<T>, kend);
	value += f_4_34_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_34_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_34_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_34_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_34_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_34_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_34_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
