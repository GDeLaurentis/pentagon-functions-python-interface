/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_219.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_219_abbreviated (const std::array<T,53>& abb) {
T z[80];
z[0] = 3 * abb[51];
z[1] = prod_pow(abb[31], 2);
z[2] = (T(1) / T(2)) * z[1];
z[3] = abb[32] + z[2];
z[4] = -z[0] + -z[3];
z[5] = abb[29] * (T(1) / T(2));
z[6] = abb[28] * (T(1) / T(2));
z[7] = z[5] + -z[6];
z[8] = 2 * abb[31];
z[9] = -abb[26] + -z[7] + z[8];
z[9] = abb[26] * z[9];
z[10] = abb[29] + -abb[31];
z[11] = -abb[28] + z[10];
z[12] = abb[27] + z[11];
z[13] = abb[26] * (T(3) / T(4));
z[14] = 4 * z[12] + z[13];
z[14] = abb[27] * z[14];
z[15] = abb[26] * (T(3) / T(2)) + -z[11];
z[16] = abb[27] * (T(13) / T(2));
z[17] = -z[15] + z[16];
z[17] = abb[30] + (T(1) / T(2)) * z[17];
z[17] = abb[30] * z[17];
z[18] = abb[34] * (T(3) / T(2));
z[4] = (T(3) / T(2)) * z[4] + z[9] + z[14] + z[17] + -z[18];
z[4] = abb[44] * z[4];
z[9] = abb[26] * (T(1) / T(2));
z[14] = abb[31] + z[9];
z[7] = -z[7] + -z[14];
z[7] = abb[26] * z[7];
z[17] = abb[30] * (T(1) / T(2));
z[19] = abb[27] * (T(-3) / T(2)) + abb[26] * (T(5) / T(2)) + z[11];
z[19] = z[17] * z[19];
z[20] = abb[51] + z[3];
z[21] = abb[26] * (T(-5) / T(4)) + -2 * z[11];
z[21] = abb[27] * z[21];
z[7] = z[7] + z[18] + z[19] + (T(3) / T(2)) * z[20] + z[21];
z[7] = abb[48] * z[7];
z[19] = z[11] + z[17];
z[20] = 2 * abb[27];
z[21] = z[19] + z[20];
z[21] = abb[30] * z[21];
z[22] = z[12] * z[20];
z[23] = z[9] + z[11];
z[24] = abb[26] * z[23];
z[21] = -z[0] + z[21] + z[22] + -z[24];
z[22] = abb[43] + abb[45];
z[21] = z[21] * z[22];
z[25] = abb[44] * (T(1) / T(2));
z[26] = abb[48] * (T(1) / T(2));
z[27] = abb[42] * (T(3) / T(2));
z[28] = z[22] + z[25] + z[26] + -z[27];
z[29] = abb[36] * z[28];
z[30] = abb[27] * (T(1) / T(2));
z[31] = z[23] + z[30];
z[31] = abb[30] * z[31];
z[32] = -abb[28] + abb[29];
z[33] = abb[26] * z[32];
z[34] = abb[34] + z[3];
z[35] = abb[27] * z[9];
z[31] = -abb[51] + z[31] + -z[33] + z[34] + -z[35];
z[33] = -z[27] * z[31];
z[36] = 3 * abb[31];
z[37] = -z[9] + z[36];
z[37] = abb[26] * z[37];
z[38] = -abb[26] + abb[27];
z[39] = z[36] + z[38];
z[40] = z[20] * z[39];
z[41] = -abb[26] + z[20];
z[42] = abb[30] + 2 * z[41];
z[42] = abb[30] * z[42];
z[43] = 2 * abb[51] + z[3];
z[37] = z[37] + -z[40] + z[42] + -3 * z[43];
z[40] = abb[41] * z[37];
z[42] = z[8] + -z[9];
z[42] = abb[27] * z[42];
z[44] = 3 * abb[27] + abb[30];
z[45] = abb[26] + -z[44];
z[45] = z[17] * z[45];
z[46] = -abb[31] + z[9];
z[47] = abb[26] * z[46];
z[42] = z[42] + z[43] + z[45] + z[47];
z[43] = 3 * abb[47];
z[42] = z[42] * z[43];
z[45] = -z[9] + z[20];
z[45] = abb[27] * z[45];
z[48] = abb[26] + abb[27];
z[49] = abb[30] + z[48];
z[49] = z[17] * z[49];
z[50] = prod_pow(abb[26], 2);
z[45] = z[45] + z[49] + -z[50];
z[51] = -abb[40] * z[45];
z[4] = z[4] + z[7] + z[21] + -z[29] + z[33] + z[40] + z[42] + z[51];
z[4] = abb[6] * z[4];
z[7] = abb[46] * (T(13) / T(2));
z[21] = 3 * abb[42];
z[33] = 4 * z[22];
z[40] = abb[48] * (T(-13) / T(2)) + abb[44] * (T(5) / T(2)) + -z[33];
z[40] = z[7] + -z[21] + (T(1) / T(3)) * z[40];
z[40] = abb[8] * z[40];
z[42] = -abb[41] + abb[47];
z[51] = abb[42] + -abb[44];
z[52] = -z[22] + z[42] + z[51];
z[53] = -abb[11] * z[52];
z[54] = abb[42] * (T(1) / T(2));
z[33] = abb[44] + -z[33];
z[33] = abb[40] * (T(-7) / T(6)) + abb[47] * (T(1) / T(2)) + abb[41] * (T(5) / T(6)) + -z[26] + (T(1) / T(3)) * z[33] + z[54];
z[33] = abb[5] * z[33];
z[55] = abb[48] * (T(19) / T(2)) + 10 * z[22] + z[25];
z[7] = -z[7] + (T(-7) / T(2)) * z[42] + (T(1) / T(3)) * z[55];
z[7] = abb[2] * z[7];
z[55] = abb[42] + -abb[48];
z[56] = abb[13] + -abb[15];
z[56] = -z[55] * z[56];
z[57] = abb[13] + abb[15];
z[57] = abb[44] * z[57];
z[56] = z[56] + z[57];
z[57] = abb[44] + abb[48];
z[58] = z[22] + -z[57];
z[59] = -abb[40] + (T(-1) / T(3)) * z[58];
z[59] = abb[1] * z[59];
z[60] = abb[40] + -abb[48];
z[61] = abb[41] + z[60];
z[61] = abb[0] * z[61];
z[42] = abb[15] * z[42];
z[62] = abb[13] * abb[40];
z[63] = -abb[44] + z[22];
z[64] = -abb[41] + z[63];
z[64] = abb[42] + abb[48] + abb[40] * (T(-5) / T(3)) + (T(-1) / T(3)) * z[64];
z[64] = -abb[47] + (T(1) / T(2)) * z[64];
z[64] = abb[6] * z[64];
z[7] = z[7] + z[33] + z[40] + -z[42] + -2 * z[53] + (T(1) / T(2)) * z[56] + (T(7) / T(2)) * z[59] + (T(13) / T(6)) * z[61] + -z[62] + z[64];
z[33] = prod_pow(m1_set::bc<T>[0], 2);
z[7] = z[7] * z[33];
z[40] = 2 * abb[29];
z[8] = -abb[28] + z[8] + -z[40];
z[56] = abb[27] + abb[30];
z[59] = z[8] + -z[56];
z[59] = abb[30] * z[59];
z[64] = prod_pow(abb[28], 2);
z[65] = (T(1) / T(2)) * z[64];
z[66] = abb[35] + abb[51];
z[67] = z[65] + -z[66];
z[12] = abb[27] * z[12];
z[12] = z[12] + z[24] + -z[59] + 3 * z[67];
z[12] = z[12] * z[22];
z[12] = z[12] + z[29];
z[29] = abb[33] + abb[35];
z[59] = z[29] + z[65];
z[67] = abb[51] + z[59];
z[67] = (T(3) / T(2)) * z[67];
z[68] = abb[31] * (T(1) / T(2));
z[5] = -z[5] + z[68];
z[69] = -abb[28] + z[9];
z[70] = -z[5] + -z[69];
z[70] = abb[26] * z[70];
z[15] = (T(-1) / T(2)) * z[15] + z[20];
z[15] = abb[27] * z[15];
z[13] = abb[27] * (T(5) / T(4)) + z[13] + z[19];
z[13] = abb[30] * z[13];
z[13] = z[13] + z[15] + -z[67] + z[70];
z[13] = abb[44] * z[13];
z[15] = abb[26] * (T(1) / T(4));
z[19] = abb[27] * (T(3) / T(4));
z[70] = z[11] + z[15] + z[19];
z[70] = abb[30] * z[70];
z[5] = abb[28] + -z[5] + z[9];
z[5] = abb[26] * z[5];
z[71] = -z[9] + z[11];
z[71] = z[30] * z[71];
z[5] = z[5] + -z[67] + z[70] + z[71];
z[5] = abb[48] * z[5];
z[67] = 3 * abb[28];
z[48] = -z[17] + z[48] + -z[67];
z[48] = abb[30] * z[48];
z[70] = -z[9] + z[67];
z[70] = abb[26] * z[70];
z[71] = abb[33] + z[64];
z[72] = abb[27] * z[38];
z[48] = z[48] + z[70] + -3 * z[71] + z[72];
z[70] = -abb[40] * z[48];
z[23] = abb[27] * z[23];
z[23] = z[23] + z[65];
z[65] = abb[26] * z[10];
z[71] = abb[33] + -z[23] + -z[65] + 3 * z[66];
z[15] = abb[27] * (T(-1) / T(4)) + -z[10] + z[15];
z[15] = abb[30] * z[15];
z[15] = z[15] + (T(1) / T(2)) * z[71];
z[15] = z[15] * z[21];
z[45] = abb[41] * z[45];
z[71] = -abb[27] + z[9];
z[71] = abb[27] * z[71];
z[71] = -z[49] + z[71];
z[71] = z[43] * z[71];
z[5] = z[5] + z[12] + z[13] + z[15] + z[45] + z[70] + z[71];
z[5] = abb[5] * z[5];
z[13] = 2 * abb[28];
z[15] = -z[13] + z[20] + z[40] + -z[68];
z[15] = abb[27] * z[15];
z[45] = z[17] + z[30];
z[6] = z[6] + z[10] + z[45];
z[6] = abb[30] * z[6];
z[70] = prod_pow(abb[29], 2);
z[71] = (T(1) / T(2)) * z[70];
z[72] = z[66] + z[71];
z[72] = 3 * z[72];
z[73] = 3 * abb[34];
z[74] = z[24] + -z[72] + -z[73];
z[15] = z[6] + z[15] + (T(1) / T(2)) * z[74];
z[15] = abb[44] * z[15];
z[74] = z[64] + -z[66] + z[71];
z[73] = z[24] + z[73] + 3 * z[74];
z[68] = -abb[27] + -z[32] + -z[68];
z[68] = abb[27] * z[68];
z[68] = z[6] + z[68] + (T(1) / T(2)) * z[73];
z[68] = abb[48] * z[68];
z[24] = -abb[34] + z[24];
z[73] = abb[27] * abb[31];
z[72] = -z[24] + z[72] + z[73];
z[6] = -z[6] + (T(1) / T(2)) * z[72];
z[6] = z[6] * z[21];
z[6] = z[6] + z[12] + z[15] + z[68];
z[6] = abb[8] * z[6];
z[8] = z[8] + -z[9];
z[8] = abb[26] * z[8];
z[12] = -abb[27] + abb[30];
z[15] = abb[26] + z[11];
z[68] = -z[12] * z[15];
z[8] = z[8] + 3 * z[59] + z[68];
z[8] = -z[8] * z[22];
z[72] = abb[28] + abb[31];
z[74] = (T(1) / T(2)) * z[72];
z[75] = -z[9] + -z[40] + z[74];
z[75] = abb[26] * z[75];
z[76] = -z[17] + z[30];
z[77] = z[15] * z[76];
z[78] = z[3] + z[59];
z[75] = z[18] + z[75] + -z[77] + (T(3) / T(2)) * z[78];
z[75] = abb[44] * z[75];
z[3] = -z[3] + -3 * z[29] + (T(-3) / T(2)) * z[64];
z[78] = abb[28] + -5 * abb[31];
z[78] = abb[26] + 4 * abb[29] + (T(1) / T(2)) * z[78];
z[78] = abb[26] * z[78];
z[3] = (T(3) / T(2)) * z[3] + -z[18] + -z[77] + z[78];
z[3] = abb[48] * z[3];
z[18] = -abb[28] + abb[31];
z[18] = abb[26] * z[18];
z[18] = z[18] + -z[34] + z[59] + z[68];
z[18] = z[18] * z[27];
z[59] = 2 * abb[48] + z[63];
z[63] = 2 * abb[36];
z[68] = z[59] * z[63];
z[3] = z[3] + z[8] + z[18] + z[68] + z[75];
z[3] = abb[14] * z[3];
z[8] = -z[29] + z[65];
z[18] = z[17] * z[38];
z[18] = -abb[51] + z[18];
z[23] = z[8] + -z[18] + -z[23];
z[23] = abb[23] * z[23];
z[31] = -abb[21] * z[31];
z[38] = -abb[27] + abb[28];
z[38] = abb[30] * z[38];
z[24] = -abb[35] + abb[36] + abb[51] + z[24] + z[38] + -z[71] + z[73];
z[24] = abb[22] * z[24];
z[38] = abb[27] * z[46];
z[18] = z[18] + z[38];
z[38] = abb[34] + z[71];
z[65] = z[8] + -z[18] + -z[38];
z[65] = abb[20] * z[65];
z[47] = abb[32] + z[47];
z[18] = z[18] + -z[47];
z[68] = abb[24] * z[18];
z[75] = abb[21] + abb[23];
z[77] = abb[20] + z[75];
z[77] = abb[36] * z[77];
z[78] = abb[22] + z[75];
z[79] = abb[50] * z[78];
z[23] = z[23] + z[24] + z[31] + z[65] + z[68] + z[77] + z[79];
z[24] = abb[20] + -abb[24];
z[31] = z[24] + z[75];
z[31] = abb[22] + (T(1) / T(2)) * z[31];
z[31] = z[31] * z[33];
z[65] = abb[25] * abb[52];
z[23] = (T(3) / T(2)) * z[23] + z[31] + (T(-3) / T(4)) * z[65];
z[23] = abb[49] * z[23];
z[30] = z[30] + z[69];
z[30] = abb[30] * z[30];
z[31] = abb[26] * z[69];
z[30] = -abb[33] + z[30] + -z[31] + -z[35];
z[31] = abb[13] * z[30];
z[18] = abb[15] * z[18];
z[35] = -z[18] + -z[31];
z[35] = abb[44] * z[35];
z[31] = -z[18] + z[31];
z[65] = -abb[48] * z[31];
z[68] = -z[64] + z[70];
z[69] = abb[27] * z[32];
z[68] = abb[34] + (T(1) / T(2)) * z[68] + -z[69];
z[69] = abb[7] * z[68];
z[31] = z[31] + z[69];
z[31] = abb[42] * z[31];
z[69] = -abb[44] + abb[48];
z[77] = abb[7] * z[69];
z[68] = -z[68] * z[77];
z[31] = z[31] + z[35] + z[65] + z[68];
z[30] = z[30] * z[62];
z[35] = -z[2] + z[71];
z[8] = abb[36] + z[8] + z[35];
z[65] = abb[46] + -z[22] + z[55];
z[65] = abb[10] * z[65];
z[8] = z[8] * z[65];
z[68] = prod_pow(abb[27], 2);
z[33] = z[33] + -z[64] + z[68];
z[68] = abb[40] + -abb[44];
z[68] = abb[12] * z[68];
z[33] = z[33] * z[68];
z[71] = z[50] + -z[64];
z[60] = abb[46] + z[60];
z[60] = abb[9] * z[60];
z[71] = z[60] * z[71];
z[8] = z[8] + z[30] + (T(1) / T(2)) * z[31] + z[33] + z[71];
z[30] = abb[7] + -abb[13];
z[30] = abb[42] * z[30];
z[31] = abb[13] * z[57];
z[30] = z[30] + z[31] + -z[77];
z[26] = abb[44] * (T(3) / T(2)) + z[22] + -z[26] + -z[54];
z[31] = abb[6] * z[26];
z[33] = -3 * abb[40] + -z[58];
z[33] = abb[1] * z[33];
z[54] = abb[44] + z[55];
z[57] = abb[8] * (T(1) / T(2));
z[54] = z[54] * z[57];
z[55] = -abb[44] + z[55];
z[55] = abb[40] + (T(1) / T(2)) * z[55];
z[55] = abb[5] * z[55];
z[30] = (T(1) / T(2)) * z[30] + -z[31] + -z[33] + -z[54] + z[55] + -z[62];
z[30] = 3 * z[30];
z[31] = abb[50] * z[30];
z[41] = z[17] + z[41];
z[41] = abb[30] * z[41];
z[39] = abb[27] * z[39];
z[39] = -z[39] + z[41];
z[0] = z[0] + z[1] + -z[39] + z[47];
z[1] = 3 * abb[41] + -z[43];
z[0] = -z[0] * z[1];
z[37] = abb[44] * z[37];
z[41] = abb[51] + z[2];
z[43] = z[39] + -3 * z[41] + -z[50];
z[43] = z[22] * z[43];
z[47] = -z[9] + -z[36];
z[47] = abb[26] * z[47];
z[54] = abb[32] + abb[51];
z[39] = -z[39] + z[47] + 3 * z[54];
z[39] = abb[48] * z[39];
z[14] = abb[26] * z[14];
z[2] = -abb[32] + z[2] + z[14];
z[14] = 3 * abb[46];
z[2] = z[2] * z[14];
z[0] = z[0] + z[2] + z[37] + z[39] + z[43];
z[0] = abb[2] * z[0];
z[2] = -z[40] + -z[46];
z[2] = abb[26] * z[2];
z[2] = z[2] + 2 * z[29] + z[34] + -z[63] + z[64];
z[2] = abb[14] * z[2];
z[34] = -abb[29] + z[9];
z[34] = abb[26] * z[34];
z[29] = abb[32] + z[29] + z[34] + z[38];
z[34] = abb[36] + -z[29];
z[34] = abb[3] * z[34];
z[37] = -abb[34] + -z[64] + -z[70];
z[37] = abb[8] * z[37];
z[2] = z[2] + z[34] + z[37];
z[2] = z[2] * z[14];
z[14] = z[33] * z[48];
z[34] = 3 * abb[11];
z[34] = z[34] * z[52];
z[37] = abb[27] + z[17];
z[37] = abb[30] * z[37];
z[37] = z[37] + -z[41] + -z[73];
z[37] = z[34] * z[37];
z[38] = abb[16] + abb[18];
z[39] = abb[17] + abb[19];
z[41] = z[38] + -z[39];
z[41] = abb[48] * z[41];
z[43] = abb[16] + z[39];
z[46] = abb[18] + z[43];
z[46] = abb[44] * z[46];
z[47] = -abb[18] + z[43];
z[47] = abb[42] * z[47];
z[41] = z[41] + z[46] + z[47];
z[39] = abb[41] * z[39];
z[43] = abb[47] * z[43];
z[38] = abb[40] * z[38];
z[38] = -z[38] + z[39] + (T(1) / T(2)) * z[41] + -z[43];
z[38] = (T(3) / T(2)) * z[38];
z[39] = abb[52] * z[38];
z[41] = abb[30] * z[10];
z[35] = -z[35] + z[41] + -z[66];
z[41] = 3 * abb[4];
z[35] = -z[28] * z[35] * z[41];
z[9] = abb[27] + z[9];
z[9] = abb[27] * z[9];
z[9] = z[9] + -z[49] + -2 * z[50];
z[9] = z[9] * z[61];
z[43] = abb[42] + z[69];
z[29] = z[29] * z[43];
z[43] = -abb[48] + -z[51];
z[43] = abb[36] * z[43];
z[29] = z[29] + z[43];
z[29] = abb[3] * z[29];
z[18] = -z[1] * z[18];
z[0] = z[0] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + 3 * z[8] + z[9] + z[14] + z[18] + z[23] + (T(3) / T(2)) * z[29] + z[31] + z[35] + z[37] + z[39];
z[2] = 2 * abb[26];
z[3] = abb[30] * (T(3) / T(4)) + -z[2] + (T(-1) / T(2)) * z[11] + z[19];
z[3] = abb[48] * z[3];
z[4] = abb[30] + z[20];
z[5] = z[4] + z[11];
z[5] = z[5] * z[22];
z[6] = abb[30] * (T(5) / T(2)) + z[11] + z[16];
z[6] = z[6] * z[25];
z[7] = z[11] + z[45];
z[8] = -z[7] * z[27];
z[9] = z[2] + z[76];
z[14] = -abb[40] * z[9];
z[16] = -abb[26] + z[4];
z[18] = 2 * abb[41];
z[19] = z[16] * z[18];
z[20] = abb[47] * (T(3) / T(2));
z[23] = -z[20] * z[44];
z[5] = -z[3] + z[5] + z[6] + z[8] + z[14] + z[19] + z[23];
z[5] = abb[6] * z[5];
z[6] = z[10] + z[12] + z[13];
z[6] = z[6] * z[22];
z[8] = abb[27] * (T(5) / T(2)) + -z[11] + z[17];
z[8] = z[8] * z[25];
z[11] = -abb[28] + z[45];
z[13] = z[10] + -z[11];
z[13] = z[13] * z[27];
z[14] = abb[27] + 2 * abb[30] + -z[2] + z[67];
z[17] = -abb[40] * z[14];
z[9] = abb[41] * z[9];
z[12] = z[12] * z[20];
z[3] = z[3] + -z[6] + z[8] + z[9] + z[12] + z[13] + z[17];
z[3] = abb[5] * z[3];
z[2] = z[2] + z[4] + z[36];
z[2] = z[2] * z[22];
z[8] = 4 * abb[26];
z[9] = -z[4] + z[8] + z[36];
z[9] = abb[48] * z[9];
z[4] = abb[31] + z[4];
z[4] = z[1] * z[4];
z[12] = 2 * abb[44];
z[13] = z[12] * z[16];
z[16] = 6 * abb[46];
z[17] = -abb[26] + -abb[31];
z[17] = z[16] * z[17];
z[2] = z[2] + z[4] + z[9] + z[13] + z[17];
z[2] = abb[2] * z[2];
z[4] = abb[13] * z[11];
z[9] = -abb[31] + z[45];
z[13] = abb[15] * z[9];
z[17] = -z[4] + -z[13];
z[17] = abb[44] * z[17];
z[4] = z[4] + -z[13];
z[19] = -abb[48] * z[4];
z[20] = -abb[7] * z[32];
z[4] = z[4] + z[20];
z[4] = abb[42] * z[4];
z[20] = z[32] * z[77];
z[4] = z[4] + z[17] + z[19] + z[20];
z[11] = z[11] * z[62];
z[4] = (T(1) / T(2)) * z[4] + z[11];
z[11] = -5 * abb[28] + abb[31];
z[11] = (T(1) / T(2)) * z[11] + -z[40] + z[76];
z[11] = abb[48] * z[11];
z[17] = abb[29] + z[74] + z[76];
z[19] = abb[44] + -z[21];
z[17] = z[17] * z[19];
z[6] = -z[6] + z[11] + z[17];
z[6] = abb[8] * z[6];
z[11] = abb[26] + -abb[28];
z[11] = -z[11] * z[60];
z[17] = -z[10] * z[65];
z[19] = abb[28] * z[68];
z[11] = z[11] + z[17] + z[19];
z[14] = z[14] * z[33];
z[17] = abb[27] + abb[31];
z[17] = z[17] * z[34];
z[13] = -z[1] * z[13];
z[15] = abb[14] * z[15];
z[19] = -z[15] * z[59];
z[8] = z[8] + -z[76];
z[8] = z[8] * z[61];
z[20] = abb[28] + abb[29];
z[20] = abb[8] * z[20];
z[15] = z[15] + z[20];
z[15] = z[15] * z[16];
z[2] = z[2] + z[3] + 3 * z[4] + z[5] + z[6] + z[8] + 6 * z[11] + z[13] + z[14] + z[15] + z[17] + 2 * z[19];
z[2] = m1_set::bc<T>[0] * z[2];
z[3] = abb[37] * z[30];
z[4] = -abb[5] + -abb[8];
z[4] = z[4] * z[28];
z[5] = 2 * abb[47] + -z[18] + -z[26];
z[5] = abb[6] * z[5];
z[1] = abb[48] + -z[1] + -z[12] + -z[22];
z[1] = abb[2] * z[1];
z[6] = abb[42] + -z[69];
z[6] = abb[15] * z[6];
z[1] = z[1] + z[4] + z[5] + (T(1) / T(2)) * z[6] + -z[42] + z[53];
z[1] = abb[38] * z[1];
z[4] = abb[39] * z[38];
z[5] = -m1_set::bc<T>[0] * z[10];
z[5] = abb[38] + z[5];
z[5] = z[5] * z[28] * z[41];
z[6] = -z[9] * z[24];
z[7] = -z[7] * z[75];
z[8] = -z[56] + z[72];
z[8] = abb[22] * z[8];
z[6] = z[6] + z[7] + z[8];
z[6] = m1_set::bc<T>[0] * z[6];
z[7] = z[24] + z[78];
z[7] = abb[38] * z[7];
z[8] = abb[25] * abb[39];
z[9] = abb[37] * z[78];
z[6] = z[6] + z[7] + (T(-1) / T(2)) * z[8] + z[9];
z[6] = abb[49] * z[6];
z[1] = 3 * z[1] + z[2] + z[3] + z[4] + z[5] + (T(3) / T(2)) * z[6];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_219_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-29.812321165349265736397436937642014984677930848764465516646227806"),stof<T>("21.120501999199564205866697115437052890912296680443702455469877917")}, std::complex<T>{stof<T>("15.450078917274434293101657209467872430583744502505932285414849113"),stof<T>("-53.134356214699376742634265603799826706847615954368603431025468477")}, std::complex<T>{stof<T>("-7.687120263558075704086300984559788078432254748391678327741697953"),stof<T>("-14.488192071346316483026338377651072600386143246861793296830825326")}, std::complex<T>{stof<T>("-17.615507745935394102840818345096835064136201461388195528260961886"),stof<T>("8.584324298882360639391283163367212198296924741406416600296859453")}, std::complex<T>{stof<T>("6.649017022093725609138036969297300005090047660717341708608668748"),stof<T>("-13.09577886334928700723304252220086849418291639961518254186002122")}, std::complex<T>{stof<T>("-17.615507745935394102840818345096835064136201461388195528260961886"),stof<T>("8.584324298882360639391283163367212198296924741406416600296859453")}, std::complex<T>{stof<T>("45.329731165513963189616948820715415230753058547167730857428348457"),stof<T>("33.446911998062466965682072841213907965343579306196733408363483264")}, std::complex<T>{stof<T>("-41.024978470615073700210068323774544434220028566552717566617134902"),stof<T>("25.031710826068782179829619406738751342025710693049350032508948328")}, std::complex<T>{stof<T>("5.5477963973201460244185816232478799154516817266589282797765971709"),stof<T>("0.5596011630320834407576285701310278015675444605778966866870027563")}, std::complex<T>{stof<T>("-1.8862785407394806987881486953293850685070953846108853041579698942"),stof<T>("1.893635402193665897979852592125278317152816937002069288848804013")}};
	
	std::vector<C> intdlogs = {rlog(k.W[1].real()/kbase.W[1].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[22].real()/kbase.W[22].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_219_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_219_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("20.169288915866978759965121300858341356942001402230393024982752214"),stof<T>("32.215972030982383726121371183691864231929787980224831705591448018")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,53> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W18(k,dl), dlog_W23(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W126(k,dv), dlog_W127(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W135(k,dv), dlog_W188(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_6(k), f_1_7(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), f_2_6_im(k), f_2_10_im(k), f_2_24_im(k), rlog(kend.W[1].real()/k.W[1].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[22].real()/k.W[22].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), f_2_6_re(k), f_2_10_re(k), f_2_24_re(k)};

                    
            return f_4_219_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_219_DLogXconstant_part(base_point<T>, kend);
	value += f_4_219_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_219_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_219_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_219_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_219_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_219_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_219_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
