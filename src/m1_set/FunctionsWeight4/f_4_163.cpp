/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_163.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_163_abbreviated (const std::array<T,17>& abb) {
T z[14];
z[0] = abb[3] + abb[4];
z[1] = abb[13] + abb[14];
z[2] = (T(3) / T(4)) * z[1];
z[2] = z[0] * z[2];
z[3] = abb[11] + abb[12];
z[4] = abb[10] + (T(3) / T(2)) * z[3];
z[5] = -abb[15] + (T(1) / T(2)) * z[4];
z[6] = abb[1] + abb[2];
z[7] = z[5] * z[6];
z[3] = 2 * abb[10] + -4 * abb[15] + 3 * z[3];
z[3] = abb[5] * z[3];
z[4] = 2 * abb[15] + -z[4];
z[8] = abb[0] * z[4];
z[2] = z[2] + -z[3] + 3 * z[7] + -z[8];
z[3] = abb[16] * z[2];
z[5] = z[0] * z[5];
z[7] = abb[0] * (T(2) / T(3));
z[8] = abb[2] + abb[1] * (T(1) / T(3));
z[8] = z[7] + (T(1) / T(4)) * z[8];
z[8] = abb[13] * z[8];
z[9] = abb[1] + abb[2] * (T(1) / T(3));
z[7] = z[7] + (T(1) / T(4)) * z[9];
z[7] = abb[14] * z[7];
z[7] = z[5] + z[7] + z[8];
z[7] = prod_pow(m1_set::bc<T>[0], 2) * z[7];
z[8] = abb[13] * (T(1) / T(2));
z[9] = abb[14] * (T(1) / T(2));
z[10] = z[8] + z[9];
z[6] = abb[0] + (T(1) / T(2)) * z[6];
z[10] = z[6] * z[10];
z[5] = z[5] + z[10];
z[10] = -abb[6] + abb[8];
z[10] = z[5] * z[10];
z[11] = -abb[0] + abb[2];
z[9] = z[9] * z[11];
z[11] = -abb[3] * z[4];
z[12] = abb[0] * (T(1) / T(2));
z[13] = 2 * abb[1] + abb[2] * (T(1) / T(2)) + -z[12];
z[13] = abb[13] * z[13];
z[9] = z[9] + z[11] + z[13];
z[9] = abb[7] * z[9];
z[9] = z[9] + z[10];
z[9] = abb[7] * z[9];
z[10] = -abb[0] + abb[1];
z[8] = z[8] * z[10];
z[10] = -abb[4] * z[4];
z[11] = 2 * abb[2] + abb[1] * (T(1) / T(2)) + -z[12];
z[11] = abb[14] * z[11];
z[8] = z[8] + z[10] + z[11];
z[8] = prod_pow(abb[8], 2) * z[8];
z[10] = -abb[6] + -abb[8];
z[10] = abb[6] * z[5] * z[10];
z[3] = z[3] + z[7] + z[8] + z[9] + z[10];
z[2] = abb[9] * z[2];
z[7] = abb[7] + abb[8];
z[5] = z[5] * z[7];
z[1] = z[1] * z[6];
z[0] = -z[0] * z[4];
z[0] = z[0] + z[1];
z[0] = abb[6] * z[0];
z[0] = z[0] + z[5];
z[0] = m1_set::bc<T>[0] * z[0];
z[0] = z[0] + z[2];
return {z[3], z[0]};
}


template <typename T> std::complex<T> f_4_163_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("3.7750428937635931319690940113069686134663500614979661092813990745"),stof<T>("-6.147904513184628857106431949762537542394915181439860528724860466")}, std::complex<T>{stof<T>("5.6625643406453896979536410169604529201995250922469491639220986118"),stof<T>("-9.221856769776943285659647924643806313592372772159790793087290699")}, std::complex<T>{stof<T>("5.6625643406453896979536410169604529201995250922469491639220986118"),stof<T>("-9.221856769776943285659647924643806313592372772159790793087290699")}, std::complex<T>{stof<T>("12.566746881450349585252947280583076751192871383858558146164888611"),stof<T>("-7.316650242258947503059593500842803694562876468963928804631194841")}, std::complex<T>{stof<T>("12.566746881450349585252947280583076751192871383858558146164888611"),stof<T>("-7.316650242258947503059593500842803694562876468963928804631194841")}, std::complex<T>{stof<T>("-7.550085787527186263938188022613937226932700122995932218562798149"),stof<T>("12.295809026369257714212863899525075084789830362879721057449720932")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[121].real()/kbase.W[121].real()), rlog(k.W[195].real()/kbase.W[195].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_163_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_163_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("4.5869504081885687151714309395736211957041328424382255089715747919"),stof<T>("0.0792548838408560019443555704378996677844803007494005944890137061")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,17> abb = {dl[0], dlog_W7(k,dl), dlog_W10(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlr[2], f_1_1(k), f_1_8(k), f_1_9(k), f_2_25_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[121].real()/k.W[121].real()), rlog(kend.W[195].real()/k.W[195].real()), f_2_25_re(k)};

                    
            return f_4_163_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_163_DLogXconstant_part(base_point<T>, kend);
	value += f_4_163_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_163_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_163_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_163_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_163_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_163_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_163_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
