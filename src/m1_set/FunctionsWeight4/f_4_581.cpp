/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_581.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_581_abbreviated (const std::array<T,31>& abb) {
T z[41];
z[0] = 2 * abb[20] + abb[21];
z[1] = 2 * z[0];
z[2] = abb[22] + abb[23];
z[3] = -abb[25] + z[1] + -z[2];
z[4] = abb[6] * z[3];
z[5] = 2 * z[4];
z[6] = abb[26] + -abb[27];
z[7] = abb[7] + abb[8];
z[7] = z[6] * z[7];
z[8] = -abb[25] + (T(5) / T(3)) * z[0];
z[9] = abb[23] * (T(2) / T(3));
z[10] = z[8] + -z[9];
z[10] = abb[22] * (T(-5) / T(6)) + 2 * z[10];
z[10] = abb[1] * z[10];
z[8] = abb[22] * (T(2) / T(3)) + -z[8];
z[11] = -z[8] + z[9];
z[11] = abb[2] * z[11];
z[8] = -z[8] + -z[9];
z[8] = abb[4] * z[8];
z[9] = -abb[25] + abb[22] * (T(-11) / T(6)) + abb[23] * (T(-8) / T(3)) + (T(7) / T(3)) * z[0];
z[9] = abb[3] * z[9];
z[12] = abb[22] + 2 * abb[23] + -z[0];
z[12] = abb[0] * z[12];
z[13] = -abb[2] + abb[3];
z[13] = -13 * abb[0] + 4 * z[13];
z[13] = abb[19] * z[13];
z[14] = -abb[1] + abb[3];
z[14] = abb[24] * z[14];
z[7] = -z[5] + 3 * z[7] + 4 * z[8] + z[9] + z[10] + z[11] + (T(13) / T(3)) * z[12] + (T(1) / T(3)) * z[13] + (T(1) / T(2)) * z[14];
z[7] = prod_pow(m1_set::bc<T>[0], 2) * z[7];
z[8] = -abb[12] + abb[13];
z[9] = 2 * z[8];
z[10] = abb[11] + z[9];
z[11] = 2 * abb[11];
z[10] = z[10] * z[11];
z[12] = 4 * abb[12];
z[13] = abb[13] + z[12];
z[13] = abb[13] * z[13];
z[14] = prod_pow(abb[12], 2);
z[15] = 2 * z[14];
z[10] = z[10] + -z[13] + z[15];
z[13] = 4 * abb[11];
z[12] = abb[13] + -z[12] + z[13];
z[12] = 2 * z[12];
z[16] = 5 * abb[14];
z[17] = z[12] + -z[16];
z[17] = abb[14] * z[17];
z[17] = -z[10] + z[17];
z[18] = z[0] * z[17];
z[19] = 3 * abb[14];
z[20] = -z[12] + z[19];
z[20] = abb[14] * z[20];
z[20] = z[10] + z[20];
z[21] = 3 * abb[25];
z[20] = z[20] * z[21];
z[12] = 11 * abb[14] + -z[12];
z[12] = abb[14] * z[12];
z[10] = z[10] + z[12];
z[10] = abb[23] * z[10];
z[12] = -abb[22] * z[17];
z[10] = z[10] + z[12] + 4 * z[18] + z[20];
z[10] = abb[1] * z[10];
z[12] = abb[13] * (T(1) / T(2));
z[17] = -3 * abb[12] + -abb[13];
z[17] = z[12] * z[17];
z[20] = 2 * abb[12];
z[22] = -z[11] + z[20];
z[23] = -abb[13] + z[22];
z[23] = z[19] + 2 * z[23];
z[23] = abb[14] * z[23];
z[24] = (T(5) / T(2)) * z[8];
z[25] = abb[11] + z[24];
z[25] = abb[11] * z[25];
z[17] = z[17] + z[23] + z[25];
z[17] = z[17] * z[21];
z[22] = abb[13] + z[22];
z[23] = abb[14] + 2 * z[22];
z[23] = abb[14] * z[23];
z[25] = abb[11] * z[8];
z[26] = abb[12] * abb[13];
z[27] = z[23] + z[25] + -3 * z[26];
z[27] = abb[23] * z[27];
z[28] = 4 * z[14];
z[23] = z[23] + z[28];
z[29] = -7 * abb[12] + -abb[13];
z[29] = z[12] * z[29];
z[30] = (T(1) / T(2)) * z[8];
z[31] = abb[11] + z[30];
z[31] = abb[11] * z[31];
z[29] = z[23] + z[29] + z[31];
z[29] = abb[22] * z[29];
z[17] = z[17] + 2 * z[18] + z[27] + z[29];
z[17] = abb[2] * z[17];
z[18] = abb[0] + abb[2];
z[18] = abb[3] + -2 * abb[10] + (T(3) / T(2)) * z[18];
z[18] = z[6] * z[18];
z[27] = -abb[7] + abb[9];
z[27] = z[0] * z[27];
z[29] = abb[8] * (T(1) / T(2));
z[31] = 2 * abb[7] + -abb[9] + z[29];
z[31] = abb[25] * z[31];
z[32] = abb[9] + z[29];
z[32] = abb[22] * z[32];
z[33] = abb[8] + abb[9];
z[33] = abb[23] * z[33];
z[29] = abb[19] * z[29];
z[18] = -z[18] + 2 * z[27] + z[29] + z[31] + -z[32] + -z[33];
z[18] = 3 * z[18];
z[27] = -abb[29] * z[18];
z[29] = z[8] + z[11];
z[31] = abb[11] * z[29];
z[32] = abb[14] * (T(1) / T(2));
z[22] = z[22] + z[32];
z[22] = abb[14] * z[22];
z[33] = -abb[12] + z[12];
z[33] = abb[13] * z[33];
z[22] = -z[14] + z[22] + z[31] + z[33];
z[22] = abb[22] * z[22];
z[31] = -abb[11] + z[8];
z[31] = z[11] * z[31];
z[20] = -abb[13] + z[20];
z[20] = abb[13] * z[20];
z[20] = z[20] + -z[23] + -z[31];
z[20] = -z[0] * z[20];
z[23] = 2 * abb[14];
z[31] = abb[11] + -abb[12];
z[31] = z[23] * z[31];
z[26] = z[26] + z[31];
z[25] = -z[14] + -z[25] + z[26];
z[25] = z[21] * z[25];
z[31] = 3 * abb[11];
z[33] = z[9] + z[31];
z[33] = abb[11] * z[33];
z[34] = abb[11] + z[8];
z[35] = 2 * z[34];
z[36] = abb[14] + z[35];
z[36] = abb[14] * z[36];
z[33] = -z[33] + z[36];
z[36] = -abb[23] * z[33];
z[20] = z[20] + z[22] + z[25] + z[36];
z[20] = abb[3] * z[20];
z[22] = -z[8] + z[11];
z[22] = abb[11] * z[22];
z[25] = abb[12] + abb[13];
z[36] = abb[13] * z[25];
z[15] = -z[15] + z[36];
z[22] = -z[15] + z[22];
z[22] = z[0] * z[22];
z[37] = 8 * abb[11];
z[38] = z[8] + -z[37];
z[38] = abb[11] * z[38];
z[15] = z[15] + z[38];
z[15] = abb[23] * z[15];
z[38] = z[13] + -z[30];
z[38] = abb[11] * z[38];
z[12] = z[12] * z[25];
z[25] = z[12] + -z[14];
z[38] = -z[25] + z[38];
z[39] = -abb[22] * z[38];
z[40] = abb[11] * z[30];
z[25] = z[25] + z[40];
z[25] = z[21] * z[25];
z[15] = z[15] + 2 * z[22] + z[25] + z[39];
z[15] = abb[0] * z[15];
z[1] = z[1] + z[2] + -z[21];
z[22] = abb[3] + abb[4];
z[1] = z[1] * z[22];
z[22] = z[2] + z[21];
z[25] = 4 * z[0] + -z[22];
z[39] = 2 * z[25];
z[39] = abb[2] * z[39];
z[25] = abb[1] * z[25];
z[1] = z[1] + 3 * z[4] + -4 * z[25] + -z[39];
z[1] = 2 * z[1];
z[4] = -abb[28] * z[1];
z[39] = 2 * abb[2];
z[39] = z[3] * z[39];
z[40] = abb[25] + -z[0];
z[40] = -abb[22] + 2 * z[40];
z[40] = abb[3] * z[40];
z[5] = -z[5] + 2 * z[25] + z[39] + z[40];
z[5] = abb[15] * z[5];
z[25] = prod_pow(abb[11], 2);
z[25] = -z[14] + z[25];
z[25] = abb[7] * z[25];
z[39] = -abb[11] + z[30];
z[39] = abb[11] * z[39];
z[12] = z[12] + z[39];
z[39] = abb[8] * z[12];
z[25] = z[25] + -z[39];
z[25] = -z[6] * z[25];
z[39] = prod_pow(abb[13], 2);
z[32] = -abb[13] + -z[32];
z[32] = abb[14] * z[32];
z[32] = abb[15] + z[32] + (T(-1) / T(2)) * z[39];
z[32] = abb[3] * z[32];
z[39] = abb[1] * prod_pow(abb[14], 2);
z[32] = z[32] + 2 * z[39];
z[32] = abb[24] * z[32];
z[5] = z[5] + z[25] + z[32];
z[25] = abb[11] * z[34];
z[14] = -z[14] + -z[25] + -z[36];
z[14] = abb[3] * z[14];
z[32] = abb[0] * z[38];
z[12] = -z[12] + z[28];
z[12] = abb[2] * z[12];
z[12] = z[12] + z[14] + z[32];
z[12] = abb[19] * z[12];
z[14] = -z[16] + z[35];
z[14] = abb[14] * z[14];
z[16] = -z[9] + z[31];
z[16] = abb[11] * z[16];
z[14] = z[14] + z[16];
z[14] = -z[0] * z[14];
z[2] = -z[2] * z[33];
z[16] = -z[19] + z[35];
z[16] = abb[14] * z[16];
z[9] = abb[11] + -z[9];
z[9] = abb[11] * z[9];
z[9] = z[9] + z[16];
z[9] = z[9] * z[21];
z[2] = z[2] + z[9] + 2 * z[14];
z[2] = abb[4] * z[2];
z[9] = -z[25] + z[26];
z[14] = 3 * abb[6];
z[9] = -z[3] * z[9] * z[14];
z[2] = abb[30] + z[2] + z[4] + 3 * z[5] + z[7] + z[9] + z[10] + z[12] + z[15] + z[17] + z[20] + z[27];
z[4] = -z[23] + z[34];
z[5] = z[0] * z[4];
z[7] = 4 * abb[14];
z[9] = z[7] + -z[11];
z[10] = z[9] + -z[24];
z[10] = z[10] * z[21];
z[7] = z[7] + -z[8];
z[7] = abb[23] * z[7];
z[9] = z[9] + -z[30];
z[9] = abb[22] * z[9];
z[7] = 8 * z[5] + z[7] + z[9] + z[10];
z[7] = abb[2] * z[7];
z[9] = -abb[14] + z[8];
z[10] = z[9] + z[31];
z[12] = abb[23] * z[10];
z[15] = z[9] + -z[31];
z[15] = -z[0] * z[15];
z[9] = -abb[11] + z[9];
z[9] = z[9] * z[21];
z[10] = -abb[22] * z[10];
z[9] = z[9] + z[10] + -z[12] + 2 * z[15];
z[9] = abb[4] * z[9];
z[10] = -z[8] + z[23];
z[15] = z[10] + z[11];
z[15] = z[0] * z[15];
z[12] = z[12] + -z[15];
z[15] = -z[10] * z[21];
z[16] = z[10] + -z[13];
z[16] = abb[22] * z[16];
z[12] = -2 * z[12] + z[15] + z[16];
z[12] = abb[3] * z[12];
z[13] = -z[8] + z[13];
z[0] = -z[0] * z[13];
z[13] = -z[30] + z[37];
z[15] = abb[22] * z[13];
z[16] = abb[25] * z[8];
z[8] = 16 * abb[11] + -z[8];
z[8] = abb[23] * z[8];
z[0] = 2 * z[0] + z[8] + z[15] + (T(-3) / T(2)) * z[16];
z[0] = abb[0] * z[0];
z[8] = abb[3] * z[29];
z[15] = -z[11] + z[30];
z[16] = abb[2] * z[15];
z[13] = -abb[0] * z[13];
z[8] = z[8] + z[13] + z[16];
z[8] = abb[19] * z[8];
z[4] = -z[4] * z[22];
z[4] = z[4] + 4 * z[5];
z[5] = 4 * abb[1];
z[4] = z[4] * z[5];
z[5] = z[10] + -z[11];
z[3] = z[3] * z[5] * z[14];
z[5] = abb[8] * z[15];
z[10] = abb[7] * z[11];
z[5] = -z[5] + z[10];
z[5] = z[5] * z[6];
z[0] = z[0] + z[3] + z[4] + 3 * z[5] + z[7] + z[8] + 2 * z[9] + z[12];
z[0] = m1_set::bc<T>[0] * z[0];
z[3] = -abb[17] * z[18];
z[1] = -abb[16] * z[1];
z[0] = abb[18] + z[0] + z[1] + z[3];
return {z[2], z[0]};
}


template <typename T> std::complex<T> f_4_581_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("10.136426419815678412697177642306140753505640737452435483270761744"),stof<T>("49.413046312118955875886141470692412640741316153393515836844825111")}, std::complex<T>{stof<T>("-6.227444306250794676779768920348826742376437561684787019366391816"),stof<T>("38.096154908035289683399412155557973913452570247365415204412138984")}, std::complex<T>{stof<T>("-3.113722153125397338389884460174413371188218780842393509683195908"),stof<T>("19.048077454017644841699706077778986956726285123682707602206069492")}, std::complex<T>{stof<T>("41.04917951169013373346481824034043736439385857123899270959413576"),stof<T>("-35.238265216236073395732605540007005411479415806327627775036051924")}, std::complex<T>{stof<T>("30.912753091874455320767640598034296610888217833786557226323374016"),stof<T>("-84.651311528355029271618747010699418052220731959721143611880877036")}, std::complex<T>{stof<T>("-37.935457358564736395074933780166023993205639790396599199910939852"),stof<T>("16.190187762218428554032899462228018454753130682644920172829982432")}, std::complex<T>{stof<T>("27.92831286295017653844769954488855020241430807694077213756770908"),stof<T>("-40.739096344846029908780604306268048317199884196507126393435065635")}, std::complex<T>{stof<T>("-27.92831286295017653844769954488855020241430807694077213756770908"),stof<T>("40.739096344846029908780604306268048317199884196507126393435065635")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[9].real()/kbase.W[9].real()), rlog(k.W[13].real()/kbase.W[13].real()), rlog(k.W[28].real()/kbase.W[28].real()), rlog(k.W[118].real()/kbase.W[118].real()), rlog(k.W[124].real()/kbase.W[124].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_581_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_581_W_17_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(4)) * (-v[0] + v[1] + -v[2] + -v[3] + v[5]) * (-8 + -3 * v[0] + -v[1] + -3 * v[2] + v[3] + 4 * v[4] + 4 * m1_set::bc<T>[1] * (4 + v[0] + v[1] + v[2] + -v[3] + -2 * v[4] + -v[5]) + 3 * v[5])) / prod_pow(tend, 2);
c[1] = (-6 * (1 + 2 * m1_set::bc<T>[1]) * (v[0] + -v[1] + v[2] + v[3] + -v[5])) / tend;


		return (4 * abb[20] + 2 * abb[21] + -abb[22] + abb[24] + -2 * abb[25]) * (t * c[0] + c[1]) * (T(1) / T(4));
	}
	{
T z[4];
z[0] = abb[22] + -abb[24];
z[1] = -2 * abb[20] + -abb[21] + abb[25];
z[2] = z[0] + 2 * z[1];
z[3] = abb[14] * z[2];
z[0] = (T(1) / T(2)) * z[0] + z[1];
z[1] = abb[13] * z[0];
z[1] = z[1] + z[3];
z[1] = abb[13] * z[1];
z[0] = -prod_pow(abb[14], 2) * z[0];
z[2] = -abb[15] * z[2];
z[0] = 3 * z[0] + z[1] + z[2];
return 3 * abb[5] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_581_W_17_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_581_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("21.459621077509670590233937235353460125442031547687652524297715566"),stof<T>("-33.422100577583928921598840244875571023198752041568562645819411427")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({16});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,31> abb = {dl[0], dlog_W4(k,dl), dlog_W7(k,dl), dlog_W10(k,dl), dlog_W14(k,dl), dlog_W17(k,dl), dlog_W29(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlr[2], f_1_1(k), f_1_8(k), f_1_9(k), f_1_11(k), f_2_15(k), f_2_2_im(k), f_2_25_im(k), T{0}, rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[9].real()/k.W[9].real()), rlog(kend.W[13].real()/k.W[13].real()), IntDLogL_W_17(t,k,kend,dl), rlog(kend.W[28].real()/k.W[28].real()), rlog(kend.W[118].real()/k.W[118].real()), rlog(kend.W[124].real()/k.W[124].real()), f_2_2_re(k), f_2_25_re(k), T{0}};
abb[18] = SpDLog_f_4_581_W_17_Im(t, path, abb);
abb[30] = SpDLog_f_4_581_W_17_Re(t, path, abb);

                    
            return f_4_581_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_581_DLogXconstant_part(base_point<T>, kend);
	value += f_4_581_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_581_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_581_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_581_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_581_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_581_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_581_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
