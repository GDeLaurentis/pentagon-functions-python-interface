/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_179.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_179_abbreviated (const std::array<T,25>& abb) {
T z[45];
z[0] = abb[18] + -abb[22];
z[1] = abb[3] * z[0];
z[2] = -abb[21] + abb[24];
z[3] = -abb[18] + z[2];
z[4] = abb[1] * z[3];
z[5] = z[1] + -z[4];
z[6] = abb[19] + abb[23];
z[7] = 2 * z[6];
z[8] = abb[4] + -abb[8];
z[9] = -abb[0] + 2 * abb[5] + z[8];
z[9] = z[7] * z[9];
z[10] = 5 * abb[22];
z[11] = abb[18] + z[2];
z[12] = abb[20] + z[10] + -z[11];
z[13] = -abb[5] * z[12];
z[14] = 4 * abb[22] + -z[11];
z[15] = abb[8] * z[14];
z[16] = abb[22] + z[2];
z[17] = -abb[4] * z[16];
z[18] = 2 * abb[22];
z[19] = -abb[18] + abb[20] + z[18];
z[20] = abb[0] * z[19];
z[16] = -2 * abb[18] + z[16];
z[16] = abb[7] * z[16];
z[9] = z[5] + z[9] + z[13] + z[15] + z[16] + z[17] + z[20];
z[9] = abb[15] * z[9];
z[13] = prod_pow(abb[11], 2);
z[16] = 2 * abb[13] + -abb[17] + -z[13];
z[16] = z[8] * z[16];
z[17] = abb[2] + abb[7];
z[20] = abb[13] + -abb[17];
z[21] = 2 * z[20];
z[17] = z[17] * z[21];
z[21] = abb[5] * abb[11];
z[22] = abb[11] * z[8];
z[23] = z[21] + z[22];
z[23] = 2 * z[23];
z[24] = -abb[9] + abb[12];
z[23] = z[23] * z[24];
z[25] = z[13] + z[20];
z[25] = abb[0] * z[25];
z[26] = abb[0] + z[8];
z[27] = prod_pow(abb[10], 2);
z[28] = z[26] * z[27];
z[29] = -5 * abb[13] + 4 * abb[17];
z[29] = abb[5] * z[29];
z[16] = -z[16] + z[17] + -z[23] + z[25] + -z[28] + z[29];
z[6] = -z[6] * z[16];
z[16] = abb[20] + -abb[22];
z[16] = abb[0] * z[16];
z[17] = z[1] + z[16];
z[23] = abb[20] * (T(1) / T(2));
z[25] = abb[18] * (T(1) / T(2));
z[28] = -abb[22] + z[25];
z[29] = z[23] + z[28];
z[29] = abb[5] * z[29];
z[17] = (T(1) / T(2)) * z[17] + -z[29];
z[17] = abb[9] * z[17];
z[30] = 3 * abb[22];
z[31] = 2 * z[2];
z[32] = abb[20] + z[30] + -z[31];
z[21] = z[21] * z[32];
z[33] = z[3] + z[18];
z[34] = abb[4] * z[33];
z[34] = -z[15] + z[34];
z[35] = -z[1] + z[34];
z[35] = abb[11] * z[35];
z[36] = abb[0] * abb[11];
z[37] = -abb[20] + z[2];
z[38] = z[36] * z[37];
z[17] = z[17] + z[21] + z[35] + z[38];
z[17] = abb[9] * z[17];
z[35] = abb[6] * z[37];
z[38] = -abb[22] + z[2];
z[39] = abb[2] * z[38];
z[40] = -z[1] + z[35] + z[39];
z[41] = abb[22] * (T(3) / T(2));
z[42] = z[2] + -z[23] + -z[41];
z[42] = abb[8] * z[42];
z[43] = abb[4] * z[2];
z[40] = (T(-1) / T(2)) * z[40] + z[42] + z[43];
z[40] = z[13] * z[40];
z[41] = -z[25] + -z[37] + z[41];
z[41] = abb[13] * z[41];
z[42] = (T(1) / T(2)) * z[2];
z[43] = -abb[20] + z[42];
z[44] = z[28] + z[43];
z[44] = abb[17] * z[44];
z[23] = z[23] + -z[28];
z[23] = z[13] * z[23];
z[23] = z[23] + z[41] + z[44];
z[23] = abb[0] * z[23];
z[32] = abb[8] * z[32];
z[41] = -abb[4] * z[18];
z[32] = z[32] + z[35] + -z[39] + z[41];
z[32] = abb[11] * z[32];
z[0] = -z[0] + z[37];
z[0] = abb[8] * z[0];
z[0] = z[0] + -z[35];
z[3] = abb[4] * z[3];
z[3] = z[3] + -z[39];
z[41] = z[0] + -z[3];
z[38] = abb[0] * z[38];
z[44] = -z[38] + z[41];
z[29] = z[29] + (T(1) / T(2)) * z[44];
z[29] = abb[12] * z[29];
z[21] = -z[21] + z[29] + z[32];
z[21] = abb[12] * z[21];
z[28] = -z[28] + z[42];
z[28] = abb[17] * z[28];
z[29] = -abb[13] * z[33];
z[28] = z[28] + z[29];
z[28] = abb[4] * z[28];
z[29] = -z[18] + z[25] + z[42];
z[32] = abb[0] + -abb[8];
z[29] = z[29] * z[32];
z[32] = abb[4] * z[11];
z[29] = z[29] + (T(-1) / T(2)) * z[32];
z[29] = z[27] * z[29];
z[12] = abb[17] * z[12];
z[25] = -2 * abb[20] + abb[22] * (T(-11) / T(2)) + z[25] + z[31];
z[25] = abb[13] * z[25];
z[12] = z[12] + z[25];
z[12] = abb[5] * z[12];
z[13] = -z[13] + z[27];
z[11] = z[11] + -z[18];
z[13] = z[11] * z[13];
z[18] = z[19] * z[20];
z[13] = z[13] + z[18];
z[13] = abb[7] * z[13];
z[0] = z[0] + z[5] + z[16];
z[0] = abb[14] * z[0];
z[3] = -z[3] + z[4] + -z[38];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[5] = -abb[0] * z[37];
z[5] = z[1] + z[5] + z[41];
z[5] = abb[16] * z[5];
z[16] = abb[2] * abb[22];
z[1] = z[1] + 2 * z[16];
z[1] = z[1] * z[20];
z[16] = abb[18] * (T(3) / T(2)) + -z[30] + -z[43];
z[16] = abb[17] * z[16];
z[18] = abb[13] * z[14];
z[16] = z[16] + z[18];
z[16] = abb[8] * z[16];
z[18] = abb[17] + -z[27];
z[18] = z[4] * z[18];
z[19] = abb[17] * z[35];
z[0] = z[0] + z[1] + (T(13) / T(12)) * z[3] + z[5] + z[6] + z[9] + z[12] + z[13] + z[16] + z[17] + z[18] + z[19] + z[21] + z[23] + z[28] + z[29] + z[40];
z[1] = abb[5] + z[8];
z[1] = -z[1] * z[24];
z[3] = abb[10] * z[26];
z[1] = z[1] + -z[3] + z[22] + z[36];
z[1] = z[1] * z[7];
z[3] = abb[0] * z[14];
z[3] = z[3] + 2 * z[4] + -z[15] + z[32];
z[3] = abb[10] * z[3];
z[4] = -abb[18] + z[10] + -z[31];
z[4] = abb[5] * z[4];
z[4] = z[4] + z[34] + z[38];
z[4] = z[4] * z[24];
z[2] = abb[18] + -3 * z[2];
z[2] = abb[4] * z[2];
z[2] = z[2] + z[15] + 2 * z[39];
z[2] = abb[11] * z[2];
z[5] = -z[33] * z[36];
z[6] = -abb[10] + abb[11];
z[6] = abb[7] * z[6] * z[11];
z[1] = z[1] + z[2] + z[3] + z[4] + z[5] + 2 * z[6];
z[1] = m1_set::bc<T>[0] * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_179_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("7.1593569497887474019236227664745536050933946786930038421589071332"),stof<T>("8.732398216787095267288414339544914309185370144553466779083451114")}, std::complex<T>{stof<T>("4.6925936352181035204994613969195293736786346234932926727690137825"),stof<T>("-7.3463101351597655110405205590701614752726809313370293032535760193")}, stof<T>("-0.88595056650299583212754242281192244051053352546785891384511314282"), std::complex<T>{stof<T>("-2.4916140455352310781051575813203065664276544493181348988368653859"),stof<T>("-1.4427111615777469628263467038753244155957167765392080775627615949")}, std::complex<T>{stof<T>("-13.4576140640390861684006993219024671046891502260365724999196731589"),stof<T>("-2.8287992432050767190742404843500772495084059897556455533926366896")}, std::complex<T>{stof<T>("4.6925936352181035204994613969195293736786346234932926727690137825"),stof<T>("-7.3463101351597655110405205590701614752726809313370293032535760193")}, std::complex<T>{stof<T>("2.4916140455352310781051575813203065664276544493181348988368653859"),stof<T>("1.4427111615777469628263467038753244155957167765392080775627615949")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[20].real()/kbase.W[20].real()), rlog(k.W[29].real()/kbase.W[29].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_179_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_179_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-8.418737171081648077072891067381548424408217917256161691161539954"),stof<T>("-14.742217690683416069499016322347251838088483012882290663748068047")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,25> abb = {dl[0], dl[1], dlog_W3(k,dl), dl[2], dl[4], dlog_W13(k,dl), dlog_W18(k,dl), dlog_W21(k,dl), dlog_W30(k,dl), f_1_1(k), f_1_3(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_3(k), f_2_5(k), f_2_8(k), f_2_21(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[20].real()/k.W[20].real()), rlog(kend.W[29].real()/k.W[29].real())};

                    
            return f_4_179_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_179_DLogXconstant_part(base_point<T>, kend);
	value += f_4_179_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_179_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_179_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_179_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_179_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_179_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_179_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
