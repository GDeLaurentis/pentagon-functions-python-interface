/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_449.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_449_abbreviated (const std::array<T,63>& abb) {
T z[67];
z[0] = -abb[30] + abb[31];
z[1] = -abb[29] + abb[32];
z[2] = z[0] + z[1];
z[3] = m1_set::bc<T>[0] * z[2];
z[4] = -abb[39] + abb[40];
z[3] = 3 * z[3] + z[4];
z[5] = abb[15] * (T(1) / T(2));
z[3] = z[3] * z[5];
z[6] = abb[33] * (T(1) / T(4));
z[7] = abb[34] * (T(-1) / T(4)) + -z[1] + z[6];
z[7] = m1_set::bc<T>[0] * z[7];
z[8] = abb[39] + abb[41];
z[9] = -abb[40] + z[8];
z[7] = z[7] + (T(1) / T(2)) * z[9];
z[7] = abb[7] * z[7];
z[9] = -abb[34] + z[0];
z[10] = -abb[29] + abb[33];
z[11] = -abb[32] + -z[9] + z[10];
z[11] = m1_set::bc<T>[0] * z[11];
z[11] = -abb[41] + z[11];
z[12] = abb[4] * (T(1) / T(2));
z[11] = z[11] * z[12];
z[13] = abb[34] * (T(1) / T(2));
z[14] = abb[33] * (T(1) / T(2));
z[15] = z[13] + -z[14];
z[16] = z[1] + z[15];
z[16] = m1_set::bc<T>[0] * z[16];
z[17] = -abb[39] + z[16];
z[18] = abb[17] * (T(1) / T(2));
z[19] = z[17] * z[18];
z[20] = abb[40] + abb[41];
z[21] = abb[30] + abb[34];
z[21] = -abb[31] + (T(1) / T(2)) * z[21];
z[21] = m1_set::bc<T>[0] * z[21];
z[20] = (T(1) / T(2)) * z[20] + -z[21];
z[22] = abb[8] * z[20];
z[23] = abb[14] * z[0];
z[24] = m1_set::bc<T>[0] * z[23];
z[25] = -abb[29] + abb[31];
z[25] = m1_set::bc<T>[0] * z[25];
z[26] = abb[9] * z[25];
z[3] = z[3] + z[7] + z[11] + z[19] + z[22] + -z[24] + -z[26];
z[4] = -z[4] + -z[25];
z[4] = abb[5] * z[4];
z[7] = -abb[32] + abb[33];
z[11] = m1_set::bc<T>[0] * z[7];
z[11] = abb[40] + -abb[41] + z[11];
z[11] = abb[6] * z[11];
z[19] = abb[33] + abb[34];
z[19] = -abb[31] + (T(1) / T(2)) * z[19];
z[19] = m1_set::bc<T>[0] * z[19];
z[19] = -abb[41] + z[19];
z[22] = -abb[16] * z[19];
z[4] = -abb[45] + z[4] + z[11] + z[22];
z[11] = -abb[1] * z[20];
z[22] = abb[18] + abb[20] + -abb[21];
z[25] = (T(1) / T(8)) * z[22];
z[25] = abb[42] * z[25];
z[27] = -abb[33] + abb[34];
z[28] = m1_set::bc<T>[0] * z[27];
z[29] = abb[0] * z[28];
z[3] = (T(1) / T(2)) * z[3] + (T(1) / T(4)) * z[4] + z[11] + z[25] + (T(1) / T(8)) * z[29];
z[3] = abb[51] * z[3];
z[4] = -abb[52] * z[20];
z[11] = m1_set::bc<T>[0] * z[10];
z[25] = -abb[41] + z[11];
z[29] = abb[49] * z[25];
z[4] = z[4] + (T(1) / T(2)) * z[29];
z[4] = abb[1] * z[4];
z[21] = abb[40] * (T(1) / T(2)) + -z[21];
z[21] = abb[6] * abb[52] * z[21];
z[3] = z[3] + z[4] + z[21];
z[4] = abb[4] * z[2];
z[21] = abb[10] * z[1];
z[29] = -abb[6] * z[7];
z[4] = z[4] + z[21] + z[29];
z[4] = m1_set::bc<T>[0] * z[4];
z[29] = abb[13] * abb[41];
z[30] = -z[26] + z[29];
z[31] = abb[7] * m1_set::bc<T>[0];
z[32] = -z[1] * z[31];
z[25] = abb[1] * z[25];
z[33] = abb[34] + z[1];
z[34] = -abb[33] + z[33];
z[34] = m1_set::bc<T>[0] * z[34];
z[34] = -abb[39] + z[34];
z[34] = abb[0] * z[34];
z[4] = abb[45] * (T(1) / T(2)) + z[4] + z[25] + z[30] + z[32] + z[34];
z[25] = abb[50] * (T(1) / T(16));
z[4] = z[4] * z[25];
z[32] = -abb[32] + z[14];
z[34] = abb[31] + -z[13] + z[32];
z[34] = m1_set::bc<T>[0] * z[34];
z[34] = abb[40] + z[34];
z[35] = abb[24] + abb[26];
z[34] = -z[34] * z[35];
z[33] = -abb[31] + z[33];
z[33] = m1_set::bc<T>[0] * z[33];
z[8] = -z[8] + z[33];
z[8] = abb[23] * z[8];
z[17] = abb[27] * z[17];
z[19] = abb[22] * z[19];
z[8] = z[8] + z[17] + z[19] + z[34];
z[17] = abb[25] * z[20];
z[19] = abb[42] * (T(1) / T(4));
z[20] = abb[28] * z[19];
z[8] = (T(1) / T(2)) * z[8] + -z[17] + -z[20];
z[17] = abb[46] + abb[47] + -abb[48];
z[20] = (T(-1) / T(16)) * z[17];
z[8] = z[8] * z[20];
z[20] = z[10] * z[31];
z[33] = -abb[29] + -z[13] + -z[14];
z[33] = abb[31] + (T(1) / T(2)) * z[33];
z[33] = abb[4] * m1_set::bc<T>[0] * z[33];
z[20] = (T(-1) / T(2)) * z[20] + z[30] + z[33];
z[30] = abb[49] * (T(1) / T(16));
z[20] = z[20] * z[30];
z[33] = abb[29] * (T(1) / T(2)) + z[32];
z[34] = z[31] * z[33];
z[36] = -abb[29] + z[14];
z[37] = z[13] + z[36];
z[38] = z[12] * z[37];
z[21] = z[21] + z[38];
z[21] = m1_set::bc<T>[0] * z[21];
z[27] = -z[1] + (T(-3) / T(4)) * z[27];
z[27] = m1_set::bc<T>[0] * z[27];
z[27] = abb[39] + z[27];
z[39] = -abb[0] * z[27];
z[21] = z[21] + z[34] + z[39];
z[39] = abb[54] * (T(1) / T(16));
z[21] = z[21] * z[39];
z[2] = abb[15] * z[2];
z[2] = z[2] + -z[23] + z[38];
z[2] = m1_set::bc<T>[0] * z[2];
z[2] = z[2] + -z[26] + z[34];
z[23] = abb[53] * (T(1) / T(16));
z[2] = z[2] * z[23];
z[0] = z[0] * z[31];
z[0] = z[0] + -z[24] + z[29];
z[24] = abb[52] * (T(1) / T(16));
z[0] = z[0] * z[24];
z[26] = abb[49] * (T(1) / T(4));
z[28] = z[26] * z[28];
z[27] = -abb[53] * z[27];
z[19] = -abb[55] * z[19];
z[19] = z[19] + z[27] + z[28];
z[27] = abb[0] * (T(1) / T(16));
z[19] = z[19] * z[27];
z[28] = abb[50] + abb[51] + abb[53] + abb[54];
z[29] = abb[2] * (T(1) / T(8));
z[28] = z[28] * z[29];
z[16] = abb[39] * (T(1) / T(2)) + -z[16];
z[16] = z[16] * z[28];
z[29] = abb[18] * (T(1) / T(32));
z[11] = z[11] * z[29];
z[31] = abb[4] * (T(1) / T(64));
z[34] = abb[42] * z[31];
z[11] = z[11] + z[34];
z[11] = abb[55] * z[11];
z[34] = abb[55] * m1_set::bc<T>[0] * z[37];
z[37] = -abb[49] + abb[53] + abb[54];
z[38] = (T(1) / T(2)) * z[37];
z[38] = abb[42] * z[38];
z[34] = z[34] + z[38];
z[34] = abb[19] * z[34];
z[0] = abb[60] + abb[61] + z[0] + z[2] + (T(1) / T(8)) * z[3] + z[4] + z[8] + z[11] + z[16] + z[19] + z[20] + z[21] + (T(1) / T(32)) * z[34];
z[2] = abb[32] * (T(1) / T(2));
z[3] = -abb[30] + z[2];
z[4] = -abb[29] + z[3];
z[4] = abb[32] * z[4];
z[8] = abb[37] + abb[38];
z[11] = -abb[36] + z[4] + z[8];
z[16] = prod_pow(abb[29], 2);
z[19] = (T(1) / T(2)) * z[16];
z[20] = -abb[58] + z[19];
z[21] = abb[29] + abb[30] * (T(1) / T(2));
z[21] = abb[30] * z[21];
z[34] = abb[29] * abb[33];
z[38] = prod_pow(m1_set::bc<T>[0], 2);
z[40] = -abb[30] + -z[7];
z[40] = abb[34] * z[40];
z[41] = abb[31] * (T(1) / T(2));
z[42] = abb[34] + -z[41];
z[42] = abb[31] * z[42];
z[40] = z[11] + -z[20] + z[21] + z[34] + z[38] + z[40] + z[42];
z[40] = z[12] * z[40];
z[42] = abb[38] + abb[57] + -abb[58] + (T(3) / T(2)) * z[16] + z[21];
z[43] = -abb[29] + abb[32] * (T(1) / T(4));
z[43] = abb[32] * z[43];
z[44] = abb[56] * (T(1) / T(2));
z[43] = abb[36] * (T(-1) / T(2)) + z[43] + -z[44];
z[45] = -abb[30] + -z[33];
z[45] = z[14] * z[45];
z[46] = -abb[30] + z[41];
z[47] = z[10] + z[46];
z[41] = z[41] * z[47];
z[48] = abb[34] * z[10];
z[49] = (T(1) / T(3)) * z[38];
z[41] = z[41] + (T(1) / T(2)) * z[42] + z[43] + z[45] + (T(1) / T(4)) * z[48] + -z[49];
z[41] = abb[7] * z[41];
z[42] = z[10] * z[13];
z[45] = abb[58] + z[19];
z[50] = abb[31] * z[10];
z[51] = abb[29] * z[14];
z[42] = z[42] + -z[45] + -z[49] + -z[50] + z[51];
z[52] = -abb[35] + z[42];
z[52] = abb[16] * z[52];
z[47] = abb[31] * z[47];
z[47] = z[21] + z[47];
z[53] = -abb[34] * z[7];
z[11] = -abb[57] + z[11] + z[45] + z[47] + z[53];
z[11] = abb[6] * z[11];
z[45] = abb[31] * z[46];
z[8] = z[8] + z[21] + z[45];
z[21] = -abb[57] + z[8];
z[3] = abb[32] * z[3];
z[3] = -abb[35] + z[3] + -z[19];
z[46] = -abb[56] + -z[3] + -z[21];
z[46] = abb[5] * z[46];
z[53] = -abb[30] + abb[32];
z[7] = z[7] * z[53];
z[53] = (T(1) / T(6)) * z[38];
z[54] = -abb[36] + z[53];
z[7] = -abb[37] + z[7] + z[54];
z[7] = abb[3] * z[7];
z[7] = z[7] + z[11] + z[46] + z[52];
z[11] = prod_pow(abb[30], 2);
z[46] = z[11] + -z[16];
z[52] = -abb[37] + -abb[57] + z[46];
z[55] = -abb[30] + z[1];
z[15] = -z[15] * z[55];
z[56] = 3 * abb[29];
z[57] = abb[30] + z[56];
z[57] = -abb[32] + (T(1) / T(2)) * z[57];
z[57] = abb[32] * z[57];
z[58] = -3 * abb[30] + -abb[34] + z[10];
z[58] = abb[31] + (T(1) / T(2)) * z[58];
z[58] = abb[31] * z[58];
z[15] = z[15] + z[44] + (T(1) / T(2)) * z[52] + z[57] + z[58];
z[15] = abb[15] * z[15];
z[44] = (T(1) / T(2)) * z[38];
z[11] = (T(1) / T(2)) * z[11] + -z[44] + z[45];
z[52] = abb[14] * z[11];
z[33] = abb[34] * z[33];
z[57] = abb[36] + z[49];
z[33] = z[33] + -z[51] + z[57];
z[51] = abb[29] * abb[32];
z[58] = -z[19] + z[51];
z[59] = abb[56] + z[58];
z[60] = z[33] + z[59];
z[18] = z[18] * z[60];
z[34] = -z[16] + z[34];
z[61] = z[34] + -z[50];
z[61] = abb[9] * z[61];
z[9] = abb[31] * z[9];
z[62] = abb[30] * abb[34];
z[9] = -abb[57] + z[9] + z[62];
z[62] = abb[58] + (T(5) / T(6)) * z[38];
z[63] = z[9] + -z[62];
z[64] = -abb[1] + abb[8] * (T(1) / T(2));
z[64] = z[63] * z[64];
z[65] = abb[59] * (T(1) / T(4));
z[22] = -z[22] * z[65];
z[5] = abb[9] + z[5] + -z[12];
z[5] = abb[35] * z[5];
z[10] = abb[33] * z[10];
z[10] = z[10] + -z[48];
z[12] = abb[0] * z[10];
z[66] = abb[62] * (T(1) / T(2));
z[5] = z[5] + (T(1) / T(2)) * z[7] + (T(1) / T(4)) * z[12] + z[15] + z[18] + z[22] + z[40] + z[41] + -z[52] + -z[61] + z[64] + z[66];
z[5] = abb[51] * z[5];
z[7] = abb[33] * z[36];
z[12] = z[7] + z[20] + -z[49];
z[15] = -abb[49] * z[12];
z[18] = -abb[52] * z[63];
z[15] = z[15] + z[18];
z[15] = abb[1] * z[15];
z[5] = z[5] + z[15];
z[15] = abb[33] * z[55];
z[15] = z[15] + z[50];
z[18] = abb[38] + z[54];
z[20] = -abb[29] * abb[30];
z[20] = -z[15] + -z[16] + -z[18] + z[20] + z[51];
z[20] = abb[4] * z[20];
z[22] = abb[30] + z[32];
z[22] = abb[33] * z[22];
z[18] = -z[18] + z[22] + -z[47] + z[58];
z[18] = abb[6] * z[18];
z[3] = z[3] + z[8];
z[3] = abb[9] * z[3];
z[8] = -abb[1] * z[12];
z[12] = z[57] + z[59];
z[22] = abb[34] * z[1];
z[32] = z[7] + z[12] + -z[22];
z[32] = abb[0] * z[32];
z[40] = -z[19] + z[44];
z[2] = -abb[29] + z[2];
z[2] = abb[32] * z[2];
z[41] = -z[2] + z[40];
z[44] = -abb[7] + abb[10];
z[44] = z[41] * z[44];
z[47] = abb[58] + -z[53];
z[47] = abb[13] * z[47];
z[8] = -z[3] + z[8] + z[18] + z[20] + z[32] + z[44] + -z[47] + -z[66];
z[8] = z[8] * z[25];
z[18] = abb[22] * z[42];
z[4] = z[4] + z[21] + -z[33];
z[4] = z[4] * z[35];
z[20] = abb[32] * z[55];
z[15] = -abb[35] + abb[37] + -abb[56] + -z[15] + z[20] + z[22] + -z[62];
z[15] = abb[23] * z[15];
z[20] = abb[27] * z[60];
z[21] = abb[25] * z[63];
z[22] = abb[22] + z[35];
z[22] = abb[35] * z[22];
z[25] = abb[28] * abb[59];
z[4] = z[4] + z[15] + z[18] + -z[20] + z[21] + -z[22] + (T(-1) / T(2)) * z[25];
z[15] = (T(1) / T(32)) * z[17];
z[4] = z[4] * z[15];
z[14] = z[14] * z[36];
z[15] = (T(1) / T(4)) * z[38];
z[16] = z[2] + -z[14] + -z[15] + (T(1) / T(4)) * z[16];
z[16] = abb[7] * z[16];
z[17] = z[34] + z[38] + -z[48];
z[18] = abb[4] * z[17];
z[16] = z[16] + (T(1) / T(4)) * z[18];
z[2] = -z[2] + z[45] + (T(1) / T(2)) * z[46];
z[2] = abb[15] * z[2];
z[2] = z[2] + -z[3] + z[16] + -z[52];
z[2] = z[2] * z[23];
z[3] = 3 * z[34] + z[48];
z[3] = (T(1) / T(4)) * z[3] + -z[15] + -z[50];
z[3] = abb[4] * z[3];
z[7] = -z[7] + z[40];
z[15] = abb[7] * z[7];
z[18] = -abb[4] + abb[9];
z[18] = abb[35] * z[18];
z[3] = z[3] + (T(-1) / T(2)) * z[15] + z[18] + -z[47] + -z[61];
z[3] = z[3] * z[30];
z[1] = z[1] * z[13];
z[1] = z[1] + -z[14] + z[19] + (T(-5) / T(12)) * z[38] + z[43];
z[1] = z[1] * z[28];
z[11] = abb[7] * z[11];
z[9] = z[9] + -z[38];
z[9] = abb[6] * z[9];
z[9] = z[9] + z[11] + -z[47] + -z[52];
z[9] = z[9] * z[24];
z[11] = -abb[33] + z[56];
z[11] = z[6] * z[11];
z[6] = -abb[32] + abb[29] * (T(3) / T(4)) + z[6];
z[6] = abb[34] * z[6];
z[6] = -z[6] + z[11] + -z[12];
z[11] = -abb[0] * z[6];
z[12] = abb[10] * z[41];
z[11] = z[11] + z[12] + z[16];
z[11] = z[11] * z[39];
z[6] = -abb[53] * z[6];
z[10] = z[10] * z[26];
z[12] = abb[55] * z[65];
z[6] = z[6] + z[10] + z[12];
z[6] = z[6] * z[27];
z[10] = abb[55] * z[17];
z[12] = -abb[59] * z[37];
z[10] = z[10] + z[12];
z[10] = abb[19] * z[10];
z[7] = z[7] * z[29];
z[12] = -abb[59] * z[31];
z[7] = z[7] + z[12];
z[7] = abb[55] * z[7];
z[1] = abb[43] + abb[44] + z[1] + z[2] + z[3] + z[4] + (T(1) / T(16)) * z[5] + z[6] + z[7] + z[8] + z[9] + (T(1) / T(64)) * z[10] + z[11];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_449_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("-0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("-0.120565460373201941664377630500544393119186441990413855185472737875"),stof<T>("0.008122919489162357404851275342099464906749082378350845516021086356")}, std::complex<T>{stof<T>("0.24262455055120729895431117267383898166367269247833256352496631353"),stof<T>("-0.14526319143004323559361219370897325063142983277059844657387255908")}, std::complex<T>{stof<T>("0.29820015762440756746539582520277103617337924209965664448267572203"),stof<T>("-0.86999491982690880885706643518656909805647868187499698232567502972")}, std::complex<T>{stof<T>("0.18777331240759861174116785172115519051324539620179252919599363809"),stof<T>("-0.42461434373862124815611804491320722074434539090298304420290379136")}, std::complex<T>{stof<T>("0.71139208474349293419636000264885361495551924473021304202603627221"),stof<T>("-0.90547547218371322812297658047063936132169816395277400480248082064")}, std::complex<T>{stof<T>("0.12999797950105475637272322004832218410277322408176031662063887682"),stof<T>("-0.41085942189242198338479599540315379086897984976985439939950975612")}, std::complex<T>{stof<T>("-0.0258587171738000399774921044042944740259148750644638551516258157"),stof<T>("-0.40174967964374244643557595952862298073520209320976199727113019872")}, std::complex<T>{stof<T>("0.17294983522561872023479601573924629543765893637965493551795324851"),stof<T>("-0.23224357088845982643188862976021287177099102762695359174229671081")}};
	
	std::vector<C> intdlogs = {rlog(k.W[6].real()/kbase.W[6].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[29].real()/kbase.W[29].real()), C{T{},(log_iphi_im(k.W[129]) - log_iphi_im(kbase.W[129]))}, C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[132]) - log_iphi_im(kbase.W[132]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[135]) - log_iphi_im(kbase.W[135]))}, C{T{},(log_iphi_im(k.W[186]) - log_iphi_im(kbase.W[186]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_449_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_449_W_16_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(128)) * (-v[3] + v[5]) * (4 + v[3] + v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(1) / T(32)) * (-v[3] + v[5])) / tend;


		return (abb[50] + abb[51] + abb[53]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[50] + abb[51] + abb[53];
z[1] = abb[30] + -abb[32];
return abb[11] * m1_set::bc<T>[0] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_449_W_16_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(512)) * (-v[3] + v[5]) * (4 * (-4 + -2 * v[0] + 2 * v[1] + v[3] + -v[5] + -m1_set::bc<T>[1] * (4 + v[3] + v[5])) + m1_set::bc<T>[2] * (12 * v[0] + -4 * v[1] + 3 * (8 + v[3] + 3 * v[5])))) / prod_pow(tend, 2);
c[1] = ((2 + 2 * m1_set::bc<T>[1] + -3 * m1_set::bc<T>[2]) * (T(-1) / T(64)) * (-v[3] + v[5])) / tend;


		return (abb[50] + abb[51] + abb[53]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[29] + -abb[30] + abb[32];
z[0] = abb[32] * z[0];
z[1] = abb[29] * abb[30];
z[0] = -abb[35] + abb[37] + abb[38] + z[0] + z[1];
z[1] = abb[50] + abb[51] + abb[53];
return abb[11] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_449_W_19_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = (m1_set::bc<T>[0] * (T(1) / T(128)) * (v[2] + v[3]) * (-4 + v[2] + v[3] + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[0] * (T(-1) / T(32)) * (v[2] + v[3])) / tend;


		return (abb[51] + abb[53] + abb[54]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = abb[51] + abb[53] + abb[54];
z[1] = abb[32] + -abb[33];
return abb[12] * m1_set::bc<T>[0] * (T(1) / T(16)) * z[0] * z[1];
}

}
template <typename T, typename TABB> T SpDLog_f_4_449_W_19_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(1) / T(128)) * (v[2] + v[3]) * (-4 * v[0] + v[2] + v[3] + -m1_set::bc<T>[1] * (-4 + v[2] + v[3] + -2 * v[5]) + -2 * v[5])) / prod_pow(tend, 2);
c[1] = (m1_set::bc<T>[1] * (T(1) / T(32)) * (v[2] + v[3])) / tend;


		return (abb[51] + abb[53] + abb[54]) * (t * c[0] + c[1]);
	}
	{
T z[2];
z[0] = -abb[29] + abb[32] * (T(1) / T(2));
z[0] = abb[32] * z[0];
z[1] = -abb[29] + abb[33] * (T(1) / T(2));
z[1] = abb[33] * z[1];
z[0] = z[0] + -z[1];
z[1] = -abb[51] + -abb[53] + -abb[54];
return abb[12] * (T(1) / T(16)) * z[0] * z[1];
}

}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_449_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-0.35831535424739108749052910012598616494601268057555492893757463245"),stof<T>("0.50909878581567110859496318142889521344901398312785653535431398459")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({15, 18});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,63> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W12(k,dl), dlog_W15(k,dl), dlog_W16(k,dl), dlog_W19(k,dl), dlog_W24(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W29(k,dl), dlog_W31(k,dl), dlog_W119(k,dv), dlog_W122(k,dv), dlog_W125(k,dv), dlog_W128(k,dv), dlog_W130(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W136(k,dv), dlog_W187(k,dv), f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_8(k), f_1_9(k), f_2_3(k), f_2_13(k), f_2_16(k), f_2_17(k), f_2_4_im(k), f_2_6_im(k), f_2_9_im(k), f_2_25_im(k), T{0}, T{0}, T{0}, rlog(kend.W[6].real()/k.W[6].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[29].real()/k.W[29].real()), (log_iphi_im(kend.W[129]) - log_iphi_im(k.W[129])), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[132]) - log_iphi_im(k.W[132])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[135]) - log_iphi_im(k.W[135])), (log_iphi_im(kend.W[186]) - log_iphi_im(k.W[186])), f_2_4_re(k), f_2_6_re(k), f_2_9_re(k), f_2_25_re(k), T{0}, T{0}, T{0}};
abb[43] = SpDLog_f_4_449_W_16_Im(t, path, abb);
abb[44] = SpDLog_f_4_449_W_19_Im(t, path, abb);
abb[45] = SpDLogQ_W_72(k,dl,dlr).imag();
abb[60] = SpDLog_f_4_449_W_16_Re(t, path, abb);
abb[61] = SpDLog_f_4_449_W_19_Re(t, path, abb);
abb[62] = SpDLogQ_W_72(k,dl,dlr).real();

                    
            return f_4_449_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_449_DLogXconstant_part(base_point<T>, kend);
	value += f_4_449_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_449_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_449_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_449_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_449_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_449_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_449_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
