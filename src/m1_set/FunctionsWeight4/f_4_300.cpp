/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_300.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_300_abbreviated (const std::array<T,26>& abb) {
T z[56];
z[0] = 3 * abb[11];
z[1] = 4 * abb[13];
z[2] = -abb[12] + z[0] + z[1];
z[3] = -abb[10] + z[2];
z[3] = abb[10] * z[3];
z[4] = 4 * abb[14];
z[5] = prod_pow(m1_set::bc<T>[0], 2);
z[6] = (T(13) / T(6)) * z[5];
z[7] = z[4] + -z[6];
z[8] = 2 * abb[11];
z[9] = abb[13] + z[8];
z[10] = abb[13] * z[9];
z[11] = abb[15] + z[10];
z[12] = prod_pow(abb[11], 2);
z[13] = -z[11] + -z[12];
z[14] = abb[11] * abb[12];
z[13] = z[3] + -z[7] + 2 * z[13] + z[14];
z[13] = abb[20] * z[13];
z[14] = 3 * abb[12];
z[15] = abb[10] + -abb[11];
z[16] = z[1] + -z[14] + -z[15];
z[16] = abb[10] * z[16];
z[17] = 3 * abb[13];
z[18] = z[8] + z[17];
z[19] = abb[13] * z[18];
z[20] = 2 * abb[15];
z[21] = -z[19] + -z[20];
z[0] = 8 * abb[13] + z[0];
z[22] = 2 * abb[12];
z[23] = z[0] + -z[22];
z[23] = abb[12] * z[23];
z[24] = 2 * abb[14];
z[16] = z[16] + 2 * z[21] + z[23] + -z[24];
z[16] = abb[22] * z[16];
z[21] = 2 * abb[10];
z[0] = z[0] + -z[14] + -z[21];
z[0] = abb[10] * z[0];
z[19] = -abb[15] + -z[19];
z[23] = -abb[11] + abb[12];
z[25] = z[1] + -z[23];
z[25] = abb[12] * z[25];
z[0] = z[0] + -z[4] + 2 * z[19] + z[25];
z[0] = abb[21] * z[0];
z[19] = 2 * abb[13];
z[25] = -z[19] + z[23];
z[26] = abb[10] + 2 * z[25];
z[26] = abb[10] * z[26];
z[27] = abb[11] + abb[13];
z[28] = z[1] * z[27];
z[29] = z[12] + z[20];
z[30] = abb[11] + z[19];
z[30] = -abb[12] + 2 * z[30];
z[30] = abb[12] * z[30];
z[26] = -z[26] + -z[28] + -z[29] + z[30];
z[7] = z[7] + -2 * z[26];
z[7] = abb[19] * z[7];
z[28] = abb[12] * z[25];
z[30] = z[20] + z[28];
z[31] = -abb[12] + z[15];
z[31] = abb[10] * z[31];
z[32] = prod_pow(abb[13], 2);
z[31] = -z[30] + z[31] + -z[32];
z[31] = abb[24] * z[31];
z[25] = abb[10] + z[25];
z[33] = abb[10] * z[25];
z[33] = z[24] + z[33];
z[34] = abb[12] * z[23];
z[34] = -z[32] + -z[33] + z[34];
z[34] = abb[25] * z[34];
z[35] = abb[12] * abb[13];
z[36] = abb[12] + -abb[13];
z[37] = abb[10] * z[36];
z[38] = -z[32] + z[35] + -z[37];
z[39] = abb[23] * z[38];
z[0] = z[0] + z[7] + z[13] + z[16] + -z[31] + -z[34] + 2 * z[39];
z[0] = abb[2] * z[0];
z[7] = 2 * z[12];
z[13] = -z[6] + z[7];
z[16] = z[4] + z[13];
z[39] = z[9] * z[19];
z[40] = -abb[11] + z[19] + -z[22];
z[40] = abb[12] * z[40];
z[3] = abb[15] + -z[3] + z[16] + z[39] + z[40];
z[3] = abb[22] * z[3];
z[39] = abb[12] + z[27];
z[39] = -abb[10] + 2 * z[39];
z[39] = abb[10] * z[39];
z[40] = abb[12] * z[19];
z[39] = -z[10] + -z[24] + z[39] + -z[40];
z[41] = -abb[21] * z[39];
z[42] = abb[11] + abb[12];
z[42] = abb[12] * z[42];
z[11] = -z[11] + -z[33] + z[42];
z[11] = abb[20] * z[11];
z[42] = 2 * z[27];
z[43] = -abb[10] + z[42];
z[43] = z[21] * z[43];
z[16] = -z[16] + z[43];
z[44] = -abb[12] + z[19];
z[44] = abb[12] * z[44];
z[45] = 4 * abb[11];
z[46] = abb[13] + z[45];
z[46] = abb[13] * z[46];
z[44] = z[44] + z[46];
z[47] = z[16] + -z[44];
z[47] = abb[19] * z[47];
z[28] = z[10] + z[20] + z[28];
z[48] = z[28] + z[33];
z[49] = abb[23] * z[48];
z[50] = -abb[10] + z[22];
z[51] = abb[10] * z[50];
z[52] = prod_pow(abb[12], 2);
z[51] = z[13] + z[51] + -z[52];
z[52] = abb[17] * z[51];
z[3] = z[3] + z[11] + z[31] + z[41] + z[47] + z[49] + z[52];
z[3] = abb[3] * z[3];
z[11] = z[21] * z[36];
z[31] = -abb[12] + z[42];
z[41] = abb[12] * z[31];
z[41] = z[10] + -z[11] + z[20] + -z[41];
z[42] = abb[5] * z[41];
z[31] = z[22] * z[31];
z[31] = 2 * z[29] + -z[31];
z[47] = abb[10] * (T(1) / T(2));
z[49] = -z[17] + z[47];
z[49] = abb[10] * z[49];
z[52] = abb[13] * (T(1) / T(2)) + -z[45];
z[52] = abb[13] * z[52];
z[49] = -abb[14] + z[6] + -z[31] + z[49] + z[52];
z[49] = abb[1] * z[49];
z[53] = abb[6] * z[48];
z[54] = abb[10] + -z[23];
z[54] = abb[10] * z[54];
z[28] = -abb[14] + -z[28] + z[54];
z[28] = abb[0] * z[28];
z[10] = z[10] + z[29];
z[2] = abb[12] * z[2];
z[2] = -z[2] + -z[6] + 2 * z[10];
z[10] = -abb[10] * z[23];
z[10] = -z[2] + z[10] + -z[24];
z[10] = abb[2] * z[10];
z[29] = (T(1) / T(2)) * z[32];
z[32] = abb[13] + -z[47];
z[32] = abb[10] * z[32];
z[32] = -z[29] + z[32];
z[32] = abb[4] * z[32];
z[47] = abb[8] + -abb[9];
z[54] = 2 * abb[7] + z[47];
z[54] = abb[16] * z[54];
z[10] = z[10] + z[28] + 3 * z[32] + z[42] + z[49] + z[53] + z[54];
z[10] = abb[17] * z[10];
z[28] = 9 * abb[13];
z[32] = abb[12] * (T(5) / T(2)) + -z[8] + -z[28];
z[32] = abb[12] * z[32];
z[42] = abb[13] * (T(13) / T(2)) + z[8];
z[42] = abb[13] * z[42];
z[11] = z[11] + z[20] + z[32] + z[42];
z[11] = abb[5] * z[11];
z[20] = abb[13] * (T(11) / T(2)) + z[45];
z[20] = z[17] * z[20];
z[32] = z[17] + -z[22];
z[49] = abb[11] + z[32];
z[49] = -abb[10] + 2 * z[49];
z[49] = z[21] * z[49];
z[54] = 8 * abb[11] + 19 * abb[13];
z[55] = abb[12] * (T(-9) / T(2)) + z[54];
z[55] = abb[12] * z[55];
z[13] = -6 * abb[14] + -9 * abb[15] + -z[13] + -z[20] + z[49] + z[55];
z[13] = abb[1] * z[13];
z[49] = 4 * z[38];
z[55] = -abb[4] * z[49];
z[11] = z[11] + z[13] + z[53] + z[55];
z[11] = abb[22] * z[11];
z[13] = abb[11] + z[17];
z[13] = -abb[12] + 2 * z[13];
z[13] = z[13] * z[22];
z[12] = -3 * abb[15] + -z[12];
z[54] = -8 * abb[12] + abb[10] * (T(-9) / T(2)) + z[54];
z[54] = abb[10] * z[54];
z[12] = -9 * abb[14] + z[6] + 2 * z[12] + z[13] + -z[20] + z[54];
z[12] = abb[1] * z[12];
z[13] = -z[8] + z[22];
z[20] = abb[10] * (T(5) / T(2)) + z[13] + -z[28];
z[20] = abb[10] * z[20];
z[20] = z[20] + z[24] + -z[40] + z[42];
z[20] = abb[4] * z[20];
z[22] = -abb[5] * z[49];
z[12] = z[12] + z[20] + z[22] + z[53];
z[12] = abb[21] * z[12];
z[20] = abb[22] * z[41];
z[22] = z[19] + -z[21] + z[23];
z[22] = abb[10] * z[22];
z[2] = abb[14] + z[2] + z[22];
z[2] = abb[21] * z[2];
z[22] = abb[20] * z[51];
z[2] = z[2] + z[20] + z[22] + z[34];
z[2] = abb[0] * z[2];
z[20] = 5 * abb[13];
z[22] = -z[13] + z[20];
z[22] = -3 * abb[10] + 2 * z[22];
z[22] = abb[10] * z[22];
z[28] = 7 * abb[13];
z[34] = z[28] + z[45];
z[34] = abb[13] * z[34];
z[1] = abb[12] * z[1];
z[1] = z[1] + -z[4] + z[22] + -z[34];
z[1] = abb[4] * z[1];
z[4] = (T(13) / T(3)) * z[5];
z[5] = 8 * abb[14] + -z[4];
z[22] = -z[5] + 4 * z[26];
z[26] = abb[1] * z[22];
z[20] = z[8] + z[20];
z[14] = -z[14] + 2 * z[20];
z[14] = abb[12] * z[14];
z[20] = abb[15] + z[37];
z[14] = z[14] + -4 * z[20] + -z[34];
z[14] = abb[5] * z[14];
z[1] = z[1] + z[14] + -z[26];
z[14] = -abb[2] * z[22];
z[19] = -abb[10] + z[19];
z[19] = abb[10] * z[19];
z[19] = z[19] + z[31] + z[46];
z[4] = z[4] + -2 * z[19];
z[4] = abb[0] * z[4];
z[7] = -z[7] + z[43] + -z[44];
z[5] = -z[5] + 2 * z[7];
z[5] = abb[3] * z[5];
z[4] = 2 * z[1] + z[4] + z[5] + z[14];
z[4] = abb[18] * z[4];
z[5] = -abb[4] * z[39];
z[7] = abb[12] * (T(1) / T(2));
z[14] = z[7] + -z[17];
z[14] = abb[12] * z[14];
z[14] = -abb[15] + z[14] + z[16] + z[52];
z[14] = abb[1] * z[14];
z[7] = abb[13] + -z[7];
z[7] = abb[12] * z[7];
z[7] = z[7] + -z[29];
z[7] = abb[5] * z[7];
z[5] = z[5] + 3 * z[7] + z[14] + z[53];
z[5] = abb[20] * z[5];
z[6] = z[6] + -z[19];
z[6] = abb[0] * z[6];
z[1] = z[1] + z[6];
z[1] = abb[19] * z[1];
z[6] = -abb[20] + -abb[21];
z[7] = abb[8] + abb[9];
z[6] = z[6] * z[7];
z[7] = abb[24] + abb[25];
z[7] = abb[9] * z[7];
z[14] = abb[24] + -abb[25];
z[14] = abb[8] * z[14];
z[16] = abb[22] * z[47];
z[19] = abb[20] + abb[22] + abb[23] + -abb[24] + -2 * abb[25];
z[19] = abb[7] * z[19];
z[6] = z[6] + z[7] + z[14] + z[16] + z[19];
z[6] = abb[16] * z[6];
z[7] = abb[13] * z[27];
z[14] = abb[15] + z[7];
z[16] = z[14] + z[33] + -z[35];
z[16] = abb[1] * z[16];
z[19] = abb[11] * abb[13];
z[20] = -abb[13] + z[23];
z[22] = abb[12] * z[20];
z[22] = abb[15] + z[19] + z[22];
z[22] = abb[5] * z[22];
z[16] = z[16] + z[22];
z[16] = 2 * z[16] + -z[53];
z[16] = abb[25] * z[16];
z[7] = abb[14] + z[7] + z[30] + z[37];
z[7] = abb[1] * z[7];
z[22] = -abb[10] + z[27];
z[26] = -abb[10] * z[22];
z[19] = abb[14] + z[19] + z[26];
z[19] = abb[4] * z[19];
z[7] = z[7] + z[19];
z[7] = 2 * z[7] + -z[53];
z[7] = abb[24] * z[7];
z[19] = z[17] + -z[23];
z[19] = abb[12] * z[19];
z[15] = -z[15] + z[32];
z[15] = abb[10] * z[15];
z[14] = -2 * z[14] + z[15] + z[19] + -z[24];
z[14] = abb[1] * z[14];
z[15] = -abb[4] + -abb[5];
z[15] = z[15] * z[38];
z[14] = z[14] + z[15];
z[15] = abb[0] * z[48];
z[14] = 2 * z[14] + z[15];
z[14] = abb[23] * z[14];
z[0] = z[0] + z[1] + z[2] + z[3] + z[4] + z[5] + z[6] + z[7] + z[10] + z[11] + z[12] + z[14] + z[16];
z[1] = 2 * m1_set::bc<T>[0];
z[2] = abb[1] * z[1] * z[25];
z[3] = m1_set::bc<T>[0] * z[36];
z[4] = abb[5] * z[3];
z[2] = z[2] + -z[4];
z[4] = abb[0] * z[20];
z[5] = abb[10] + -abb[13];
z[6] = abb[4] * z[5];
z[4] = z[4] + z[6];
z[7] = -m1_set::bc<T>[0] * z[4];
z[7] = z[2] + z[7];
z[7] = abb[19] * z[7];
z[10] = abb[19] * z[25];
z[11] = abb[20] * z[22];
z[10] = z[10] + z[11];
z[10] = m1_set::bc<T>[0] * z[10];
z[11] = abb[22] * z[3];
z[12] = m1_set::bc<T>[0] * z[5];
z[14] = -abb[21] * z[12];
z[10] = z[10] + -z[11] + z[14];
z[10] = abb[2] * z[10];
z[7] = z[7] + z[10];
z[6] = z[1] * z[6];
z[10] = abb[12] + z[9] + -z[21];
z[14] = abb[1] * m1_set::bc<T>[0];
z[10] = z[10] * z[14];
z[10] = z[6] + z[10];
z[10] = abb[20] * z[10];
z[15] = -5 * abb[12] + z[8] + -z[21] + z[28];
z[15] = z[14] * z[15];
z[16] = abb[5] * z[36];
z[19] = z[1] * z[16];
z[15] = z[15] + z[19];
z[15] = abb[22] * z[15];
z[19] = -5 * abb[10] + -z[13] + z[28];
z[19] = z[14] * z[19];
z[6] = z[6] + z[19];
z[6] = abb[21] * z[6];
z[19] = -abb[11] * abb[20] * m1_set::bc<T>[0];
z[11] = z[11] + z[19];
z[13] = abb[10] + z[13] + -z[17];
z[13] = abb[21] * m1_set::bc<T>[0] * z[13];
z[11] = 2 * z[11] + z[13];
z[11] = abb[0] * z[11];
z[13] = -abb[2] * z[20];
z[13] = z[13] + z[16];
z[13] = z[1] * z[13];
z[9] = z[9] + -z[50];
z[9] = z[9] * z[14];
z[12] = -abb[0] * z[12];
z[9] = z[9] + z[12] + z[13];
z[9] = abb[17] * z[9];
z[12] = abb[12] + -z[18] + z[21];
z[12] = abb[22] * z[12];
z[8] = -abb[17] * z[8];
z[8] = z[8] + z[12];
z[8] = m1_set::bc<T>[0] * z[8];
z[5] = abb[21] * z[5];
z[12] = abb[19] * z[22];
z[5] = z[5] + z[12];
z[1] = z[1] * z[5];
z[3] = -abb[20] * z[3];
z[1] = z[1] + z[3] + z[8];
z[1] = abb[3] * z[1];
z[3] = abb[2] * z[25];
z[5] = abb[3] * z[22];
z[3] = z[3] + -z[4] + z[5];
z[3] = m1_set::bc<T>[0] * z[3];
z[2] = z[2] + z[3];
z[2] = abb[18] * z[2];
z[1] = z[1] + 4 * z[2] + z[6] + 2 * z[7] + z[9] + z[10] + z[11] + z[15];
z[1] = 2 * z[1];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_300_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("-1.816834929564168158427810420531668671780929344028350772048591738"),stof<T>("-20.878109371112254113855084554735812438630595734177313375413754034")}, std::complex<T>{stof<T>("-21.511752154288725683520688729444056897913795541457427047424159027"),stof<T>("59.371417210407179050236294838840064522148092302150362199120543438")}, std::complex<T>{stof<T>("-10.755876077144362841760344364722028448956897770728713523712079513"),stof<T>("29.685708605203589525118147419420032261074046151075181099560271719")}, std::complex<T>{stof<T>("24.77138491119728902465567607695734832980912783255536302450460652"),stof<T>("-25.977092870063856768177941126952133954772089921561968364493025535")}, std::complex<T>{stof<T>("12.809891092750555378750580714805888324787023682277694251763276753"),stof<T>("-8.807599234091335411263062864684219822443450416897867724146517685")}, std::complex<T>{stof<T>("-13.778328748010901804332905782683128676803033494306019544789921506"),stof<T>("-3.708615735139732756940206292467898306301956229513212735067246185")}, stof<T>("-1.6635215418508366353143158232196869031947321234279417324702487368"), stof<T>("8.1467639364098125700497265545092086142014652425291793387481963645"), stof<T>("-6.7204224806010003132978366608417129150559296866218675622805531285")};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[2].real()/kbase.W[2].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[12].real()/kbase.W[12].real()), rlog(k.W[17].real()/kbase.W[17].real()), rlog(k.W[29].real()/kbase.W[29].real()), rlog(k.W[36].real()/kbase.W[36].real()), rlog(k.W[55].real()/kbase.W[55].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_300_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_300_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("21.690856396483965018774020299156709664029418871861380699989521926"),stof<T>("-5.255069343308453279319214124796160092351450571050644955352273775")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,26> abb = {dl[0], dlog_W3(k,dl), dl[2], dl[4], dlog_W13(k,dl), dlog_W18(k,dl), dlog_W30(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), f_1_1(k), f_1_5(k), f_1_6(k), f_1_10(k), f_2_1(k), f_2_8(k), f_2_23(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[2].real()/k.W[2].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[12].real()/k.W[12].real()), rlog(kend.W[17].real()/k.W[17].real()), rlog(kend.W[29].real()/k.W[29].real()), rlog(kend.W[36].real()/k.W[36].real()), rlog(kend.W[55].real()/k.W[55].real())};

                    
            return f_4_300_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_300_DLogXconstant_part(base_point<T>, kend);
	value += f_4_300_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_300_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_300_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_300_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_300_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_300_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_300_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
