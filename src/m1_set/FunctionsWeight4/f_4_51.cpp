/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_51.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_51_abbreviated (const std::array<T,58>& abb) {
T z[96];
z[0] = abb[40] * (T(25) / T(12));
z[1] = abb[41] * (T(1) / T(3));
z[2] = abb[47] * (T(1) / T(4));
z[3] = 11 * abb[42] + abb[44];
z[3] = -2 * abb[48] + abb[43] * (T(5) / T(12)) + z[0] + z[1] + -z[2] + (T(1) / T(6)) * z[3];
z[3] = abb[8] * z[3];
z[4] = abb[43] + abb[47];
z[5] = abb[42] * (T(1) / T(2));
z[6] = abb[44] + z[5];
z[7] = abb[45] + abb[46];
z[4] = -abb[48] + abb[41] * (T(2) / T(3)) + (T(1) / T(6)) * z[4] + (T(1) / T(3)) * z[6] + (T(1) / T(2)) * z[7];
z[4] = abb[2] * z[4];
z[8] = abb[48] * (T(3) / T(2));
z[6] = -z[6] + z[8];
z[9] = abb[47] * (T(1) / T(2));
z[10] = abb[43] * (T(1) / T(2));
z[11] = z[9] + z[10];
z[12] = z[6] + -z[11];
z[13] = -abb[41] + (T(1) / T(2)) * z[12];
z[14] = -abb[14] * z[13];
z[15] = abb[48] * (T(1) / T(4));
z[16] = 7 * abb[44] + abb[42] * (T(-25) / T(4));
z[0] = abb[45] * (T(-13) / T(2)) + abb[47] * (T(7) / T(3)) + abb[41] * (T(14) / T(3)) + z[0] + -z[15] + (T(1) / T(3)) * z[16];
z[0] = abb[0] * z[0];
z[16] = 4 * abb[44] + abb[42] * (T(35) / T(4));
z[15] = abb[40] * (T(-11) / T(6)) + abb[46] * (T(-9) / T(2)) + abb[41] * (T(8) / T(3)) + abb[43] * (T(13) / T(12)) + z[2] + z[15] + (T(1) / T(3)) * z[16];
z[15] = abb[3] * z[15];
z[16] = 3 * abb[46];
z[17] = abb[48] * (T(1) / T(2));
z[18] = abb[44] + abb[42] * (T(13) / T(2));
z[18] = abb[47] * (T(-5) / T(6)) + abb[43] * (T(7) / T(6)) + -z[16] + z[17] + (T(1) / T(3)) * z[18];
z[1] = z[1] + (T(1) / T(2)) * z[18];
z[1] = abb[6] * z[1];
z[18] = abb[51] + abb[52] + abb[53] + -abb[54];
z[19] = abb[22] * z[18];
z[20] = abb[20] * z[18];
z[21] = z[19] + z[20];
z[22] = abb[21] * z[18];
z[23] = z[21] + z[22];
z[24] = abb[23] * z[18];
z[25] = z[23] + -z[24];
z[26] = abb[19] * z[18];
z[27] = z[25] + z[26];
z[28] = abb[49] + abb[50];
z[29] = abb[15] * z[28];
z[30] = z[27] + -z[29];
z[31] = -abb[42] + abb[48];
z[32] = -abb[43] + abb[46] + z[31];
z[33] = 2 * abb[41] + abb[44];
z[34] = z[32] + -z[33];
z[34] = abb[10] * z[34];
z[35] = z[9] + -z[10];
z[36] = abb[42] + abb[48];
z[36] = -abb[46] + -z[35] + (T(1) / T(2)) * z[36];
z[37] = abb[4] * z[36];
z[38] = -abb[45] + abb[47] + -abb[48];
z[39] = z[33] + z[38];
z[39] = abb[9] * z[39];
z[40] = abb[43] + -abb[47];
z[41] = abb[40] + z[40];
z[42] = abb[1] * z[41];
z[0] = z[0] + z[1] + z[3] + z[4] + z[14] + z[15] + (T(1) / T(4)) * z[30] + 2 * z[34] + (T(1) / T(2)) * z[37] + -3 * z[39] + (T(7) / T(3)) * z[42];
z[1] = prod_pow(m1_set::bc<T>[0], 2);
z[0] = z[0] * z[1];
z[3] = z[5] + z[33];
z[4] = z[8] + -z[10];
z[14] = -z[3] + z[4] + -z[9];
z[15] = abb[14] * z[14];
z[30] = abb[8] * z[14];
z[43] = 2 * abb[44];
z[44] = 4 * abb[41] + z[43];
z[45] = 3 * abb[48];
z[46] = z[44] + -z[45];
z[47] = abb[42] + abb[47];
z[48] = abb[43] + z[46] + z[47];
z[49] = abb[2] * z[48];
z[30] = z[30] + -z[49];
z[15] = z[15] + -z[30];
z[17] = z[17] + -z[33];
z[50] = abb[43] * (T(3) / T(2));
z[51] = -z[9] + -z[17] + z[50];
z[52] = -2 * abb[46] + abb[42] * (T(3) / T(2)) + z[51];
z[53] = abb[3] + abb[6];
z[52] = z[52] * z[53];
z[27] = z[15] + (T(-1) / T(2)) * z[27] + z[34] + -z[37] + z[52];
z[52] = -abb[57] * z[27];
z[54] = -abb[45] + (T(1) / T(2)) * z[31];
z[55] = z[35] + z[54];
z[56] = abb[12] * z[55];
z[55] = abb[5] * z[55];
z[56] = -z[55] + z[56];
z[57] = abb[3] * z[41];
z[58] = abb[13] * z[41];
z[59] = z[57] + -z[58];
z[59] = -z[42] + (T(-1) / T(2)) * z[59];
z[60] = abb[40] * (T(1) / T(2));
z[54] = -z[54] + -z[60];
z[54] = abb[0] * z[54];
z[61] = (T(1) / T(2)) * z[28];
z[62] = -abb[17] * z[61];
z[63] = (T(1) / T(2)) * z[19];
z[54] = z[54] + z[56] + -z[59] + z[62] + z[63];
z[54] = abb[32] * z[54];
z[51] = -z[5] + z[51];
z[51] = abb[6] * z[51];
z[62] = (T(1) / T(2)) * z[23] + -z[39];
z[64] = z[31] + z[40];
z[65] = abb[7] * z[64];
z[66] = z[62] + (T(1) / T(2)) * z[65];
z[67] = abb[47] * (T(3) / T(2));
z[10] = -z[10] + z[67];
z[5] = 2 * abb[45] + z[5] + -z[10] + z[17];
z[5] = abb[12] * z[5];
z[5] = z[5] + z[55];
z[17] = abb[8] * (T(1) / T(2));
z[68] = z[17] * z[64];
z[51] = z[5] + z[51] + -z[66] + z[68];
z[68] = -abb[56] * z[51];
z[69] = z[20] + z[22];
z[70] = -z[65] + z[69];
z[64] = abb[6] * z[64];
z[64] = z[64] + z[70];
z[71] = abb[15] + abb[17];
z[72] = -z[61] * z[71];
z[73] = z[17] * z[41];
z[59] = -z[59] + (T(-1) / T(2)) * z[64] + z[72] + -z[73];
z[59] = abb[33] * z[59];
z[64] = (T(1) / T(2)) * z[21];
z[36] = -z[36] * z[53];
z[7] = abb[48] + -z[7];
z[7] = abb[2] * z[7];
z[7] = z[7] + z[36] + z[37] + -z[56] + -z[64];
z[7] = abb[34] * z[7];
z[36] = z[22] + z[24];
z[56] = (T(1) / T(2)) * z[36];
z[5] = z[5] + -z[15] + z[39] + -z[56];
z[15] = abb[55] * z[5];
z[12] = abb[18] * z[12];
z[41] = -z[41] * z[71];
z[12] = z[12] + (T(1) / T(2)) * z[41];
z[41] = abb[0] + abb[8];
z[41] = abb[3] + -2 * abb[25] + abb[13] * (T(1) / T(4)) + (T(3) / T(4)) * z[41];
z[41] = -z[28] * z[41];
z[72] = -abb[18] * abb[41];
z[12] = (T(1) / T(2)) * z[12] + z[41] + z[72];
z[12] = abb[36] * z[12];
z[41] = -abb[32] + abb[33];
z[72] = -abb[34] + -z[41];
z[72] = z[26] * z[72];
z[74] = -prod_pow(abb[27], 2);
z[1] = -z[1] + z[74];
z[74] = abb[45] + z[40];
z[74] = abb[11] * z[74];
z[1] = z[1] * z[74];
z[1] = z[1] + z[7] + z[12] + z[15] + z[52] + z[54] + z[59] + z[68] + (T(1) / T(2)) * z[72];
z[7] = abb[2] * z[14];
z[12] = abb[6] * z[13];
z[7] = z[7] + z[12] + -z[42];
z[12] = abb[42] * (T(5) / T(2)) + -z[8];
z[15] = -abb[44] + z[12];
z[52] = 3 * abb[45];
z[54] = abb[41] + abb[40] * (T(5) / T(4)) + z[9] + (T(-1) / T(2)) * z[15] + -z[52];
z[54] = abb[0] * z[54];
z[59] = 3 * abb[14];
z[68] = z[13] * z[59];
z[72] = (T(-3) / T(4)) * z[28];
z[75] = -abb[15] + abb[17];
z[72] = z[72] * z[75];
z[75] = (T(3) / T(2)) * z[55];
z[76] = -z[20] + z[24];
z[38] = abb[44] + z[38];
z[38] = abb[41] + (T(1) / T(2)) * z[38];
z[38] = abb[9] * z[38];
z[76] = z[38] + (T(1) / T(4)) * z[76];
z[77] = abb[43] + z[60];
z[78] = -z[6] + z[77];
z[78] = abb[41] + (T(1) / T(2)) * z[78];
z[78] = abb[8] * z[78];
z[79] = z[3] + -z[8];
z[77] = z[77] + z[79];
z[77] = abb[3] * z[77];
z[80] = (T(3) / T(4)) * z[58];
z[72] = -z[7] + -z[54] + z[68] + z[72] + -z[75] + 3 * z[76] + z[77] + z[78] + -z[80];
z[76] = prod_pow(abb[26], 2);
z[72] = z[72] * z[76];
z[77] = abb[40] * (T(1) / T(4));
z[78] = abb[43] * (T(3) / T(4));
z[2] = z[2] + z[77] + z[78] + z[79];
z[2] = abb[8] * z[2];
z[81] = -abb[41] + (T(1) / T(2)) * z[6];
z[82] = abb[47] * (T(5) / T(4));
z[78] = abb[40] + z[78] + z[81] + -z[82];
z[78] = abb[3] * z[78];
z[83] = (T(3) / T(4)) * z[26];
z[84] = -z[68] + z[83];
z[85] = -z[49] + z[84];
z[60] = -abb[47] + z[60];
z[86] = z[6] + z[60];
z[86] = -abb[41] + (T(1) / T(2)) * z[86];
z[86] = abb[0] * z[86];
z[87] = (T(3) / T(4)) * z[71];
z[87] = z[28] * z[87];
z[80] = z[80] + z[87];
z[87] = 2 * z[42];
z[88] = z[80] + z[86] + -z[87];
z[89] = abb[12] * z[14];
z[19] = -z[19] + z[36];
z[2] = -z[2] + (T(-3) / T(4)) * z[19] + z[78] + z[85] + -z[88] + z[89];
z[2] = abb[26] * z[2];
z[19] = (T(1) / T(2)) * z[24];
z[20] = -z[19] + z[20] + (T(1) / T(2)) * z[22] + z[63];
z[36] = abb[43] * (T(1) / T(4));
z[78] = abb[47] * (T(3) / T(4));
z[77] = -z[36] + z[77] + -z[78] + -z[79];
z[77] = abb[8] * z[77];
z[90] = abb[6] * z[14];
z[81] = -abb[40] + abb[43] * (T(-5) / T(4)) + z[78] + z[81];
z[81] = abb[3] * z[81];
z[20] = (T(3) / T(2)) * z[20] + z[77] + z[81] + z[85] + z[88] + z[90];
z[20] = abb[30] * z[20];
z[77] = -abb[40] + z[10];
z[6] = -z[6] + z[77];
z[6] = abb[41] + (T(1) / T(2)) * z[6];
z[81] = -abb[3] * z[6];
z[10] = -abb[42] + abb[44] + abb[40] * (T(-5) / T(2)) + z[10];
z[10] = abb[41] + (T(1) / T(2)) * z[10];
z[10] = abb[8] * z[10];
z[10] = -z[7] + z[10] + (T(-3) / T(4)) * z[70] + -z[80] + z[81] + -z[83] + z[86];
z[10] = abb[31] * z[10];
z[70] = -z[49] + (T(3) / T(2)) * z[65];
z[81] = abb[3] * z[14];
z[85] = 2 * abb[43];
z[86] = z[33] + z[85];
z[47] = -z[47] + z[86];
z[88] = 2 * abb[6];
z[91] = abb[8] + z[88];
z[47] = z[47] * z[91];
z[26] = (T(3) / T(2)) * z[26];
z[47] = z[26] + z[47] + -z[70] + z[81] + z[89];
z[47] = abb[27] * z[47];
z[10] = z[2] + z[10] + z[20] + z[47];
z[10] = abb[31] * z[10];
z[20] = z[16] + z[52];
z[3] = z[3] + z[8] + z[11] + -z[20];
z[3] = abb[2] * z[3];
z[11] = abb[42] + -z[33] + z[52];
z[47] = 2 * abb[47];
z[52] = abb[43] + z[11] + -z[47];
z[91] = 2 * abb[12];
z[52] = z[52] * z[91];
z[23] = (T(-1) / T(4)) * z[23] + z[38];
z[91] = abb[43] * (T(-7) / T(2)) + abb[47] * (T(5) / T(2));
z[15] = -z[15] + -z[91];
z[15] = abb[41] + (T(1) / T(2)) * z[15];
z[15] = abb[8] * z[15];
z[92] = abb[42] * (T(7) / T(2));
z[8] = z[8] + z[92];
z[93] = abb[44] + z[8];
z[93] = abb[41] + -z[16] + (T(1) / T(2)) * z[93];
z[82] = abb[43] * (T(-7) / T(4)) + z[82] + -z[93];
z[82] = abb[3] * z[82];
z[94] = abb[47] * (T(7) / T(2));
z[95] = 5 * abb[44] + abb[43] * (T(17) / T(2)) + -z[8] + -z[94];
z[95] = 5 * abb[41] + (T(1) / T(2)) * z[95];
z[95] = abb[6] * z[95];
z[3] = z[3] + z[15] + 3 * z[23] + z[52] + z[75] + z[82] + -z[83] + z[95];
z[3] = abb[27] * z[3];
z[15] = z[26] + z[90];
z[23] = z[15] + z[81];
z[55] = 3 * z[55];
z[52] = -z[23] + z[30] + z[52] + z[55] + -3 * z[62];
z[62] = abb[26] * z[52];
z[3] = z[3] + z[62];
z[3] = abb[27] * z[3];
z[62] = 2 * abb[40];
z[36] = z[36] + z[62] + -z[78] + -z[93];
z[36] = abb[3] * z[36];
z[32] = -abb[44] + z[32];
z[32] = -abb[41] + (T(1) / T(2)) * z[32];
z[78] = 3 * abb[10];
z[32] = z[32] * z[78];
z[32] = z[32] + (T(3) / T(2)) * z[37];
z[6] = abb[8] * z[6];
z[60] = z[60] + -z[79];
z[60] = abb[0] * z[60];
z[6] = z[6] + -z[7] + (T(-3) / T(4)) * z[25] + (T(-3) / T(2)) * z[29] + -z[32] + z[36] + -z[60] + -z[84];
z[6] = abb[30] * z[6];
z[7] = 2 * abb[42];
z[16] = -abb[47] + z[7] + -z[16] + z[86];
z[36] = -2 * abb[3] + -z[88];
z[16] = z[16] * z[36];
z[36] = 3 * z[37];
z[34] = 3 * z[34];
z[37] = -z[34] + z[36];
z[16] = z[16] + (T(3) / T(2)) * z[22] + z[30] + z[37] + -z[89];
z[16] = abb[27] * z[16];
z[2] = -z[2] + z[6] + z[16];
z[2] = abb[30] * z[2];
z[6] = 6 * abb[46] + -z[33];
z[8] = z[6] + -z[8] + z[91];
z[8] = -z[8] * z[53];
z[22] = -z[39] + z[64];
z[12] = -6 * abb[45] + abb[43] * (T(-5) / T(2)) + -z[12] + z[33] + z[94];
z[12] = abb[12] * z[12];
z[30] = 2 * z[49];
z[48] = abb[8] * z[48];
z[8] = z[8] + z[12] + 3 * z[22] + z[26] + z[30] + -z[37] + z[48] + -z[55];
z[8] = abb[27] * z[8];
z[12] = -z[23] + z[48] + -z[89];
z[22] = -z[45] + z[47];
z[23] = abb[42] + z[43];
z[20] = 8 * abb[41] + -z[20] + z[22] + 2 * z[23] + z[85];
z[20] = abb[2] * z[20];
z[19] = z[19] + -z[21];
z[19] = (T(1) / T(2)) * z[19] + -z[38];
z[19] = z[12] + 3 * z[19] + z[20] + z[32] + z[68] + z[75];
z[19] = abb[29] * z[19];
z[14] = z[14] * z[59];
z[20] = -z[21] + z[24];
z[20] = z[14] + (T(3) / T(2)) * z[20];
z[12] = z[12] + z[20] + z[30];
z[21] = -abb[30] + abb[31];
z[23] = -abb[26] + z[21];
z[23] = z[12] * z[23];
z[8] = z[8] + z[19] + z[23];
z[8] = abb[29] * z[8];
z[19] = -z[63] + -z[69];
z[19] = (T(1) / T(2)) * z[19] + z[38] + (T(-1) / T(4)) * z[65] + z[74];
z[23] = 2 * z[57] + z[89] + -z[90];
z[24] = -5 * abb[40] + 3 * z[31];
z[24] = (T(1) / T(2)) * z[24] + -z[40];
z[17] = z[17] * z[24];
z[17] = z[17] + 3 * z[19] + z[23] + z[42] + -z[54] + z[75] + -z[80];
z[17] = abb[28] * z[17];
z[19] = z[58] + z[69];
z[24] = (T(3) / T(2)) * z[71];
z[24] = z[24] * z[28];
z[19] = (T(3) / T(2)) * z[19] + -z[23] + z[24] + -4 * z[42] + z[60] + z[73];
z[21] = abb[26] + z[21];
z[19] = z[19] * z[21];
z[21] = abb[27] * z[51];
z[17] = z[17] + z[19] + -3 * z[21];
z[17] = abb[28] * z[17];
z[19] = abb[35] * z[52];
z[21] = -prod_pow(abb[31], 2);
z[21] = z[21] + -z[41] + z[76];
z[21] = z[21] * z[61];
z[13] = abb[36] * z[13];
z[13] = z[13] + z[21];
z[21] = 3 * abb[16];
z[13] = z[13] * z[21];
z[18] = abb[24] * abb[36] * z[18];
z[0] = z[0] + 3 * z[1] + z[2] + z[3] + z[8] + z[10] + z[13] + z[17] + (T(3) / T(4)) * z[18] + z[19] + z[72];
z[1] = -4 * abb[40] + z[4] + -z[6] + z[67] + z[92];
z[1] = abb[3] * z[1];
z[2] = z[14] + z[49];
z[3] = -z[77] + -z[79];
z[3] = abb[8] * z[3];
z[4] = -abb[40] + abb[42] + z[22] + z[44];
z[6] = abb[0] * z[4];
z[6] = z[6] + z[87];
z[8] = 3 * z[29];
z[1] = z[1] + -z[2] + z[3] + -z[6] + z[8] + z[15] + (T(3) / T(2)) * z[25] + z[34] + z[36];
z[1] = abb[30] * z[1];
z[3] = -z[39] + -z[56];
z[9] = -abb[40] + z[9] + -z[50] + -z[79];
z[9] = abb[8] * z[9];
z[10] = -abb[40] + -abb[47] + z[11];
z[11] = 2 * abb[0];
z[10] = z[10] * z[11];
z[10] = z[10] + -z[87];
z[4] = -abb[3] * z[4];
z[2] = -z[2] + 3 * z[3] + z[4] + -z[8] + z[9] + -z[10] + z[55] + z[89];
z[2] = abb[26] * z[2];
z[3] = abb[43] + z[7] + z[33] + -z[45] + z[62];
z[3] = abb[8] * z[3];
z[4] = abb[40] + abb[42] + z[46] + z[85];
z[4] = abb[3] * z[4];
z[3] = z[3] + z[4] + z[6] + z[20] + -z[70] + -z[90];
z[3] = abb[31] * z[3];
z[4] = (T(-3) / T(2)) * z[31] + -z[35] + z[62];
z[4] = abb[8] * z[4];
z[4] = z[4] + -z[10] + -z[23] + -z[55] + 3 * z[66] + -6 * z[74];
z[4] = abb[28] * z[4];
z[6] = abb[29] * z[12];
z[7] = -abb[26] + abb[31];
z[7] = z[7] * z[21] * z[28];
z[1] = z[1] + z[2] + z[3] + z[4] + z[6] + z[7] + -z[16];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = abb[39] * z[27];
z[3] = abb[37] * z[5];
z[4] = -abb[38] * z[51];
z[2] = -z[2] + z[3] + z[4];
z[1] = z[1] + 3 * z[2];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_51_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("33.271302908052661481167078569208983297177814471470482193923039048"),stof<T>("-36.458798005758148808683659217590063869288248014628839802993076557")}, std::complex<T>{stof<T>("-11.284444247253300634450668680231391920085313346835503668065877295"),stof<T>("-93.978475082956705979382675503518827943607543947852490139337101957")}, std::complex<T>{stof<T>("25.996096719050313163028523272983761939281517389342543909164783863"),stof<T>("1.909467438863238212162253636809716512380364447035697400795706822")}, std::complex<T>{stof<T>("-0.411189830911843253803548841941122335239105254802223610923303082"),stof<T>("-28.517266376639371267559883910526324911154307779614351608082408499")}, std::complex<T>{stof<T>("-5.642222123626650317225334340115695960042656673417751834032938648"),stof<T>("-46.989237541478352989691337751759413971803771973926245069668550978")}, std::complex<T>{stof<T>("-20.592022310759988479797910568414920119275337539329025035832629111"),stof<T>("93.334000971454399545748376888366367415465522573584484962992860093")}, std::complex<T>{stof<T>("-18.547848414472447225081140770364272386182591875816614974184009492"),stof<T>("36.493764361994030802771010192733497973147445917608054297617934235")}, std::complex<T>{stof<T>("-5.231032292714807063421785498174573624803551418615528223109635565"),stof<T>("-18.471971164838981722131453841233089060649464194311893461586142479")}, std::complex<T>{stof<T>("-1.8060261809512156207220481625037935930562688401081771009478357238"),stof<T>("8.5860057406210839747580739222161994862759616092824933712549099219")}, std::complex<T>{stof<T>("-2.301289249236299788229142824963213884607522828012936456197368814"),stof<T>("-23.623397538326071070186375418229279950247593024717894811595338102")}, std::complex<T>{stof<T>("-2.301289249236299788229142824963213884607522828012936456197368814"),stof<T>("-23.623397538326071070186375418229279950247593024717894811595338102")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("-2.3942604334021693618807095331703794821900028915318216865090078087")}, std::complex<T>{stof<T>("-1.9504317785871062505509887843483729006069269154644898723723607619"),stof<T>("2.3942604334021693618807095331703794821900028915318216865090078087")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[4].real()/kbase.W[4].real()), rlog(k.W[5].real()/kbase.W[5].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[21].real()/kbase.W[21].real()), rlog(k.W[27].real()/kbase.W[27].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[122].real()/kbase.W[122].real()), rlog(k.W[128].real()/kbase.W[128].real()), C{T{},(log_iphi_im(k.W[130]) - log_iphi_im(kbase.W[130]))}, C{T{},(log_iphi_im(k.W[131]) - log_iphi_im(kbase.W[131]))}, C{T{},(log_iphi_im(k.W[133]) - log_iphi_im(kbase.W[133]))}, C{T{},(log_iphi_im(k.W[136]) - log_iphi_im(kbase.W[136]))}};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_51_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_51_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-81.701678748927859828747716895376446215485408716061030251777447478"),stof<T>("53.719723605513469360500246047387446211830147738387670947090835096")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,58> abb = {dl[0], dl[1], dl[5], dl[2], dlog_W7(k,dl), dlog_W8(k,dl), dl[3], dlog_W10(k,dl), dl[4], dlog_W15(k,dl), dlog_W22(k,dl), dlog_W27(k,dl), dlog_W28(k,dl), dlog_W30(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W124(k,dv), dlog_W129(k,dv), dlog_W131(k,dv), dlog_W132(k,dv), dlog_W133(k,dv), dlog_W134(k,dv), dlog_W137(k,dv), dlog_W186(k,dv), dlr[1], f_1_1(k), f_1_2(k), f_1_3(k), f_1_4(k), f_1_5(k), f_1_6(k), f_2_3(k), f_2_5(k), f_2_16(k), f_2_17(k), f_2_23(k), f_2_4_im(k), f_2_6_im(k), f_2_7_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[4].real()/k.W[4].real()), rlog(kend.W[5].real()/k.W[5].real()), rlog(kend.W[8].real()/k.W[8].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[21].real()/k.W[21].real()), rlog(kend.W[27].real()/k.W[27].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[122].real()/k.W[122].real()), rlog(kend.W[128].real()/k.W[128].real()), (log_iphi_im(kend.W[130]) - log_iphi_im(k.W[130])), (log_iphi_im(kend.W[131]) - log_iphi_im(k.W[131])), (log_iphi_im(kend.W[133]) - log_iphi_im(k.W[133])), (log_iphi_im(kend.W[136]) - log_iphi_im(k.W[136])), f_2_4_re(k), f_2_6_re(k), f_2_7_re(k)};

                    
            return f_4_51_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_51_DLogXconstant_part(base_point<T>, kend);
	value += f_4_51_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_51_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_51_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_51_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_51_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_51_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_51_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
