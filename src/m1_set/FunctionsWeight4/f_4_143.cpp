/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_143.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_143_abbreviated (const std::array<T,27>& abb) {
T z[39];
z[0] = 2 * abb[20];
z[1] = 2 * abb[19];
z[2] = abb[18] + abb[22];
z[3] = -abb[21] + z[2];
z[3] = -z[0] + z[1] + (T(1) / T(2)) * z[3];
z[3] = abb[0] * z[3];
z[4] = -abb[19] + z[0];
z[5] = abb[18] + abb[21];
z[6] = -abb[22] + 3 * z[5];
z[7] = -z[4] + (T(1) / T(2)) * z[6];
z[8] = abb[6] * z[7];
z[9] = 3 * abb[22] + -z[5];
z[9] = -z[4] + (T(1) / T(2)) * z[9];
z[9] = abb[5] * z[9];
z[10] = -abb[22] + z[5];
z[11] = abb[1] * z[10];
z[12] = 3 * z[11];
z[13] = -abb[18] + abb[22];
z[14] = -abb[19] + z[13];
z[15] = abb[4] * z[14];
z[10] = 2 * z[10];
z[10] = abb[3] * z[10];
z[3] = z[3] + -z[8] + z[9] + z[10] + z[12] + z[15];
z[3] = prod_pow(abb[14], 2) * z[3];
z[9] = abb[7] + abb[8];
z[16] = abb[23] + -abb[24];
z[17] = (T(1) / T(4)) * z[16];
z[9] = z[9] * z[17];
z[17] = abb[19] * (T(1) / T(2));
z[18] = abb[22] * (T(1) / T(2));
z[19] = z[17] + z[18];
z[20] = abb[18] * (T(1) / T(2));
z[21] = abb[21] + z[20];
z[22] = z[19] + -z[21];
z[23] = abb[2] * z[22];
z[9] = z[9] + (T(-1) / T(2)) * z[23];
z[24] = 3 * abb[19];
z[2] = z[2] + z[24];
z[25] = -z[0] + (T(1) / T(2)) * z[2];
z[25] = abb[0] * z[25];
z[26] = 2 * z[11];
z[27] = z[8] + -z[26];
z[28] = 3 * abb[21];
z[29] = abb[19] * (T(5) / T(2));
z[30] = abb[18] * (T(-7) / T(2)) + abb[22] * (T(3) / T(2)) + -z[28] + -z[29];
z[30] = z[0] + (T(1) / T(2)) * z[30];
z[30] = abb[3] * z[30];
z[30] = z[9] + -z[25] + z[27] + z[30];
z[30] = abb[11] * z[30];
z[31] = 4 * abb[20];
z[1] = z[1] + -z[31];
z[6] = z[1] + z[6];
z[32] = abb[6] * z[6];
z[1] = abb[22] + z[1] + z[5];
z[33] = abb[0] * z[1];
z[1] = abb[5] * z[1];
z[33] = z[1] + -z[32] + z[33];
z[34] = z[10] + 4 * z[11] + z[33];
z[35] = abb[14] * z[34];
z[36] = abb[0] * (T(1) / T(2));
z[37] = z[14] * z[36];
z[38] = abb[8] * z[16];
z[23] = z[11] + z[23] + z[37] + (T(-1) / T(2)) * z[38];
z[23] = abb[13] * z[23];
z[23] = z[23] + z[30] + z[35];
z[23] = abb[13] * z[23];
z[30] = -z[0] + z[19] + z[21];
z[30] = abb[0] * z[30];
z[37] = abb[22] * (T(5) / T(2)) + -z[21] + z[29];
z[37] = -z[0] + (T(1) / T(2)) * z[37];
z[37] = abb[3] * z[37];
z[27] = -z[1] + -z[9] + z[27] + -z[30] + z[37];
z[30] = -abb[11] + abb[13];
z[27] = z[27] * z[30];
z[14] = abb[3] * z[14];
z[30] = abb[7] * z[16];
z[14] = z[14] + z[30];
z[8] = z[8] + -z[11];
z[37] = abb[5] * z[7];
z[17] = abb[18] + abb[21] * (T(3) / T(2)) + -z[0] + z[17];
z[17] = abb[0] * z[17];
z[17] = -z[8] + (T(-1) / T(2)) * z[14] + z[17] + z[37];
z[17] = abb[12] * z[17];
z[17] = z[17] + z[27] + -z[35];
z[17] = abb[12] * z[17];
z[27] = abb[2] + abb[3];
z[36] = abb[10] + (T(-3) / T(4)) * z[27] + -z[36];
z[16] = z[16] * z[36];
z[7] = abb[9] * z[7];
z[19] = -z[19] + -3 * z[21];
z[19] = z[0] + (T(1) / T(2)) * z[19];
z[19] = abb[7] * z[19];
z[21] = abb[8] * z[22];
z[7] = z[7] + z[16] + z[19] + (T(3) / T(2)) * z[21];
z[7] = abb[15] * z[7];
z[2] = z[2] + -z[31];
z[2] = abb[0] * z[2];
z[16] = abb[3] * z[6];
z[2] = z[2] + z[12] + -z[15] + z[16] + -z[32];
z[12] = -abb[25] * z[2];
z[16] = abb[19] * (T(3) / T(2)) + abb[18] * (T(5) / T(2)) + -z[18] + z[28];
z[16] = -z[0] + (T(1) / T(2)) * z[16];
z[16] = abb[3] * z[16];
z[8] = -z[8] + z[9] + -z[15] + z[16] + z[25];
z[8] = abb[11] * z[8];
z[8] = z[8] + -z[35];
z[8] = abb[11] * z[8];
z[9] = -abb[26] * z[34];
z[0] = -4 * abb[21] + z[0] + -z[18] + -z[20] + z[29];
z[0] = abb[0] * z[0];
z[4] = abb[22] + z[4] + -2 * z[5];
z[4] = abb[5] * z[4];
z[0] = z[0] + 2 * z[4] + (T(1) / T(2)) * z[11] + (T(5) / T(2)) * z[15];
z[4] = abb[20] * (T(-2) / T(3)) + abb[19] * (T(1) / T(3));
z[11] = abb[22] * (T(-1) / T(3)) + z[5];
z[11] = z[4] + (T(1) / T(2)) * z[11];
z[11] = abb[6] * z[11];
z[5] = -abb[22] + (T(5) / T(3)) * z[5];
z[4] = z[4] + (T(1) / T(2)) * z[5];
z[4] = abb[3] * z[4];
z[0] = (T(1) / T(3)) * z[0] + z[4] + z[11];
z[0] = prod_pow(m1_set::bc<T>[0], 2) * z[0];
z[0] = z[0] + z[3] + z[7] + z[8] + z[9] + z[12] + z[17] + z[23];
z[3] = -abb[19] + 2 * abb[21] + -z[13];
z[4] = -z[3] * z[27];
z[4] = z[4] + 2 * z[15] + -z[26] + -z[30] + -z[33] + -z[38];
z[4] = abb[11] * z[4];
z[5] = z[26] + -z[32];
z[7] = 2 * abb[18] + -z[31];
z[8] = abb[21] + z[7] + z[24];
z[8] = abb[0] * z[8];
z[3] = abb[2] * z[3];
z[1] = z[1] + z[3] + z[5] + z[8] + z[10] + z[38];
z[1] = abb[13] * z[1];
z[3] = -abb[5] * z[6];
z[6] = -abb[19] + -z[7] + -z[28];
z[6] = abb[0] * z[6];
z[3] = z[3] + -z[5] + z[6] + z[14];
z[3] = abb[12] * z[3];
z[1] = z[1] + z[3] + z[4] + z[35];
z[1] = m1_set::bc<T>[0] * z[1];
z[2] = -abb[16] * z[2];
z[3] = -abb[17] * z[34];
z[1] = z[1] + z[2] + z[3];
return {z[0], z[1]};
}


template <typename T> std::complex<T> f_4_143_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("9.3361501135027504031209301615522382541871762358803525092696440927"),stof<T>("-9.7080093829666551411474809698761490242569202917479731588354582807")}, std::complex<T>{stof<T>("5.921725156701037974619703459797355605487446777474280398236655458"),stof<T>("-23.639098640369510675912280605150661454610799369632665324982718648")}, std::complex<T>{stof<T>("-18.321442235757942627263103869895267278697449411163576465215235134"),stof<T>("14.340830424865579725418454432773318982907616715719835527769573338")}, std::complex<T>{stof<T>("12.575146074680683742132778636702516288048454163987860343640606201"),stof<T>("6.760674044970065672055572418887852938900070720024774402262473698")}, std::complex<T>{stof<T>("-0.1754289956237790894893782266046046148384515302985642766620265255"),stof<T>("2.5375941705338652784382537534894895328031119338880553949506716115")}, std::complex<T>{stof<T>("0.5121113943864244331929380064625054643259069593013927369576886889"),stof<T>("-7.8744658461086903567287918060764266500825310082392982705317793672")}, std::complex<T>{stof<T>("-0.5121113943864244331929380064625054643259069593013927369576886889"),stof<T>("7.8744658461086903567287918060764266500825310082392982705317793672")}};
	
	std::vector<C> intdlogs = {rlog(k.W[0].real()/kbase.W[0].real()), rlog(k.W[10].real()/kbase.W[10].real()), rlog(k.W[14].real()/kbase.W[14].real()), rlog(k.W[24].real()/kbase.W[24].real()), rlog(k.W[32].real()/kbase.W[32].real()), rlog(k.W[117].real()/kbase.W[117].real()), rlog(k.W[128].real()/kbase.W[128].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_143_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 



template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_143_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = std::complex<T>{stof<T>("-19.552214692086303146858412872454903714555349689083152329145581435"),stof<T>("-15.783612008983532294683115572035230892438595718108854545605498321")};
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,27> abb = {dl[0], dlog_W4(k,dl), dl[2], dl[4], dlog_W14(k,dl), dlog_W25(k,dl), dlog_W33(k,dl), dlog_W118(k,dv), dlog_W123(k,dv), dlog_W129(k,dv), dlr[1], f_1_1(k), f_1_5(k), f_1_6(k), f_1_11(k), f_2_23(k), f_2_2_im(k), f_2_12_im(k), rlog(kend.W[0].real()/k.W[0].real()), rlog(kend.W[10].real()/k.W[10].real()), rlog(kend.W[14].real()/k.W[14].real()), rlog(kend.W[24].real()/k.W[24].real()), rlog(kend.W[32].real()/k.W[32].real()), rlog(kend.W[117].real()/k.W[117].real()), rlog(kend.W[128].real()/k.W[128].real()), f_2_2_re(k), f_2_12_re(k)};

                    
            return f_4_143_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_143_DLogXconstant_part(base_point<T>, kend);
	value += f_4_143_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_143_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_143_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_143_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_143_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_143_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_143_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
