/**
 * THIS FILE IS GENERATED AUTOMATICALLY
 */
#include "PentagonFunctions_config.h"
#include "f_4_553.h"

#include "PhaseSpacePath.h"

#include "../DLog.h"
#include "../SpDLogQ.h"
#include "../IntDLogL.hpp"
#include "../SpSigma5.h"

#include "FunctionsWeight1.h"
#include "FunctionsWeight2.h"

#include "Integrator/tanh_sinh.hpp"

#include "ChangePrecision.h"

namespace PentagonFunctions {
namespace m1_set {

template <typename T> std::complex<T> 
#ifdef __clang__
    __attribute__((optnone))
#elif defined(__GNUC__)
    __attribute__((optimize("O0")))
#endif
f_4_553_abbreviated (const std::array<T,30>& abb) {
T z[24];
z[0] = -abb[24] + -abb[25] + abb[26];
z[1] = (T(1) / T(2)) * z[0];
z[1] = abb[8] * z[1];
z[2] = abb[7] * z[0];
z[3] = z[1] + (T(1) / T(2)) * z[2];
z[4] = -abb[20] + abb[22];
z[5] = abb[19] + abb[21];
z[6] = z[4] + -z[5];
z[7] = abb[3] * z[6];
z[8] = 3 * abb[19];
z[9] = -abb[23] + z[8];
z[10] = abb[22] * (T(1) / T(2));
z[9] = abb[21] + (T(1) / T(2)) * z[9] + -z[10];
z[11] = abb[6] * z[9];
z[12] = abb[20] + abb[23];
z[13] = abb[21] * (T(1) / T(2));
z[14] = abb[19] + z[13];
z[15] = (T(1) / T(2)) * z[12] + -z[14];
z[16] = -abb[2] * z[15];
z[14] = -z[10] + z[14];
z[14] = abb[1] * z[14];
z[5] = -abb[23] + z[5];
z[5] = abb[4] * z[5];
z[14] = -z[3] + (T(3) / T(2)) * z[5] + (T(-1) / T(2)) * z[7] + -z[11] + z[14] + z[16];
z[14] = abb[11] * z[14];
z[16] = abb[20] + 3 * abb[23];
z[17] = 2 * abb[19];
z[16] = abb[21] * (T(5) / T(4)) + -z[10] + (T(-1) / T(4)) * z[16] + z[17];
z[16] = abb[3] * z[16];
z[18] = abb[0] * (T(1) / T(2));
z[18] = z[15] * z[18];
z[19] = abb[2] * (T(1) / T(2));
z[6] = z[6] * z[19];
z[6] = (T(1) / T(4)) * z[2] + z[6] + -z[18];
z[18] = abb[22] + abb[23];
z[8] = -2 * abb[21] + -z[8] + z[18];
z[20] = abb[1] * z[8];
z[16] = z[6] + -z[11] + z[16] + -z[20];
z[21] = abb[12] + -abb[13];
z[16] = z[16] * z[21];
z[14] = z[14] + -z[16];
z[14] = abb[11] * z[14];
z[21] = abb[23] * (T(1) / T(2));
z[4] = abb[19] * (T(-1) / T(2)) + z[4] + -z[13] + -z[21];
z[4] = abb[3] * z[4];
z[13] = -abb[23] + -z[13] + -z[17];
z[13] = z[10] + (T(1) / T(3)) * z[13];
z[13] = abb[1] * z[13];
z[18] = abb[19] + abb[21] * (T(2) / T(3)) + (T(-1) / T(3)) * z[18];
z[18] = abb[6] * z[18];
z[21] = abb[19] * (T(-5) / T(6)) + abb[21] * (T(-1) / T(3)) + abb[22] * (T(-1) / T(6)) + abb[20] * (T(2) / T(3)) + z[21];
z[21] = abb[2] * z[21];
z[3] = z[3] + (T(1) / T(3)) * z[4] + (T(-4) / T(3)) * z[5] + z[13] + z[18] + z[21];
z[3] = prod_pow(m1_set::bc<T>[0], 2) * z[3];
z[4] = -abb[20] + abb[23];
z[13] = -abb[22] + z[17];
z[18] = abb[21] * (T(-3) / T(2)) + (T(1) / T(2)) * z[4] + -z[13];
z[18] = abb[2] * z[18];
z[15] = abb[0] * z[15];
z[21] = abb[19] + -abb[20];
z[21] = abb[3] * z[21];
z[1] = z[1] + z[15] + z[18] + -z[20] + 2 * z[21];
z[1] = abb[12] * z[1];
z[15] = -abb[20] + 5 * abb[23];
z[15] = -4 * abb[19] + abb[21] * (T(-11) / T(4)) + abb[22] * (T(3) / T(2)) + (T(1) / T(4)) * z[15];
z[15] = abb[3] * z[15];
z[11] = z[6] + z[11] + z[15] + 2 * z[20];
z[11] = abb[13] * z[11];
z[1] = z[1] + z[11];
z[1] = abb[12] * z[1];
z[11] = -abb[0] + -abb[3];
z[15] = (T(3) / T(4)) * z[0];
z[11] = z[11] * z[15];
z[15] = 3 * abb[20] + abb[21] + abb[23];
z[15] = -abb[22] + (T(1) / T(2)) * z[15];
z[18] = abb[7] * (T(1) / T(2));
z[15] = z[15] * z[18];
z[18] = -abb[10] + z[19];
z[18] = z[0] * z[18];
z[19] = -abb[8] + abb[9];
z[19] = z[9] * z[19];
z[11] = z[11] + z[15] + -z[18] + -z[19];
z[15] = -abb[28] * z[11];
z[18] = abb[3] * z[8];
z[8] = abb[6] * z[8];
z[19] = -z[8] + 2 * z[18] + 3 * z[20];
z[20] = -abb[12] * z[19];
z[21] = -abb[23] + z[17];
z[22] = 3 * abb[21];
z[21] = -abb[22] + 2 * z[21] + z[22];
z[21] = abb[1] * z[21];
z[18] = z[5] + z[8] + -z[18] + z[21];
z[21] = -abb[11] * z[18];
z[23] = abb[3] * z[9];
z[22] = 5 * abb[19] + -abb[23] + z[22];
z[22] = -abb[22] + (T(1) / T(2)) * z[22];
z[22] = abb[1] * z[22];
z[22] = (T(-1) / T(2)) * z[5] + z[22] + z[23];
z[22] = abb[14] * z[22];
z[20] = z[20] + z[21] + z[22];
z[20] = abb[14] * z[20];
z[9] = -abb[1] * z[9];
z[4] = -abb[19] + abb[21] * (T(-3) / T(4)) + (T(1) / T(4)) * z[4] + z[10];
z[4] = abb[3] * z[4];
z[4] = z[4] + z[6] + z[9];
z[4] = prod_pow(abb[13], 2) * z[4];
z[6] = abb[15] * z[19];
z[9] = -abb[27] * z[18];
z[1] = abb[29] + z[1] + z[3] + z[4] + z[6] + z[9] + z[14] + z[15] + z[20];
z[3] = -abb[21] + z[12] + -z[17];
z[3] = abb[2] * z[3];
z[4] = -abb[21] + -z[13];
z[4] = abb[1] * z[4];
z[0] = abb[8] * z[0];
z[0] = z[0] + z[2] + z[3] + z[4] + -3 * z[5] + z[7] + -z[8];
z[0] = abb[11] * z[0];
z[2] = abb[14] * z[18];
z[0] = z[0] + z[2] + z[16];
z[0] = m1_set::bc<T>[0] * z[0];
z[2] = -abb[17] * z[11];
z[3] = -abb[16] * z[18];
z[0] = abb[18] + z[0] + z[2] + z[3];
return {z[1], z[0]};
}


template <typename T> std::complex<T> f_4_553_DLogXconstant_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {
	using C = std::complex<T>;
	static std::vector<C> constants = {std::complex<T>{stof<T>("8.4826062472844473500507772560365633551157010987468415924117219089"),stof<T>("4.8233090595606968235023843241994575849967853837846137108416481981")}, std::complex<T>{stof<T>("-12.3283672886785364847232001551944048439285359397906020651212025211"),stof<T>("-1.1320407441903722082871918205716230148170823948741651600313691976")}, std::complex<T>{stof<T>("-3.8457610413940891346724228991578414888128348410437604727094806123"),stof<T>("3.6912683153703246152151925036278345701797029889104485508102790005")}, std::complex<T>{stof<T>("3.8457610413940891346724228991578414888128348410437604727094806123"),stof<T>("-3.6912683153703246152151925036278345701797029889104485508102790005")}, std::complex<T>{stof<T>("-4.6547188104916960897412832574814250337357180128234620229279515134"),stof<T>("6.7898493908076716514634340510446747195333140327511877322391776059")}, std::complex<T>{stof<T>("-4.6547188104916960897412832574814250337357180128234620229279515134"),stof<T>("6.7898493908076716514634340510446747195333140327511877322391776059")}, std::complex<T>{stof<T>("4.6547188104916960897412832574814250337357180128234620229279515134"),stof<T>("-6.7898493908076716514634340510446747195333140327511877322391776059")}};
	
	std::vector<C> intdlogs = {rlog(k.W[3].real()/kbase.W[3].real()), rlog(k.W[7].real()/kbase.W[7].real()), rlog(k.W[8].real()/kbase.W[8].real()), rlog(k.W[31].real()/kbase.W[31].real()), rlog(k.W[119].real()/kbase.W[119].real()), rlog(k.W[120].real()/kbase.W[120].real()), rlog(k.W[126].real()/kbase.W[126].real())};

	C result{};

	for (size_t i = 0; i < constants.size(); ++i) {
		result += (constants[i]*intdlogs[i]);
	}
	
	return result;
}

template <typename T> std::complex<T> f_4_553_IntDLogProduct_part (const Kin<T,KinType::m1>& kbase, const Kin<T,KinType::m1>& k) {return {};} 

template <typename T, typename TABB> T SpDLog_f_4_553_W_20_Re (const T& t, const PhaseSpacePath<KinType::m1,T>& path, const TABB& abb) {
	if (t < detail::SpDLogZeroThreshold<T>) {
		std::array<T,2> c;
        const auto& v = path.path_points.at(1);
        T tend = T(1)/(path.path_points.size()-1);

		c[0] = ((T(3) / T(16)) * (v[1] + v[2] + -v[3] + -v[4]) * (-24 + -8 * v[0] + 3 * v[1] + -5 * v[2] + -7 * v[3] + 5 * v[4] + 4 * m1_set::bc<T>[1] * (4 + 2 * v[0] + -v[1] + v[2] + v[3] + -v[4] + -2 * v[5]) + 8 * v[5])) / prod_pow(tend, 2);
c[1] = ((-1 + 2 * m1_set::bc<T>[1]) * (v[1] + v[2] + -v[3] + -v[4])) / tend;


		return (3 * abb[19] + 2 * abb[21] + -abb[22] + -abb[23]) * (2 * t * c[0] + 3 * c[1]) * (T(1) / T(6));
	}
	{
T z[4];
z[0] = -abb[13] + abb[14];
z[0] = abb[12] * z[0];
z[0] = -abb[15] + z[0];
z[1] = -3 * abb[19] + abb[22] + abb[23];
z[2] = -2 * abb[21] + z[1];
z[0] = z[0] * z[2];
z[2] = -prod_pow(abb[13], 2);
z[3] = prod_pow(abb[14], 2);
z[2] = z[2] + z[3];
z[1] = -abb[21] + (T(1) / T(2)) * z[1];
z[1] = z[1] * z[2];
z[0] = z[0] + z[1];
return abb[5] * z[0];
}

}
template <typename T, typename TABB> T SpDLog_f_4_553_W_20_Im (const T& t, const PhaseSpacePath<KinType::m1,T>& path,  const TABB& ) {return T{0};}

template <typename T> FunctionObjectTypeError<T, KinType::m1> get_f_4_553_evaluator_impl(int max_halvings, const T& tolerance = IntegrationTolerance<T>, std::shared_ptr<std::vector<double>> l1_measures = {}) {
    using namespace boost::math::quadrature;
    tanh_sinh<T> integrator(max_halvings);

    static const std::complex<T> constant = T(0);
    auto evaluator = [integrator = std::move(integrator), tolerance, l1_measures = std::move(l1_measures)](const Kin<T, KinType::m1>& kend) mutable -> integration_result<T> {

        PhaseSpacePath<KinType::m1, T> path{base_point<T>.v, kend.v};
        auto path_break_point = path.get_path_break_points({19});

        auto f_integrand = [&kend, &path](auto t, auto tc) {
            auto [k, dv, dl, dlr] = path(t);

            path.set_divergent_letters(t,tc,k);

            std::array<T,30> abb = {dl[0], dlog_W4(k,dl), dlog_W8(k,dl), dl[3], dlog_W14(k,dl), dlog_W20(k,dl), dlog_W32(k,dl), dlog_W120(k,dv), dlog_W121(k,dv), dlog_W127(k,dv), dlr[3], f_1_1(k), f_1_2(k), f_1_7(k), f_1_11(k), f_2_14(k), f_2_2_im(k), f_2_24_im(k), T{0}, rlog(kend.W[3].real()/k.W[3].real()), rlog(kend.W[7].real()/k.W[7].real()), rlog(kend.W[8].real()/k.W[8].real()), IntDLogL_W_20(t,k,kend,dl), rlog(kend.W[31].real()/k.W[31].real()), rlog(kend.W[119].real()/k.W[119].real()), rlog(kend.W[120].real()/k.W[120].real()), rlog(kend.W[126].real()/k.W[126].real()), f_2_2_re(k), f_2_24_re(k), T{0}};
abb[18] = SpDLog_f_4_553_W_20_Im(t, path, abb);
abb[29] = SpDLog_f_4_553_W_20_Re(t, path, abb);

                    
            return f_4_553_abbreviated(abb);
        };

        auto [value, error] = integrate_multiple_path_segments(std::move(f_integrand), integrator, path_break_point, tolerance, l1_measures);

        value += f_4_553_DLogXconstant_part(base_point<T>, kend);
	value += f_4_553_IntDLogProduct_part(base_point<T>, kend);
        value += constant;

        error = sqrt(error)/to_double(fabs(value));

        return {value,error};
    };

    return evaluator;
}


template <typename T> FunctionObjectType<T, KinType::m1> get_f_4_553_evaluator() {
    auto l1_measures = std::make_shared<std::vector<double>>();
    FunctionObjectTypeError<T,KinType::m1> evaluator = get_f_4_553_evaluator_impl<T>(detail::TanhSinh_MaxHalvings<T>, IntegrationTolerance<T>, l1_measures);

    using TH = raise_precision_t<T>;

    if constexpr (std::is_same_v<T,TH>) { return get_HP_improved_evaluator<T, KinType::m1>(evaluator, FunctionObjectTypeError<TH, KinType::m1>{}); }
    else {
        FunctionObjectTypeError<TH,KinType::m1> evaluator_hp = get_f_4_553_evaluator_impl<TH>(detail::TanhSinh_MaxHalvings<TH>, IntegrationTolerance<T>, l1_measures);
        return get_HP_improved_evaluator<T, KinType::m1>(evaluator, evaluator_hp); 
    }
}

template FunctionObjectType<double, KinType::m1> get_f_4_553_evaluator();
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
template FunctionObjectType<dd_real, KinType::m1> get_f_4_553_evaluator();
template FunctionObjectType<qd_real, KinType::m1> get_f_4_553_evaluator();
#endif


} // namespace m1_set
} // namespace PentagonFunctions
