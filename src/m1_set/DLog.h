#pragma once

#include "Kin.h"
#include "Constants.h"

namespace PentagonFunctions {


template <typename T> std::array<T,3> get_dlog_sqrt_series_1(const T& a, const T& s, const T& da, const T& ds) {
    return {ds / a , (((ds + a * da * T(-2))) / prod_pow(a, 3)), (((ds + a * da * T(-2))) / prod_pow(a, 5))};
}

/**
 *
 * Compute 
 *
 * r * DLog[(a + r)/(a - r)] 
 *
 * with r = sqrt(s) on a line. 
 *
 * Use an expansion if s is small.
 *
 */
template <typename T> T get_dlog_sqrt_1(const T& a, const T& s, const T& da, const T& ds) {
    using std::abs;

    if (abs(s) > ::constants::root_epsilon<T>) {
        return (a * ds - 2 * da * s) / (a * a - s); 
    }

    // we have small s
    auto coeffs = get_dlog_sqrt_series_1(a,s,da,ds);
    T result{coeffs[0]};
    result += (coeffs[1]*s);
    result += (coeffs[2]*s*s);

    return result;
}

template <typename T> std::array<T, 3> get_dlog_sqrt_series_2(const T& a, const T& s1, const T& s2, const T& da, const T& ds1, const T& ds2) {
    T z[7];
    z[0] = 1 / a;
    z[1] = ds1 * z[0];
    z[2] = s2 * z[1];
    z[3] = prod_pow(s2, 2);
    z[4] = z[1] * z[3];
    z[5] = 2 * da;
    z[6] = -s2 * z[5];
    z[4] = z[4] + z[6];
    z[4] = z[0] * z[4];
    z[4] = ds2 + z[4];
    z[4] = z[0] * z[4];
    z[3] = -z[3] * z[5];
    z[1] = prod_pow(s2, 3) * z[1];
    z[1] = z[1] + z[3];
    z[1] = z[0] * z[1];
    z[3] = ds2 * s2;
    z[1] = z[1] + z[3];
    z[1] = prod_pow(z[0], 3) * z[1];
    return {z[2], z[4], z[1]};
}
/**
 *
 * Compute 
 *
 * r1*r2 * DLog[(a + r1 r2)/(a - r1 r2)] 
 *
 * with r1 = sqrt(s1), r2 = sqrt(s2) on a line. 
 *
 * Use an expansion if s is small.
 *
 */
template <typename T> T get_dlog_sqrt_2(const T& a, const T& s1, const T& s2, const T& da, const T& ds1, const T& ds2) {
    using std::abs;

    bool small_s1 = abs(s1) < ::constants::root_epsilon<T>;
    bool small_s2 = abs(s2) < ::constants::root_epsilon<T>;

    if (small_s1 and small_s2) { return (ds2 * s1 + ds1 * s2) / a - 2 * da * s1 * s2 / (a * a); }

    if (small_s1) {

        auto coeffs = get_dlog_sqrt_series_2(a, s1, s2, da, ds1, ds2);
        T result{coeffs[0]};
        result += (coeffs[1] * s1);
        result += (coeffs[2] * s1 * s1);

        return result;
    }

    if (small_s2) {
        auto coeffs = get_dlog_sqrt_series_2(a, s2, s1, da, ds2, ds1);
        T result{coeffs[0]};
        result += (coeffs[1] * s2);
        result += (coeffs[2] * s2 * s2);

        return result;
    }

    return (a * ds2 * s1 + a * ds1 * s2 + -2 * da * s1 * s2) / (-s1 * s2 + prod_pow(a, 2));
}


template <typename T> std::array<T, 3> get_dlog_sqrt_series_3(const T& a, const T& s1, const T& s2, const T& da, const T& ds1, const T& ds2) {
    T z[11];
    z[0] = 1 / (-s2 + prod_pow(a, 2));
    z[1] = z[0] * z[0];
    z[2] = z[1];
    z[1] *= z[0];
    z[2] *= z[1];
    z[3] = 2 * ds1;
    z[4] = -s2 * z[0] * z[3];
    z[5] = prod_pow(a, 2);
    z[6] = a * da;
    z[7] = 4 * z[6];
    z[8] = -3 * ds1 + z[7];
    z[8] = z[5] * z[8];
    z[9] = ds1 + -ds2;
    z[7] = -z[7] + -z[9];
    z[7] = s2 * z[7];
    z[7] = z[7] + z[8];
    z[7] = s2 * z[7];
    z[8] = prod_pow(a, 4);
    z[10] = -ds2 * z[8];
    z[7] = z[7] + z[10];
    z[7] = 2 * z[1] * z[7];
    z[3] = ds2 + -z[3];
    z[3] = z[3] * z[5];
    z[5] = 8 * z[6];
    z[6] = -z[5] + -z[9];
    z[6] = s2 * z[6];
    z[3] = 5 * z[3] + z[6];
    z[3] = s2 * z[3];
    z[6] = -ds1 + -ds2;
    z[5] = z[5] + 5 * z[6];
    z[5] = z[5] * z[8];
    z[3] = z[3] + z[5];
    z[3] = s2 * z[3];
    z[5] = -ds2 * prod_pow(a, 6);
    z[3] = z[3] + z[5];
    z[3] = 2 * z[2] * z[3];
    return {z[4], z[7], z[3]};
}
/**
 *
 * Compute 
 *
 * r1*r2 * DLog[(a - r1 - r2)*(a + r1 + r2)/(a - r1 + r2)/(a + r1 - r2)] 
 *
 * with r1 = sqrt(s1), r2 = sqrt(s2) on a line. 
 *
 * Use an expansion if s is small.
 *
 */
template <typename T> T get_dlog_sqrt_3(const T& a, const T& s1, const T& s2, const T& da, const T& ds1, const T& ds2) {
    using std::abs;

    bool small_s1 = abs(s1) < ::constants::root_epsilon<T>;
    bool small_s2 = abs(s2) < ::constants::root_epsilon<T>;

    if (small_s1 and small_s2) {
        return (-2 * (ds2 * s1 + ds1 * s2)) / prod_pow(a, 2) + (-2 * (-4 * a * da * s1 * s2 + ds1 * s2 * (3 * s1 + s2) + ds2 * s1 * (s1 + 3 * s2))) / prod_pow(a, 4);
    }

    if (small_s1) {

        auto coeffs = get_dlog_sqrt_series_3(a, s1, s2, da, ds1, ds2);
        T result{coeffs[0]};
        result += (coeffs[1] * s1);
        result += (coeffs[2] * s1 * s1);

        return result;
    }

    if (small_s2) {
        auto coeffs = get_dlog_sqrt_series_3(a, s2, s1, da, ds2, ds1);
        T result{coeffs[0]};
        result += (coeffs[1] * s2);
        result += (coeffs[2] * s2 * s2);

        return result;
    }

    {
        T z[4];
        z[0] = 1 / (prod_pow(a, 4) + prod_pow(s1 + -s2, 2) + -2 * (s1 + s2) * prod_pow(a, 2));
        z[1] = a * da;
        z[1] = -ds2 + 4 * z[1];
        z[1] = s1 * z[1];
        z[2] = prod_pow(a, 2);
        z[3] = -s1 + s2 + -z[2];
        z[3] = ds1 * z[3];
        z[1] = z[1] + z[3];
        z[1] = s2 * z[1];
        z[2] = s1 + -z[2];
        z[2] = ds2 * s1 * z[2];
        z[1] = z[1] + z[2];
        return 2 * z[0] * z[1];
    }
}



#ifdef  PENTAGON_FUNCTIONS_M1_ENABLED 

namespace m1_set {

#include "DLog.hpp"

}

#endif //  PENTAGON_FUNCTIONS_M1_ENABLED

}
