// Copyright Nick Thompson, 2017
// Use, modification and distribution are subject to the
// Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt
// or copy at http://www.boost.org/LICENSE_1_0.txt)
//
/**
 * This file was modified by Vasily Sotnikov, 2020
 *
 * Modifications concern:
 * 1. Removing non-essential boost-specific infrastructure.
 * 2. Adding support for dd_real and qd_real types from QD library.
 *
 * Modifications by Vasily Sotnikov, 2021:
 * 1. Pull changes from boost 1.76.0
 * 2. Disable have_small_left/have_small_right. 
 *  If enabled, it leads to evaluation of the integrand exactly at the endpoint sometimes.
 *  Not clear what the goal of this special case was in the first place.
 */

/*
 * This class performs tanh-sinh quadrature on the real line.
 * Tanh-sinh quadrature is exponentially convergent for integrands in Hardy spaces,
 * (see https://en.wikipedia.org/wiki/Hardy_space for a formal definition), and is optimal for a random function from that class.
 *
 * The tanh-sinh quadrature is one of a class of so called "double exponential quadratures"-there is a large family of them,
 * but this one seems to be the most commonly used.
 *
 * As always, there are caveats: For instance, if the function you want to integrate is not holomorphic on the unit disk,
 * then the rapid convergence will be spoiled. In this case, a more appropriate quadrature is (say) Romberg, which does not
 * require the function to be holomorphic, only differentiable up to some order.
 *
 * In addition, if you are integrating a periodic function over a period, the trapezoidal rule is better.
 *
 * References:
 *
 * 1) Mori, Masatake. "Quadrature formulas obtained by variable transformation and the DE-rule." Journal of Computational and Applied Mathematics 12 (1985): 119-130.
 * 2) Bailey, David H., Karthik Jeyabalan, and Xiaoye S. Li. "A comparison of three high-precision quadrature schemes." Experimental Mathematics 14.3 (2005): 317-329.
 * 3) Press, William H., et al. "Numerical recipes third edition: the art of scientific computing." Cambridge University Press 32 (2007): 10013-2473.
 *
 */

#ifndef BOOST_MATH_QUADRATURE_TANH_SINH_HPP
#define BOOST_MATH_QUADRATURE_TANH_SINH_HPP

#include <cmath>
#include <limits>
#include <memory>
#include "tanh_sinh_detail.hpp"

namespace boost{ namespace math{ namespace quadrature {

template<class Real>
class tanh_sinh
{
public:
    tanh_sinh(size_t max_refinements = 15, const Real& min_complement = std::numeric_limits<Real>::min() * 4)
    : m_imp(std::make_shared<detail::tanh_sinh_detail<Real>>(max_refinements, min_complement)) {}

    template<class F>
    auto integrate(const F f, Real a, Real b, Real tolerance = constants::root_epsilon<Real>, Real* error = nullptr, Real* L1 = nullptr, std::size_t* levels = nullptr) ->decltype(std::declval<F>()(std::declval<Real>())) const;
    template<class F>
    auto integrate(const F f, Real a, Real b, Real tolerance = constants::root_epsilon<Real>, Real* error = nullptr, Real* L1 = nullptr, std::size_t* levels = nullptr) ->decltype(std::declval<F>()(std::declval<Real>(), std::declval<Real>())) const;

    template<class F>
    auto integrate(const F f, Real tolerance = constants::root_epsilon<Real>, Real* error = nullptr, Real* L1 = nullptr, std::size_t* levels = nullptr) ->decltype(std::declval<F>()(std::declval<Real>())) const;
    template<class F>
    auto integrate(const F f, Real tolerance = constants::root_epsilon<Real>, Real* error = nullptr, Real* L1 = nullptr, std::size_t* levels = nullptr) ->decltype(std::declval<F>()(std::declval<Real>(), std::declval<Real>())) const;

private:
    std::shared_ptr<detail::tanh_sinh_detail<Real>> m_imp;
};

template<class Real>
template<class F>
auto tanh_sinh<Real>::integrate(const F f, Real a, Real b, Real tolerance, Real* error, Real* L1, std::size_t* levels) ->decltype(std::declval<F>()(std::declval<Real>())) const
{
    using std::isnan;
    using std::isfinite;
    using constants::half;
    using boost::math::quadrature::detail::tanh_sinh_detail;

    static const char* function = "tanh_sinh<%1%>::integrate";

    typedef decltype(std::declval<F>()(std::declval<Real>())) result_type;

    if (!isnan(a) && !isnan(b))
    {

       // Infinite limits:
       if ((a <= -std::numeric_limits<Real>::max()) && (b >= std::numeric_limits<Real>::max()))
       {
          auto u = [&](const Real& t, const Real& tc)->result_type
          { 
             Real t_sq = t*t; 
             Real inv;
             if (t > 0.5f)
                inv = 1 / ((2 - tc) * tc);
             else if(t < -0.5)
                inv = 1 / ((2 + tc) * -tc);
             else
                inv = 1 / (1 - t_sq);
             return f(t*inv)*(1 + t_sq)*inv*inv; 
          };
          Real limit = sqrt(std::numeric_limits<Real>::min()) * 4;
          return m_imp->integrate(u, error, L1, function, limit, limit, tolerance, levels);
       }

       // Right limit is infinite:
       if (isfinite(a) && (b >= std::numeric_limits<Real>::max()))
       {
          auto u = [&](const Real& t, const Real& tc)->result_type
          { 
             Real z, arg;
             if (t > -0.5f)
                z = 1 / (t + 1);
             else
                z = -1 / tc;
             if (t < 0.5)
                arg = 2 * z + a - 1;
             else
                arg = a + tc / (2 - tc);
             return f(arg)*z*z; 
          };
          Real left_limit = sqrt(std::numeric_limits<Real>::min()) * 4;
          result_type Q = Real(2) * m_imp->integrate(u, error, L1, function, left_limit, std::numeric_limits<Real>::min(), tolerance, levels);
          if (L1)
          {
             *L1 *= 2;
          }
          if (error)
          {
             *error *= 2;
          }

          return Q;
       }

       if (isfinite(b) && (a <= -std::numeric_limits<Real>::max()))
       {
          auto v = [&](const Real& t, const Real& tc)->result_type
          { 
             Real z;
             if (t > -0.5)
                z = 1 / (t + 1);
             else
                z = -1 / tc;
             Real arg;
             if (t < 0.5)
                arg = 2 * z - 1;
             else
                arg = tc / (2 - tc);
             return f(b - arg) * z * z;
          };

          Real left_limit = sqrt(std::numeric_limits<Real>::min()) * 4;
          result_type Q = Real(2) * m_imp->integrate(v, error, L1, function, left_limit, std::numeric_limits<Real>::min(), tolerance, levels);
          if (L1)
          {
             *L1 *= 2;
          }
          if (error)
          {
             *error *= 2;
          }
          return Q;
       }

       if (isfinite(a) && isfinite(b))
       {
           using std::nextafter;
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED 
           using detail::nextafter;
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED

          if (a == b)
          {
             return result_type(0);
          }
          if (b < a)
          {
             return -this->integrate(f, b, a, tolerance, error, L1, levels);
          }
          Real avg = (a + b)*half<Real>;
          Real diff = (b - a)*half<Real>;
          Real avg_over_diff_m1 = a / diff;
          Real avg_over_diff_p1 = b / diff;
          /*bool have_small_left = fabs(a) < 0.5f;*/
          /*bool have_small_right = fabs(b) < 0.5f;*/
          Real left_min_complement = nextafter(avg_over_diff_m1, std::numeric_limits<Real>::max()) - avg_over_diff_m1;
          Real min_complement_limit = std::max(static_cast<Real>(std::numeric_limits<Real>::min()), static_cast<Real>(std::numeric_limits<Real>::min() / diff));
          if (left_min_complement < min_complement_limit)
             left_min_complement = min_complement_limit;
          Real right_min_complement = avg_over_diff_p1 - nextafter(avg_over_diff_p1, -(std::numeric_limits<Real>::max()));
          if (right_min_complement < min_complement_limit)
             right_min_complement = min_complement_limit;
          //
          // These asserts will fail only if rounding errors on
          // type Real have accumulated so much error that it's
          // broken our internal logic.  Should that prove to be
          // a persistent issue, we might need to add a bit of fudge
          // factor to move left_min_complement and right_min_complement
          // further from the end points of the range.
          //
          assert((left_min_complement * diff + a) > a);
          assert((b - right_min_complement * diff) < b);
          auto u = [&](Real z, Real zc)->result_type
          { 
             Real position;
             if (z < -0.5)
             {
                /*if(have_small_left)*/
                  /*return f(diff * (avg_over_diff_m1 - zc));*/
                position = a - diff * zc;
             }
             else if (z > 0.5)
             {
                /*if(have_small_right)*/
                  /*return f(diff * (avg_over_diff_p1 - zc));*/
                position = b - diff * zc;
             }
             else
                position = avg + diff*z;
             assert(position != a);
             assert(position != b);
             return f(position);
          };
          result_type Q = diff*m_imp->integrate(u, error, L1, function, left_min_complement, right_min_complement, tolerance, levels);

          if (L1)
          {
             *L1 *= diff;
          }
          if (error)
          {
             *error *= diff;
          }
          return Q;
       }
    }
    throw std::runtime_error("The domain of integration is not sensible; please check the bounds.");
}

template<class Real>
template<class F>
auto tanh_sinh<Real>::integrate(const F f, Real a, Real b, Real tolerance, Real* error, Real* L1, std::size_t* levels) ->decltype(std::declval<F>()(std::declval<Real>(), std::declval<Real>())) const
{
    using boost::math::quadrature::detail::tanh_sinh_detail;
    using constants::half;
    using std::isfinite;

    static const char* function = "tanh_sinh<%1%>::integrate";

    typedef decltype(std::declval<F>()(std::declval<Real>(), std::declval<Real>())) result_type;

    if (isfinite(a) && isfinite(b)) {
        using std::nextafter;
#ifdef PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED
        using detail::nextafter;
#endif // PENTAGON_FUNCTIONS_HIGH_PRECISION_ENABLED

        if (a == b) { return result_type(0); }

        if (b < a) { return -this->integrate(f, b, a, tolerance, error, L1, levels); }

        Real avg = (a + b) * half<Real>;
        Real diff = (b - a) * half<Real>;
        Real avg_over_diff_m1 = a / diff;
        Real avg_over_diff_p1 = b / diff;
        /*bool have_small_left = fabs(a) < 0.5f;*/
        /*bool have_small_right = fabs(b) < 0.5f;*/
        Real left_min_complement = nextafter(avg_over_diff_m1, std::numeric_limits<Real>::max()) - avg_over_diff_m1;
        Real min_complement_limit = std::max(static_cast<Real>(std::numeric_limits<Real>::min()), static_cast<Real>(std::numeric_limits<Real>::min() / diff));
        if (left_min_complement < min_complement_limit) left_min_complement = min_complement_limit;
        Real right_min_complement = avg_over_diff_p1 - nextafter(avg_over_diff_p1, -(std::numeric_limits<Real>::max()));
        if (right_min_complement < min_complement_limit) right_min_complement = min_complement_limit;
        //
        // These asserts will fail only if rounding errors on
        // type Real have accumulated so much error that it's
        // broken our internal logic.  Should that prove to be
        // a persistent issue, we might need to add a bit of fudge
        // factor to move left_min_complement and right_min_complement
        // further from the end points of the range.
        //
        assert((left_min_complement * diff + a) > a);
        assert((b - right_min_complement * diff) < b);

        auto u = [&](Real z, Real zc) -> result_type {
            Real position;
            if (z < -0.5) {
                /*if(have_small_left)*/
                /*return f(diff * (avg_over_diff_m1 - zc));*/
                position = a - diff * zc;
            }
            else if (z > 0.5) {
                /*if(have_small_right)*/
                /*return f(diff * (avg_over_diff_p1 - zc));*/
                position = b - diff * zc;
            }
            else {
                position = avg + diff * z;
            }

            assert(position != a);
            assert(position != b);

            return f(position, diff * zc);
        };

        result_type Q = diff * m_imp->integrate(u, error, L1, function, left_min_complement, right_min_complement, tolerance, levels);

        if (L1) { *L1 *= diff; }
        if (error) { *error *= diff; }
        return Q;
    }
    throw std::runtime_error("The domain of integration is not sensible; please check the bounds.");
}

template<class Real>
template<class F>
auto tanh_sinh<Real>::integrate(const F f, Real tolerance, Real* error, Real* L1, std::size_t* levels) ->decltype(std::declval<F>()(std::declval<Real>())) const
{
   using boost::math::quadrature::detail::tanh_sinh_detail;
   static const char* function = "tanh_sinh<%1%>::integrate";
   Real min_complement = std::numeric_limits<Real>::epsilon();
   return m_imp->integrate([&](const Real& arg, const Real&) { return f(arg); }, error, L1, function, min_complement, min_complement, tolerance, levels);
}

template<class Real>
template<class F>
auto tanh_sinh<Real>::integrate(const F f, Real tolerance, Real* error, Real* L1, std::size_t* levels) ->decltype(std::declval<F>()(std::declval<Real>(), std::declval<Real>())) const
{
   using boost::math::quadrature::detail::tanh_sinh_detail;
   static const char* function = "tanh_sinh<%1%>::integrate";
   Real min_complement = std::numeric_limits<Real>::min() * 4;
   return m_imp->integrate(f, error, L1, function, min_complement, min_complement, tolerance, levels);
}

}
}
}
#endif
