# PentagonFunctions++ Python3 Interface

## Requirements
```
numpy, mpmath, lips
```

## Installation
In the parent folder type:
```
pip install -e .
```
then the package can be imported as
```
from pentagon_functions import evaluate_pentagon_functions
from pentagon_monomial import PentagonMonomial
```
