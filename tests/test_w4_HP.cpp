#include "catch.hpp"

#include "Kin.h"
#include "Constants.h"
#include "FunctionID.h"
#include "test_utilities.h"

namespace PentagonFunctions {

#include "test_point1.hpp"

template <typename T>
const std::vector<std::pair<FunctionID, std::complex<T>>> targets =
#include "targets_w4.hpp"
;

} // PentagonFunctions



TEST_CASE("Weight 4 functions quad precision", "[W4:quad]"){
    using namespace PentagonFunctions;
    using T = std::complex<dd_real>;
    using TR = typename T::value_type;

    TR worst_accuracy = 100;

    size_t i = 0;

    for(auto& fi_data : targets<TR>){
        ++i;
        // let's check only third of them here
        if(i%3!=0) continue;

        FunctionID fID = fi_data.first;
        T target = fi_data.second;

        auto f = fID.get_evaluator<TR>();

        T value = f(test_point<TR>);

        CHECK(check(value, target, 28));

        TR accuracy = compute_accuracy(value, target);
        if (accuracy < worst_accuracy) worst_accuracy = accuracy;
    }

    std::cout << "Worst accuracy: " << worst_accuracy << std::endl;
}

