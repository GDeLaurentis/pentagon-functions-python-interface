#include "catch.hpp"

#include "Kin.h"
#include "Constants.h"
#include "FunctionID.h"
#include "test_utilities.h"

namespace PentagonFunctions {
    
template <typename T>
const Kin<T,KinType::m1> test_point {stof<T>("153.6336165048543689320388349514563106796116504854368932038834951456311"),stof<T>("369.4884403343410990574426462742308376311577449759914636315134269962653"),stof<T>("103.2909137620964288772801233193457223601952556307270703091547486511947"),stof<T>("-70.45251361672822888624553934764915795404745508044825643273023226695048"),stof<T>("666"),stof<T>("-45.97237858358423877470873150935986385652572326220709516952480691189946")};

template <typename T>
const std::vector<std::pair<FunID<KinType::m1>, std::complex<T>>> targets =
#include "targets_m1_w3.hpp"
;

} // PentagonFunctions



TEST_CASE("Weight 3 functions, double precision", "[M1:W3:double]"){
    using namespace PentagonFunctions;

    using T = std::complex<double>;
    using TR = typename T::value_type;

    TR worst_accuracy = 100;

    for(auto& fi_data : targets<TR>){
        FunID<KinType::m1> fID = fi_data.first;
        T target = fi_data.second;

        std::cout << fID << "\n";

        auto f = fID.get_evaluator<TR>();

        T value = f(test_point<TR>);

        CHECK(check(value, target, 12.5));

        TR accuracy = compute_accuracy(value, target);
        if (accuracy < worst_accuracy) worst_accuracy = accuracy;
    }

    std::cout << "Worst accuracy: " << worst_accuracy << std::endl;
}
