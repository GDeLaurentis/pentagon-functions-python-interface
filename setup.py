import subprocess

from pathlib import Path

from setuptools import setup, find_packages
from setuptools.command.build_ext import build_ext

this_directory = Path(__file__).parent


class MesonBuildExt(build_ext):
    def run(self):
        # Configure the Meson build directory
        build_dir = this_directory / 'build'
        build_dir.mkdir(parents=True, exist_ok=True)

        # Check if Meson is already configured in build_dir
        if not (build_dir / 'meson-private').exists():
            # Run Meson setup outside build_dir - TODO: improve, e.g. if QD is available
            print("\nRunning Meson setup:")
            meson_cmd = ['meson', 'setup', str(build_dir), f'-Dprefix={build_dir}']
            subprocess.run(meson_cmd, check=True, capture_output=False, text=True)
        else:
            print("\nMeson setup already complete; skipping reconfiguration.")

        # Run Ninja build inside build_dir
        print("\nRunning Ninja build:")
        ninja_cmd = ['ninja', '-C', str(build_dir)]
        subprocess.run(ninja_cmd, check=True, capture_output=False, text=True)

        # Run Ninja install inside build_dir
        print("\nRunning Ninja install:")
        install_cmd = ['ninja', '-C', str(build_dir), 'install']
        subprocess.run(install_cmd, check=True, capture_output=False, text=True)


setup(
    name='pentagon_functions',
    version='v0.0.1',
    author='Giuseppe De Laurentis, and the Pentagon Functions authors',
    packages=find_packages(),
    include_package_data=True,
    install_requires=['numpy',
                      'mpmath',
                      'lips',],
    extras_require={
        'with-cpp': ['meson', 'ninja']
    },
    cmdclass={'build_ext': MesonBuildExt} if {'meson', 'ninja'}.issubset(set(x.key for x in __import__('pkg_resources').working_set)) else {},
)
